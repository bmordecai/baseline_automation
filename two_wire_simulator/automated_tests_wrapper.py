import os

from common.imports import opcodes
import common.user_configuration as user_conf_module

# 1000 use cases
# import bs1000_scripts.use_case_2 as cuc_1000_2_module
# import bs1000_scripts.use_case_3 as cuc_1000_3_module
# import bs1000_scripts.use_case_4 as cuc_1000_4_module
# import bs1000_scripts.use_case_5 as cuc_1000_5_module
# import bs1000_scripts.use_case_6 as cuc_1000_6_module
# import bs1000_scripts.use_case_7 as cuc_1000_7_module

# 3200 use cases
import bs3200_scripts.use_case_1 as cuc_3200_1_module
import bs3200_scripts.use_case_2 as cuc_3200_2_module
import bs3200_scripts.use_case_3 as cuc_3200_3_module
import bs3200_scripts.use_case_4 as cuc_3200_4_module
import bs3200_scripts.use_case_5 as cuc_3200_5_module
import bs3200_scripts.use_case_5_1 as cuc_3200_5_1_module
import bs3200_scripts.use_case_5_2 as cuc_3200_5_2_module
import bs3200_scripts.use_case_5_3 as cuc_3200_5_3_module
import bs3200_scripts.use_case_5_4 as cuc_3200_5_4_module
import bs3200_scripts.use_case_6 as cuc_3200_6_module
import bs3200_scripts.use_case_7 as cuc_3200_7_module
import bs3200_scripts.use_case_8 as cuc_3200_8_module
import bs3200_scripts.use_case_9 as cuc_3200_9_module
import bs3200_scripts.use_case_10 as cuc_3200_10_module
import bs3200_scripts.use_case_11 as cuc_3200_11_module
import bs3200_scripts.use_case_12 as cuc_3200_12_module
import bs3200_scripts.use_case_13 as cuc_3200_13_module
import bs3200_scripts.use_case_14 as cuc_3200_14_module
import bs3200_scripts.use_case_15 as cuc_3200_15_module
import bs3200_scripts.use_case_16 as cuc_3200_16_module
import bs3200_scripts.use_case_17 as cuc_3200_17_module
import bs3200_scripts.use_case_18 as cuc_3200_18_module
import bs3200_scripts.use_case_19 as cuc_3200_19_module
import bs3200_scripts.use_case_20 as cuc_3200_20_module
import bs3200_scripts.use_case_20_1 as cuc_3200_20_1_module
import bs3200_scripts.use_case_20_2 as cuc_3200_20_2_module
import bs3200_scripts.use_case_20_3 as cuc_3200_20_3_module
import bs3200_scripts.use_case_20_4 as cuc_3200_20_4_module
import bs3200_scripts.use_case_20_5 as cuc_3200_20_5_module
import bs3200_scripts.use_case_21 as cuc_3200_21_module
import bs3200_scripts.use_case_22 as cuc_3200_22_module
import bs3200_scripts.use_case_23 as cuc_3200_23_module
import bs3200_scripts.use_case_24 as cuc_3200_24_module
import bs3200_scripts.use_case_25 as cuc_3200_25_module
import bs3200_scripts.use_case_26 as cuc_3200_26_module
import bs3200_scripts.use_case_27 as cuc_3200_27_module
import bs3200_scripts.use_case_28 as cuc_3200_28_module
import bs3200_scripts.use_case_29 as cuc_3200_29_module
import bs3200_scripts.use_case_30 as cuc_3200_30_module
import bs3200_scripts.use_case_31 as cuc_3200_31_module
import bs3200_scripts.use_case_32 as cuc_3200_32_module
import bs3200_scripts.use_case_33 as cuc_3200_33_module
import bs3200_scripts.use_case_34 as cuc_3200_34_module
import bs3200_scripts.use_case_35 as cuc_3200_35_module
import bs3200_scripts.use_case_36 as cuc_3200_36_module
import bs3200_scripts.use_case_37 as cuc_3200_37_module
import bs3200_scripts.use_case_38 as cuc_3200_38_module
import bs3200_scripts.use_case_39 as cuc_3200_39_module
import bs3200_scripts.use_case_40 as cuc_3200_40_module
import bs3200_scripts.use_case_41 as cuc_3200_41_module
import bs3200_scripts.use_case_43 as cuc_3200_43_module
import bs3200_scripts.use_case_43_1 as cuc_3200_43_1_module
import bs3200_scripts.use_case_44 as cuc_3200_44_module
import bs3200_scripts.use_case_44_1 as cuc_3200_44_1_module
import bs3200_scripts.use_case_44_2 as cuc_3200_44_2_module
import bs3200_scripts.use_case_44_3 as cuc_3200_44_3_module
import bs3200_scripts.use_case_44_4 as cuc_3200_44_4_module
import bs3200_scripts.use_case_44_5 as cuc_3200_44_5_module
import bs3200_scripts.use_case_44_6 as cuc_3200_44_6_module
import bs3200_scripts.use_case_44_7 as cuc_3200_44_7_module
import bs3200_scripts.use_case_45 as cuc_3200_45_module
import bs3200_scripts.use_case_46 as cuc_3200_46_module
import bs3200_scripts.use_case_47 as cuc_3200_47_module
import bs3200_scripts.use_case_47_1 as cuc_3200_47_1_module
import bs3200_scripts.use_case_48 as cuc_3200_48_module
import bs3200_scripts.use_case_49 as cuc_3200_49_module
import bs3200_scripts.use_case_50 as cuc_3200_50_module
import bs3200_scripts.use_case_52 as cuc_3200_52_module
import bs3200_scripts.use_case_53 as cuc_3200_53_module
import bs3200_scripts.use_case_54 as cuc_3200_54_module
import bs3200_scripts.use_case_54_1 as cuc_3200_54_1_module
import bs3200_scripts.use_case_54_2 as cuc_3200_54_2_module
import bs3200_scripts.use_case_54_3 as cuc_3200_54_3_module
import bs3200_scripts.use_case_55 as cuc_3200_55_module
import bs3200_scripts.use_case_56 as cuc_3200_56_module
import bs3200_scripts.use_case_57_1 as cuc_3200_57_1_module
import bs3200_scripts.use_case_57_2 as cuc_3200_57_2_module
import bs3200_scripts.use_case_57_3 as cuc_3200_57_3_module
import bs3200_scripts.use_case_57_4 as cuc_3200_57_4_module
import bs3200_scripts.use_case_58 as cuc_3200_58_module
import bs3200_scripts.use_case_59 as cuc_3200_59_module
import bs3200_scripts.use_case_60 as cuc_3200_60_module


# base manager use cases
import bmw_bs3200_v16_scripts.use_case_1 as uc1_module
# import bmw_scripts.use_case_2 as uc2_module
import bmw_bs3200_v16_scripts.use_case_3 as uc3_module
import bmw_bs3200_v16_scripts.use_case_4 as uc4_module
import bmw_bs3200_v16_scripts.use_case_7 as uc7_module
# import bmw_scripts.use_case_5 as uc5_module
# import bmw_scripts.use_case_6 as uc6_module
# import bmw_scripts.use_case_6 as uc6_module

# jade use cases
# import bs_foo_1000_aka_jade.use_case_1 as cuc_jade_1_module
# import bs_foo_1000_aka_jade.use_case_2 as cuc_jade_2_module
# import bs_foo_1000_aka_jade.use_case_3 as cuc_jade_3_module
# import bs_foo_1000_aka_jade.use_case_4 as cuc_jade_4_module
# import bs_foo_1000_aka_jade.use_case_5 as cuc_jade_5_module
# import bs_foo_1000_aka_jade.use_case_6 as cuc_jade_6_module
# import bs_foo_1000_aka_jade.use_case_7 as cuc_jade_7_module
# import bs_foo_1000_aka_jade.use_case_8 as cuc_jade_8_module
# import bs_foo_1000_aka_jade.use_case_9 as cuc_jade_9_module
# import bs_foo_1000_aka_jade.use_case_10 as cuc_jade_10_module
# import bs_foo_1000_aka_jade.use_case_11 as cuc_jade_11_module
# import bs_foo_1000_aka_jade.use_case_12 as cuc_jade_12_module
# import bs_foo_1000_aka_jade.use_case_13 as cuc_jade_13_module
# import bs_foo_1000_aka_jade.use_case_14 as cuc_jade_14_module
# import bs_foo_1000_aka_jade.use_case_15 as cuc_jade_15_module
# import bs_foo_1000_aka_jade.use_case_16 as cuc_jade_16_module
# import bs_foo_1000_aka_jade.use_case_17 as cuc_jade_17_module
# import bs_foo_1000_aka_jade.use_case_18 as cuc_jade_18_module

# bacnet use cases
import bacnet_scripts.use_case_1 as cuc_bacnet_1_module
import bacnet_scripts.use_case_2 as cuc_bacnet_2_module

# Substation use cases
import bs_substation_scripts.uc_1_timed_zones as cuc_substation_1_module
import bs_substation_scripts.uc_2_soak_cycles as cuc_substation_2_module
import bs_substation_scripts.uc_3_disconnect_reconnect as cuc_substation_3_module
import bs_substation_scripts.uc_4_basic_programming as cuc_substation_4_module
import bs_substation_scripts.uc_5_replace_devices as cuc_substation_5_module
import bs_substation_scripts.uc_6_concurrent_zones as cuc_substation_6_module
import bs_substation_scripts.uc_7_move_devices as cuc_substation_7_module
import bs_substation_scripts.uc_8_learn_flow as cuc_substation_8_module
import bs_substation_scripts.uc_9_messages as cuc_substation_9_module
import bs_substation_scripts.uc_10_backup_restore as cuc_substation_10_module
import bs_substation_scripts.uc_11_device_bug_test as cuc_substation_11_module


_author__ = 'Eldin'


def run_use_cases(firmware_version_1000,
                  firmware_version_3200,
                  firmware_version_substation,
                  user_configuration_file_name,
                  tests_to_run_1000,
                  tests_to_run_jade,
                  tests_to_run_bacnet,
                  tests_to_run_base_manager,
                  tests_to_run_3200,
                  tests_to_run_substation,
                  serial_number_3200_controller='3K10001'):
    """
    This method is used to run all of our use cases and allow the person running them to step away from the computer

    :param firmware_version_1000: The firmware version of the 1000 controller you will be testing. \n
    :type firmware_version_1000: str

    :param firmware_version_3200: The firmware version of the 3200 controller you will be testing. \n
    :type firmware_version_3200: str

    :param firmware_version_substation: The firmware version of the substation you will be testing. \n
    :type firmware_version_substation: str

    :param user_configuration_file_name: The name of the configuration file we will be using for the tests. \n
    :type user_configuration_file_name: str

    :param tests_to_run_1000: A list with the number of the use cases we want to run, or 'opcodes.run_all_use_cases' \n
                                    Ex: [1, 5, 6, 13] would run use cases 1, 5, 6, and 13
    :type tests_to_run_1000: list[int] | str

    :param tests_to_run_jade: A list with the number of the use cases we want to run, or 'opcodes.run_all_use_cases' \n
    :type tests_to_run_jade: list[int] | str

    :param tests_to_run_bacnet: A list with the number of the use cases we want to run, or 'opcodes.run_all_use_cases' \n
    :type tests_to_run_bacnet: list[int] | str

    :param tests_to_run_base_manager: A list with the number of the use cases we want to run, or 'opcodes.run_all_use_cases' \n
    :type tests_to_run_base_manager: list[int] | str

    :param tests_to_run_3200: A list with the number of the use cases we want to run, or 'opcodes.run_all_use_cases' \n
    :type tests_to_run_3200: list[int] | str

    :param tests_to_run_substation: A list with the number of the use cases we want to run, or 'opcodes.run_all_use_cases' \n
    :type tests_to_run_substation: list[int] | str

    :param serial_number_3200_controller: The serial number that you would like assigned to the controller. \n
    :type serial_number_3200_controller: str
    """

    # Load in user configured items from text file (necessary for serial connection)
    user_conf = user_conf_module.UserConfiguration(os.path.join('common/user_credentials', user_configuration_file_name))

    # ---------------------------------------------------------------------------------------------------------------- #
    #     1000 Tests                                                                                                   #
    # ---------------------------------------------------------------------------------------------------------------- #

    # if tests_to_run_1000 is opcodes.run_all_use_cases or 2 in tests_to_run_1000:
    #     uc2 = cuc_1000_2_module.ControllerUseCase2(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="EPATestConfiguration",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='EPA_test_configuration.json')
    #
    #     uc2.run_use_case()
    #
    # if tests_to_run_1000 is opcodes.run_all_use_cases or 3 in tests_to_run_1000:
    #     uc3 = cuc_1000_3_module.ControllerUseCase3(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="SetMessages",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='set_messages.json')
    #
    #     uc3.run_use_case()
    #
    # if tests_to_run_1000 is opcodes.run_all_use_cases or 4 in tests_to_run_1000:
    #     uc4 = cuc_1000_4_module.ControllerUseCase4(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="NelsonFeatureProgramCycles",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='nelson_features.json')
    #
    #     uc4.run_use_case()
    #
    # if tests_to_run_1000 is opcodes.run_all_use_cases or 5 in tests_to_run_1000:
    #     uc5 = cuc_1000_5_module.ControllerUseCase5(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="NelsonFeatureOver15Concurrent",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='nelson_features.json')
    #
    #     uc5.run_use_case()
    #
    # if tests_to_run_1000 is opcodes.run_all_use_cases or 6 in tests_to_run_1000:
    #     uc6 = cuc_1000_6_module.ControllerUseCase6(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="NelsonMasterValesWithZones",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='nelson_features.json')
    #
    #     uc6.run_use_case()
    #
    # if tests_to_run_1000 is opcodes.run_all_use_cases or 7 in tests_to_run_1000:
    #     uc7 = cuc_1000_7_module.ControllerUseCase7(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="NelsonFeatureMirroredZones",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='nelson_features.json')
    #
    #     uc7.run_use_case()

    # ---------------------------------------------------------------------------------------------------------------- #
    #    Jade Tests                                                                                                    #
    # ---------------------------------------------------------------------------------------------------------------- #

    # if tests_to_run_jade is opcodes.run_all_use_cases or 1 in tests_to_run_jade:
    #     uc1 = cuc_jade_1_module.ControllerUseCase1(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="BasicLearnFlowTest",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='basic_flow_test.json')
    #
    #     uc1.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 2 in tests_to_run_jade:
    #     uc2 = cuc_jade_2_module.ControllerUseCase2(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="BasicDesignFlowTest",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='basic_flow_test.json')
    #
    #     uc2.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 3 in tests_to_run_jade:
    #     uc3 = cuc_jade_3_module.ControllerUseCase3(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="BasicProgrammingTest",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='basic_programming.json')
    #
    #     uc3.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 4 in tests_to_run_jade:
    #     uc4 = cuc_jade_4_module.ControllerUseCase4(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="ConcurrentZonesTest",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='concurrent_zones.json')
    #
    #     uc4.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 5 in tests_to_run_jade:
    #     uc5 = cuc_jade_5_module.ControllerUseCase5(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="EventSwitchTest",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='event_decoder_test.json')
    #
    #     uc5.run_use_case()
    # #     TODO fails step 9 line 408 program is watering when it should be paused
    # # if tests_to_run_jade is opcodes.run_all_use_cases or 6 in tests_to_run_jade:
    # #     uc6 = cuc_jade_6_module.ControllerUseCase6(controller_type="10",
    # #                                                controller_firmware_version="1.17",
    # #                                                fw_database_id="88",
    # #                                                test_name="MoistureDecoderTest",
    # #                                                user_configuration_instance=user_conf,
    # #                                                json_configuration_file='moisture_decoder_test.json')
    # #
    # #     uc6.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 7 in tests_to_run_jade:
    #     uc7 = cuc_jade_7_module.ControllerUseCase7(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="TemperatureDecoderTest",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='temperature_decoder_test.json')
    #
    #     uc7.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 8 in tests_to_run_jade:
    #     uc8 = cuc_jade_8_module.ControllerUseCase8(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="StartTimesTest",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='start_times_test.json')
    #
    #     uc8.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 9 in tests_to_run_jade:
    #     uc9 = cuc_jade_9_module.ControllerUseCase9(controller_type="10",
    #                                                controller_firmware_version=firmware_version_1000,
    #                                                fw_database_id="88",
    #                                                test_name="MasterValvePumpTest",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='mv_pump_test.json')
    #
    #     uc9.run_use_case()
    # #     TODO This test needs a complete rewrite
    # # if tests_to_run_jade is opcodes.run_all_use_cases or 10 in tests_to_run_jade:
    # #     uc10 = cuc_jade_10_module.ControllerUseCase10(controller_type="10",
    # #                                                   controller_firmware_version="0.171",
    # #                                                   fw_database_id="88",
    # #                                                   test_name="OneTimeCalibrationTest",
    # #                                                   user_configuration_instance=user_conf,
    # #                                                   json_configuration_file='calibration_tests.json')
    # #
    # #     uc10.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 11 in tests_to_run_jade:
    #     uc11 = cuc_jade_11_module.ControllerUseCase11(controller_type="10",
    #                                                   controller_firmware_version=firmware_version_1000,
    #                                                   fw_database_id="88",
    #                                                   test_name="ReplacingDevicesTest",
    #                                                   user_configuration_instance=user_conf,
    #                                                   json_configuration_file='replacing_devices_test.json')
    #
    #     uc11.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 12 in tests_to_run_jade:
    #     uc12 = cuc_jade_12_module.ControllerUseCase12(controller_type="10",
    #                                                   controller_firmware_version=firmware_version_1000,
    #                                                   fw_database_id="88",
    #                                                   test_name="SoakCycles",
    #                                                   user_configuration_instance=user_conf,
    #                                                   json_configuration_file='soak_cycles_test.json')
    #
    #     uc12.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 13 in tests_to_run_jade:
    #     uc13 = cuc_jade_13_module.ControllerUseCase13(controller_type="10",
    #                                                   controller_firmware_version=firmware_version_1000,
    #                                                   fw_database_id="88",
    #                                                   test_name="WateringDayScheduleTest",
    #                                                   user_configuration_instance=user_conf,
    #                                                   json_configuration_file='watering_day_schedule_test.json')
    #
    #     uc13.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 14 in tests_to_run_jade:
    #     uc14 = cuc_jade_14_module.ControllerUseCase14(controller_type="10",
    #                                                   controller_firmware_version=firmware_version_1000,
    #                                                   fw_database_id="88",
    #                                                   test_name="UpperLimitTest",
    #                                                   user_configuration_instance=user_conf,
    #                                                   json_configuration_file='upper_limit_watering.json')
    #
    #     uc14.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 15 in tests_to_run_jade:
    #     uc15 = cuc_jade_15_module.ControllerUseCase15(controller_type="10",
    #                                                   controller_firmware_version=firmware_version_1000,
    #                                                   fw_database_id="88",
    #                                                   test_name="AlertRelayTest",
    #                                                   user_configuration_instance=user_conf,
    #                                                   json_configuration_file='alert_relay.json')
    #
    #     uc15.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 16 in tests_to_run_jade:
    #     uc16 = cuc_jade_16_module.ControllerUseCase16(controller_type="10",
    #                                                   controller_firmware_version=firmware_version_1000,
    #                                                   fw_database_id="88",
    #                                                   test_name="TimedZonesWithSoakCycles",
    #                                                   user_configuration_instance=user_conf,
    #                                                   json_configuration_file='timed_zones_with_soak_cycles.json')
    #
    #     uc16.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 17 in tests_to_run_jade:
    #     uc17 = cuc_jade_17_module.ControllerUseCase17(controller_type="10",
    #                                                   controller_firmware_version=firmware_version_1000,
    #                                                   fw_database_id="88",
    #                                                   test_name="CNUseCase17firmwareupdate",
    #                                                   user_configuration_instance=user_conf,
    #                                                   json_configuration_file='back_restore_firmware_updates.json')
    #
    #     uc17.run_use_case()
    #
    # if tests_to_run_jade is opcodes.run_all_use_cases or 18 in tests_to_run_jade:
    #     uc18 = cuc_jade_18_module.ControllerUseCase18(controller_type="10",
    #                                                   controller_firmware_version=firmware_version_1000,
    #                                                   fw_database_id="88",
    #                                                   test_name="CNUseCase18backuprestoreprogramming",
    #                                                   user_configuration_instance=user_conf,
    #                                                   json_configuration_file='back_restore_firmware_updates.json')
    #
    #     uc18.run_use_case()

    # ---------------------------------------------------------------------------------------------------------------- #
    #     3200 Tests                                                                                                   #
    # ---------------------------------------------------------------------------------------------------------------- #

    if tests_to_run_3200 is opcodes.run_all_use_cases or 1 in tests_to_run_3200:
        uc1 = cuc_3200_1_module.ControllerUseCase1(test_name="CN-UseCase1-RebootConfigVerify",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='update_cn.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 2 in tests_to_run_3200:
        uc2 = cuc_3200_2_module.ControllerUseCase2(test_name="CN-UseCase2-EPATestConfiguration",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='EPA_test_configuration.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 3 in tests_to_run_3200:
        uc3 = cuc_3200_3_module.ControllerUseCase3(test_name="CN-UseCase3-SetMessages",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='set_messages.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 4 in tests_to_run_3200:
        uc4 = cuc_3200_4_module.ControllerUseCase4(test_name="CN-UseCase4-LowerLimitOneTimeCalibration",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='calibration_tests.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 5 in tests_to_run_3200:
        uc5 = cuc_3200_5_module.ControllerUseCase5(test_name="CN-UseCase5-POC_empty_conditions",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='empty_conditions.json')
        cuc_3200_5_1_module.ControllerUseCase5_1(test_name="CN-UseCase5_1-WS_empty_conditions",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='water_budget.json')
        cuc_3200_5_2_module.ControllerUseCase5_2(test_name="CN-UseCase5_2-Water_Budget_2",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='water_budget.json')
        cuc_3200_5_3_module.ControllerUseCase5_3(test_name="CN-UseCase5_3-Water_Budget_3",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='water_budget.json')
        # cuc_3200_5_4_module.ControllerUseCase5_4(test_name="CN-UseCase5_4-WS_empty_conditions",
        #                                          user_configuration_instance=user_conf,
        #                                          json_configuration_file='water_budget.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 6 in tests_to_run_3200:
        uc6 = cuc_3200_6_module.ControllerUseCase6(test_name="CN-UseCase6-check cn serial number",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='check_cn_serial_number.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 7 in tests_to_run_3200:
        uc7 = cuc_3200_7_module.ControllerUseCase7(test_name="CN-UseCase7-multi ssp with event decoders",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='multiple_ssp_with_event_decoders.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 8 in tests_to_run_3200:
        uc8 = cuc_3200_8_module.ControllerUseCase8(test_name="CN-UseCase8-multi ssp with all devices",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='multiple_ssp_with_all_devices.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 9 in tests_to_run_3200:
        cuc_3200_9_module.ControllerUseCase9(test_name="CN-UseCase9-memory_usage",
                                             user_configuration_instance=user_conf,
                                             json_configuration_file='ram_test_3200.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 10 in tests_to_run_3200:
        uc10 = cuc_3200_10_module.ControllerUseCase10(test_name="CN-UseCase10-SeasonalAdjust",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='seasonal_adjust.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 11 in tests_to_run_3200:
        cuc_3200_11_module.ControllerUseCase11(test_name="CNUseCase11-firmwareupdate",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='back_restore_firmware_updates.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 12 in tests_to_run_3200:
        uc12 = cuc_3200_12_module.ControllerUseCase12(test_name="CN-UseCase12-backuprestoreprograming",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='back_restore_firmware_updates_no_et.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 13 in tests_to_run_3200:
        uc13 = cuc_3200_13_module.ControllerUseCase13(test_name="CN-UseCase13-learnflowmultiplemainlines",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='learn_flow_multiple_mainlines.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 14 in tests_to_run_3200:
        uc14 = cuc_3200_14_module.ControllerUseCase14(test_name="CN-UseCase14-lowflowvariance",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='flow_variance.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 15 in tests_to_run_3200:
        uc15 = cuc_3200_15_module.ControllerUseCase15(test_name="CN-UseCase15-basicProgramming",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='basic_programming.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 16 in tests_to_run_3200:
        uc16 = cuc_3200_16_module.ControllerUseCase16(test_name="CN-UseCase16-timedZones",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='timed_zones_with_soak_cycles.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 17 in tests_to_run_3200:
        uc17 = cuc_3200_17_module.ControllerUseCase17(test_name="CN-UseCase17-soak_cycles",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='timed_zones_with_soak_cycles.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 18 in tests_to_run_3200:
        uc18 = cuc_3200_18_module.ControllerUseCase18(test_name="CN-UseCase18-basic_design_flow",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='basic_flow_test.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 19 in tests_to_run_3200:
        uc19 = cuc_3200_19_module.ControllerUseCase19(test_name="CN-UseCase19-highflowvariance",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='flow_variance.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 20 in tests_to_run_3200:
        cuc_3200_20_module.ControllerUseCase20(test_name="CN-UseCase20-AdvancedFlowVariance",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='advanced_flow_variance.json')
        cuc_3200_20_1_module.ControllerUseCase20_1(test_name="CN-UseCase20_1-AdvancedHiFlowVarianceTier1",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='advanced_flow_variance.json')
        cuc_3200_20_2_module.ControllerUseCase20_2(test_name="CN-UseCase20_2-AdvancedLoFlowVarianceTier1",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='advanced_flow_variance.json')
        cuc_3200_20_3_module.ControllerUseCase20_3(test_name="CN-UseCase20_3-AdvancedHiFlowVarianceTier2",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='advanced_flow_variance.json')
        cuc_3200_20_4_module.ControllerUseCase20_4(test_name="CN-UseCase20_4-AdvancedLoFlowVarianceTier2",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='advanced_flow_variance.json')
        cuc_3200_20_5_module.ControllerUseCase20_5(test_name="CN-UseCase20_5-MLStrikeRuleAdvVariance",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='advanced_flow_variance_20_5.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 21 in tests_to_run_3200:
        uc21 = cuc_3200_21_module.ControllerUseCase21(test_name="CN-UseCase21-StartTimes",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='start_times_test.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 22 in tests_to_run_3200:
        cuc_3200_22_module.ControllerUseCase22(test_name="CN-UseCase22-MainlinesUpdateV16",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='use_case_22.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 23 in tests_to_run_3200:
        cuc_3200_23_module.ControllerUseCase23(test_name="CN-UseCase23-Connect/Disconnect BM",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='updating_to_v16.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 24 in tests_to_run_3200:
        uc24 = cuc_3200_24_module.ControllerUseCase24(test_name="CN-UseCase24-PrimaryZoneTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='primary_zone_test.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 25 in tests_to_run_3200:
        uc25 = cuc_3200_25_module.ControllerUseCase25(test_name="CN-UseCase25-PrimaryZoneZeroRuntime",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='updating_to_v16.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 26 in tests_to_run_3200:
        uc26 = cuc_3200_26_module.ControllerUseCase26(test_name="CN-UseCase26-UpdatePrimaryZoneZeroRuntime",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='updating_to_v16.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 27 in tests_to_run_3200:
        uc27 = cuc_3200_27_module.ControllerUseCase27(test_name="CN-UseCase27-WateringDays",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='watering_day_schedule_test.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 28 in tests_to_run_3200:
        uc28 = cuc_3200_28_module.ControllerUseCase28(test_name="CN-UseCase28-WaterWindows",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='water_windows.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 29 in tests_to_run_3200:
        uc29 = cuc_3200_29_module.ControllerUseCase29(test_name="CN-UseCase29-EventDecoderTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='event_decoder_test.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 30 in tests_to_run_3200:
        uc30 = cuc_3200_30_module.ControllerUseCase30(test_name="CN-UseCase30-TemperatureDecoderTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='temperature_decoder_test.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 31 in tests_to_run_3200:
        uc31 = cuc_3200_31_module.ControllerUseCase31(test_name="CN-UseCase31-ConcurrentZonesTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='concurrent_zones.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 32 in tests_to_run_3200:
        uc32 = cuc_3200_32_module.ControllerUseCase32(test_name="CN-UseCase32-MoistureDecoderTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='moisture_decoder_test.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 33 in tests_to_run_3200:
        uc33 = cuc_3200_33_module.ControllerUseCase33(test_name="CN-UseCase33-MvPumpTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='mv_pump_test.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 34 in tests_to_run_3200:
        uc34 = cuc_3200_34_module.ControllerUseCase34(test_name="CN-UseCase34-BasicLearnFlow",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='basic_learn_flow_test.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 35 in tests_to_run_3200:
        uc35 = cuc_3200_35_module.ControllerUseCase35(test_name="CN-UseCase35-LargeConfigTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='large_configuration_programming.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 36 in tests_to_run_3200:
        uc36 = cuc_3200_36_module.ControllerUseCase36(test_name="CN-UseCase36-UpperLimitWateringTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='upper_limit_watering.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 37 in tests_to_run_3200:
        uc37 = cuc_3200_37_module.ControllerUseCase37(test_name="CN-UseCase37-LowerLimitWateringTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='lower_limit_watering.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 38 in tests_to_run_3200:
        uc38 = cuc_3200_38_module.ControllerUseCase38(test_name="CN-UseCase38-RebootBugTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='reboot_bug_test.json')

    # TODO: Whole test is commented out.
    # if tests_to_run_3200 is opcodes.run_all_use_cases or 39 in tests_to_run_3200:
    #     cuc_3200_39_module.ControllerUseCase39(test_name="CN-UseCase39-MoistureSensorWatering",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='reboot_bug_test.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 40 in tests_to_run_3200:
        cuc_3200_40_module.ControllerUseCase40(test_name="CNUseCase40-firmwareupdatefromlocaldir",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='back_restore_firmware_updates.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 41 in tests_to_run_3200:
        cuc_3200_41_module.ControllerUseCase41(test_name="CN-UseCase41-RebootConfigVerify",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='update_cn.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 43 in tests_to_run_3200:
        cuc_3200_43_module.ControllerUseCase43(test_name="CN-UseCase43-verify_data_group_packets",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='update_cn.json')
        cuc_3200_43_1_module.ControllerUseCase43_1(test_name="CN-UseCase43_1-verify_data_group_packets",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='update_cn.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 44 in tests_to_run_3200:
        cuc_3200_44_module.ControllerUseCase44(test_name="CN-UseCase44-BasicMainlineZoneDelays",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='basic_mainline_zone_delays.json')
        cuc_3200_44_1_module.ControllerUseCase44_1(test_name="CN-UseCase44_1-BasicMainlineZoneDelays",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_mainline_zone_delays.json')
        cuc_3200_44_2_module.ControllerUseCase44_2(test_name="CN-UseCase44_2-BasicMainlineZoneDelays",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_mainline_zone_delays.json')
        cuc_3200_44_3_module.ControllerUseCase44_3(test_name="CN-UseCase44_3-BasicMainlineZoneDelays",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_mainline_zone_delays.json')
        # cuc_3200_44_4_module.ControllerUseCase44_4(test_name="CN-UseCase44_4-BasicMainlineZoneDelays",
        #                                            user_configuration_instance=user_conf,
        #                                            json_configuration_file='basic_mainline_zone_delays.json')
        cuc_3200_44_5_module.ControllerUseCase44_5(test_name="CN-UseCase44_5-BasicMainlineZoneDelays",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_mainline_zone_delays.json')
        cuc_3200_44_6_module.ControllerUseCase44_6(test_name="CN-UseCase44_6-BasicMainlineZoneDelays",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_mainline_zone_delays.json')
        cuc_3200_44_7_module.ControllerUseCase44_7(test_name="CN-UseCase44_7-BasicMainlineZoneDelays",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_mainline_zone_delays.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 45 in tests_to_run_3200:
        cuc_3200_45_module.ControllerUseCase45(test_name="CN-UseCase45-DCLatchSerialNumbers",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='dc_latching_decoder_serial_numbers.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 46 in tests_to_run_3200:
        cuc_3200_46_module.ControllerUseCase46(test_name="CN-UseCase46-AssignDevices",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='assign_devices.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 47 in tests_to_run_3200:
        cuc_3200_47_module.ControllerUseCase47(test_name="CN-UseCase47-V12ToV16FirmwareUpdate",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='back_restore_firmware_updates.json')
        cuc_3200_47_1_module.ControllerUseCase47_1(test_name="CN-UseCase47_1-Local/BaseManagerFirmware",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='empty_configuration.json')
        
    if tests_to_run_3200 is opcodes.run_all_use_cases or 48 in tests_to_run_3200:
        cuc_3200_48_module.ControllerUseCase48(test_name="CN-UseCase48-BasicLocalDirectoryUpdate",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='empty_configuration.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 49 in tests_to_run_3200:
        cuc_3200_49_module.ControllerUseCase49(test_name="CN-UseCase49-VerifyProgramOverrunLogic",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='program_overrun_logic.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 50 in tests_to_run_3200:
        cuc_3200_50_module.ControllerUseCase50(test_name="CN-UseCase50-ProgramOverRuns",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='water_windows.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 52 in tests_to_run_3200:
        cuc_3200_52_module.ControllerUseCase52(test_name="CN-UseCase52-BasicSearchAssignRealDevices",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='real_devices.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 53 in tests_to_run_3200:
        cuc_3200_53_module.ControllerUseCase53(test_name="CN-UseCase53-Rain_Jumper",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='empty_conditions.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 54 in tests_to_run_3200:
        cuc_3200_54_module.ControllerUseCase54(test_name="CN-UseCase54-POCPressureShutdown",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='empty_conditions.json')
        cuc_3200_54_1_module.ControllerUseCase54_1(test_name="CN-UseCase54_1-POCPressureShutdown",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='empty_conditions.json')
        cuc_3200_54_2_module.ControllerUseCase54_2(test_name="CN-UseCase54_2-POCHighFlowShutdown",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='empty_conditions.json')
        cuc_3200_54_3_module.ControllerUseCase54_3(test_name="CN-UseCase54_3-POCUnscheduledFlowShutdown",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='empty_conditions.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 55 in tests_to_run_3200:
        cuc_3200_55_module.ControllerUseCase55(test_name="CN-UseCase55-Flow_Jumper",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='empty_conditions.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 56 in tests_to_run_3200:
        cuc_3200_56_module.ControllerUseCase56(test_name="CN-UseCase56-Pause_Jumper",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='empty_conditions.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 57 in tests_to_run_3200:
        cuc_3200_57_1_module.ControllerUseCase57_1(test_name="CN-UseCase57_1-IgnoreStopPauseConditions",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='ignore_global.json')
        cuc_3200_57_2_module.ControllerUseCase57_2(test_name="CN-UseCase57_2-IgnoreEventDates",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='ignore_global.json')
        cuc_3200_57_3_module.ControllerUseCase57_3(test_name="CN-UseCase57_3-IgnoreRainDays",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='ignore_global.json')
        cuc_3200_57_4_module.ControllerUseCase57_4(test_name="CN-UseCase57_4-IgnoreJumpers",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='ignore_global.json')

    if tests_to_run_3200 is opcodes.run_all_use_cases or 58 in tests_to_run_3200:
        cuc_3200_58_module.ControllerUseCase58(test_name="CN-UseCase58-MultiSSPWithPressureSensor",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='multiple_ssp_with_pressure_sensors.json')
    if tests_to_run_3200 is opcodes.run_all_use_cases or 59 in tests_to_run_3200:
        cuc_3200_59_module.ControllerUseCase59(test_name="CN-UseCase59-IgnoreConcurrentZones",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='ignore_global.json')
    if tests_to_run_3200 is opcodes.run_all_use_cases or 59 in tests_to_run_3200:
        cuc_3200_60_module.ControllerUseCase60(test_name="CN-UseCase60-dynamic_flow",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='basic_flow_test.json')

    # TODO: Need to add 1000 and v12 3200 BM tests

    # ---------------------------------------------------------------------------------------------------------------- #
    #     BaseManager Tests (v16 3200)                                                                                 #
    # ---------------------------------------------------------------------------------------------------------------- #

    if tests_to_run_base_manager is opcodes.run_all_use_cases or 1 in tests_to_run_base_manager:
        uc1_module.BaseManagerUseCase1(test_name="BM-UseCase1-LoginTest-3200",
                                       user_configuration_instance=user_conf,
                                       json_configuration_file='update_cn.json')

    if tests_to_run_base_manager is opcodes.run_all_use_cases or 3 in tests_to_run_base_manager:
        uc3_module.BaseManagerUseCase3(test_name="BM-UseCase3-MenuTabTest-3200",
                                       user_configuration_instance=user_conf,
                                       json_configuration_file='update_cn.json')

    if tests_to_run_base_manager is opcodes.run_all_use_cases or 4 in tests_to_run_base_manager:
        uc4_module.BaseManagerUseCase4(test_name="BM-UseCase4-AddressDevTest-3200",
                                       user_configuration_instance=user_conf,
                                       json_configuration_file='update_cn.json')

    if tests_to_run_base_manager is opcodes.run_all_use_cases or 7 in tests_to_run_base_manager:
        uc7_module.BaseManagerUseCase7(test_name="BM-UseCase7-VerifyPocUI-v16-3200",
                                       user_configuration_instance=user_conf,
                                       json_configuration_file='update_cn.json')

    # ---------------------------------------------------------------------------------------------------------------- #
    #      BacNet Tests                                                                                                #
    # ---------------------------------------------------------------------------------------------------------------- #

    if tests_to_run_bacnet is opcodes.run_all_use_cases or 1 in tests_to_run_bacnet:
        uc1 = cuc_bacnet_1_module.ControllerUseCase1(test_name="CN-UseCase1-Bacnet-Alarms",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='set_messages.json')

    if tests_to_run_bacnet is opcodes.run_all_use_cases or 2 in tests_to_run_bacnet:
        uc1 = cuc_bacnet_2_module.ControllerUseCase2(test_name="CN-UseCase2-Find-Controller",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='empty_configuration.json')



    # ---------------------------------------------------------------------------------------------------------------- #
    #      SubStation Tests                                                                                            #
    # ---------------------------------------------------------------------------------------------------------------- #

    if tests_to_run_substation is opcodes.run_all_use_cases or 1 in tests_to_run_substation:
        cuc_substation_1_module.UseCase1TimedZones(
            test_name="SB UC 1 - Timed Zones",
            user_configuration_instance=user_conf,
            json_configuration_file='SB_timed_zones.json')

    if tests_to_run_substation is opcodes.run_all_use_cases or 2 in tests_to_run_substation:
        cuc_substation_2_module.UseCase2SoakCycles(
            test_name="SB UC 2 - Soak Cycles",
            user_configuration_instance=user_conf,
            json_configuration_file='SB_soak_cycles.json')

    if tests_to_run_substation is opcodes.run_all_use_cases or 3 in tests_to_run_substation:
        cuc_substation_3_module.UseCase3DisconnectReconnect(
            test_name="SB UC 3 - Disconnect/Reconnect",
            user_configuration_instance=user_conf,
            json_configuration_file='SB_timed_zones.json')

    if tests_to_run_substation is opcodes.run_all_use_cases or 4 in tests_to_run_substation:
        cuc_substation_4_module.UseCase4BasicProgramming(
            test_name="SB UC 4-Basic Programming",
            user_configuration_instance=user_conf,
            json_configuration_file="SB_basic_programming.json")

    if tests_to_run_substation is opcodes.run_all_use_cases or 5 in tests_to_run_substation:
        cuc_substation_5_module.UseCase5ReplaceDevices(
            test_name="SB UC 5 - Replace Devices",
            user_configuration_instance=user_conf,
            json_configuration_file="SB_replace_devices.json")

    if tests_to_run_substation is opcodes.run_all_use_cases or 6 in tests_to_run_substation:
        cuc_substation_6_module.UseCase6ConcurrentZones(
            test_name="SB UC 6 - Concurrent Zones",
            user_configuration_instance=user_conf,
            json_configuration_file="SB_concurrent_zones.json")

    if tests_to_run_substation is opcodes.run_all_use_cases or 7 in tests_to_run_substation:
        cuc_substation_7_module.UseCase7MoveDevices(
            test_name="SB UC 7 - Move Devices",
            user_configuration_instance=user_conf,
            json_configuration_file="SB_move_devices.json")

    if tests_to_run_substation is opcodes.run_all_use_cases or 8 in tests_to_run_substation:
        cuc_substation_8_module.UseCase8LearnFlow(
            test_name="SB UC 8 - Learn Flow",
            user_configuration_instance=user_conf,
            json_configuration_file="SB_learn_flow.json")

    if tests_to_run_substation is opcodes.run_all_use_cases or 9 in tests_to_run_substation:
        cuc_substation_9_module.UseCase9Messages(
            test_name="SB UC 9 - Messages",
            user_configuration_instance=user_conf,
            json_configuration_file="SB_messages.json")

    if tests_to_run_substation is opcodes.run_all_use_cases or 10 in tests_to_run_substation:
        # TODO: THIS USE CASE 10 REQUIRES A BASEMANAGER CONNECTION & USB PLUGGED INTO 3200.
        # TODO: TO NOT TEST USING BASEMANAGER, COMMENT OUT LINES 152, 153, 154
        # TODO: TO NOT TEST USING USB, COMMENT OUT LINES 157, 158, 159
        cuc_substation_10_module.UseCase10BackupRestore(
            test_name="SB UC 10 - Backup Restore",
            user_configuration_instance=user_conf,
            json_configuration_file="SB_backup_restore.json",
            use_basemanager_for_backup=True,
            use_usb_for_backup=True)

    if tests_to_run_substation is opcodes.run_all_use_cases or 11 in tests_to_run_substation:
        cuc_substation_11_module.SubStationUseCase11(
            test_name="SB UC 11 - Device Bug",
            user_configuration_instance=user_conf,
            json_configuration_file=None)

    print("######################### ----YOU ARE A WINNER WINNER CHICKEN DINNER---- ##################################")
