import os

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
import old_32_10_sb_objects_dec_29_2017.common.user_configuration as user_conf_module

# 1000 use cases
import old_32_10_sb_objects_dec_29_2017.bs1000_scripts.use_case_2 as cuc_1000_2_module
import old_32_10_sb_objects_dec_29_2017.bs1000_scripts.use_case_3 as cuc_1000_3_module
import old_32_10_sb_objects_dec_29_2017.bs1000_scripts.use_case_4 as cuc_1000_4_module
import old_32_10_sb_objects_dec_29_2017.bs1000_scripts.use_case_5 as cuc_1000_5_module
import old_32_10_sb_objects_dec_29_2017.bs1000_scripts.use_case_6 as cuc_1000_6_module
import old_32_10_sb_objects_dec_29_2017.bs1000_scripts.use_case_7 as cuc_1000_7_module

# 3200 use cases
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_1 as cuc_3200_1_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_2 as cuc_3200_2_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_3 as cuc_3200_3_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_4 as cuc_3200_4_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_5 as cuc_3200_5_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_6 as cuc_3200_6_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_7 as cuc_3200_7_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_8 as cuc_3200_8_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_10 as cuc_3200_10_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_12 as cuc_3200_12_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_13 as cuc_3200_13_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_14 as cuc_3200_14_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_15 as cuc_3200_15_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_16 as cuc_3200_16_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_17 as cuc_3200_17_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_18 as cuc_3200_18_module
import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_19 as cuc_3200_19_module

# base manager use cases
import bmw_bs3200_v12_scripts.uc1_login_test as uc1_module
import old_32_10_sb_objects_dec_29_2017.bmw_bs1000_v1_scripts.use_case_2 as uc2_module
import bmw_bs3200_v12_scripts.uc3_menu_tab_test as uc3_module
import bmw_bs3200_v12_scripts.uc4_address_devices_test as uc4_module
import old_32_10_sb_objects_dec_29_2017.bmw_bs1000_v1_scripts.use_case_5 as uc5_module
import old_32_10_sb_objects_dec_29_2017.bmw_bs1000_v1_scripts.use_case_6 as uc6_module

# jade use cases
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_1 as cuc_jade_1_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_2 as cuc_jade_2_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_3 as cuc_jade_3_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_4 as cuc_jade_4_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_5 as cuc_jade_5_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_7 as cuc_jade_7_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_8 as cuc_jade_8_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_9 as cuc_jade_9_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_11 as cuc_jade_11_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_12 as cuc_jade_12_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_13 as cuc_jade_13_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_14 as cuc_jade_14_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_15 as cuc_jade_15_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_16 as cuc_jade_16_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_17 as cuc_jade_17_module
import old_32_10_sb_objects_dec_29_2017.bs_foo_1000_aka_jade.use_case_18 as cuc_jade_18_module

# bacnet use cases
import bacnet_scripts.use_case_1 as cuc_bacnet_1_module
import bacnet_scripts.use_case_2 as cuc_bacnet_2_module
import bacnet_scripts.use_case_3 as cuc_bacnet_3_module
import bacnet_scripts.use_case_4 as cuc_bacnet_4_module

# Substation use cases
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_1_timed_zones as cuc_substation_1_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_2_soak_cycles as cuc_substation_2_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_3_disconnect_reconnect as cuc_substation_3_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_4_basic_programming as cuc_substation_4_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_5_replace_devices as cuc_substation_5_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_6_concurrent_zones as cuc_substation_6_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_7_move_devices as cuc_substation_7_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_8_learn_flow as cuc_substation_8_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_9_messages as cuc_substation_9_module

_author__ = 'Eldin'


def run_use_cases(firmware_version_1000,
                  firmware_version_3200,
                  firmware_version_substation,
                  user_configuration_file_name,
                  tests_to_run_1000,
                  tests_to_run_jade,
                  tests_to_run_bacnet,
                  tests_to_run_base_manager,
                  tests_to_run_3200,
                  tests_to_run_substation,
                  serial_number_3200_controller='3K10001'):
    """
    This method is used to run all of our use cases and allow the person running them to step away from the computer

    :param firmware_version_1000: The firmware version of the 1000 controller you will be testing. \n
    :type firmware_version_1000: str

    :param firmware_version_3200: The firmware version of the 3200 controller you will be testing. \n
    :type firmware_version_3200: str

    :param firmware_version_substation: The firmware version of the substation you will be testing. \n
    :type firmware_version_substation: str

    :param user_configuration_file_name: The name of the configuration file we will be using for the tests. \n
    :type user_configuration_file_name: str

    :param tests_to_run_1000: A list with the number of the use cases we want to run, or 'opcodes.run_all_use_cases' \n
                                    Ex: [1, 5, 6, 13] would run use cases 1, 5, 6, and 13
    :type tests_to_run_1000: list[int] | str

    :param tests_to_run_jade: A list with the number of the use cases we want to run, or 'opcodes.run_all_use_cases' \n
    :type tests_to_run_jade: list[int] | str

    :param tests_to_run_bacnet: A list with the number of the use cases we want to run, or 'opcodes.run_all_use_cases' \n
    :type tests_to_run_bacnet: list[int] | str

    :param tests_to_run_base_manager: A list with the number of the use cases we want to run, or 'opcodes.run_all_use_cases' \n
    :type tests_to_run_base_manager: list[int] | str

    :param tests_to_run_3200: A list with the number of the use cases we want to run, or 'opcodes.run_all_use_cases' \n
    :type tests_to_run_3200: list[int] | str

    :param tests_to_run_substation: A list with the number of the use cases we want to run, or 'opcodes.run_all_use_cases' \n
    :type tests_to_run_substation: list[int] | str

    :param serial_number_3200_controller: The serial number that you would like assigned to the controller. \n
    :type serial_number_3200_controller: str
    """

    # Load in user configured items from text file (necessary for serial connection)
    user_conf = user_conf_module.UserConfiguration(os.path.join('common/user_credentials', user_configuration_file_name))

    # ---------------------------------------------------------------------------------------------------------------- #
    #     1000 Tests                                                                                                   #
    # ---------------------------------------------------------------------------------------------------------------- #

    if tests_to_run_1000 is opcodes.run_all_use_cases or 2 in tests_to_run_1000:
        uc2 = cuc_1000_2_module.ControllerUseCase2(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="EPATestConfiguration",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='EPA_test_configuration.json')

        uc2.run_use_case()

    if tests_to_run_1000 is opcodes.run_all_use_cases or 3 in tests_to_run_1000:
        uc3 = cuc_1000_3_module.ControllerUseCase3(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="SetMessages",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='set_messages.json')

        uc3.run_use_case()

    if tests_to_run_1000 is opcodes.run_all_use_cases or 4 in tests_to_run_1000:
        uc4 = cuc_1000_4_module.ControllerUseCase4(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="NelsonFeatureProgramCycles",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='nelson_features.json')

        uc4.run_use_case()

    if tests_to_run_1000 is opcodes.run_all_use_cases or 5 in tests_to_run_1000:
        uc5 = cuc_1000_5_module.ControllerUseCase5(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="NelsonFeatureOver15Concurrent",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='nelson_features.json')

        uc5.run_use_case()

    if tests_to_run_1000 is opcodes.run_all_use_cases or 6 in tests_to_run_1000:
        uc6 = cuc_1000_6_module.ControllerUseCase6(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="NelsonMasterValesWithZones",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='nelson_features.json')

        uc6.run_use_case()

    if tests_to_run_1000 is opcodes.run_all_use_cases or 7 in tests_to_run_1000:
        uc7 = cuc_1000_7_module.ControllerUseCase7(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="NelsonFeatureMirroredZones",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='nelson_features.json')

        uc7.run_use_case()

    # ---------------------------------------------------------------------------------------------------------------- #
    #    Jade Tests                                                                                                    #
    # ---------------------------------------------------------------------------------------------------------------- #

    if tests_to_run_jade is opcodes.run_all_use_cases or 1 in tests_to_run_jade:
        uc1 = cuc_jade_1_module.ControllerUseCase1(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="BasicLearnFlowTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_flow_test.json')

        uc1.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 2 in tests_to_run_jade:
        uc2 = cuc_jade_2_module.ControllerUseCase2(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="BasicDesignFlowTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_flow_test.json')

        uc2.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 3 in tests_to_run_jade:
        uc3 = cuc_jade_3_module.ControllerUseCase3(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="BasicProgrammingTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_programming.json')

        uc3.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 4 in tests_to_run_jade:
        uc4 = cuc_jade_4_module.ControllerUseCase4(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="ConcurrentZonesTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='concurrent_zones.json')

        uc4.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 5 in tests_to_run_jade:
        uc5 = cuc_jade_5_module.ControllerUseCase5(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="EventSwitchTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='event_decoder_test.json')

        uc5.run_use_case()
    #     TODO fails step 9 line 408 program is watering when it should be paused
    # if tests_to_run_jade is opcodes.run_all_use_cases or 6 in tests_to_run_jade:
    #     uc6 = cuc_jade_6_module.ControllerUseCase6(controller_type="10",
    #                                                controller_firmware_version="1.17",
    #                                                fw_database_id="88",
    #                                                test_name="MoistureDecoderTest",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='moisture_decoder_test.json')
    #
    #     uc6.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 7 in tests_to_run_jade:
        uc7 = cuc_jade_7_module.ControllerUseCase7(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="TemperatureDecoderTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='temperature_decoder_test.json')

        uc7.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 8 in tests_to_run_jade:
        uc8 = cuc_jade_8_module.ControllerUseCase8(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="StartTimesTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='start_times_test.json')

        uc8.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 9 in tests_to_run_jade:
        uc9 = cuc_jade_9_module.ControllerUseCase9(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="MasterValvePumpTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='mv_pump_test.json')

        uc9.run_use_case()
    #     TODO This test needs a complete rewrite
    # if tests_to_run_jade is opcodes.run_all_use_cases or 10 in tests_to_run_jade:
    #     uc10 = cuc_jade_10_module.ControllerUseCase10(controller_type="10",
    #                                                   controller_firmware_version="0.171",
    #                                                   fw_database_id="88",
    #                                                   test_name="OneTimeCalibrationTest",
    #                                                   user_configuration_instance=user_conf,
    #                                                   json_configuration_file='calibration_tests.json')
    #
    #     uc10.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 11 in tests_to_run_jade:
        uc11 = cuc_jade_11_module.ControllerUseCase11(controller_type="10",
                                                      controller_firmware_version=firmware_version_1000,
                                                      fw_database_id="88",
                                                      test_name="ReplacingDevicesTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='replacing_devices_test.json')

        uc11.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 12 in tests_to_run_jade:
        uc12 = cuc_jade_12_module.ControllerUseCase12(controller_type="10",
                                                      controller_firmware_version=firmware_version_1000,
                                                      fw_database_id="88",
                                                      test_name="SoakCycles",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='soak_cycles_test.json')

        uc12.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 13 in tests_to_run_jade:
        uc13 = cuc_jade_13_module.ControllerUseCase13(controller_type="10",
                                                      controller_firmware_version=firmware_version_1000,
                                                      fw_database_id="88",
                                                      test_name="WateringDayScheduleTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='watering_day_schedule_test.json')

        uc13.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 14 in tests_to_run_jade:
        uc14 = cuc_jade_14_module.ControllerUseCase14(controller_type="10",
                                                      controller_firmware_version=firmware_version_1000,
                                                      fw_database_id="88",
                                                      test_name="UpperLimitTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='upper_limit_watering.json')

        uc14.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 15 in tests_to_run_jade:
        uc15 = cuc_jade_15_module.ControllerUseCase15(controller_type="10",
                                                      controller_firmware_version=firmware_version_1000,
                                                      fw_database_id="88",
                                                      test_name="AlertRelayTest",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='alert_relay.json')

        uc15.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 16 in tests_to_run_jade:
        uc16 = cuc_jade_16_module.ControllerUseCase16(controller_type="10",
                                                      controller_firmware_version=firmware_version_1000,
                                                      fw_database_id="88",
                                                      test_name="TimedZonesWithSoakCycles",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='timed_zones_with_soak_cycles.json')

        uc16.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 17 in tests_to_run_jade:
        uc17 = cuc_jade_17_module.ControllerUseCase17(controller_type="10",
                                                      controller_firmware_version=firmware_version_1000,
                                                      fw_database_id="88",
                                                      test_name="CNUseCase17firmwareupdate",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='back_restore_firmware_updates.json')

        uc17.run_use_case()

    if tests_to_run_jade is opcodes.run_all_use_cases or 18 in tests_to_run_jade:
        uc18 = cuc_jade_18_module.ControllerUseCase18(controller_type="10",
                                                      controller_firmware_version=firmware_version_1000,
                                                      fw_database_id="88",
                                                      test_name="CNUseCase18backuprestoreprogramming",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='back_restore_firmware_updates.json')

        uc18.run_use_case()

    # ---------------------------------------------------------------------------------------------------------------- #
    #     3200 Tests                                                                                                   #
    # ---------------------------------------------------------------------------------------------------------------- #

    if tests_to_run_3200 is opcodes.run_all_use_cases or 1 in tests_to_run_3200:
        uc1 = cuc_3200_1_module.ControllerUseCase1(controller_type="32",
                                                   controller_firmware_version=firmware_version_3200,
                                                   cn_serial_number=serial_number_3200_controller,
                                                   fw_database_id="88",
                                                   test_name="CN-UseCase1-RebootConfigVerify",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='update_cn.json')
        uc1.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 2 in tests_to_run_3200:
        uc2 = cuc_3200_2_module.ControllerUseCase2(controller_type="32",
                                                   controller_firmware_version=firmware_version_3200,
                                                   cn_serial_number=serial_number_3200_controller,
                                                   fw_database_id="88",
                                                   test_name="EPATestConfiguration",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='EPA_test_configuration.json')

        uc2.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 3 in tests_to_run_3200:
        uc3 = cuc_3200_3_module.ControllerUseCase3(controller_type="32",
                                                   controller_firmware_version=firmware_version_3200,
                                                   cn_serial_number=serial_number_3200_controller,
                                                   fw_database_id="88",
                                                   test_name="SetMessages",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='set_messages.json')

        uc3.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 4 in tests_to_run_3200:
        uc4 = cuc_3200_4_module.ControllerUseCase4(controller_type="32",
                                                   controller_firmware_version=firmware_version_3200,
                                                   cn_serial_number=serial_number_3200_controller,
                                                   fw_database_id="88",
                                                   test_name="LowerLimitOneTimeCalibration",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='calibration_tests.json')

        uc4.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 5 in tests_to_run_3200:
        uc5 = cuc_3200_5_module.ControllerUseCase5(controller_type="32",
                                                   controller_firmware_version=firmware_version_3200,
                                                   cn_serial_number=serial_number_3200_controller,
                                                   fw_database_id="88",
                                                   test_name="POC_empty_conditions",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='empty_conditions.json')

        uc5.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 6 in tests_to_run_3200:
        uc6 = cuc_3200_6_module.ControllerUseCase6(controller_type="32",
                                                   controller_firmware_version=firmware_version_3200,
                                                   cn_serial_number=serial_number_3200_controller,
                                                   fw_database_id="88",
                                                   test_name="check cn serial number",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='check_cn_serial_number.json')

        uc6.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 7 in tests_to_run_3200:
        uc7 = cuc_3200_7_module.ControllerUseCase7(controller_type="32",
                                                   controller_firmware_version=firmware_version_3200,
                                                   cn_serial_number=serial_number_3200_controller,
                                                   fw_database_id="88",
                                                   test_name="multiple ssp with event decoders",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='multiple_ssp_with_event_decoders.json')

        uc7.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 8 in tests_to_run_3200:
        uc8 = cuc_3200_8_module.ControllerUseCase8(controller_type="32",
                                                   controller_firmware_version=firmware_version_3200,
                                                   cn_serial_number=serial_number_3200_controller,
                                                   fw_database_id="88",
                                                   test_name="multiple ssp with all devices",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='multiple_ssp_with_all_devices.json')

        uc8.run_use_case()
    #     TODO the controller slows down and this test now fails
    # if tests_to_run_3200 is opcodes.run_all_use_cases or 9 in tests_to_run_3200:
    #     uc9 = cuc_3200_9_module.ControllerUseCase9(controller_type="32",
    #                                                controller_firmware_version=firmware_version_3200,
    #                                                cn_serial_number=cn_serial_num,
    #                                                fw_database_id="88",
    #                                                test_name="memory_usage",
    #                                                user_configuration_instance=user_conf,
    #                                                json_configuration_file='ram_test_3200.json')
    #
    #     uc9.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 10 in tests_to_run_3200:
        uc10 = cuc_3200_10_module.ControllerUseCase10(controller_type="32",
                                                      controller_firmware_version=firmware_version_3200,
                                                      cn_serial_number=serial_number_3200_controller,
                                                      fw_database_id="88",
                                                      test_name="CN-UseCase10-SeasonalAdjust",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='seasonal_adjust.json')
        uc10.run_use_case()

    #     TODO this test doesnt pass
    # if tests_to_run_3200 is opcodes.run_all_use_cases or 11 in tests_to_run_3200:
    #     uc11 = cuc_3200_11_module.ControllerUseCase11(controller_type="32",
    #                                                   controller_firmware_version=firmware_version_3200,
    #                                                   cn_serial_number=cn_serial_num,
    #                                                   fw_database_id="14",
    #                                                   test_name="CNUseCase11firmwareupdate",
    #                                                   user_configuration_instance=user_conf,
    #                                                   json_configuration_file='back_restore_firmware_updates.json')
    #     uc11.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 12 in tests_to_run_3200:
        uc12 = cuc_3200_12_module.ControllerUseCase12(controller_type="32",
                                                      controller_firmware_version=firmware_version_3200,
                                                      cn_serial_number=serial_number_3200_controller,
                                                      fw_database_id="88",
                                                      test_name="CNbackuprestoreprograming",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='back_restore_firmware_updates.json')

        uc12.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 13 in tests_to_run_3200:
        uc13 = cuc_3200_13_module.ControllerUseCase13(controller_type="32",
                                                      controller_firmware_version=firmware_version_3200,
                                                      cn_serial_number=serial_number_3200_controller,
                                                      fw_database_id="88",
                                                      test_name="CN-UseCase13-learnflowmultiplemainlines",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='flow_variance.json')
        uc13.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 14 in tests_to_run_3200:
        uc14 = cuc_3200_14_module.ControllerUseCase14(controller_type="32",
                                                      controller_firmware_version=firmware_version_3200,
                                                      cn_serial_number=serial_number_3200_controller,
                                                      fw_database_id="88",
                                                      test_name="CN-UseCase14-lowflowvariance",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='flow_variance.json')
        uc14.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 15 in tests_to_run_3200:
        uc15 = cuc_3200_15_module.ControllerUseCase15(controller_type="32",
                                                      controller_firmware_version=firmware_version_3200,
                                                      cn_serial_number=serial_number_3200_controller,
                                                      fw_database_id="88",
                                                      test_name="CN-UseCase15-basicProgramming",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='basic_programming.json')
        uc15.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 16 in tests_to_run_3200:
        uc16 = cuc_3200_16_module.ControllerUseCase16(controller_type="32",
                                                      controller_firmware_version=firmware_version_3200,
                                                      cn_serial_number=serial_number_3200_controller,
                                                      fw_database_id="88",
                                                      test_name="CN-UseCase16-timedZones",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='timed_zones_with_soak_cycles.json')
        uc16.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 17 in tests_to_run_3200:
        uc17 = cuc_3200_17_module.ControllerUseCase17(controller_type="32",
                                                      controller_firmware_version=firmware_version_3200,
                                                      cn_serial_number=serial_number_3200_controller,
                                                      fw_database_id="88",
                                                      test_name="CN-UseCase17-soak_cycles",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='timed_zones_with_soak_cycles.json')
        uc17.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 18 in tests_to_run_3200:
        uc18 = cuc_3200_18_module.ControllerUseCase18(controller_type="32",
                                                      controller_firmware_version=firmware_version_3200,
                                                      cn_serial_number=serial_number_3200_controller,
                                                      fw_database_id="88",
                                                      test_name="CN-UseCase18-basic_design_flow",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='basic_flow_test.json')
        uc18.run_use_case()

    if tests_to_run_3200 is opcodes.run_all_use_cases or 19 in tests_to_run_3200:
        uc19 = cuc_3200_19_module.ControllerUseCase19(controller_type="32",
                                                      controller_firmware_version=firmware_version_3200,
                                                      cn_serial_number=serial_number_3200_controller,
                                                      fw_database_id="88",
                                                      test_name="CN-UseCase19-highflowvariance",
                                                      user_configuration_instance=user_conf,
                                                      json_configuration_file='flow_variance.json')
        uc19.run_use_case()

    # ---------------------------------------------------------------------------------------------------------------- #
    #     BaseManager Tests                                                                                            #
    # ---------------------------------------------------------------------------------------------------------------- #

    if tests_to_run_base_manager is opcodes.run_all_use_cases or 1 in tests_to_run_base_manager:
        uc1 = uc1_module.BaseManagerUseCase1(controller_type="32",
                                             test_name="BM-UseCase1-LoginTest-3200",
                                             user_configuration_instance=user_conf,
                                             json_configuration_file='update_cn.json')
        uc1.run_use_case()

    if tests_to_run_base_manager is opcodes.run_all_use_cases or 2 in tests_to_run_base_manager:
        uc2 = uc2_module.BaseManagerUseCase2(controller_type="10",
                                             test_name="BM-UseCase2-MenuTabTest-1000",
                                             user_configuration_instance=user_conf,
                                             json_configuration_file='update_cn.json')
        uc2.run_use_case()

    if tests_to_run_base_manager is opcodes.run_all_use_cases or 3 in tests_to_run_base_manager:
        uc3 = uc3_module.BaseManagerUseCase3(controller_type="32",
                                             test_name="BM-UseCase3-MenuTabTest-3200",
                                             user_configuration_instance=user_conf,
                                             json_configuration_file='update_cn.json')
        uc3.run_use_case()

    if tests_to_run_base_manager is opcodes.run_all_use_cases or 4 in tests_to_run_base_manager:
        uc4 = uc4_module.BaseManagerUseCase4(controller_type="32",
                                             test_name="BM-UseCase4-AddressDevTest-3200",
                                             user_configuration_instance=user_conf,
                                             json_configuration_file='update_cn.json')
        uc4.run_use_case()

    if tests_to_run_base_manager is opcodes.run_all_use_cases or 5 in tests_to_run_base_manager:
        uc5 = uc5_module.BaseManagerUseCase5(controller_type="10",
                                             test_name="BM-UseCase5-AddressDevTest-1000",
                                             user_configuration_instance=user_conf,
                                             json_configuration_file='update_cn.json')
        uc5.run_use_case()

    if tests_to_run_base_manager is opcodes.run_all_use_cases or 6 in tests_to_run_base_manager:
        uc6 = uc6_module.BaseManagerUseCase6(controller_type="10",
                                             test_name="BM-UseCase6-AddressDevTest-1000",
                                             user_configuration_instance=user_conf,
                                             json_configuration_file='update_cn.json')
        uc6.run_use_case()

    # ---------------------------------------------------------------------------------------------------------------- #
    #      BacNet Tests                                                                                                #
    # ---------------------------------------------------------------------------------------------------------------- #

    if tests_to_run_bacnet is opcodes.run_all_use_cases or 1 in tests_to_run_bacnet:
        uc1 = cuc_bacnet_1_module.ControllerUseCase1(controller_type="32",
                                                   controller_firmware_version=firmware_version_3200,
                                                   cn_serial_number=serial_number_3200_controller,
                                                   fw_database_id="88",
                                                   test_name="3200 FindBacnetDevice",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='set_messages.json')

        uc1.run_use_case()

    if tests_to_run_bacnet is opcodes.run_all_use_cases or 2 in tests_to_run_bacnet:
        uc2 = cuc_bacnet_2_module.ControllerUseCase2(controller_type="10",
                                                   controller_firmware_version=firmware_version_1000,
                                                   fw_database_id="88",
                                                   test_name="1000 BacnetTestMessages",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='set_messages.json')

        uc2.run_use_case()

    if tests_to_run_bacnet is opcodes.run_all_use_cases or 3 in tests_to_run_bacnet:
        uc3 = cuc_bacnet_3_module.ControllerUseCase3(controller_type="32",
                                                   controller_firmware_version=firmware_version_3200,
                                                   cn_serial_number=serial_number_3200_controller,
                                                   fw_database_id="88",
                                                   test_name="3200 BacnetTestMessages",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='set_messages.json')

        uc3.run_use_case()

    if tests_to_run_bacnet is opcodes.run_all_use_cases or 4 in tests_to_run_bacnet:
        uc4 = cuc_bacnet_4_module.ControllerUseCase4(controller_type="10",
                                                     controller_firmware_version=firmware_version_1000,
                                                     fw_database_id="88",
                                                     test_name="1000 FindBacnetDevice",
                                                     user_configuration_instance=user_conf,
                                                     json_configuration_file='set_messages.json')
        uc4.run_use_case()

    # ---------------------------------------------------------------------------------------------------------------- #
    #      SubStation Tests                                                                                            #
    # ---------------------------------------------------------------------------------------------------------------- #

    if tests_to_run_substation is opcodes.run_all_use_cases or 1 in tests_to_run_substation:
        sbuc1 = cuc_substation_1_module.UseCase1TimedZones(
            controller_type="32",
            controller_firmware_version=firmware_version_3200,
            cn_serial_number=serial_number_3200_controller,
            fw_database_id="88",
            test_name="SB UC 1 - Timed Zones",
            user_configuration_instance=user_conf,
            json_configuration_file='SB_timed_zones.json',
            substation_firmware_version=firmware_version_substation,
            number_of_substations_to_use=1
        )
        sbuc1.run_use_case()

    if tests_to_run_substation is opcodes.run_all_use_cases or 2 in tests_to_run_substation:
        sbuc2 = cuc_substation_2_module.UseCase2SoakCycles(
            controller_type="32",
            controller_firmware_version=firmware_version_3200,
            cn_serial_number=serial_number_3200_controller,
            fw_database_id="88",
            test_name="SB UC 2 - Soak Cycles",
            user_configuration_instance=user_conf,
            json_configuration_file='SB_soak_cycles.json',
            substation_firmware_version=firmware_version_substation,
            number_of_substations_to_use=1
        )
        sbuc2.run_use_case()

    if tests_to_run_substation is opcodes.run_all_use_cases or 3 in tests_to_run_substation:
        sbuc3 = cuc_substation_3_module.UseCase3DisconnectReconnect(
            controller_type="32",
            controller_firmware_version=firmware_version_3200,
            cn_serial_number=serial_number_3200_controller,
            fw_database_id="88",
            test_name="SB UC 3 - Disconn/Reconn",
            user_configuration_instance=user_conf,
            json_configuration_file='SB_timed_zones.json',
            substation_firmware_version=firmware_version_substation,
            number_of_substations_to_use=1
        )
        sbuc3.run_use_case()

    if tests_to_run_substation is opcodes.run_all_use_cases or 4 in tests_to_run_substation:
        sbuc4 = cuc_substation_4_module.UseCase4BasicProgramming(
            controller_type="32",
            controller_firmware_version=firmware_version_3200,
            cn_serial_number=serial_number_3200_controller,
            fw_database_id="88",
            test_name="SB UC 4-Basic Programming",
            user_configuration_instance=user_conf,
            json_configuration_file="SB_basic_programming.json",
            substation_firmware_version=firmware_version_substation,
            number_of_substations_to_use=1
        )
        sbuc4.run_use_case()

    if tests_to_run_substation is opcodes.run_all_use_cases or 5 in tests_to_run_substation:
        sbuc5 = cuc_substation_5_module.UseCase5ReplaceDevices(
            controller_type="32",
            controller_firmware_version=firmware_version_3200,
            cn_serial_number=serial_number_3200_controller,
            fw_database_id="88",
            test_name="SB UC 5 - Replace Devices",
            user_configuration_instance=user_conf,
            json_configuration_file="SB_replace_devices.json",
            substation_firmware_version=firmware_version_substation,
            number_of_substations_to_use=1
        )
        sbuc5.run_use_case()

    if tests_to_run_substation is opcodes.run_all_use_cases or 6 in tests_to_run_substation:
        sbuc6 = cuc_substation_6_module.UseCase6ConcurrentZones(
            controller_type="32",
            controller_firmware_version=firmware_version_3200,
            cn_serial_number=serial_number_3200_controller,
            fw_database_id="88",
            test_name="SB UC 6 - Concurrent Zones",
            user_configuration_instance=user_conf,
            json_configuration_file="SB_concurrent_zones.json",
            substation_firmware_version=firmware_version_substation,
            number_of_substations_to_use=1
        )
        sbuc6.run_use_case()

    if tests_to_run_substation is opcodes.run_all_use_cases or 7 in tests_to_run_substation:
        sbuc7 = cuc_substation_7_module.UseCase7MoveDevices(
            controller_type="32",
            controller_firmware_version=firmware_version_3200,
            cn_serial_number=serial_number_3200_controller,
            fw_database_id="88",
            test_name="SB UC 7 - Move Devices",
            user_configuration_instance=user_conf,
            json_configuration_file="SB_move_devices.json",
            substation_firmware_version=firmware_version_substation,
            number_of_substations_to_use=1
        )
        sbuc7.run_use_case()

    if tests_to_run_substation is opcodes.run_all_use_cases or 8 in tests_to_run_substation:
        sbuc8 = cuc_substation_8_module.UseCase8LearnFlow(
            controller_type="32",
            controller_firmware_version=firmware_version_3200,
            cn_serial_number=serial_number_3200_controller,
            fw_database_id="88",
            test_name="SB UC 8 - Learn Flow",
            user_configuration_instance=user_conf,
            json_configuration_file="SB_learn_flow.json",
            substation_firmware_version=firmware_version_substation,
            number_of_substations_to_use=1
        )
        sbuc8.run_use_case()

    if tests_to_run_substation is opcodes.run_all_use_cases or 9 in tests_to_run_substation:
        sbuc9 = cuc_substation_9_module.UseCase9Messages(
            controller_type="32",
            controller_firmware_version=firmware_version_3200,
            cn_serial_number=serial_number_3200_controller,
            fw_database_id="88",
            test_name="SB UC 9 - Messages",
            user_configuration_instance=user_conf,
            json_configuration_file="SB_messages.json",
            substation_firmware_version=firmware_version_substation,
            number_of_substations_to_use=1
        )
        sbuc9.run_use_case()
    #
    #     TODO: THIS USE CASE 10 REQUIRES A BASEMANAGER CONNECTION & USB PLUGGED INTO 3200.
    #     TODO: TO NOT TEST USING BASEMANAGER, COMMENT OUT LINES 152, 153, 154
    #     TODO: TO NOT TEST USING USB, COMMENT OUT LINES 157, 158, 159
    # if tests_to_run_substation is opcodes.run_all_use_cases or 10 in tests_to_run_substation:
    #     sbuc10 = cuc_substation_10_module.UseCase10BackupRestore(
    #         controller_type="32",
    #         controller_firmware_version=firmware_version_3200,
    #         cn_serial_number=cn_serial_num,
    #         fw_database_id="88",
    #         test_name="SB UC 10 - Backup Restore",
    #         user_configuration_instance=user_conf,
    #         json_configuration_file="SB_backup_restore.json",
    #         substation_firmware_version=firmware_version_substation,
    #         number_of_substations_to_use=1
    #     )
    #     sbuc10.run_use_case()

    print("######################### ----YOU ARE A WINNER WINNER CHICKEN DINNER---- ##################################")

    exit()
