import sys

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

from time import sleep
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_1000 import POC1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_1000 import StartConditionFor1000, PauseConditionFor1000, StopConditionFor1000

from old_32_10_sb_objects_dec_29_2017.common import helper_methods


class ControllerUseCase3(object):
    """
    Test name: \n
        Basic Programming Test \n
    Purpose: \n
        To test that each command and verify that they work \n
    Covers: \n
        1. Load devices \n
        2. Search for devices \n
        3. Set descriptions and locations \n
        4. Can assign devices to addresses \n
        5. Can assign zones to programs \n
        6. Can set up programs with; start times, watering days, water windows, zone concurrency, and assign programs to
    water sources \n
        7. Can set up moisture sensors and give them values \n
        8. Can set up temperature sensors and give them values \n
        9. Can set up event switches and give them values \n
        10. Can set up master valves \n
        11. Can set up flow meters and give them values \n
        12. Can set up water sources \n
        13. Can set up booster pumps \n
        14. Can set up water sources \n
        15. Can test all devices \n

    added new verify flow values section do to a bug that was found by support in the 1000 version .123 if the
    controller was reboot the 1000 would read incorrect flow values the decimal point would be changed to be 1/10th
    of the current value \n
    a reboot of the the controller was add and then all values abre verified \n
    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase3' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        Step 1:
            - configure controller:
            - configure BaseManager: \n
        step 2:
            - setting up devices:
        Step 3:
            - Over write zone values:
        step 4:
            - Over write moisture sensor values: \n
        Step 5:
            - Over write temperature sensor values: \n
        Step 6:
            - Over write event switch values: \n
        Step 7:
            - Over write flow meter values: \n
        Step 8:
            - set up programs: \n
        Step 9:
            - set up zone programs : \n
        Step 10:
            - set up pocs: \n
        Step 11:
            - set up start stop pause conditions: \n
        Step 12:
            - self test all of the devices: \n
        Step 13:
            - verify all of the device statuses: \n
        Step 14:
            - verify the full configuration of the controller: \n
        Step 15:
            - reboot the controller: \n
        Step 16:
            - test all devices and update the objects: \n
        Step 17:
            - verify the full configuration of the controller: \n
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # only need this for BaseManager
            # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock
            # for the reboot process.
            # self.config.controllers[1].set_sim_mode_to_off()
            self.config.basemanager_connection[1].verify_ip_address_state()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=4)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # load all devices need into the controller so that they are available for use in the configuration
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-100
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(
                ms_object_dict=self.config.moisture_sensors,
                ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(
                ts_object_dict=self.config.temperature_sensors,
                ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_1000_poc_objects()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

        self.config.create_1000_poc_objects()

    def step_3(self):
        """
        Over write zone values: \n
        set up the zones \n
            - Change the design flow for each zone \n
            - disable zone 98
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zones[1].set_design_flow_on_cn(_df=10)
            self.config.zones[2].set_design_flow_on_cn(_df=10)
            self.config.zones[49].set_design_flow_on_cn(_df=30)
            self.config.zones[50].set_design_flow_on_cn(_df=30)
            self.config.zones[97].set_design_flow_on_cn(_df=80)
            self.config.zones[98].set_design_flow_on_cn(_df=80)
            self.config.zones[99].set_design_flow_on_cn(_df=80)
            self.config.zones[100].set_design_flow_on_cn(_df=100)
            self.config.zones[98].set_enable_state_on_cn(_state='FA')
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_4(self):
        """
        Over write Moisture sensor values: \n
        Set up Moisture Sensors \n
            - set new moisture values for each moisture sensor \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=40.0)
            self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=10.0)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_5(self):
        """
        Over write Temperature Sensors values \n
        - set new temperature value for temperature sensor to 82 degrees fahrenheit \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=82.0)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_6(self):
        """
        Over write event switch values:  \n
        Set up Event Switches \n
        - set event switch to have a closed value \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.event_switches[1].set_contact_state_on_cn(_contact_state='CL')
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_7(self):
        """
        Over write flow meter values: \n
        Set up flow meters \n
            - set new k values for each flow meter \n
            - set flow values for each flow meter \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.flow_meters[1].set_k_value_on_cn(_k_value=6.99)
            self.config.flow_meters[2].set_k_value_on_cn(_k_value=.86)
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=125.0)
            self.config.flow_meters[2].set_flow_rate_on_cn(_flow_rate=550.0)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_8(self):
        """
        This is set in the PG1000 object
        set_up_programs
            Program 1:
                - address 1
                - water window is closed form 8:00 am to 8:00 pm
                - Max concurrent Zones 4
                - Seasonal adjust  set to 100
                - Point of Connection set to 1
                - Soak cycle Mode program
                - Cycle count set to 4
                - Soak time set to 5 minutes
            Program 3:
                - address 3
                - water window is open all day
                - Max concurrent Zones 6
                - Seasonal adjust  set to 150
                - Point of Connection set to 1 and 2
                - Master Valve set to 2 which is it address
                - Soak cycle Mode  Smart program soake cycles
                - Cycle count set to 6
                - Soak time set to 10 minutes
            Program 40:
                - address 40
                - water window is open all day
                - Max concurrent Zones 6
                - Seasonal adjust  set to 150
                - Point of Connection set to 3
                - Soak cycle Mode zone
                - Cycle count set to 8
                - Soak time set to 20 minutes

        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        daily_water_window = ['011111100001111111111110']
        fully_open_water_window = ['111111111111111111111111']
        weekly_water_windows = ['011111100001111111111110', '011111100000111111111110', '111111100000000000011110',
                                '111111100000000000011110', '111111100000000000011110', '111111100000000000011110',
                                '111111100000000000011110']
        try:
            self.config.programs[1] = PG1000(_address=1,
                                             _enabled_state=opcodes.true,
                                             _water_window=daily_water_window,
                                             _max_concurrent_zones=4,
                                             _seasonal_adjust=100,
                                             _point_of_connection_address=[1],
                                             _soak_cycle_mode=opcodes.program_cycles,
                                             _cycle_count=4,
                                             _soak_time=300
                                             )

            self.config.programs[3] = PG1000(_address=3,
                                             _enabled_state=opcodes.true,
                                             _water_window=weekly_water_windows,
                                             _max_concurrent_zones=5,
                                             _seasonal_adjust=50,
                                             _point_of_connection_address=[1, 2],
                                             _master_valve_address=2,
                                             _soak_cycle_mode=opcodes.intelligent_soak,
                                             _cycle_count=6,
                                             _soak_time=600
                                             )
            self.config.programs[40] = PG1000(_address=40,
                                              _enabled_state=opcodes.true,
                                              _water_window=fully_open_water_window,
                                              _max_concurrent_zones=6,
                                              _seasonal_adjust=150,
                                              _point_of_connection_address=[3],
                                              _soak_cycle_mode=opcodes.zone_soak_cycles,
                                              _cycle_count=8,
                                              _soak_time=1200
                                              )
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_9(self):
        """
        zone programs
            - assign zones to a program
                - set zone 1 and 2
                    - program 1
                    - run time to 15 minutes
                - set zone 49 and 50
                    - program 3
                    - run time to 20 minutes
                - set zone 97, 98, 99, and 100
                    - program 40
                    - run time to 33 minutes
        """
        #TODO need to add some zones that are ET based where you dont set the runtime
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900)
            self.config.zone_programs[49] = ZoneProgram(zone_obj=self.config.zones[49],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=1200)
            self.config.zone_programs[50] = ZoneProgram(zone_obj=self.config.zones[50],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=1200)
            self.config.zone_programs[97] = ZoneProgram(zone_obj=self.config.zones[97],
                                                        prog_obj=self.config.programs[40],
                                                        _rt=1980)
            self.config.zone_programs[98] = ZoneProgram(zone_obj=self.config.zones[98],
                                                        prog_obj=self.config.programs[40],
                                                        _rt=1980)
            self.config.zone_programs[99] = ZoneProgram(zone_obj=self.config.zones[99],
                                                        prog_obj=self.config.programs[40],
                                                        _rt=1980)
            self.config.zone_programs[100] = ZoneProgram(zone_obj=self.config.zones[100],
                                                         prog_obj=self.config.programs[40],
                                                         _rt=1980)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_10(self):
        """
        set up water source 1 \n
            enable water source 1 \n
            assign master valve TMV0003 which is address 1
            assign flow meter TWF0003 which is address 1 \n
            assign water source 1 a target flow of 500 \n
            set the high flow limit to 550 and enable high flow shut down \n
            set the unscheduled flow limit to 10 and enable the unscheduled flow shut down \n
            enable limit concurrent to target \n
            set the pipe fill time to 2 minutes \n
            set the flow variance limit to 10% and enable the flow variance \n
        \n
        set up water source 3 \n
            enable water source 3 \n
            assign master valve TMV0004 which is address 7 \n
            assign flow meter TWF0004 which is address 2 \n
            assign water source 3 a target flow of 50 \n
            set the high flow limit to 75 and disable high flow shut down \n
            set the unscheduled flow limit to 5 and disable the unscheduled flow shut down \n
            enable limit concurrent to target \n
            set the pipe fill time to 4 minutes \n
            set the flow variance limit to 25% and enable the flow variance \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1] = POC1000(
                     _address=1,
                     _flow_meter_address=1,
                     _master_valve_address=1,
                     _enabled_state=opcodes.true,
                     _target_flow=500.0,
                     _high_flow_limit=550.0,
                     _shutdown_on_high_flow=opcodes.true,
                     _unscheduled_flow_limit=10.0,
                     _shutdown_on_unscheduled=opcodes.true,
                     _limit_concurrent_zones_to_target=opcodes.true,
                     _fill_time=2,
                     _flow_variance_percent=10,
                     _flow_variance_enable=opcodes.true,
                     _flow_fault=opcodes.false)
            self.config.poc[2] = POC1000(
                    _address=2,
                    _enabled_state=opcodes.true,
                    _target_flow=500.0,
                    _high_flow_limit=550.0,
                    _shutdown_on_high_flow=opcodes.true,
                    _unscheduled_flow_limit=10.0,
                    _shutdown_on_unscheduled=opcodes.true,
                    _limit_concurrent_zones_to_target=opcodes.true,
                    _fill_time=2,
                    _flow_variance_percent=10,
                    _flow_variance_enable=opcodes.true,
                    _flow_fault=opcodes.false)

            self.config.poc[3] = POC1000(
                     _address=3,
                     _flow_meter_address=2,
                     _master_valve_address=7,
                     _enabled_state=opcodes.true,
                     _target_flow=50.0,
                     _high_flow_limit=75.0,
                     _shutdown_on_high_flow=opcodes.false,
                     _unscheduled_flow_limit=5.0,
                     _shutdown_on_unscheduled=opcodes.false,
                     _limit_concurrent_zones_to_target=opcodes.true,
                     _fill_time=4,
                     _flow_variance_percent=25,
                     _flow_variance_enable=opcodes.true,
                     _flow_fault=opcodes.false
                     )
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_11(self):
        """
        set up start stop pause conditions
            Program 1:
                - start times 8:00 am, 9:00 am, 10:00 am, 11:00 am \n
                - watering days are set to weekly \n
                - Watering days enabled are Monday, Wednesday, Friday \n
                - assign moisture sensor SB05308 to start program 40 and give it a threshold of 38 \n

            Program 3:
                - start time start times 10:00 am, 11:00 am, 12:00 am, 1:00 pm \n
                - watering days are set calendar interval's on odd days \n
                - set up a temperature stop when the sensor reads below 32 degrees \n

            Program 40:
                - start times 10:00 am, 11:00 am, 12:00 am, 1:00 pm \n
                - watering days are set to semi monthly  \n
                - set the event switch to stop program 40 when the contacts open \n
        """

        start_times_800_900_1000_1100 = [480, 540, 600, 660]
        start_times_1000_1100_1200_1300 = [600, 660, 720, 780]
        mon_wed_fri_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
        semi_monthly = [0, 0, 0, 0, 0, 0, 8, 8, 6, 5, 5, 4, 3, 3, 3, 3, 4, 5, 6, 7, 0, 0, 0, 0]
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.programs[1].set_day_time_start(_st_list=start_times_800_900_1000_1100)
            # self.config.programs[3].set_day_time_start(_st_list=start_times_1000_1100_1200_1300)
            # self.config.programs[40].set_day_time_start(_st_list=start_times_1000_1100_1200_1300)
            #
            # self.config.programs[1].set_day_time_start(_interval_type=opcodes.week_days,
            #                                            _interval_args=mon_wed_fri_watering_days)
            # self.config.programs[3].set_day_time_start(_interval_type=opcodes.calendar_interval,
            #                                            _interval_args=opcodes.odd_day)
            # self.config.programs[40].set_day_time_start(_interval_type=opcodes.semi_month_interval,
            #                                             _interval_args=semi_monthly)
            #
            # self.config.programs[1].set_moisture_condition(serial_number=self.config.moisture_sensors[1].sn,
            #                                                condition_type=opcodes.program_start_condition,
            #                                                mode=opcodes.lower_limit,
            #                                                threshold=24.0,
            #                                                dt=opcodes.true)
            # self.config.programs[3].set_temperature_condition(serial_number=self.config.temperature_sensors[1].sn,
            #                                                   condition_type=opcodes.program_stop_condition,
            #                                                   mode=opcodes.lower_limit,
            #                                                   threshold=32.0)
            # self.config.programs[40].set_event_switch_condition(serial_number=self.config.event_switches[1].sn,
            #                                                     condition_type=opcodes.program_pause_condition,
            #                                                     mode=opcodes.contacts_open,
            #                                                     pt=10)

            self.config.program_start_conditions[1] = StartConditionFor1000(program_ad=1)
            self.config.program_start_conditions[2] = StartConditionFor1000(program_ad=3)
            self.config.program_start_conditions[3] = StartConditionFor1000(program_ad=40)
            self.config.program_start_conditions[1].set_day_time_start(_dt=opcodes.true,
                                                                       _st_list=start_times_800_900_1000_1100,
                                                                       _interval_type=opcodes.week_days,
                                                                       _interval_args=mon_wed_fri_watering_days)
            self.config.program_start_conditions[2].set_day_time_start(_dt=opcodes.true,
                                                                       _st_list=start_times_1000_1100_1200_1300,
                                                                       _interval_type=opcodes.calendar_interval,
                                                                       _interval_args=opcodes.odd_day)
            self.config.program_start_conditions[3].set_day_time_start(_dt=opcodes.true,
                                                                       _st_list=start_times_1000_1100_1200_1300,
                                                                       _interval_type=opcodes.semi_month_interval,
                                                                       _interval_args=semi_monthly)

            self.config.program_start_conditions[4] = StartConditionFor1000(program_ad=1)
            self.config.program_stop_conditions[1] = StopConditionFor1000(program_ad=3)
            self.config.program_pause_conditions[1] = PauseConditionFor1000(program_ad=40)
            self.config.program_start_conditions[4].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.lower_limit, threshold=24.0, dt=opcodes.true)
            self.config.program_stop_conditions[1].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.lower_limit, threshold=32.0)
            self.config.program_pause_conditions[1].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.contacts_open, pt=10)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_12(self):
        """
        perform a test on all devices to verify that they are functioning properly \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].do_self_test()
            for moisture_sensors in self.config.ms_ad_range:
                self.config.moisture_sensors[moisture_sensors].do_self_test()
            for master_valves in self.config.mv_ad_range:
                self.config.master_valves[master_valves].do_self_test()
            for flow_meters in self.config.fm_ad_range:
                self.config.flow_meters[flow_meters].do_self_test()
            for event_switches in self.config.sw_ad_range:
                self.config.event_switches[event_switches].do_self_test()
            for temperature_sensors in self.config.ts_ad_range:
                self.config.temperature_sensors[temperature_sensors].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_13(self):
        """
        this goes out and verifies the status on each zone and program all should be set to done
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].do_increment_clock(minutes=1)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[49].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[50].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(opcodes.disabled)
            self.config.zones[99].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[1].get_data()
            self.config.programs[3].get_data()
            self.config.programs[40].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[40].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_14(self):
        """
        verify the full configuration of the controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_15(self):
        """
        this reboot the controller
        waits 10 seconds after controller reconnects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            # reboot controller
            self.config.controllers[1].do_reboot_controller()
            sleep(10)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_16(self):
        """
        after the reboot all of the devices get set to default values in the controller so you have to do a self test
        and than do an update to get each object to match what the controller has
        perform a test on all zones to verify that they are functioning properly \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].self_test_and_update_object_attributes()
            for moisture_sensors in self.config.ms_ad_range:
                self.config.moisture_sensors[moisture_sensors].self_test_and_update_object_attributes()
            for master_valves in self.config.mv_ad_range:
                self.config.master_valves[master_valves].self_test_and_update_object_attributes()
            for flow_meters in self.config.fm_ad_range:
                self.config.flow_meters[flow_meters].self_test_and_update_object_attributes()
            for event_switches in self.config.sw_ad_range:
                self.config.event_switches[event_switches].self_test_and_update_object_attributes()
            for temperature_sensors in self.config.ts_ad_range:
                self.config.temperature_sensors[temperature_sensors].self_test_and_update_object_attributes()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_17(self):
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)
