import sys
from time import sleep

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# object
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_1000 import POC1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

from old_32_10_sb_objects_dec_29_2017.common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase1(object):
    """
    Test name: \n
        Basic Learn Flow Test \n
    Purpose: \n
        This test covers basic Learn flow operations: \n
            First it clears the controller so we have a base point to start from, then it loads some device so that we
            can configure them into the controller (4 zones 1 POC 1 mainline, 1 program) \n
    Coverage area: \n
        1. Setting up the controller \n
        2. Searching and assigning: \n
            - Zones
            - Master Valves
            - Flow Meters
        3. Giving zones an address, description, serial number, latitude, and longitude \n
        4. Assigning zones to a program, and set their runtime to 1 hour \n
        5. Setting the default design flow values for the zones \n
        6. Setting a POC \n
        7. Change flow without changing the learn flow \n
        8. Change the design flow of multiple zones by enabling learn flow of a program \n
        9.  Learn flow of multiple zones one at a time \n
        10. Learn flow of multiple zones one at a time \n
        11. Learn flow of multiple zones one at a time \n
        12. Design flow of a zone being over written by a learn flow after a learn flow is complete \n
    """
    # TODO Things to add to the test or to a new test \n
    # TODO Changing flow rate while learning flow of a zone causing learn flow failures \n
    # TODO Changing flow rate during pipe fill time verify learn flow of a zone is still successful \n
    # TODO Changing flow rate during pipe fill time verify learn flow of a program is still successful \n
    # TODO check pipe file time \n
    # (The pipe fill time is a variable in the POC1000 object instead of mainline)
    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 2
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()
            # only need this for BaseManager
            # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock
            # for the reboot process.
            # self.config.controllers[1].set_sim_mode_to_off()
            self.config.basemanager_connection[1].verify_ip_address_state()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=4)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Load all devices to controller
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_1000_poc_objects()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_3(self):
        """
        set the start time for each program to 8:00 \n
        set the number of soak cycles to 15 and the soak time to 300 seconds on each program \n
        assign zones to a program and give each zone a run time of 1 hour \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the start time for each program to 8:00 am
            self.config.programs[1] = PG1000(_address=1, _max_concurrent_zones=4)
            self.config.programs[2] = PG1000(_address=2, _max_concurrent_zones=4)

            # Set the number of soak cycles to 15 and the soak time to 300 seconds on each program
            # Assign zones to a program and give each zone a run time of 1 hour
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[51],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600,
                                                       _ct=15,
                                                       _so=300)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[52],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600,
                                                       _ct=15,
                                                       _so=300)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[53],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600,
                                                       _ct=15,
                                                       _so=300)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[54],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600,
                                                       _ct=15,
                                                       _so=300)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[55],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600,
                                                       _ct=15,
                                                       _so=300)
            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[56],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=3600,
                                                       _ct=15,
                                                       _so=300)
            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[75],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=3600,
                                                       _ct=15,
                                                       _so=300)
            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[76],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=3600,
                                                       _ct=15,
                                                       _so=300)
            self.config.zone_programs[9] = ZoneProgram(zone_obj=self.config.zones[78],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=3600,
                                                       _ct=15,
                                                       _so=300)
            self.config.zone_programs[10] = ZoneProgram(zone_obj=self.config.zones[79],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=3600,
                                                        _ct=15,
                                                        _so=300)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_4(self):
        """
        assign a design flow value to each zone so that they have a default setting \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.zones[51].set_design_flow_on_cn(_df=20)
            self.config.zones[52].set_design_flow_on_cn(_df=20)
            self.config.zones[53].set_design_flow_on_cn(_df=20)
            self.config.zones[54].set_design_flow_on_cn(_df=12)
            self.config.zones[55].set_design_flow_on_cn(_df=7.5)
            self.config.zones[56].set_design_flow_on_cn(_df=15)
            self.config.zones[75].set_design_flow_on_cn(_df=15)
            self.config.zones[76].set_design_flow_on_cn(_df=7.5)
            self.config.zones[78].set_design_flow_on_cn(_df=7.5)
            self.config.zones[79].set_design_flow_on_cn(_df=7.5)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_5(self):
        """
        set water source 1 to have with the following values: \n
        enable water source 1 \n
        assign flow meter TWF0003 to water source 1 \n
        assign master valve TMV0003 to water source 1 \n
        set the target flow to be 40gpm \n
        set the high flow shut down to 50gpm \n
        set the unscheduled flow to be 10gpm \n
        disable unscheduled flow shutdown \n
        set the pipe fill time to 2 minutes \n

        set water source 2 to have the follow values: \n
        enable water source 2 \n
        water source 2 is just enable to verify it doesnt effect water source 1 \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1] = POC1000(_address=1,
                                         _flow_meter_address=self.config.flow_meters[1].ad,
                                         _master_valve_address=self.config.master_valves[1].ad,
                                         _enabled_state=opcodes.true,
                                         _target_flow=40,
                                         _high_flow_limit=50,
                                         _unscheduled_flow_limit=10,
                                         _shutdown_on_unscheduled=opcodes.false)
            self.config.poc[2] = POC1000(_address=2,
                                         _enabled_state=opcodes.false)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_6(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_7(self):
        """
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        set flow sensor TWF0003 to 15 GPM \n
        verify the original zone flow settings \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_date_and_time_on_cn(_date="04/08/2014", _time="08:00:00")  # Set Date/Time
            self.config.controllers[1].do_increment_clock(minutes=1)  # Set the flow sensor TWF0003 to 15 GPM

            # Set flow sensor TWF0003 to 15 GPM
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=15)

            # Verify the original zone flow settings
            self.config.zones[51].verify_design_flow_on_cn()
            self.config.zones[52].verify_design_flow_on_cn()
            self.config.zones[53].verify_design_flow_on_cn()
            self.config.zones[54].verify_design_flow_on_cn()
            self.config.zones[55].verify_design_flow_on_cn()
            self.config.zones[56].verify_design_flow_on_cn()
            self.config.zones[75].verify_design_flow_on_cn()
            self.config.zones[76].verify_design_flow_on_cn()
            self.config.zones[78].verify_design_flow_on_cn()
            self.config.zones[79].verify_design_flow_on_cn()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_8(self):
        """
        turn on sim mode in the controller \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        set flow sensor TWF0003 to 15 GPM \n
        perform a learn flow on program 1 \n
        increment the clock by 25 minutes so that the learn flow has time to run all the way through \n
        Verify that zones 1 through 5 have successfully learned flow, they should be at 15 GPM \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_sim_mode_to_on()
            self.config.controllers[1].stop_clock()
            self.config.controllers[1].set_date_and_time_on_cn(_date="04/08/2014", _time="08:00:00")

            # Set flow sensor TWF0003 to 15 GPM
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=15)
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=self.config.programs[1].ad)
            self.config.controllers[1].do_increment_clock(minutes=25)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            # These are the zones we expect are going to have their design flow changed by the learn flow
            for zone in range(51, 56):
                self.config.zones[zone].df = 15

            # Verify the original zone flow settings
            self.config.zones[51].verify_design_flow_on_cn()
            self.config.zones[52].verify_design_flow_on_cn()
            self.config.zones[53].verify_design_flow_on_cn()
            self.config.zones[54].verify_design_flow_on_cn()
            self.config.zones[55].verify_design_flow_on_cn()
            self.config.zones[56].verify_design_flow_on_cn()
            self.config.zones[75].verify_design_flow_on_cn()
            self.config.zones[76].verify_design_flow_on_cn()
            self.config.zones[78].verify_design_flow_on_cn()
            self.config.zones[79].verify_design_flow_on_cn()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_9(self):
        """
        set flow sensor TWF0003 to 25 GPM \n
        set the pipe fill time to 1 minute \n
        perform a learn flow on zone 51 \n
        increment clock by 5 minutes so that the learn flow has time to run all the way through \n
        verify that all zones remained the same except for zone 1 which should be 25 GPM now \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=25)
            self.config.poc[1].set_fill_time_in_minutes_on_cn(_new_fill_time=1)
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=self.config.programs[1].ad,
                                                              _zn_ad=self.config.zones[51].ad)
            self.config.controllers[1].do_increment_clock(minutes=5)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            # This is the only zone we expect to be changed by the learn flow
            self.config.zones[51].df = 25

            # Verify the original zone flow settings
            self.config.zones[51].verify_design_flow_on_cn()
            self.config.zones[52].verify_design_flow_on_cn()
            self.config.zones[53].verify_design_flow_on_cn()
            self.config.zones[54].verify_design_flow_on_cn()
            self.config.zones[55].verify_design_flow_on_cn()
            self.config.zones[56].verify_design_flow_on_cn()
            self.config.zones[75].verify_design_flow_on_cn()
            self.config.zones[76].verify_design_flow_on_cn()
            self.config.zones[78].verify_design_flow_on_cn()
            self.config.zones[79].verify_design_flow_on_cn()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_10(self):
        """
        change to number of concurrent zones for the controller and each program \n
        turn on sim mode in the controller \n
        stop  the clock so that it can be incremented manually \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        Set flow sensor TWF0003 to 50 GPM
        Perform a learn flow on zone 2
        increment clock by 12 minutes so that the learn flow has time to run all the way through \n
        verify that the only zones affected by the learn flow were 52 and 53 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=10)
            self.config.programs[1].set_max_concurrent_zones_on_cn(_number_of_zones=2)
            self.config.programs[2].set_max_concurrent_zones_on_cn(_number_of_zones=3)
            self.config.controllers[1].set_sim_mode_to_on()
            self.config.controllers[1].stop_clock()
            self.config.controllers[1].set_date_and_time_on_cn(_date="04/08/2014", _time="08:00:00")
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=50)
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=self.config.programs[1].ad,
                                                              _zn_ad=self.config.zones[52].ad)
            self.config.controllers[1].do_increment_clock(minutes=12)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            # This is the only zone we expect to be changed by the learn flow
            self.config.zones[52].df = 50

            # Verify the original zone flow settings
            self.config.zones[51].verify_design_flow_on_cn()
            self.config.zones[52].verify_design_flow_on_cn()
            self.config.zones[53].verify_design_flow_on_cn()
            self.config.zones[54].verify_design_flow_on_cn()
            self.config.zones[55].verify_design_flow_on_cn()
            self.config.zones[56].verify_design_flow_on_cn()
            self.config.zones[75].verify_design_flow_on_cn()
            self.config.zones[76].verify_design_flow_on_cn()
            self.config.zones[78].verify_design_flow_on_cn()
            self.config.zones[79].verify_design_flow_on_cn()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_11(self):
        """
        change to number of concurrent zones for the controller and each program \n
        turn on sim mode in the controller \n
        stop the clock so that it can be incremented manually \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        Set flow sensor TWF0003 to 50 GPM \n
        Perform a learn flow on zone 53 \n
        increment clock by 12 minutes \n
        verify that all zones remained the same except zones 52 and 53, they should be 50 GPM \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=10)
            self.config.programs[1].set_max_concurrent_zones_on_cn(_number_of_zones=2)
            self.config.programs[2].set_max_concurrent_zones_on_cn(_number_of_zones=3)
            self.config.controllers[1].set_sim_mode_to_on()
            self.config.controllers[1].stop_clock()
            self.config.controllers[1].set_date_and_time_on_cn(_date="04/08/2014", _time="08:00:00")
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=50)
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=self.config.programs[1].ad,
                                                              _zn_ad=self.config.zones[53].ad)
            self.config.controllers[1].do_increment_clock(minutes=12)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            # This is the only zone we expect to be changed by the learn flow
            self.config.zones[53].df = 50

            # Verify the original zone flow settings
            self.config.zones[51].verify_design_flow_on_cn()
            self.config.zones[52].verify_design_flow_on_cn()
            self.config.zones[53].verify_design_flow_on_cn()
            self.config.zones[54].verify_design_flow_on_cn()
            self.config.zones[55].verify_design_flow_on_cn()
            self.config.zones[56].verify_design_flow_on_cn()
            self.config.zones[75].verify_design_flow_on_cn()
            self.config.zones[76].verify_design_flow_on_cn()
            self.config.zones[78].verify_design_flow_on_cn()
            self.config.zones[79].verify_design_flow_on_cn()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_12(self):
        """
        change the pipe fill time to 3 minutes \n
        turn on sim mode in the controller \n
        stop the clock so that it can be incremented manually \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        set flow sensor TWF0003 to 15 GPM \n
        perform a learn flow on program 1 \n
        increment the clock by 15 minutes \n
        perform a learn flow on zone 2 \n
        change the flow sensor reading from 15 for a minute to 20 for a minute to 5 for a minute \n
        verify that program 1 learned flow and that zone 2 successful learned flow again \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1].set_fill_time_in_minutes_on_cn(_new_fill_time=3)
            self.config.controllers[1].set_sim_mode_to_on()
            self.config.controllers[1].stop_clock()
            self.config.controllers[1].set_date_and_time_on_cn(_date="04/08/2014", _time="08:00:00")
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=15)
            self.config.controllers[1].set_learn_flow_enabled(self.config.programs[1].ad)
            self.config.controllers[1].do_increment_clock(minutes=25)
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=self.config.programs[1].ad,
                                                              _zn_ad=self.config.zones[52].ad)
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=20)
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=5)
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].do_increment_clock(minutes=15)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            # These are the zones expected to be changed by learn flow
            self.config.zones[51].df = 15
            self.config.zones[52].df = 5
            self.config.zones[53].df = 15

            # Verify the original zone flow settings
            self.config.zones[51].verify_design_flow_on_cn()
            self.config.zones[52].verify_design_flow_on_cn()
            self.config.zones[53].verify_design_flow_on_cn()
            self.config.zones[54].verify_design_flow_on_cn()
            self.config.zones[55].verify_design_flow_on_cn()
            self.config.zones[56].verify_design_flow_on_cn()
            self.config.zones[75].verify_design_flow_on_cn()
            self.config.zones[76].verify_design_flow_on_cn()
            self.config.zones[78].verify_design_flow_on_cn()
            self.config.zones[79].verify_design_flow_on_cn()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_13(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)
