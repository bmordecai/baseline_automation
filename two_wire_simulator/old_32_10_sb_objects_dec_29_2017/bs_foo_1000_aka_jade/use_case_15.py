import sys

from time import sleep
import time

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_1000 import POC1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ar import AlertRelay

from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_1000 import StartConditionFor1000, PauseConditionFor1000

from old_32_10_sb_objects_dec_29_2017.common import helper_methods

__author__ = "Baseline"


class ControllerUseCase15(object):
    """
    Test name: \n
        Alert Relay Test \n
    Purpose: \n
        Test to verify that the alert relay's contact state opens and closes with its flags.
    Coverage area: \n
        1. Set up the controller and limit its concurrent zones \n
        2. Searching and assigning: \n
            - Zones \n
            - Moisture sensors \n
            - Flow meters \n
        3. Set up programs and give them a run time using commands \n
        4. Assign zones to programs and give them run times \n
        5. Set up pause conditions for both programs \n
        6. Assign values to the flow meter \n
        7. Build a POC object and instantiate its variables \n
        8. Load the alert relay to the controller and instantiate all of its variables \n
        9. Verify the full configuration \n
        10. Set alert relay flag to 1 and verify that its contact is open when there is an unexpected high flow message \n
        11. Set alert relay flag to 2 and verify that its contact is open when there is a high flow message \n
        12. Set alert relay flag to 4 and verify that its contact is open when there is a zone flow variance message \n
        13. Set alert relay flag to 8 and verify that its contact is open when there is a device no reply message \n
        14. Set alert relay flag to 16 and verify that its contact is open when there is a checksum message \n
        15. Set alert relay flag to 32 and verify that its contact is open when there is a solenoid error message \n
        16. Set alert relay flag to 64 and verify that its contact is open when there is a program overrun message \n
        17. Verify the full configuration
    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - sets up the controller \n
        - verify basemanager connection
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # only need this for BaseManager
            # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock for
            # the reboot process.
            # self.config.controllers[1].set_sim_mode_to_off()
            self.config.basemanager_connection[1].verify_ip_address_state()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=4)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_2(self):
        """
        Sets the devices that will be used in the configuration of the controller \n
        Search and address the devices:
            - Zones                 {zn}
            - Moisture Sensors      {ms}
            - Master Valves         {mv}
            - Flow Meter            {fm}
        Once the devices are found they can be addressed so that they can be used in the programming
            - Zones can use addresses {1-200}
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # load all devices need into the controller so that they are available for use in the configuration
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.master_valves,
                                                                             ms_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_1000_poc_objects()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_3(self):
        """
        Assign zones to programs and give them run times \n
        Give programs start times \n
        Set the controllers max zone concurrency and the programs max zone concurrency \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            program_8am_start_time = [480]

            self.config.programs[1] = PG1000(_address=1,
                                             _max_concurrent_zones=2,
                                             _point_of_connection_address=[1])
            self.config.programs[2] = PG1000(_address=2,
                                             _max_concurrent_zones=2)

            self.config.program_start_conditions[1] = StartConditionFor1000(program_ad=1)
            self.config.program_start_conditions[1].set_day_time_start(_dt=opcodes.true, _st_list=program_8am_start_time)

            self.config.program_start_conditions[2] = StartConditionFor1000(program_ad=2)
            self.config.program_start_conditions[2].set_day_time_start(_dt=opcodes.true, _st_list=program_8am_start_time)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_4(self):
        """
        Assign the zones to programs
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=2700,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=2700,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=2700,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=2700,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=2700,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[6],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=2700,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[7],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=2700,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[8],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=2700,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_5(self):
        """
        Set up pause conditions for both programs to pause when the moisture percent hits the lower limit of 10% \n
        Set the moisture percent on SB07270 and SB07263 to 11 and 14, respectively \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.program_pause_conditions[1] = PauseConditionFor1000(program_ad=1)
            self.config.program_pause_conditions[1].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.lower_limit, threshold=10)

            self.config.program_pause_conditions[2] = PauseConditionFor1000(program_ad=2)
            self.config.program_pause_conditions[2].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[2].sn, mode=opcodes.lower_limit, threshold=10)

            self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=11)
            self.config.moisture_sensors[1].do_self_test()
            self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=14)
            self.config.moisture_sensors[2].do_self_test()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_6(self):
        """
        Set the flow meters k-value to 2.01 and it's flow rate to 25 gpm \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.flow_meters[1].set_k_value_on_cn(_k_value=2.01)
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=25)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_7(self):
        """
        Set up water source 1 \n
            Enable water source 1 \n
            Give water source 1 a description \n
            Assign master valve TMV0003 and flow meter TWF0003 to water source 1 \n
            Assign water source 1 a target flow of 500 \n
            Set the high flow limit to 550 and enable high flow shut down \n
            Set the unscheduled flow limit to 10 and enable the unscheduled flow shut down \n
            Enable limit concurrent to target \n
            Set the pipe fill time to 2 minutes \n
            Set the flow variance limit to 10% and enable the flow variance \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.poc[1] = POC1000(_address=1,
                                         _enabled_state=opcodes.true,
                                         _master_valve_address=1,
                                         _flow_meter_address=1,
                                         _target_flow=500,
                                         _shutdown_on_high_flow=opcodes.true,
                                         _unscheduled_flow_limit=10,
                                         _shutdown_on_unscheduled=opcodes.true,
                                         _limit_concurrent_zones_to_target=opcodes.true,
                                         _fill_time=2,
                                         _flow_variance_percent=10,
                                         _flow_variance_enable=opcodes.true,
                                         _flow_fault=opcodes.true
                                         )
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_8(self):
        """
        Load the alert relay to controller \n
        Give the alert relay a serial number and address \n
        Initialize all of it's values \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # TODO This way seems to be the best because adding it to the JSON would require a refactor of all JSONs
            self.config.controllers[1].load_dv_to_cn(dv_type=opcodes.alert_relay, list_of_decoder_serial_nums=['AR00001'])
            self.config.alert_relays[1] = AlertRelay(_serial='AR00001', _address=1)
            self.config.alert_relays[1].set_address_on_cn(_ad=1)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.alert_relay)
            self.config.alert_relays[1].set_default_values()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_9(self):
        """
        Increment the clock by 10 seconds to allow the controller to react to commands \n
        Verify the full configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.verify_full_configuration()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_10(self):
        """
        Set the flag on the alert relay to 1 (unexpected high flow) \n
        Set an unexpected high flow message on poc 1 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 1 \n
        Clear message from poc 1 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.alert_relays[1].set_flags_on_cn(_flag=1)
            self.config.poc[1].set_message_on_cn(opcodes.unscheduled_flow_shutdown)
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.open
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
            self.config.poc[1].clear_message_on_cn(opcodes.unscheduled_flow_shutdown)
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.closed
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_11(self):
        """
        Set the flag on the alert relay to 2 (high flow) \n
        Set an high flow message on poc 1 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 2 \n
        Clear message from poc 1 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.alert_relays[1].set_flags_on_cn(_flag=2)
            self.config.poc[1].set_message_on_cn(opcodes.high_flow_shutdown)
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.open
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
            self.config.poc[1].clear_message_on_cn(opcodes.high_flow_shutdown)
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.closed
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_12(self):
        """
        Set the flag on the alert relay to 4 (zone flow variance) \n
        Set a high flow variance message on zone 4 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 4 \n
        Clear message from zone 4 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.alert_relays[1].set_flags_on_cn(_flag=4)
            self.config.zones[4].set_message_on_cn(opcodes.high_flow_variance_shutdown)
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.open
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
            self.config.zones[4].clear_message_on_cn(opcodes.high_flow_variance_shutdown)
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.closed
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_13(self):
        """
        Set the flag on the alert relay to 8 (device no reply) \n
        Set a device no reply message on zone 1 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 8 \n
        Clear message from zone 1 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.alert_relays[1].set_flags_on_cn(_flag=8)
            self.config.zones[1].set_message_on_cn(opcodes.no_response)
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.open
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
            self.config.zones[1].clear_message_on_cn(opcodes.no_response)
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.closed
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_14(self):
        """
        Set the flag on the alert relay to 16 (device checksum) \n
        Set a device checksum message on moisture sensor 1 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 16 \n
        Clear message from moisture sensor 1 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.alert_relays[1].set_flags_on_cn(_flag=16)
            self.config.moisture_sensors[1].set_message_on_cn(opcodes.checksum)
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.open
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
            self.config.moisture_sensors[1].clear_message_on_cn(opcodes.checksum)
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.closed
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_15(self):
        """
        Set the flag on the alert relay to 32 (solenoid error) \n
        Set a device checksum message on zone 2 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 32 \n
        Clear message from zone 2 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.alert_relays[1].set_flags_on_cn(_flag=32)
            self.config.zones[2].set_message_on_cn('OP')
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.open
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
            self.config.zones[2].clear_message_on_cn('OP')
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.closed
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_16(self):
        """
        Set the flag on the alert relay to 64 (program overrun) \n
        Set a device checksum message on program 1 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 64 \n
        Clear message from program 1 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.alert_relays[1].set_flags_on_cn(_flag=64)
            self.config.programs[1].set_message_on_cn(opcodes.program_overrun)
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.open
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
            self.config.programs[1].clear_message_on_cn(opcodes.program_overrun)
            self.config.alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.alert_relays[1].vc = opcodes.closed
            self.config.controllers[1].do_increment_clock(seconds=10)
            self.config.alert_relays[1].verify_contact_state_on_cn()
            self.config.alert_relays[1].verify_flag_on_cn()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_17(self):
        """
        Verify the full configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)
