import sys

import time
from time import sleep

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_1000 import POC1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_1000 import StartConditionFor1000, PauseConditionFor1000, StopConditionFor1000

from old_32_10_sb_objects_dec_29_2017.common import helper_methods

__author__ = "Eldin"


class ControllerUseCase9(object):
    """
    Test name: \n
        MV Pump Test \n
    Purpose: \n
        This test assigns a zone to one or more programs and then sets start and run times. It then runs the controller
        and verifies that all the zones are running at the correct time and follow the concurrent zone limits. \n
    Coverage area: \n
        1. Set up the controller and limit its concurrent zones \n
        2. Searching and assigning: \n
            - Zones \n
            - Master Valves \n
        3. Set up programs and give them a start times and limit their concurrent zones \n
        4. Assign zones to programs and give them run times \n
        5. Set up the POC objects and assign master valves to them \n
        6. Verify the full configuration \n
        7. Set the time to 3:45 A.M., start the controller, and then verify that all zones are responding \n
        8. Increment the time so it is 4:00 A.M. and verify that 3 programs were activated, but only two zones should
           be running \n
        9. Increment by 10 minutes and verify that the zones that were watering at first are done and that new zones
           are watering \n
        10. Increment by another 10 minutes and verify that programs 1 and 2 are done watering along with their zones.
            Program 15 should now be running instead of waiting \n
        11. Increment by 5 minutes so that program 15's first zone is done and verify that the second zone starts \n
        12. Increment by 5 minutes again so program 15's second zone is done and verify that everything is done \n
        13. Increment the time to 12:00 P.M., this makes program 32 start and both of it's zones should also start \n
        14. Increment by 20 minutes and verify that program 32 and both of it's zones are done watering \n
        15. Increment the time to 9:00 P.M., this should start up both program 15 and 23. Both limit their zone
            concurrency to 1, so only the first zone from each program should be running \n
        16. Increment by 5 minutes and verify that both of the first zones on program 15 and 23 are done and that the
            second zones are running \n
        17. Increment by another 5 minutes and verify that everything is done watering \n
        18. Verify the full configuration again \n
    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    self.step_18()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)


    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # only need this for BaseManager
            # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock
            # for the reboot process.
            # self.config.controllers[1].set_sim_mode_to_off()
            self.config.basemanager_connection[1].verify_ip_address_state()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=2)
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller failed: " + ae.message)

    def step_2(self):
        """
        load all devices need into the controller so that they are available for use in the configuration \n
        sets the devices that will be used in the configuration of the controller \n
            - search and address the devices: \n
               - zones                 {zn} \n
               - Master Valves         {mv} \n
            - once the devices are found they can be addressed so that they can be used in the programming \n
               - zones can use addresses {1-200} \n
                - Master Valves can use address {1-8} \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # Load all devices to controller
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.create_1000_poc_objects()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_3(self):
        """
        Set up the programs \n
            - Give programs 1, 2, 15, and 23 a max concurrent zone value of 1 and program 32 a value of 2 \n
            - Give programs start times \n
            - Give program 32 a master valve address of 4, which is serial number TSE0012 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.programs[1] = PG1000(_address=1, _max_concurrent_zones=1,
                                             _water_window=['011111100001111111111110'],
                                             _point_of_connection_address=[1])
            self.config.programs[2] = PG1000(_address=2, _max_concurrent_zones=1,
                                             _water_window=['011111100000111111111111', '000011000000000000011110',
                                                            '111111100000000000011110', '111111100000000000011110',
                                                            '111111100000000000011110', '111111100000000000011110',
                                                            '111111100000000000011110'],
                                             _point_of_connection_address=[3])
            self.config.programs[15] = PG1000(_address=15, _max_concurrent_zones=1,
                                              _water_window=['011111100001111111111110'],
                                              _point_of_connection_address=[3])
            self.config.programs[23] = PG1000(_address=23, _max_concurrent_zones=1,
                                              _water_window=['011111100001111111111110'],
                                              _point_of_connection_address=[1])
            self.config.programs[32] = PG1000(_address=32, _max_concurrent_zones=2,
                                              _water_window=['011111100001111111111110'], _master_valve_address=7,
                                              _point_of_connection_address=[1])

            self.config.program_start_conditions[1] = StartConditionFor1000(program_ad=1)
            self.config.program_start_conditions[2] = StartConditionFor1000(program_ad=2)
            self.config.program_start_conditions[3] = StartConditionFor1000(program_ad=15)
            self.config.program_start_conditions[4] = StartConditionFor1000(program_ad=23)
            self.config.program_start_conditions[5] = StartConditionFor1000(program_ad=32)
            self.config.program_start_conditions[1].set_day_time_start(_dt=opcodes.true, _st_list=[240])
            self.config.program_start_conditions[2].set_day_time_start(_dt=opcodes.true, _st_list=[240])
            self.config.program_start_conditions[3].set_day_time_start(_dt=opcodes.true, _st_list=[240, 1260])
            self.config.program_start_conditions[4].set_day_time_start(_dt=opcodes.true, _st_list=[1260])
            self.config.program_start_conditions[5].set_day_time_start(_dt=opcodes.true, _st_list=[720])
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_4(self):
        """
        Assign zones to programs and give them run times \n
        Notice the different primary zones \n
        The address doesn't always match the pz number \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=600)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=600)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=600)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=600)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[15],
                                                       _rt=300,
                                                       _pz=1)
            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[15],
                                                       _rt=300,
                                                       _pz=2)
            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[23],
                                                       _rt=300,
                                                       _pz=3)
            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[23],
                                                       _rt=300,
                                                       _pz=4)
            self.config.zone_programs[10] = ZoneProgram(zone_obj=self.config.zones[5],
                                                        prog_obj=self.config.programs[32],
                                                        _rt=1200)
            self.config.zone_programs[51] = ZoneProgram(zone_obj=self.config.zones[51],
                                                        prog_obj=self.config.programs[32],
                                                        _rt=1200)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_5(self):
        """
        Set up the water sources \n
        assign master valves to pocs and enable them \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.poc[1] = POC1000(_address=1, _master_valve_address=4, _enabled_state=opcodes.true)
            self.config.poc[3] = POC1000(_address=3, _master_valve_address=6, _enabled_state=opcodes.true)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_6(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_7(self):
        """
        Set the date and time on the controller \n
        Start the controller and verify all the zones and programs are working \n
        set the clock to 04/07/2014 3:45 am \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 done watering \n
            - zone 76 done watering \n
            - zone 78 done watering \n
            - zone 79 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_date_and_time_on_cn(_date="04/07/2014",_time="03:45:00")
            self.config.controllers[1].set_controller_to_run()

            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            self.config.programs[15].get_data()
            self.config.programs[23].get_data()
            self.config.programs[32].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[15].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[23].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[32].verify_status_on_cn(_expected_status=opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_8(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 15 minutes to put the time at 04/07/2014 4:00 A.M. \n
        At 4:00 A.M. programs 1, 2, and 15 all start. However, each program only allows 1 concurrent zone, and the
         controller only allows 2 zones. So only the first zone of program 1 and 2 will be running. \n
        Start the controller and verify all the zones and programs are working \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
            - program 15 waiting to water \n
            - program 23 done watering \n
            - program 32 done watering \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=15)
            self.config.controllers[1].verify_date_and_time()

            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            self.config.programs[15].get_data()
            self.config.programs[23].get_data()
            self.config.programs[32].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[15].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
            self.config.programs[23].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[32].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[51].verify_status_on_cn(status=opcodes.done_watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_9(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 10 minutes to put the time at 04/07/2014  4:10 A.M. \n
        Programs 1 and 2 have a runtime of 600 seconds (10 minutes) so their first zones should be done watering \n
        At 4:10 A.M. programs 1, 2, and 15 are running their second zones. There should still only be two zones running,
         but zone 1 should have finished running. However, since zone 1 is also attached to program 15, it should be
         waiting to water. \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
            - program 15 waiting to water \n
            - program 23 done watering \n
            - program 32 done watering \n
        - Zone status: \n
            - zone 1 waiting to water \n
            - zone 2 watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()

            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            self.config.programs[15].get_data()
            self.config.programs[23].get_data()
            self.config.programs[32].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[15].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
            self.config.programs[23].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[32].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[51].verify_status_on_cn(status=opcodes.done_watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_10(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 10 minutes to put the time at 04/07/2014 4:20 A.M. \n
        Programs 1 and 2 have a runtime of 600 seconds (10 minutes) so their second zones should be done watering \n
        At 4:20 A.M. programs 1 and 2 should be done watering since they both had two zones attached and both should
         have run for 10 minutes. Program 15 is the only other program that started at 4:00 A.M. so it should be the
         only program running. It allows only 1 concurrent zone so only zone 1 should be running with a runtime of 300
         seconds (5 minutes). \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 watering \n
            - program 23 done watering \n
            - program 32 done watering \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()

            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            self.config.programs[15].get_data()
            self.config.programs[23].get_data()
            self.config.programs[32].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[15].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[23].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[32].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[51].verify_status_on_cn(status=opcodes.done_watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_11(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 5 minutes to put the time at 04/07/2014  4:25 A.M. \n
        Program 15 has a runtime of 300 seconds (5 minutes), so now it's first zone should be done running. \n
        At 4:25 A.M. program 15 should be the only program left running. Since the clock was incremented 5 minutes,
         zone 1 should be done watering again, and zone 2 should start up again and run for 5 minutes. \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 watering \n
            - program 23 done watering \n
            - program 32 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()

            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            self.config.programs[15].get_data()
            self.config.programs[23].get_data()
            self.config.programs[32].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[15].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[23].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[32].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[51].verify_status_on_cn(status=opcodes.done_watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_12(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 5 minutes to put the time at 04/07/2014 4:30 A.M. \n
        Program 15 has a runtime of 300 seconds (5 minutes), so now it's second zone should be done running. \n
        At 4:30 A.M. program 15 should be done watering. Both of it's attached zones had a runtime of 5 minutes, and
         now both of them are done. \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 done watering \n
            - program 23 done watering \n
            - program 32 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()

            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            self.config.programs[15].get_data()
            self.config.programs[23].get_data()
            self.config.programs[32].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[15].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[23].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[32].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[51].verify_status_on_cn(status=opcodes.done_watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_13(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 7 hours and 30 minutes to put the time at 04/07/2014 12:00 P.M. \n
        Program 32 is the only program with a start time at 12:00 P.M. and it can run two concurrent zones. \n
        At 12:00 P.M. program 32 should be the only program running. Both of it's zones have a runtime of 20 minutes
         and both of them should be running since the controller and program 32 both have a two concurrent zone limit \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 done watering \n
            - program 23 done watering \n
            - program 32 running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
            - zone 51 watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=7, minutes=30)
            self.config.controllers[1].verify_date_and_time()

            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            self.config.programs[15].get_data()
            self.config.programs[23].get_data()
            self.config.programs[32].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[15].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[23].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[32].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[51].verify_status_on_cn(status=opcodes.watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_14(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 20 minutes to put the time at 04/07/2014 12:20 P.M. \n
        Program 32's zones have a runtime of 20 minutes, so they should both be done watering \n
        At 12:20 P.M. program 32 should be done watering. Both of it's zones should also be done after 20 minutes \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 done watering \n
            - program 23 done watering \n
            - program 32 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=20)
            self.config.controllers[1].verify_date_and_time()

            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            self.config.programs[15].get_data()
            self.config.programs[23].get_data()
            self.config.programs[32].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[15].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[23].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[32].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[51].verify_status_on_cn(status=opcodes.done_watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_15(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 8 hours and 40 minutes to put the time at 04/07/2014 9:00 P.M. \n
        Program 15 had two run times and one of them was 9:00 P.M., so zones 1 and 2 should be watering soon \n
        Program 23 has a run time of 9:00, so zones , so zones 3 and 4 should be watering soon \n
        At 9:00 P.M. programs 15 and 32 both have a concurrent zone limit of 1. This means that only the first zones
         attached to each program should be running \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 running \n
            - program 23 running \n
            - program 32 done watering \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=8, minutes=40)
            self.config.controllers[1].verify_date_and_time()

            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            self.config.programs[15].get_data()
            self.config.programs[23].get_data()
            self.config.programs[32].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[15].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[23].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[32].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[51].verify_status_on_cn(status=opcodes.done_watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_16(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 5 minutes to put the time at 04/07/2014 9:05 P.M. \n
        At 9:05 P.M. both program 15's and 23's zones have a run time of 5 minutes. After the increment of 5 minutes,
         the first zones should be done and the second zones on each program should be watering \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 running \n
            - program 23 running \n
            - program 32 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()

            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            self.config.programs[15].get_data()
            self.config.programs[23].get_data()
            self.config.programs[32].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[15].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[23].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[32].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[51].verify_status_on_cn(status=opcodes.done_watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_17(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 5 minutes to put the time at 04/07/2014 9:10 P.M. \n
        At 9:10 P.M. both program 15's and 23's zones have run for 5 minutes, which means that everything should be
         done \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 done watering \n
            - program 23 done watering \n
            - program 32 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()

            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            self.config.programs[15].get_data()
            self.config.programs[23].get_data()
            self.config.programs[32].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[15].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[23].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[32].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[51].verify_status_on_cn(status=opcodes.done_watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_18(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)
