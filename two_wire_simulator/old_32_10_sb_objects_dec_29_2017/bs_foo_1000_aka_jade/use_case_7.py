import sys


# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

import time
from time import sleep
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_1000 import StartConditionFor1000, PauseConditionFor1000, StopConditionFor1000

from old_32_10_sb_objects_dec_29_2017.common import helper_methods

__author__ = "Eldin"


class ControllerUseCase7(object):
    """
    Test name: \n
        Temperature Decoder Test \n
    Purpose: \n
        This test covers using start/stop/pause using temperature sensors \n
    Coverage area: \n
        1. Set up the controller and limit its concurrent zones \n
        2. Searching and assigning: \n
            - Zones \n
            - Temperature sensors \n
        3. Set up programs and give them a run time using commands \n
        4. Assign zones to programs and give them run times \n
        5. Set up pause conditions for programs 1 and 2 to pause when the respective temperature sensor reading is below
           32 degrees \n
        6. Verify the full configuration \n
        7-8. Run the programs without any conditions to ensure that everything runs as expected \n
        9. Set temperature to be lower than both programs lower limits and verify that all programs/zones are paused \n
        10-12. Set the temperature to exceed both programs upper limit and verify that all programs/zones are paused \n
        13-14. Set program 1 to start when the temperature on TAT0001 is below 90. Verify that program 1 runs \n
        15-17. Set program 2 to also start when the temperature is below 90. Verify that both programs are running \n
        18-21. Set program 1 and 2 to have a start condition, but only make a stop condition for program 1. Verify that
               both start correctly, and then verify that only program 1 and its zones stop \n
        22-24. Keep the conditions from the previous steps and give program 2 a stop condition that is the same as
               program 1. Verify that both programs start and stop when their conditions are triggered. \n
        25. Verify the full configuration again \n
    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    self.step_18()
                    self.step_19()
                    self.step_20()
                    self.step_21()
                    self.step_22()
                    self.step_23()
                    self.step_24()
                    self.step_25()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - Turn on sim moder \n
            - set the time out on the serial port \n
            - Give the controller a serial number \n
            - Give the controller a description \n
            - Give the controller a location \n
            - Limit the number of concurrent zones to 4 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # only need this for BaseManager
            # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock
            # for the reboot process.
            # self.config.controllers[1].set_sim_mode_to_off()
            self.config.basemanager_connection[1].verify_ip_address_state()
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=4)
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller failed: " + ae.message)

    def step_2(self):
        """
        load all devices need into the controller so that they are available for use in the configuration \n
        sets the devices that will be used in the configuration of the controller \n
            - search and address the devices: \n
               - zones                 {zn} \n
               - Master Valves         {mv} \n
            - once the devices are found they can be addressed so that they can be used in the programming \n
               - zones can use addresses {1-200} \n
            - temperature sensor TAT0001     {ts} \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # Load all devices to controller
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                             ts_ad_range=self.config.ts_ad_range)
            self.config.create_1000_poc_objects()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_3(self):
        """
        Set up the programs \n
            - Give programs a max concurrent zone value of 2 \n
            - Give programs start times \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.programs[1] = PG1000(_address=1, _max_concurrent_zones=2)
            self.config.programs[2] = PG1000(_address=2, _max_concurrent_zones=2)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_4(self):
        """
        assign zones to programs and give them run times \n
            - give each zone a runtime \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=2700)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=2700)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=2700)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=2700)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=2700)
            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[6],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=2700)
            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[7],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=2700)
            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[8],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=2700)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_5(self):
        """
        set up start stop pause conditions
            Program 1:
                - start times 8:00 am \n
                - Setup pause programs to pause when the temperature reading goes below 32 for both program 1 and 2 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.program_start_conditions[1] = StartConditionFor1000(program_ad=1)
            self.config.program_start_conditions[2] = StartConditionFor1000(program_ad=2)
            self.config.program_start_conditions[1].set_day_time_start(_dt=opcodes.true, _st_list=[480])
            self.config.program_start_conditions[2].set_day_time_start(_dt=opcodes.true, _st_list=[480])

            self.config.program_pause_conditions[1] = PauseConditionFor1000(program_ad=1)
            self.config.program_pause_conditions[2] = PauseConditionFor1000(program_ad=2)
            self.config.program_pause_conditions[1].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.lower_limit, threshold=32)
            self.config.program_pause_conditions[2].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.lower_limit, threshold=32)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_6(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_7(self):
        """
        set the clock 2 minutes to make the time 04/08/2014 8:00 am \n
        Verify that all zones are set to done before the test starts
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_date_and_time_on_cn(_date="04/08/2014", _time="08:00:00")
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].verify_date_and_time()
            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_8(self):
        """
        Start program 1 & program 2
        Start the programs and verify that only two zones are running on each program due to concurrent limit \n
        advance the clock 5 minutes to make the time 04/08/2014 8:05
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[1].ad, _function="SR")
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[2].ad, _function="SR")
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_9(self):
        """
        Test that a single temperature sensor can pause both programs \n
        Set TAT0001 to be 31, which is lower than the 'lower limit', and verify that both programs pause \n
        advance the clock 5 minutes to make the time 04/08/2014 8:10 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 paused \n
            - program 2 paused \n
        - Zone status: \n
            - zone 1 paused \n
            - zone 2 paused \n
            - zone 3 paused \n
            - zone 4 paused \n
            - zone 5 paused \n
            - zone 6 paused \n
            - zone 7 paused \n
            - zone 8 paused \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=31)
            self.config.temperature_sensors[1].do_self_test()
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.paused)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.paused)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.paused)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_10(self):
        """
        Setup pause programs to pause when the temperature reading is above 90 for both program 1 and 2 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.program_pause_conditions[1].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.upper_limit, threshold=90)
            self.config.program_pause_conditions[2].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.upper_limit, threshold=90)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_11(self):
        """
        Test that a single temperature sensor can pause both programs \n
        Set TAT0001 to be 95, which is higher than the 'upper limit', verify that both programs and all zones pause \n
        advance the clock 5 minutes to make the time 04/08/2014 8:15 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 paused \n
            - program 2 paused \n
        - Zone status: \n
            - zone 1 paused \n
            - zone 2 paused \n
            - zone 3 paused \n
            - zone 4 paused \n
            - zone 5 paused \n
            - zone 6 paused \n
            - zone 7 paused \n
            - zone 8 paused \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=95)
            self.config.temperature_sensors[1].do_self_test()
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.paused)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.paused)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.paused)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_12(self):
        """
        Verify that all zones are set to done before the next test starts
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[1].ad, _function="SP")
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[2].ad, _function="SP")
            self.config.controllers[1].set_controller_to_run()
            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_13(self):
        """
        Turn off the pause conditions that were previously set \n
        Setup a start condition when the temperature reading is above 90 for program 1 only \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.program_pause_conditions[1].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.off)
            self.config.program_pause_conditions[2].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.off)

            self.config.program_start_conditions[3] = StartConditionFor1000(program_ad=1)
            self.config.program_start_conditions[3].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.lower_limit, threshold=90)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_14(self):
        """
        Set the temperature to 65 on temp sensor TAT0001, which is below the lower limit for temperature \n
        This should cause program 1 to start as it has a lower limit condition set in the previous step, but only the
         first two zones should be running since there is a max zone concurrency of 2 \n
         advance the clock 5 minutes to make the time 04/08/2014 8:20
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=65)
            self.config.temperature_sensors[1].do_self_test()
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.done_watering)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_15(self):
        """
        Setup a start condition when the temperature reading is above 90 for program 2 as well \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=95)
            self.config.temperature_sensors[1].do_self_test()

            self.config.program_start_conditions[4] = StartConditionFor1000(program_ad=2)
            self.config.program_start_conditions[4].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.lower_limit, threshold=90)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_16(self):
        """
        Set the temperature to 75 on temp sensor TAT0001, which is below the lower limit for temperature \n
        This should cause program 1 and 2 to start running because they have a lower limit of 90. However, only the
         first two zones should be running in each program since there is a max zone concurrency of 2 \n
         advance the clock 5 minutes to make the time 04/08/2014 8:25
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=75)
            self.config.temperature_sensors[1].do_self_test()
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_17(self):
        """
        Turn off both programs \n
        Turn off the start conditions that were previously set \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_program_start_stop(self.config.programs[1].ad, _function="SP")
            self.config.controllers[1].set_program_start_stop(self.config.programs[2].ad, _function="SP")

            self.config.program_start_conditions[3].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.off)
            self.config.program_start_conditions[4].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.off)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_18(self):
        """
        Set up two new start conditions that start both programs 1 and 2 when the temperature drops below 70 \n
        Set up a stop condition for program 1 when the temperature is higher than 90 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.program_start_conditions[3].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.lower_limit, threshold=70)
            self.config.program_start_conditions[4].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.lower_limit, threshold=70)
            self.config.program_stop_conditions[1] = StopConditionFor1000(program_ad=1)
            self.config.program_stop_conditions[1].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.upper_limit, threshold=90, si=opcodes.true)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_19(self):
        """
        Set the temperature to 65 on temp sensor TAT0001, which is below the lower limit for temperature \n
        This should cause program 1 and 2 to start running because they have a lower limit of 70. However, only the
         first two zones should be running in each program since there is a max zone concurrency of 2 \n
        advance the clock 5 minutes to make the time 04/08/2014 8:30
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=65)
            self.config.temperature_sensors[1].do_self_test()
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_20(self):
        """
        Set the temperature to 95 on temp sensor TAT0001, which is above the upper limit for temperature \n
        This should cause program 1 to stop running because it has an upper limit of 90. This should cause program 1
         and all of it's zones to be done watering \n
        advance the clock 5 minutes to make the time 04/08/2014 8:35
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=95)
            self.config.temperature_sensors[1].do_self_test()
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_21(self):
        """
        Turn off both programs \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_program_start_stop(self.config.programs[1].ad, _function="SP")
            self.config.controllers[1].set_program_start_stop(self.config.programs[2].ad, _function="SP")
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_22(self):
        """
        Set up a the temperature so it neither starts or stops the programs \n
        Set up a stop condition for program 2 when the temperature is higher than 90 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=80)
            self.config.temperature_sensors[1].do_self_test()
            self.config.program_stop_conditions[2] = StopConditionFor1000(program_ad=2)
            self.config.program_stop_conditions[2].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.upper_limit, threshold=90, si=opcodes.true)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_23(self):
        """
        Set the temperature to 65 on temp sensor TAT0001, which is below the lower limit for temperature \n
        This should cause program 1 and 2 to start running because they have a lower limit of 70. However, only the
         first two zones should be running in each program since there is a max zone concurrency of 2 \n
        advance the clock 5 minutes to make the time 04/08/2014 8:40
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=65)
            self.config.temperature_sensors[1].do_self_test()
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_24(self):
        """
        Raise the value of the temperature on the temperature sensor to 95 which is above both programs upper limit \n
        This should cause both programs and their zones to stop \n
        advance the clock 1 minutes to make the time 04/08/2014 8:41
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=95)
            self.config.temperature_sensors[1].do_self_test()
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            # This is necessary for our data in zone to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_25(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)
