import sys
from time import sleep
import time

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_1000 import StartConditionFor1000, PauseConditionFor1000, StopConditionFor1000

from old_32_10_sb_objects_dec_29_2017.common import helper_methods

__author__ = "Eldin"


class ControllerUseCase5(object):
    """
    Test name: \n
        Event Switch Test \n
    Purpose: \n
        The purpose of this test is to verify that an event switch causes programs to act in a certain way when opened
        and closed. This includes the event switch starting, stopping, and pausing programs. \n
    Coverage Area: \n
        This test covers using the start/stop/pause functions with event decoders. \n
            1. Initialize the controller \n
            2. Searching and assigning: \n
                - Zones \n
                - Flow Meters \n
                - Event Switches \n
            3. Set up programs \n
            4. Assign each zone to a program \n
            5. Set max zone concurrency for the controller and programs \n
            6. Verify the full configuration \n
            7. Set program 2 stop condition when the event switch is open \n
            8. Disable day/time start for both program 2 and 3 \n
            9. Set the event switch to closed and verify that all zones and programs are 'DN' before the programs start \n
            10. Start both programs and verify that all zones and programs are in line with concurrency settings \n
            11. Open the event switch and verify that program 2 stops while program 3 keeps running \n
            12. Close the event switch and verify that program 2 is still stopped and program 3 is still running \n
            13. Stop both programs and verify that all zones and programs are 'done watering'
            14. Set program 3 to start when the event switch is open and then close the event switch. Verify that all zones
                and programs have the expected statuses. \n
            15. Open the event switch and verify that program 3 starts \n
            16. Close the event switch and again verify that program 2 is still stopped and program 3 is still running
            17. Stop both programs and verify that all zones and programs are 'done watering'
            18. Set program 2 to pause for 60 seconds when the event switch is opened. Turn off any start or stop commands
                from previously. Close the event switch and start the programs then verify the zones and programs \n
            19. Open the switch and verify that program 2 and it's attached zones are paused while program 3 is running \n
            20. Close the event switch and verify that program 2 is still paused and program 3 is continuing to run \n
            21. Verify the full configuration before ending the use case \n
    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    self.step_18()
                    self.step_19()
                    self.step_20()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - Turn on sim moder \n
            - set the time out on the serial port \n
            - Give the controller a serial number \n
            - Give the controller a description \n
            - Give the controller a location \n
            - Limit the number of concurrent zones to 1 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # only need this for BaseManager
            # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock
            # for the reboot process.
            # self.config.controllers[1].set_sim_mode_to_off()
            self.config.basemanager_connection[1].verify_ip_address_state()
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=1)
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller failed: " + ae.message)

    def step_2(self):
        """
         Load Devices to controller
         load all devices need into the controller so that they are available for use in the configuration \n
         sets the devices that will be used in the configuration of the controller \n
             - search and address the devices: \n
                - zones                 {zn} \n
                - Master Valves         {mv} \n
             - once the devices are found they can be addressed so that they can be used in the programming \n
                - zones can use addresses {1-200} \n
                 - Master Valves can use address {1-8} \n
             - the 3200 auto address certain devices in the order it receives them: \n
                 - Master Valves         {mv} \n
                 - Moisture Sensors      {ms} \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # Load all devices to controller
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.create_1000_poc_objects()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_3(self):
        """
        Set up the programs
            - Give program 2 a max concurrency for the program of 3 zones \n
            - Give program 3 a max concurrency for the program of 3 zones \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.programs[2] = PG1000(_address=2,
                                             _max_concurrent_zones=3)
            self.config.programs[3] = PG1000(_address=3,
                                             _max_concurrent_zones=3)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_4(self):
        """
        Assign zones to programs \n
        Give zones run times \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.zone_programs[52] = ZoneProgram(zone_obj=self.config.zones[52],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=2700)
            self.config.zone_programs[53] = ZoneProgram(zone_obj=self.config.zones[53],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=2700)
            self.config.zone_programs[54] = ZoneProgram(zone_obj=self.config.zones[54],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=2700)
            self.config.zone_programs[55] = ZoneProgram(zone_obj=self.config.zones[55],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=2700)
            self.config.zone_programs[75] = ZoneProgram(zone_obj=self.config.zones[75],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=300)
            self.config.zone_programs[76] = ZoneProgram(zone_obj=self.config.zones[76],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=300)
            self.config.zone_programs[78] = ZoneProgram(zone_obj=self.config.zones[78],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=300)
            self.config.zone_programs[79] = ZoneProgram(zone_obj=self.config.zones[79],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=300)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_5(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_6(self):
        """
        set the original event switch sensor settings \n
        set event switch TPD0001 to stop program 2 when contacts open \n
        This sets up the condition for the event switch to trigger on
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.program_stop_conditions[1] = StopConditionFor1000(program_ad=2)
            self.config.program_stop_conditions[1].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.open, si=opcodes.true)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_7(self):
        """
        disable day time start for program 2 \n
        disable day time start for program 3 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.program_start_conditions[1] = StartConditionFor1000(program_ad=2)
            self.config.program_start_conditions[2] = StartConditionFor1000(program_ad=3)
            self.config.program_start_conditions[1].set_day_time_start(_dt=opcodes.false)
            self.config.program_start_conditions[2].set_day_time_start(_dt=opcodes.false)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_8(self):
        """
        Set event switch TPD0001 to closed \n
        By setting the initial state of the switch to closed you are telling the program not to run
        Because of how the 1000 is set up you either have to wait a few minutes or change the dial position
        Before you can start a program, after doing a search \n
        Set the date and time of the controller so that there is a known days of the week and days of the month \n
        Increment time by one second so that the controller is at the top of the hour this will get the correct zone
        Status set \n
        Verify that all zones are functioning \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 done watering \n
            - zone 76 done watering \n
            - zone 78 done watering \n
            - zone 79 done watering \n
        start both programs \n
        verify that both programs are still functioning properly \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.closed)
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].set_date_and_time_on_cn(_date="04/08/2014", _time="06:59:59")
            self.config.controllers[1].do_increment_clock(seconds=1)
            # This is necessary for our data in zone to update
            self.config.programs[2].get_data()
            self.config.programs[3].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            # Verify that no zones are running. This is known by checking the status for 'DN'
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_9(self):
        """
        Start both programs \n
        Verify that both programs are still functioning properly \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 waiting to water \n
        - Zone status: \n
            - zone 52 watering \n
            - zone 53 waiting to water \n
            - zone 54 waiting to water \n
            - zone 55 waiting to water \n
            - zone 75 waiting to water \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[2].ad, _function="SR")
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[3].ad, _function="SR")
            self.config.controllers[1].do_increment_clock(minutes=1)
            # This is necessary for our data in zone to update
            self.config.programs[2].get_data()
            self.config.programs[3].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[52].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[53].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[54].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[55].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
            self.config.zones[75].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[76].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[78].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[79].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_10(self):
        """
        Set event switch TPD0001 to open \n
        This section runs off the time from the section above \n
        Advance clock 2 minutes \n
        Because the the event switch gets opened program 2 will stop running
        Verify that program 2 stops and that program 3 continues to run \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 running \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 watering \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.open)
            self.config.controllers[1].do_increment_clock(minutes=2)
            # This is necessary for our data in zone to update
            self.config.programs[2].get_data()
            self.config.programs[3].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[52].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[53].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[54].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[55].verify_status_on_cn(status=opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[75].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[76].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[78].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[79].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_11(self):
        """
        Set event switch TPD0001 to closed \n
        This section runs off the same time settings as the first verification \n
        Run the clock for 2 more minutes \n
        verify that closing the switch didn't cause program two to start running
        Verify that program 2 is still stopped and that program 3 is still running \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 running \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 watering \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        Stop both programs \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.closed)
            self.config.event_switches[1].set_two_wire_drop_value_on_cn(_value=1.8)
            self.config.controllers[1].do_increment_clock(minutes=2)
            # This is necessary for our data in zone to update
            self.config.programs[2].get_data()
            self.config.programs[3].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[52].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[53].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[54].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[55].verify_status_on_cn(status=opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[75].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[76].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[78].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[79].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_12(self):
        """
        Stop both programs and verify that everything is 'done watering' \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 done watering \n
            - zone 76 done watering \n
            - zone 78 done watering \n
            - zone 79 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[2].ad,
                                                              _function="SP")
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[3].ad,
                                                              _function="SP")
            self.config.controllers[1].do_increment_clock(seconds=1)
            # This is necessary for our data in zone to update
            self.config.programs[2].get_data()
            self.config.programs[3].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            # Verify that no zones are running. This is known by checking the status for 'DN'
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_13(self):
        """
        Change the original event switch sensor settings \n
        assign event switch to program 3 so that it is on program 2 and 3 \n
                Set event switch TPD0001 to start program 3 when contacts open \n
        Now the event switch is set to stop program 2 and start program 3 when switch opens \n
        Start both programs \n
        Advance the clock 2 minutes \n
        Verify that program 2 starts watering while program 3 is waiting \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 waiting to water \n
        - Zone status: \n
            - zone 52 watering \n
            - zone 53 waiting to water \n
            - zone 54 waiting to water \n
            - zone 55 waiting to water \n
            - zone 75 waiting to water \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.program_start_conditions[3] = StartConditionFor1000(program_ad=3)
            self.config.program_start_conditions[3].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.open)
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[2].ad, _function="SR")
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[3].ad, _function="SR")
            self.config.controllers[1].do_increment_clock(minutes=2)
            # This is necessary for our data in zone to update
            self.config.programs[2].get_data()
            self.config.programs[3].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[52].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[53].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[54].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[55].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
            self.config.zones[75].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[76].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[78].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[79].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_14(self):
        """
        Set event switch TPD0001 to open \n
        Advance time 2 minutes \n
        Verify that program 2 stops when the switch is opened and program 3 continues to water  when the switch opens \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 running \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 watering \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.open)
            self.config.controllers[1].do_increment_clock(minutes=2)
            # This is necessary for our data in zone to update
            self.config.programs[2].get_data()
            self.config.programs[3].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[52].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[53].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[54].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[55].verify_status_on_cn(status=opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[75].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[76].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[78].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[79].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_15(self):
        """
        Set event switch TPD0001 to closed \n
        Advance the clock 2 minutes \n
        Verify that program 2 stays stopped and program 3 continues to water even though the switch is set to closed\n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 running \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 watering \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.closed)
            self.config.controllers[1].do_increment_clock(minutes=2)
            # This is necessary for our data in zone to update
            self.config.programs[2].get_data()
            self.config.programs[3].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[52].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[53].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[54].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[55].verify_status_on_cn(status=opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[75].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[76].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[78].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[79].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_16(self):
        """
        Stop both programs and verify that everything is 'done watering' \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 done watering \n
            - zone 76 done watering \n
            - zone 78 done watering \n
            - zone 79 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[2].ad,
                                                              _function="SP")
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[3].ad,
                                                              _function="SP")
            self.config.controllers[1].do_increment_clock(seconds=1)
            # This is necessary for our data in zone to update
            self.config.programs[2].get_data()
            self.config.programs[3].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            # Verify that no zones are running. This is known by checking the status for 'DN'
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_17(self):
        """
        Change the event switch sensor settings again \n
        Set event switch TPD0001 to pause program 2 for 60 minutes when contacts open \n
        disable event switch TPD0001 from stopping program 2 \n
        disable event switch TPD0001 from starting program 3 \n
        Set Event switch to closed \n
        Start both programs \n
        Advance the clock 5 minutes \n
        Verify that program 2 starts off running and program 3 starts off waiting \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 waiting to water \n
        - Zone status: \n
            - zone 52 watering \n
            - zone 53 waiting to water \n
            - zone 54 waiting to water \n
            - zone 55 waiting to water \n
            - zone 75 waiting to water \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.program_pause_conditions[1] = PauseConditionFor1000(program_ad=2)
            self.config.program_pause_conditions[1].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.open, pt=60)
            self.config.program_stop_conditions[1].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.off)
            self.config.program_start_conditions[3].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.off)

            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.closed)
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[2].ad, _function="SR")
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[3].ad, _function="SR")
            self.config.controllers[1].do_increment_clock(minutes=5)
            # This is necessary for our data in zone to update
            self.config.programs[2].get_data()
            self.config.programs[3].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[52].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[53].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[54].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[55].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
            self.config.zones[75].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[76].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[78].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[79].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_18(self):
        """
        Set event switch TPD0001 to open \n
        Advance the clock 1 minute \n
        Verify that program 2 pauses when the switch opens and that program 3 starts running \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 paused \n
            - program 2 running \n
        - Zone status: \n
            - zone 52 paused \n
            - zone 53 paused \n
            - zone 54 paused \n
            - zone 55 paused \n
            - zone 75 watering \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.open)
            self.config.event_switches[1].set_two_wire_drop_value_on_cn(_value=1.8)
            self.config.controllers[1].do_increment_clock(minutes=1)
            # This is necessary for our data in zone to update
            self.config.programs[2].get_data()
            self.config.programs[3].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.paused)
            self.config.zones[52].verify_status_on_cn(status=opcodes.paused)
            self.config.zones[53].verify_status_on_cn(status=opcodes.paused)
            self.config.zones[54].verify_status_on_cn(status=opcodes.paused)
            self.config.zones[55].verify_status_on_cn(status=opcodes.paused)
            self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[75].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[76].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[78].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[79].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_19(self):
        """
        Set event switch TPD0001 to open \n
        Advance the clock 8 minutes \n
        Verify that program 2 remains paused and that program 3 continues to run \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 paused \n
            - program 2 running \n
        - Zone status: \n
            - zone 52 paused \n
            - zone 53 paused \n
            - zone 54 paused \n
            - zone 55 paused \n
            - zone 75 done \n
            - zone 76 watering \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.open)
            self.config.controllers[1].do_increment_clock(minutes=8)
            # This is necessary for our data in zone to update
            self.config.programs[2].get_data()
            self.config.programs[3].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.paused)
            self.config.zones[52].verify_status_on_cn(status=opcodes.paused)
            self.config.zones[53].verify_status_on_cn(status=opcodes.paused)
            self.config.zones[54].verify_status_on_cn(status=opcodes.paused)
            self.config.zones[55].verify_status_on_cn(status=opcodes.paused)
            self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[75].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[76].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[78].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[79].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_20(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.verify_full_configuration()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)
