
import sys
from datetime import time
from time import sleep

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.cn import Controller
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zn import Zone
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_1000 import StartConditionFor1000

__author__ = 'Tige'


class ControllerUseCase8(object):
    """
    Test name: \n
        -  Stat Times Test \n
    Coverage Area:
        1. test sever start times turning on at one time \n
        2. test the controller hitting a second start time while zones are watering \n
        3. verify that odd zones run for 1 minute and even zones run for 2 minutes \n
        4. tests that only 10 programs can start at one do to memory in the 1000 even when 20 program are requested to start \n
        5. able to set zone concurrency for the controller and each program. controller has 2 max zones and each program
           has 1 max zone \n
        6. verify that 2 zones are always running \n
        7. Test the when new start times are reach the controller starts over
    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file ):
        """
        Initialize 'UseCase8' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """

        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        Step 1:
            - configure controller:
            - configure BaseManager: \n
        step 2:
            - setting up devices:
        Step 3:
            - set up programs: \n
        Step 4:
            - set up zone programs : \n
        Step 5:
            - set up pocs: \n
        Step 6:
            - set up start stop pause conditions: \n
        Step 7:
            - verify the full configuration of the controller: \n

        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    self.step_18()
                    self.step_19()
                    self.step_20()
                    self.step_21()
                    self.step_22()
                    self.step_23()
                    self.step_24()
                    self.step_25()
                    self.step_26()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise

        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        """
        try:

            self.config.controllers[1].init_cn()
            self.config.basemanager_connection[1].verify_ip_address_state()
        except AssertionError, ae:
            raise AssertionError("Set the controllers serial number and give a description and location" + ae.message)

        try:
            # self.set_number_of_concurrent_zones_for_controller(number_of_concurrent_zones=4)
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=4)
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller" + ae.message)

    def step_2(self):
        """
        load all devices need into the controller so that they are available for use in the configuration \n
        sets the devices that will be used in the configuration of the controller \n
            - search and address the devices: \n
               - zones                 {zn} \n
               - Master Valves         {mv} \n
            - once the devices are found they can be addressed so that they can be used in the programming \n
               - zones can use addresses {1-200} \n
                - Master Valves can use address {1-8} \n
            - the 3200 auto address certain devices in the order it receives them: \n
                - Master Valves         {mv} \n
                - Moisture Sensors      {ms} \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # load all devices need into the controller so that they are available for use in the configuration
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-100
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.create_1000_poc_objects()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_3(self):
        """
        assign each zone to their own program and a run time either 1 minute or 2 minutes \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=2)
            for program in range(1, 21):
                self.config.programs[program] = PG1000(_address=program, _max_concurrent_zones=1)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_4(self):
        """
        set up start stop pause conditions
            Program 1 - 20:
                - start times 12:00 pm and a 11:45 pm\n
                - watering days are set to weekly \n
                - Watering days enabled are every day\n
                - assign moisture sensor SB05308 to start program 1 and give it a threshold of 25 \n
        """
        start_times = [0, 1425]
        every_day_watering = [1, 1, 1, 1, 1, 1, 1]  # runs every day
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for program in range(1, 21):
                self.config.program_start_conditions[program] = StartConditionFor1000(program_ad=program)

                self.config.program_start_conditions[program].set_day_time_start(_dt=opcodes.true,
                                                                                 _st_list=start_times,
                                                                                 _interval_type=opcodes.week_days,
                                                                                 _interval_args=every_day_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_5(self):
        """
        assign each zone to their own program and a run time either 1 minute or 2 minutes \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1], _rt=60)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[2], _rt=120)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[3], _rt=60)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[4], _rt=120)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[5], _rt=60)
            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[6],
                                                       prog_obj=self.config.programs[6], _rt=120)
            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[7],
                                                       prog_obj=self.config.programs[7], _rt=60)
            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[8],
                                                       prog_obj=self.config.programs[8], _rt=120)
            self.config.zone_programs[9] = ZoneProgram(zone_obj=self.config.zones[9],
                                                       prog_obj=self.config.programs[9], _rt=60)
            self.config.zone_programs[10] = ZoneProgram(zone_obj=self.config.zones[10],
                                                        prog_obj=self.config.programs[10], _rt=120)
            self.config.zone_programs[11] = ZoneProgram(zone_obj=self.config.zones[11],
                                                        prog_obj=self.config.programs[11], _rt=60)
            self.config.zone_programs[12] = ZoneProgram(zone_obj=self.config.zones[12],
                                                        prog_obj=self.config.programs[12], _rt=120)
            self.config.zone_programs[17] = ZoneProgram(zone_obj=self.config.zones[17],
                                                        prog_obj=self.config.programs[13], _rt=60)
            self.config.zone_programs[18] = ZoneProgram(zone_obj=self.config.zones[18],
                                                        prog_obj=self.config.programs[14], _rt=120)
            self.config.zone_programs[19] = ZoneProgram(zone_obj=self.config.zones[19],
                                                        prog_obj=self.config.programs[15], _rt=60)
            self.config.zone_programs[96] = ZoneProgram(zone_obj=self.config.zones[96],
                                                        prog_obj=self.config.programs[16], _rt=120)
            self.config.zone_programs[97] = ZoneProgram(zone_obj=self.config.zones[97],
                                                        prog_obj=self.config.programs[17], _rt=60)
            self.config.zone_programs[98] = ZoneProgram(zone_obj=self.config.zones[98],
                                                        prog_obj=self.config.programs[18], _rt=120)
            self.config.zone_programs[99] = ZoneProgram(zone_obj=self.config.zones[99],
                                                        prog_obj=self.config.programs[19], _rt=60)
            self.config.zone_programs[100] = ZoneProgram(zone_obj=self.config.zones[100],
                                                         prog_obj=self.config.programs[20], _rt=120)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_6(self):
        """
        turn on sim mode in the controller \n
        stop the clock so that it can be incremented manually \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        because of how the 1000 is set up you either have to wait a few minutes or change the dial position
        before you can start a program, after doing a search \n
        verify that none of the zones start early by checking 1 minute before the first start time \n
        zone concurrency varies between programs odd numbered programs have a zone concurrency of 1 meaning those zones
        should run 1 time before stopping \n
        even numbered programs have a zone concurrency of 2 meaning those zones should run twice back to back before
        stopping \n
        verify odd programs run 1 time before stopping and even programs run 2 times before stopping \n
        due to a lack of memory on the 1000 only 10 programs can run at one time \n
        set clock to 06/18/2012 12:44 this should start the zones watering \n
        \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 - 20 done watering \n
            - Zone status: \n
                - zone 1 done watering \n
                - zone 2 done watering \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n
                - zone 6 done watering  \n
                - zone 7 done watering \n
                - zone 8 done watering  \n
                - zone 9 done watering \n
                - zone 10 done watering  \n
                - zone 11 done watering  \n
                - zone 12 done watering  \n
                - zone 17 done watering  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering  \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 done watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for program in range(1, 21):
                self.config.programs[program].get_data()
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/18/2012', _time='23:44:00')
            self.config.controllers[1].verify_date_and_time()
            self.config.controllers[1].set_controller_to_run()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_7(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:45 this should start the zones watering \n
        get data from programs so that it can be verified against the object
        get data from zones that are addressed from the json file so that it can be verified against the object
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n
                - program 3 waiting to water \n
                - program 4 waiting to water \n
                - program 5 waiting to water \n
                - program 6 waiting to water \n
                - program 7 waiting to water \n
                - program 8 waiting to water \n
                - program 9 waiting to water \n
                - program 10 waiting to water \n
                - program 11 waiting to water \n
                - program 12 waiting to water \n
                - program 13 waiting to water \n
                - program 14 waiting to water \n
                - program 15 waiting to water \n
                - program 16 waiting to water \n
                - program 17 waiting to water \n
                - program 18 waiting to water \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 watering \n
                - zone 2 watering \n
                - zone 3 waiting to water   \n
                - zone 4 waiting to water   \n
                - zone 5 waiting to water   \n
                - zone 6 waiting to water   \n
                - zone 7 waiting to water  \n
                - zone 8 waiting to water  \n
                - zone 9 waiting to water  \n
                - zone 10 waiting to water   \n
                - zone 11 done watering  \n
                - zone 12 done watering  \n
                - zone 17 done watering  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering  \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 done watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 3):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for program in range(3, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[9].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[10].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[11].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[12].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[17].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[18].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_8(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 12:46 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 running \n
                - program 3 running  \n
                - program 4 waiting to water \n
                - program 5 waiting to water \n
                - program 6 waiting to water  \n
                - program 7 waiting to water  \n
                - program 8 waiting to water  \n
                - program 9 waiting to water  \n
                - program 10 waiting to water  \n
                - program 11 waiting to water \n
                - program 12 waiting to water  \n
                - program 13 waiting to water  \n
                - program 14 waiting to water  \n
                - program 15 waiting to water \n
                - program 16 waiting to water  \n
                - program 17 waiting to water  \n
                - program 18 waiting to water  \n
                - program 19 waiting to water  \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering \n
                - zone 2 watering \n
                - zone 3 watering  \n
                - zone 4 waiting to water   \n
                - zone 5 waiting to water   \n
                - zone 6 waiting to water   \n
                - zone 7 waiting to water  \n
                - zone 8 waiting to water  \n
                - zone 9 waiting to water  \n
                - zone 10 waiting to water   \n
                - zone 11 done watering  \n
                - zone 12 done watering  \n
                - zone 17 done watering  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering  \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 done watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.running)
            self.config.programs[3].verify_status_on_cn(opcodes.running)
            for program in range(4, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[9].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[10].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[11].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[12].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[17].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[18].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_9(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:48 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 running \n
                - program 3 running  \n
                - program 4 wating to water \n
                - program 5 waiting t water \n
                - program 6 waiting to water  \n
                - program 7 waiting to water  \n
                - program 8 waiting to water  \n
                - program 9 waiting to water  \n
                - program 10 waiting to water \n
                - program 11 waiting to water \n
                - program 12 waiting to water \n
                - program 13 waiting to water \n
                - program 14 waiting to water \n
                - program 15 waiting to water \n
                - program 16 waiting to water \n
                - program 17 waiting to water \n
                - program 18 waiting to water \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering \n
                - zone 2 watering \n
                - zone 3 done watering  \n
                - zone 4 watering   \n
                - zone 5 waiting to water   \n
                - zone 6 waiting to water   \n
                - zone 7 waiting to water  \n
                - zone 8 waiting to water  \n
                - zone 9 waiting to water  \n
                - zone 10 waiting to water   \n
                - zone 11 waiting to water  \n
                - zone 12 waiting to water  \n
                - zone 17 waiting to water \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering  \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 done watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.running)
            self.config.programs[5].verify_status_on_cn(opcodes.running)
            for program in range(6, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[9].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[10].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[11].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[12].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[17].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[18].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_10(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:49 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 done watering \n
                - program 3 done watering  \n
                - program 4 running \n
                - program 5 done watering \n
                - program 6 running  \n
                - program 7 waiting to water  \n
                - program 8 waiting to water  \n
                - program 9 waiting to water  \n
                - program 10 waiting to water  \n
                - program 11 waiting to water \n
                - program 12 waiting to water \n
                - program 13 waiting to water \n
                - program 14 waiting to water \n
                - program 15 waiting to water \n
                - program 16 waiting to water \n
                - program 17 waiting to water \n
                - program 18 waiting to water \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering \n
                - zone 2 done watering \n
                - zone 3 done watering  \n
                - zone 4 watering   \n
                - zone 5 done watering   \n
                - zone 6 watering   \n
                - zone 7 waiting to water  \n
                - zone 8 waiting to water  \n
                - zone 9 waiting to water  \n
                - zone 10 waiting to water   \n
                - zone 11 waiting to water  \n
                - zone 12 waiting to water  \n
                - zone 17 waiting to water  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering  \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 done watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 4):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.running)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.running)
            for program in range(7, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[9].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[10].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[11].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[12].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[17].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[18].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[19].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_11(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:50 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 done watering \n
                - program 3 done watering  \n
                - program 4 done watering \n
                - program 5 done watering \n
                - program 6 running  \n
                - program 7 running  \n
                - program 8 waiting to water  \n
                - program 9 waiting to water  \n
                - program 10 waiting to water  \n
                - program 11 waiting to water \n
                - program 12 waiting to water \n
                - program 13 waiting to water \n
                - program 14 waiting to water \n
                - program 15 waiting to water \n
                - program 16 waiting to water \n
                - program 17 waiting to water \n
                - program 18 waiting to water \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering \n
                - zone 2 done watering \n
                - zone 3 done watering  \n
                - zone 4 done watering   \n
                - zone 5 done watering   \n
                - zone 6 watering   \n
                - zone 7 watering  \n
                - zone 8 waiting to water  \n
                - zone 9 waiting to water  \n
                - zone 10 waiting to water   \n
                - zone 11 waiting to water  \n
                - zone 12 waiting to water  \n
                - zone 17 waiting to water  \n
                - zone 18 waiting to water  \n
                - zone 19 waiting to water  \n
                - zone 96 done watering  \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 done watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 6):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for program in range(6, 8):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for program in range(8, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[9].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[10].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[11].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[12].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[17].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[18].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[19].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[96].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_12(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:51 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 done watering \n
                - program 3 done watering  \n
                - program 4 done watering \n
                - program 5 done watering \n
                - program 6 done watering  \n
                - program 7 done watering  \n
                - program 8 running \n
                - program 9 done watering \n
                - program 10 running \n
                - program 11 waiting to water \n
                - program 12 waiting to water \n
                - program 13 waiting to water \n
                - program 14 waiting to water \n
                - program 15 waiting to water \n
                - program 16 waiting to water \n
                - program 17 waiting to water \n
                - program 18 waiting to water \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering  \n
                - zone 2 done watering  \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n
                - zone 6 done watering  \n
                - zone 7 done watering  \n
                - zone 8 watering  \n
                - zone 9 watering  \n
                - zone 10 waiting to water  \n
                - zone 11 waiting to water  \n
                - zone 12 waiting to water  \n
                - zone 17 waiting to water  \n
                - zone 18 waiting to water  \n
                - zone 19 waiting to water  \n
                - zone 96 waiting to water  \n
                - zone 97 waiting to water  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 done watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 8):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[8].verify_status_on_cn(opcodes.running)
            self.config.programs[9].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[10].verify_status_on_cn(opcodes.running)
            for program in range(11, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[9].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[10].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[11].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[12].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[17].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[18].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[19].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[96].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[97].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[98].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[99].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_13(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:52 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 done watering \n
                - program 3 done watering \n
                - program 4 done watering \n
                - program 5 done watering \n
                - program 6 done watering \n
                - program 7 done watering \n
                - program 8 done watering \n
                - program 9 done watering \n
                - program 10 running \n
                - program 11 running \n
                - program 12 waiting to water \n
                - program 13 waiting to water \n
                - program 14 waiting to water \n
                - program 15 waiting to water \n
                - program 16 waiting to water \n
                - program 17 waiting to water \n
                - program 18 waiting to water \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering  \n
                - zone 2 done watering  \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n
                - zone 6 done watering  \n
                - zone 7 done watering  \n
                - zone 8 done watering  \n
                - zone 9 done watering  \n
                - zone 10 watering \n
                - zone 11 watering \n
                - zone 12 waiting to water  \n
                - zone 17 waiting to water  \n
                - zone 18 waiting to water  \n
                - zone 19 waiting to water  \n
                - zone 96 waiting to water  \n
                - zone 97 waiting to water  \n
                - zone 98 waiting to water  \n
                - zone 99 waiting to water  \n
                - zone 100 done watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 10):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for program in range(10, 12):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for program in range(12, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[9].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[10].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[11].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[12].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[17].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[18].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[19].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[96].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[97].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[98].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[99].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[100].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_14(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:53 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 done watering \n
                - program 3 done watering \n
                - program 4 done watering \n
                - program 5 done watering \n
                - program 6 done watering \n
                - program 7 done watering \n
                - program 8 done watering \n
                - program 9 done watering \n
                - program 10 done watering \n
                - program 11 done watering \n
                - program 12 running \n
                - program 13 running \n
                - program 14 waiting to water \n
                - program 15 waiting to water \n
                - program 16 waiting to water \n
                - program 17 waiting to water \n
                - program 18 waiting to water \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering  \n
                - zone 2 done watering  \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n
                - zone 6 done watering  \n
                - zone 7 done watering  \n
                - zone 8 done watering  \n
                - zone 9 done watering  \n
                - zone 10 done watering \n
                - zone 11 done watering \n
                - zone 12 watering  \n
                - zone 17 watering  \n
                - zone 18 waiting to water  \n
                - zone 19 waiting to water  \n
                - zone 96 waiting to water  \n
                - zone 97 waiting to water  \n
                - zone 98 waiting to water  \n
                - zone 99 waiting to water  \n
                - zone 100 waiting to water  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 12):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for program in range(12, 14):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for program in range(14, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[9].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[10].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[11].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[12].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[17].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[18].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[19].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[96].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[97].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[98].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[99].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[100].verify_status_on_cn(status=opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_15(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:54 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 done watering \n
                - program 3 done watering \n
                - program 4 done watering \n
                - program 5 done watering \n
                - program 6 done watering \n
                - program 7 done watering \n
                - program 8 done watering \n
                - program 9 done watering \n
                - program 10 done watering \n
                - program 11 done watering \n
                - program 12 done watering \n
                - program 13 done watering \n
                - program 14 running \n
                - program 15 done watering \n
                - program 16 running \n
                - program 17 waiting to water \n
                - program 18 waiting to water \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering  \n
                - zone 2 done watering  \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n
                - zone 6 done watering  \n
                - zone 7 done watering  \n
                - zone 8 done watering  \n
                - zone 9 done watering  \n
                - zone 10 done watering \n
                - zone 11 done watering \n
                - zone 12 watering  \n
                - zone 17 done watering  \n
                - zone 18 watering  \n
                - zone 19 waiting to water  \n
                - zone 96 waiting to water  \n
                - zone 97 waiting to water  \n
                - zone 98 waiting to water  \n
                - zone 99 waiting to water  \n
                - zone 100 waiting to water  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 12):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[12].verify_status_on_cn(opcodes.running)
            self.config.programs[13].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[14].verify_status_on_cn(opcodes.running)
            for program in range(15, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[9].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[10].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[11].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[12].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[17].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[18].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[96].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[97].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[98].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[99].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[100].verify_status_on_cn(status=opcodes.waiting_to_water)

        except AssertionError, ae:
            raise AssertionError(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_16(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:55 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 done watering \n
                - program 3 done watering \n
                - program 4 done watering \n
                - program 5 done watering \n
                - program 6 done watering \n
                - program 7 done watering \n
                - program 8 done watering \n
                - program 9 done watering \n
                - program 10 done watering \n
                - program 11 done watering \n
                - program 12 done watering \n
                - program 13 done watering \n
                - program 14 running \n
                - program 15 running \n
                - program 16 waiting to water \n
                - program 17 waiting to water \n
                - program 18 waiting to water \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering  \n
                - zone 2 done watering  \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n
                - zone 6 done watering  \n
                - zone 7 done watering  \n
                - zone 8 done watering  \n
                - zone 9 done watering  \n
                - zone 10 done watering \n
                - zone 11 done watering \n
                - zone 12 done watering  \n
                - zone 17 done watering  \n
                - zone 18 watering  \n
                - zone 19 watering  \n
                - zone 96 waiting to water  \n
                - zone 97 waiting to water  \n
                - zone 98 waiting to water  \n
                - zone 99 waiting to water  \n
                - zone 100 waiting to water  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 14):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for program in range(14, 16):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for program in range(16, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[9].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[10].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[11].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[12].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[17].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[18].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[97].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[98].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[99].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[100].verify_status_on_cn(status=opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_17(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:56 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 done watering \n
                - program 3 done watering \n
                - program 4 done watering \n
                - program 5 done watering \n
                - program 6 done watering \n
                - program 7 done watering \n
                - program 8 done watering \n
                - program 9 done watering \n
                - program 10 done watering \n
                - program 11 done watering \n
                - program 12 done watering \n
                - program 13 done watering \n
                - program 14 done watering \n
                - program 15 done watering \n
                - program 16 running \n
                - program 17 running \n
                - program 18 waiting to water \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering  \n
                - zone 2 done watering  \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n
                - zone 6 done watering  \n
                - zone 7 done watering  \n
                - zone 8 done watering  \n
                - zone 9 done watering  \n
                - zone 10 done watering \n
                - zone 11 done watering \n
                - zone 12 done watering  \n
                - zone 17 done watering  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 watering  \n
                - zone 97 watering  \n
                - zone 98 waiting to water  \n
                - zone 99 waiting to water  \n
                - zone 100 waiting to water  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 16):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for program in range(16, 18):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for program in range(18, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[9].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[10].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[11].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[12].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[17].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[18].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[99].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[100].verify_status_on_cn(status=opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_18(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:57 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 done watering \n
                - program 3 done watering \n
                - program 4 done watering \n
                - program 5 done watering \n
                - program 6 done watering \n
                - program 7 done watering \n
                - program 8 done watering \n
                - program 9 done watering \n
                - program 10 done watering \n
                - program 11 done watering \n
                - program 12 done watering \n
                - program 13 done watering \n
                - program 14 done watering \n
                - program 15 done watering \n
                - program 16 running \n
                - program 17 done watering \n
                - program 18 running \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering  \n
                - zone 2 done watering  \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n
                - zone 6 done watering  \n
                - zone 7 done watering  \n
                - zone 8 done watering  \n
                - zone 9 done watering  \n
                - zone 10 done watering \n
                - zone 11 done watering \n
                - zone 12 done watering  \n
                - zone 17 done watering  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 watering  \n
                - zone 97 done watering  \n
                - zone 98 watering  \n
                - zone 99 waiting to water  \n
                - zone 100 waiting to water  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 16):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[16].verify_status_on_cn(opcodes.running)
            self.config.programs[17].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[18].verify_status_on_cn(opcodes.running)
            for program in range(19, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[9].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[10].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[11].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[12].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[17].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[18].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[100].verify_status_on_cn(status=opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_19(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:58 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 done watering \n
                - program 3 done watering \n
                - program 4 done watering \n
                - program 5 done watering \n
                - program 6 done watering \n
                - program 7 done watering \n
                - program 8 done watering \n
                - program 9 done watering \n
                - program 10 done watering \n
                - program 11 done watering \n
                - program 12 done watering \n
                - program 13 done watering \n
                - program 14 done watering \n
                - program 15 done watering \n
                - program 16 done watering \n
                - program 17 done watering \n
                - program 18 running \n
                - program 19 running \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering  \n
                - zone 2 done watering  \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n
                - zone 6 done watering  \n
                - zone 7 done watering  \n
                - zone 8 done watering  \n
                - zone 9 done watering  \n
                - zone 10 done watering \n
                - zone 11 done watering \n
                - zone 12 done watering  \n
                - zone 17 done watering  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering  \n
                - zone 97 done watering  \n
                - zone 98 watering  \n
                - zone 99 watering  \n
                - zone 100 waiting to water  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 18):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for program in range(18, 20):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            self.config.programs[20].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[9].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[10].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[11].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[12].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[17].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[18].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_20(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:59 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 done watering \n
                - program 3 done watering \n
                - program 4 done watering \n
                - program 5 done watering \n
                - program 6 done watering \n
                - program 7 done watering \n
                - program 8 done watering \n
                - program 9 done watering \n
                - program 10 done watering \n
                - program 11 done watering \n
                - program 12 done watering \n
                - program 13 done watering \n
                - program 14 done watering \n
                - program 15 done watering \n
                - program 16 done watering \n
                - program 17 done watering \n
                - program 18 done watering \n
                - program 19 done watering \n
                - program 20 running \n
            - Zone status: \n
                - zone 1 done watering  \n
                - zone 2 done watering  \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n
                - zone 6 done watering  \n
                - zone 7 done watering  \n
                - zone 8 done watering  \n
                - zone 9 done watering  \n
                - zone 10 done watering \n
                - zone 11 done watering \n
                - zone 12 done watering  \n
                - zone 17 done watering  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 20):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[20].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[9].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[10].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[11].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[12].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[17].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[18].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_21(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 12:00 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 waiting_to_water \n
                - program 3 waiting_to_water\n
                - program 4 waiting_to_water\n
                - program 5 waiting_to_water\n
                - program 6 waiting_to_water \n
                - program 7 waiting_to_water \n
                - program 8 waiting_to_water \n
                - program 9 done watering \n
                - program 10 done watering \n
                - program 11 done watering \n
                - program 12 done watering \n
                - program 13 done watering \n
                - program 14 done watering \n
                - program 15 done watering \n
                - program 16 done watering \n
                - program 17 done watering \n
                - program 18 done watering \n
                - program 19 done watering \n
                - program 20 running \n
            - Zone status: \n
                - zone 1 watering  \n
                - zone 2 waiting_to_water  \n
                - zone 3 waiting_to_water  \n
                - zone 4 waiting_to_water  \n
                - zone 5 waiting_to_water  \n
                - zone 6 waiting_to_water \n
                - zone 7 waiting_to_water \n
                - zone 8 waiting_to_water  \n
                - zone 9 waiting_to_water \n
                - zone 10 done watering \n
                - zone 11 done watering \n
                - zone 12 done watering  \n
                - zone 17 done watering  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 watering\n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            for program in range(2, 20):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[20].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[9].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[10].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[11].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[12].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[17].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[18].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_22(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 12:01 this should start the zones watering \n
        get data from programs so that it can be verified against the object
        get data from zones that are addressed from the json file so that it can be verified against the object
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n
                - program 3 waiting to water \n
                - program 4 waiting to water \n
                - program 5 waiting to water \n
                - program 6 waiting to water \n
                - program 7 waiting to water \n
                - program 8 waiting to water \n
                - program 9 waiting to water \n
                - program 10 waiting to water \n
                - program 11 waiting to water \n
                - program 12 waiting to water \n
                - program 13 waiting to water \n
                - program 14 waiting to water \n
                - program 15 waiting to water \n
                - program 16 waiting to water \n
                - program 17 waiting to water \n
                - program 18 waiting to water \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 watering \n
                - zone 2 watering \n
                - zone 3 waiting to water   \n
                - zone 4 waiting to water   \n
                - zone 5 waiting to water   \n
                - zone 6 waiting to water   \n
                - zone 7 waiting to water  \n
                - zone 8 waiting to water  \n
                - zone 9 waiting to water  \n
                - zone 10 waiting to water   \n
                - zone 11 done watering  \n
                - zone 12 done watering  \n
                - zone 17 done watering  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering  \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 done watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.running)
            self.config.programs[3].verify_status_on_cn(opcodes.running)
            for program in range(4, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[9].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[10].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[11].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[12].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[17].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[18].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_23(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 12:02 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 running \n
                - program 3 running  \n
                - program 4 waiting to water \n
                - program 5 waiting to water \n
                - program 6 waiting to water  \n
                - program 7 waiting to water  \n
                - program 8 waiting to water  \n
                - program 9 waiting to water  \n
                - program 10 waiting to water  \n
                - program 11 waiting to water \n
                - program 12 waiting to water  \n
                - program 13 waiting to water  \n
                - program 14 waiting to water  \n
                - program 15 waiting to water \n
                - program 16 waiting to water  \n
                - program 17 waiting to water  \n
                - program 18 waiting to water  \n
                - program 19 waiting to water  \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering \n
                - zone 2 watering \n
                - zone 3 watering  \n
                - zone 4 waiting to water   \n
                - zone 5 waiting to water   \n
                - zone 6 waiting to water   \n
                - zone 7 waiting to water  \n
                - zone 8 waiting to water  \n
                - zone 9 waiting to water  \n
                - zone 10 waiting to water   \n
                - zone 11 done watering  \n
                - zone 12 done watering  \n
                - zone 17 done watering  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering  \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 done watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.running)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.running)
            for program in range(5, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[9].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[10].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[11].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[12].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[17].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[18].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_24(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 12:03 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 running \n
                - program 3 done watering  \n
                - program 4 running \n
                - program 5 waiting t water \n
                - program 6 waiting to water  \n
                - program 7 waiting to water  \n
                - program 8 waiting to water  \n
                - program 9 waiting to water  \n
                - program 10 waiting to water \n
                - program 11 waiting to water \n
                - program 12 waiting to water \n
                - program 13 waiting to water \n
                - program 14 waiting to water \n
                - program 15 waiting to water \n
                - program 16 waiting to water \n
                - program 17 waiting to water \n
                - program 18 waiting to water \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering \n
                - zone 2 watering \n
                - zone 3 done watering  \n
                - zone 4 watering   \n
                - zone 5 waiting to water   \n
                - zone 6 waiting to water   \n
                - zone 7 waiting to water  \n
                - zone 8 waiting to water  \n
                - zone 9 waiting to water  \n
                - zone 10 waiting to water   \n
                - zone 11 waiting to water  \n
                - zone 12 waiting to water  \n
                - zone 17 waiting to water \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering  \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 done watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.running)
            self.config.programs[5].verify_status_on_cn(opcodes.running)
            for program in range(6, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[9].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[10].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[11].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[12].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[17].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[18].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_25(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 12:04 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering \n
                - program 2 done watering \n
                - program 3 done watering  \n
                - program 4 done watering \n
                - program 5 done watering \n
                - program 6 running  \n
                - program 7 running \n
                - program 8 waiting to water  \n
                - program 9 waiting to water  \n
                - program 10 waiting to water  \n
                - program 11 waiting to water \n
                - program 12 waiting to water \n
                - program 13 waiting to water \n
                - program 14 waiting to water \n
                - program 15 waiting to water \n
                - program 16 waiting to water \n
                - program 17 waiting to water \n
                - program 18 waiting to water \n
                - program 19 waiting to water \n
                - program 20 waiting to water \n
            - Zone status: \n
                - zone 1 done watering \n
                - zone 2 done watering \n
                - zone 3 done watering  \n
                - zone 4 done Watering   \n
                - zone 5 done watering   \n
                - zone 6 watering   \n
                - zone 7 watering  \n
                - zone 8 waiting to water  \n
                - zone 9 waiting to water  \n
                - zone 10 waiting to water   \n
                - zone 11 waiting to water  \n
                - zone 12 waiting to water  \n
                - zone 17 waiting to water  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering  \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 done watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 6):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.running)
            self.config.programs[7].verify_status_on_cn(opcodes.running)
            for program in range(8, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[9].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[10].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[11].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[12].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[17].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[18].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[19].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[96].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_26(self):
        """
        advance the clock 12 minutes to make the time 06/18/2012 12:16 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1- 20 done watering \n
              - Zone status: \n
                - zone 1 done watering  \n
                - zone 2 done watering  \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n
                - zone 6 done watering  \n
                - zone 7 done watering  \n
                - zone 8 done watering  \n
                - zone 9 done watering  \n
                - zone 10 done watering \n
                - zone 11 done watering \n
                - zone 12 done watering  \n
                - zone 17 done watering  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering \n
                - zone 100 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=12)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 21):
                self.config.programs[program].get_data()
            for program in range(1, 21):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[9].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[10].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[11].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[12].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[17].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[18].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[19].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[96].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[97].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[98].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[99].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[100].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)