import sys

import time
from time import sleep

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_1000 import POC1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zn import Zone
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ms import MoistureSensor
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.mv import MasterValve

from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_1000 import StartConditionFor1000, PauseConditionFor1000, StopConditionFor1000

from old_32_10_sb_objects_dec_29_2017.common import helper_methods

__author__ = "baseline"


class ControllerUseCase11(object):
    """
    Test name: \n
        Replacing Devices Test \n
    Purpose: \n
        The purpose of this test is to make sure that we can replace devices in a specific address and everything will
        still function as expected \n
    Coverage area: \n
        1. Set up the controller \n
        2. Search and assign: \n
            - Zones \n
            - Master Valves \n
            - Moisture Sensors \n
            - Temperature Sensors \n
            - Event Switch \n
            - Flow Meters \n
        3. Give the zones a design flow \n
        4. Set up programs and give them a start times \n
        5. Assign zones to programs and give them run times \n
        6. Set a start condition for each moisture sensor and then set their moisture percent to hit the lower limit \n
        7. Set a stop/pause condition for each temp sensor and then set their temperature to hit the thresholds \n
        8. Set a pause condition for an event switch to pause program 39 when contacts are open \n
        9. Assign master valves to programs 1 and 3, and POC 1 and 3, and set the 'normally open state' \n
        10. Set flow meter values \n
        11. Set up the POC objects and assign master valves to them \n
        12. Reboot the controller and then set it to sim mode again, and turn on the faux io \n
        13. Reset the moisture/temperature sensor and flow meter values \n
        14. Perform a self test on all the devices to make sure the default values from the controller are stored
            in the objects \n
        15. Verify the full configuration \n
        16. Load the devices that are going to be used as replacements \n
        17. Initialize the new zones and replace the zones at address' 1, 2, 99, and 100 \n
        18. Replace soil moisture sensor at address 2 with sensor SB07263 \n
        19. Replace the master valve at address 7 with TSQ0084 \n
        20. Perform a test on all devices to make sure the new devices are working properly \n
        21. Verify the full configuration with the new devices \n
    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    self.step_18()
                    self.step_19()
                    self.step_20()
                    self.step_21()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        """

        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # only need this for BaseManager
            # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock
            # for the reboot process.
            # self.config.controllers[1].set_sim_mode_to_off()
            self.config.basemanager_connection[1].verify_ip_address_state()
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller failed: " + ae.message)

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # load all devices need into the controller so that they are available for use in the configuration
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(
                ms_object_dict=self.config.moisture_sensors,
                ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(
                ts_object_dict=self.config.temperature_sensors,
                ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_1000_poc_objects()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_3(self):
        """
        Over write zone values: \n
        set up the zones \n
            - Change the design flow for each zone \n
            - disable zone 98
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zones[1].set_design_flow_on_cn(_df=10)
            self.config.zones[2].set_design_flow_on_cn(_df=10)
            self.config.zones[49].set_design_flow_on_cn(_df=30)
            self.config.zones[50].set_design_flow_on_cn(_df=30)
            self.config.zones[97].set_design_flow_on_cn(_df=80)
            self.config.zones[98].set_design_flow_on_cn(_df=80)
            self.config.zones[99].set_design_flow_on_cn(_df=80)
            self.config.zones[100].set_design_flow_on_cn(_df=100)
            self.config.zones[98].set_enable_state_on_cn(_state='FA')
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_4(self):
        """
        This is set in the PG1000 object
        set_up_programs
            Program 1:
                - address 1
                - water window is closed form 8:00 am to 8:00 pm
                - Max concurrent Zones 4
                - Seasonal adjust  set to 100
                - Point of Connection set to 1
                - Soak cycle Mode program
                - Cycle count set to 4
                - Soak time set to 5 minutes
            Program 3:
                - address 3
                - water window is open all day
                - Max concurrent Zones 6
                - Seasonal adjust  set to 150
                - Point of Connection set to 1 and 2
                - Master Valve set to 2 which is it address
                - Soak cycle Mode  Smart program soak cycles
                - Cycle count set to 6
                - Soak time set to 10 minutes
            Program 39:
                - address 39
                - water window is open all day
                - Max concurrent Zones 6
                - Seasonal adjust  set to 150
                - Point of Connection set to 3
                - Soak cycle Mode zone
                - Cycle count set to 8
                - Soak time set to 20 minutes

        Set the start time on all programs to 8:00 A.M., 8:59 A.M., 10:00 A.M., 11:00 A.M.
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        daily_water_window = ['011111100001111111111110']
        weekly_water_windows = ['011111100001111111111110', '011111100001111111111110', '011111100001111111111110',
                                '011111100001111111111110', '011111100001111111111110', '011111100001111111111110',
                                '011111100001111111111110']
        mon_wed_fri_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
        semi_monthly = [0, 0, 0, 0, 0, 0, 8, 8, 6, 5, 5, 4, 3, 3, 3, 3, 4, 5, 6, 7, 0, 0, 0, 0]
        try:
            self.config.programs[1] = PG1000(_address=1,
                                             _enabled_state=opcodes.true,
                                             _water_window=daily_water_window,
                                             _max_concurrent_zones=4,
                                             _seasonal_adjust=100,
                                             _point_of_connection_address=[1],
                                             _soak_cycle_mode=opcodes.program_cycles,
                                             _cycle_count=4,
                                             _soak_time=300
                                             )

            self.config.programs[3] = PG1000(_address=3,
                                             _enabled_state=opcodes.true,
                                             _water_window=daily_water_window,
                                             _max_concurrent_zones=4,
                                             _seasonal_adjust=50,
                                             _point_of_connection_address=[1, 2],
                                             _master_valve_address=2,
                                             _soak_cycle_mode=opcodes.intelligent_soak,
                                             _cycle_count=6,
                                             _soak_time=600
                                             )
            self.config.programs[39] = PG1000(_address=39,
                                              _enabled_state=opcodes.true,
                                              _water_window=weekly_water_windows,
                                              _max_concurrent_zones=4,
                                              _seasonal_adjust=150,
                                              _point_of_connection_address=[3],
                                              _soak_cycle_mode=opcodes.zone_soak_cycles,
                                              _cycle_count=8,
                                              _soak_time=1200
                                              )

            self.config.program_start_conditions[1] = StartConditionFor1000(program_ad=1)
            self.config.program_start_conditions[2] = StartConditionFor1000(program_ad=3)
            self.config.program_start_conditions[3] = StartConditionFor1000(program_ad=39)
            self.config.program_start_conditions[1].set_day_time_start(
                _dt='TR', _st_list=[480, 540, 600, 660], _interval_type=opcodes.week_days, _interval_args=mon_wed_fri_watering_days)

            self.config.program_start_conditions[2].set_day_time_start(
                _dt='TR', _st_list=[480, 540, 600, 660], _interval_type=opcodes.calendar_interval, _interval_args=opcodes.odd_day)

            self.config.program_start_conditions[3].set_day_time_start(
                _dt='TR', _st_list=[480, 540, 600, 660], _interval_type=opcodes.semi_month_interval, _interval_args=semi_monthly)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_5(self):
        """
        zone programs
            - assign zones to a program
                - set zone 1 and 2
                    - program 1
                    - run time to 15 minutes
                - set zone 49 and 50
                    - program 3
                    - run time to 20 minutes
                - set zone 97, 98, 99, and 100
                    - program 39
                    - run time to 33 minutes
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900)
            self.config.zone_programs[49] = ZoneProgram(zone_obj=self.config.zones[49],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=1200)
            self.config.zone_programs[50] = ZoneProgram(zone_obj=self.config.zones[50],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=1200)
            self.config.zone_programs[97] = ZoneProgram(zone_obj=self.config.zones[97],
                                                        prog_obj=self.config.programs[39],
                                                        _rt=1980)
            self.config.zone_programs[98] = ZoneProgram(zone_obj=self.config.zones[98],
                                                        prog_obj=self.config.programs[39],
                                                        _rt=1980)
            self.config.zone_programs[99] = ZoneProgram(zone_obj=self.config.zones[99],
                                                        prog_obj=self.config.programs[39],
                                                        _rt=1980)
            self.config.zone_programs[100] = ZoneProgram(zone_obj=self.config.zones[100],
                                                         prog_obj=self.config.programs[39],
                                                         _rt=1980)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_6(self):
        """
        Set up two separate moisture conditions \n
            - Set the moisture percents on both moisture sensors to be below their programs lower limit \n
            - Self test the moisture sensor to update their values \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.program_start_conditions[4] = StartConditionFor1000(program_ad=1)
            self.config.program_start_conditions[5] = StartConditionFor1000(program_ad=39)
            self.config.program_start_conditions[4].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[2].sn, mode=opcodes.lower_limit, threshold=24)
            self.config.program_start_conditions[5].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.lower_limit, threshold=38)

            self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=26.0)
            self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=10.0)
            self.config.moisture_sensors[1].set_two_wire_drop_value_on_cn(1.8)
            self.config.moisture_sensors[2].set_two_wire_drop_value_on_cn(1.6)
            self.config.moisture_sensors[1].do_self_test()
            self.config.moisture_sensors[2].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_7(self):
        """
        Set up temperature conditions \n
            - Set temperature sensor TAT0001 to pause program 1 when the sensor reads below 32 degrees \n
            - Set temperature sensor TAT0002 to stop program 3 when the sensor reads below 60 degrees \n
            - Set the temperature reading of both sensors to 26 \n
            - Set the two wire drop value of TAT0001 and TAT0002 to 1.7 and 1.8, respectively \n
        Do a self test to update the values on both temperature sensors \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.program_pause_conditions[1] = PauseConditionFor1000(program_ad=1)
            self.config.program_stop_conditions[1] = StopConditionFor1000(program_ad=3)
            self.config.program_pause_conditions[1].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.lower_limit, threshold=32.0)
            self.config.program_stop_conditions[1].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[2].sn, mode=opcodes.lower_limit, threshold=60.0)

            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=26.0)
            self.config.temperature_sensors[2].set_temperature_reading_on_cn(_temp=26.0)
            self.config.temperature_sensors[1].set_two_wire_drop_value_on_cn(_value=1.7)
            self.config.temperature_sensors[2].set_two_wire_drop_value_on_cn(_value=1.8)
            self.config.temperature_sensors[1].do_self_test()
            self.config.temperature_sensors[2].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_8(self):
        """
        Set up event switch conditions \n
            - Set event switch TPD0001 to pause program 39 when the contracts open \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.program_pause_conditions[2] = PauseConditionFor1000(program_ad=39)
            self.config.program_pause_conditions[2].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.open)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_9(self):
        """
        set up master valve assignments \n
            - Assign master valves to programs 1 and 3 \n
            - Assign master valves to POCs 1 and 3 \n
            - Set the 'normally open state' on each master valve \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.programs[1].set_master_valve_on_cn(_mv_address=self.config.master_valves[1].ad)
            self.config.programs[3].set_master_valve_on_cn(_mv_address=self.config.master_valves[1].ad)
            self.config.poc[1].set_master_valve_on_cn(_master_valve_address=self.config.master_valves[2].ad)
            self.config.poc[3].set_master_valve_on_cn(_master_valve_address=self.config.master_valves[7].ad)
            self.config.master_valves[1].set_normally_open_state_on_cn(_normally_open=opcodes.false)
            self.config.master_valves[2].set_normally_open_state_on_cn(_normally_open=opcodes.false)
            self.config.master_valves[7].set_normally_open_state_on_cn(_normally_open=opcodes.true)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_10(self):
        """
        Set the enable state on both flow meters to be true \n
            - Set the k values for both flow meters \n
            - Set the flow rate for both flow meters \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.flow_meters[1].set_enable_state_on_cn(_state=opcodes.true)
            self.config.flow_meters[2].set_enable_state_on_cn(_state=opcodes.true)
            self.config.flow_meters[1].set_k_value_on_cn(_k_value=3.10)
            self.config.flow_meters[2].set_k_value_on_cn(_k_value=5.01)
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=25)
            self.config.flow_meters[2].set_flow_rate_on_cn(_flow_rate=50)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_11(self):
        """
        set up water source 1 \n
            enable water source 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to water source 1 \n
            set water source 1 a target flow of 500 GPM \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            enable limit concurrent to target \n
            set pipe fill time to 5 minutes \n
            set flow variance to 42% and enable flow variance \n
        \n
        set up water source 3 \n
            enable water source 3 \n
            assign master valve TMV0004 and flow meter TWF0004 to water source 1 \n
            set water source 3 a target flow of 50 \n
            set high flow limit to 75 and disable high flow shut down \n
            set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
            disable limit concurrent to target \n
            set pipe fill time to 2 minutes \n
            disable flow variance
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1] = POC1000(
                     _address=1,
                     _flow_meter_address=1,
                     _master_valve_address=2,
                     _enabled_state=opcodes.true,
                     _target_flow=500.0,
                     _high_flow_limit=550.0,
                     _shutdown_on_high_flow=opcodes.true,
                     _unscheduled_flow_limit=10.0,
                     _shutdown_on_unscheduled=opcodes.true,
                     _limit_concurrent_zones_to_target=opcodes.true,
                     _fill_time=5,
                     _flow_variance_percent=42,
                     _flow_variance_enable=opcodes.true)

            self.config.poc[3] = POC1000(
                     _address=3,
                     _flow_meter_address=2,
                     _master_valve_address=7,
                     _enabled_state=opcodes.true,
                     _target_flow=50.0,
                     _high_flow_limit=75.0,
                     _shutdown_on_high_flow=opcodes.false,
                     _unscheduled_flow_limit=5.0,
                     _shutdown_on_unscheduled=opcodes.false,
                     _limit_concurrent_zones_to_target=opcodes.false,
                     _fill_time=2,
                     _flow_variance_enable=opcodes.false)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_12(self):
        """
        Increment the clock by two minutes \n
        Reboot the controller. Set 10 seconds of sleep to allow it to reboot \n
        Turn on sim mode, faux io, stop the clock, and reset the date time \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=2)
            self.config.controllers[1].do_reboot_controller()
            time.sleep(10)
            self.config.controllers[1].set_date_and_time_on_cn(_date='01/01/2014', _time='01:00:00')
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_13(self):
        """
        Set both moisture sensor values so the programs can start watering and not be effected \n
        Set both temperature sensor values so that the programs can start watering and not be effected \n
        Set flow meter readings \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=26.0)
            self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=10.0)
            self.config.moisture_sensors[1].set_two_wire_drop_value_on_cn(_value=1.8)
            self.config.moisture_sensors[2].set_two_wire_drop_value_on_cn(_value=1.6)
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=26.0)
            self.config.temperature_sensors[2].set_temperature_reading_on_cn(_temp=26.0)
            self.config.temperature_sensors[1].set_two_wire_drop_value_on_cn(_value=1.7)
            self.config.temperature_sensors[2].set_two_wire_drop_value_on_cn(_value=1.8)
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=25)
            self.config.flow_meters[2].set_flow_rate_on_cn(_flow_rate=50)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_14(self):
        """
        Perform a test on all devices to verify that they are functioning properly \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].self_test_and_update_object_attributes()
            for moisture_sensors in self.config.ms_ad_range:
                self.config.moisture_sensors[moisture_sensors].self_test_and_update_object_attributes()
            for master_valves in self.config.mv_ad_range:
                self.config.master_valves[master_valves].self_test_and_update_object_attributes()
            for flow_meters in self.config.fm_ad_range:
                self.config.flow_meters[flow_meters].self_test_and_update_object_attributes()
            for event_switches in self.config.sw_ad_range:
                self.config.event_switches[event_switches].self_test_and_update_object_attributes()
            for temperature_sensors in self.config.ts_ad_range:
                self.config.temperature_sensors[temperature_sensors].self_test_and_update_object_attributes()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_15(self):
        """
        verify the full configuration of the controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            self.config.controllers[1].set_controller_to_run()
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_16(self):
        """
        Load replacement devices these are not located in the json file so they must be a str \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            self.config.controllers[1].load_dv_to_cn(dv_type=opcodes.four_valve_decoder,
                                                     list_of_decoder_serial_nums=["TSQ0031"])
            self.config.controllers[1].load_dv_to_cn(dv_type=opcodes.single_valve_decoder,
                                                     list_of_decoder_serial_nums=["TSQ0084"])
            self.config.controllers[1].load_dv_to_cn(dv_type=opcodes.moisture_sensor,
                                                     list_of_decoder_serial_nums=["SB07263"])
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_17(self):
        """
        Search for zones \n
        Initialize the new zones and replace the zones at address' 1, 2, 99, and 100 \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)

            self.config.zones[1] = Zone(_address=1, _serial="TSQ0031")
            self.config.zones[2] = Zone(_address=2, _serial="TSQ0032")
            self.config.zones[99] = Zone(_address=99, _serial="TSQ0033")
            self.config.zones[100] = Zone(_address=100, _serial="TSQ0034")

            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=[1, 2, 99, 100])

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_18(self):
        """
        Search for moisture sensors \n
        Replace soil moisture sensor at address 2 with sensor SB07263 \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)

            self.config.moisture_sensors[2] = MoistureSensor(_address=2, _serial="SB07263")

            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=[2])
            # this changes the serial number in the object to match the controller
            self.config.program_start_conditions[4]._device_serial = self.config.moisture_sensors[2].sn
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_19(self):
        """
        Search for master valves \n
        Replace the master valve at address 7 with TSQ0084 \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)

            self.config.master_valves[7] = MasterValve(_address=7, _serial="TSQ0084")
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=[7])
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_20(self):
        """
        Perform a test on all devices to verify that they are functioning properly and verify the new devices work \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].self_test_and_update_object_attributes()
            for moisture_sensors in self.config.ms_ad_range:
                self.config.moisture_sensors[moisture_sensors].self_test_and_update_object_attributes()
            for master_valves in self.config.mv_ad_range:
                self.config.master_valves[master_valves].self_test_and_update_object_attributes()
            for flow_meters in self.config.fm_ad_range:
                self.config.flow_meters[flow_meters].self_test_and_update_object_attributes()
            for event_switches in self.config.sw_ad_range:
                self.config.event_switches[event_switches].self_test_and_update_object_attributes()
            for temperature_sensors in self.config.ts_ad_range:
                self.config.temperature_sensors[temperature_sensors].self_test_and_update_object_attributes()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_21(self):
        """
        Verify the full configuration of the controller with the new devices \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            self.config.controllers[1].set_controller_to_run()
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)


