import sys

import time
from time import sleep

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_1000 import StartConditionFor1000
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

__author__ = 'Tige'


class ControllerUseCase13(object):
    """
        Test name: \n
            Watering Day Schedule 1000 Test \n
        Purpose: \n
            This test sets up different watering schedules and verifies that programs run correctly \n
        Coverage area: \n
            1.	Set up a programs to water for the different watering scheduled modes: \n
            2.  Set up a week days watering schedule and verify that the program only runs on week days set in the watering
                schedule \n
            3.  Set up an interval watering schedule for 3 days and verify that the program runs then skips 3 days and runs on
                the 4th \n
            4.  Set up an even days watering schedule and verify that the program only runs on even days of the week \n
            5.  Set up an odd days watering schedule and verify that the program only runs on odd days of the week \n
            6.  Set up an odd days skip 31st watering schedule and verify that the program only runs on odd days of the week
                and skips the 31st \n
            7.  Set up a historical ET calendar and verify that the set intervals work and that the interval changes every half
                a month \n
    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):

        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """

        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    self.step_18()
                    self.step_19()
                    self.step_20()
                    self.step_21()
                    self.step_22()
                    self.step_23()
                    self.step_24()
                    self.step_25()
                    self.step_26()
                    self.step_27()
                    self.step_28()
                    self.step_29()
                    self.step_30()
                    self.step_31()
                    self.step_32()
                    self.step_33()
                    self.step_34()
                    self.step_35()
                    self.step_36()
                    self.step_37()
                    self.step_38()
                    self.step_39()
                    self.step_40()
                    self.step_41()
                    self.step_42()
                    self.step_43()
                    self.step_44()
                    self.step_45()
                    self.step_46()
                    self.step_47()
                    self.step_48()
                    self.step_49()
                    self.step_50()
                    self.step_51()
                    self.step_52()
                    self.step_53()
                    self.step_54()
                    self.step_55()
                    self.step_56()
                    self.step_57()
                    self.step_58()
                    self.step_59()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.set_number_of_concurrent_zones_for_controller(number_of_concurrent_zones=4)
            self.config.controllers[1].init_cn()
            # only need this for BaseManager
            self.config.basemanager_connection[1].verify_ip_address_state()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=1)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-100
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(
                ts_object_dict=self.config.temperature_sensors,
                ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_1000_poc_objects()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_3(self):
        """
        assign each zone to a program and give them run times \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for programs in range(1, 7):
                self.config.programs[programs] = PG1000(_address=programs)
            for zone_programs in range(1, 7):
                self.config.zone_programs[zone_programs] = ZoneProgram(zone_obj=self.config.zones[zone_programs],
                                                                       prog_obj=self.config.programs[zone_programs],
                                                                       _rt=3600)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_4(self):
        """
        give each program a start time \n
        this time is given in minutes past midnight \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for programs in range(1, 7):
                self.config.program_start_conditions[programs] = StartConditionFor1000(program_ad=programs)
            for programs in range(1, 7):
                self.config.program_start_conditions[programs].set_day_time_start(_dt=opcodes.true, _st_list=[60])

        except AssertionError, ae:
            raise AssertionError("Assign Programs a start time Command Failed: " + ae.message)

    def step_5(self):
        """
        assign each program to use a different watering days strategy so we can test each one \n
        assign program 1 to use days of the week watering \n
        assign program 2 to use event days \n
        assign program 3 to use odd days \n
        assign program 4 to use odd days skip 31st day \n
        assign program 5 to use historical day calendar \n
        assign program 6 to use day intervals \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            mon_fri_list = [0,  # Sunday
                            1,  # Monday
                            0,  # Tuesday
                            1,  # Wednesday
                            0,  # Thursday
                            1,  # Friday
                            0   # Saturday
                            ]

            semi_month_list = [7, 6,       # January 1-15, January 16-31
                               6, 6,       # February 1-15, February 16-End of month
                               4, 3,       # March 1-15, March 16-31
                               3, 3,       # April 1-15, April 16-30
                               3, 2,       # May 1-15, May 16-31
                               2, 2,       # June 1-15, June 16-30
                               2, 2,       # July 1-15, July 16-31
                               2, 2,       # August 1-15, August 16-31
                               3, 3,       # September 1-15, September 16-30
                               3, 4,       # October 1-15, October 16-31
                               6, 6,       # November 1-15, November 16-30
                               6, 7        # December 1-15, December 16-31
                               ]

            self.config.program_start_conditions[1].set_day_time_start(_interval_type=opcodes.week_days,
                                                                       _interval_args=mon_fri_list)
            self.config.program_start_conditions[2].set_day_time_start(_interval_type=opcodes.calendar_interval,
                                                                       _interval_args=opcodes.even_day)
            self.config.program_start_conditions[3].set_day_time_start(_interval_type=opcodes.calendar_interval,
                                                                       _interval_args=opcodes.odd_day)
            self.config.program_start_conditions[4].set_day_time_start(_interval_type=opcodes.calendar_interval,
                                                                       _interval_args=opcodes.odd_days_skip_31)
            self.config.program_start_conditions[5].set_day_time_start(_interval_type=opcodes.semi_month_interval,
                                                                       _interval_args=semi_month_list)
            self.config.program_start_conditions[6].set_day_time_start(_interval_type=opcodes.day_interval,
                                                                       _interval_args=3)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_6(self):
        """
        Verify Full Configuration
        time \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_7(self):
        """
        start of first day:
        1 turn off sim mode in the controller \n
        2 stop the clock so that it can be manually incremented \n
        3 set the date and time of the controller so that there is a known days of the week and days of the month \n
        4 increment time by one second so that the controller is at the top of the hour this will get the correct zone
        status set \n
        5 set dial to run so that the controller will work in sim mode \n
        6 increment the time on the controller long enough to cross midnight the controller uses number of time pass
        midnight to determine days since last watering \n
        7 set date and time to 1 second before the start time after each date change increment the time by one second
        to allow status to change and get to the ball rolling \n
        8 after each date a time get set we increment the time one hour at a time in order to see the watering patterns
         \n
        9 at the end of each hour get zone status and verify the watering behavior is correct: \n
        10 on first day only programs 1 and 2 should water program 1 because its a monday and program 2 because it is an
        even day \n
        11 second day program 3 and 4 should run because they are odd days \n
        12 zone five is set to smart watering and will water ever third day \n
        13 zone six is set to day intervals and will water ever second day \n
            turn on sim mode in the controller \n
            stop the clock so that it can be incremented manually \n
            set the date and time of the controller so that there is a known days of the week and days of the
            month \n
            because of how the 1000 is set up you either have to wait a few minutes or change the dial position
            before you can start a program, after doing a search \n
            verify that none of the zones start early by checking 1 minute before the first start time \n
            zone concurrency varies between programs odd numbered programs have a zone concurrency of 1 meaning
            those zones should run 1 time before stopping \n
        set clock to 06/18/2012 12:59:59  which should start the zones watering \n
        increment clock 1 second to 6/18/2012 1:00 pm \n
        Verify the status for each program and zone and on the controller: \n
        get data from controller for each program \n
        get data from controller for each zone \n
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
                - Program 2 waiting
                - Program 3 done watering
                - Program 4 done watering
                - Program 5 done watering
                - Program 6 done watering
            - Zone Status
                - Zone 1 watering
                - Zone 2 waiting to water
                - Zone 3 done watering
                - Zone 4 done watering
                - Zone 5 done watering
                - Zone 6 done watering
            """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/18/2012', _time='00:59:59')
            self.config.controllers[1].set_controller_to_run()
            # self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[2].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_8(self):
        """
        First Day Second Hour \n
        increment clock 1 hour to 6/18/2012, which will make the time 2:00 A.M.\n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.running)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_9(self):
        """
        First Day Third Hour \n
        set clock to 06/18/2012 and increment by one hour, which will make a time of 3:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_10(self):
        """
        Second Day First Hour \n
        increment clock 21 hours and 15 minutes to 6/19/2012 \n
        set clock to 06/19/2012 12:59:59 one second before the zones begin watering \n
        Increment clock 1 second to 6/19/2012 1:00 pm \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.controllers[1].set_date_and_time_on_cn(_date='06/18/2012', _time='23:55:00')
            # self.config.controllers[1].do_increment_clock(minutes=6)
            self.config.controllers[1].do_increment_clock(hours=21, minutes=15)
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/19/2012', _time='00:59:59')
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.running)
            self.config.programs[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.watering)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_11(self):
        """
        Second Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 pm. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.running)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_12(self):
        """
        Second Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 pm. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_13(self):
        """
        Second Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 pm. \n
        Then verify the statuses of the following: \n
           - Program Status
                - Program 1 - 6 done watering
           - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_14(self):
        """
        Third Day First Hour
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/20/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second to 1:00 pm. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 running
            - Program 2 waiting to water
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 done watering
        - Zone Status
            - Zone 1 watering
            - Zone 2 waiting to water
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.controllers[1].set_date_and_time_on_cn(_date='06/19/2012', _time='23:55:00')
            # self.config.controllers[1].do_increment_clock(minutes=6)
            self.config.controllers[1].do_increment_clock(hours=20, minutes=15)
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/20/2012', _time='00:59:59')
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[2].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_15(self):
        """
        Third Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.running)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_16(self):
        """
        Third Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 running
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.running)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_17(self):
        """
        Third Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M . \n
        Then verify the statuses of the following: \n
           - Program Status
                - Program 1 - 6 done watering
           - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_18(self):
        """
        Fourth Day First Hour
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/21/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second which will make a time of 1:00 A.M \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 waiting to water
                """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.controllers[1].set_date_and_time_on_cn(_date='06/20/2012', _time='23:55:00')
            # self.config.controllers[1].do_increment_clock(minutes=6)
            self.config.controllers[1].do_increment_clock(hours=20, minutes=15)
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/21/2012', _time='00:59:59')
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.running)
            self.config.programs[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.waiting_to_water)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.watering)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_19(self):
        """
        Fourth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.running)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.waiting_to_water)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_20(self):
        """
        Fourth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 done watering
            - Program 6 running
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 done watering
            - Zone 6 watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.running)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_21(self):
        """
        Fifth Day First Hour
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/22/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 running
            - Program 2 waiting to water
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 done watering
        - Zone Status
            - Zone 1 watering
            - Zone 2 waiting to water
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.controllers[1].set_date_and_time_on_cn(_date='06/21/2012', _time='23:55:00')
            # self.config.controllers[1].do_increment_clock(minutes=6)
            self.config.controllers[1].do_increment_clock(hours=21, minutes=15)
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/22/2012', _time='00:59:59')
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[2].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_22(self):
        """
        Fifth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.running)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_23(self):
        """
        Fifth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 running
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.running)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_24(self):
        """
        Fifth Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 - 6 done watering
        - Zone Status
            - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_25(self):
        """
        Sixth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/23/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.controllers[1].set_date_and_time_on_cn(_date='06/22/2012', _time='23:55:00')
            # self.config.controllers[1].do_increment_clock(minutes=6)
            self.config.controllers[1].do_increment_clock(hours=20, minutes=15)
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/23/2012', _time='00:59:59')
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.running)
            self.config.programs[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.watering)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_26(self):
        """
        Sixth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.running)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_27(self):
        """
        Sixth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_28(self):
        """
        Sixth Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_29(self):
        """
        Seventh Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/24/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.controllers[1].set_date_and_time_on_cn(_date='06/23/2012', _time='23:55:00')
            # self.config.controllers[1].do_increment_clock(minutes=6)
            self.config.controllers[1].do_increment_clock(hours=20, minutes=15)
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/24/2012', _time='00:59:59')
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.running)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[6].verify_status_on_cn(opcodes.waiting_to_water)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_30(self):
        """
        Seventh Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 running
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 watering
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.running)
            self.config.programs[6].verify_status_on_cn(opcodes.waiting_to_water)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.watering)
            self.config.zones[6].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_31(self):
        """
        Seventh Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 done watering
            - Program 6 running
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 done watering
            - Zone 6 watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.running)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_32(self):
        """
        Seventh Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_33(self):
        """
        Eighth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/25/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 running
            - Program 2 done watering
            - Program 3 waiting to water
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 watering
            - Zone 2 done watering
            - Zone 3 waiting to water
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=20, minutes=15)
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/25/2012', _time='00:59:59')
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_34(self):
        """
        Eighth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.running)
            self.config.programs[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.watering)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_35(self):
        """
        Eighth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.running)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_36(self):
        """
        Eighth Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_37(self):
        """
        Ninth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/26/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=20, minutes=15)
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/26/2012', _time='00:59:59')
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.running)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_38(self):
        """
        Ninth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 running
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.running)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_39(self):
        """
        Ninth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_40(self):
        """
        Tenth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/27/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 running
            - Program 2 done watering
            - Program 3 waiting to water
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 watering
            - Zone 2 done watering
            - Zone 3 waiting to water
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=21, minutes=15)
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/27/2012', _time='00:59:59')
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.waiting_to_water)

            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_41(self):
        """
        Tenth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.running)
            self.config.programs[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.waiting_to_water)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.watering)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_42(self):
        """
        Tenth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.running)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.waiting_to_water)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_43(self):
        """
        Tenth Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 done watering
            - Program 6 running
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 done watering
            - Zone 6 watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.running)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_44(self):
        """
        Tenth Day Fifth Hour \n
        Increment the time by one hour, which will make a time of 5:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_45(self):
        """
        Eleventh Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/28/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=19, minutes=15)
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/28/2012', _time='00:59:59')
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.running)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_46(self):
        """
        Eleventh Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 running
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.running)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_47(self):
        """
        Eleventh Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_48(self):
        """
        Twelfth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/29/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 running
            - Program 2 done watering
            - Program 3 waiting to water
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 watering
            - Zone 2 done watering
            - Zone 3 waiting to water
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=21, minutes=15)
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/29/2012', _time='00:59:59')
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_49(self):
        """
        Twelfth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.running)
            self.config.programs[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.watering)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_50(self):
        """
        Twelfth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.running)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_51(self):
        """
        Twelfth Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_52(self):
        """
        Thirteenth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/30/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=20, minutes=15)
            self.config.controllers[1].set_date_and_time_on_cn(_date='06/30/2012', _time='00:59:59')
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.running)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[6].verify_status_on_cn(opcodes.waiting_to_water)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_53(self):
        """
        Thirteenth Day Second Hour \n
        Increment time to one hour later, which will make a time of 2:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 running
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 watering
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.running)
            self.config.programs[6].verify_status_on_cn(opcodes.waiting_to_water)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.watering)
            self.config.zones[6].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_54(self):
        """
        Thirteenth Day Third Hour \n
        Increment time to one hour later, which will make a time of 3:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 done watering
            - Program 6 running
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 done watering
            - Zone 6 watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.running)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_55(self):
        """
        Thirteenth Day Fourth Hour \n
         Increment the time by one hour, which will make a time of 4:00 A.M. \n
         Then verify the statuses of the following: \n
             - Program Status
                 - Program 1 - 6 done watering
             - Zone Status
                 - Zone 1 - 6 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_56(self):
        """
        Fourteenth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 07/01/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=20, minutes=15)
            self.config.controllers[1].set_date_and_time_on_cn(_date='07/01/2012', _time='00:59:59')
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.running)
            self.config.programs[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.watering)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_57(self):
        """
        Fourteenth Day Second Hour \n
        set clock to 06/30/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one hour, which will make a time of 2:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[3].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[4].verify_status_on_cn(opcodes.running)
            self.config.programs[5].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[6].verify_status_on_cn(opcodes.done_watering)

            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_58(self):
        """
        Fourteenth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_59(self):
        """
        Fourteenth Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1)
            self.config.controllers[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.programs[program].get_data()
            for zones in range(1, 7):
                self.config.zones[zones].get_data()

            for program in range(1, 7):
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            for zones in range(1, 7):
                self.config.zones[zones].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

