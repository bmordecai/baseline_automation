import sys
from time import sleep
import time

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_1000 import POC1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

from old_32_10_sb_objects_dec_29_2017.common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase4(object):
    """
    Test name: \n
        Concurrent Zones Test \n
    Purpose: \n
        This test covers zone concurrency. It verifies that max controller concurrency and program concurrency dictate
        the number of zones running at a given time \n
    Coverage area: \n
        1. Setting up the controller and limiting the concurrent zones to 1 \n
        2. Searching and assigning: \n
            - Zones \n
        3. Set up the program and assign it's concurrent zones \n
        4. Assign all zones to a program \n
        5. Verify the full configuration \n
        6. Verify that all zones and programs have a done watering status before programs are started \n
        7. Verify that only the first zone is running after the program starts due to the controller having a 1 zone
           concurrent limit \n
        8. Change the Controller max zone concurrency to 7 and make sure that the zone's running are only restricted by
           the program's concurrency \n
        9. the program's concurrency \n
        10.Verify the full configuration again \n
    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise

        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # only need this for BaseManager
            # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock
            # for the reboot process.
            # self.config.controllers[1].set_sim_mode_to_off()
            self.config.basemanager_connection[1].verify_ip_address_state()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=1)
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller failed: " + ae.message)

    def step_2(self):
        """
        Load Devices to controller
        load all devices need into the controller so that they are available for use in the configuration \n
        sets the devices that will be used in the configuration of the controller \n
            - search and address the devices: \n
               - zones                 {zn} \n
               - Master Valves         {mv} \n
            - once the devices are found they can be addressed so that they can be used in the programming \n
               - zones can use addresses {1-200} \n
                - Master Valves can use address {1-8} \n
            - the 3200 auto address certain devices in the order it receives them: \n
                - Master Valves         {mv} \n
                - Moisture Sensors      {ms} \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # Load all devices to controller
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                             ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)

            self.config.create_1000_poc_objects()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_3(self):
        """
        set_up_programs \n
            - Give program 1 a max concurrency for the program of 3 zones \n
            - Give program 2 a max concurrency for the program of 3 zones \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.programs[1] = PG1000(_address=1, _max_concurrent_zones=3)
            self.config.programs[2] = PG1000(_address=2, _max_concurrent_zones=3)
    # TODO give the programs a 1:00 am start time
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_4(self):
        """
        Assign zones to programs \n
            - Give each zone a run time of 1 hour \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600)

            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600)

            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600)

            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600)

            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=3600)

            self.config.zone_programs[51] = ZoneProgram(zone_obj=self.config.zones[51],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=3600)

            self.config.zone_programs[53] = ZoneProgram(zone_obj=self.config.zones[53],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=3600)

            self.config.zone_programs[78] = ZoneProgram(zone_obj=self.config.zones[78],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=3600)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_5(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_6(self):
        """
        Set the date of the controller so that there is a known days of the week and days of the month \n
        Verify that all zones are working properly by verifying the zones before starting the programs \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 51 done watering \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 56 done watering \n
            - zone 75 done watering \n
            - zone 76 done watering \n
            - zone 78 done watering \n
            - zone 79 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_date_and_time_on_cn(_date="04/08/2014", _time="08:00:00")
            self.config.controllers[1].set_controller_to_run()

            # This is necessary for our data in zone and program to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_7(self):
        """
        Start Both programs \n
        Only one zone should be running since the controller's zone concurrency is 1 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 waiting to water \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
            - zone 51 waiting to water \n
            - zone 53 waiting to water \n
            - zone 78 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[1].ad, _function="SR")
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[2].ad, _function="SR")

            # Allows the controller to update the Zone status
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone and program to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[51].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[53].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[78].verify_status_on_cn(status=opcodes.waiting_to_water)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_8(self):
        """
        Stop Both programs \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 Done \n
            - program 2 Done \n
        - Zone status: \n
            - zone 1 Done \n
            - zone 2 Done \n
            - zone 3 Done \n
            - zone 4 Done  \n
            - zone 5 Done \n
            - zone 51 Done  \n
            - zone 53 Done  \n
            - zone 78 Done  \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[1].ad, _function="SP")
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[2].ad, _function="SP")
            self.config.controllers[1].do_increment_clock(minutes=1)
            # This is necessary for our data in zone and program to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[51].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[53].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[78].verify_status_on_cn(status=opcodes.done_watering)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_9(self):
        """
        Change the max zone concurrency for the controller to 7 \n
        Because the controller can run 7 zones but each program can only run 3 zones the total zones running should be
        6 zones
        Start programs 1 and 2 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 51 watering \n
            - zone 53 watering \n
            - zone 78 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=7)
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[1].ad, _function="SR")
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[2].ad, _function="SR")

            # Allows the controller to update the Zone status
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone and program to update
            self.config.programs[1].get_data()
            self.config.programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[51].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[53].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[78].verify_status_on_cn(status=opcodes.waiting_to_water)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_10(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)