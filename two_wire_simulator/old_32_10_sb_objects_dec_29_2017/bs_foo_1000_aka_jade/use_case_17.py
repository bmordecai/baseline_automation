from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_1000 import POC1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

__author__ = 'Tige'


class ControllerUseCase17(object):
    """
    Test name:
        - CN UseCase 17 firmware update
    purpose:
        - set up a full configuration on the controller
            - verify that no programming is lost when you:
                - update the firmware of the controller
            - verify that firmware update take effects:
                - verify all programing even with certain devices or attributes are disabled

    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n

        - setting programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign zones to programs \n
            - give each zone a run time of 1 hour and 30 minutes \n
            - assign moisture sensors to a primary zone 1 and set to lower limit watering

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n
                - priority
                    - low
                    - medium
                    - high\n
            - set high flow limit
            - high flow shut down \n
            - set unscheduled flow limit
            - enable unscheduled flow shut down \n

            - disable
                - all one of each device type \n
                - program
                - mainline
                - poc
            - firmware update from both BaseManager and usb: \n
            - verify message
            - verify configuration all stayed \n
    Date References:
        - configuration for script is located common\configuration_files\back_restore_firmware_update.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance,
                 json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        Step 1:
            - configure controller:
                -  initiate controller to a known state so that it doesnt have a configuration or any devices loaded
                - turn on echo so the commands are displayed in the console
                - turn on sim mode so the clock can be stopped
                - stop the clock
                - Set the date and time so  that the controller is in a known state
                - turn on faux IO
                - clear all devices
                - clear all programming

            - configure basemanager: \n
                - verify the controller is connected to basemanager \n

        step 2:
            - setting up devices:
                - Loading devices into controller
                - Searching for devices so that they can be addressed
                - Address zones and master valves
                - Set all devices:
                    - descriptions
                    - locations
                    - default parameters
        Step 3:
            - setting up programming:
                - set_up_programs
                - assign zones to programs \n
                - set up primary linked zones \n
                - give each zone a run time of 1 hour and 30 minutes \n
                - give each program a start time of 8:00 A.M. \n
        step 4:
            - setup zone programs: \n
                - must make zone 200 a primary zone before you can link zones to it \n
                - assign sensor to primary zone 1
                - assign moisture sensors to a primary zone 1 and set to lower limit watering
        Step 5:
            - setting up POCs: \n
                Set up POC 1 \n
                    - flow meter address of 1 \n
                    - master valve address of 1 \n
                    - enable state set to true \n
                    - target flow is 500 gallons per minute \n
                    - high flow limit is 550 gallons per minute \n
                    - shutdown on high flow is enabled \n
                    - unscheduled flow limit is set to 10 gallons per minute \n
                    - shutdown on unscheduled flow is enabled \n
                Set up POC 8
                    - flow meter address of 1 \n
                    - master valve address of 1 \n
                    - enabled state set to true \n
                    - target flow is set to 50 gallons per minute \n
                    - high flow limit is set to 75 gallons per minute \n
                    - shutdown on high flow is disabled \n
                    - unscheduled flow limit is 5 gallons per minute \n
                    - shutdown on unscheduled flow is disabled \n

        Step 6:
            - disable able devices:
        Step 7:
            - update firmware from BaseManager:
            - verify message
        step 8:
            - update all objects
        step 9:
            - verify that all attributes for each device did not change
            - because the clock was increment we can verify status and verify that the zone didn't start \n
        Step 10:
            - update firmware from usb:
            - verify message
        step 11:
            - update all objects
        step 12:
            - verify that all attributes for each device did not change
            - because the clock was increment we can verify status and verify that the zone didn't start \n
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - sets up the controller \n
        - verify basemanager connection
        """
        self.config.controllers[1].init_cn()

        # only need this for BaseManager
        # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock for
        self.config.basemanager_connection[1].verify_ip_address_state()


    def step_2(self):
        # """
        # - sets the devices that will be used in the configuration of the controller \n
        # - search and address the devices:
        #     - zones                 {zn}
        #     - Master Valves         {mv}
        #     - Moisture Sensors      {ms}
        #     - Temperature Sensors   {ts}
        #     - Event Switches        {sw}
        #     - Flow Meter            {fm}
        # - once the devices are found they can be addressed so that they can be used in the programming
        #     - zones can use addresses {1-200}
        #     - Master Valves can use address {1-8}
        # - the 3200 auto address certain devices in the order it receives them:
        #     - Master Valves         {mv}
        #     - Moisture Sensors      {ms}
        #     - Temperature Sensors   {ts}
        #     - Event Switches        {sw}
        #     - Flow Meter            {fm}
        # """
        # # load all devices need into the controller so that they are available for use in the configuration
        self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                     mv_d1_list=self.config.mv_d1,
                                                     d2_list=self.config.d2,
                                                     mv_d2_list=self.config.mv_d2,
                                                     d4_list=self.config.d4,
                                                     dd_list=self.config.dd,
                                                     ms_list=self.config.ms,
                                                     fm_list=self.config.fm,
                                                     ts_list=self.config.ts,
                                                     sw_list=self.config.sw)

        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
        # assign zones an address between 1-200
        self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                         zn_ad_range=self.config.zn_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
        self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                         mv_ad_range=self.config.mv_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                         ms_ad_range=self.config.ms_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                         ts_ad_range=self.config.ts_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
        self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                         sw_ad_range=self.config.sw_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
        self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                         fm_ad_range=self.config.fm_ad_range)
        self.config.create_1000_poc_objects()

    def step_3(self):
        """
        set_up_programs
        """
        # this is set in the PG3200 object
        program_number_1_water_windows = ['011111100001111111111110']
        program_number_3_water_windows = ['011111100001111111111110']
        program_number_4_water_windows = ['011111100001111111111110']
        program_number_20_water_windows = ['011111100000111111111110']

        self.config.programs[1] = PG1000(
            _address=1,
            _enabled_state=opcodes.true,
            _water_window=program_number_1_water_windows,
            _max_concurrent_zones=1,
            _seasonal_adjust=100,
            _point_of_connection_address=[1],
            _master_valve_address=1,
            _soak_cycle_mode=opcodes.disabled)

        self.config.programs[3] = PG1000(
            _address=3,
            _enabled_state=opcodes.true,
            _water_window=program_number_3_water_windows,
            _max_concurrent_zones=1,
            _seasonal_adjust=100,
            _point_of_connection_address=[1],
            _master_valve_address=1,
            _soak_cycle_mode=opcodes.disabled)

        self.config.programs[4] = PG1000(
            _address=4,
            _enabled_state=opcodes.true,
            _water_window=program_number_4_water_windows,
            _max_concurrent_zones=4,
            _seasonal_adjust=100,
            _point_of_connection_address=[3],
            _master_valve_address=1,
            _soak_cycle_mode=opcodes.disabled)

        self.config.programs[20] = PG1000(
            _address=20,
            _enabled_state=opcodes.true,
            _water_window=program_number_20_water_windows,
            _max_concurrent_zones=1,
            _seasonal_adjust=100,
            _point_of_connection_address=[3],
            _master_valve_address=1,
            _soak_cycle_mode=opcodes.disabled)

    def step_4(self):
        """
        Attach each zone to a zone program and give programs a runtime to make sure all the values will remain the
        same after a firmware update.
        """
        # Zone Programs
        self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                   prog_obj=self.config.programs[1],
                                                   _rt=900)
        self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                   prog_obj=self.config.programs[1],
                                                   _rt=self.config.zone_programs[1].rt)
        self.config.zone_programs[49] = ZoneProgram(zone_obj=self.config.zones[49],
                                                    prog_obj=self.config.programs[3],
                                                    _rt=1200)
        self.config.zone_programs[50] = ZoneProgram(zone_obj=self.config.zones[50],
                                                    prog_obj=self.config.programs[3],
                                                    _rt=1200)
        self.config.zone_programs[50] = ZoneProgram(zone_obj=self.config.zones[50],
                                                    prog_obj=self.config.programs[4],
                                                    _rt=1200)

        self.config.zone_programs[99] = ZoneProgram(zone_obj=self.config.zones[99],
                                                    prog_obj=self.config.programs[20],
                                                    _rt=1980)
        self.config.zone_programs[96] = ZoneProgram(zone_obj=self.config.zones[96],
                                                    prog_obj=self.config.programs[20],
                                                    _rt=self.config.zone_programs[99].rt)
        self.config.zone_programs[97] = ZoneProgram(zone_obj=self.config.zones[97],
                                                    prog_obj=self.config.programs[20],
                                                    _rt=self.config.zone_programs[99].rt)
        self.config.zone_programs[98] = ZoneProgram(zone_obj=self.config.zones[98],
                                                    prog_obj=self.config.programs[20],
                                                    _rt=self.config.zone_programs[99].rt)

    def step_5(self):
        """
        set_poc_1000
            Set up POC 1
                - flow meter address of 1
                - master valve address of 1
                - enable state set to true
                - target flow is 500 gallons per minute
                - high flow limit is 550 gallons per minute
                - shutdown on high flow is enabled
                - unscheduled flow limit is set to 10 gallons per minute
                - shutdown on unscheduled flow is enabled
            Set up POC 8
                - flow meter address of 1
                - master valve address of 1
                - enabled state set to true
                - target flow is set to 50 gallons per minute
                - high flow limit is set to 75 gallons per minute
                - shutdown on high flow is disabled
                - unscheduled flow limit is 5 gallons per minute
                - shutdown on unscheduled flow is disabled
        With the way these two POCs are set up, we have a fairly good coverage on the high and low, true or false
        variables that we want to stay consistent in a controller after an update.
        """
        self.config.poc[1] = POC1000(
            _address=1,
            _flow_meter_address=1,
            _master_valve_address=1,
            _enabled_state=opcodes.true,
            _target_flow=500,
            _high_flow_limit=550,
            _shutdown_on_high_flow=opcodes.true,
            _unscheduled_flow_limit=10,
            _shutdown_on_unscheduled=opcodes.true
        )

        self.config.poc[3] = POC1000(
            _address=3,
            _flow_meter_address=1,
            _master_valve_address=1,
            _enabled_state=opcodes.true,
            _target_flow=50,
            _high_flow_limit=75,
            _shutdown_on_high_flow=opcodes.false,
            _unscheduled_flow_limit=5,
            _shutdown_on_unscheduled=opcodes.false
        )

    def step_6(self):
        """
        Disable devices
        This area covers not losing programing when a device is disabled during a firmware update or reboot
        """
        # disable Zone (97}
        self.config.zones[97].set_enable_state_on_cn("FA")

        # disable flow meter {1}
        self.config.flow_meters[1].set_enable_state_on_cn("FA")

    def step_7(self):
        """
        Do a firmware update from a USB that will have an older version of firmware.
        :return:
        :rtype:
        """
        # TODO we are going to need a filename once we actually want to run the test
        self.config.controllers[1].do_firmware_update(were_from=opcodes.usb_flash_storage, file_name='Update')
        self.config.controllers[1].stop_clock()
        self.config.controllers[1].verify_message_on_cn(opcodes.restore_successful, opcodes.usb_flash_storage_source)

    def step_8(self):
        """
        After the reboot all of the devices get set to default values in the controller so you have to do a self test
        and than do an update to get each object to match what the controller has.
            - This doesn't update every value in our objects, only certain ones. Usually things like solenoid current,
              two-wire drop, etc.
        Perform a self test on all zones to verify that they are functioning properly \n
        """
        for zone in self.config.zn_ad_range:
            self.config.zones[zone].self_test_and_update_object_attributes()
        for moisture_sensors in self.config.ms_ad_range:
            self.config.moisture_sensors[moisture_sensors].self_test_and_update_object_attributes()
        for master_valves in self.config.mv_ad_range:
            self.config.master_valves[master_valves].self_test_and_update_object_attributes()
        for flow_meters in self.config.fm_ad_range:
            self.config.flow_meters[flow_meters].self_test_and_update_object_attributes()
        for event_switches in self.config.sw_ad_range:
            self.config.event_switches[event_switches].self_test_and_update_object_attributes()
        for temperature_sensors in self.config.ts_ad_range:
            self.config.temperature_sensors[temperature_sensors].self_test_and_update_object_attributes()

    def step_9(self):
        """
        Verify that all the values on our objects are still matching the values in the controller. This includes
        the firmware version.
        """
        # this also verifies firmware version
        self.config.verify_full_configuration()

    def step_10(self):
        """
        We do a firmware update from BaseManager.
        """
        # TODO need to create cn1000 files on BaseManager and pass the ID into this method
        self.config.controllers[1].do_firmware_update(were_from=opcodes.basemanager, bm_id_number=121)
        self.config.controllers[1].stop_clock()
        self.config.controllers[1].verify_message_on_cn(_status_code=opcodes.restore_successful,
                                                        _helper_object=opcodes.basemanager_source)

    def step_11(self):
        """
        After the reboot all of the devices get set to default values in the controller so you have to do a self test
        and than do an update to get each object to match what the controller has.
            - This doesn't update every value in our objects, only certain ones. Usually things like solenoid current,
              two-wire drop, etc.
        Perform a test on all zones to verify that they are functioning properly \n
        """
        for zone in self.config.zn_ad_range:
            self.config.zones[zone].self_test_and_update_object_attributes()
        for moisture_sensors in self.config.ms_ad_range:
            self.config.moisture_sensors[moisture_sensors].self_test_and_update_object_attributes()
        for master_valves in self.config.mv_ad_range:
            self.config.master_valves[master_valves].self_test_and_update_object_attributes()
        for flow_meters in self.config.fm_ad_range:
            self.config.flow_meters[flow_meters].self_test_and_update_object_attributes()
        for event_switches in self.config.sw_ad_range:
            self.config.event_switches[event_switches].self_test_and_update_object_attributes()
        for temperature_sensors in self.config.ts_ad_range:
            self.config.temperature_sensors[temperature_sensors].self_test_and_update_object_attributes()

    def step_12(self):
        """
        Verifies all of our object's variables are matching their controller counterparts
        """
        # this also verifies firmware version
        self.config.verify_full_configuration()




