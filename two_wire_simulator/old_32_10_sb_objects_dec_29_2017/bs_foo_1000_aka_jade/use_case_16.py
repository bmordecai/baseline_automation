import sys

from time import sleep
import time

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

from old_32_10_sb_objects_dec_29_2017.common import helper_methods

__author__ = "Baseline"


class ControllerUseCase16(object):
    """
    Test name: \n
        Timed Zones With Soak Cycles \n
    Purpose: \n
        This test covers using regular soak cycles and intelligent soak cycles. \n
    Coverage area: \n
        1. Set up the controller and limit its concurrent zones \n
        2. Searching and assigning: \n
            - Zones \n
            - Moisture sensors \n
            - Flow meters \n
        3. Set up programs and give them a run time using commands \n
        4. Assign zones to programs and give them run times \n
        5. Verify the full configuration \n
        6-26. This code goes through regular watering, and each step increments by one minute and then verifies that
              the correct zones are running \n
        27. Set both programs to use intelligent soak cycles, and set cycle counts, soak times, and run-times \n
        28-48. This code runs through the intelligent soak and each step increments one minute and then verifies that
               the correct zones are running/soaking/waiting \n
        49. Set the soak cycle mode on program 2 to be 'program cycles' \n
        50-70. This code runs through regular soak cycles and each step increments one minute and then verifies that
               the correct zones are running/soaking/waiting
        71. Re-verify the full configuration \n
    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    self.step_18()
                    self.step_19()
                    self.step_20()
                    self.step_21()
                    self.step_22()
                    self.step_23()
                    self.step_24()
                    self.step_25()
                    self.step_26()
                    self.step_27()
                    self.step_28()
                    self.step_29()
                    self.step_30()
                    self.step_31()
                    self.step_32()
                    self.step_33()
                    self.step_34()
                    self.step_35()
                    self.step_36()
                    self.step_37()
                    self.step_38()
                    self.step_39()
                    self.step_40()
                    self.step_41()
                    self.step_42()
                    self.step_43()
                    self.step_44()
                    self.step_45()
                    self.step_46()
                    self.step_47()
                    self.step_48()
                    self.step_49()
                    self.step_50()
                    self.step_51()
                    self.step_52()
                    self.step_53()
                    self.step_54()
                    self.step_55()
                    self.step_56()
                    self.step_57()
                    self.step_58()
                    self.step_59()
                    self.step_60()
                    self.step_61()
                    self.step_62()
                    self.step_63()
                    self.step_64()
                    self.step_65()
                    self.step_66()
                    self.step_67()
                    self.step_68()
                    self.step_69()
                    self.step_70()
                    self.step_71()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # only need this for BaseManager
            # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock
            # for the reboot process.
            # self.config.controllers[1].set_sim_mode_to_off()
            self.config.basemanager_connection[1].verify_ip_address_state()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=1)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_2(self):
        """
        Sets the devices that will be used in the configuration of the controller \n
        Search and address the devices: \n
            - Zones                 {zn} \n
        Once the devices are found they can be addressed so that they can be used in the programming \n
            - Zones can use addresses {1-200} \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # load all devices need into the controller so that they are available for use in the configuration
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.create_1000_poc_objects()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_3(self):
        """
        Set up program objects \n
        Set the programs max zone concurrency \n
        Disable Zone 5 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.programs[1] = PG1000(_address=1)
            self.config.programs[2] = PG1000(_address=2)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_4(self):
        """
        Assign the zones to programs \n
        Disable zone 5 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=180)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=300)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=240)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=180)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=180)
            self.config.zones[5].set_enable_state_on_cn(_state=opcodes.false)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_5(self):
        """
        Verify the entire configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_6(self):
        """
        Set the controller to off and then turn it back on \n
        Stop both programs \n
        Increment the clock by 1 second \n
        Set the time to be '04/08/2014 02:37:58' \n
        Increment the clock by 1 second again \n
        Verify that all zones are responding and have a 'done watering' status \n
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_controller_to_off()
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.stop)
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function=opcodes.stop)
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].set_date_and_time_on_cn(_date="04/08/2014", _time="02:37:58")
            self.config.controllers[1].do_increment_clock(seconds=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_7(self):
        """
        Start both programs \n
        Increment the clock by 1 second again so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function="SR")
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function="SR")
            self.config.controllers[1].do_increment_clock(seconds=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_8(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:39:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_9(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:40:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_10(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:41:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_11(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:42:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_12(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:43:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_13(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:44:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_14(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:45:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    #
    def step_15(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:46:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_16(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:47:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_17(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:48:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_18(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:49:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_19(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:50:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_20(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:51:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_21(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:52:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_22(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:53:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_23(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:54:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_24(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:55:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_25(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:56:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_26(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:57:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_27(self):
        """
        Set both programs to use intelligent soak cycles \n
        Set cycle counts on both programs \n
        Set soak times for both programs \n
        Change the run-times on each zone program \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.programs[1].set_soak_cycle_mode_on_cn(_mode=opcodes.intelligent_soak)
            self.config.programs[1].set_cycle_count_on_cn(_new_count=20)
            self.config.programs[1].set_soak_time_in_seconds_on_cn(_seconds=480)
            self.config.programs[2].set_soak_cycle_mode_on_cn(_mode=opcodes.intelligent_soak)
            self.config.programs[2].set_cycle_count_on_cn(_new_count=6)
            self.config.programs[2].set_soak_time_in_seconds_on_cn(_seconds=300)

            self.config.zone_programs[1].set_run_time_on_cn(_run_time=3600)
            self.config.zone_programs[2].set_run_time_on_cn(_run_time=720)
            self.config.zone_programs[3].set_run_time_on_cn(_run_time=1080)
            self.config.zone_programs[4].set_run_time_on_cn(_run_time=360)
            self.config.zone_programs[5].set_run_time_on_cn(_run_time=720)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_28(self):
        """
        Set the controller to off and then turn it back on \n
        Stop both programs \n
        Increment the clock by 1 second \n
        Set the time to be '04/08/2014 02:37:58' \n
        Increment the clock by 1 second again \n
        Verify that all zones are responding and have a 'done watering' status \n
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_controller_to_off()
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.stop)
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function=opcodes.stop)
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].set_date_and_time_on_cn(_date="04/08/2014", _time="02:37:58")
            self.config.controllers[1].do_increment_clock(seconds=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_29(self):
        """
        Start both programs \n
        Increment the clock by 1 second again so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function="SR")
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function="SR")
            self.config.controllers[1].do_increment_clock(seconds=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_30(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:39:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_31(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:40:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_32(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:41:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_33(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:42:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_34(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:43:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_35(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:44:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_36(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:45:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_37(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:46:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_38(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:47:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_39(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:48:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_40(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:49:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 waiting to water \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_41(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:50:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_42(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:51:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 waiting to water \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_43(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:52:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_44(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:53:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_45(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:54:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_46(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:55:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 waiting to water \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_47(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:56:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_48(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:57:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_49(self):
        """
        Set the soak cycle mode of program 2 to program cycles \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.programs[2].set_soak_cycle_mode_on_cn(_mode=opcodes.program_cycles)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_50(self):
        """
        Set the controller to off and then turn it back on \n
        Stop both programs \n
        Increment the clock by 1 second \n
        Set the time to be '04/08/2014 02:37:58' \n
        Increment the clock by 1 second again \n
        Verify that all zones are responding and have a 'done watering' status \n
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_controller_to_off()
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.stop)
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function=opcodes.stop)
            self.config.controllers[1].do_increment_clock(seconds=1)
            self.config.controllers[1].set_date_and_time_on_cn(_date="04/08/2014", _time="02:37:58")
            self.config.controllers[1].do_increment_clock(seconds=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_51(self):
        """
        Start both programs \n
        Increment the clock by 1 second again so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function="SR")
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function="SR")
            self.config.controllers[1].do_increment_clock(seconds=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_52(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_53(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_54(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_55(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_56(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_57(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_58(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_59(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_60(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_61(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_62(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 waiting to water \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_63(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_64(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 waiting to water \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_65(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)


    def step_66(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_67(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_68(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 waiting to water \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_69(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 waiting to water \n
            - zone 3 soaking \n
            - zone 4 watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_70(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_71(self):
        """
        Re-verify the full configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)
