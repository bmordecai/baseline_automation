import sys
from time import sleep

import time

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_1000 import POC1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

from old_32_10_sb_objects_dec_29_2017.common import helper_methods

__author__ = 'Baseline'


class ControllerUseCase2(object):
    """
    Test name: \n
        Basic Design Flow Test \n
    Purpose: \n
        This test covers different flow operations: \n
            - When the flow limit only allows certain zones to be run. The zones that run should utilize as much of the
             flow limit as they can depending on their design flow. \n
            - When there is no flow limit and we want to run a certain number of concurrent zones. \n
            - Running multiple programs with a flow limit from a single water source (POC) \n
            - Running multiple programs with separate water sources \n
    Coverage area: \n
        1. Setting up the controller \n
        2. Searching and assigning: \n
            - Zones \n
            - Master Valves \n
            - Flow Meters \n
        3. Assign each zone a design flow \n
        4. Setting up water sources \n
        5. Set up the programs \n
        6. Assign each zone to a program \n
        7. Set up the zone concurrency for the controller and programs \n
        8. Verify the full configuration
        9. Run programs with a design flow restriction on program 1 with Limit Concurrent active \n
        10. Run programs with two design flow one on each program, one with Limit Concurrent active and one without \n
        11. Run programs with two design flow one on each program, both with Limit Concurrent active \n
        12. Verify the full configuration again
    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # only need this for BaseManager
            # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock
            # for the reboot process.
            # self.config.controllers[1].set_sim_mode_to_off()
            self.config.basemanager_connection[1].verify_ip_address_state()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=4)
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller failed: " + ae.message)

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Load all devices to controller
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_1000_poc_objects()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_3(self):
        """
        Give each zone a design flow
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.zones[51].set_design_flow_on_cn(_df=20)
            self.config.zones[52].set_design_flow_on_cn(_df=20)
            self.config.zones[53].set_design_flow_on_cn(_df=20)
            self.config.zones[54].set_design_flow_on_cn(_df=12)
            self.config.zones[55].set_design_flow_on_cn(_df=7.5)
            self.config.zones[56].set_design_flow_on_cn(_df=15)
            self.config.zones[75].set_design_flow_on_cn(_df=15)
            self.config.zones[76].set_design_flow_on_cn(_df=7.5)
            self.config.zones[78].set_design_flow_on_cn(_df=7.5)
            self.config.zones[79].set_design_flow_on_cn(_df=7.5)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_4(self):
        """
        Set up water source 1 with the initial settings \n
            Set design flow to 50 and set limit concurrent to target to true \n
        Set up water source 2 with a design flow of 25 and disable limit concurrent to target \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1] = POC1000(_address=1,
                                         _flow_meter_address=1,
                                         _master_valve_address=1,
                                         _target_flow=50,
                                         _limit_concurrent_zones_to_target=opcodes.true)

            self.config.poc[2] = POC1000(_address=2,
                                         _flow_meter_address=1,
                                         _master_valve_address=1,
                                         _target_flow=25,
                                         _limit_concurrent_zones_to_target=opcodes.false)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_5(self):
        """
        Set up the programs:
            Assign each program a start time (Can't be currently done) \n
            Set up program soak cycles \n
            Assign each program to water source 1 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.programs[1] = PG1000(_address=1,
                                             _cycle_count=2,
                                             _soak_time=240,
                                             _point_of_connection_address=self.config.poc[1].ad)
            self.config.programs[2] = PG1000(_address=2,
                                             _cycle_count=2,
                                             _soak_time=240,
                                             _point_of_connection_address=self.config.poc[1].ad)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_6(self):
        """
        Assign each zone to a program and give them an hour run time \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zone_programs[51] = ZoneProgram(zone_obj=self.config.zones[51],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=3600)
            self.config.zone_programs[52] = ZoneProgram(zone_obj=self.config.zones[52],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=3600)
            self.config.zone_programs[53] = ZoneProgram(zone_obj=self.config.zones[53],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=3600)
            self.config.zone_programs[54] = ZoneProgram(zone_obj=self.config.zones[54],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=3600)
            self.config.zone_programs[55] = ZoneProgram(zone_obj=self.config.zones[55],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=3600)
            self.config.zone_programs[56] = ZoneProgram(zone_obj=self.config.zones[56],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=3600)
            self.config.zone_programs[75] = ZoneProgram(zone_obj=self.config.zones[75],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=3600)
            self.config.zone_programs[76] = ZoneProgram(zone_obj=self.config.zones[76],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=3600)
            self.config.zone_programs[78] = ZoneProgram(zone_obj=self.config.zones[78],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=3600)
            self.config.zone_programs[79] = ZoneProgram(zone_obj=self.config.zones[79],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=3600)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_7(self):
        """
        Set up max zone concurrency for the controller and each program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=10)
            self.config.programs[1].set_max_concurrent_zones_on_cn(_number_of_zones=4)
            self.config.programs[2].set_max_concurrent_zones_on_cn(_number_of_zones=3)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_8(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_9(self):
        """
        turn on sim mode in the controller \n
        stop the clock so that it can be incremented manually \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        verify that no zones are currently running \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 51 done watering \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 56 done watering \n
            - zone 75 done watering \n
            - zone 76 done watering \n
            - zone 78 done watering \n
            - zone 79 done watering \n
        increment time by one minute so that the controller will get the correct zone status set \n
        verify that only zones 51, 52, and 55 are watering because water source 1 is limited to 50 GPM while zones 51
        and 52 run at 20 GPM each and zone 55 runs at 7.5 bring it up to a grand total of 47.5 GPM leaving
        nothing for the rest of the water source \n
        verify that program 2 does not run because it runs off the flow from water source 1 and there is not enough flow
        left to run any zones on program 2 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 51 watering \n
            - zone 52 watering \n
            - zone 53 waiting to water \n
            - zone 54 waiting to water \n
            - zone 55 watering \n
            - zone 56 waiting to water \n
            - zone 75 waiting to water \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_date_and_time_on_cn(_date="06/18/2012", _time="08:00:00")
            self.config.controllers[1].set_controller_to_run()
            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            # Verify that no zones are running. This is known by checking the status for 'DN'
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)

            # This starts the programs
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[1].ad, _function="SR")
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[2].ad, _function="SR")

            # Allows the controller to update the Zone status
            self.config.controllers[1].do_increment_clock(minutes=1)
            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            # Verify that only zones 51, 52, and 55 are watering and the rest are waiting
            for zone in [51, 52, 55]:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.watering)
            # Verify that the rest of the zones are waiting to water
            for zone in [53, 54, 56, 75, 76, 78, 79]:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_10(self):
        """
        re-assign program 2 to water source 2 \n
        turn dial to off to ensure that nothing is left running \n
        increment the clock 1 second so that the programs stop running \n
        Because of how the 1000 is set up you either have to wait a few minutes or change the dial position to run
        Before you can start a program, after doing a search \n
        start programs 1 & 2 \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        increment clock by 1 minute to get the programs running \n
        verify that zones 51, 52, and 55 are the only zones running on program 1 because water source 1 is limited to
        50 GPM, and those 3 zones take up 47.5 GPM \n
        verify that zones 56, 75, and 76 are the only zones running on program 2 because water source 2 is not limited
        by flow so the program runs off concurrency \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program watering \n
        - Zone status: \n
            - zone 51 watering \n
            - zone 52 watering \n
            - zone 53 waiting to water \n
            - zone 54 waiting to water \n
            - zone 55 watering \n
            - zone 56 watering \n
            - zone 75 watering \n
            - zone 76 watering \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.programs[2].set_point_of_connection_on_cn(_poc_address=self.config.poc[2].ad)
            self.config.controllers[1].set_controller_to_off()
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[1].ad, _function="SR")
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[2].ad, _function="SR")
            self.config.controllers[1].set_date_and_time_on_cn(_date="06/18/2012", _time="08:00:00")
            self.config.controllers[1].do_increment_clock(minutes=1)
            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            # Verify that only zones 51, 52, and 55 are watering and the rest are waiting
            for zone in [51, 52, 55, 56, 75, 76]:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.watering)
            # Verify that the rest of the zones are waiting to water
            for zone in [53, 54, 78, 79]:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_11(self):
        """
        enable limit concurrent to target on water source 2 \n
        turn dial to off to ensure that nothing is left running \n
        advance the clock 1 second so that the programs stop running \n
        Because of how the 1000 is set up you either have to wait a few minutes or change the dial position
        Before you can start a program, after doing a search \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        increment clock so that the programs can start \n
        verify that zones 51, 52, and 55 are the only zones running on program 1 because water source 1 is limited to
        50 GPM and those 3 zones take up 47.5 GPM \n
        verify that zones 56 and 76 are the only zones running on program 2 because water source 2 is limited to 25 GPM
        because it is now limited by flow and both zones take up 22.5 GPM \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 51 watering \n
            - zone 52 watering \n
            - zone 53 waiting to water \n
            - zone 54 waiting to water \n
            - zone 55 watering \n
            - zone 56 watering \n
            - zone 75 waiting to water \n
            - zone 76 watering \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[2].set_limit_concurrent_to_target_state_on_cn(_new_state=opcodes.true)
            self.config.controllers[1].set_controller_to_off()
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[1].ad, _function="SR")
            self.config.controllers[1].set_program_start_stop(_pg_ad=self.config.programs[2].ad, _function="SR")
            self.config.controllers[1].set_date_and_time_on_cn(_date="06/18/2012", _time="08:00:00")
            self.config.controllers[1].do_increment_clock(minutes=1)
            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            # Verify that only zones 51, 52, and 55 are watering and the rest are waiting
            for zone in [51, 52, 55, 56, 76]:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.watering)
            # Verify that the rest of the zones are waiting to water
            for zone in [53, 54, 75, 78, 79]:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_12(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)