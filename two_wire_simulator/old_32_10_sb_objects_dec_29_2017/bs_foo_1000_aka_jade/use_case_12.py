# this import allows us to directly use the date_mngr
import sys
from datetime import time
from time import sleep
# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_1000 import POC1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_1000 import StartConditionFor1000, PauseConditionFor1000, StopConditionFor1000

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram


__author__ = 'Tige'


class ControllerUseCase12(object):
    """
    Test name:
        - Soak cycles
    purpose:
        -
      Coverage area: \n
    1. Able to disable zones \n
    2. Able to set zone concurrency \n
    3. Able to set up soak cycles and verify that they run properly \n

    Date References:
        - configuration for script is located common\configuration_files\soak_cycles.json
        - the devices and addresses range is read from the .json file

    """
    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance,
                 json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        Step 1:
            - configure controller:
            - configure BaseManager: \n
        step 2:
            - setting up devices:
        Step 3:
            - set up programs: \n
        Step 4:
            - set up zone programs : \n
        Step 5:
            - set up pocs: \n
        Step 6:
            - set up start stop pause conditions: \n
        Step 7:
            - verify the full configuration of the controller: \n

        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_5_1()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    self.step_18()
                    self.step_19()
                    self.step_20()
                    self.step_21()
                    self.step_22()
                    self.step_23()
                    self.step_24()
                    self.step_25()
                    self.step_26()
                    self.step_27()
                    self.step_28()
                    self.step_29()
                    self.step_30()
                    self.step_31()
                    self.step_32()
                    self.step_33()
                    self.step_34()
                    self.step_35()
                    self.step_36()
                    self.step_37()
                    self.step_38()
                    self.step_39()
                    self.step_40()
                    self.step_41()
                    self.step_42()
                    self.step_43()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise

        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()
            # only need this for BaseManager
            self.config.basemanager_connection[1].verify_ip_address_state()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_2(self):
        """
        load all devices need into the controller so that they are available for use in the configuration \n
        sets the devices that will be used in the configuration of the controller \n
            - search and address the devices: \n
               - zones                 {zn} \n
               - Master Valves         {mv} \n
            - once the devices are found they can be addressed so that they can be used in the programming \n
               - zones can use addresses {1-200} \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=2)
            self.config.create_1000_poc_objects()

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_3(self):
        """
        set_up_programs \n
        """
        # this is set in the PG3200 object
        # TODO need to have concurrent zones per program added

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        program_full_open_water_windows = ['111111111111111111111111']
        try:
            self.config.programs[1] = PG1000(_address=1,
                                             _enabled_state=opcodes.true,
                                             _water_window=program_full_open_water_windows,
                                             _max_concurrent_zones=1,
                                             _seasonal_adjust=100,
                                             _point_of_connection_address=[1],
                                             _soak_cycle_mode=opcodes.intelligent_soak,
                                             _cycle_count=6,
                                             _soak_time=1500)
            self.config.programs[2] = PG1000(_address=2,
                                             _enabled_state=opcodes.true,
                                             _water_window=program_full_open_water_windows,
                                             _max_concurrent_zones=1,
                                             _seasonal_adjust=100,
                                             _point_of_connection_address=[1],
                                             _soak_cycle_mode=opcodes.intelligent_soak,
                                             _cycle_count=4,
                                             _soak_time=900)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_4(self):
        """
        give each zone a run time of 1 hour, and soak cycle \n
        set program start time \n
        set up max controller concurrency \n
            - assign zones to programs \n
            - set up primary linked zones \n
            - give each zone a run time of 1 hour and 30 minutes \n
            - must make zone 200 a primary zone before you can link zones to it \n
            - assign sensor to primary zone 1
            - assign moisture sensors to a primary zone 1 and set to lower limit watering
        disable zone 5
        :return:
        :rtype:
        """
        # Zone Programs


        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600)

            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=7200)

            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=1800)

            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=3600)

            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=3600)
            self.config.zones[5].set_enable_state_on_cn(_state=opcodes.false)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_5(self):
        """
        set up start stop pause conditions
            Program 1:
                - start times 8:00 am \n
                - watering days are set to weekly \n
                - Watering days enabled are every day\n
                - assign moisture sensor SB05308 to start program 1 and give it a threshold of 25 \n
        """
        start_times_800am = [480]
        every_day_watering = [1, 1, 1, 1, 1, 1, 1]  # runs every day
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.program_start_conditions[1] = StartConditionFor1000(program_ad=1)
            self.config.program_start_conditions[2] = StartConditionFor1000(program_ad=2)

            self.config.program_start_conditions[1].set_day_time_start(_dt=opcodes.true,
                                                                       _st_list=start_times_800am,
                                                                       _interval_type=opcodes.week_days,
                                                                       _interval_args=every_day_watering)
            self.config.program_start_conditions[2].set_day_time_start(_dt=opcodes.true,
                                                                       _st_list=start_times_800am,
                                                                       _interval_type=opcodes.week_days,
                                                                       _interval_args=every_day_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_5_1(self):
        """
        verify the full configuration of the controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_6(self):
        """
        zone concurrency varies between programs odd numbered programs have a zone concurrency of 1 meaning those zones
        should run 1 time before stopping \n
        even numbered programs have a zone concurrency of 2 meaning those zones should run twice back to back before
        stopping \n
        verify odd programs run 1 time before stopping and even programs run 2 times before stopping \n
        due to a lack of memory on the 1000 only 10 programs can run at one time \n
        set clock to 04/08/2014 7:55 am this should start the zones watering \n
        \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 - 2 done watering \n
            - Zone status: \n
                - zone 1 done watering \n
                - zone 2 done watering \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for program in range(1, 3):
                self.config.programs[program].get_data()
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].set_date_and_time_on_cn(_date='04/08/2014', _time='07:55:00')
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_7(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:00 this should start the zones watering \n
        get data from programs so that it can be verified against the object
        get data from zones that are addressed from the json file so that it can be verified against the object
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n
            - Zone status: \n
                - zone 1 watering \n
                - zone 2 watering \n
                - zone 3 waiting to water   \n
                - zone 4 watering   \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            for program in range(1, 3):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_8(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:05 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 watering \n
                - zone 2 waiting to water\n
                - zone 3 waiting to water  \n
                - zone 4 watering   \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            for program in range(1, 3):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_9(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:10 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 soaking \n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            for program in range(1, 3):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_10(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:15 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water \n

            - Zone status: \n
                - zone 1 soaking \n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_11(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:20 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water \n

            - Zone status: \n
                - zone 1 soaking \n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_12(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:25 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water \n

            - Zone status: \n
                - zone 1 soaking \n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_13(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:30 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 soaking \n
                - zone 2 soaking\n
                - zone 3 watering  \n
                - zone 4 watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            for program in range(1, 3):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_14(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:35 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 soaking  \n
                - zone 4 watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            for program in range(1, 3):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_15(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:40 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 watering \n
                - zone 2 soaking\n
                - zone 3 soaking  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            for program in range(1, 3):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_16(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:45 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 waiting to water \n
                - program 2 waiting to water\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 soaking  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[2].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_17(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:50 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 waiting to water \n
                - program 2 waiting to water\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 soaking  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[2].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_18(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:55 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 soaking  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_19(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:00 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            for program in range(1, 3):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_20(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:05 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            for program in range(1, 3):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_21(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:10 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 waiting to water\n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            for program in range(1, 3):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_22(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:15 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water \n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_23(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:20 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water \n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_24(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:25 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water \n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_25(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:30 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 waiting to water\n
                - program 2 running \n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 soaking  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[2].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_26(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:35 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 waiting to water\n
                - program 2 running \n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 soaking  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[2].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_27(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:40 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 running \n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 soaking  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            for program in range(1, 3):
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_28(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:45 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 soaking  \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_29(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:50 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 waiting to water\n
                - zone 2 watering\n
                - zone 3 soaking  \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_30(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:55 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 waiting to water \n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_31(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:00 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water  \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_32(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:05 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water  \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_33(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:10 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 watering \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_34(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:15 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 waiting to water\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 soaking \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_35(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:20 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 waiting to water\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 soaking \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_36(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:25 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 soaking \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_37(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:30 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 soaking \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_38(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:35 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 waiting to water\n
                - zone 2 watering\n
                - zone 3 soaking \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_39(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:40 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 waiting to water\n
                - zone 2 watering\n
                - zone 3 waiting to water \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_40(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:45 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_41(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:50 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=5)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_42(self):
        """
        advance the clock 1 hour and 35 minutes to make the time 04/08/2014 12:25 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 done watering\n
                - zone 2 done watering\n
                - zone 3 done watering \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(hours=1, minutes=35)
            self.config.controllers[1].verify_date_and_time()
            for program in range(1, 3):
                self.config.programs[program].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[2].verify_status_on_cn(opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_43(self):
        """
        verify the full configuration of the controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)