import sys

from time import sleep

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_1000 import StartConditionFor1000, PauseConditionFor1000, \
    StopConditionFor1000

from old_32_10_sb_objects_dec_29_2017.common import helper_methods

__author__ = "baseline"


class ControllerUseCase14(object):
    """
    Test name: \n
        Upper Limit Watering \n
    Purpose: \n
        This test covers using the stop immediately function on a moisture stop. \n
    Coverage area: \n
        1. Set up the controller \n
        2. Search and assign: \n
            - Zones \n
            - Moisture Sensors \n
        3. Give the zones a design flow \n
        4. Set up programs and give them a start times \n
        5. Assign zones to programs and give them run times \n
        6. Set a stop condition for program 1 to stop when the moisture percent reaches the upper limit of 25% \n
        7. Verify the configuration \n
        8. Set up the date and time and make sure that the all zones and programs are connected \n
        9. Increment the clock so the programs start watering \n
        10. Increase the moisture percent so program 1's stop condition is triggered, confirm zones are done watering \n
        11. Reset the moisture percent to 20% and create a new stop condition that disables stop immediately \n
        12-17. Verify that program will finish running when told to stop when stop immediately is disabled \n
        18. Verify the full configuration again
    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    self.step_18()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    # def all_upper_limit_watering_1000(self):
    #     print("Running Upper Limit Watering test for 1000")
    #     try:
    #         try:
    #             self.ser.setTimeout(1200)
    #             self.initiate_controller()
    #             self.set_up_controller()
    #             self.search_and_assign_and_setup_zones()
    #             self.set_up_programs()
    #             self.search_and_assign_and_setup_moisture_sensors()
    #         except AssertionError, ae:
    #             print("basic programming " + ae.message)
    #             raise
    #         try:
    #             self.run_first_exercise()
    #             self.run_second_exercise()
    #         except AssertionError, ae:
    #             raise AssertionError('exercise failed' + ae.message)
    #     except AssertionError, ae:
    #         raise AssertionError("upper_limit_watering_test_for_the_100" + ae.message)

    def step_1(self):
        """
        Sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        Set the max number of concurrent zones to 10 \n
        """

        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # only need this for BaseManager
            self.config.basemanager_connection[1].verify_ip_address_state()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=10)
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller failed: " + ae.message)

    def step_2(self):
        """
        Sets the devices that will be used in the configuration of the controller \n
        Search and address the devices:
            - Zones                 {zn}
            - Moisture Sensors      {ms}
        Once the devices are found they can be addressed so that they can be used in the programming
            - Zones can use addresses {1-200}
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # load all devices need into the controller so that they are available for use in the configuration
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.create_1000_poc_objects()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_3(self):
        """
        Set the design flow for all zones:
            - Zone 1: 10 (gpm)
            - Zone 2: 10 (gpm)
            - Zone 3: 10 (gpm)
            - Zone 4: 10 (gpm)
            - Zone 5: 10 (gpm)
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].set_design_flow_on_cn(_df=10)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_4(self):
        """
        Create the programs \n
        Limit the program's concurrent zones to 1 \n
        Give the program a start time of 8:00 A.M. \n
        Set the program to week day watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.programs[1] = PG1000(_address=1, _max_concurrent_zones=1)
            self.config.program_start_conditions[1] = StartConditionFor1000(program_ad=1)
            self.config.program_start_conditions[1].set_day_time_start(_dt=opcodes.true,
                                                                       _st_list=[480],
                                                                       _interval_type=opcodes.week_days,
                                                                       _interval_args='1111111')
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_5(self):
        """
        Assign zones to programs \n
        Assign run-times to the zones: \n
            - Zone 1: 30 minutes \n
            - Zone 2: 30 minutes \n
            - Zone 3: 15 minutes \n
            - Zone 4: 45 minutes \n
            - Zone 5: 30 minutes \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=1800)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=1800)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=2700)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=1800)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_6(self):
        """
        Set moisture sensor to stop program immediately when moisture level is above the limit \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.program_stop_conditions[1] = StopConditionFor1000(program_ad=1)
            self.config.program_stop_conditions[1].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn,
                mode=opcodes.upper_limit,
                threshold=25,
                si=opcodes.true)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_7(self):
        """
        Verify the entire configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_8(self):
        """
        Set the moisture percent on the moisture sensor to be 20%, which is below the upper limit \n
        Set up the time to be 08/28/2014 7:45 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=20)
            self.config.moisture_sensors[1].do_self_test()
            self.config.controllers[1].set_date_and_time_on_cn(_date="08/28/2014", _time="07:45:00")
            self.config.controllers[1].set_controller_to_run()

            self.config.programs[1].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_9(self):
        """
        Increment the clock by 16 minutes so the time is 08/28/2014 8:01 \n
        This should trigger the start time of program 1 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=16)

            self.config.programs[1].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_10(self):
        """
        Set the moisture percent on the moisture sensor to be 26%, which is above the upper limit \n
        Increment the clock by 4 minutes so the time is 08/28/2014 8:05 \n
        This should trigger the stop condition associated with program 1 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=26)
            self.config.moisture_sensors[1].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=4)

            self.config.programs[1].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_11(self):
        """
        Set the moisture percent on the moisture sensor to be 20%, which is below the upper limit \n
        Set a stop condition for program 1 when the moisture percent reads above 25% \n
        Set the stop condition so that it does not stop immediately \n
        Set up the time to be 08/28/2014 7:45 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=20)
            self.config.moisture_sensors[1].do_self_test()
            self.config.program_stop_conditions[1].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn,
                mode=opcodes.upper_limit,
                threshold=25,
                si=opcodes.false)
            self.config.controllers[1].set_date_and_time_on_cn(_date="08/28/2014", _time="07:45:00")
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_12(self):
        """
        Increment the clock by 16 minutes so the time is 08/28/2014 8:01 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=16)
            self.config.controllers[1].set_controller_to_run()

            self.config.programs[1].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_13(self):
        """
        Increment the clock by 30 minutes so the time is 08/28/2014 8:31 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=30)
            self.config.controllers[1].set_controller_to_run()

            self.config.programs[1].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_14(self):
        """
        Set the moisture percent to 26%, which is above program 1's upper limit \n
        Increment the clock by 34 minutes so the time is 08/28/2014 9:05 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=26)
            self.config.moisture_sensors[1].do_self_test()
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].do_increment_clock(minutes=34)

            self.config.programs[1].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_15(self):
        """
        Increment the clock by 15 minutes so the time is 08/28/2014 9:20 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=15)

            self.config.programs[1].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_16(self):
        """
        Increment the clock by 45 minutes so the time is 08/28/2014 10:05 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=45)

            self.config.programs[1].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.watering)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_17(self):
        """
        Increment the clock by 30 minutes so the time is 08/28/2014 10:35 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=30)

            self.config.programs[1].get_data()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_18(self):
        """
        Verify the full configuration again \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)
