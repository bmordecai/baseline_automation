import json
import os
import platform

__author__ = 'Baseline'


class UserConfiguration(object):
    """
    Online Documentation Area: \n
    The main use of this class is to allow the user to pass in different user configurations as far as log in or
    other data. \n
    On class __init__ invoke: \n
    1. Opens the .json file for file read access. \n
    2. Loads the .json file into a recognizable Python 'object' giving us access to the contained key value pairs. \n
    3. Initializes test suite variables to None for information storing. \n
    4. Gets user login and other BaseManager needed items from the file. \n
    5. Turns on/off logging based on value read in from file with key-value 'logging'. \n
    6. Lastly, loads user specific Selenium Web Driver Paths in from file.
    :param user_file: A .json formatted file populated with current user specific test suite configurations. \n
    """
    firefox_supported_platforms = ['Windows', 'Darwin', 'Linux']
    chrome_supported_platforms = ['Windows', 'Darwin']
    ie_supported_platforms = ['Windows']
    browser_dictionary = {'chrome': chrome_supported_platforms, 'firefox': firefox_supported_platforms,
                          'ie': ie_supported_platforms}

    def __init__(self, user_file):
        # import os
        # print os.getcwd()
        # Open user credential file (.json) for access
        opened_user_file = open(user_file)

        # Converts the .json file into a Python readable 'Object' so we can access it's information.
        loaded_user_json_file = json.load(opened_user_file)

        # Initiate non .json dependent variables to None (These variables are manipulated by the test scripts on a per
        # test basis setting these values as needed.)
        self.name_of_browser_under_test = None
        self.browser_view = None
        self.web_driver = None
        self.mac_address = None
        self.socket_port = None
        self.eventListener = None

        # This variable is set in test_base_browser_common to true, that way all sleeps are ignored for
        # faster testing response
        self.unit_testing = False

        # Get developer specific information per user configuration file
        self.user_name = loaded_user_json_file['username']
        self.user_password = loaded_user_json_file['password']
        self.url = loaded_user_json_file['url']

        # In Selenium Webdriver standards, a valid url must start with 'http' or 'https', so check if substring [
        # http] exists in the url, if not the url is considered invalid so add 'https://' before the url.
        # i.e.,
        #   webdriver.get("google.com") throws a selenium webdriver exception (invalid url).
        #   webdriver.get("http://google.com") is considered valid and opens correctly.
        if "http" not in self.url:
            new_url = "https://" + str(self.url)
            self.url = new_url

        self.site_name = loaded_user_json_file['site_name']
        self.company = loaded_user_json_file['company']

        # Attempts to get logging information, if no logging entry in .json config file, system defaults logging to
        # the "on".
        try:
            self.debug = loaded_user_json_file['logging']
        except Exception:
            self.debug = "on"

        # Get controller information from the user's .json credential file
        self.mac_address_for_1000 = loaded_user_json_file['controller_1000']['mac_address']
        self.port_address_for_1000 = loaded_user_json_file['controller_1000']['port_address']
        self.socket_port_for_1000 = loaded_user_json_file['controller_1000']['socket_port']

        self.mac_address_for_3200 = loaded_user_json_file['controller_3200']['mac_address']
        self.port_address_for_3200 = loaded_user_json_file['controller_3200']['port_address']
        self.socket_port_for_3200 = loaded_user_json_file['controller_3200']['socket_port']

        self.mac_address_for_substation = {}
        self.port_address_for_substation = {}
        self.socket_port_for_substation = {}
        self.ip_address_for_substation = {}

        try:
            for index, substation_json in enumerate(loaded_user_json_file['substations']):
                # Set address to `index + 1` because index intially starts as 0, so to make substation's address start
                # from 1 to 8, start `address` as `index + 1`.
                address = index + 1
                self.mac_address_for_substation[address] = substation_json['mac_address']
                self.port_address_for_substation[address] = substation_json['port_address']
                self.socket_port_for_substation[address] = substation_json['socket_port']
                self.ip_address_for_substation[address] = substation_json['ip_address']
        except KeyError:
            e_msg = "\nUser credentials file is missing substations area. Insert (COPY/PASTE) the following line in " \
                    "your user configuration file below `\"controller_3200\":` and try again.\n" \
                    "COPY/PASTE: -->  \"substations\":[],  <-- \n\n" \
                    "If you are trying to run substation use cases, please refer to `user_credentials_ben.json` file " \
                    "to use as an example template for substation configuration."
            raise Exception(e_msg)

        # Get base unit configuration stuff
        try:
            self.bu_comport = loaded_user_json_file['base_unit']['comport']
        except KeyError:
            pass

        # Get browsers for testing, .json returns the "list" of browsers in unicode format, which shouldn't affect
        # the string comparisons.
        self.browsers_for_testing = loaded_user_json_file['browsers']

        # --------
        # Start fail fast for browser support vs current os system.
        supported_browsers = {
            "Windows": ["chrome", "firefox", "ie"],
            "Darwin": ["chrome", "firefox"],
            "Linux": ["chrome", "firefox"]
        }
        list_of_supported_browsers = supported_browsers[platform.system()]

        for browser_to_use in self.browsers_for_testing:
            if browser_to_use not in list_of_supported_browsers:
                e_msg = "Browser to use for testing: {0} is not currently supported for your operating system: {1}. " \
                        "Supported browsers for your operating system are: {2}".format(
                            browser_to_use,
                            platform.system(),
                            list_of_supported_browsers
                        )
                raise ValueError(e_msg)

        # PATH VARIABLES
        self.windows_chrome_webdriver_exe_path = loaded_user_json_file['windows']['chrome']
        self.windows_ie_webdriver_exe_path = loaded_user_json_file['windows']['ie']
        self.mac_chrome_webdriver_exe_path = loaded_user_json_file['mac']['chrome']
        self.mac_ie_webdriver_exe_path = loaded_user_json_file['mac']['ie']
        self.linux_chrome_webdriver_exe_path = loaded_user_json_file['linux']['chrome']
        self.linux_ie_webdriver_exe_path = loaded_user_json_file['linux']['ie']

        try:
            self.bacnet_server_ip = loaded_user_json_file['bacnet']
        except Exception:
            self.bacnet_server_ip = "10.11.12.27"


        opened_user_file.close()

if __name__ == "__main__":
    tige = UserConfiguration(os.path.join('user_credentials', 'user_credentials_ben.json'))
