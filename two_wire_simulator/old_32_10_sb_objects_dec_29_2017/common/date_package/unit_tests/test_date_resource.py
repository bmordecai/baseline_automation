__author__ = 'Ben'

import unittest
import mock
import datetime
from dateutil.parser import parse
from old_32_10_sb_objects_dec_29_2017.common.date_package import date_resource


class TestDateResourceObject(unittest.TestCase):
    """
    Unit test class for date_package.date_resource.py
    """

    ########################################
    def setUp(self):
        """ Setting up for the test. """
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + self.shortDescription())

    ########################################
    def tearDown(self):
        """ Cleaning up after the test. """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_date_object_set_from_datetime_obj_pass1(self):
        """ Verifies valid attribute values set """
        datetime_obj = parse("2015-10-06").date()
        date_obj = date_resource.Date()
        date_obj.set_from_datetime_obj(datetime_obj=datetime_obj)
        self.assertEqual(date_obj.weekDayIndex, datetime_obj.weekday())
        self.assertEqual(date_obj.obj, datetime_obj)
        self.assertEqual(date_obj.day, 6)
        self.assertEqual(date_obj.year, 2015)
        self.assertEqual(date_obj.weekDayKey, date_resource.tuesday_key)
        self.assertEqual(date_obj.weekDayName, "Tuesday")
        self.assertEqual(date_obj.monthKey, "OCT")
        self.assertEqual(date_obj.monthName, "October")

    def test_date_mngr_init_pass(self):
        """ Verifies valid init of date_mngr object instance """
        date_mngr = date_resource.DateManager()
        self.assertIsInstance(date_mngr.curr_day, date_resource.Date)
        self.assertIsInstance(date_mngr.next_day, date_resource.Date)
        self.assertIsInstance(date_mngr.prev_day, date_resource.Date)

    def test_date_mngr_set_dates_from_date_range_pass1(self):
        """ Verifies a valid date range contained within date_mngr for dates passed in """
        sdate = "10-06-2015"
        edate = "10-13-2015"
        sdate_datetime = parse(sdate).date()
        edate_datetime = parse(edate).date()
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(sdate_datetime, date_mngr.begin_date)
        self.assertEqual(edate_datetime, date_mngr.end_date)

    def test_date_mngr_set_dates_from_date_range_pass2(self):
        """ Verifies a valid date range contained within date_mngr for dates passed in """
        sdate = "02-01-2015"
        edate = "02-28-2015"
        sdate_datetime = parse(sdate).date()
        edate_datetime = parse(edate).date()
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(sdate_datetime, date_mngr.begin_date)
        self.assertEqual(edate_datetime, date_mngr.end_date)

    def test_add_new_week_to_front_for_intial_prev_day_pass1(self):
        """ Verifies the parser adds a week at the beginning of the date range if the start date falls on a monday """
        sdate = "06-01-2015"
        edate = "06-30-2015"
        sdate_datetime = parse(sdate).date()
        edate_datetime = parse(edate).date()
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(first=date_mngr.prev_day.day, second=31)
        self.assertEqual(first=date_mngr.prev_day.weekDayKey, second=date_resource.sunday_key)

    def test_add_addtnl_week_for_final_next_day_pass1(self):
        """ Verifies the parser adds a final week onto the range of days if the month ends on a sunday """
        sdate = "05-01-2015"
        edate = "05-31-2015"
        sdate_datetime = parse(sdate).date()
        edate_datetime = parse(edate).date()
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)

        while date_mngr.reached_last_date() != True:
            date_mngr.skip_to_next_day()

        # a passing test must mean that as our "current day", the "next day" should be the 1st of the next month
        self.assertEqual(first=date_mngr.next_day.day, second=1)
        self.assertEqual(first=date_mngr.next_day.weekDayKey, second=date_resource.monday_key)


    def test_date_mngr_skip_to_next_day_pass1(self):
        """ Verifies a valid iteration to the next day from the current day """
        sdate = "10-06-2015"
        cdate = "10-07-2015"
        ndate = "10-08-2015"
        edate = "10-13-2015"
        sdate_datetime = parse(sdate).date()
        cdate_datetime = parse(cdate).date()
        ndate_datetime = parse(ndate).date()
        edate_datetime = parse(edate).date()

        prev_date = date_resource.Date()
        prev_date.set_from_datetime_obj(sdate_datetime)

        expected_curr_date = date_resource.Date()
        expected_curr_date.set_from_datetime_obj(cdate_datetime)

        expected_next_date = date_resource.Date()
        expected_next_date.set_from_datetime_obj(ndate_datetime)

        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(sdate_datetime, date_mngr.curr_day.obj)

        date_mngr.skip_to_next_day()
        self.assertEqual(prev_date.obj, date_mngr.prev_day.obj)
        self.assertEqual(expected_curr_date.obj, date_mngr.curr_day.obj)
        self.assertEqual(expected_next_date.obj, date_mngr.next_day.obj)

    def test_date_mngr_skip_to_next_day_pass2(self):
        """ Verifies a valid next day is set if trying to move current day from saturday to sunday which changes
        current week index """
        sdate = "10-10-2015"
        cdate = "10-11-2015"
        ndate = "10-12-2015"
        edate = "10-14-2015"
        sdate_datetime = parse(sdate).date()
        cdate_datetime = parse(cdate).date()
        ndate_datetime = parse(ndate).date()
        edate_datetime = parse(edate).date()

        prev_date = date_resource.Date()
        prev_date.set_from_datetime_obj(sdate_datetime)

        curr_date = date_resource.Date()
        curr_date.set_from_datetime_obj(cdate_datetime)

        next_date = date_resource.Date()
        next_date.set_from_datetime_obj(ndate_datetime)

        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(sdate_datetime, date_mngr.curr_day.obj)

        date_mngr.skip_to_next_day()
        self.assertEqual(prev_date.obj, date_mngr.prev_day.obj)
        self.assertEqual(curr_date.obj, date_mngr.curr_day.obj)
        self.assertEqual(next_date.obj, date_mngr.next_day.obj)
        self.assertEqual(edate_datetime, date_mngr.end_date)
        self.assertEqual(1, date_mngr.get_current_day_index())
        date_mngr.skip_to_next_day()
        date_mngr.skip_to_next_day()
        date_mngr.skip_to_next_day()
        self.assertEqual(4, date_mngr.get_current_day_index())
        self.assertTrue(date_mngr.reached_last_date())

    def test_date_mngr_skip_to_prev_day_pass1(self):
        """ Verifies a valid iteration to the previous day from the current day """
        sdate = "10-06-2015"
        cdate = "10-07-2015"
        ndate = "10-08-2015"
        edate = "10-15-2015"

        sdate_datetime = parse(sdate).date()
        cdate_datetime = parse(cdate).date()
        ndate_datetime = parse(ndate).date()
        edate_datetime = parse(edate).date()

        prev_date = date_resource.Date()
        prev_date.set_from_datetime_obj(sdate_datetime)

        curr_date = date_resource.Date()
        curr_date.set_from_datetime_obj(cdate_datetime)

        next_date = date_resource.Date()
        next_date.set_from_datetime_obj(ndate_datetime)

        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(sdate_datetime, date_mngr.curr_day.obj)

        date_mngr.skip_to_next_day()
        date_mngr.skip_to_next_day()

        date_mngr.skip_to_prev_day()
        self.assertEqual(prev_date.obj, date_mngr.prev_day.obj)
        self.assertEqual(curr_date.obj, date_mngr.curr_day.obj)
        self.assertEqual(next_date.obj, date_mngr.next_day.obj)
        self.assertEqual(edate_datetime, date_mngr.end_date)

    def test_date_mngr_contains_date_pass(self):
        """ Verifies a true is returned when looking for a date in the current range """
        sdate = "10-06-2015"
        edate = "10-15-2015"

        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)

        self.assertTrue(date_mngr.contains_date(date="10-10-2015"))


