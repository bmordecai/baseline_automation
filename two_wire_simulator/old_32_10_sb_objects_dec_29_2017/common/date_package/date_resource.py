from dateutil.parser import parse
import calendar
# import datetime
from datetime import timedelta, datetime, date, time

sunday_key      = "SUN"
sunday_str      = "Sunday"
monday_key      = "MON"
monday_str      = "Monday"
tuesday_key     = "TUE"
tuesday_str     = "Tuesday"
wednesday_key   = "WED"
wednesday_str   = "Wednesday"
thursday_key    = "THU"
thursday_str    = "Thursday"
friday_key      = "FRI"
friday_str      = "Friday"
saturday_key    = "SAT"
saturday_str    = "Saturday"

weekday_dict = {
    0: (sunday_key, sunday_str),
    1: (monday_key, monday_str),
    2: (tuesday_key, tuesday_str),
    3: (wednesday_key, wednesday_str),
    4: (thursday_key, thursday_str),
    5: (friday_key, friday_str),
    6: (saturday_key, saturday_str)
}


class Time(object):
    """
    Time object used in DateManager
    """

    ########################################
    def __init__(self):
        # time = date_mngr.curr_computer_time of the computer set by the date manger when it asked the computer for
        # its time
        self.obj = None
        """:type: datetime.time"""

        self.hour = 0
        self.minute = 0
        self.second = 0
        self.microsecond = 0

    ########################################
    def update_from_time_string(self, time_string):
        self.obj = datetime.strptime(time_string, '%H:%M:%S').time()
        self.hour = self.obj.hour
        self.minute = self.obj.minute
        self.second = self.obj.second
        self.microsecond = self.obj.microsecond

    def string_with_format(self, format_string):
        """
        Returns a string representation of this date in the specified format. \n

        Example formats: \n
        -   format_string="%H:%M:%S"
        -   format_string="%H:%M:%S:%f"

        :param format_string:   Time format to use
        :type format_string:    str
        :return:                Time in specified format as string
        :rtype:                 str
        """
        return self.obj.strftime(format_string)


class Date(object):
    """
    Date object used in DateManager
    """
    # TODO consider making a merge date and time method so we can add time in a more readable way
    ########################################
    def __init__(self):
        # day = date_mngr.curr_day.weekDayName this will return the name of the week
        self.obj = None
        """:type: datetime.date"""
        self.time_obj = Time()
        """:type: Time"""

        # Date attributes
        self.day = 0
        self.year = 0
        self.weekDayName = ""
        self.weekDayKey = ""
        self.weekDayIndex = 0
        self.monthName = ""
        self.monthKey = ""
        self.day_index_in_range = 0

    ########################################
    def set_from_datetime_obj(self, datetime_obj, time_string=None, is_starting_day=False):
        """
        Sets current 'Date' and 'Time' object to the contents of the datetime_obj passed in.
        :param datetime_obj:    A datetime instance to be wrapped
        :type datetime_obj:     datetime.date
        :param time_string:    A time string to be converted into an object
        :type time_string:     str
        """
        self.obj = datetime_obj
        self.day = datetime_obj.day
        self.year = datetime_obj.year
        self.weekDayIndex = datetime_obj.weekday()      # returns a value 0-6 (0=Monday - 6=Sunday)

        # %A says to grab the whole weekday name
        # ref: https://docs.python.org/2/library/datetime.html#strftime-and-strptime-behavior
        self.weekDayName = datetime_obj.strftime("%A")
        self.weekDayKey = datetime_obj.strftime("%a").upper()
        self.monthName = datetime_obj.strftime("%B")
        self.monthKey = datetime_obj.strftime("%b").upper()

        # flagged from date_mngr.set_date_from....
        if is_starting_day:
            self.day_index_in_range = 1

        if time_string is not None:
            try:
                self.time_obj.update_from_time_string(time_string=time_string)
            except ValueError:
                raise ValueError("Incorrect time format passed into the date object, should be HH:MM:SS")

    def date_string_for_controller(self):
        """
        Return's the date string representation accepted by the controller for setting its date
        example
        :return:    Date in format recognized by the controller
        :rtype:     str
        """
        return self.obj.strftime('%m/%d/%Y')

    def msg_date_string_for_controller_1000(self):
        """
        Return's the date string representation accepted by the 1000 controller for setting its date
        :return:    Date in format recognized by the controller
        :rtype:     str
        """
        # The replace methods allow us to remove any leading zeros in the output for day and month
        return self.obj.strftime('X%m/X%d/%y').replace('X0', 'X').replace('X', '')

    def msg_date_string_for_controller_3200(self):
        """
        Return's the date string representation accepted by the 3200 controller for setting its date
        :return:    Date in format recognized by the controller
        :rtype:     str
        """
        return self.obj.strftime('%m/%d/%y')

    def formatted_date_string(self, format_string):
        """
        Returns a string representation of this date in the specified format. \n

        Example formats: \n
        -   format_string="%m/%d/%Y"
        -   format_string="%m-%d-%Y"
        -   format_string="%Y-%m-%d"
        -   format_string="%Y%m%d"

        :param format_string:   Date format to use
        :type format_string:    str
        :return:                Date in specified format as string
        :rtype:                 str
        """
        return self.obj.strftime(format_string)

    def time_string_for_controller(self):
        """
        Return's the time string representation accepted by the controller for setting its time
        example : 00:00:00 this is in military time
        :return:    Date in format recognized by the controller
        :rtype:     str
        """
        return self.time_obj.string_with_format('%H:%M:%S')

    def msg_time_string_for_controller_1000(self):
        """
        Return's the time string representation accepted by the 1000 controller for setting its time
        :return:    Date in format recognized by the controller
        :rtype:     str
        """
        # return self.obj.strftime('%H:%M:%S')

        # This weill return a time that isn't zero padded
        return self.time_obj.string_with_format('X%H:%M:%S').replace('X0', 'X').replace('X', '')

    def msg_time_string_for_controller_3200(self):
        """
        Return's the time string representation accepted by the 3200 controller for setting its time
        :return:    Date in format recognized by the controller
        :rtype:     str
        """
        return self.time_obj.string_with_format('%H:%M:%S')

    def formatted_time_string(self, format_string):
        """
        Returns a string representation of this date in the specified format. \n

        Example formats: \n
        -   format_string="%H:%M:%S"
        -   format_string="%H:%M:%S:%f"

        :param format_string:   Time format to use
        :type format_string:    str
        :return:                Time in specified format as string
        :rtype:                 str
        """
        return self.time_obj.string_with_format(format_string)

    def increment_date_time(self, hours=0, minutes=0, seconds=0):
        """
        Returns a string representation of this date in the specified format. \n

        :param hours:       Time format to use
        :type hours:        str
        :param minutes:     Time format to use
        :type minutes:      str
        :param seconds:     Time format to use
        :type seconds:      str
        """
        # We need to create a datetime object before we can add timedelta
        datetime_to_add = datetime.combine(self.obj, self.time_obj.obj)
        # Add the time that was passed in
        datetime_to_add += timedelta(seconds=int(seconds), minutes=int(minutes), hours=int(hours))
        # This takes the datetime object stored within this object and puts it through the method to update all the
        # variables of this object
        date = datetime_to_add.date()
        time_string = datetime_to_add.strftime('%H:%M:%S')
        self.set_from_datetime_obj(datetime_obj=date, time_string=time_string)

    def formatted_date_time_string(self, date_format_string, time_format_string):
        """
        Returns a string representation of the date and time in a single formatted string.
        
        i.e., "08/01/2017 01:15:00"
        
        :param date_format_string: Date format to return.
        :type date_format_string: str
        
        :param time_format_string: Time format to return.
        :type time_format_string: str
        
        :return: 
        :rtype: str
        """
        return self.formatted_date_string(format_string=date_format_string) + \
            " " + self.formatted_time_string(format_string=time_format_string)


class DateManager(object):
    # ToDo: def rangeToString() => "Date range of test: " + begin_date + " - " + end_date
    # ToDo: def dateToString() => "Date: " + date_mngr.curr_day.string_for_controller()

    ########################################
    def __init__(self):
        self.range_of_dates = []
        """:type: list[list[datetime.date]]"""      # force range_of_dates to be a list of lists with datetime.date elements

        self.curr_day = Date()
        """:type: Date"""     # force curr_day to be of type: Date -> for IDE auto complete

        self.prev_day = Date()
        """:type: Date"""     # force prev_day to be of type: Date -> for IDE auto complete

        self.next_day = Date()
        """:type: Date"""     # force next_day to be of type: Date -> for IDE auto complete

        self.curr_week_index = 0

        self.begin_date = None
        """:type: datetime.date"""

        self.end_date = None
        """:type: datetime.date"""

        self.curr_computer_date = Date()
        """:type: Date"""

        self.controller_datetime = Date()
        """:type: Date"""

    def set_date_to_default(self):
        # Resets all variables to their initial values
        self.__init__()

    def set_current_date_to_match_computer(self):
        # Grabs the date and time currently set on the controller
        current_computer_datetime = datetime.now()
        # Assigns the attributes to a variable
        self.curr_computer_date.set_from_datetime_obj(datetime_obj=current_computer_datetime.date(),
                                                      time_string=current_computer_datetime.strftime('%H:%M:%S'))
        self.curr_day.set_from_datetime_obj(datetime_obj=current_computer_datetime.date(),
                                            time_string=current_computer_datetime.strftime('%H:%M:%S'))

        # self.set_dates_from_date_range(start_date=str(self.curr_computer_date), end_date=str(self.curr_computer_date))

    def set_controller_datetime(self, date, time):
        """
        
        :param date: 
        :param time: 
        :return: 
        """
        self.controller_datetime.set_from_datetime_obj(datetime_obj=self.parse_date_input(date=date), time_string=time)

    def reached_last_date(self):
        return self.curr_day.obj == self.end_date

    def contains_date(self, date):
        """
        Returns whether the given date as string is in the current date range
        :param date:    A date as a string in the form: '2015-10-31' or '10/14/2015'
        :type date:     str
        :return:        If the date is in the current range
        :rtype:         bool
        """
        found = False
        date_in = parse(date).date()
        for each_week in self.range_of_dates:
            if date_in in each_week:
                found = True
                break
        return found

    def get_current_day_index(self):
        """
        Returns the day number in the range.
        this doesnt appear to be used
        # :param date:    A date as a string in the form: '2015-10-31' or '10/14/2015'
        # :type date:     str
        :return:        If the date is in the current range
        :rtype:         bool
        """
        found_begin_date = False
        day_index = 0
        date_in = self.curr_day.obj

        for each_week in self.range_of_dates:

            # go through each day in the week
            for each_day in each_week:

                # day we are looking for?
                if each_day == self.begin_date:

                    found_begin_date = True

                    if each_day == date_in:
                        return day_index
                    else:
                        day_index += 1

                elif each_day == date_in:
                    return day_index

                else:
                    if found_begin_date:
                        # nope
                        day_index += 1

        return None

    def start_date_is_a_monday(self):
        """
        Return if the start date is a Sunday
        :return:  Return true if the start date is sunday
        :rtype:  bool
        """
        # start_date.strftime("%a") returns "SUN" or "MON" ect.
        # this says return if "SUN" == "SUN" (TRUE) or "MON" == "SUN" (FALSE) ect.
        return self.begin_date.strftime("%a").upper() == monday_key.upper()

    def end_date_is_a_sunday(self):
        """
        Return if the end date is a Sunday
        :return:  Return true if the end date is sunday
        :rtype:  bool
        """
        # end_date.strftime("%a") returns "SUN" or "MON" ect.
        # this says return if "SUN" == "SUN" (TRUE) or "MON" == "SUN" (FALSE) ect.
        return self.end_date.strftime("%a").upper() == sunday_key.upper()

    def add_addtnl_week_for_final_next_day(self):
        """
        FOo
        """
        # add one to get the next month
        # % 12 to stay within range
        curr_month_int = self.end_date.month
        curr_year_int = self.end_date.year

        # if we are december, increment the year to the next year
        if curr_month_int == 12:
            curr_year_int += 1

        temp_new_month_int = (curr_month_int + 1) % 12
        temp_month_list = calendar.Calendar().monthdatescalendar(year=curr_year_int, month=temp_new_month_int)
        week_to_append = temp_month_list[0]
        self.range_of_dates.append(week_to_append)

    def add_new_week_to_front_for_intial_prev_day(self):
        """
        Inserts an additional week at the beginning of the date range for a valid initial previous day
        """
        curr_month_int = self.begin_date.month
        curr_year_int = self.begin_date.year

        # if curr_month is january, subtract year to previous year
        if curr_month_int == 1:
            curr_month_int = 13     # set to 13 to force (curr_month_int-1)=12 helping go from JAN (1) to DEC (12)
            curr_year_int -= 1

        temp_new_month_int = (curr_month_int - 1) % 12
        if temp_new_month_int == 0:
            temp_new_month_int = 12
        temp_month_list = calendar.Calendar().monthdatescalendar(year=curr_year_int, month=temp_new_month_int)
        length = len(temp_month_list)
        week_to_insert = temp_month_list[length-1] # get last week from this month to insert at the front of our range

        # insert new week at front of date range
        self.range_of_dates.insert(0, week_to_insert)

    ########################################
    def set_dates_from_date_range(self, start_date, end_date):
        """
        Returns a list of 'datetime.date' objects within the range specified.

        1.  First attempts to get dates within the specified date range. These dates are returned in a list, with each
            list index being a week worth of "date" or "day" objects (so each index contains a sub list of 7 values).

            * NOTE: These "weeks" start on monday (index 0) and end on sunday (index 6) compared to normal calendar with
                    the week starting on SUNDAY and ending on SATURDAY

            SPECIAL CASES DURING RANGE GATHERING
            1.1: If the start date falls on a MON, it is necessary to insert an additional week at the front of the range
                 which includes the "only" "previous" day needed for initial eto calculations

            1.2: If the end date falls on a SUN, it is necessary to append a final week at the end of the range to maintain
                 consitency of having a "final" next day just like having an "initial" prev day listed above.

        2.


        :param start_date:  Starting date of range
        :type start_date:   str
        :param end_date:    Ending date of range (inclusive)
        :type end_date:     str
        :return:
        :rtype:
        """
        # flags
        added_last_week = False
        found_start = False

        self.begin_date = self.parse_date_input(date=start_date)
        """:type: datetime.date"""
        self.end_date = self.parse_date_input(date=end_date)
        """:type: datetime.date"""

        # This returns a list, with a list for each week in the indicated month
        # If the month ends on anything other than sunday, the first week for 
        # the next month is included in the returned weeks. Conversely, if the 
        # month starts on anything other than monday, the week including partial 
        # days from the previous month is included.
        temp_start_range = calendar.Calendar().monthdatescalendar(year=self.begin_date.year, month=self.begin_date.month)
        for each_week in temp_start_range:

            # if we have already iterated through the list and found the starting 
            # week, then append the rest of the weeks as long as we haven't come 
            # across the end date
            if found_start and not added_last_week:
                self.range_of_dates.append(each_week)

                # if our end date is contained within the last week we just added, flagging this to true allows for the
                # next for loop to skip trying to add more weeks until the last week is added
                if self.end_date in each_week:
                    added_last_week = True
                    break
            elif found_start and added_last_week:
                break

            if self.begin_date in each_week:
                self.range_of_dates.append(each_week)
                found_start = True

                # need to check for end_date being in the same week as the start date
                if self.end_date in each_week:
                    added_last_week = True

        # if we haven't added the last week to the range yet, then get the end date month calendar and add weeks until
        # the last week for the date range has been added
        if not added_last_week:

            # this returns a list containing a list for each week for the month including 'end' date
            temp_end_range = calendar.Calendar().monthdatescalendar(year=self.end_date.year, month=self.end_date.month)

            for each_week in temp_end_range:

                # add week to current date range
                if each_week not in self.range_of_dates and not added_last_week:
                    self.range_of_dates.append(each_week)

                # break out of for loop
                if self.end_date in each_week:
                    added_last_week = True
                    break

        if found_start and added_last_week:

            # set the current day pointer
            self.curr_day.set_from_datetime_obj(self.begin_date, is_starting_day=True)

            if self.start_date_is_a_monday():
                self.add_new_week_to_front_for_intial_prev_day()
                self.curr_week_index = 1  # need to force curr_week_index to 1 since we just added a week at the front

            if self.end_date_is_a_sunday():
                self.add_addtnl_week_for_final_next_day()

            #
            #
            # # if the current day is on a Sunday, go ahead and increment curr_week_index to the next week
            # if self.begin_date.weekday() == 6:
            #     self.curr_week_index += 1

            # get weekday index, this ensures that we stay within range of 0-6 for list indexing, 6 is the last valid
            # index
            temp_next_week_index = self.curr_week_index
            next_weekday_index = (self.begin_date.weekday()+1) % 7
            if self.curr_day.weekDayKey == sunday_key:
                temp_next_week_index += 1
            # set the next day pointer, we can assume the first week in the range of dates includes our start date
            self.next_day.set_from_datetime_obj(self.range_of_dates[temp_next_week_index][next_weekday_index])

            temp_prev_week_index = self.curr_week_index
            prev_weekday_index = (self.begin_date.weekday()-1)%7
            # if prev_weekday_index == 0:
            #     prev_weekday_index = 6

            if self.curr_day.weekDayKey == monday_key:
                temp_prev_week_index -= 1
            self.prev_day.set_from_datetime_obj(self.range_of_dates[temp_prev_week_index][prev_weekday_index])

        else:
            # TODO: Could raise error here too instead
            pass

    ########################################
    def parse_date_input(self, date):
        """
        Parses the input into a 'datetime.date' object. This allows us to see if this object is in the list of
        'datetime.date' objects returned when we use the calendar date range.
        :param date:    Date to be parsed
        :type date:     str
        :return:        datetime.date object
        :rtype:         datetime.date
        """
        try:
            # 'parse' returns a date in 'datetime.datetime' format, but calendar returns 'datetime.date' objects
            parsed_date = parse(date)
        except Exception as e:
            e_msg = "Invalid date format entered into date parser in date_resource.py: {invalid_date}. Caught " \
                    "exception: {e}".format(
                        invalid_date=date,
                        e=e.message
                    )
            raise Exception(e_msg)
        else:
            # .date() causes return value to be of type: 'datetime.date'
            return parsed_date.date()

    ########################################
    def skip_to_next_day(self):
        """
        Moves the curr_day, next_day and prev_day pointers respectively
        """

        # If we are at the last day of the range, set curr and next day to None, preserving the prev_day pointer which
        # ends up being the last day in the range
        if self.curr_day.obj == self.end_date:

            # set next_day to None to give us a value to look for outside of this module (if we are looping through the
            # list or anything of that nature to check for a None)
            self.next_day = None
            return

        # Case: If current day is last day of the week, aka Sunday with a weekday index value of 6
        if self.next_day.weekDayIndex == 6:

            self.prev_day.set_from_datetime_obj(self.curr_day.obj)
            self.curr_day.set_from_datetime_obj(self.next_day.obj)

            # increment day index
            self.curr_day.day_index_in_range += 1

            # since curr_day is sunday, increment to the next week
            self.curr_week_index += 1
            self.next_day.set_from_datetime_obj(self.range_of_dates[self.curr_week_index][0])

        else:
            self.prev_day.set_from_datetime_obj(self.curr_day.obj)
            self.curr_day.set_from_datetime_obj(self.next_day.obj)
            self.next_day.set_from_datetime_obj(
                datetime_obj=self.range_of_dates[self.curr_week_index][(self.next_day.weekDayIndex + 1)])

    # No current use case, commented out for less clutter
    #
    def skip_to_prev_day(self):
        """
        Moves the curr_day, next_day and prev_day pointers back one day
        """
        # If we are at the first day of the range, set curr and prev day to None, preserving the next_day pointer which
        # ends up being the last day in the range
        if self.curr_day.obj == self.begin_date:

            # set prev_day to None to give us a value to look for outside of this module (if we are looping through the
            # list or anything of that nature to check for a None)
            self.prev_day = None
            return

        if self.prev_day.weekDayIndex == 0:

            if self.curr_week_index == 0:
                e_msg = "DateMngr unable to move back one day. At the start of the range: {date}".format(
                            date=self.curr_day.formatted_date_string(format_string="%m-%d-%Y")
                        )
                raise IndexError(e_msg)

            else:
                self.curr_week_index -= 1
                self.next_day.set_from_datetime_obj(self.curr_day.obj)
                self.curr_day.set_from_datetime_obj(self.prev_day.obj)
                self.prev_day.set_from_datetime_obj(datetime_obj=self.range_of_dates[self.curr_week_index][6])

        else:
            self.next_day.set_from_datetime_obj(self.curr_day.obj)
            self.curr_day.set_from_datetime_obj(self.prev_day.obj)
            self.prev_day.set_from_datetime_obj(
                datetime_obj=self.range_of_dates[self.curr_week_index][(self.prev_day.weekDayIndex - 1)])

# GLOBAL Declaration: init date_mngr
date_mngr = DateManager()

