import logging


class LoggingHandler(object):
    """
    This class handles the logging functionality that we use in product assessments and our use cases

    There is a tightly coupled relationship with our logging and the continuous running of our use cases

    To use logging functionality in any class (like a use case) simply put this line at the top:
        from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

    You can then access the methods of this class by using the 'log_handler' variable.
    To actually log a message, it will look something like this in the use case: log_handler.debug('example message')
    """
    # The file name that our logger will output to
    LOG_FILENAME = "run_tests_log.txt"
    logger = logging

    # Boolean value stating if we should be logging, will also allow our use cases to continue running the next use case
    # even if a use cases bombs out with an exception.
    enabled = False

    # Boolean value stating if we should clear the contents of the log file before we run
    overwrite = True

    ########################################
    def __init__(self, filename=LOG_FILENAME, level=logging.DEBUG):
        # Clears the contents of the log file
        if LoggingHandler.overwrite:
            open(self.LOG_FILENAME, 'w').close()

        # Creates the logging
        logging.basicConfig(filename=filename, level=level)

    ########################################
    @staticmethod
    def overwrite_log_file(overwrite):
        LoggingHandler.overwrite = overwrite

    ########################################
    @staticmethod
    def enable_continuous_run_and_logging_for_use_cases(enabled):
        """
        This method controls if we will be logging in our use cases. If we do enable logging, then instead of crashing
        on errors, we instead log the error and continue running. \n

        :param enabled: True or False value that determines if we will log in our use cases \n
        :type enabled: bool
        """
        LoggingHandler.enabled = enabled

    ########################################
    @staticmethod
    def is_enabled():
        return LoggingHandler.enabled

    ########################################
    def debug(self, message):
        """
        Logs a message at the 'DEBUG' level if logging is enabled
        """
        if LoggingHandler.is_enabled():
            self.logger.debug(message)

    ########################################
    def info(self, message):
        """
        Logs a message at the 'INFO' level if logging is enabled
        """
        if LoggingHandler.is_enabled():
            self.logger.info(message)

    ########################################
    def warning(self, message):
        """
        Logs a message at the 'WARNING' level if logging is enabled
        """
        if LoggingHandler.is_enabled():
            self.logger.warning(message)

    ########################################
    def error(self, message):
        """
        Logs a message at the 'ERROR' level if logging is enabled
        """
        if LoggingHandler.is_enabled():
            self.logger.error(message)

    ########################################
    def critical(self, message):
        """
        Logs a message at the 'CRITICAL' level if logging is enabled
        """
        if LoggingHandler.is_enabled():
            self.logger.critical(message)

    ########################################
    def exception(self, message):
        """
        Logs a message at the 'CRITICAL' level if logging is enabled
        """
        if LoggingHandler.is_enabled():
            self.logger.exception(message)

# GLOBAL Declaration: This must be initialized in product assessments, and then it can be used anywhere with a simple
# import statement that will look like this: 'import old_32_10_sb_objects_dec_29_2017.common.logging_handler
log_handler = LoggingHandler()
