import os
import json

# Pass controller lat/lng to all Device objects
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
Devices.controller_lat = 43.609768
Devices.controller_long = -116.310569

from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.user_configuration import UserConfiguration

# Imports for setting Serial Port
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.poc import POC
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.programs import Programs
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.pg_start_stop_pause_cond import BaseStartStopPause
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.ser import Ser

from old_32_10_sb_objects_dec_29_2017.common.objects.substation.sb import Substation
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.valve_bicoder import ValveBicoder
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.flow_bicoder import FlowBicoder
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.pump_bicoder import PumpBicoder
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.switch_bicoder import SwitchBicoder
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.moisture_bicoder import MoistureBicoder
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.temp_bicoder import TempBicoder

from old_32_10_sb_objects_dec_29_2017.common.objects.controller.cn import Controller
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zn import Zone
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.mv import MasterValve
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ms import MoistureSensor
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.fm import FlowMeter
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.sw import EventSwitch
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ts import TemperatureSensor
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_1000 import POC1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.bm import BaseManagerConnection

# Serial and Web Driver Module
from old_32_10_sb_objects_dec_29_2017.common.resource_handler import ResourceHandler

# Get objects from bucket
from old_32_10_sb_objects_dec_29_2017.common.objects import object_bucket
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import controllers, substations, zones, moisture_sensors, master_valves
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import temperature_sensors, flow_meters, event_switches
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import mainlines32, zone_programs, poc10, poc32, programs10, programs32
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import basemanager_connection
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import program_start_conditions32, program_stop_conditions32, program_pause_conditions32
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import program_start_conditions10, program_stop_conditions10, program_pause_conditions10
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import alert_relays



__author__ = 'bens'


class Configuration(object):
    """
    :type resource_handler: common.resource_handler.ResourceHandler
    :type d1: list[str]
    :type sb_d1: dict[int, list[str]]
    :type sb_d2: dict[int, list[str]]
    :type sb_d4: dict[int, list[str]]
    :type sb_dd: dict[int, list[str]]
    :type sb_fm: dict[int, list[str]]
    :type sb_ms: dict[int, list[str]]
    :type sb_ts: dict[int, list[str]]
    :type sb_sw: dict[int, list[str]]
    :type sb_pu: dict[int, list[str]]
    """

    def __init__(self, cn_type, test_name, user_conf_file, data_json_file, cn_serial_number=None, cn_fw_version=None,
                 sb_fw_version=None, number_of_substations_to_use=0):
        """
        Configuration initializer.

        :param cn_type:             Type of controller \n
        :type cn_type:              str

        :param cn_fw_version:       Controller's firmware version \n
        :type cn_fw_version:        str

        :param cn_serial_number:    Controller's serial number \n
        :type cn_serial_number:     str

        :param test_name:           Name of test to set as the controller's description. \n
        :type test_name:            str

        :param user_conf_file:      User configuration instance. \n
        :type user_conf_file:       UserConfiguration

        :param data_json_file:      Name of json file containing all data for test. \n
        :type data_json_file:       str
        
        :param sb_fw_version:       Firmware version for Substations.
        :type sb_fw_version:        str
        
        :param number_of_substations_to_use: Number of substations to initialize for Use case (if specified.)
        :type number_of_substations_to_use: int
        """
        self.resource_handler = None

        # Initialize common variables based on controller type
        if cn_type == "10":
            self.mac = user_conf_file.mac_address_for_1000
        else:
            self.mac = user_conf_file.mac_address_for_3200

        # Controller type and fw version
        self.cn_ty = cn_type
        self.cn_fw_version = cn_fw_version
        self.cn_serial_number = cn_serial_number

        self.sb_fw_version = sb_fw_version
        self.number_of_substations_to_use = number_of_substations_to_use

        # Set the test name as the controller's description (to name the controller to the same thing as the test)
        self.test_name = test_name

        # User configuration file
        self.user_conf = user_conf_file

        # JSON configuration file
        self.data_file = data_json_file

        # Files
        self.converted_file = ''

        # Placeholders for Device Serial Numbers loaded onto the controller which are read from the json file.
        self.d1 = []           # Single Valve Decoders
        self.mv_d1 = []        # Single Valve Decoder used as a master valve
        self.d2 = []           # Dual Valve Decoders
        self.mv_d2 = []        # Dual Valve Decoder used as a master valve
        self.d4 = []           # Quad Valve Decoders
        self.dd = []           # Twelve Valve Decoders
        self.mv = []           # Master Valve Decoders
        self.ms = []           # Moisture Sensor Serial Numbers
        self.ts = []           # Temperature Sensor Serial Numbers
        self.sw = []           # Event Switch Serial Numbers
        self.fm = []           # Flow Meter Serial Numbers

        # Shared decoder serial numbers between controller and all connected substations.
        self.shared_d1 = []
        self.shared_mv_d1 = []
        self.shared_d2 = []
        self.shared_mv_d2 = []
        self.shared_d4 = []
        self.shared_dd = []
        self.shared_ms = []
        self.shared_ts = []
        self.shared_fm = []
        self.shared_sw = []

        # Substation Placeholders dict[substation_address, list_of_serial_numbers]
        self.sb_d1 = dict()     # Single valve bicoder serial numbers
        self.sb_d2 = dict()     # Dual valve bicoder serial numbers
        self.sb_d4 = dict()     # Quad valve bicoder serial numbers
        self.sb_dd = dict()     # Twelve valve bicoder serial numbers
        self.sb_fm = dict()     # Flow bicoder serial numbers
        self.sb_ms = dict()     # Moisture bicoder serial numbers
        self.sb_ts = dict()     # Temperature bicoder serial numbers
        self.sb_sw = dict()     # Switch bicoder serial numbers
        self.sb_pu = dict()     # Pump bicoder serial numbers

        #####
        # Address ranges for each addressable object for configuration
        # This is read from the .json file with method load_data_from_json_file
        #####
        self.zn_ad_range = []
        self.ms_ad_range = []
        self.mv_ad_range = []
        self.fm_ad_range = []
        self.sw_ad_range = []
        self.ts_ad_range = []
        self.pg_ad_range = []

        # This is read from the .json file with method load_data_from_json_file
        self.daily_eto_value = []
        self.daily_rainfall_value = []

        self.controllers = controllers
        self.substations = substations
        self.zones = zones
        self.moisture_sensors = moisture_sensors
        self.flow_meters = flow_meters
        self.temperature_sensors = temperature_sensors
        self.master_valves = master_valves
        self.event_switches = event_switches
        self.zone_programs = zone_programs
        self.alert_relays = alert_relays
        self.basemanager_connection = basemanager_connection

        if self.cn_ty == "32":
            self.programs = programs32
            self.mainlines = mainlines32
            self.poc = poc32
            self.program_start_conditions = program_start_conditions32
            self.program_stop_conditions = program_stop_conditions32
            self.program_pause_conditions = program_pause_conditions32

        else:
            self.programs = programs10
            self.poc = poc10
            self.program_start_conditions = program_start_conditions10
            self.program_stop_conditions = program_stop_conditions10
            self.program_pause_conditions = program_pause_conditions10

    #################################
    def initialize_for_test(self):
        """
        Initializes objects and other things required for the test.
        
        1) Resets the object bucket to a clean starting state.
        2) Pass controller type to base classes.
        3) Init Resource Manager for webdriver and serial connection.
        4) Get serial connection to controller.
        5) Pass controller serial connection to base classes.
        6) Init serial connection to controller and verify connection established.
        7) Initialize controller devices, watersource and other required objects related to the controller.
        8) If substations specified, create them.
        """
        # print "\n\n ########################   Starting Test " + self.test_name + "    ############################\n\n"
        helper_methods.print_test_started(self.test_name)

        # 1 - Reset object bucket
        object_bucket.reset_all()

        # 2 - Base Class 'Controller Type' inheritance
        Devices.controller_type = self.cn_ty
        Programs.controller_type = self.cn_ty
        ZoneProgram.controller_type = self.cn_ty
        POC.controller_type = self.cn_ty
        Mainline.controller_type = self.cn_ty
        Ser.controller_type = self.cn_ty
        
        # 3 - Initialize serial and browser instances for use.
        self.resource_handler = ResourceHandler(user_conf=self.user_conf)
        
        if self.cn_ty == "10":
            controller_comport = self.user_conf.port_address_for_1000
            controller_eth_port = self.user_conf.socket_port_for_1000
        else:
            controller_comport = self.user_conf.port_address_for_3200
            controller_eth_port = self.user_conf.socket_port_for_3200

        # 4 - Init controller serial
        controller_ser = self.resource_handler.create_serial_connection(comport=controller_comport, 
                                                                        ethernet_port=controller_eth_port)

        # 5 - Pass controller serial connection to Base Classes
        Devices.ser = controller_ser
        Programs.ser = controller_ser
        ZoneProgram.ser = controller_ser
        Mainline.ser = controller_ser
        POC.ser = controller_ser
        BaseManagerConnection.ser = controller_ser
        BaseStartStopPause.ser = controller_ser

        # 6 - Init Serial connection and verify it's connected to controller
        controller_ser.init_ser_connection()
        controller_ser.check_if_cn_connected()

        # 7 - Initialize configuration
        self.load_data_from_json_file()
        self.create_cn_object()
        self.create_zn_objects()
        self.create_mv_objects()
        self.create_ms_objects()
        self.create_fm_objects()
        self.create_sw_objects()
        self.create_ts_objects()
        self.create_bm_objects()

        # 8 - If user has specified number of substations between 1 and 8, create them
        if 0 < self.number_of_substations_to_use <= 8:
            self.create_substation_objects(number_of_substations=self.number_of_substations_to_use)
            
    #################################
    def load_data_from_json_file(self):
        """
        Gets all device information from a config file based on the type of controller ('10' | '32'), and the size of
        configuration requested. \n
        :return:
        """
        # Open the .json file, at this point the .json file is un-readable by our python script.
        config_file = open(os.path.join('old_32_10_sb_objects_dec_29_2017/common/configuration_files', self.data_file))

        # Convert the .json file into a Python readable 'Object' so we can access the information stored.
        self.converted_file = json.load(config_file)

        # Get all device serial numbers
        self.d1 = map(str, self.converted_file[self.cn_ty]['D1'])
        self.mv_d1 = map(str, self.converted_file[self.cn_ty]['MV_D1'])
        self.d2 = map(str, self.converted_file[self.cn_ty]['D2'])
        self.mv_d2 = map(str, self.converted_file[self.cn_ty]['MV_D2'])
        self.d4 = map(str, self.converted_file[self.cn_ty]['D4'])
        self.dd = map(str, self.converted_file[self.cn_ty]['DD'])
        self.mv = map(str, self.converted_file[self.cn_ty]['MV']['serial'])
        self.ms = map(str, self.converted_file[self.cn_ty]['MS']['serial'])
        self.fm = map(str, self.converted_file[self.cn_ty]['FM']['serial'])
        self.ts = map(str, self.converted_file[self.cn_ty]['TS']['serial'])
        self.sw = map(str, self.converted_file[self.cn_ty]['SW']['serial'])

        # If we have specified substations to use, grab any shared devices for the controller from the substation.
        if 0 < self.number_of_substations_to_use <= 8:

            # Attempt to read shared device serial numbers from JSON file. If any of the sections are missing, raise
            # an exception explaining the problem.
            try:
                # Get all shared serial numbers.
                self.shared_d1 = map(str, self.converted_file[self.cn_ty]['SB_D1'])
                self.shared_mv_d1 = map(str, self.converted_file[self.cn_ty]['SB_MV_D1'])
                self.shared_d2 = map(str, self.converted_file[self.cn_ty]['SB_D2'])
                self.shared_mv_d2 = map(str, self.converted_file[self.cn_ty]['SB_MV_D2'])
                self.shared_d4 = map(str, self.converted_file[self.cn_ty]['SB_D4'])
                self.shared_dd = map(str, self.converted_file[self.cn_ty]['SB_DD'])
                self.shared_ms = map(str, self.converted_file[self.cn_ty]['SB_MS'])
                self.shared_ts = map(str, self.converted_file[self.cn_ty]['SB_TS'])
                self.shared_fm = map(str, self.converted_file[self.cn_ty]['SB_FM'])
                self.shared_sw = map(str, self.converted_file[self.cn_ty]['SB_SW'])
            except KeyError:
                e_msg = "\n\nException occurred trying to load shared device serial numbers from the use case " \
                        "configuration file: `{0}`.\n\n" \
                        "Please refer to `common/configuration_files/SB_timed_zones.json` for an example on how " \
                        "to structure the JSON for shared devices.\n\n"
                raise KeyError(e_msg)

        # Get all ranges for each device
        # in the json file this is looking at the
        # Example "ZN": {"range": [1, 150, 187, 200]},
        self.zn_ad_range = self.converted_file[self.cn_ty]['ZN']['range']
        self.ms_ad_range = self.converted_file[self.cn_ty]['MS']['range']
        self.fm_ad_range = self.converted_file[self.cn_ty]['FM']['range']
        self.mv_ad_range = self.converted_file[self.cn_ty]['MV']['range']
        self.ts_ad_range = self.converted_file[self.cn_ty]['TS']['range']
        self.sw_ad_range = self.converted_file[self.cn_ty]['SW']['range']

    # get the eto and rain fall from a list
    # in the json file this is looking at the
    # Example "Daily_ETO_and_rain_fall_values": {"eto": 0.00 , "rain_fall" : 0.00}
    #################################
    def load_eto_and_rain_fall_values(self):
        # Open the .json file, at this point the .json file is un-readable by our python script.
        config_file = open(os.path.join('common/configuration_files', self.data_file))
        # Convert the .json file into a Python readable 'Object' so we can access the information stored.
        converted_file = json.load(config_file)
        foo = converted_file["Daily_ETO_and_rain_fall_values"]
        r = range(0, len(foo))
        for index in r:
            self.daily_eto_value.append(foo[index]['eto'])
            self.daily_rainfall_value.append(foo[index]['rain_fall'])

    #################################
    def determine_zone_range_available(self):
        """
        Takes the list of zone range start/end values from the json file and creates a range of zone addresses based
        on the values from the json file. Expects the zone range list in the json file to have two numbers at least,
        a start and an end. \n
        :return:
        """
        ad_range = []       # Zone list of ranges to append to
        start = ''          # Starting range number
        stop = ''           # Ending range number

        # Flag for when we have seen the required two numbers for a valid range creation
        valid_range_available = False

        # Available zone address's specified by the .json config file.
        list_of_available_zn_ad = self.zn_ad_range

        # Makes sure that we have a valid number of start/stop values in the .json file
        if len(list_of_available_zn_ad) % 2 != 0:
            e_msg = "Invalid list of zone ranges provided in the data .json file for the current test. Valid Zone " \
                    "ranges must have 2 numbers for each range. Correct format in .json file should look like: \n" + \
                    "'range': [1, 200] -> says to create a range from 1 to 200. \n" + \
                    "'range': [1, 5, 10, 20] -> says to create a range from 1 to 5 and 10 to 20 with a gap between 5 " \
                    "and 10."
            raise ValueError(e_msg)

        #
        for index, address in enumerate(list_of_available_zn_ad):

            # index starts at 0, add 1 to account for off by one
            if (index + 1) % 2 == 0:
                # We are an ending range number, add 1 to this number because the range(i, j) returns from i to j-1
                # values
                stop = address + 1
                valid_range_available = True
            else:
                # We are the first of the two numbers required by 'range()', we are the 'start' number
                start = address
                
            # After we have seen the stop number for a start/end range combination, create the range and append the
            # range to the existing ranges
            if valid_range_available:
                ad_range += range(start, stop)

                # Toggle valid_range_available to False to allow for a new start and stop values
                valid_range_available = False
                start = ''
                stop = ''

        # return range
        return ad_range

    #################################
    def create_zn_objects(self):
        """
        Creates a Zone object for each value in the zone address range. Will only create as many zones as specified
        in the .json file for zones. \n
        :return:
        """
        # These ranges tell us how many of each decoder to create and address
        decoder_range_dict = {
            "D1": range(1, 2),
            "D2": range(1, 3),
            "D4": range(1, 5),
            "DD": range(1, 13)
        }

        # Get available zone ranges
        self.zn_ad_range = self.determine_zone_range_available()

        # Generate a list of all decoder's combined to for loop through each of them.
        list_of_zn_sn = self.d1 + self.d2 + self.d4 + self.dd
        # list_of_zn_sn += self.shared_d1 + self.shared_d2 + self.shared_d4 + self.shared_dd

        # is_shared_zone = False

        zn_list_index = 0
        for zn_sn in list_of_zn_sn:

            # TODO: Look for length of string

            # Parse everything besides the last two integers, this parsed string remains constant the entire method
            # this return the first 5 characters
            first_half = zn_sn[:5]

            # Gets the int portion of the serial number to increment (the last two digits)
            # this return the last 2 characters
            int_half = zn_sn[5:]

            # ------------------------------------------------------------------------------------------------------
            # Case 1: Single Valve Decoders
            if zn_sn.startswith(("TSD", "TPR", "TMV", "D")):

                # Make sure that we have zone addresses left in the list to address too
                if zn_list_index < len(self.zn_ad_range):

                    # Check shared single valve decoder serial numbers to see if zone is shared with a Substation.
                    # is_shared_zone = zn_sn in self.shared_d1

                    # Get the next available zone address from range for addressing
                    zn_ad = self.zn_ad_range[zn_list_index]
                    self.zones[zn_ad] = Zone(_serial=zn_sn, _address=zn_ad)
                    zn_list_index += 1

            # ------------------------------------------------------------------------------------------------------
            # Case 2: Dual Valve Decoders
            elif zn_sn.startswith(("TVE", "TSE", "E")):

                # Check shared dual valve decoder serial numbers to see if zone is shared with a Substation.
                # is_shared_zone = zn_sn in self.shared_d2

                # For loop to create two serial numbers for every dual valve decoder serial number
                for i in decoder_range_dict["D2"]:

                    # Make sure that we have zones addresses left to address to
                    if zn_list_index < len(self.zn_ad_range):
                        # Get the next available zone address from range for addressing
                        zn_ad = self.zn_ad_range[zn_list_index]

                        # Skip the first iteration of the loop to send the initial dual valve decoder serial number
                        # before incrementing it
                        if i > 1:
                            int_half = int(int_half) + 1

                            # If last two digits for incrementing start with '0', ie: "01", after incrementing "01",
                            # int_half has a value as an integer of 2, length 1, but we need a length of 2, thus add
                            # the '0' back to the front for a valid serial
                            if len(str(int_half)) == 1:
                                int_half = "0" + str(int_half)

                        zn_sn = first_half + str(int_half)
                        self.zones[zn_ad] = Zone(_serial=zn_sn, _address=zn_ad)
                        zn_list_index += 1

            # ------------------------------------------------------------------------------------------------------
            # Case 3: Quad Valve Decoders
            elif zn_sn.startswith(("TSQ", "Q")):

                # Check shared quad valve decoder serial numbers to see if zone is shared with a Substation.
                # is_shared_zone = zn_sn in self.shared_d4

                # For loop to create two serial numbers for every quad valve decoder serial number
                for i in decoder_range_dict["D4"]:

                    # Make sure that we have zones addresses left to address to
                    if zn_list_index < len(self.zn_ad_range):
                        # Get the next available zone address from range for addressing
                        zn_ad = self.zn_ad_range[zn_list_index]

                        # Skip the first iteration of the loop to send the initial dual valve decoder serial number
                        # before incrementing it
                        if i > 1:
                            int_half = int(int_half) + 1

                            # If last two digits for incrementing start with '0', ie: "01", after incrementing "01",
                            # int_half has a value as an integer of 2, length 1, but we need a length of 2, thus add
                            # the '0' back to the front for a valid serial
                            if len(str(int_half)) == 1:
                                int_half = "0" + str(int_half)

                        zn_sn = first_half + str(int_half)
                        self.zones[zn_ad] = Zone(_serial=zn_sn, _address=zn_ad)
                        zn_list_index += 1

            # ------------------------------------------------------------------------------------------------------
            # Case 4: Twelve Valve Decoders
            elif zn_sn.startswith(("TVA", "TVB", "V")):

                # Check shared twelve valve decoder serial numbers to see if zone is shared with a Substation.
                # is_shared_zone = zn_sn in self.shared_dd

                # For loop to create two serial numbers for every twelve valve decoder serial number
                for i in decoder_range_dict["DD"]:

                    # Make sure that we have zones addresses left to address to
                    if zn_list_index < len(self.zn_ad_range):
                        # Get the next available zone address from range for addressing
                        zn_ad = self.zn_ad_range[zn_list_index]

                        # Skip the first iteration of the loop to send the initial dual valve decoder serial number
                        # before incrementing it
                        if i > 1:
                            int_half = int(int_half) + 1

                            # If last two digits for incrementing start with '0', ie: "01", after incrementing "01",
                            # int_half has a value as an integer of 2, length 1, but we need a length of 2, thus add
                            # the '0' back to the front for a valid serial
                            if len(str(int_half)) == 1:
                                int_half = "0" + str(int_half)

                        zn_sn = first_half + str(int_half)
                        self.zones[zn_ad] = Zone(_serial=zn_sn, _address=zn_ad)
                        zn_list_index += 1

    #################################
    def create_cn_object(self):
        """
        :return:
        :rtype:
        """
        # dict(self.controller).update((1, Controller(_type=self.cn_ty, _mac=self.mac, _description=self.test_name)))
        # self.objects["CN"][1] = Controller(_type=self.cn_ty, _mac=self.mac, _description=self.test_name)
        self.controllers[1] = Controller(_type=self.cn_ty, _mac=self.mac, _description=self.test_name,
                                         _cn_serial_number=self.cn_serial_number, _firmware_version=self.cn_fw_version)

        # Add bicoder serial numbers to controller for loading. This is the NEW way.
        self.controllers[1].d1_bicoders.extend(self.d1)
        self.controllers[1].d1_bicoders.extend(self.mv_d1)
        self.controllers[1].d2_bicoders.extend(self.d2)
        self.controllers[1].d2_bicoders.extend(self.mv_d2)
        self.controllers[1].d4_bicoders.extend(self.d4)
        self.controllers[1].dd_bicoders.extend(self.dd)
        self.controllers[1].ms_bicoders.extend(self.ms)
        self.controllers[1].ts_bicoders.extend(self.ts)
        self.controllers[1].fm_bicoders.extend(self.fm)
        self.controllers[1].sw_bicoders.extend(self.sw)

    #################################
    def create_ms_objects(self):
        """
        :return:
        """
        ms_serial_list = self.ms

        for index, ms_ad in enumerate(self.ms_ad_range):
            # 'enumerate' makes ms_address start at 0 (off by 1 problem) so we add 1 to compensate because we can't
            # have a starting address of 0.
            serial = ms_serial_list[index]

            # Here we are creating an integer key value to lookup each moisture sensor
            self.moisture_sensors[ms_ad] = MoistureSensor(_serial=serial, _address=ms_ad)

    #################################
    def create_mv_objects(self):
        """
        :return:
        """
        mv_serial_list = self.mv

        for index, mv_ad in enumerate(self.mv_ad_range):
            # 'enumerate' makes ms_address start at 0 (off by 1 problem) so we add 1 to compensate because we can't
            # have a starting address of 0.
            serial = mv_serial_list[index]

            # Here we are creating an integer key value to lookup each master valve
            self.master_valves[mv_ad] = MasterValve(_serial=serial, _address=mv_ad)

    #################################
    def create_fm_objects(self):
        """
        :return:
        """
        fm_serial_list = self.fm

        for index, fm_ad in enumerate(self.fm_ad_range):
            # 'enumerate' makes ms_address start at 0 (off by 1 problem) so we add 1 to compensate because we can't
            # have a starting address of 0.
            serial = fm_serial_list[index]

            # Here we are creating an integer key value to lookup each flow meter
            self.flow_meters[fm_ad] = FlowMeter(_serial=serial, _address=fm_ad)

    #################################
    def create_sw_objects(self):
        """
        :return:
        """
        sw_serial_list = self.sw

        for index, sw_ad in enumerate(self.sw_ad_range):
            # 'enumerate' makes ms_address start at 0 (off by 1 problem) so we add 1 to compensate because we can't
            # have a starting address of 0.
            serial = sw_serial_list[index]

            # Here we are creating an integer key value to lookup each event switch
            self.event_switches[sw_ad] = EventSwitch(_serial=serial, _address=sw_ad)

    #################################
    def create_ts_objects(self):
        """
        :return:
        """
        ts_serial_list = self.ts

        for index, ts_ad in enumerate(self.ts_ad_range):
            # 'enumerate' makes ms_address start at 0 (off by 1 problem) so we add 1 to compensate because we can't
            # have a starting address of 0.
            serial = ts_serial_list[index]

            # Here we are creating an integer key value to lookup each temperature sensor
            self.temperature_sensors[ts_ad] = TemperatureSensor(_serial=serial, _address=ts_ad)

    #################################
    def create_mainline_objects(self):
        """
        Creates starting 8 default mainline objects for use in test if a '3200' controller type is used. \n
        """
        # For loop to create 8 Mainline objects, there are always 8.
        for each_mainline_address in range(1, 9):

            # Create each Mainline and store into the dictionary using the address as a lookup key
            self.mainlines[each_mainline_address] = Mainline(_ad=each_mainline_address)

    #################################
    def create_3200_poc_objects(self):
        """
        Creates starting 8 default Point of Connection objects for use in test if a '3200' controller type is used. \n
        """
        # For loop to create 8 Point Of Connection objects, there are always 8.
        for each_poc_address in range(1, 9):

            # PER ERS:
            # Only the first point of connection is assigned a flow meter and master valve (the first flow meter
            # loaded on the controller as well as the first master valve loaded on the controller)
            if each_poc_address == 1:
                # Create each Point Of Connection and store into the dictionary using the address as a lookup key
                self.poc[each_poc_address] = POC3200(
                    _ad=each_poc_address,
                    _fm=1,
                    _mv=1
                )

            else:
                # Create each Point Of Connection and store into the dictionary using the address as a lookup key
                self.poc[each_poc_address] = POC3200(
                    _ad=each_poc_address
                )

    #################################
    def create_1000_poc_objects(self):
        """
        Creates starting 8 default Point of Connection objects for use in test if a '1000' controller type is used. \n
        """
        # For loop to create 3 Point Of Connection objects, there are always 3.
        for each_poc_address in range(1, 4):

            # Create each Point Of Connection and store into the dictionary using the address as a lookup key
            self.poc[each_poc_address] = POC1000(
                _address=each_poc_address
            )

    #################################
    def create_bm_objects(self):
        """
        :return:
        :rtype:
        """
        self.basemanager_connection[1] = BaseManagerConnection(_url=self.user_conf.url)

    #################################
    def verify_objects_on_cn(self, list_of_obj_pointers):
        """
        Verifies all objects in the list passed in. The list should be a list of object dictionaries. \n
        ie: [self.config.zones, self.config.master_valves, ... ect.]
        :param list_of_obj_pointers:    List of objects to verify. \n
        :type list_of_obj_pointers:     list \n
        """
        for each_object in list_of_obj_pointers:
            for _ad in sorted(each_object.keys()):
                each_object[_ad].verify_who_i_am()

    #################################
    def verify_full_configuration(self):
        """
        This goes through each object in the list and sorts them by address
        then it verifies all attributes of the object against what the controller thinks
        :return:
        :rtype:
        """
        if self.cn_ty == "32":
            list_of_object_pointers = [
                self.controllers,
                self.zones,
                self.moisture_sensors,
                self.master_valves,
                self.temperature_sensors,
                # This should have been fixed in ZZ-522 bug
                self.flow_meters,
                self.event_switches,
                self.programs,
                self.zone_programs,
                self.mainlines,
                self.poc,
                self.program_start_conditions,
                self.program_stop_conditions,
                self.program_pause_conditions,
            ]
        else:
            list_of_object_pointers = [
                self.controllers,
                self.zones,
                self.moisture_sensors,
                self.master_valves,
                self.temperature_sensors,
                # This should have been fixed in ZZ-522 bug
                self.flow_meters,
                self.event_switches,
                self.programs,
                self.zone_programs,
                self.poc,
                self.program_start_conditions,
                self.program_stop_conditions,
                self.program_pause_conditions,
                self.alert_relays
            ]
        self.verify_objects_on_cn(list_of_obj_pointers=list_of_object_pointers)

    #################################
    def create_substation_objects(self, number_of_substations):
        """
        Create's the number of substation's specified for the use case.
        
        :param number_of_substations: Number of substations to create.
        :type number_of_substations: int
        :return: 
        """
        # Open the .json file, at this point the .json file is un-readable by our python script.
        config_file = open(os.path.join('common/configuration_files', self.data_file))

        # Convert the .json file into a Python readable 'Object' so we can access the information stored.
        controller_data_file = json.load(config_file)

        # Temp substation dict to store in
        substations_to_store = dict()

        # Substation serial number
        substation_serial_number = "SK1000"

        # For each Substation JSON object
        for index, substation_json in enumerate(controller_data_file[opcodes.substation]):
            address = index + 1

            # Break out of for loop after we have created the specified number of sub stations
            if address > number_of_substations:
                break
            
            # Get Serial Port attributes and create a Serial connection instance
            sb_comport = self.user_conf.port_address_for_substation[address]
            sb_eth_port = self.user_conf.socket_port_for_substation[address]
            sb_ser_conn = self.resource_handler.create_serial_connection(comport=sb_comport, ethernet_port=sb_eth_port)
            
            # Init Substation serial connection
            sb_ser_conn.init_ser_connection()
            sb_ser_conn.check_if_cn_connected()
            
            # Store bicoder serial numbers for each substation by address.
            self.sb_d1[address] = map(str, substation_json['D1'])
            self.sb_d2[address] = map(str, substation_json['D2'])
            self.sb_d4[address] = map(str, substation_json['D4'])
            self.sb_dd[address] = map(str, substation_json['DD'])
            self.sb_fm[address] = map(str, substation_json['FB'])
            self.sb_sw[address] = map(str, substation_json['SB'])
            self.sb_ts[address] = map(str, substation_json['TB'])
            self.sb_ms[address] = map(str, substation_json['MB'])
            self.sb_pu[address] = map(str, substation_json['PB'])

            # Create substation instance
            substation = Substation(
                _ip=self.user_conf.ip_address_for_substation[address],
                _mac=self.user_conf.mac_address_for_substation[address],
                _ds=self.test_name + "-SB-" + str(address),
                _ser=sb_ser_conn,
                _sn=substation_serial_number + str(address),
                _vr=self.sb_fw_version
            )

            # Add assigned bicoder serial numbers to substation attribute placeholders for loading.
            substation.d1_bicoders.extend(self.sb_d1[address])
            substation.d2_bicoders.extend(self.sb_d2[address])
            substation.d4_bicoders.extend(self.sb_d4[address])
            substation.dd_bicoders.extend(self.sb_dd[address])
            substation.ms_bicoders.extend(self.sb_ms[address])
            substation.ts_bicoders.extend(self.sb_ts[address])
            substation.fm_bicoders.extend(self.sb_fm[address])
            substation.sw_bicoders.extend(self.sb_sw[address])

            # Get and create valve bicoder serial numbers
            valve_serial_numbers = self.sb_d1[address]

            # Add dual valve bicoder serial numbers
            valve_serial_numbers.extend(
                self.create_valve_decoder_serial_numbers(serial_list=self.sb_d2[address], serial_type='DUAL')
            )

            # Add quad valve bicoder serial numbers
            valve_serial_numbers.extend(
                self.create_valve_decoder_serial_numbers(serial_list=self.sb_d4[address], serial_type='QUAD')
            )

            # Add twelve valve bicoder serial numbers
            valve_serial_numbers.extend(
                self.create_valve_decoder_serial_numbers(serial_list=self.sb_dd[address], serial_type='TWELVE')
            )
            
            # Add valve bicoders to substation
            substation.valve_bicoders.update(
                self.create_valve_bicoders_for_sb(serial_list=valve_serial_numbers, sb_ser=sb_ser_conn)
            )
            
            # Add Flow bicoders to substation
            substation.flow_bicoders.update(
                self.create_flow_bicoders_for_sb(serial_list=self.sb_fm[address], sb_ser=sb_ser_conn)
            )
            
            # Add Pump bicoders to substation
            substation.pump_bicoders.update(
                self.create_pump_bicoders_for_sb(serial_list=self.sb_pu[address], sb_ser=sb_ser_conn)
            )
            
            # Add Switch bicoders to substation
            substation.switch_bicoders.update(
                self.create_switch_bicoders_for_sb(serial_list=self.sb_sw[address], sb_ser=sb_ser_conn)
            )

            # Add Moisture bicoders to substation
            substation.moisture_bicoders.update(
                self.create_moisture_bicoders_for_sb(serial_list=self.sb_ms[address], sb_ser=sb_ser_conn)
            )

            # Add Temperature bicoders to substation
            substation.temp_bicoders.update(
                self.create_temperature_bicoders_for_sb(serial_list=self.sb_ts[address], sb_ser=sb_ser_conn)
            )

            # Add substation to temp substation dict
            substations_to_store[address] = substation

        # Update config/object bucket's substation dictionary with substations created.
        self.substations.update(substations_to_store)

        # Close JSON file
        config_file.close()

    #################################
    def create_valve_decoder_serial_numbers(self, serial_list, serial_type):
        """
        :param serial_list: Serial number to expand.
        :type serial_list: list[str]
        
        :param serial_type: 'DUAL' | 'QUAD' | 'TWELVE' | 'TWENTYFOUR' | 'FORTYEIGHT'
        :type serial_type: str
        
        :return: List of serial numbers generated from the Serial and Serial Type
        :rtype: list[str]
        """
        number_of_sn_to_create = {
            'DUAL': 2,
            'QUAD': 4,
            'TWELVE': 12,
            'TWENTYFOUR': 24,
            'FORTYEIGHT': 48
        }
        list_of_serial_numbers_generated = []
        
        # For each serial in the list
        for serial in serial_list:

            # Parse everything besides the last two integers, this parsed string remains constant the entire method
            # this return the first 5 characters
            first_half = serial[:5]

            # Gets the int portion of the serial number to increment (the last two digits)
            # this return the last 2 characters
            int_half = int(serial[5:])

            number_of_serial_numbers_to_create = number_of_sn_to_create[serial_type]
            for number_to_create in range(0, number_of_serial_numbers_to_create):

                if number_to_create > 0:
                    int_half += 1

                int_half_as_str = str(int_half)

                # Check if `int_half` is a single digit, if so, add a "0" before to end as "07"
                if len(int_half_as_str) == 1:
                    int_half_as_str = "0" + int_half_as_str

                # Combine serial number back to a 7 character string value
                serial_to_add = first_half + int_half_as_str
                list_of_serial_numbers_generated.append(serial_to_add)

        return list_of_serial_numbers_generated

    #################################
    def create_valve_bicoders_for_sb(self, serial_list, sb_ser):
        """
        :param serial_list: List of valve bicoder serial numbers to create.
        :type serial_list: list[str]
        
        :param sb_ser: Substation's serial connection instance.
        :type sb_ser: common.objects.base_classes.ser.Ser
        
        :return: Dictionary of Valve Bicoders indexed by Serial Number.
        :rtype: dict[str, common.objects.substation.valve_bicoder.ValveBicoder]
        """
        valve_bicoders = dict()
        
        for serial_number in serial_list:
            valve_bicoder = ValveBicoder(_sn=serial_number, _ser=sb_ser)
            valve_bicoders[serial_number] = valve_bicoder

        return valve_bicoders

    #################################
    def create_flow_bicoders_for_sb(self, serial_list, sb_ser):
        """
        :param serial_list: List of flow bicoder serial numbers to create.
        :type serial_list: list[str]
        
        :param sb_ser: Substation's serial connection instance.
        :type sb_ser: common.objects.base_classes.ser.Ser
        
        :return: Dictionary of Flow Bicoders indexed by Serial Number.
        :rtype: dict[str, common.objects.substation.flow_bicoder.FlowBicoder]
        """
        flow_bicoders = dict()

        for serial_number in serial_list:
            flow_bicoder = FlowBicoder(_sn=serial_number, _ser=sb_ser)
            flow_bicoders[serial_number] = flow_bicoder

        return flow_bicoders

    #################################
    def create_pump_bicoders_for_sb(self, serial_list, sb_ser):
        """
        :param serial_list: List of pump bicoder serial numbers to create.
        :type serial_list: list[str]
        
        :param sb_ser: Substation's serial connection instance.
        :type sb_ser: common.objects.base_classes.ser.Ser
        
        :return: Dictionary of Pump Bicoders indexed by Serial Number.
        :rtype: dict[str, common.objects.substation.pump_bicoder.PumpBicoder]
        """
        pump_bicoders = dict()

        for serial_number in serial_list:
            pump_bicoder = PumpBicoder(_sn=serial_number, _ser=sb_ser)
            pump_bicoders[serial_number] = pump_bicoder

        return pump_bicoders

    #################################
    def create_switch_bicoders_for_sb(self, serial_list, sb_ser):
        """
        :param serial_list: List of switch bicoder serial numbers to create.
        :type serial_list: list[str]
        
        :param sb_ser: Substation's serial connection instance.
        :type sb_ser: common.objects.base_classes.ser.Ser
        
        :return: Dictionary of Switch Bicoders indexed by Serial Number.
        :rtype: dict[str, common.objects.substation.switch_bicoder.SwitchBicoder]
        """
        switch_bicoders = dict()

        for serial_number in serial_list:
            switch_bicoder = SwitchBicoder(_sn=serial_number, _ser=sb_ser)
            switch_bicoders[serial_number] = switch_bicoder

        return switch_bicoders

    #################################
    def create_moisture_bicoders_for_sb(self, serial_list, sb_ser):
        """
        :param serial_list: List of moisture bicoder serial numbers to create.
        :type serial_list: list[str]
        
        :param sb_ser: Substation's serial connection instance.
        :type sb_ser: common.objects.base_classes.ser.Ser
        
        :return: Dictionary of Moisture Bicoders indexed by Serial Number.
        :rtype: dict[str, common.objects.substation.moisture_bicoder.MoistureBicoder]
        """
        moisture_bicoders = dict()

        for serial_number in serial_list:
            moisture_bicoder = MoistureBicoder(_sn=serial_number, _ser=sb_ser)
            moisture_bicoders[serial_number] = moisture_bicoder

        return moisture_bicoders

    #################################
    def create_temperature_bicoders_for_sb(self, serial_list, sb_ser):
        """
        :param serial_list: List of temperature bicoder serial numbers to create.
        :type serial_list: list[str]
        
        :param sb_ser: Substation's serial connection instance.
        :type sb_ser: common.objects.base_classes.ser.Ser
        
        :return: Dictionary of Temperature Bicoders indexed by Serial Number.
        :rtype: dict[str, common.objects.substation.temp_bicoder.TempBicoder]
        """
        temperature_bicoders = dict()

        for serial_number in serial_list:
            temperature_bicoder = TempBicoder(_sn=serial_number, _ser=sb_ser)
            temperature_bicoders[serial_number] = temperature_bicoder

        return temperature_bicoders

    #################################
    def share_sb_device_with_cn(self, device_type, device_serial, address, substation_number=1):
        """
        Share a Substation device with the Controller being tested.
        
        Operations:
        1) Creates a new instance of the device and adds it to the respecitve object dictionary.
        2) Addresses and sets default values on the controller.
        
        :param device_type: Type of device to share. (i.e., "FM" | "ZN" | "MV" | "SW" | "TS" | "MS" )
        :type device_type: str
        
        :param device_serial: Serial number of device to share.
        :type device_serial: str
        
        :param address: Address to set for device.
        :type address: int
        
        :param substation_number: Address of Substation to share device for.
        :type substation_number: int
        """
        valid_device_types = [opcodes.zone, opcodes.flow_meter, opcodes.master_valve, opcodes.event_switch,
                              opcodes.temperature_sensor, opcodes.moisture_sensor]

        # Validate device type
        if device_type not in valid_device_types:
            e_msg = "Attempted to share an invalid device type ('{0}') with a controller. Valid device types " \
                    "are: {1}".format(device_type, valid_device_types)
            raise ValueError(e_msg)

        # device_type == "ZN"
        if device_type == opcodes.zone:

            # Validate zone address
            if address in self.zones.keys():
                e_msg = "Attempted to share a Substation valve serial number: {0} at address: {1} which is already " \
                        "addressed on the Controller. Current valve addresses are: {2}".format(
                            device_serial,
                            address,
                            self.zones.keys()
                        )
                raise ValueError(e_msg)

            else:
                self.zones[address] = Zone(_serial=device_serial, _address=address, _shared_with_sb=True)
                self.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.zones,
                                                                          zn_ad_range=[address])

        # device_type == "MV"
        elif device_type == opcodes.master_valve:

            # Validate master valve address
            if address in self.master_valves.keys():
                e_msg = "Attempted to share a Substation valve serial number: {0} at address: {1} which is already " \
                        "addressed on the Controller. Current valve addresses are: {2}".format(
                            device_serial,
                            address,
                            self.master_valves.keys()
                        )
                raise ValueError(e_msg)

            else:
                self.master_valves[address] = MasterValve(_serial=device_serial, _address=address, _shared_with_sb=True)
                self.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.master_valves,
                                                                          mv_ad_range=[address])

        # device_type == "FM"
        elif device_type == opcodes.flow_meter:

            # Validate flow meter address
            if address in self.flow_meters.keys():
                e_msg = "Attempted to share a Substation flow bicoder serial number: {0} at address: {1} which is " \
                        "already addressed on the Controller. Current flow device addresses are: {2}".format(
                            device_serial,
                            address,
                            self.flow_meters.keys()
                        )
                raise ValueError(e_msg)

            else:
                self.flow_meters[address] = FlowMeter(_serial=device_serial, _address=address, _shared_with_sb=True)
                self.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.flow_meters,
                                                                          fm_ad_range=[address])

                # Added when the flow rate count/flow usage count equations were implemented. The kvalue from the
                # Controller's Flow Meter is required for calculations. It works for only 1 substation for right now.
                self.substations[substation_number].flow_bicoders[device_serial].fm_on_cn = self.flow_meters[address]

        # device_type == "MS"
        elif device_type == opcodes.moisture_sensor:

            # Validate moisture sensor address
            if address in self.moisture_sensors.keys():
                e_msg = "Attempted to share a Substation moisture bicoder serial number: {0} at address: {1} which " \
                        "is already addressed on the Controller. Current moisture device addresses are: {2}".format(
                            device_serial,
                            address,
                            self.moisture_sensors.keys()
                        )
                raise ValueError(e_msg)

            else:
                self.moisture_sensors[address] = MoistureSensor(_serial=device_serial, _address=address,
                                                                _shared_with_sb=True)
                self.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.moisture_sensors,
                                                                          ms_ad_range=[address])

        # device_type == "TS"
        elif device_type == opcodes.temperature_sensor:

            # Validate temperature sensor address
            if address in self.temperature_sensors.keys():
                e_msg = "Attempted to share a Substation temp bicoder serial number: {0} at address: {1} which is " \
                        "already addressed on the Controller. Current temp device addresses are: {2}".format(
                            device_serial,
                            address,
                            self.temperature_sensors.keys()
                        )
                raise ValueError(e_msg)

            else:
                self.temperature_sensors[address] = TemperatureSensor(_serial=device_serial, _address=address,
                                                                      _shared_with_sb=True)
                self.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.temperature_sensors,
                                                                          ts_ad_range=[address])

        # device_type == "SW"
        # NOTE - 'else' can be used here because "SW" is the last 'valid' device type, and since we already validated
        #        device types above, we can assume we have a "SW" at this point.
        else:

            # Validate event switch address
            if address in self.event_switches.keys():
                e_msg = "Attempted to share a Substation switch bicoder serial number: {0} at address: {1} which is " \
                        "already addressed on the Controller. Current switch device addresses are: {2}".format(
                            device_serial,
                            address,
                            self.event_switches.keys()
                        )
                raise ValueError(e_msg)

            else:
                self.event_switches[address] = EventSwitch(_serial=device_serial, _address=address,
                                                           _shared_with_sb=True)
                self.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.event_switches,
                                                                          sw_ad_range=[address])
