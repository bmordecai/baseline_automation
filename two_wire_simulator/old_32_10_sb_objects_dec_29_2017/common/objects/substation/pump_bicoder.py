from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.bicoders import BiCoder

__author__ = 'bens'


class PumpBicoder(BiCoder):

    def __init__(self, _sn, _ser):
        """
        :param _sn: Serial number to set for BiCoder.
        :type _sn: str
        
        :param _ser: Serial connection from controller
        :type _ser: common.objects.base_classes.ser.Ser
        """
        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(PumpBicoder, self).__init__(_serial_number=_sn, _serial_connection=_ser)
        self.ty = self.bicoder_types.PUMP
