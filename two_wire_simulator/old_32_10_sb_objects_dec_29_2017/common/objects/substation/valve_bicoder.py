from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.bicoders import BiCoder

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

__author__ = 'bens'


class ValveBicoder(BiCoder):

    def __init__(self, _sn, _ser):
        """
        :param _sn: Serial number to set for BiCoder.
        :type _sn: str
        
        :param _ser: Serial connection from substation
        :type _ser: common.objects.base_classes.ser.Ser
        """
        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(ValveBicoder, self).__init__(_serial_number=_sn, _serial_connection=_ser)
        self.ty = self.bicoder_types.VALVE
        self.id = self.bicoder_ids.VALVE

        self.va = 0.23  # Solenoid Current
        self.vv = 28.7  # Solenoid Voltage

        # Attributes that we will send to the controller by default
        self.default_attributes = [
            (opcodes.solenoid_current, 'va'),
            (opcodes.solenoid_voltage, 'vv'),
            (opcodes.two_wire_drop, 'vt')
        ]

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_solenoid_current(self, _solenoid_current=None):
        """
        Sets the solenoid's current value. \n
        By passing in a value for '_va', method assumes this value to overwrite current value. \n

        :param _solenoid_current: Value to set for solenoid current.
        :type _solenoid_current: Integer, Float
        """
        # Check if user wants to overwrite current value
        if _solenoid_current is not None:

            # Verifies the solenoid current passed in is an integer / float type
            if not isinstance(_solenoid_current, (int, float)):
                e_msg = "Failed trying to set {0} {1}'s solenoid current. Invalid type passed in, expected int or " \
                        "float. Received type: {2}".format(
                            self.ty,                    # {0}
                            str(self.sn),               # {1}
                            type(_solenoid_current)     # {2}
                        )
                raise TypeError(e_msg)
            else:
                # Valid type, overwrite current value with new value
                self.va = _solenoid_current

        # Command for sending
        command = "SET,{0}={1},{2}={3}".format(
            self.id,                    # {0}
            str(self.sn),               # {1}
            opcodes.solenoid_current,   # {2}
            str(self.va)                # {3}
        )

        try:
            # Attempt to set solenoid current for zone at the substation
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} {1}'s 'Solenoid Current' to: '{2}'".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.va)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} {1}'s 'Solenoid Current' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.va)    # {2}
            ))

    #################################
    def set_solenoid_voltage(self, _solenoid_voltage=None):
        """
        Sets the solenoid's voltage value for this BiCoder. \n
        By passing in a value for '_solenoid_voltage', method assumes this value to overwrite current value. \n

        :param _solenoid_voltage: Value to set for solenoid voltage.
        :type _solenoid_voltage: Integer, Float
        """
        # Check if user wants to overwrite current value
        if _solenoid_voltage is not None:

            # Verify the correct type is passed in.
            if not isinstance(_solenoid_voltage, (int, float)):
                e_msg = "Failed trying to set {0} {1}'s solenoid voltage. Invalid type passed in, expected int or " \
                        "float. Received type: {2}".format(
                            self.ty,                    # {0}
                            str(self.sn),               # {1}
                            type(_solenoid_voltage)     # {2}
                        )
                raise TypeError(e_msg)
            else:
                # Valid type, overwrite current value with new value
                self.vv = _solenoid_voltage

        # Command for sending
        command = "SET,{0}={1},{2}={3}".format(
            self.id,                    # {0}
            str(self.sn),               # {1}
            opcodes.solenoid_voltage,   # {2}
            str(self.vv)                # {3}
        )

        try:
            # Attempt to set solenoid voltage for zone at the substation
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Solenoid Voltage' to: '{2}'".format(
                self.ty, str(self.sn), str(self.vv))
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Solenoid Voltage' to: {2}".format(
                self.ty, str(self.sn), str(self.vv)))

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    ################################
    def verify_solenoid_current(self,  _data=None):
        """
        Verifies the solenoid current set for this Valve BiCoder. Expects the Substation's value and this
        instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Compare status versus what is on the substation
        zn_va_on_sb = float(data.get_value_string_by_key('VA'))
        rounded_va = round(number=self.va, ndigits=2)
        if abs(rounded_va - zn_va_on_sb) > 0.02:
            e_msg = "Unable to verify {0} {1}'s solenoid current. Received: {2}, Expected: {3}".format(
                self.ty,            # {0}
                str(self.sn),       # {1}
                str(zn_va_on_sb),   # {2}
                str(self.va)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s solenoid current value: '{2}' on substation".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.va)    # {2}
            ))
            return True

    #################################
    def verify_solenoid_voltage(self, _data=None):
        """
        Verifies the solenoid voltage set for this Valve BiCoder. Expects the Substation's value and this
        instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Compare status versus what is on the substation
        zn_vv_on_sb = float(data.get_value_string_by_key('VV'))
        rounded_vv = round(number=self.vv, ndigits=2)
        if abs(rounded_vv - zn_vv_on_sb) > 0.02:
            e_msg = "Unable to verify {0} {1}'s solenoid voltage. Received: {2}, Expected: {3}".format(
                self.ty,            # {0}
                str(self.sn),       # {1}
                str(zn_vv_on_sb),   # {2}
                str(self.vv)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s solenoid voltage value: '{2}' on substation".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vv)    # {2}
            ))
            return True

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str
        """
        data = self.get_data()

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify valve specific attributes
        self.verify_serial_number(_data=data)
        self.verify_solenoid_current(_data=data)
        self.verify_solenoid_voltage(_data=data)
        self.verify_two_wire_drop_value(_data=data)

        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################
