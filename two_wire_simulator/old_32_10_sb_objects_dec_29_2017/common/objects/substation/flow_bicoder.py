from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.bicoders import BiCoder

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_equations

__author__ = 'bens'


class FlowBicoder(BiCoder):
    """
    :type fm_on_cn: common.objects.controller.fm.FlowMeter
    """

    def __init__(self, _sn, _ser):
        """
        :param _sn: Serial number to set for BiCoder.
        :type _sn: str
        
        :param _ser: Serial connection from substation
        :type _ser: common.objects.base_classes.ser.Ser
        """
        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(FlowBicoder, self).__init__(_serial_number=_sn, _serial_connection=_ser)
        self.ty = self.bicoder_types.FLOW
        self.id = self.bicoder_ids.FLOW

        self.vr = 0.0   # Flow rate in Gallons Per Minute
        self.vg = 0.0   # Flow reported as usage in Total Gallons

        self.cr = 0     # Flow Rate Count
        self.cp = 0     # Flow Pulse Count
        self.cu = 0     # Total Usage Count

        # Attributes that we will send to the controller by default
        self.default_attributes = [
            # (opcodes.flow_rate, 'vr'),
            # (opcodes.total_usage, 'vg'),
            (opcodes.flow_rate_count, 'cr'),
            (opcodes.flow_pulse_count, 'cp'),
            (opcodes.flow_usage_count, 'cu'),
            (opcodes.two_wire_drop, 'vt')
        ]

        # Flow Meter object reference from Controller objects.
        # NOTE:
        #   This is set when the substation shares a device with the controller in `configuration.py`.
        self.fm_on_cn = None

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_flow_rate(self, _flow_rate=None):
        """
        Set the flow rate for the Flow Meter (Faux IO Only) \n

        :param _flow_rate: Value to set the Flow Meter flow rate as a float in gallon per minute\n
        :type _flow_rate: int, float
        """
        # If a flow rate is passed in for overwrite
        if _flow_rate is not None:

            # Verifies the flow rate passed in is an int or float value
            if not isinstance(_flow_rate, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s Flow Rate. Invalid argument type, expected an int " \
                        "or float. Received type: {2}".format(self.ty, self.sn, type(_flow_rate))
                raise TypeError(e_msg)
            else:
                self.vr = _flow_rate

        # Get K-Value from FlowMeter on Controller
        fm_kval = self.fm_on_cn.kv

        # Calculate flow rate count
        flow_rate_count = helper_equations.calculate_flow_rate_count_from_gpm(gpm=self.vr, kval=fm_kval)

        # Command for sending
        command = "SET,{0}={1},{2}={3}".format(self.id, self.sn, opcodes.flow_rate_count, flow_rate_count)

        try:
            # Attempt to set flow rate for Flow Meter at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Flow Rate' to: '{2}'".format(
                self.ty, self.sn, str(self.vr))
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Flow Rate' to: {2}".format(
                self.ty, self.sn, str(self.vr)))

    #################################
    def set_water_usage(self, _water_usage=None):
        """
        Set the water usage for the Flow Meter (Faux IO Only) \n

        :param _water_usage: Value to set the Flow Meter total water used as a float in gallons\n
        :type _water_usage: int | float
        """
        # If a water usage is passed in for overwrite
        if _water_usage is not None:

            # Verifies the water usage passed in is an int or float value
            if not isinstance(_water_usage, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s Water Usage. Invalid argument type, expected an int or " \
                        "float. Received type: {2}".format(self.ty, self.sn, type(_water_usage))
                raise TypeError(e_msg)
            else:
                self.vg = _water_usage

        # Get K-Value from FlowMeter on Controller
        fm_kval = self.fm_on_cn.kv

        # Calculate flow usage count
        flow_usage_count = helper_equations.calculate_flow_usage_count_from_gallons(gal=self.vg, kval=fm_kval)

        # Command for sending
        command = "SET,{0}={1},{2}={3}".format(self.id, str(self.sn), opcodes.total_usage, flow_usage_count)

        try:
            # Attempt to set water usage for Flow Meter at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'water usage' to: '{2}'".format(
                self.ty, self.sn, str(self.vg))
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'water usage' to: {2}".format(
                self.ty, self.sn, str(self.vg)))

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_flow_rate(self, _data=None):
        """
        Verifies flow rate for this Flow Meter on the Substation. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Get the flow rate from the data object (we cast to float first because a string with '0.00' cannot be
        # converted to an int
        flow_rate_count = int(float(data.get_value_string_by_key(opcodes.flow_rate_count)))

        # Get K-Value from FlowMeter on Controller
        fm_kval = self.fm_on_cn.kv

        # Compute GPM value from flow rate count returned from the Substation Flow BiCoder
        calculated_flow_rate = helper_equations.calculate_gpm_from_flow_rate_count(count=flow_rate_count, kval=fm_kval)

        # Compare status versus what is on the substation
        if abs(float(self.vr) - float(calculated_flow_rate)) > .5:
            e_msg = "Unable to verify {0} ({1})'s flow rate. Received: {2}, Expected: {3}".format(
                self.ty,            # {0}
                self.sn,            # {1}
                str(calculated_flow_rate),     # {2}
                str(self.vr)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} ({1})'s flow rate: '{2}' on substation".format(
                self.ty,          # {0}
                self.sn,          # {1}
                str(self.vr)      # {2}
            ))
            return True

    #################################
    def verify_water_usage(self, _data=None):
        """
        Verifies water usage for this Flow Meter on the Substation. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        :return:
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Gets the water usage from the data object (we cast to float first because a string with '0.00' cannot be
        # converted to an int
        water_usage_count = int(float(data.get_value_string_by_key(opcodes.flow_usage_count)))

        # Get K-Value from FlowMeter on Controller
        fm_kval = self.fm_on_cn.kv

        calculated_water_usage = helper_equations.calculate_gallons_from_flow_usage_count(count=water_usage_count,
                                                                                          kval=fm_kval)

        # Compare status versus what is on the substation
        if abs(self.vg - calculated_water_usage) > .5:
            e_msg = "Unable to verify {0} ({1})'s water usage. Received: {2}, Expected: {3}".format(
                self.ty,            # {0}
                self.sn,            # {1}
                str(calculated_water_usage),   # {2}
                str(self.vg)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} ({1})'s water usage: '{2}' on substation".format(
                self.ty,          # {0}
                self.sn,          # {1}
                str(self.vg)      # {2}
            ))
            return True

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str
        """
        data = self.get_data()

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify flow specific attributes
        self.verify_serial_number(_data=data)
        self.verify_flow_rate(_data=data)
        self.verify_water_usage(_data=data)
        self.verify_two_wire_drop_value(_data=data)
        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################
