from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
import time
from datetime import datetime, timedelta

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.variables.common import dictionary_for_status_codes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

__author__ = 'bens'


class Substation(object):
    """
    Specify the types of bicoder dictionaries. You can really specify the type for each attribute for a Substation here
    if needed. But I figured these were the only attributes for now that type specification was necessary. That way 
    we can have access to bicoder methods in the use_case as so:
    
        - self.config.substations[1].valve_bicoders['TSD0001'].foo()
        
    :type valve_bicoders: dict[str, common.objects.substation.valve_bicoder.ValveBicoder]
    :type flow_bicoders: dict[str, common.objects.substation.flow_bicoder.FlowBicoder]
    :type pump_bicoders: dict[str, common.objects.substation.pump_bicoder.PumpBicoder]
    :type switch_bicoders: dict[str, common.objects.substation.switch_bicoder.SwitchBicoder]
    :type moisture_bicoders: dict[str, common.objects.substation.moisture_bicoder.MoistureBicoder]
    :type temp_bicoders: dict[str, common.objects.substation.temp_bicoder.TempBicoder]
    """

    def __init__(self, _ip, _mac, _sn, _ds, _ser, _vr=None):
        """

        :param _ip: IP Address for Substation
        :type _ip: str
        
        :param _mac: MAC Address
        :type _mac: str

        :param _sn: Serial Number for Substation
        :type _sn: str
        
        :param _ds: Description
        :type _ds: str
        
        :param _ser: Serial Connection
        :type _ser: common.objects.base_classes.ser.Ser

        :param _vr: Code Version
        :type _vr: str
        """
        self.ser = _ser     # Serial Connection

        # Substation Attributes
        self.ad = _ip       # IP Address
        self.mac = _mac     # MAC Address
        self.sn = _sn       # Serial Number
        # Verify the description is less than 32 characters
        self.verify_description_length(_desc=_ds)
        self.ds = _ds       # Description
        self.la = 43.609768     # Latitude
        self.lg = -116.310569   # Longitude

        # Read Only Attributes
        self.vr = _vr                   # Code Version
        self.ty = opcodes.substation    # substation Type
        self.controller_type = opcodes.substation   # substation Type (again for the sake of compatibility in messages
        self.ss = ''                    # Status

        # Substation BiCoder serial numbers assigned.
        self.d1_bicoders = list()        # Single valve
        self.d2_bicoders = list()        # Dual valve
        self.d4_bicoders = list()        # Quad valve
        self.dd_bicoders = list()        # Twelve valve
        self.ms_bicoders = list()        # Moisture bicoder serial numbers
        self.fm_bicoders = list()        # Flow bicoder serial numbers
        self.ts_bicoders = list()        # Temperature bicoder serial numbers
        self.sw_bicoders = list()        # Switch bicoder serial numbers

        # Substation BiCoder object dictionaries
        self.valve_bicoders = dict()
        self.flow_bicoders = dict()
        self.pump_bicoders = dict()
        self.switch_bicoders = dict()
        self.moisture_bicoders = dict()
        self.temp_bicoders = dict()

        # Attribute so we can store the messages
        self.build_message_string = ''  # place to store the message that is compared to the substation message

        self.set_default_values()

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        command = "SET,CN,SN={0},DS={1},LA={2},LG={3}".format(
            self.sn,  # {0}
            self.ds,  # {1}
            self.la,  # {2}
            self.lg  # {3}
        )

        try:
            # Attempt to set Controller default values
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Substation {0}'s 'Default values' to: '{1}'. " \
                    "Exception received: {2}".format(self.mac, command, e.message)
            raise Exception(e_msg)
        else:
            print("Successfully set Substation {0}'s 'Default values' to: {1}".format(self.mac, command))

    #################################
    def set_serial_number(self, _serial_num):
        """
        Sets the serial number of the Substation. The current instance value can be overwritten by passing in a value
        for _serial_num. \n

        :param _serial_num: serial number of the substation. \n
        :type _serial_num: str
        :return:
        """
        # take the serial number that is passed in from the use case strip off the last 5 characters
        # take the new serial number passed is in to overwrite the old and strip of the first two characters
        # add the two together giving you a new serial number this allow the first to character to be kept the same
        # value from what you passed in at the use case level

        # first take the serial number a see if it is a string
        if not isinstance(_serial_num, str):
            e_msg = "Serial number must be in a string format '{0}'".format(
                str(_serial_num))
            raise TypeError(e_msg)

        mn_serial = self.sn[:2]
        serial_nm = _serial_num[2:]
        new_sn = str(mn_serial + serial_nm)

        # count the number of characters in the string
        if len(new_sn) != 7:
            raise ValueError("Substation serial number must be a seven digit string " +
                             str(new_sn))
        else:
            # Overwrite substation serial number with new value
            self.sn = new_sn

        # Command for sending
        command = "{0},{1},{2}={3}".format(
            opcodes.set_action,     # {0} SET
            opcodes.controller,     # {1} CN
            opcodes.serial_number,  # {2} SN
            str(self.sn)            # {3} Serial number
        )

        try:
            # Attempt to set serial number of the substation
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Substations's ({0}) 'Serial number' to: '{1}'".format(
                self.mac,
                str(new_sn))
            raise Exception(e_msg)
        else:
            print("Successfully set Substation ({0}) 'Serial Number' to: {1}".format(self.mac, str(self.sn)))

    #################################
    def set_description(self, _description=None):
        """
        Sets the description of the Substation. \n

        :param _description: Description to overwrite current object description. \n
        :type _description: str
        """
        # If a description is passed in
        if _description is not None:
            self.ds = str(_description)

        # Verifies that the description passed in is the correct size for a Substation
        if len(self.ds) > 31:
            raise ValueError("Attempted to set a description to Substation ({0}) that is too long: {1}".format(
                self.mac,           # MAC Address
                str(len(self.ds)))  # Description to be set
            )

        # Build the command for the Substation
        command = "SET,{0},{1}={2}".format(opcodes.controller, opcodes.description, str(self.ds))

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Substation's {0}'s ds to: {1}".format(self.mac, self.ds)
            raise Exception(e_msg)
        else:
            print("Successfully set description for Substation ({0}) to: {1}".format(self.mac, self.ds))

    #############################
    def set_date_and_time(self, _date, _time, update_date_mngr=True):
        """
        Set the date and time for the Substation. \n
        :param _date:       Date for substation 'MM/DD/YYYY' format. \n
        :param _time:       Time for substation 'HH:MM:SS' format. \n
        :param update_date_mngr: Flag set when updating controller and substations at the same time.
        :return:
        """
        print ('Set Substation date and time to ' + str(_date) + ' ' + str(_time))
        try:
            datetime.strptime(_date, '%m/%d/%Y')
        except ValueError:
            raise ValueError("Incorrect data format, should be MM/DD/YYYY")
        try:
            datetime.strptime(_time, '%H:%M:%S')
        except ValueError:
            raise ValueError("Incorrect data format, should be HH:MM:SS")

        try:
            self.ser.send_and_wait_for_reply('SET,DT=' + str(_date) + ' ' + str(_time))

            if update_date_mngr:
                date = datetime.strptime(_date, '%m/%d/%Y').date()
                date_mngr.controller_datetime.set_from_datetime_obj(datetime_obj=date, time_string=_time)
                date_mngr.curr_day.set_from_datetime_obj(datetime_obj=date, time_string=_time)
        except Exception:
            e_msg = "Exception occurred trying to set Substation's ({0}) Date and Time to: '{1}' '{2}'" \
                .format(str(self.mac), str(_date), str(_time))
            raise Exception(e_msg)
        else:
            print("Successfully set substation's Date and time to: {0} {1}".format(str(_date), str(_time)))

    #################################
    def set_message(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        :param _helper_object
        :type _helper_object : BaseStartStopPause
        """
        try:
            build_message_string = messages.message_to_set(self,
                                                           _status_code=_status_code,
                                                           _ct_type=opcodes.controller,
                                                           _helper_object=_helper_object)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.set_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=build_message_string)

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_description(self, _data=None):
        """
        Verifies the description set on the Substation. \n

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Get the description from the data object
        ds_on_sb = data.get_value_string_by_key('DS')

        # Compare descriptions
        if self.ds != ds_on_sb:
            e_msg = "Unable to verify Substation's ({0}) description. Received: {1}, Expected: {2}".format(
                self.mac,       # {0}
                str(ds_on_sb),  # {1}
                str(self.ds)    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Substation's ({0}) description: '{1}' on substation".format(
                self.mac,   # {0}
                self.ds     # {1}
            ))
            return True

    #################################
    def verify_status(self, _status, _data=None):
        """
        Verifies the status on the Substation. \n

        :param _status: Status that we want to verify on the Substation. \n
        :type _status: str

        :param _data: Data Object that holds the substation's attributes. If it isn't passed in we call get_data \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        if _status not in dictionary_for_status_codes:
            e_msg = "Unable to verify Substation's {0} status. Received invalid expected status: ({1}),\n" \
                    " Expected one of the following: ({2})".format(
                        str(self.mac),                          # {0}
                        str(_status),                           # {1}
                        dictionary_for_status_codes.keys()      # {2}
                    )
            raise KeyError(e_msg)

        # Get the status from the data object
        dv_ss_on_sb = data.get_value_string_by_key(opcodes.status_code)

        # Since status is a 'get' only, we want to overwrite our object with the expected status passed in to retain
        # the state of our object.
        self.ss = _status

        # Compare status versus what is on the substation
        if self.ss != dv_ss_on_sb:
            e_msg = "Unable to verify Substation's ({0}) status. Received: {1}, Expected: {2}".format(
                self.mac,           # {0}
                str(dv_ss_on_sb),   # {1}
                str(self.ss)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Substation's ({0}) status: '{1}' on substation".format(
                self.mac,   # {0}
                self.ss     # {1}
            ))
            return True

    #################################
    def verify_serial_number(self, _data=None):
        """
        Verifies the Serial Number set on the Substation. \n

        :param _data: Data Object that holds the substation's attributes. If it isn't passed in we call get_data \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Get the serial number from the data object
        sn_on_sb = data.get_value_string_by_key('SN')

        # Compare Serial Numbers
        if self.sn != sn_on_sb:
            e_msg = "Unable to verify Substation's ({0}) 'Serial Number'. Received: {1}, Expected: {2}".format(
                self.mac,       # {0}
                str(sn_on_sb),  # {1}
                str(self.sn)    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Substation's ({0}) Serial Number: '{1}' on substation".format(
                self.mac,       # {0}
                str(self.sn)    # {2}
            ))
            return True

    #################################
    def verify_code_version(self, _data=None):
        """
        Verifies the firmware version of the Substation. \n

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Get the description from the data object
        vr_on_sb = data.get_value_string_by_key('VR')

        # Compare descriptions
        if self.vr != vr_on_sb:
            e_msg = "Unable to verify Substation's ({0}) firmware version. Received: {1}, Expected: {2}".format(
                self.mac,       # {0}
                str(vr_on_sb),  # {1}
                str(self.vr)    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Substation's ({0}) firmware version: '{1}' on substation".format(
                self.mac,   # {0}
                self.vr     # {1}
            ))
            return True

    #################################
    def verify_date_and_time(self):
        """
        Verify Substation object date and time against the Substation date and time \n
        """
        substation_date_and_time_str = self.get_date_and_time().get_value_string_by_key(opcodes.date_time)
        substation_date_and_time = datetime.strptime(substation_date_and_time_str, "%m/%d/%Y %H:%M:%S")
        substation_object_date_time = datetime.combine(date_mngr.controller_datetime.obj,
                                                       date_mngr.controller_datetime.time_obj.obj)

        if substation_date_and_time != substation_object_date_time:
            if abs(substation_object_date_time - substation_date_and_time) >= timedelta(days=0,
                                                                                        hours=0,
                                                                                        minutes=0,
                                                                                        seconds=60):
                e_msg = "The date and time of the substation didn't match the substation object:\n" \
                        "\tSubstation Object Date Time:       \t\t'{0}'\n" \
                        "\tDate Time Received From Substation:\t\t'{1}'\n".format(
                            substation_object_date_time,  # {0} The date of the substation object
                            substation_date_and_time      # {1} The date the substation has
                        )
                raise ValueError(e_msg)
            # only print this if they were not exact
            e_msg = "############################  Date and time were not exact but were within +- 60 Seconds \n" \
                    "\tSubstation Object Date Time:       \t\t'{0}'\n" \
                    "\tDate Time Received From Substation:\t\t'{1}'\n".format(
                        substation_object_date_time,  # {0} The date of the substation object
                        substation_date_and_time      # {1} The date the substation has
                    )
            print(e_msg)
        else:
            print "Verified substation current date and time against substation object date time:\n" \
                  "\tSubstation Object Date Time:       \t\t'{0}'\n" \
                  "\tDate Time Received From Substation:\t\t'{1}'\n".format(
                        substation_object_date_time,  # {0} The date of the substation object
                        substation_date_and_time      # {1} The date the substation has
                    )

    #################################
    def verify_connection_to_controller(self, _expected_status):
        """
        Verifies the connection to a controller from a substation.
        
        :param _expected_status: Expected status to verify against.
        :type _expected_status: str
        
        :return: 
        :rtype: bool
        """
        # TODO: Currently, substation doesn't have a test command to get connection status to controller. For now,
        # TODO: it seems that the BaseManager status on the Substation has a direct correlation for when the
        # TODO: controller is connected. So use BaseManager status for verification for now.
        command = "GET,{0}".format(opcodes.basemanager)

        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to get Substation {0}'s 'Controller Connection " \
                    "Status'.".format(self.ad)
            raise Exception(e_msg)

        else:
            cn_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if _expected_status != cn_ss_on_cn:
                e_msg = "Exception occurred trying to verify Substation {0}'s 'Controller Connection Status'. " \
                        "Expected Status: '{1}', Received: '{2}'.".format(
                            self.ad,
                            _expected_status,
                            cn_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified Substation {0}'s 'Controller Connection Status'. Status Received: " \
                      "'{1}'.".format(self.ad, _expected_status)
                return True

    #################################
    def verify_message(self, _status_code, _helper_object=None):
        """
        The expected values are retrieved from the substation and than compared against the values that are returned
        from the message module. \n

        :param _status_code: The status code we will pass to the substation
        :type _status_code: str

        :param _helper_object:
        :type _helper_object:
        :return:
        """
        mg_on_sb = self.get_message(_status_code=_status_code, _helper_object=_helper_object)
        expected_message_text_from_sb = mg_on_sb.get_value_string_by_key(opcodes.message_text)
        expected_date_time_from_sb = mg_on_sb.get_value_string_by_key(opcodes.date_time)

        mess_date_time = datetime.strptime(self.build_message_string[opcodes.date_time], '%m/%d/%y %H:%M:%S')
        substation_date_time = datetime.strptime(expected_date_time_from_sb, '%m/%d/%y %H:%M:%S')
        if mess_date_time != substation_date_time:
            if abs(substation_date_time - mess_date_time) >= timedelta(days=0, hours=0, minutes=60, seconds=0):
                e_msg = "The date and time of the message didn't match the substation:\n" \
                        "\tCreated: \t\t'{0}'\n" \
                        "\tReceived:\t\t'{1}'\n".format(
                    self.build_message_string[opcodes.date_time],  # {0} The date message that was built
                    expected_date_time_from_sb  # {1} The date message returned from substation
                )
                raise ValueError(e_msg)
            # only print this if they were not exact
            e_msg = "############################  Date and time were not exact but were within +- 30 minutes \n" \
                    "############################  Substation Date and Time = {0} \n" \
                    "############################  Message Date time = {1}".format(
                expected_date_time_from_sb,  # {0} Date time received from substation
                self.build_message_string[opcodes.date_time]  # {1} Date time the message was verified
            )
            print(e_msg)

        if self.build_message_string[opcodes.message_text] != expected_message_text_from_sb:
            e_msg = "Created TX message did not match the TX received from the substation:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                self.build_message_string[opcodes.message_text],  # {0} The TX message that was built
                expected_message_text_from_sb  # {1} The TX message returned from substation
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified:\n" \
                  "\tID: '{0}'\n" \
                  "\tDT: '{1}'\n" \
                  "\tTX: '{2}'\n".format(
                self.build_message_string[opcodes.message_id],
                self.build_message_string[opcodes.date_time],
                self.build_message_string[opcodes.message_text]
            )

    #################################
    def verify_description_length(self, _desc):
        """
        Verify the length of the description before sending to the substation. \n
        :return:
        """
        max_chars = 31      # Substation Max description length

        length = len(_desc)

        # Compare lengths, if _desc is within length requirements, return True (valid description)
        if length > max_chars:
            e_msg = "Substation description '{0}' is too long. Max description length for Substation: {1}, " \
                    "Current length: {2}".format(
                        str(_desc),         # {0}
                        str(max_chars),     # {1}
                        str(length)         # {2}
                    )
            raise ValueError(e_msg)
        else:
            return True

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this Substation. \n

        :param _expected_status:    Expected status to compare against. \n
        :type _expected_status:     str \n
        :return:
        """
        # Get all information about the device from the substation.
        data = self.get_data()

        # Verify base attributes
        # self.verify_date_and_time()  # verify date time object against current date time of the substation
        self.verify_description(_data=data)
        self.verify_serial_number(_data=data)

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify 'Substation' specific attributes
        self.verify_code_version(_data=data)

    #################################
    def verify_full_configuration(self):
        """
        Verifies itself and all of the assigned bicoders.
        """
        self.verify_who_i_am()

        # Set default values for valve bicoders
        for valve_sn in self.valve_bicoders.keys():
            self.valve_bicoders[valve_sn].verify_who_i_am()

        # Set default values for flow bicoders
        for flow_sn in self.flow_bicoders.keys():
            self.flow_bicoders[flow_sn].verify_who_i_am()

        # Set default values for switch bicoders
        for switch_sn in self.switch_bicoders.keys():
            self.switch_bicoders[switch_sn].verify_who_i_am()

        # Set default values for moisture bicoders
        for moisture_sn in self.moisture_bicoders.keys():
            self.moisture_bicoders[moisture_sn].verify_who_i_am()

        # Set default values for temperature bicoders
        for temperature_sn in self.temp_bicoders.keys():
            self.temp_bicoders[temperature_sn].verify_who_i_am()

        # TODO... Fix??
        # Set default values for pump bicoders
        # for pump_sn in self.pump_bicoders.keys():
        #     self.pump_bicoders[pump_sn].verify_who_i_am()
        
        

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #############################
    def init_sb(self):
        """
        initiate substation to a known state so that it doesnt have a configuration or any biCoder loaded
        turn on echo so the commands are displayed in the console
        turn on sim mode so the clock can be stopped
        stop the clock
        Set the date and time so  that the substation is in a known state
        turn on faux IO
        clear all devices
        Clear all programming
        """

        # turn on echo
        try:
            # turn on echo so the command will display in the debugger
            print "Turn on echo"
            try:
                self.turn_on_echo()
            except AssertionError, ae:
                raise AssertionError("Turn on Echo Command Failed: " + ae.message)
            try:
                self.set_sim_mode_to_on()
            except AssertionError, ae:
                raise AssertionError("Turn on sim Mode to substation failed: " + ae.message)
            # stop the clock
            print "Stop the Clock"
            try:
                self.stop_clock()
            except AssertionError, ae:
                raise AssertionError("stop Command Failed: " + ae.message)
            print "Set date and Time of the substation to 1/1/2014"
            try:
                self.set_date_and_time(_date='01/01/2014', _time='01:00:00')
            except AssertionError, ae:
                raise AssertionError("stop Command Failed: " + ae.message)
            # turn on faux io so that the substation can simulate two-wired IO
            try:
                self.turn_on_faux_io()
            except AssertionError, ae:
                raise AssertionError("Turn on FauxIO Command Failed: " + ae.message)
            # clear all biCoders that are in the substation so a clean test can be performed
            print "Clear all devices from substation"
            try:
                self.clear_all_devices()
                # self.ser.send_and_wait_for_reply('DEV,CL=AL')
            except AssertionError, ae:
                raise AssertionError("Clear All Devices Command Failed: " + ae.message)
            # clear all programming in the substation so a clean test can be performed
            print "Clear all programming from substation"
            try:
                self.clear_all_programming()
                # self.ser.send_and_wait_for_reply('DO,CL=AL')
            except AssertionError, ae:
                raise AssertionError("Clear All Programming Command Failed: " + ae.message)
            print "-------------------------------------------------------"
            print "     | - Successfully initiated substation. - |        "
            print "-------------------------------------------------------"
        except AssertionError, ae:
            raise AssertionError("Initiate Substation Command Failed: " + ae.message)

    #################################
    def clear_all_devices(self):
        """
        Convenience method to clear devices.
        :return: 
        """
        print "Clear all devices on Substation {0}".format(self.mac)
        try:
            self.ser.send_and_wait_for_reply('DEV,CL=AL')
            print "-------------------------------------------------------"
            print "| - Successfully Cleared All devices on Substation. - | "
            print "-------------------------------------------------------"
        except Exception, ae:
            raise Exception("Clear all devices Command Failed: " + ae.message)

    #################################
    def clear_all_programming(self):
        """
        Convenience method to clear all programming. 
        :return: 
        """
        print "Clear all programming on Substation {0}".format(self.mac)
        try:
            self.ser.send_and_wait_for_reply('DO,CL=AL')
            print "-----------------------------------------------------------"
            print "| - Successfully Cleared All Programming on Substation. - | "
            print "-----------------------------------------------------------"
        except Exception, ae:
            raise Exception("Clear All Programming Command Failed: " + ae.message)

    #################################
    def get_date_and_time(self):
        """
        Get the date and time on the Substation. \n
        """
        try:
            command = "GET,{0}".format(
                opcodes.date_time  # {0}
            )
            substation_date_time = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Getting Substation ({0}) date time command failed".format(self.mac)
            raise Exception(e_msg)
        else:
            print "Successfully Retrieved the Substation ({0}) Date and Time".format(self.mac)
            return substation_date_time

    # TODO: For substation, a search is never required.
    #############################
    # def do_search_for_dv(self, dv_type):
    #     """
    #     Tells the Substation to search for two-wire devices of the specified type. \n
    #     """
    #     # Validate decoder type passed in against accepted types
    #     if dv_type not in [opcodes.zone, opcodes.moisture_sensor, opcodes.flow_meter, opcodes.master_valve,
    #                        opcodes.temperature_sensor, opcodes.event_switch, opcodes.four_to_20_milliamp_sensor_decoder,
    #                        opcodes.alert_relay]:
    #         raise ValueError("[CONTROLLER] incorrect two-wire device type passed in: " + str(dv_type))
    #
    #     print "Search for device type: " + str(dv_type)
    #     try:
    #         command = "DO,SR=" + str(dv_type)
    #         self.ser.send_and_wait_for_reply(command)
    #     except Exception as e:
    #         print(e.message)
    #         raise Exception("Searching for two-wire devices failed.. " + str(e.message))

    # TODO: For substation, a search is never required.
    #############################
    # def do_search_for_all_devices(self):
    #     """
    #     Searches for all available devices on the two wire.
    #     :return:
    #     """
    #     try:
    #         self.do_search_for_dv(dv_type=opcodes.zone)
    #         self.do_search_for_dv(dv_type=opcodes.master_valve)
    #         self.do_search_for_dv(dv_type=opcodes.moisture_sensor)
    #         self.do_search_for_dv(dv_type=opcodes.temperature_sensor)
    #         self.do_search_for_dv(dv_type=opcodes.event_switch)
    #         self.do_search_for_dv(dv_type=opcodes.flow_meter)
    #     except Exception as e:
    #         # Re-raise caught exception with additional info plus the exception info.
    #         appended_msg = "Exception occurred searching for all available devices on Substation {0}: {1}".format(
    #             self.mac,
    #             e.message
    #         )
    #         raise Exception(appended_msg)
    #     else:
    #         print "[SUCCESS] Searched for all available devices on Substation {0}".format(self.mac)

    #############################
    def load_assigned_devices(self, flow_bicoder_version=None):
        """
        Helper method. Loads all devices from the configuration file into the controller. \n
        :return: 
        """
        print "Loading all devices from configuration into the Substation {0}".format(self.mac)
        try:
            self.load_dv_to_cn(dv_type="D1", list_of_decoder_serial_nums=self.d1_bicoders)
            self.load_dv_to_cn(dv_type="D2", list_of_decoder_serial_nums=self.d2_bicoders)
            self.load_dv_to_cn(dv_type="D4", list_of_decoder_serial_nums=self.d4_bicoders)
            self.load_dv_to_cn(dv_type="DD", list_of_decoder_serial_nums=self.dd_bicoders)
            self.load_dv_to_cn(dv_type="MS", list_of_decoder_serial_nums=self.ms_bicoders)
            self.load_dv_to_cn(dv_type="FM", list_of_decoder_serial_nums=self.fm_bicoders, flow_bicoder_version=flow_bicoder_version)
            self.load_dv_to_cn(dv_type="TS", list_of_decoder_serial_nums=self.ts_bicoders)
            self.load_dv_to_cn(dv_type="SW", list_of_decoder_serial_nums=self.sw_bicoders)
        except Exception as e:
            # Re-raise caught exception with additional info plus the exception info.
            appended_msg = "Exception occurred loading all available devices to Substation {0}: {1}".format(
                                self.mac,
                                e.message
                            )
            raise Exception(appended_msg)
        else:
            # Added a sleep here with a theory that the controller was attempting to search for devices when the thread
            # spun to load all of the devices wasn't finished. Thus when we attempted to search and assign the first
            # zone, a BC was returned
            time.sleep(10)
            print "[SUCCESS] Loaded all available devices to Substation {0}".format(self.mac)

    #############################
    def load_all_dv_to_cn(self, d1_list, mv_d1_list, d2_list, mv_d2_list, d4_list, dd_list, ms_list, fm_list, ts_list,
                          sw_list, is_list, ar_list):
        """
        Helper method. Loads all devices from the configuration file into the Substation. \n
        had to have mv_d1_list and mv_d2_list in order to loader decoder that can be used as master valves\n
        master valve can only be single or dual decoders
        :param d1_list:
        :param mv_d1_list
        :param d2_list:
        :param mv_d2_list
        :param d4_list:
        :param dd_list:
        :param ms_list:
        :param fm_list:
        :param ts_list:
        :param sw_list:
        :param is_list:
        :param ar_list:
        :return:
        """
        print "Loading all devices from configuration on Substation {0}".format(self.mac)
        try:
            self.load_dv_to_cn(dv_type="D1", list_of_decoder_serial_nums=d1_list)
            self.load_dv_to_cn(dv_type="D1", list_of_decoder_serial_nums=mv_d1_list)
            self.load_dv_to_cn(dv_type="D2", list_of_decoder_serial_nums=d2_list)
            self.load_dv_to_cn(dv_type="D2", list_of_decoder_serial_nums=mv_d2_list)
            self.load_dv_to_cn(dv_type="D4", list_of_decoder_serial_nums=d4_list)
            self.load_dv_to_cn(dv_type="DD", list_of_decoder_serial_nums=dd_list)
            self.load_dv_to_cn(dv_type="MS", list_of_decoder_serial_nums=ms_list)
            self.load_dv_to_cn(dv_type="FM", list_of_decoder_serial_nums=fm_list)
            self.load_dv_to_cn(dv_type="TS", list_of_decoder_serial_nums=ts_list)
            self.load_dv_to_cn(dv_type="SW", list_of_decoder_serial_nums=sw_list)
            self.load_dv_to_cn(dv_type="AR", list_of_decoder_serial_nums=is_list)
            self.load_dv_to_cn(dv_type="IS", list_of_decoder_serial_nums=ar_list)
        except Exception as e:
            # Re-raise caught exception with additional info plus the exception info.
            appended_msg = "Exception occurred loading all available devices to Substation: " + str(e.message)
            raise Exception(appended_msg)
        else:
            # Added a sleep here with a theory that the substation was attempting to search for devices when the thread
            # spun to load all of the devices wasn't finished. Thus when we attempted to search and assign the first
            # zone, a BC was returned
            time.sleep(10)
            print "[SUCCESS] Loaded all available devices to Substation."

    #############################
    def load_dv_to_cn(self, dv_type, list_of_decoder_serial_nums, flow_bicoder_version=None):
        """
        Pass in the decoder type and serial number
        Pass that value to the verify functions

        :param dv_type: Type of device to load. \n
        :type dv_type: str

        :param list_of_decoder_serial_nums: List of all the serial numbers of a certain device type
        :type list_of_decoder_serial_nums: list[str]
        """
        # Validate decoder type passed in against accepted types
        if dv_type not in [opcodes.single_valve_decoder, opcodes.two_valve_decoder, opcodes.four_valve_decoder,
                           opcodes.twelve_valve_decoder, opcodes.flow_meter, opcodes.moisture_sensor,
                           opcodes.temperature_sensor, opcodes.four_to_20_milliamp_sensor_decoder,
                           opcodes.event_switch, opcodes.alert_relay]:
            raise ValueError("[CONTROLLER] incorrect decoder type: " + str(dv_type))

        # For each serial number for the decoder type passed in from the list.
        for serial_number in list_of_decoder_serial_nums:

            # Validate Serial Number
            if len(serial_number) != 7:
                # count the number of characters in the string
                raise ValueError("Device id is not a valid serial number: {0}".format(str(serial_number)))
            else:
                print "Loading device of type: {0} with serial: {1}".format(str(dv_type), str(serial_number))
                try:
                    if flow_bicoder_version:
                        command = 'DEV,{0}={1},{2}={3}'.format(dv_type, str(serial_number), "VE", flow_bicoder_version)
                    else:
                        command = 'DEV,{0}={1}'.format(dv_type, str(serial_number))
                    self.ser.send_and_wait_for_reply(command)
                except Exception as e:
                    print(e.message)
                    raise Exception("Load device(s) failed.. " + str(e.message))

    #############################
    def turn_on_faux_io(self):
        """
        Turn on faux io so that the Substation can simulate two-wired IO \n
        :return:
        """
        print "Enable faux IO devices on Substation {0}".format(self.mac)
        try:
            self.ser.send_and_wait_for_reply('DO,FX=TR')
            print "-------------------------------------------------------"
            print "| - Successfully Turned On Faux IO in Substation.  - | "
            print "-------------------------------------------------------"
        except Exception, ae:
            raise Exception("Turn on FauxIO Command Failed: " + ae.message)

    #############################
    def turn_on_echo(self):
        """
        Turn on Echo this will tell the Substation to echo the packet that was sent to the Substation
        """
        print "Turn on Echo on Substation {0}".format(self.mac)
        try:
            self.ser.send_and_wait_for_reply('DO,EC=TR')
            print "-------------------------------------------------------"
            print "  | - Successfully Turned on echo in Substation.  - |   "
            print "-------------------------------------------------------"
        except Exception, ae:
            raise Exception("Turning on Echo Command Failed: " + ae.message)

    #############################
    def turn_off_echo(self):
        """
        Turn off Echo this will tell the Substation not to echo the packet that was sent to the Substation
        """
        print "Turn off Echo on Substation {0}".format(self.mac)
        try:
            self.ser.send_and_wait_for_reply('DO,EC=FA')
            print "-------------------------------------------------------"
            print "  | - Successfully Turned off echo in Substation.  - |   "
            print "-------------------------------------------------------"
        except Exception, ae:
            raise Exception("Turning off Echo Command Failed: " + ae.message)

    #############################
    def set_sim_mode_to_on(self):
        """
        Put Substation in sim mode \n
        """
        print "Put Substation into sim mode on Substation {0}".format(self.mac)
        try:
            self.ser.send_and_wait_for_reply('DO,SM=TR')
            print "-------------------------------------------------------"
            print "       | - Successfully Turned On Sim Mode.  - |       "
            print "-------------------------------------------------------"
        except Exception as e:
            e_msg = "Turn On Sim Mode Command Failed: " + e.message
            raise Exception(e_msg)

    #############################
    def set_sim_mode_to_off(self):
        """
        Turn off sim mode this will also start the clock but doesnt send second \n
        Command to restart clock because clock restarts when you exit sim mode \n
        """
        print "Turn off sim mode on Substation {0}".format(self.mac)
        try:
            self.ser.send_and_wait_for_reply('DO,SM=FA')
            print "-------------------------------------------------------"
            print "       | - Successfully Turned off Sim Mode.  - |      "
            print "-------------------------------------------------------"
        except Exception as e:
            e_msg = "Turn Off Sim Mode Command Failed: " + str(e.message)
            raise Exception(e_msg)

    #############################
    def stop_clock(self):
        """
        Stop clock for sim mode on Substation \n
        """
        print "Stop Clock on Substation {0}".format(self.mac)
        try:
            self.ser.send_and_wait_for_reply('DO,CK,TM=0')
            print "-------------------------------------------------------"
            print "  | - Successfully Stopped Clock in Substation.  - |   "
            print "-------------------------------------------------------"
        except Exception, ae:
            raise Exception("Stop Clock Command Failed: " + ae.message)

    #############################
    def start_clock(self):
        """
        Start clock for use in browser mode \n
        """
        print "Start Clock on Substation {0}".format(self.mac)
        try:
            self.ser.send_and_wait_for_reply('DO,CK,TM=-1')
        except Exception, ae:
            raise Exception("Start Clock Command Failed: " + ae.message)

    #################################
    def do_increment_clock(self, hours=0, minutes=0, seconds=0, update_date_mngr=True):
        """
        compare each value of hours, minutes, seconds \n
        this allows to send in one of the parameter to increment the clock in the substation \n
        the clock must be stopped in order it increment it
        :param hours:
        :type hours:            int
        :param minutes:
        :type minutes:      int
        :param seconds:
        :type seconds:      int
        :return:            ok from substation
        :rtype:             ok is a str
        """
        print('Increment Clock ' + str(hours) + ' Hours ' + str(minutes) + ' Minutes')
        if hours == 0 and minutes == 0 and seconds == 0:
            e_msg = "Failed trying to increment clock. Invalid type passed in for hours. Received: {0}, " \
                    "Expected: int ".format(str(hours))
            raise ValueError(e_msg)
        if hours != 0:
            if not isinstance(hours, int):
                e_msg = "Failed trying to increment clock. Invalid type passed in for hours. Received: {0}, " \
                        "Expected: int ".format(str(hours))
                raise TypeError(e_msg)
            elif hours not in range(24):
                e_msg = "Failed trying to increment clock. Invalid range for hours. Expected a number between " \
                        "0 - 24, Received: {0},".format(str(hours))
                raise ValueError(e_msg)
            else:
                hours = str(hours).zfill(2)
        if minutes != 0:
            if not isinstance(minutes, int):
                e_msg = "Failed trying to increment clock. Invalid type passed in for minutes. Received: {0}, " \
                        "Expected: int ".format(str(minutes))
                raise TypeError(e_msg)
            elif minutes not in range(60):
                e_msg = "Failed trying to increment clock. Invalid range for minutes. Expected a number between " \
                        "0 - 60, Received: {0},".format(str(minutes))
                raise ValueError(e_msg)
            else:
                minutes = str(minutes).zfill(2)
        if seconds != 0:
            if not isinstance(seconds, int):
                e_msg = "Failed trying to increment clock. Invalid type passed in for seconds. Received: {0}, " \
                        "Expected: int ".format(str(seconds))
                raise TypeError(e_msg)
            elif seconds not in range(60):
                e_msg = "Failed trying to increment clock. Invalid range for seconds. Expected a number between " \
                        "0 - 60, Received: {0},".format(str(seconds))
                raise ValueError(e_msg)
            else:
                seconds = str(seconds).zfill(2)

        # hours input?
        if hours != 0:
            if len(str(hours)) == 1:
                hours_str = "0" + str(hours)
            else:
                hours_str = str(hours)
        else:
            hours_str = "00"

        # minutes input?
        if minutes != 0:
            if len(str(minutes)) == 1:
                minutes_str = "0" + str(minutes)
            else:
                minutes_str = str(minutes)
        else:
            minutes_str = "00"

        # seconds input?
        if seconds != 0:
            if len(str(seconds)) == 1:
                seconds_str = "0" + str(seconds)
            else:
                seconds_str = str(seconds)
        else:
            seconds_str = "00"

        clock = hours_str + ":" + minutes_str + ":" + seconds_str
        datetime.strptime(clock, '%H:%M:%S')

        # `update_date_mngr` flag is toggled when we want to control when date_mngr is incremented.
        if update_date_mngr:

            try:
                date_mngr.controller_datetime.increment_date_time(hours=int(hours),
                                                                  minutes=int(minutes),
                                                                  seconds=int(seconds))
                if date_mngr.curr_day:
                    date_mngr.curr_day.increment_date_time(hours=int(hours),
                                                           minutes=int(minutes),
                                                           seconds=int(seconds))
            except ValueError:
                e_msg = "Failed trying to increment clock. Date time values failed to set in the date_resource " \
                        "module. We passed in three strings representing: hours = {0}, min = {1}, sec = {2}".format(
                            str(hours), str(minutes), str(seconds))
                raise ValueError(e_msg)

        self.ser.send_and_wait_for_reply('DO,CK,TM=' + str(clock))

    #############################
    def do_reboot(self):
        """
        Reboot substation than wait for 10 seconds for the substation to wake back up \n
        """
        print "Reboot Substation {0}".format(self.mac)
        try:
            self.ser.send_and_wait_for_reply('DO,EC=FA')
            print "---------------------------------------------------"
            print "  | - Successfully Rebooted the Substation.  - |   "
            print "---------------------------------------------------"
        except Exception, ae:
            raise Exception("Rebooting Substation {0} Failed: ".format(self.mac) + ae.message)

    #################################
    def get_message(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module

        :param _status_code
        :type _status_code :str

        :param _helper_object
        :type _helper_object: BaseStartStopPause
        """
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=opcodes.controller,
                                                                _helper_object=_helper_object)
            sb_mg = self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            msg = "Exception caught in messages.get_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=self.build_message_string[opcodes.message])
            return sb_mg

    def get_data(self):
        """
        Gets the data for the Substation and returns it as an object we can parse through. \n

        :return: status_parser.KeyValues
        """
        # Build the command to get data from the Substation
        command = "GET,{0}".format(
            opcodes.controller    # {0}
        )

        # Attempt to get data from substation
        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Unable to get data for Substation ({0}) using command: '{1}'. Exception raised: {2}".format(
                self.mac,           # {0}
                command,            # {1}
                str(ae.message)     # {2}
            )
            raise ValueError(e_msg)
        else:
            return data
