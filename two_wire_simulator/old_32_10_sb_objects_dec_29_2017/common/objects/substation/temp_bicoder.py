from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.bicoders import BiCoder

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

__author__ = 'bens'


class TempBicoder(BiCoder):

    def __init__(self, _sn, _ser):
        """
        :param _sn: Serial number to set for BiCoder.
        :type _sn: str
        
        :param _ser: Serial connection from controller
        :type _ser: common.objects.base_classes.ser.Ser
        """
        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(TempBicoder, self).__init__(_serial_number=_sn, _serial_connection=_ser)
        self.ty = self.bicoder_types.TEMPERATURE
        self.id = self.bicoder_ids.TEMPERATURE

        self.vd = 93.1      # Temperature

        # Attributes that we will send to the controller by default
        self.default_attributes = [
            (opcodes.temperature_value, 'vd'),
            (opcodes.two_wire_drop, 'vt')
        ]

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_temperature_reading(self, _temp=None):
        """
        Set the temperature reading for the temperature BiCoder (Faux IO Only) \n

        :param _temp: Value to set the temperature BiCoder temperature reading to \n
        :type _temp: Integer, Float
        """
        # If a temperature value is passed in for overwrite
        if _temp is not None:

            # Verifies the temperature reading passed in is an int or float value
            if not isinstance(_temp, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s temperature reading. Invalid argument type, expected an " \
                        "int or float, received: {2}".format(self.ty, str(self.sn), type(_temp))
                raise TypeError(e_msg)
            else:
                # Overwrite current value
                self.vd = _temp

        # Command for sending
        command = "SET,{0}={1},{2}={3}".format(self.id, str(self.sn), opcodes.temperature_value, str(self.vd))

        try:
            # Attempt to set temperature reading for temperature biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Temperature Reading' to: '{2}'". \
                format(self.ty, str(self.sn), str(self.vd))
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Temperature Reading' to: {2}".format(
                self.ty, str(self.sn), str(self.vd)))

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_temperature_reading(self, _data=None):
        """
        Verifies the temperature reading for this Temperature BiCoder. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Gets the temperature from the data object
        temperature = float(data.get_value_string_by_key(opcodes.temperature_value))

        rounded_vd = round(number=self.vd, ndigits=2)
        if abs(rounded_vd - temperature) > 0.2:
            # Compare status versus what is on the substation
            if self.vd != temperature:
                e_msg = "Unable to verify {0} ({1})'s temperature reading. Received: {2}, Expected: {3}". \
                    format(
                        self.ty,            # {0}
                        str(self.sn),       # {1}
                        str(temperature),   # {2}
                        str(self.vd)        # {3}
                    )
                print(e_msg)
                raise ValueError(e_msg)
        # If no exception is thrown, print a success message
        print("Verified {0} ({1})'s temperature reading: '{2}' on substation".format(
            self.ty,          # {0}
            str(self.sn),     # {1}
            str(self.vd)      # {2}
        ))
        return True

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str
        """
        data = self.get_data()

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify temperature specific attributes
        self.verify_serial_number(_data=data)
        self.verify_temperature_reading(_data=data)
        self.verify_two_wire_drop_value(_data=data)

        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################
