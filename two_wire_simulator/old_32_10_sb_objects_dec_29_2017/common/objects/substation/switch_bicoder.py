from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.bicoders import BiCoder

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

__author__ = 'bens'


class SwitchBicoder(BiCoder):

    def __init__(self, _sn, _ser):
        """
        :param _sn: Serial number to set for BiCoder.
        :type _sn: str
        
        :param _ser: Serial connection from controller
        :type _ser: common.objects.base_classes.ser.Ser
        """
        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(SwitchBicoder, self).__init__(_serial_number=_sn, _serial_connection=_ser)
        self.ty = self.bicoder_types.SWITCH
        self.id = self.bicoder_ids.SWITCH

        self.vc = opcodes.contacts_closed       # Contact State

        # Attributes that we will send to the controller by default
        self.default_attributes = [
            (opcodes.contacts_state, 'vc'),
            (opcodes.two_wire_drop, 'vt')
        ]

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_contact_state(self, _contact_state=None):
        """
        Set the contact for the switch BiCoder (Faux IO Only) \n

        :param _contact_state: Open ('OP') or Closed ('CL') \n
        :type _contact_state: str
        """
        # If a contact switch value is passed in for overwrite
        if _contact_state is not None:

            # Verifies the contact switch value passed in is an int or float value
            if not isinstance(_contact_state, str):
                e_msg = "Failed trying to set {0} ({1})'s contact state. Invalid argument type, expected a " \
                        "str, received: {2}".format(self.ty, str(self.sn), type(_contact_state))
                raise TypeError(e_msg)
            else:
                # Overwrite current value
                self.vc = _contact_state

        # Command for sending
        command = "SET,{0}={1},{2}={3}".format(self.id, str(self.sn), opcodes.contacts_state, str(self.vc))

        try:
            # Attempt to set contact state for switch biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Contact State' to: '{2}'". \
                format(self.ty, str(self.sn), str(self.vc))
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Contact State' to: {2}".format(
                self.ty, str(self.sn), str(self.vc)))

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_contact_state(self, _data=None):
        """
        Verifies the contact state for this Switch BiCoder. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Gets the contact state from the data object
        contact_state = data.get_value_string_by_key(opcodes.contacts_state)

        # Compare status versus what is on the controller
        if self.vc != contact_state:
            e_msg = "Unable to verify {0} ({1})'s contact state reading. Received: {2}, Expected: {3}".format(
                self.ty,                # {0}
                str(self.sn),           # {1}
                str(contact_state),     # {2}
                str(self.vc)            # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} ({1})'s contact state: '{2}' on substation".format(
                self.ty,          # {0}
                str(self.sn),     # {1}
                str(self.vc)      # {2}
            ))
            return True

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str
        """
        data = self.get_data()

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify switch specific attributes
        self.verify_serial_number(_data=data)
        self.verify_contact_state(_data=data)
        self.verify_two_wire_drop_value(_data=data)

        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################
