__author__ = 'baseline'

from old_32_10_sb_objects_dec_29_2017.common import user_configuration
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import ser
from old_32_10_sb_objects_dec_29_2017.common.objects.baseunit import bu, bu_imports, bu_helpers
from old_32_10_sb_objects_dec_29_2017.common.objects.controller import zn, ms, ts, mv, sw, fm


class MFGFactory(object):

    def __init__(self, conf):
        """

        :param conf: User Configuration instance
        :type conf: user_configuration.UserConfiguration
        """
        self._conf = conf

    def get_base_unit_object(self, _ad=1):
        """
        Get a Base Unit Instance
        :return: BaseUnit object
        :rtype:
        """
        try:
            # serial_connection = ser.Ser(comport=self._conf.bu_comport)
            serial_connection = ser.Ser(comport=1)
            serial_connection.init_ser_connection(_baud_rate=9600)
        except Exception as e:
            raise Exception("Exception occurred trying to create serial instance in MFGController: " + str(e.message))
        else:
            this_bu = bu.BaseUnit(_ser=serial_connection)
            this_bu.ad = _ad

            return this_bu

    def get_remote_zone(self, _sn="00R0001", _ad=1, _decoder_type=bu_imports.DeviceTypes.SINGLE_VALVE_STR):
        """
        get a remote zone unit
        :param _sn: serial of remote zone unit
        :type _sn: str
        :param _ad: address of remote zone unit
        :type _ad: int
        :param _decoder_type: type of decoder ('1V' | '2V' | '4V' | '12V' | '24V')
        :type _decoder_type: str
        :return: new zone instance
        :rtype: zn.Zone
        """
        new_zone = zn.Zone(_serial=_sn, _address=_ad)
        new_zone.r_decoder_type = _decoder_type
        return new_zone

    def get_remote_moisture_sensor(self, _sn="00R0001", _ad=1, _decoder_type=bu_imports.DeviceTypes.SINGLE_VALVE_STR):
        """
        get a remote moisture sensor unit
        :param _sn: serial of remote moisture sensor unit
        :type _sn: str
        :param _ad: address of remote moisture sensor unit
        :type _ad: int
        :param _decoder_type: type of decoder ('1V' | '2V' | '4V' | '12V' | '24V')
        :type _decoder_type: str
        :return: new remote moisture sensor unit instance
        :rtype: ms.MoistureSensor
        """
        new_ms = ms.MoistureSensor(_serial=_sn, _address=_ad)
        new_ms.r_decoder_type = _decoder_type
        return new_ms

    def get_remote_temp_sensor(self, _sn="00R0001", _ad=1, _decoder_type=bu_imports.DeviceTypes.SINGLE_VALVE_STR):
        """
        get a remote temperature sensor
        :param _sn: serial of remote temperature sensor unit
        :type _sn: str
        :param _ad: address of remote temperature sensor unit
        :type _ad: int
        :param _decoder_type: type of decoder ('1V' | '2V' | '4V' | '12V' | '24V')
        :type _decoder_type: str
        :return: new remote temperature sensor unit instance
        :rtype: ts.TemperatureSensor
        """
        new_ts = ts.TemperatureSensor(_serial=_sn, _address=_ad)
        new_ts.r_decoder_type = _decoder_type
        return new_ts

    def get_remote_flow_meter(self, _sn="00R0001", _ad=1, _decoder_type=bu_imports.DeviceTypes.SINGLE_VALVE_STR):
        """
        get a remote flow meter object
        :param _sn: serial of flow meter
        :type _sn: str
        :param _ad: address of flow meter
        :type _ad: int
        :param _decoder_type: type of decoder ('1V' | '2V' | '4V' | '12V' | '24V')
        :type _decoder_type: str
        :return: new flow meter instance
        :rtype: fm.FlowMeter
        """
        new_fm = fm.FlowMeter(_serial=_sn, _address=_ad)
        new_fm.r_decoder_type = _decoder_type
        return new_fm

    def get_remote_master_valve(self, _sn="00R0001", _ad=1, _decoder_type=bu_imports.DeviceTypes.SINGLE_VALVE_STR):
        """
        get a remote master valve object
        :param _sn: serial of master valve
        :type _sn: str
        :param _ad: address of master valve
        :type _ad: int
        :param _decoder_type: type of decoder ('1V' | '2V' | '4V' | '12V' | '24V')
        :type _decoder_type: str
        :return: new master valve instance
        :rtype: mv.MasterValve
        """
        new_mv = mv.MasterValve(_serial=_sn, _address=_ad)
        new_mv.r_decoder_type = _decoder_type
        return new_mv

    def get_remote_event_switch(self, _sn="00R0001", _ad=1, _decoder_type=bu_imports.DeviceTypes.SINGLE_VALVE_STR):
        """
        get a remote master valve object
        :param _sn: serial of master valve
        :type _sn: str
        :param _ad: address of master valve
        :type _ad: int
        :param _decoder_type: type of decoder ('1V' | '2V' | '4V' | '12V' | '24V')
        :type _decoder_type: str
        :return: new master valve instance
        :rtype: sw.EventSwitch
        """
        new_sw = sw.EventSwitch(_serial=_sn, _address=_ad)
        new_sw.r_decoder_type = _decoder_type
        return new_sw



