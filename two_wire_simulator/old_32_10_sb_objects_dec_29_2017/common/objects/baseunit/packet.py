__author__ = 'baseline'

import struct


class BUSerialPacket(object):
    """
    Base Unit Serial Packet.

    ################
    Sending Usage:
    ################

    1. First create a BU Serial Packet
    > > > packet_for_send = BUSerialPacket(_address_base=10, _command=BUCommands.Reboot.hexs)

    2. Second, pack the BU Serial Packet into a binary string for sending over the serial
    > > > binary_string_to_send = packet_for_send.pack()

    - 'binary_string_to_send' is now a 14 byte binary string ready to be sent over the serial port to a Base Unit. \n

    ################
    Receiving Usage:
    ################

    1. First create a BU Serial Packet for receiving data into and unpack the data. \n
    > > > packet_for_rec = BUSerialPacket()
    > > > packet_for_rec.unpack(some_string_returned_by_base_unit)

    - The 'unpack()' call assumes that we have received some reply from the Base Unit and stored that reply in a
    string called 'some_string_returned_by_base_unit'. \n
    """

    # ~ Class Variables ~
    max_int = 256               # Max unsigned byte value
    max_size = 14               # max total bytes for a packet
    format = "B" * max_size     # For packing and unpacking the data, see 'python struct formatting api'

    def __init__(self, _address_base=0, _command=0):
        """
        Two Part constructor: \n

            -   Empty Arguments: User can create a 'BUSerialPacket' not passing in an _address_base or _command and
                will have a blank 'BUSerialPacket' returned. (Use Case: for the 'reply' packets.)

            -   Non-empty Arguments: User can specify an _address_base and _command for the packet to assign as
                attributes and will have a addressed 'BUSerialPacket' ready to send. (Use Case: for 'sending' packets.)

        :param _address_base:   Address of BaseUnit \n
        :type _address_base:    int \n

        :param _command:    Command to send to BaseUnit \n
        :type _command:     int | str \n
        """
        self.address = _address_base
        self.command = _command
        self.data0 = 0
        self.data1 = 0
        self.data2 = 0
        self.data3 = 0
        self.data4 = 0
        self.data5 = 0
        self.data6 = 0
        self.data7 = 0
        self.data8 = 0
        self.status = 0
        self.check_sum_lo = 0
        self.check_sum_hi = 0
        self.data_string = ''

        self.packet_struct = struct.Struct(self.format)

    def address_reply(self):
        """
        Returns the reply address for the packet. \n
        :return:    The reply address to send back \n
        :rtype:     int \n
        """
        return self.command

    def compute_check_sum(self):
        """
        Computes the check sum total value without the size of a max int being taken into consideration. In other
        words, adds up the address, command, status and all 9 bytes of data and returns that value. \n
        :return:    The sum of the data for the current packet \n
        :rtype:     int \n
        """
        return (self.address + self.command + self.data0 + self.data1 + self.data2 + self.data3 + self.data4 +
                self.data5 + self.data6 + self.data7 + self.data8 + self.status)

    def compute_check_sum_hi(self):
        """
        Computes the hi byte value for the check sum for the packet. \n
        :return:    The computed check sum hi byte value \n
        :rtype:     int \n
        """
        # return (self.compute_check_sum() / self.max_int)

        try:
            res = self.compute_check_sum() / self.max_int
            return res
        except Exception as e:
            print e.message

    def compute_check_sum_lo(self):
        """
        Computes the lo byte value for the check sum for the packet. \n
        :return:    The computed check sum lo byte value \n
        :rtype:     int \n
        """
        # use 'mod' here to return the remainder of whatever 'self.compute_check_sum()' return divided by 246
        # return (self.compute_check_sum() % self.max_int)

        try:
            res = self.compute_check_sum() % self.max_int
            return res
        except Exception as e:
            print "Check_sum_lo: " + e.message


    def check_sum_valid(self):
        """
        Checks that the current data against current check sum values \n
        :return:    A boolean value, true means valid, false means invalid \n
        :rtype:     bool \n
        """
        return self.compute_check_sum() == self.max_int * self.check_sum_hi + self.check_sum_lo

    def pack(self):
        """
        Packs the current packet into a 14 byte string ready to send over the serial to the Base Unit \n
        :return:    A binary string of converted hex values \n
        :rtype:     str \n
        """
        # compute total's for constructing packet
        self.check_sum_lo = self.compute_check_sum_lo()
        self.check_sum_hi = self.compute_check_sum_hi()

        # print "Packing, {0}".format(
        #     self.text_to_csv()
        # )

        # returns a 14 byte string containing this packet's data in hex values
        return self.packet_struct.pack(
            self.address,
            self.command,
            self.data0,
            self.data1,
            self.data2,
            self.data3,
            self.data4,
            self.data5,
            self.data6,
            self.data7,
            self.data8,
            self.status,
            self.check_sum_lo,
            self.check_sum_hi
        )

    def unpack(self, reply_string):
        """
        Unpacks the string returned by
        :param reply_string:
        :type reply_string:
        :return:
        :rtype:
        """
        returned_tuple = self.packet_struct.unpack(reply_string)

        # Unpack attributes in order from the tuple
        self.address = returned_tuple[0]
        self.command = returned_tuple[1]
        self.data0 = returned_tuple[2]
        self.data1 = returned_tuple[3]
        self.data2 = returned_tuple[4]
        self.data3 = returned_tuple[5]
        self.data4 = returned_tuple[6]
        self.data5 = returned_tuple[7]
        self.data6 = returned_tuple[8]
        self.data7 = returned_tuple[9]
        self.data8 = returned_tuple[10]
        self.status = returned_tuple[11]
        self.check_sum_lo = returned_tuple[12]
        self.check_sum_hi = returned_tuple[13]

        # print "Unpacking, {0}".format(
        #     self.text_to_csv()
        # )

    def text_to_csv(self):
        """
        Returns a string with each byte comma separated with a value starting with byte 0 (address). \n
        :return:    Comma separated string containing values for each stored byte of the packet. \n
        :rtype:     str \n
        """
        # example output: "Packet Data: 1, 193, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 195, 0"
        return "Serial Packet Data: " + \
               self.address.__str__() + ", " + \
               self.command.__str__() + ", " + \
               self.data0.__str__() + ", " + \
               self.data1.__str__() + ", " + \
               self.data2.__str__() + ", " + \
               self.data3.__str__() + ", " + \
               self.data4.__str__() + ", " + \
               self.data5.__str__() + ", " + \
               self.data6.__str__() + ", " + \
               self.data7.__str__() + ", " + \
               self.data8.__str__() + ", " + \
               self.status.__str__() + ", " + \
               self.check_sum_lo.__str__() + ", " + \
               self.check_sum_hi.__str__()

    def populate_data_with_two_wire_packet(self, bu_two_wire_packet):
        """
        Takes in the BU two wire packet and maps that data with the 'BU Serial Packets' data attributes. \n
        :param bu_two_wire_packet:  BU Two Wire Packet to merge \n
        :type bu_two_wire_packet:   BUTwoWirePacket \n
        """
        if not isinstance(bu_two_wire_packet, BUTwoWirePacket):
            e_msg = "Invalid packet type. Expected BU Two Wire Packet instance, received: {0}".format(
                type(bu_two_wire_packet)
            )
            raise TypeError(e_msg)

        else:
            self.data0 = bu_two_wire_packet.r_address
            self.data1 = bu_two_wire_packet.r_command
            self.data2 = bu_two_wire_packet.arg0
            self.data3 = bu_two_wire_packet.arg1
            self.data4 = bu_two_wire_packet.arg2
            self.data5 = bu_two_wire_packet.arg3
            self.data6 = bu_two_wire_packet.status
            self.data7 = bu_two_wire_packet.check_sum_lo
            self.data8 = bu_two_wire_packet.check_sum_hi
            self.status = 0

            # print "BU Serial Packet: Received data from BU Two Wire Packet: {0}".format(
            #     self.text_to_csv()
            # )


class BUTwoWirePacket(object):
    """
    Base Unit Two Wire Packet.

    ################
    Sending Usage:
    ################

    1. First create a BU Serial Packet and if not already done so, a BU Two Wire Packet
    > > > packet_for_send = BUSerialPacket(_address_base=10, _command=BUCommands.Some_command)
    > > > two_wire_packet_ex = BUTwoWirePacket(_remote_address=10, _remote_command=BUCommands.some_command)

    2. Next pass the BU Two Wire Packet data into the BU Serial Packet's 9 data fields
    > > > packet_for_send.populate_data_with_two_wire_packet(bu_two_wire_packet = two_wire_packet_ex)

    3. Pack up the packet for sending over serial port,
    > > > binary_string_to_send = packet_for_send.pack()

    - 'binary_string_to_send' is now a 14 byte binary string ready to be sent over the serial port to a Base Unit. \n

    ################
    Receiving Usage:
    ################

    1. First create a BU Serial Packet for receiving data into and unpack the data. \n
    > > > packet_for_rec = BUSerialPacket()
    > > > packet_for_rec.unpack(some_string_returned_by_base_unit)

        - The 'unpack()' call assumes that we have received some reply from the Base Unit and stored that reply in a
        string called 'some_string_returned_by_base_unit'. \n

    2. Second, create a BU Two Wire Packet for converting from BU Serial Packet
    > > > bu_two_wire_packet = BUTwoWirePacket()

    3. Populate new BU Two Wire Packet with data received over the serial connection stored in the BU Serial Packet
    > > > bu_two_wire_packet.populate_data_from_bu_serial_packet(some_bu_serial_packet_response)
    """

    def __init__(self, _remote_address=0, _remote_command=0):
        self.r_address = _remote_address
        self.r_command = _remote_command
        self.arg0 = 0
        self.arg1 = 0
        self.arg2 = 0
        self.arg3 = 0
        self.status = 0
        self.check_sum_lo = 0
        self.check_sum_hi = 0
        self.data_string = ''

    def address_reply(self):
        """
        Returns the reply address for the packet. \n
        :return:    The reply address to send back \n
        :rtype:     int \n
        """
        return self.r_command

    def text_to_csv(self):
        """
        Returns a string with each byte comma separated with a value starting with byte 0 (address). \n
        :return:    Comma separated string containing values for each stored byte of the packet. \n
        :rtype:     str \n
        """
        # example output: "Packet Data: 1, 193, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 195, 0"
        return "Two Wire Packet Data: " + \
               self.r_address.__str__() + ", " + \
               self.r_command.__str__() + ", " + \
               self.arg0.__str__() + ", " + \
               self.arg1.__str__() + ", " + \
               self.arg2.__str__() + ", " + \
               self.arg3.__str__() + ", " + \
               self.status.__str__() + ", " + \
               self.check_sum_lo.__str__() + ", " + \
               self.check_sum_hi.__str__()

    def populate_data_from_bu_serial_packet(self, bu_serial_packet):
        """
        Populates the BU Two Wire Packet with data from the data attributes of the BU Serial Packet. \n
        :param bu_serial_packet:    BU Serial Packet received from Base Unit (reply) \n
        :type bu_serial_packet:     BUSerialPacket \n
        """
        if not isinstance(bu_serial_packet, BUSerialPacket):
            e_msg = "Invalid packet type. Expected BU Serial Packet instance, received: {0}".format(
                type(bu_serial_packet)
            )
            raise TypeError(e_msg)

        else:
            self.r_address = bu_serial_packet.data0
            self.r_command = bu_serial_packet.data1
            self.arg0 = bu_serial_packet.data2
            self.arg1 = bu_serial_packet.data3
            self.arg2 = bu_serial_packet.data4
            self.arg3 = bu_serial_packet.data5
            self.status = bu_serial_packet.data6
            self.check_sum_lo = bu_serial_packet.data7
            self.check_sum_hi = bu_serial_packet.data8

            print "BU Two Wire packet: Received data from Serial Packet: {0}".format(
                self.text_to_csv()
            )
