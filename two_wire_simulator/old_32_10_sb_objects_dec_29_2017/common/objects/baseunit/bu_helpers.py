__author__ = 'baseline'

import math

import json_handler
from old_32_10_sb_objects_dec_29_2017.common.objects.baseunit import bu_imports
from old_32_10_sb_objects_dec_29_2017.common.objects.baseunit import packet


def get_device_info(_company=bu_imports.CompanyNames.BASELINE, _valve=bu_imports.DeviceTypes.SINGLE_VALVE_STR):
    """
    Returns the version, serial number for the specified decoder valve type and company.
    :param _company: Company name abbreviation
    :type _company: str
    :param _valve: Decoder valve abbreviation (e.g., "12V" or DeviceTypes.TWELVE_VALVE_STR)
    :type _valve: str
    :return: A dictionary object
    :rtype: dict
    """
    info_file = json_handler.get_data(file_name=bu_imports.FileNamePaths.DEVICE_INFO)
    return info_file[_company][_valve]


def get_device_limits(_company=bu_imports.CompanyNames.BASELINE, _valve=bu_imports.DeviceTypes.SINGLE_VALVE_STR):
    """
    Returns the limits for Base, TwoWireDrop, Voltage, Current, and BitSkewMax in a dictionary object for the device
    with specified company and valve type.
    :param _company: Company name abbreviation
    :type _company: str
    :param _valve: Decoder valve abbreviation (e.g., "12V" or DeviceTypes.TWELVE_VALVE_STR)
    :type _valve: str
    :return: A dictionary object
    :rtype: dict
    """
    info_file = json_handler.get_data(file_name=bu_imports.FileNamePaths.DEVICE_LIMITS_BY_VALVES)
    # info_file = json_handler.get_data(file_name=)
    return info_file[_company][_valve]


def get_default_sn(_type):
    """
    :param _type: Decoder Type (e.g., bu_imports.DeviceTypes.SINGLE_VALVE_STR)
    :type _type: str
    :return: Default serial number for decoder
    :rtype: str
    """
    # Case 1: Single Valve
    if _type == bu_imports.DeviceTypes.SINGLE_VALVE_STR:
        return bu_imports.Defaults.SerialNumbers.SINGLE

    # Case 2: Dual Valve
    elif _type == bu_imports.DeviceTypes.DUAL_VALVE_STR:
        return bu_imports.Defaults.SerialNumbers.DUAL

    # Case 3: Quad Valve
    elif _type == bu_imports.DeviceTypes.QUAD_VALVE_STR:
        return bu_imports.Defaults.SerialNumbers.QUAD

    # Case 4: Twelve Valve
    elif _type in [bu_imports.DeviceTypes.TWELVE_VALVE_STR, bu_imports.DeviceTypes.TWENTY_FOUR_VALVE_STR]:
        return bu_imports.Defaults.SerialNumbers.TWELVE

    # Default:
    else:
        return ''


def get_next_sn(_company, _type):
    """
    Gets serial number for valve type from file.
    :param _company: Company for decoder (e.g., bu_imports.CompanyNames.BASELINE)
    :type _company: str
    :param _type: Decoder Type (e.g., bu_imports.DeviceTypes.SINGLE_VALVE_STR)
    :type _type: str
    :return: Serial number for valve type
    :rtype: str | None
    """
    new_sn = None

    # Filename
    fn = bu_imports.FileNamePaths.SERIAL_NUMBERS_IN_USE

    # For DEBUGGING FilePaths
    # import os
    # exists = os.path.exists(fn)
    # print exists

    # Get json data
    sn_file = json_handler.get_data(file_name=fn)

    current_sn_list_for_valve_type = sn_file[_company][_type]

    # Need at least one sn value for the valve type to increment, there SHOULD be 1 default value ALWAYS
    if len(current_sn_list_for_valve_type) >= 1:
        # Get default serial number
        current_sn = current_sn_list_for_valve_type.pop()

        # increment
        new_sn = increment_sn_based_on_type(_current_sn=current_sn, _type=_type)

    return new_sn


def increment_sn_based_on_type(_current_sn, _type):
    """
    Increments the serial number based on valve type
    :param _current_sn: Serial number to increment
    :type _current_sn: str
    :param _type: Type of decoder
    :type _type: str
    :return: Next serial number in sequence
    :rtype: str
    """
    new_sn = None
    sn_as_int = convert_sn_string_to_int(sn_as_string=_current_sn)

    if _type == bu_imports.DeviceTypes.SINGLE_VALVE_STR:
        sn_as_int += 1

    elif _type == bu_imports.DeviceTypes.DUAL_VALVE_STR:
        sn_as_int += 10

    elif _type == bu_imports.DeviceTypes.QUAD_VALVE_STR:
        sn_as_int += 10

    elif _type in [bu_imports.DeviceTypes.TWELVE_VALVE_A_STR, bu_imports.DeviceTypes.TWELVE_VALVE_STR]:
        # create int from characters 2..7 'AANNNNN' -> 'NNNNN'
        temp_int = int(_current_sn[2:]) + 100
        if temp_int >= 99901:
            e_msg = "End of 24R serial numbers...call engineering"
            raise ValueError(e_msg)

        else:
            # Create new serial number, appending first to characters from previous serial number along with new int
            new_sn = _current_sn[:2] + str(temp_int).zfill(5)

    elif _type == bu_imports.DeviceTypes.TWELVE_VALVE_B_STR:
        # replace first to characters of current sn with VB
        new_sn = 'VB' + _current_sn[2:]

    elif _type == bu_imports.DeviceTypes.TWELVE_VALVE_E_STR:
        # replace first to characters of current sn with VE
        new_sn = 'VE' + _current_sn[2:]

    else:
        e_msg = 'Invalid decoder type used for incrementing serial number. SN: {sn}, TYPE: {ty}'.format(
            sn=_current_sn,
            ty=_type
        )
        raise ValueError(e_msg)

    # This checks if we were a SINGLE, DUAL, QUAD decoder type, if so, grab the int value retained in sn_as_int,
    # otherwise, new_sn is set by TWELVE_VALVE+ decoders
    if new_sn is None:
        new_sn = convert_int_to_sn_string(sn_as_int=sn_as_int)

    return new_sn


def put_sn_to_file(_company, _type, _sn):
    """
    Adds the serial number to the file keeping track of used serial numbers
    :param _company: Company for decoder (e.g., bu_imports.CompanyNames.BASELINE)
    :type _company: str
    :param _valve_type: Decoder type (e.g.,  bu_imports.DeviceTypes.SINGLE_VALVE_STR)
    :type _valve_type: str
    :param _sn: Serial number (e.g., "TSD0001")
    :type _sn: str
    """
    # Filename
    fn = bu_imports.FileNamePaths.SERIAL_NUMBERS_IN_USE

    # Get json data
    sn_file = json_handler.get_data(file_name=fn)

    # Add serial to list
    sn_file[_company][_type].append(_sn)

    # Update json file with data
    json_handler.update_data(file_name=bu_imports.FileNamePaths.SERIAL_NUMBERS_IN_USE, data=sn_file)


def reset_file_with_template(_file_name, _template):
    """
    Resets the passed in file name with the respective json content
    :param _file_name: File to be reset
    :type _file_name: str
    :param _template: String representation of dictionary json object
    :type _template: str
    """
    json_handler.update_data(file_name=_file_name, data=_template)


def is_sn_used(_sn):
    """
    Checks _sn against used serial numbers stored in the json file. If the serial number exists returns true.
    :param _sn: Serial number being checked against used serial numbers.
    :type _sn: str
    :return: If serial number is being used (boolean)
    :rtype: bool
    """
    sn_file = json_handler.get_data(file_name=bu_imports.FileNamePaths.SERIAL_NUMBERS_IN_USE)

    # go through each company
    for each_company in sn_file:

        # go through each valve type
        for each_valve_type in sn_file[each_company]:

            # check if _sn is in the list of serial numbers
            if _sn in sn_file[each_company][each_valve_type]:
                return True

    # Return false if serial number not found.
    return False


def get_status_text(_status=bu_imports.BaseErrors.STATUS_OK):
    """
    Returns the text representation of the error code returned by the base/remote unit command.
    :param _status: Received status from packet
    :type _status: int
    :return: Status text for received status
    :rtype: str
    """
    # Communication Errors
    if _status == bu_imports.BaseErrors.COMM_PORT_ERROR:
        return "Serial Port Communications Error"
    elif _status == bu_imports.BaseErrors.FRAME_ERROR:
        return "Serial Port Frame Error"
    elif _status == bu_imports.BaseErrors.NO_RESPONSE_SERIAL:
        return "No Response From BaseUnit"

    # BASE ERRORS
    elif _status == bu_imports.BaseErrors.STATUS_OK:
        return "Success"
    elif _status == bu_imports.BaseErrors.OVER_CURRENT_BASE:
        return "Base Unit Over Current Detected"
    elif _status == bu_imports.BaseErrors.NO_RESPONSE_REMOTE:
        return "No Response From Remote"
    elif _status == bu_imports.BaseErrors.I2C_ERROR_BASE:
        return "Base Unit Function Error"
    elif _status == bu_imports.BaseErrors.ILLEGAL_COMMAND_BASE:
        return "Illegal Base Unit Command"
    elif _status == bu_imports.BaseErrors.COMMAND_REJECTED_BASE:
        return "Command Rejected By BaseUnit"
    elif _status == bu_imports.BaseErrors.CHECKSUM_BAD:
        return "Checksum Error From Remote"
    elif _status == bu_imports.BaseErrors.MORE_DATA_READY:
        return "Additional Data To Read"
    elif _status == bu_imports.BaseErrors.BRIDGE_SLOW_POWER_UP:
        return "Bridge Slow Power Up"
    elif _status == bu_imports.BaseErrors.BRIDGE_OVER_CURRENT:
        return "Bridge Over Current"
    elif _status == bu_imports.BaseErrors.FRAME_ERROR_BASE:
        return "Framing Error From Remote"
    elif _status == bu_imports.BaseErrors.TWO_WIRE_MODE_FAULT:
        return "Two-Wire Mode Error"
    elif _status == bu_imports.BaseErrors.BRIDGE_CHECKSUM:
        return "Bridge-Wire Checksum Error"
    elif _status == bu_imports.BaseErrors.BRIDGE_NO_RESPONSE:
        return "No Response From Remote BaseUnit"

    # REMOTE ERRORS
    elif _status == bu_imports.RemoteErrors.OVER_CURRENT_REMOTE:
        return "Success"
    elif _status == bu_imports.RemoteErrors.BYTE_CIRCUIT:
        return "Base Unit Over Current Detected"
    elif _status == bu_imports.RemoteErrors.OPEN_CIRCUIT:
        return "No Response From Remote"
    elif _status == bu_imports.RemoteErrors.I2C_ERROR_REMOTE:
        return "Base Unit Function Error"
    elif _status == bu_imports.RemoteErrors.COMMAND_REJECTED_REMOTE:
        return "Illegal Base Unit Command"
    elif _status == bu_imports.RemoteErrors.ILLEGAL_COMMAND_REMOTE:
        return "Command Rejected By BaseUnit"
    elif _status == bu_imports.RemoteErrors.ILLEGAL_RESPONSE_REMOTE:
        return "Checksum Error From Remote"
    elif _status == bu_imports.RemoteErrors.PARTIAL_READING:
        return "Additional Data To Read"
    elif _status == bu_imports.RemoteErrors.LIMIT_EXCEEDED:
        return "Bridge Slow Power Up"
    elif _status == bu_imports.RemoteErrors.PORTS_EXCEEDED:
        return "Bridge Over Current"
    elif _status == bu_imports.RemoteErrors.LOW_STARTUP_VOLTAGE:
        return "Framing Error From Remote"
    elif _status == bu_imports.RemoteErrors.DATA_READY:
        return "Two-Wire Mode Error"
    elif _status == bu_imports.RemoteErrors.DATA_OVERFLOW:
        return "Bridge-Wire Checksum Error"
    elif _status == bu_imports.RemoteErrors.NO_AC_VOLTAGE:
        return "No Response From Remote BaseUnit"
    else:
        return "Multiple Or Unknown Error: " + _status


def get_help_text(_help_code=None, _status=bu_imports.BaseErrors.STATUS_OK):
    """
    Returns the help text associated with the help code.
    :param _help_code: Help code
    :type _help_code: int
    :return: Description for help code
    :rtype: str
    """
    if _status == bu_imports.BaseErrors.OVER_CURRENT_BASE:
        return "Check biLine wires and for a shorted decoder."

    else:

        if _help_code == bu_imports.ErrorHelpText.BASE_NO_REPLY:
            return "Check power and serial cable connections to BaseUnit."
        elif _help_code == bu_imports.ErrorHelpText.BASE_WRONG_TYPE:
            return "Check the BaseUnit for Baseline/OEM version."
        elif _help_code in [bu_imports.ErrorHelpText.BASE_POWER_UP, bu_imports.ErrorHelpText.BASE_CURRENT_VOLTAGE]:
            return "Check the BaseUnit power supply, biLine wires, and for a broken decoder."
        elif _help_code in [bu_imports.ErrorHelpText.REMOTE_READ_SN,
                            bu_imports.ErrorHelpText.REMOTE_UNLOCK_SN,
                            bu_imports.ErrorHelpText.REMOTE_VERSION_OEM]:
            return "Check biLine wiring and for a broken decoder."
        elif _help_code == bu_imports.ErrorHelpText.REMOTE_VERSION_BAD:
            return "Check decoder firmware for update or wrong programming."
        elif _help_code in [bu_imports.ErrorHelpText.REMOTE_WRITE_SN,
                            bu_imports.ErrorHelpText.REMOTE_WRITE_ADDRESS,
                            bu_imports.ErrorHelpText.REMOTE_TWO_WIRE_DROP,
                            bu_imports.ErrorHelpText.REMOTE_BIT_TIMES,
                            bu_imports.ErrorHelpText.REMOTE_TURN_ON_VALVE]:
            return "Check decoder connections and for a broken decoder."
        elif _help_code == bu_imports.ErrorHelpText.REMOTE_SENSOR_IO:
            return "Check biSensor connection to 24R and 24R IO circuitry."
        else:
            return "Solve all problems: Unknown Help Code: " + str(_help_code)


def put_sn_into_bu_serial_packet(sn, bu_serial_packet):
    """
    Usage:
        -   Intended to be a wrapper to do the following:
            1.  Convert sn from 7-character string to 32-bit integer
            2.  Convert sn from 32-bit integer into 4 8-bit integer values
            3.  Insert 4 8-bit integer values into bu_serial_packet

    :param sn: Serial number to insert into packet
    :type sn: str

    :param bu_serial_packet: BaseUnit serial packet reference to insert sn into
    :type bu_serial_packet: packet.BUSerialPacket
    """
    # 1
    _32bit_sn_as_int = convert_sn_string_to_int(sn_as_string=sn)

    # 2
    bits_31_to_24, bits_23_to_16, bits_15_to_8, bits_7_to_0 = convert_sn_from_32bit_int_to_4_8bit_ints(arg0=_32bit_sn_as_int)

    # 3
    bu_serial_packet.data2 = bits_7_to_0
    bu_serial_packet.data3 = bits_15_to_8
    bu_serial_packet.data4 = bits_23_to_16
    bu_serial_packet.data5 = bits_31_to_24


def get_sn_from_packet_data(bu_serial_packet):
    """
    Usage:
        -   Intended to be a wrapper to do the following:
            1.  First, pull out the 4 '8-bit' integer values representing the 32-bit serial number as an integer.
            2.  Next, calls 'convert_4_8bit_numbers_to_32bit_number' to merge the 4 '8-bit' ints into a '32-bit' int.
            3.  Finally calls 'convert_int_to_sn_string' to get the 7 character string.
    :param bu_serial_packet: Received Base Unit packet over serial port.
    :type bu_serial_packet: packet.BUSerialPacket
    :return: A 7 character string representing the serial number.
    :rtype: str
    """
    # 1
    bits_7_to_0 = bu_serial_packet.data2
    bits_15_to_8 = bu_serial_packet.data3
    bits_23_to_16 = bu_serial_packet.data4
    bits_31_to_24 = bu_serial_packet.data5

    # 2
    _32bit_sn_as_int = convert_4_8bit_numbers_to_32bit_number(arg0=bits_31_to_24, arg1=bits_23_to_16,
                                                              arg2=bits_15_to_8, arg3=bits_7_to_0)

    # _32bit_sn_as_int = convert_4_8bit_numbers_to_32bit_number(arg0=bits_7_to_0, arg1=bits_15_to_8,
    #                                                           arg2=bits_23_to_16, arg3=bits_31_to_24)

    # 3
    sn_from_32bit_int = convert_int_to_sn_string(sn_as_int=_32bit_sn_as_int)

    # i.e., |=> 'D003987'
    return sn_from_32bit_int


def convert_4_8bit_numbers_to_32bit_number(arg0, arg1, arg2, arg3):
    """
    Convert four integers into a single '32-bit' value. \n
    -   Each arg is 1 byte, 8 bits, 1111 1111 (could be all 0's, or 1's and 0's)
        ie:
            Say a Base Unit replied with a packet containing it's serial number which comes in four 1-byte values. \n
            Packet: (255, 1, 255, 1, 227, 162, 49, 14, 1, 197, 2, 1, 141, 4)

            Let,
                arg3 = 14   (data5 byte in serial packet)
                arg2 = 49   (data4 byte in serial packet)
                arg1 = 162  (data3 byte in serial packet)
                arg0 = 227  (data2 byte in serial packet)

            To make it easier to visualize, this example shows what the binary string of the final 32 bit integer
            looks like during the bit shifting process.

            ** NOTE: Looking left to right at a binary string, the first bit read is the most significant bit at bit
            position 32 (leftmost bit), with the least significant bit at bit position 1 (rightmost bit).

            In binary,
                arg3 in binary: '0000 0000 0000 0000 0000 0000 0000 1110'
                arg2 in binary: '0000 0000 0000 0000 0000 0000 0011 0001'
                arg1 in binary: '0000 0000 0000 0000 0000 0000 1010 0010'
                arg0 in binary: '0000 0000 0000 0000 0000 0000 1110 0011'

            Let our initial string be,
                binary_32_string = '0000 0000 0000 0000 0000 0000 0000 0000'

            Here is the intended outcome,
                binary_32_string = '0000 1110 0011 0001 1010 0010 1110 0011'
                                         arg3      arg2      arg1      arg0

            To get there,
                1.  Shift arg3 left 24 bits so that arg3 occupies bits 32 to 25
                    Result: binary_32_string = '0000 1110 0000 0000 0000 0000 0000 0000'

                2. Shift arg2 left 16 bits so that arg2 occupies bits 24 to 17
                    Result: binary_32_string = '0000 1110 0011 0001 0000 0000 0000 0000'

                3. Shift arg1 left 8 bits so that arg1 occupies bits 16 to 9
                    Result: binary_32_string = '0000 1110 0011 0001 1010 0010 0000 0000'

                4. arg0 doesn't need to be shifted because it currently occupies bits 8 to 1, just add it
                    Result: binary_32_string = '0000 1110 0011 0001 1010 0010 1110 0011'

            Finally,
                binary_32_string = '0000 1110 0011 0001 1010 0010 1110 0011' = 238,133,987

            The non-binary code to achieve this:
                (14 << 24) + (49 << 16) + (162 << 8) + 227 = 238,133,987

    :param arg0: Data2 from serial packet \n
    :type arg0: int \n

    :param arg1: Data3 from serial packet \n
    :type arg1: int \n

    :param arg2: Data4 from serial packet \n
    :type arg2: int \n

    :param arg3: Data5 from serial packet \n
    :type arg3: int \n

    :return: A 32-bit integer value from each 8 bit (1 byte) argument \n
    :rtype: int \n
    """
    bits_31_to_24 = arg0 << 24
    bits_23_to_16 = arg1 << 16
    bits_15_to_8 = arg2 << 8
    bits_7_to_0 = arg3

    # Add up all the bits to get the final 32-bit value and return
    _32_bit_int = bits_31_to_24 + bits_23_to_16 + bits_15_to_8 + bits_7_to_0

    return _32_bit_int


def convert_sn_from_32bit_int_to_4_8bit_ints(arg0):
    """
    Convert a single '32-bit' value into 4 '8-bit' values. This method is intended for use for setting a serial 
    number for a decoder over the serial port. The BaseUnit serial packet takes four 8-bit values in place of a 
    32-bit integer for a serial number \n
    
    -   arg0 is an unsigned integer that can be within range 0...(4,294,967,296)
    
        ie: \n
            Let, \n
                arg0 = 238,133,987 \n

            To make it easier to visualize, this example shows what the binary strings of each of the 8-bit values 
            to be returned. \n

            ** NOTE: Looking left to right at a binary string, the first bit read is the most significant bit at bit
            position 32 (leftmost bit), with the least significant bit at bit position 1 (rightmost bit).\n

            In binary, \n
                arg0 in binary: '0000 1110 0011 0001 1010 0010 1110 0011' \n

            Let our final 4 8-bit binary strings be, where their bit range is specified in the name. Bit 31 is the 
            furthest left bit, \n
                rval_31bits_24bits in binary:   '0000 1110 0000 0000 0000 0000 0000 0000' \n
                rval_23bits_16bits in binary:   '0000 0000 0011 0001 0000 0000 0000 0000' \n
                rval_15bits_8bits in binary:    '0000 0000 0000 0000 1010 0010 0000 0000' \n
                rval_7bits_0bits in binary:     '0000 0000 0000 0000 0000 0000 1110 0011' \n
                
            Here is the intended outcome, \n
                rval_31bits_24bits in binary:   '0000 0000 0000 0000 0000 0000 0000 1110' = 14 \n
                rval_23bits_16bits in binary:   '0000 0000 0000 0000 0000 0000 0011 0001' = 49 \n
                rval_15bits_8bits in binary:    '0000 0000 0000 0000 0000 0000 1010 0010' = 162 \n
                rval_7bits_0bits in binary:     '0000 0000 0000 0000 0000 0000 1110 0011' = 227 \n
                
            To get there, \n
                1.  Divide the initial 32-bit unsigned int into four 8-bit binary values. This can be accomplished (
                    starting from left to right) by grouping 8 digits together (each digit is effectively a bit).
                    Result:
                        rval_31bits_24bits in binary:   '0000 1110 0000 0000 0000 0000 0000 0000' \n
                        rval_23bits_16bits in binary:   '0000 0000 0011 0001 0000 0000 0000 0000' \n
                        rval_15bits_8bits in binary:    '0000 0000 0000 0000 1010 0010 0000 0000' \n
                        rval_7bits_0bits in binary:     '0000 0000 0000 0000 0000 0000 1110 0011' \n

                2. Shift rval_31bits_24bits right 24 bits so that rval_31bits_24bits occupies bits 0 to 7. In bit 
                   notation: (234,881,024 >> 24)
                   Result:
                        rval_31bits_24bits in binary:   '0000 0000 0000 0000 0000 0000 0000 1110' = 14 \n

                3. Shift rval_23bits_16bits right 16 bits so that rval_23bits_16bits occupies bits 0 to 7. In bit
                   notation: (3,211,264 >> 16)
                   Result:
                        rval_23bits_16bits in binary:   '0000 0000 0000 0000 0000 0000 0011 0001' = 49 \n

                4. Shift rval_15bits_8bits right 8 bits so that rval_15bits_8bits occupies bits 0 to 7. In bit
                   notation: (41,472 >> 8)
                   Result:
                        rval_15bits_8bits in binary:   '0000 0000 0000 0000 0000 0000 1010 0010' = 162 \n
                        
                5. Shift rval_7bits_0bits right 0 bits so that rval_7bits_0bits occupies bits 0 to 7. In bit
                   notation: (227 >> 0)
                   Result:
                        rval_7bits_0bits in binary:   '0000 0000 0000 0000 0000 0000 1110 0011' = 227 \n
                        
            Finally, the non-binary code to achieve this:
                rval_31bits_24bits = (238,133,987 & 0xff000000) >> 24 (logical AND notation) \n
                rval_23bits_16bits = (238,133,987 & 0x00ff0000) >> 16 \n
                rval_15bits_8bits = (238,133,987 & 0x0000ff00) >> 8 \n
                rval_7bits_04bits = (238,133,987 & 0x000000ff) >> 0 \n

    :param arg0: 32-bit unsigned integer
    :type arg0: int, str \n

    :return: A 4 8-bit integer values from a 32-bit unsigned integer \n
    :rtype: tuple \n
    """
    bits_31_to_24 = (arg0 & 0xFF000000) >> 24
    bits_23_to_16 = (arg0 & 0x00FF0000) >> 16
    bits_15_to_8 =  (arg0 & 0x0000FF00) >> 8
    bits_7_to_0 =   (arg0 & 0x000000FF) >> 0

    return bits_31_to_24, bits_23_to_16, bits_15_to_8, bits_7_to_0


def convert_int_to_sn_string(sn_as_int):
    """
    Accepts a serial number in numeric format (as a 32 bit value unsigned value) and converts it into a standard
    formatted serial number string "AAANNNN" \n

    ie:
        let,
            sn_as_int = 238133987

        then,
            right_half = 3987
            left_half = 23813

            first_digit = 13
            second_digit = 20
            third_digit = 20
            fourth_digit = 3
            fifth_digit = 9
            sixth_digit = 8
            seventh_digit = 7

            serial_number = "D003987"

    :param sn_as_int: 32 bit integer to convert to string \n
    :type sn_as_int: int \n

    :return: 7 character string representing a serial number. \n
    :rtype: str \n
    """
    # Get the rightmost 4 integers, ie: (238133987 % 10000) returns 3987
    right_half = sn_as_int % 10000

    # get the remaining digits, ie: (238133987 // 10000) returns 23813
    # here we use '//' instead of '/' to divide because '//' returns a whole number disregarding remainders
    left_half = sn_as_int // 10000

    # First 3 digits
    first_digit = left_half % bu_imports.max_char_from_number_keys
    second_digit = (left_half // bu_imports.max_char_from_number_keys) % bu_imports.max_char_from_number_keys
    third_digit = int(left_half // (math.pow(bu_imports.max_char_from_number_keys, 2)))

    # Last 4 digits
    fourth_digit = (right_half // bu_imports.BaseUnitConstants.THOUSANDTH) % bu_imports.BaseUnitConstants.BASE_10
    fifth_digit = (right_half // bu_imports.BaseUnitConstants.HUNDREDTH) % bu_imports.BaseUnitConstants.BASE_10
    sixth_digit = (right_half // bu_imports.BaseUnitConstants.TENTH) % bu_imports.BaseUnitConstants.BASE_10
    seventh_digit = (right_half % bu_imports.BaseUnitConstants.BASE_10)

    # Result
    serial_number = "{0}{1}{2}{3}{4}{5}{6}".format(
        bu_imports.char_from_number[first_digit],
        bu_imports.char_from_number[second_digit],
        bu_imports.char_from_number[third_digit],
        fourth_digit,
        fifth_digit,
        sixth_digit,
        seventh_digit
    )

    return serial_number


def convert_sn_string_to_int(sn_as_string):
    """
    Accepts a serial number in standard format (7 characters long, "AAANNNN") and returns a numeric 32-bit unsigned
    representation of it. \n
    :param sn_as_string: Serial number in standard format ("AAANNNN")
    :type sn_as_string: str \n
    :return: Numeric 32-bit unsigned representation of the serial number \n
    :rtype: int \n
    """
    # make sure the serial number we are acting on is at least 7 characters long.
    valid_sn = sn_as_string.zfill(7)

    try:
        int_from_char_1 = bu_imports.number_from_char[valid_sn[0]]
        int_from_char_2 = bu_imports.number_from_char[valid_sn[1]] * bu_imports.max_number_from_char_keys
        int_from_char_3 = bu_imports.number_from_char[valid_sn[2]] * (math.pow(bu_imports.max_number_from_char_keys, 2))
        int_from_char_4_thru_7 = int(valid_sn[3:])
        final_result = int(10000 * (int_from_char_1 + int_from_char_2 + int_from_char_3) + int_from_char_4_thru_7)
    except KeyError as ke:
        caught_text = ke.message
        e_msg = "Key not in 'number_from_char' dictionary in 'bu_imports': {0}".format(
            caught_text
        )
        raise KeyError(e_msg)
    except Exception as e:
        caught_text = e.message
        e_msg = "Exception occurred trying to convert {0} to it's numeric 32-bit value. {1}".format(
            sn_as_string,
            caught_text
        )
        raise Exception(e_msg)
    else:
        return final_result
