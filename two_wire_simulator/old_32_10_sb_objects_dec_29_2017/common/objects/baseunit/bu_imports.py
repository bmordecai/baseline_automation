__author__ = 'Tige'
"""
-   Hexadecimal (base 16) "Short"
    --> signed 16-bit (2-byte) integers in range (-32768, 32767)
"""

# Python Built-In Libraries
from datetime import datetime
import time
from urlparse import urlparse
import re
import types
from decimal import Decimal
from collections import namedtuple
from multi_key_dict import multi_key_dict

# Multi-Key Dictionary for converting string characters to integer values
number_from_char = multi_key_dict()
number_from_char[" "] = 0
number_from_char["0", "o", "O"] = 20            # treat all 0's, o's and O's the same
number_from_char["1", "l", "L", "i", "I"] = 1   # treat all 1's, l's, L's, i's and I's the same
number_from_char["2"] = 2
number_from_char["3"] = 3
number_from_char["4"] = 4
number_from_char["5"] = 5
number_from_char["6"] = 6
number_from_char["7"] = 7
number_from_char["8"] = 8
number_from_char["9"] = 9
number_from_char["a", "A"] = 10
number_from_char["b", "B"] = 11
number_from_char["c", "C"] = 12
number_from_char["d", "D"] = 13
number_from_char["e", "E"] = 14
number_from_char["f", "F"] = 15
number_from_char["g", "G"] = 16
number_from_char["h", "H"] = 17
number_from_char["j", "J"] = 18
number_from_char["k", "K"] = 19
number_from_char["m", "M"] = 21
number_from_char["n", "N"] = 22
number_from_char["p", "P"] = 23
number_from_char["q", "Q"] = 24
number_from_char["r", "R"] = 25
number_from_char["s", "S"] = 26
number_from_char["t", "T"] = 27
number_from_char["u", "U"] = 28
number_from_char["v", "V"] = 29
number_from_char["w", "W"] = 30
number_from_char["x", "X"] = 31
number_from_char["y", "Y"] = 32
number_from_char["z", "Z"] = 33

# Used to convert a Serial Number string to a 32 bit value
max_number_from_char_keys = len(number_from_char)

# Multi-Key Dictionary for converting integer values to equivalent string characters
char_from_number = multi_key_dict()
char_from_number[0] = chr(ord("0"))         # ord() returns the equivalent ascii value, chr() converts ascii to char
char_from_number[1] = chr(ord("1"))
char_from_number[2] = chr(ord("2"))
char_from_number[3] = chr(ord("3"))
char_from_number[4] = chr(ord("4"))
char_from_number[5] = chr(ord("5"))
char_from_number[6] = chr(ord("6"))
char_from_number[7] = chr(ord("7"))
char_from_number[8] = chr(ord("8"))
char_from_number[9] = chr(ord("9"))
char_from_number[10] = chr(ord("A"))
char_from_number[11] = chr(ord("B"))
char_from_number[12] = chr(ord("C"))
char_from_number[13] = chr(ord("D"))
char_from_number[14] = chr(ord("E"))
char_from_number[15] = chr(ord("F"))
char_from_number[16] = chr(ord("G"))
char_from_number[17] = chr(ord("H"))
char_from_number[18] = chr(ord("J"))
char_from_number[19] = chr(ord("K"))
char_from_number[20] = chr(ord("0"))
char_from_number[21] = chr(ord("M"))
char_from_number[22] = chr(ord("N"))
char_from_number[23] = chr(ord("P"))
char_from_number[24] = chr(ord("Q"))
char_from_number[25] = chr(ord("R"))
char_from_number[26] = chr(ord("S"))
char_from_number[27] = chr(ord("T"))
char_from_number[28] = chr(ord("U"))
char_from_number[29] = chr(ord("V"))
char_from_number[30] = chr(ord("W"))
char_from_number[31] = chr(ord("X"))
char_from_number[32] = chr(ord("Y"))
char_from_number[33] = chr(ord("Z"))

# Used to convert a 32 bit value to a 7 character Serial Number string
max_char_from_number_keys = len(char_from_number)


class BaseUnitConstants(object):
    BASE_10 = 10
    HUNDREDTH = 100
    TENTH = 10
    THOUSANDTH = 1000

    BASE_UNIT_ADDRESS = 1
    GLOBAL_ADDRESS = 0
    NULL_ADDRESS = 255

    BASE_UNIT_CURRENT_OFFSET = 1.4475
    BASE_UNIT_CURRENT_SCALE = -0.0064
    BASE_UNIT_VOLTAGE_OFFSET = 0
    BASE_UNIT_VOLTAGE_SCALE = 29 / 172

    VALVE_CURRENT_OFFSET = 0.0
    VALVE_CURRENT_OFFSET_12VALVE = 2.883
    VALVE_CURRENT_SCALE = 0.0051
    VALVE_CURRENT_SCALE_12VALVE = -0.0125

    VALVE_VOLTAGE_OFFSET = 0
    VALVE_VOLTAGE_OFFSET_12VALVE = 0
    VALVE_VOLTAGE_SCALE = 23.1 / 222
    VALVE_VOLTAGE_SCALE_12VALVE = 0.1

    BIT_TIME_MARGIN = 0.08
    BIT_TIME_SCALE_FACTOR = 2.5
    DECODER_BIT_SKEW_SCALE = 3.0

    COMMA = ", "
    SLASH = "/"
    FAIL = "FAIL"
    PASS = "PASS"
    F_TYPE_TXT = ".txt"
    F_TYPE_JSON = ".json"


class FileNamePaths(object):
    import os
    # SERIAL_NUMBERS_IN_USE = 'common/objects/baseunit/json_config_files/serial_numbers_in_use.json'
    SERIAL_NUMBERS_IN_USE = os.path.join('common/objects/baseunit/json_config_files', 'serial_numbers_in_use.json')

    # SERIAL_NUMBERS_IN_USE = "json_config_files/serial_numbers_in_use.json"
    DEVICE_INFO = "../common/objects/baseunit/json_config_files/device_info.json"
    DEVICE_LIMITS = "../common/objects/baseunit/json_config_files/device_limits.json"
    DEVICE_LIMITS_BY_VALVES = "common/objects/baseunit/json_config_files/device_limits_by_valve.json"


class CompanyNames(object):
    BASELINE = "BL"
    RAIN_MASTER = "RM"
    TORO = "TO"
    WEATHERMATIC = "WM"


class WireColors(object):
    DECODER_ADDRESS_PORT_1_WIRE = "Orange Wire "
    DECODER_ADDRESS_PORT_2_WIRE = "Yellow Wire "
    DECODER_ADDRESS_PORT_3_WIRE = "Green Wire"
    DECODER_ADDRESS_PORT_4_WIRE = "Blue Wire"
    COMMON_ADDRESS_WIRE = "White"


class DefaultDescriptions(object):
    # SINGLE_VALVE_DEFAULT_PRODUCT_NUMBER = "5201"
    SINGLE = "One-Valve Decoder"
    DUAL = "Two-Valve Decoder"
    QUAD = "Four-Valve Decoder"
    TWELVE = "Twelve Valve Decoder"
    MASTER_VALVE = "Master Valve Decoder"
    # DUAL_VALVE_DEFAULT_PRODUCT_NUMBER = "5202"
    # QUAD_VALVE_DEFAULT_PRODUCT_NUMBER = "5204"
    # TWELVE_VALVE_DEFAULT_PRODUCT_NUMBER = "5212R"
    # TWENTY_FOUR_VALVE_DEFAULT_PRODUCT_NUMBER = "5224R"


class DefaultSerialNumbers(object):
    SINGLE = "00D0000"
    DUAL = "00E0000"
    QUAD = "00Q0000"
    TWELVE = "VA00001"
    TWENTY_FOUR = ""

    # SINGLE_VALVE_DEFAULT_SERIAL_NUMBER = "00D0000",
    # DUAL_VALVE_DEFAULT_SERIAL_NUMBER = "00E0000"
    # QUAD_VALVE_DEFAULT_SERIAL_NUMBER = "00Q0000"
    # TWELVE_VALVE_DEFAULT_SERIAL_NUMBER = "VA00001"


class JSONFileTemplates(object):
    SERIAL_NUMBERS_IN_USE = {
        "_comments_": {
            "Adding More Companies": {
                "First": "Include 2 character Key value at top level of json file",
                "Second": "Add 2 character valve sub-key values",
                "Third": "Serial numbers should be added/removed by test script"
            },
            "JSON Structure": {
                "Company": {
                    "ValveType": [
                        "Returns a list of Serial numbers",
                        "Serial Number1",
                        "Serial Number2"
                    ]
                }
            }
        },

        "BL": {
            "1V": [DefaultSerialNumbers.SINGLE],
            "2V": [DefaultSerialNumbers.DUAL],
            "4V": [DefaultSerialNumbers.QUAD],
            "12V": [DefaultSerialNumbers.TWELVE]
        }
    }


class Defaults(object):
    """
    More 'Defaults' may be added to this class, but they must be defined above this class declaration. Refer to
    existing code structure.
    """
    SerialNumbers = DefaultSerialNumbers
    Descriptions = DefaultDescriptions
    FileTemplates = JSONFileTemplates


class BaseUnitCommands(object):
    """
    BaseUnitCommands 'enum' class. Defines all test engine BaseUnitCommands that are used. \n
    These can be accessed once this 'bu_imports.py' module has been imported, for example: \n
    -   from old_32_10_sb_objects_dec_29_2017.common.bu_imports import * \n
        BaseUnitCommands.serial_number now returns "SN"
    """
    RESET_BASE_UNIT = 193           # 0xC1
    ENABLE_BASE_UNIT = 194          # 0xC2     # 'inactive'
    TEST_BASE_CHANNEL = 195         # 0xC3
    SET_BASE_CHANNEL_PARAMS = 196   # 0xC4
    STATUS_BASE_UNIT = 197          # 0xC5
    STATUS_BASE_UNIT_CLEAR = 198    # 0xC6
    READ_POWER = 199                # 0xC7
    POWER_UP_2WIRE = 200            # 0xC8
    POWER_DOWN_2WIRE = 201          # 0xC9
    READ_CLOCK = 202                # 0xCA                     # ' inactive
    SET_CLOCK = 203                 # 0xCB                      # ' inactive
    READ_VERSION = 204              # 0xCC
    # READ_BASE_UNIT_VERSION = 0xCC
    READ_SERIAL_NUM_BASE_UNIT = 205  # 0xCD
    TEST_TWO_WIRE_DROP = 206        # 0xCE
    FORWARD_COMMAND = 207           # 0xCF
    UNLOCK_BASE_UNIT = 208          # 0xD0
    WRITE_BASE_SERIAL_NUM = 209     # 0xD1
    WRITE_BASE_CONFIG_SET = 210     # 0xD2         # ' inactive
    READ_BASE_CONFIG_SET = 211      # 0xD3          # ' inactive
    READ_LED_STATE = 212            # 0xD4                # ' inactive for 3.0&
    WRITE_LED_STATE = 213           # 0xD5               # ' inactive for 3.0&
    READ_DATA_BUFFER = 215          # 0xD7              # ' inactive
    SLEEP_BASE_SERIAL_NUM = 216     # 0xD8
    UNLOCK_BASE_SERIAL_NUM = 217    # 0xD9
    WRITE_BASE_ADDRESS = 218        # 0xDA
    WRITE_TEMP_BASE_ADDRESS = 219   # 0xDB
    POLL_BASE_UNIT_STATUS = 220     # 0xDC
    FORCE_TWO_WIRE_MODE = 221       # 0xDD
    RE_BOOT = 222                   # 0xDE
    COMPANY_ID = 223                # 0xDF
    TEST_BRIDGE_CHANNEL = 224       # 0xE0
    TEST_BRIDGE_WIRE_DROP = 14      # 0xE
    BASE_ECHO = 226                 # 0xE2


class RemoteCommands(object):
    """
    RemoteCommands 'enum' class. Defines all test engine RemoteCommands that are used. \n
    These can be accessed once this 'bu_imports.py' module has been imported, for example: \n
    -   from old_32_10_sb_objects_dec_29_2017.common.bu_imports import * \n
        RemoteCommands.serial_number now returns "SN"
    """
    RESET_REMOTE_DEVICE = 1         # 0x1
    TEST_REMOTE_CHANNEL = 2         # 0x2
    READ_REMOTE_VERSION = 3         # 0x3
    READ_SERIAL_NUMBER = 4          # 0x4
    GET_REMOTE_STATUS = 5           # 0x5
    GET_STATUS_AND_CLEAR = 6        # 0x6
    READ_REMOTE_POWER = 7           # 0x7
    TURN_VALVE_ON = 8               # 0x8
    TURN_VALVE_LIST_ON = 9          # 0x9
    TURN_VALVE_OFF = 10             # 0xA
    TURN_ALL_VALVES_OFF = 11        # 0xB
    READ_SENSOR_SET1 = 12           # 0xC
    ECHO_PACKET = 13                # 0xD
    VERIFY_ADDRESS = 15             # 0xF
    UNLOCK_REMOTE = 17              # 0x11
    UNLOCK_SERIAL_NUMBER = 18       # 0x12
    WRITE_ADDRESS = 19              # 0x13
    WRITE_TEMP_ADDRESS = 20         # 0x14
    SET_CHANNEL_PARAMS = 21         # 0x15
    SLEEP_SERIAL_NUMBER = 22        # 0x16
    READ_CHANNEL_PARAMS = 23        # 0x17
    READ_SENSOR_SET2 = 24           # 0x18
    WRITE_REMOTE_CONFIG_SET = 25    # 0x19
    READ_REMOTE_CONFIG_SET = 26     # 0x1A
    READ_SENSOR_SET3 = 27           # 0x1B
    READ_BYTE_SET = 28              # 0x1C
    WRITE_BYTE_SET = 48             # 0x30         #  ' long command extra 1
    POLL_UNIT_STATUS = 32           # 0x20
    READ_PAUSE_UNIT = 33            # 0x21
    WRITE_PAUSE_UNIT = 34           # 0x22
    READ_WATER_RATE = 35            # 0x23
    READ_WATER_USED = 36            # 0x24
    SET_WATER_LIMITS = 37           # 0x25
    SET_WATER_USED = 38             # 0x26
    SET_VALVE_BOOST = 39            # 0x27
    READ_VALVE_BOOST = 40           # 0x28
    READ_STARTUP_POWER = 41         # 0x29
    READ_REMOTE_KEYS = 42           # 0x2A
    COMPANY_ID_REMOTE = 43          # 0x2B
    WRITE_SERIAL_NUMBER = 127       # 0x7F


class DeviceTypes(object):
    """
    DeviceTypes 'enum' class. Defines all test engine DeviceTypes that are used. \n
    These can be accessed once this 'bu_imports.py' module has been imported, for example: \n
    -   from old_32_10_sb_objects_dec_29_2017.common.bu_imports import * \n
        DeviceTypes.serial_number now returns "SN"
    """
    # DeviceTypes = namedtuple('RemoteCommands', ['value', 'string', 'opcodes'])
    # Device = namedtuple('Devicetype', ['value', 'string', 'opcodes', 'beginning_serial_number', 'hexs'])
    # BASE_UNIT_hex, BASE_UNIT_str = 0x101, 'BaseUnit'
    # BASE_UNIT = Devicety(value=None, string=None, opcodes=None, beginning_serial_number=None, hex_value=0x101)
    # print(BASE_UNIT.hexs)
    ZONE_STR = "ZN"
    MOISTURE_SENSOR_STR = "MS"
    MASTER_VALVE_STR = "MV"
    TEMPERATURE_SENSOR_STR = "TS"
    EVENT_SWITCH_STR = "SW"
    FLOW_METER_STR = "FM"

    SINGLE_VALVE_INT = 1
    SINGLE_VALVE_STR = "1V"
    DUAL_VALVE_INT = 2
    DUAL_VALVE_STR = "2V"
    QUAD_VALVE_INT = 4
    QUAD_VALVE_STR = "4V"
    TWELVE_VALVE_INT = 12
    TWELVE_VALVE_STR = "12V"
    TWELVE_VALVE_A_STR = "12VA"
    TWELVE_VALVE_B_STR = "12VB"
    TWELVE_VALVE_E_STR = "12VE"
    TWENTY_FOUR_VALVE_INT = 24
    TWENTY_FOUR_VALVE_STR = "24V"
    BASE_UNIT_II = 258              # 0x102
    REMOTE_BASE_UNIT = 259          # 0x103
    BRIDGE_UNIT = 260               # 0x104
    BASIC_DECODER_TYPE = 2          # 0x2  # 'Signifies any type of decoder, cluster or quad used with 0xXXX / 256#
    DECODER_UNIT = 513              # 0x201
    DUAL_DECODER = 514              # 0x202
    QUAD_DECODER = 516              # 0x204
    CLUSTER_UNIT = 524              # 0x20C
    CLUSTER_AC = 3084               # 0xC0C
    MASTER_VALVE_DECODER = 529      # 0x211
    AIR_TEMPERATURE = 769           # 0x301
    AIR_TEMPERATURE_II = 770        # 0x302          # ' programmable address
    MOISTURE_SENSOR_INT_I = 1025          # 0x401
    MOISTURE_SENSOR_INT_II = 1026       # 0x402
    MOISTURE_SENSOR_INT_III = 1027      # 0x403       # ' 5' and 1.5' sensors *NEW*
    moisture_sensor_wt = 1041       # 0x411         # ' 1.5' sensor for WaterTec Only
    WATER_METER = 1281              # 0x501
    PAUSE_BUTTON_REMOTE = 1537      # 0x601


class CommunicationErrors(object):
    """
    CommunicationErrors 'enum' class. Defines all test engine CommunicationErrors that are used. \n
    These can be accessed once this 'bu_imports.py' module has been imported, for example: \n
    -   from old_32_10_sb_objects_dec_29_2017.common.bu_imports import * \n
        CommunicationErrors.serial_number now returns "SN"
    """
    COMM_PORT_ERROR = 240       # 0xF0
    FRAME_ERROR = 241           # 0xF1
    NO_RESPONSE_SERIAL = 242    # 0xF2


class BaseErrors(object):
    """
    BaseErrors 'enum' class. Defines all test engine BaseErrors that are used. \n
    These can be accessed once this 'bu_imports.py' module has been imported, for example: \n
    -   from old_32_10_sb_objects_dec_29_2017.common.bu_imports import * \n
        BaseErrors.serial_number now returns "SN"
    """

    # COMMUNICATION ERRORS
    COMM_PORT_ERROR = 240           # 0xF0
    FRAME_ERROR = 241               # 0xF1
    NO_RESPONSE_SERIAL = 242        # 0xF2

    # BASE ERRORS
    STATUS_OK = 1                   # 0x1
    OVER_CURRENT_BASE = 2           # 0x2
    NO_RESPONSE_REMOTE = 3          # 0x3
    I2C_ERROR_BASE = 4              # 0x4
    ILLEGAL_COMMAND_BASE = 5        # 0x5
    COMMAND_REJECTED_BASE = 6       # 0x6
    CHECKSUM_BAD = 7                # 0x7
    MORE_DATA_READY = 8             # 0x8
    BRIDGE_SLOW_POWER_UP = 9        # 0x9
    BRIDGE_OVER_CURRENT = 10        # 0xA
    FRAME_ERROR_BASE = 11           # 0xB
    TWO_WIRE_MODE_FAULT = 12        # 0xC
    BRIDGE_CHECKSUM = 13            # 0xD
    BRIDGE_NO_RESPONSE = 14         # 0xE


class ErrorHelpText(object):
    """
    ErrorHelpText 'enum' class. Defines all supported help messages that have further description.
    """
    BASE_NO_REPLY = 0
    BASE_WRONG_TYPE = 1
    BASE_POWER_UP = 2
    BASE_CURRENT_VOLTAGE = 3
    REMOTE_READ_SN = 4
    REMOTE_UNLOCK_SN = 5
    REMOTE_VERSION_OEM = 6
    REMOTE_VERSION_BAD = 7
    REMOTE_WRITE_SN = 8
    REMOTE_WRITE_ADDRESS = 9
    REMOTE_TWO_WIRE_DROP = 10
    REMOTE_BIT_TIMES = 11
    REMOTE_TURN_ON_VALVE = 12
    REMOTE_SENSOR_IO = 13


class RemoteErrors(object):
    """
    RemoteErrors 'enum' class. Defines all test engine RemoteErrors that are used. \n
    These can be accessed once this 'bu_imports.py' module has been imported, for example: \n
    -   from old_32_10_sb_objects_dec_29_2017.common.bu_imports import * \n
        RemoteErrors.serial_number now returns "SN"
    """
    # Remote Errors
    OVER_CURRENT_REMOTE = 64        # 0x40
    BYTE_CIRCUIT = 65               # 0X41
    OPEN_CIRCUIT = 66               # 0x42
    I2C_ERROR_REMOTE = 67           # 0x43
    ILLEGAL_COMMAND_REMOTE = 68     # 0x44
    COMMAND_REJECTED_REMOTE = 69    # 0x45
    PARTIAL_READING = 70            # 0x46 - water flow reading is partial minute
    REMOTE_FRAMING_ERROR = 71       # 0x47
    LIMIT_EXCEEDED = 72             # 0x48
    PORTS_EXCEEDED = 73             # 0x49 - valve ports exceeded on decoder
    LOW_STARTUP_VOLTAGE = 74        # 0x4A - valve step 1 voltage dropped to less than 12 volts
    DATA_READY = 75                 # 0x4B - remote keypad has data
    BELOW_LOWER_LIMIT = 76          # 0x4C - dropped below lower limit
    DATA_OVERFLOW = 77              # 0x4D
    NO_AC_VOLTAGE = 78              # 0x4E - no 24 VAC on 12-valve AC decoder ##########
    ILLEGAL_RESPONSE_REMOTE = 254   # 0xFE
    SENSOR_ZERO_READING = 224       # 0xE0


class CurrentVoltageScalers (object):
    BASE_UNIT_CURRENT_OFFSET = 1.4475
    BASE_UNIT_CURRENT_SCALE = -0.0064
    BASE_UNIT_VOLTAGE_OFFSET = 0
    BASE_UNIT_VOLTAGE_SCALE = 29.0 / 172.0

    VALVE_CURRENT_OFFSET = 0.0
    VALVE_CURRENT_OFFSET_12V = 2.883
    VALVE_CURRENT_SCALE = 0.0051
    VALVE_CURRENT_SCALE_12V = -0.0125
    VALVE_VOLTAGE_OFFSET = 0
    VALVE_VOLTAGE_OFFSET_12v = 0
    VALVE_VOLTAGE_SCALE = 23.1 / 222.0
    VALVE_VOLTAGE_SCALE_12V = 0.1

    BIT_TIME_MARGIN = 0.08
    BIT_TIME_SCALE_FACTOR = 2.5
    DECODER_BIT_SKEW_SCALE = 3.0


class PacketTypes(object):
    """
    Type of packet sent between UI and Python
    """
    SETUP = "SETUP"
    PROMPT_UI = "PROMPT_UI"
    UI_RESPONSE = "UI_RESPONSE"
    STEP_OUTPUT = "STEP_OUTPUT"
    TEST_RESULTS = "TEST RESULTS"


class PacketTemplates(object):
    """
    Templates for each type of packet
    """
    PROMPT_UI = {
        "HEADER": {
            "STEP_NUMBER": 0,
            "TYPE": PacketTypes.PROMPT_UI,
            "RESULT": 0
        },
        "MESSAGE": "",
        "PROMPT_TYPE": "",
        "TEST_NAME": ""
    }

    INIT_UI_SETUP = {
        "HEADER": {
            "STEP_NUMBER": 0,
            "TYPE": PacketTypes.SETUP,
            "RESULT": 0
        },
        "MESSAGE": "",
        "TEST_LIMITS": {

            "Current": {
                "Min": 0,
                "Max": 0
            },

            "TwoWireDrop": {
                "Min": 0,
                "Max": 0
            },

            "Voltage": {
                "Min": 0,
                "Max": 0
            },

            "Version": {
                "Min": 0,
                "Maj": 0
            }
        }
    }

    UI_RESPONSE = {
        "HEADER": {
            "STEP_NUMBER": 0,
            "TYPE": PacketTypes.UI_RESPONSE,
            "RESULT": 0
        },
        "MESSAGE": "",
        "PROMPT_TYPE": "",
        "TEST_NAME": ""
    }

    STEP_OUTPUT = {
        "HEADER": {
            "STEP_NUMBER": 0,
            "TYPE": PacketTypes.STEP_OUTPUT,
            "RESULT": 0
        },
        "MESSAGE": "",
        "PROMPT_TYPE": "",
        "TEST_NAME": ""
    }

    TEST_RESULTS = {
        "HEADER": {
            "STEP_NUMBER": 0,
            "TYPE": PacketTypes.TEST_RESULTS,
            "RESULT": 0
        },
        "MESSAGE": "",
        "PROMPT_TYPE": "",
        "TEST_NAME": ""
    }


class PromptTypes(object):
    """
    Type of prompt message to present to user.
    """
    OK_CANCEL = "OK_CANCEL"
    YES_NO = "YES_NO"
    INPUT = "INPUT"


class TestResults(object):
    """
    Results of test
    """
    PASSED = "1"
    FAILED = "0"
    NA = "2"


class ReplyMessages(object):
    """
    Replies received from UI
    """
    YES = "YES"
    NO = "NO"
    OK = "OK"
    CANCEL = "CANCEL"


class TCP(object):
    """
    TCP Wrapper (this helps wrap all relative tcp classes into a single access point)
    """
    PROMPT_TYPES = PromptTypes
    PACKET_TYPES = PacketTypes
    PACKET_TEMPLATES = PacketTemplates
    TEST_RESULTS = TestResults
    REPLY_MESSAGES = ReplyMessages



