__author__ = 'baseline'

# -------------------------------------------------------------------------------------------------------------------- #
# Common object buckets
# -------------------------------------------------------------------------------------------------------------------- #

basemanager_connection = {}
""":type: dict[int, common.objects.controller.bm.BaseManagerConnection]"""

substations = {}
""":type: dict[int, common.objects.substation.sb.Substation]"""

controllers = {}
""":type: dict[int, common.objects.controller.cn.Controller]"""

zones = {}
""":type: dict[int, common.objects.controller.zn.Zone]"""

moisture_sensors = {}
""":type: dict[int, common.objects.controller.ms.MoistureSensor]"""

temperature_sensors = {}
""":type: dict[int, common.objects.controller.ts.TemperatureSensor]"""

flow_meters = {}
""":type: dict[int, common.objects.controller.fm.FlowMeter]"""

master_valves = {}
""":type: dict[int, common.objects.controller.mv.MasterValve]"""

event_switches = {}
""":type: dict[int, common.objects.controller.sw.EventSwitch]"""

alert_relays = {}
""":type: dict[int, common.objects.controller.ar.AlertRelay]"""

zone_programs = {}
""":type: dict[int, common.objects.controller.zp.ZoneProgram]"""

# -------------------------------------------------------------------------------------------------------------------- #
# 3200 specific object buckets
# -------------------------------------------------------------------------------------------------------------------- #

mainlines32 = {}
""":type: dict[int, common.objects.controller.ml.Mainline]"""

poc32 = {}
""":type: dict[int, common.objects.controller.poc_3200.POC3200]"""

programs32 = {}
""":type: dict[int, common.objects.controller.pg_3200.PG3200]"""

program_start_conditions32 = {}
""":type: dict[int, common.objects.controller.pg_start_stop_pause_3200.StartConditionFor3200]"""

program_stop_conditions32 = {}
""":type: dict[int, common.objects.controller.pg_start_stop_pause_3200.StopConditionFor3200]"""

program_pause_conditions32 = {}
""":type: dict[int, common.objects.controller.pg_start_stop_pause_3200.PauseConditionFor3200]"""

# -------------------------------------------------------------------------------------------------------------------- #
# 1000 specific object buckets
# -------------------------------------------------------------------------------------------------------------------- #

poc10 = {}
""":type: dict[int, common.objects.controller.poc_1000.POC1000]"""

programs10 = {}
""":type: dict[int, common.objects.controller.pg_1000.PG1000]"""

program_start_conditions10 = {}
""":type: dict[int, common.objects.controller.pg_start_stop_pause_1000.StartConditionFor1000]"""

program_stop_conditions10 = {}
""":type: dict[int, common.objects.controller.pg_start_stop_pause_1000.StopConditionFor1000]"""

program_pause_conditions10 = {}
""":type: dict[int, common.objects.controller.pg_start_stop_pause_1000.PauseConditionFor1000]"""


def reset_all():
    """
    Resets all object bucket dictionaries to clean state.
    """
    controllers.clear()
    substations.clear()

    zones.clear()
    moisture_sensors.clear()
    temperature_sensors.clear()
    flow_meters.clear()
    master_valves.clear()
    event_switches.clear()
    alert_relays.clear()
    zone_programs.clear()
    basemanager_connection.clear()

    programs10.clear()
    program_start_conditions10.clear()
    program_stop_conditions10.clear()
    program_pause_conditions10.clear()
    poc10.clear()

    programs32.clear()
    program_start_conditions32.clear()
    program_stop_conditions32.clear()
    program_pause_conditions32.clear()
    mainlines32.clear()
    poc32.clear()
