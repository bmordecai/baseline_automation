from decimal import Decimal
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices

__author__ = 'tige'

########################################################################################################################
# MOISTURE SENSOR
########################################################################################################################


class MoistureSensor(Devices):
    """
    Moisture Sensor Object \n
    """

    starting_lat = Decimal(Devices.controller_lat)
    starting_long = Decimal(Devices.controller_long) + Decimal('.000500')

    #################################
    def __init__(self, _serial, _address, _shared_with_sb=False):

        # Create description for initialization of Device parent class for inheritance
        description = "{0} Test Moisture Sensor {1}".format(_serial, str(_address))

        # create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        # Initialize parent class
        Devices.__init__(self, _sn=_serial, _ds=description, _ad=_address, _la=lat, _lg=lng, is_shared=_shared_with_sb)

        self.dv_type = opcodes.moisture_sensor
        # Moisture Sensor specific attributes
        self.vp = 21.3       # Moisture Percent
        self.vd = 71.5       # Temperature
        self.vt = 0.0       # Two Wire Drop

    #################################
    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        # Update current description in case we changed address or serial number
        self.ds = self.ds if self.ds == self.build_description() else self.build_description()

        command = "SET,MS={0},DS={1},LA={2},LG={3}".format(
            str(self.sn),   # {0}
            str(self.ds),   # {1}
            str(self.la),   # {2}
            str(self.lg)    # {3}
        )

        # If the device is not shared with the substation, set these values because we own them.
        if not self.is_shared_with_substation:
            command += ",VP={0},VD={1},VT={2}".format(
                self.vp,   # {0}
                self.vd,   # {1}
                self.vt    # {2}
            )

        try:
            # Attempt to set moisture sensor default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Moisture Sensor {0} ({1})'s 'Default values' to: '{2}'".format(
                    self.sn, str(self.ad), command)
            raise Exception(e_msg)
        else:
            print("Successfully set Moisture Sensor {0} ({1})'s 'Default values' to: {2}".format(
                  self.sn, str(self.ad), command))

    #################################
    def set_moisture_percent_on_cn(self, _percent=None):
        """
        Set the moisture percent for the moisture sensor (Faux IO Only) \n
        :param _percent:        Value to set the moisture sensor moisture reading to (in percent) \n
        :return:
        """
        # If a moisture percent is passed in for overwrite
        if _percent is not None:

            # Verifies the moisture sensor reading passed in is an int or float value
            if not isinstance(_percent, (int, float)):
                e_msg = "Failed trying to set MS {0} ({1}) moisture percent. Invalid argument type, expected an int " \
                        "or float, received: {2}".format(self.sn, str(self.ad), type(_percent))
                raise TypeError(e_msg)
            else:
                # Overwrite current value with new value
                self.vp = _percent

        # Command for sending
        command = "SET,MS={0},VP={1}".format(str(self.sn), str(self.vp))

        try:
            # Attempt to set moisture percent for moisture sensor at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Moisture Sensor {0} ({1})'s 'Moisture Percent' to: '{2}'".format(
                    self.sn, str(self.ad), str(self.vp))
            raise Exception(e_msg)
        else:
            print("Successfully set Moisture Sensor {0} ({1})'s 'Moisture Percent' to: {2}".format(
                  self.sn, str(self.ad), str(self.vp)))

    #################################
    def set_temperature_reading_on_cn(self, _temp=None):
        """
        Set the temperature reading for the moisture sensor (Faux IO Only) \n
        :param _temp:        Value to set the moisture sensor temperature reading to \n
        :return:
        """
        # If a temperature value is passed in for overwrite
        if _temp is not None:

            # Verifies the temperature reading passed in is an int or float value
            if not isinstance(_temp, (int, float)):
                e_msg = "Failed trying to set MS {0} ({1}) temperature reading. Invalid argument type, expected an " \
                        "int or float, received: {2}".format(self.sn, str(self.ad), type(_temp))
                raise TypeError(e_msg)
            else:
                # Overwrite current value
                self.vd = _temp

        # Command for sending
        command = "SET,MS={0},VD={1}".format(str(self.sn), str(self.vd))

        try:
            # Attempt to set temperature reading for moisture sensor at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Moisture Sensor {0} ({1})'s 'Temperature Reading' to: '{2}'".\
                    format(self.sn, str(self.ad), str(self.vd))
            raise Exception(e_msg)
        else:
            print("Successfully set Moisture Sensor {0} ({1})'s 'Temperature Reading' to: {2}".format(
                  self.sn, str(self.ad), str(self.vd)))

    #################################
    def set_two_wire_drop_value_on_cn(self, _value=None):
        """
        Set the two wire drop value for the moisture sensor (Faux IO Only) \n
        :param _value:        Value to set the moisture sensor two wire drop value to \n
        :return:
        """
        # If a two wire drop value is passed in for overwrite
        if _value is not None:
            # Verifies the two wire drop passed in is an int or float value
            if not isinstance(_value, (int, float)):
                e_msg = "Failed trying to set MS {0} ({1}) two wire drop. Invalid argument type, expected an " \
                        "int or float, received: {2}".format(self.sn, str(self.ad), type(_value))
                raise TypeError(e_msg)
            else:
                self.vt = _value

        # Command for sending
        command = "SET,MS={0},VT={1}".format(str(self.sn), str(self.vt))

        try:
            # Attempt to set two wire drop for moisture sensor at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Moisture Sensor {0} ({1})'s 'Two Wire Drop Value' to: '{2}'".\
                format(self.sn, str(self.ad), str(self.vt))
            raise Exception(e_msg)
        else:
            print("Successfully set Moisture Sensor {0} ({1})'s 'Two Wire Drop Value' to: {2}".format(
                  self.sn, str(self.ad), str(self.vt)))

    #################################
    def verify_moisture_percent_on_cn(self):
        """
        Verifies the moisture percent set for this Moisture Sensor on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        moisture_percent = float(self.data.get_value_string_by_key(opcodes.moisture_percent))
        rounded_vp = round(number=self.vt, ndigits=2)
        if abs(rounded_vp - moisture_percent) > 0.02:
            # Compare status versus what is on the controller
            if self.vp != moisture_percent:
                e_msg = "Unable verify Moisture Sensor {0} ({1})s moisture percent reading. Received: {2}, " \
                        "Expected: {3}".format(
                            self.sn,                # {0}
                            str(self.ad),           # {1}
                            str(moisture_percent),  # {2}
                            str(self.vp)            # {3}
                        )
                raise ValueError(e_msg)
        # If no exception is thrown, print a success message
        print("Verified Moisture Sensor {0} ({1})'s moisture reading: '{2}' on controller".format(
              self.sn,          # {0}
              str(self.ad),     # {1}
              str(self.vp)      # {2}
              ))

    #################################
    def verify_temperature_reading_on_cn(self):
        """
        Verifies the temperature reading for this Moisture Sensor on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        temperature = float(self.data.get_value_string_by_key(opcodes.temperature_value))
        rounded_vd = round(number=self.vd, ndigits=2)
        if abs(rounded_vd - temperature) > 0.2:
            # Compare status versus what is on the controller
            if self.vd != temperature:
                e_msg = "Unable verify Moisture Sensor {0} ({1})'s temperature reading. Received: {2}, Expected: {3}".\
                    format(
                        self.sn,            # {0}
                        str(self.ad),       # {1}
                        str(temperature),   # {2}
                        str(self.vd)        # {3}
                    )
                print(e_msg)
                raise ValueError(e_msg)
        # If no exception is thrown, print a success message
        print("Verified Moisture Sensor {0} ({1})'s temperature reading: '{2}' on controller".format(
              self.sn,          # {0}
              str(self.ad),     # {1}
              str(self.vd)      # {2}
              ))

    #################################
    def verify_two_wire_drop_value_on_cn(self):
        """
        Verifies the two wire drop value for this Moisture Sensor on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        two_wire_drop_val = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        rounded_vt = round(number=self.vt, ndigits=2)
        if abs(rounded_vt - two_wire_drop_val) > 0.02:
            # Compare status versus what is on the controller
            if self.vt != two_wire_drop_val:
                e_msg = "Unable verify Moisture Sensor {0} ({1})'s two wire drop value. Received: {2}, Expected: {3}".\
                        format(
                            self.sn,                    # {0}
                            str(self.ad),               # {1}
                            str(two_wire_drop_val),     # {2}
                            str(self.vt)
                        )
                raise ValueError(e_msg)
        # If no exception is thrown, print a success message
        print("Verified Moisture Sensor {0} ({1})'s two wire drop value: '{2}' on controller".format(
              self.sn,          # {0}
              str(self.ad),     # {1}
              str(self.vt)      # {2}
              ))

    #################################
    def self_test_and_update_object_attributes(self):
        """
        this does a self test on the device
        than does a get date a resets the attributes of the device to match the controller
        self.vp       # Moisture Percent
        self.vd       # Temperature
        self.vt       # Two Wire Drop
        """
        try:
            self.do_self_test()
            self.get_data()
            self.vp = float(self.data.get_value_string_by_key(opcodes.moisture_percent))
            self.vd = float(self.data.get_value_string_by_key(opcodes.temperature_value))
            self.vt = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        except Exception as e:
            e_msg = "Exception occurred trying to updating attributes of the moisture sensor {0} object." \
                    " Moisture Value: '{1}'," \
                    " Temperature Value: '{2}'," \
                    " Two Wire Drop Value: '{3}'" \
                .format(
                        str(self.ad),  # {0}
                        str(self.vp),  # {1}
                        str(self.vd),  # {2}
                        str(self.vt),  # {3}
                )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the moisture sensor  {0} object. "
                  "moisture Value: '{1}', "
                  "Temperature Value: '{2}', "
                  "Two Wire Drop Value: '{3}' ".format(
                    str(self.ad),  # {0}
                    str(self.vp),  # {1}
                    str(self.vd),  # {2}
                    str(self.vt),  # {3}
                  ))

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Flow Meter'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify base attributes
        self.verify_description_on_cn()
        self.verify_serial_number_on_cn()
        self.verify_latitude_on_cn()
        self.verify_longitude_on_cn()
        if _expected_status is not None:
            self.verify_status_on_cn(status=_expected_status)

        # if the device is shared by a substation than do a self test and update object attributes
        if self.is_shared_with_substation:
            self.self_test_and_update_object_attributes()

        # Verify Moisture Sensor specific attributes
        self.verify_moisture_percent_on_cn()
        self.verify_temperature_reading_on_cn()
        # self.verify_two_wire_drop_value_on_cn()