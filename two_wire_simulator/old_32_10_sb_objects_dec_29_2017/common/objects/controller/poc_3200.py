from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import mainlines32, moisture_sensors, event_switches
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.poc import POC

__author__ = 'baseline'


########################################################################################################################
# 3200 POC Class
########################################################################################################################
class POC3200(POC):
    """
    3200 Point of Connection Object \n
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    mainline_objects = None
    moisture_sensor_objects = None
    event_switch_objects = None

    #################################
    def __init__(self,
                 _ad,
                 _fm=None,
                 _mv=None,
                 _ds='',
                 _en=opcodes.true,
                 _fl=0,
                 _hf=0,
                 _hs=opcodes.false,
                 _uf=0.0,
                 _us=opcodes.false,
                 _pr=2,
                 _wb=0,
                 _ws=opcodes.false,
                 _wr=opcodes.false,
                 _ml=1,
                 _ms=None,
                 _me=25,
                 _mn=opcodes.false,
                 _ew=5,
                 _sw=None,
                 _se=opcodes.closed,
                 _sn=opcodes.false
                 ):
        """
        3200 Point of Connection Constructor. \n

        :param _ad:     Address (Point of Connection number) \n
        :type _ad:      int \n

        :param _fm:     Flow Meter Object Address \n
        :type _fm:      int \n

        :param _mv:     Master Valve Object Address \n
        :type _mv:      int \n

        :param _ds:     Description \n
        :type _ds:      str \n

        :param _en:     Enabled State (True: 'TR', False: 'FA') - Default: 'TR' \n
        :type _en:      str \n

        :param _fl:     Target Flow - Default: 0 \n
        :type _fl:      int \n

        :param _hf:     High Flow Limit - Default: 0 \n
        :type _hf:      int \n

        :param _hs:     Shutdown on High Flow (True: 'TR', False: 'FA') - Default: 'FA' \n
        :type _hs:      str \n

        :param _uf:     Unscheduled Flow Limit - Default: 0 \n
        :type _uf:      float \n

        :param _us:     Shutdown on Unscheduled (True: 'TR', False: 'FA') - Default: 'FA' \n
        :type _us:      str \n

        :param _pr:     Priority Number (1=high, 2=medium, 3=low) - Default: 2 \n
        :type _pr:      int \n

        :param _wb:     Monthly Water Budget (GPM) - Default: 0 \n
        :type _wb:      int \n

        :param _ws:     Shutdown on over budget (GPM) ('TR'=true, 'FA'=false) - Default: 'FA' \n
        :type _ws:      str \n

        :param _wr:     Water Rationing Enable ('TR'=true, 'FA'=false) - Default: 'FA' \n
        :type _wr:      str \n

        :param _ml:     Mainline number - Default: 1 \n
        :type _ml:      int \n

        :param _ms:     Moisture Sensor empty Object Address - Default: '' \n
        :type _ms:      int \n

        :param _me:     Moisture empty limit (Percent) - Default: 25 \n
        :type _me:      int \n

        :param _mn:     Moisture sensor empty enable ('TR'=true, 'FA'=false) - Default: 'FA' \n
        :type _mn:      str \n

        :param _ew:     Empty wait time (minutes) - Default: 5 \n
        :type _ew:      int \n

        :param _sw:     Event Switch empty Object Address - Default: '' \n
        :type _sw:      int \n

        :param _se:     Switch empty condition ('OP'=open, 'CL'=closed) - Default: 'CL' \n
        :type _se:      str \n

        :param _sn:     Switch empty enable ('TR'=true, 'FA'=false) - Default: 'FA' \n
        :type _sn:      str \n
        """
        self.mainline_objects = mainlines32
        self.moisture_sensor_objects = moisture_sensors
        self.event_switch_objects = event_switches

        # Description for POC
        desc = _ds if _ds else "Test POC {0}".format(str(_ad))

        # Init parent class
        POC.__init__(self, _ad=_ad, _ds=desc, _en=_en, _fl=_fl, _hf=_hf, _hs=_hs, _uf=_uf, _us=_us, _fm=_fm, _mv=_mv)

        # 3200 POC Specific Attributes
        self.pr = _pr
        self.wb = _wb
        self.ws = _ws
        self.wr = _wr
        self.ml = _ml
        self.ms = _ms
        self.me = _me
        self.mn = _mn
        self.ew = _ew
        self.sw = _sw
        self.se = _se
        self.sn = _sn

        # Send state of POC instance to controller
        self.send_programming_to_cn()

    #################################
    def build_obj_configuration_for_send(self):
        """
        Builds the 'SET' string for sending this Point of Connection Instance to the Controller. This method ensures
        that attributes with blank values don't get sent to the controller. This helps avoid 'BC' response's from
        controller. \n
        :return:        Formatted 'Set' string for the Point of Connection \n
        :rtype:         str \n
        """
        # Starting string, always will need this, 'SET,PC=Blah'
        current_config = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13},{14}={15}".format(
            opcodes.point_of_connection,        # {0}
            str(self.ad),                       # {1}
            opcodes.description,                # {2}
            self.ds,                            # {3}
            opcodes.enabled,                    # {4}
            self.en,                            # {5}
            opcodes.target_flow,                # {6}
            str(self.fl),                       # {7}
            opcodes.high_flow_limit,            # {8}
            str(self.hf),                       # {9}
            opcodes.shutdown_on_high_flow,      # {10}
            self.hs,                            # {11}
            opcodes.unscheduled_flow_limit,     # {12}
            str(self.uf),                       # {13}
            opcodes.shutdown_on_unscheduled,    # {14}
            self.us,                            # {15}
        )

        # 3200 Point of Connection attribute list of tuples (each tuple represents the OpCode string and associated
        # value)
        poc_attr_list = [
            (opcodes.flow_meter, self.fm),
            (opcodes.master_valve, self.mv),
            (opcodes.priority, self.pr),
            (opcodes.monthly_water_budget, self.wb),
            (opcodes.shutdown_on_over_budget, self.ws),
            (opcodes.water_rationing_enable, self.wr),
            (opcodes.mainline, self.ml),
            (opcodes.moisture_sensor, self.ms),
            (opcodes.moisture_empty_limit, self.me),
            (opcodes.moisture_sensor_empty_enable, self.mn),
            (opcodes.empty_wait_time, self.ew),
            (opcodes.event_switch, self.sw),
            (opcodes.switch_empty_condition, self.se),
            (opcodes.switch_empty_enable, self.sn),
        ]

        # For each attribute, check if the attribute has a value that isn't 'None' or '' (empty string).
        for attr_tuple in poc_attr_list:

            # If the attribute value isn't 'None' or '', append it to the current_config string
            #   -> Added `attr_tuple[0] == opcodes.mainline and attr_tuple[1] == 0` logic to allow setting ML to 0 on
            #      v12 POC
            if attr_tuple[1] or attr_tuple[0] == opcodes.mainline and attr_tuple[1] == 0:
                arg1 = attr_tuple[0]

                # If the attribute is Flow Meters, and we have an address, get the serial number for the address
                if attr_tuple[0] == opcodes.flow_meter:
                    # get flow meter object's serial number based on the address
                    arg2 = self.flow_meter_objects[attr_tuple[1]].sn

                elif attr_tuple[0] == opcodes.master_valve:
                    # get master valve object's serial number based on the address
                    arg2 = self.master_valve_objects[attr_tuple[1]].sn

                elif attr_tuple[0] == opcodes.event_switch:
                    # get Event Switch object's serial number based on the address
                    arg2 = self.event_switch_objects[attr_tuple[1]].sn

                elif attr_tuple[0] == opcodes.moisture_sensor:
                    # get moisture sensor object's serial number based on the address
                    arg2 = self.moisture_sensor_objects[attr_tuple[1]].sn

                else:
                    # attribute was not based on an object.
                    arg2 = attr_tuple[1]

                current_config += ",{0}={1}".format(
                    arg1,  # {0}
                    arg2   # {1}
                )

        return current_config

    #################################
    def send_programming_to_cn(self):
        """
        Sends the current programming for the 3200 POC to the controller. \n
        """
        # Command for sending
        command = self.build_obj_configuration_for_send()

        try:

            # Attempt to set program default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)

        # catch all exceptions
        except Exception as e:
            e_msg = "Exception occurred trying to 'SET' POC {0}'s 'Values' to: '{1}'\n -> {2}".format(
                str(self.ad),   # {0}
                command,        # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)

        # If no exceptions
        else:
            print("Successfully set POC {0}'s 'Values' to: {1}".format(
                str(self.ad),   # {0}
                command         # {1}
            ))

    #################################
    def set_priority_on_cn(self, _new_priority):
        """
        Sets the 'Priority' for the Point of Connection on the controller. \n

        :param _new_priority:    New 'Priority' for Point Of Connection \n
        :type _new_priority:     int \n
        """
        # Assign new 'Priority' to object attribute
        self.pr = _new_priority
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.point_of_connection,  # {0}
            str(self.ad),       # {1}
            opcodes.priority,   # {2}
            str(self.pr)        # {3}
        )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Priority' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.pr),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Priority' to: {1}".format(
                str(self.ad),  # {0}
                str(self.pr),  # {1}
            ))

    #################################
    def set_monthly_water_budget_on_cn(self, _new_budget):
        """
        Sets the 'Monthly Water Budget' for the Point of Connection on the controller. \n

        :param _new_budget:    New 'Monthly Water Budget' for Point Of Connection \n
        :type _new_budget:     int \n
        """
        # Assign new 'Monthly Water Budget' to object attribute
        self.wb = _new_budget
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.point_of_connection,    # {0}
            str(self.ad),                   # {1}
            opcodes.monthly_water_budget,   # {2}
            str(self.wb)                    # {3}
        )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Monthly Water Budget' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.wb),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Monthly Water Budget' to: {1}".format(
                str(self.ad),  # {0}
                str(self.wb),  # {1}
            ))

    #################################
    def set_shutdown_on_over_budget_state_on_cn(self, _shutdown):
        """
        Sets the 'Shutdown On Over Budget' for the Point of Connection on the controller. \n

        :param _shutdown:    New 'Shutdown On Over Budget' for Point Of Connection \n
        :type _shutdown:     string \n
        """

        # Validate argument value is within accepted values
        if _shutdown not in [opcodes.true, opcodes.false]:
            e_msg = "Invalid 'Shutdown On Over Budget' state for POC {0} to set: {1}. Valid states are '{2}' or " \
                    "'{3}'".format(
                        str(self.ad),       # {0}
                        str(_shutdown),     # {1}
                        opcodes.true,       # {2}
                        opcodes.false       # {3}
                    )
            raise ValueError(e_msg)

        # Assign new 'Shutdown On Over Budget' to object attribute
        else:
            self.ws = _shutdown
            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,        # {0}
                str(self.ad),                       # {1}
                opcodes.shutdown_on_over_budget,    # {2}
                str(self.ws)                        # {3}
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Shutdown On Over Budget' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.ws),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Shutdown On Over Budget' to: {1}".format(
                str(self.ad),  # {0}
                str(self.ws),  # {1}
            ))

    #################################
    def set_water_rationing_enable_state_on_cn(self, _state):
        """
        Sets the 'Water Rationing Enable State' for the Point of Connection on the controller. \n

        :param _state:    New 'Water Rationing Enable State' for Point Of Connection \n
        :type _state:     string \n
        """

        # Validate argument value is within accepted values
        if _state not in [opcodes.true, opcodes.false]:
            e_msg = "Invalid 'Water Rationing Enable' state for POC {0} to set: {1}. Valid states are '{2}' or " \
                    "'{3}'".format(
                        str(self.ad),       # {0}
                        str(_state),     # {1}
                        opcodes.true,       # {2}
                        opcodes.false       # {3}
                    )
            raise ValueError(e_msg)

        # Assign new 'Water Rationing Enable State' to object attribute
        else:
            self.wr = _state
            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,        # {0}
                str(self.ad),                       # {1}
                opcodes.water_rationing_enable,     # {2}
                str(self.wr)                        # {3}
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Water Rationing Enable State' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.wr),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Water Rationing Enable State' to: {1}".format(
                str(self.ad),  # {0}
                str(self.wr),  # {1}
            ))

    #################################
    def set_mainline_on_cn(self, _mainline_number):
        """
        Sets the 'Mainline' for the Point of Connection on the controller. \n

        :param _mainline_number:    New 'Mainline' for Point Of Connection \n
        :type _mainline_number:     int \n
        """
        # Validate mainline number
        if _mainline_number not in range(1, 9):
            e_msg = "Invalid 'Mainline' address entered for POC {0}. Available mainlines are 1 - 8, user entered: {1}" \
                .format(
                    str(self.ad),           # {0}
                    str(_mainline_number)   # {1}
                )
            raise ValueError(e_msg)

        # Assign new 'Mainline' to object attribute
        else:
            self.ml = _mainline_number
            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,        # {0}
                str(self.ad),                       # {1}
                opcodes.mainline,                   # {2}
                str(self.ml)                        # {3}
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Mainline' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.ml),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Mainline' to: {1}".format(
                str(self.ad),  # {0}
                str(self.ml),  # {1}
            ))

    #################################
    def set_moisture_sensor_on_cn(self, _moisture_sensor_address):
        """
        Sets the 'Moisture Sensor' with the specified address for the Point of Connection on the controller. \n
    
        :param _moisture_sensor_address:    New 'Moisture Sensor' address for Point Of Connection \n
        :type _moisture_sensor_address:     int \n
        """
        # Validate argument value is within accepted values
        if _moisture_sensor_address not in self.moisture_sensor_objects:
            e_msg = "Invalid Moisture Sensor address. Verify address exists in object json configuration and/or in " \
                    "current test. Received address: {0}, available addresses: {1}".format(
                        str(_moisture_sensor_address),              # {0}
                        str(self.moisture_sensor_objects.keys()),   # {1}
                    )
            raise ValueError(e_msg)

        # Assign new 'Moisture Sensor' to object attribute
        else:
            # Assign address to poc ms attributea
            self.ms = _moisture_sensor_address

            # Get ms serial for sending
            ms_serial = self.moisture_sensor_objects[self.ms].sn

            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,    # {0}
                str(self.ad),                   # {1}
                opcodes.moisture_sensor,        # {2}
                ms_serial                       # {3}
            )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Moisture Sensor' to: '{1}({2})' " \
                    "-> {3}".format(
                        str(self.ad),   # {0}
                        str(self.ms),   # {1}
                        ms_serial,      # {2}
                        e.message       # {33
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Moisture Sensor' to: {1}({2})".format(
                str(self.ad),   # {0}
                ms_serial,      # {1}
                str(self.ms)    # {2}
            ))

    #################################
    def set_moisture_empty_limit_on_cn(self, _limit):
        """
        Sets the 'Moisture Empty Limit' for the Point of Connection on the controller. \n
    
        :param _limit:    New 'Moisture Empty Limit' (percent) for Point Of Connection \n
        :type _limit:     float \n
        """
        # Assign new 'Moisture Empty Limit' to object attribute
        self.me = _limit
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.point_of_connection,    # {0}
            str(self.ad),                   # {1}
            opcodes.moisture_empty_limit,   # {2}
            str(self.me)                    # {3}
        )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Moisture Empty Limit' to: '{1}' " \
                    "-> {2}".format(
                        str(self.ad),   # {0}
                        str(self.me),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Moisture Empty Limit' to: {1}".format(
                str(self.ad),  # {0}
                str(self.me),  # {1}
            ))

    #################################
    def set_moisture_sensor_empty_enable_state_on_cn(self, _state):
        """
        Sets the 'Moisture Sensor Empty Enable State' for the Point of Connection on the controller. \n
    
        :param _state:    New 'Moisture Sensor Empty Enable State' (percent) for Point Of Connection \n
        :type _state:     int, float \n
        """
        # Validate argument value is within accepted values
        if _state not in [opcodes.true, opcodes.false]:
            e_msg = "Invalid 'Moisture Sensor Empty' state for POC {0} to set: {1}. Valid states are '{2}' or " \
                    "'{3}'".format(
                        str(self.ad),   # {0}
                        str(_state),    # {1}
                        opcodes.true,   # {2}
                        opcodes.false   # {3}
                    )
            raise ValueError(e_msg)

        # Assign new 'Moisture Sensor Empty Enable State' to object attribute
        else:
            self.mn = _state
            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,            # {0}
                str(self.ad),                           # {1}
                opcodes.moisture_sensor_empty_enable,   # {2}
                str(self.mn)                            # {3}
            )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Moisture Sensor Empty Enable State' to: '{1}' " \
                    "-> {2}".format(
                        str(self.ad),   # {0}
                        str(self.mn),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Moisture Sensor Empty Enable State' to: {1}".format(
                str(self.ad),  # {0}
                str(self.mn),  # {1}
            ))

    #################################
    def set_empty_wait_time_on_cn(self, _wait_time):
        """
        Sets the 'Empty Wait Time' for the Point of Connection on the controller. \n
    
        :param _wait_time:    New 'Empty Wait Time' (minutes) for Point Of Connection \n
        :type _wait_time:     int \n
        """
        # Assign new 'Empty Wait Time' to object attribute
        self.ew = _wait_time
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.point_of_connection,    # {0}
            str(self.ad),                   # {1}
            opcodes.empty_wait_time,        # {2}
            str(self.ew)                    # {3}
        )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Empty Wait Time' to: '{1}' " \
                    "-> {2}".format(
                        str(self.ad),   # {0}
                        str(self.ew),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Empty Wait Time' to: {1}".format(
                str(self.ad),  # {0}
                str(self.ew),  # {1}
            ))

    #################################
    def set_event_switch_empty_on_cn(self, _event_switch_address):
        """
        Sets the 'Event Switch Empty SN' for the Point of Connection on the controller. \n
    
        :param _event_switch_address:    New 'Event Switch Empty SN' for Point Of Connection \n
        :type _event_switch_address:     int \n
        """
        # Validate argument value is within accepted values
        if _event_switch_address not in self.event_switch_objects:
            e_msg = "Invalid Event Switch address. Verify address exists in object json configuration and/or in " \
                    "current test. Received address: {0}, available addresses: {1}".format(
                        str(_event_switch_address),              # {0}
                        str(self.event_switch_objects.keys()),   # {1}
                    )
            raise ValueError(e_msg)

        # Assign new 'Event Switch Empty SN' to object attribute
        else:
            # Assign address to POC Event Switch attribute
            self.sw = _event_switch_address

            # Get serial from event switch object
            sw_serial = self.event_switch_objects[self.sw].sn

            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,    # {0}
                str(self.ad),                   # {1}
                opcodes.event_switch,           # {2}
                sw_serial                       # {3}
            )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Event Switch Empty SN' to: '{1}' ({2}) " \
                    "-> {3}".format(
                        str(self.ad),   # {0}
                        sw_serial,      # {1}
                        str(self.sw),   # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Event Switch Empty SN' to: {1} ({2})".format(
                str(self.ad),   # {0}
                sw_serial,      # {1}
                str(self.sw),   # {2}
            ))

    #################################
    def set_switch_empty_condition_state_on_cn(self, _state):
        """
        Sets the 'Event Switch Condition State' for the Point of Connection on the controller. \n
    
        :param _state:    New 'Event Switch Condition State' ('OP'=open, 'CL'=closed) for Point Of Connection \n
        :type _state:     str \n
        """
        # Validate argument value is within accepted values
        if _state not in [opcodes.closed, opcodes.open]:
            e_msg = "Invalid 'Event Switch Condition' state for POC {0} to set: {1}. Valid states are '{2}' or " \
                    "'{3}'".format(
                        str(self.ad),   # {0}
                        str(_state),    # {1}
                        opcodes.open,   # {2}
                        opcodes.closed   # {3}
                    )
            raise ValueError(e_msg)

        # Assign new 'Event Switch Condition State' to object attribute
        else:
            self.se = _state
            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,        # {0}
                str(self.ad),                       # {1}
                opcodes.switch_empty_condition,     # {2}
                str(self.se)                        # {3}
            )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Event Switch Condition State' to: '{1}' " \
                    "-> {2}".format(
                        str(self.ad),   # {0}
                        str(self.se),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Event Switch Condition State' to: {1}".format(
                str(self.ad),  # {0}
                str(self.se),  # {1}
            ))

    #################################
    def set_switch_empty_enable_state_on_cn(self, _state):
        """
        Sets the 'Event Switch Empty Enable State' for the Point of Connection on the controller. \n
    
        :param _state:    New 'Event Switch Empty Enable State' (percent) for Point Of Connection \n
        :type _state:     int, float \n
        """
        # Validate argument value is within accepted values
        if _state not in [opcodes.true, opcodes.false]:
            e_msg = "Invalid 'Event Switch Empty' state for POC {0} to set: {1}. Valid states are '{2}' or " \
                    "'{3}'".format(
                        str(self.ad),   # {0}
                        str(_state),    # {1}
                        opcodes.true,   # {2}
                        opcodes.false   # {3}
                    )
            raise ValueError(e_msg)

        # Assign new 'Event Switch Empty Enable State' to object attribute
        else:
            self.sn = _state
            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,            # {0}
                str(self.ad),                           # {1}
                opcodes.switch_empty_enable,            # {2}
                str(self.sn)                            # {3}
            )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Event Switch Empty Enable State' to: '{1}' " \
                    "-> {2}".format(
                        str(self.ad),   # {0}
                        str(self.sn),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Event Switch Empty Enable State' to: {1}".format(
                str(self.ad),  # {0}
                str(self.sn),  # {1}
            ))

    #################################
    def verify_priority_on_cn(self):
        """
        Verifies the 'Priority' for the Point of Connection set on the controller. \n
        :return:
        """
        # expect int type
        priority_from_controller = int(self.data.get_value_string_by_key(opcodes.priority))

        # Comparison of values between controller and object
        if self.pr != priority_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Priority'. Received: {1}, Expected: {2}".format(
                str(self.ad),                   # {0}
                str(priority_from_controller),  # {1}
                str(self.pr)                    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Priority': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.pr)    # {1}
            ))

    #################################
    def verify_monthly_water_budget_on_cn(self):
        """
        Verifies the 'Monthly Water Budget' for the Point of Connection set on the controller. \n
        :return:
        """
        # expect int type
        budget_from_controller = int(self.data.get_value_string_by_key(opcodes.monthly_water_budget))

        # Comparison of values between controller and object
        if self.wb != budget_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Monthly Water Budget'. Received: {1}, Expected: {2}".format(
                str(self.ad),                   # {0}
                str(budget_from_controller),    # {1}
                str(self.wb)                    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Monthly Water Budget': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.wb)    # {1}
            ))

    #################################
    def verify_shutdown_on_over_budget_on_cn(self):
        """
        Verifies the 'Shutdown On Over Budget State' for the Point of Connection set on the controller. \n
        :return:
        """
        # expect string type
        state_from_controller = self.data.get_value_string_by_key(opcodes.shutdown_on_over_budget)

        # Comparison of values between controller and object
        if self.ws != state_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Shutdown On Over Budget State'. Received: {1}, Expected: {2}".format(
                str(self.ad),                   # {0}
                str(state_from_controller),     # {1}
                str(self.ws)                    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Shutdown On Over Budget State': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.ws)    # {1}
            ))

    #################################
    def verify_water_rationing_enable_state_on_cn(self):
        """
        Verifies the 'Water Rationing Enable State' for the Point of Connection set on the controller. \n
        :return:
        """
        # expect string type
        enabled_state_from_controller = self.data.get_value_string_by_key(opcodes.water_rationing_enable)

        # Comparison of values between controller and object
        if self.wr != enabled_state_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Water Rationing Enable State'. Received: {1}, Expected: {2}".format(
                str(self.ad),                           # {0}
                str(enabled_state_from_controller),     # {1}
                str(self.wr)                            # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Water Rationing Enable State': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.wr)    # {1}
            ))

    #################################
    def verify_mainline_on_cn(self):
        """
        Verifies the 'Mainline' for the Point of Connection set on the controller. \n
        :return:
        """
        # expect int type
        wait_time_from_controller = int(self.data.get_value_string_by_key(opcodes.mainline))

        # Comparison of values between controller and object
        if self.ml != wait_time_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Mainline'. Received: {1}, Expected: {2}".format(
                str(self.ad),                      # {0}
                str(wait_time_from_controller),     # {1}
                str(self.ml)                       # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Mainline': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.ml)    # {1}
            ))

    #################################
    def verify_moisture_sensor_on_cn(self):
        """
        Verifies the 'Moisture Sensor' for the Point of Connection set on the controller. \n
        :return:
        """
        # expect string type
        ms_from_controller = self.data.get_value_string_by_key(opcodes.moisture_sensor)

        # Comparison of values between controller and object
        ms_obj_serial = self.moisture_sensor_objects[self.ms].sn

        if ms_obj_serial != ms_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Moisture Sensor'. Received: {1}, Expected: {2}".format(
                str(self.ad),                # {0}
                str(ms_from_controller),     # {1}
                str(ms_obj_serial)           # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Moisture Sensor': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.ms)    # {1}
            ))

    #################################
    def verify_moisture_empty_limit_on_cn(self):
        """
        Verifies the 'Moisture Empty Limit' for the Point of Connection set on the controller. \n
        :return:
        """
        # expect float type
        empty_limit_from_controller = float(self.data.get_value_string_by_key(opcodes.moisture_empty_limit))

        # Comparison of values between controller and object
        # Cast 'self.me' to float here in case we have an integer whole value, ie: 25 instead of 25.0
        if float(self.me) != empty_limit_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Moisture Empty Limit'. Received: {1}, Expected: {2}".format(
                str(self.ad),                       # {0}
                str(empty_limit_from_controller),   # {1}
                str(self.me)                        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Moisture Empty Limit': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.me)    # {1}
            ))

    #################################
    def verify_moisture_empty_enable_state_on_cn(self):
        """
        Verifies the 'Moisture Empty Enable State' for the Point of Connection set on the controller. \n
        :return:
        """
        # expect string type
        state_from_controller = self.data.get_value_string_by_key(opcodes.moisture_sensor_empty_enable)

        # Comparison of values between controller and object
        if self.mn != state_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Moisture Empty Enable State'. Received: {1}, Expected: {2}".format(
                str(self.ad),                 # {0}
                str(state_from_controller),   # {1}
                str(self.mn)                  # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Moisture Empty Enable State': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.mn)    # {1}
            ))

    #################################
    def verify_empty_wait_time_on_cn(self):
        """
        Verifies the 'Empty Wait Time' for the Point of Connection set on the controller. \n
        :return:
        """
        # expect int type
        wait_time_from_controller = int(self.data.get_value_string_by_key(opcodes.empty_wait_time))

        # Comparison of values between controller and object
        if self.ew != wait_time_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Empty Wait Time'. Received: {1}, Expected: {2}".format(
                str(self.ad),                       # {0}
                str(wait_time_from_controller),     # {1}
                str(self.ew)                        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Empty Wait Time': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.ew)    # {1}
            ))

    #################################
    def verify_event_switch_on_cn(self):
        """
        Verifies the 'Event Switch' for the Point of Connection set on the controller. \n
        :return:
        """
        # expect string type
        sw_from_controller = self.data.get_value_string_by_key(opcodes.event_switch)

        # Comparison of values between controller and object
        sw_obj_serial = self.event_switch_objects[self.sw].sn

        if sw_obj_serial != sw_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Event Switch'. Received: {1}, Expected: {2}".format(
                str(self.ad),  # {0}
                str(sw_from_controller),  # {1}
                str(sw_obj_serial)  # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Event Switch': '{1}' on controller".format(
                str(self.ad),  # {0}
                str(self.sw)  # {1}
            ))

    #################################
    def verify_switch_empty_condition_on_cn(self):
        """
        Verifies the 'Switch Empty Condition' for the Point of Connection set on the controller. \n
        :return:
        """
        # expect string type
        empty_condition_from_controller = self.data.get_value_string_by_key(opcodes.switch_empty_condition)

        # Comparison of values between controller and object
        if self.se != empty_condition_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Switch Empty Condition'. Received: {1}, Expected: {2}".format(
                str(self.ad),                           # {0}
                str(empty_condition_from_controller),   # {1}
                str(self.se)                            # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Switch Empty Condition': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.se)    # {1}
            ))

    #################################
    def verify_switch_empty_enable_on_cn(self):
        """
        Verifies the 'Switch Empty Enable State' for the Point of Connection set on the controller. \n
        :return:
        """
        # expect string type
        state_from_controller = self.data.get_value_string_by_key(opcodes.switch_empty_enable)

        # Comparison of values between controller and object
        if self.sn != state_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Switch Empty Enable State'. Received: {1}, Expected: {2}".format(
                str(self.ad),                 # {0}
                str(state_from_controller),   # {1}
                str(self.sn)                  # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Switch Empty Enable State': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.sn)    # {1}
            ))

    #################################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies all attributes associated with this Point Of Connection. \n
        :param expected_status:     An expected status to verify for POC \n
        :type expected_status:      str \n
        """
        self.get_data()

        self.verify_description_on_cn()
        self.verify_enabled_state_on_cn()
        self.verify_target_flow_on_cn()
        self.verify_high_flow_limit_on_cn()
        self.verify_shutdown_on_high_flow_on_cn()
        self.verify_shutdown_on_unscheduled_on_cn()
        self.verify_priority_on_cn()
        self.verify_monthly_water_budget_on_cn()
        self.verify_shutdown_on_over_budget_on_cn()
        self.verify_water_rationing_enable_state_on_cn()
        self.verify_moisture_empty_limit_on_cn()
        self.verify_moisture_empty_enable_state_on_cn()
        self.verify_empty_wait_time_on_cn()
        self.verify_switch_empty_condition_on_cn()
        self.verify_switch_empty_enable_on_cn()

        # TODO..
        # Currently, if we don't assign a POC a MV, and the POC is not POC 1, then the controller is returning a
        # serial number that isn't even loaded onto the controller during the get_data() call. This line says that if
        # we specified a master valve, then go check
        if self.mv:
            self.verify_master_valve_on_cn()

        if self.fm:
            self.verify_flow_meter_on_cn()

        if self.ml:
            self.verify_mainline_on_cn()

        if self.ms:
            self.verify_moisture_sensor_on_cn()

        if self.sw:
            self.verify_event_switch_on_cn()

        if expected_status is not None:
            self.verify_status_on_cn(_expected_status=expected_status)

