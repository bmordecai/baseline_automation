from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.programs import Programs
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import poc10

__author__ = 'baseline'


########################################################################################################################
# 1000 Program Class
########################################################################################################################
class PG1000(Programs):
    """
    1000 Program Object \n
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    # poc_objects = {1: pc10.POC1000}
    poc_objects = None

    #################################
    def __init__(self,
                 _address,
                 _description=None,
                 _enabled_state=opcodes.true,
                 _water_window=None,
                 _max_concurrent_zones=1,
                 _seasonal_adjust=100,
                 _point_of_connection_address=None,
                 _master_valve_address=None,
                 _soak_cycle_mode=opcodes.disabled,
                 _cycle_count=2,
                 _soak_time=600,
                 _program_cycles=None,
                 _ee=opcodes.false
                 ):
        """
        :param _address:     Program number \n
        :type _address:      int \n

        :param _description:    Description for 1000 Program if user wants a specific description \n
        :type _description:     str \n

        :param _enabled_state:  The enabled state for the program. Enabled = 'TR', Disabled = 'FA'
        :type _enabled_state:   str \n

        :param _max_concurrent_zones:   Max number of concurrent zones for programming \n
        :type _max_concurrent_zones:    int \n

        :param _seasonal_adjust:    Seasonal adjust for programming with an integer representing a percentage,
                                    i.e: 100 = 100% \n
        :type _seasonal_adjust:     int \n

        :param _water_window:   Water window for the program. Two cases accepted.\n
                                - Weekly:   A list with a single element is passed in, the single element having 24 0's
                                            or 1's in a string where 1=On, 0=Off, i.e: ['111111111111111111111111']
                                - Daily:    A list with seven elements, 1 for each day of the week starting with sunday,
                                            with the same format as above, \n
                                            i.e: ['111111111111111111111111', '111111111111111111111111',   \n
                                                  '111111111111111111111111', '111111111111111111111111',   \n
                                                  '111111111111111111111111', '111111111111111111111111',   \n
                                                  '111111111111111111111111'] \n
        :type _water_window:    list[str] \n

        :param _max_concurrent_zones:           Max concurrent zones to run for program. \n
        :type _max_concurrent_zones:            int \n

        :param _point_of_connection_address:    Point of connection to assign to program \n
        :type _point_of_connection_address:     list \n

        :param _master_valve_address:           Master Valve to assign to program \n
        :type _master_valve_address:            int \n

        :param _soak_cycle_mode:                Soak Cycle mode to set for program. Available modes are: \n
                                                opcodes.disabled | opcodes.program_cycles | opcodes.intelligent_soak \n
        :type _soak_cycle_mode:                 str \n

        :param _cycle_count:                    Cycle count (valid cycle counts are 2-100) \n
        :type _cycle_count:                     int \n

        :param _soak_time:                      Soak Time for program in seconds \n
        :type _soak_time:                       int \n

        :param _program_cycles:                 Program cycles  this is a nelson feature nad is not used with \n
                                                Baseline watering opertations \n
        :type _program_cycles:                  int \n
        """
        self.poc_objects = poc10

        description = _description if _description is not None else "Test Program {0}".format(_address)

        # Water Window default, can't have a list be a object constructor parameter
        if _water_window is None:
            _water_window = ['111111111111111111111111']

        # Init parent class
        Programs.__init__(self, _ds=description, _ad=_address, _en=_enabled_state, _ww=_water_window,
                          _mc=_max_concurrent_zones, _sa=_seasonal_adjust)

        # ----- Instance Variables ----- #
        # If an int is passed in, then we will automatically put it in a list
        if _point_of_connection_address is None:
            self.pc = [1]
        elif isinstance(_point_of_connection_address, int):
            self.pc = [_point_of_connection_address]
        else:
            self.pc = _point_of_connection_address
        self.mv = _master_valve_address
        self.sk = _soak_cycle_mode
        self.ct = _cycle_count
        self.so = _soak_time
        self.cy = _program_cycles
        self.ee = _ee

        self.send_programming_to_cn()

    #################################
    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    #################################
    def send_programming_to_cn(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        # Water Window Case
        if len(self.ww) == 1:
            # A len(self.ww) == 1 says we are a weekly water window with a single value
            water_window_weekly_str = "{0}={1}".format(
                opcodes.weekly_water_window,    # {0}
                self.ww[0]                      # {1}
            )
        else:
            water_window_weekly_str = "{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13}".format(
                opcodes.sunday,     # {0}
                self.ww[0],         # {1}
                opcodes.monday,     # {2}
                self.ww[1],         # {3}
                opcodes.tuesday,    # {4}
                self.ww[2],         # {5}
                opcodes.wednesday,  # {6}
                self.ww[3],         # {7}
                opcodes.thursday,   # {8}
                self.ww[4],         # {9}
                opcodes.friday,     # {10}
                self.ww[5],         # {11}
                opcodes.saturday,   # {12}
                self.ww[6]          # {13}
            )

        poc_string = str(self.poc_objects[self.pc[0]].ad)
        for poc_ad in self.pc[1:]:
            poc_string += "={0}".format(self.poc_objects[poc_ad].ad)

        command = "SET,{0}={1},{2}={3},{4}={5},{6},{7}={8},{9}={10},{11}={12},{13}={14},{15}={16},{17}={18}" \
                  ",{19}={20}".format(
                      opcodes.program,                      # {0}
                      str(self.ad),                         # {1}
                      opcodes.enabled,                      # {2}
                      str(self.en),                         # {3}
                      opcodes.description,                  # {4}
                      str(self.ds),                         # {5}
                      water_window_weekly_str,              # {6}
                      opcodes.max_concurrent_zones,         # {7}
                      str(self.mc),                         # {8}
                      opcodes.seasonal_adjust,              # {9}
                      str(self.sa),                         # {10}
                      opcodes.point_of_connection,          # {11}
                      poc_string,                           # {12}
                      opcodes.soak_cycle_mode,              # {13}
                      self.sk,                              # {14}
                      opcodes.cycle_count,                  # {15}
                      self.ct,                              # {16}
                      opcodes.soak_cycle,                   # {17}
                      self.so,                              # {18}
                      opcodes.enable_et_runtime,            # {19}
                      self.ee                               # {20}
                  )
        
        # Only add master valve if we have an address assigned to it
        
        if self.mv:
            add_mv_to_command = ",{0}={1}".format(
                opcodes.master_valve,           # {0}
                self.mv_objects[self.mv].sn    # {1}
            )
            command += add_mv_to_command

        # Only add program cycles if we have assigned and integer

        if self.cy is not None:
            add_cy_to_command = ",{0}={1}".format(
                opcodes.program_cycles,       # {0}
                self.cy                       # {1}
            )
            command += add_cy_to_command

        try:
            # Attempt to set program default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Values' to: '{1}'\n -> {2}".format(
                self.ad,   # {0}
                command,   # {1}
                e.message  # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0}'s 'Values' to: {1}".format(
                self.ad,     # {0}
                command      # {1}
            ))

    #################################
    def set_point_of_connection_on_cn(self, _poc_address):
        """
        Sets the 'Point of Connection' for the current program on the controller. \n
        :param _poc_address:   Point of Connection address to overwrite current instance value. \n
        :type _poc_address:    list \n
        """
        # If an int is passed in, we define it in a list
        if isinstance(_poc_address, int):
            _poc_address = [_poc_address]

        # Valid poc address?
        for i in range(0, len(_poc_address)):
            if _poc_address[i] not in self.poc_objects:
                e_msg = "Invalid Point of Connection address for 'SET' for 1000 Program {0}. Valid address are: " \
                        "1 - 3. Received: {1}".format(
                            self.ad,        # {0}
                            _poc_address    # {1}
                        )
                raise ValueError(e_msg)
        # valid address
        self.pc = _poc_address

        # Create a string of the different poc addresses, eg. "1=2=3"
        poc_string = str(self.poc_objects[self.pc[0]].ad)
        for poc_ad in self.pc[1:]:
            poc_string += "={0}".format(self.poc_objects[poc_ad].ad)

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,                # {0}
            str(self.ad),                   # {1}
            opcodes.point_of_connection,    # {2}
            str(poc_string)                 # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Point of Connection' to: {1} -> {2}".format(
                self.ad,    # {0}
                self.pc,    # {1}
                e.message   # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Point of Connection' for 1000 Program: {0} to: {1}".format(
                self.ad,   # {0}
                self.pc    # {1}
            ))
            
    #################################
    def set_master_valve_on_cn(self, _mv_address):
        """
        Sets the 'Master Valve' for the current program on the controller. \n
        :param _mv_address:   Master Valve address to overwrite current instance value. \n
        :type _mv_address:    int \n
        """
        # Valid address?
        if _mv_address not in self.mv_objects:
            e_msg = "Invalid Master Valve address for 'SET' for 1000 Program {0}. Valid address are: {1}. " \
                    "Received: {2}".format(
                        self.ad,                    # {0}
                        self.mv_objects.keys(),     # {1}
                        _mv_address                 # {2}
                    )
            raise ValueError(e_msg)
            
        # valid address
        else:
            # Assign address to mv attribute
            self.mv = _mv_address

            # Get serial from Master Valve with specified address
            mv_serial_for_send = self.mv_objects[self.mv].sn

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,         # {0}
            self.ad,                 # {1}
            opcodes.master_valve,    # {2}
            mv_serial_for_send       # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Master Valve' to: {1}({2}) -> {3}".format(
                self.ad,                # {0}
                mv_serial_for_send,     # {1}
                self.mv,                # {2}
                e.message               # {3}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Master Valve' for 1000 Program: {0} to: {1}({2})".format(
                self.ad,                # {0}
                mv_serial_for_send,     # {1}
                self.mv                 # {2}
            ))            
            
    #################################
    def set_soak_cycle_mode_on_cn(self, _mode):
        """
        Sets the 'Soak Cycle Mode' for the current program on the controller. \n
        :param _mode:   Soak Cycle Mode to overwrite current instance value. \n
        :type _mode:    str \n
        """
        valid_modes = [opcodes.disabled, opcodes.intelligent_soak, opcodes.program_cycles, opcodes.zone_soak_cycles]

        # Valid argument type?
        if _mode not in valid_modes:
            e_msg = "Invalid mode passed in for 'SET' Soak Cycle Mode for 1000 Program {0}. Valid modes: " \
                    "{1}, received: {2}".format(
                        self.ad,        # {0}
                        valid_modes,    # {1}
                        _mode           # {2}
                    )
            raise ValueError(e_msg)
            
        # valid mode
        else:
            self.sk = _mode

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,            # {0}
            self.ad,                    # {1}
            opcodes.soak_cycle_mode,    # {2}
            self.sk                     # {3}
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Soak Cycle Mode' to: {1}".format(
                self.ad,                # {0}
                self.sk                 # {1}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Soak Cycle Mode' for 1000 Program: {0} to: {1}".format(
                self.ad,                # {0}
                self.sk                 # {1}
            ))            
            
    #################################
    def set_cycle_count_on_cn(self, _new_count):
        """
        Sets the 'Cycle Count' for the current program on the controller. \n
        :param _new_count:   Cycle Count to overwrite current instance value. \n
        :type _new_count:    int \n
        """
        valid_cycle_count_range = range(2, 101)

        # Valid argument range?
        if _new_count not in valid_cycle_count_range:
            e_msg = "Invalid cycle count value passed in for 'SET' Cycle Count for 1000 Program {0}. Valid values: " \
                    "2 - 100, received: {1}".format(
                        self.ad,    # {0}
                        _new_count  # {2}
                    )
            raise ValueError(e_msg)
            
        # valid mode
        else:
            self.ct = _new_count

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,        # {0}
            self.ad,                # {1}
            opcodes.cycle_count,    # {2}
            self.ct                 # {3}
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Cycle Count' to: {1} -> {2}".format(
                self.ad,                # {0}
                self.ct,                # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Cycle Count' for 1000 Program: {0} to: {1}".format(
                self.ad,                # {0}
                self.ct                 # {1}
            ))
                        
    #################################
    def set_soak_time_in_seconds_on_cn(self, _seconds):
        """
        Sets the 'Soak Time' for the current program on the controller. \n
        :param _seconds:   Soak Time to overwrite current instance value. \n
        :type _seconds:    int \n
        """
        # valid mode
        self.so = _seconds
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,        # {0}
            self.ad,                # {1}
            opcodes.soak_cycle,      # {2}
            self.so                 # {3}
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Soak Time' to: {1} seconds -> {2}".format(
                self.ad,                # {0}
                self.so,                # {1}
                e.message               # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Soak Time' for 1000 Program: {0} to: {1} seconds".format(
                self.ad,                # {0}
                self.so                 # {1}
            ))
                                    
    #################################
    def set_program_cycles_on_cn(self, _number_of_cycles):
        """
        Sets the 'Program Cycles' for the current program on the controller. \n
        :param _number_of_cycles:   Program Cycles to overwrite current instance value. \n
        :type _number_of_cycles:    int \n
        """
        # Valid type
        self.cy = _number_of_cycles
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,            # {0}
            self.ad,                    # {1}
            opcodes.program_cycles,     # {2}
            self.cy                     # {3}
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Program Cycles' to: {1} cycles -> {2}".format(
                self.ad,                # {0}
                self.cy,                # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Program Cycles' for 1000 Program: {0} to: {1} cycles".format(
                self.ad,                # {0}
                self.cy                 # {1}
            ))

    #################################
    def set_enable_et_state_on_cn(self, _state=None):
        """
        Sets the Enabled State for ET on a program . \n
        :return:
        """
        # If trying to overwrite current value
        if _state is not None:

            # If program isn't enabled, make sure to update instance variable to match what the controller will show.
            if _state not in [opcodes.true, opcodes.false]:
                e_msg = "Invalid enabled state entered for Program {0}. Received: {1}, Expected: 'TR' or 'FA'".format(
                        str(self.ad),   # {0}
                        _state)         # {1}
                raise ValueError(e_msg)
            else:
                # Overwrite current attribute value
                self.ee = _state

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,                        # {0}
            self.ad,                                # {1}
            opcodes.enable_et_runtime,      # {2}
            self.ee                                 # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Program {0})'s 'Enable ET Runtime state' " \
                    "to: {1}, {2}".format(
                        self.ad,                # {0}
                        self.ee,                # {1}
                        str(ae.message)         # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Enable ET runtime state' for (Program {0} to: {1}"
                  .format(
                    self.ad,                # {0}
                    self.ee,                # {1}
                  ))

    #################################
    def verify_poc_on_cn(self):
        """
        Verifies the assigned POC returned from the controller. \n
        """
        assigned_poc_from_controller = map(int, self.data.get_value_string_by_key(opcodes.point_of_connection).split('='))
        # assigned_poc_from_controller = int(self.data.get_value_string_by_key(opcodes.point_of_connection))

        # We must sort our list first because the controller returns it's poc addresses in order
        self.pc.sort()
        # Compare versus what is on the controller, both are lists so we must iterate through them
        for i in range(0, len(self.pc)):
            if self.pc[i] != assigned_poc_from_controller[i]:
                e_msg = "Unable to verify 1000 Program {0}'s assigned 'POC'. Received: {1}, Expected: {2}".format(
                    self.ad,                        # {0}
                    assigned_poc_from_controller,   # {1}
                    self.pc                         # {2}
                )
                raise ValueError(e_msg)

        print("Verified 1000 Program {0}'s assigned 'POC': '{1}' on controller".format(
            self.ad,    # {0}
            self.pc     # {1}
        ))
            
    #################################
    def verify_master_valve_on_cn(self):
        """
        Verifies the assigned Master Valve returned from the controller. \n
        """
        assigned_mv_sn_from_controller = self.data.get_value_string_by_key(opcodes.master_valve)

        # Get serial number from our assigned master valve object
        sn_from_assigned_mv_object = self.mv_objects[self.mv].sn
        
        # Compare versus what is on the controller
        if sn_from_assigned_mv_object != assigned_mv_sn_from_controller:
            e_msg = "Unable to verify 1000 Program {0}'s assigned 'Master Valve'. Received: {1}, Expected: {2}".format(
                self.ad,                            # {0}
                assigned_mv_sn_from_controller,     # {1}
                sn_from_assigned_mv_object          # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 1000 Program {0}'s assigned 'Master Valve': '{1}' on controller".format(
                self.ad,                    # {0}
                sn_from_assigned_mv_object  # {1}
            ))            
            
    #################################
    def verify_soak_cycle_mode_on_cn(self):
        """
        Verifies the assigned Soak Cycle Mode returned from the controller. \n
        """
        # expect string value
        mode_from_controller = self.data.get_value_string_by_key(opcodes.soak_cycle_mode)

        # Compare versus what is on the controller
        if self.sk != mode_from_controller:
            e_msg = "Unable to verify 1000 Program {0}'s 'Soak Cycle Mode'. Received: {1}, Expected: {2}".format(
                self.ad,                  # {0}
                mode_from_controller,     # {1}
                self.sk                   # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 1000 Program {0}'s 'Soak Cycle Mode': '{1}' on controller".format(
                self.ad,    # {0}
                self.sk     # {1}
            ))            
            
    #################################
    def verify_cycle_count_on_cn(self):
        """
        Verifies the assigned Cycle Count returned from the controller. \n
        """
        # expect int value
        cycle_count_from_controller = int(self.data.get_value_string_by_key(opcodes.cycle_count))

        # Compare versus what is on the controller
        if self.ct != cycle_count_from_controller:
            e_msg = "Unable to verify 1000 Program {0}'s 'Cycle Count'. Received: {1}, Expected: {2}".format(
                self.ad,                        # {0}
                cycle_count_from_controller,    # {1}
                self.ct                         # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 1000 Program {0}'s 'Cycle Count': '{1}' on controller".format(
                self.ad,    # {0}
                self.ct     # {1}
            ))            
            
    #################################
    def verify_soak_time_on_cn(self):
        """
        Verifies the assigned Soak Time returned from the controller. \n
        """
        # expect int value
        soak_time_from_controller = int(self.data.get_value_string_by_key(opcodes.soak_cycle))

        # Compare versus what is on the controller
        if self.so != soak_time_from_controller:
            e_msg = "Unable to verify 1000 Program {0}'s 'Soak Time'. Received: {1} seconds, Expected: {2} " \
                    "seconds".format(
                        self.ad,                        # {0}
                        soak_time_from_controller,      # {1}
                        self.so                         # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified 1000 Program {0}'s 'Soak Time': '{1}' seconds on controller".format(
                self.ad,    # {0}
                self.so     # {1}
            ))            
            
    #################################
    def verify_program_cycles_on_cn(self):
        """
        Verifies the assigned Program Cycles returned from the controller. \n
        """
        # expect int value
        pg_cycles_from_controller = int(self.data.get_value_string_by_key(opcodes.program_cycles))

        # Compare versus what is on the controller
        if self.cy != pg_cycles_from_controller:
            e_msg = "Unable to verify 1000 Program {0}'s 'Program Cycles'. Received: {1}, Expected: {2}".format(
                    self.ad,                        # {0}
                    pg_cycles_from_controller,      # {1}
                    self.cy                         # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 1000 Program {0}'s 'Program Cycles': '{1}' on controller".format(
                self.ad,    # {0}
                self.cy     # {1}
            ))

    #################################
    def verify_enable_et_state_on_cn(self):
        """
        Verifies the 'Enable ET Runtime State' value for the Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'ET Runtime State' value from the controller reply.
        -   Lastly, compares the instance 'ET Runtime State' value with the controller's programming.
        """

        et_state_from_controller = self.data.get_value_string_by_key(opcodes.enable_et_runtime)

        # Compare status versus what is on the controller
        if self.ee != et_state_from_controller:
            e_msg = "Unable to verify (Program {0})'s 'Enable Et Runtime State' value. " \
                    "Received: {1}, Expected: {2}".format(
                            self.ad,                                     # {0}
                            str(et_state_from_controller),                  # {1}
                            str(self.ee)                                    # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Program {0})'s 'Enable Et Runtime State' value: '{1}' on controller"
                  .format(
                        self.ad,                # {0}
                        self.ee                 # {1}
                  ))

    #################################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this program's values against what the controller has. \n
        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()

        self.verify_description_on_cn()
        self.verify_enabled_state_on_cn()
        self.verify_water_window_on_cn()
        self.verify_max_concurrent_zones_on_cn()
        self.verify_seasonal_adjust_on_cn()

        if expected_status is not None:
            self.verify_status_on_cn(_expected_status=expected_status)

        self.verify_poc_on_cn()

        if self.mv:
            self.verify_master_valve_on_cn()

        self.verify_soak_cycle_mode_on_cn()
        self.verify_cycle_count_on_cn()
        self.verify_soak_time_on_cn()

        self.verify_enable_et_state_on_cn()
        if self.cy is not None:
            self.verify_program_cycles_on_cn()
