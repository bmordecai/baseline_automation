import math
import old_32_10_sb_objects_dec_29_2017.common.epa_package.equations as equations
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from decimal import Decimal
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
from old_32_10_sb_objects_dec_29_2017.common.epa_package.wbw_imports import *
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages
#
# # from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.messages import Messages
from old_32_10_sb_objects_dec_29_2017.common.imports import types

__author__ = 'ben'


########################################################################################################################
# ZONE
########################################################################################################################
class Zone(Devices):
    """
    Zone Object \n
    """
    # starting_lat = Decimal(dev_mod.Devices.controller_lat)
    # starting_long = Decimal(dev_mod.Devices.controller_long) + Decimal('.000300')
    # Water-Based Watering Additional Attributes
    ei = None              # initial ETo value for each zone
    date_of_et = 00000000         # Date ETo was sent to controller for each zone
    starting_lat = Decimal(str(Devices.controller_lat))
    starting_long = Decimal(str(Devices.controller_long)) + Decimal('.000300')
    #TODO I think I need to move MB up here
    # zp_objects = {1: zp.ZoneProgram}

    #################################
    def __init__(self, _serial, _address, _shared_with_sb=False):
        # Create description for zone
        description = "{0} Test Zone {1}".format(str(_serial), str(_address))

        # create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        # Init parent class to inherit respective attributes
        Devices.__init__(self, _sn=_serial, _ds=description, _ad=_address, _la=lat, _lg=lng, is_shared=_shared_with_sb)

        # Set device type to inheriting class type (aka, set it to "ZN" for Zone object)
        self.dv_type = 'ZN'

        # Zone specific attributes for both controllers
        self.en = 'TR'              # Enabled / Disabled (TR | FA)
        self.df = 0.0               # Design Flow (GPM)
        self.va = 0.23              # Solenoid Current
        self.vv = 28.7              # Solenoid Voltage
        self.vt = 1.7               # Two wire drop
        self.last_ss = ''           # last known status

        # Weather-Based Watering Additional Attributes
        self.kc = 0.00              # Crop coefficient for hydrozone
        self.pr = 0.00              # Precipitation rate for hydrozone
        self.du = 0.00              # Distribution Uniformity for hydrozone
        self.rz = 0.00              # Root zone working water storage capacity in as inches per foot

        # Attributes that are not sent to the zone where there is no setters or verifiers for these attributes
        self.allowable_depletion = 0.00  # Percentage of allowable depletion assigned to the zone
        self.sun_exposure = 0.00         # Percentage of Sun exposure assigned to the zone
        self.slope = 0.00                # Percentage of Slope assigned to the zone
        self.soil_type = ''              # Soil Type pass in a string
        self.gross_area = 0              # Gross Area returned in square feet
        self.root_depth = 0.00           # Root Depth in as inches per foot
        # this is set by the set enable et state on cn from the ZP.py object
        self.enable_et = 'FA'            # set zone to use ET based watering

        # Attributes that are not sent to the zone where there is no setters but there is verifiers for these attributes
        self.yesterdays_mb = 0.00   # Yesterdays Moisture Balance assigned to a zone
        self.mb = 0.00              # Moisture Balance assigned to a zone
        self.rn = 0.00              # rainfall assigned to a zone
        self.yesterdays_rt = 0.00   # Yesterdays runtime assigned to a zone
        self.rt = 0.00              # runtime assigned to a zone
        self.etc = 0.00             # ETC assigned to a zone

        # 1000 controller specific attributes
        # TODO: Need to implement methods below for 1000 implementation
        self.mr = 0                 # Mirror Zone (Zone # | 0=none)
        self.mv = 1                 # Pointer to a master valve to be ran when the zone is ran
        self.ff = 'FA'              # High Flow Variance Fault (TR | FA)
        self.so = 0                 # zone soak time for a 1000 only
        self.ct = 0                 # zone cycle time for a 1000 only
        
        # these are place holders to keep track of the status of the zone for each minute
        self.seconds_zone_ran = 0
        self.seconds_zone_soaked = 0
        self.seconds_zone_waited = 0
        self.seconds_zone_paused = 0
        # these are place holders to keep track of the status of the zone for each minute
        self.seconds_for_each_cycle_duration = []  # this is the minutes a zone cycled before it soaked
        self.seconds_for_each_soak_duration = []  # this is the minutes a zone soaked before it went to waiting or watering



    #################################
    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        # Update current description in case we changed address or serial number
        self.ds = self.ds if self.ds == self.build_description() else self.build_description()

        if self.controller_type == "10":
            command = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13},{14}={15},{16}={17}," \
                      "{18}={19},{20}={21},{22}={23},{24}={25}".format(
                          opcodes.zone,                                     # {0}
                          str(self.ad),                                     # {1}
                          opcodes.enabled,                                  # {2}
                          str(self.en),                                     # {3}
                          opcodes.description,                              # {4}
                          str(self.ds),                                     # {5}
                          opcodes.latitude,                                 # {6}
                          str(self.la),                                     # {7}
                          opcodes.longitude,                                # {8}
                          str(self.lg),                                     # {9}
                          opcodes.design_flow,                              # {10}
                          str(self.df),                                     # {11}
                          opcodes.mirror_zone,                              # {12}
                          str(self.mr),                                     # {13}
                          opcodes.master_valve,                             # {14}
                          str(self.mv),                                     # {15}
                          opcodes.high_flow_variance,                       # {16}
                          str(self.ff),                                     # {17}
                          WaterSenseCodes.Crop_Coefficient,                 # {18}
                          str(self.kc),                                     # {19}
                          WaterSenseCodes.Precipitation_Rate,               # {20}
                          str(self.pr),                                     # {21}
                          WaterSenseCodes.Efficiency_Percentage,            # {22}
                          str(self.du),                                     # {23}
                          WaterSenseCodes.Root_Zone_Working_Water_Storage,  # {24}
                          str(self.rz)                                      # {25}
                      )
        else:
            command = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13},{14}={15},{16}={17}," \
                      "{18}={19}".format(
                          opcodes.zone,                                     # {0}
                          str(self.ad),                                     # {1}
                          opcodes.enabled,                                  # {2}
                          str(self.en),                                     # {3}
                          opcodes.description,                              # {4}
                          str(self.ds),                                     # {5}
                          opcodes.latitude,                                 # {6}
                          str(self.la),                                     # {7}
                          opcodes.longitude,                                # {8}
                          str(self.lg),                                     # {9}
                          opcodes.design_flow,                              # {10}
                          str(self.df),                                     # {11}
                          WaterSenseCodes.Crop_Coefficient,                 # {12}
                          str(self.kc),                                     # {13}
                          WaterSenseCodes.Precipitation_Rate,               # {14}
                          str(self.pr),                                     # {15}
                          WaterSenseCodes.Efficiency_Percentage,            # {16}
                          str(self.du),                                     # {17}
                          WaterSenseCodes.Root_Zone_Working_Water_Storage,  # {18}
                          str(self.rz)                                      # {19}
                      )

        # If the device is not shared with the substation, set these values because we own them.
        if not self.is_shared_with_substation:
            command += ",{0}={1},{2}={3},{4}={5}".format(
                opcodes.solenoid_current,                    # {12}
                self.va,                                     # {13}
                opcodes.solenoid_voltage,                    # {14}
                self.vv,                                     # {15}
                opcodes.two_wire_drop,                       # {16}
                self.vt                                      # {17}
            )

        try:
            # Attempt to set flow meter default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Zone {0}'s 'Default values' to: '{1}'".format(
                str(self.ad),   # {0}
                command         # {1}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Zone {0}'s 'Default values' to: {1}".format(
                str(self.ad),   # {0}
                command)        # {1}
            )

    #################################
    def set_enable_state_on_cn(self, _state=None):
        """
        Sets the Enabled State for the Zone on the controller. \n
        :param _state:  New enabled state for Zone (TR | FA)
        :type _state:   str
        """
        # if it is None, use current value
        if _state is not None:

            # If zone isn't enabled, make sure to update instance variable to match what the controller will show.
            if _state not in ['TR', 'FA']:
                e_msg = "Invalid enabled state entered for Zone {0}. Received: {1}, Expected: 'TR' or 'FA'".format(
                            str(self.ad),   # {0}
                            _state          # {1}
                        )
                raise ValueError(e_msg)
            else:
                self.en = _state

        # Command for sending
        command = "SET,ZN={0},EN={1}".format(
            str(self.ad),   # {0}
            str(self.en)    # {1}
        )
        try:
            # Attempt to enable flow meter
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Enabled State for Zone {0}".format(
                str(self.ad)    # {0}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Zone {0}'s 'Enabled State' to: {1}".format(
                str(self.ad),   # {0}
                str(self.en)    # {1}
            ))

    #################################
    def set_design_flow_on_cn(self, _df=None):
        """
        Sets the design flow for the zone on the controller to the value contained in 'self.df'. If the user passes
        in a design flow value as a parameter ('_df'), then it will overwrite the value that the instance contains as
        well as set the new value on the controller. \n
        :param _df:     Design flow value specified by user to overwrite current value.
        :type _df:      Integer, Float.
        :return:
        """

        # Set instance attribute to design flow passed in prior to setting at the controller
        if _df is not None:

            # Verifies the design flow passed in is an integer or float value
            if not isinstance(_df, (int, float)):
                e_msg = "Failed trying to set ZN {0} design flow. Invalid design flow type, Received type: {1}, " \
                        "Expected: int or float.".format(
                            str(self.ad),   # {0}
                            type(_df)       # {1}
                        )
                raise TypeError(e_msg)
            else:
                # Valid type, overwrite current value with new value
                self.df = _df

        # Command for sending
        command = "SET,ZN={0},DF={1}".format(
            str(self.ad),   # {0}
            str(self.df)    # {1}
        )

        try:
            # Attempt to set design flow at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Zone {0}'s 'DF' to: '{1}'".format(str(self.ad), str(self.df))
            raise Exception(e_msg)
        else:
            print("Successfully set Zone {0}'s 'Design Flow' to: {1}".format(
                str(self.ad),   # {0}
                str(self.df)    # {1}
            ))

    #################################
    def set_solenoid_current_on_cn(self, _va=None):
        """
        Sets the solenoid's current value on the controller for the zone. \n
        By passing in a value for '_va', method assumes this value to overwrite current value. \n
        :param _va:     Value to set for solenoid current.
        :type _va:      Integer, Float
        :return:
        """
        # Check if user wants to overwrite current value
        if _va is not None:

            # Verifies the solenoid current passed in is an integer / float type
            if not isinstance(_va, (int, float)):
                e_msg = "Failed trying to set ZN {0}'s solenoid current. Invalid type passed in, expected int or " \
                        "float. Received type: {1}".format(
                            str(self.ad),   # {0}
                            type(_va)       # {1}
                        )
                raise TypeError(e_msg)
            else:
                # Valid type, overwrite current value with new value
                self.va = _va

        # Command for sending
        command = "SET,ZN={0},VA={1}".format(
            str(self.ad),   # {0}
            str(self.va)    # {1}
        )

        try:
            # Attempt to set solenoid current for zone at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Zone {0}'s 'Solenoid Current' to: '{1}'".format(
                str(self.ad),   # {0}
                str(self.va)    # {1}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Zone {0}'s 'Solenoid Current' to: {1}".format(
                str(self.ad),   # {0}
                str(self.va)    # {1}
            ))

    #################################
    def set_solenoid_voltage_on_cn(self, _vv=None):
        """
        Sets the solenoid's voltage value on the controller for the zone. \n
        By passing in a value for '_vv', method assumes this value to overwrite current value. \n
        :param _vv:     Value to set for solenoid voltage.
        :type _vv:      Integer, Float
        :return:
        """
        # Check if user wants to overwrite current value
        if _vv is not None:

            # Verify the correct type is passed in.
            if not isinstance(_vv, (int, float)):
                e_msg = "Failed trying to set ZN {0}'s solenoid voltage. Invalid type passed in, expected int or " \
                        "float. Received type: {1}".format(
                            str(self.ad),   # {0}
                            type(_vv)       # {1}
                        )
                raise TypeError(e_msg)
            else:
                # Valid type, overwrite current value with new value
                self.vv = _vv

        # Command for sending
        command = "SET,ZN={0},VV={1}".format(
            str(self.ad),   # {0}
            str(self.vv)    # {1}
        )

        try:
            # Attempt to set solenoid voltage for zone at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Zone {0}'s 'Solenoid Voltage' to: '{1}'".format(str(self.ad),
                                                                                                      str(self.vv))
            raise Exception(e_msg)
        else:
            print("Successfully set Zone {0}'s 'Solenoid Voltage' to: {1}".format(str(self.ad), str(self.vv)))

    #################################
    def set_two_wire_drop_value_on_cn(self, _value=None):
        """
        Set the two wire drop value for the Zone (Faux IO Only) \n
        By passing in a value for '_value', method assumes this value to overwrite current value. \n
        :param _value:        Value to set the Zone two wire drop value to \n
        :return:
        """
        # Check if user wants to overwrite current value
        if _value is not None:

            # Verify the correct type is passed in.
            if not isinstance(_value, (int, float)):
                e_msg = "Failed trying to set ZN {0}'s two wire drop value. Invalid type passed in, expected int or " \
                        "float. Received type: {1}".format(
                            str(self.ad),   # {0}
                            type(_value)    # {1}
                        )
                raise TypeError(e_msg)
            else:
                # Valid type, overwrite current value with new value
                self.vt = _value

        # Command for sending
        command = "SET,ZN={0},VT={1}".format(
            str(self.ad),   # {0}
            str(self.vt)    # {1}
        )

        try:
            # Attempt to set two wire drop for zone at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Zone {0}'s 'Two Wire Drop Value' to: '{1}'".format(
                    str(self.ad), str(self.vt))
            raise Exception(e_msg)
        else:
            print("Successfully set Zone {0}'s 'Two Wire Drop Value' to: {1}".format(str(self.ad), str(self.vt)))

    #################################
    def set_mirror_zone_number_on_cn(self, _mirrored_zone):
        """
        1000 Specific Method \n
        Sets the zone to mirror the specified zone number. \n
        :param _mirrored_zone:      Zone in which to mirror the current zone with. \n
        :type _mirrored_zone:       Integer \n
        :return:
        """
        # this is a 1000 specific method
        if self.controller_type == "10":

            # Verify integer is passed in
            if not isinstance(_mirrored_zone, int):
                e_msg = "Failed trying to set ZN {0} to mirror a zone. Not an integer passed in. Received type: " \
                        "{1}".format(
                            str(self.ad),           # {0}
                            type(_mirrored_zone)    # {1}
                        )
                raise TypeError(e_msg)

            # Set zone attribute before setting at the controller
            self.mr = _mirrored_zone

            # Command for sending
            command = "SET,ZN={0},MR={1}".format(
                str(self.ad),   # {0}
                str(self.mr)    # {1}
            )

            try:
                # Send command
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to set Zone {0} to Mirror Zone: {1}".format(str(self.ad),
                                                                                               str(self.mr))
                raise Exception(e_msg)
            else:
                print("Successfully set Zone {0} to Mirror Zone: {1}".format(str(self.ad), str(self.mr)))
        else:
            raise ValueError("Attempting to Mirror a Zone on a 3200, which is currently not supported.")

    #################################
    def set_master_valve_pointer_on_cn(self, _mv_number):
        """
        Sets the master valve number passed in as a reference for a master valve object for the zone. It's a pointer,
        which points to the address of the master valve.
        :param _mv_number:  A master valve address to point to.
        :return:
        """
        # this is a 1000 specific method
        if self.controller_type == "10":

            # Verify integer is passed in
            if not isinstance(_mv_number, int):
                e_msg = "Failed trying to set ZN {0}'s master valve. Not an integer passed in. Received type: " \
                        "{1}".format(
                            str(self.ad),           # {0}
                            type(_mv_number)        # {1}
                        )
                raise TypeError(e_msg)

            # Verify value is within accepted master valve address's
            elif _mv_number not in range(1, 8):
                e_msg = "Attempting to set reference to a master valve outside of available MV addresses: " \
                        "{0}".format(str(_mv_number))
                raise ValueError(e_msg)
            else:
                self.mv = _mv_number

            # Command for sending
            command = "SET,ZN={0},MV={1}".format(
                str(self.ad),   # {0}
                str(self.mv)    # {1}
            )

            try:
                # Send command
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to set Zone {0}'s MV to: MV {1}".format(str(self.ad), str(self.mv))
                raise Exception(e_msg)
            else:
                print("Successfully set Zone {0}'s MV to: MV {1}".format(str(self.ad), str(self.mv)))
        else:
            raise ValueError("Attempting to assign MV to a ZN on a 3200, which is currently not supported.")

    #################################
    def set_high_flow_variance_on_cn(self, _value=None):
        """
        1000 Specific method \n
        Sets the high flow variance value for the zone on the controller. \n
        By passing in a value for '_value', method assumes this value to overwrite current value. \n
        :rtype : object
        :param _value:  'TR' for a true, 'FA' for a false
        :type _value:   int \n
        """
        # this is a 1000 specific method
        if self.controller_type == "10":

            # Check if user wants to overwrite current value
            if _value is not None:

                # Verify integer is passed in
                if not isinstance(_value, str):
                    e_msg = "Failed trying to set ZN {0}'s high flow variance. Not a string passed in. Received " \
                            "type: {1}".format(str(self.ad), type(_value))
                    raise TypeError(e_msg)

                # Make sure entered correct value
                elif _value not in ["FA", "TR"]:
                    e_msg = "Exception occurred attempting to set incorrect 'High Flow Variance' value for " \
                            "controller: {0}. Expects 'TR' or 'FA'.".format(str(_value))
                    raise ValueError(e_msg)
                else:
                    # Valid type, overwrite current value with new value
                    self.ff = _value

            # Command for sending
            command = "SET,ZN={0},FF={1}".format(
                str(self.ad),   # {0}
                str(self.ff)    # {1}
            )

            try:
                # Send command
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to set Zone {0}'s high flow variance to: {1}".format(
                    str(self.ad), str(self.ff))
                raise Exception(e_msg)
            else:
                print("Successfully set Zone {0}'s high flow variance to: {1}".format(str(self.ad), str(self.ff)))
        else:
            raise ValueError("Attempting to set high flow variance to a ZN on a 3200, which is currently not "
                             "supported.")

    #################################
    def set_crop_coefficient_value_on_cn(self, _kc_value=None, _ks_value=None, _kd_value=None, _kmc_value=None):
        """
        - set a crop coefficient value on the zone on the controller \n
            - Send an KC value
        :param _kc_value:     KC value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _kc_value:     float

        :param _ks_value:     Species factor value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _ks_value:     float

        :param _kd_value:     Density factor value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _kd_value:     float

        :param _kmc_value:    Micro Climate value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _kmc_value:    float

        """
        # check to see if we are using_kc_value or _ks_value,_kd_value, _kmc_value
        # if _kc_value is none than _ks_value, _kd_value, _kmc_value must not be none
        if _kc_value is None and _ks_value is not None and _kd_value is not None and _kmc_value is not None:

            # Check if  _ks, and _kd_kmc are in range 0.00 - 5.00
            if (_ks_value < 0.00 or _ks_value > 5.00) \
                    or (_kd_value < 0.00 or _kd_value > 5.00) \
                    or (_kmc_value < 0.00 or _kmc_value > 5.00):
                e_msg = "Exception occurred trying to set the crop coefficient value. \n" \
                        "Species factor received was and Invalid was not between (0.00 and 5.00) received: {0}. \n" \
                        "Density factor received was and Invalid was not between (0.00 and 5.00) received: {1}. \n" \
                        "Micro Climate received was and Invalid was not between (0.00 and 5.00) received: {2}.\n"\
                        .format(
                            _ks_value,          # {0}
                            _kd_value,          # {1}
                            _kmc_value,         # {2}
                        )
                raise ValueError(e_msg)

            else:
                 # this returns the _kl which is set to the kc value
                cal_kl_value = equations.calculate_landscape_coefficient(_ks=_ks_value, _kd=_kd_value, _kmc=_kmc_value)

                # this just verifies after the math we make sure we still make the kc value a two decimal number
                self.kc = cal_kl_value

        else:
            # Check if  _kc is in range 0.00 - 5.00
            if (_kc_value < 0.00 or _kc_value > 5.00):
                e_msg = "Exception occurred trying to set the crop coefficient value. \n" \
                        "crop coefficient received was and Invalid was not between (0.00 and 5.00) received: {0}. \n" \
                        .format(
                            _kc_value,          # {0}
                        )
                raise ValueError(e_msg)
            self.kc = _kc_value

        # Command for sending
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.zone,                       # {0}
            self.ad,                            # {1}
            WaterSenseCodes.Crop_Coefficient,   # {2}
            self.kc                             # {3}
        )

        try:
            # Attempt to set ETo and date stamp on the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set the crop coefficient value on the zone: value = '{0}' " \
                    .format(self.kc)
            raise Exception(e_msg)
        print("Successfully set the initial crop coefficient value on controller and all zones = '{0}' "
              .format(self.kc))

    #################################
    def set_precipitation_rate_value_on_cn(self, _pr_value):
        """
        - set a precipitation_rate value on the zone on the controller \n
            - Send an precipitation rate value
        :param _pr_value:     Precipitation rate value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _pr_value:     float \n
        """
        # if not _pr_value in range(0.00, 5.00):
        if _pr_value < 0.0 or _pr_value > 5.0:
            e_msg = "Exception occurred trying to set the precipitation rate value. Value received {0} was not between"\
                    "(0.00 and 5.00)"\
                    .format(_pr_value)
            raise ValueError(e_msg)
        self.pr = helper_methods.truncate_float_value(_float_value=_pr_value)
        # Command for sending
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.zone,                   # {0}
            self.ad,                        # {1}
            opcodes.precipitation_rate,     # {2}
            self.pr)                        # {3}

        try:
            # Attempt to set ETo and date stamp on the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set the precipitation rate value on the zone " \
                    "value = '{0}' " \
                    .format(self.pr)
            raise Exception(e_msg)
        print("Successfully set the initial precipitation rate value on zone = '{0}' "
              .format(self.pr))

    #################################
    def set_distribution_uniformity_value_on_cn(self, _du_value):
        """
        - set a Distribution Uniformity value on the zone on the controller \n
            - Send an Distribution Uniformity value
        :param _du_value:     Distribution Uniformity value range between (0.00 and 1.00) and only 2 decimals 3.45 \n
        :type  _du_value:     float
        """
        # Check if in range 0.00 - 1.0
        if int(_du_value < 1 or _du_value > 250):
            e_msg = "Exception occurred trying to set the Distribution Uniformity value. Value received {0} was not " \
                    "between(1 and 100) "\
                    .format(_du_value)
            raise ValueError(e_msg)

        self.du = _du_value
        # Command for sending
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.zone,                               # {0}
            self.ad,                                    # {1}
            WaterSenseCodes.Efficiency_Percentage,      # {2}
            self.du                                     # {3}
        )

        try:
            # Attempt to set Distribution Uniformity on the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set the Distribution Uniformity value on the zone " \
                    "value = '{0}' " \
                    .format(self.du)
            raise Exception(e_msg)
        else:
            print("Successfully set the initial Distribution Uniformity value on zone = '{0}' "
                  .format(self.du))

    #################################
    def set_root_zone_working_storage_capacity_on_cn(self):
        """
        - set a Root Zone Water Holding Capacity  value on the zone on the controller \n
            - Send an Root zone water holding capacity  value
        :return self.rz:        Root zone water holding capacity value range between (0.00 and 1.00) and only 2 \n
                                decimals 3.45 \n
        :rtype  self.rz:      float \n
        """
        # Recalculate rz value (with current zone attributes)
        self.rz = equations.return_calculate_root_zone_working_water_storage(
            _sl=self.soil_type,     # the soil Type is set at the script
            _ad=self.allowable_depletion,     # the allowable depletion is set at the script
            _rd=self.root_depth)         # the root_depth is set at the script

        # Overwrite current attribute value
        self.rz = helper_methods.truncate_float_value(_float_value=self.rz)

        # Command for sending
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.zone,                                       # {0}
            self.ad,                                            # {1}
            WaterSenseCodes.Root_Zone_Working_Water_Storage,    # {2}
            self.rz                                             # {3}
        )

        try:
            # Attempt to set Root Zone Water Holding Capacity on the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set the Root Zone Water Holding Capacity value on the zone " \
                    "value = '{0}' " \
                    .format(self.rz)
            raise Exception(e_msg)
        else:
            print("Successfully set the initial Root Zone Water Holding Capacity value on zone = '{0}' "
                  .format(self.rz))

    #################################
    def set_cycle_time_on_cn(self, _cycle_time=None, _use_calculated_cycle_time=False):
        """
        Sets the 'Cycle Time' for the Zone Program on the controller. \n
        :param _cycle_time:                     Cycle time to set \n
        :type _cycle_time:                      int \n

        :param _use_calculated_cycle_time:      Cycle time to set \n
        :type _use_calculated_cycle_time:       bool  | float\n
        """

        if self.controller_type == "10":
            # this goes out to the equations module and returns a calculated Cycle time to the zp.py object
            if _use_calculated_cycle_time is True:
                _cycle_time = self.get_calculated_cycle_time()
            # If trying to overwrite current value
            if _cycle_time is not None:

                # Overwrite current attribute value
                # take the value in seconds divide by 60 to convert it to minutes convert it to an integer and than
                # convert back to seconds this way you are dropping of the fractional part of the seconds
                self.ct = 60*(int(_cycle_time/60))

            command = "SET,{0}={1},{2}={3}".format(
                      opcodes.zone,         # {0}
                      str(self.ad),         # {1}
                      opcodes.cycle_time,   # {2}
                      str(self.ct)          # {3}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone {0}, 'Cycle Time' to: {1}\n " \
                        "{2}".format(
                            str(self.ad),               # {0}
                            str(self.ct),               # {1}
                            str(ae.message)             # {2}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Cycle Time' for (Zone {0}, to: {1}".format(
                      str(self.ad),      # {0}
                      str(self.ct)       # {1}          # {2}
                      ))
        else:
            raise ValueError("Attempting to set Zone Cycle Time not Zone Program Cycle Time for 3200 "
                             "which is NOT SUPPORTED")

    #################################
    def set_soak_time_on_cn(self, _soak_time=None, _use_calculated_soak_time=False):
        """
        Sets the 'Soak Time' for the Zone Program on the controller. \n
        :param _soak_time:                      Soak time to set \n
        :type _soak_time:                       int  | float\n

        :param _use_calculated_soak_time:       Soak time to set \n
        :type _use_calculated_soak_time:        bool  | float\n
        """
        if self.controller_type == "10":
            # this goes out to the equations module and returns a calculated soak time to the zp.py object
            if _use_calculated_soak_time is True:
                _soak_time = self.get_calculated_soak_time()

            # If trying to overwrite current value
            if _soak_time is not None:

                # Overwrite current attribute value
                # take the value in seconds divide by 60 to convert it to minutes convert it to an integer and than
                # add 1 to the value to always round up and then convert back to seconds
                if _soak_time == 0:
                    self.so = 0
                else:
                    self.so = 60*(math.ceil(_soak_time/60.0))

            command = "SET,{0}={1},{2}={3}".format(
                      opcodes.zone,         # {0}
                      str(self.ad),         # {1}
                      opcodes.soak_time,   # {2}
                      str(self.so)          # {3}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone {0}, 'Soak Time' to: {1}\n " \
                        "{2}".format(
                            str(self.ad),               # {0}
                            str(self.so),               # {1}
                            str(ae.message)             # {2}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Soak Time' for (Zone {0}, to: {1}".format(
                      str(self.ad),      # {0}
                      str(self.so)       # {1}
                      ))
        else:
            raise ValueError("Attempting to set zone Soak Time for a Zone for 3200 which is NOT SUPPORTED")

    #################################
    def get_calculated_cycle_time(self):

        _calculated_cycle_time = equations.return_calculated_cycle_time(
            _sl=self.soil_type,     # the soil type is set at the script
            _pr=self.pr,            # the precipitation rate is set at the script
            _sp=self.slope)         # the soil is set at the script
        return _calculated_cycle_time

    #################################
    def get_calculated_soak_time(self):

        _calculated_soak_time = equations.return_calculated_soak_time(
            _sl=self.soil_type,     # the soil type is set at the script
            _pr=self.pr,            # the precipitation rate is set at the script
            _sp=self.slope,         # the soil is set at the script
            _du=self.du)            # the distribution uniformity is set at the script
        return _calculated_soak_time

    #################################
    def verify_crop_coefficient_value_on_cn(self):
        """
        Verifies the crop coefficient set for this Zone on the Controller. Expects the controller's value and this
        instance's value to be equal.
        :return:
        """
        zn_kc_on_cn = float(self.data.get_value_string_by_key(WaterSenseCodes.Crop_Coefficient))

        # Compare status versus what is on the controller
        if self.kc != zn_kc_on_cn:
            e_msg = "Unable to verify Zone {0}'s crop coefficient. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(zn_kc_on_cn),   # {1}
                str(self.kc)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s crop coefficient: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.kc)      # {1}
                  ))

    #################################
    def verify_precipitation_rate_value_on_cn(self):
        """
        Verifies the precipitation rate set for this Zone on the Controller. Expects the controller's value and this
        instance's value to be equal.
        :return:
        """
        zn_pr_on_cn = float(self.data.get_value_string_by_key(opcodes.precipitation_rate))

        # Compare status versus what is on the controller
        if self.pr != zn_pr_on_cn:
            e_msg = "Unable to verify Zone {0}'s precipitation rate. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(zn_pr_on_cn),   # {1}
                str(self.pr)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s precipitation rate: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.pr)      # {1}
                  ))

    #################################
    def verify_distribution_uniformity_value_on_cn(self):
        """
        Verifies the Distribution Uniformity set for this Zone on the Controller. Expects the controller's value and
        this instance's value to be equal.
        :return:
        """
        zn_du_on_cn = float(self.data.get_value_string_by_key(WaterSenseCodes.Distribution_uniformity))

        # Compare status versus what is on the controller
        if self.du != zn_du_on_cn:
            e_msg = "Unable to verify Zone {0}'s Distribution Uniformity. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(zn_du_on_cn),   # {1}
                str(self.du)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s Distribution Uniformity: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.du)      # {1}
                  ))

    #################################
    def verify_root_zone_working_storage_capacity_on_cn(self):
        """
        Verifies the Root Zone Water Holding Capacity set for this Zone on the Controller. Expects the controller's
        value and this instance's value to be equal.
        :return:
        """
        zn_rz_on_cn = float(self.data.get_value_string_by_key(WaterSenseCodes.Root_Zone_Working_Water_Storage))

        # Compare status versus what is on the controller
        if self.rz != zn_rz_on_cn:
            e_msg = "Unable to verify Zone {0}'s Root Zone Water Holding Capacity . Received: {1}, Expected: {2}"\
                .format(
                    str(self.ad),       # {0}
                    str(zn_rz_on_cn),   # {1}
                    str(self.rz)        # {2}
                )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s Root Zone Water Holding Capacity: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.rz)      # {1}
                  ))

    #################################
    def verify_moisture_balance_on_cn(self):
        """
        Verifies the moisture balance set for this Zone on the Controller. Expects the controller's value and this
        instance's value to be equal.
        :return:
        """
        self.mb = equations.return_todays_calculated_moisture_balance(
            _yesterdays_mb=self.yesterdays_mb,
            _yesterdays_rt=self.yesterdays_rt,
            _pr=self.pr,
            _du=self.du,
            _ra=self.ra,
            _etc=self.etc,
        )
        # check Moisture balance against the root zone working storage capacity
        # - if the calculate moisture balance is higher than the root zone working storage capacity
        # -  than set the moisture balance equal to the root zone working storage capacity so that there is not
        # -  more water applied than the root zone can handle
        # rz is a positive number after calculation mb is a neg number
        # this makes rz a negative for comparison
        neg_rz = -self.rz
        if self.mb < neg_rz:
            self.mb = neg_rz
            print "Moisture Balance was adjusted due to the fact that the root zone couldn't hold that much water"
        print("Print numbers use to Calculate MB  yesterdays_mb: '{0}' yesterdays_rt: '{1}' kc: '{2}'  "
              "pr: '{3} du: '{4}' ra: '{5}'  etc: '{6}' Calculated mb: '{7}' on controller".format(
                  str(self.yesterdays_mb),      # {0}
                  str(self.yesterdays_rt),      # {1}
                  str(self.kc),                 # {2}
                  str(self.pr),                 # (3)
                  str(self.du),                 # {4}
                  str(self.ra),                 # {5}
                  str(self.etc),                # {6}
                  str(self.mb),))               # {7}
        # Compare status versus what is on the controller
        # Todo add if runtime is less than 4 minutes make runtime = to zero
        zn_mb_on_cn = float(self.data.get_value_string_by_key(WaterSenseCodes.Daily_Moisture_Balance))
        rounded_mb = round(number=self.mb, ndigits=3)
        if abs(rounded_mb - zn_mb_on_cn) > 0.01:
            e_msg = "Unable to verify Zone {0}'s daily moisture balance. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(zn_mb_on_cn),   # {1}
                str(rounded_mb)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s calculated daily moisture balance: '{1}' against daily moisture balance on "
                  "controller: '{2}'".format(
                    str(self.ad),     # {0}
                    str(self.mb),     # {1}
                    str(zn_mb_on_cn)  # {2}
                    ))

    #################################
    def verify_design_flow_on_cn(self):
        """
        Verifies the design flow set for this Zone on the Controller. Expects the controller's value and this
        instance's value to be equal.
        :return:
        """
        zn_df_on_cn = float(self.data.get_value_string_by_key('DF'))

        # Compare status versus what is on the controller
        if self.df != zn_df_on_cn:
            e_msg = "Unable to verify Zone {0}'s design flow. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(zn_df_on_cn),   # {1}
                str(self.df)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s design flow: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.df)      # {1}
                  ))

    ################################
    def verify_solenoid_current_on_cn(self):
        """
        Verifies the solenoid current set for this Zone on the Controller. Expects the controller's value and this
        instance's value to be equal.
        :return:
        """
        # Compare status versus what is on the controller
        zn_va_on_cn = float(self.data.get_value_string_by_key('VA'))
        rounded_va = round(number=self.va, ndigits=2)
        if abs(rounded_va - zn_va_on_cn) > 0.02:
            e_msg = "Unable to verify Zone {0}'s solenoid current. Received: {1}, Expected: {2}".format(
                    str(self.ad),       # {0}
                    str(zn_va_on_cn),   # {1}
                    str(self.va)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s solenoid current value: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.va)      # {1}
                  ))

    #################################
    def verify_solenoid_voltage_on_cn(self):
        """
        Verifies the solenoid voltage set for this Zone on the Controller. Expects the controller's value and this
        instance's value to be equal.
        :return:
        """

        # Compare status versus what is on the controller
        zn_vv_on_cn = float(self.data.get_value_string_by_key('VV'))
        rounded_vv = round(number=self.vv, ndigits=2)
        if abs(rounded_vv - zn_vv_on_cn) > 0.02:
            e_msg = "Unable to verify Zone {0}'s solenoid voltage. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(zn_vv_on_cn),   # {1}
                str(self.vv)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s solenoid voltage value: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.vv)      # {1}
                  ))

    #################################
    def verify_two_wire_drop_value_on_cn(self):
        """
        Verifies the two wire drop value for this Zone on the Controller. Expects the controller's value and this
        instance's value to be equal.
        :return:
        """

        # Compare status versus what is on the controller
        two_wire_drop_val = float(self.data.get_value_string_by_key('VT'))
        rounded_vt = round(number=self.vt, ndigits=2)
        if abs(rounded_vt - two_wire_drop_val) > 0.02:
            e_msg = "Unable to verify Zone {0}'s two wire drop value. Received: {1}, Expected: {2}".format(
                    str(self.ad),               # {0}
                    str(two_wire_drop_val),     # {1}
                    str(self.vt)                # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s two wire drop value: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.vt)      # {1}
                  ))

    #################################
    def verify_mirrored_zone_on_cn(self):
        """
        Verifies the mirrored zone value associated with this zone on the controller.
        :return:
        """
        # this is a 1000 specific method
        if self.controller_type == "10":
            mirrored_zone_val = int(self.data.get_value_string_by_key('MR'))

            # Compare mirrored zone value returned versus what is assigned to the zone currently
            if self.mr != mirrored_zone_val:
                e_msg = "Unable to verify Zone {0}'s mirrored zone. Received: {1}, Expected: {2}".format(
                    str(self.ad),               # {0}
                    str(mirrored_zone_val),     # {1}
                    str(self.mr)                # {2}
                )
                raise ValueError(e_msg)
            else:
                print("Verified Zone {0}'s mirrored zone value: '{1}' on controller".format(
                    str(self.ad),     # {0}
                    str(self.mr)      # {1}
                    ))
        else:
            raise ValueError("Attempting to verify mirrored zone for a zone on a 3200, which is currently not "
                             "supported.")

    #################################
    def verify_master_valve_assigned_on_cn(self):
        """
        Verifies the master valve assigned to this zone. \n
        :return:
        """
        # this is a 1000 specific method
        if self.controller_type == "10":
            assigned_mv_zone_val = self.data.get_value_string_by_key('MV')
            if assigned_mv_zone_val is not None:
                # TODO this is giving me a not literal int type in some cases (running 1000 2015 use case 6)
                # assigned_mv_zone_val = int(assigned_mv_zone_val)
                # Compare master valve assignment returned versus what is assigned to the zone currently
                if self.mv != int(assigned_mv_zone_val):
                    e_msg = "Unable to verify Zone {0}'s Master Valve pointer. Received: {1}, Expected: {2}".format(
                        str(self.ad),               # {0}
                        str(assigned_mv_zone_val),  # {1}
                        str(self.mv)                # {2}
                    )
                    raise ValueError(e_msg)
                else:
                    print("Verified Zone {0}'s assignment to master value: '{1}' on controller".format(
                        str(self.ad),     # {0}
                        str(self.mv)      # {1}
                    ))
            else:
                print("Verified Zone {0}'s master value: '{1}' on controller is set to None".format(
                    str(self.ad),     # {0}
                    str(self.mv)      # {1}
                ))
        else:
            raise ValueError("Attempting to verify Master valve assignment for a zone on a 3200, which is "
                             "currently not supported.")

    #################################
    def verify_high_flow_variance_val_on_cn(self):
        """
        verifies the high flow variance value on the controller versus what the object currently has set for a value. \n
        :return:
        """
        # this is a 1000 specific method
        if self.controller_type == "10":
            high_flow_variance_val = self.data.get_value_string_by_key('FF')
            if high_flow_variance_val != 'FA':
                high_flow_variance_val = int(high_flow_variance_val)
                # Compare high flow variance value returned versus what is assigned to the zone currently
                if self.ff != high_flow_variance_val:
                    e_msg = "Unable to verify Zone {0}'s high flow variance. Received: {1}, Expected: {2}".format(
                        str(self.ad),                   # {0}
                        str(high_flow_variance_val),    # {1}
                        str(self.ff)                    # {2}
                    )
                    raise ValueError(e_msg)
                else:
                    print("Verified Zone {0}'s high flow variance value: '{1}' on controller".format(
                        str(self.ad),     # {0}
                        str(self.ff)      # {1}
                        ))
            else:
                print("Verified Zone {0}'s high flow variance value: '{1}' on controller is set to False".format(
                    str(self.ad),     # {0}
                    str(self.ff)      # {1}
                ))
        else:
            raise ValueError("Attempting to verify high flow variance for a zone on a 3200, which is currently "
                             "not supported.")

    #################################
    def verify_enable_state_on_cn(self):
        """
        Verifies that the enabled state for the zone on the controller. \n
        :return:
        """
        enable_state = self.data.get_value_string_by_key('EN')

        # Compare status versus what is on the controller
        if self.en != enable_state:
            e_msg = "Unable to verify Zone {0}'s enabled state. Received: {1}, Expected: {2}".format(
                    str(self.ad),       # {0}
                    str(enable_state),  # {1}
                    str(self.en)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s enabled state: '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.en))   # {1}
            )

    #################################
    def self_test_and_update_object_attributes(self):
        """
        this does a self test on the device
        than does a get date a resets the attributes of the device to match the controller
        self.va         # Solenoid Current
        self.vv         # Solenoid Voltage
        self.vt         # Two wire drop
        """
        try:
            self.do_self_test()
            self.get_data()
            self.va = float(self.data.get_value_string_by_key(opcodes.solenoid_current))
            self.vv = float(self.data.get_value_string_by_key(opcodes.solenoid_voltage))
            self.vt = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        except Exception as e:
            e_msg = "Exception occurred trying to update attributes of the zone {0} object." \
                    " Solenoid Current Value: '{1}'," \
                    " Solenoid Voltage'Value: '{2}',"\
                    " Two Wire Drop Value: '{3}'"\
                .format(
                    str(self.ad),  # {0}
                    str(self.va),  # {1}
                    str(self.vv),  # {2}
                    str(self.vt)   # {3}
                )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the zone {0} object. "
                  "Solenoid Current Value: '{1}', "
                  "Solenoid Voltage'Value: '{2}', "
                  "Two Wire Drop Value: '{3}'" .format(
                    str(self.ad),  # {0}
                    str(self.va),  # {1}
                    str(self.vv),  # {2}
                    str(self.vt)   # {3}
                  ))

    #################################
    def verify_who_i_am(self, _expected_status=None, _expected_moisture_balance=None, include_messages=False,
                        _status_code=None):
        """

        TODO how do i verify status or where is it passed in from
        TODO need to add verify for moisture balance its a read only
        Verifier wrapper which verifies all attributes for this 'Zone'. \n
        :return:
        Get all information about the device from the controller.
        :param _expected_status
        :type _expected_status
        :param _expected_moisture_balance
        :type _expected_moisture_balance
        :param include_messages
        :type include_messages
        :param _status_code
        :type _status_code
        """

        self.get_data()

        # Verify base attributes
        self.verify_description_on_cn()
        self.verify_latitude_on_cn()
        self.verify_longitude_on_cn()
        if _expected_status is not None:
            self.verify_status_on_cn(status=_expected_status)
        # if the device is shared by a substation than do a self test and update object attributes
        if self.is_shared_with_substation:
            self.self_test_and_update_object_attributes()
        # Verify Zone specific attributes
        self.verify_serial_number_on_cn()
        self.verify_enable_state_on_cn()
        self.verify_design_flow_on_cn()
        self.verify_solenoid_current_on_cn()
        self.verify_solenoid_voltage_on_cn()
        self.verify_two_wire_drop_value_on_cn()

        # For 1000 controller
        if self.controller_type == "10":

            # Verify Zone specific attributes for a 1000
            self.verify_mirrored_zone_on_cn()
            self.verify_master_valve_assigned_on_cn()
            self.verify_high_flow_variance_val_on_cn()

        # For Water-Based Watering - If we set a value for 'ei', then verify all pertinent data
        if self.enable_et != opcodes.false:
            self.verify_etc_value_on_cn()
            self.verify_crop_coefficient_value_on_cn()
            self.verify_precipitation_rate_value_on_cn()
            self.verify_distribution_uniformity_value_on_cn()
            self.verify_root_zone_working_storage_capacity_on_cn()
            self.verify_crop_coefficient_value_on_cn()
            self.verify_moisture_balance_on_cn()
        if include_messages:
            self.verify_message_on_cn(_status_code)
