import math
import status_parser
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.ser import Ser
from old_32_10_sb_objects_dec_29_2017.common.epa_package.wbw_imports import *
from old_32_10_sb_objects_dec_29_2017.common.epa_package import equations
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import moisture_sensors, zone_programs
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
# from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zn import Zone
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages
from datetime import datetime, timedelta


__author__ = 'baseline'


class ZoneProgram(object):
    """
    Zone Program Object. \n
    """
    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    # Serial Instance
    ser = Ser.serial_conn

    # Browser Instance
    browser = ''

    # Controller type for controller specific methods across all devices
    controller_type = ''

    # Moisture sensor object reference which is pointed from object_bucket.py

    moisture_sensor_objects = None
    zone_objects = None
    zone_program_objects = None

    #################################
    def __init__(self, zone_obj, prog_obj, _rt=None, _ct=None, _so=None, _pz=None, _ra=None, _ws=None, _ms=None,
                 _ll=None, _ul=None, _cc=None, _ee=None):
        """
        Zone Program object constructor. The same object is used for the 1000 and 3200 controllers. \n
        -   A run time is required for the zone program for use on both the 1000 and
            3200 controller.
        -   1000 controller parameters: _rt
        -   3200 controller parameters: _rt, _ct, _so, _pz, _ra, _ws, _ms, _ll, _ul, _cc, _ee

        :param zone_obj: Address of one of the zone objects to convert into a program zone \n
        :type zone_obj:  Zone

        :param prog_obj: Program address from one of the program objects to associate this zone program to. \n
        :type prog_obj:  PG1000 | PG3200

        :param _rt:     Run time (in seconds) \n
        :type _rt:      int \n

        :param _ct:     Cycle time (in seconds) \n
        :type _ct:      int \n

        :param _so:     Soak time (in seconds) \n
        :type _so:      int \n

        :param _pz:     Primary zone number ('0': timed, 'Zone #': self & linked) \n
                        i.e:
                            - a timed ZoneProgram has a value: _pz=0 \n
                            - a Primary ZoneProgram has a value: _pz=itself \n
                            - a Linked ZoneProgram has a value: _pz=primary zone number \n
        :type _pz:      int \n

        :param _ra:     Runtime tracking ratio (percent) - linked zones only \n
        :type _ra:      int \n

        :param _ws:     Water strategy ('TM': Timed, 'LL': Lower Limit, 'UL': Upper Limit) \n
        :type _ws:      str \n

        :param _ms:     Primary zone assigned moisture sensor address \n
        :type _ms:      int \n

        :param _ll:     Lower limit watering threshold (percent) \n
        :type _ll:      float \n

        :param _ul:     Upper limit watering threshold (percent) \n
        :type _ul:      float \n

        :param _cc:     Calibration cycle ('NV': Never, 'SG': Single, 'MO': Monthly) \n
        :type _cc:      str \n

        :param _ee:     Enable et Calculated Runtime (Default value is 'FA')\n
        :type _ee:      str \n
        """
        # Assign objects at init time
        # self.zone_objects = zones
        self.moisture_sensor_objects = moisture_sensors
        self.zone_program_objects = zone_programs

        self.data = status_parser.KeyValues('')

        # Address of a zone (from a zone object) to associate this zone program with. This could be different from the
        # primary zone number parameter (_pz) if this zone program is linked to a different zone or has a strategy of
        # timed.
        self.zone = zone_obj
        self.program = prog_obj

        # Address of a program (from a program object) to associate this zone program with.
        # self.program.ad = prog_num

        # Run time
        if _rt:
            self.rt = _rt
        else:
            self.rt = 60

        # Check to see if we are a 3200 controller
        if self.controller_type == "32":

            # Runtime Tracking Ratio
            if _ra:
                self.ra = _ra
            else:
                self.ra = 100   # default to 100%

            # Cycle Time Passed In
            if _ct:
                self.ct = _ct
            else:
                self.ct = 0     # default to 0 seconds

            # Soak time passed in
            if _so:
                self.so = _so
            else:
                self.so = 0     # default to 0 seconds
            # TODO if zone is a linked need to check what program the primary is in and set it to that or raise an exeception
            # Zone number
            if _pz:
                self.pz = _pz
            else:
                self.pz = 0     # default to '0' - Timed

            self.linked = self.pz != self.zone.ad and self.pz != 0
            self.timed = self.pz == 0

            # Water Strategy
            if _ws:
                self.ws = _ws
            else:
                self.ws = 'TM'  # default to 'TM' - Timed

            # Primary Zone Moisture Sensor
            if _ms:
                self.ms = _ms
            else:
                self.ms = ''    # default to ''

            # Lower limit threshold
            if _ll:
                self.ll = _ll
            else:
                self.ll = 26.0    # default to ''

            # Upper limit threshold
            if _ul:
                self.ul = _ul
            else:
                self.ul = 30.0   # default to ''

            # Calibration Cycle
            if _cc:
                self.cc = _cc
            else:
                self.cc = 'NV'  # default to 'NA' - Never

            # Enable et Calculated Runtime
            if _ee:
                self.ee = _ee
            else:
                self.ee = 'FA' # default to 'FA' Enabled false

            # Re-calculate run time based off of runtime tracking ratio (this needs to be done in case the program
            # zone is linked and have the run times be a percentage of the primary zone)
            self.rt = self.calculate_3200_runtime(runtime=self.rt)
            # Re-calculate cycle time based off of runtime tracking ratio (this needs to be done in case the program
            # zone is linked and have the cycle times be a percentage of the primary zone)
            self.ct = self.calculate_3200_cycletime(cycletime=self.ct)
            # if a zone is a linked zone the soak time is equal to the primary zone soak time
            self.so = self.calculate_3200_soaktime(soaktime=self.so)

        self.send_programming_to_cn()

        # Holds the messages return value
        # self.build_message_string = ''

    #################################
    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    #################################
    def calculate_3200_runtime(self, runtime):
        """
        Calculates the runtime to return based on the 'Runtime Tracking Ratio' set for the 3200.
        if et is enabled on a zone that the runtime gets rounded to the nearest second
        :param runtime:     Run time / cycle time to recalculate \n
        :type runtime:      int
        """
        # TODO do need to check if et is enabled
        if self.linked:
            runtime = self.zone_program_objects[self.pz].rt
        if runtime < 60:
            return 60
        seconds = runtime * (float(self.ra) / float(100))
        # this calculates if we are divisible by 60 or not, if not we want to add it to the total (for rounding)
        if self.ee == opcodes.true:
            # TODO the controller is not rounding
            remainder = seconds % 60.0

            # if odd minutes
            if remainder != 0:
                seconds += remainder
                return int(seconds)
        else:
            return int(seconds)

    #################################
    def calculate_3200_cycletime(self, cycletime):
        """
        Calculates the cycletime to return based on the 'Runtime Tracking Ratio' set for the 3200.
        if et is enabled on a zone that the cycletime gets rounded to the nearest second
        :param cycletime:     Run time / cycle time to recalculate \n
        :type cycletime:      int
        """
        if self.linked:
            cycletime = self.zone_program_objects[self.pz].ct
        if self.ee is opcodes.true:
            if cycletime < 60:
                return 60
        # TODO need to look if RA is = to zero
        seconds = cycletime * (float(self.ra) / float(100))
        # this calculates if we are divisible by 60 or not, if not we want to add it to the total (for rounding)
        # if self.ee == opcodes.true:
        # TODO the controller is not rounding
        remainder = seconds % 60.0

        # if odd minutes
        if remainder != 0:
            seconds += remainder
            return int(seconds)
        else:
            return int(seconds)

    #################################
    def calculate_3200_soaktime(self, soaktime):
            """
            Calculates the soaktime based on the primary zone
            :param soaktime:     soaktime
            :type soaktime:      int
            """
            if self.linked:
                soaktime = self.zone_program_objects[self.pz].so
            else:
                soaktime = self.so
            return int(soaktime)

    #################################
    def build_obj_configuration_for_send(self):
        """
        Builds the 'SET' string for sending this Zone Program Instance to the Controller. This method was created in
        this case only for the 3200 Zone Program because there are numerous attributes that could have no value when
        trying to set at the controller, thus we want to make sure to not send any empty attributes. \n
        :return:
        :rtype:
        """
        # Starting string, always will need this, 'SET,PZ=Blah,PG=Blah'
        current_config = "SET,{0}={1},{2}={3}".format(
            opcodes.zone_program,       # {0}
            str(self.zone.ad),      # {1}
            opcodes.program,            # {2}
            str(self.program.ad),   # {3}
        )

        # 3200 Zone Program attribute list of tuples (each tuple represents the OpCode string and associated value)
        if self.linked:
            if self.program.ad != self.zone_program_objects[self.pz].program.ad:
                e_msg = "Zone must be in the same program as the primary zone"
                raise Exception(e_msg)
            zone_program_attr = [
                (opcodes.runtime_tracking_ratio, self.ra),
                (opcodes.zone_program, self.pz),
            ]
        else:
            zone_program_attr = [
                (opcodes.run_time, self.rt),
                (WaterSenseCodes.Enable_ET_Runtime, self.ee),
                (opcodes.cycle_time, self.ct),
                (opcodes.soak_cycle, self.so),
                (opcodes.runtime_tracking_ratio, self.ra),
                (opcodes.zone_program, self.pz),
                (opcodes.water_strategy, self.ws),
                (opcodes.moisture_sensor, self.ms),
                (opcodes.lower_limit, self.ll),
                (opcodes.upper_limit, self.ul),
                (opcodes.calibrate_cycle, self.cc)

            ]
        # For each attribute, check if the attribute has a value that isn't 'None' or '' (empty string).
        for attr_tuple in zone_program_attr:
            arg1 = attr_tuple[0]
            arg2 = attr_tuple[1]

            # If the attribute value isn't 'None' or '', append it to the current_config string
            if attr_tuple[1]:

                # Check if current attribute being processed is moisture sensor, if so, go grab that moisture sensor
                # serial number
                if arg1 == opcodes.moisture_sensor:
                    if isinstance(self.ms, int):
                        arg2 = self.moisture_sensor_objects[self.ms].sn
                    else:
                        arg2 = ''

                current_config += ",{0}={1}".format(
                    arg1,   # {0}
                    arg2    # {1}
                )

        return current_config

    #################################
    def send_programming_to_cn(self):
        """
        Sends the current programming for the zone program to the controller. \n
        """
        # If 1000 controller
        if self.controller_type == "10":
            command = "SET,{0}={1},{2}={3},RT={4}".format(
                opcodes.zone_program,       # {0}
                str(self.zone.ad),      # {1}
                opcodes.program,            # {2}
                str(self.program.ad),   # {3}
                str(self.rt)                # {4}
            )

        # Else, 3200 controller
        else:
            command = self.build_obj_configuration_for_send()

        try:
            # Attempt to set program default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Zone Program {0} for Program {1}'s 'Values' to: '{2}'\n -> " \
                    "{3}".format(
                        str(self.zone.ad),      # {0}
                        str(self.program.ad),   # {1}
                        command,                    # {2}
                        e.message                   # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Zone Program {0} - Program {1}'s 'Values' to: {2}".format(
                  str(self.zone.ad),        # {0}
                  str(self.program.ad),     # {1}
                  # self.__str__()              # {2}
                  command                       # {2}
                  ))

    #################################
    def __str__(self):
        """
        Returns string representation of this Zone Program Object. \n
        :return:    String representation of this Zone Program Object
        :rtype:     str
        """
        return_str = "\n-----------------------------------------\n" \
                     "Zone Program {0} associated with Program {1}: \n" \
                     "Run Time:         {2} seconds\n" \
                     "Cycle Time:       {3} seconds\n" \
                     "Soak Time:        {4} seconds\n" \
                     "Zone Number:      {5} \n" \
                     "Runtime ratio:    {6}% \n" \
                     "Water Strategy:   {7} \n" \
                     "Moisture Sensor:  {8} \n" \
                     "Lower limit:      {9}% \n" \
                     "Upper limit:      {10}% \n" \
                     "Calibrate Cycle:  {11} \n" \
                     "Enable Et Runtime: {12} \n"\
                     "-----------------------------------------\n".format(
                         str(self.zone.ad),     # {0}
                         str(self.program.ad),  # {1}
                         str(self.rt),              # {2}
                         str(self.ct),              # {3}
                         str(self.so),              # {4}
                         str(self.pz),              # {5}
                         str(self.ra),              # {6}
                         self.ws,                   # {7}
                         self.ms,                   # {8}
                         str(self.ll),              # {9}
                         str(self.ul),              # {10}
                         self.cc,                   # {11}
                         self.ee                    # {12}
                     )
        return return_str

    #################################
    def set_run_time_on_cn(self, _run_time=None):
        """
        Sets the 'Run Time' for the Zone program on the controller. \n
        :param _run_time:   Run time for zone program in seconds \n
        :type _run_time:    int \n
        """
        # If trying to overwrite current run time
        if _run_time is not None:

            # Overwrite current attribute value
            self.rt = _run_time

        command = "SET,{0}={1},{2}={3},{4}={5}".format(
                  opcodes.zone_program,         # {0}
                  str(self.zone.ad),        # {1}
                  opcodes.program,              # {2}
                  str(self.program.ad),     # {3}
                  opcodes.run_time,      # {4}
                  str(self.rt)                  # {5}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Run Time' to: {2}\n " \
                    "{3}".format(
                        str(self.zone.ad),      # {0}
                        str(self.program.ad),   # {1}
                        str(self.rt),               # {2}
                        str(ae.message)             # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Run Time' for (Zone Program {0}, Program {1}) to: {2}".format(
                  str(self.zone.ad),      # {0}
                  str(self.program.ad),   # {1}
                  str(self.rt)                # {2}
                  ))

    #################################
    def set_cycle_time_on_cn(self, _cycle_time=None, _use_calculated_cycle_time=False):
        """
        Sets the 'Cycle Time' for the Zone Program on the controller. \n
        :param _cycle_time:                     Cycle time to set \n
        :type _cycle_time:                      int \n

        :param _use_calculated_cycle_time:      Cycle time to set \n
        :type _use_calculated_cycle_time:       bool  | float\n
        """

        if self.controller_type == "32":
            # this goes out to the equations module and returns a calculated Cycle time to the zp.py object
            if _use_calculated_cycle_time is True:
                _cycle_time = self.zone.get_calculated_cycle_time()
            # If trying to overwrite current value
            if _cycle_time is not None:

                # Overwrite current attribute value
                # take the value in seconds divide by 60 to convert it to minutes convert it to an integer and than
                # convert back to seconds this way you are dropping of the fractional part of the seconds
                self.ct = 60*(int(_cycle_time/60))

            command = "SET,{0}={1},{2}={3},{4}={5}".format(
                      opcodes.zone_program,         # {0}
                      str(self.zone.ad),        # {1}
                      opcodes.program,              # {2}
                      str(self.program.ad),     # {3}
                      opcodes.cycle_time,           # {4}
                      str(self.ct)                  # {5}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Cycle Time' to: {2}\n " \
                        "{3}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            str(self.ct),               # {2}
                            str(ae.message)             # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Cycle Time' for (Zone Program {0}, Program {1}) to: {2}".format(
                      str(self.zone.ad),      # {0}
                      str(self.program.ad),   # {1}
                      str(self.ct)                # {2}
                      ))
        else:
            raise ValueError("Attempting to set Cycle Time for a Zone Program for 1000 which is NOT SUPPORTED")

    #################################
    def set_soak_time_on_cn(self, _soak_time=None, _use_calculated_soak_time=False):
        """
        Sets the 'Soak Time' for the Zone Program on the controller. \n
        :param _soak_time:                      Soak time to set \n
        :type _soak_time:                       int  | float\n

        :param _use_calculated_soak_time:       Soak time to set \n
        :type _use_calculated_soak_time:        bool  | float\n
        """
        if self.controller_type == "32":
            # this goes out to the equations module and returns a calculated soak time to the zp.py object
            if _use_calculated_soak_time is True:
                _soak_time = self.zone.get_calculated_soak_time()

            # If trying to overwrite current value
            if _soak_time is not None:

                # Overwrite current attribute value
                # take the value in seconds divide by 60 to convert it to minutes convert it to an integer and than
                # add 1 to the value to always round up and then convert back to seconds
                if _soak_time == 0:
                    self.so = 0
                else:
                    self.so = 60*(math.ceil(_soak_time/60.0))

            command = "SET,{0}={1},{2}={3},{4}={5}".format(
                      opcodes.zone_program,         # {0}
                      str(self.zone.ad),        # {1}
                      opcodes.program,              # {2}
                      str(self.program.ad),     # {3}
                      opcodes.soak_cycle,           # {4}
                      str(self.so)                  # {5}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Soak Time' to: {2}\n " \
                        "{3}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            str(self.so),               # {2}
                            str(ae.message)             # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Soak Time' for (Zone Program {0}, Program {1}) to: {2}".format(
                      str(self.zone.ad),      # {0}
                      str(self.program.ad),   # {1}
                      str(self.so)                # {2}
                      ))
        else:
            raise ValueError("Attempting to set Soak Time for a Zone Program for 1000 which is NOT SUPPORTED")

    #################################
    def set_zone_mode_on_cn(self, mode, _linked_zone_ad=None):
        """
        Set's the Zone Program's mode on the controller. \n
        :param mode:    Timed: 'TIMED', Primary: 'PRIMARY' or Linked: 'LINKED' \n
        :type mode:     str \n
        :param _linked_zone_ad:     If mode is 'linked', then this represents the zone to link to \n
        :type _linked_zone_ad:      int \n
        """
        # Dictionary which maps the 'Mode' to the correct Zone address or zero respectively 
        mode_dict = {
            opcodes.timed_mode: 0,
            opcodes.primary_mode:self.zone.ad,
            opcodes.linked_mode: _linked_zone_ad
        }
        
        # 3200 only method
        if self.controller_type == "32":

            # Check for valid mode type
            if mode not in [opcodes.timed_mode, opcodes.linked_mode, opcodes.primary_mode]:
                e_msg = "Invalid zone 'Mode' entered for (Zone Program {0}, Program {1}. Received: {2}, " \
                        "Expected 'opcodes.timed_mode', 'opcodes.linked_mode' or 'opcodes.primary_mode'.".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            str(mode)                   # {2}
                        )
                raise ValueError(e_msg)
            
            # Check that a linked zone was provided given a 'LINKED' mode
            elif mode == opcodes.linked_mode and _linked_zone_ad is None:
                e_msg = "Missing arguments. Specified a 'LINKED' mode for (Zone Program {0}, Program {1}) but didn't " \
                        "provide a Zone address to link to as a second argument to the function call.".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad)    # {1}
                        )
                raise IOError(e_msg)
            
            else:
                self.pz = mode_dict[mode]

            command = "SET,{0}={1},{2}={3},{4}={5}".format(
                      opcodes.zone_program,         # {0}
                      str(self.zone.ad),        # {1}
                      opcodes.program,              # {2}
                      str(self.program.ad),     # {3}
                      opcodes.primary_zone_number,  # {4}
                      str(self.pz)                  # {5}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Primary Zone' to: {2}\n " \
                        "{3}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            str(self.pz),               # {2}
                            str(ae.message)             # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Primary Zone' for (Zone Program {0}, Program {1}) to: {2}".format(
                      str(self.zone.ad),      # {0}
                      str(self.program.ad),   # {1}
                      str(self.pz)                # {2}
                      ))
        else:
            raise ValueError("Attempting to set Primary Zone for a Zone Program for 1000 which is NOT SUPPORTED")

    #################################
    def set_runtime_ratio_on_cn(self, _runtime_ratio=None):
        """
        Sets the 'Runtime Ratio' for the current Zone Program \n
        :param _runtime_ratio:  Runtime ratio as an integer \n
        :type _runtime_ratio:   int \n
        """
        if self.controller_type == "32":

            # If trying to overwrite current value
            if _runtime_ratio is not None:

                # Overwrite current attribute value
                self.ra = _runtime_ratio
                #TODO need to add this to the unit test
                # this updates rt and ct with the new values that have been adjusted do to the tracking ration
                self.rt = self.calculate_3200_runtime(runtime=self.rt)
                self.ct = self.calculate_3200_cycletime(cycletime=self.ct)

            command = "SET,{0}={1},{2}={3},{4}={5}".format(
                      opcodes.zone_program,             # {0}
                      str(self.zone.ad),                # {1}
                      opcodes.program,                  # {2}
                      str(self.program.ad),             # {3}
                      opcodes.runtime_tracking_ratio,   # {4}
                      str(self.ra)                      # {5}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Runtime Ratio' to: {2}\n" \
                        "{3}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            str(self.ra),               # {2}
                            str(ae.message)             # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Runtime Ratio' for (Zone Program {0}, Program {1}) to: {2}".format(
                      str(self.zone.ad),      # {0}
                      str(self.program.ad),   # {1}
                      str(self.ra)                # {2}
                      ))

        else:
            raise ValueError("Attempting to set Runtime Ratio for a Zone Program for 1000 which is NOT SUPPORTED")

    #################################
    def set_water_strategy_on_cn(self, _water_strategy=None):
        """
        Sets the 'Water Strategy' for the Zone Program. \n
        :param _water_strategy:  Water Strategy to set ('TM': Timed, 'LL': Lower Limit, 'UL': Upper Limit \n
        :type _water_strategy:   str \n
        """
        if self.controller_type == "32":

            # If trying to overwrite current value
            if _water_strategy is not None:

                # Overwrite current attribute value
                self.ws = _water_strategy

                # Verify valid strategy type
                if self.ws not in [opcodes.timed, opcodes.lower_limit, opcodes.upper_limit]:
                    e_msg = "Invalid 'Watering Strategy' value for 'SET' for Zone Program : {0}, Program {1}. Expects "\
                            "'TM', 'LL', 'UL', received: {2}".format(
                                str(self.zone.ad),      # {0}
                                str(self.program.ad),   # {1}
                                self.ws                     # {2}
                            )
                    raise ValueError(e_msg)

            command = "SET,{0}={1},{2}={3},{4}={5}".format(
                      opcodes.zone_program,         # {0}
                      str(self.zone.ad),        # {1}
                      opcodes.program,              # {2}
                      str(self.program.ad),     # {3}
                      opcodes.water_strategy,       # {4}
                      str(self.ws)                  # {5}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Watering Strategy' to: " \
                        "{2}\n{3}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            str(self.ws),               # {2}
                            str(ae.message)             # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Watering Strategy' for (Zone Program {0}, Program {1}) to: {2}".format(
                      str(self.zone.ad),      # {0}
                      str(self.program.ad),   # {1}
                      str(self.ws)                # {2}
                      ))

        else:
            raise ValueError("Attempting to set 'Watering Strategy' for a Zone Program for 1000 which is NOT SUPPORTED")

    #################################
    def set_primary_zone_moisture_sensor_on_cn(self, _moisture_sensor_ad=None):
        """
        Sets the 'Moisture Sensor Pointer' for the Zone program to the specified moisture sensor serial number. \n
        :param _moisture_sensor_ad:  Address of moisture sensor to assign to Zone program \n
        :type _moisture_sensor_ad:   int \n
        """
        if self.controller_type == "32":

            # If trying to overwrite current value
            if _moisture_sensor_ad is not None:

                # Validate argument value is within accepted values
                if _moisture_sensor_ad not in self.moisture_sensor_objects:
                    e_msg = "Invalid Moisture Sensor address. " \
                            "Verify address exists in object json configuration and/or in " \
                            "current test. Received address: {0}, available addresses: {1}".format(
                                str(_moisture_sensor_ad),                   # {0}
                                str(self.moisture_sensor_objects.keys()),   # {1}
                            )
                    raise ValueError(e_msg)

                else:
                    # Overwrite current attribute value
                    self.ms = _moisture_sensor_ad

            # Get MS Serial from object
            ms_serial_from_obj = self.moisture_sensor_objects[self.ms].sn

            command = "SET,{0}={1},{2}={3},{4}={5}".format(
                opcodes.zone_program,         # {0}
                str(self.zone.ad),        # {1}
                opcodes.program,              # {2}
                str(self.program.ad),     # {3}
                opcodes.moisture_sensor,      # {4}
                ms_serial_from_obj            # {5}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Moisture Sensor Serial " \
                        "Number' to: {2} ({3})\n{4}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            ms_serial_from_obj,         # {2}
                            str(self.ms),               # {3}
                            str(ae.message)             # {4}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Moisture Sensor Serial Number' for (Zone Program {0}, Program {1}) to: {2} "
                      "({3})".format(
                          str(self.zone.ad),      # {0}
                          str(self.program.ad),   # {1}
                          ms_serial_from_obj,         # {2}
                          str(self.ms),               # {3}
                      ))
        else:
            raise ValueError("Attempting to set Moisture Sensor Serial Number for a Zone Program for 1000 which is "
                             "NOT SUPPORTED")

    #################################
    def set_lower_limit_threshold_on_cn(self, _lower_limit=None):
        """
        Sets the 'Lower Limit Threshold' for the current Zone program \n
        :param _lower_limit:    New lower limit to set to \n
        :type _lower_limit:     float \n
        """
        if self.controller_type == "32":

            # If trying to overwrite current value
            if _lower_limit is not None:

                # Overwrite current attribute value
                self.ll = _lower_limit

            command = "SET,{0}={1},{2}={3},{4}={5}".format(
                opcodes.zone_program,         # {0}
                str(self.zone.ad),        # {1}
                opcodes.program,              # {2}
                str(self.program.ad),     # {3}
                opcodes.lower_limit,          # {4}
                str(self.ll)                  # {5}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Lower Limit Threshold' " \
                        "to: {2}\n{3}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            str(self.ll),               # {2}
                            str(ae.message)             # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Lower Limit Threshold' for (Zone Program {0}, Program {1}) to: {2}"
                      .format(
                          str(self.zone.ad),      # {0}
                          str(self.program.ad),   # {1}
                          str(self.ll)                # {2}
                      ))
        else:
            raise ValueError("Attempting to set Lower Limit Threshold for a Zone Program for 1000 which is "
                             "NOT SUPPORTED")
        
    #################################
    def set_upper_limit_threshold_on_cn(self, _upper_limit=None):
        """
        Sets the 'Upper Limit Threshold' for the current Zone program \n
        :param _upper_limit:    New upper limit to set to \n
        :type _upper_limit:     float \n
        """
        if self.controller_type == "32":

            # If trying to overwrite current value
            if _upper_limit is not None:

                # Overwrite current attribute value
                self.ul = _upper_limit

            command = "SET,{0}={1},{2}={3},{4}={5}".format(
                opcodes.zone_program,         # {0}
                str(self.zone.ad),            # {1}
                opcodes.program,              # {2}
                str(self.program.ad),          # {3}
                opcodes.upper_limit,          # {4}
                str(self.ul)                  # {5}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Upper Limit Threshold' " \
                        "to: {2}\n{3}".format(
                            str(self.zone.ad),          # {0}
                            str(self.program.ad),       # {1}
                            str(self.ul),               # {2}
                            str(ae.message)             # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Upper Limit Threshold' for (Zone Program {0}, Program {1}) to: {2}"
                      .format(
                          str(self.zone.ad),        # {0}
                          str(self.program.ad),     # {1}
                          str(self.ul)              # {2}
                      ))
        else:
            raise ValueError("Attempting to set Upper Limit Threshold for a Zone Program for 1000 which is "
                             "NOT SUPPORTED")
                
    #################################
    def set_calibration_cycle_on_cn(self, _calibration_cycle=None):
        """
        Sets the 'Calibration Cycle' for the current Zone program \n
        :param _calibration_cycle:    New upper limit to set to \n
        :type _calibration_cycle:     str \n
        """
        if self.controller_type == "32":

            # If trying to overwrite current value
            if _calibration_cycle is not None:

                # Overwrite current attribute value
                self.cc = _calibration_cycle

                # Verify valid calibration cycle passed in
                if self.cc not in [opcodes.calibrate_never, opcodes.calibrate_one_time, opcodes.calibrate_monthly]:
                    e_msg = "Invalid 'Calibration Cycle' value for 'SET' for Zone Program : " \
                            "{0}, Program {1}. Expects 'NV', 'SG' or 'MO', received: {2}".format(
                                str(self.zone.ad),      # {0}
                                str(self.program.ad),   # {1}
                                self.cc                 # {2}
                            )
                    raise ValueError(e_msg)

            command = "SET,{0}={1},{2}={3},{4}={5}".format(
                opcodes.zone_program,       # {0}
                str(self.zone.ad),          # {1}
                opcodes.program,            # {2}
                str(self.program.ad),       # {3}
                opcodes.calibrate_cycle,    # {4}
                self.cc                     # {5}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Calibration Cycle' " \
                        "to: {2}\n{3}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            self.cc,                # {2}
                            str(ae.message)         # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Calibration Cycle' for (Zone Program {0}, Program {1}) to: {2}"
                      .format(
                          str(self.zone.ad),      # {0}
                          str(self.program.ad),   # {1}
                          self.cc                 # {2}
                      ))
        else:
            raise ValueError("Attempting to set Calibration Cycle for a Zone Program for 1000 which is "
                             "NOT SUPPORTED")

    #################################
    def set_enable_et_state_on_cn(self, _state=None):
        """
        Sets the Enabled State for ET on a program . \n
        :return:
        """
        if self.controller_type == "32":

            # If trying to overwrite current value
            if _state is not None:

                # If zone isn't enabled, make sure to update instance variable to match what the controller will show.
                if _state not in [opcodes.true, opcodes.false]:
                    e_msg = "Invalid enabled state entered for Zone {0}. Received: {1}, Expected: 'TR' or 'FA'".format(
                            str(self.zone.ad),   # {0}
                            _state)              # {1}
                    raise ValueError(e_msg)
                else:
                    # Overwrite current attribute value
                    self.ee = _state

                # Verify valid enable et passed in

            if self.ee not in [opcodes.true, opcodes.false]:
                e_msg = "Invalid 'Enable ET Runtime state' value for 'SET' for Zone Program : " \
                        "{0}, Program {1}. Expects 'TR',or 'FA', received: {2}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            self.ee                 # {2}
                            )
                raise ValueError(e_msg)

            command = "SET,{0}={1},{2}={3},{4}={5}".format(
                opcodes.zone_program,                   # {0}
                str(self.zone.ad),                      # {1}
                opcodes.program,                        # {2}
                str(self.program.ad),                   # {3}
                WaterSenseCodes.Enable_ET_Runtime,      # {4}
                self.ee                                 # {5}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)

                # this goes to the zone object and sets an attribute so that the verifier can word
                self.zone.enable_et = self.ee

            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Enable ET Runtime' " \
                        "to: {2}\n{3}".format(
                            str(self.zone.ad),          # {0}
                            str(self.program.ad),       # {1}
                            self.ee,                    # {2}
                            str(ae.message)             # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Enable ET runtime' for (Zone Program {0}, Program {1}) to: {2}"
                      .format(
                          str(self.zone.ad),      # {0}
                          str(self.program.ad),   # {1}
                          self.ee                 # {2}
                      ))
        else:
            raise ValueError("Attempting to set Enable ET Runtime for a Zone Program for 1000 which is "
                             "NOT SUPPORTED")

    #################################
    def set_message_on_cn(self, _status_code):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        """
        try:
            build_message_string = messages.message_to_set(self, _status_code=_status_code,
                                                           _ct_type=opcodes.zone_program)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.set_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=build_message_string)

    #################################
    def get_data(self):
        """
        Gets all data for the zone program from the controller. \n
        :return:
        """

        # Example command: "GET,PG=1"
        command = "GET,{0}={1},{2}={3}".format(
            opcodes.zone_program,       # {0}
            str(self.zone.ad),          # {1}
            opcodes.program,            # {2}
            str(self.program.ad),       # {3}
        )

        # Attempt to get data from controller
        try:
            self.data = self.ser.get_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Unable to get data for (Zone Program: {0}, Program {1}) using command: '{2}'. Exception raised: " \
                    "{3}".format(
                        str(self.zone.ad),      # {0}
                        str(self.program.ad),   # {1}
                        command,                # {2}
                        str(ae.message)         # {3}
                    )
            raise ValueError(e_msg)

    #################################
    def get_message_on_cn(self, _status_code):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        """
        try:
            self.build_message_string = messages.message_to_get(self, _status_code=_status_code,
                                                                _ct_type=opcodes.zone_program)
            cn_mg = self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            msg = "Exception caught in messages.get_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=self.build_message_string[opcodes.message])
            return cn_mg

    ###################################
    def clear_message_on_cn(self, _status_code):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        """
        try:
            build_message_string = messages.message_to_clear(self, _status_code=_status_code,
                                                             _ct_type=opcodes.zone_program)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.clear_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=build_message_string)

        # Verifies that the message was cleared by catching the 'NM' command that we would expect back
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=opcodes.zone_program)
            self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            if e.message == "NM No Message Found":
                print "Message cleared and verified that the message is gone."
            else:
                print "Message cleared, but was unable to verify if the message was gone."

    #################################
    def verify_runtime_on_cn(self):
        """
        First Check to see if ET based watering is being used to calculate rt by checking to see if ee is 'TR'.
        Than Verify the 'Runtime' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Runtime' value from the controller reply.
        -   Lastly, compares the instance 'Runtime' value with the controller's programming. 
        # """
        if self.controller_type == "32":
            if self.ee != opcodes.false:
                # goes to equations and returns to object a calculated total runtime to compare against the
                # controller rt

                self.rt = equations.return_todays_calculated_runtime(_du=self.zone.du,
                                                                     _pr=self.zone.pr,
                                                                     _mb=self.zone.mb
                                                                     )
        else:
            # goes to equations and returns to object a calculated total runtime to compare against the controller rt
            # this is for the 1000
            if self.program.ee == opcodes.true:
                self.rt = equations.return_todays_calculated_runtime(_du=self.zone.du,
                                                                     _pr=self.zone.pr,
                                                                     _mb=self.zone.mb
                                                                     )

        print("Print numbers used to Calculate RT:  du: '{0}' pr: '{1}' mb: '{2}' Calculated rt: '{3}' on controller"
              .format(
                  str(self.zone.du),       # {0}
                  str(self.zone.pr),       # {1}
                  str(self.zone.mb),       # {2}
                  str(self.rt),))          # {3}
        # get runtime data from the controller
        runtime_from_controller = int(self.data.get_value_string_by_key(opcodes.run_time))
        # take the value in rt and add .00001 to it to make it a float with at least 6 significant digits
        rounded_rt = int(round(self.rt+.00001))
        # if the 1000 controller calculates a runtime less than 4 minutes it will return a value of zero
        if self.controller_type == "10":
            # if rounded_rt < 240:
            #     rounded_rt = 0
            #     print "The 1000 calculated a runtime less than 4 minutes. Calculated runtime was: "\
            #           + str(rounded_rt) + " seconds, so it is set to zero."
            # Compare status versus what is on the controller
            # because of the tolerances of the number the runtime can be a difference of 29 seconds
            if abs(rounded_rt - runtime_from_controller) > 29:
                e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Runtime' value. Received: {2}, " \
                        "Expected: {3}".format(
                            str(self.zone.ad),              # {0}
                            str(self.program.ad),           # {1}
                            str(runtime_from_controller),   # {2}
                            str(rounded_rt)                 # {3}
                        )
                raise ValueError(e_msg)
        if self.controller_type == "32":
            if abs(rounded_rt - runtime_from_controller) > 1:
                e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Runtime' value. Received: {2}, " \
                        "Expected: {3}".format(
                            str(self.zone.ad),              # {0}
                            str(self.program.ad),           # {1}
                            str(runtime_from_controller),   # {2}
                            str(rounded_rt)                 # {3}
                        )
                raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Calculated Runtime' value: '{2}' against controller"
                  "Runtime value: '{3}' ".format(
                    str(self.zone.ad),              # {0}
                    str(self.program.ad),           # {1}
                    str(rounded_rt),                # {2}
                    str(runtime_from_controller)    # {3}
                    ))

    #################################
    def verify_cycle_time_on_cn(self):
        """
        Verifies the 'Cycle Time' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Cycle Time' value from the controller reply.
        -   Lastly, compares the instance 'Cycle Time' value with the controller's programming. 
        """

        cycle_time_from_controller = int(self.data.get_value_string_by_key(opcodes.cycle_time))
        # if using weather base water and cycle time for the controller is set to less than 240 seconds than the controller
        # will return 240 seconds
        ct_compare = self.ct

        if self.ee != opcodes.false:
            if 0 < ct_compare <= 239:
                # check to see if controller is 240
                ct_compare = 240

        # Compare status versus what is on the controller
        if ct_compare != cycle_time_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Cycle Time' value. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self.zone.ad),                  # {0}
                        str(self.program.ad),               # {1}
                        str(cycle_time_from_controller),    # {2}
                        str(self.ct)                        # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Cycle Time' value: '{2}' on controller".format(
                str(self.zone.ad),          # {0}
                str(self.program.ad),       # {1}
                str(self.ct)                # {2}
            ))            
            
    #################################
    def verify_soak_time_on_cn(self):
        """
        Verifies the 'Soak Time' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Soak Time' value from the controller reply.
        -   Lastly, compares the instance 'Soak Time' value with the controller's programming. 
        """

        soak_time_from_controller = int(self.data.get_value_string_by_key(opcodes.soak_cycle))

        # Compare status versus what is on the controller
        if self.so != soak_time_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Soak Time' value. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self.zone.ad),                  # {0}
                        str(self.program.ad),               # {1}
                        str(soak_time_from_controller),     # {2}
                        str(self.so)                        # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Soak Time' value: '{2}' on controller".format(
                str(self.zone.ad),          # {0}
                str(self.program.ad),       # {1}
                str(self.so)                # {2}
            ))
            
    #################################
    def verify_primary_zone_number_on_cn(self):
        """
        Verifies the 'Primary Zone Number' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Primary Zone Number' value from the controller reply.
        -   Lastly, compares the instance 'Primary Zone Number' value with the controller's programming. 
        """

        zone_pointer_from_controller = int(self.data.get_value_string_by_key(opcodes.zone_program))
    
        # Compare status versus what is on the controller
        if self.pz != zone_pointer_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Primary Zone Number' value. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self.zone.ad),                      # {0}
                        str(self.program.ad),                   # {1}
                        str(zone_pointer_from_controller),      # {2}
                        str(self.pz)                            # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Primary Zone Number' value: '{2}' on controller".format(
                str(self.zone.ad),          # {0}
                str(self.program.ad),       # {1}
                str(self.pz)                # {2}
            ))
            
    #################################
    def verify_runtime_tracking_ratio_on_cn(self):
        """
        Verifies the 'Runtime Tracking Ratio' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Runtime Tracking Ratio' value from the controller reply.
        -   Lastly, compares the instance 'Runtime Tracking Ratio' value with the controller's programming. 
        """
        runtime_ratio_from_controller = int(self.data.get_value_string_by_key(opcodes.runtime_tracking_ratio))

        # Compare status versus what is on the controller
        if self.ra != runtime_ratio_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Runtime Tracking Ratio' value. Received: {2}" \
                    ", Expected: {3}".format(
                        str(self.zone.ad),                      # {0}
                        str(self.program.ad),                   # {1}
                        str(runtime_ratio_from_controller),     # {2}
                        str(self.ra)                            # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Runtime Tracking Ratio' value: '{2}' on controller"
                  .format(
                      str(self.zone.ad),          # {0}
                      str(self.program.ad),       # {1}
                      str(self.ra)                # {2}
                  ))
            
    #################################
    def verify_water_strategy_on_cn(self):
        """
        Verifies the 'Water Strategy' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Water Strategy' value from the controller reply.
        -   Lastly, compares the instance 'Water Strategy' value with the controller's programming. 
        """

        water_strategy_from_controller = str(self.data.get_value_string_by_key(opcodes.water_strategy))
    
        # Compare status versus what is on the controller
        if self.ws != water_strategy_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Water Strategy' value. Received: {2}" \
                    ", Expected: {3}".format(
                        str(self.zone.ad),                      # {0}
                        str(self.program.ad),                   # {1}
                        str(water_strategy_from_controller),    # {2}
                        str(self.ws)                            # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Water Strategy' value: '{2}' on controller".format(
                  str(self.zone.ad),            # {0}
                  str(self.program.ad),         # {1}
                  str(self.ws)                  # {2}
                  ))
            
    #################################
    def verify_primary_zone_moisture_sensor_on_cn(self):
        """
        Verifies the 'Primary Zone Moisture Sensor' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Primary Zone Moisture Sensor' value from the controller reply.
        -   Lastly, compares the instance 'Primary Zone Moisture Sensor' value with the controller's programming. 
        """

        primary_zone_moisture_from_controller = str(self.data.get_value_string_by_key(opcodes.moisture_sensor))

        # Get the current assigned serial number
        current_assigned_ms_serial = self.moisture_sensor_objects[self.ms].sn

        # Compare status versus what is on the controller
        if current_assigned_ms_serial != primary_zone_moisture_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Primary Zone Moisture Sensor' value. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.zone.ad),                              # {0}
                        str(self.program.ad),                           # {1}
                        str(primary_zone_moisture_from_controller),     # {2}
                        current_assigned_ms_serial                      # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Primary Zone Moisture Sensor' value: '{2}' on controller"
                  .format(
                      str(self.zone.ad),            # {0}
                      str(self.program.ad),         # {1}
                      current_assigned_ms_serial    # {3}
                  ))
            
    #################################
    def verify_lower_limit_threshold_on_cn(self):
        """
        Verifies the 'Lower Limit Threshold' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Lower Limit Threshold' value from the controller reply.
        -   Lastly, compares the instance 'Lower Limit Threshold' value with the controller's programming. 
        """

        lower_limit_threshold_from_controller = float(self.data.get_value_string_by_key(opcodes.lower_limit))
    
        # Compare status versus what is on the controller
        if self.ll != lower_limit_threshold_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Lower Limit Threshold' value. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.zone.ad),                              # {0}
                        str(self.program.ad),                           # {1}
                        str(lower_limit_threshold_from_controller),     # {2}
                        str(self.ll)                                    # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Lower Limit Threshold' value: '{2}' on controller"
                  .format(
                      str(self.zone.ad),            # {0}
                      str(self.program.ad),         # {1}
                      str(self.ll)                  # {2}
                  ))
            
    #################################
    def verify_upper_limit_threshold_on_cn(self):
        """
        Verifies the 'Upper Limit Threshold' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Upper Limit Threshold' value from the controller reply.
        -   Lastly, compares the instance 'Upper Limit Threshold' value with the controller's programming. 
        """

        upper_limit_threshold_from_controller = float(self.data.get_value_string_by_key(opcodes.upper_limit))
    
        # Compare status versus what is on the controller
        if self.ul != upper_limit_threshold_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Upper Limit Threshold' value. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.zone.ad),                              # {0}
                        str(self.program.ad),                           # {1}
                        str(upper_limit_threshold_from_controller),     # {2}
                        str(self.ul)                                    # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Upper Limit Threshold' value: '{2}' on controller"
                  .format(
                      str(self.zone.ad),          # {0}
                      str(self.program.ad),       # {1}
                      str(self.ul)                # {2}
                  ))
            
    #################################
    def verify_calibrate_cycle_on_cn(self):
        """
        Verifies the 'Calibrate Cycle' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Calibrate Cycle' value from the controller reply.
        -   Lastly, compares the instance 'Calibrate Cycle' value with the controller's programming. 
        """

        calibrate_cycle_from_controller = self.data.get_value_string_by_key(opcodes.calibrate_cycle)
    
        # Compare status versus what is on the controller
        if self.cc != calibrate_cycle_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Calibrate Cycle' value. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.zone.ad),                              # {0}
                        str(self.program.ad),                           # {1}
                        str(calibrate_cycle_from_controller),           # {2}
                        str(self.cc)                                    # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Calibrate Cycle' value: '{2}' on controller"
                  .format(
                      str(self.zone.ad),          # {0}
                      str(self.program.ad),       # {1}
                      str(self.cc)                # {2}
                  ))

    #################################
    def verify_enable_et_state_on_cn(self):
        """
        Verifies the 'Enable ET Runtime State' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'ET Runtime State' value from the controller reply.
        -   Lastly, compares the instance 'ET Runtime State' value with the controller's programming.
        """

        et_state_from_controller = self.data.get_value_string_by_key(WaterSenseCodes.Enable_ET_Runtime)

        # Compare status versus what is on the controller
        if self.ee != et_state_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Enable Et Runtime State' value. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.zone.ad),                          # {0}
                        str(self.program.ad),                       # {1}
                        str(et_state_from_controller),              # {2}
                        str(self.ee)                                # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Enable Et Runtime State' value: '{2}' on controller"
                  .format(
                      str(self.zone.ad),          # {0}
                      str(self.program.ad),       # {1}
                      str(self.ee)                # {2}
                  ))

    #################################
    def verify_message_on_cn(self, _status_code):
        """
        the expected values are retrieved from the controller and then compared against the values that are returned
        from the message module
        :param _status_code
        :type _status_code :str
        :return:
        """
        mg_on_cn = self.get_message_on_cn(_status_code=_status_code)
        expected_message_text_from_cn = mg_on_cn.get_value_string_by_key(opcodes.message_text)
        expected_message_id_from_cn = mg_on_cn.get_value_string_by_key(opcodes.message_id)
        expected_date_time_from_cn = mg_on_cn.get_value_string_by_key(opcodes.date_time)
        # Compare status versus what is on the controller
        if self.build_message_string[opcodes.message_id] != expected_message_id_from_cn:
            e_msg = "Created ID message did not match the ID received from the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_id],  # {0} The ID that was built
                        expected_message_id_from_cn                     # {1} The ID returned from controller
                    )
            raise ValueError(e_msg)

        mess_date_time = datetime.strptime(self.build_message_string[opcodes.date_time], '%m/%d/%y %H:%M:%S')
        controller_date_time = datetime.strptime(expected_date_time_from_cn, '%m/%d/%y %H:%M:%S')
        if mess_date_time != controller_date_time:
            if abs(controller_date_time - mess_date_time) >= timedelta(days=0, hours=0, minutes=60, seconds=0):
                e_msg = "The date and time of the message didn't match the controller:\n" \
                        "\tCreated: \t\t'{0}'\n" \
                        "\tReceived:\t\t'{1}'\n".format(
                    self.build_message_string[opcodes.date_time],  # {0} The date message that was built
                    expected_date_time_from_cn  # {1} The date message returned from controller
                )
                raise ValueError(e_msg)
            # only print this if they were not exact
            e_msg = "############################  Date and time were not exact but were within +- 30 minutes \n" \
                    "############################  Controller Date and Time = {0} \n" \
                    "############################  Message Date time = {1}".format(
                        expected_date_time_from_cn,                     # {0} Date time received from controller
                        self.build_message_string[opcodes.date_time]    # {1} Date time the message was verified
                        )
            print(e_msg)

        if self.build_message_string[opcodes.message_text] != expected_message_text_from_cn:
            e_msg = "Created TX message did not match the TX received from the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_text],   # {0} The TX message that was built
                        expected_message_text_from_cn                      # {1} The TX message returned from controller
                    )
            raise ValueError(e_msg)
        else:
            print "Successfully verified:\n" \
                  "\tID: '{0}'\n" \
                  "\tDT: '{1}'\n" \
                  "\tTX: '{2}'\n".format(
                      self.build_message_string[opcodes.message_id],
                      self.build_message_string[opcodes.date_time],
                      self.build_message_string[opcodes.message_text]
                  )

    #################################
    def verify_who_i_am(self):
        """
        Verifier wrapper which verifies all attributes for this 'Zone Program'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify the one attribute that both 1000 and 3200 zone programs share
        self.verify_runtime_on_cn()

        # Verify 3200 zone program specific attributes
        if self.controller_type == "32":
            self.verify_cycle_time_on_cn()
            self.verify_soak_time_on_cn()
            self.verify_primary_zone_number_on_cn()
            self.verify_enable_et_state_on_cn()

            # Check if we are a linked zone, if so, verify runtime tracking ratio
            if self.pz != 0 and self.pz != self.zone.ad:
                self.verify_runtime_tracking_ratio_on_cn()

            # Primary zone and timed watering strategy
            elif self.pz ==self.zone.ad:
                self.verify_water_strategy_on_cn()

                # Not a timed water strategy? Need to verify more things
                if self.ws != opcodes.timed:

                    # Do we have an assigned ms? If so verify it
                    if self.ms:
                        self.verify_primary_zone_moisture_sensor_on_cn()

                    self.verify_lower_limit_threshold_on_cn()
                    self.verify_upper_limit_threshold_on_cn()
                    self.verify_calibrate_cycle_on_cn()

