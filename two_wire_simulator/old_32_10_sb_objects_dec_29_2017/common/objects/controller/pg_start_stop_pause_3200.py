from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.imports.types import StartStopPauseEvent, StartStopPauseCondition
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.pg_start_stop_pause_cond import BaseStartStopPause

__author__ = 'ben'


#
# START_COND Program Condition
#
class StartConditionFor3200(BaseStartStopPause):
    """
    Start Condition for 3200:

    Usage (must be done in this sequence):
        - creating stop condition instance:
            new_instance_name = StartConditionFor3200(condition_number=1) \n

        - setting event type:
            new_instance_name.set_as_ts_event(ts_obj=some_ts_object_instance, tm=opcodes.upper_limit, tt=72.0) \n
    """

    def __init__(self, program_ad):
        """
        Start Condition Init
        :param program_ad: Current condition number for controller \n
        :type program_ad: int
        """
        # init base class
        BaseStartStopPause.__init__(self,
                                    program_ad=program_ad,
                                    event_type=StartStopPauseCondition.start,
                                    cn_type="32")

        # init attributes
        self._em = None     # ET mode: 'TR' | 'FA' (i.e., opcodes.true | opcodes.false)
        self._et = None     # ETc deficit trigger threshold (0.05 to 5.00)

    def set_as_et_event(self, mode, threshold=0.05):
        """
        Sets the current condition to use deficit trigger thresholds events.
        :param mode: Deficit mode
        :type mode: str
        :param threshold: Threshold to trigger event
        :type threshold: float
        """
        self._ty = StartStopPauseEvent.weather_based  # event type

        # valid mode entered
        if mode in [opcodes.true, opcodes.false]:
            self._em = mode
        else:
            e_msg = "Invalid Deficit trigger threshold mode for 3200 start condition #{cond_num}: {mode}. Valid " \
                    "options are: 'TR' | 'FA'".format(
                        cond_num=self._program_ad,
                        mode=mode
                    )
            raise ValueError(e_msg)

        self._et = threshold


#
# STOP_COND Program Condition
#
class StopConditionFor3200(BaseStartStopPause):
    """
    Stop Condition for 3200:

    Usage (must be done in this sequence):
        - creating stop condition instance:
            new_instance_name = StopConditionFor3200(condition_number=1) \n

        - setting event type:
            new_instance_name.set_as_ts_event(ts_obj=some_ts_object_instance, tm=opcodes.upper_limit, tt=72.0) \n
    """

    def __init__(self, program_ad):
        """
        Stop Condition Init
        :param program_ad: Current condition number for controller \n
        :type program_ad: int
        """
        # init base class
        BaseStartStopPause.__init__(self,
                                    program_ad=program_ad,
                                    event_type=StartStopPauseCondition.stop,
                                    cn_type="32")


#
# PAUSE_COND Program Condition
#
class PauseConditionFor3200(BaseStartStopPause):
    """
    Pause Condition for 3200:

    Usage (must be done in this sequence):
        - creating start condition instance:
            new_instance_name = PauseConditionFor3200(condition_number=1) \n

        - setting event type:
            new_instance_name.set_as_ts_event(ts_obj=some_ts_object_instance, tm=opcodes.upper_limit, tt=72.0) \n
    """

    def __init__(self, program_ad):
        """
        Pause Condition Init
        :param program_ad: Current condition number for controller \n
        :type program_ad: int
        """
        # init base class
        BaseStartStopPause.__init__(self,
                                    program_ad=program_ad,
                                    event_type=StartStopPauseCondition.pause,
                                    cn_type="32")

        # init attributes
        self._mp = None     # Moisture Pause Time (minutes)
        self._tp = None     # Temperature Pause Time (minutes)
