from datetime import datetime, timedelta
import time
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import zones
from old_32_10_sb_objects_dec_29_2017.common.epa_package import equations
from old_32_10_sb_objects_dec_29_2017.common.epa_package.wbw_imports import WaterSenseCodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.programs import Programs
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zn import Zone
# from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.messages import Messages
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import basemanager_connection
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.bm import BaseManagerConnection

import file_transfer_handler


__author__ = 'bens'


########################################################################################################################
# CONTROLLER CLASS
########################################################################################################################
class Controller(Devices):
    """
    Controller Object \n
    """

    # zone objects are assigned to this variable from the object_bucket.py when test scripts are ran.

    # zone_objects = {1: zn.Zone}
    zone_objects = None

    #############################
    def __init__(self, _type, _mac, _description, _cn_serial_number=None, _firmware_version=None):
        """
        Initialize a controller instance with the specified parameters \n
        :param _type:           Type of controller, '10' or '32' accepted. String. \n
        :param _mac:            Mac address for controller. \n
        :param _description:    Description to set for controller. \n
        :return:
        """
        self.zone_objects = zones
        """:type: dict[int, common.objects.controller.zn.Zone]"""
        # Initialize Controller Object Attributes
        self.mac = _mac
        # Base serial number off of type of controller.
        if _type == "32":
            if _cn_serial_number:
                self.serial_num = _cn_serial_number
            else:
                self.serial_num = "3K10001"
        elif _type == "10":
            if _cn_serial_number:
                self.serial_num = _cn_serial_number
            else:
                self.serial_num = "1K10001"
        # base unit case
        else:
            self.serial_num = "00B0001"

        # If we return true, the description is valid, otherwise fail and let the user know.
        if self.verify_description_length(_desc=_description, _type=_type):

            # Controller Starting latitude and longitude
            # latitude = 43.609768
            # longitude = -116.310569

            # Initialize parent class for inheritance
            Devices.__init__(
                self,
                _sn=self.serial_num,            # Serial number for controller
                _ds=_description,               # Description for controller
                _ty=_type,                      # Type of controller
                _la=self.controller_lat,        # Latitude for controller
                _lg=self.controller_long        # Longitude for controller
            )

            # Set controller type, lat, and lng to parent class for all device objects to be able to reference
            # Devices.controller_type = self.ty
            # Devices.controller_lat = self.la
            # Devices.controller_long = self.lg

        self.dv_type = opcodes.controller

        # Controller Attributes
        self.mc = 1                     # Max Concurrent Zones
        self.rp = 0                     # Rain Pause Days (Rain Delay)
        self.vr = _firmware_version     # Code Version (Firmware) - GET ONLY
        self.jr = 'CL'                  # Rain Jumper (OP | CL)
        self.ed = '00000000'            # Controller ETo Date
        self.ei = ''                    # Initial Eto value

        if self.is3200():
            self.mu = 0.0                   # Memory_Usage  - GET ONLY
            self.jf = 'CL'                  # Flow Jumper (OP | CL)
            self.jp = 'CL'                  # Pause Jumper (OP | CL)

        # Assigned device serial numbers per bicoder type.
        self.d1_bicoders = list()        # Single valve
        self.d2_bicoders = list()        # Dual valve
        self.d4_bicoders = list()        # Quad valve
        self.dd_bicoders = list()        # Twelve valve
        self.ms_bicoders = list()        # Moisture bicoder serial numbers
        self.fm_bicoders = list()        # Flow bicoder serial numbers
        self.ts_bicoders = list()        # Temperature bicoder serial numbers
        self.sw_bicoders = list()        # Switch bicoder serial numbers

        if self.ty in ["32", "10"]:
            self.set_default_values()

        # Date and time on the controller
        self.controller_object_current_date_time = date_mngr.controller_datetime

    #################################
    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    #################################
    def __str__(self):
        """
        Returns string representation of this Controller Object. \n
        :return:    String representation of this Controller Object
        :rtype:     str
        """
        return_str = "\n-----------------------------------------\n" \
                     "Controller Object:\n" \
                     "Type:                     {0}\n" \
                     "Serial Number:            {1}\n" \
                     "Mac Address:              {2}\n" \
                     "Description:              {3}\n" \
                     "Latitude:                 {4}\n" \
                     "Longitude:                {5}\n" \
                     "Max Concurrent Zones:     {6}\n" \
                     "Rain Pause Days:          {7} days\n" \
                     "Code Version:             {8}\n" \
                     "Status:                   {9}\n" \
                     "Rain Jumper State:        {10}\n" \
                     "Flow Jumper State:        {11}\n" \
                     "Pause Jumper State:       {12}\n" \
                     "Controller ETo Value:     {13}\n" \
                     "Controller rain value:    {14}\n" \
                     "Controller ETo Date:      {15}\n" \
                     "Initial Controller ETo:   {16}\n" \
                     "Memory Usage:             {17}\n" \
                     "-----------------------------------------\n".format(
                         self.ty,       # {0}
                         self.sn,       # {1}
                         self.mac,      # {2}
                         self.ds,       # {3}
                         str(self.la),  # {4}
                         str(self.lg),  # {5}
                         str(self.mc),  # {6}
                         str(self.rp),  # {7}
                         str(self.vr),  # {8}
                         self.ss,       # {9}
                         str(self.jr),  # {10}
                         str(self.jf),  # {11}
                         str(self.jp),  # {12}
                         str(Devices.et),  # {13}
                         str(Devices.ra),  # {14}
                         str(self.ed),  # (15}
                         str(self.ei),   # {16}
                         str(self.mu)   # {17}
                     )
        return return_str

    #################################
    # def set_message(self, _status_code):
    #     """
    #     Sets the message for the zone
    #     :param obj:
    #     :param msg_type:
    #     :return:
    #     """
    #     # TODO find out how to fill out the rest of the values
    #     # temp = Messages.ControllerMessages(_ss=_status_code, _serial_number=self.sn, _command=opcodes.set_action,
    #                                        _days=self.rp, _current_value=, _limit_value=)
    #     command = temp.msg
    #
    #     try:
    #         self.ser.send_and_wait_for_reply(tosend=command)
    #     except Exception as e:
    #         msg = "Exception caught in messages.set_message: " + str(e.message)
    #         raise msg
    #     else:
    #         print "Successfully sent message: {msg}".format(msg=command)

    #################################
    def is3200(self):
        """
        Returns whether this controller instance is a 3200 or not
        :return:    True, if this instance is a 3200 controller
                    False, otherwise
        """
        return self.ty == "32"

    #################################
    def is1000(self):
        """
        Returns whether this controller instance is a 1000 or not
        :return:    True, if this instance is a 1000 controller
                    False, otherwise
        """
        return self.ty == "10"

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        if self.is1000():
            command = "SET,CN,SN={0},DS={1},LA={2},LG={3},MC={4},RP={5},JR={6}".format(
                self.sn,  # {0}
                str(self.ds),  # {1}
                str(self.la),  # {2}
                str(self.lg),  # {3}
                str(self.mc),  # {4}
                str(self.rp),  # {5}
                str(self.jr)   # {6}
            )
        else:
            command = "SET,CN,SN={0},DS={1},LA={2},LG={3},MC={4},RP={5},JR={6},JF={7},JP={8}".format(
                self.sn,        # {0}
                str(self.ds),   # {1}
                str(self.la),   # {2}
                str(self.lg),   # {3}
                str(self.mc),   # {4}
                str(self.rp),   # {5}
                str(self.jr),   # {6}
                str(self.jf),   # {7}
                str(self.jp)    # {8}
            )
        try:
            # Attempt to set Controller default values
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Controller {0}'s 'Default values' to: '{1}'. " \
                    "Exception received: {2}".format(self.mac, command, e.message)
            raise Exception(e_msg)
        else:
            print("Successfully set Controller {0}'s 'Default values' to: {1}".format(self.mac, command))

    #################################
    def set_connection_to_substation(self, _address, _ip_address=None, _enabled=opcodes.true):
        """
        Establishes a connection from a 3200 to a substation using local IP's \n

        :param _address:        The address the controller assigns to the substation (1-8) \n
        :type _address:         int
        :param _ip_address:     The ip address of the Substation we want to connect to (ex: 10.11.12.144) \n
        :type _ip_address:      str
        :param _enabled:        Enabled state for the substation (TR | FA) \n
        :type _enabled:         str
        """
        # Verify a 3200 is calling this method
        if self.is3200():

            # Verify the ranges for addressing
            if _address not in range(1, 9):
                e_msg = "Can not establish a connection to a substation from a controller with an address that is not" \
                        "in the range 1 through 8. Value passed in: {0}.".format(_address)
                raise ValueError(e_msg)

            # Build the command string
            command = "SET,{0}={1},{2}={3}".format(opcodes.substation, _address, opcodes.enabled, _enabled)

            if _ip_address:
                command += ",{0}={1}".format(
                    opcodes.address,
                    _ip_address,
                )

            try:
                # Send the command through the serial port
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception as e:
                e_msg = "Exception occurred trying to set Controller's 'Substation Connection' for " \
                        "Substation: '{0}', IP Address: '{1}', Enabled: '{2}'.".format(_address, _ip_address, _enabled)
                raise Exception(e_msg)
            else:
                print "Successfully set controller 'Substation Connection' for Substation: '{0}', IP Address: '{1}', " \
                      "Enabled: '{2}'.".format(_address, _ip_address, _enabled)

        else:
            raise ValueError("Method 'set_connection_to_substation' can only be called by a 3200 controller.")

    #################################
    def set_max_concurrent_zones_on_cn(self, _max_zones=None):
        """
        Sets the max number of concurrent zones for the controller. The current instance value can be overwritten by
        passing in a value for _max_zones. \n
        :param _max_zones:  Number of concurrent zones to set. \n
        :type _max_zones:   Int \n
        :return:
        """
        # Check if overwrite is intended
        if _max_zones is not None:
            # Verify integer is passed in for max number of concurrent zones to be set
            if not isinstance(_max_zones, int):
                e_msg = "Failed trying to set max concurrent zones for controller. Invalid type passed in, expected " \
                        "integer.  Type Received: {0}".format(type(_max_zones))
                raise TypeError(e_msg)
            else:
                # Overwrite existing max concurrent zones with new value
                self.mc = _max_zones

        # Command for sending
        command = "SET,CN,MC={0}".format(str(self.mc))

        try:
            # Attempt to set max concurrent zones at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Max Concurrent Zones' to: '{0}'".format(
                    str(self.mc))
            raise Exception(e_msg)
        else:
            print("Successfully set controller 'Max Concurrent Zones' to: {0}".format(str(self.mc)))

    #################################
    def set_serial_number_on_cn(self, _serial_num):
        """
        Sets the serial number of the controller. The current instance value can be overwritten by
        passing in a value for _serial_num. \n
        :param _serial_num:  serial number of the controller. \n
        :type _serial_num:   str \n
        :return:
        """
        # take the serial number that is passed in from the use case strip off the last 5 characters
        # take the new serial number passed is in to overwrite the old and strip of the first two characters
        # add the two together giving you a new serial number this allow the first to character to be kept the same
        # value from what you passed in at the use case level

        # first take the serial number a see if it is a string
        if not isinstance(_serial_num, str):
            e_msg = "Serial number must be in a string format '{0}'".format(
                    str(_serial_num))
            raise TypeError(e_msg)

        mn_serial = self.serial_num[:2]
        serial_nm = _serial_num[2:]
        new_sn = str(mn_serial + serial_nm)

        # count the number of characters in the string
        if len(new_sn) != 7:
            raise ValueError("controller serial number must be a seven digit string " +
                             str(new_sn))
        else:
            # Overwrite controller serial number with new value
            self.sn = new_sn

        # Command for sending
        command = "{0},{1},{2}={3}".format(
            opcodes.set_action,     # {0} SET
            opcodes.controller,     # {1} CN
            opcodes.serial_number,  # {2} SN
            str(self.sn)            # {3} Serial number
        )

        try:
            # Attempt to set serial number of the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Serial number' to: '{0}'".format(
                    str(new_sn))
            raise Exception(e_msg)
        else:
            print("Successfully set controller 'Serial Number' to: {0}".format(str(self.sn)))

    #################################
    def set_rain_delay_on_cn(self, _days_to_delay=None):
        """
        Sets the rain delay for the controller to the number of days specified in the parameter
        :param _days_to_delay:      Number of days to pause. \n
        :type _days_to_delay:       Integer. \n
        :return:
        """
        # Check if overwrite is intended
        if _days_to_delay is not None:

            # Verify integer is passed in for number of days to set for rain delay
            if not isinstance(_days_to_delay, int):
                e_msg = "Failed trying to set rain delay for controller. Invalid type passed in, " \
                        "expected integer. Type Received: {0}".format(type(_days_to_delay))
                raise TypeError(e_msg)
            else:
                # Set rain delay attribute to value passed in
                self.rp = _days_to_delay

        # Command for sending
        command = "SET,CN,RP={0}".format(str(self.rp))

        try:
            # Send command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Rain Delay' to: '{0}'".format(str(self.rp))
            raise Exception(e_msg)
        else:
            print("Successfully set controller's 'Rain Delay' to: {0}".format(str(self.rp)))

    #################################
    def set_rain_jumper_state_on_cn(self, _value=None):
        """
        Set the rain jumper to be init_driver or closed based on the value passed in. \n
        :param _value:      Accepted values: 'OP' (init_driver), 'CL' (closed) \n
        :type _value:       str \n
        :return:
        """
        # Check if overwrite is intended
        if _value is not None:

            # Make sure entered correct value
            if _value not in ["CL", "OP"]:
                e_msg = "Exception occurred attempting to set incorrect 'Rain Jumper State' value for controller: " \
                        "'{0}', Expects 'CL' | 'OP'.".format(str(_value))
                raise ValueError(e_msg)
            else:
                self.jr = _value

        # Command for sending
        command = "SET,CN,JR={0}".format(str(self.jr))

        try:
            # Send command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Rain Jumper State' to: '{0}'".format(str(self.jr))
            raise Exception(e_msg)
        else:
            print("Successfully set controller's 'Rain Jumper' to: {0}".format(str(self.jr)))

    #################################
    def set_flow_jumper_state_on_cn(self, _value=None):
        """
        Set the flow jumper to be init_driver or closed based on the value passed in for a 3200. \n
        :param _value:      Accepted values: 'OP' (init_driver), 'CL' (closed) \n
        :type _value:       String. \n
        :return:
        """
        # this is a 3200 specific method
        if self.is3200():

            # Check if overwrite is intended
            if _value is not None:

                # Make sure entered correct value
                if _value not in ["CL", "OP"]:
                    e_msg = "Exception occurred attempting to set incorrect 'Flow Jumper State' value for controller: " \
                            "'{0}', Expects 'CL' | 'OP'.".format(str(_value))
                    raise ValueError(e_msg)
                else:
                    self.jf = _value

            # Command for sending
            command = "SET,CN,JF={0}".format(str(self.jf))

            try:
                # Send command
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to set Controller's 'Flow Jumper State' to: '{0}'".format(
                    str(self.jf)    # {0}
                )
                raise Exception(e_msg)
            else:
                print("Successfully set controller's 'Flow Jumper' to: {0}".format(str(self.jf)))
        else:
            raise ValueError("Attempting to set flow jumper value to a 1000, which is currently not supported.")

    #################################
    def set_pause_jumper_state_on_cn(self, _value=None):
        """
        Set the pause jumper to be init_driver or closed based on the value passed in for a 3200. \n
        :param _value:      Accepted values: 'OP' (init_driver), 'CL' (closed) \n
        :type _value:       String. \n
        :return:
        """
        # this is a 3200 specific method
        if self.is3200():

            # Check if overwrite is intended
            if _value is not None:

                # Make sure entered correct value
                if _value not in ["CL", "OP"]:
                    e_msg = "Exception occurred attempting to set incorrect 'Pause Jumper State' value for " \
                            "controller: '{0}', Expects 'CL' | 'OP'.".format(str(_value))
                    raise ValueError(e_msg)
                else:
                    self.jp = _value

            # Command for sending
            command = "SET,CN,JP={0}".format(str(self.jp))

            try:
                # Send command
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to set Controller's 'Pause Jumper State' to: '{0}'" \
                        .format(str(self.jp))
                raise Exception(e_msg)
            else:
                print("Successfully set controller's 'Pause Jumper' to: {0}".format(str(self.jp)))
        else:
            raise ValueError("Attempting to set pause jumper value to a 1000, which is currently not supported.")

    #############################
    def set_date_and_time_on_cn(self, _date, _time, update_date_mngr=True):
        """
        Set the date and time for the controller. \n
        :param _date: Date for controller 'MM/DD/YYYY' format. \n
        :param _time: Time for controller 'HH:MM:SS' format. \n
        :param update_date_mngr: Flag set when updating controller and substations at the same time.
        :return:
        """
        print ('Set controller date and time to ' + str(_date) + ' ' + str(_time))
        try:
            datetime.strptime(_date, '%m/%d/%Y')
        except ValueError:
            raise ValueError("Incorrect data format, should be MM/DD/YYYY")
        try:
            datetime.strptime(_time, '%H:%M:%S')
        except ValueError:
            raise ValueError("Incorrect data format, should be HH:MM:SS")

        try:
            self.ser.send_and_wait_for_reply('SET,DT=' + str(_date) + ' ' + str(_time))

            if update_date_mngr:
                date = datetime.strptime(_date, '%m/%d/%Y').date()
                date_mngr.controller_datetime.set_from_datetime_obj(datetime_obj=date, time_string=_time)
                date_mngr.curr_day.set_from_datetime_obj(datetime_obj=date, time_string=_time)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's Date and Time to: '{0}' '{1}'" \
                    .format(str(_date), str(_time))
            raise Exception(e_msg)
        else:
                print("Successfully set controller's Date and time to: {0} {1}".format(str(_date), str(_time)))

    #############################
    def set_sim_mode_to_on(self):
        """
        put controller in sim mod
        """
        print "Put controller into sim mode"
        try:
            self.ser.send_and_wait_for_reply('DO,SM=TR')
            print "-------------------------------------------------------"
            print "       | - Successfully Turned On Sim Mode.  - |       "
            print "-------------------------------------------------------"
        except Exception as e:
            e_msg = "Turn On Sim Mode Command Failed: " + e.message
            raise Exception(e_msg)

    #############################
    def set_sim_mode_to_off(self):
        """
        turn off sim mode this will also start the clock but doesnt send second
        command to restart clock because clock restarts when you exit sim mode
        """
        print "Turn off sim mode"
        try:
            self.ser.send_and_wait_for_reply('DO,SM=FA')
            print "-------------------------------------------------------"
            print "       | - Successfully Turned off Sim Mode.  - |      "
            print "-------------------------------------------------------"
        except Exception as e:
            e_msg = "Turn Off Sim Mode Command Failed: " + str(e.message)
            raise Exception(e_msg)


    #############################
    def set_controller_to_run(self):
        """
        the controller does most of its internal processing when the run button is selected
        in order to get the controller status to update after certain events are trigger the run button needs to be
        selected and then the clock needs to increment one second
        Items like the search or test function will set the controller to a paused state and not allow the zones to be added to
        the program until the run key is selected and the controller process the command
        :return:
        :rtype:
        """
        print "Set Controller To Run"
        if self.ty is "10":
            # select run button
            try:
                self.ser.send_and_wait_for_reply('KEY,DL=5')
                self.do_increment_clock(seconds=1)
                print "-------------------------------------------------------"
                print "       | - Successfully Set Controller to Run.  - |    "
                print "-------------------------------------------------------"
            except Exception as e:
                e_msg = "Setting Controller to Run Position Command Failed: " + str(e.message)
                raise Exception(e_msg)
        else:
            # select run button
            try:
                self.ser.send_and_wait_for_reply('KEY,DL=15')
                self.do_increment_clock(seconds=1)
                print "-------------------------------------------------------"
                print "       | - Successfully Set Controller to Run.  - |    "
                print "-------------------------------------------------------"
            except Exception as e:
                e_msg = "Setting Controller to Run Position Command Failed: " + str(e.message)
                raise Exception(e_msg)

    #############################
    def set_controller_to_off(self):
        """
        Turn controller to off to stop all programs running
        :return:
        :rtype:
        """
        print "Set Controller To Off"
        if self.ty is "10":
            # select off button
            try:
                self.ser.send_and_wait_for_reply('KEY,DL=1')
                self.do_increment_clock(seconds=1)
                print "-------------------------------------------------------"
                print "       | - Successfully Set Controller to off.  - |    "
                print "-------------------------------------------------------"
            except Exception as e:
                e_msg = "Setting Controller to Off Position Command Failed: " + str(e.message)
                raise Exception(e_msg)
        else:
            # select off button
            try:
                self.ser.send_and_wait_for_reply('KEY,DL=0')
                self.do_increment_clock(seconds=1)
                print "-------------------------------------------------------"
                print "       | - Successfully Set Controller to off.  - |    "
                print "-------------------------------------------------------"
            except Exception as e:
                e_msg = "Setting Controller to Off Position Command Failed: " + str(e.message)
                raise Exception(e_msg)

    ##############################
    def set_eto_and_date_stamp_on_cn(self, _controller_et_value, _controller_rain_value, _controller_date):
        """
        - set an ETo value and rain on the controller
            - Send an ETo value
            - send a Rain value
            - send a valid date
                - The value takes effect on the controller after 1:00am (controller time)
        :param _controller_et_value:     Controller ETo value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _controller_et_value:     float \n

        :param _controller_rain_value:   Controller rain value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _controller_rain_value:   float \n

        :param _controller_date:         date format: 20150703 format (yyyymmdd)
        :type  _controller_date:         str \n
        """
        # Check if controller et value is a valid float
        if not isinstance(_controller_et_value, float):
            e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was and Invalid " \
                    "argument type, expected a float, received: {1}"\
                    .format(_controller_et_value, type(_controller_et_value))
            raise TypeError(e_msg)

        # Check if in controller eto value is in range 0.00 - 5.00
        if _controller_et_value < 0.00 or _controller_et_value > 5.00:
            e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was not between" \
                    "(0.00 and 5.00)"\
                    .format(_controller_et_value)
            raise ValueError(e_msg)
        else:
            # Here, 'format_float_value_to_the_hundredths returns a float value with 2 decimal places, ALWAYS
            Devices.et = round(number=_controller_et_value, ndigits=3)

        # Check if in controller rain value range 0.00 - 5.00
        if _controller_rain_value < 0.00 or _controller_rain_value > 5.00:
            e_msg = "Exception occurred trying to set the controller Rain value. Value received {0} was not between" \
                    "(0.00 and 5.00)"\
                    .format(_controller_rain_value)
            raise ValueError(e_msg)
        else:
            # Here, 'format_float_value_to_the_hundredths returns a float value with 2 decimal places, ALWAYS
            Devices.ra = round(number=_controller_rain_value, ndigits=3)

        # Check if controller et date is a valid str
        if not isinstance(_controller_date, str):
            e_msg = "Exception occurred trying to set the controller ET Date. Value received {0} was and Invalid " \
                    "argument type, expected a str YYYYMMDD" \
                    .format(_controller_date, type(_controller_date))
            raise TypeError(e_msg)

        # Verify the str format and length
        string_length = 8
        if not helper_methods.verify_value_is_string(string1=_controller_date,
                                                     string2=_controller_date,
                                                     length=string_length):
            e_msg = "Exception occurred trying to set the controller ET Date. Value received {0} was and invalid " \
                    "length, expected a {1} characters string (YYYYMMDD)"\
                    .format(_controller_date, string_length)
            raise TypeError(e_msg)
        else:
            self.ed = _controller_date

        # Command for sending
        command = "DO,{0}={1},{2}={3},{4}={5}".format(opcodes.eto, Devices.et,
                                                      opcodes.date_time, self.ed,
                                                      WaterSenseCodes.Rain_Fall, Devices.ra)

        try:
            # Attempt to set ETo and date stamp on the controller
            self.ser.send_and_wait_for_reply(tosend=command)

            for zone in self.zone_objects:
                self.zone_objects[zone].etc = equations.calculate_etc(_eto=Devices.et, _kc=self.zone_objects[zone].kc)
                # self.config.zones[zone].etc = equations.calculate_etc(_eto=Devices.et, _kc=self.config.zones[zone].kc)

        except Exception:
            e_msg = "Exception occurred trying to set the controller ET and date stamp ET and rain fall values. " \
                    "ET = '{0}'Date Stamp = '{1}'and Rain Fall = '{2}'".format(Devices.et, self.ed, Devices.ra)
            raise Exception(e_msg)
        else:
            print("Successfully set the controller ET and date stamp ET and rain fall values. "
                  "ET = '{0}' Date Stamp = '{1}' and Rain Fall = '{2}'".format(Devices.et, self.ed, Devices.ra))

    #################################
    def set_initial_eto_value_cn(self, _initial_et_value):
        """
        - set an initial ETo value on the controller that will be populated to all zones giving them an initial ETo \n
          value\n
            - Send an ETo value
        :param _initial_et_value:     Initial ETo value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _initial_et_value:     float \n
        """
        # Check if controller et value is a valid float
        if not isinstance(_initial_et_value, float):
            e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was and Invalid " \
                    "argument type, expected a float, received: {1}"\
                    .format(_initial_et_value, type(_initial_et_value))
            raise TypeError(e_msg)

        #Check if intial et value in range 0.00 - 5.00
        if _initial_et_value < 0.00 or _initial_et_value > 5.00:
            e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was not between" \
                    "(0.00 and 5.00)"\
                    .format(_initial_et_value)
            raise ValueError(e_msg)
        else:
            self.ei = round(number=_initial_et_value, ndigits=3)

        # Command for sending
        command = "DO,{0}={1}".format(opcodes.initial_ETo, self.ei)

        try:
            # Attempt to set ETo and date stamp on the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set the controller ETo = '{0}' " \
                    "and Date Stamp = '{1}'".format(self.ei, self.ed)
            raise Exception(e_msg)

        # the command above send the controller a packet that set all ETo values of all zones
        # the below for loop sets all of the zones to have an initial ETo value this way we have a way to verify
        # the object against what the controller has
        Devices.et = self.ei

        print("Successfully set the initial ETo value on controller and all zones = '{0}' "
              .format(self.ei))

    #################################
    def set_learn_flow_enabled(self, _pg_ad, _zn_ad=None, _time_delay=None):
        """
        - Turns on the learn flow for a particular program or zone
        :param _pg_ad:     The program address that will enable the learn flow \n
        :type  _pg_ad:     Program \n
        :param _zn_ad:     The zone address that will enable the learn flow \n
        :type  _zn_ad:     Zone \n
        :param _time_delay:     The time delay in seconds \n
        :type  _time_delay:     int \n
        """
        # Check if the program address passed in is an int
        if not isinstance(_pg_ad, int):
            e_msg = "Exception occurred trying to enable learn flow. Argument passed in was an Invalid " \
                    "argument type, expected an int, received: {0}." \
                .format(type(_pg_ad))
            raise TypeError(e_msg)

        # Check if the zone address passed in is an int
        if _zn_ad is not None and not isinstance(_zn_ad, int):
            e_msg = "Exception occurred trying to enable learn flow. Argument passed in was an Invalid " \
                    "argument type, expected an int, received: {0}" \
                .format(type(_zn_ad))
            raise TypeError(e_msg)

        # Check if the zone address passed in was an int
        if _time_delay is not None and not isinstance(_time_delay, int):
            e_msg = "Exception occurred trying to enable learn flow. Time delay passed in was an Invalid " \
                    "argument type, expected an int, received: {0}" \
                .format(type(_time_delay))
            raise TypeError(e_msg)

        # Command for sending
        if _zn_ad is None:
            command = "DO,LF,{0}={1}".format(opcodes.program, _pg_ad)
        elif _time_delay is None:
            command = "DO,LF,{0}={1},{2}={3}".format(opcodes.program, _pg_ad, opcodes.zone, _zn_ad)
        else:
            command = "DO,LF,{0}={1},{2}={3},{4}={5}".format(opcodes.program, _pg_ad, opcodes.zone, _zn_ad,
                                                             opcodes.time_delay, _time_delay)

        try:
            # Send the learn flow command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set learn flow on program '{0}' and zone '{1}'.".format(
                _pg_ad,
                _zn_ad)
            raise Exception(e_msg)
        # If only a program was passed in then leave out the zone
        if _zn_ad is None:
            print "Successfully set the learn flow on program '{0}'".format(_pg_ad)
        else:
            print "Successfully set the learn flow on program '{0}' and zone '{1}'.".format(_pg_ad, _zn_ad)

    #################################
    def set_program_start_stop(self, _pg_ad, _function):
        """
        - Set a program to either start or stop
        :param _pg_ad:     The program's address \n
        :type  _pg_ad:     int \n
        :param _function:     Either an 'SR' or 'SP' \n
        :type  _function:     str \n
        """
        # Check if the program address passed in is an int
        if not isinstance(_pg_ad, int):
            e_msg = "Exception occurred trying to set program's start/stop. Argument passed in was an Invalid " \
                    "argument type, expected an int, received: {0}." \
                .format(type(_pg_ad))
            raise TypeError(e_msg)

        # Check if controller et value is a valid str
        if _function not in ["SR", "SP"]:
            e_msg = "Exception occurred trying to set the program's start/stop condition. Value received {0} was and " \
                    "Invalid argument type, expected either string 'SR' or 'SP', got {0}" \
                .format(_function)
            raise TypeError(e_msg)

        # Command for sending
        command = "DO,{0}={1},{2}={3}".format(opcodes.program, _pg_ad, opcodes.function, _function)

        try:
            # Attempt to start/stop a program
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to start/stop program '{0}' with opcode '{1}'".format(_pg_ad, _function)
            raise Exception(e_msg)
        print("Successfully started/stopped the program '{0}'.".format(_pg_ad))

    ################################
    def do_firmware_update(self, were_from, bm_id_number=None, directory=None, file_name=None):
        """
        1000 must have a .bin file and the 3200 must be a .zip file

        :param were_from:       this is the location of the firmware to update either from BaseManager or USB Storage device
        :type were_from:        str
        :param bm_id_number:    this is the database id from the server where the firmware is located
        :type bm_id_number:     int
        :param directory:       _directory = "common/firmware_update_files_files/test.zip"
        :type directory:        str
        :param file_name:       this the the file name located on the USB
        :type file_name:        str

        :return:
        :rtype:
        """
        # Fail Fast if user passes in an invalid location of the firmware update
        if were_from != opcodes.basemanager and were_from != opcodes.usb_flash_storage and were_from != opcodes.local_directory:
            e_msg = "where from is an incorrect location. Accepted locations are {0} or {1}.".format(
                opcodes.basemanager, opcodes.usb_flash_storage)
            raise Exception(e_msg)

        # 1000 must be a .bin file and the 3200 must be a.zip file to do a firmware update
        # 1000 and 3200 both currently need 1000 byte strings
        file_type = ''
        maxlinelen = 0
        if self.controller_type == opcodes.basestation_1000:
            # 1000 must be a .bin file to do a firmware update
            file_type = opcodes.bin_file
            # this is the number of bytes the controller can handle in a single packet
            maxlinelen = 1000
        if self.controller_type == opcodes.basestation_3200:
            # 3200 must be a.zip file to do a firmware update
            file_type = opcodes.zip_file
            # this is the number of bytes the controller can handle in a single packet
            maxlinelen = 1000

        # TODO find a way to update the firmware variable in the controller object

        if were_from == opcodes.basemanager:
            # if the controller is not connected to BaseManager throw an error
            if not basemanager_connection[1].verify_cn_connected_to_bm():
                e_msg = "Controller Not Connected to BaseManager unable to do firmware update"
                raise Exception(e_msg)
            # build command to send to controller to ask basemanager for firmware update
            command = "DO,{0}={1},{2}={3}".format(
                opcodes.update_firmware,    # {0}
                opcodes.basemanager,        # {1}
                opcodes.file_name,          # {2}
                str(bm_id_number))          # {3)
            try:
                # need to turn echo off because the data is to large for the reply command
                self.turn_off_echo()
                self.ser.sendln(tosend=command)
            except Exception as e:
                e_msg = "Unable to update firmware from BaseManager: " + e.message
                raise Exception(e_msg)

            if self.controller_type == '32':
                self.wait_for_controller_after_reboot()
            self.turn_on_echo()

        elif were_from == opcodes.usb_flash_storage:
            # verify controller has a USB inserted this will throw an error if there isn't one inserted
            # self.verify_usb_is_inserted()
            # build command to send to controller to ask for usb firmware update
            command = "DO,{0}={1},{2}={3}".format(
                opcodes.update_firmware,            # {0}
                opcodes.usb_flash_storage,          # {1}
                opcodes.file_name,                  # {2}
                file_name)                          # {3)
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
                if self.controller_type == '32':
                    self.wait_for_controller_after_reboot()
            except Exception as e:
                e_msg = "Unable to update firmware from USB flash Storage: " + e.message
                raise Exception(e_msg)

        if were_from == opcodes.local_directory:
            if directory is None or file_name is None:
                e_msg = "if useing a directory than the directory, file name must be passed in: "
                raise Exception(e_msg)

            list_of_commands = file_transfer_handler.packet_to_send_to_cn(_cn_type=self.controller_type,
                                                                          _directory=directory,
                                                                          _file_name=file_name,
                                                                          _file_type=file_type,
                                                                          _maxlinelen=maxlinelen)
            # need to turn echo off because the data is to large for the reply command
            self.turn_off_echo()
            last_packet = len(list_of_commands)-1
            print list_of_commands[last_packet]
            for index, packet in enumerate(list_of_commands):
                packet_to_send = list_of_commands[index]
                print "index number: " + str(index)

                command = packet_to_send

                try:
                    self.ser.send_and_wait_for_reply(tosend=command)
                except Exception as e:
                    e_msg = "Unable to update firmware from file in directory: " + e.message
                    raise Exception(e_msg)
                if index == last_packet:
                    if self.controller_type == '32':
                        self.wait_for_controller_after_reboot()
            self.stop_clock()
            self.turn_on_echo()

        # Updates the controller object's firmware version variable to be the same as the the firmware version.
        self.get_data()
        self.vr = self.data.get_value_string_by_key('VR')

        # Print success message
        print("Successfully updated firmware from '{0}'.".format(were_from))

    # #################################
    def do_backup_programming(self, were_to):
        """
        Backs up the programming of the controller to a specified location.
        :param were_to: this is the location to send backup to either from BaseManager or USB Storage device
        :type were_to:  str
        :return:
        :rtype:
        """
        # Fail Fast if user passes in an invalid location of the firmware update
        if were_to != opcodes.basemanager and were_to != opcodes.usb_flash_storage:
            e_msg = "where tp is an incorrect location. Accepted locations are {0} or {1}.".format(
                opcodes.basemanager, opcodes.usb_flash_storage)
            raise Exception(e_msg)

        if were_to == opcodes.basemanager:
            # if the controller is not connected to BaseManager throw an error
            if not basemanager_connection[1].verify_cn_connected_to_bm():
                e_msg = "Controller Not Connected to BaseManager unable to do firmware update"
                raise Exception(e_msg)
            # build command to send to controller to send back up to basemanager
            command = "DO,{0}={1}".format(
                opcodes.backup_programming,    # {0}
                opcodes.basemanager)           # {1}
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Unable to backup to BaseManager"
                raise Exception(e_msg)
        elif were_to == opcodes.usb_flash_storage:
            # verify controller has a USB inserted this will throw an error if there isn't one inserted
            self.verify_usb_is_inserted()
            # build command to send to controller to send backup to flash drive
            command = "DO,{0}={1}".format(
                opcodes.backup_programming,         # {0}
                opcodes.usb_flash_storage)          # {1}
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Unable to backup to USB flash Storage"
                raise Exception(e_msg)
        # Print success message
        print("Successfully sent backup file to '{0}'.".format(were_to))

    #################################
    def do_restore_programming(self, were_from, file_number=None):
        """

        :param were_from:   this is the location of the restore file either from BaseManager or USB Storage device
        :type were_from:    str
        :param file_number:   this the slot located on the USB
                            - 1000: FL={1 to 5} backup file slot number
                            - 3200:  FL={1 to 5} backup file number in list with 1=most recent
        :type file_number:    int

        :return:
        :rtype:
        """
        # Fail Fast if user passes in an invalid location of the firmware update
        if were_from != opcodes.basemanager and were_from != opcodes.usb_flash_storage:
            e_msg = "where from is an incorrect location. Accepted locations are {0} or {1}.".format(
                opcodes.basemanager, opcodes.usb_flash_storage)
            raise Exception(e_msg)

        if were_from == opcodes.basemanager:
            # if the controller is not connected to BaseManager throw an error
            if not basemanager_connection[1].verify_cn_connected_to_bm():
                e_msg = "Controller Not Connected to BaseManager unable to do firmware update"
                raise Exception(e_msg)
            # build command to send to controller to ask basemanager to restore programming
            command = "DO,{0}={1}".format(
                opcodes.restore_programming,    # {0}
                opcodes.basemanager)            # {1}
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Unable to restore programming from BaseManager"
                raise Exception(e_msg)
        elif were_from == opcodes.usb_flash_storage:
            # verify controller has a USB inserted this will throw an error if there isn't one inserted
            self.verify_usb_is_inserted()
            # build command to send to controller to ask for a restore command
            command = "DO,{0}={1},{2}={3}".format(
                opcodes.restore_programming,  # {0}
                opcodes.usb_flash_storage,    # {1}
                opcodes.file_name,            # {2}
                file_number)                  # {3)
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Unable to restore programming from USB flash Storage"
                raise Exception(e_msg)
        # Print success message
        print("Successfully restored programming from '{0}'.".format(were_from))


    # #################################
    def set_address_for_objects(self, object_dict, ad_range):
        """
        :param object_dict: Dictionary containing objects to address
        :type object_dict: dict
        :param ad_range: Address range to address each object
        :type ad_range: list[int]
        """
        for address in ad_range:
            object_dict[address].set_address_on_cn()

    #############################
    def set_address_and_default_values_for_zn(self, zn_object_dict, zn_ad_range):
        """
        Address zones on the controller. \n
        :param zn_object_dict:      Dictionary containing Zone objects. \n
        :param zn_ad_range:         Range for addressing Zone objects read from json file.. \n
        :return:
        """
        for zn_ad in zn_ad_range:
            zn_object_dict[zn_ad].set_address_on_cn()
            zn_object_dict[zn_ad].set_default_values()

    #############################
    def set_address_and_default_values_for_ms(self, ms_object_dict, ms_ad_range):
        """
        Address Moisture Sensors on the controller. \n
        :param ms_object_dict:      Dictionary containing Moisture Sensor objects. \n
        :param ms_ad_range:         Range for addressing Moisture Sensor objects read from json file.. \n
        :return:
        """
        if self.ty is '10':
            for ms_ad in ms_ad_range:
                ms_object_dict[ms_ad].set_address_on_cn()
                ms_object_dict[ms_ad].set_default_values()
        else:
            for ms_ad in ms_ad_range:
                ms_object_dict[ms_ad].set_default_values()

    #############################
    def set_address_and_default_values_for_fm(self, fm_object_dict, fm_ad_range):
        """
        Address Flow Meters on the controller. \n
        :param fm_object_dict:      Dictionary containing Flow Meter objects. \n
        :param fm_ad_range:         Range for addressing Flow Meter objects read from json file.. \n
        :return:
        """
        if self.ty is '10':
            for fm_ad in fm_ad_range:
                fm_object_dict[fm_ad].set_address_on_cn()
                fm_object_dict[fm_ad].set_default_values()
        else:
            for fm_ad in fm_ad_range:
                fm_object_dict[fm_ad].set_default_values()

    #############################
    def set_address_and_default_values_for_mv(self, mv_object_dict, mv_ad_range):
        """
        Address Master Valve on the controller. \n
        :param mv_object_dict:      Dictionary containing Master Valve objects. \n
        :param mv_ad_range:         Range for addressing Master Valve objects read from json file. \n
        :return:
        """
        for mv_ad in mv_ad_range:
            mv_object_dict[mv_ad].set_address_on_cn()
            mv_object_dict[mv_ad].set_default_values()

    #############################
    def set_address_and_default_values_for_ts(self, ts_object_dict, ts_ad_range):
        """
        Address Temperature Sensor on the controller. \n
        :param ts_object_dict:      Dictionary containing Temperature Sensor objects. \n
        :param ts_ad_range:         Range for addressing Temperature Sensor objects read from json file. \n
        :return:
        """
        if self.ty is '10':
            for ts_ad in ts_ad_range:
                ts_object_dict[ts_ad].set_address_on_cn()
                ts_object_dict[ts_ad].set_default_values()
        else:
            for ts_ad in ts_ad_range:
                ts_object_dict[ts_ad].set_default_values()

    #############################
    def set_address_and_default_values_for_sw(self, sw_object_dict, sw_ad_range):
        """
        Address Event Switch on the controller. \n
        :param sw_object_dict:      Dictionary containing Event Switch objects. \n
        :param sw_ad_range:         Range for addressing Event Switch objects read from json file. \n
        :return:
        """
        if self.ty is '10':
            for sw_ad in sw_ad_range:
                sw_object_dict[sw_ad].set_address_on_cn()
                sw_object_dict[sw_ad].set_default_values()
        else:
            for sw_ad in sw_ad_range:
                sw_object_dict[sw_ad].set_default_values()

    #################################
    def set_serial_port_timeout(self, timeout):
        """
        this is used to reset the length of time before the serial port times out
        """
        self.ser.serial_conn.setTimeout(timeout)

    #################################
    def get_date_and_time(self):
        """
        Command GET,DT
        :return:
        :rtype:
        """

        try:
            command = "GET,{0}".format(
                opcodes.date_time  # {0}
            )
            controller_date_time = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Getting controller date time command failed"
            raise Exception(e_msg)
        else:
            print "Successfully Retrieved the Controller Date and Time"
            return controller_date_time

    #################################
    def get_memory_usage_on_cn(self):
        """
        Verifies the memory usage on controller. \n
        :return:
        """
        # Get the current memory usage from controller
        try:
            self.get_data()
            self.mu = float(self.data.get_value_string_by_key('MU'))
        except Exception:
            e_msg = "Getting controller memory usage command failed"
            raise Exception(e_msg)
        print("Verified Controller {0}'s 'Memory Usage': '{1}' of 100%.".format(
                str(self.mac),  # {0}
                self.mu  # {1}
            ))

    #################################
    def verify_usb_is_inserted(self):
        """
        send command to controller to get usb connection status
        parse status
        verify connected
        """
        try:
            command = "GET,US"
            controller_reply = self.ser.get_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            exception_message = "Exception occurred after sending command to controller: 'GET,US'.\n" + \
                                "Controller Response: [" + str(ae.message) + "]"
            raise AssertionError(exception_message)

        # this parses the status from controller reply
        status_from_cn = controller_reply.get_value_string_by_key(opcodes.status_code)

        # fail if the controller doesn't return back the usb flash storage recognized command
        if status_from_cn != opcodes.usb_flash_storage_recognized:
            e_msg = "Getting controller memory usage command failed. Status returned was '{0}', we expected '{1}'."\
                .format(
                    status_from_cn,
                    opcodes.usb_flash_storage_recognized
                )
            raise AssertionError(e_msg)
        else:
            print "Successfully verify USB flash storage present"

    #################################
    def verify_date_and_time(self):
        """
        Verify controller object date and time against the controller date and time
        :return:
        :rtype:
        """
        controller_date_and_time_str = self.get_date_and_time().get_value_string_by_key(opcodes.date_time)
        controller_date_and_time = datetime.strptime(controller_date_and_time_str, "%m/%d/%Y %H:%M:%S")
        controller_object_date_time = datetime.combine(date_mngr.controller_datetime.obj,
                                                       date_mngr.controller_datetime.time_obj.obj)

        if controller_date_and_time != controller_object_date_time:
            if abs(controller_object_date_time - controller_date_and_time) >= timedelta(days=0,
                                                                                        hours=0,
                                                                                        minutes=0,
                                                                                        seconds=60):
                e_msg = "The date and time of the controller didn't match the controller object:\n" \
                        "\tController Object Date Time:       \t\t'{0}'\n" \
                        "\tDate Time Received From Controller:\t\t'{1}'\n".format(
                            controller_object_date_time,  # {0} The date of the controller object
                            controller_date_and_time      # {1} The date the controller has
                        )
                raise ValueError(e_msg)
            # only print this if they were not exact
            e_msg = "############################  Date and time were not exact but were within +- 60 Seconds \n" \
                    "\tController Object Date Time:       \t\t'{0}'\n" \
                    "\tDate Time Received From Controller:\t\t'{1}'\n".format(
                            controller_object_date_time,  # {0} The date of the controller object
                            controller_date_and_time      # {1} The date the controller has
                        )
            print(e_msg)
        else:
            print "Verified controller current date and time against controller object date time:\n" \
                        "\tController Object Date Time:       \t\t'{0}'\n" \
                        "\tDate Time Received From Controller:\t\t'{1}'\n".format(
                            controller_object_date_time,  # {0} The date of the controller object
                            controller_date_and_time      # {1} The date the controller has
                        )

    #################################
    def verify_connection_to_substation(self, _address, _expected_status):
        """
        Verifies the substation connection against an expected status.
        
        :param _address: Address of substation to verify status for.
        :type _address: int
        
        :param _expected_status: Expected status to verify against.
        :type _expected_status: str
        
        :return: 
        :rtype: bool
        """
        if self.ty in ['10', '32']:
            # data = _data if _data else self.get_data()

            command = "GET,{0}={1}".format(opcodes.substation, _address)

            try:
                data = self.ser.get_and_wait_for_reply(tosend=command)
            except Exception as e:
                e_msg = "Exception occurred trying to get Controller's 'Substation Connection Status' for " \
                        "Substation: '{0}'.".format(_address)
                raise Exception(e_msg)

            else:
                sb_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

                if _expected_status != sb_ss_on_cn:
                    e_msg = "Exception occurred trying to verify Controller's 'Substation Connection Status' for " \
                            "Substation: '{0}'. Expected Status: '{1}', Received: '{2}'.".format(
                                _address,
                                _expected_status,
                                sb_ss_on_cn
                            )
                    raise ValueError(e_msg)
                else:
                    print "Successfully verified Controller's 'Substation Connection Status' for Substation: '{0}', " \
                          "Status: '{1}'.".format(_address, _expected_status)
                    return True

        else:
            raise ValueError("Attempting to verify 'Connection from Controller to Substation' on a Substation which "
                             "is not supported.")

    #################################
    def verify_max_concurrent_zones_on_cn(self):
        """
        Verifies the max concurrent zones set for this controller. Expects the controller's value returned and
        this instance's value to be equal.
        :return:
        """
        # Get max concurrent zones set at the controller
        mc_from_cn = int(self.data.get_value_string_by_key('MC'))

        # Compare status versus what is on the controller
        if self.mc != mc_from_cn:
            e_msg = "Unable to verify Controller's 'Max Concurrent Zones'. Received: {0}, Expected: {1}".format(
                    str(mc_from_cn),    # {0}
                    str(self.mc)        # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Controller {0}'s 'Max Concurrent Zones': '{1}'.".format(
                  str(self.mac),    # {0}
                  str(self.mc)      # {1}
                  ))

    #################################
    def verify_code_version_on_cn(self):
        """
        Verifies the code version for this controller. Expects the controller's value returned and the version passed in
        as a parameter to be equal. \n
        :return:
        """
        # Get the current code version on the controller
        fw_from_cn = self.data.get_value_string_by_key('VR')

        # Compare status versus what is on the controller
        if self.vr != fw_from_cn:
            e_msg = "Unable to verify Controller's 'Code Version'. Received: {0}, Expected: {1}".format(
                    str(fw_from_cn),    # {0}
                    str(self.vr)        # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Controller {0}'s 'Code Version': '{1}'.".format(
                  str(self.mac),    # {0}
                  self.vr           # {1}
                  ))

    #################################
    def verify_rain_jumper_state_on_cn(self):
        """
        Verifies the rain jumper's current state on the controller against the expected state passed in as a parameter.
        :return:
        """
        # Get the current state of the rain jumper on the controller
        jr_state_from_cn = self.data.get_value_string_by_key('JR')

        # Compare against what is on the controller
        if self.jr != jr_state_from_cn:
            e_msg = "Unable to verify Controller's 'Rain Jumper State'. Received: {0}, Expected: {1}".format(
                    str(jr_state_from_cn),  # {0}
                    self.jr                 # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Controller {0}'s 'Rain Jumper State': '{1}'.".format(
                  str(self.mac),    # {0}
                  self.jr           # {1}
                  ))

    #################################
    def verify_flow_jumper_state_on_cn(self):
        """
        Verifies the flow jumper's current state on the controller against the expected state passed in as a parameter.
        :return:
        """
        # Verify only 3200 is trying to use method
        if self.ty == "32":

            # Get the current state of the 'Flow Jumper' on the controller
            jf_state_from_cn = self.data.get_value_string_by_key('JF')

            # Compare against what is on the controller
            if self.jf != jf_state_from_cn:
                e_msg = "Unable to verify Controller's 'Flow Jumper State'. Received: {0}, Expected: {1}".format(
                        str(jf_state_from_cn),  # {0}
                        self.jf                 # {1}
                )
                raise ValueError(e_msg)
            else:
                print("Verified Controller {0}'s 'Flow Jumper State': '{1}'.".format(
                      str(self.mac),    # {0}
                      self.jf           # {1}
                      ))

        else:
            raise ValueError("Attempting to verify a 'Pause Jumper' state on a 1000 which is not supported.")

    #################################
    def verify_pause_jumper_state_on_cn(self):
        """
        Verifies the pause jumper's current state on the controller against the expected state passed in as a parameter.
        :return:
        """
        # Verify only 3200 is trying to use method
        if self.ty == "32":

            # Get the current state of the 'Pause Jumper' on the controller
            jp_state_from_cn = self.data.get_value_string_by_key('JP')

            # Compare against what is on the controller
            if self.jp != jp_state_from_cn:
                e_msg = "Unable to verify Controller's 'Pause Jumper State'. Received: {0}, Expected: {1}".format(
                    str(jp_state_from_cn),  # {0}
                    self.jp                 # {1}
                )
                raise ValueError(e_msg)
            else:
                print("Verified Controller {0}'s 'Pause Jumper State': '{1}'.".format(
                    str(self.mac),    # {0}
                    self.jp           # {1}
                ))
        else:
            raise ValueError("Attempting to verify a 'Pause Jumper' state on a 1000 which is not supported.")

    #################################
    def verify_description_length(self, _type, _desc):
        """
        Verify the length of the description before sending to the controller. \n
        :return:
        """
        if _type == "32":
            max_chars = 42      # 3200 Max description length
        else:
            max_chars = 32      # 1000 Max description length

        length = len(_desc)

        # Compare lengths, if _desc is within length requirements, return True (valid description)
        if length > max_chars:
            e_msg = "Controller description '{0}' is too long. Max description length for {1}00: {2}, " \
                    "Current length: {3}".format(
                    str(_desc),           # {0}
                    str(_type),           # {1}
                    str(max_chars),   # {2}
                    str(length)           # {3}
                    )
            raise ValueError(e_msg)
        else:
            return True

    #################################
    def verify_serial_number_on_cn(self):
        """
        Verifies the Serial Number set on the controller for the specified device type. \n
        :return:
        """
        sn_on_cn = self.data.get_value_string_by_key('SN')
        # this can be a 3K or a 3X

        # Compare Serial Numbers
        if self.sn != sn_on_cn:
            if sn_on_cn != self.sn:
                e_msg = "Unable verify {0}: {1}'s 'Serial Number'. Received: {2}, Expected: {3}".format(
                    self.dv_type,   # {0}
                    str(self.ad),   # {1}
                    str(sn_on_cn),  # {2}
                    str(self.sn)    # {3}
                )
                raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s Serial Number: '{2}' on controller".format(
                self.dv_type,  # {0}
                str(self.ad),  # {1}
                str(self.sn)  # {2}
            ))
            #################################
    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Controller'. \n
        :param _expected_status:    Expected status to compare against. \n
        :type _expected_status:     str \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify base attributes
        # self.verify_date_and_time()  # verify date time object against current date time of the controller
        self.verify_description_on_cn()

        #TODO in the 3200 i need to be able to verify two different serial number either 3K or 3X
        self.verify_serial_number_on_cn()
        self.verify_latitude_on_cn()
        self.verify_longitude_on_cn()

        if _expected_status is not None:
            self.verify_status_on_cn(status=_expected_status)

        # Verify 'Controller' specific attributes
        self.verify_code_version_on_cn()
        self.verify_rain_jumper_state_on_cn()

        # For 3200 controller
        if self.controller_type == "32":

            # Verify 3200 specific attributes
            self.verify_flow_jumper_state_on_cn()
            self.verify_pause_jumper_state_on_cn()

    #############################
    def init_cn(self):
        """
        initiate controller to a known state so that it doesnt have a configuration or any devices loaded
        turn on echo so the commands are displayed in the console
        turn on sim mode so the clock can be stopped
        stop the clock
        Set the date and time so  that the controller is in a known state
        turn on faux IO
        clear all devices
        Clear all programming
        """

        # turn on echo
        try:
            # turn on echo so the command will display in the debugger
            print "Turn on echo"
            try:
                self.turn_on_echo()
            except AssertionError, ae:
                raise AssertionError("Turn on Echo Command Failed: " + ae.message)
            try:
                self.set_sim_mode_to_on()
            except AssertionError, ae:
                raise AssertionError("Turn on sim Mode to controller failed: " + ae.message)
            # stop the clock
            print "Stop the Clock"
            try:
                self.stop_clock()
            except AssertionError, ae:
                raise AssertionError("stop Command Failed: " + ae.message)
            print "Set date and Time of the controller to 1/1/2014"
            try:
                self.set_date_and_time_on_cn(_date='01/01/2014', _time='01:00:00')
            except AssertionError, ae:
                raise AssertionError("stop Command Failed: " + ae.message)
            # turn on faux io so that the controller can simulate two-wired IO
            try:
                self.turn_on_faux_io()
            except AssertionError, ae:
                raise AssertionError("Turn on FauxIO Command Failed: " + ae.message)
            # clear all devices that are in the controller so a clean test can be performed
            print "Clear all devices from controller"
            try:
                self.ser.send_and_wait_for_reply('DEV,CL=AL')
            except AssertionError, ae:
                raise AssertionError("Clear All Devices Command Failed: " + ae.message)
            # clear all programming in the controller so a clean test can be performed
            print "Clear all programming from controller"
            try:
                self.ser.send_and_wait_for_reply('DO,CL=AL')
            except AssertionError, ae:
                raise AssertionError("Clear All Programming Command Failed: " + ae.message)
            print "-------------------------------------------------------"
            print "     | - Successfully initiated controller. - |        "
            print "-------------------------------------------------------"
        except AssertionError, ae:
            raise AssertionError("Initiate Controller Command Failed: " + ae.message)

    #############################
    def do_enable_packet_sending_to_bm(self):
        """
        1000 Specific method. Called after devices have been loaded on the controller to bring the 1000 out of a
        paused state.
        """
        print "Starting packet communication for 1000 between BaseManager and controller"
        try:
            self.do_increment_clock(minutes=5)
            self.set_sim_mode_to_off()
            self.start_clock()
            self.ser.send_and_wait_for_reply('DO,BM=PN')  # ping Basemanager to sync clock with basemanager
        except Exception as e:
            err_text = e.message
            err_msg = "Exception occurred initializing 1000 controller packet sending to BM: {error}".format(
                error=err_text)
            raise Exception(err_msg)

    #############################
    def do_search_for_dv(self, dv_type):
        """
        Tells the controller to search for two-wire devices of the specified type. \n
        """
        # Validate decoder type passed in against accepted types
        if dv_type not in [opcodes.zone, opcodes.moisture_sensor, opcodes.flow_meter, opcodes.master_valve,
                           opcodes.temperature_sensor, opcodes.event_switch, opcodes.alert_relay]:
            raise ValueError("[CONTROLLER] incorrect two-wire device type passed in: " + str(dv_type))
    
        print "Search for device type: " + str(dv_type)
        try:
            command = "DO,SR=" + str(dv_type)
            self.ser.send_and_wait_for_reply(command)
        except Exception as e:
            print(e.message)
            raise Exception("Searching for two-wire devices failed.. " + str(e.message))

    #############################
    def do_search_for_all_devices(self):
        """
        Searches for all available devices on the two wire.
        :return: 
        """
        try:
            self.do_search_for_dv(dv_type=opcodes.zone)
            self.do_search_for_dv(dv_type=opcodes.master_valve)
            self.do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.do_search_for_dv(dv_type=opcodes.event_switch)
            self.do_search_for_dv(dv_type=opcodes.flow_meter)
        except Exception as e:
            # Re-raise caught exception with additional info plus the exception info.
            appended_msg = "Exception occurred searching for all available devices on Controller {0}: {1}".format(
                self.mac,
                e.message
            )
            raise Exception(appended_msg)
        else:
            print "[SUCCESS] Searched for all available devices on Controller {0}".format(self.mac)

    #############################
    def load_assigned_devices(self):
        """
        Helper method. Loads all devices from the configuration file into the controller. \n
        :return: 
        """
        print "Loading all devices from configuration into the Controller {0}".format(self.mac)
        try:
            self.load_dv_to_cn(dv_type="D1", list_of_decoder_serial_nums=self.d1_bicoders)
            self.load_dv_to_cn(dv_type="D2", list_of_decoder_serial_nums=self.d2_bicoders)
            self.load_dv_to_cn(dv_type="D4", list_of_decoder_serial_nums=self.d4_bicoders)
            self.load_dv_to_cn(dv_type="DD", list_of_decoder_serial_nums=self.dd_bicoders)
            self.load_dv_to_cn(dv_type="MS", list_of_decoder_serial_nums=self.ms_bicoders)
            self.load_dv_to_cn(dv_type="FM", list_of_decoder_serial_nums=self.fm_bicoders)
            self.load_dv_to_cn(dv_type="TS", list_of_decoder_serial_nums=self.ts_bicoders)
            self.load_dv_to_cn(dv_type="SW", list_of_decoder_serial_nums=self.sw_bicoders)
        except Exception as e:
            # Re-raise caught exception with additional info plus the exception info.
            appended_msg = "Exception occurred loading all available devices to Controller {0}: {1}".format(
                                self.mac,
                                e.message
                            )
            raise Exception(appended_msg)
        else:
            # Added a sleep here with a theory that the controller was attempting to search for devices when the thread
            # spun to load all of the devices wasn't finished. Thus when we attempted to search and assign the first
            # zone, a BC was returned
            time.sleep(10)
            print "[SUCCESS] Loaded all available devices to Controller {0}.".format(self.mac)

    #############################
    def load_all_dv_to_cn(self, d1_list, mv_d1_list, d2_list, mv_d2_list, d4_list, dd_list, ms_list, fm_list, ts_list,
                          sw_list):
        """
        Helper method. Loads all devices from the configuration file into the controller. \n
        had to have mv_d1_list and mv_d2_list in order to loader decoder that can be used as master valves\n
        master valve can only be single or dual decoders
        :param d1_list:
        :param mv_d1_list
        :param d2_list:
        :param mv_d2_list
        :param d4_list:
        :param dd_list:
        :param ms_list:
        :param fm_list:
        :param ts_list:
        :param sw_list:
        :return:
        """
        print "Loading all devices from configuration into the controller"
        try:
            self.load_dv_to_cn(dv_type="D1", list_of_decoder_serial_nums=d1_list)
            self.load_dv_to_cn(dv_type="D1", list_of_decoder_serial_nums=mv_d1_list)
            self.load_dv_to_cn(dv_type="D2", list_of_decoder_serial_nums=d2_list)
            self.load_dv_to_cn(dv_type="D2", list_of_decoder_serial_nums=mv_d2_list)
            self.load_dv_to_cn(dv_type="D4", list_of_decoder_serial_nums=d4_list)
            self.load_dv_to_cn(dv_type="DD", list_of_decoder_serial_nums=dd_list)
            self.load_dv_to_cn(dv_type="MS", list_of_decoder_serial_nums=ms_list)
            self.load_dv_to_cn(dv_type="FM", list_of_decoder_serial_nums=fm_list)
            self.load_dv_to_cn(dv_type="TS", list_of_decoder_serial_nums=ts_list)
            self.load_dv_to_cn(dv_type="SW", list_of_decoder_serial_nums=sw_list)
        except Exception as e:
            # Re-raise caught exception with additional info plus the exception info.
            appended_msg = "Exception occurred loading all available devices to controller: " + str(e.message)
            raise Exception(appended_msg)
        else:
            # Added a sleep here with a theory that the controller was attempting to search for devices when the thread
            # spun to load all of the devices wasn't finished. Thus when we attempted to search and assign the first
            # zone, a BC was returned
            time.sleep(10)
            print "[SUCCESS] Loaded all available devices to controller."
    
    #############################
    def load_dv_to_cn(self, dv_type, list_of_decoder_serial_nums):
        """
        pass in the decoder type and serial number serial number
        pass that value to the verify functions
        """
        # Validate decoder type passed in against accepted types
        if dv_type not in [opcodes.single_valve_decoder, opcodes.two_valve_decoder, opcodes.four_valve_decoder,
                           opcodes.twelve_valve_decoder, opcodes.flow_meter, opcodes.moisture_sensor,
                           opcodes.temperature_sensor, opcodes.four_to_20_milliamp_sensor_decoder,
                           opcodes.event_switch, opcodes.alert_relay]:
            raise ValueError("[CONTROLLER] incorrect decoder type: " + str(dv_type))
    
        # For each serial number for the decoder type passed in from the list.
        for serial_number in list_of_decoder_serial_nums:
    
            # Validate Serial Number
            if len(serial_number) != 7:
                # count the number of characters in the string
                raise ValueError("Device id is not a valid serial number: " + str(serial_number))
            else:
                print "Loading device of type: " + str(dv_type) + " with serial: " + str(serial_number)
                try:
                    command = 'DEV,{0}={1}'.format(dv_type, str(serial_number))
                    self.ser.send_and_wait_for_reply(command)
                except Exception as e:
                    print(e.message)
                    raise Exception("Load device(s) failed.. " + str(e.message))

    #############################
    def turn_on_faux_io(self):
        """
        Turn on faux io so that the controller can simulate two-wired IO \n
        :return:
        """
        print "Enable faux IO devices"
        try:
            self.ser.send_and_wait_for_reply('DO,FX=TR')
            print "-------------------------------------------------------"
            print "| - Successfully Turned On Faux IO in Controller.  - | "
            print "-------------------------------------------------------"
        except Exception, ae:
            raise Exception("Turn on FauxIO Command Failed: " + ae.message)
    
    #############################
    def turn_on_echo(self):
        """
        Turn on Echo this will tell the controller to echo the packet that was sent to the controller
        """
        print "Turn on Echo"
        try:
            self.ser.send_and_wait_for_reply('DO,EC=TR')
            print "-------------------------------------------------------"
            print "  | - Successfully Turned on echo in Controller.  - |   "
            print "-------------------------------------------------------"
        except Exception, ae:
            raise Exception("Turning on Echo Command Failed: " + ae.message)

    #############################
    def turn_off_echo(self):
        """
        Turn off Echo this will tell the controller not to echo the packet that was sent to the controller
        """
        print "Turn off Echo"
        try:
            self.ser.send_and_wait_for_reply('DO,EC=FA')
            print "-------------------------------------------------------"
            print "  | - Successfully Turned off echo in Controller.  - |   "
            print "-------------------------------------------------------"
        except Exception, ae:
            raise Exception("Turning off Echo Command Failed: " + ae.message)

    #############################
    def stop_clock(self):
        """
        stop clock for sim mod
        """
        print "Stop Clock"
        try:
            self.ser.send_and_wait_for_reply('DO,CK,TM=0')
            print "-------------------------------------------------------"
            print "  | - Successfully Stopped Clock in Controller.  - |   "
            print "-------------------------------------------------------"
        except Exception, ae:
            raise Exception("Stop Clock Command Failed: " + ae.message)
    
    #############################
    def start_clock(self):
        """
        start clock for use in browser mode
        """
        print "Start Clock"
        try:
            self.ser.send_and_wait_for_reply('DO,CK,TM=-1')
        except Exception, ae:
            raise Exception("Start Clock Command Failed: " + ae.message)

    #################################
    def do_increment_clock(self, hours=0, minutes=0, seconds=0, update_date_mngr=True):
        """
        compare each value of hours, minutes, seconds \n
        this allows to send in one of the parameter to increment the clock in the controller \n
        the clock must be stopped in order it increment it
        :param hours:
        :type hours:            int
        :param minutes:
        :type minutes:      int
        :param seconds:
        :type seconds:      int
        :return:            ok from controller
        :rtype:             ok is a str
        """
        print('Increment Clock ' + str(hours) + ' Hours ' + str(minutes) + ' Minutes')
        if hours == 0 and minutes == 0 and seconds == 0:
            e_msg = "Failed trying to increment clock. Invalid type passed in for hours. Received: {0}, " \
                    "Expected: int ".format(str(hours))
            raise ValueError(e_msg)
        if hours != 0:
            if not isinstance(hours, int):
                e_msg = "Failed trying to increment clock. Invalid type passed in for hours. Received: {0}, " \
                        "Expected: int ".format(str(hours))
                raise TypeError(e_msg)
            elif hours not in range(24):
                e_msg = "Failed trying to increment clock. Invalid range for hours. Expected a number between " \
                        "0 - 24, Received: {0},".format(str(hours))
                raise ValueError(e_msg)
            else:
                hours = str(hours).zfill(2)
        if minutes != 0:
            if not isinstance(minutes, int):
                e_msg = "Failed trying to increment clock. Invalid type passed in for minutes. Received: {0}, " \
                        "Expected: int ".format(str(minutes))
                raise TypeError(e_msg)
            elif minutes not in range(60):
                e_msg = "Failed trying to increment clock. Invalid range for minutes. Expected a number between " \
                        "0 - 60, Received: {0},".format(str(minutes))
                raise ValueError(e_msg)
            else:
                minutes = str(minutes).zfill(2)
        if seconds != 0:
            if not isinstance(seconds, int):
                e_msg = "Failed trying to increment clock. Invalid type passed in for seconds. Received: {0}, " \
                        "Expected: int ".format(str(seconds))
                raise TypeError(e_msg)
            elif seconds not in range(60):
                e_msg = "Failed trying to increment clock. Invalid range for seconds. Expected a number between " \
                        "0 - 60, Received: {0},".format(str(seconds))
                raise ValueError(e_msg)
            else:
                seconds = str(seconds).zfill(2)

        # If 1000:
        if self.ty == '10':
            # hours input?
            if hours != 0:
                if len(str(hours)) == 1:
                    hours_str = "0" + str(hours)
                else:
                    hours_str = str(hours)
            else:
                hours_str = "00"
                
            # minutes input?
            if minutes != 0:
                if len(str(minutes)) == 1:
                    minutes_str = "0" + str(minutes)
                else:
                    minutes_str = str(minutes)
            else:
                minutes_str = "00"
                
            # seconds input?
            if seconds != 0:
                if len(str(seconds)) == 1:
                    seconds_str = "0" + str(seconds)
                else:
                    seconds_str = str(seconds)
            else:
                seconds_str = "00"
                
            clock = hours_str + ":" + minutes_str + ":" + seconds_str
            datetime.strptime(clock, '%H:%M:%S')

            # TODO this value error seems to be incorrect the method is not looking for an in it is looking to see
            # TODO if the two method work and raise the error at this level

            # `update_date_mngr` flag is toggled when we want to control when date_mngr is incremented.
            if update_date_mngr:
                try:
                    date_mngr.controller_datetime.increment_date_time(hours=int(hours),
                                                                      minutes=int(minutes),
                                                                      seconds=int(seconds))
                    if date_mngr.curr_day:
                        date_mngr.curr_day.increment_date_time(hours=int(hours),
                                                               minutes=int(minutes),
                                                               seconds=int(seconds))

                except ValueError:
                    e_msg = "Failed trying to increment clock. date time values are incorrect. Received: {0}, " \
                            "Expected: int ".format(str(hours))
                    raise ValueError(e_msg)

        # Otherwise, for 3200:
        else:
            clock = str(hours) + ":" + str(minutes)
            datetime.strptime(clock, '%H:%M')

            # `update_date_mngr` flag is toggled when we want to control when date_mngr is incremented.
            if update_date_mngr:
                try:
                    date_mngr.controller_datetime.increment_date_time(hours=int(hours),
                                                                      minutes=int(minutes))
                    if date_mngr.curr_day:
                        date_mngr.curr_day.increment_date_time(hours=int(hours),
                                                               minutes=int(minutes),
                                                               seconds=int(seconds))
                except ValueError:
                    e_msg = "Failed trying to increment clock. date time values are incorrect. Received: {0}, " \
                            "Expected: int ".format(str(hours))
                    raise ValueError(e_msg)

        self.ser.send_and_wait_for_reply('DO,CK,TM=' + str(clock))

    #############################
    def wait_for_controller_after_reboot(self):
        # self.ser.flush_serial() #this does nothing
        # TODO this is not setting my timer
        self.set_serial_port_timeout(timeout=480)
        result = self.ser.wait(waitfors=['OK'], _maxchars=13800)
        ## Tries and retries the method for 240 seconds (4 minutes)
        # end_time = time.time() + 240
        # while time.time() < end_time:
        #     try:
        #         self.stop_clock()
        #         self.turn_on_echo()
        #     except Exception:
        #         pass
        #
        # time.sleep(120)
        self.set_serial_port_timeout(timeout=120)

        if result == 0:
            self.ser.working()
            print "Successfully waited for controller after reboot."
        else:
            raise ValueError("Reconnection to controller failed")

    #############################
    def do_reboot_controller(self):
        """
        reboot controller than wait for 30 seconds for the controller to wake back up
        flush the input buffer
        listen for the controller to tell you its serial port is back up and running
            wait for the controller to be full back up and running before  a new command is sent
            the controller will send a "ok /n/r" when is ready to start taking command
            The 1000 doesnt need to do this
        :return: 0 if timeout, position of string in list (starting from 1) if the string is received,
                 -1 if we received more than the max # of characters (currently 500) without getting a
                 match.
        """
        # TODO need a unit test for this method
        self.ser.send_and_wait_for_reply('DO,RB=TR')
        if self.controller_type == '32':
            self.wait_for_controller_after_reboot()


