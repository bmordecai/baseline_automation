import time
from datetime import datetime
from urlparse import urlparse
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.ser import Ser
__author__ = 'tige'


########################################################################################################################
# BaseManager CLASS
########################################################################################################################
class BaseManagerConnection(object):
    """
    BaseManager Object \n
    """
    # Serial Instance
    ser = Ser.serial_conn

    #############################
    def __init__(self, _ai='', _ua='FA', _ky='', _ss='', _url=''):
        """
        Initialize a BaseManger instance with the specified parameters \n

        :param _ai:     Alternate Server IP \n
        :type _ai:      str \n
        :param _ua:     Use Alternate Server IP \n
        :type _ua:      str \n
        :param _ky:     Registration Key \n
        :type _ky:      str \n
        :param _ss:     Status \n
        :type _ss:      str \n

        """

        # BaseManager Attributes
        self.ai = _ai           # Alternate Server IP
        self.ua = _ua           # Use Alternate Server IP
        self.ky = _ky           # Registration Key
        self.ss = _ss           # Status
        users_parsed_url_tuple = urlparse(_url)
        self.ad = users_parsed_url_tuple.netloc
        self.set_default_values()

    #################################
    def __str__(self):
        """
        Returns string representation of this BaseManger Object. \n
        :return:    String representation of this BaseManager Object
        :rtype:     str
        """
        return_str = "\n-----------------------------------------\n" \
                     "BaseManager Object:\n" \
                     "Alternate Server IP:      {0}\n" \
                     "Use Alternate Server IP:  {1}\n" \
                     "Current BM Server Address:{2}\n" \
                     "Status:                   {3}\n" \
                     "Registration Key:         {4}\n" \
                     "-----------------------------------------\n".format(
                         self.ai,       # {0}
                         self.ua,       # {1}
                         self.ad,       # {2}
                         self.ss,       # {3}
                         self.ky        # {4}
                     )
        return return_str

    #################################
    def set_default_values(self):
        """
        set the default values of BaseManager on the Controller
        :return:
        :rtype:
        """
        command = "SET,{0},{1}={2}".format(
            str(opcodes.basemanager),               # {0}
            str(opcodes.use_alternate_server_ip),   # {3}
            str(self.ua)                            # {4}
        )

        if self.ai:
            command += ",{0}={1}".format(opcodes.alternate_server_ip, self.ai)

        try:
            # Attempt to set BaseManger default values on the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set {0}'s 'Default values'".format(
                    str(opcodes.basemanager),               # {0}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0}'s 'Default values' to: \n {1} = {2}\n {3} = 4 \n ".format(
                str(opcodes.basemanager),               # {0}
                str(opcodes.alternate_server_ip),       # {1}
                str(self.ai),                           # {2}
                str(opcodes.use_alternate_server_ip),   # {3}
                str(self.ua)                            # {4}
            ))

    #############################
    def set_ai_for_cn(self, ip_address, unit_testing=False):
        """
        The 1000, 3200 act differently in this category. The 3200 tries to reconnect to BaseManager automatically,
        whereas the 1000 needs to be disconnected and reconnected manually.
        1.  Sends a disconnect command to the controller telling it to disconnect from BaseManager. \n
        2.  Set the controllers alternate ip address to the user's entered ip address and configure the controller to
            use alternate address. \n
        3.  Reconnect the controller to BaseManager. \n
        4.  Verify controller is connected to the new ip address by pinging BM. \n
        """
        # Todo: Check format for IP address

        start = datetime.now()  # a set timer will start time once the firefox web browser opens
        try:
            # The 3200 can switch BM pointers without sending the disconnect from BM command, but the 1000 needs to be
            # told to disconnect and reconnect
            self.ser.send_and_wait_for_reply('DO,BM=DC')
        except Exception:
            e_msg = " <- unable to send disconnect from BaseManager to controller: 'DO,BM=DC' -> "
            raise Exception(e_msg)

        # Build set message string with current users ip address pointer
        set_message = 'SET,BM,AI=' + ip_address + ',UA=TR'
        try:
            # Set controllers AI to point to correct IP specified by the user, ADDED: Cast to String to ensure string
            # object stays in tact.
            self.ser.send_and_wait_for_reply(str(set_message))
        except Exception:
            e_msg = " <- BC received after issuing command: " + set_message + " -> "
            raise Exception(e_msg)

        try:
            # Reconnect controller to BM
            self.ser.send_and_wait_for_reply('DO,BM=CN')
        except AssertionError:
            raise AssertionError(" <- unable to send connect to BaseManger from controller: 'DO,BM=CN' -> ")

        # We need to wait for the controller to connect to the server on the controller side
        # after sending the 'SET' and 'DO' command to the controller
        current_controller_status = ''
        expected_controller_status = 'CN'
        num_tries = 1
        while current_controller_status != expected_controller_status:
            # Added sleep timer to increase the time interval between sending "GET,BM" commands
            if unit_testing is False:
                time.sleep(2)
            try:
                controller_reply = self.ser.get_and_wait_for_reply("GET,BM")
                current_controller_status = controller_reply.get_value_string_by_key('SS')
            except AssertionError:
                raise AssertionError(" <- BC received after issuing command: 'GET,BM' -> ")
            # Allow method to fail after 30 attempts (effectively 1 minute duration timeout)
            if num_tries == 30:
                raise AssertionError("Unable to get connected status key value pair 'SS=CN' from controller. "
                                     "Check that the controller is connected to the network.\n")
            else:
                num_tries += 1
        try:
            # Verify connected with a ping
            self.ser.send_and_wait_for_reply('DO,BM=PN')
        except AssertionError:
            raise AssertionError(" <- unable to ping BaseManger from controller: 'DO,BM=PN' -> ")

        stop = datetime.now()
        output1 = stop - start
        msg = "\n - host: \t" + ip_address + \
              "\n - ip: \t\t" + ip_address + \
              "\n - total disconnect and reconnect time: \n\t\t[ " + str(output1) + " seconds ]"
        print "------------------------------------------------------"
        print " Verified controller is connected to: " + msg
        print "------------------------------------------------------"

    ############################
    def connect_cn_to_bm(self):
        """
        Connect the controller to Basemanager
        """
        print "Connect the controller to Basemanager"
        try:
            self.ser.send_and_wait_for_reply('DO,{0}={1}'.format(
                str(opcodes.basemanager),                   # {0}
                str(opcodes.connect_to_basemanager)         # {1}
            ))

            print "-------------------------------------------------------"
            print "       | - Successfully Connected the Controller to Basemanager.  - |       "
            print "-------------------------------------------------------"
        except Exception, e:
            e_msg = "Connect the controller to Basemanager Command Failed: " + e.message
            raise Exception(e_msg)

    #############################
    def disconnect_cn_to_bm(self):
        """
        Disconnect the controller from Basemanager
        """
        print "Disconnect the controller to Basemanager"
        try:
            self.ser.send_and_wait_for_reply('DO,{0}={1}').format(
                str(opcodes.basemanager),                       # {0}
                str(opcodes.disconnect_from_basemanager)        # {1}
            )
            print "-------------------------------------------------------"
            print "       | - Successfully Disconnected the Controller from Basemanager.  - |       "
            print "-------------------------------------------------------"
        except Exception, e:
            e_msg = "disconnect the controller to Basemanager Command Failed: " + e.message
            raise Exception(e_msg)

    #############################
    def ping_bm_from_cn(self):
        """
        Ping Basemanager from the controller
        """
        print "Ping Basemanager from the controller"
        try:
            self.ser.send_and_wait_for_reply('DO,{1}={2}').format(
                str(opcodes.basemanager),       # {0}
                str(opcodes.ping_server)        # {1}
            )
            print "-------------------------------------------------------"
            print "       | - Successfully Pinged Basemanager from the controller.  - |       "
            print "-------------------------------------------------------"
        except Exception, e:
            e_msg = "Pinging Basemanager from the controller Command Failed: " + e.message
            raise Exception(e_msg)

    #############################
    def verify_ip_address_state(self):
        """
        1.  Parse the url with build-in 'urlparse' python module . \n
        3.  Communicates with the controller and gets the current BaseManager settings from the controller. \n
        4.  Determine whether the controller is pointing to the same url provided in the user credentials. \n
        5.  If not pointing correctly, \n
                - Disconnects the controller from BM, verifies disconnected looking for a reply of SS=DC \n
                - Sends the correct ip address to the controller \n
                - Reconnects to BM looking for a SS=CN \n
                - Verifies connected by pinging BaseManager \n
        :return:
        :rtype:
        """
        # Verify that there is a connection to BaseManager before we check anything else
        # check for connection wait 1 second if than check again if not connected after 10 seconds than fail
        number_of_checks = 0
        while not self.verify_cn_connected_to_bm():
            time.sleep(1)
            number_of_checks += 1
            if number_of_checks == 10:
                e_msg = "Your controller is not connected to BaseManager! Please connect it."
                raise AttributeError(e_msg)

        dictionary_for_basemanager_address_pointers = {
            'p1.basemanager.net': '104.130.159.113',
            'p2.basemanager.net': '104.130.246.18',
            'p3.basemanager.net': '104.130.242.33',
            '104.130.159.113': '104.130.159.113',
            '104.130.246.18': '104.130.246.18',
            '104.130.242.33': '104.130.242.33'
        }

        # without a defined url scheme, a.k.a 'http'/'https', the url parser won't separate anything on the left of the
        # slash in 10.11.12.12/login.php?debug=true from the right. In other words, the url on the left would parse to:
        # bad_result: parsed_address = 10.11.12.13/login.php
        # intended_result: parsed_address= 10.11.12.13

        try:
            # Need to get the current controller BM configuration for verification.
            controller_reply = self.ser.get_and_wait_for_reply("GET,BM")
        except AssertionError as ae:
            exception_message = "Exception occurred after sending command to controller: 'GET,BM'.\n" + \
                                "Controller Response: [" + str(ae.message) + "]"
            raise AssertionError(exception_message)

        # For a 1000, the DNS Host Name is equivalent to the ip address value, so use a dictionary to convert any p1,
        #  p2 and p3 url address' to their equivalent ip address values if possible.
        try:
            current_users_bm_ip_address = dictionary_for_basemanager_address_pointers[self.ad]
        except KeyError:
            # User is using a local address, so use the local address for comparisons
            current_users_bm_ip_address = self.ad

        # Determine if the controller is set up to use an alternate address, TR (true)/FA (false). If the user is trying
        # to switch ip address pointer of the controller, this value must be true.
        controllers_configured_to_use_ai_address = controller_reply.get_value_string_by_key('UA')

        # If we are a 1000, always force this condition
        if controllers_configured_to_use_ai_address == 'TR' or Ser.controller_type == "10":
            # Controller is configured to use an alternate ip address for BaseManager, so get the controllers
            # configured alternate ip address for comparison
            controllers_configured_ai_key_value = controller_reply.get_value_string_by_key('AI')

            # If they aren't pointing to the same thing, we want to disconnect and re-point them
            if controllers_configured_ai_key_value != current_users_bm_ip_address:
                self.set_ai_for_cn(ip_address=current_users_bm_ip_address)

        # Else, the controller is configured as, BM,UA=FA, so check the pointer value for key value 'AD' because 'AI' is
        # being ignored.
        else:
            current_users_bm_dns_address = self.ad
            controllers_configured_ad_key_value = controller_reply.get_value_string_by_key('AD')

            # TODO: This if statement works for a 3200, but for the 1000, the controller's 'AD' value is an IP address,
            # TODO: not a DNS host name, thus, we need to fix this if statement to handle both cases,
            # TODO: it works regardless.
            # If they aren't pointing to the same thing, we want to disconnect and re-point them
            if controllers_configured_ad_key_value != current_users_bm_dns_address:
                self.set_ai_for_cn(ip_address=current_users_bm_ip_address)

    def verify_cn_connected_to_bm(self):
        try:
            # Need to get the current controller BM configuration for verification.
            controller_reply = self.ser.get_and_wait_for_reply("GET,BM")
        except AssertionError as ae:
            exception_message = "Exception occurred after sending command to controller: 'GET,BM'.\n" + \
                                "Controller Response: [" + str(ae.message) + "]"
            raise AssertionError(exception_message)

        # Get the connected status
        status_from_cn = controller_reply.get_value_string_by_key(opcodes.status_code)

        # are we connected?
        if status_from_cn != opcodes.connect_to_basemanager:
            return False
        else:
            print "|- Contoller is connected to Basemanager.  - |"
            return True

    def wait_for_bm_connection(self, unit_testing=False):
        number_of_retries = 1

        while not self.verify_cn_connected_to_bm():

            if number_of_retries == 6:  # this makes a 3 minute max time
                e_msg = "Connection to Basemanager failed: "
                raise Exception(e_msg)

            else:
                if not unit_testing:
                    time.sleep(15)
                    number_of_retries += 1
