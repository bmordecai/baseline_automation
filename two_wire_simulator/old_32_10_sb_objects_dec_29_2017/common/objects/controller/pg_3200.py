import re
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.programs import Programs
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import mainlines32

__author__ = 'baseline'


########################################################################################################################
# 3200 Program Class
########################################################################################################################
class PG3200(Programs):
    """
    3200 Program Object \n
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    mainline_objects = None

    # obj_type = opcodes.program + "32"

    #################################
    def __init__(self, _ad, _en=None, _ww=None, _mc=None, _sa=None, _ci=None, _di=None, _wd=None, _sm=None, _st='',
                 _pr=None, _ml=None, _bp=None, _wr=None):
        """
        :param _ad:     Program number \n
        :type _ad:      int \n

        :param _en:     The enabled state for the program. Enabled = 'TR', Disabled = 'FA'
        :type _en:      str \n

        :param _ww:     Water window for the program. Two cases accepted.\n
                        - Weekly:   A list with a single element is passed in, the single element having 24 0's or 1's
                                    in a string where 1=On, 0=Off, i.e: ['111111111111111111111111']
                        - Daily:    A list with seven elements, 1 for each day of the week starting with sunday,
                                    with the same format as above, \n
                                    i.e: ['111111111111111111111111', '111111111111111111111111',   \n
                                          '111111111111111111111111', '111111111111111111111111',   \n
                                          '111111111111111111111111', '111111111111111111111111',   \n
                                          '111111111111111111111111'] \n
        :type _ww:      list[str] \n

        :param _mc:     Max number of concurrent zones for programming \n
        :type _mc:      int \n

        :param _sa:     Seasonal adjust for programming with an integer representing a percentage, i.e: 100 = 100% \n
        :type _sa:      int \n

        :param _ci:     Calendar Interval ('EV', 'OD', 'OS', 'ET', 'ID', 'WD') \n
        :type _ci:      str \n

        :param _di:     Day Interval \n
        :type _di:      int, None \n

        :param _wd:     Week Days in a list of seven 0's and 1's (a value for each day of the week) starting with
                        sunday. A 1=On(water), 0=Off(don't water) \n
                        i.e: [0, 1, 1, 1, 1, 1, 0]' says don't water on the weekend (Saturday, Sunday are 0's) \n
        :type _wd:      list[int] \n

        :param _sm:     Semi Month interval as a list of 24 comma separated integers. \n
                        i.e: [1, 2, 4, 19, 1, 0, 4, 30, 22, 1, 15, 8, 13, 19, 28, 20, 1, 1, 3, 2, 21, 2, 13, 24] \n
        :type _sm:      list[int] \n

        :param _st:     Start Times as a list of 1 to 8 integers representing minutes past midnight \n
                        i.e: [480] - 1 start time of 8am, [480, 540, 600] - 3 start times of 8am, 9am and 10am \n
        :type _st:      list[int] \n
        :type _st:      Optional list[str] \n

        :param _pr:     Priority Level (1=high | 2=medium | 3=low) \n
        :type _pr:      int \n

        :param _ml:     Mainline for program (1 through 8) \n
        :type _ml:      int \n

        :param _bp:     Booster pump for program (address of a MV) \n
        :type _bp:      int | str \n

        :param _wr:     Water ration % (0-100%) \n
        :type _wr:      int \n
        """

        self.mainline_objects = mainlines32

        description = "Test Program {0}".format(str(_ad))

        # Init parent class
        Programs.__init__(self, _ds=description, _ad=_ad, _en=_en, _ww=_ww, _mc=_mc, _sa=_sa)

        # ----- Instance Variables ----- #

        # Calendar Interval
        if _ci:
            self.ci = _ci
        else:
            # default
            self.ci = 'WD'

        # Day Interval
        if _di:
            self.di = _di
        else:
            # default
            self.di = 1

        # Week Days (in binary, 1's and 0's)
        if _wd:
            self.wd = self.verify_valid_week_day_list(_week_day_list=_wd)
        else:
            # default
            self.wd = [
                1,          # Sunday
                1,          # Monday
                1,          # Tuesday
                1,          # Wednesday
                1,          # Thursday
                1,          # Friday
                1           # Saturday
            ]

        # Semi-Monthly Watering (opcode: CI='ET')
        if _sm:
            self.sm = self.verify_valid_semi_month_interval_list(_semi_month_interval_list=_sm)
        else:
            # default
            self.sm = [
                1, 1,       # January 1-15, January 16-31
                1, 1,       # February 1-15, February 16-End of month
                1, 1,       # March 1-15, March 16-31
                1, 1,       # April 1-15, April 16-30
                1, 1,       # May 1-15, May 16-31
                1, 1,       # June 1-15, June 16-30
                1, 1,       # July 1-15, July 16-31
                1, 1,       # August 1-15, August 16-31
                1, 1,       # September 1-15, September 16-30
                1, 1,       # October 1-15, October 16-31
                1, 1,       # November 1-15, November 16-30
                1, 1        # December 1-15, December 16-31
            ]

        # Start times (minutes past midnight)
        if _st:
            self.st = self.verify_valid_start_times(_list_of_times=_st)
            pass
        else:
            # default
            self.st = [
                480
            ]

        # Priority Level
        if _pr:
            self.pr = self.verify_valid_priority_level(_priority=_pr)
        else:
            # default
            self.pr = 2                 # Priority

        # Mainline Assignment
        if _ml:
            self.ml = self.verify_valid_mainline(_mainline=_ml)
        else:
            # default
            self.ml = 1                 # Mainline Assignment

        # Booster Pump
        if _bp:
            self.bp = self.verify_valid_booster_pump(_booster=_bp)
        else:
            # default
            # TODO: booster pump should default to 'FA', but the 3200 doesn't currently accept an 'FA' value for 'SET'
            # TODO: command.
            self.bp = ''              # Booster Pump (Serial number of MV | FA)

        # Water Ration Percentage
        if _wr:
            self.wr = self.verify_valid_water_ration(_water_ration=_wr)
        else:
            self.wr = 20    # Arbitrary value between 0-100

        self.calendar_interval_dict = {
            'EV': None,
            'OD': None,
            'OS': None,
            'ET': self.sm,
            'ID': self.di,
            'WD': self.wd
        }
        self.send_programming_to_cn()

    #################################
    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    #################################
    def send_programming_to_cn(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        # Water Window Case
        if len(self.ww) == 1:
            # A len(self.ww) == 1 says we are a weekly water window with a single value
            water_window_weekly_str = "WW={0}".format(
                str(self.ww[0])
            )
        else:
            water_window_weekly_str = "W1={0},W2={1},W3={2},W4={3},W5={4},W6={5},W7={6}".format(
                self.ww[0],     # {0} - Sunday
                self.ww[1],     # {1} - Monday
                self.ww[2],     # {2} - Tuesday
                self.ww[3],     # {3} - Wednesday
                self.ww[4],     # {4} - Thursday
                self.ww[5],     # {5} - Friday
                self.ww[6]      # {6} - Saturday
            )

        # Calendar Interval Case
        if self.ci == 'ET':
            calendar_interval_str = ",SM={0}".format(
                self.build_semi_month_interval_string(_sm_list=self.sm)     # {0}
            )
        elif self.ci == 'ID':
            calendar_interval_str = ",DI={0}".format(
                str(self.di)    # {0}
            )
        elif self.ci == 'WD':
            calendar_interval_str = ",WD={0}".format(
                self.build_week_day_string(self.wd)
            )
        else:
            calendar_interval_str = ""

        command = "SET,PG={0},EN={1},DS={2},{3},MC={4},SA={5},CI={6}{7},ST={8},PR={9},ML={10}".format(
            str(self.ad),                                       # {0}
            str(self.en),                                       # {1}
            str(self.ds),                                       # {2}
            water_window_weekly_str,                            # {3}
            str(self.mc),                                       # {4}
            str(self.sa),                                       # {5}
            str(self.ci),                                       # {6}
            calendar_interval_str,                              # {7}
            self.build_start_time_string(_st_list=self.st),     # {8}
            str(self.pr),                                       # {9}
            str(self.ml)                                        # {10}
        )
        # TODO this will not allow a blank string
        if self.bp != '':
            # this appends bp to the end of the command string because you cant send the controller a BP= with
            # equaling something
            _bp_str = ",{0}={1}".format(
                opcodes.booster_pump,   # {0}
                self.bp                 # {1}
            )

            command += _bp_str

        try:
            # Attempt to set program default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Values' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                command,        # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0}'s 'Values' to: {1}".format(
                  str(self.ad),     # {0}
                  command           # {1}
                  ))

    #################################
    def set_calendar_interval_on_cn(self, _ci=None, _di=None, _sm=None, _wd=None):
        """
        Sets the calendar interval for the program on the controller. \n

        :param _ci:     Calendar Interval, accepted values are: 'EV', 'OD', 'OS', 'ET', 'ID' or 'WD'.\n
        :type _ci:      str \n

        :param _di:     Number of days for regular intervals \n
        :type _di:      int \n

        :param _sm:     Semi month interval days \n
        :type _sm:      list[int] \n

        :param _wd:     Week Days to water (Sunday - Saturday). \n
                        String of 7 integers, only 0's and 1's, with sunday being the value of the first integer \n
        :type _wd:      list[int] \n
        """
        # If user wants to overwrite current calendar interval
        if _ci is not None:

            # if _ci not in ['EV', 'OD', 'OS', 'ET', 'ID', 'WD']:
            if _ci not in self.calendar_interval_dict:
                e_msg = "Invalid calendar interval type specified for Program {0}. Expected: ('EV','OD','OS','ET'," \
                        "'ID','WD'), but received: {1}".format(
                            str(self.ad),   # {0}
                            _ci             # {1}
                        )
                raise ValueError(e_msg)
            else:
                # Valid calendar interval entered, overwrite current value
                self.ci = _ci

        # -------------------------------------------------------------------------------------------------------------
        # case 1: the historical et calendar setting
        if self.ci == 'ET':
            if _sm is not None:
                # Make sure _sm is a valid list
                self.sm = self.verify_valid_semi_month_interval_list(_semi_month_interval_list=_sm)

            value_for_key = self.build_semi_month_interval_string(_sm_list=self.sm)

            # create command string
            command = "SET,PG={0},CI={1},SM={2}".format(
                str(self.ad),   # {0}
                self.ci,        # {1}
                value_for_key   # {2}
            )

        # -------------------------------------------------------------------------------------------------------------
        # case 2: daily interval
        elif self.ci == 'ID':
            if _di is not None:
                # make sure di is a valid interval
                self.di = _di

            # create command string
            command = "SET,PG={0},CI={1},DI={2}".format(
                str(self.ad),   # {0}
                self.ci,        # {1}
                self.di         # {2}
            )

        # -------------------------------------------------------------------------------------------------------------
        # case 3: week day water schedule selected
        elif self.ci == 'WD':
            if _wd is not None:
                # make sure week day list is a valid list
                self.wd = self.verify_valid_week_day_list(_week_day_list=_wd)

            # Build the string for sending
            value_for_key = self.build_week_day_string(_wd_list=self.wd)

            # create command string
            command = "SET,PG={0},CI={1},WD={2}".format(
                str(self.ad),   # {0}
                self.ci,        # {1}
                value_for_key   # {2}
            )

        # -------------------------------------------------------------------------------------------------------------
        # default case: Even, Odd or Odd Skip 31 water schedule specified
        else:
            # Command for calendar interval not requiring additional settings. i.e: EV, OD, OS
            command = "SET,PG={0},CI={1}".format(str(self.ad), self.ci)

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Calendar Interval' to: {1} -> {2}".format(
                str(self.ad),   # {0}
                command,        # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Calendar Interval' for Program: {0} to: {1}".format(
                str(self.ad),   # {0}
                command         # {1}
            ))

    #################################
    def set_start_times_on_cn(self, _st_list=None):
        """
        Sets the start times for the 3200 program on the controller. \n
        :param _st_list:    List of up to 8 integers representing the time in minutes past midnight \n
        :type _st_list:     list[int] \n
        :return:
        """
        # check to see if a start time list is passed in to overwrite current start time list
        if _st_list is not None:
            self.st = self.verify_valid_start_times(_list_of_times=_st_list)

        # Build start time string for sending to controller
        built_st_string = self.build_start_time_string(_st_list=self.st)

        # Command for sending to controller
        command = "SET,PG={0},ST={1}".format(
            str(self.ad),       # {0}
            built_st_string     # {1}
        )

        # Attempt to send command to controller
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Start Times' to: {1} -> {2}".format(
                str(self.ad),       # {0}
                built_st_string,    # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Start Times' for Program: {0} to: {1}".format(
                  str(self.ad),     # {0}
                  built_st_string   # {1}
                  ))

    #################################
    def set_priority_level_on_cn(self, _pr_level=None):
        """
        Sets the water priority for the current program on the controller using the serial object. \n
        :param _pr_level:   Water priority level to overwrite current instance value. \n
        :type _pr_level:    int \n
        """
        # Check for overwrite
        if _pr_level is not None:
            self.pr = self.verify_valid_priority_level(_priority=_pr_level)
            
        command = "SET,PG={0},PR={1}".format(str(self.ad), str(self.pr))

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Priority Level' to: {1} -> {2}".format(
                str(self.ad),   # {0}
                str(self.pr),   # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Priority Level' for Program: {0} to: {1}".format(
                str(self.ad),   # {0}
                str(self.pr)    # {1}
            ))

    #################################
    def set_mainline_on_cn(self, _ml_num=None):
        """
        Sets the mainline for the program to the specified mainline passed in or to it's default mainline, 1. \n
        :param _ml_num:     Address of mainline to assign to the program \n
        :type _ml_num:      int \n
        """
        # Check for overwrite
        if _ml_num is not None:
            _ml_num = self.verify_valid_mainline(_ml_num)
            self.ml = self.mainline_objects[_ml_num].ad

        command = "SET,PG={0},ML={1}".format(str(self.ad), str(self.ml))

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Mainline' to: {1} -> {2}".format(
                str(self.ad),   # {0}
                str(self.ml),   # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Mainline' for Program: {0} to: {1}".format(
                str(self.ad),   # {0}
                str(self.ml)    # {1}
            ))

    #################################
    def set_booster_pump_on_cn(self, _mv_address):
        """
        Sets the booster pump for the program to the master valve based on the address of the master valve passed in
        as an argument on the controller. \n
        :param _mv_address:  address of a master valve to assign to program as booster pump. \n
        :type _mv_address:   int \n
        """
        # Check for overwrite
        self.bp = self.verify_valid_booster_pump(_booster=_mv_address)
        bp_obj = self.mv_objects[self.bp]

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,
            self.ad,
            opcodes.booster_pump,
            bp_obj.sn
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Booster Pump' to: {1}({2}) -> {3}".format(
                str(self.ad),   # {0}
                bp_obj.sn,      # {1}
                self.bp,        # {2}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Booster Pump' for Program: {0} to: {1}({2})".format(
                str(self.ad),   # {0}
                bp_obj.sn,      # {1}
                self.bp         # {2}
            ))

    #################################
    def verify_start_times_on_cn(self):
        """
        Verifies the list of start times returned from the controller. \n
        """
        # When you pass the controller a [''] it sets the controller to not have a start time
        list_of_start_times_from_cn = ['']
        start_times = self.data.get_value_string_by_key(opcodes.start_times)
        if start_times:
            list_of_start_times_from_cn = map(int, start_times.split('='))

        # Compare versus what is on the controller
        if self.st != list_of_start_times_from_cn:
            e_msg = "Unable to verify 3200 Program {0}'s 'Start Times'. Received: {1}, Expected: {2}".format(
                str(self.ad),                       # {0}
                str(list_of_start_times_from_cn),   # {1}
                str(self.st)                        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 3200 Program {0}'s 'Start Times': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.st))   # {1}
            )

    #################################
    def verify_calendar_interval_on_cn(self, unit_test_value=1):
        """
        Verifies the calendar interval for the instance against what the controller has programed. \n
        """

        # Case 1: Historical ET Calendar, compare Semi-Month list values
        if self.ci == opcodes.historical_calendar:
            # Two things happen,
            # First, self.data.get_key_value_by_key() returns a string with integers separated by '='
            # Second, ".split('=')" creates a list from the string returned with each list element being type 'str'
            # Last, use 'map(int, blah)' to cast all list elements to the 'int' type.
            # This allows us to compare the 'self.sm' list of 'int' types against the result of this string,
            # lists should be equal
            key = opcodes.program_semi_month_interval
            expected_value = self.sm
            if unit_test_value is not 1:
                received_value = unit_test_value
            else:
                received_value = map(int, self.data.get_value_string_by_key(
                    opcodes.program_semi_month_interval).split("="))

        # Case 2: Week days, compare Weekday list values
        elif self.ci == opcodes.week_days:
            # Get the expected weekday list for comparison
            expected_value = self.wd

            # First, the output of 'self.data.get_key_value_by_key('WD')' is changed into list form by the 'list()' cast
            # Next, map(int, blah) converts all the list elements from strings to ints
            key = opcodes.week_days
            received_value = map(int, list(self.data.get_value_string_by_key(opcodes.week_days)))

        # Case 3: Day Interval, compare day interval values
        elif self.ci == opcodes.interval_days:
            # Get the expected Daily interval value from the local object
            expected_value = self.di

            # Get the Daily interval value from the data returned from the controller
            key = opcodes.day_interval
            received_value = int(self.data.get_value_string_by_key(opcodes.day_interval))

        # Case 4: Even, Odd or Odd Skip 31, verify
        else:
            # Get expected 'Even', 'Odd' or 'Odd Skip 31' calendar interval value
            expected_value = self.ci

            # Get the value for calendar interval set at the controller
            key = opcodes.calendar_interval
            received_value = self.data.get_value_string_by_key(opcodes.calendar_interval)

        # If received_value is None, then the key was not found in the data send by the controller located in self.data
        # Thus, the program must not have the expected programming.
        if received_value is None:
            e_msg = "Unable to verify 'Calendar Interval' type: {0} for current program. Unable to find a value " \
                    "associated with key: '{1}'. Current program doesn't have the expected 'Calendar Interval'".format(
                        self.ci,    # {0}
                        key         # {1}
                    )
            raise ValueError(e_msg)

        # Compare values
        elif expected_value != received_value:
            e_msg = "Unable to verify 'Calendar Interval' type: {0}, for current program. Expected:"\
                    "(Key: {1}, Value: {2}), Received: (Key: {3}, Value: {4})".format(
                        self.ci,            # {0}
                        key,                # {1}
                        expected_value,     # {2}
                        key,                # {3}
                        received_value      # {4}
                    )
            raise ValueError(e_msg)

        # Successful verification
        else:
            print("Verified 3200 Program {0}'s 'Calendar Interval': Type: '{1}', Value: {2}, on controller".format(
                  str(self.ad),   # {0}
                  self.ci,        # {1}
                  key             # {2}
                  ))

    #################################
    def verify_priority_level_on_cn(self):
        """
        Verifies the Priority Level set for the 3200 program on the controller. \n
        """
        priority_level_from_cn = int(self.data.get_value_string_by_key(opcodes.priority))

        # Compare versus what is on the controller
        if self.pr != priority_level_from_cn:
            e_msg = "Unable to verify 3200 Program {0}'s 'Priority Level'. Received: {1}, Expected: {2}".format(
                    str(self.ad),                   # {0}
                    str(priority_level_from_cn),    # {1}
                    str(self.pr)                    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 3200 Program {0}'s 'Priority Level': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.pr))   # {1}
            )

    #################################
    def verify_mainline_on_cn(self):
        """
        Verifies the Mainline set for the 3200 program on the controller. \n
        """
        # This returns a mainline address
        mainline = int(self.data.get_value_string_by_key(opcodes.mainline))

        # Get the object
        pg3200_current_mainline_obj = self.mainline_objects[self.ml]

        # Compare versus what is on the controller
        if pg3200_current_mainline_obj.ad != mainline:
            e_msg = "Unable to verify 3200 Program {0}'s 'Mainline'. Received: {1}, Expected: {2}".format(
                    str(self.ad),   # {0}
                    str(mainline),  # {1}
                    str(self.ml)    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 3200 Program {0}'s 'Mainline': '{1}' on controller".format(
                self.ad,
                pg3200_current_mainline_obj.ad
            ))

    #################################
    def verify_booster_pump_on_cn(self):
        """
        Verifies the Booster Pump set for the 3200 program on the controller. \n
        """
        booster_pump = self.data.get_value_string_by_key(opcodes.booster_pump)

        # TODO...
        # We cannot set a booster pump value of 'FA' currently, thus we have to leave our object's attribute as ''
        # instead of 'FA'. When the controller sends back information from the self.get_data() call, the controller
        # responds with 'BP=FA' when no booster pump is assigned. Thus this is a temp fix.
        if self.bp == '':
            curr_bp_obj_sn = opcodes.false

        else:
            curr_bp_object = self.mv_objects[self.bp]
            curr_bp_obj_sn = curr_bp_object.sn

        # Compare versus what is on the controller
        if curr_bp_obj_sn != booster_pump:
            e_msg = "Unable to verify 3200 Program {0}'s 'Booster Pump'. Received: {1}, Expected: {2}".format(
                    str(self.ad),       # {0}
                    str(booster_pump),  # {1}
                    curr_bp_obj_sn      # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 3200 Program {0}'s 'Booster Pump': '{1}' on controller".format(
                str(self.ad),   # {0}
                curr_bp_obj_sn  # {1}
            ))

    #################################
    def verify_valid_semi_month_interval_list(self, _semi_month_interval_list):
        """
        Verifies a valid semi month interval list of 24 integers, two for each month. \n
        :param _semi_month_interval_list:   A list of 24 integers. \n
        :type _semi_month_interval_list:        list[int]  \n
        :return:                            A valid semi-month interval list of 24 integers
        :rtype:                                 list[int]  \n
        """
        # Check for proper list length
        if len(_semi_month_interval_list) != 24:
            e_msg = "Invalid semi month interval argument passed in. List must contain 24 comma separated integers, " \
                    "instead found: {0}".format(
                        str(len(_semi_month_interval_list))  # {0}
                    )
            raise ValueError(e_msg)

        # Else statement represents valid
        else:
            return _semi_month_interval_list

    #################################
    def verify_valid_week_day_list(self, _week_day_list):
        """
        Verifies a valid week day list of integers for setting the calendar interval. \n
        :param _week_day_list:  A list of 7 integers, the first being Sunday, second being Monday and so on. \n
        :type _week_day_list:       list[int] \n
        :return:                A valid list of week day integers for program \n
        :rtype:                     list[int] \n
        """
        # Check for proper list length
        if len(_week_day_list) != 7:
            e_msg = "Invalid week day water schedule argument passed in. List must contain 7 comma separated " \
                    "integers, instead found: {0}".format(
                        str(len(_week_day_list))  # {0}
                    )
            raise ValueError(e_msg)

        # Check all list elements to make sure they are only 0's and 1's using a regular expression. If a non '0' or
        # '1' is found, then an exception raised.
        elif any(re.search(r'\D|[2-9]+|[0-9][0-9]+', str(each_day)) for each_day in _week_day_list):
            e_msg = "Invalid list of week day values for Program {0}. Expects only 1's and/or 0's".format(str(self.ad))
            raise ValueError(e_msg)

        # Valid week day list
        else:
            return _week_day_list
    #
    # #################################
    # def verify_valid_day_interval(self, _day_interval):
    #     """
    #     Verifies a valid day interval value entered for a program. \n
    #     :param _day_interval:   Day interval for program to water \n
    #     :type _day_interval:        Integer \n
    #     :return:                A valid day interval value for program \n
    #     :rtype:                     Integer \n
    #     """
    #     # Check that the interval passed in is an integer
    #     if not isinstance(_day_interval, int):
    #         e_msg = "Invalid interval argument ('_day_interval') type for Program {0}. Expected type: int, " \
    #                 "Received type: {1}".format(
    #                     str(self.ad),  # {0}
    #                     type(_day_interval)  # {1}
    #                 )
    #         raise TypeError(e_msg)
    #     else:
    #         return _day_interval

    #################################
    def verify_valid_start_times(self, _list_of_times):
        """
        Verifies a valid list of start times. \n
        :param _list_of_times:      List of up to 8 start time integers \n
        :type _list_of_times:       list[int]  \n
        :type _list_of_times:       Optional list[str]  \n
        :return:                    A valid list of up to 8 start times as integers. \n
        :rtype:                     list[int]  \n
        """
        # Check for correct number of start times
        if _list_of_times != '':
            if len(_list_of_times) == 0 or len(_list_of_times) > 8:
                e_msg = "Invalid number of start times to set for Program {0}. Expects 1 to 8 start times, received: " \
                        "{1}".format(
                            str(self.ad),               # {0}
                            str(len(_list_of_times))    # {1}
                        )
                raise ValueError(e_msg)

            else:
                # Valid list of start times, return
                return _list_of_times

    #################################
    def verify_valid_booster_pump(self, _booster):
        """
        Verifies a valid booster pump address by checking that it is in the dictionary. \n
        :param _booster:    A master valve serial number or 'FA'. \n
        :type _booster:     int | str\n
        :return:            A valid booster pump value. \n
        :rtype:             int \n
        """
        # Check valid type
        if int(_booster) not in self.mv_objects:
            e_msg = "Invalid Booster Pump address for 3200 Program. Valid address are: {bp_address}".format(
                bp_address=self.mv_objects.keys()
            )
            raise IndexError(e_msg)

        # Valid booster pump
        else:
            return int(_booster)

    #################################
    def verify_valid_priority_level(self, _priority):
        """
        Verifies a valid priority level for program. \n
        :param _priority:   Priority level \n
        :type _priority:    Integer \n
        :return:            A valid priority level \n
        :rtype:             Integer \n
        """
        # Check valid value
        if _priority not in [1, 2, 3]:
            e_msg = "Invalid '_priority' (Priority Level) value for 'Set' function for Program {0}: {1}."\
                    " Accepted values are: 1=high | 2=medium | 3=low".format(
                        str(self.ad),       # {0}
                        str(_priority)      # {1}
                    )
            raise ValueError(e_msg)

        # Overwrite current value
        else:
            return _priority

    #################################
    def verify_valid_mainline(self, _mainline):
        """
        Verifies the mainline format expected.
        :param _mainline: A mainline to validate. \n
        :type _mainline: int \n
        :return: A valid mainline address \n
        :rtype: int \n
        """
        if _mainline not in self.mainline_objects:
            e_msg = "Invalid Mainline address for 3200 Program. Valid address are: {ml_address}".format(
                ml_address=self.mainline_objects.keys()
            )
            raise IndexError(e_msg)

        # Valid _mainline
        else:
            return _mainline

    #################################
    def verify_valid_water_ration(self, _water_ration):
        """
        Verifies the water ration expected.
        :param _water_ration: A water ration to validate. \n
        :type _water_ration: int \n
        :return: A valid water ration \n
        :rtype: int \n
        """
        if _water_ration not in range(100):
            e_msg = "Invalid water ration percentage, the variable must be between 0 and 100."
            raise IndexError(e_msg)

        # Valid _mainline
        else:
            return _water_ration

    #################################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this program's values against what the controller has. \n
        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()

        self.verify_description_on_cn()
        self.verify_enabled_state_on_cn()
        self.verify_water_window_on_cn()
        self.verify_max_concurrent_zones_on_cn()
        self.verify_seasonal_adjust_on_cn()

        if expected_status is not None:
            self.verify_status_on_cn(_expected_status=expected_status)

        self.verify_calendar_interval_on_cn()
        self.verify_start_times_on_cn()
        self.verify_priority_level_on_cn()
        self.verify_mainline_on_cn()
        self.verify_booster_pump_on_cn()

    #################################
    def build_start_time_string(self, _st_list):
        """
        Builds the correct start time string in correct format for sending to the controller. \n
        :param _st_list:    List of start times as integers for sending to the controller. \n
        :type _st_list:     list[int] \n
        :type _st_list:     Optional list[str] \n
        :return:            Returns a string of start times for sending to the controller. \n
        :rtype:             str \n
        """
        string_for_return = ''

        # Iterate through each start time
        for index, each_time in enumerate(_st_list):
            # Don't want to append an equal sign at the front of this string
            if index == 0:
                string_for_return += str(each_time)
            else:
                # adding/appending '=400' to the string for example
                string_for_return += ("=" + str(each_time))

        # Example output for 3 start times: '480=720=900'
        return string_for_return

    #################################
    def build_week_day_string(self, _wd_list):
        """
        Builds the week day string for sending to controller in correct format. \n
        :param _wd_list:    List containing 7 values (1 for each day of the week), first value being sunday \n
        :type _wd_list:     list[int]  \n
        :return:            Returns properly formatted string for week day water schedule. \n
        :rtype:             String \n
        """
        formatted_string = ''

        # Loop through the list of integers
        for each_int in _wd_list:
            formatted_string += str(each_int)

        # expected returned string format example: '1100111'
        return formatted_string

    #################################
    def build_semi_month_interval_string(self, _sm_list):
        """
        Builds the semi month interval string for sending to controller in the correct format. \n
        :param _sm_list:    List of 24 integers to set for semi month interval. \n
        :type _sm_list:     list[int] \n
        :return:            Returns properly formatted string for sending to controller. \n
        :rtype:             String \n
        """
        formatted_string = ''

        # Loop through the list of integers
        for index, each_int in enumerate(_sm_list):

            # Don't want to append an equal sign before the first integer in the string
            if index == 0:
                formatted_string += str(each_int)
            else:
                formatted_string += "=" + str(each_int)

        # expected returned string format example: '1=1=9=1=29=1=3=2=4=1=1=1=1=2=3=4=2=1=1=9=15=19=1=1'
        return formatted_string
