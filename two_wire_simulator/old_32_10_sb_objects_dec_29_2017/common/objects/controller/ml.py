import status_parser
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.ser import Ser
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages
from datetime import datetime, timedelta

__author__ = 'baseline'


class Mainline(object):
    """
    Mainline Object. \n
    """
    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    # Serial Instance
    ser = Ser.serial_conn

    # Browser Instance
    browser = ''

    # Controller type for controller specific methods across all devices
    controller_type = ''

    #################################
    def __init__(self, _ad, _ds=None, _ft=2, _fl=0.0, _lc='FA', _hv=0, _hs='FA', _lv=0, _ls='FA'):
        """
        Mainline constructor. Accepts no arguments to create a default mainline with default values. \n

        :param _ad:     Mainline address (number) \n
        :type _ad:      int \n

        :param _ds:     Description \n
        :type _ds:      str \n

        :param _ft:     Pipe fill time: minutes \n
        :type _ft:      int \n

        :param _fl:     Target Flow: gallons per minute \n
        :type _fl:      int, float \n

        :param _lc:     Limit zones by flow: 'TR' | 'FA' \n
        :type _lc:      str \n

        :param _hv:     High variance limit: percent \n
        :type _hv:      int \n

        :param _hs:     High variance shut down: 'TR' | 'FA' \n
        :type _hs:      str \n

        :param _lv:     Low variance limit: percent \n
        :type _lv:      int \n

        :param _ls:     Low variance shut down: 'TR' | 'FA' \n
        :type _ls:      str \n
        """

        # Address
        self.ad = _ad

        # Description
        if _ds:
            self.ds = _ds
        else:
            self.ds = "Test Mainline {0}".format(str(_ad))

        # Pipe fill time
        self.ft = _ft

        # Target flow
        self.fl = _fl

        # Limit zones by flow
        self.lc = _lc

        # High variance limit
        self.hv = _hv

        # High variance shut down
        self.hs = _hs

        # Low variance limit
        self.lv = _lv

        # Low variance shutdown
        self.ls = _ls

        # Data place holder
        self.data = status_parser.KeyValues('')

        # Send initial starting state to controller
        self.send_programming_to_cn()

        # messages
        self.build_message_string = ''  # place to store the message that is compared to the controller message

    #################################
    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    #################################
    def convert_fill_time_minutes_to_seconds(self, fill_time):
        """
        Converts the fill time from minutes to total seconds.
        :param fill_time:   Fill time in minutes to convert to seconds. \n
        :type fill_time:    int \n
        """
        return fill_time * 60

    #################################
    def send_programming_to_cn(self):
        """
        Sends the current mainline instance programming to the controller. \n
        """
        # convert from minutes to seconds for set
        converted_fill_time = self.convert_fill_time_minutes_to_seconds(fill_time=self.ft)

        command = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13},{14}={15},{16}={17}".format(
            opcodes.mainline,                   # {0}
            str(self.ad),                       # {1}
            opcodes.description,                # {2}
            self.ds,                            # {3}
            opcodes.fill_time,                  # {4}
            str(converted_fill_time),           # {5}
            opcodes.target_flow,                # {6}
            str(self.fl),                       # {7}
            opcodes.limit_zones_by_flow,        # {8}
            self.lc,                            # {9}
            opcodes.high_variance_limit,        # {10}
            str(self.hv),                       # {11}
            opcodes.high_flow_variance_shutdown,    # {12}
            self.hs,                            # {13}
            opcodes.low_variance_limit,         # {14}
            str(self.lv),                       # {15}
            opcodes.low_variance_shut_down,     # {16}
            self.ls                             # {17}
        )
        try:
            # Attempt to set program default values at the controller in faux io
            self.ser.send_and_wait_for_reply(command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Default Values' to: '{1}' -> {2}".format(
                    str(self.ad),   # {0}
                    command,        # {1}
                    e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Default Values' to: {1}".format(
                str(self.ad),       # {0}
                command,            # {1}
            ))

    #################################
    def set_description_on_cn(self, _new_description):
        """
        Sets the 'Description' for this Mainline on the controller to the description passed as an argument. \n

        :param _new_description:    New 'Description' for Mainline \n
        :type _new_description:     str \n
        """

        # Assign new description to object attribute
        self.ds = _new_description
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.mainline,       # {0}
            str(self.ad),           # {1}
            opcodes.description,    # {2}
            self.ds                 # {3}
        )
        try:
            # Attempt to set program default values at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Description' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                self.ds,        # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Description' to: {1}".format(
                str(self.ad),   # {0}
                self.ds,        # {1}
            ))
            
    #################################
    def set_pipe_fill_time_on_cn(self, _new_target_flow):
        """
        Sets the 'Pipe Fill Time' for this Mainline on the controller. \n

        :param _new_target_flow:    New 'Pipe Fill Time' for Mainline (minutes) \n
        :type _new_target_flow:     int \n
        """

        # Assign new Pipe Fill Time to object attribute
        self.ft = _new_target_flow

        # convert fill time from minutes to seconds for sending to controller
        converted_fill_time = self.convert_fill_time_minutes_to_seconds(fill_time=self.ft)
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.mainline,           # {0}
            str(self.ad),               # {1}
            opcodes.fill_time,          # {2}
            str(converted_fill_time)    # {3}
        )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Pipe Fill Time' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.ft),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Pipe Fill Time' to: {1}".format(
                str(self.ad),   # {0}
                str(self.ft),   # {1}
            ))
                      
    #################################
    def set_target_flow_on_cn(self, _new_target_flow):
        """
        Sets the 'Target Flow' for this Mainline on the controller. \n

        :param _new_target_flow:    New 'Target Flow' for Mainline \n
        :type _new_target_flow:     int, float \n
        """
        # Assign new Target Flow to object attribute
        self.fl = _new_target_flow
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.mainline,       # {0}
            str(self.ad),           # {1}
            opcodes.target_flow,    # {2}
            str(self.fl)            # {3}
        )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Target Flow' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.fl),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Target Flow' to: {1}".format(
                str(self.ad),   # {0}
                str(self.fl),   # {1}
            ))
                                  
    #################################
    def set_limit_zones_by_flow_state_on_cn(self, _new_state):
        """
        Sets the 'Limit Zones By Flow' for this Mainline on the controller. \n

        :param _new_state:    New 'Limit Zones By Flow' state for Mainline: 'TR' | 'FA' \n
        :type _new_state:     str \n
        """
        # Validate argument type
        if _new_state not in [opcodes.true, opcodes.false]:
            e_msg = "Invalid 'Limit Zones By Flow' state entered for Mainline {0}. Expects '{1}' or '{2}'. " \
                    "Received: {3}".format(
                        str(self.ad),       # {0}
                        opcodes.true,       # {1}
                        opcodes.false,      # {2}
                        _new_state          # {3}
                    )
            raise ValueError(e_msg)

        # Assign new Limit Zones By Flow to object attribute
        else:
            self.lc = _new_state
            command = "SET,{0}={1},{2}={3}".format(
                opcodes.mainline,               # {0}
                str(self.ad),                   # {1}
                opcodes.limit_zones_by_flow,    # {2}
                str(self.lc)                    # {3}
            )
            
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Limit Zones By Flow' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.lc),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Limit Zones By Flow' to: {1}".format(
                str(self.ad),   # {0}
                str(self.lc),   # {1}
            ))                                  
            
    #################################
    def set_high_variance_limit_on_cn(self, _new_high_variance):
        """
        Sets the 'High Variance Limit' for this Mainline on the controller. \n

        :param _new_high_variance:    New 'High Variance Limit' for Mainline:  \n
        :type _new_high_variance:     int \n
        """
        # Assign new High Variance Limit to object attribute
        self.hv = _new_high_variance
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.mainline,               # {0}
            str(self.ad),                   # {1}
            opcodes.high_variance_limit,    # {2}
            str(self.hv)                    # {3}
        )
            
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'High Variance Limit' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.hv),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'High Variance Limit' to: {1}".format(
                str(self.ad),   # {0}
                str(self.hv),   # {1}
            ))            
                        
    #################################
    def set_high_variance_shut_down_on_cn(self, _new_high_variance_shutdown):
        """
        Sets the 'High Variance Shut Down' for this Mainline on the controller. \n

        :param _new_high_variance_shutdown:    New 'High Variance Shut Down' for Mainline:  'TR' or 'FA' \n
        :type _new_high_variance_shutdown:     str \n
        """
        # Validate argument type
        if _new_high_variance_shutdown not in [opcodes.true, opcodes.false]:
            e_msg = "Invalid 'High Variance Shut Down' state entered for Mainline {0}. Expects '{1}' or '{2}'. " \
                    "Received: {3}".format(
                        str(self.ad),                   # {0}
                        opcodes.true,                   # {1}
                        opcodes.false,                  # {2}
                        _new_high_variance_shutdown     # {3}
                    )
            raise ValueError(e_msg)

        # Assign new High Variance Shut Down to object attribute
        else:
            self.hs = _new_high_variance_shutdown
            command = "SET,{0}={1},{2}={3}".format(
                opcodes.mainline,                   # {0}
                str(self.ad),                       # {1}
                opcodes.high_flow_variance_shutdown,    # {2}
                str(self.hs)                        # {3}
            )
            
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'High Variance Shut Down' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.hs),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'High Variance Shut Down' to: {1}".format(
                str(self.ad),   # {0}
                str(self.hs),   # {1}
            ))            
            
    #################################
    def set_low_variance_limit_on_cn(self, _new_low_variance):
        """
        Sets the 'Low Variance Limit' for this Mainline on the controller. \n

        :param _new_low_variance:    New 'Low Variance Limit' for Mainline:  \n
        :type _new_low_variance:     int \n
        """

        # Assign new Low Variance Limit to object attribute
        self.lv = _new_low_variance
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.mainline,               # {0}
            str(self.ad),                   # {1}
            opcodes.low_variance_limit,     # {2}
            str(self.lv)                    # {3}
        )
            
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Low Variance Limit' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.lv),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Low Variance Limit' to: {1}".format(
                str(self.ad),   # {0}
                str(self.lv),   # {1}
            ))

    #################################
    def set_low_variance_shut_down_on_cn(self, _new_low_variance_shutdown):
        """
        Sets the 'Low Variance Shut Down' for this Mainline on the controller. \n
    
        :param _new_low_variance_shutdown:    New 'Low Variance Shut Down' for Mainline:  'TR' or 'FA' \n
        :type _new_low_variance_shutdown:     str \n
        """
        # Validate argument type
        if _new_low_variance_shutdown not in [opcodes.true, opcodes.false]:
            e_msg = "Invalid 'Low Variance Shut Down' state entered for Mainline {0}. Expects '{1}' or '{2}'. " \
                    "Received: {3}".format(
                        str(self.ad),  # {0}
                        opcodes.true,  # {1}
                        opcodes.false,  # {2}
                        _new_low_variance_shutdown  # {3}
                    )
            raise ValueError(e_msg)
    
        # Assign new Low Variance Shut Down to object attribute
        else:
            self.ls = _new_low_variance_shutdown
            command = "SET,{0}={1},{2}={3}".format(
                opcodes.mainline,  # {0}
                str(self.ad),  # {1}
                opcodes.low_flow_variance_shutdown,  # {2}
                str(self.ls)  # {3}
            )
    
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Low Variance Shut Down' to: '{1}' -> {2}".format(
                str(self.ad),  # {0}
                str(self.ls),  # {1}
                e.message  # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Low Variance Shut Down' to: {1}".format(
                str(self.ad),  # {0}
                str(self.ls),  # {1}
            ))

    #################################
    def set_message_on_cn(self, _status_code):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        """
        try:
            build_message_string = messages.message_to_set(self, _status_code=_status_code, _ct_type=opcodes.mainline)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.set_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=build_message_string)

    #################################
    def get_data(self):
        """
        Gets all data for the Mainline from the controller. \n
        :return:
        """

        # Example command: "GET,ML=1"
        command = "GET,{0}={1}".format(
            opcodes.mainline,       # {0}
            str(self.ad),           # {1}
        )

        # Attempt to get data from controller
        try:
            self.data = self.ser.get_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Unable to get data for Mainline {0} using command: '{1}'. Exception raised: " \
                    "{2}".format(
                        str(self.ad),       # {0}
                        command,            # {2}
                        str(ae.message)     # {3}
                    )
            raise ValueError(e_msg)

    #################################
    def get_message_on_cn(self, _status_code):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        """
        try:
            self.build_message_string = messages.message_to_get(self, _status_code=_status_code, _ct_type=opcodes.mainline)
            cn_mg = self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            msg = "Exception caught in messages.get_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=self.build_message_string[opcodes.message])
            return cn_mg

    #################################
    def clear_message_on_cn(self, _status_code):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        """
        try:
            build_message_string = messages.message_to_clear(self, _status_code=_status_code, _ct_type=opcodes.mainline)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.clear_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=build_message_string)

        # Verifies that the message was cleared by catching the 'NM' command that we would expect back
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=opcodes.mainline)
            self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            if e.message == "NM No Message Found":
                print "Message cleared and verified that the message is gone."
            else:
                print "Message cleared, but was unable to verify if the message was gone."

    #################################
    def verify_description_on_cn(self):
        """
        Verifies the 'Description' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Description' value from the controller reply.
        -   Lastly, compares the instance 'Description' value with the controller's programming. 
        """

        description_from_controller = self.data.get_value_string_by_key(opcodes.description)

        # Compare status versus what is on the controller
        if self.ds != description_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'Description' value. Received: {1}, Expected: {2}".format(
                str(self.ad),                       # {0}
                str(description_from_controller),   # {1}
                str(self.ds)                        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Description' value: '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.ds)    # {1}
            ))
            
    #################################
    def verify_pipe_fill_time_on_cn(self):
        """
        Verifies the 'Pipe Fill Time' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Pipe Fill Time' value from the controller reply.
        -   Lastly, compares the instance 'Pipe Fill Time' value with the controller's programming. 
        """

        pipe_fill_time_from_controller = int(self.data.get_value_string_by_key(opcodes.fill_time))

        # Compare status versus what is on the controller
        if self.ft != pipe_fill_time_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'Pipe Fill Time' value. Received: {1}, Expected: {2}".format(
                str(self.ad),                       # {0}
                str(pipe_fill_time_from_controller),   # {1}
                str(self.ft)                        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Pipe Fill Time' value: '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.ft)    # {1}
            ))
                        
    #################################
    def verify_target_flow_on_cn(self):
        """
        Verifies the 'Target Flow' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Target Flow' value from the controller reply.
        -   Lastly, compares the instance 'Target Flow' value with the controller's programming. 
        """

        target_flow_from_controller = float(self.data.get_value_string_by_key(opcodes.target_flow))

        # Compare status versus what is on the controller
        if float(self.fl) != target_flow_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'Target Flow' value. Received: {1}, Expected: {2}".format(
                str(self.ad),                       # {0}
                str(target_flow_from_controller),   # {1}
                str(self.fl)                        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Target Flow' value: '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.fl)    # {1}
            ))
                                
    #################################
    def verify_limit_zones_by_flow_state_on_cn(self):
        """
        Verifies the 'Limit Zones By Flow State' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Limit Zones By Flow State' value from the controller reply.
        -   Lastly, compares the instance 'Limit Zones By Flow State' value with the controller's programming. 
        """

        limit_zones_by_flow_state_from_controller = self.data.get_value_string_by_key(opcodes.limit_zones_by_flow)

        # Compare status versus what is on the controller
        if self.lc != limit_zones_by_flow_state_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'Limit Zones By Flow State' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.ad),                                       # {0}
                        str(limit_zones_by_flow_state_from_controller),     # {1}
                        str(self.lc)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Limit Zones By Flow State' value: '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.lc)    # {1}
            ))
                                            
    #################################
    def verify_high_variance_limit_on_cn(self):
        """
        Verifies the 'High Variance Limit' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'High Variance Limit' value from the controller reply.
        -   Lastly, compares the instance 'High Variance Limit' value with the controller's programming. 
        """

        high_variance_limit_from_controller = int(self.data.get_value_string_by_key(opcodes.high_variance_limit))

        # Compare status versus what is on the controller
        if self.hv != high_variance_limit_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'High Variance Limit' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.ad),                                 # {0}
                        str(high_variance_limit_from_controller),     # {1}
                        str(self.hv)                                  # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'High Variance Limit' value: '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.hv)    # {1}
            ))
                                                       
    #################################
    def verify_high_variance_shut_down_on_cn(self):
        """
        Verifies the 'High Variance Shutdown' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'High Variance Shutdown' value from the controller reply.
        -   Lastly, compares the instance 'High Variance Shutdown' value with the controller's programming. 
        """

        high_variance_shutdown_from_controller = self.data.get_value_string_by_key(opcodes.high_flow_variance_shutdown)

        # Compare status versus what is on the controller
        if self.hs != high_variance_shutdown_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'High Variance Shutdown' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.ad),                                   # {0}
                        str(high_variance_shutdown_from_controller),    # {1}
                        str(self.hs)                                    # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'High Variance Shutdown' value: '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.hs)    # {1}
            ))
                                                   
    #################################
    def verify_low_variance_limit_on_cn(self):
        """
        Verifies the 'Low Variance Limit' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Low Variance Limit' value from the controller reply.
        -   Lastly, compares the instance 'Low Variance Limit' value with the controller's programming. 
        """

        low_variance_limit_from_controller = int(self.data.get_value_string_by_key(opcodes.low_variance_limit))

        # Compare status versus what is on the controller
        if self.lv != low_variance_limit_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'Low Variance Limit' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.ad),                                 # {0}
                        str(low_variance_limit_from_controller),     # {1}
                        str(self.lv)                                  # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Low Variance Limit' value: '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.lv)    # {1}
            ))

    #################################
    def verify_low_variance_shut_down_on_cn(self):
        """
        Verifies the 'Low Variance Shutdown' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Low Variance Shutdown' value from the controller reply.
        -   Lastly, compares the instance 'Low Variance Shutdown' value with the controller's programming. 
        """

        low_variance_shutdown_from_controller = self.data.get_value_string_by_key(opcodes.low_variance_shut_down)

        # Compare status versus what is on the controller
        if self.ls != low_variance_shutdown_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'Low Variance Shutdown' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.ad),                                   # {0}
                        str(low_variance_shutdown_from_controller),     # {1}
                        str(self.ls)                                    # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Low Variance Shutdown' value: '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.ls)    # {1}
            ))

    #################################
    def verify_message_on_cn(self, _status_code):
        """
        the expected values are retrieved from the controller and than compared against the values that are returned
        from the message module
        :param _status_code
        :type _status_code :str
        :return:
        """
        mg_on_cn = self.get_message_on_cn(_status_code=_status_code)
        expected_message_text_from_cn = mg_on_cn.get_value_string_by_key(opcodes.message_text)
        expected_message_id_from_cn = mg_on_cn.get_value_string_by_key(opcodes.message_id)
        expected_date_time_from_cn = mg_on_cn.get_value_string_by_key(opcodes.date_time)
        # Compare status versus what is on the controller
        if self.build_message_string[opcodes.message_id] != expected_message_id_from_cn:
            e_msg = "Created ID message did not match the ID received from the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_id],  # {0} The ID that was built
                        expected_message_id_from_cn                     # {1} The ID returned from controller
                    )
            raise ValueError(e_msg)

        mess_date_time = datetime.strptime(self.build_message_string[opcodes.date_time], '%m/%d/%y %H:%M:%S')
        controller_date_time = datetime.strptime(expected_date_time_from_cn, '%m/%d/%y %H:%M:%S')
        if mess_date_time != controller_date_time:
            if abs(controller_date_time - mess_date_time) >= timedelta(days=0, hours=0, minutes=60, seconds=0):
                e_msg = "The date and time of the message didn't match the controller:\n" \
                        "\tCreated: \t\t'{0}'\n" \
                        "\tReceived:\t\t'{1}'\n".format(
                            self.build_message_string[opcodes.date_time],  # {0} The date message that was built
                            expected_date_time_from_cn  # {1} The date message returned from controller
                        )
                raise ValueError(e_msg)
            # only print this if they were not exact
            e_msg = "############################  Date and time were not exact but were within +- 30 minutes \n" \
                    "############################  Controller Date and Time = {0} \n" \
                    "############################  Message Date time = {1}".format(
                        expected_date_time_from_cn,  # {0} Date time received from controller
                        self.build_message_string[opcodes.date_time]  # {1} Date time the message was verified
                    )
            print(e_msg)

        if self.build_message_string[opcodes.date_time] != expected_date_time_from_cn:
            e_msg = "The date and time of the message didn't match the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.date_time],   # {0} The date message that was built
                        expected_date_time_from_cn                      # {1} The date message returned from controller
                    )
            raise ValueError(e_msg)
        if self.build_message_string[opcodes.message_text] != expected_message_text_from_cn:
            e_msg = "Created TX message did not match the TX received from the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_text],   # {0} The TX message that was built
                        expected_message_text_from_cn                      # {1} The TX message returned from controller
                    )
            raise ValueError(e_msg)
        else:
            print "Successfully verified:\n" \
                  "\tID: '{0}'\n" \
                  "\tDT: '{1}'\n" \
                  "\tTX: '{2}'\n".format(
                      self.build_message_string[opcodes.message_id],
                      self.build_message_string[opcodes.date_time],
                      self.build_message_string[opcodes.message_text]
                  )

    #################################
    def verify_who_i_am(self):
        """
        Verifier wrapper which verifies all attributes for this 'Mainline'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()
        
        # Verify all Mainline attributes
        self.verify_description_on_cn()
        self.verify_pipe_fill_time_on_cn()
        self.verify_target_flow_on_cn()
        self.verify_limit_zones_by_flow_state_on_cn()
        self.verify_high_variance_limit_on_cn()
        self.verify_high_variance_shut_down_on_cn()
        self.verify_low_variance_limit_on_cn()
        self.verify_low_variance_shut_down_on_cn()
