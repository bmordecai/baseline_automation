from decimal import Decimal
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices

__author__ = 'baseline'


class AlertRelay(Devices):
    """
    Alert Relay Object \n
    """

    starting_lat = Decimal(Devices.controller_lat)
    starting_long = Decimal(Devices.controller_long) + Decimal('.000700')

    #################################
    def __init__(self, _serial, _address):

        # Create description for initialization of Device parent class for inheritance
        description = "{0} Test Alert Relay {1}".format(_serial, str(_address))

        # create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        self.sn = None

        # Initialize parent class
        Devices.__init__(self, _sn=_serial, _ds=description, _ad=_address, _la=lat, _lg=lng)

        self.dv_type = opcodes.alert_relay
        # Alert relay specific attributes
        self.en = 'TR'      # Enabled / Disabled (TR | FA)
        self.vc = 'CL'      # Contact State
        self.vt = 1.7       # Two Wire Drop
        # flags (1=unexpected high flow, 2=high flow, 4=zone flow variance, 8=device no reply, 16=device checksum,
        # 32=solenoid error, 64=program overrun)
        self.fg = 15  # This is the default value on the controller
        # self.set_default_values()

    #################################
    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        # Update current description in case we changed address or serial number
        self.ds = self.ds if self.ds == self.build_description() else self.build_description()

        command = "SET,AR={0},EN={1},DS={2},LA={3},LG={4},VT={5}".format(
            str(self.sn),   # {0}
            str(self.en),   # {1}
            str(self.ds),   # {2}
            self.la,        # {3}
            self.lg,        # {4}
            str(self.vt)    # {5}
        )
        try:
            # Attempt to set alert relay default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Alert Relay {0} ({1})'s 'Default values' to: '{2}'".format(
                    self.sn, str(self.ad), command)
            raise Exception(e_msg)
        else:
            print("Successfully set Alert Relay {0} ({1})'s 'Default values' to: {2}".format(
                  self.sn, str(self.ad), command))

    #################################
    def set_enable_state_on_cn(self, _state=None):
        """
        Enables the Alert Relay on the controller. \n
        :return:
        """
        if _state is not None:

            # If Alert Relay isn't enabled, make sure to update instance variable to match what the controller will
            # show.
            if _state not in ['TR', 'FA']:
                e_msg = "Invalid enabled state entered for Alert Relay {0} ({1}). Received: {2}, Expected: 'TR' or " \
                        "'FA'".format(self.sn, str(self.ad), _state)
                raise ValueError(e_msg)
            else:
                self.en = _state

        # Command for sending
        command = "SET,AR={0},EN={1}".format(str(self.sn), self.en)

        try:
            # Attempt to enable Alert Relay
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Alert Relay {0} ({1})'s Enable State".format(
                    self.sn, str(self.ad))
            raise Exception(e_msg)
        else:
            print("Successfully set Alert Relay {0} ({1})'s 'Enabled State' to: {2}".format(
                  self.sn, str(self.ad), str(self.en)))

    # TODO documentation says this is read only
    # #################################
    # def set_contact_state_on_cn(self, _contact_state=None):
    #     """
    #     Set the contact state for the Alert Relay (Faux IO Only) \n
    #     :param _contact_state: Open ('OP') or Closed ('CL') \n
    #     :return:
    #     """
    #
    #     if _contact_state is not None:
    #
    #         # verify state passed in, make sure to update instance variable to match what the controller will show.
    #         if _contact_state not in ['OP', 'CL']:
    #             e_msg = "Unable to set Alert Relay {0} ({1})'s Contact State. Received: {2}, needs to be FA or " \
    #                     "TR".format(self.sn, str(self.ad), str(_contact_state))
    #             raise ValueError(e_msg)
    #         else:
    #             self.vc = _contact_state
    #
    #     # Command for sending
    #     command = "SET,AR={0},VC={1}".format(str(self.sn), str(self.vc))
    #
    #     try:
    #         # Attempt to set Alert Relay state for Alert Relay at the controller in faux io
    #         self.ser.send_and_wait_for_reply(tosend=command)
    #     except Exception:
    #         e_msg = "Exception occurred trying to set Alert Relay {0} ({1})'s 'Contact State' to: '{2}'".format(
    #                 self.sn, str(self.ad), str(self.vc))
    #         raise Exception(e_msg)
    #     else:
    #         print("Successfully set Alert Relay {0} ({1})'s 'Contact State' to: {2}".format(
    #               self.sn, str(self.ad), str(self.vc)))

    #################################
    def set_flags_on_cn(self, _flag):
        """
        Gives the alert relay a flag. \n
        :param: _flag       The flag to set on the controller \n
                            1=unexpected high flow,
                            2=high flow,
                            4=zone flow variance,
                            8=device no reply,
                            16=device checksum,
                            32=solenoid error,
                            64=program overrun)
        :type: _flag        int \n
        :return:
        """
        if _flag not in [1, 2, 4, 8, 16, 32, 64]:
            # If Alert Relay isn't enabled, make sure to update instance variable to match what the controller will
            # show.
            e_msg = "Invalid flag entered for Alert Relay {0} ({1}). Received: {2}, Expected: " \
                    "1, 2, 4, 8, 16, 32, 64".format(
                        self.sn,
                        str(self.ad),
                        _flag)
            raise ValueError(e_msg)
        else:
            self.fg = _flag

        # Command for sending
        command = "SET,AR={0},FG={1}".format(str(self.sn), self.fg)

        try:
            # Attempt to set a flag on Alert Relay
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Alert Relay {0} ({1})'s Flag".format(
                    self.sn, str(self.ad))
            raise Exception(e_msg)
        else:
            print("Successfully set Alert Relay {0} ({1})'s 'Flag' to: {2}".format(
                  self.sn, str(self.ad), str(self.fg)))

    #################################
    def set_two_wire_drop_value_on_cn(self, _value=None):
        """
        Set the two wire drop value for the Alert Relay (Faux IO Only) \n
        :param _value: Value to set the Alert Relay two wire drop value to \n
        :return:
        """
        # If a two wire drop value is passed in for overwrite
        if _value is not None:

            # Verifies the two wire drop passed in is an int or float value
            if not isinstance(_value, (int, float)):
                e_msg = "Failed trying to set Alert Relay {0} ({1}) two wire drop. Invalid argument type, " \
                        "expected an int or float, received: {2}".format(str(self.sn), str(self.ad), type(_value))
                raise TypeError(e_msg)
            else:
                self.vt = _value

        # Command for sending
        command = "SET,AR={0},VT={1}".format(str(self.sn), str(self.vt))

        try:
            # Attempt to set two wire drop for Alert Relay at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Alert Relay {0} ({1})'s 'Two Wire Drop Value' to: '{2}'".format(
                    self.sn, str(self.ad), str(self.vt))
            raise Exception(e_msg)
        else:
            print("Successfully set Alert Relay {0} ({1})'s 'Two Wire Drop Value' to: {2}".format(
                self.sn, str(self.ad), str(self.vt)))

    #################################
    def verify_contact_state_on_cn(self):
        """
        Verifies contact state for this Alert Relay on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        contact_state = self.data.get_value_string_by_key('VC')

        # Compare status versus what is on the controller
        if self.vc != contact_state:
            e_msg = "Unable verify Alert Relay {0} ({1})'s contact state. Received: {2}, Expected: {3}".format(
                    self.sn,                # {0}
                    str(self.ad),           # {1}
                    str(contact_state),     # {2}
                    str(self.vc)            # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Alert Relay {0} ({1})'s contact state: '{2}' on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.vc)      # {2}
                  ))

    #################################
    def verify_two_wire_drop_value_on_cn(self):
        """
        Verifies the two wire drop value for this Alert Relay on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        two_wire_drop_val = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        rounded_vt = round(number=self.vt, ndigits=2)
        if abs(rounded_vt - two_wire_drop_val) > 0.2:
            # Compare status versus what is on the controller
            if self.vt != float(two_wire_drop_val):
                e_msg = "Unable verify Alert Relay {0} ({1})'s two wire drop value. Received: {2}, Expected: {3}".format(
                        self.sn,                    # {0}
                        str(self.ad),               # {1}
                        str(two_wire_drop_val),     # {2}
                        str(self.vt)                # {3}
                )
                raise ValueError(e_msg)
        else:
            print("Verified Alert Relay {0} ({1})'s two wire drop value: '{2}' on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.vt)      # {2}
                  ))

    #################################
    def verify_enable_state_on_cn(self):
        """
        Verifies that the enabled state Alert Relay on the controller. \n
        :return:
        """
        enable_state = self.data.get_value_string_by_key('EN')
        # Compare status versus what is on the controller
        if self.en != enable_state:
            e_msg = "Unable verify Alert Relay {0} ({1})'s enabled state. Received: {2}, Expected: {3}".format(
                    self.sn,            # {0}
                    str(self.ad),       # {1}
                    str(enable_state),  # {2}
                    str(self.en)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Alert Relay {0} ({1})'s state: '{2}' on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.en)      # {2}
                  ))

    #################################
    def verify_flag_on_cn(self):
        """
        Verifies the flag of the Alert Relay on the controller. \n
        :return:
        """
        flag = self.data.get_value_string_by_key(opcodes.flag)
        # Compare status versus what is on the controller
        if self.fg != int(flag):
            e_msg = "Unable verify Alert Relay {0} ({1})'s flag. Received: {2}, Expected: {3}".format(
                    self.sn,            # {0}
                    str(self.ad),       # {1}
                    str(flag),          # {2}
                    str(self.fg)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Alert Relay {0} ({1})'s flag: '{2}' on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.fg)      # {2}
                  ))

    #################################
    def self_test_and_update_object_attributes(self):
        """
        this does a self test on the device
        than does a get date a resets the attributes of the device to match the controller
        self.vc      # Contact State
        self.vt      # Two Wire Drop
        """
        try:
            self.do_self_test()
            self.get_data()
            self.vc = str(self.data.get_value_string_by_key(opcodes.contacts_state))
            self.vt = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        except Exception as e:
            e_msg = "Exception occurred trying to updating attributes of the Alert Relay {0} object." \
                    " Contact State Value: '{1}'," \
                    " Two Wire Drop Value: '{2}'," \
                .format(
                    str(self.ad),  # {0}
                    str(self.vc),  # {1}
                    str(self.vt),  # {2}
                   )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the Alert Relay {0} object. "
                  "Contact State Value: '{1}', "
                  "Two Wire Drop Value: '{2}', ".format(
                    str(self.ad),  # {0}
                    str(self.vc),  # {1}
                    str(self.vt),  # {2}
                  ))

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Flow Meter'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify base attributes
        self.verify_description_on_cn()
        self.verify_serial_number_on_cn()
        self.verify_latitude_on_cn()
        self.verify_longitude_on_cn()
        if _expected_status is not None:
            self.verify_status_on_cn(status=_expected_status)

        # Verify Event Switch specific attributes
        self.verify_enable_state_on_cn()
        self.verify_contact_state_on_cn()
        self.verify_two_wire_drop_value_on_cn()
        self.verify_flag_on_cn()
