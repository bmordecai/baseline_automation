from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.poc import POC

__author__ = 'baseline'


########################################################################################################################
# 1000 POC Class
########################################################################################################################
class POC1000(POC):
    """
    1000 Point of Connection Object \n
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    #################################
    def __init__(self,
                 _address,
                 _flow_meter_address=None,
                 _master_valve_address=None,
                 _description=None,
                 _enabled_state=opcodes.true,
                 _target_flow=1000.0,
                 _high_flow_limit=0.0,
                 _shutdown_on_high_flow=opcodes.false,
                 _unscheduled_flow_limit=0.0,
                 _shutdown_on_unscheduled=opcodes.false,
                 _limit_concurrent_zones_to_target=opcodes.false,
                 _fill_time=1,
                 _flow_variance_percent=10,
                 _flow_variance_enable=opcodes.false,
                 _flow_fault=opcodes.false
                 ):
        """
        1000 Water Source Constructor. \n

        :param _address:    Address for water source. \n
        :type _address:     int \n

        :param _flow_meter_address:     Address for assigned flow meter. \n
        :type _flow_meter_address:      int \n

        :param _master_valve_address:   Address for assigned master valve. \n
        :type _master_valve_address:    int \n

        :param _description:    Description for water source if a specific is wanted. \n
        :type _description:     str \n

        :param _enabled_state:  Enable / disable water source. ('TR'=true, 'FA'=false) \n
        :type _enabled_state:   str \n

        :param _target_flow:    Target flow for water source in GPM \n
        :type _target_flow:     int, float \n

        :param _high_flow_limit:   High flow limit for water source in GPM \n
        :type _high_flow_limit:    int, float \n

        :param _shutdown_on_high_flow:  Shutdown on high flow bound ('TR'=true, 'FA'=false) \n
        :type _shutdown_on_high_flow:   str \n

        :param _unscheduled_flow_limit:     Unscheduled flow limit in GPM \n
        :type _unscheduled_flow_limit:      int, float \n

        :param _shutdown_on_unscheduled:    Shutdown on unscheduled flow bound ('TR'=true, 'FA'=false) \n
        :type _shutdown_on_unscheduled:     str \n

        :param _limit_concurrent_zones_to_target:   Limit concurrent zones to target ('TR'=true, 'FA'=false) \n
        :type _limit_concurrent_zones_to_target:    str \n

        :param _fill_time:  Fill time in minutes \n
        :type _fill_time:   int \n

        :param _flow_variance_percent:  Flow variance percent \n
        :type _flow_variance_percent:   int, float \n

        :param _flow_variance_enable:   Flow variance enable ('TR'=true, 'FA'=false) \n
        :type _flow_variance_enable:    str \n

        :param _flow_fault:     Flow fault state ('TR'=true, 'FA'=false) \n
        :type _flow_fault:      str \n
        """

        # Description for POC
        desc = _description if _description else "Test Water Source {0}".format(str(_address))

        # Init parent class
        POC.__init__(self, _ad=_address, _ds=desc, _en=_enabled_state, _fl=_target_flow, _hf=_high_flow_limit,
                     _hs=_shutdown_on_high_flow, _uf=_unscheduled_flow_limit, _us=_shutdown_on_unscheduled,
                     _fm=_flow_meter_address, _mv=_master_valve_address)

        # 1000 POC Specific Attributes
        self.lf = _limit_concurrent_zones_to_target
        self.ft = _fill_time
        self.vf = _flow_variance_percent
        self.ve = _flow_variance_enable
        self.ff = _flow_fault

        # Send state of POC instance to controller
        self.send_programming_to_cn()

    #################################
    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    #################################
    def build_obj_configuration_for_send(self):
        """
        Builds the 'SET' string for sending this Point of Connection Instance to the Controller. This method ensures
        that attributes with blank values don't get sent to the controller. This helps avoid 'BC' response's from
        controller. \n
        :return:        Formatted 'Set' string for the Point of Connection \n
        :rtype:         str \n
        """
        # Starting string, always will need this, 'SET,PC=Blah'
        current_config = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13},{14}={15}".format(
            opcodes.point_of_connection,        # {0}
            str(self.ad),                       # {1}
            opcodes.description,                # {2}
            self.ds,                            # {3}
            opcodes.enabled,                    # {4}
            self.en,                            # {5}
            opcodes.target_flow,                # {6}
            str(self.fl),                       # {7}
            opcodes.high_flow_limit,            # {8}
            str(self.hf),                       # {9}
            opcodes.shutdown_on_high_flow,      # {10}
            self.hs,                            # {11}
            opcodes.unscheduled_flow_limit,     # {12}
            str(self.uf),                       # {13}
            opcodes.shutdown_on_unscheduled,    # {14}
            self.us,                            # {15}
        )

        # 1000 Point of Connection attribute list of tuples (each tuple represents the OpCode string and associated
        # value)
        poc_attr_list = [
            (opcodes.flow_meter, self.fm),
            (opcodes.master_valve, self.mv),
            (opcodes.limit_concurrent_to_target, self.lf),
            (opcodes.fill_time, self.ft),
            (opcodes.flow_variance, self.vf),
            (opcodes.flow_variance_enable, self.ve),
            (opcodes.flow_fault, self.ff)
        ]

        # For each attribute, check if the attribute has a value that isn't 'None' or '' (empty string).
        for attr_tuple in poc_attr_list:

            # If the attribute value isn't 'None' or '', append it to the current_config string
            if attr_tuple[1]:
                arg1 = attr_tuple[0]

                # If the attribute is Flow Meters, and we have an address, get the serial number for the address
                if attr_tuple[0] == opcodes.flow_meter:
                    # get flow meter object's serial number based on the address
                    arg2 = self.flow_meter_objects[attr_tuple[1]].sn

                elif attr_tuple[0] == opcodes.master_valve:
                    # get master valve object's serial number based on the address
                    arg2 = self.master_valve_objects[attr_tuple[1]].sn

                else:
                    # attribute was not based on an object.
                    arg2 = attr_tuple[1]

                # Append additional attributes if they have values.
                current_config += ",{0}={1}".format(
                    arg1,  # {0}
                    arg2   # {1}
                )

        return current_config

    #################################
    def send_programming_to_cn(self):
        """
        Sends the current programming for the 1000 Water Source to the controller. \n
        """
        # Command for sending
        command = self.build_obj_configuration_for_send()

        try:

            # Attempt to set program default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)

        # catch all exceptions
        except Exception as e:
            e_msg = "Exception occurred trying to 'SET' Water Source {0}'s 'Values' to: '{1}'\n -> {2}".format(
                str(self.ad),   # {0}
                command,        # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)

        # If no exceptions
        else:
            print("Successfully set Water Source {0}'s 'Values' to: {1}".format(
                str(self.ad),   # {0}
                command         # {1}
            ))

    #################################
    def set_limit_concurrent_to_target_state_on_cn(self, _new_state):
        """
        Sets the 'Limit Concurrent To Target State' for the Water Source on the controller. \n

        :param _new_state:    New 'Limit Concurrent To Target State' for Water Source \n
        :type _new_state:     str \n
        """
        # Validate argument type
        if _new_state not in [opcodes.true, opcodes.false]:
            e_msg = "Invalid state entered for 'SET' 'Limit Concurrent To Target State' for 1000 Water Source {0}. " \
                    "Expects: {1} | {2}, received: {3}".format(
                        str(self.ad),       # {0}
                        opcodes.true,       # {1}
                        opcodes.false,      # {2}
                        _new_state          # {3}
                    )
            raise ValueError(e_msg)

        else:
            self.lf = _new_state
            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,            # {0}
                str(self.ad),                           # {1}
                opcodes.limit_concurrent_to_target,     # {2}
                self.lf                                 # {3}
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000's Water Source {0}'s 'Limit Concurrent To Target State' " \
                    "to: '{1}' -> {2}".format(
                        str(self.ad),   # {0}
                        self.lf,        # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 1000 Water Source {0}'s 'Limit Concurrent To Target State' to: {1}".format(
                str(self.ad),   # {0}
                self.lf,        # {1}
            ))
            
    #################################
    def set_fill_time_in_minutes_on_cn(self, _new_fill_time):
        """
        Sets the 'Fill Time' for the Water Source on the controller. \n

        :param _new_fill_time:    New 'Fill Time' for Water Source \n
        :type _new_fill_time:     int \n
        """

        self.ft = _new_fill_time
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.point_of_connection,    # {0}
            self.ad,                        # {1}
            opcodes.fill_time,              # {2}
            self.ft                         # {3}
        )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000's Water Source {0}'s 'Fill Time' " \
                    "to: '{1}' -> {2}".format(
                        self.ad,    # {0}
                        self.ft,    # {1}
                        e.message   # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 1000 Water Source {0}'s 'Fill Time' to: {1}".format(
                self.ad,   # {0}
                self.ft    # {1}
            ))

    #################################
    def set_flow_variance_percent_on_cn(self, _new_flow_variance):
        """
        Sets the 'Flow Variance' for the Water Source on the controller. \n

        :param _new_flow_variance:    New 'Flow Variance' for Water Source \n
        :type _new_flow_variance:     int, float \n
        """
        # Validate argument type
        if not isinstance(_new_flow_variance, (int, float)):
            e_msg = "Invalid argument type entered for 'SET' 'Flow Variance' for 1000 Water Source {0}. " \
                    "Expects type: int or float, received: {1}".format(
                        self.ad,                    # {0}
                        type(_new_flow_variance)    # {1}
                    )
            raise ValueError(e_msg)

        else:
            self.vf = _new_flow_variance
            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,    # {0}
                self.ad,                        # {1}
                opcodes.flow_variance,          # {2}
                self.vf                         # {3}
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000's Water Source {0}'s 'Flow Variance' " \
                    "to: '{1}' -> {2}".format(
                        self.ad,    # {0}
                        self.vf,    # {1}
                        e.message   # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 1000 Water Source {0}'s 'Flow Variance' to: {1}".format(
                self.ad,   # {0}
                self.vf    # {1}
            ))

    #################################
    def set_flow_variance_enable_state_on_cn(self, _new_state):
        """
        Sets the 'Flow Variance Enable State' for the Water Source on the controller. \n

        :param _new_state:    New 'Flow Variance Enable State' for Water Source \n
        :type _new_state:     str \n
        """
        # Validate argument type
        if _new_state not in [opcodes.true, opcodes.false]:
            e_msg = "Invalid state entered for 'SET' 'Flow Variance Enable State' for 1000 Water Source {0}. " \
                    "Expects: {1} | {2}, received: {3}".format(
                        str(self.ad),       # {0}
                        opcodes.true,       # {1}
                        opcodes.false,      # {2}
                        _new_state          # {3}
                    )
            raise ValueError(e_msg)

        else:
            self.ve = _new_state
            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,    # {0}
                self.ad,                        # {1}
                opcodes.flow_variance_enable,   # {2}
                self.ve                         # {3}
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000's Water Source {0}'s 'Flow Variance Enable State' " \
                    "to: '{1}' -> {2}".format(
                        self.ad,   # {0}
                        self.ve,   # {1}
                        e.message  # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 1000 Water Source {0}'s 'Flow Variance Enable State' to: {1}".format(
                self.ad,   # {0}
                self.ve,   # {1}
            ))
            
    #################################
    def set_flow_fault_state_on_cn(self, _new_state):
        """
        Sets the 'Flow Fault State' for the Water Source on the controller. \n

        :param _new_state:    New 'Flow Fault State' for Water Source \n
        :type _new_state:     str \n
        """
        # Validate argument type
        if _new_state not in [opcodes.true, opcodes.false]:
            e_msg = "Invalid state entered for 'SET' 'Flow Fault State' for 1000 Water Source {0}. " \
                    "Expects: {1} | {2}, received: {3}".format(
                        str(self.ad),       # {0}
                        opcodes.true,       # {1}
                        opcodes.false,      # {2}
                        _new_state          # {3}
                    )
            raise ValueError(e_msg)

        else:
            self.ff = _new_state
            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,    # {0}
                self.ad,                        # {1}
                opcodes.flow_fault,             # {2}
                self.ff                         # {3}
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000's Water Source {0}'s 'Flow Fault State' " \
                    "to: '{1}' -> {2}".format(
                        self.ad,   # {0}
                        self.ff,   # {1}
                        e.message  # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 1000 Water Source {0}'s 'Flow Fault State' to: {1}".format(
                self.ad,   # {0}
                self.ff,   # {1}
            ))

    #################################
    def verify_limit_concurrent_to_target_on_cn(self):
        """
        Verifies the 'Limit Concurrent To Target State' for the 1000 Water Source set on the controller. \n
        """
        # expect str type
        state_from_controller = self.data.get_value_string_by_key(opcodes.limit_concurrent_to_target)
    
        # Comparison of values between controller and object
        if self.lf != state_from_controller:
            e_msg = "Unable to verify 1000's Water Source {0}'s 'Limit Concurrent To Target State'. Received: {1}, " \
                    "Expected: {2}".format(
                        self.ad,                # {0}
                        state_from_controller,  # {1}
                        self.lf                 # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified 1000's Water Source {0}'s 'Limit Concurrent To Target State': '{1}' on controller".format(
                self.ad,    # {0}
                self.lf     # {1}
            ))
            
    #################################
    def verify_fill_time_on_cn(self):
        """
        Verifies the 'Fill Time' for the 1000 Water Source set on the controller. \n
        """
        # expect int type
        fill_time_from_controller = int(self.data.get_value_string_by_key(opcodes.fill_time))
    
        # Comparison of values between controller and object
        if self.ft != fill_time_from_controller:
            e_msg = "Unable to verify 1000's Water Source {0}'s 'Fill Time'. Received: {1}, " \
                    "Expected: {2}".format(
                        self.ad,                    # {0}
                        fill_time_from_controller,  # {1}
                        self.ft                     # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified 1000's Water Source {0}'s 'Fill Time': '{1}' on controller".format(
                self.ad,    # {0}
                self.ft     # {1}
            ))      
            
    #################################
    def verify_flow_variance_on_cn(self):
        """
        Verifies the 'Flow Variance' for the 1000 Water Source set on the controller. \n
        """
        # expect float type
        flow_variance_from_controller = float(self.data.get_value_string_by_key(opcodes.flow_variance))
    
        # Comparison of values between controller and object
        # Verify both values as float values because the controller returns the values in float notation
        if float(self.vf) != flow_variance_from_controller:
            e_msg = "Unable to verify 1000's Water Source {0}'s 'Flow Variance'. Received: {1}, " \
                    "Expected: {2}".format(
                        self.ad,                        # {0}
                        flow_variance_from_controller,  # {1}
                        self.vf                         # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified 1000's Water Source {0}'s 'Flow Variance': '{1}' on controller".format(
                self.ad,    # {0}
                self.vf     # {1}
            ))

    #################################
    def verify_flow_variance_enable_on_cn(self):
        """
        Verifies the 'Flow Variance Enable State' for the 1000 Water Source set on the controller. \n
        """
        # expect str type
        state_from_controller = self.data.get_value_string_by_key(opcodes.flow_variance_enable)

        # Comparison of values between controller and object
        if self.ve != state_from_controller:
            e_msg = "Unable to verify 1000's Water Source {0}'s 'Flow Variance Enable State'. Received: {1}, " \
                    "Expected: {2}".format(
                        self.ad,                # {0}
                        state_from_controller,  # {1}
                        self.ve                 # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified 1000's Water Source {0}'s 'Flow Variance Enable State': '{1}' on controller".format(
                self.ad,    # {0}
                self.ve     # {1}
            ))

    #################################
    def verify_flow_fault_state_on_cn(self):
        """
        Verifies the 'Flow Fault State' for the 1000 Water Source set on the controller. \n
        """
        # expect str type
        state_from_controller = self.data.get_value_string_by_key(opcodes.flow_fault)

        # Comparison of values between controller and object
        if self.ff != state_from_controller:
            e_msg = "Unable to verify 1000's Water Source {0}'s 'Flow Fault State'. Received: {1}, " \
                    "Expected: {2}".format(
                        self.ad,                # {0}
                        state_from_controller,  # {1}
                        self.ff                 # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified 1000's Water Source {0}'s 'Flow Fault State': '{1}' on controller".format(
                self.ad,    # {0}
                self.ff     # {1}
            ))

    #################################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies all attributes associated with this Point Of Connection. \n
        :param expected_status:     An expected status to verify for POC \n
        :type expected_status:      str \n
        """
        self.get_data()

        self.verify_description_on_cn()
        self.verify_enabled_state_on_cn()
        self.verify_target_flow_on_cn()
        self.verify_high_flow_limit_on_cn()
        self.verify_shutdown_on_high_flow_on_cn()
        self.verify_shutdown_on_unscheduled_on_cn()
        self.verify_limit_concurrent_to_target_on_cn()
        self.verify_fill_time_on_cn()
        self.verify_flow_variance_on_cn()
        self.verify_flow_variance_enable_on_cn()
        self.verify_flow_fault_state_on_cn()

        # TODO..
        # Currently, if we don't assign a POC a MV, and the POC is not POC 1, then the controller is returning a
        # serial number that isn't even loaded onto the controller during the get_data() call. This line says that if
        # we specified a master valve, then go check
        if self.mv:
            self.verify_master_valve_on_cn()

        if self.fm:
            self.verify_flow_meter_on_cn()

        if expected_status is not None:
            self.verify_status_on_cn(_expected_status=expected_status)