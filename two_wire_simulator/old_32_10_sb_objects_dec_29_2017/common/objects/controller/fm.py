# from old_32_10_sb_objects_dec_29_2017.common.imports import *
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from decimal import Decimal
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
# from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.messages import Messages

__author__ = 'tige'


########################################################################################################################
# FLOW METER
########################################################################################################################
class FlowMeter(Devices):
    """
    Flow Meter Object \n
    """
    starting_lat = Decimal(Devices.controller_lat)
    starting_long = Decimal(Devices.controller_long) + Decimal('.000400')

    #################################
    def __init__(self, _serial, _address, _shared_with_sb=False):

        # Create description for initialization of Device parent class for inheritance
        description = "{0} Test Flow Meter {1}".format(_serial, str(_address))

        # create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        # Initialize parent class
        Devices.__init__(self, _sn=_serial, _ds=description, _ad=_address, _la=lat, _lg=lng, is_shared=_shared_with_sb)

        self.dv_type = opcodes.flow_meter
        # Flow Meter specific attributes
        self.en = 'TR'     # Enabled / Disabled (TR | FA)
        self.kv = 2.0      # K Value
        self.vr = 0.0      # Flow rate in Gallons Per Minute
        self.vg = 0.0      # Flow reported as usage in Total Gallons
        self.vt = 0.0      # Two Wire Drop

    #################################
    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        # Update current description in case we changed address or serial number
        self.ds = self.ds if self.ds == self.build_description() else self.build_description()

        command = "SET,FM={0},EN={1},DS={2},LA={3},LG={4},KV={5}".format(
            str(self.sn),   # {0}
            str(self.en),   # {1}
            str(self.ds),   # {2}
            str(self.la),   # {3}
            str(self.lg),   # {4}
            str(self.kv),   # {5}
        )

        # If the device is not shared with the substation, set these values because we own them.
        if not self.is_shared_with_substation:
            command += ",VR={0},VG={1},VT={2}".format(
                self.vr,   # {0}
                self.vg,   # {1}
                self.vt    # {2}
            )

        try:
            # Attempt to set flow meter default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'Default values' to: '{2}'".format(
                    self.sn, str(self.ad), command)
            raise Exception(e_msg)
        else:
            print("Successfully set Flow Meter {0} ({1})'s 'Default values' to: {2}".format(
                  self.sn, str(self.ad), command))

    #################################
    def set_enable_state_on_cn(self, _state=None):
        """
        Enables the flow meter on the controller. \n
        :return:
        """
        # if it is None, use current value
        if _state is not None:

            # If flow meter isn't enabled, make sure to update instance variable to match what the controller will show.
            if _state not in ['TR', 'FA']:
                e_msg = "Invalid enabled state entered for Flow Meter {0} ({1}). Received: {2}, Expected: 'TR' or " \
                        "'FA'".format(self.sn, str(self.ad), _state)
                raise ValueError(e_msg)
            else:
                self.en = _state

        # Command for sending
        command = "SET,FM={0},EN={1}".format(self.sn, self.en)

        try:
            # Attempt to enable flow meter
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to enable flow meter {0}".format(self.sn)
            raise Exception(e_msg)
        else:
            print("Successfully set Flow Meter {0} ({1})'s 'Enabled State' to: {2}".format(self.sn,
                                                                                           str(self.ad),
                                                                                           str(self.en)))

    #################################
    def set_k_value_on_cn(self, _k_value=None):
        """
        Set the k_value for the Flow Meter (Faux IO Only) \n
        :param _k_value:        Value to set the Flow Meter k value as a float \n
        :return:
        """

        # if is None, use current value
        if _k_value is not None:

            # Verifies the k_value passed in is an float value
            if not isinstance(_k_value, float):
                e_msg = "Failed trying to set FM {0} ({1})'s k value. Invalid argument type, expected a float, " \
                        "received: {2}".format(self.sn, str(self.ad), type(_k_value))
                raise TypeError(e_msg)
            else:
                self.kv = _k_value

        # Command for sending
        command = "SET,FM={0},KV={1}".format(self.sn, str(self.kv))

        try:
            # Attempt to set k_value for Flow Meter at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'K Value' to: '{2}'".format(
                    self.sn, str(self.ad), str(self.kv))
            raise Exception(e_msg)
        else:
            print("Successfully set Flow Meter {0} ({1})'s 'K Value' to: {2}".format(
                  self.sn, str(self.ad), str(self.kv)))

    #################################
    def set_flow_rate_on_cn(self, _flow_rate=None):
        """
        Set the flow rate for the Flow Meter (Faux IO Only) \n
        :param _flow_rate:        Value to set the Flow Meter flow rate as a float in gallon per minute\n
        :return:
        """
        # If a flow rate is passed in for overwrite
        if _flow_rate is not None:

            # Verifies the flow rate passed in is an int or float value
            if not isinstance(_flow_rate, (int, float)):
                e_msg = "Failed trying to set FM {0} ({1})'s Flow Rate, invalid argument type, expected a int " \
                        "or float, received: {2}".format(self.sn, str(self.ad), type(_flow_rate))
                raise TypeError(e_msg)
            else:
                self.vr = _flow_rate

        # Command for sending
        command = "SET,FM={0},VR={1}".format(self.sn, str(self.vr))

        try:
            # Attempt to set flow rate for Flow Meter at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'Flow Rate' to: '({2})'".format(
                    self.sn, str(self.ad), str(self.vr))
            raise Exception(e_msg)
        else:
            print("Successfully set Flow Meter {0} ({1})'s 'Flow Rate' to: ({2})".format(
                  self.sn, str(self.ad), str(self.vr)))

    #################################
    def set_water_usage_on_cn(self, _water_usage=None):
        """
        Set the water usage for the Flow Meter (Faux IO Only) \n
        :param _water_usage:        Value to set the Flow Meter total water used as a float in gallons\n
        :return:
        """
        # If a water usage is passed in for overwrite
        if _water_usage is not None:

            # Verifies the water usage passed in is an int or float value
            if not isinstance(_water_usage, (int, float)):
                e_msg = "Failed trying to set FM {0} ({1})'s Water Usage, invalid argument type, expected a int or " \
                        "float, received: {2}".format(self.sn, str(self.ad), type(_water_usage))
                raise TypeError(e_msg)
            else:
                self.vg = _water_usage

        # Command for sending
        command = "SET,FM={0},VG={1}".format(str(self.sn), str(self.vg))

        try:
            # Attempt to set water usage for Flow Meter at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'water usage' to: '{2}'".format(
                    self.sn, str(self.ad), str(self.vg))
            raise Exception(e_msg)
        else:
            print("Successfully set Flow Meter {0} ({1})'s 'water usage' to: {2}".format(
                  self.sn, str(self.ad), str(self.vg)))

    #################################
    def set_two_wire_drop_value_on_cn(self, _value=None):
        """
        Set the two wire drop value for the Flow Meter (Faux IO Only) \n
        :param _value:        Value to set the Flow Meter two wire drop value to \n
        :return:
        """
        # If a two wire drop value is passed in for overwrite
        if _value is not None:

            # Verifies the two wire drop passed in is an int or float value
            if not isinstance(_value, (int, float)):
                e_msg = "Failed trying to set FM {0} ({1})'s Two Wire Drop, invalid argument type, expected a int or " \
                        "float, received: {2}".format(self.sn, str(self.ad), type(_value))
                raise TypeError(e_msg)
            else:
                self.vt = _value

        # Command for sending
        command = "SET,FM={0},VT={1}".format(str(self.sn), str(self.vt))

        try:
            # Attempt to set two wire drop for Flow Meter at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'Two Wire Drop Value' to: '{2}'".format(
                    self.sn, str(self.ad), str(self.vt))
            raise Exception(e_msg)
        else:
            print("Successfully set Flow Meter {0} ({1})'s 'Two Wire Drop Value' to: {2}".format(
                  self.sn, str(self.ad), str(self.vt)))

    #################################
    def verify_k_value_on_cn(self):
        """
        Verifies k value for this Flow Meter on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        k_value = float(self.data.get_value_string_by_key(opcodes.k_value))

        # Compare status versus what is on the controller
        if self.kv != k_value:
            e_msg = "Unable verify Flow Meter {0} ({1})'s k_value. Received: {2}, Expected: {3}".format(
                    self.sn,        # {0}
                    str(self.ad),   # {1}
                    str(k_value),   # {2}
                    str(self.kv)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Flow Meter {0} ({1})'s k_value: '{2}' on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.kv)      # {2}
                  ))

    #################################
    def verify_flow_rate_on_cn(self):
        """
        Verifies flow rate for this Flow Meter on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        flow_rate = float(self.data.get_value_string_by_key(opcodes.flow_rate))

        # Compare status versus what is on the controller
        if int(self.vr) != int(flow_rate):
            e_msg = "Unable verify Flow Meter {0} ({1})'s flow rate. Received: {2}, Expected: {3}".format(
                    self.sn,            # {0}
                    str(self.ad),       # {1}
                    str(flow_rate),     # {2}
                    str(self.vr)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Flow Meter {0} ({1})'s flow rate: '{2}' on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.vr)      # {2}
                  ))

    #################################
    def verify_water_usage_on_cn(self):
        """
        Verifies water usage for this Flow Meter on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        water_usage = float(self.data.get_value_string_by_key(opcodes.total_usage))

        # Compare status versus what is on the controller
        if self.vg != water_usage:
            e_msg = "Unable verify Flow Meter {0} ({1})'s water usage. Received: {2}, Expected: {3}".format(
                    self.sn,            # {0}
                    str(self.ad),       # {1}
                    str(water_usage),   # {2}
                    str(self.vg)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Flow Meter {0} ({1})'s water usage: '{2}' on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.vg)      # {2}
                  ))

    #################################
    def verify_two_wire_drop_value_on_cn(self):
        """
        Verifies the two wire drop value for this Flow Meter on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        two_wire_drop_val = float(self.data.get_value_string_by_key('VT'))

        # Compare status versus what is on the controller
        if self.vt != two_wire_drop_val:
            e_msg = "Unable verify Flow Meter {0} ({1})'s two wire drop value. Received: {2}, Expected: {3}".format(
                    self.sn,                    # {0}
                    str(self.ad),               # {1}
                    str(two_wire_drop_val),     # {2}
                    str(self.vt)                # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Flow Meter {0} ({1})'s two wire drop value: '{2}' on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.vt)      # {2}
                  ))

    #################################
    def verify_enable_state_on_cn(self):
        """
        Verifies that the enabled state flow meter on the controller. \n
        :return:
        """
        enable_state = self.data.get_value_string_by_key('EN')
        # Compare status versus what is on the controller
        if str(self.en) != str(enable_state):
            e_msg = "Unable verify Flow Meter {0} ({1})'s enabled state. Received: {2}, Expected: {3}".format(
                    self.sn,            # {0}
                    str(self.ad),       # {1}
                    str(enable_state),  # {2}
                    str(self.en)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified flow meter {0} ({1})'s enabled state: '{2}' on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.en)      # {2}
                  ))

    #################################
    def self_test_and_update_object_attributes(self):
        """
        this does a self test on the device
        than does a get date a resets the attributes of the device to match the controller
        self.vr    # Flow rate in Gallons Per Minute
        self.vg    # Flow reported as usage in Total Gallons
        self.vt    # Two Wire Drop
        """
        try:
            self.do_self_test()
            self.get_data()
            self.vr = float(self.data.get_value_string_by_key(opcodes.flow_rate))
            self.vg = float(self.data.get_value_string_by_key(opcodes.total_usage))
            self.vt = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        except Exception as e:
            e_msg = "Exception occurred trying to updating attributes of the flow meter {0} object." \
                    " Flow Rate Value: '{1}'," \
                    " Total Usage Value: '{2}'," \
                    " Two Wire Drop Value: '{3}'" \
                .format(
                    str(self.ad),  # {0}
                    str(self.vr),  # {1}
                    str(self.vg),  # {2}
                    str(self.vt),  # {3}
                )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the flow meter {0} object. "
                  "Flow Rate Value: '{1}', "
                  "Total Usage Value: '{1}', "
                  "Two Wire Drop Value: '{3}' ".format(
                    str(self.ad),  # {0}
                    str(self.vr),  # {1}
                    str(self.vg),  # {2}
                    str(self.vt),  # {3}
                  ))

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Flow Meter'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify base attributes
        self.verify_description_on_cn()
        self.verify_serial_number_on_cn()
        self.verify_latitude_on_cn()
        self.verify_longitude_on_cn()

        if _expected_status is not None:
            self.verify_status_on_cn(status=_expected_status)

        # Verify Flow Meter specific attributes
        self.verify_enable_state_on_cn()
        self.verify_k_value_on_cn()
        # if the device is shared by a substation than do a self test and update object attributes
        if self.is_shared_with_substation:
            self.self_test_and_update_object_attributes()
        self.verify_flow_rate_on_cn()
        self.verify_water_usage_on_cn()
        # self.verify_two_wire_drop_value_on_cn()
