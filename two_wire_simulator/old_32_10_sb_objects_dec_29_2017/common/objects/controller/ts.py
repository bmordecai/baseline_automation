from decimal import Decimal
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices

__author__ = 'tige'


########################################################################################################################
# TEMPERATURE SENSOR
########################################################################################################################
class TemperatureSensor(Devices):
    """
    Temperature Sensor Object \n
    """
    starting_lat = Decimal(Devices.controller_lat)
    starting_long = Decimal(Devices.controller_long) + Decimal('.000800')

    #################################
    def __init__(self, _serial, _address, _shared_with_sb=False):

        # Create description for initialization of Device parent class for inheritance
        description = "{0} Test Temp Sensor {1}".format(_serial, str(_address))

        # create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        # Initialize parent class
        Devices.__init__(self, _sn=_serial, _ds=description, _ad=_address, _la=lat, _lg=lng, is_shared=_shared_with_sb)
        self.dv_type = opcodes.temperature_sensor
        # Temperature Sensor specific attributes
        self.vd = 93.1       # Temperature
        self.vt = 1.7       # Two Wire Drop

    #################################
    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        # Update current description in case we changed address or serial number
        self.ds = self.ds if self.ds == self.build_description() else self.build_description()

        command = "SET,TS={0},DS={1},LA={2},LG={3}".format(
            str(self.sn),   # {0}
            str(self.ds),   # {1}
            str(self.la),   # {2}
            str(self.lg)    # {3}
        )

        # If the device is not shared with the substation, set these values because we own them.
        if not self.is_shared_with_substation:
            command += ",VD={0},VT={1}".format(
                self.vd,
                self.vt
            )

        try:
            # Attempt to set temperature default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Temperature Sensor {0} {1}'s 'Default values' to: '{2}'".format(
                    self.sn, str(self.ad), command)
            raise Exception(e_msg)
        else:
            print("Successfully set Temperature Sensor {0} ({1})'s 'Default values' to: {2}".format(
                  self.sn, str(self.ad), command))

    #################################
    def set_temperature_reading_on_cn(self, _temp=None):
        """
        Set the temperature reading for the Temperature sensor (Faux IO Only) \n
        :param _temp:        Value to set the Temperature sensor temperature reading to \n
        :return:
        """
        # If a temperature value is passed in for overwrite
        if _temp is not None:

            # Verifies the temperature reading passed in is an int or float value
            if not isinstance(_temp, (int, float)):
                e_msg = "Failed trying to set Temperature Sensor {0} ({1})'s temperature reading. Invalid type input " \
                        "as argument. Expected int or float, received: {2}".format(str(self.sn), str(self.ad),
                                                                                   type(_temp))
                raise TypeError(e_msg)
            else:
                self.vd = _temp

        # Command for sending
        command = "SET,TS={0},VD={1}".format(str(self.sn), str(self.vd))

        try:
            # Attempt to set temperature reading for Temperature sensor at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Temperature Sensor {0} ({1})'s 'Temperature Reading' to: " \
                    "'{2}'".format(self.sn, str(self.ad), str(self.vd))
            raise Exception(e_msg)
        else:
            print("Successfully set Temperature Sensor {0} ({1})'s 'Temperature Reading' to: {2}".format(
                  self.sn, str(self.ad), str(self.vd)))

    #################################
    def set_two_wire_drop_value_on_cn(self, _value=None):
        """
        Set the two wire drop value for the Temperature sensor (Faux IO Only) \n
        :param _value:        Value to set the Temperature sensor two wire drop value to \n
        :return:
        """
        # If a two wire drop value is passed in for overwrite
        if _value is not None:

            # Verifies the two wire drop passed in is an int or float value
            if not isinstance(_value, (int, float)):
                e_msg = "Failed trying to set Temperature Sensor {0} ({1})'s two wire drop. Invalid argument type, " \
                        "expected an int or float, received: {2}".format(self.sn, str(self.ad), type(_value))
                raise TypeError(e_msg)
            else:
                self.vt = _value

        # Command for sending
        command = "SET,TS={0},VT={1}".format(str(self.sn), str(self.vt))

        try:
            # Attempt to set two wire drop for Temperature sensor at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Temperature Sensor {0} ({1})'s 'Two Wire Drop Value' to: " \
                    "'{2}'".format(self.sn, str(self.ad), str(self.vt))
            raise Exception(e_msg)
        else:
            print("Successfully set Temperature Sensor {0} ({1})'s 'Two Wire Drop Value' to: {2}".format(
                  self.sn, str(self.ad), str(self.vt)))

    #################################
    def verify_temperature_reading_on_cn(self):
        """
        Verifies the temperature reading for this Temperature Sensor on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        temperature = float(self.data.get_value_string_by_key(opcodes.temperature_value))
        rounded_vd = round(number=self.vd, ndigits=2)
        if abs(rounded_vd - temperature) > 0.1:
            # Compare status versus what is on the controller
            if str(self.vd) != temperature:
                e_msg = "Unable verify Temperature Sensor {0} ({1})'s temperature reading. Received: {2}, Expected: {3}"\
                        .format(
                            self.sn,            # {0}
                            str(self.ad),       # {1}
                            str(temperature),   # {2}
                            str(self.vd)        # {3}
                        )
                raise ValueError(e_msg)
        else:
            print("Verified Temperature Sensor {0} ({1})'s temperature reading: '{2}' on controller"
                  .format(
                      self.sn,          # {0}
                      str(self.ad),     # {1}
                      str(self.vd)      # {2}
                  ))

    #################################
    def verify_two_wire_drop_value_on_cn(self):
        """
        Verifies the two wire drop value for this Temperature Sensor on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        two_wire_drop_val = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        rounded_vt = round(number=self.vt, ndigits=2)
        if abs(rounded_vt - two_wire_drop_val) > 0.02:
            # Compare status versus what is on the controller
            if str(self.vt) != two_wire_drop_val:
                e_msg = "Unable verify Temperature Sensor {0} ({1})'s two wire drop value. Received: {2}, Expected: {3}"\
                        .format(
                            self.sn,                    # {0}
                            str(self.ad),               # {1}
                            str(two_wire_drop_val),     # {2}
                            str(self.vt)                # {3}
                        )
                raise ValueError(e_msg)
        else:
            print("Verified Temperature Sensor {0} ({1})'s two wire drop value: '{2}' on controller"
                  .format(
                      self.sn,          # {0}
                      str(self.ad),     # {1}
                      str(self.vt)      # {2}
                  ))

    #################################
    def self_test_and_update_object_attributes(self):
        """
        this does a self test on the device
        then does a get date a resets the attributes of the device to match the controller
        self.vd       # Temperature
        self.vt       # Two Wire Drop
        """
        try:
            self.do_self_test()
            self.get_data()
            self.vd = float(self.data.get_value_string_by_key(opcodes.temperature_value))
            self.vt = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        except Exception as e:
            e_msg = "Exception occurred trying to updating attributes of the temperature sensor {0} object." \
                    " Temperature Value: '{1}'," \
                    " Two Wire Drop Value: '{2}'" \
                .format(
                    str(self.ad),  # {0}
                    str(self.vd),  # {1}
                    str(self.vt),  # {2}
                )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the temperature sensor {0} object. "
                  "Temperature Value: '{1}', "
                  "Two Wire Drop Value: '{2}' " .format(
                    str(self.ad),  # {0}
                    str(self.vd),  # {1}
                    str(self.vt),  # {2}
                  ))

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Flow Meter'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()
        #TODO need to verify serial number
        #TODO need to verify enabled sate
        # Verify base attributes
        self.verify_description_on_cn()
        self.verify_serial_number_on_cn()
        self.verify_latitude_on_cn()
        self.verify_longitude_on_cn()
        if _expected_status is not None:
            self.verify_status_on_cn(status=_expected_status)

        # if the device is shared by a substation than do a self test and update object attributes
        if self.is_shared_with_substation:
            self.self_test_and_update_object_attributes()

        # Verify Temperature Sensor specific attributes
        self.verify_temperature_reading_on_cn()
        self.verify_two_wire_drop_value_on_cn()
