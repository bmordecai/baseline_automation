from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from decimal import Decimal
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices

__author__ = 'tige'


########################################################################################################################
# Master Valve
########################################################################################################################
class MasterValve(Devices):
    """
    Master Valve Object \n
    """

    starting_lat = Decimal(Devices.controller_lat)
    starting_long = Decimal(Devices.controller_long) + Decimal('.000600')

    #################################
    def __init__(self, _serial, _address, _shared_with_sb=False):
        # Create description for master valve instance
        description = "{0} Test Master Valve {1}".format(str(_serial), str(_address))

        # create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        # Init parent class to inherit respective attributes
        Devices.__init__(self, _sn=_serial, _ds=description, _ad=_address, _la=lat, _lg=lng, is_shared=_shared_with_sb)

        # Set device type to inheriting class type (aka, set it to "MV" for Master Valve object)
        self.dv_type = 'MV'

        # Zone specific attributes for both controllers
        self.en = 'TR'            # Enabled / Disabled (TR | FA) default to TR
        self.no = 'FA'            # Normally Open (TR | FA) default to FA
        self.vt = 0.0           # Two Wire Drop

        # 3200 Specific attributes
        self.bp = 'FA'            # Booster enable (TR | FA)

    #################################
    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        # Update current description in case we changed address or serial number
        self.ds = self.ds if self.ds == self.build_description() else self.build_description()

        command = "SET,MV={0},EN={1},DS={2},LA={3},LG={4},NO={5}".format(
            str(self.sn),   # {0}
            str(self.en),   # {1}
            str(self.ds),   # {2}
            str(self.la),   # {3}
            str(self.lg),   # {4}
            str(self.no)    # {5}
        )

        # If the device is not shared with the substation, set these values because we own them.
        if not self.is_shared_with_substation:
            command += ",{0}={1}".format(
                opcodes.two_wire_drop,
                self.vt
            )

        if self.controller_type == "32":
            bp_string = ",{0}={1}".format(
                opcodes.booster_pump,
                str(self.bp)
            )
            command += bp_string
        try:
            # Attempt to set event switch default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Master Valve {0} ({1})'s 'Default values' to: '{2}'".format(
                    self.sn, str(self.ad), command)
            raise Exception(e_msg)
        else:
            print("Successfully set Master Valve {0} ({1})'s 'Default values' to: {2}".format(
                  self.sn, str(self.ad), command))

    #################################
    def set_enable_state_on_cn(self, _state=None):
        """
        Enables the master valve on the controller. \n
        :return:
        """
        if _state is not None:

            # If master valve isn't enabled, make sure to update instance variable to match what the controller will
            # show.
            if _state not in ['TR', 'FA']:
                e_msg = "Invalid enabled state for Master Valve {0} ({1}). Received: {2}, expected: 'TR' or " \
                        "'FA'".format(str(self.sn), str(self.ad), str(_state))
                raise ValueError(e_msg)
            else:
                self.en = _state

        # Command for sending
        command = "SET,MV={0},EN={1}".format(str(self.sn), self.en)

        try:
            # Attempt to enable master valve
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set enabled state for master valve {0} ({1})".format(
                    self.sn, str(self.ad))
            raise Exception(e_msg)
        else:
            print("Successfully set Master Valve {0} ({1})'s 'Enabled State' to: {2}".format(self.sn,
                                                                                             str(self.ad),
                                                                                             str(self.en)))

    #################################
    def set_normally_open_state_on_cn(self, _normally_open=None):
        """
        Sets the normally open attribute of Master Valve to true on the controller. \n
        :return:
        """
        # If a different normally open state specified
        if _normally_open is not None:

            # Verify valid states
            if _normally_open not in ['TR', 'FA']:
                e_msg = "Invalid normally open state for Master Valve {0} ({1}). Received: {2}, expected: 'TR' or " \
                        "'FA'".format(str(self.sn), str(self.ad), str(_normally_open))
                raise ValueError(e_msg)
            else:
                self.no = _normally_open

        # If current master valve instance is not set to true or doesn't have a value, set the value for the object.
        # Command for sending
        command = "SET,MV={0},NO={1}".format(self.sn, self.no)

        try:
            # Attempt to set master valve's normally open attribute at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Master Valve {0} ({1})'s normally open to {2}".format(
                self.sn, str(self.ad), str(_normally_open))
            raise Exception(e_msg)
        else:
            print("Successfully set Master Valve {0} ({1})'s 'Normally Open State' to: {2}".format(
                self.sn, str(self.ad), str(self.no)))

    #################################
    def set_two_wire_drop_value_on_cn(self, _value=None):
        """
        Set the two wire drop value for the master valve (Faux IO Only) \n
        :param _value:        Value to set the moisture sensor two wire drop value to \n
        :return:
        """
        # If a two wire drop value is passed in for overwrite
        if _value is not None:

            # Verifies the two wire drop passed in is an int or float value
            if not isinstance(_value, (int, float)):
                e_msg = "Failed trying to set Master Valve {0} ({1})'s two wire drop. Invalid number type, " \
                        "expected an int or float, received: ({2})".format(self.sn, str(self.ad), type(_value))
                raise TypeError(e_msg)
            else:
                self.vt = _value

        # Command for sending
        command = "SET,MV={0},VT={1}".format(str(self.sn), str(self.vt))

        try:
            # Attempt to set two wire drop for master valve at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Master Valve {0} ({1})'s 'Two Wire Drop Value' to: '{2}'".format(
                    str(self.sn), str(self.ad), str(self.vt))
            raise Exception(e_msg)
        else:
            print("Successfully set Master Valve {0} ({1})'s 'Two Wire Drop Value' to: {2}".format(
                str(self.sn), str(self.ad), str(self.vt)))

    #################################
    def set_booster_enable_state_on_cn(self, booster_state=None):
        """
        Enables the master valve on the controller. \n
        :return:
        """
        if booster_state is not None:

            # If booster isn't enabled, make sure to update instance variable to match what the controller will show.
            if booster_state not in ['TR', 'FA']:
                e_msg = "Invalid booster pump enable state for Master Valve {0} ({1}). Received: {2}, expected: 'TR' " \
                        "or 'FA'".format(str(self.sn), str(self.ad), str(booster_state))
                raise ValueError(e_msg)
            else:
                self.bp = booster_state

        # Command for sending
        command = "SET,MV={0},BP={1}".format(str(self.sn), self.bp)

        try:
            # Attempt to enable booster state
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Master Valve {0} ({1})'s 'Booster Pump State' to: {2}".format(
                    self.sn, str(self.ad), str(booster_state))
            raise Exception(e_msg)
        else:
            print("Successfully set Master Valve {0} ({1})'s 'Booster Pump State' to: {2}".format(
                  self.sn, str(self.ad), str(self.bp)))

    #################################
    def verify_normally_open_state_on_cn(self):
        """
        Verifies that the Master Valve's attribute normally open matches what is expected on the controller. \n
        :return:
        """
        # Get the normally open  state value from controller
        no_on_cn = self.data.get_value_string_by_key('NO')

        # Compare status versus what is on the controller
        if self.no != no_on_cn:
            e_msg = "Unable verify Master Valve {0} ({1})'s normally open state. Received: {2}, Expected: {3}".format(
                    self.sn,        # {0}
                    str(self.ad),   # {1}
                    no_on_cn,       # {2}
                    self.no)        # {3}
            raise ValueError(e_msg)
        else:
            print("Verified  Master Valve {0} ({1})'s normally open state on controller: {2}".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  self.no           # {2}
                  ))

    #################################
    def verify_enable_state_on_cn(self):
        """
        Verifies that the enabled state master valve on the controller. \n
        :return:
        """
        enable_state = self.data.get_value_string_by_key('EN')

        # Compare status versus what is on the controller
        if self.en != enable_state:
            e_msg = "Unable verify master valve {0} ({1})'s enabled state. Received: {2}, Expected: {3}".format(
                    self.sn,            # {0}
                    str(self.ad),       # {1}
                    str(enable_state),  # {2}
                    str(self.en)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified master valve {0} ({1})'s state: '{2}' on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.en)      # {2}
                  ))

    #################################
    def verify_booster_enable_state_on_cn(self):
        """
        Verifies that the booster enabled state on the controller. \n
        :return:
        """
        booster_enable_state = self.data.get_value_string_by_key(opcodes.booster_pump)

        # Compare status versus what is on the controller
        if self.bp != booster_enable_state:
            e_msg = "Unable verify Master Valve {0} ({1})'s Booster pump enabled state. Received: {2}, Expected: " \
                    "{3}".format(
                        self.sn,                    # {0}
                        str(self.ad),               # {1}
                        str(booster_enable_state),  # {2}
                        str(self.bp)                # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Master Valve {0} ({1})'s 'Booster Enabled State': {2} on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.bp)      # {2}
                  ))

    #################################
    def verify_two_wire_drop_value_on_cn(self):
        """
        Verifies the two wire drop value for this Master Valve on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        two_wire_drop_val = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))

        # Compare status versus what is on the controller
        if self.vt != two_wire_drop_val:
            e_msg = "Unable verify master valve {0} ({1})'s two wire drop value. Received: {2}, Expected: {3}".format(
                    self.sn,                    # {0}
                    str(self.ad),               # {1}
                    str(two_wire_drop_val),     # {2}
                    str(self.vt)                # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified master valve {0} ({1})'s two wire drop value: '{2}' on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.vt)      # {2}
                  ))

    #################################
    def self_test_and_update_object_attributes(self):
        """
        this does a self test on the device
        than does a get date a resets the attributes of the device to match the controller
        self.vt         # Two wire drop
        """
        try:
            self.do_self_test()
            self.get_data()
            self.vt = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        except Exception as e:
            e_msg = "Exception occurred trying to updating attributes of the master valve {0} object." \
                    " Two Wire Drop Value: '{1}'" \
                .format(
                    str(self.ad),  # {0}
                    str(self.vt)  # {1}
                )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the master valve {0} object. "
                  "Two Wire Drop Value: '{1}' ".format(
                    str(self.ad),  # {0}
                    str(self.vt)  # {1}
                    ))

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Flow Meter'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify base attributes
        self.verify_description_on_cn()
        self.verify_serial_number_on_cn()
        self.verify_latitude_on_cn()
        self.verify_longitude_on_cn()

        # if the device is shared by a substation than do a self test and update object attributes
        if self.is_shared_with_substation:
            self.self_test_and_update_object_attributes()

        self.verify_two_wire_drop_value_on_cn()
        if _expected_status is not None:
            self.verify_status_on_cn(status=_expected_status)

        # Verify Master Valve specific attributes
        self.verify_enable_state_on_cn()
        self.verify_normally_open_state_on_cn()

        # 3200 controller specific attributes for Master Valves
        if self.controller_type == "32":
            self.verify_booster_enable_state_on_cn()

