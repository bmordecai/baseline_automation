# Our modules
from old_32_10_sb_objects_dec_29_2017.common.variables import common as common_vars
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages

# Python libraries
from datetime import datetime, timedelta

__author__ = 'bens'


class BiCoderTypes(object):
    """
    Enumeration Class for string representations of the type of BiCoder.
    """
    VALVE = 'Valve BiCoder'
    FLOW = 'Flow BiCoder'
    TEMPERATURE = 'Temperature BiCoder'
    MOISTURE = 'Moisture BiCoder'
    SWITCH = 'Switch BiCoder'
    PUMP = 'Pump BiCoder'
    FOUR_TO_TWENTY_MA = '4 to 20 mA BiCoder'
    ALERT = 'Alert Relay BiCoder'


class BiCoderIdentifiers(object):
    """
    Enumeration Class for string representations of the identifiers of a BiCoder.
    """
    VALVE = 'ZN'
    FLOW = 'FM'
    TEMPERATURE = 'TS'
    MOISTURE = 'MS'
    SWITCH = 'SW'
    PUMP = 'Pump BiCoder'  # TODO find out what object this is used for
    FOUR_TO_TWENTY_MA = 'IS'
    ALERT = 'AR'


class BiCoder(object):
    """
    Parent Class for other BiCoder object's to inherit from. \n
    """

    bicoder_types = BiCoderTypes
    bicoder_ids = BiCoderIdentifiers

    def __init__(self, _serial_number, _serial_connection):
        """
        :param _serial_number: Serial number to set for BiCoder.
        :type _serial_number: str
        
        :param _serial_connection: Serial connection from substation
        :type _serial_connection: common.objects.base_classes.ser.Ser
        """
        self.sn = _serial_number        # Serial Number for BiCoder
        self.ser = _serial_connection   # Serial Port connection instance for BiCoder to communicate over
        self.ty = None                  # BiCoder "TYPE" from BiCoderTypes class.
        self.id = ''                    # The identifier of the BiCoder

        # Shard BiCoder Attributes
        self.vt = 1.7   # Two wire drop

        # Shared Read Only Attributes
        self.ss = ''
        self.controller_type = opcodes.substation

        # Attributes that we will send to the substation by default
        self.default_attributes = [
            (opcodes.two_wire_drop, 'vt')
        ]

        # Attribute so we can store the messages
        self.build_message_string = ''  # place to store the message that is compared to the substation message

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_default_values(self):
        """
        Set the default values of the device on the substation. \n
        :return:
        :rtype:
        """
        # Build the basic command that will be added on to
        command = "SET,{0}={1}".format(
            self.id,        # {0} Identifier for the biCoder (ex: 'ZN', 'FM', etc.)
            str(self.sn),   # {1} Serial number for the biCoder
        )

        # Loops through the attribute list and adds the values to our string that we will send to the substation
        for attribute_tuple in self.default_attributes:
            # attribute_tuple[0] is the name of the attribute in the string we send to the substation
            attribute_name = attribute_tuple[0]

            # attribute_tuple[1] is the attribute for the device to update
            bicoder_attribute = getattr(self, attribute_tuple[1])

            # Add the attribute to the command string
            command += ",{0}={1}".format(attribute_name, bicoder_attribute)

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Default values' to: '{2}'".format(
                self.ty,        # {0}
                str(self.sn),   # {0}
                command         # {1}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Default values' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                command)        # {2}
            )

    #################################
    def set_two_wire_drop_value(self, _value=None):
        """
        Set the two wire drop value for the BiCoder (Faux IO Only) \n
        By passing in a value for '_value', method assumes this value to overwrite current value. \n

        :param _value: Value to set the Zone two wire drop value to \n
        :type _value: int, float
        """
        # Check if user wants to overwrite current value
        if _value is not None:

            # Verify the correct type is passed in.
            if not isinstance(_value, (int, float)):
                e_msg = "Failed trying to set BiCoder {0}'s two wire drop value. Invalid type passed in, expected " \
                        "int or float. Received type: {1}".format(
                            str(self.sn),   # {0}
                            type(_value)    # {1}
                        )
                raise TypeError(e_msg)
            else:
                # Valid type, overwrite current value with new value
                self.vt = _value

        # Command for sending
        command = "SET,{0}={1},VT={2}".format(
            str(self.id),   # {0}
            str(self.sn),   # {1}
            str(self.vt)    # {2}
        )

        try:
            # Attempt to set two wire drop for zone at the substation
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} {1}'s 'Two Wire Drop Value' to: '{2}'".format(
                str(self.ty), str(self.sn), str(self.vt))
            raise Exception(e_msg)
        else:
            print("Successfully set {0} {1}'s 'Two Wire Drop Value' to: {2}".format(str(
                self.ty), str(self.sn), str(self.vt)))

    #################################
    def set_message(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        :param _helper_object
        :type _helper_object : BaseStartStopPause
        """
        try:
            build_message_string = messages.message_to_set(self,
                                                           _status_code=_status_code,
                                                           _ct_type=self.id,
                                                           _helper_object=_helper_object)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.set_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=build_message_string)

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_two_wire_drop_value(self, _data=None):
        """
        Verifies the two wire drop value for this BiCoder on the Substation. Expects the substations's value and this
        instance's value to be equal. \n

        We round the value from the substation to the hundredth place (.01). We then allow a variance of .2 units
        between our substation value and object value to account for the rounding that takes place in the substation. \n

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Compare status versus what is on the substation
        two_wire_drop_val = float(data.get_value_string_by_key('VT'))
        rounded_vt = round(number=self.vt, ndigits=2)
        if abs(rounded_vt - two_wire_drop_val) > 0.2:
            e_msg = "Unable to verify {0} {1}'s two wire drop value. Received: {2}, Expected: {3}".format(
                str(self.ty),               # {0}
                str(self.sn),               # {1}
                str(two_wire_drop_val),     # {2
                str(self.vt)                # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s two wire drop value: '{2}' on substation".format(
                str(self.ty),     # {0}
                str(self.sn),     # {1}
                str(self.vt)      # {2}
            ))
            return True

    #################################
    def verify_serial_number(self, _data=None):
        """
        Verifies the Serial Number set on the BiCoder. \n

        :param _data: Data Object that holds the substation's attributes. If it isn't passed in we call get_data \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Get the serial number from the data object
        sn_on_sb = data.get_value_string_by_key('SN')

        # Compare Serial Numbers
        if self.sn != sn_on_sb:
            e_msg = "Unable to verify {0} ({1})'s 'Serial Number'. Received: {2}, Expected: {3}".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(sn_on_sb),  # {2}
                str(self.sn)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0}'s ({1}) Serial Number: '{2}' on substation".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(self.sn)    # {2}
            ))
            return True

    #################################
    def verify_status(self, _status, _data=None):
        """
        Verifies the status on the substation for the specified biCoder type. \n

        :param _status: Status that we expect to be on the substation. \n
        :type _status: str

        :param _data: Data Object that holds the substation's attributes. If it isn't passed in we call get_data \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        if _status not in common_vars.dictionary_for_status_codes:
            e_msg = "Unable to verify {0} ({1})'s status. Received invalid expected status: ({2}),\n" \
                    "Expected one of the following: ({3})".format(
                        str(self.ty),                                   # {0}
                        str(self.sn),                                   # {1}
                        str(_status),                                   # {2}
                        common_vars.dictionary_for_status_codes.keys()  # {3}
                    )
            raise KeyError(e_msg)

        # Get the actual status from our data object
        dv_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

        # Since status is a 'get' only, we want to overwrite our object with the expected status passed in to retain
        # the state of our object.
        self.ss = _status

        # Compare status versus what is on the substation
        if self.ss != dv_ss_on_cn:
            e_msg = "Unable to verify {0} ({1})'s status. Received: {2}, Expected: {3}".format(
                self.ty,            # {0}
                self.sn,            # {1}
                str(dv_ss_on_cn),   # {2}
                str(self.ss)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} ({1})'s status: '{2}' on substation".format(
                self.ty,    # {0}
                self.sn,    # {1}
                self.ss     # {2}
            ))
            return True

    #################################
    def verify_message(self, _status_code, _helper_object=None):
        """
        The expected values are retrieved from the substation and than compared against the values that are returned
        from the message module. \n

        :param _status_code: The status code we will pass to the substation
        :type _status_code: str

        :param _helper_object:
        :type _helper_object:
        :return:
        """
        mg_on_sb = self.get_message(_status_code=_status_code, _helper_object=_helper_object)
        expected_message_text_from_sb = mg_on_sb.get_value_string_by_key(opcodes.message_text)
        expected_date_time_from_sb = mg_on_sb.get_value_string_by_key(opcodes.date_time)

        mess_date_time = datetime.strptime(self.build_message_string[opcodes.date_time], '%m/%d/%y %H:%M:%S')
        substation_date_time = datetime.strptime(expected_date_time_from_sb, '%m/%d/%y %H:%M:%S')
        if mess_date_time != substation_date_time:
            if abs(substation_date_time - mess_date_time) >= timedelta(days=0, hours=0, minutes=60, seconds=0):
                e_msg = "The date and time of the message didn't match the substation:\n" \
                        "\tCreated: \t\t'{0}'\n" \
                        "\tReceived:\t\t'{1}'\n".format(
                            self.build_message_string[opcodes.date_time],  # {0} The date message that was built
                            expected_date_time_from_sb  # {1} The date message returned from substation
                        )
                raise ValueError(e_msg)
            # only print this if they were not exact
            e_msg = "############################  Date and time were not exact but were within +- 30 minutes \n" \
                    "############################  Substation Date and Time = {0} \n" \
                    "############################  Message Date time = {1}".format(
                        expected_date_time_from_sb,  # {0} Date time received from substation
                        self.build_message_string[opcodes.date_time]  # {1} Date time the message was verified
                    )
            print(e_msg)

        if self.build_message_string[opcodes.message_text] != expected_message_text_from_sb:
            e_msg = "Created TX message did not match the TX received from the substation:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_text],  # {0} The TX message that was built
                        expected_message_text_from_sb  # {1} The TX message returned from substation
                    )
            raise ValueError(e_msg)
        else:
            print "Successfully verified:\n" \
                  "\tID: '{0}'\n" \
                  "\tDT: '{1}'\n" \
                  "\tTX: '{2}'\n".format(
                        self.build_message_string[opcodes.message_id],
                        self.build_message_string[opcodes.date_time],
                        self.build_message_string[opcodes.message_text]
                   )

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def get_message(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module

        :param _status_code
        :type _status_code :str

        :param _helper_object
        :type _helper_object: BaseStartStopPause
        """
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=self.id,
                                                                _helper_object=_helper_object)
            sb_mg = self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            msg = "Exception caught in messages.get_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=self.build_message_string[opcodes.message])
            return sb_mg

    #################################
    def get_data(self):
        """
        Gets the data for the Substation and returns it as an object we can parse through. \n

        :return: status_parser.KeyValues
        """
        # Build the command to get data from the Substation
        command = "GET,{0}={1}".format(
            self.id,    # {0}
            self.sn     # {1}
        )

        # Attempt to get data from substation
        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Unable to get data for {0} ({1}) using command: '{2}'. Exception raised: {3}".format(
                self.ty,            # {0}
                self.sn,            # {1}
                command,            # {2}
                str(ae.message)     # {3}
            )
            raise ValueError(e_msg)
        else:
            return data
