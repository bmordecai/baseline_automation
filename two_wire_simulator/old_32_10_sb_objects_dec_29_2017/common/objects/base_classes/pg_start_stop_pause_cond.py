from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from decimal import Decimal
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.ser import Ser
from old_32_10_sb_objects_dec_29_2017.common.imports.types import StartStopPauseCondition, StartStopPauseEvent
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import temperature_sensors, event_switches, moisture_sensors
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages

__author__ = 'ben'


#
# Base Class: StartStopPause Condition object
#
class BaseStartStopPause(object):
    """
    Creates a condition that causes a program to start/stop/pause.  \n

    Usage (must be done in this sequence): \n
        - creating start condition instance: \n
            new_instance_name = StartConditionFor1000(program_ad=1) \n

        - sending the programming to the controller: \n
            new_instance_name.set_moisture_condition_on_pg(serial_number="XXX", mode="XXX", threshold="XXX") \n

    Note: Only one of each type of condition can be set with one of each type: \n
        i.e. you can have one start condition for a moisture sensor and one start condition for a moisture sensor,
         but you can't have two start conditions for a moisture sensor. \n
    """

    # Serial Instance
    ser = Ser.serial_conn

    def __init__(self, program_ad, event_type, cn_type):
        """

        :param program_ad: The program that will be attached to this condition \n
        :type program_ad: int \n
        :param event_type: Either a start stop or pause condition ('PP' for stop | 'PS' for pause | 'PT' for start) \n
        :type event_type: str \n
        :param cn_type: The type of controller ('10' | '32') \n
        :type cn_type: str \n
        """
        # -------------------------------------- #
        # Init all condition common to arguments #
        # -------------------------------------- #
        self._program_ad = program_ad
        self._event_type = event_type
        self._cn_type = cn_type

        self._device_type = None

        # Regardless of what device is passed in, these attribute are pretty global
        self._device_serial = None  # The serial number of the device that will be doing to setting
        self._mode = None  # For moisture or temp sensor: (OF | UL | LL), for event switch: (OF | OP | CL)
        self._threshold = None  # For temperature and moisture sensor only
        self._pause_time = None  # Temp and moisture sensors only have this attribute for 32, even switch always has it

        # # for 1000's only
        # self._ty = None         # event type

        # ---------------------------------------------- #
        # Init all condition specific attributes to None #
        # ---------------------------------------------- #
        # STOP
        self._si = None         # Stop immediately: TR | FA

        # PAUSE

        # START
        # 1000 ONLY START:
        self._cc = None  # Calibration cycle
        self._dt = None  # Start on date & time (i.e., 'TR'=opcodes.true | 'FA'=opcodes.false)
        self._ci = None  # Calendar interval (i.e., Even='EV', Odd='OD', Odd Skip 31='OS')
        self._di = None  # Day Interval (i.e., 1 to xx days)
        self._wd = None  # Week days: binary days of week SUN - SAT (i.e., '0111110'=water only on weekdays)
        self._smi = None  # Semi-month Interval (i.e, a list with 24 int values)
        self._st = None  # Start times - minutes past midnight (i.e., 8:00am = 480)
                         # If multiple start times, input as list: [480, 540, 600]=[8am, 9am, 10am]

        # Interval type (1000 ONLY) - only gets set if user add's a date/time attribute to start condition
        self._interval_ty = None

        # Place Holder for program data
        self.data = None

        # Holds the messages return value
        self.build_message_string = ''

    def set_moisture_condition_on_pg(self, serial_number, mode, threshold=None, si=None, pt=None, dt=None, cc=None):
        """
        This method creates a start/stop/pause condition string that depends on a moisture sensor \n
        :param serial_number: The serial number of the moisture sensor you will be passing in \n
        :type serial_number: str \n
        :param mode: The type of thing being set ('OF' | 'LL' | 'UL') \n
        :type mode: str \n
        :param threshold: The number to set the threshold at \n
        :type threshold: int \n
        :param si: Whether or not you want the program to stop immediately ('TR' | 'FA') \n
        :type si: str \n
        :param pt: The amount of time in minutes to pause the program during a pause command \n
        :type pt: int \n
        :param dt: the variable to include date time as well as condition only a 1000 option ('TR' | 'FA') \n
        :type dt: str \n
        :param cc: this sets the calibration to start on next scheduled start time pass in a start or stop \n
        :type cc: str \n
        :return: str (start/stop/pause string that can be sent to the controller) \n
        """
        # Resets all attributes every call
        BaseStartStopPause.__init__(self, program_ad=self._program_ad, event_type=self._event_type, cn_type=self._cn_type)
        if self._cn_type == '10':
            base_str = "SET,{0}={1},{2}={3},{4}={5}".format(
                self._event_type,
                self._program_ad,
                opcodes.type,
                opcodes.moisture_sensor,
                opcodes.moisture_sensor,
                serial_number
            )
        else:
            base_str = "SET,{0}={1},{2}={3}".format(
                self._event_type,
                self._program_ad,
                opcodes.moisture_sensor,
                serial_number
            )

        self._device_serial = serial_number
        self._device_type = opcodes.moisture_sensor

        # Check to make sure the values that passed in are valid. If an upper or lower limit is passed in, make sure a
        # threshold limit is also passed in
        if mode in [opcodes.upper_limit, opcodes.lower_limit] and threshold is None:
            e_msg = "Since the mode that was passed in was '{0}', a threshold value is expected. Please pass in a " \
                    "threshold value when using this method.".format(mode)
            raise ValueError(e_msg)
        elif mode in [opcodes.upper_limit, opcodes.lower_limit] and threshold is not None:
            pass
        elif mode == opcodes.off:
            pass
        else:
            e_msg = "The 'mode' variable that was passed in is not valid. Expected: '{0}', '{1}', or '{2}'. " \
                    "Received: '{3}'".format(opcodes.off, opcodes.upper_limit, opcodes.lower_limit, mode)
            raise ValueError(e_msg)
        if mode == opcodes.off:
            base_str += ",{0}={1}".format(
                opcodes.moisture_mode,
                mode
            )
            self._mode = mode
        else:
            base_str += ",{0}={1},{2}={3}".format(
                opcodes.moisture_mode,
                mode,
                opcodes.moisture_trigger_threshold,
                threshold
            )
            self._mode = mode
            self._threshold = threshold

        if self._event_type == opcodes.program_stop_condition and si in [opcodes.true, opcodes.false]:
            base_str += ",{0}={1}".format(opcodes.stop_immediately, si)
            self._si = si

        if self._event_type == opcodes.program_pause_condition:
            if pt is not None and self._cn_type == "32":
                pt_in_seconds = pt * 60
                base_str += ",{0}={1}".format(opcodes.moisture_pause_time, pt)
                self._pause_time = pt

        if self._event_type == opcodes.program_start_condition and self._cn_type == '10' and dt in [opcodes.true, opcodes.false]:
            base_str += ",{0}={1}".format(opcodes.date_time, dt)
            self._dt = dt
        # the  1000 only has one type of calibration cycle
        if self._event_type == opcodes.program_start_condition and self._cn_type == '10' and cc is not None:
            base_str += ",{0}={1}".format(opcodes.calibrate_cycle, cc)
            self._cc = cc

        try:
            # Attempt to set program default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=base_str)
        except Exception as e:
            e_msg = "Exception occurred trying to set moisture condition for\nCN: {0}\nevent type: {1}" \
                    "\ncommand: {2}".format(
                        self._cn_type,
                        self._event_type,
                        base_str
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set moisture condition for\nCN: {0}\nevent type: {1}\ncommand: {2}".format(
                self._cn_type,
                self._event_type,
                base_str
            ))

    def set_temperature_condition_on_pg(self, serial_number, mode, threshold=None, si=None, pt=None, dt=None):
        """
        This method creates a start/stop/pause condition string that depends on a temperature sensor \n
        :param serial_number: The serial number of the temperature sensor you will be passing in \n
        :type serial_number: str \n
        :param mode: The type of thing being set ('OF' | 'LL' | 'UL') \n
        :type mode: str \n
        :param threshold: The number to set the threshold at \n
        :type threshold: int \n
        :param si: Whether or not you want the program to stop immediately ('TR' | 'FA') \n
        :type si: str \n
        :param pt: The amount of time in minutes to pause the program during a pause command \n
        :type pt: int \n
        :param dt: the variable to include date time as well as condition only a 1000 option ('TR' | 'FA') \n
        :type dt: str \n
        :return: str (start/stop/pause string that can be sent to the controller) \n
        """
        # Resets all attributes every call
        BaseStartStopPause.__init__(self, program_ad=self._program_ad, event_type=self._event_type, cn_type=self._cn_type)
        if self._cn_type == '10':
            base_str = "SET,{0}={1},{2}={3},{4}={5}".format(
                self._event_type,
                self._program_ad,
                opcodes.type,
                opcodes.temperature_sensor,
                opcodes.temperature_sensor,
                serial_number
            )
        else:
            base_str = "SET,{0}={1},{2}={3}".format(
                self._event_type,
                self._program_ad,
                opcodes.temperature_sensor,
                serial_number
            )

        self._device_serial = serial_number
        self._device_type = opcodes.temperature_sensor

        # Check to make sure the values that passed in are valid. If an upper or lower limit is passed in, make sure a
        # threshold limit is also passed in
        if mode in [opcodes.upper_limit, opcodes.lower_limit] and threshold is None:
            e_msg = "Since the mode that was passed in was '{0}', a threshold value is expected. Please pass in a " \
                    "threshold value when using this method.".format(mode)
            raise ValueError(e_msg)
        elif mode in [opcodes.upper_limit, opcodes.lower_limit] and threshold is not None:
            pass
        elif mode == opcodes.off:
            pass
        else:
            e_msg = "The 'mode' variable that was passed in is not valid. Expected: '{0}', '{1}', or '{2}'. " \
                    "Received: '{3}'".format(
                        opcodes.off,
                        opcodes.upper_limit,
                        opcodes.lower_limit,
                        mode
                    )
            raise ValueError(e_msg)
        if mode == opcodes.off:
            base_str += ",{0}={1}".format(
                opcodes.temperature_sensor_mode,
                mode
            )
            self._mode = mode
        else:
            base_str += ",{0}={1},{2}={3}".format(
                opcodes.temperature_sensor_mode,
                mode,
                opcodes.temperature_trigger_threshold,
                threshold
            )
            self._mode = mode
            self._threshold = threshold

        if self._event_type == opcodes.program_stop_condition and si in [opcodes.true, opcodes.false]:
            base_str += ",{0}={1}".format(opcodes.stop_immediately, si)
            self._si = si

        if self._event_type == opcodes.program_pause_condition:
            if pt is not None and self._cn_type == "32":
                pt_in_seconds = pt * 60
                base_str += ",{0}={1}".format(opcodes.temperature_pause_time, pt_in_seconds)
                self._pause_time = pt

        if self._event_type == opcodes.program_start_condition and self._cn_type == '10' \
                and dt in [opcodes.true, opcodes.false]:
            base_str += ",{0}={1}".format(opcodes.date_time, dt)
            self._dt = dt

        try:
            # Attempt to set program default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=base_str)
        except Exception as e:
            e_msg = "Exception occurred trying to set temperature condition for\nCN: {0}\nevent type: {1}" \
                    "\ncommand: {2}".format(
                        self._cn_type,
                        self._event_type,
                        base_str
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set temperature condition for\nCN: {0}\nevent type: {1}\ncommand: {2}".format(
                self._cn_type,
                self._event_type,
                base_str
            ))

    def set_event_switch_condition_on_pg(self, serial_number, mode, si=None, pt=None, dt=None):
        """
        This method creates a start/stop/pause condition string that depends on an event switch \n
        :param serial_number: The serial number of the event switch you will be passing in \n
        :type serial_number: str \n
        :param mode: The type of thing being set ('OF' | 'OP' | 'CL') \n
        :type mode: str \n
        :param si: Whether or not you want the program to stop immediately ('TR' | 'FA') \n
        :type si: str \n
        :param pt: The amount of time in minutes to pause the program during a pause command \n
        :type pt: int \n
        :param dt: the variable to include date time as well as condition only a 1000 option ('TR' | 'FA') \n
        :type dt: str \n
        :return: str (start/stop/pause string that can be sent to the controller) \n
        """
        # Resets all attributes every call
        BaseStartStopPause.__init__(self, program_ad=self._program_ad, event_type=self._event_type, cn_type=self._cn_type)
        # Check to make sure the values that passed in are valid. If an upper or lower limit is passed in, make sure a
        # threshold limit is also passed in
        if mode not in [opcodes.contacts_open, opcodes.contacts_closed, opcodes.off]:
            e_msg = "The 'mode' variable that was passed in is not valid. Expected: '{0}', '{1}', or '{2}'. " \
                    "Received: '{3}'".format(
                        opcodes.off,
                        opcodes.contacts_open,
                        opcodes.contacts_closed,
                        mode
                    )
            raise ValueError(e_msg)

        if self._cn_type == '10':
            base_str = "SET,{0}={1},{2}={3},{4}={5},{6}={7}".format(
                self._event_type,
                self._program_ad,
                opcodes.type,
                opcodes.event_switch,
                opcodes.event_switch,
                serial_number,
                opcodes.switch_mode,
                mode
            )
        else:
            base_str = "SET,{0}={1},{2}={3},{4}={5}".format(
                self._event_type,
                self._program_ad,
                opcodes.event_switch,
                serial_number,
                opcodes.switch_mode,
                mode
            )

        self._device_serial = serial_number
        self._mode = mode
        self._device_type = opcodes.event_switch

        if self._event_type == opcodes.program_stop_condition and si in [opcodes.true, opcodes.false]:
            base_str += ",{0}={1}".format(opcodes.stop_immediately, si)
            self._si = si

        if self._event_type == opcodes.program_pause_condition and pt is not None:
            pt_in_seconds = pt * 60
            base_str += ",{0}={1}".format(opcodes.switch_pause_time, pt_in_seconds)
            self._pause_time = pt

        if self._event_type == opcodes.program_start_condition and self._cn_type == '10' \
                and dt in [opcodes.true, opcodes.false]:
            base_str += ",{0}={1}".format(opcodes.date_time, dt)
            self._dt = dt

        try:
            # Attempt to set program default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=base_str)
        except Exception as e:
            e_msg = "Exception occurred trying to set event switch condition for\nCN: {0}\nevent type: {1}" \
                    "\ncommand: {2}".format(
                        self._cn_type,
                        self._event_type,
                        base_str
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set event switch condition for\nCN: {0}\nevent type: {1}\ncommand: {2}".format(
                self._cn_type,
                self._event_type,
                base_str
            ))

    #################################
    def get_data(self):
        """
        Gets all data for the program from the controller. \n
        :return:
        """
        if self._cn_type == "10":
            # Example command: "GET,PS=1,TY=MS"
            command = "GET,{0}={1},{2}={3}".format(
                str(self._event_type),  # {0}
                str(self._program_ad),  # {1}
                str(opcodes.type),      # {2}
                str(self._device_type)  # {3}
            )
        else:
            command = "GET,{0}={1},{2}={3}".format(
                str(self._event_type),      # {0}
                str(self._program_ad),      # {1}
                str(self._device_type), # {2}
                str(self._device_serial)    # {3}
            )

        # Attempt to get data from controller
        try:
            self.data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception as ae:
            e_msg = "Unable to get data for Program: '{0}' using command: '{1}'. Exception raised: {2}".format(
                str(self._program_ad),  # {0}
                command,  # {1}
                str(ae.message)  # {2}
            )
            raise Exception(e_msg)

    #################################
    def verify_device_serial_on_cn(self):
        """
        Verifies the device serial number associated with this StartStopPause object that was set on the controller. \n
        :return:
        """
        if self._device_type == opcodes.moisture_sensor:
            dv_sn_on_cn = self.data.get_value_string_by_key(opcodes.moisture_sensor)
        elif self._device_type == opcodes.temperature_sensor:
            dv_sn_on_cn = self.data.get_value_string_by_key(opcodes.temperature_sensor)
        elif self._device_type == opcodes.event_switch:
            dv_sn_on_cn = self.data.get_value_string_by_key(opcodes.event_switch)
        else:
            dv_sn_on_cn = None

        # Compare serial numbers
        if self._device_serial != dv_sn_on_cn:
            e_msg = "Unable to verify StartStopPause({0}) Condition: {1}'s device serial number. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self._event_type),      # {0}
                        str(self._program_ad),      # {1}
                        str(dv_sn_on_cn),           # {2}
                        str(self._device_serial))   # {3}
            raise ValueError(e_msg)
        else:
            print("Verified StartStopPause({0}) Condition: {1}'s device serial number: '{2}' on controller".format(
                str(self._event_type),      # {0}
                str(self._program_ad),      # {1}
                self._device_serial         # {2}
                ))

    #################################
    def verify_mode_on_cn(self):
        """
        Verifies the mode associated with this StartStopPause object that was set on the controller. \n
        :return:
        """
        if self._device_type is opcodes.moisture_sensor:
            md_on_cn = self.data.get_value_string_by_key(opcodes.moisture_mode)
        elif self._device_type is opcodes.temperature_sensor:
            md_on_cn = self.data.get_value_string_by_key(opcodes.temperature_sensor_mode)
        elif self._device_type is opcodes.event_switch:
            md_on_cn = self.data.get_value_string_by_key(opcodes.switch_mode)
        else:
            e_msg = "Unable to verify StartStopPause Condition: {0}'s mode. No device type was found. Make sure " \
                    "to use one of the set methods before trying to verify.".format(self._program_ad)
            raise ValueError(e_msg)

        # Compare modes
        if self._mode != md_on_cn:
            e_msg = "Unable to verify StartStopPause({0}) Condition: {1}'s mode. Received: {2}, Expected: {3}".format(
                str(self._event_type),      # {0}
                str(self._program_ad),      # {1}
                str(md_on_cn),              # {2}
                str(self._mode))            # {3}
            raise ValueError(e_msg)
        else:
            print("Verified StartStopPause({0}) Condition: {1}'s mode: '{2}' on controller".format(
                str(self._event_type),      # {0}
                str(self._program_ad),      # {1}
                self._mode                  # {2}
            ))

    #################################
    def verify_threshold_on_cn(self):
        """
        Verifies the threshold associated with this StartStopPause object that was set on the controller. \n
        :return:
        """
        if self._device_type is opcodes.moisture_sensor:
            th_on_cn = float(self.data.get_value_string_by_key(opcodes.moisture_trigger_threshold))
        elif self._device_type is opcodes.temperature_sensor:
            th_on_cn = float(self.data.get_value_string_by_key(opcodes.temperature_trigger_threshold))
        else:
            e_msg = "Unable to verify StartStopPause Condition: {0}'s threshold. No device type was found. Make sure " \
                    "to use one of the set methods before trying to verify.".format(self._program_ad)
            raise ValueError(e_msg)

        # Compare thresholds
        if self._threshold != th_on_cn:
            e_msg = "Unable to verify StartStopPause({0}) Condition: {1}'s threshold. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self._event_type),      # {0}
                        str(self._program_ad),      # {1}
                        str(th_on_cn),              # {2}
                        str(self._threshold))       # {3}
            raise ValueError(e_msg)
        else:
            print("Verified StartStopPause({0}) Condition: {1}'s threshold: '{2}' on controller".format(
                str(self._event_type),      # {0}
                str(self._program_ad),      # {0}
                str(self._threshold)        # {1}
            ))

    #################################
    def verify_pause_time_on_cn(self):
        """
        Verifies the pause time associated with this StartStopPause object that was set on the controller. \n
        :return:
        """
        if self._device_type is opcodes.moisture_sensor:
            pt_on_cn = float(self.data.get_value_string_by_key(opcodes.moisture_pause_time))
        elif self._device_type is opcodes.temperature_sensor:
            pt_on_cn = float(self.data.get_value_string_by_key(opcodes.temperature_pause_time))
        elif self._device_type is opcodes.event_switch:
            pt_on_cn = float(self.data.get_value_string_by_key(opcodes.switch_pause_time))
        else:
            e_msg = "Unable to verify StartStopPause Condition: {0}'s pause time. No device type was found. Make " \
                    "sure to use one of the set methods before trying to verify.".format(self._program_ad)
            raise ValueError(e_msg)

        # Compare pause times
        if (self._pause_time * 60) != pt_on_cn:
            e_msg = "Unable to verify StartStopPause({0}) Condition: {1}'s pause time. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self._event_type),      # {0}
                        str(self._program_ad),      # {1}
                        str(pt_on_cn),              # {2}
                        str(self._pause_time))      # {3}
            raise ValueError(e_msg)
        else:
            print("Verified StartStopPause({0}) Condition: {1}'s pause time: '{2}' on controller".format(
                str(self._event_type),      # {0}
                str(self._program_ad),     # {0}
                self._pause_time           # {1}
            ))

    #################################
    def verify_stop_immediately_on_cn(self):
        """
        Verifies the stop immediately associated with this StartStopPause object that was set on the controller. \n
        :return:
        """
        si_on_cn = self.data.get_value_string_by_key(opcodes.stop_immediately)

        # Compare stop immediately
        if self._si != si_on_cn:
            e_msg = "Unable to verify StartStopPause({0}) Condition: {1}'s stop immediately. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self._event_type),      # {0}
                        str(self._program_ad),      # {1}
                        str(si_on_cn),              # {2}
                        str(self._si))              # {3}
            raise ValueError(e_msg)
        else:
            print("Verified StartStopPause({0}) Condition: {1}'s stop immediately: '{2}' on controller".format(
                str(self._event_type),      # {0}
                str(self._program_ad),    # {0}
                self._si                  # {1}
            ))

    #################################
    def verify_who_i_am(self):
        """
        Verifies this program's values against what the controller has. \n
        """
        self.get_data()

        # If the mode is off, the only thing we care about verifying is the controller is off
        if self._mode == opcodes.off:
            self.verify_mode_on_cn()
            return

        self.verify_device_serial_on_cn()
        self.verify_mode_on_cn()

        if self._threshold is not None:
            self.verify_threshold_on_cn()
        if self._pause_time is not None:
            self.verify_pause_time_on_cn()
        if self._si is not None:
            self.verify_stop_immediately_on_cn()