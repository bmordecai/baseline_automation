import status_parser
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.ser import Ser
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import master_valves, flow_meters
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages
from datetime import datetime, timedelta

__author__ = 'baseline'


########################################################################################################################
# Point Of Connection Base Class
########################################################################################################################
class POC(object):
    """
    Point Of Connection Base Class.
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    # Serial Instance
    ser = Ser.serial_conn

    # Browser Instance
    browser = ''

    # Controller type for controller specific methods across all devices
    controller_type = ''

    # Object pointers - These are overwritten
    master_valve_objects = None
    flow_meter_objects = None

    #################################
    def __init__(self, _ad, _ds, _en, _fl, _hf, _hs, _uf, _us, _fm=None, _mv=None):
        """
        Point Of Connection Constructor. \n

        :param _ad:     Point Of Connection Number (address) \n
        :type _ad:      int \n

        :param _ds:     Description \n
        :type _ds:      str \n

        :param _en:     Enabled State (True: 'TR', False: 'FA') - Default: 'TR' \n
        :type _en:      str \n

        :param _fl:     Target Flow - Default: 0 \n
        :type _fl:      int \n

        :param _hf:     High Flow Limit - Default: 0 \n
        :type _hf:      int \n

        :param _hs:     Shutdown on High Flow (True: 'TR', False: 'FA') - Default: 'FA' \n
        :type _hs:      str \n

        :param _uf:     Unscheduled Flow Limit - Default: 0 \n
        :type _uf:      float\n

        :param _us:     Shutdown on Unscheduled (True: 'TR', False: 'FA') - Default: 'FA' \n
        :type _us:      str \n

        :param _fm:     Flow Meter Address \n
        :type _fm:      int \n

        :param _mv:     Master Valve Address \n
        :type _mv:      int \n
        """
        self.flow_meter_objects = flow_meters
        self.master_valve_objects = master_valves

        self.ad = _ad   # Address
        self.ds = _ds   # Description
        self.en = _en   # Enabled State
        self.fl = _fl   # Target Flow
        self.hf = _hf   # High Flow Limit
        self.hs = _hs   # Shutdown on High Flow
        self.uf = _uf   # Unscheduled Flow Limit
        self.us = _us   # Shutdown on Unscheduled
        self.fm = _fm   # Flow Meter Object Address
        self.mv = _mv   # Master Valve Object Address
        self.ss = ''    # Status (Read only)

        # Container to hold POC programming from controller when a 'self.get_data()' call is made.
        self.data = status_parser.KeyValues('')

        # Holds the messages return value
        self.build_message_string = ''

    #################################
    def set_description_on_cn(self, _new_description=None):
        """
        Sets the 'Description' for the Point of Connection on the controller. \n

        :param _new_description:    New 'Description' for Point Of Connection \n
        :type _new_description:     str \n
        """
        # Assign new description to object attribute
        if _new_description is not None:
            self.ds = str(_new_description)

        command = "SET,{0}={1},{2}={3}".format(opcodes.point_of_connection, str(self.ad), opcodes.description, self.ds)
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Description' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                self.ds,        # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Description' to: {1}".format(str(self.ad), self.ds))

    #################################
    def set_enabled_state_on_cn(self, _new_state=None):
        """
        Sets the 'Enabled State' for the Point of Connection on the controller. \n

        :param _new_state:    New 'Enabled State' for Point Of Connection \n
        :type _new_state:     str \n
        """

        if _new_state is not None:
            # Validate argument value is within accepted values
            if _new_state not in [opcodes.true, opcodes.false]:
                e_msg = "Invalid state for POC {0} to set: {1}. Valid states are '{2}' or '{3}'".format(
                    str(self.ad),       # {0}
                    str(_new_state),    # {1}
                    opcodes.true,       # {2}
                    opcodes.false       # {3}
                    )
                raise ValueError(e_msg)
            else:
                # Assign new 'Enabled State' to object attribute
                self.en = _new_state

        command = "SET,{0}={1},{2}={3}".format(opcodes.point_of_connection, str(self.ad), opcodes.enabled, self.en)
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Enabled State' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                self.en,        # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Enabled State' to: {1}".format(
                str(self.ad),   # {0}
                self.en,        # {1}
            ))

    #################################
    def set_target_flow_on_cn(self, _new_flow=None):
        """
        Sets the 'Target Flow' for the Point of Connection on the controller. \n

        :param _new_flow:    New 'Target Flow' for Point Of Connection \n
        :type _new_flow:     int \n
        """

        if _new_flow is not None:
            # Assign new 'Target Flow' to object attribute
            self.fl = _new_flow

        command = "SET,{0}={1},{2}={3}".format(opcodes.point_of_connection, str(self.ad), opcodes.target_flow,
                                               str(self.fl))
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Target Flow' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.fl),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Target Flow' to: {1}".format(str(self.ad), str(self.fl)))
            
    #################################
    def set_high_flow_limit_on_cn(self, _new_limit=None):
        """
        Sets the 'High Flow Limit' for the Point of Connection on the controller. \n

        :param _new_limit:    New 'High Flow Limit' for Point Of Connection \n
        :type _new_limit:     int \n
        """

        # Assign new 'High Flow Limit' to object attribute
        if _new_limit is not None:
            self.hf = _new_limit

        command = "SET,{0}={1},{2}={3}".format(opcodes.point_of_connection, str(self.ad), opcodes.high_flow_limit,
                                               str(self.hf))
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'High Flow Limit' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.hf),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'High Flow Limit' to: {1}".format(str(self.ad), str(self.hf)))
            
    #################################
    def set_shutdown_on_high_flow_state_on_cn(self, _new_state=None):
        """
        Sets the 'Shutdown On High Flow' for the Point of Connection on the controller. \n

        :param _new_state:    New 'Shutdown On High Flow' for Point Of Connection \n
        :type _new_state:     str \n
        """
        # Validate argument value is within accepted values
        if _new_state is not None:
            # Validate argument value is within accepted values
            if _new_state not in [opcodes.true, opcodes.false]:
                e_msg = "Invalid state for POC {0} to set: {1}. Valid states are '{2}' or '{3}'".format(
                    str(self.ad),       # {0}
                    str(_new_state),    # {1}
                    opcodes.true,       # {2}
                    opcodes.false       # {3}
                    )
                raise ValueError(e_msg)
            else:
                # Assign new 'Enabled State' to object attribute
                self.hs = _new_state

        command = "SET,{0}={1},{2}={3}".format(opcodes.point_of_connection, str(self.ad),
                                               opcodes.shutdown_on_high_flow, str(self.hs))
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Shutdown On High Flow' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.hs),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Shutdown On High Flow' to: {1}".format(str(self.ad), str(self.hs)))

    #################################
    def set_unscheduled_flow_limit_on_cn(self, _new_limit=None):
        """
        Sets the 'Unscheduled Flow Limit' for the Point of Connection on the controller. \n
    
        :param _new_limit:    New 'Unscheduled Flow Limit' for Point Of Connection \n
        :type _new_limit:     int \n
        """
    
        # Assign new 'Unscheduled Flow Limit' to object attribute
        if _new_limit is not None:
            self.uf = _new_limit

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.point_of_connection,        # {0}
            str(self.ad),                       # {1}
            opcodes.unscheduled_flow_limit,     # {2}
            str(self.uf)                        # {3}
        )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Unscheduled Flow Limit' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                str(self.uf),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Unscheduled Flow Limit' to: {1}".format(str(self.ad), str(self.uf)))

    #################################
    def set_shutdown_on_unscheduled_state_on_cn(self, _new_state=None):
        """
        Sets the 'Shutdown On Unscheduled' state for the Point of Connection on the controller. \n
    
        :param _new_state:    New 'Shutdown On Unscheduled' state for Point Of Connection \n
        :type _new_state:     str \n
        """
        # Validate argument value is within accepted values
        if _new_state is not None:
            if _new_state not in [opcodes.true, opcodes.false]:
                e_msg = "Invalid state for POC {0} to 'SET' for 'Shutdown on Unscheduled': {1}. Valid states are " \
                        "'{2}' or '{3}'".format(str(self.ad), str(_new_state), opcodes.true, opcodes.false)
                raise ValueError(e_msg)
            # Assign new 'Shutdown On Unscheduled' to object attribute
            else:
                self.uf = _new_state

        command = "SET,{0}={1},{2}={3}".format(opcodes.point_of_connection, str(self.ad),
                                               opcodes.shutdown_on_unscheduled, str(self.us))
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Shutdown On Unscheduled' state to: '{1}' " \
                    "-> {2}".format(
                        str(self.ad),   # {0}
                        str(self.uf),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Shutdown On Unscheduled' state to: {1}".format(
                str(self.ad),  # {0}
                str(self.uf),  # {1}
            ))
            
    #################################
    def set_flow_meter_on_cn(self, _flow_meter_address):
        """
        Sets the 'Flow Meter' for the Point of Connection on the controller. \n
    
        :param _flow_meter_address:    New 'Flow Meter' for Point Of Connection \n
        :type _flow_meter_address:     int \n
        """
        # Validate argument value is within accepted values
        if _flow_meter_address not in self.flow_meter_objects:
            e_msg = "Invalid Flow Meter address. Verify address exists in object json configuration and/or in " \
                    "current test. Received address: {0}, available addresses: {1}".format(
                        str(_flow_meter_address),              # {0}
                        str(self.flow_meter_objects.keys()),   # {1}
                    )
            raise ValueError(e_msg)
    
        # Assign new 'Flow Meter' to object attribute
        else:
            # Assign Flow Meter attribute for POC to new Flow Meter address
            self.fm = _flow_meter_address

            # Get Flow Meter's serial from object to set at the controller
            fm_serial_from_obj = self.flow_meter_objects[self.fm].sn

            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,    # {0}
                str(self.ad),                   # {1}
                opcodes.flow_meter,             # {2}
                fm_serial_from_obj              # {3}
            )
    
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Flow Meter' to: '{1}' ({2})" \
                    "-> {3}".format(
                        str(self.ad),           # {0}
                        fm_serial_from_obj,     # {1}
                        str(self.fm),           # {2}
                        e.message               # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Flow Meter' to: {1} ({2})".format(
                str(self.ad),           # {0}
                fm_serial_from_obj,     # {1}
                str(self.fm),           # {2}
            ))
            
    #################################
    def set_master_valve_on_cn(self, _master_valve_address):
        """
        Sets the 'Master Valve' for the Point of Connection on the controller. \n
    
        :param _master_valve_address:    New 'Master Valve' for Point Of Connection \n
        :type _master_valve_address:     int \n
        """
        # Validate argument value is within accepted values
        if _master_valve_address not in self.master_valve_objects:
            e_msg = "Invalid Master Valve address. Verify address exists in object json configuration and/or in " \
                    "current test. Received address: {0}, available addresses: {1}".format(
                        str(_master_valve_address),              # {0}
                        str(self.master_valve_objects.keys()),   # {1}
                    )
            raise ValueError(e_msg)
    
        # Assign new 'Master Valve' to object attribute
        else:
            # Assign new Master Valve address to POCs Master Valve attribute
            self.mv = _master_valve_address

            # Get serial number from assigned Master Valve
            mv_serial_from_obj = self.master_valve_objects[self.mv].sn

            command = "SET,{0}={1},{2}={3}".format(
                opcodes.point_of_connection,    # {0}
                str(self.ad),                   # {1}
                opcodes.master_valve,           # {2}
                mv_serial_from_obj              # {3}
            )
    
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Master Valve' to: '{1}' ({2})" \
                    "-> {3}".format(
                        str(self.ad),           # {0}
                        mv_serial_from_obj,     # {1}
                        str(self.mv),           # {2}
                        e.message               # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Master Valve' to: {1} ({2})".format(
                str(self.ad),           # {0}
                mv_serial_from_obj,     # {1}
                str(self.mv),           # {2}
            ))

    #################################
    def set_message_on_cn(self, _status_code):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        """
        try:
            build_message_string = messages.message_to_set(self,
                                                           _status_code=_status_code,
                                                           _ct_type=opcodes.point_of_connection,
                                                           _use_to_verify=True)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.set_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=build_message_string)

    #################################
    def get_data(self):
        """
        Gets all data for the Point Of Connection from the controller. \n
        :return:
        """
        # Example command: "GET,PC=1"
        command = "GET,{0}={1}".format(
            opcodes.point_of_connection,    # {0}
            str(self.ad),                   # {1}
        )

        # Attempt to get data from controller
        try:
            self.data = self.ser.get_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Unable to get data for POC {0} using command: '{1}'. Exception raised: " \
                    "{2}".format(
                        str(self.ad),       # {0}
                        command,            # {2}
                        str(ae.message)     # {3}
                    )
            raise ValueError(e_msg)

    ##################################
    def get_message_on_cn(self, _status_code):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        """
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=opcodes.point_of_connection,
                                                                _use_to_verify=True)

            command = messages.message_to_get(self, _status_code=_status_code,
                                              _ct_type=opcodes.point_of_connection)

            cn_mg = self.ser.get_and_wait_for_reply(tosend=command[opcodes.message])
        except Exception as e:
            msg = "Exception caught in messages.get_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=self.build_message_string[opcodes.message])
            return cn_mg

    ##################################
    def clear_message_on_cn(self, _status_code):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        """
        try:
            build_message_string = messages.message_to_clear(self,
                                                             _status_code=_status_code,
                                                             _ct_type=opcodes.point_of_connection)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.clear_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=build_message_string)

        # Verifies that the message was cleared by catching the 'NM' command that we would expect back
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=opcodes.point_of_connection)
            self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            if e.message == "NM No Message Found":
                print "Message cleared and verified that the message is gone."
            else:
                print "Message cleared, but was unable to verify if the message was gone."

    ##################################
    def verify_message_not_present_on_cn(self, _status_code):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        """
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=opcodes.point_of_connection)
            self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            if e.message == "NM No Message Found":
                print "Message was to found on controller."
            else:
                print "Was unable to verify if the message was gone."

    #################################
    def verify_description_on_cn(self):
        """
        Verifies the 'Description' for the Point of Connection set on the controller. \n
        :return:
        """
        # expect string type
        ds_on_cn = self.data.get_value_string_by_key(opcodes.description)

        # Compare descriptions
        if self.ds != ds_on_cn:
            e_msg = "Unable to verify POC {0}'s 'Description'. Received: {1}, Expected: {2}".format(
                str(self.ad),   # {0}
                str(ds_on_cn),  # {1}
                self.ds         # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Description': '{1}' on controller".format(
                str(self.ad),   # {0}
                self.ds         # {1}
            ))

    #################################
    def verify_enabled_state_on_cn(self):
        """
        Verifies the Point Of Connection 'Enabled State' set on the controller. \n
        """
        # expect string type
        en_state = self.data.get_value_string_by_key(opcodes.enabled)

        # Compare enabled state
        if self.en != en_state:
            e_msg = "Unable verify POC {0}'s 'Enabled State'. Received: {1}, Expected: {2}".format(
                str(self.ad),   # {0}
                str(en_state),  # {1}
                self.en         # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Enabled State': '{1}' on controller".format(
                str(self.ad),   # {0}
                self.en         # {1}
            ))
            
    #################################
    def verify_target_flow_on_cn(self):
        """
        Verifies the Point Of Connection 'Target Flow' set on the controller. \n
        :return:
        """
        # expect int type
        target_flow_from_controller = float(self.data.get_value_string_by_key(opcodes.target_flow))

        # Compare enabled state
        if self.fl != target_flow_from_controller:
            e_msg = "Unable verify POC {0}'s 'Target Flow'. Received: {1}, Expected: {2}".format(
                str(self.ad),                       # {0}
                str(target_flow_from_controller),   # {1}
                str(self.fl)                        # {2}
            )
            print(e_msg)
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Target Flow': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.fl)    # {1}
            ))    
            
    #################################
    def verify_high_flow_limit_on_cn(self):
        """
        Verifies the Point Of Connection 'High Flow Limit' set on the controller. \n
        :return:
        """
        # expect int type
        high_flow_limit_from_controller = float(self.data.get_value_string_by_key(opcodes.high_flow_limit))

        # Compare enabled state
        if self.hf != high_flow_limit_from_controller:
            e_msg = "Unable verify POC {0}'s 'High Flow Limit'. Received: {1}, Expected: {2}".format(
                str(self.ad),                           # {0}
                str(high_flow_limit_from_controller),   # {1}
                str(self.hf)                            # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'High Flow Limit': '{1}' on controller".format(
                str(self.ad),   # {0}
                int(self.hf)    # {1}
            ))            
            
    #################################
    def verify_shutdown_on_high_flow_on_cn(self):
        """
        Verifies the Point Of Connection 'Shutdown on High Flow' set on the controller. \n
        :return:
        """
        # expect string type
        shutdown_on_high_flow_from_controller = self.data.get_value_string_by_key(opcodes.shutdown_on_high_flow)

        # Compare enabled state
        if self.hs != shutdown_on_high_flow_from_controller:
            e_msg = "Unable verify POC {0}'s 'Shutdown on High Flow'. Received: {1}, Expected: {2}".format(
                str(self.ad),                              # {0}
                shutdown_on_high_flow_from_controller,     # {1}
                self.hs                                    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Shutdown on High Flow': '{1}' on controller".format(
                str(self.ad),   # {0}
                self.hs         # {1}
            ))

    #################################
    def verify_unscheduled_flow_limit_on_cn(self):
        """
        Verifies the Point Of Connection 'Unscheduled Flow Limit' set on the controller. \n
        :return:
        """
        # expect int type
        unscheduled_flow_limit_from_controller = float(self.data.get_value_string_by_key(opcodes.unscheduled_flow_limit))

        # Compare enabled state
        if self.uf != unscheduled_flow_limit_from_controller:
            e_msg = "Unable verify POC {0}'s 'Unscheduled Flow Limit'. Received: {1}, Expected: {2}".format(
                str(self.ad),                                   # {0}
                str(unscheduled_flow_limit_from_controller),    # {1}
                str(self.uf)                                    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Unscheduled Flow Limit': '{1}' on controller".format(
                str(self.ad),   # {0}
                int(self.uf)    # {1}
            ))

    #################################
    def verify_shutdown_on_unscheduled_on_cn(self):
        """
        Verifies the Point Of Connection 'Shutdown on Unscheduled' set on the controller. \n
        :return:
        """
        # expect string type
        shutdown_on_unscheduled_from_controller = self.data.get_value_string_by_key(opcodes.shutdown_on_unscheduled)

        # Compare enabled state
        if self.us != shutdown_on_unscheduled_from_controller:
            e_msg = "Unable verify POC {0}'s 'Shutdown on Unscheduled'. Received: {1}, Expected: {2}".format(
                str(self.ad),                              # {0}
                shutdown_on_unscheduled_from_controller,   # {1}
                self.us                                    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Shutdown on Unscheduled': '{1}' on controller".format(
                str(self.ad),   # {0}
                self.us         # {1}
            ))
            
    #################################
    def verify_flow_meter_on_cn(self):
        """
        Verifies the Point Of Connection 'Flow Meter' set on the controller. \n
        :return:
        """
        # expect string type
        flow_meter_serial_number_from_controller = self.data.get_value_string_by_key(opcodes.flow_meter)

        # Get the current serial number for the current assigned Flow Meter
        current_fm_object_serial = self.flow_meter_objects[self.fm].sn

        # Compare
        if current_fm_object_serial != flow_meter_serial_number_from_controller:
            e_msg = "Unable verify POC {0}'s 'Flow Meter'. Received: {1}, Expected: {2}".format(
                str(self.ad),                              # {0}
                flow_meter_serial_number_from_controller,  # {1}
                current_fm_object_serial                   # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Flow Meter': '{1}' on controller".format(
                str(self.ad),   # {0}
                self.fm         # {1}
            ))

    #################################
    def verify_master_valve_on_cn(self):
        """
        Verifies the Point Of Connection 'Master Valve' set on the controller. \n
        :return:
        """
        # expect string type
        master_valve_serial_number_from_controller = self.data.get_value_string_by_key(opcodes.master_valve)

        # Get the current serial number for the current assigned Master Valve
        current_mv_object_serial = self.master_valve_objects[self.mv].sn

        # Compare
        if current_mv_object_serial != master_valve_serial_number_from_controller:
            e_msg = "Unable verify POC {0}'s 'Master Valve'. Received: {1}, Expected: {2}".format(
                str(self.ad),                                   # {0}
                master_valve_serial_number_from_controller,     # {1}
                current_mv_object_serial                        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Master Valve': '{1}' on controller".format(
                str(self.ad),   # {0}
                self.mv         # {1}
            ))

    #################################
    def verify_status_on_cn(self, _expected_status):
        """
        Verifies the 'Status' for the Point of Connection  on the controller. \n
        :param _expected_status:    Expected status for program. \n
        :type _expected_status:     str \n
        """
        ss_on_cn = self.data.get_value_string_by_key(opcodes.status_code)

        # Since status is a 'get' only, we want to overwrite our object with the expected status passed in to retain
        # the state of our object.
        self.ss = _expected_status

        # Compare status versus what is on the controller
        if self.ss != ss_on_cn:
            e_msg = "Unable verify POC {0}'s 'Status'. Received: {1}, Expected: {2}".format(
                str(self.ad),   # {0}
                ss_on_cn,       # {1}
                str(self.ss)    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Status': '{1}' on controller".format(
                str(self.ad),   # {0}
                self.ss         # {1}
            ))

    #################################
    def verify_message_on_cn(self, _status_code):
        """
        the expected values are retrieved from the controller and then compared against the values that are returned
        from the message module
        :param _status_code
        :type _status_code :str
        :return:

        """
        mg_on_cn = self.get_message_on_cn(_status_code=_status_code)
        expected_message_text_from_cn = mg_on_cn.get_value_string_by_key(opcodes.message_text)
        expected_message_id_from_cn = mg_on_cn.get_value_string_by_key(opcodes.message_id)
        expected_date_time_from_cn = mg_on_cn.get_value_string_by_key(opcodes.date_time)
        # Compare status versus what is on the controller
        if self.build_message_string[opcodes.message_id] != expected_message_id_from_cn:
            e_msg = "Created ID message did not match the ID received from the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_id],  # {0} The ID that was built
                        expected_message_id_from_cn                     # {1} The ID returned from controller
                    )
            raise ValueError(e_msg)

        mess_date_time = datetime.strptime(self.build_message_string[opcodes.date_time], '%m/%d/%y %H:%M:%S')
        controller_date_time = datetime.strptime(expected_date_time_from_cn, '%m/%d/%y %H:%M:%S')
        if mess_date_time != controller_date_time:
            if abs(controller_date_time - mess_date_time) >= timedelta(days=0, hours=0, minutes=60, seconds=0):
                e_msg = "The date and time of the message didn't match the controller:\n" \
                        "\tCreated: \t\t'{0}'\n" \
                        "\tReceived:\t\t'{1}'\n".format(
                            self.build_message_string[opcodes.date_time],  # {0} The date message that was built
                            expected_date_time_from_cn  # {1} The date message returned from controller
                        )
                raise ValueError(e_msg)
            # only print this if they were not exact
            e_msg = "############################  Date and time were not exact but were within +- 30 minutes \n" \
                    "############################  Controller Date and Time = {0} \n" \
                    "############################  Message Date time = {1}".format(
                        expected_date_time_from_cn,  # {0} Date time received from controller
                        self.build_message_string[opcodes.date_time]  # {1} Date time the message was verified
                    )
            print(e_msg)

        if self.build_message_string[opcodes.message_text] != expected_message_text_from_cn:
            e_msg = "Created TX message did not match the TX received from the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_text],   # {0} The TX message that was built
                        expected_message_text_from_cn                      # {1} The TX message returned from controller
                    )
            raise ValueError(e_msg)
        else:
            print "Successfully verified:\n" \
                  "\tID: '{0}'\n" \
                  "\tDT: '{1}'\n" \
                  "\tTX: '{2}'\n".format(
                      self.build_message_string[opcodes.message_id],
                      self.build_message_string[opcodes.date_time],
                      self.build_message_string[opcodes.message_text]
                  )
