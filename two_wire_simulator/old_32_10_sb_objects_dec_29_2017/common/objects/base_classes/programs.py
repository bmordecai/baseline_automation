import re
import status_parser
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.ser import Ser
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import master_valves,zone_programs
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages
from datetime import datetime, timedelta
import dateutil.parser as dparser

__author__ = 'baseline'


########################################################################################################################
# Program Base Class
########################################################################################################################
class Programs(object):
    """
    Programs Base Class.
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    # Serial Instance
    ser = Ser.serial_conn

    # Browser Instance
    browser = ''

    # Controller type for controller specific methods across all devices
    controller_type = ''

    # Master Valve objects.
    mv_objects = None

    #################################
    def __init__(self, _ds, _ad, _en=None, _ww=None, _mc=None, _sa=None, _ss=None):
        """
        Base Program Constructor.

        :param _ds:     A description for the program. \n
        :type _ds:      str \n

        :param _ad:     Program number \n
        :type _ad:      int \n

        :param _en:     The enabled state for the program. Enabled = 'TR', Disabled = 'FA'
        :type _en:      str \n

        :param _ww:     Water window for the program. Two cases accepted.\n
                        - Weekly:   A list with a single element is passed in, the single element having 24 0's or 1's
                                    in a string where 1=On, 0=Off, i.e: ['111111111111111111111111']
                        - Daily:    A list with seven elements, 1 for each day of the week starting with sunday,
                                    with the same format as above, \n
                                    i.e: ['111111111111111111111111', '111111111111111111111111',   \n
                                          '111111111111111111111111', '111111111111111111111111',   \n
                                          '111111111111111111111111', '111111111111111111111111',   \n
                                          '111111111111111111111111'] \n
        :type _ww:      list[str] \n

        :param _mc:     Max number of concurrent zones for programming \n
        :type _mc:      int\n

        :param _sa:     Seasonal adjust for programming with an integer representing a percentage, i.e: 100 = 100% \n
        :type _sa:      int \n

        :param _ss:     Status for program \n
        :type _ss:     str \n
        """
        # ----- Instance Variables ----- #
        self.mv_objects = master_valves
        self.zp_objects = zone_programs
        self.ad = _ad       # Program Number
        self.ds = _ds       # Description

        # Place holder for program data from controller when needed.
        self.data = status_parser.KeyValues(string_in=None)

        # these are place holders to keep track of the status of the program for each minute
        self.total_seconds_program_ran = 0
        self.total_seconds_program_waited = 0

        # default enabled state to true if nothing is passed in to instance constructor
        if _en is None:
            self.en = opcodes.true
        else:
            self.en = _en

        # default water window is set to all 1's if nothing is passed in "None"
        # to watering all day if nothing is passed in to instance constructor
        # example weekly water window for 1 day 24 hours ['111111111111111111111111']
        # example daily water window for 7 day 24 hours per day:
        # _ww = [
        # '111111111111111111111111',   - Sunday (says water all day sunday)
        # '000000001111111111111111',   - Monday (says to not water Monday from Midnight to 8am)
        # '000011111111111111110000',   - Tuesday (says to not water from Midnight to 4am AND from 8pm to Midnight)
        # '111111111111111111111111',   - Wednesday
        # '111111111111111111111111',   - Thursday
        # '111111111111111111111111',   - Friday
        # '111111111111111111111111'    - Saturday
        # ]
        if _ww is None:
            self.ww = ['111111111111111111111111']
        else:
            self.ww = _ww

        # default max concurrent zones to 1 if nothing is passed in to instance constructor
        if _mc is None:
            self.mc = 1
        else:
            self.mc = _mc

        # default seasonal adjust percentage to 100% if nothing is passed in to instance constructor
        if _sa is None:
            self.sa = 100
        else:
            self.sa = _sa

        # default status to empty string if nothing passed in to instance constructor
        if _ss is None:
            self.ss = ''
        else:
            self.ss = _ss

        # Holds the messages return value
        self.build_message_string = ''

    #################################
    def set_description_on_cn(self, _ds=None):
        """
        Sets the description of the program on the controller. If a description is specified in the parameter,
        it will overwrite the current set description and set the new one at the controller as well as in the object. \n
        :param _ds:             Description to overwrite current object description.
        :return:
        """
        # If a description is passed in
        if _ds is not None:
            self.ds = str(_ds)

        # Command for sending to controller.
        command = "SET,{0}={1},{2}={3}".format(opcodes.program, str(self.ad), opcodes.description, str(self.ds))
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s description to: {1}".format(str(self.ad), self.ds)
            raise Exception(e_msg)
        else:
            print("Successfully set description for Program: {0} to: {1}".format(str(self.ad), self.ds))

    #################################
    def set_enable_state_on_cn(self, _state=None):
        """
        Sets the enabled state of the program on the controller. \n
        :param _state:      Enabled ('TR') or Disabled ('FA') \n
        :return:
        """
        if _state is not None:
            if _state not in ['TR', 'FA']:
                e_msg = "Invalid state for program to set: {0}. Valid states are 'TR' or 'FA'".format(_state)
                raise ValueError(e_msg)
            else:
                self.en = _state

        # Command for sending to controller.
        command = "SET,PG={0},EN={1}".format(str(self.ad), self.en)

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0}'s enabled state to: {1}".format(str(self.ad), self.en)
            raise Exception(e_msg)
        else:
            print("Successfully set enabled state for Program: {0} to: {1}".format(str(self.ad), self.en))

    #################################
    def set_water_window_on_cn(self, _ww=None, _is_weekly=True):
        """
        Sets the water window for the program instance on the controller. \n
        :param _ww:     Water Window for set. Requires binary notation for hours 0-23 (1's and 0's) \n
                        String of 1's and 0's (i.e. '00000011111111111111111' says don't water from midnight to 5am) \n
        :type _ww:      list[str] \n
        :return:
        """
        # If user wants to overwrite default value, they can pass in a value
        if _ww is not None:
            index = 0
            for each_item in _ww:
                # For each water window value (if weekly there wil only be one value, otherwise there will be a
                # string for each day of the week in the list.)
                # Searches the water window string input for anything that isn't a 0 or a 1, if it finds anything that
                # is not a 0 or a 1, then an exception is raised
                if re.search(r'\D|[2-9]+', each_item):
                    e_msg = "Invalid water window for program to set at index {0}: {1}. Valid water window must " \
                            "be a string of 0's and 1's.".format(str(index), str(_ww[index]))
                    raise ValueError(e_msg)
                if len(each_item) is not 24:
                    e_msg = "Invalid water window for program to set at index {0}: {1}. Valid water window must be" \
                            "exactly 24 characters long".format(str(index), str(_ww[index]))
                    raise ValueError(e_msg)
                index += 1

            if _is_weekly is True and len(_ww) is not 1:
                e_msg = "Invalid # of list elements for a water window: if weekly water window = true, " \
                        "# of arguments = 1"
                raise ValueError(e_msg)
            elif _is_weekly is False and len(_ww) is not 7:
                e_msg = "Invalid # of list elements for a water window: if weekly water window = false, " \
                        "# of arguments = 7"
                raise ValueError(e_msg)

            # Set water window list to new list, all items were valid syntax
            self.ww = _ww

        # Command for sending to controller.
        command = "SET,PG={0}".format(
            str(self.ad)    # {0}
        )

        # If water window type is weekly
        if _is_weekly:
            command += ",WW={0}".format(
                self.ww[0]      # {0}
            )
        # Else, water window type is daily, append all days starting with w1=sunday through saturday
        else:
            command += ",W1={0},W2={1},W3={2},W4={3},W5={4},W6={5},W7={6}".format(
                self.ww[0],     # {0}
                self.ww[1],     # {1}
                self.ww[2],     # {2}
                self.ww[3],     # {3}
                self.ww[4],     # {4}
                self.ww[5],     # {5}
                self.ww[6]      # {6}
            )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0}'s Water Window to: {1}".format(str(self.ad), self.ww)
            raise Exception(e_msg)
        else:
            print("Successfully set Water Window for Program: {0} to: {1}".format(str(self.ad), self.ww))

    #################################
    def set_max_concurrent_zones_on_cn(self, _number_of_zones=None):
        """
        Sets the max number of concurrent zones to run during program. \n
        :param _number_of_zones:        Number of zones to set for concurrent zones for programming.
        :type _number_of_zones:         Integer \n
        :return:
        """
        # If user wants to overwrite default value, they can pass in a value
        if _number_of_zones is not None:

            # Verifies the number of zones passed in is an int
            if not isinstance(_number_of_zones, int):
                e_msg = "Failed trying to set PG {0} concurrent zones. Invalid type, expected an int, " \
                        "received: {1}".format(str(self.ad), type(_number_of_zones))
                raise TypeError(e_msg)
            else:
                self.mc = _number_of_zones

        # Command for sending to controller.
        command = "SET,PG={0},MC={1}".format(str(self.ad), str(self.mc))

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0}'s max concurrent zones to: {1}".format(str(self.ad),
                                                                                                         str(self.mc))
            raise Exception(e_msg)
        else:
            print("Successfully set Max Concurrent Zones for Program: {0} to: {1}".format(str(self.ad), str(self.mc)))

    #################################
    def set_seasonal_adjust_on_cn(self, _percent=None):
        """
        Sets the seasonal adjust for program on the controller. \n
        :param _percent:        Percent to assign to seasonal adjust value for program. \n
        :type _percent:         Integer \n
        :return:
        """
        # If user wants to overwrite default value, they can pass in a value
        if _percent is not None:

            # Verifies the percent passed in is an int or float
            if not isinstance(_percent, int):
                e_msg = "Failed trying to set PG {0} seasonal adjust. Invalid type, expected an int, " \
                        "received: {1}".format(str(self.ad), type(_percent))
                raise TypeError(e_msg)
            else:
                self.sa = _percent
            # this goes through and updates the runtime of the zone in the zone program
            for index, zone in self.zp_objects.iteritems():
                if zone.program.ad == self.ad:
                    zone.rt = float(zone.rt) * (float(self.sa)/100)
                    zone.rt = 60*(int(round(float(zone.rt)/(float(60)))))

        # Command for sending to controller.
        command = "SET,PG={0},SA={1}".format(str(self.ad), str(self.sa))

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0}'s Seasonal Adjust to: {1}".format(str(self.ad),
                                                                                                    str(self.sa))
            raise Exception(e_msg)
        else:
            print("Successfully set Seasonal Adjust for Program: {0} to: {1}".format(str(self.ad), str(self.sa)))

    #################################
    def set_message_on_cn(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        :param _helper_object
        :type _helper_object : BaseStartStopPause
        """
        try:
            build_message_string = messages.message_to_set(self,
                                                           _status_code=_status_code,
                                                           _ct_type=opcodes.program,
                                                           _helper_object=_helper_object)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.set_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=build_message_string)

    #################################
    def get_data(self):
        """
        Gets all data for the program from the controller. \n
        :return:
        """

        # Example command: "GET,PG=1"
        command = "GET,PG={0}".format(
            str(self.ad)    # {0}
        )

        # Attempt to get data from controller
        try:
            self.data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception as ae:
            e_msg = "Unable to get data for Program: '{0}' using command: '{1}'. Exception raised: {2}".format(
                    str(self.ad),       # {0}
                    command,            # {1}
                    str(ae.message)     # {2}
            )
            raise Exception(e_msg)

    #################################
    def get_message_on_cn(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        :param _helper_object
        :type _helper_object : BaseStartStopPause
        """
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=opcodes.program,
                                                                _helper_object=_helper_object)
            cn_mg = self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            msg = "Exception caught in messages.get_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=self.build_message_string[opcodes.message])
            return cn_mg

    #################################
    def clear_message_on_cn(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        :param _helper_object
        :type _helper_object : BaseStartStopPause
        """
        try:
            build_message_string = messages.message_to_clear(self,
                                                             _status_code=_status_code,
                                                             _ct_type=opcodes.program,
                                                             _helper_object=_helper_object)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.clear_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=build_message_string)

        # Verifies that the message was cleared by catching the 'NM' command that we would expect back
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=opcodes.program,
                                                                _helper_object=_helper_object)
            self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            if e.message == "NM No Message Found":
                print "Message cleared and verified that the message is gone."
            else:
                print "Message cleared, but was unable to verify if the message was gone."

    #################################
    def verify_description_on_cn(self):
        """
        Verifies the Program Description set on the controller. \n
        :return:
        """
        ds_on_cn = self.data.get_value_string_by_key(opcodes.description)

        # Compare descriptions
        if self.ds != ds_on_cn:
            e_msg = "Unable verify Program: {0}'s Description. Received: {1}, Expected: {2}".format(
                    str(self.ad),   # {0}
                    str(ds_on_cn),  # {1}
                    str(self.ds))   # {2}
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s Description: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  self.ds           # {1}
                  ))

    #################################
    def verify_enabled_state_on_cn(self):
        """
        Verifies the Program Enabled State set on the controller. \n
        :return:
        """
        en_state = self.data.get_value_string_by_key('EN')

        # Compare enabled state
        if self.en != en_state:
            e_msg = "Unable verify Program: {0}'s Enabled State. Received: {1}, Expected: {2}".format(
                    str(self.ad),   # {0}
                    str(en_state),  # {1}
                    str(self.en))   # {2}
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s Enabled State: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  self.en           # {1}
                  ))

    #################################
    def verify_water_window_on_cn(self):
        """
        Verifies the Program Water Window set on the controller. \n
        :return:
        """
        # empty list place holder
        ww_from_cn = []

        # First check to see which water window type is currently being used, either a weekly or daily schedule.
        # If weekly schedule being used, then the length of our 'self.ww' list should be 1, otherwise the length of
        # 'self.ww' list should be 7 (one for each day of the week starting with sunday to saturday)
        if len(self.ww) == 1:
            ww_from_cn.append(self.data.get_value_string_by_key('WW'))

        # Else, assume daily water window scheduling, so append each day to the list
        else:
            # 'W1' - Sunday
            # 'W2' - Monday
            # 'W3' - Tuesday
            # 'W4' - Wednesday
            # 'W5' - Thursday
            # 'W6' - Friday
            # 'W7' - Saturday
            for each_weekday in ['W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7']:
                ww_from_cn.append(self.data.get_value_string_by_key(each_weekday))

        # Compare Water Window values, here we are comparing lists to each other, which is equivalent to comparing
        # each list item individually.
        if self.ww != ww_from_cn:
            e_msg = "Unable verify Program: {0}'s Water Window. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(ww_from_cn),    # {1}
                str(self.ww)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s Water Window: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  self.ww           # {1}
                  ))

    #################################
    def verify_max_concurrent_zones_on_cn(self):
        """
        Verifies the Program Max Concurrent Zones set on the controller. \n
        :return:
        """
        mc = int(self.data.get_value_string_by_key('MC'))

        # Compare Max Concurrent Zone values
        if self.mc != mc:
            e_msg = "Unable verify Program: {0}'s Max Concurrent Zones. Received: {1}, Expected: {2}".format(
                    str(self.ad),   # {0}
                    str(mc),        # {1}
                    str(self.mc)    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s Max Concurrent Zones: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.mc)      # {1}
                  ))

    #################################
    def verify_seasonal_adjust_on_cn(self):
        """
        Verifies the Program Seasonal Adjust set on the controller. \n
        :return:
        """
        seasonal_adjust = int(self.data.get_value_string_by_key('SA'))

        # Compare Seasonal Adjust values
        if self.sa != seasonal_adjust:
            e_msg = "Unable verify Program: {0}'s Seasonal Adjust. Received: {1}, Expected: {2}".format(
                    str(self.ad),           # {0}
                    str(seasonal_adjust),   # {1}
                    str(self.sa)            # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s Seasonal Adjust: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.sa)      # {1}
                  ))

    #################################
    def verify_status_on_cn(self, _expected_status):
        """
        Verifies the Program Status on the controller. \n
        :param _expected_status:    Expected status for program. \n
        :type _expected_status:     str \n
        :return:
        """
        ss_on_cn = self.data.get_value_string_by_key('SS')

        # Since status is a 'get' only, we want to overwrite our object with the expected status passed in to retain
        # the state of our object.
        self.ss = _expected_status

        # Compare status versus what is on the controller
        if self.ss != ss_on_cn:
            e_msg = "Unable verify Program: {0}'s status. Received: {1}, Expected: {2}".format(
                    str(self.ad),   # {0}
                    ss_on_cn,       # {1}
                    str(self.ss)    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s status: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  self.ss           # {1}
                  ))

    #################################
    def verify_message_on_cn(self, _status_code, _helper_object=None):
        """
        The expected values are retrieved from the controller and then compared against the values that are returned
        from the message module
        :param _status_code
        :type _status_code :str
        :param _helper_object
        :type _helper_object : BaseStartStopPause
        :return:
        """
        # TODO need to comment this method
        mg_on_cn = self.get_message_on_cn(_status_code=_status_code, _helper_object=_helper_object)
        expected_message_text_from_cn = mg_on_cn.get_value_string_by_key(opcodes.message_text)
        expected_message_id_from_cn = mg_on_cn.get_value_string_by_key(opcodes.message_id)
        expected_date_time_from_cn = mg_on_cn.get_value_string_by_key(opcodes.date_time)
        # Compare status versus what is on the controller
        if self.build_message_string[opcodes.message_id] != expected_message_id_from_cn:
            e_msg = "Created ID message did not match the ID received from the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_id],  # {0} The ID that was built
                        expected_message_id_from_cn                     # {1} The ID returned from controller
                    )
            raise ValueError(e_msg)

        mess_date_time = datetime.strptime(self.build_message_string[opcodes.date_time], '%m/%d/%y %H:%M:%S')
        controller_date_time = datetime.strptime(expected_date_time_from_cn, '%m/%d/%y %H:%M:%S')
        if mess_date_time != controller_date_time:
            if abs(controller_date_time - mess_date_time) >= timedelta(days=0, hours=0, minutes=60, seconds=0):
                e_msg = "The date and time of the message didn't match the controller:\n" \
                        "\tCreated: \t\t'{0}'\n" \
                        "\tReceived:\t\t'{1}'\n".format(
                            self.build_message_string[opcodes.date_time],  # {0} The date message that was built
                            expected_date_time_from_cn  # {1} The date message returned from controller
                        )
                raise ValueError(e_msg)
            # only print this if they were not exact
            e_msg = "############################  Date and time were not exact but were within +- 30 minutes \n" \
                    "############################  Controller Date and Time = {0} \n" \
                    "############################  Message Date time = {1}".format(
                        expected_date_time_from_cn,  # {0} Date time received from controller
                        self.build_message_string[opcodes.date_time]  # {1} Date time the message was verified
                    )
            print(e_msg)

        # If the controller is 1000 and the TX strings don't match, check if they don't match because of the dates
        if self.controller_type == '10':
            if self.build_message_string[opcodes.message_text] != expected_message_text_from_cn:
                # Parses the date and the rest of the string and stores it as a two element tuple
                build_message_date_only = dparser.parse(self.build_message_string[opcodes.message_text], fuzzy_with_tokens=True)
                message_received_from_cn_date_only = dparser.parse(expected_message_text_from_cn, fuzzy_with_tokens=True)
                # If the dates that were parsed are not within 1 hour of each other, throw an error
                if abs(build_message_date_only[0] - message_received_from_cn_date_only[0]) >= timedelta(
                        days=0, hours=0, minutes=60, seconds=0):
                    e_msg = "The date and time of the message didn't match the controller:\n" \
                            "\tCreated: \t\t'{0}'\n" \
                            "\tReceived:\t\t'{1}'\n".format(
                                build_message_date_only[0],  # {0} The date message that was built
                                message_received_from_cn_date_only[0]  # {1} The date message returned from controller
                            )
                    raise ValueError(e_msg)
                # If the rest of the message from both strings aren't equal, throw an error
                if build_message_date_only[1] != message_received_from_cn_date_only[1]:
                    e_msg = "The date and time of the message didn't match the controller:\n" \
                            "\tCreated: \t\t'{0}'\n" \
                            "\tReceived:\t\t'{1}'\n".format(
                                build_message_date_only[1],  # {0} The date message that was built
                                message_received_from_cn_date_only[1]  # {1} The date message returned from controller
                            )
                    raise ValueError(e_msg)

        elif self.build_message_string[opcodes.message_text] != expected_message_text_from_cn:
            e_msg = "Created TX message did not match the TX received from the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_text],   # {0} The TX message that was built
                        expected_message_text_from_cn                      # {1} The TX message returned from controller
                    )
            raise ValueError(e_msg)
        print "Successfully verified:\n" \
              "\tID: '{0}'\n" \
              "\tDT: '{1}'\n" \
              "\tTX: '{2}'\n".format(
                      self.build_message_string[opcodes.message_id],
                      self.build_message_string[opcodes.date_time],
                      self.build_message_string[opcodes.message_text]
                  )
