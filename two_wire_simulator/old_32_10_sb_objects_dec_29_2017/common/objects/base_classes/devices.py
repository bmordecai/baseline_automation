import status_parser
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.variables import common as common_vars
# from old_32_10_sb_objects_dec_29_2017.common.epa_package.wbw_imports import *
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr as resource_date_mngr
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.ser import Ser
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from datetime import time, date, datetime, timedelta
__author__ = 'bens'


class Devices(object):

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    # Serial Instance
    ser = Ser.serial_conn

    # Browser Instance
    browser = ''

    # Controller type for controller specific methods across all devices
    controller_type = ''
    controller_lat = 0.0    # this gets overwritten with configuration.py when we run our test scripts
    controller_long = 0.0   # this gets overwritten with configuration.py when we run our test scripts
    et = 0.00               # Controller ETo Value
    ra = 0                  # Controller Rain Value
    # TODO this needs a setter on it
    # Attributes that are not sent to all devices where there is no setters or verifiers for these attributes
    # Dictionary for converting device types into address assignment op code
    dv_assign_op_code_dict = {
        'ZN': 'AZ',
        'MS': 'AM',
        'FM': 'AF',
        'MV': 'AV',
        'TS': 'AT',
        'SW': 'AS',
        'AR': 'AA',
        'IS': 'AI'
    }

    date_mngr = resource_date_mngr

    #################################
    def __init__(self, _sn, _ds, _ad='', _la=0.0, _lg=0.0, _ty='', _ss='', is_shared=False):
        """

        :param _sn:     serial number
        :type _sn:
        :param _ds:     Description
        :type _ds:
        :param _ad:     Address Number
        :type _ad:
        :param _la:     Latitude
        :type _la:      float
        :param _lg:     Longitude
        :type _lg:      float
        :param _ty:     Type
        :type _ty:
        :param _ss:     Status
        :type _ss:
        :param is_shared: Is the device shared with a Substation.
        :type is_shared: bool
        :return:
        :rtype:
        """
        self.is_shared_with_substation = is_shared

        # Get serial instance for use
        # UPDATE: Commented out because this was overwriting Devices.ser which is set in configuration.py,
        # thus we do not need to assign self.ser to Ser.serial_conn here because it has already been established
        # self.ser = Ser.serial_conn
        # Messages.__init__(self, _mg=_mg)
        # Initialize device type
        self.dv_type = ''

        # ----- Instance Variables ----- #
        self.sn = _sn       # Serial Number
        self.ad = _ad       # Address Number
        self.ds = _ds       # Description
        self.la = _la       # Latitude
        self.lg = _lg       # Longitude
        self.ty = _ty       # Type
        self.ss = _ss       # Status
        # Initialize place holder for data received from controller when needed.
        self.data = status_parser.KeyValues(string_in=None)
        # this is a place holder for a date range of the test
        self.calendar_range = None       # this is the calendar ra range of the test

        # For Manufacturing Base Units
        self.vr = None  # version

        # For Manufacturing Remote Units
        self.r_va = None  # current
        self.r_vv = None  # voltage
        self.r_temp_sn = self.sn
        self.final_sn = [self.sn, "VB" + self.sn[2:], "VE" + self.sn[2:]]
        self.r_decoder_type = ''
        # self.test_bu.r_final_sn[1] = "VB{0}".format(self.test_bu.r_final_sn[0][2:])
        # self.test_bu.r_final_sn[2] = "VE{0}".format(self.test_bu.r_final_sn[0][2:])
        # self.final_sn[0] = self.sn

        # messages
        self.build_message_string = ''  # place to store the message that is compared to the controller message

    #################################
    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    def build_description(self):
        """
        Builds the description for all non-controller devices. \n
        :return:    Returns the description for the device. \n
        :rtype:     str \n
        """
        description_dict = {
            "ZN": "Test Zone",
            "MS": "Test Moisture Sensor",
            "MV": "Test Master Valve",
            "FM": "Test Flow Meter",
            "TS": "Test Temp Sensor",
            "SW": "Test Event Switch",
            "AR": "Test Alert Relay"
        }

        description = "{0} {1} {2}".format(
            self.sn,
            description_dict[self.dv_type],
            str(self.ad)
        )

        return description
    # TODO this needs work

    # #################################
    # def set_date_range_for_test(self,start_date, end_date):
    #     self.date_mngr.set_dates_from_date_range(start_date=start_date, end_date=end_date)
    #     #
    #     # start = datetime.datetime.strptime(start_date, "%d-%m-%Y")
    #     # end = datetime.datetime.strptime(end_date, "%d-%m-%Y")
    #     # date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end-start).days)]
    #     #
    #     # for date in date_generated:
    #     #     return date.strftime("%d-%m-%Y")

    #################################
    def set_address_on_cn(self, _ad=None):
        """
        Address the specified device type to the controller. \n
        :param _ad:             Address to overwrite current address.
        :type _ad:   int \n
        :return:
        """
        if _ad is not None:
            self.ad = _ad

        # Example command: 'DO,AZ=TSD0001,NU=1'
        command = "DO,{0}={1},NU={2}".format(
            self.dv_assign_op_code_dict[self.dv_type],
            str(self.sn),
            str(self.ad)
        )
        try:
            print "Sending command: " + command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to address {0} {1} with serial: {2} to the controller".format(
                self.dv_type,
                str(self.ad),
                self.sn
            )
            raise Exception(e_msg)
        else:
            print("Successfully addressed Device: {0}, Serial: {1}, Address {2}".format(self.dv_type, self.sn,
                                                                                        str(self.ad)))

    #################################
    def set_description_on_cn(self, _ds=None):
        """
        Sets the description of the device on the controller. If a description is specified in the parameter,
        it will overwrite the current set description and set the new one at the controller as well as in the object. \n
        :param _ds:             Description to overwrite current object description.
        :return:
        """
        # Checks for valid device type to use this method.
        if self.dv_type not in common_vars.list_of_device_types:
            e_msg = "Invalid device type: {0} entered while trying to send ds to cn".format(str(self.dv_type))
            raise ValueError(e_msg)

        # If a description is passed in
        if _ds is not None:
            self.ds = str(_ds)

        # Use Case: Trying to set 1000 controller description, verifies length is within controller boundaries
        if self.dv_type == "CN" and self.controller_type == "10" and len(self.ds) > 31:
            raise ValueError("Attempted to set a description to 1000 that is too long: {0}".format(str(len(self.ds))))

        # Use Case: Trying to set 3200 controller description, verifies length is within controller boundaries
        elif self.dv_type == "CN" and self.controller_type == "32" and len(self.ds) > 43:
            raise ValueError("Attempted to set a description to 3200 that is too long: {0}".format(str(len(self.ds))))

        # If we are a zone, need to use address instead of serial number on set command
        if self.dv_type == "ZN":
            command = "SET,{0}={1},DS={2}".format(self.dv_type, str(self.ad), str(self.ds))
        else:
            command = "SET,{0}={1},DS={2}".format(self.dv_type, str(self.sn), str(self.ds))

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0}'s ds to: {1}".format(self.dv_type, self.ds)
            raise Exception(e_msg)
        else:
            print("Successfully set description for device: {0} to: {1}".format(self.dv_type, self.ds))

    #################################
    def set_latitude_on_cn(self, _la=None):
        """
        Sets the current latitude for the object on the controller. If a latitude is passed in as a parameter,
        it will overwrite the object's latitude as well as set the new latitude on the controller. \n
        :param _la:             Latitude to overwrite current object latitude
        :return:
        """
        # If a latitude is passed in
        try:
            if _la is not None:
                self.la = helper_methods.format_lat_long(_lat=_la)
            else:
                self.la = helper_methods.format_lat_long(_lat=self.la)
        except Exception as e:
            e_msg = "Exception occurred trying to set latitude for device: {0}\n".format(self.dv_type) + str(e.message)
            raise Exception(e_msg)

        if self.dv_type not in common_vars.list_of_device_types:
            e_msg = "Invalid device type: {0} entered while trying to send latitude to cn".format(str(self.dv_type))
            raise ValueError(e_msg)

        # If we are a zone, need to use address instead of serial number on set command
        if self.dv_type == "ZN":
            command = "SET,{0}={1},LA={2}".format(self.dv_type, str(self.ad), str(self.la))
        else:
            command = "SET,{0}={1},LA={2}".format(self.dv_type, str(self.sn), str(self.la))

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0}'s latitude to: {1}".format(self.dv_type, self.la)
            raise Exception(e_msg)
        else:
            print("Successfully set latitude for device: {0}, to: {1}".format(self.dv_type, str(self.la)))

    #################################
    def set_longitude_on_cn(self, _lg=None):
        """
        Sets the current longitude for the object on the controller. If a longitude is passed in as a parameter,
        it will overwrite the object's longitude as well as set the new longitude on the controller. \n
        :param _lg:          Longitude to overwrite current object longitude
        :return:
        """
        # If a description is passed in
        try:
            if _lg is not None:
                self.lg = helper_methods.format_lat_long(_lat=_lg)
            else:
                self.lg = helper_methods.format_lat_long(_lat=self.lg)
        except Exception as e:
            e_msg = "Exception occurred trying to set longitude for device: {0}\n".format(self.dv_type) + str(e.message)
            raise Exception(e_msg)

        if self.dv_type not in common_vars.list_of_device_types:
            e_msg = "Invalid device type: {0} entered while trying to send longitude to cn".format(str(self.dv_type))
            raise ValueError(e_msg)

        # If we are a zone, need to use address instead of serial number on set command
        if self.dv_type == "ZN":
            command = "SET,{0}={1},LG={2}".format(self.dv_type, str(self.ad), str(self.lg))
        else:
            command = "SET,{0}={1},LG={2}".format(self.dv_type, str(self.sn), str(self.lg))

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0}'s lg to: {1}".format(self.dv_type, str(self.lg))
            raise Exception(e_msg)
        else:
            print("Successfully set longitude for device: {0}, to: {1}".format(self.dv_type, str(self.lg)))

    #################################
    def set_message_on_cn(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        :param _helper_object
        :type _helper_object : BaseStartStopPause
        """
        try:
            build_message_string = messages.message_to_set(self,
                                                           _status_code=_status_code,
                                                           _ct_type=self.dv_type,
                                                           _helper_object=_helper_object)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.set_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=build_message_string)

    def get_data(self):
        """
        Gets the data for the device with specified 'address' or 'serial' and stores it in the place holder
        'self.data'. \n
        """

        # Case 1: Controller Device
        if self.dv_type == 'CN':
            command = "GET,{0}".format(
                self.dv_type    # {0}
            )
        # Case 2: Zone (need to use address for get command)
        elif self.dv_type == 'ZN':
            command = "GET,{0}={1}".format(
                self.dv_type,   # {0}
                str(self.ad)    # {1}
            )

        # Case 3: Moisture Sensor, Temperature Sensor, Flow Meter, Master Valve, Event Switch (need serial)
        else:
            command = "GET,{0}={1}".format(
                self.dv_type,   # {0}
                self.sn         # {1}
            )

        # Attempt to get data from controller
        try:
            self.data = self.ser.get_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Unable to get data for device: '{0}' using command: '{1}'. Exception raised: {2}".format(
                    self.dv_type,       # {0}
                    command,            # {1}
                    str(ae.message)     # {2}
            )
            raise ValueError(e_msg)

    #################################
    def get_message_on_cn(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        :param _helper_object
        :type _helper_object : BaseStartStopPause
        """
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=self.dv_type,
                                                                _helper_object=_helper_object)
            cn_mg = self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            msg = "Exception caught in messages.get_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=self.build_message_string[opcodes.message])
            return cn_mg

    ###################################
    def clear_message_on_cn(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        :param _helper_object
        :type _helper_object : BaseStartStopPause
        """
        try:
            build_message_string = messages.message_to_clear(self,
                                                             _status_code=_status_code,
                                                             _ct_type=self.dv_type,
                                                             _helper_object=_helper_object)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.clear_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=build_message_string)
        # Verifies that the message was cleared by catching the 'NM' command that we would expect back
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=self.dv_type,
                                                                _helper_object=_helper_object)
            self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            if e.message == "NM No Message Found":
                print "Message cleared and verified that the message is gone."
            else:
                # TODO should we make this throw an exception or leave it as is?
                print "Message cleared, but was unable to verify if the message was gone."

    #################################
    def do_self_test(self):
        """
        Do a test on the device
        """

        if self.dv_type not in common_vars.list_of_device_types:
            e_msg = "Invalid device type: {0} entered while trying to send longitude to cn".format(str(self.dv_type))
            raise ValueError(e_msg)

        # If we are a zone, need to use address instead of serial number on set command
        if self.dv_type == "ZN":
            command = "DO,DT,{0}={1}".format(self.dv_type, str(self.ad))
        else:
            command = "DO,DT,{0}={1}".format(self.dv_type, str(self.sn))

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to test {0}'s".format(self.dv_type)
            raise Exception(e_msg)
        else:
            print("Successfully tested: {0}".format(self.dv_type))

    #################################
    def verify_description_on_cn(self):
        """
        Verifies the description set on the controller for the specified device type. \n
        :return:
        """
        ds_on_cn = self.data.get_value_string_by_key('DS')

        # If we are a zone, use zone address to identify it in the exception print string, otherwise use serial number
        # Here, 'arg1' represents a Zone address or any other device serial number
        if self.dv_type == "ZN":
            arg1 = str(self.ad)
        else:
            arg1 = self.sn

        # Compare descriptions
        if self.ds != ds_on_cn:
            e_msg = "Unable verify {0}: {1}'s description. Received: {2}, Expected: {3}".format(
                    self.dv_type,   # {0}
                    arg1,           # {1}
                    str(ds_on_cn),  # {2}
                    str(self.ds)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s description: '{2}' on controller".format(
                  self.dv_type,     # {0}
                  arg1,             # {1}
                  self.ds           # {2}
                  ))

    def get_data_and_verify_status_on_cn(self, status):
        """
        Get the dat from the controller and than
        Verifies the status on the controller for the specified device type. \n
        :return:
        """
        self.get_data()
        self.verify_status_on_cn(status=status)

    #################################
    def verify_status_on_cn(self, status):
        """
        Verifies the status on the controller for the specified device type. \n
        :return:
        """

        # ToDo: Change common_vars... references to OpCodes

        if status not in common_vars.dictionary_for_status_codes:
            e_msg = "Unable to verify {0} {1}'s status. Received invalid expected status: ({2}),\n" \
                    " Expected one of the following: ({3})".format(
                        str(self.dv_type),                                  # {0}
                        str(self.ad),                                       # {1}
                        str(status),                                        # {2}
                        common_vars.dictionary_for_status_codes.keys()      # {3}
                    )
            raise KeyError(e_msg)
        dv_ss_on_cn = self.data.get_value_string_by_key(opcodes.status_code)
        # If we are a zone, use zone address to identify it in the exception print string, otherwise use serial number
        # Here, 'arg1' represents a Zone address or any other device serial number
        if self.dv_type == opcodes.zone:
            arg1 = str(self.ad)
        else:
            arg1 = self.sn

        # Since status is a 'get' only, we want to overwrite our object with the expected status passed in to retain
        # the state of our object.
        self.ss = status

        # Compare status versus what is on the controller
        if self.ss != dv_ss_on_cn:
            e_msg = "Unable verify {0}: {1}'s status. Received: {2}, Expected: {3}".format(
                    self.dv_type,       # {0}
                    arg1,               # {1}
                    str(dv_ss_on_cn),   # {2}
                    str(self.ss)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s status: '{2}' on controller".format(
                  self.dv_type,     # {0}
                  arg1,             # {1}
                  self.ss           # {2}
                  ))


    #################################
    def verify_latitude_on_cn(self):
        """
        Verifies the latitude set on the controller for the specified device type. \n
        :return:
        """
        la_on_cn = float(self.data.get_value_string_by_key('LA'))

        # If we are a zone, use zone address to identify it in the exception print string, otherwise use serial number
        # Here, 'arg1' represents a Zone address or any other device serial number
        if self.dv_type == "ZN":
            arg1 = str(self.ad)
        else:
            arg1 = self.sn

        # Compare latitudes
        # if self.la != la_on_cn:
        if abs(self.la - la_on_cn) > 0.0000001:
            e_msg = "Unable verify {0}: {1}'s 'latitude'. Received: {2}, Expected: {3}".format(
                    self.dv_type,   # {0}
                    arg1,           # {1}
                    str(la_on_cn),  # {2}
                    str(self.la)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s latitude: '{2}' on controller".format(
                self.dv_type,   # {0}
                arg1,           # {1}
                str(self.la)    # {2}
            ))

    #################################
    def verify_longitude_on_cn(self):
        """
        Verifies the longitude set on the controller for the specified device type. \n
        :return:
        """
        lg_on_cn = float(self.data.get_value_string_by_key('LG'))

        # If we are a zone, use zone address to identify it in the exception print string, otherwise use serial number
        # Here, 'arg1' represents a Zone address or any other device serial number
        if self.dv_type == "ZN":
            arg1 = str(self.ad)
        else:
            arg1 = self.sn

        # Compare longitudes
        # if self.lg != lg_on_cn:
        if abs(self.lg - lg_on_cn) > 0.0000001:
            e_msg = "Unable verify {0}: {1}'s 'longitude'. Received: {2}, Expected: {3}".format(
                    self.dv_type,   # {0}
                    arg1,           # {1}
                    str(lg_on_cn),  # {2}
                    str(self.lg)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s longitude: '{2}' on controller".format(
                  self.dv_type,     # {0}
                  arg1,             # {1}
                  str(self.lg)      # {2}
                  ))
            
    #################################
    def verify_serial_number_on_cn(self):
        """
        Verifies the Serial Number set on the controller for the specified device type. \n
        :return:
        """
        sn_on_cn = self.data.get_value_string_by_key('SN')

        # Compare Serial Numbers
        if self.sn != sn_on_cn:
            e_msg = "Unable verify {0}: {1}'s 'Serial Number'. Received: {2}, Expected: {3}".format(
                    self.dv_type,   # {0}
                    str(self.ad),   # {1}
                    str(sn_on_cn),  # {2}
                    str(self.sn)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s Serial Number: '{2}' on controller".format(
                  self.dv_type,     # {0}
                  str(self.ad),     # {1}
                  str(self.sn)      # {2}
                  ))

    #################################
    def verify_message_on_cn(self, _status_code, _helper_object=None):
        """
        the expected values are retrieved from the controller and than compared against the values that are returned
        from the message module
        :param _status_code
        :type _status_code :str
        :param _helper_object
        :type _helper_object : BaseStartStopPause
        :return:
        """
        mg_on_cn = self.get_message_on_cn(_status_code=_status_code, _helper_object=_helper_object)
        expected_message_text_from_cn = mg_on_cn.get_value_string_by_key(opcodes.message_text)
        expected_message_id_from_cn = mg_on_cn.get_value_string_by_key(opcodes.message_id)
        expected_date_time_from_cn = mg_on_cn.get_value_string_by_key(opcodes.date_time)
        # Compare status versus what is on the controller
        if self.build_message_string[opcodes.message_id] != expected_message_id_from_cn:
            e_msg = "Created ID message did not match the ID received from the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_id],  # {0} The ID that was built
                        expected_message_id_from_cn  # {1} The ID returned from controller
                    )
            raise ValueError(e_msg)

        mess_date_time = datetime.strptime(self.build_message_string[opcodes.date_time], '%m/%d/%y %H:%M:%S')
        controller_date_time = datetime.strptime(expected_date_time_from_cn, '%m/%d/%y %H:%M:%S')
        if mess_date_time != controller_date_time:
            if abs(controller_date_time - mess_date_time) >= timedelta(days=0, hours=0, minutes=60, seconds=0):
                e_msg = "The date and time of the message didn't match the controller:\n" \
                        "\tCreated: \t\t'{0}'\n" \
                        "\tReceived:\t\t'{1}'\n".format(
                            self.build_message_string[opcodes.date_time],  # {0} The date message that was built
                            expected_date_time_from_cn  # {1} The date message returned from controller
                        )
                raise ValueError(e_msg)
            # only print this if they were not exact
            e_msg = "############################  Date and time were not exact but were within +- 30 minutes \n" \
                    "############################  Controller Date and Time = {0} \n" \
                    "############################  Message Date time = {1}".format(
                        expected_date_time_from_cn,  # {0} Date time received from controller
                        self.build_message_string[opcodes.date_time]  # {1} Date time the message was verified
                    )
            print(e_msg)

        if self.build_message_string[opcodes.message_text] != expected_message_text_from_cn:
            e_msg = "Created TX message did not match the TX received from the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_text],  # {0} The TX message that was built
                        expected_message_text_from_cn  # {1} The TX message returned from controller
                    )
            raise ValueError(e_msg)
        else:
            print "Successfully verified:\n" \
                  "\tID: '{0}'\n" \
                  "\tDT: '{1}'\n" \
                  "\tTX: '{2}'\n".format(
                        self.build_message_string[opcodes.message_id],
                        self.build_message_string[opcodes.date_time],
                        self.build_message_string[opcodes.message_text]
                    )
