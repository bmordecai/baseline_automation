__author__ = 'Tige'

from old_32_10_sb_objects_dec_29_2017.common.imports import *
from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.locators import *


class MobileBasePage(object):
    
    def __init__(self, web_driver):
        """
        Init base page instance
        :param web_driver:  WebDriver instance \n
        :type web_driver:   WebDriver \n
        """
        self.driver = web_driver


class MobileMainPage(MobileBasePage):

    def select_a_menu_tab(self, _menu_name_locators, _sub_menu_locators=None):
        """
        pass in the value for the main tab menus by name if the tab has sub menus pass in the name of the sub menu
        when a sub menu is used the hover over command drops the menu down so that the sub menu can be selected
        the hover command must execute before the the sub menu can be selected
        if an incorrect  value is passed in a value error is raised and the program stops
        :param, menu_name:, quick view, programs, liveview, devices, watersource
        :param, sub_menu:, zones, moisture, flow, master valve, temperature, events switches, point of connections,
        mainlines
        :rtype: str
        """
        if _menu_name_locators not in [MainPageLocators.MAPS_TAB,
                                       MainPageLocators.QUICK_VIEW_TAB,
                                       MainPageLocators.PROGRAMS_TAB,
                                       MainPageLocators.DEVICES_TAB,
                                       MainPageLocators.WATER_SOURCES_TAB,
                                       MainPageLocators.LIVEVIEW_TAB]:
            raise ValueError("Menu Name is incorrect:"+' ' + str(_menu_name_locators))

        # Try and locate the indicated menu item. If not found, handle exception raised by web driver
        try:
            # If the menu name is within the set below, the menu has sub-menus that we need to hover to access
            if _menu_name_locators in (MainPageLocators.MAPS_TAB,
                                       MainPageLocators.DEVICES_TAB,
                                       MainPageLocators.WATER_SOURCES_TAB):
                self.driver.find_element_then_click(_menu_name_locators)

                # Give animation time
                time.sleep(0.5)
                #select a subMenu
                if _sub_menu_locators is not None:
                    self.select_a_sub_menu(sub_menu_locators=_sub_menu_locators)
            # A menu tab without a sub-list of menu items has been requested
            else:
                # Get the menu tab id from the dictionary
                self.driver.find_element_then_click(_menu_name_locators)
                # Give animation time
                time.sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except NoSuchElementException:
            raise ValueError("Unable to locate menu item: " + str(_menu_name_locators)+str(_sub_menu_locators))
        else:
            print "-------------------------------------------------------------------"
            print "| - Successfully Selected Main Menu > " + str(_menu_name_locators) + " > " + str(_sub_menu_locators)
            print "-------------------------------------------------------------------"

    def select_a_sub_menu(self, sub_menu_locators):
        """
        Selects a sub menu from a known list of available sub menu's. If a sub menu name isn't found,
        a ValueError is raised, signifying a misspelled sub menu name or an invalid sub menu selection.
        When the user specifies the 'markers' sub menu option, they must also specify which 'marker'
        sub menu they would like to access, i.e., 'marker_menu_options' shown below.
        :param sub_menu_locators:            Sub menu options available.
        :return:
        """
        # Iterate through supported sub menu options, if found a match, get the respective web_id associated
        if sub_menu_locators not in [MainPageLocators.COMPANY,
                                     MainPageLocators.CURRENT_SITE,
                                     MainPageLocators.CURRENT_CONTROLLER,
                                     MainPageLocators.ZONES,
                                     MainPageLocators.MOISTURE_SENSORS,
                                     MainPageLocators.FLOW,
                                     MainPageLocators.MASTER_VALVES,
                                     MainPageLocators.TEMPERATURE,
                                     MainPageLocators.EVENT_SWITCHES,
                                     MainPageLocators.POINT_OF_CONNECTION,
                                     MainPageLocators.MAINLINES]:
            raise ValueError("Bad Sub Menu Name" + ' ' + sub_menu_locators)

        try:
            # Attempt to locate indicated sub menu, if found and 'click-able', click it.
            self.driver.find_element_then_click(sub_menu_locators)
            
            # give animation some time to finish
            time.sleep(0.5)
            
        # Handle exception when we are unable to locate sub menu item before clicking
        except StaleElementReferenceException:
            raise StaleElementReferenceException("Unable to locate sub menu item: " + str(sub_menu_locators))

    def select_main_menu(self):
        """
        Selects the main upper left menu.
        """
        try:
            # Look for the upper left main menu and click it
            self.driver.wait_for_element_clickable(MainPageLocators.MAIN_MENU_BUTTON)
            self.driver.find_element_then_click(MainPageLocators.MAIN_MENU_BUTTON)

        except NoSuchElementException as e:
            e_msg = "Unable to locate main menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,     # {0}
                e.message                       # {1}
            )
            raise NoSuchAttributeException(e_msg)

        else:
            # for animation to finish
            time.sleep(1)

    def wait_for_selected_controller_to_reconnect(self):
        """
        Attempts to wait for the controller to go offline and to come back online.
        :return:
        :rtype:
        """
        status = self.driver.get_status(MainPageLocators.FOOTER_CONTROLLER_STATUS)

        while status != "No Connection":
            time.sleep(2)
            status = self.driver.get_status(MainPageLocators.FOOTER_CONTROLLER_STATUS)

        while status != "Done":
            time.sleep(2)
            status = self.driver.get_status(MainPageLocators.FOOTER_CONTROLLER_STATUS)

        print "Selected controller has reconnected to BaseManager."

    def select_site(self, site_name=None):
        """
        Helper method for selecting a site from upper left menu. Navigation is as follows:
        menu -> sites and controllers -> (Next available options are selectable sites)
        :param site_name: Site to be selected.
        :return:
        """
        locator = (By.ID, '')

        if site_name is None:
            site_name = self.driver.conf.site_name

        try:
            # Look for 'Sites and Controllers' sub-menu option and click it to display sites
            self.driver.wait_for_element_clickable(MainMenuLocators.SITES)
            self.driver.find_element_then_click(MainMenuLocators.SITES)

            # Look for indicated site, site_name
            site_list_index = 1
            site_selected = None

            # Loop through each site and compare site names, if equal click the site.
            while not site_selected:

                # Construct site 'id' for locating
                current_web_id = 'menu-sites-%i' % site_list_index

                # Wait for element to be present, and attempt to click
                locator = (By.ID, current_web_id)
                self.driver.wait_for_element_clickable(locator)

                # Get the site to compare the name against what we are looking for.
                site_selected = self.driver.web_driver.find_element(*locator)

                # correct site?
                if str(site_selected.text.lower()).lstrip() == site_name.lower():
                    # yes
                    site_selected.click()
                    time.sleep(1)
                else:
                    # Keep looping, site label text didn't match site_name
                    site_list_index += 1
                    # set to None to remain in the while loop.
                    site_selected = None

            print "Successfully Selected Site: {0}".format(str(site_name))

        except NoSuchElementException as e:
            e_msg = "Unable to locate site name element and click WebElement ID: {0}. {1}".format(
                locator,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def select_a_controller(self, mac_address):
        """
        pass in the controller mac address as a string the method waits for the controller status to change colors
        before selecting the ID
        :param mac_address:     Mac Address of controller to select. \n
        :type mac_address:      str \n
        :rtype:                 str \n
        """
        controller_web_id = 'menu-controllers-' + mac_address
        locator = (By.ID, controller_web_id)
        try:
            # Verify controller is online
            self.verify_controller_is_online(mac_address=mac_address)

            # Count number of controller's online
            self.driver.number_of_controllers_online = self.count_number_of_controllers_online()

            # Look for the last needed option, the controller being selected with the specified mac add
            self.driver.wait_for_element_clickable(locator)

            # Click on the controller
            self.driver.find_element_then_click(locator)

            print "Successfully Selected Controller [mac_address = {0}]".format(mac_address)

            # Wait for animations to finish
            time.sleep(5)

        except NoSuchElementException:
            raise NoSuchElementException("Unable to locate controller with mac_address: %s" % str(mac_address))
    
    def select_a_company(self, company=None):
        """
        Method assumes Super User is signing in to Base Manager. \n
        :param company:     A company to select. \n
        :type company:      str \n
        """
        locator = (By.ID, '')
        if company is None:
            company = self.driver.conf.company

        try:
            # Look for 'Companies' sub-menu option and click it to display sites
            self.driver.wait_for_element_clickable(MainMenuLocators.COMPANIES)
            self.driver.find_element_then_click(MainMenuLocators.COMPANIES)

            # Look for indicated company, company_name
            company_list_index = 0
            company_selected = None

            # Loop through each company and compare company names, if equal click the company.
            while not company_selected:

                # Construct company 'id' for locating
                current_web_id = 'menu-companies-{0}'.format(company_list_index)

                # Wait for element to be present, and attempt to click
                locator = (By.ID, current_web_id)
                self.driver.wait_for_element_clickable(locator)

                # Get the company to compare the name against what we are looking for.
                company_selected = self.driver.web_driver.find_element(*locator)

                # correct company?
                if str(company_selected.text).lstrip() == company:
                    # yes
                    company_selected.click()
                    time.sleep(1)
                else:
                    # Keep looping, company label text didn't match company_name
                    company_list_index += 1
                    # set to None to remain in the while loop.
                    company_selected = None

            print "Successfully Selected Company: {0}".format(str(company))

        except NoSuchElementException as e:
            e_msg = "Unable to locate Company name element and click WebElement ID: {0}. {1}".format(
                locator,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def click_logout_button(self):
        pass

    def wait_for_main_menu_close(self):
        """
        Waits for the main menu to close (sliding out of view).
        """

        is_open = True
        count = 1
        while is_open:

            # returns true if the menu element has the "slidein" css class, this means that the menu is still showing.
            is_open = "slidein" in self.driver.web_driver.find_element(*MainPageLocators.MAIN_MENU)\
                .get_attribute("class")

            # 30 seconds elapsed?
            if count == 30 and is_open:
                e_msg = "Timed out waiting for main menu to close after 30 seconds. Still trying to download company " \
                        "data?"
                raise TimeoutException(e_msg)

            # < 30 seconds elapsed
            else:
                time.sleep(1)
                count += 1

        # tell us how we did
        print "Successfully waited for main menu to close."

        # Give client a change to catch up, wait for animation to finish
        time.sleep(2)

    def count_number_of_controllers_online(self):
        """
        First, get the list of controller elements
        Next, for each element check whether the controllers name is in black (online) text color or in gray (offline)
        Return number of controllers online
        :return:
        """
        controllers_online = 0
        controller_web_elements = self.driver.web_driver.find_elements_by_class_name('controller')

        for each_controller in controller_web_elements:
            self.driver.total_number_of_controllers += 1
            try:
                current_mac = each_controller.get_attribute('mac')
                self.verify_controller_is_online(mac_address=current_mac)
                controllers_online += 1
            except AssertionError:
                pass

        return controllers_online

    def verify_controller_is_online(self, mac_address):
        """
        Waits for the controller to come online. If the controller element label is Gray in color, the controller is
        offline, so keep checking the color of the controller element label until it is not Gray, meaning it's
        online.\n
        In these terms, the label is the visible name on the controller, and we are refering to the color of the
        text. \n
        :param mac_address:
        :return:
        """
        controller_web_id = 'menu-controllers-label-' + str(mac_address)
        locator = (By.ID, str(controller_web_id))

        is_offline = True
        count = 1
        while is_offline:

            # returns true if controller has the css class 'status-disconnected' otherwise, returns false
            is_offline = "status-disconnected" in self.driver.web_driver.find_element(*locator).get_attribute("class")

            if count == 30 and is_offline:
                raise AssertionError("Waited for controller with mac_address: %s and timed out after 30 seconds."
                                     % str(mac_address))
            else:
                time.sleep(1)
                count += 1

        print "Controller: " + str(mac_address) + " is online."

    def get_color_of_element_in_hex(self, css_property, css_selector=None, web_id=None):
        """
        Finds an element by it's css_selector
        Strips string of non-integer values and assigns to r, g, b respectively
        Formats the values into a Hex value represented as a string
            Example color returned from browser:    "rgba(72, 72, 72, 1)"
            Example output converted by method:     "#484848" (Black)
        :param css_selector:
        :param css_property:
        :return:
        """
        if not self.driver.conf.unit_testing:
            pass
            # time.sleep(5)
        color = ''

        if web_id is not None:
            color = str(self.driver.web_driver.find_element_by_id(id_=web_id).value_of_css_property(css_property))

        if css_selector is not None:
            color = str(self.driver.web_driver.find_element_by_css_selector(css_selector).value_of_css_property(
                str(css_property)))

        # Waits up to 60 seconds for a pop-up dialogue box to display.
        count = 1
        if color == 'transparent':
            while color == 'transparent':
                # Added below so it doesn't print so many "Alert is present" messages to console
                if web_id is not None:
                    color = str(self.driver.web_driver.find_element_by_id(id_=web_id).value_of_css_property(
                        css_property))
                else:
                    color = str(self.driver.web_driver.find_element_by_css_selector(css_selector).value_of_css_property(
                        str(css_property)))
                if count % 100 == 0:
                    print "Waiting for status color to not be transparent"
                    count = 1
                else:
                    count += 1

        print (color)
        strip_leading_characters = color.split('(')[1:]
        strip_ending_characters = strip_leading_characters[0].split(')')[:1]
        color_numbers = strip_ending_characters[0].split(',')
        r = int(color_numbers[0])
        g = int(color_numbers[1])
        b = int(color_numbers[2])
        color_in_hex = '#%02x%02x%02x' % (r, g, b)

        try:
            color_temp_conversion = common_vars.dictionary_reverse_status_color_dictionary[color_in_hex]
            color_name_converted = common_vars.dictionary_for_status_colors[color_temp_conversion]
        except KeyError:
            raise KeyError("Found color with hex value: " + str(color_in_hex) + ", not recognized by test.")
        else:
            print "Color in hex = [%s], %s" % (color_in_hex, color_name_converted)
            return color_in_hex
