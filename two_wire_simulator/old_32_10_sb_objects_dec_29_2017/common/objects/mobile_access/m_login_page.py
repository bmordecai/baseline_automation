__author__ = 'Tige'
from old_32_10_sb_objects_dec_29_2017.common.imports import *
from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.locators import *

from old_32_10_sb_objects_dec_29_2017.common.objects.mobile_access.m_main_page import MobileBasePage


class MobileLoginPage(MobileBasePage):
    """
    login Object \n
    """

    def enter_login_info(self, _user_name=None, _password=None):
        """
        Types in username and password and clicks the login button.
        :return:
        """
        # user = _user_name if _user_name is not None else self.driver.conf.user_name
        if _user_name is not None:
            user = _user_name
        else:
            user = self.driver.conf.user_name
        # Clear input and enter in username
        self.driver.send_text_to_element(LoginLocators.USER_NAME_INPUT, text_to_send=user)

        # password = _password if _password is not None else self.driver.conf.user_password
        if _password is not None:
            password = _password
        else:
            password = self.driver.conf.user_password
        # clear input and enter in password
        self.driver.send_text_to_element(LoginLocators.PASSWORD_INPUT, text_to_send=password)

    def verify_open(self):
        #TODO move to mobile main page
        # Wait for the page to load and the main menu to be visible.
        self.driver.wait_for_element_visible(MobileAccessLocators.GO_TO_DESKTOP_VERSION)

    def verify_login_error(self):
        """
        this looks at the text displayed when an incorrect username or password is entered
        :return:
        :rtype:     str\n
        """
        login_error_text = self.driver.web_driver.find_element(LoginLocators.LOGIN_ERROR).text
        expected = 'Your username or password is invalid'
        if login_error_text != expected:
            e_msg = "login error '{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected),           # {0}
                str(login_error_text),   # {1}
            )
            raise ValueError(e_msg)

    def click_login_button(self):
        """
        Clicks the login button and wait's 5 seconds
        :return:
        :rtype:
        """
        # Finds and selects the login button in order to login
        self.driver.find_element_then_click(LoginLocators.SIGN_IN_BUTTON)

        # Wait for the page to successfully load displaying "Connected" status in the upper right.
        self.driver.wait_for_element_visible(MainPageLocators.MAIN_MENU_BUTTON)

        # Wait for the page to load and the main menu to be visible.
        self.driver.wait_for_element_visible(MainPageLocators.MAIN_MENU_BUTTON)

        # Wait for page to fully load
        time.sleep(5)
    #TODO move id to login locators
    #TODO verify links are still active
