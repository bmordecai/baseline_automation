__author__ = 'baseline'

# Selenium Imports
from selenium.common.exceptions import *

# from old_32_10_sb_objects_dec_29_2017.common.locators import MapTabLocators
# import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page as bm_main
# import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page as mp
import locators
import main_page


class MapsTab(main_page.BasePage):
    
    page_title = 'Maps Tab'

    #################################
    def verify_open(self):
        """
        Verifies that the Maps tab is open by looking for the edit button
        :return:
        :rtype:
        """
        if self.driver.is_visible(_locator=locators.MapTabLocators.MAP_EDIT_BUTTON):
            print "Successfully verified the Maps Tab is open."
        else:
            e_msg = "Unable to verify Maps Tab is open. Couldn't find the edit button in the bottom right"
            raise Exception(e_msg)

    #################################
    def open_edit_menu(self):
        """
        Selects the edit button on the bottom of the view to display the maps edit menu.
        """
        if self.driver.is_visible(_locator=locators.MapTabLocators.MAP_EDIT_BUTTON):
            self.driver.find_element_then_click(_locator=locators.MapTabLocators.MAP_EDIT_BUTTON)
        else:
            e_msg = "{0}: Unable to locate edit button to select. Wasn't visible.".format(
                self.page_title
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def select_markers_menu_option(self):
        """
        Selects the Markers menu option in the Map Edit menu.
        """
        if self.driver.is_visible(_locator=locators.MapTabLocators.MARKERS_MENU_OPTION):
            self.driver.find_element_then_click(_locator=locators.MapTabLocators.MARKERS_MENU_OPTION)
        else:
            e_msg = "{0}: Unable to locate Markers Menu Option to select. Wasn't visible.".format(
                self.page_title
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def verify_flowstation_markers_are_hidden_jira_bm_1924(self):
        """
        Added for regression testing against a v12 3200 where the FlowStation markers menu option was being\n
        displayed when this is only a v16 3200 feature.
        """
        what_is_being_verified = "FlowStation Markers Menu Option"
        jira_bug_ref = "BM-1924 (https://baseline.atlassian.net/browse/BM-1924)"
    
        if not self.driver.is_visible(_locator=locators.MapTabLocators.FLOWSTATION_MARKERS_MENU_OPTION):
            print "{0} Detail View: Successfully verified '{1}' is not visible.\n->\tThis verifies " \
                  "JIRA Bug: {2}".format(
                      self.page_title,
                      what_is_being_verified,
                      jira_bug_ref
                  )
        else:
            e_msg = "\n{0} Detail View: Unable to verify '{1}' is not visible.\n->\tIt should not be visible " \
                    "for v12 3200 on BaseManager.\n->\tSee JIRA Bug: {2}\n".format(
                        self.page_title,
                        what_is_being_verified,
                        jira_bug_ref
                    )
            raise AssertionError(e_msg)
