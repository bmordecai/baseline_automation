__author__ = 'baseline'

from selenium.webdriver.common.by import By


#################################
class ControllerSettingsLocators(object):
    """
    Locators that are specific to the controller setting info dialog box
    """
    CLOSE_BUTTON = (By.ID, "trEditControllerSerial")
    CANCEL_BUTTON = (By.ID, "trEditControllerSerial")
    EDIT_BUTTON = (By.ID, "editControllerDesc")
    INPUT_BOX_FOR_DATABASE_ID = (By.ID, "user-update-input")
    INPUT_BOX_FOR_NAME = (By.ID, "controllerEditName")
    SAVE_BUTTON = (By.ID, "editControllerDesc")
    SEND_FIRMWARE_VERSION_BUTTON = (By.ID, "upgradeFwSpecific")
    SERIAL_NUMBER_TEXT_LOCATION = (By.ID, "trEditControllerSerial")


#################################
class DeviceTabLocators(object):
    """
    Locators for the device tab. \n
    """
    CANCEL_BUTTON = (By.ID, "sacancel")
    EVENT_SWITCH_ASSIGN = (By.ID, "switchAssign")
    EVENT_SWITCH_REFRESH_BUTTON = (By.ID, "switchRefreshButton")
    FLOW_METER_REFRESH_BUTTON = (By.ID, "flowRefreshButton")
    FLOW_METER_ASSIGN = (By.ID, "flowAssign")
    MASTER_VALVE_ASSIGN = (By.ID, "masterAssign")
    MASTER_VALVE_REFRESH_BUTTON = (By.ID, "masterRefreshButton")
    MOISTURE_SENSOR_ASSIGN = (By.ID, "moistureAssign")
    MOISTURE_SENSOR_REFRESH_BUTTON = (By.ID, "moistureRefreshButton")
    SAVE_BUTTON = (By.ID, "sasave")
    TEMPERATURE_REFRESH_BUTTON = (By.ID, "temperatureRefreshButton")
    TEMPERATURE_SENSOR_ASSIGN = (By.ID, "temperatureAssign")
    
    ZONE_ASSIGN = (By.ID, "zoneAssign")
    ZONE_REFRESH_BUTTON = (By.ID, "zoneRefreshButton")
    ZONE_EDIT_BUTTON = (By.ID, 'editZoneButton')
    ZONE_EDIT_SAVE_BUTTON = (By.ID, 'zoneEditSave')
    ZONE_EDIT_CANCEL_BUTTON = (By.ID, 'zoneEditCancel')

    ZONE_SUMMARY_HEADER = (By.ID, 'zoneGeneralSettings')
    ZONE_EDIT_DESCRIPTION_INPUT = (By.ID, 'zone-description')
    
    EDIT_BUTTON = (By.ID, "{dv_type}-{serial}-edit")
    EDIT_DIALOG_HEADER = (By.ID, "dvtitle")
    EDIT_DIALOG_DESCRIPTION_INPUT = (By.ID, "tpzone-description")
    EDIT_DIALOG_SAVE_BUTTON = (By.ID, "dvsave")


#################################
class DialogBoxLocators(object):
    """
    Dialog box locators. \n
    """
    INFO_DIALOG = (By.ID, "infoDialog")


#################################
class LoginLocators(object):
    USER_NAME_INPUT = (By.ID, "username")
    PASSWORD_INPUT = (By.ID, "password")
    SIGN_IN_BUTTON = (By.ID, "login")
    LOGIN_ERROR = (By.ID, "loginError")
    USE_PREVIOUS_VERSION = (By.ID, "desktop")
    GO_TO_MOBILE_VERSION = (By.ID, "mobile")
    RETRIEVE_FROGOTTEN_PASSWORD = (By.ID, "lostpwd")
    SET_UP_A_NEW_ACCOUNT = (By.ID, "support")


#################################
class MainPageLocators(object):
    """
    Locator's that are not tab specific, but include tabs
    """
    COLOR_BUTTON = (By.ID, "colorbutton")
    COMPANY = (By.ID, "global_view_l")
    CONTROLLER_SETTINGS = (By.ID, "cnteditbutton")
    CURRENT_CONTROLLER = (By.ID, "controller_view_l")
    CURRENT_SITE = (By.ID, "company_view_l")
    DEVICES_TAB = (By.ID, "zonesensor_view_l")
    EVENT_SWITCHES = (By.ID, "switch_view_l")
    FLOW = (By.ID, "flow_view_l")
    FOOTER_CONTROLLER_STATUS = (By.ID, "footer_controller_status")
    LIVEVIEW_TAB = (By.ID, "remote_view_l")
    MAINLINES = (By.ID, "mainline_view_l")
    MAIN_MENU = (By.ID, "menu")
    MAIN_MENU_BUTTON = (By.ID, "mainmenu")
    MAPS_TAB = (By.ID, "maptype")
    MASTER_VALVES = (By.ID, "mv_view_l")
    MOISTURE_SENSORS = (By.ID, "moisture_view_l")
    POINT_OF_CONNECTION = (By.ID, "poc_view_l")
    PROGRAMS_TAB = (By.ID, "programs_view_l")
    QUICK_VIEW_TAB = (By.ID, "manage_view_l")
    TEMPERATURE = (By.ID, "temperature_view_l")
    WATER_SOURCES_TAB = (By.ID, "water_view_l")
    ZONES = (By.ID, "zones_view_l")
    LOGOUT = (By.ID, "logout")  # This is for the old website


#################################
class MainMenuLocators(object):
    """
    Locators that are specific to the menu
    """
    ADMINISTRATION = (By.ID, "menu-admin")
    COMPANIES = (By.ID, "menu-companies")
    RAIN_DELAY = (By.ID, "menu-rain-delay")
    REPORTS = (By.ID, "menu-reports")
    SITES = (By.ID, "menu-sites-and-controllers")
    SIGN_OUT = (By.ID, "menu-logout")
    SUBSCRIPTIONS = (By.ID, "menu-subscriptions")


#################################
class MapTabLocators(object):
    """
    All locators associated with map tab.
    """
    MENU_TAB_ITEM = (By.ID, "map_view_li")
    MAP_EDIT_BUTTON = (By.ID, "mapEditButton")
    COMPANY_VIEW = (By.ID, "global_view_l")
    SITE_VIEW = (By.ID, "company_view_l")
    CONTROLLER_VIEW = (By.ID, "controller_view_l")
    MARKERS_MENU_OPTION = (By.ID, "map-menu-markers")
    FLOWSTATION_MARKERS_MENU_OPTION = (By.ID, "map-menu-markers-type-flowStations")


#################################
class MobileAccessLocators(object):
    GO_TO_DESKTOP_VERSION = (By.ID, "td-desktop")


#################################
class ProgramsTabLocators(object):
    """
    Locator's for the programs tab
    """

    #################################
    class DetailView(object):
        HEADER_TEXT = (By.CSS_SELECTOR, "#programsListDetail > div.scrollwrapper > h1")
        VIEW = (By.ID, "programsListDetail")
        SAVE_BUTTON = (By.ID, "programEditSave")
        CANCEL_BUTTON = (By.ID, "programEditCancel")
        ENABLE_ET_CHECKBOX = (By.ID, "program-enable-et-label")
        ZONE_ADD_ROW = (By.ID, "zoneAdd")
        MAINLINE_DESCRIPTION = (By.ID, "programMainLine")

    #################################
    class ListView(object):
        SEARCH_FIELD = (By.ID, "programsListSearch")
        ADD_PROGRAM_ROW = (By.ID, "programAdd")
        PROGRAM_ROW = (By.ID, "programRow-{pg_number}")


#################################
class QuickViewLocators(object):
    """
    Locator's that are specific to the QuickView Tab menu
    """
    ZONE_STATUS_HEADER = (By.ID, "quickview_zone_status_header")
    
    # Status box locators
    ZN_STATUS_BOX = (By.ID, "idZoneStatus_{address}")
    PG_STATUS_BOX = (By.ID, "idProgramStatus_{address}")
    MV_STATUS_BOX = (By.ID, "idMVStatus_{address}_{serial}")
    TOOLTIP_DESCRIPTION = (By.ID, "qtip-tooltip-title")
    MS_STATUS_ROW = (By.ID, "idMoistureStatus_{serial}")
    MS_DESCRIPTION = (By.ID, "qv_moisturesensor1_{serial}")
    TS_STATUS_ROW = (By.ID, "idTemperatureStatus_{serial}")
    TS_DESCRIPTION = (By.ID, "qv_temperaturesensor1_{serial}")
    FM_STATUS_ROW = (By.ID, "idFlowStatus_{serial}")
    FM_DESCRIPTION = (By.ID, "qv_flowmeter1_{serial}")
    SW_STATUS_ROW = (By.ID, "idSwitchStatus_{serial}")
    SW_DESCRIPTION = (By.ID, "qv_switch1_{serial}")


#################################
class SubscriptionPageLocators(object):
    """
    Locator's that are specific to the Subscriptions page
    """
    SUBSCRIPTION_HEADER = (By.ID, "subscriptions_view_header")
    SUBSCRIPTION_EDIT = (By.ID, "subscriptionEditButton")
    SUBSCRIPTION_TYPE_LITE = (By.ID, "subscriptionsLabelLite")
    SUBSCRIPTION_TYPE_BASEMANAGER = (By.ID, "subscriptionsLabelBM")
    SUBSCRIPTION_TYPE_PLUS = (By.ID, "subscriptionsLabelBMP")
    SUBSCRIPTION_RENEWAL_DATE = (By.ID, "renewalDate")
    SUBSCRIPTION_SAVE_EDIT = (By.ID, "subscriptionSaveButton")


#################################
class WaterSourcesLocators3200(object):
    """
    Locator's that are specific to the 3200 Water Sources Tab
    """
    POINT_OF_CONNECTION_VIEW = (By.ID, "poc_view")
    MAINLINES_VIEW = (By.ID, "mainline_view")


#################################
class PocLocators3200(object):
    """
    Locator's that are specific to the v12 3200 POC Tab
    """

    #################################
    class ListView(object):
        # headers
        HEADER = (By.ID, "pocListHeaderText")
        ID_COLUMN_HEADER = (By.ID, "poc_status_table_id_header")
        DESCRIPTION_COLUMN_HEADER = (By.ID, "poc_status_table_desc_header")
        MAINLINE_COLUMN_HEADER = (By.ID, "poc_status_table_mainline_header")
        # row
        POC_ROW = (By.ID, "")
        # column values
        DESCRIPTION_COLUMN_VALUE = (By.ID, "pocRow-{poc_number}-description")
        MAINLINE_COLUMN_VALUE = (By.ID, "pocRow-{poc_number}-mainline")

    #################################
    class DetailView(object):
        # view
        MAIN_VIEW = (By.ID, "pocDetail")
        # buttons
        EDIT_BUTTON = (By.ID, "poc_edit_button")
        SAVE_BUTTON = (By.ID, "poc_save_button")
        # attributes
        DESCRIPTION_INPUT = (By.ID, "poc_description")
        MAINLINE_ASSIGNMENT = (By.ID, "poc_mainline")
        MV_ENABLE = (By.ID, "poc_master_valve_enabled_value")
        POC_ID = (By.ID, "poc_id")
        SHARE_THIS_POC = (By.ID, "poc_flowstation_assigned")
        SW_EMPTY_LIMIT_ELEMENT = (By.ID, "poc_event_switch_empty_limit_value")
        SW_EMPTY_TIME_ELEMENT = (By.ID, "poc_event_switch_empty_time_value")
        
        
#################################
class MainlineLocators3200(object):
    """
    Locator's that are specific to the v12 3200 Mainline Tab
    """
    
    #################################
    class ListView(object):
        # headers
        HEADER = (By.ID, "pocListHeaderText")
        ID_COLUMN_HEADER = (By.ID, "mainline_status_table_id_header")
        DESCRIPTION_COLUMN_HEADER = (By.ID, "mainline_status_table_desc_header")
        # row
        MAINLINE_ROW = (By.ID, "mainlineRow-{ml_number}")
        # column values
        DESCRIPTION_COLUMN_VALUE = (By.CSS_SELECTOR, "#mainlineRow-{ml_number} > td.desc")
    
    #################################
    class DetailView(object):
        # view
        MAIN_VIEW = (By.ID, "mainlineDetail")
        MAINLINE_ID = (By.ID, "mainline_id")
        # buttons
        EDIT_BUTTON = (By.ID, "mainline_edit_button")
        SAVE_BUTTON = (By.ID, "mainline_save_button")
        # attributes
        DESCRIPTION_INPUT = (By.ID, "mainline_description")
