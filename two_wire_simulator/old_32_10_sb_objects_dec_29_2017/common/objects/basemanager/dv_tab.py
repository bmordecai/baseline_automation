__author__ = 'bens'

# Selenium Imports
from selenium.common.exceptions import *
from selenium.webdriver.common.by import By

import bm_enums
import locators
import main_page

device_type_dict = {
    "ZN": "Zones Tab",
    "MS": "Moisture Sensor Tab",
    "FM": "Flow Meter Tab",
    "TS": "Temperature Sensor Tab",
    "MV": "Master Valve Tab",
    "SW": "Event Switch Tab"
}


class DevicesTab(main_page.BasePage):
    """
    Base Devices Tab Class

    Theory:
        -   If you need a method in the Zones tab to do something, chances are you will need to use the same method
            throughout all of the devices.
        -   Same can be said for attributes.

    Purpose:
        -   This class provides methods for all device tabs to use to simulate on BaseManager.
        -   Any methods that are needed in the Zones tab or Event Switches tab should be considered in this base class
            first, for inheritance by all devices.
        -   Any attribute needed by the Zones tab or any other device tab should have a blank place holder added to
            this base class as well, again for inheritance by all classes.
    """
    def __init__(self, web_driver):
        # init super class
        main_page.BasePage.__init__(self, web_driver=web_driver)
        
        self.driver = web_driver
        
        # ==== START OF DEVICE ATTRIBUTES ==== #
        #   - Add attributes as needed across inheriting classes. Must be added here and set to None or ''
        #       i.e.: some_new_attribute = ''

        # Device Page Attributes (ID's, other constants)
        self.page_title = tuple()
        self.device_type = tuple()

        # Base id string placeholder for all inheriting classes
        self.summary_view_base_id = tuple()

        # Holder for each child's objects
        from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import devices
        self.objects = {1: devices.Devices}

        # Locator attributes for each child
        self.refresh_button_locator = tuple()
        self.assign_bicoder_locator = tuple()
        
        # default locators used for all non-zone devices
        self.edit_button_locator = locators.DeviceTabLocators.EDIT_BUTTON
        self.save_button_locator = locators.DeviceTabLocators.EDIT_DIALOG_SAVE_BUTTON
        self.cancel_button_locator = tuple()
        self.edit_view_header_locator = locators.DeviceTabLocators.EDIT_DIALOG_HEADER
        self.description_input_locator = locators.DeviceTabLocators.EDIT_DIALOG_DESCRIPTION_INPUT

    # ==== END OF DEVICE ATTRIBUTES ==== #

    #################################
    def select_save_button(self):
        """
        Selects the save button for the device (in edit view)
        """
        if self.driver.is_visible(_locator=self.save_button_locator):
            self.driver.find_element_then_click(_locator=self.save_button_locator)
            self.driver.handle_dialog_box()
        else:
            e_msg = "Unable to locate '{type} Edit Button' for {page}. Wasn't visible.".format(
                type=self.device_type,
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def verify_is_open(self):
        """
        Verifies that the devices tab of the specified type is open by verifying the refresh button at the top of the
        screen
        """
        if self.driver.is_visible(_locator=self.refresh_button_locator):
            print "Successfully verified {page_title} is open".format(page_title=self.page_title)
        else:
            e_msg = "Unable to verify {page_title} is open. Couldn't find element with id: {web_element}".format(
                page_title=self.page_title,
                web_element=self.refresh_button_locator
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def verify_non_zone_edit_dialog_is_open(self):
        """
        Verifies the edit dialog for non-zone device is displayed.
        """
        if self.driver.wait_for_element_visible(_locator=self.edit_view_header_locator):
            print "Successfully verified {page_title} is open".format(page_title=self.page_title)
        else:
            e_msg = "Unable to verify {page_title} is open. Couldn't find element with id: {web_element}".format(
                page_title=self.page_title,
                web_element=self.edit_view_header_locator
            )
            raise ElementNotVisibleException(e_msg)

    def select_assign_bicoders(self):
        """
        Selects the Assign biCoders option.
        """
        if self.driver.is_visible(_locator=self.assign_bicoder_locator):
            self.driver.find_element_then_click(_locator=self.assign_bicoder_locator)

        else:
            e_msg = "Unable to locate 'Assign biCoders' button for {page}. Wasn't visible.".format(
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)
        self.driver.handle_dialog_box()

    def save_device_assignment(self):
        """
        Selects the save button at the bottom of the Assign biCoder
        :return:
        :rtype:
        """
        locator = locators.DeviceTabLocators.SAVE_BUTTON
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
        else:
            e_msg = "Unable to locate 'Assign biCoders' SAVE button. Not visible to user from page: {page}".format(
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)

        self.driver.handle_dialog_box()

        if self.device_type == "ZN":
            locator = (By.ID, self.summary_view_base_id.format(header_index=2, serial=self.objects[1].ad))

        else:
            locator = (By.ID, self.summary_view_base_id.format(header_index=2, serial=self.objects[1].sn))

        self.driver.wait_for_element_visible(_locator=locator)
        self.driver.wait_for_element_text_change(_locator=locator, _text=self.objects[1].ds)

    def cancel_device_assignment(self):
        """
        Selects the cancel button at the bottom of the Assign biCoder
        :return:
        :rtype:
        """
        locator = locators.DeviceTabLocators.CANCEL_BUTTON
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
        else:
            e_msg = "Unable to locate 'Assign biCoders' CANCEL button. Not visible to user from page: {page}".format(
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)

    def set_description(self, address):
        """
        Sets the description of the addressed object on BaseManager. This assumes we are in the 'assign bicoder' screen.
        :param address: Address of device to set description for
        :type address: int
        """
        locator = (By.ID, "decoderdesc_" + str(address))
        current_obj = self.objects[address]

        if self.driver.is_visible(_locator=locator):
            self.driver.send_text_to_element(_locator=locator, text_to_send=current_obj.ds, clear_field=True)
        else:
            e_msg = "Unable to set description for {d_type} {addr}: {desc}".format(
                d_type=self.device_type,
                addr=address,
                desc=current_obj.ds
            )
            raise ElementNotVisibleException(e_msg)

    def set_assigned_serial_number(self, address):
        """
        Assigns the serial number to the device with the specified address. This assumes we are in the 'assign
        bicoder' screen.
        :param address: Address of device to set serial number for
        :type address: int
        """
        locator = (By.ID, "decoderselect_" + str(address))
        current_obj = self.objects[address]

        if self.driver.is_visible(_locator=locator):
            self.driver.send_text_to_element(_locator=locator, text_to_send=current_obj.sn, drop_down_element=True)
        else:
            e_msg = "Unable to set serial number for {d_type} {addr}: {serial}".format(
                d_type=self.device_type,
                addr=address,
                serial=current_obj.sn
            )
            raise ElementNotVisibleException(e_msg)

    def verify_address(self, address):
        """
        Verifies the address/sensor sn for the device with the given address. This method assumes we are in the
        regular device "view" / "summary list view"
        :param address: Address of device to set serial number for
        :type address: int
        """
        current_obj = self.objects[address]

        # if we are a zone, use the address as "serial" for formatting the string, other devices use actual serial
        # numbers in the id's
        if self.device_type == "ZN":
            header_id = bm_enums.DeviceSummaryViewHeaders.ZONE
            serial = current_obj.ad
        else:
            header_id = bm_enums.DeviceSummaryViewHeaders.SENSOR
            serial = current_obj.sn

        # insert the header index and device identifier (address for zones, otherwise is serial)
        locator = (By.ID, self.summary_view_base_id.format(
            header_index=header_id,
            serial=serial
        ))

        # Get the text from the web element
        if self.driver.is_visible(_locator=locator):
            address_serial_from_bm = self.driver.get_text_from_web_element(_locator=locator)
        else:
            e_msg = "Unable to get address/serial for {d_type} {addr}: {serial}. Unable to locate element.".format(
                d_type=self.device_type,
                addr=address,
                serial=current_obj.sn
            )
            raise ElementNotVisibleException(e_msg)

        # Equal?
        if str(serial) != str(address_serial_from_bm):
            e_msg = "Unable to verify address/serial on BM for {d_type} {addr}: {serial}. Expected: {exp_ad}, " \
                    "Received: {rec_ad}".format(
                        d_type=self.device_type,
                        addr=address,
                        serial=current_obj.sn,
                        exp_ad=serial,
                        rec_ad=address_serial_from_bm
                    )
            raise ValueError(e_msg)

        # Yes equal
        else:
            print "Successfully verified {d_type} {addr} ({serial}) address/serial of: {ad}".format(
                d_type=self.device_type,
                addr=address,
                serial=current_obj.sn,
                ad=address_serial_from_bm
            )

    def verify_description(self, address):
        """
        Verifies the selected device description based on the given address. This method assumes we are in the
        regular device "view" / "summary list view"
        :param address: Address of device to set serial number for
        :type address: int
        """
        current_obj = self.objects[address]

        # if we are a zone, use the address as "serial" for formatting the string, other devices use actual serial
        # numbers in the id's
        if self.device_type == "ZN":
            serial = current_obj.ad
        else:
            serial = current_obj.sn

        # insert the header index and device identifier (address for zones, otherwise is serial)
        locator = (By.ID, self.summary_view_base_id.format(
            header_index=bm_enums.DeviceSummaryViewHeaders.DESCRIPTION,
            serial=serial
        ))

        # Get the text from the web element
        if self.driver.is_visible(_locator=locator):
            description = self.driver.get_text_from_web_element(_locator=locator)
        else:
            e_msg = "Unable to get description for {d_type} {addr}: {serial}. Unable to locate element.".format(
                d_type=self.device_type,
                addr=address,
                serial=current_obj.sn
            )
            raise ElementNotVisibleException(e_msg)

        # Equal?
        if current_obj.ds != description:
            e_msg = "Unable to verify description on BM for {d_type} {addr}: {serial}. Expected: {exp_desc}, " \
                    "Received: {rec_desc}".format(
                        d_type=self.device_type,
                        addr=address,
                        serial=current_obj.sn,
                        exp_desc=current_obj.ds,
                        rec_desc=description
                    )
            raise ValueError(e_msg)

        # Yes equal
        else:
            print "Successfully verified {d_type} {addr} ({serial}) description of: {desc}".format(
                d_type=self.device_type,
                addr=address,
                serial=current_obj.sn,
                desc=description
            )

    #################################
    def select_non_zone_device_edit_button(self, serial):
        """
        Selects the edit button for the zone.
        """
        locator = self.format_locator_multi_key(
            locator=self.edit_button_locator,
            keyvals=[
                ["dv_type", self.device_type],
                ["serial", serial]
            ]
        )
        if self.driver.wait_for_element_clickable(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
            self.driver.handle_dialog_box()
        else:
            e_msg = "Unable to locate '{type} {serial}'s Edit Button' for {page}. Wasn't visible.".format(
                type=self.device_type,
                serial=serial,
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def set_description_in_edit_view(self, address, description):
        """
        Sets the description of the addressed object on BaseManager. This assumes we are in the 'assign bicoder' screen.
        :param address: Address of device to set description for
        :type address: int
        :param description: The new description to set for zone
        :type description: str
        """
        # locator for description input
        locator = self.description_input_locator
    
        # update objects description attribute
        current_obj = self.objects[address]
        current_obj.ds = description
    
        # input new description
        if self.driver.is_visible(_locator=locator):
            self.driver.send_text_to_element(_locator=locator, text_to_send=description, clear_field=True)
        else:
            e_msg = "Unable to set description for {d_type} {addr}: {desc}".format(
                d_type=self.device_type,
                addr=address,
                desc=description
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def verify_description_updates(self, address):
        """
        Verifies the description of the device at index "address"
        :param address: Address of device in device dictionary
        :type address: int
        """
        if self.device_type == "ZN":
            # insert the header index and device identifier (address for zones, otherwise is serial)
            locator = (By.ID, self.summary_view_base_id.format(
                header_index=bm_enums.DeviceSummaryViewHeaders.DESCRIPTION,
                serial=address
            ))
            # locator = (By.ID, self.summary_view_base_id.format(header_index=2, serial=self.objects[1].ad))

        else:
            locator = (By.ID, self.summary_view_base_id.format(
                header_index=bm_enums.DeviceSummaryViewHeaders.DESCRIPTION,
                serial=self.objects[address].sn
            ))
        
        # get description from dv object
        dv_description = self.objects[address].ds
        
        try:
            self.driver.wait_for_element_visible(_locator=locator)
            self.driver.wait_for_element_text_change(_locator=locator, _text=dv_description)
        except WebDriverException as e:
            e_msg = "Unable to verify new description for {dvtype} {address}: {expected}.\nException: {e}".format(
                dvtype=self.device_type,
                address=address,
                expected=dv_description,
                e=e.msg
            )
            raise WebDriverException(e_msg)


class ZonesTab(DevicesTab):

    def __init__(self, driver):
        # init super class
        DevicesTab.__init__(self, web_driver=driver)

        # get reference to Zone objects
        from old_32_10_sb_objects_dec_29_2017.common.objects import object_bucket
        self.objects = object_bucket.zones

        # Page Specific Attributes
        self.page_title = "Zones Tab"
        self.device_type = "ZN"

        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.ZONE_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.ZONE_REFRESH_BUTTON

        self.edit_button_locator = locators.DeviceTabLocators.ZONE_EDIT_BUTTON
        self.save_button_locator = locators.DeviceTabLocators.ZONE_EDIT_SAVE_BUTTON
        self.cancel_button_locator = locators.DeviceTabLocators.ZONE_EDIT_CANCEL_BUTTON
        
        self.edit_view_header_locator = locators.DeviceTabLocators.ZONE_SUMMARY_HEADER
        self.description_input_locator = locators.DeviceTabLocators.ZONE_EDIT_DESCRIPTION_INPUT

        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "zonedetail_gridzone1_TSD0001"
        self.summary_view_base_id = "zonedetail_gridzone{header_index}_{serial}"

    #################################
    def select_zone(self, address):
        """
        Select a zone from the zone list
        :param address: The address of the zone to select.
        :type address: int
        """
        # insert the header index and device identifier (address for zones, otherwise is serial)
        locator = (By.ID, self.summary_view_base_id.format(
            header_index=bm_enums.DeviceSummaryViewHeaders.DESCRIPTION,
            serial=address
        ))
        
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)

        else:
            e_msg = "Unable to locate 'Zone Row' for {page}. Wasn't visible.".format(
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def select_zone_edit_button(self):
        """
        Selects the edit button for the zone.
        """
        if self.driver.is_visible(_locator=self.edit_button_locator):
            self.driver.find_element_then_click(_locator=self.edit_button_locator)
        else:
            e_msg = "Unable to locate 'Zone Edit Button' for {page}. Wasn't visible.".format(
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)
        
    #################################
    def verify_zone_detail_view_is_visible(self):
        """
        Verifies the zone detail view is visible.
        """
        try:
            self.driver.wait_for_element_visible(_locator=self.edit_view_header_locator)
        except WebDriverException as e:
            e_msg = "Unable to verify zone detail view is visible.\nException: {0}".format(
                e.msg
            )
            raise WebDriverException(e_msg)


class MoistureSensorsTab(DevicesTab):

    def __init__(self, driver):
        # init super class
        DevicesTab.__init__(self, web_driver=driver)

        # get reference to Moisture Sensor objects
        from old_32_10_sb_objects_dec_29_2017.common.objects import object_bucket
        self.objects = object_bucket.moisture_sensors

        # Page Specific Attributes
        self.page_title = "Moisture Sensors Tab"
        self.device_type = "MS"

        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.MOISTURE_SENSOR_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.MOISTURE_SENSOR_REFRESH_BUTTON

        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "moisturedetail_sensor1_TSD0001"
        self.summary_view_base_id = "moisturedetail_sensor{header_index}_{serial}"


class FlowMetersTab(DevicesTab):

    def __init__(self, driver):
        # init super class
        DevicesTab.__init__(self, web_driver=driver)

        # get reference to Flow Meter objects
        from old_32_10_sb_objects_dec_29_2017.common.objects import object_bucket
        self.objects = object_bucket.flow_meters

        # Page Specific Attributes
        self.page_title = "Flow Meters Tab"
        self.device_type = "FM"

        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.FLOW_METER_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.FLOW_METER_REFRESH_BUTTON

        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "flowmeterdetail_sensor1_TSD0001"
        self.summary_view_base_id = "flowmeterdetail_sensor{header_index}_{serial}"


class MasterValvesTab(DevicesTab):

    def __init__(self, driver):
        # init super class
        DevicesTab.__init__(self, web_driver=driver)

        # get reference to Master Valve objects
        from old_32_10_sb_objects_dec_29_2017.common.objects import object_bucket
        self.objects = object_bucket.master_valves

        # Page Specific Attributes
        self.page_title = "Master Valves Tab"
        self.device_type = "MV"

        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.MASTER_VALVE_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.MASTER_VALVE_REFRESH_BUTTON

        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "mastervalvedetail_sensor1_TSD0001"
        self.summary_view_base_id = "mastervalvedetail_sensor{header_index}_{serial}"
        

class TemperatureSensorsTab(DevicesTab):

    def __init__(self, driver):
        # init super class
        DevicesTab.__init__(self, web_driver=driver)

        # get reference to Temperature Sensor objects
        from old_32_10_sb_objects_dec_29_2017.common.objects import object_bucket
        self.objects = object_bucket.temperature_sensors

        # Page Specific Attributes
        self.page_title = "Temperature Sensors Tab"
        self.device_type = "TS"

        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.TEMPERATURE_SENSOR_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.TEMPERATURE_REFRESH_BUTTON

        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "tempdetail_sensor1_TSD0001"
        self.summary_view_base_id = "tempdetail_sensor{header_index}_{serial}"


class EventSwitchTab(DevicesTab):

    def __init__(self, driver):
        # init super class
        DevicesTab.__init__(self, web_driver=driver)

        # get reference to Event Switch objects
        from old_32_10_sb_objects_dec_29_2017.common.objects import object_bucket
        self.objects = object_bucket.event_switches

        # Page Specific Attributes
        self.page_title = "Event Switch Tab"
        self.device_type = "SW"

        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.EVENT_SWITCH_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.EVENT_SWITCH_REFRESH_BUTTON

        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "eventswitchdetail_sensor1_TSD0001"
        self.summary_view_base_id = "eventswitchdetail_sensor{header_index}_{serial}"
