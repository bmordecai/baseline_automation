__author__ = 'baseline'

from old_32_10_sb_objects_dec_29_2017.common.imports import *
from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.locators import *
# import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page as main_page
import main_page
from selenium.common.exceptions import ElementNotVisibleException


class LiveViewTab(main_page.BasePage):

    def verify_open(self):
        """
        Verifies that the Live View Tab opens successfully by verifying text at the top of the page. \n
        """
        # LiveView panel selector
        locator = (By.ID, "remote_content")
        try:
            live_view_tab = self.driver.web_driver.find_element(*locator)

            if not live_view_tab.is_displayed():
                raise ElementNotVisibleException("Unable to locate Live View Panel.")

        except Exception as ve:
            caught_text = ve.message
            e_msg = "Unable to verify LiveView tab is open. Exception received: {0}".format(
                caught_text
            )
            raise Exception(e_msg)
        else:
            print "Successfully verified LiveView Tab opened"