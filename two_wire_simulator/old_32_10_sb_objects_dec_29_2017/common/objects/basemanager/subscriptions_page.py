__author__ = 'Eldin'

from old_32_10_sb_objects_dec_29_2017.common.imports import *
from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.locators import *
# import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page as main_page
import main_page
import time
from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.locators import *
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchAttributeException

class BasePage(object):

    def __init__(self, web_driver):
        """
        Init base page instance
        :param web_driver:  WebDriver instance \n
        :type web_driver:   driver.WebDriver
        """
        self.driver = web_driver


class SubscriptionsPage(main_page.BasePage):

    def verify_open(self):
        # TODO move this to the baseclass
        # Wait for the page to successfully load displaying "Subscriptions" in the header
        self.driver.wait_for_element_visible(SubscriptionPageLocators.SUBSCRIPTION_HEADER)
        time.sleep(3)

    def click_edit_button(self):
        """
        Clicks the edit button if the current page is Subscriptions
        """
        try:
            # Finds the element
            self.driver.find_element_then_click(SubscriptionPageLocators.SUBSCRIPTION_EDIT)

            print "Successfully Selected Edit Subscriptions."

        except NoSuchElementException as e:
            e_msg = "Unable to locate the subscription edit button and click WebElement ID: {0}. {1}".format(
                SubscriptionPageLocators.SUBSCRIPTION_EDIT,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def click_save_button(self):
        """
        Clicks the save button if the current page is Subscriptions and the edit button was clicked
        """
        try:
            # Finds the element
            self.driver.find_element_then_click(SubscriptionPageLocators.SUBSCRIPTION_SAVE_EDIT)

            print "Successfully Selected Edit Subscriptions."

        except NoSuchElementException as e:
            e_msg = "Unable to locate the subscription save button and click WebElement ID: {0}. {1}".format(
                SubscriptionPageLocators.SUBSCRIPTION_EDIT,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def select_lite_subscription(self, mac_address=None):
        """
        Method assumes Super User is signing in to Base Manager. Selects the lite option after you're in the edit menu \n
        :param mac_address:     A company to select. \n
        :type mac_address:      str \n
        """
        locator = (By.ID, '')

        if mac_address is None:
            mac_address = self.driver.conf.mac_address

        try:
            for controller in self.driver.web_driver.find_elements_by_class_name("controllerrow"):
                controller_id = controller.get_attribute("controllerid")
                current_subscription = controller.find_element_by_id("subscriptionController_{0}".format(controller_id))
                current_mac = current_subscription.find_element_by_class_name("mac").get_attribute("innerHTML").strip()
                if current_mac == mac_address:
                    locator = SubscriptionPageLocators.SUBSCRIPTION_TYPE_LITE
                    locator = (By.ID, locator[1] + "_{0}".format(controller_id))
                    if self.driver.is_visible(_locator=locator):
                        self.driver.find_element_then_click(locator)
                    print "Successfully selected subscription type lite for Controller mac address: {0}".format(
                        str(mac_address))
                    break

        except NoSuchElementException as e:
            e_msg = "Unable to locate necessary elements and click WebElement ID: {0}. {1}".format(
                locator,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def select_bm_subscription(self, mac_address=None):
        """
        Method assumes Super User is signing in to Base Manager. Selects the BM option after you're in the edit menu \n
        :param mac_address:     A controller mac address \n
        :type mac_address:      str \n
        """
        locator = (By.ID, '')

        if mac_address is None:
            mac_address = self.driver.conf.mac_address

        try:
            for controller in self.driver.web_driver.find_elements_by_class_name("controllerrow"):
                controller_id = controller.get_attribute("controllerid")
                current_subscription = controller.find_element_by_id("subscriptionController_{0}".format(controller_id))
                current_mac = current_subscription.find_element_by_class_name("mac").get_attribute("innerHTML").strip()
                if current_mac == mac_address:
                    locator = SubscriptionPageLocators.SUBSCRIPTION_TYPE_BASEMANAGER
                    locator = (By.ID, locator[1] + "_{0}".format(controller_id))
                    if self.driver.is_visible(_locator=locator):
                        self.driver.find_element_then_click(locator)
                    print "Successfully selected subscription type BaseManager for Controller mac address: {0}".format(str(mac_address))
                    break

        except NoSuchElementException as e:

            e_msg = "Unable to locate necessary elements and click WebElement ID: {0}. {1}".format(
                locator,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def select_plus_subscription(self, mac_address=None):
        """
        Method assumes Super User is signing in to Base Manager. Selects the plus option after you're in the edit menu \n
        :param mac_address:     A controller mac address to select. \n
        :type mac_address:      str \n
        """
        locator = (By.ID, '')

        if mac_address is None:
            mac_address = self.driver.conf.mac_address

        try:
            for controller in self.driver.web_driver.find_elements_by_class_name("controllerrow"):
                # Gets the controller ID that is currently being looped through
                controller_id = controller.get_attribute("controllerid")
                current_subscription = controller.find_element_by_id("subscriptionController_{0}".format(controller_id))
                # Gets the mac address text from the website
                current_mac = current_subscription.find_element_by_class_name("mac").get_attribute("innerHTML").strip()
                if current_mac == mac_address:
                    locator = SubscriptionPageLocators.SUBSCRIPTION_TYPE_PLUS
                    locator = (By.ID, locator[1] + "_{0}".format(controller_id))
                    if self.driver.is_visible(_locator=locator):
                        self.driver.find_element_then_click(locator)
                    print "Successfully selected subscription type BaseManager Plus for Controller mac address: {0}".format(str(mac_address))
                    break

        except NoSuchElementException as e:

            e_msg = "Unable to locate necessary elements and click WebElement ID: {0}. {1}".format(
                locator,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def set_renewal_date(self, date_string, mac_address=None):
        """
        Takes in a date and mac address and changes the
        :param date_string:     The date to set as the new renewal date. \n
        :type date_string:      str \n
        :param mac_address:     A controller mac address to select. \n
        :type mac_address:      str \n
        """
        locator = (By.ID, '')

        # Strips leading zeros
        date_string = date_string.lstrip("0")

        # Gets the default mac address in the configuration if one isn't passed in
        if mac_address is None:
            mac_address = self.driver.conf.mac_address

        try:
            # Loops through the controller rows
            for controller in self.driver.web_driver.find_elements_by_class_name("controllerrow"):
                controller_id = controller.get_attribute("controllerid")
                current_subscription = controller.find_element_by_id("subscriptionController_{0}".format(controller_id))
                current_mac = current_subscription.find_element_by_class_name("mac").get_attribute("innerHTML").strip()
                # if current_mac == mac_address:
                # Use "in" checker to allow for extra characters to be next to MAC address field. i.e., (No MEXP-BM)
                # text
                if mac_address in current_mac:
                    locator = SubscriptionPageLocators.SUBSCRIPTION_RENEWAL_DATE
                    locator = (By.ID, locator[1] + "_{0}".format(controller_id))
                    if self.driver.is_visible(_locator=locator):
                        self.driver.web_driver.execute_script("document.getElementById('{0}').setAttribute('value', '{1}')".format(locator[1], date_string))
                        break
                    else:
                        e_msg = "Unable to set value for renewal date to: {0} for mac address: {1}".format(
                            date_string,
                            mac_address
                        )
                        raise ElementNotVisibleException(e_msg)


        except NoSuchElementException as e:

            e_msg = "Unable to locate necessary elements and click WebElement ID: {0}. {1}".format(
                locator,
                e.message
            )
            raise NoSuchElementException(e_msg)


    def select_a_company(self, company=None):
        """
        Method assumes Super User is signing in to Base Manager. \n
        :param company:     A company to select. \n
        :type company:      str \n
        """
        locator = (By.ID, '')
        if company is None:
            company = self.driver.conf.company

        try:
            # Look for 'Companies' sub-menu option and click it to display sites
            self.driver.wait_for_element_clickable(MainMenuLocators.COMPANIES)
            self.driver.find_element_then_click(MainMenuLocators.COMPANIES)

            # This list represents list of known DB ids for Automated_Test_Systems company on P1, P2 and P3.
            # This was added to hopefully make subscriptions test faster (faster searching for the company to select).
            # To use a company outside of this list, the ID of that company needs to be inserted.
            #
            # Sorry for this BAD HACK :)
            automated_test_systems_company_db_ids = [
                533,    # DB ID on P1
                219,    # DB ID on P2
                240     # DB ID on P3
            ]

            # Look for indicated company, company_name
            # TODO set this to 0 once the bug that stops us from looping through is fixed
            company_list_index = 0
            company_selected = None

            # Loop through each company and compare company names, if equal click the company.
            while not company_selected:
                
                automated_test_systems_db_id_to_use = automated_test_systems_company_db_ids[company_list_index]

                # Construct company 'id' for locating
                current_web_id = 'menu-companies-{0}'.format(automated_test_systems_db_id_to_use)
                
                print "Looking for company: " + current_web_id

                # Wait for element to be present, and attempt to click
                locator = (By.ID, current_web_id)
                
                # If we were unable to find the element to click, try a different menu index
                if not self.driver.is_visible(locator):
                    company_list_index += 1
                    
                # Otherwise, we were able to find a company, now lets see if it's the one we want
                else:
                    # Get the company to compare the name against what we are looking for.
                    company_selected = self.driver.web_driver.find_element(*locator)

                    # correct company?
                    if str(company_selected.text).lstrip() == company:
                        # yes
                        company_selected.click()
                        time.sleep(1)
                    else:
                        # Keep looping, company label text didn't match company_name
                        company_list_index += 1
                        # set to None to remain in the while loop.
                        company_selected = None

            print "Successfully Selected Company: {0}".format(str(company))

        except NoSuchElementException as e:
            e_msg = "Unable to locate Company name element and click WebElement ID: {0}. {1}".format(
                locator,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def select_main_menu(self):
        """
        Selects the main upper left menu.
        """
        try:
            # Look for the upper left main menu and click it
            self.driver.wait_for_element_clickable(MainPageLocators.MAIN_MENU_BUTTON)
            self.driver.find_element_then_click(MainPageLocators.MAIN_MENU_BUTTON)

        except NoSuchElementException as e:
            e_msg = "Unable to locate main menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,     # {0}
                e.message                       # {1}
            )
            raise NoSuchAttributeException(e_msg)

        else:
            # for animation to finish
            time.sleep(2)
