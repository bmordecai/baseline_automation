__author__ = 'Tige'

from old_32_10_sb_objects_dec_29_2017.common.imports import *
from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.locators import *
# from old_32_10_sb_objects_dec_29_2017.common import page_factory
# from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import WebDriver
import old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver as driver
import old_32_10_sb_objects_dec_29_2017.page_factory
import time
from selenium.webdriver import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoSuchAttributeException
from selenium.common.exceptions import TimeoutException
from old_32_10_sb_objects_dec_29_2017.common.variables import browser


class BasePage(object):

    def __init__(self, web_driver):
        """
        Init base page instance
        :param web_driver:  WebDriver instance \n
        :type web_driver:   driver.WebDriver
        """
        self.driver = web_driver

    #################################
    def format_locator_single_key(self, locator, key, value):
        """
        Formats the locator id string replacing the "key" with the "value".
        
        :param locator: Locator tuple (i.e., (By.ID, "someIdString_{key}).
        :type locator: tuple
        :param key: The string in the locator tuple to replace.
        :type key: str
        :param value: The value to replace the key with.
        :type value: str | int
        
        :returns: Updated tuple at index 1 (the locator string (jQuery selector).
        :rtype: tuple
        """
        # (By.ID, "someIdString_{key}") => [By.ID, "someIdString_{key}"]
        tuple_as_list = list(locator)
        # "key" => "{key}"
        key_as_find_replace_string = "{" + key + "}"
        # [By.ID, "someIdString_{key}"] => [By.ID, "someIdString_value"]
        tuple_as_list[1] = str(tuple_as_list[1]).replace(key_as_find_replace_string, str(value))
        # [By.ID, "someIdString_value"] => (By.ID, "someIdString_value")
        return tuple(tuple_as_list)

    #################################
    def format_locator_multi_key(self, locator, keyvals):
        """
        Formats the locator id string replacing the "key" with the "value".
        
        :param locator: Locator tuple (i.e., (By.ID, "someIdString_{key}).
        :type locator: tuple
        :param keyvals: List of key/value lists to replace in the locator.
        :type keyvals: list(list)
        
        :returns: Updated tuple at index 1 (the locator string (jQuery selector).
        :rtype: tuple
        """
        # (By.ID, "someIdString_{key}") => [By.ID, "someIdString_{key1}_{key2}"]
        tuple_as_list = list(locator)
        
        # iteration 1:
        #   [By.ID, "someIdString_{key1}_{key2}"] => [By.ID, "someIdString_value1_{key2}"]
        # iteration 2:
        #   [By.ID, "someIdString_value1_{key2}"] => [By.ID, "someIdString_value1_value2"]
        for keyval in keyvals:
            key = keyval[0]
            value = keyval[1]
            key_as_find_replace_string = "{" + key + "}"
            tuple_as_list[1] = str(tuple_as_list[1]).replace(key_as_find_replace_string, str(value))
            
        # [By.ID, "someIdString_value1_value2"] => (By.ID, "someIdString_value1_value2")
        return tuple(tuple_as_list)
        

class MainPage(BasePage):

    def open_info_dialog_for_cn(self):

        # get the controller serial number
        try:
            self.driver.wait_for_element_clickable(MainPageLocators.CONTROLLER_SETTINGS)
            element_to_hover = self.driver.web_driver.find_element(*MainPageLocators.CONTROLLER_SETTINGS)
            hover = ActionChains(self.driver.web_driver).move_to_element(element_to_hover)
            hover.click()
            hover.perform()
        except Exception:
            e_msg = "unable to init_driver the controller setting dialog box"
            raise Exception(e_msg)
        else:
            print("Successfully opened controller settings dialog box")

    def verify_open(self):
        # TODO move this to the baseclass
        # Wait for the page to successfully load displaying "Connected" status in the upper right.
        self.driver.wait_for_element_visible(MainPageLocators.MAIN_MENU_BUTTON)
        time.sleep(3)

    def verify_closed(self):
        # TODO move this to the baseclass
        # Wait for the page to successfully load displaying "Connected" status in the upper right.
        self.driver.wait_for_element_visible(MainPageLocators.MAIN_MENU_BUTTON)
        time.sleep(3)

    def select_maps_tab(self, map_view):
        """
        Selects the maps tab and then selects the specified map view. \n

        Usage:
            .select_maps_tab(map_view="company")

        :param map_view: Map view to select when selecting the maps tab \n
        :type map_view: str \n
        """
        map_view_dict = {
            "company": MainPageLocators.COMPANY,
            "site": MainPageLocators.CURRENT_SITE,
            "controller": MainPageLocators.CURRENT_CONTROLLER
        }
        if map_view not in map_view_dict:
            raise KeyError("Invalid map view for selecting maps tab.")
        else:
            locator = map_view_dict[map_view.lower()]

        # Try and locate the indicated menu item. If not found, handle exception raised by web driver
        try:
            # Select Maps Tab
            self.driver.find_element_then_click(MainPageLocators.MAPS_TAB)
            time.sleep(0.5)
            # Select Maps Sub Menu
            self.driver.find_element_then_click(_locator=locator)

        # Web driver unable to locate menu item indicated, handle exception raised.
        except Exception as e:
            caught_exception_text = e.__str__()
            e_msg = "Exception occurred trying to select Maps Tab with map view: '{0}'. Exception Received: {1}".format(
                map_view,
                caught_exception_text
            )
            raise KeyError(e_msg)
        else:
            print "Successfully Selected Maps Tab > {0}".format(
                map_view
            )

    def select_quick_view_tab(self):
        """
        Selects the QuickView tab. \n
        """
        # Try and locate the indicated menu item. If not found, handle exception raised by web driver
        try:
            # Select Maps Tab
            self.driver.find_element_then_click(MainPageLocators.QUICK_VIEW_TAB)
            time.sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except Exception as e:
            caught_exception_text = e.__str__()
            e_msg = "Exception occurred trying to select QuickView Tab. Exception Received: {0}".format(
                caught_exception_text
            )
            raise KeyError(e_msg)
        else:
            print "Successfully Selected QuickView Tab"

    def select_programs_tab(self):
        """
        Selects the programs tab. \n
        """
        try:
            self.driver.find_element_then_click(MainPageLocators.PROGRAMS_TAB)
            time.sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except Exception as e:
            caught_exception_text = e.__str__()
            e_msg = "Exception occurred trying to select Programs Tab. Exception Received: {0}".format(
                caught_exception_text
            )
            raise KeyError(e_msg)
        else:
            print "Successfully Selected Programs Tab"

    def select_devices_tab(self, device_type):
        """
        Selects the devices tab and then selects the sub menu based on the device type passed in. \n

        Usage:
            .select_devices_tab(device_type="ZN")

        :param device_type: Device type to select, "ZN", "MS", "FM", "TS", "MV", "SW" \n
        :type device_type: str \n
        """
        device_type_to_locator_dict = {
            "ZN": MainPageLocators.ZONES,
            "MS": MainPageLocators.MOISTURE_SENSORS,
            "FM": MainPageLocators.FLOW,
            "TS": MainPageLocators.TEMPERATURE,
            "MV": MainPageLocators.MASTER_VALVES,
            "SW": MainPageLocators.EVENT_SWITCHES
        }

        if device_type not in device_type_to_locator_dict:
            e_msg = "Invalid device type entered for selecting devices tab, entered {0}. Valid types are: {1}".format(
                device_type,
                device_type_to_locator_dict.keys()
            )
            raise KeyError(e_msg)
        else:
            # Sub menu locator
            locator = device_type_to_locator_dict[device_type]

        try:
            # Select Devices Tab
            self.driver.find_element_then_click(MainPageLocators.DEVICES_TAB)

            time.sleep(0.5)

            # Select Devices Sub Menu
            self.driver.find_element_then_click(_locator=locator)

        except Exception as e:
            caught_exception_text = e.__str__()
            e_msg = "Exception occurred trying to select Devices Tab, Device: '{0}'. Exception Received: {1}".format(
                device_type,
                caught_exception_text
            )
            raise Exception(e_msg)
        else:
            print "Successfully Selected Devices Tab > {0}".format(
                device_type
            )

    def select_water_sources_tab(self, water_source_type=None):
        """
        Selects the water sources tab. If a water source type is specified, attempt to select the specified sub menu.\n

        Usage:
            - 1000 Controller:
                1. foo.select_water_sources_tab()
                    - No water source type is needed

            - 3200 Controller:
                1. foo.select_water_sources_tab(water_source_type="PC")
                    - Water source type is needed for sub menu selection

        :param water_source_type: If a 3200 controller, specify either 'PC'=Point Of Connection or 'ML'=Mainline
        :type water_source_type: str \n
        """
        water_source_type_to_locator_dict = {
            "PC": MainPageLocators.POINT_OF_CONNECTION,
            "ML": MainPageLocators.MAINLINES
        }
        try:
            # Select water sources tab
            self.driver.find_element_then_click(MainPageLocators.WATER_SOURCES_TAB)

            time.sleep(0.5)

            # Did user specify a water source type? ("PC" | "ML)
            if water_source_type is not None:
                locator = water_source_type_to_locator_dict[water_source_type]
                self.driver.find_element_then_click(_locator=locator)

        except Exception as e:
            caught_exception_text = e.__str__()
            e_msg = "Exception occurred trying to select Water Sources Tab. Exception Received: {0}".format(
                caught_exception_text
            )
            raise Exception(e_msg)
        else:
            print "Successfully Selected Water Sources Tab > {0}".format(
                water_source_type
            )

    def select_live_view_tab(self):
        """
        Selects the LiveView tab. \n
        """
        try:
            self.driver.find_element_then_click(MainPageLocators.LIVEVIEW_TAB)
            time.sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except Exception as e:
            caught_exception_text = e.__str__()
            e_msg = "Exception occurred trying to select LiveView Tab. Exception Received: {0}".format(
                caught_exception_text
            )
            raise KeyError(e_msg)
        else:
            print "Successfully Selected LiveView Tab"

    def select_a_menu_tab(self, _menu_name_locator, _sub_menu_locator=None):
        """
        pass in the value for the main tab menus by name if the tab has sub menus pass in the name of the sub menu
        when a sub menu is used the hover over command drops the menu down so that the sub menu can be selected
        the hover command must execute before the the sub menu can be selected
        if an incorrect  value is passed in a value error is raised and the program stops
        :param, menu_name:, quick view, programs, liveview, devices, watersource
        :param, sub_menu:, zones, moisture, flow, master valve, temperature, events switches, point of connections,
        mainlines
        :rtype: str
        """
        if _menu_name_locator not in [MainPageLocators.MAPS_TAB,
                                      MainPageLocators.QUICK_VIEW_TAB,
                                      MainPageLocators.PROGRAMS_TAB,
                                      MainPageLocators.DEVICES_TAB,
                                      MainPageLocators.WATER_SOURCES_TAB,
                                      MainPageLocators.LIVEVIEW_TAB]:
            raise ValueError("Menu Name is incorrect:"+' ' + str(_menu_name_locator))
        
        # Try and locate the indicated menu item. If not found, handle exception raised by web driver
        try:
            # If the menu name is within the set below, the menu has sub-menus that we need to hover to access
            if _menu_name_locator in (MainPageLocators.MAPS_TAB,
                                      MainPageLocators.DEVICES_TAB,
                                      MainPageLocators.WATER_SOURCES_TAB):
                self.driver.find_element_then_click(_menu_name_locator)

                # Give animation time
                time.sleep(0.5)

                # select a sub Menu
                if _sub_menu_locator is not None:
                    self.select_a_sub_menu(sub_menu_locator=_sub_menu_locator)
            # A menu tab without a sub-list of menu items has been requested
            else:
                # Get the menu tab id from the dictionary
                self.driver.find_element_then_click(_menu_name_locator)
                # Give animation time
                time.sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except NoSuchElementException:
            raise ValueError("Unable to locate Main Menu: {0}, Sub Menu: {1}".format(
                _menu_name_locator,
                _sub_menu_locator
            ))
        else:
            print "Successfully Selected Main Menu > {0} > {1}".format(
                _menu_name_locator,
                _sub_menu_locator
            )

    def select_a_sub_menu(self, sub_menu_locator):
        """
        Selects a sub menu from a known list of available sub menu's. If a sub menu name isn't found,
        a ValueError is raised, signifying a misspelled sub menu name or an invalid sub menu selection.
        When the user specifies the 'markers' sub menu option, they must also specify which 'marker'
        sub menu they would like to access, i.e., 'marker_menu_options' shown below.
        :param sub_menu_locator:            Sub menu options available.
        :return:
        """
        # Iterate through supported sub menu options, if found a match, get the respective web_id associated
        if sub_menu_locator not in [MainPageLocators.COMPANY,
                                    MainPageLocators.CURRENT_SITE,
                                    MainPageLocators.CURRENT_CONTROLLER,
                                    MainPageLocators.ZONES,
                                    MainPageLocators.MOISTURE_SENSORS,
                                    MainPageLocators.FLOW,
                                    MainPageLocators.MASTER_VALVES,
                                    MainPageLocators.TEMPERATURE,
                                    MainPageLocators.EVENT_SWITCHES,
                                    MainPageLocators.POINT_OF_CONNECTION,
                                    MainPageLocators.MAINLINES]:
            raise ValueError("Bad Sub Menu Name" + ' ' + sub_menu_locator)

        try:
            # Attempt to locate indicated sub menu, if found and 'click-able', click it.
            self.driver.find_element_then_click(sub_menu_locator)
            
            # give animation some time to finish
            time.sleep(0.5)
            
        # Handle exception when we are unable to locate sub menu item before clicking
        except StaleElementReferenceException:
            raise StaleElementReferenceException("Unable to locate sub menu item: " + str(sub_menu_locator))

    def select_main_menu(self):
        """
        Selects the main upper left menu.
        """
        try:
            # Look for the upper left main menu and click it
            self.driver.wait_for_element_clickable(MainPageLocators.MAIN_MENU_BUTTON)
            self.driver.find_element_then_click(MainPageLocators.MAIN_MENU_BUTTON)

        except NoSuchElementException as e:
            e_msg = "Unable to locate main menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,     # {0}
                e.message                       # {1}
            )
            raise NoSuchAttributeException(e_msg)

        else:
            # for animation to finish
            time.sleep(2)

    def wait_for_selected_controller_to_reconnect(self):
        """
        Attempts to wait for the controller to go offline and to come back online.
        :return:
        :rtype:
        """
        status = self.driver.get_status(MainPageLocators.FOOTER_CONTROLLER_STATUS)

        while status != "No Connection":
            time.sleep(2)
            status = self.driver.get_status(MainPageLocators.FOOTER_CONTROLLER_STATUS)

        while status != "Done":
            time.sleep(2)
            status = self.driver.get_status(MainPageLocators.FOOTER_CONTROLLER_STATUS)

        print "Selected controller has reconnected to BaseManager."

    def click_logout_button(self):
        """
        this is for the old version of the web site
        Clicks the logout button and wait's 5 seconds
        :return:
        :rtype:
        """
        # Finds and selects the login button in order to login
        self.driver.find_element_then_click(MainPageLocators.LOGOUT)

        # # Wait for page to fully load
        # time.sleep(5)

        # Wait for the login page to successfully load.
        self.driver.wait_for_element_visible(LoginLocators.SIGN_IN_BUTTON)


class MainPageMenu(BasePage):

    def select_site(self, site_name=None):
        """
        Helper method for selecting a site from upper left menu. Navigation is as follows:
        menu -> sites and controllers -> (Next available options are selectable sites)
        :param site_name: Site to be selected.
        :return:
        """
        locator = (By.ID, '')

        if site_name is None:
            site_name = self.driver.conf.site_name

        try:
            # Look for 'Sites and Controllers' sub-menu option and click it to display sites
            self.driver.wait_for_element_clickable(MainMenuLocators.SITES)
            self.driver.find_element_then_click(MainMenuLocators.SITES)

            # Look for indicated site, site_name
            site_list_index = 1
            site_selected = None

            # Loop through each site and compare site names, if equal click the site.
            while not site_selected:

                # Construct site 'id' for locating
                current_web_id = 'menu-sites-%i' % site_list_index

                # Wait for element to be present, and attempt to click
                locator = (By.ID, current_web_id)
                self.driver.wait_for_element_clickable(locator)

                # Get the site to compare the name against what we are looking for.
                site_selected = self.driver.web_driver.find_element(*locator)

                # correct site?
                if str(site_selected.text.lower()).lstrip() == site_name.lower():
                    # yes
                    site_selected.click()
                    time.sleep(1)
                else:
                    # Keep looping, site label text didn't match site_name
                    site_list_index += 1
                    # set to None to remain in the while loop.
                    site_selected = None

            print "Successfully Selected Site: {0}".format(str(site_name))

        except NoSuchElementException as e:
            e_msg = "Unable to locate site name element and click WebElement ID: {0}. {1}".format(
                locator,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def select_a_controller(self, mac_address):
        """
        pass in the controller mac address as a string the method waits for the controller status to change colors
        before selecting the ID
        :param mac_address:     Mac Address of controller to select. \n
        :type mac_address:      str \n
        :rtype:                 str \n
        """
        controller_web_id = 'menu-controllers-' + mac_address
        locator = (By.ID, controller_web_id)
        try:
            # Verify controller is online
            self.verify_controller_is_online(mac_address=mac_address)

            # UPDATE: Commented this out because it appears to be on a use case basis, we don't need to count all
            # controllers online every test unless that is what we are testing.
            # Count number of controller's online
            # self.driver.number_of_controllers_online = self.count_number_of_controllers_online()

            # Look for the last needed option, the controller being selected with the specified mac add
            self.driver.wait_for_element_clickable(locator)

            # Click on the controller
            self.driver.find_element_then_click(locator)

        except NoSuchElementException:
            raise NoSuchElementException("Unable to locate controller with mac_address: %s" % str(mac_address))
        else:
            print "Successfully Selected Controller [mac_address = {0}]".format(mac_address)
            # Wait for animations to finish
            time.sleep(5)

    def select_a_company(self, company=None):
        """
        Method assumes Super User is signing in to Base Manager. \n
        :param company:     A company to select. \n
        :type company:      str \n
        """
        locator = (By.ID, '')
        if company is None:
            company = self.driver.conf.company

        try:
            # Look for 'Companies' sub-menu option and click it to display sites
            self.driver.wait_for_element_clickable(MainMenuLocators.COMPANIES)
            self.driver.find_element_then_click(MainMenuLocators.COMPANIES)

            # Look for indicated company, company_name
            company_list_index = 0
            company_selected = None

            # Loop through each company and compare company names, if equal click the company.
            while not company_selected:

                # Construct company 'id' for locating
                current_web_id = 'menu-companies-{0}'.format(company_list_index)

                # Wait for element to be present, and attempt to click
                locator = (By.ID, current_web_id)
                self.driver.wait_for_element_clickable(locator)

                # Get the company to compare the name against what we are looking for.
                company_selected = self.driver.web_driver.find_element(*locator)

                # correct company?
                if str(company_selected.text).lstrip() == company:
                    # yes
                    company_selected.click()
                    time.sleep(1)
                else:
                    # Keep looping, company label text didn't match company_name
                    company_list_index += 1
                    # set to None to remain in the while loop.
                    company_selected = None

            print "Successfully Selected Company: {0}".format(str(company))

        except NoSuchElementException as e:
            e_msg = "Unable to locate Company name element and click WebElement ID: {0}. {1}".format(
                locator,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def select_administration(self):
        """
        Method assumes Super User is signing in to Base Manager. \n
        """

        try:
            # Look for 'Administration' in the sub-menu, and then click it
            self.driver.wait_for_element_clickable(MainMenuLocators.ADMINISTRATION)
            self.driver.find_element_then_click(MainMenuLocators.ADMINISTRATION)

            print "Successfully Selected Administration."

        except NoSuchElementException as e:
            e_msg = "Unable to locate the administration button and click WebElement: {0}. {1}".format(
                MainMenuLocators.ADMINISTRATION,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def select_subscriptions(self):
        """
        Method assumes Super User is signing in to Base Manager. \n
        """

        try:
            # Look for 'Administration' in the sub-menu, and then click it
            self.driver.wait_for_element_clickable(MainMenuLocators.SUBSCRIPTIONS)
            self.driver.find_element_then_click(MainMenuLocators.SUBSCRIPTIONS)

            print "Successfully Selected Manage Subscriptions."

        except NoSuchElementException as e:
            e_msg = "Unable to locate the subscriptions button and click WebElement ID: {0}. {1}".format(
                MainMenuLocators.SUBSCRIPTIONS,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def click_sign_out_button(self):
        """
        Clicks the Sign Out button and wait's 5 seconds
        :return:
        :rtype:
        """
        # Finds and selects the login button in order to login
        self.driver.find_element_then_click(MainMenuLocators.SIGN_OUT)

        # Wait for page to fully load
        time.sleep(5)

        # Wait for the login page to successfully load.
        self.driver.wait_for_element_visible(LoginLocators.SIGN_IN_BUTTON)

    def wait_for_main_menu_close(self):
        """
        Waits for the main menu to close (sliding out of view).
        """

        is_open = True
        count = 1
        while is_open:

            # returns true if the menu element has the "slidein" css class, this means that the menu is still showing.
            is_open = "slidein" in self.driver.web_driver.find_element(*MainPageLocators.MAIN_MENU)\
                .get_attribute("class")

            # 30 seconds elapsed?
            if count == 30 and is_open:
                e_msg = "Timed out waiting for main menu to close after 30 seconds. Still trying to download company " \
                        "data?"
                raise TimeoutException(e_msg)

            # < 30 seconds elapsed
            else:
                time.sleep(1)
                count += 1

        # tell us how we did
        print "Successfully waited for main menu to close."

        # Give client a change to catch up, wait for animation to finish
        time.sleep(1)

    def count_number_of_controllers_online(self):
        """
        First, get the list of controller elements
        Next, for each element check whether the controllers name is in black (online) text color or in gray (offline)
        Return number of controllers online
        :return:
        """
        controllers_online = 0
        controller_web_elements = self.driver.web_driver.find_elements_by_class_name('controller')

        for each_controller in controller_web_elements:
            self.driver.total_number_of_controllers += 1
            try:
                current_mac = each_controller.get_attribute('mac')
                self.verify_controller_is_online(mac_address=current_mac)
                controllers_online += 1
            except AssertionError:
                pass

        return controllers_online

    def verify_controller_is_online(self, mac_address):
        """
        Waits for the controller to come online. If the controller element label is Gray in color, the controller is
        offline, so keep checking the color of the controller element label until it is not Gray, meaning it's
        online.\n
        In these terms, the label is the visible name on the controller, and we are refering to the color of the
        text. \n
        :param mac_address:
        :return:
        """
        controller_web_id = 'menu-controllers-label-' + str(mac_address)
        locator = (By.ID, str(controller_web_id))

        is_offline = True
        count = 1
        while is_offline:

            # ONLY WANT TO CHECK CONTROLLER'S THAT ARE VISIBLE TO US
            if self.driver.is_visible(_locator=locator):
                # returns true if controller has the css class 'status-disconnected' otherwise, returns false
                is_offline = "status-disconnected" in self.driver.web_driver.find_element(*locator).get_attribute("class")

                if count == 30 and is_offline:
                    raise AssertionError("Waited for controller with mac_address: %s and timed out after 30 seconds."
                                         % str(mac_address))
                else:
                    time.sleep(1)
                    count += 1
            else:
                is_offline = False

        print "Controller: " + str(mac_address) + " is online."

    def get_color_of_element_in_hex(self, css_property, css_selector=None, web_id=None):
        """
        Finds an element by it's css_selector
        Strips string of non-integer values and assigns to r, g, b respectively
        Formats the values into a Hex value represented as a string
            Example color returned from browser:    "rgba(72, 72, 72, 1)"
            Example output converted by method:     "#484848" (Black)
        :param css_selector:
        :param css_property:
        :return:
        """
        if not self.driver.conf.unit_testing:
            pass
            # time.sleep(5)
        color = ''

        if web_id is not None:
            color = str(self.driver.web_driver.find_element_by_id(id_=web_id).value_of_css_property(css_property))

        if css_selector is not None:
            color = str(self.driver.web_driver.find_element_by_css_selector(css_selector).value_of_css_property(
                str(css_property)))

        # Waits up to 60 seconds for a pop-up dialogue box to display.
        count = 1
        if color == 'transparent':
            while color == 'transparent':
                # Added below so it doesn't print so many "Alert is present" messages to console
                if web_id is not None:
                    color = str(self.driver.web_driver.find_element_by_id(id_=web_id).value_of_css_property(
                        css_property))
                else:
                    color = str(self.driver.web_driver.find_element_by_css_selector(css_selector).value_of_css_property(
                        str(css_property)))
                if count % 100 == 0:
                    print "Waiting for status color to not be transparent"
                    count = 1
                else:
                    count += 1

        print (color)
        strip_leading_characters = color.split('(')[1:]
        strip_ending_characters = strip_leading_characters[0].split(')')[:1]
        color_numbers = strip_ending_characters[0].split(',')
        r = int(color_numbers[0])
        g = int(color_numbers[1])
        b = int(color_numbers[2])
        color_in_hex = '#%02x%02x%02x' % (r, g, b)

        try:
            color_temp_conversion = browser.dictionary_reverse_status_color_dictionary[color_in_hex]
            color_name_converted = browser.dictionary_for_status_colors[color_temp_conversion]
        except KeyError:
            raise KeyError("Found color with hex value: " + str(color_in_hex) + ", not recognized by test.")
        else:
            print "Color in hex = [%s], %s" % (color_in_hex, color_name_converted)
            return color_in_hex


class ControllerSettingsDialogBox(BasePage):

    def verify_info_dialog_opened_for_correct_cn(self, _cn_sn):
        """
        Verify that the controller setting dialog box opened and that the correct controller was selected \n
        wait for the serial number to visible on the page if it is not than one of two things happened\n
            - the controller has an incorrect serial number \n
            - the controller in off line\n
        :param _cn_sn:  controller serial number
        :type _cn_sn:   str \n
        """

        self.driver.wait_for_element_visible(ControllerSettingsLocators.SERIAL_NUMBER_TEXT_LOCATION)
        _received_text = self.driver.get_text_from_web_element(ControllerSettingsLocators.SERIAL_NUMBER_TEXT_LOCATION)
        if _cn_sn != _received_text.strip():
            e_msg = "Exception occurred trying to verify controller serial number: expected {0}, received: {1}".format(
                str(_cn_sn), _received_text)
            raise Exception(e_msg)
        else:
            print("Successfully verified controller Serial Number {0}".format(str(_cn_sn)))

    def input_database_id(self, _database_id):
        """
        enter a data base id into the input field
        :param _database_id: the id must be looked up in the database
        :type _database_id: str\n
        :return:
        :rtype:
        """
        self.driver.send_text_to_element(_locator=ControllerSettingsLocators.INPUT_BOX_FOR_DATABASE_ID,
                                         text_to_send=_database_id)
        # self.driver.conf.web_driver.find_element(ControllerSettingsLocators.INPUT_BOX_FOR_DATABASE_ID).send_keys(
        #     _database_id)
        print("Successfully entered database id {0}".format(str(_database_id)))

    def click_send_firmware_version_button(self):
        self.driver.find_element_then_click(ControllerSettingsLocators.SEND_FIRMWARE_VERSION_BUTTON)

    def click_close_button(self):
        self.driver.find_element_then_click(ControllerSettingsLocators.CLOSE_BUTTON)