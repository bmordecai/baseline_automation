from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200

__author__ = 'baseline'

from selenium.common.exceptions import *
from selenium.webdriver.support.ui import Select
import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page as main_page
from old_32_10_sb_objects_dec_29_2017.common.imports import *
from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.locators import *
from selenium.webdriver.common.by import By
from old_32_10_sb_objects_dec_29_2017.common.objects import object_bucket
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes


#################################
class WaterSourcesPage3200(main_page.BasePage):

    #################################
    def __init__(self, web_driver):
        # init super class
        main_page.BasePage.__init__(self, web_driver=web_driver)
        self.driver = web_driver

        # Set constants for inheriting classes
        self.page_title = ""
        self.page_header_text = ""
        self.object_type = ""
        
        # objects ref
        self.objects = dict()
        
        # locator attrs for shared code
        
        # list view
        self.list_view_description_locator = tuple()
        # detail/edit view
        self.edit_button_locator = tuple()
        self.save_button_locator = tuple()
        self.description_input_locator = tuple()

    #################################
    def select_edit_button(self):
        """
        Selects the edit button in POC/Mainline detail view.
        """
        locator = self.edit_button_locator
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
            self.driver.handle_dialog_box()
        else:
            e_msg = "Unable to locate '{type} Edit Button' for {page}. Wasn't visible.".format(
                type=self.object_type,
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def select_save_button(self):
        """
        Selects the save button for the POC/Mainline (in edit view)
        """
        locator = self.save_button_locator
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
            self.driver.handle_dialog_box()
        else:
            e_msg = "Unable to locate '{type} Save Button' for {page}. Wasn't visible.".format(
                type=self.object_type,
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def set_description(self, number, description):
        """
        Sets the description of the poc or mainline on BaseManager.
        :param number: Address of poc/mainline to set description for.
        :type number: int
        :param description: New description to set.
        :type description: str
        """
        locator = self.description_input_locator
    
        # update object description reference.
        self.objects[number].ds = description
    
        if self.driver.wait_for_element_visible(_locator=locator):
            self.driver.send_text_to_element(_locator=locator, text_to_send=description, clear_field=True)
        else:
            e_msg = "Unable to set description for '{type} {addr}' to '{desc}'".format(
                type=self.object_type,
                addr=number,
                desc=description
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def verify_description_updates(self, number):
        """
        Verifies the description of the mainline/poc at index number
        :param number: Address of mainline/poc in dictionary.
        :type number: int
        """
        if type(self) is PointOfConnectionPage3200:
            locator_key = opcodes.poc_number_locator_key
        else:
            locator_key = opcodes.mainline_number_locator_key
            
        # insert the header index and device identifier (address for zones, otherwise is serial)
        locator = self.format_locator_single_key(
            locator=self.list_view_description_locator,
            key=locator_key,
            value=number
        )
    
        # get description from object
        expected_description = self.objects[number].ds
    
        try:
            self.driver.wait_for_element_visible(_locator=locator)
            self.driver.wait_for_element_text_change(_locator=locator, _text=expected_description)
        except WebDriverException as e:
            e_msg = "Unable to verify new description for '{type} {address}': '{expected}'.\nException: {e}".format(
                type=self.object_type,
                address=number,
                expected=expected_description,
                e=e.msg
            )
            raise WebDriverException(e_msg)
        else:
            print "Successfully verified '{type} {address}s' description '{desc}' updated in list view".format(
                type=self.object_type,
                address=number,
                desc=expected_description
            )
        
    #################################
    def verify_is_open(self, _locator):
        """
        Verifies that the devices tab of the specified type is open by verifying the refresh button at the top of the
        screen
        """
        # try to get the page element
        try:
            water_source_page = self.driver.web_driver.find_element(*_locator)

        # exception occurred trying to locate the element
        except (NoSuchElementException, StaleElementReferenceException, WebDriverException) as error:
            caught_err_text = error.msg
            e_msg = "Exception occurred trying to verify Water Sources > {page_title} is open. Exception Received: " \
                    "{exception_text}".format(
                        page_title=self.page_title,
                        exception_text=caught_err_text
                    )
            raise WebDriverException(e_msg)

        # no exception locating element
        else:

            # check to see if it is displayed
            page_is_displayed = water_source_page.is_displayed()
            if page_is_displayed:
                print "Successfully verified Water Sources > {page_title} is open".format(page_title=self.page_title)

            # not displayed
            else:
                e_msg = "Unable to verify Water Sources > {page_title} is open. Couldn't find element with id: " \
                        "{web_element}".format(
                            page_title=self.page_title,
                            web_element=_locator
                        )
                raise ElementNotVisibleException(e_msg)

    #################################
    def verify_header_text_is_displayed(self, _locator):
        """
        Verifies the header displayed for the respective water source view
        :param _locator: Selector object to use to find the header element
        :type _locator: (selenium.webdriver.common.by.By, str)
        """
        # try to get the page element
        try:
            header_el = self.driver.web_driver.find_element(*_locator)
        # exception occurred trying to locate the element
        except WebDriverException as error:
            caught_err_text = error.msg
            e_msg = "Exception occurred trying to find Water Sources > {page_title}'s header element. Exception " \
                    "Received: {exception_text}".format(
                        page_title=self.page_title,
                        exception_text=caught_err_text
                    )
            raise WebDriverException(e_msg)

        header_el_text = header_el.text
        
        if self.page_header_text != header_el_text:
            e_msg = "Unable to verify Water Sources > {0}'s header text. Received: {1}, Expected: {2}".format(
                self.page_title,
                header_el_text,
                self.page_header_text
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified Water Sources > {0}'s header text: {1}".format(
                self.page_title,
                self.page_header_text
            )

    #################################
    def get_element(self, _locator):
        """
        Returns the element specified by the _locator tuple.
        
        :param _locator: Selector object to use to find the header element. \n
        :type _locator: (selenium.webdriver.common.by.By, str)
        
        :rtype: selenium.webdriver.remote.webelement.WebElement
        """
        # try to get the page element
        try:
            return self.driver.web_driver.find_element(*_locator)
        # exception occurred trying to locate the element
        except WebDriverException as error:
            caught_err_text = error.msg
            e_msg = "Exception occurred trying to find an element located in {page_title} View. Exception " \
                    "Received: {exception_text}".format(
                        page_title=self.page_title,
                        exception_text=caught_err_text
                    )
            raise WebDriverException(e_msg)


#################################
class PointOfConnectionPage3200(WaterSourcesPage3200):
    """
    Point of Connection page object class for BaseManager and BaseStation 3200.
    """

    #################################
    def __init__(self, web_driver):
        # init super class
        WaterSourcesPage3200.__init__(self, web_driver=web_driver)

        # Set constants for POC tab
        self.page_title = "Points Of Connection"
        self.page_header_text = "POCs"
        self.object_type = "POC"
    
        # get reference to poc objects
        self.objects = object_bucket.poc32
        """:type: dict[int, old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200.POC3200]"""

        # reference to mainlines for setting/verifying
        self.mainlines = object_bucket.mainlines32
        """:type: dict[int, old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml.Mainline]"""

        # set locator attrs for shared code
        
        # list view
        self.list_view_description_locator = PocLocators3200.ListView.DESCRIPTION_COLUMN_VALUE
        # detail/edit view
        self.edit_button_locator = PocLocators3200.DetailView.EDIT_BUTTON
        self.save_button_locator = PocLocators3200.DetailView.SAVE_BUTTON
        self.description_input_locator = PocLocators3200.DetailView.DESCRIPTION_INPUT

    @staticmethod
    #################################
    def build_mainline_description_string(mainline):
        """
        Constructs the string used to display the assigned mainline for the program.
        :param mainline: Mainline object to construct string for.
        :type mainline: old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml.Mainline
        
        :rtype: str
        """
        displayed_ml_description = "{ml_number} - {ml_description}".format(
            ml_number=mainline.ad,
            ml_description=mainline.ds
        )
        return displayed_ml_description

    #################################
    def get_poc_obj(self, poc_number):
        """
        Get the point of connection object.
        :param poc_number: POC number used to address it in the dictionary.
        :type poc_number: int
        
        :rtype: old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200.POC3200
        """
        try:
            return self.objects[poc_number]
        except Exception as e:
            e_msg = "Unable to get POC: {number} from object bucket. Might not have been created yet.\nException: {e}".format(
                number=poc_number,
                e=e.message
            )
            raise Exception(e_msg)

    #################################
    def get_poc_mainline_obj(self, ml_number):
        """
        Get the assigned mainline object for a control point.
        :param ml_number: Assigned mainline number from the poc objects attribute "ML".
        :type ml_number: int
        
        :rtype: old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml.Mainline
        """
        try:
            return self.mainlines[ml_number]
        except Exception as e:
            e_msg = "Unable to get mainline: {number} from object bucket. Might not have been created yet.\nException: {e}".format(
                number=ml_number,
                e=e.message
            )
            raise Exception(e_msg)

    #################################
    def select_poc(self, poc_number):
        """
        Selects the POC row with respect to the POC number passed in.
        :param poc_number: POC number of POC to select. \n
        :type poc_number: int
        """
        poc_row_locator = (By.ID, "pocRow-{0}".format(poc_number))
        
        if self.driver.is_visible(_locator=poc_row_locator):
            self.driver.find_element_then_click(_locator=poc_row_locator)
        else:
            e_msg = "Unable to locate row for POC: {0} in {1} List View. Wasn't visible.".format(
                poc_number,
                self.page_title
            )
            raise ElementNotVisibleException(e_msg)
    
        self.driver.handle_dialog_box()

    #################################
    def verify_open(self):
        """
        Verifies that the Water Sources Tab opens successfully by verifying text at the top of the page. \n
        """
        WaterSourcesPage3200.verify_is_open(self, _locator=WaterSourcesLocators3200.POINT_OF_CONNECTION_VIEW)

    #################################
    def verify_header_text(self):
        """
        Verifies that the Points of Control view displays the correct header.
        """
        WaterSourcesPage3200.verify_header_text_is_displayed(self, _locator=PocLocators3200.ListView.HEADER)

    #################################
    def verify_list_view_assigned_mainline_description(self, poc_number):
        """
        Verifies the description column text displayed for the POC list table.
        :param poc_number: The address of the POC in the dictionary.
        :type poc_number: int
        """
        # get reference to object instance
        poc_obj = self.get_poc_obj(poc_number=poc_number)
        ml_obj = self.get_poc_mainline_obj(ml_number=poc_obj.ml)
        
        # build expected description string
        expected_description = PointOfConnectionPage3200.build_mainline_description_string(mainline=ml_obj)
        
        # locator to select the ML column in list view
        locator = self.format_locator_single_key(
            locator=PocLocators3200.ListView.MAINLINE_COLUMN_VALUE,
            key=opcodes.poc_number_locator_key,
            value=poc_number
        )
        
        # get displayed text
        column_el_text = self.driver.get_text_from_web_element(_locator=locator)
    
        # compare displayed against expected
        if column_el_text != expected_description:
            e_msg = "Unable to verify {0}'s List View Mainline Description Column Text. Received: {1}, " \
                    "Expected: {2}.".format(
                        self.page_title,
                        column_el_text,
                        expected_description
                    )
            raise ValueError(e_msg)
        else:
            print "Successfully verified {0}'s List View POC {1}'s Assigned Mainline's Description Column Text: {2}".format(
                self.page_title,
                poc_number,
                expected_description
            )

    #################################
    def verify_list_view_table_id_column_displayed(self):
        """
        Verifies the ID column text displayed for the POC list table.
        """
        column_el = self.get_element(_locator=PocLocators3200.ListView.ID_COLUMN_HEADER)
        column_el_text = column_el.text
        expected_el_text = 'ID'
        
        if column_el_text != expected_el_text:
            e_msg = "Unable to verify {0}'s List View ID Column Text. Received: {1}, " \
                    "Expected: {2}".format(
                        self.page_title,
                        column_el_text,
                        expected_el_text
                    )
            raise ValueError(e_msg)
        else:
            print "Successfully verified {0}'s List View ID Column Text: {1}".format(
                self.page_title,
                expected_el_text
            )

    #################################
    def verify_list_view_table_description_column_displayed(self):
        """
        Verifies the description column text displayed for the POC list table.
        """
        column_el = self.get_element(_locator=PocLocators3200.ListView.DESCRIPTION_COLUMN_HEADER)
        column_el_text = column_el.text
        expected_el_text = 'Description'
    
        if column_el_text != expected_el_text:
            e_msg = "Unable to verify {0}'s List View Description Column Text. Received: {1}, " \
                    "Expected: {2}".format(
                        self.page_title,
                        column_el_text,
                        expected_el_text
                    )
            raise ValueError(e_msg)
        else:
            print "Successfully verified {0}'s List View Description Column Text: {1}".format(
                self.page_title,
                expected_el_text
            )

    #################################
    def verify_list_view_table_mainline_column_displayed(self):
        """
        Verifies the description column text displayed for the POC list table.
        """
        column_el = self.get_element(_locator=PocLocators3200.ListView.MAINLINE_COLUMN_HEADER)
        column_el_text = column_el.text
        expected_el_text = 'Mainline'
    
        if column_el_text != expected_el_text:
            e_msg = "Unable to verify {0}'s List View Mainline Column Text. Received: {1}, " \
                    "Expected: {2}".format(
                        self.page_title,
                        column_el_text,
                        expected_el_text
                    )
            raise ValueError(e_msg)
        else:
            print "Successfully verified {0}'s List View Mainline Column Text: {1}".format(
                self.page_title,
                expected_el_text
            )

    #################################
    def verify_detail_view_displayed(self, poc_number):
        """
        :return:
        :rtype:
        """
        detail_view = self.driver.wait_for_element_visible(_locator=PocLocators3200.DetailView.MAIN_VIEW)
        poc_id_displayed = self.get_element(_locator=PocLocators3200.DetailView.POC_ID)
        
        poc_id_displayed_text = poc_id_displayed.get_attribute("value")
        
        if detail_view.is_displayed() and str(poc_number) == poc_id_displayed_text:
            print "Successfully verified {0} {1}'s Detail View is Displayed.".format(
                self.page_title,
                poc_number
            )
        else:
            e_msg = "Unable to verify {0} {1}'s Detail View is Displayed.".format(
                self.page_title,
                poc_number
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def verify_share_this_poc_option_is_hidden_jira_bm_1925(self):
        """
        Added for regression testing against a v12 POC detail view bug where "Share this POC" option was
        being displayed under the POC Priority field. It shouldn't be visible.
        """
        what_is_being_verified = "Share this POC"
        jira_bug_ref = "BM-1925 (https://baseline.atlassian.net/browse/BM-1925)"
    
        if not self.driver.is_visible(_locator=PocLocators3200.DetailView.SHARE_THIS_POC):
            print "{0} Detail View: Successfully verified '{1}' is not visible.\n->\tThis verifies " \
                  "JIRA Bug: {2}".format(
                      self.page_title,
                      what_is_being_verified,
                      jira_bug_ref
                  )
        else:
            e_msg = "\n{0} Detail View: Unable to verify '{1}' is not visible.\n->\tIt should not be visible " \
                    "for v12 3200 on BaseManager.\n->\tSee JIRA Bug: {2}\n".format(
                        self.page_title,
                        what_is_being_verified,
                        jira_bug_ref
                    )
            raise AssertionError(e_msg)

    #################################
    def verify_flowstation_option_visible_in_mainline_assignment_jira_bm_1932(self):
        """
        Added for regression testing against a v12 POC detail view bug where "FlowStation" option wasn't available in
        the Mainline assignment dropdown.
        """
        what_is_being_verified = "FlowStation Option Available as Mainline Assignment"
        jira_bug_ref = "BM-1932 (https://baseline.atlassian.net/browse/BM-1932)"
        
        select_element = self.get_element(_locator=PocLocators3200.DetailView.MAINLINE_ASSIGNMENT)
        select_el_text = select_element.text
        expected_text = "FlowStation"

        if expected_text not in select_el_text:
            e_msg = "\n{0} Detail View: Unable to verify '{1}'.\n->\tExpected: {2}, Received: {3} " \
                    "for v12 3200 on BaseManager.\n->\tSee JIRA Bug: {4}\n".format(
                        self.page_title,
                        what_is_being_verified,
                        expected_text,
                        select_el_text,
                        jira_bug_ref
                    )
            raise AssertionError(e_msg)
        else:
            print "{0} Detail View: Successfully verified '{1}' is displayed.\n->\tThis verifies " \
                  "JIRA Bug: {2}".format(
                      self.page_title,
                      what_is_being_verified,
                      jira_bug_ref
                  )

    #################################
    def verify_flowstation_mainline_assignment_is_visible_in_list_view_jira_bm_1932(self, poc_number):
        """
        Added for regression testing against a v12 POC detail view bug where "FlowStation" option wasn't available in
        the Mainline assignment drop-down. This test verifies the display of "FlowStation" assignment in list view
        """
        what_is_being_verified = "FlowStation as Mainline Assignment"
        jira_bug_ref = "BM-1932 (https://baseline.atlassian.net/browse/BM-1932)"
        
        locator = (By.CSS_SELECTOR, "#pocRow-{0}-mainline".format(poc_number))
        
        flowstation_text_in_list_view = self.get_element(_locator=locator).text
        expected_text = "FlowStation"
    
        if flowstation_text_in_list_view != expected_text:
            e_msg = "\n{0} List View: Unable to verify '{1}'.\n->\tExpected: {2}, Received: {3} " \
                    "for v12 3200 on BaseManager.\n->\tSee JIRA Bug: {4}\n".format(
                        self.page_title,
                        what_is_being_verified,
                        expected_text,
                        flowstation_text_in_list_view,
                        jira_bug_ref
                    )
            raise AssertionError(e_msg)
        else:
            print "{0} List View: Successfully verified '{1}' is displayed.\n->\tThis verifies " \
                  "JIRA Bug: {2}".format(
                      self.page_title,
                      what_is_being_verified,
                      jira_bug_ref
                  )

    #################################
    def verify_flowstation_mainline_assignment_is_visible_in_detail_view_jira_bm_1932(self):
        """
        Added for regression testing against a v12 POC detail view bug where "FlowStation" option wasn't available in
        the Mainline assignment drop-down. This test verifies the display of "FlowStation" assignment in detail view
        """
        what_is_being_verified = "FlowStation as Mainline Assignment"
        jira_bug_ref = "BM-1932 (https://baseline.atlassian.net/browse/BM-1932)"

        select_element = Select(self.get_element(_locator=PocLocators3200.DetailView.MAINLINE_ASSIGNMENT))
        flowstation_text_in_detail_view = select_element.first_selected_option.text
        expected_text = "FlowStation"
    
        if flowstation_text_in_detail_view != expected_text:
            e_msg = "\n{0} Detail View: Unable to verify '{1}'.\n->\tExpected: {2}, Received: {3} " \
                    "for v12 3200 on BaseManager.\n->\tSee JIRA Bug: {4}\n".format(
                        self.page_title,
                        what_is_being_verified,
                        expected_text,
                        flowstation_text_in_detail_view,
                        jira_bug_ref
                    )
            raise AssertionError(e_msg)
        else:
            print "{0} Detail View: Successfully verified '{1}' is displayed.\n->\tThis verifies " \
                  "JIRA Bug: {2}".format(
                      self.page_title,
                      what_is_being_verified,
                      jira_bug_ref
                  )

    #################################
    def verify_master_valve_enable_is_hidden_jira_bm_1926(self):
        """
        Added for regression testing against a v12 POC detail view bug where Master Valve "Enable" flag was being
        displayed for v12 3200. This is only available for v16 3200. This verifies it was hidden and fixed.
        """
        what_is_being_verified = "Master Valve Enable"
        jira_bug_ref = "BM-1926 (https://baseline.atlassian.net/browse/BM-1926)"
    
        if not self.driver.is_visible(_locator=PocLocators3200.DetailView.MV_ENABLE):
            print "{0} Detail View: Successfully verified '{1}' is not visible.\n->\tThis verifies " \
                  "JIRA Bug: {2}".format(
                      self.page_title,
                      what_is_being_verified,
                      jira_bug_ref
                  )
        else:
            e_msg = "\n{0} Detail View: Unable to verify '{1}' is not visible.\n->\tIt should not be visible " \
                    "for v12 3200 on BaseManager.\n->\tSee JIRA Bug: {2}\n".format(
                        self.page_title,
                        what_is_being_verified,
                        jira_bug_ref
                    )
            raise AssertionError(e_msg)

    #################################
    def verify_switch_empty_condition_components_are_visible_jira_bm_1927(self):
        """
        Added for regression testing against a v12 POC detail view bug where switch empty conditions weren't having
        their limit/wait time displayed. This verifies fix.
        """
        what_is_being_verified = "Event Switch Empty Condition Limit"
        jira_bug_ref = "BM-1927 (https://baseline.atlassian.net/browse/BM-1927)"
        
        # .getFirstSelectedOption()
        # .getText()
    
        # First verify the empty condition limit is displayed
        if self.driver.is_visible(_locator=PocLocators3200.DetailView.SW_EMPTY_LIMIT_ELEMENT):
            print "{0} Detail View: Successfully verified '{1}' is visible.\n->\tThis verifies " \
                  "JIRA Bug: {2}".format(
                      self.page_title,
                      what_is_being_verified,
                      jira_bug_ref
                  )
        else:
            e_msg = "\n{0} Detail View: Unable to verify '{1}' is visible.\n->\tIt should be visible " \
                    "for v12 3200 on BaseManager.\n->\tSee JIRA Bug: {2}\n".format(
                        self.page_title,
                        what_is_being_verified,
                        jira_bug_ref
                    )
            raise AssertionError(e_msg)

        # Now verify the empty wait time for switch empty conditions are being displayed.
        what_is_being_verified = "Event Switch Empty Condition Wait Time"
        if self.driver.is_visible(_locator=PocLocators3200.DetailView.SW_EMPTY_TIME_ELEMENT):
            print "{0} Detail View: Successfully verified '{1}' is visible.\n->\tThis verifies " \
                  "JIRA Bug: {2}".format(
                       self.page_title,
                       what_is_being_verified,
                       jira_bug_ref
                  )
        else:
            e_msg = "\n{0} Detail View: Unable to verify '{1}' is visible.\n->\tIt should be visible " \
                    "for v12 3200 on BaseManager.\n->\tSee JIRA Bug: {2}\n".format(
                        self.page_title,
                        what_is_being_verified,
                        jira_bug_ref
                    )
            raise AssertionError(e_msg)


#################################
class MainlinesPage3200(WaterSourcesPage3200):
    """
    Mainline page object class for BaseManager and BaseStation 3200.
    """

    #################################
    def __init__(self, web_driver):
        # init super class
        WaterSourcesPage3200.__init__(self, web_driver=web_driver)
    
        # Set constants for ML tab
        self.page_title = "Mainlines"
        self.page_header_text = "Mainlines"
        self.object_type = "ML"
    
        # get reference to ML objects
        self.objects = object_bucket.mainlines32
        """:type: dict[int, old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml.Mainline]"""

        # set locator attrs for shared code
        
        # list view
        self.list_view_description_locator = MainlineLocators3200.ListView.DESCRIPTION_COLUMN_VALUE
        # detail/edit view
        self.edit_button_locator = MainlineLocators3200.DetailView.EDIT_BUTTON
        self.save_button_locator = MainlineLocators3200.DetailView.SAVE_BUTTON
        self.description_input_locator = MainlineLocators3200.DetailView.DESCRIPTION_INPUT

    #################################
    def select_mainline(self, ml_number):
        """
        Selects the ML row with respect to the ML number passed in.
        :param ml_number: ML number of ML to select. \n
        :type ml_number: int
        """
        locator = self.format_locator_single_key(
            locator=MainlineLocators3200.ListView.MAINLINE_ROW,
            key=opcodes.mainline_number_locator_key,
            value=ml_number
        )
    
        if self.driver.wait_for_element_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
        else:
            e_msg = "Unable to locate row for 'ML {0}' in {1} List View. Wasn't visible.".format(
                ml_number,
                self.page_title
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def verify_detail_view_displayed(self, ml_number):
        """
        Verifies the correct ML is being displayed in the detail view.
        :param ml_number: ML number of ML to select. \n
        :type ml_number: int
        """
        detail_view = self.driver.wait_for_element_visible(_locator=MainlineLocators3200.DetailView.MAIN_VIEW)
        ml_id_displayed = self.get_element(_locator=MainlineLocators3200.DetailView.MAINLINE_ID)
    
        ml_id_displayed_text = ml_id_displayed.get_attribute("value")
    
        if detail_view.is_displayed() and str(ml_number) == ml_id_displayed_text:
            print "Successfully verified {0} {1}'s Detail View is Displayed.".format(
                self.page_title,
                ml_number
            )
        else:
            e_msg = "Unable to verify {0} {1}'s Detail View is Displayed.".format(
                self.page_title,
                ml_number
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def verify_open(self):
        """
        Verifies that the Water Sources Tab opens successfully by verifying text at the top of the page. \n
        """
        WaterSourcesPage3200.verify_is_open(self, _locator=WaterSourcesLocators3200.MAINLINES_VIEW)
