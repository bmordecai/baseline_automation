from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

__author__ = 'baseline'

from selenium.common.exceptions import *
from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.locators import *
import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page as main_page
from old_32_10_sb_objects_dec_29_2017.common.objects import object_bucket


class ProgramsTab(main_page.BasePage):
    
    def __init__(self, web_driver):
        # init super class
        main_page.BasePage.__init__(self, web_driver=web_driver)
        self.driver = web_driver
        
        self.page_title = "Programs Tab"
        
        # give programs tab a reference to available programs.
        self.objects = object_bucket.programs32
        """:type: dict[int, old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200.PG3200]"""
        
        # reference to mainlines for setting/verifying
        self.mainlines = object_bucket.mainlines32
        """:type: dict[int, old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml.Mainline]"""

    #################################
    def get_program_obj(self, pg_number):
        """
        Get the program object.
        :param pg_number: Program number used to address it in the dictionary.
        :type pg_number: int
        
        :rtype: old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200.PG3200
        """
        try:
            return self.objects[pg_number]
        except Exception as e:
            e_msg = "Unable to get program: {number} from object bucket. Might not have been created yet.\nException: {e}".format(
                number=pg_number,
                e=e.message
            )
            raise Exception(e_msg)

    #################################
    def get_program_mainline_obj(self, ml_number):
        """
        Get the assigned mainline object for a program.
        :param ml_number: Assigned mainline number from the program objects attribute "ML".
        :type ml_number: int
        
        :rtype: old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml.Mainline
        """
        try:
            return self.mainlines[ml_number]
        except Exception as e:
            e_msg = "Unable to get mainline: {number} from object bucket. Might not have been created yet.\nException: {e}".format(
                number=ml_number,
                e=e.message
            )
            raise Exception(e_msg)

    @staticmethod
    #################################
    def build_mainline_description_string(mainline):
        """
        Constructs the string used to display the assigned mainline for the program.
        :param mainline: Mainline object to construct string for.
        :type mainline: old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml.Mainline
        
        :rtype: str
        """
        displayed_ml_description = "{ml_number} - {ml_description}".format(
            ml_number=mainline.ad,
            ml_description=mainline.ds
        )
        return displayed_ml_description

    #################################
    def verify_open(self):
        """
        Verifies that the Programs Tab opens successfully by looking for the program search field. \n
        """
        try:
            self.driver.wait_for_element_clickable(_locator=ProgramsTabLocators.ListView.SEARCH_FIELD)
        except Exception as ve:
            caught_text = ve.message
            e_msg = "Unable to verify Programs tab is open. Exception received: {0}".format(
                caught_text
            )
            raise Exception(e_msg)
        else:
            print "Successfully verified Programs Tab opened"

    #################################
    def select_program(self, pg_number):
        """
        Selects the specified program.
        :param pg_number: Program number to select in program view.
        :type pg_number: int
        """
        locator = self.format_locator_single_key(
            locator=ProgramsTabLocators.ListView.PROGRAM_ROW,
            key=opcodes.program_locator_key,
            value=pg_number
        )
        if self.driver.wait_for_element_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
            self.driver.handle_dialog_box()
        else:
            e_msg = "{title}: Unable to locate and select Program {number}.".format(
                title=self.page_title,
                number=pg_number
            )
            raise WebDriverException(e_msg)

    #################################
    def select_add_program(self):
        """
        Selects the "Add Program" row on BaseManager in Programs Tab.
        """
        if self.driver.is_visible(_locator=ProgramsTabLocators.ListView.ADD_PROGRAM_ROW):
            self.driver.find_element_then_click(_locator=ProgramsTabLocators.ListView.ADD_PROGRAM_ROW)
            self.driver.handle_dialog_box()
        else:
            raise WebDriverException("Programs Tab: Unable to locate and click Add Program row. Wasn't visible.")

    #################################
    def select_add_zone(self):
        """
        Selects the "Add Zone" row on BaseManager in Programs Tab - Detail View.
        """
        if self.driver.is_visible(_locator=ProgramsTabLocators.DetailView.ZONE_ADD_ROW):
            self.driver.find_element_then_click(_locator=ProgramsTabLocators.DetailView.ZONE_ADD_ROW)
        else:
            raise WebDriverException("Programs Tab - Detail View: Unable to locate and click Add Zone row. "
                                     "Wasn't visible.")

    #################################
    def select_enable_et_for_1000_program(self):
        """
        Selects the enable checkbox for "Enable Weather Based Watering" program attribute for 1000 controllers on
        BaseManager in Programs Tab - Detail View.
        """
        if self.driver.is_clickable(_locator=ProgramsTabLocators.DetailView.ENABLE_ET_CHECKBOX):
            self.driver.find_element_then_click(_locator=ProgramsTabLocators.DetailView.ENABLE_ET_CHECKBOX)
        else:
            raise WebDriverException("Programs Tab - Detail View: Unable to locate and click 'Enable Weather Based "
                                     "Watering' program setting. Wasn't visible.")

    #################################
    def verify_program_add_or_detail_view_displayed(self):
        """
        Verifies the header displayed at the top of the program detail view. \n
        For new programs, "Add Program" is displayed, otherwise, the program's description is displayed. \n
        """
        if not self.driver.is_visible(_locator=ProgramsTabLocators.DetailView.VIEW):
            raise Exception("Unable to verify Programs tab - Add/Detail view is open.")
        else:
            print "Successfully verified Programs Tab - Add/Detail view opened."

    #################################
    def save_new_program(self):
        """
        Selects the save button and handles the TP pop-up that explains what data is being sent/received from
        the controller.
        """
        if self.driver.is_visible(_locator=ProgramsTabLocators.DetailView.SAVE_BUTTON):
            self.driver.find_element_then_click(_locator=ProgramsTabLocators.DetailView.SAVE_BUTTON)
        else:
            e_msg = "{0}: Unable to locate SAVE button. Not visible to user.".format(
                self.page_title
            )
            raise ElementNotVisibleException(e_msg)

        self.driver.handle_dialog_box()

    #################################
    def verify_newly_added_program(self, new_pg_number, new_pg_description):
        """
        Waits for and verifies the new program with address `new_pg_number` is added to the list of displayed programs
        and verifies the new program's description `new_pg_description` is displayed.

        :param new_pg_number: Program number of newly added program.
        :param new_pg_description: Description of newly added program.
        :return:
        """
        row_locator = self.format_locator_single_key(
            locator=ProgramsTabLocators.ListView.PROGRAM_ROW,
            key=opcodes.program_locator_key,
            value=new_pg_number
        )
        text_locator = (By.CSS_SELECTOR, "#programRow-{0} > td.desc".format(new_pg_number))
        try:
            self.driver.wait_for_element_visible(_locator=row_locator)
            self.driver.wait_for_element_text_change(_locator=text_locator, _text=new_pg_description)
        except Exception:
            e_msg = "{0}: Unable locate newly added program: {1}. Didn't appear in list after save".format(
                self.page_title,
                new_pg_description
            )
            raise Exception(e_msg)
        else:
            print "{0}: Successfully verified new program showed up in Program List view after save.".format(
                self.page_title
            )

    #################################
    def verify_assigned_mainline_in_detail_view(self, pg_number):
        """
        Verifies the displayed ML description for the program.
        :param pg_number: Program number to get the mainline assignment from.
        :type pg_number: int
        """
        # get objects needed
        pg_obj = self.get_program_obj(pg_number=pg_number)
        ml_obj = self.get_program_mainline_obj(ml_number=pg_obj.ml)
        
        # constructed expected description to verify
        expected_ml_description = ProgramsTab.build_mainline_description_string(mainline=ml_obj)

        # get displayed ml description
        locator = ProgramsTabLocators.DetailView.MAINLINE_DESCRIPTION
        displayed_ml_description = self.driver.get_text_from_web_element(_locator=locator)
        
        # verify
        if displayed_ml_description != expected_ml_description:
            e_msg = "Unable to verify Program {pg_number}'s Assigned Mainline Description. Received: {displayed}, " \
                    "Expected: {expected}.".format(
                        pg_number=pg_number,
                        displayed=displayed_ml_description,
                        expected=expected_ml_description
                    )
            raise ValueError(e_msg)
        else:
            print "Successfully verified Program {pg_number}'s assigned Mainline's Description: {ml_desc}".format(
                pg_number=pg_number,
                ml_desc=displayed_ml_description
            )
