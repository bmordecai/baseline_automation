__author__ = 'Tige'
# from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.locators import *
# import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page as main_page
# from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import *
import main_page
from locators import *
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes


class QuickViewTab(main_page.BasePage):
    
    #################################
    def verify_open(self):
        """
        Verifies that the QuickView Tab opens successfully by verifying text at the top of the page. \n
        """
        try:
            self.verify_zone_status_header()
        except ValueError as ve:
            caught_text = ve.message
            e_msg = "Unable to verify QuickView tab is open. Exception received: {0}".format(
                caught_text
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified QuickView Tab opened"

    #################################
    def verify_not_present(self):
        """
        Verifies that the QuickView Tab doesnt display on the UI. \n
        """
        # TODO need to Clean this up this is call the wrong thing need to catch a NoSuchElementfound
        try:
            self.driver.web_driver.find_element(MainPageLocators.QUICK_VIEW_TAB)
        except NoSuchElementException:
            print "Successfully verified quickview tab is not present."
        else:
            raise ValueError("QuickView tab was present when we expected it not to be.")

    #################################
    def verify_zone_status_header(self):
        """
        this looks at the text displayed above the zone
        :return:
        :rtype:     str\n
        """
        zone_status_header = self.driver.web_driver.find_element(*QuickViewLocators.ZONE_STATUS_HEADER).text
        expected = 'Zone Status'
        if zone_status_header != expected:
            e_msg = "zone_status_header'{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected),              # {0}
                str(zone_status_header),   # {1}
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified Zone Status Header on QuickView Tab"

    #################################
    def select_zone_status_box(self, address):
        """
        Selects a zone in QV to display its tooltip.
        :param address: Address of zone to select.
        :type address: int
        """
        locator = self.format_locator_single_key(
            locator=QuickViewLocators.ZN_STATUS_BOX,
            key="address",
            value=address
        )
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
        else:
            e_msg = "Unable to locate 'Zone {address} Status Box' in QuickView. Wasn't visible.".format(
                address=address
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def select_master_valve_status_box(self, address, serial):
        """
        Selects a zone in QV to display its tooltip.
        :param address: Address of MV to select.
        :type address: int
        :param serial: Serial of master valve.
        :type serial: str
        """
        locator = self.format_locator_multi_key(
            locator=QuickViewLocators.MV_STATUS_BOX,
            keyvals=[
                ["address", address],
                ["serial", serial]
            ]
        )
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
        else:
            e_msg = "Unable to locate 'MV {address} Status Box' in QuickView. Wasn't visible.".format(
                address=address
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def verify_description(self, dv_type, id, description):
        """
        Verifies the displayed description for the given device type, identifier.
        
        :param dv_type: Type of device to verify description for.
        :type dv_type: str
        :param id: Identifier for device (address or serial number depending on type)
        :type id: int | str
        :param description: Expected description to verify.
        :type description: str
        """
        locator = None
        expected_ds = None
        description_in_ui = ""
    
        # if we are a zone, use the address as "serial" for formatting the string, other devices use actual serial
        # numbers in the id's
        if dv_type == opcodes.zone or dv_type == opcodes.master_valve or dv_type == opcodes.program:
            locator = QuickViewLocators.TOOLTIP_DESCRIPTION
            # "Done" is appended onto the end of the expected text because when
            # selenium gets the text from the element, it gets the description text
            # as well as text from child elements (namely the status text). Since
            # we aren't running programming, the status should always be Done.
            expected_ds = "{id} - {description}Done".format(
                id=id,
                description=description
            )
            
        elif dv_type == opcodes.moisture_sensor:
            locator = self.format_locator_single_key(
                locator=QuickViewLocators.MS_DESCRIPTION,
                key="serial",
                value=id
            )
            expected_ds = description
            
        elif dv_type == opcodes.flow_meter:
            locator = self.format_locator_single_key(
                locator=QuickViewLocators.FM_DESCRIPTION,
                key="serial",
                value=id
            )
            expected_ds = description

        elif dv_type == opcodes.temperature_sensor:
            locator = self.format_locator_single_key(
                locator=QuickViewLocators.TS_DESCRIPTION,
                key="serial",
                value=id
            )
            expected_ds = description

        elif dv_type == opcodes.event_switch:
            locator = self.format_locator_single_key(
                locator=QuickViewLocators.SW_DESCRIPTION,
                key="serial",
                value=id
            )
            expected_ds = description

        else:
            locator = None
            expected_ds = None
            
        # fail-fast case
        if locator is None and expected_ds is None:
            e_msg = "Invalid device type to verify description in QuickView: {type}".format(type=dv_type)
            raise AssertionError(e_msg)
    
        # Get the text from the web element
        if self.driver.wait_for_element_visible(_locator=locator):
            description_in_ui = self.driver.get_text_from_web_element(_locator=locator)
        else:
            e_msg = "Unable to get description for {d_type} {addr}. Unable to locate element.".format(
                d_type=dv_type,
                addr=id
            )
            raise ElementNotVisibleException(e_msg)
    
        # Equal?
        if description_in_ui != expected_ds:
            e_msg = "Unable to verify description on BM for {d_type} {addr}. Expected: {exp_desc}, " \
                    "Received: {rec_desc}".format(
                        d_type=dv_type,
                        addr=id,
                        exp_desc=expected_ds,
                        rec_desc=description_in_ui
                    )
            raise ValueError(e_msg)
    
        # Yes equal
        else:
            print "Successfully verified {d_type} {addr} description of: {desc}".format(
                d_type=dv_type,
                addr=id,
                desc=description
            )
