__author__ = 'baseline'

import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page as main_page
from old_32_10_sb_objects_dec_29_2017.common.imports import *
from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.locators import *
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *


class WaterSourcesTab1000(main_page.BasePage):

    def verify_open(self):
        """
        Verifies that the Water Sources Tab opens successfully by verifying text at the top of the page. \n
        """
        try:
            locator = (By.ID, "poc_view")
            water_source_page = self.driver.web_driver.find_element(*locator)
            if not water_source_page.is_displayed():
                raise ElementNotVisibleException("Unable to locate Water Sources page.")
        except Exception as ve:
            caught_text = ve.message
            e_msg = "Unable to verify Water Sources tab is open. Exception received: {0}".format(
                caught_text
            )
            raise Exception(e_msg)
        else:
            print "Successfully verified Water Sources Tab opened"
