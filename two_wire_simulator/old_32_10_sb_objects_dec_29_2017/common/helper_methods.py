import time
import old_32_10_sb_objects_dec_29_2017.common.variables.common as pvar
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler
from datetime import timedelta, datetime, date
import time
import socket


def test_elapsed_time(_time_started, _time_expected_to_run):
    time_test_finished = datetime.now()
    elapsed_time = time_test_finished - _time_started
    if elapsed_time >= _time_expected_to_run:
        e_msg = "##############################################################################################\n" \
                "FAILURE: The test normally takes '{0}' time to run. This time the test took '{1}' to complete\n" \
                "#############################################################################################\n" \
            .format(
                _time_expected_to_run,
                elapsed_time
            )
        raise Exception, Exception(e_msg)
    else:
        e_msg = "#################################################################################################\n" \
                "SUCCESS: Test took less time to complete than normal. Usually it takes {0} this time it took {1} \n" \
                "#################################################################################################\n" \
            .format(
                _time_expected_to_run,
                elapsed_time
            )
        print e_msg
        
        
def test_suite_elapsed_time(_time_started, _time_expected_to_run):
    time_test_finished = datetime.now()
    elapsed_time = time_test_finished - _time_started
    if elapsed_time >= _time_expected_to_run:
        e_msg = "##############################################################################################\n" \
                "# WARNING: \n" \
                "# \n" \
                "# The test suite normally takes '{0}' time to run. This time the test suite took '{1}' to complete\n" \
                "#############################################################################################\n" \
            .format(
                _time_expected_to_run,
                elapsed_time
            )
        print e_msg
    else:
        e_msg = "##############################################################################################\n" \
                "# SUCCESS: \n" \
                "# \n" \
                "# The test suite took less time to complete than normal!\n" \
                "#  -> Last logged execution time:  '{0}'\n" \
                "#  -> Current execution time:      '{1}'\n" \
                "#############################################################################################\n" \
            .format(
                _time_expected_to_run,
                elapsed_time
            )
        print e_msg


def print_starting_test_suite_countdown(seconds):
    print("\nStarting in...")
    for count in reversed(range(1, seconds+1)):
        print count
        time.sleep(1)


# def string_convertes(self):
#     msg_tx_date = date.strftime(format='%m/%d/%Y')
#     msg_tx_time = time.strftime('%H:%M:%S')
#     standardcontroller_date = (('%m/%d/%Y')
#     controller_time = ('%H:%M:%S')
#     pass
def truncate_float_value(_float_value, number_of_decimal_places=2):
    """
    Formatter for float values. Takes an float value and does the following: \n
    1.  Parses the float value passed in at the decimal to verify the number of decimal places contained (requires 2,
        if less we append a number on to the end of decimal, if more than 2 are there, it is truncated and returned.) \n
    :param _float_value:    Float Value \n
    :type _float_value:     float \n

    :return: Returns the formatted float value with 2 decimal places as required by the controller. \n
    :rtype: float
    """
    # Check if _du_value is a valid float
    if not isinstance(_float_value, float):
        # e_msg = "Exception occurred value received {0} was and invalid " \
        #         "argument type, expected a float (0.00), received: {1}"\
        #         .format(_float_value, type(_float_value))
        # raise TypeError(e_msg)
        #
        # TODO: Tige Reference
        # INSTEAD
        #       What if we don't want to bomb out when an integer is passed into this method. The name of the method
        #       makes me assume that the value we input will output as a float, not bomb out, so a _float_value=1 would
        #       output 1.0.. if that's the case then we could do something like this:
        non_decimal_half = str(_float_value)
        decimal_half = "00"

    else:
        value_et_format = str(_float_value)

        # Gets the decimal half of the _float_value value passed in.
        # i.e: "3.45".split('.') returns a list ["3", "45"]
        non_decimal_half = value_et_format.split('.')[0]
        decimal_half = value_et_format.split('.')[1][:number_of_decimal_places]

        # Verify decimal place goes to at least 2 digit places
        if len(decimal_half) < 2:
            decimal_half += "0"

    return float(non_decimal_half + "." + decimal_half)


def format_lat_long(_lat=None, _long=None):
    """
    Formatter for latitude and longitude values. Takes either a latitude or a longitude value and does the following: \n
    1.  Parses the lat/long passed in at the decimal to verify the number of decimal places contained (requires 6,
        if less an exception is raised, if more than 6 are there, it is truncated and returned.) \n
    :param _lat:    Latitude for formatting \n
    :type _lat:     Float \n
    :param _long:   Longitude for formatting \n
    :type _long:    Float \n
    :return:        Returns the formatted lat/long value with 6 decimal places as required by the controller. \n
    """
    if _lat is None and _long is None:
        raise ValueError("No lat/long values specified for 'format_lat_long'.")
    elif _lat is not None and not isinstance(_lat, float):
        raise TypeError("'_lat' parameter for 'format_lat_long' must be of type float, entered: {0}".format(type(_lat)))
    elif _long is not None and not isinstance(_long, float):
        raise TypeError("'_long' parameter for 'format_lat_long' must be of type float, entered: {0}".format(
                        type(_long)))
    else:
        # If lat is not none, attempt to use _lat, otherwise attempt to use _long
        value_for_format = str(_lat) if _lat is not None else str(_long)

        # Gets the decimal half of the _lat / _long value passed in.
        # i.e: "123.45678910".split('.') returns a list ["123", "45678910"]
        non_decimal_half = value_for_format.split('.')[0]
        decimal_half = value_for_format.split('.')[1]

        # Verify decimal place goes to at least 6 digit places
        if len(decimal_half) < 6:
            raise ValueError("Invalid lat/long passed into 'format_lat_long'. Controller requires 6 decimal places.")

        # Assume lat/long passed in has at least 6 decimal places, so we truncate and return the lat/long with only 6
        # decimal places, i.e., input: _lat=123.45678910 -> output: 123.456789
        else:
            # i.e., returns: float("123" + "." + "456789") = 123.456789
            return float(non_decimal_half + "." + decimal_half[:6])


def verify_address_range(thing, range_number):
    """
    :param
    :type
    :return:
    :rtype:
    """
    if thing not in pvar.dictionary_for_address_ranges:
        raise ValueError("Incorrect thing: " + str(thing))
    verify_value_is_int(integer1=range_number, integer2=range_number)
    # range of address from list
    address_range_list = pvar.dictionary_for_address_ranges[thing]
    # get first number in list
    first_number_in_list = address_range_list[0]
    # get last number in list
    last_number_in_list = address_range_list[len(address_range_list)-1]
    if int(range_number) not in pvar.dictionary_for_address_ranges[thing]:
        raise ValueError("invalid range for "+thing+" needs to be between " +
                         str(first_number_in_list)+" and "+str(last_number_in_list))


def verify_value_is_string(string1, string2, length=None):
    """
    casts each parameter to string
    if strings are not equal, raise an assertion error, otherwise, return true
    :param string1:
    :param string2:
    :return:
    """
    if length is not None and (len(string1) > length or len(string2) > length):
        formated_error = "string1= [%s], string2= [%s]" % (string1, string2)
        raise AssertionError(("String value exceeds the specified max length of %s. " % length) + formated_error)
    received_str = str(string1)
    expected_str = str(string2)
    if received_str != expected_str:
        raise AssertionError("Received: " + received_str + ", Expected: " + expected_str)
    return True


def verify_value_is_float(float1, float2, tolerance=None):
    """
    On method invocation:
        try casting each float parameter to the "float" data type, if unsuccessful raise exception
    :param float1:      First float value
    :param float2:      Second float value
    :param tolerance:   Difference percentage allowed between both float numbers
    :return:
    """
    try:
        received_float = float(float1)
    except ValueError:
        raise AssertionError("Received value not a valid floating point number: " + str(float1))

    try:
        expected_float = float(float2)
    except ValueError:
        raise AssertionError("Expected value not a valid floating point number: " + str(float2))

    if tolerance is not None:
        try:
            tolerance_float = float(tolerance)
            lowlimit = (1 - tolerance_float / 100.0) * expected_float
            highlimit = (1 + tolerance_float / 100.0) * expected_float
        except ValueError:
            raise AssertionError("Tolerance value not a valid floating point number: " + str(tolerance))
    else:
        lowlimit = expected_float
        highlimit = expected_float

    if (received_float < 0 <= expected_float) or (received_float >= 0 > expected_float):
        raise AssertionError("Signs of the received and expected values are different.  Received: " + str(float1)
                             + ", Expected: " + str(float2))

    if (received_float < lowlimit) or (received_float > highlimit):
        raise AssertionError("Received value is greater than tolerance. Received: " + str(float1) + ", Expected: "
                             + str(float2) + ", Tolerance: " + str(tolerance))

    return True


def verify_value_is_int(integer1, integer2):
    """
    verify that a value received from the browser is an integer
    verify that the value expected is also an integer type
    lastly verifies that the received value matches the expected value
    :param integer1:
    :param integer2:
    :return:
    """
    try:
        received_int = int(str(integer1))
    except ValueError:
        raise AssertionError("Received value not a valid integer: " + str(integer1))

    try:
        expected_int = int(str(integer2))
    except ValueError:
        raise AssertionError("Expected value not a valid integer: " + str(integer2))

    if received_int != expected_int:
        raise AssertionError("Received: " + str(integer1) + ", Expected: " + str(integer2))

    return True


def rbg_to_html_color(rgb_tuple):
    """
    Converts the rgb tuple returned from the browser into a hex value color. \n
    :param rgb_tuple:   RGB Tuple received from browser \n
    :type rgb_tuple:    tuple \n
    :return hex_color:  Hex value representing the html color received from the browser.
    :rtype hex_color:   str \n
    """
    # '%02x' means zero-padded, 2-digit hex value, so we end up with a 6 digit hex value, ie: '#123456'
    hex_color = '#%02x%02x%02x' % rgb_tuple
    return hex_color


# -------------------------------------------------------------------------------------------------------------------- #
#                                                                                                                      #
#   Use Case Helpers                                                                                                   #
#                                                                                                                      #
# -------------------------------------------------------------------------------------------------------------------- #


#################################
def set_controller_substation_date_and_time(controller, substations, _date, _time):
    """
    Updates and verifies the date and time on controller and substations.
    
    :param controller: Controller to increment clock.
    :type controller: common.objects.controller.cn.Controller
    
    :param substations: Dict of substations to increment clocks.
    :type substations: dict[int, common.objects.substation.sb.Substation]
    
    :param _date: Date to set on the controller and substations. (i.e., date="04/01/2017")
    :type _date: str
    
    :param _time: Time to set on the controller and substations. (i.e., time="02:38:00")
    :type _time: str
    """
    # Verify date format passed in
    try:
        datetime.strptime(_date, '%m/%d/%Y')
    except ValueError:
        raise ValueError("Incorrect data format specified to set for controller and substations, should be MM/DD/YYYY")

    # Verify time format passed in
    try:
        datetime.strptime(_time, '%H:%M:%S')
    except ValueError:
        raise ValueError("Incorrect time format to set for controller and substations, should be HH:MM:SS")

    try:
        # Update date_mngr
        parsed_date = datetime.strptime(_date, '%m/%d/%Y').date()
        date_mngr.controller_datetime.set_from_datetime_obj(datetime_obj=parsed_date, time_string=_time)
        date_mngr.curr_day.set_from_datetime_obj(datetime_obj=parsed_date, time_string=_time)

        # Get date and time strings formatted for controller.
        date_to_set = date_mngr.controller_datetime.date_string_for_controller()
        time_to_set = date_mngr.controller_datetime.time_string_for_controller()

        # Update and verify controllers date and time
        controller.set_date_and_time_on_cn(_date=date_to_set, _time=time_to_set, update_date_mngr=False)
        controller.verify_date_and_time()

        # Update and verify each substation's date and time
        for sb_address in substations.keys():
            substations[sb_address].set_date_and_time(_date=date_to_set, _time=time_to_set, update_date_mngr=False)
            substations[sb_address].verify_date_and_time()
    except Exception as e:
        e_msg = "Exception occurred trying to set both controller and substations date and time to:\n" \
                "date={0}, time={1}\nException Received: {2}".format(_date, _time, e.message)
        raise Exception(e_msg)


#################################
def increment_controller_substation_clocks(controller, substations, hours=0, minutes=0, seconds=0):
    """
    Helper method to increment clocks for controller and substations without destroying date_mngr's current controller 
    date and time value.

    NOTE: The controller and substation must increment 1 minute at a time to stay in sync.
    
    USAGE: helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                 substations=self.config.substations,
                                                                 hours=10)
    
    :param controller: Controller to increment clock.
    :type controller: common.objects.controller.cn.Controller
    
    :param substations: Dict of substations to increment clocks.
    :type substations: dict[int, common.objects.substation.sb.Substation]

    :param hours: Hours to increment forward.
    :type hours: int
    
    :param minutes: Minutes to increment forward.
    :type minutes: int
    
    :param seconds: Seconds to increment forward.
    :type seconds: int
    """
    # Convert the total number of hours into minutes and add it to the total number of minutes
    total_minutes = (hours * 60) + minutes

    # If the amount of total minutes is not 0
    if total_minutes != 0:
        # Loop through until you run every minute individually
        while total_minutes > 0:

            # Update date_mngr every minute
            try:
                # Update controller's `datetime` place holder
                date_mngr.controller_datetime.increment_date_time(minutes=1)

                # Update the current day place holder if exists
                if date_mngr.curr_day:
                    date_mngr.curr_day.increment_date_time(minutes=1)
            except ValueError:
                e_msg = "Exception occurred trying to update `date_mngr`. Failed to set the date and time in the " \
                        "`date_mngr`. Attempted to increment clocks by: hours={0}, minutes={1}, seconds={2}.".format(
                            hours,
                            minutes,
                            seconds
                        )
                raise ValueError(e_msg)

            # Increment controller clock without updating date_mngr
            controller.do_increment_clock(minutes=1, update_date_mngr=False)

            # For each substation, increment clock without updating date_mngr
            for sb_address in substations.keys():
                substations[sb_address].do_increment_clock(minutes=1, update_date_mngr=False)

            # Decrement the number of minutes left to increment by 1
            total_minutes -= 1

    # If seconds was not 0 increment everything once, we don't want to loop through every second
    if seconds:

        # Update date_mngr every minute
        try:
            # Update controller's `datetime` place holder
            date_mngr.controller_datetime.increment_date_time(hours=hours, minutes=minutes, seconds=seconds)

            # Update the current day place holder if exists
            if date_mngr.curr_day:
                date_mngr.curr_day.increment_date_time(hours=hours, minutes=minutes, seconds=seconds)
        except ValueError:
            e_msg = "Exception occurred trying to update `date_mngr`. Failed to set the date and time in the " \
                    "`date_mngr`. Attempted to increment clocks by: hours={0}, minutes={1}, seconds={2}.".format(
                        hours,
                        minutes,
                        seconds
                    )
            raise ValueError(e_msg)

        # NOTE:
        #   The 3200 doesn't support incrementing clock by seconds, and since hours/minutes incrementation was done
        #   above, this line isn't necessary. - Ben
        #
        # Increment controller clock without updating date_mngr
        # controller.do_increment_clock(hours=hours, minutes=minutes, seconds=seconds, update_date_mngr=False)

        # NOTE:
        #   Since we have already incremented the substation clock above for hours/minutes, we only need to increment
        #   the seconds at this point. - Ben
        #
        # For each substation, increment clock without updating date_mngr
        for sb_address in substations.keys():
            substations[sb_address].do_increment_clock(seconds=seconds, update_date_mngr=False)

    # Verify Date and Time
    controller.verify_date_and_time()

    # For each substation, verify Date and Time
    for sb_address in substations.keys():
        substations[sb_address].verify_date_and_time()

#################################
def load_devices_on_controller_and_substations(controller, substations):
    """
    Loads devices onto the Controller and Substations. 
    
    :param controller: Controller attempting to connect to substations.
    :type controller: common.objects.controller.cn.Controller
    
    :param substations: Substations attempting to connect to controller.
    :type substations: dict[int, common.objects.substation.sb.Substation]
    """
    controller.load_assigned_devices()

    for sb_address in substations.keys():
        substations[sb_address].load_assigned_devices()


#################################
def set_bicoder_default_values_on_substation(substations):
    """
    Search for all available two-wire devices on a substation and program found devices with default values.
    
    :param substations: Dictionary of Substations to search and program devices for.
    :type substations: dict[int, common.objects.substation.sb.Substation]
    """
    # For each Substation, search for all available devices and then set their default values.
    for sb_address in substations.keys():

        substation = substations[sb_address]

        # Set default values for valve bicoders
        for valve_sn in substation.valve_bicoders.keys():
            substation.valve_bicoders[valve_sn].set_default_values()
            
        # Set default values for flow bicoders
        for flow_sn in substation.flow_bicoders.keys():
            substation.flow_bicoders[flow_sn].set_default_values()
            
        # Set default values for switch bicoders
        for switch_sn in substation.switch_bicoders.keys():
            substation.switch_bicoders[switch_sn].set_default_values()
            
        # Set default values for moisture bicoders
        for moisture_sn in substation.moisture_bicoders.keys():
            substation.moisture_bicoders[moisture_sn].set_default_values()
            
        # Set default values for temperature bicoders
        for temperature_sn in substation.temp_bicoders.keys():
            substation.temp_bicoders[temperature_sn].set_default_values()
            
        # Set default values for pump bicoders
        for pump_sn in substation.pump_bicoders.keys():
            substation.pump_bicoders[pump_sn].set_default_values()


#################################
def set_all_device_default_values_on_controller(config, controller):
    """
    Loads all serial numbers assigned to the controller.
    
    :param config:   Test configuration instance
    :type config:    old_32_10_sb_objects_dec_29_2017.common.configuration.Configuration
    
    :param controller: Controller to search and program devices for.
    :type controller: old_32_10_sb_objects_dec_29_2017.common.objects.controller.cn.Controller
    """
    # Zones
    controller.set_address_and_default_values_for_zn(zn_object_dict=config.zones, zn_ad_range=config.zn_ad_range)

    # Master Valves
    controller.set_address_and_default_values_for_mv(mv_object_dict=config.master_valves,
                                                     mv_ad_range=config.mv_ad_range)
    # Moisture sensors
    controller.set_address_and_default_values_for_ms(ms_object_dict=config.moisture_sensors,
                                                     ms_ad_range=config.ms_ad_range)
    # Temperature sensors
    controller.set_address_and_default_values_for_ts(ts_object_dict=config.temperature_sensors,
                                                     ts_ad_range=config.ts_ad_range)
    # Event Switches
    controller.set_address_and_default_values_for_sw(sw_object_dict=config.event_switches,
                                                     sw_ad_range=config.sw_ad_range)
    # Flow Meters
    controller.set_address_and_default_values_for_fm(fm_object_dict=config.flow_meters, fm_ad_range=config.fm_ad_range)
    

#################################
def connect_controller_to_substations(controller, substations):
    """
    Establishes connection between controller and substations.
    
    :param controller: Controller attempting to connect to substations.
    :type controller: common.objects.controller.cn.Controller
    
    :param substations: Substations attempting to connect to controller.
    :type substations: dict[int, common.objects.substation.sb.Substation]
    :return: 
    """
    # Connect each Substation to the Controller.
    for sb_address in substations.keys():
        substation = substations[sb_address]

        # Connect controller to Substation
        controller.set_connection_to_substation(_address=sb_address, _ip_address=substation.ad)


#################################
def wait_for_controller_and_substations_connection_status(controller, substations, max_wait,
                                                          expected_status=opcodes.connected):
    """
    Waits for the Controller and Substation to establish a connection.
    
    :param expected_status: 
    :param expected_status: 
    :param controller: Controller attempting to connect to substations.
    :type controller: common.objects.controller.cn.Controller
    
    :param substations: Substations attempting to connect to controller.
    :type substations: dict[int, common.objects.substation.sb.Substation]
    
    :param max_wait: Number of minutes to wait before raising exception.
    :type max_wait: int
    :return: 
    """
    max_wait_for_connection = max_wait  # Maximum number of minutes to wait for reconnection to succeed
    current_wait_for_connection = 1     # Current number of minutes waited for reconnection

    while current_wait_for_connection <= max_wait_for_connection:

        try:
            time.sleep(1)
            increment_controller_substation_clocks(controller=controller, substations=substations, minutes=1)

            # Now, for each substation, verify connection.
            for sb_address in substations.keys():
                # Verify Controller is connected
                controller.verify_connection_to_substation(_address=sb_address, _expected_status=expected_status)
                # Verify Substation is connected.
                substations[sb_address].verify_connection_to_controller(_expected_status=expected_status)

            # If we hit this point we verified the connection successfully
            break

        # Catch ValueError exception for when status is not connected
        except ValueError as e:

            # If we have waited for 5 minutes and controllers haven't reconnected, raise exception.
            if current_wait_for_connection == max_wait_for_connection:
                e_msg = "Connection timed out (waited up to {0} minutes) attempting to connect 3200 " \
                        "to substation.\n {1}".format(max_wait_for_connection, e.message)
                raise Exception(e_msg)
            else:
                current_wait_for_connection += 1


#################################
def end_controller_test(config_object):
    """
    this method is used a double check to verify that the serial port gets closed and sim mode gets turned off
    its like a big "Hammer"
    :param config_object:   Test configuration instance
    :type config_object:    old_32_10_sb_objects_dec_29_2017.common.configuration.Configuration
    """
    try:
        if (1 in config_object.controllers) and config_object.controllers[1].ser.serial_conn.isOpen():
            config_object.controllers[1].set_sim_mode_to_off()
    except Exception as e:
        print "Error trying to turn sim mode off during test: {0}. \nError: {1}".format(config_object.test_name,
                                                                                        e.message)
    try:
        config_object.resource_handler.release()
    except Exception as e:
        print "Error trying to release serial port during test: {0}. \nError: {1}".format(config_object.test_name,
                                                                                          e.message)


#################################
def end_multiple_controller_test(config_object):
    """
    This method is for Use Cases involving Substations. Called prior to the test exiting on a pass/fail.
    
    :param config_object:   Test configuration instance
    :type config_object:    old_32_10_sb_objects_dec_29_2017.common.configuration.Configuration
    """
    # Set sim mode to off for 3200
    for cn in config_object.controllers.keys():
        config_object.controllers[cn].set_sim_mode_to_off()

    # Set sim mode to off for Substations
    for sb in config_object.substations.keys():
        config_object.substations[sb].set_sim_mode_to_off()

    # Release all serial port connections and web driver (if applicable)
    config_object.resource_handler.release()


#################################
def print_test_passed(test_name):

    message = "\n####################    " + test_name + " Test Passed    ####################\n"
    print message
    log_handler.debug(message=message)   # Will only log if logging is enabled

    time.sleep(5)


#################################
def print_test_failed(test_name):

    message = "\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!WARNING!!!!!!!!!!!!ERROR!!!!!!!!!!!WARNING!!!!!!!!!!!!!!!!!!!!!!!!!!!!" \
              "\n##########################    " + test_name + " TEST FAILED    ###########################" \
              "\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!WARNING!!!!!!!!!!!!ERROR!!!!!!!!!!!WARNING!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
    print message
    log_handler.debug(message=message)   # Will only log if logging is enabled


#################################
def print_test_started(test_name):

    message = "\n####################    Starting Test " + test_name + "    ############################\n"
    print message
    log_handler.debug(message=message)   # Will only log if logging is enabled


#################################
def print_method_name():
    import inspect

    # inspect.stack() returns an array
    method_name = inspect.stack()[1][3]

    # Remove underscore from method name and capitalize it.
    # "step_2" => "Step 2"
    method_name = method_name.replace("_", " ").title()

    method = "\n\n####################################################################################################"
    method += "\n|"
    method += "\n|-->  Running " + method_name.title() + "\n"
    method += "|\n"
    method += "####################################################################################################\n\n"
    print method


#################################
def print_step_success(test_name, msg):
    import inspect
    # inspect.stack() returns an array
    method_name = inspect.stack()[1][3]
    # Remove underscore from method name and capitalize it.
    # "step_2" => "Step 2"
    method_name = method_name.replace("_", " ").title()
    method_name = method_name.title()
    
    success_msg = "\n----\n"
    success_msg += "Successfully finished {0} in {1} Test\n".format(method_name, test_name)
    success_msg += "-> {0}".format(msg)
    success_msg += "\n----\n"
    print(success_msg)


def get_ip_currentsystem():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP