__author__ = 'Ben'
"""
Helper module to house all common equations that can be shared/used among the test suite.

This module was created to add substation equations for the following calculations plus any other equations needed that
do not relate to EPA:
    - Flow Rate Count
    - Flow Usage Count
"""


#################################
def calculate_flow_rate_count_from_gpm(gpm, kval):
    """
    Calculates the Flow Rate Count from a GPM value.
    
    Equation: 
        Flow Rate Count = GPM * 12 / kValue
    
    :param gpm: Flow rate to convert to a count.
    :type gpm: int | float
    
    :param kval: BiCoder's kValue used for calculation.
    :type kval: int | float
    
    :return: Flow Rate Count converted from GPM
    :rtype: int
    """
    flow_rate_count = (gpm * 12) / kval
    return int(flow_rate_count)


#################################
def calculate_gpm_from_flow_rate_count(count, kval):
    """
    Calculates the GPM value from the Flow Rate Count passed in.
    
    Equation: 
        GPM = rateCount * kValue / 12
    
    :param count: Flow rate count to convert.
    :type count: int
    
    :param kval: BiCoder's kValue used for calculation.
    :type kval: int | float
    
    :return: GPM value
    :rtype: int | float
    """
    gpm = (count * kval) / 12
    return gpm


#################################
def calculate_flow_usage_count_from_gallons(gal, kval):
    """
    Calculates the Flow Usage Count from a Gallons value.
    
    Equation: 
        usageCount = GAL * 60 / kValue
    
    :param gal: Flow usage in GAL to convert to a count.
    :type gal: int | float
    
    :param kval: BiCoder's kValue used for calculation.
    :type kval: int | float
    
    :return: Flow Usage Count converted from a total gallons value.
    :rtype: int
    """
    flow_usage_count = (gal * 60) / kval
    return int(flow_usage_count)


#################################
def calculate_gallons_from_flow_usage_count(count, kval):
    """
    Calculates the total Gallons used from the Flow Usage Count passed in.
    
    Equation: 
        GAL = usageCount * kValue / 60
    
    :param count: Flow usage count to convert.
    :type count: int
    
    :param kval: BiCoder's kValue used for calculation.
    :type kval: int | float
    
    :return: GAL value
    :rtype: int | float
    """
    total_gallons = (count * kval) / 60
    return total_gallons

