__author__ = 'baseline'
#
#
# class OpCodes(object):
#     """
#     OpCode 'enum' class. Defines all test engine opcodes that are used for the 1000 and 3200 controller's. \n
#     These can be accessed once this 'imports.py' module has been imported, for example: \n
#     -   from old_32_10_sb_objects_dec_29_2017.common.imports import * \n
#         OpCodes.serial_number now returns "SN"
#     """
#     alert_relay = "AR"
#     alternate_server_ip = "AI"
#     assign_4to20_milliamp_sensor_decoder = "AI"
#     assign_event_switch_decoder = "AS"
#     assign_flow_meter_decoder = "AF"
#     assign_master_valve_decoder = "AV"
#     assign_moisture_sensor_decoder = "AM"
#     assign_temperature_Sensor_decoder = "AT"
#     assign_zone_decoder = "AZ"
#     bad_command = "BC"
#     baseLine = "BL"
#     basemanager = "BM"
#     basemanager_registration_key = "KY"
#     booster_pump = "BP"
#     budget_exceeded = "BX"
#     calendar_interval = "CI"
#     calibrate_cycle = "CC"
#     calibrate_monthly = "MO"
#     calibrate_never = "NV"
#     calibrate_one_time = "SG"
#     clear_all = "CL"
#     closed = "CL"
#     connect_to_basemanager = "CN"
#     connecting = "CG"
#     contacts_closed = "CL"
#     contacts_open = "OP"
#     contacts_state = "VC"
#     controller = "CN"
#     crop_coefficient = "KC"
#     current_basemanger_server_ip_address = "AD"
#     cycle_count = "CT"
#     cycle_time = "CT"
#     date_time = "DT"
#     day_interval = "DI"
#     description = "DS"
#     design_flow = "DF"
#     device_type = "DT"
#     disconnect_from_basemanager = "DC"
#     disconnected = "DC"
#     disabled = "DS"
#     done_watering = "DN"
#     empty_wait_time = "EW"
#     enabled = "EN"
#     error = "ER"
#     eto = "ET"
#     et_mode = "EM"
#     etc_deficit_trigger_threshold = "ET"
#     even_day = "EV"
#     event_switch = "SW"
#     false = "FA"
#     fill_time = "FT"
#     flow_fault = "FF"
#     flow_meter = "FM"
#     flow_rate = "VR"
#     flow_variance = "VF"
#     flow_variance_enable = "VE"
#     four_valve_decoder = "D4"
#     four_to_20_milliamp_sensor_decoder = "IS"
#     friday = "W6"
#     high_flow_limit = "HF"
#     high_flow_shutdown = "HF"
#     high_flow_variance = "FF"
#     high_variance_limit = "HV"
#     high_flow_variance_shutdown = "HS"
#     historical_calendar = "ET"
#     initial_ETo = "EI"
#     interval_days = "ID"
#     intelligent_soak = "SM"
#     k_value = "KV"
#     learn_flow = "LF"
#     latitude = "LA"
#     limit_concurrent_to_target = "LF"
#     limit_zones_by_flow = "LC"
#     linked_mode = "LINKED"
#     longitude = "LG"
#     low_flow_limit = "LF"
#     low_flow_shutdown = "LF"
#     low_variance_limit = "LV"
#     low_variance_shut_down = "LS"
#     lower_limit = "LL"
#     mainline = "ML"
#     master_valve = "MV"
#     max_concurrent_zones = "MC"
#     mirror_zone = "MR"
#     moisture_empty_limit = "ME"
#     moisture_mode = "MM"
#     moisture_pause_time = "MP"
#     moisture_percent = "VP"
#     moisture_sensor = "MS"
#     moisture_sensor_empty_enable = "MN"
#     moisture_trigger_threshold = "MT"
#     monday = "W2"
#     monthly_water_budget = "WB"
#     Nelson = "NE"
#     normally_open = "NO"
#     odd_day = "OD"
#     odd_days_skip_31 = "OS"
#     off = "OF"
#     okay = "OK"
#     open = "OP"
#     out_of_water = "OW"
#     paused = "PA"
#     ping_server = "PN"
#     point_of_connection = "PC"
#     precipitation_rate = "PR"
#     primary_zone_number = "PZ"
#     primary_mode = "PRIMARY"
#     priority = "PR"
#     program = "PG"
#     program_cycles = "CY"
#     program_pause_condition = "PS"
#     program_start_condition = "PT"
#     program_stop_condition = "PP"
#     program_semi_month_interval = "SM"
#     rain_master = "RM"
#     registering = "RG"
#     not_registered = "RG"
#     run_time = "RT"
#     running = "RN"
#     runtime_tracking_ratio = "RA"
#     saturday = "W7"
#     search_for_device = "SR"
#     seasonal_adjust = "SA"
#     semi_month_interval = "SI"
#     serial_number = "SN"
#     shutdown_on_high_flow = "HS"
#     shutdown_on_over_budget = "WS"
#     shutdown_on_unscheduled = "US"
#     simulation_mode = "SM"
#     single_valve_decoder = "D1"
#     soak_cycle = "SO"
#     soak_time = "ST"
#     soaking = "SO"
#     soak_cycle_mode = "SK"
#     solenoid_current = "VA"
#     solenoid_voltage = "VV"
#     start_times = "ST"
#     status_code = "SS"
#     stop_immediately = "SI"
#     sunday = "W1"
#     switch_mode = "SM"
#     switch_empty_condition = "SE"
#     switch_pause_time = "SP"
#     switch_empty_enable = "SN"
#     target_flow = "FL"
#     temperature_value = "VD"
#     temperature_pause_time = "TP"
#     temperature_sensor = "TS"
#     temperature_sensor_mode = "TM"
#     temperature_trigger_threshold = "TT"
#     thursday = "W5"
#     time = "TM"
#     timed = "TM"
#     timed_mode = "TIMED"
#     toro = "TO"
#     total_usage = "VG"
#     true = "TR"
#     tuesday = "W3"
#     twelve_valve_decoder = "DD"
#     two_valve_decoder = "D2"
#     two_wire_drop = "VT"
#     type = "TY"
#     unassigned = "UN"
#     unscheduled_flow_limit = "UF"
#     upper_limit = "UL"
#     use_alternate_server_ip = "UA"
#     version = "VZ"
#     waiting_to_water = "WA"
#     water_rationing_enable = "WR"
#     water_strategy = "WS"
#     watering = "WT"
#     weathermatic = "WM"
#     wednesday = "W4"
#     week_days = "WD"
#     weekly_water_window = "WW"
#     zone = "ZN"
#     zone_program = "PZ"
#     zone_soak_cycles = "ZN"
#
#
# class ObjectTypes(object):
#     """
#     Object Type 'enum' class. Defines all object's that are used in test scripts. \n
#     These can be accessed once this 'imports.py' module has been imported, for example: \n
#     -   from old_32_10_sb_objects_dec_29_2017.common.imports import * \n
#         ObjectTypes.Zone now returns "ZN"
#     """
#     Zone = OpCodes.zone
#     Moisture_Sensor = OpCodes.moisture_sensor
#     Flow_Meter = OpCodes.flow_meter
#     Temperature_Sensor = OpCodes.temperature_sensor
#     Master_Valve = OpCodes.master_valve
#     Event_Switch = OpCodes.event_switch
#     Controller = OpCodes.controller
#     Program = OpCodes.program
#
#
# # Enum Class
# class StartStopPauseConditionType(object):
#     """
#     Based on the 'Test Commands Specification' document
#     """
#     STOP_COND = "PP"
#     START_COND = "PS"
#     PAUSE_COND = "PT"
#
#
# # Enum Class
# class StartStopPauseEventType(object):
#     EVENT_SWITCH = "SW"
#     TEMP_SENSOR = "TS"
#     MOISTURE_SENSOR = "MS"
#     DATE_TIME = "DT"
#     WEATHER_BASED = "ET"
#
#
# class CSVHeaders (object):
#
#     hydrozone_information = ([" "], ["Soil Type"], ["Crop Coefficient"], ["Slope"], ["Sun Exposure"],
#                              ["Allowable Depletion"], ["Root Depth"], ["Precipitation rate"],
#                              ["Distribution Uniformity"], ["Basic Soil Intake Rate"],
#                              ["Allowable Surface Accumulation"], ["Cycle Time"], ["Soak Time"])
#
#     # Zone Headers for 3200
#     class ZoneHeadersFor3200(object):
#         """
#         When you do a get command on the zone this is all of the attributes that are return
#         """
#
#         all_zone_headers = [OpCodes.zone, OpCodes.serial_number, OpCodes.description, OpCodes.enabled, OpCodes.type,
#                             OpCodes.latitude, OpCodes.longitude, OpCodes.design_flow, OpCodes.status_code,
#                             OpCodes.crop_coefficient, OpCodes.precipitation_rate,
#                             WaterSenseCodes.Root_Zone_Working_Water_Storage, WaterSenseCodes.Distribution_uniformity,
#                             WaterSenseCodes.Daily_Moisture_Balance, OpCodes.solenoid_current,
#                             OpCodes.solenoid_voltage, OpCodes.two_wire_drop, OpCodes.run_time, OpCodes.cycle_time,
#                             OpCodes.soak_cycle, OpCodes.zone_program, WaterSenseCodes.Enable_ET_Runtime,
#                             OpCodes.water_strategy
#                             ]
#
#     # Zone Headers for 1000
#     class ZoneHeadersFor1000(object):
#
#         all_zone_headers = [OpCodes.zone, OpCodes.serial_number, OpCodes.description, OpCodes.enabled, OpCodes.type,
#                             OpCodes.latitude, OpCodes.longitude, OpCodes.design_flow, OpCodes.status_code,
#                             OpCodes.crop_coefficient, OpCodes.precipitation_rate,
#                             WaterSenseCodes.Root_Zone_Working_Water_Storage, WaterSenseCodes.Distribution_uniformity,
#                             WaterSenseCodes.Daily_Moisture_Balance, OpCodes.solenoid_current,
#                             OpCodes.solenoid_voltage, OpCodes.two_wire_drop, OpCodes.run_time, OpCodes.cycle_time,
#                             OpCodes.soak_cycle, OpCodes.zone_program, WaterSenseCodes.Enable_ET_Runtime,
#                             OpCodes.water_strategy
#                             ]
