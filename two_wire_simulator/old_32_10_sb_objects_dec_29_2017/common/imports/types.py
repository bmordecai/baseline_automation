"""
Object Type 'enum' class. Defines all object's that are used in test scripts.

These can be accessed once this 'types.py' module has been imported, for example:
    - First, import module as:

        >>> from old_32_10_sb_objects_dec_29_2017.common.imports import types

    - Now,

        >>> print types.program
        "PG"

        >>> print types.Device.flow_meter
        "FM"
"""
import opcodes

# general types go below.
program = opcodes.program


class Device(object):
    """
    Contains different device types (or objects we consider to be "Device").
    """
    controller = opcodes.controller
    event_switch = opcodes.event_switch
    flow_meter = opcodes.flow_meter
    master_valve = opcodes.master_valve
    moisture_sensor = opcodes.moisture_sensor
    temperature_sensor = opcodes.temperature_sensor
    zone = opcodes.zone


class MessageCategory(object):
    """
    Contains different message category types
    """
    basemanager = opcodes.basemanager
    controller = opcodes.controller
    event_switch = opcodes.event_switch
    flow_meter = opcodes.flow_meter
    flow_station = opcodes.flow_station
    mainline = opcodes.mainline
    moisture_sensor = opcodes.moisture_sensor
    master_valve = opcodes.master_valve
    point_of_connection = opcodes.point_of_connection
    program = opcodes.program
    pump_station = opcodes.pump_station
    temperature_sensor = opcodes.temperature_sensor
    zone = opcodes.zone
    zone_program =opcodes.zone_program

class MessagePriority(object):
    low = 'LW'
    medium = 'MD'
    high = 'HH'
    none = 'NN'

class Action(object):
    """
    Contains different types of actions
    """
    set = opcodes.set_action
    get = opcodes.get_action


class Message(object):
    """
    Contains different message types and status codes used.
    """
    admin = opcodes.admin
    bad_serial = opcodes.bad_serial
    base_manager = opcodes.basemanager
    base_manager_source = opcodes.basemanager_source
    boot_up = opcodes.boot_up
    budget_exceeded = "BD"
    budget_exceeded_shutdown = opcodes.budget_exceeded_shutdown
    calibrate_failure_no_change = opcodes.calibrate_failure_no_change
    calibrate_failure_no_saturation = opcodes.calibrate_failure_no_saturation
    calibrate_successful = opcodes.calibrate_successful
    checksum = opcodes.checksum
    commander_paused = opcodes.commander_paused
    day_time = opcodes.date_time
    day_and_time_done = opcodes.date_time
    device_error = "DE"
    empty_shutdown = opcodes.empty_shutdown
    error_operation_terminated = opcodes.error_operation_terminated
    event_date_stop = opcodes.event_date_stop
    event_day = opcodes.event_day
    event_switch = opcodes.event_switch
    exceeds_design_flow = opcodes.exceeds_design_flow
    fallback_mode_booster_pump = opcodes.fallback_mode_booster_pump
    fallback_mode_program = opcodes.fallback_mode_program
    fallback_mode_poc = opcodes.fallback_mode_poc
    flow_jumper_stopped = opcodes.flow_jumper_stopped
    flow_learn_errors = opcodes.flow_learn_errors
    flow_learn_ok = opcodes.flow_learn_ok
    high_flow_detected = opcodes.high_flow_detected
    high_flow_shutdown = opcodes.high_flow_shutdown
    high_flow_shutdown_by_flow_station = opcodes.high_flow_shutdown_by_flow_station
    high_flow_variance_detected = opcodes.high_flow_variance_detected
    high_flow_variance_shutdown = opcodes.high_flow_variance_shutdown
    internal_failure = opcodes.internal_failure
    learn_flow_fail_flow_biCoders_disabled = opcodes.learn_flow_fail_flow_biCoders_disabled
    learn_flow_fail_flow_biCoders_error = opcodes.learn_flow_fail_flow_biCoders_error
    learn_flow_with_errors = opcodes.learn_flow_with_errors
    learn_flow_success = opcodes.learn_flow_success
    low_flow_shutdown_by_flow_station = opcodes.low_flow_shutdown_by_flow_station
    low_flow_variance_detected = opcodes.low_flow_variance_detected
    low_flow_variance_shutdown = opcodes.low_flow_variance_shutdown
    low_voltage = "LV"
    message = opcodes.message
    no_message = opcodes.no_message
    moisture = opcodes.moisture
    no_24_vac = opcodes.no_24_vac
    no_response = opcodes.no_response
    open_circuit = opcodes.open_circuit
    operator = opcodes.operator
    open_circuit_1000 = "OP"
    over_run_start_event = opcodes.over_run_start_event
    pause = "PU"
    pause_event_switch = opcodes.pause_event_switch
    pause_jumper = opcodes.pause_jumper
    pause_moisture_sensor = opcodes.pause_moisture_sensor
    pause_temp_sensor = opcodes.pause_temp_sensor
    priority_paused = opcodes.priority_paused
    programmer = opcodes.programmer
    rain_delay = opcodes.rain_delay
    rain_delay_stopped = opcodes.rain_delay_stopped
    rain_jumper_stopped = opcodes.rain_jumper_stopped
    rain_switch = opcodes.rain_switch
    restore_failed = opcodes.restore_failed
    restore_successful = opcodes.restore_successful
    restricted_time_by_water_ration = opcodes.restricted_time_by_water_ration
    requires_soak_cycle = opcodes.requires_soak_cycle
    sensor_disabled = opcodes.sensor_disabled
    serial_number = "DV"
    set_upper_limit_failed = opcodes.set_upper_limit_failed
    short_circuit = opcodes.short_circuit
    short_circuit_1000 = "OC"
    skipped_by_moisture_sensor = opcodes.skipped_by_moisture_sensor
    start = "ST"
    started_event_switch = opcodes.started_event_switch
    started_moisture_sensor = opcodes.started_moisture_sensor
    started_temp_sensor = opcodes.started_temp_sensor
    started_by_bad_moisture_sensor = opcodes.started_by_bad_moisture_sensor
    stop = "SP"
    stop_event_switch = opcodes.stop_event_switch
    stop_moisture_sensor = opcodes.stop_moisture_sensor
    stop_temp_sensor = opcodes.stop_temp_sensor
    system = opcodes.system
    system_off = opcodes.system_off
    temperature = opcodes.temperature_sensor
    two_wire_high_current_shutdown = opcodes.two_wire_high_current_shutdown
    two_wire_over_current = "OC"
    unscheduled_flow_detected = opcodes.unscheduled_flow_detected
    unscheduled_flow_shutdown = opcodes.unscheduled_flow_shutdown
    variable_one = "V1"
    variable_two = "V2"
    usb_flash_storage_failure = opcodes.usb_flash_storage_failure
    usb_flash_storage_source = opcodes.usb_flash_storage_source
    user = opcodes.user
    water_window_paused = opcodes.water_window_paused
    water_window = opcodes.water_window_paused
    weather_eto_data_not_available = "ET"
    zero_reading = opcodes.zero_reading


class IrrigationSystem(object):
    """
    Contains the different types of sprinklers and other irrigation systems used for EPA programming.
    """
    bubblers = "Bubblers"
    impact = "Impacts"
    popup_rotory_heads = "Popup Rotors Heads"
    popup_spray_heads = "Popup Spray Heads"
    rotors = "Rotors"
    surface_drip = "Surface Drip"


class SoilTexture(object):
    """
    Contains different soil textures used for EPA programming.
    """
    clay = "Clay"
    clay_loam = "Clay Loam"
    loam = "Loam"
    loamy_sand = "Loamy Sand"
    sand = "Sand"
    sandy_loam = "Sandy Loam"
    silty_clay = "Silty Clay"


class StartStopPauseCondition(object):
    """
    Contains the different program start/stop/pause condition types.
    """
    pause = opcodes.program_pause_condition     # "PS"
    start = opcodes.program_start_condition     # "PT"
    stop = opcodes.program_stop_condition       # "PP"


class StartStopPauseEvent(object):
    """
    Contains the different program start/stop/pause event types.
    """
    date_time = opcodes.date_time               # "DT"
    event_switch = opcodes.event_switch         # "SW"
    moisture_sensor = opcodes.moisture_sensor   # "MS"
    temp_sensor = opcodes.temperature_sensor    # "TS"
    weather_based = opcodes.program_event_weather_based     # "ET"


class Vegetation(object):
    """
    Contains the different types of vegetation used for EPA programming.
    """
    bermuda = "Bermuda"
    fescue = "Fescue"
    ground_cover = "Ground Cover"
    trees_and_ground_cover = "Trees and Ground Cover"
    woody_shrubs = "Woody Shrubs"


status_code_dict_1000 = {
    MessageCategory.controller: [
        Message.two_wire_over_current
    ],
    MessageCategory.event_switch: [
        Message.checksum,
        Message.no_response,
        Message.bad_serial
    ],
    MessageCategory.flow_meter: [
        Message.checksum,
        Message.no_response,
        Message.bad_serial
    ],
    MessageCategory.moisture_sensor: [
        Message.checksum,
        Message.no_response,
        Message.bad_serial
    ],
    MessageCategory.master_valve: [
        Message.checksum,
        Message.no_response,
        Message.bad_serial,
        Message.low_voltage,
        "OP",
        "OC"
    ],
    MessageCategory.point_of_connection: [
        Message.device_error,
        Message.high_flow_shutdown,
        Message.unscheduled_flow_shutdown
    ],
    MessageCategory.program: [
        Message.over_run_start_event,
        Message.learn_flow_with_errors,
        Message.calibrate_failure_no_change,
        Message.start,
        Message.pause,
        Message.stop
    ],
    MessageCategory.temperature_sensor: [
        Message.checksum,
        Message.no_response,
        Message.bad_serial
    ],
    MessageCategory.zone: [
        Message.checksum,
        Message.no_response,
        Message.bad_serial,
        Message.low_voltage,
        Message.open_circuit_1000,
        Message.short_circuit_1000,
        Message.high_flow_variance_shutdown
    ]
}

# HOW TO USE
# pg=_status_code_dict_3200[cy_type][_status_code]
# This dictionary is used to iterate through the status codes of each object
status_plus_priority_code_dict_3200 = {
    MessageCategory.controller: {
        Message.commander_paused: MessagePriority.low,
        Message.boot_up: MessagePriority.medium,
        Message.event_date_stop: MessagePriority.low,
        Message.usb_flash_storage_failure: MessagePriority.medium,
        Message.flow_jumper_stopped: MessagePriority.high,
        Message.two_wire_high_current_shutdown: MessagePriority.high,
        Message.pause_event_switch: MessagePriority.low,
        Message.pause_jumper: MessagePriority.low,
        Message.pause_moisture_sensor: MessagePriority.low,
        Message.pause_temp_sensor: MessagePriority.low,
        Message.rain_delay_stopped: MessagePriority.low,
        Message.rain_jumper_stopped: MessagePriority.low,
        Message.stop_event_switch: MessagePriority.low,
        Message.stop_moisture_sensor: MessagePriority.low,
        Message.stop_temp_sensor: MessagePriority.low,
        Message.internal_failure: MessagePriority.high,
        Message.restore_failed: MessagePriority.high,
        Message.restore_successful: MessagePriority.low,
    },
    MessageCategory.event_switch: {
        Message.bad_serial: MessagePriority.low,
        Message.no_response: MessagePriority.high,
    },
    MessageCategory.flow_meter: {
        Message.bad_serial: MessagePriority.low,
        Message.no_response: MessagePriority.high,
        Message.set_upper_limit_failed: MessagePriority.medium,
    },
    MessageCategory.flow_station: {
        Message.bad_serial: MessagePriority.low,
        Message.no_response: MessagePriority.high,
        Message.set_upper_limit_failed: MessagePriority.medium,
        Message.fallback_mode_booster_pump: MessagePriority.medium,
        Message.fallback_mode_program: MessagePriority.medium,
        Message.fallback_mode_poc: MessagePriority.medium,
        Message.error_operation_terminated: MessagePriority.medium,
    },
    MessageCategory.mainline: {
        Message.learn_flow_fail_flow_biCoders_disabled: MessagePriority.medium,
        Message.learn_flow_fail_flow_biCoders_error: MessagePriority.medium,
        Message.high_flow_variance_detected: MessagePriority.medium,
        Message.low_flow_variance_detected: MessagePriority.low,
    },
    MessageCategory.moisture_sensor: {
        Message.bad_serial: MessagePriority.low,
        Message.sensor_disabled: MessagePriority.medium,
        Message.no_response: MessagePriority.high,
        Message.zero_reading: MessagePriority.medium,
    },
    MessageCategory.master_valve: {
        Message.bad_serial: MessagePriority.low,
        Message.no_response: MessagePriority.medium,
        Message.open_circuit: MessagePriority.medium,
        Message.short_circuit: MessagePriority.medium,
    },
    MessageCategory.point_of_connection: {
        Message.budget_exceeded: MessagePriority.low,
        Message.budget_exceeded_shutdown: MessagePriority.medium,
        Message.high_flow_detected: MessagePriority.high,
        Message.high_flow_shutdown: MessagePriority.high,
        Message.empty_shutdown: MessagePriority.medium,
        Message.unscheduled_flow_detected: MessagePriority.low,
        Message.unscheduled_flow_shutdown: MessagePriority.high,
    },
    MessageCategory.program: {
        Message.event_date_stop: MessagePriority.none,
        Message.skipped_by_moisture_sensor: MessagePriority.low,
        Message.learn_flow_with_errors: MessagePriority.medium,
        Message.learn_flow_success: MessagePriority.low,
        Message.started_by_bad_moisture_sensor: MessagePriority.high,
        Message.over_run_start_event: MessagePriority.medium,
        Message.pause_event_switch: MessagePriority.none,
        Message.pause_moisture_sensor: MessagePriority.none,
        Message.priority_paused: MessagePriority.none,
        Message.pause_temp_sensor: MessagePriority.none,
        Message.restricted_time_by_water_ration: MessagePriority.none,
        Message.stop_event_switch: MessagePriority.none,
        Message.stop_moisture_sensor: MessagePriority.none,
        Message.stop_temp_sensor: MessagePriority.none,
        Message.started_event_switch: MessagePriority.none,
        Message.started_moisture_sensor: MessagePriority.none,
        Message.started_temp_sensor: MessagePriority.none,
        Message.water_window_paused: MessagePriority.none,
    },
    MessageCategory.temperature_sensor: {
        Message.bad_serial: MessagePriority.low,
        Message.no_response: MessagePriority.high,
    },
    MessageCategory.zone: {
        Message.bad_serial: MessagePriority.medium,
        Message.no_24_vac: MessagePriority.high,
        Message.no_response: MessagePriority.high,
        Message.open_circuit: MessagePriority.medium,
        Message.short_circuit: MessagePriority.medium,
    },
    MessageCategory.zone_program: {
        Message.calibrate_failure_no_change: MessagePriority.medium,
        Message.calibrate_successful: MessagePriority.none,
        Message.calibrate_failure_no_saturation: MessagePriority.medium,
        Message.exceeds_design_flow: MessagePriority.medium,
        Message.flow_learn_errors: MessagePriority.medium,
        Message.flow_learn_ok: MessagePriority.low,
        Message.high_flow_shutdown_by_flow_station: MessagePriority.high,
        Message.high_flow_variance_shutdown: MessagePriority.high,
        Message.high_flow_variance_detected: MessagePriority.high,
        Message.low_flow_shutdown_by_flow_station: MessagePriority.medium,
        Message.low_flow_variance_shutdown: MessagePriority.medium,
        Message.low_flow_variance_detected: MessagePriority.medium,
        Message.requires_soak_cycle: MessagePriority.medium,
    }
}


program_1000_who_dict = {
    Message.start: {
        Message.base_manager: "BaseManager",
        Message.user: "User",
        Message.operator: "Operator",
        Message.programmer: "Programmer",
        Message.admin: "Admin",
        Message.moisture: "Moisture",
        Message.event_switch: "E. Switch",
        Message.temperature: "Temperature",
        Message.day_time: "Day & Time"
    },
    Message.pause: {
        Message.system: "System Wait",
        Message.moisture: "Moisture",
        Message.event_switch: "E. Switch",
        Message.temperature: "Temperature",
        Message.water_window: "Water Win.",
        Message.event_day: "Event Day",
    },
    Message.stop: {
        Message.base_manager: "BaseManager",
        Message.user: "User",
        Message.operator: "Operator",
        Message.programmer: "Programmer",
        Message.admin: "Admin",
        Message.moisture: "Moisture",
        Message.event_switch: "E. Switch",
        Message.temperature: "Temperature",
        Message.day_and_time_done: "Day & Time",
        Message.system_off: "System Off",
        Message.rain_switch: "Rain Switch",
        Message.rain_delay: "Rain Delay"
    }
}