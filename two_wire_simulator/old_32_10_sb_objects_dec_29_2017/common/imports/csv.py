"""
Object CSV 'enum' class. Defines all CSV related data that are used in test scripts.

These can be accessed once this 'csv.py' module has been imported, for example:
    - First, import module as:

        >>> from old_32_10_sb_objects_dec_29_2017.common.imports import csv

    - Now,

        >>> print csv.For1000.zone_headers
        ["ZN", "SN", "DS", "EN", "TY", "LA", "LG"...]

        >>> print csv.EPA.hydro_zone_headers
        ([" "], ["Soil Type"], ["Crop Coefficient"], ["Slope"], ["Sun Exposure"], ...)



    example class.. don't know what we need yet.
    class CSVHeaderTemplates(object):
        zone_config = ["dv_type", "ad", "ds", "en", "df", "va", "vv"]
        zone_epa_config =

        defaults = {
            opcodes.zone: zone_config,
            "ZN": zone_config
        }
"""
import opcodes


class For1000(object):
    """
    1000 specific CSV headers and other relative data
    """
    zone_headers = [opcodes.zone, opcodes.serial_number, opcodes.description, opcodes.enabled, opcodes.type,
                    opcodes.latitude, opcodes.longitude, opcodes.design_flow, opcodes.status_code,
                    opcodes.crop_coefficient, opcodes.precipitation_rate,
                    opcodes.root_zone_working_water_storage, opcodes.distribution_uniformity,
                    opcodes.daily_moisture_balance, opcodes.solenoid_current,
                    opcodes.solenoid_voltage, opcodes.two_wire_drop, opcodes.run_time, opcodes.cycle_time,
                    opcodes.soak_cycle, opcodes.zone_program, opcodes.enable_et_runtime,
                    opcodes.water_strategy
                    ]


class For3200(object):
    """
    3200 specific CSV headers and other relative data
    """
    zone_headers = [opcodes.zone, opcodes.serial_number, opcodes.description, opcodes.enabled, opcodes.type,
                    opcodes.latitude, opcodes.longitude, opcodes.design_flow, opcodes.status_code,
                    opcodes.crop_coefficient, opcodes.precipitation_rate,
                    opcodes.root_zone_working_water_storage, opcodes.distribution_uniformity,
                    opcodes.daily_moisture_balance, opcodes.solenoid_current,
                    opcodes.solenoid_voltage, opcodes.two_wire_drop, opcodes.run_time, opcodes.cycle_time,
                    opcodes.soak_cycle, opcodes.zone_program, opcodes.enable_et_runtime,
                    opcodes.water_strategy
                    ]


class EPA(object):
    hydro_zone_headers = ([" "],
                          ["Soil Type"],
                          ["Crop Coefficient"],
                          ["Slope"],
                          ["Sun Exposure"],
                          ["Allowable Depletion"],
                          ["Root Depth"],
                          ["Precipitation rate"],
                          ["Distribution Uniformity"],
                          ["Basic Soil Intake Rate"],
                          ["Allowable Surface Accumulation"],
                          ["Cycle Time"],
                          ["Soak Time"]
                          )

    hydro_zone_headers_zn_var_names = ["ad", "soil_type", "kc", "slope", "sun_exposure", "allowable_depletion",
                                       "root_depth", "pr", "du"]

    hydro_zone_headers_zp_var_names = ["ct", "so"]
