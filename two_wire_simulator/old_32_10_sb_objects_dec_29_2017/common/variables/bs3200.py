__author__ = 'Tige'

import old_32_10_sb_objects_dec_29_2017.common


common.dictionary_for_address_ranges = {
    'ZN': range(1, 201),
    'PG': range(1, 100),
    'MV': range(1, 9),
    'FM': range(1, 9),
    'MS': range(1, 26),
    'TS': range(1, 9),
    'SW': range(1, 9)
}

dictionary_of_zone_headers_max_length = {
    'ZN': 3,
    'DS': 42,
    'ST': 8,
    'PG': ''
}

dictionary_of_moisture_headers_max_length = {
    'SN': 7,
    'DS': 42,
    'SS': 8,
    'VP': 4,
    'VD': 4
}

dictionary_of_flow_meter_headers_max_length = {
    'SN': 7,
    'DS': 42,
    'SS': 8,
    'VR': 4,
    'VG': '',
    'Stable': 9}

dictionary_of_master_valve_headers_max_length = {
    'SN': 7,
    'DS': 42,
    'SS': 8
}

dictionary_of_temperature_headers_max_length = {
    'SN': 7,
    'DS': 42,
    'SS': 8,
    'VD': 4}

dictionary_of_event_switches_headers_max_length = {
    'SN': 7,
    'DS': 42,
    'SS': 8,
    'State': 9
}

common.dictionary_for_device_type_headers_max_lengths = {
    'CN': 25,
    'ZN': dictionary_of_zone_headers_max_length,
    'MS': dictionary_of_moisture_headers_max_length,
    'FM': dictionary_of_flow_meter_headers_max_length,
    'MV': dictionary_of_master_valve_headers_max_length,
    'TS': dictionary_of_temperature_headers_max_length,
    'SW': dictionary_of_event_switches_headers_max_length
}