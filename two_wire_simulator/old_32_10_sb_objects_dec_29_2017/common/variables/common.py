__author__ = 'Tige'

from decimal import Decimal

DEFAULT_TIMEOUT = 120

# Even though this dictionary is initialized either in the common 1000 or the common 3200 variables,
# we need an empty place holder here in order to pass inheritance from the common variables to
# base_browser_common
dictionary_for_address_ranges = {}
dictionary_for_device_type_headers_max_lengths = {}

days_per_month = {
    1: 31,
    2: 28,
    3: 31,
    4: 30,
    5: 31,
    6: 30,
    7: 31,
    8: 31,
    9: 30,
    10: 31,
    11: 30,
    12: 31
}

dictionary_for_basemanager_address_pointers = {
    'p1.basemanagerrr.net': '104.130.159.113',
    'p2.basemanagerrr.net': '104.130.246.18',
    'p3.basemanagerrr.net': '104.130.242.33',
    '104.130.159.113': '104.130.159.113',
    '104.130.246.18': '104.130.246.18',
    '104.130.242.33': '104.130.242.33'
}

list_of_controller_types = [
    '10',
    '32'
]

# These colors are located in css property of the element, specifically the 'background-color' property
# the header is based on the word that is displayed in the device drop menu
list_of_device_types = [
    'CN',
    'ZN',
    'MS',
    'FM',
    'MV',
    'TS',
    'SW',
    'AR'
]

# Non-zone device specific web element headers
# the header is based on the word that is displayed in the header location on the web site

list_of_zone_headers = [
    'Zone',
    'Description',
    'Status',
    'Programs'
]

list_of_moisture_headers = [
    'Sensor',
    'Description',
    'Status',
    'Moisture',
    'Temp'
]

list_of_flow_meter_headers = [
    'Sensor',
    'Description',
    'Status',
    'Rate',
    'Usage',
    'Stable'
]

list_of_master_valve_headers = [
    'Sensor',
    'Description',
    'Status'
]

list_of_temperature_headers = [
    'Sensor',
    'Description',
    'Status',
    'Temperature'
]

list_of_event_switches_headers = [
    'Sensor',
    'Description',
    'Status',
    'State'
]

dictionary_for_status_colors = {
    'Okay': '#70bf41',
    'Watering': '#0155a4',
    'Done': '#70bf41',
    'Waiting': '#f6d328',
    'Soaking': '#57a7f9',
    'Paused': '#ff2f92',
    'Rain Delay': '#a349a4',
    'Error': '#c82605',
    'No Connection': '#a6aaa9',
    'Disabled': '#a6aaa9',
    'Off': '#ff9b34',
    'Unassigned': '',
    'Online': '#484848',            # This is for the controller text in the main menu, if black, it is online
    '#70bf41': 'Done',
    '#0155a4': 'Watering',
    '#f6d328': 'Waiting',
    '#57a7f9': 'Soaking',
    '#ff2f92': 'Paused',
    '#a349a41': 'Rain Delay',
    '#c82605': 'Error',
    '#a6aaa9': 'No Connection',
    '#ff9b34': 'Off',
    '': 'Unassigned'
}

dictionary_for_device_header_value_types = {
    'Programs': int,        # Zone device header
    'Moisture': float,      # Moisture Sensors header
    'Temp': float,          # Moisture Sensors header
    'Rate': float,          # Flow Meters header
    'Usage': '',            # TODO Flow Meters header
    'Stable': '',           # TODO Flow Meters header
    'Temperature': float    # Temperature Sensors header
}

dictionary_reverse_status_color_dictionary = {
    '#0155a4': 'Watering',
    '#70bf41': 'Done',
    '#f6d328': 'Waiting',
    '#57a7f9': 'Soaking',
    '#ff2f92': 'Paused',
    '#a349a4': 'Rain Delay',
    '#c82605': 'Error',
    '#a6aaa9': 'Disabled',
    '#c8c8c8': 'No Connection',
    '#ffff60': 'Off',
    '#484848': 'Online',
    '': 'Unassigned'
}

dictionary_web_id_for_device_status = {
    'Zones': 'zoneStatusColor'
}

dictionary_for_status_codes = {
    'BC': 'Bad Command',
    'BX': 'Budget Exceeded',
    'DN': 'Done Watering',
    'DS': 'Disabled',
    'ER': 'Error',
    'HF': 'High flow shutdown',
    'LA': 'Learn Flow Active',
    'LF': 'Low flow shutdown',
    'OK': 'Okay',
    'OW': 'Out of water',
    'PA': 'Paused',
    'RN': 'Running',
    'SO': 'Soaking',
    'UN': 'Unassigned',
    'WA': 'Waiting to water',
    'WT': 'Watering',
    'CN': 'Connected',
    'DC': 'Disconnected',
    'RG': 'Registering or not registered',
    'CG': 'Connecting',
    #'Rain Delay': 'Rain Delay',
    #'No connection': 'No connection',
    #'Off': 'Off',
    #'Unassigned': 'Unassigned'
}

dictionary_for_assign_device_buttons = {
    'zoneAssign': 'zone',
    'moistureAssign': 'moisture',
    'flowAssign': 'flowmeter',
    'masterAssign': 'mastervalve',
    'temperatureAssign': 'temp',
    'switchAssign': 'eventswitch'
}

dictionary_web_ids_for_main_menu_tabs = {
    'Maps': 'maptype',
    'Quick View': 'manage_view_l',
    'Programs': 'programs_view_l',
    'LiveView': 'remote_view_l',
    'Devices': 'zonesensor_view_l',
    'Water Sources': 'water_view_l'
}

# All current sub menu items supported
dictionary_web_ids_for_sub_menus = {
    'ZN': 'zones_view_l',
    'MS': 'moisture_view_l',
    'FM': 'flow_view_l',
    'MV': 'mv_view_l',
    'TS': 'temperature_view_l',
    'SW': 'switch_view_l',
    'PC': 'poc_view_l',
    'ML': 'mainline_view_l',
    'Company': 'global_view_l',
    'Site': 'company_view_l',
    'CN': 'controller_view_l'
}

# Web id's for the edit marker form input fields
dictionary_for_edit_marker_form_ids = {
    'Label': 'map_marker_label',
    'Name': 'map_marker_name',
    'Url': 'map_marker_url',
    'Description': 'map_marker_description',
    'Notes': 'map_marker_notes'
}

# the 'range(a, b)' function returns a range from a to b-1, i.e., range(1, 10) returns from 1 to 9
# a range starting with a 1 means that it requires at least 1 character.
dictionary_for_edit_marker_form_max_input_ranges = {
    'label': range(1, 3),
    'name': range(1, 25),
    'url': range(0, 2000),
    'description': range(0, 70),
    'notes': range(0, 125)
}

dictionary_for_map_edit_sub_menu_ids = {
    'Company': 'map-menu-allsites',
    'Current Site': 'map-menu-currentsite',
    'Current Controller': 'map-menu-controller',
    'Markers': 'map-menu-markers',
    'Sites': 'map-menu-markers-type-sites',
    'Controllers': 'map-menu-markers-type-controllers',
    'Zones': 'map-menu-markers-type-devices-ZN',
    'Moisture Sensors': 'map-menu-markers-type-devices-MS',
    'Temperature Sensors': 'map-menu-markers-type-devices-TS',
    'Master Valves': 'map-menu-markers-type-devices-MV',
    'Event Switches': 'map-menu-markers-type-devices-SW',
    'Flow Meters': 'map-menu-markers-type-devices-FM',
    'Custom Markers': 'map-menu-markers-type-custom'
}

dictionary_for_map_edit_sub_menu_back_ids = {
    'Company Markers': 'menu-marker-list',
    'Site Markers': 'menu-marker-list',
    'Controller Markers': 'menu-marker-list',
    'Sites': 'menu-marker-list-sites',
    'Controllers': 'menu-marker-list-controllers',
    'Custom Markers': 'menu-marker-list-custom',
    'Zones': 'menu-marker-list-devices',
    'Moisture Sensors': 'menu-marker-list-devices',
    'Temperature Sensors': 'menu-marker-list-devices',
    'Master Valves': 'menu-marker-list-devices',
    'Event Switches': 'menu-marker-list-devices',
    'Flow Meters': 'menu-marker-list-devices'
}

dictionary_for_edit_marker_color_ids = {
    'black': 'mapMarkerColor9',
    'white': 'mapMarkerColor142',
    'blue': 'mapMarkerColor130',
    'green': 'mapMarkerColor100',
    'orange': 'mapMarkerColor143',
    'purple': 'mapMarkerColor144'
}

# Device Type Dictionary
dictionary_for_device_assignment_options = {
    'Zones': 'Assign biCoder',
    'Moisture': 'Assign Sensors',
    'Flow': 'Assign Flow Meters',
    'Master Valves': 'Assign Master Valves',
    'Temperature': 'Assign Temp Sensors',
    'Event Switches': 'Assign Event Switches'
}

dictionary_for_convert_device_search_button_to_device_element_id = {
    'Assign biCoder': 'zoneAssign',
    'Assign Sensors': 'moistureAssign',
    'Assign Flow Meters': 'flowAssign',
    'Assign Master Valves': 'masterAssign',
    'Assign Temp Sensors': 'temperatureAssign',
    'Assign Event Switches': 'switchAssign'
}

dictionary_for_edit_map_marker_sub_menu_web_ids = {
    'Sites': 'map-menu-markers-type-sites',
    'Controllers': 'map-menu-markers-type-controllers',
    'Zones': 'map-menu-markers-type-devices-ZN',
    'Moisture Sensors': 'map-menu-markers-type-devices-MS',
    'Temperature Sensors': 'map-menu-markers-type-devices-TS',
    'Master Valves': 'map-menu-markers-type-devices-MV',
    'Event Switches': 'map-menu-markers-type-devices-ES',
    'Flow Meters': 'map-menu-markers-type-devices-FM',
    'Custom Markers': 'map-menu-markers-type-custom'
}

dictionary_for_device_type_headers = {
    'Controller': '',
    'Zones': list_of_zone_headers,
    'Moisture': list_of_moisture_headers,
    'Flow':  list_of_flow_meter_headers,
    'Master Valves': list_of_master_valve_headers,
    'Temperature': list_of_temperature_headers,
    'Event Switches': list_of_event_switches_headers
}

dictionary_of_device_type_with_key_value = {
    'Zones': 'ZN',
    'Moisture': 'MS',
    'Flow': 'FM',
    'Master Valves': 'MV',
    'Temperature': 'TS',
    'Event Switches': 'SW'
}

dictionary_for_map_marker_pin_key_values = {
    "Sites": "site",
    "Controllers": "CN",
    "Zones": "ZN",
    "Moisture Sensors": "MS",
    "Temperature Sensors": "TS",
    "Master Valves": "MV",
    "Event Switches": "SW",
    "Flow Meters": "FM",
    "Custom Markers": "custom"
}

map_marker_form_max_character_counts = {
    'Name': 25,
    'Url': 2000,
    'Description': 70,
    'Notes': 125
}

menu_rain_delay_set_footer_ids = {
    'Current Controller': 'rainDelayControllerfooter',
    'Current Site': 'rainDelaySitefooter',
    'Current Company': 'rainDelayGlobalfooter'
}

menu_rain_delay_end_date_footer_ids = {
    'Current Controller': 'rainDelayControllerfooterEndDate',
    'Current Site': 'rainDelaySitefooterEndDate',
    'Current Company': 'rainDelayGlobalfooterEndDate'
}

menu_rain_delay_input_ids = {
    'Current Controller': 'rainDelayControllerInput',
    'Current Site': 'rainDelaySiteInput',
    'Current Company': 'rainDelayGlobalInput'
}

menu_rain_delay_submit_button_ids = {
    'Current Controller': 'rainDelayControllerSubmit',
    'Current Site': 'rainDelaySiteSubmit',
    'Current Company': 'rainDelayGlobalSubmit'
}

menu_rain_delay_cancel_button_ids = {
    'Current Controller': 'rainDelayControllerCancel',
    'Current Site': 'rainDelaySiteCancel',
    'Current Company': 'rainDelayGlobalCancel'
}

list_of_valid_marker_types = [
    "Sites",
    "Controllers",
    "Zones",
    "Moisture Sensors",
    "Temperature Sensors",
    "Master Valves",
    "Event Switches",
    "Flow Meters",
    "Custom Markers"
]

dictionary_for_device_status_colors = {
    'Moisture': 'moisture',
    'Flow': 'flowmeter',
    'Master Valves': 'mastervalve',
    'Temperature': 'temp',
    'Event Switches': 'eventswitch'
}