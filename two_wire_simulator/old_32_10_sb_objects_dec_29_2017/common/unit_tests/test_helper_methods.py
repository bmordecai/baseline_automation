__author__ = 'Baseline'

import unittest
import mock
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
import old_32_10_sb_objects_dec_29_2017.common.variables.common as pvar


class TestHelperMethods(unittest.TestCase):
    """
    Unit test class for helper_methods.py
    """

    ########################################
    def setUp(self):
        """ Setting up for the test. """
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + self.shortDescription())

    ########################################
    def tearDown(self):
        """ Cleaning up after the test. """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_format_lat_long_fail_1(self):
        """ Tries running the method but doesn't pass in any parameters """
        expected_msg = "No lat/long values specified for 'format_lat_long'."
        with self.assertRaises(ValueError) as context:
            helper_methods.format_lat_long()
        self.assertEqual(expected_msg, context.exception.message)

    def test_format_lat_long_fail_2(self):
        """ Tries running the method but doesn't pass in any parameters """
        lat = 5
        expected_msg = "'_lat' parameter for 'format_lat_long' must be of type float, entered: {0}".format(type(lat))
        with self.assertRaises(TypeError) as context:
            helper_methods.format_lat_long(_lat=lat)
        self.assertEqual(expected_msg, context.exception.message)

    def test_format_lat_long_fail_3(self):
        """ Tries running the method but doesn't pass in any parameters """
        lon = 5
        expected_msg = "'_long' parameter for 'format_lat_long' must be of type float, entered: {0}".format(type(lon))
        with self.assertRaises(TypeError) as context:
            helper_methods.format_lat_long(_long=lon)
        self.assertEqual(expected_msg, context.exception.message)

    def test_format_lat_long_fail_4(self):
        """ Tries running the method but doesn't pass in any parameters """
        invalid_lat_and_long = 12.345
        expected_msg = "Invalid lat/long passed into 'format_lat_long'. Controller requires 6 decimal places."
        with self.assertRaises(ValueError) as context:
            helper_methods.format_lat_long(_lat=invalid_lat_and_long, _long=invalid_lat_and_long)
        self.assertEqual(expected_msg, context.exception.message)

    def test_verify_address_range_pass_1(self):
        """ Make sure the method runs through without error when a valid 'thing' parameter is passed in """
        pvar.dictionary_for_address_ranges['ZN'] = range(1, 101)
        with mock.patch('common.helper_methods.verify_value_is_int', return_value=True):
            helper_methods.verify_address_range(thing="ZN", range_number=11)

    def test_verify_address_range_fail_1(self):
        """ Parameter passed in isn't in the valid address range """
        expected_msg = "Incorrect thing: " + "Hello"
        with self.assertRaises(ValueError) as context:
            helper_methods.verify_address_range(thing="Hello", range_number=11)
        self.assertEqual(expected_msg, context.exception.message)

    def test_verify_address_range_fail_2(self):
        """ Pass in a range parameter that is not in the range dictionary """
        expected_msg = "invalid range for ZN needs to be between " + str(1) + " and " + str(100)
        pvar.dictionary_for_address_ranges['ZN'] = range(1, 101)
        with self.assertRaises(ValueError) as context:
            with mock.patch('common.helper_methods.verify_value_is_int', return_value=True):
                helper_methods.verify_address_range(thing="ZN", range_number=121)
        self.assertEqual(expected_msg, context.exception.message)

    def test_verify_value_is_string_fail_1(self):
        """ Pass in a string that is longer than the passed in length limit """
        string1 = "foo"
        string2 = 'bar'
        expected_msg = "String value exceeds the specified max length of 2. string1= [foo], string2= [bar]"
        with self.assertRaises(AssertionError) as context:
            helper_methods.verify_value_is_string(string1=string1, string2=string2, length=2)
        self.assertEqual(expected_msg, context.exception.message)

    def test_verify_value_is_string_fail_2(self):
        """ Received string does not equal the expected string """
        string1 = "foo"
        string2 = 'bar'
        expected_msg = "Received: " + string1 + ", Expected: " + string2
        with self.assertRaises(AssertionError) as context:
            helper_methods.verify_value_is_string(string1=string1, string2=string2)
        self.assertEqual(expected_msg, context.exception.message)

    def test_verify_value_is_float_pass_1(self):
        """ Pass in a float and verify that the method returns true """
        float1 = 5.0
        float2 = 5.0
        tolerance = 1.0
        self.assertTrue(helper_methods.verify_value_is_float(float1=float1, float2=float2, tolerance=tolerance))

    def test_verify_value_is_float_fail_1(self):
        """ Pass in the first float value and make it an invalid float """
        float1 = 'foo'
        float2 = 5.0
        tolerance = 1.0
        expected_msg = "Received value not a valid floating point number: " + str(float1)
        with self.assertRaises(AssertionError) as context:
            helper_methods.verify_value_is_float(float1=float1, float2=float2, tolerance=tolerance)
        self.assertEqual(expected_msg, context.exception.message)

    def test_verify_value_is_float_fail_2(self):
        """ Pass in the second float value and make it an invalid float """
        float1 = 5.0
        float2 = 'bar'
        tolerance = 1.0
        expected_msg = "Expected value not a valid floating point number: " + str(float2)
        with self.assertRaises(AssertionError) as context:
            helper_methods.verify_value_is_float(float1=float1, float2=float2, tolerance=tolerance)
        self.assertEqual(expected_msg, context.exception.message)

    def test_verify_value_is_float_fail_3(self):
        """ Pass in a tolerance and make it an invalid float """
        float1 = 5.0
        float2 = 5.0
        tolerance = 'spam'
        expected_msg = "Tolerance value not a valid floating point number: " + str(tolerance)
        with self.assertRaises(AssertionError) as context:
            helper_methods.verify_value_is_float(float1=float1, float2=float2, tolerance=tolerance)
        self.assertEqual(expected_msg, context.exception.message)

    def test_verify_value_is_float_fail_4(self):
        """ Pass in two floats with different signs and verify the error message """
        float1 = 5.0
        float2 = -5.0
        expected_msg = "Signs of the received and expected values are different.  Received: 5.0, Expected: -5.0"
        with self.assertRaises(AssertionError) as context:
            helper_methods.verify_value_is_float(float1=float1, float2=float2)
        self.assertEqual(expected_msg, context.exception.message)

    def test_verify_value_is_float_fail_5(self):
        """ Pass in two floats with different signs and verify the error message """
        float1 = 5.0
        float2 = 6.0
        tolerance = 10.0
        expected_msg = "Received value is greater than tolerance. Received: 5.0, Expected: 6.0, Tolerance: 10.0"
        with self.assertRaises(AssertionError) as context:
            helper_methods.verify_value_is_float(float1=float1, float2=float2, tolerance=tolerance)
        self.assertEqual(expected_msg, context.exception.message)

    def test_verify_value_is_int_pass_1(self):
        """ Both integers passed in are valid ints and the method returns true """
        integer1 = 1
        integer2 = 1
        self.assertTrue(helper_methods.verify_value_is_int(integer1=integer1, integer2=integer2))

    def test_verify_value_is_int_fail_1(self):
        """ The first parameter passed in isn't a valid integer and an AssertionError is raised """
        integer1 = 'foo'
        integer2 = 1
        expected_msg = "Received value not a valid integer: " + str(integer1)
        with self.assertRaises(AssertionError) as context:
            helper_methods.verify_value_is_int(integer1=integer1, integer2=integer2)
        self.assertEqual(expected_msg, context.exception.message)

    def test_verify_value_is_int_fail_2(self):
        """ The second parameter passed in isn't a valid integer and an AssertionError is raised """
        integer1 = 1
        integer2 = 'bar'
        expected_msg = "Expected value not a valid integer: " + str(integer2)
        with self.assertRaises(AssertionError) as context:
            helper_methods.verify_value_is_int(integer1=integer1, integer2=integer2)
        self.assertEqual(expected_msg, context.exception.message)

    def test_verify_value_is_int_fail_3(self):
        """ The two parameters do not match """
        integer1 = 1
        integer2 = 2
        expected_msg = "Received: " + str(integer1) + ", Expected: " + str(integer2)
        with self.assertRaises(AssertionError) as context:
            helper_methods.verify_value_is_int(integer1=integer1, integer2=integer2)
        self.assertEqual(expected_msg, context.exception.message)

    def test_print_test_passed_pass_1(self):
        """ The method runs without error """
        helper_methods.print_test_passed('Testing')

    def test_print_test_failed_pass_1(self):
        """ The method runs without error """
        helper_methods.print_test_failed('Testing')
