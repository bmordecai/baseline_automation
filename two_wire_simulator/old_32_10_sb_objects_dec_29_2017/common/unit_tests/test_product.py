__author__ = 'baseline'

import unittest
import mock
from old_32_10_sb_objects_dec_29_2017.common.helper_methods import truncate_float_value


class TestProductObject(unittest.TestCase):

    def setUp(self):
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_truncate_float_value_pass1(self):
        """ Verifies valid truncation down to 2 decimal places """
        val_in = 12.12345
        exp_out = 12.12
        val_returned = truncate_float_value(_float_value=val_in)
        self.assertEqual(first=exp_out, second=val_returned)

    def test_truncate_float_value_pass2(self):
        """ Verifies valid truncation down to 3 decimal places """
        val_in = 12.12345
        exp_out = 12.123
        val_returned = truncate_float_value(_float_value=val_in, number_of_decimal_places=3)
        self.assertEqual(first=exp_out, second=val_returned)

    def test_truncate_float_value_pass3(self):
        """ Verifies valid handling of an integer passed in """
        val_in = 12
        exp_out = 12.00
        val_returned = truncate_float_value(_float_value=val_in)
        self.assertEqual(first=exp_out, second=val_returned)

    # def test_truncate_float_value_pass4(self):
    #     """ Verifies valid handling of a string passed in"""
    #     val_in = "12"
    #     exp_out = 12.00
    #     val_returned = truncate_float_value(_float_value=val_in)
    #     self.assertEqual(first=exp_out, second=val_returned)


if __name__ == '__main__':
    unittest.main()
