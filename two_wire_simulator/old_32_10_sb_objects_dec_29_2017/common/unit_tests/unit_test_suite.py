__author__ = 'baseline'
"""
READ BEFORE MODIFICATION

In order for a unit test to be added to the unit test suite, proper 'setUp' and 'tearDown' methods must be present in
the unit test file. Refer to other unit test files for further examples.

Usage:

    -   To Add Additional Tests:
        1.  Add import statement below in related group, or create new group if there are no similar groups.
        2.  Add the imported 'Class' Name (the token following the 'import' statement below) to the 'test_list' list
            in similar fashion.

    -   To Run:
        1.  For Specific Tests:
            - Comment/Uncomment the 'Class' names that you would like to run tests for.
            - Right click this file and click the 'Debug 'test_objects'' option
        2.  For All Tests:
            - Make sure all 'Class' names in the 'test_list' list are un-commented.
            - Right click this file and click the 'Debug 'test_objects'' option
"""

import unittest

from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.test_product import TestProductObject

Devices.controller_lat = float(43.609768)
Devices.controller_long = float(-116.310569)

# OBJECTS

# Base Classes
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.base_classes.test_programs import TestProgramsObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.base_classes.test_ser import TestSerialObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.base_classes.test_poc import TestPOCObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.base_classes.test_web_driver import TestWebDriverObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.base_classes.test_devices import TestDeviceObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.base_classes.test_pg_start_stop_pause_cond import TestBaseStartStopPause
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.base_classes.test_bu_ser import TestBUSerialObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.base_classes.test_messages import TestMessages

# Base Manager

# Base Unit
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.baseunit.test_bu_helpers import TestBUHelpers

# Controller
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_zn import TestZoneObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_cn import TestControllerObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_fm import TestFlowMeterObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_ms import TestMoistureSensorObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_mv import TestMasterValveObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_ml import TestMainLineObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_poc_3200 import TestPOC3200Object
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_poc_1000 import TestPOC1000Object
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_pg_1000 import TestPG1000Object
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_pg_3200 import TestPG3200Object
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_pg_start_stop_pause_1000 import TestStartStopPause1000
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_pg_start_stop_pause_3200 import TestStartStopPause3200
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_sw import TestEventSwitchObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_ts import TestTemperatureSensorObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.controller.test_zp import TestZoneProgramObject

# Substation
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.substation.test_flow_bicoder import TestFlowBiCoderObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.substation.test_moisture_bicoder import TestMoistureBiCoderObject
# from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.substation.test_pump_bicoder import TestPumpBiCoderObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.substation.test_switch_bicoder import TestSwitchBiCoderObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.substation.test_temp_bicoder import TestTempBiCoderObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.substation.test_valve_bicoder import TestValveBiCoderObject


# Weather Based Watering
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.weather_based_watering.test_equations import TestEquationsObject
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.objects.weather_based_watering.test_ir_asa_resources import TestIrAsaResourcesObject

# USER CREDENTIALS
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.user_credentials.test_user_configuration import TestUserConfiguration

# Handlers
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.test_json_handler import TestJsonHandler
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.test_date_resource import TestDateResourceObject

# Helper Methods
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.test_helper_methods import TestHelperMethods

# Status Parser
from old_32_10_sb_objects_dec_29_2017.common.unit_tests.test_status_parser import TestKeyValue, TestKeyValues, TestValue

# List of object test case class names for execution
test_list = [
    TestProductObject,
    # Controller Objects
    TestZoneObject,
    TestControllerObject,
    TestFlowMeterObject,
    TestMoistureSensorObject,
    TestMasterValveObject,
    TestProgramsObject,
    TestSerialObject,
    TestWebDriverObject,
    TestPOCObject,
    TestMainLineObject,
    TestDeviceObject,
    TestPOC3200Object,
    TestPOC1000Object,
    TestPG1000Object,
    TestPG3200Object,
    TestStartStopPause1000,
    TestStartStopPause3200,
    TestBaseStartStopPause,

    # Substation Objects
    TestFlowBiCoderObject,
    TestMoistureBiCoderObject,
    TestSwitchBiCoderObject,
    TestTempBiCoderObject,
    TestValveBiCoderObject,

    # TestBUSerialObject,
    TestEventSwitchObject,
    TestTemperatureSensorObject,
    TestZoneProgramObject,

    # WeatherBasedWatering
    TestEquationsObject,
    TestIrAsaResourcesObject,

    # USER CREDENTIALS
    TestUserConfiguration,

    # Messages Module
    TestMessages,

    # Handlers
    TestJsonHandler,
    #TestDateResourceObject,

    # Helper Methods
    TestHelperMethods,

    # Status Parser
    TestValue,
    TestKeyValue,
    TestKeyValues,

    # BASE UNIT
    # TestBUHelpers, Can't be ran because of the File I/O involved, must individually run this test
    ]


# ---------------------------------------------------------------------------------------------------------------------
# For each object in the test_list, create a test suite for the object, and add the suite to the suite_list
suite_list = []
for test_case in test_list:

    # Create a test suite for each object
    test_suite = unittest.TestLoader().loadTestsFromTestCase(testCaseClass=test_case)

    # Append each object's test suite to a list
    suite_list.append(test_suite)

# ---------------------------------------------------------------------------------------------------------------------
# Create a suite wrapper containing all object's test suite
main_test_suite = unittest.TestSuite(tests=suite_list)

# ---------------------------------------------------------------------------------------------------------------------
# Create an executor to run each suite and all test cases contained in each suite.
test_runner = unittest.TextTestRunner(verbosity=0)
test_runner.run(test=main_test_suite)
