__author__ = 'baseline'

import unittest
import mock
import json_handler
import os
import runpy


class TestJsonHandler(unittest.TestCase):

    def setUp(self):
        self.file_name = 'test_json_file.json'
        self.data = {"TestFirstLevel": ["TestSecondLevel0", "TestSecondLevel1", "TestSecondLevel2"]}

        # Makes sure the Json starts out the same for each test
        json_handler.update_data(file_name=self.file_name, data=self.data)

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_get_data_pass_1(self):
        """ Test Get Data Pass Case 1: load data from a specified file """
        expected_data = {"TestFirstLevel": ["TestSecondLevel0", "TestSecondLevel1", "TestSecondLevel2"]}
        actual_data = json_handler.get_data(self.file_name)
        self.assertEqual(expected_data, actual_data)

    def test_update_data_pass_1(self):
        """ Test Update Data Pass Case 1: update the data in a specified Json file """
        new_data = {"NewData": ["1", "2", "3"]}
        json_handler.update_data(file_name=self.file_name, data=new_data)
        actual_data = json_handler.get_data(self.file_name)
        self.assertEqual(new_data, actual_data)

    def test_create_json_object_pass_1(self):
        """ Test Create Json Object Pass Case 1: create a json object and verify it is there """
        new_file = 'new_file.json'
        json_handler.create_json_object(new_file)
        self.assertTrue(os.path.exists(new_file))
        os.remove(new_file)

    def test_is_file_present_pass_1(self):
        """ Test Is File Present Pass Case 1: test to see if our json file exists, pass in a valid path """
        self.assertTrue(json_handler.is_file_present(self.file_name))

    def test_is_file_present_fail_1(self):
        """ Test Is File Present Fail Case 1: test to see if our json file exists, pass in an invalid path """
        self.assertFalse(json_handler.is_file_present('invalid/file/path'))

    def test_convert_string_to_json(self):
        """ Test Convert String To Json Pass Case 1: pass in a string and check to make sure it converts into a json """
        json_string = '{"TestFirstLevel": ["TestSecondLevel0", "TestSecondLevel1", "TestSecondLevel2"]}'
        json_handler.convert_string_to_json(json_string)

    def test_convert_string_to_json_pass_1(self):
        """ Test Convert String To Json Pass Case 1: pass in a string and check to make sure it converts into a json """
        json_string = '{"TestFirstLevel": ["TestSecondLevel0", "TestSecondLevel1", "TestSecondLevel2"]}'
        expected_json = {"TestFirstLevel": ["TestSecondLevel0", "TestSecondLevel1", "TestSecondLevel2"]}
        actual_json = json_handler.convert_string_to_json(json_string)
        self.assertEqual(expected_json, actual_json)

    def test_convert_json_to_string_pass_1(self):
        """ Test Convert Json To String Pass Case 1: pass in a json and check to make sure it converts into a string """
        json = {"TestFirstLevel": ["TestSecondLevel0", "TestSecondLevel1", "TestSecondLevel2"]}
        expected_string = u'{\n    "TestFirstLevel": [\n        "TestSecondLevel0", \n        "TestSecondLevel1", \n        "TestSecondLevel2"\n    ]\n}'
        actual_string = json_handler.convert_json_to_string(json)
        self.assertEqual(expected_string, actual_string)

    def test_main_pass_1(self):
        """ Test Main Pass Case 1: When the json_handler is executed, verify it's 'main' works """
        # Create a starting and expected dictionary that will turn into a json
        starting_data = {'Baseline': {'Versions': {'1-valve': {'major': 0}, '4-valve': {'minor': 0}}}}
        expected_data = {'Baseline': {'Versions': {'1-valve': {'major': 10}, '4-valve': {'minor': 7}}}}

        # Create a json file and populate it with the starting data
        json_handler.create_json_object('device_info.json')
        json_handler.update_data(file_name='device_info.json', data=starting_data)

        # This runs the json_handler as if it were the main module
        runpy.run_path(path_name='../../json_handler.py', run_name='__main__')

        # Retrieve the actual data from the json file after the statements have been executed
        actual_data = json_handler.get_data('device_info.json')
        self.assertEqual(expected_data, actual_data)

        # Remove the json file
        os.remove('device_info.json')



        #
        # with mock.patch('json_handler.get_data', return_value=starting_data):
        #     runpy.run_path(path_name='../../json_handler.py', run_name='__main__')
        #
        # actual_data = json_handler.get_data(file_name='device_info.json')
        #
        # # json_handler.get_data = method_reference
        #
        # self.assertEqual(expected_data, actual_data)
        #
        # os.remove('device_info.json')





if __name__ == '__main__':
    unittest.main()
