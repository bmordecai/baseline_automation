__author__ = 'bens'

import unittest

import mock
import os
from selenium import webdriver
from selenium.webdriver.support.event_firing_webdriver import EventFiringWebDriver as EFWD

import old_32_10_sb_objects_dec_29_2017.common.user_configuration as uc


username = "tige1"
password = "tige1"
url = "http://p2.basemanagerrr.net/login.php"


class TestUserConfiguration(unittest.TestCase):

    def setUp(self):
        """ Setting up for the test. """
        # Comment these out because the are not used for the test and "webdriver" is not user configuration attribute "web_driver" is
        #uc.webdriver.Firefox = mock.MagicMock(spec=webdriver)
        #uc.webdriver.Chrome = mock.MagicMock(spec=webdriver)
        #uc.webdriver.Ie = mock.MagicMock(spec=webdriver)
        #uc.EFWD = mock.MagicMock(spec=EFWD)
        # self.user_conf = uc.UserConfiguration(os.path.join('json_files', 'load_dv_set_ds_use_case.json'))

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_init_user_config_pass(self):
        """
        Tests the initialization of a new user configuration object. Only a file path
        is accepted as a parameter. Because we are testing over
        multiple web browsers on multiple viewing devices (i.e., tablet, desktop and
        mobile devices), there are setter methods provided for the user to specify
        which web browser and which viewing device they would like to test over.
        :return:
        """
        self.user_conf = uc.UserConfiguration(os.path.join('user_credentials/json_files',
                                                           'test_user_credentials_json.json'))
        self.assertEqual(self.user_conf.user_name, "BenAutoTests")
        self.assertEqual(self.user_conf.user_password, "BenTest@10259")
        self.assertEqual(self.user_conf.url, "https://p2.basemanager.net/login.php")
        self.assertEqual(self.user_conf.browser_view, None)
        self.assertEqual(self.user_conf.web_driver, None)
        self.assertEqual(self.user_conf.bacnet_server_ip, "10.11.12.35")

    def test_init_user_config_fail1(self):
        """ Verifies exception and message for attempt to use an unsupported web driver for Linux. """
        mock_platform = mock.MagicMock(return_value="Linux")
        uc.platform.system = mock_platform
        with self.assertRaises(ValueError) as context:
            self.user_conf = uc.UserConfiguration(os.path.join('user_credentials/json_files', 'test_user_credentials_json_1.json'))
        expected_message = "Browser to use for testing: ie is not currently supported for your operating system: " \
                           "Linux. Supported browsers for your operating system are: ['chrome', 'firefox']"
        self.assertEqual(first=expected_message, second=context.exception.message)

    def test_init_user_config_fail2(self):
        """ Verifies exception and message for attempt to use an unsupported web driver for Windows. """
        mock_platform = mock.MagicMock(return_value="Windows")
        uc.platform.system = mock_platform
        with self.assertRaises(ValueError) as context:
            self.user_conf = uc.UserConfiguration(os.path.join('user_credentials/json_files', 'test_user_credentials_json_2.json'))
        expected_message = "Browser to use for testing: opera is not currently supported for your operating system: " \
                           "Windows. Supported browsers for your operating system are: ['chrome', 'firefox', 'ie']"
        self.assertEqual(first=expected_message, second=context.exception.message)

    def test_init_user_config_fail3(self):
        """ Verifies exception and message for attempt to use an unsupported web driver for Darwin. """
        mock_platform = mock.MagicMock(return_value="Darwin")
        uc.platform.system = mock_platform
        with self.assertRaises(ValueError) as context:
            self.user_conf = uc.UserConfiguration(os.path.join('user_credentials/json_files', 'test_user_credentials_json_3.json'))
        expected_message = "Browser to use for testing: ie is not currently supported for your operating system: " \
                           "Darwin. Supported browsers for your operating system are: ['chrome', 'firefox']"
        self.assertEqual(first=expected_message, second=context.exception.message)

    def test_init_user_config_fail4(self):
        self.user_conf = uc.UserConfiguration(os.path.join('user_credentials/json_files',
                                                           'test_user_credentials_json_4.json'))

        self.assertEqual(self.user_conf.bacnet_server_ip, "10.11.12.27")

# if __name__ == '__main__':
#     unittest.main()
