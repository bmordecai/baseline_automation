import unittest
import mock
import serial
import status_parser

from old_32_10_sb_objects_dec_29_2017.common import helper_equations
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.flow_bicoder import FlowBicoder
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.fm import FlowMeter

__author__ = 'Eldin'


class TestFlowBiCoderObject(unittest.TestCase):
    """
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_flow_biCoder_object(self, _serial_number="TWF0001"):
        """ Creates a new Master Valve object for testing purposes """
        bicoder = FlowBicoder(_sn=_serial_number, _ser=self.mock_ser)

        # Added when equations for calculating flow rate/usage counts implemented.
        bicoder.fm_on_cn = mock.MagicMock(spec=FlowMeter)
        bicoder.fm_on_cn.kv = 5.0

        return bicoder

    #################################
    def test_set_flow_rate_pass1(self):
        """ Set Flow Rate Pass Case 1: Using Default vr value """
        bicoder = self.create_test_flow_biCoder_object()

        # Expected value is the vr value set at object Valve BiCoder object creation
        bicoder.set_flow_rate()

        # Calculated expected flow rate count being sent
        expected_value = helper_equations.calculate_flow_rate_count_from_gpm(gpm=bicoder.vr, kval=bicoder.fm_on_cn.kv)

        # vr value is set during this method and should equal the original value
        expected_command = "SET,{0}={1},{2}={3}".format(bicoder.id, bicoder.sn, opcodes.flow_rate_count, expected_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_flow_rate_pass2(self):
        """ Set Flow Rate Pass Case 2: Using 6 as passed in value for vr """
        bicoder = self.create_test_flow_biCoder_object()

        # Expected value is the vr value set at object Valve BiCoder object creation
        new_flow_rate = 6
        bicoder.set_flow_rate(_flow_rate=new_flow_rate)

        # Calculated expected flow rate count being sent
        expected_value = helper_equations.calculate_flow_rate_count_from_gpm(gpm=new_flow_rate,
                                                                             kval=bicoder.fm_on_cn.kv)

        # vr value is set during this method and should equal the original value
        expected_command = "SET,{0}={1},{2}={3}".format(bicoder.id, bicoder.sn, opcodes.flow_rate_count, expected_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_flow_rate_fail1(self):
        """ Set Flow Rate Fail Case 1: Pass String value as argument """
        bicoder = self.create_test_flow_biCoder_object()

        new_va_value = "b"
        with self.assertRaises(Exception) as context:
            bicoder.set_flow_rate(new_va_value)

        expected_message = "Failed trying to set Flow BiCoder ({0})'s Flow Rate. Invalid argument type, " \
                           "expected an int or float. Received type: {1}".format(
                               str(bicoder.sn),    # {0}
                               type(new_va_value)  # {1}
                           )
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_flow_rate_fail2(self):
        """ Set Flow Rate Fail Case 2: Failed communication with substation """
        bicoder = self.create_test_flow_biCoder_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        new_flow_rate = 5

        with self.assertRaises(Exception) as context:
            bicoder.set_flow_rate(_flow_rate=new_flow_rate)

        e_msg = "Exception occurred trying to set {0} ({1})'s 'Flow Rate' to: '{2}'".format(
            str(bicoder.ty), str(bicoder.sn), str(new_flow_rate))

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_water_usage_pass1(self):
        """ Set Water Usage Pass Case 1: Using Default vv value """
        bicoder = self.create_test_flow_biCoder_object()

        # Expected value is the _va value set at object Valve BiCoder object creation
        expected_value = bicoder.vg
        bicoder.set_water_usage()

        # Calculated expected flow rate count being sent
        expected_value = helper_equations.calculate_flow_usage_count_from_gallons(gal=bicoder.vg, kval=bicoder.fm_on_cn.kv)

        # The correct command should be built and passed into the send and wait for reply method
        expected_command = "SET,{0}={1},{2}={3}".format(bicoder.id, bicoder.sn, opcodes.total_usage, expected_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_usage_pass2(self):
        """ Set Water Usage Pass Case 2: Using 6 as passed in value for vv """
        bicoder = self.create_test_flow_biCoder_object()

        # Expected value is the vt value set at object Valve BiCoder object creation
        expected_value = 6
        bicoder.set_water_usage(_water_usage=expected_value)

        # Calculated expected flow rate count being sent
        expected_value = helper_equations.calculate_flow_usage_count_from_gallons(gal=bicoder.vg, kval=bicoder.fm_on_cn.kv)

        # The correct command should be built and passed into the send and wait for reply method
        expected_command = "SET,{0}={1},{2}={3}".format(bicoder.id, bicoder.sn, opcodes.total_usage, expected_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_usage_fail1(self):
        """ Set Water Usage Fail Case 1: Pass String value as argument """
        bicoder = self.create_test_flow_biCoder_object()

        new_vg_value = "b"
        with self.assertRaises(Exception) as context:
            bicoder.set_water_usage(new_vg_value)
        expected_message = "Failed trying to set {0} ({1})'s Water Usage. Invalid argument type, " \
                           "expected an int or float. Received type: {2}".format(
                                bicoder.ty,         # {0}
                                bicoder.sn,         # {1}
                                type(new_vg_value)  # {2}
                            )
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_water_usage_fail2(self):
        """ Set Water Usage Fail Case 2: Failed communication with substation """
        bicoder = self.create_test_flow_biCoder_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            bicoder.set_water_usage()
        e_msg = "Exception occurred trying to set {0} ({1})'s 'water usage' to: '{2}'".format(
            str(bicoder.ty), str(bicoder.sn), str(bicoder.vg))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_flow_rate_pass1(self):
        """ Verify Flow Rate Pass Case 1: Exception is not raised """
        bicoder = self.create_test_flow_biCoder_object()
        flow_rate_count = 5
        bicoder.vr = helper_equations.calculate_gpm_from_flow_rate_count(count=flow_rate_count, kval=bicoder.fm_on_cn.kv)
        test_pass = False

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.flow_rate_count, flow_rate_count))
        bicoder.data = mock_data

        test_pass = bicoder.verify_flow_rate(_data=mock_data)

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_flow_rate_fail1(self):
        """ Verify Flow Rate Fail Case 1: Value on substation does not match what is stored in the object """
        bicoder = self.create_test_flow_biCoder_object()
        incorrect_flow_rate_count = 10
        bicoder.vr = 50
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.flow_rate_count, incorrect_flow_rate_count))
        bicoder.data = mock_data

        calculated_flow_rate = helper_equations.calculate_gpm_from_flow_rate_count(count=incorrect_flow_rate_count,
                                                                                   kval=bicoder.fm_on_cn.kv)

        expected_message = "Unable to verify {0} ({1})'s flow rate. Received: {2}, Expected: {3}".format(
            str(bicoder.ty),        # {0}
            str(bicoder.sn),        # {1}
            calculated_flow_rate,   # {2} calculated value
            str(bicoder.vr)         # {3}
        )
        try:
            bicoder.verify_flow_rate(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_water_usage_pass1(self):
        """ Verify Water Usage Pass Case 1: Exception is not raised """
        bicoder = self.create_test_flow_biCoder_object()
        water_usage_count = 5
        bicoder.vg = helper_equations.calculate_gallons_from_flow_usage_count(count=water_usage_count,
                                                                              kval=bicoder.fm_on_cn.kv)
        test_pass = False

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.flow_usage_count, water_usage_count))
        bicoder.data = mock_data

        test_pass = bicoder.verify_water_usage(_data=mock_data)

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_water_usage_fail1(self):
        """ Verify Water Usage Fail Case 1: Value on substation does not match what is stored in the object"""
        bicoder = self.create_test_flow_biCoder_object()
        incorrect_water_usage_count = 10
        bicoder.vg = 50
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.flow_usage_count,
                                                                        incorrect_water_usage_count))
        bicoder.data = mock_data

        calcualted_water_usage = helper_equations.calculate_gallons_from_flow_usage_count(count=incorrect_water_usage_count,
                                                                                          kval=bicoder.fm_on_cn.kv)

        expected_message = "Unable to verify {0} ({1})'s water usage. Received: {2}, Expected: {3}".format(
            str(bicoder.ty),            # {0}
            str(bicoder.sn),            # {1}
            calcualted_water_usage,     # {2} calculated value with incorrect parameter
            str(bicoder.vg)             # {3}
        )
        try:
            bicoder.verify_water_usage(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Verify Who I Am Pass Case 1: Exception is not raised """
        bicoder = self.create_test_flow_biCoder_object()

        # Set up mock data to return the default values of our biCoder
        mock_data = status_parser.KeyValues("SN=TWF0001,{0}=0,{1}=0,VT=1.7,SS=OK".format(
            opcodes.flow_rate_count,
            opcodes.flow_usage_count
        ))

        # Mock the get data method so we don't attempt to send a packet to the controller
        bicoder.get_data = mock.MagicMock(return_value=mock_data)

        self.assertEqual(bicoder.verify_who_i_am(_expected_status=opcodes.okay), True)

    if __name__ == "__main__":
        unittest.main()
