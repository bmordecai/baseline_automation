__author__ = 'Brice "Ajo Grande" Garlick'

import unittest
import mock
import status_parser
import serial
from old_32_10_sb_objects_dec_29_2017.common.objects.baseunit.packet import BUSerialPacket
from old_32_10_sb_objects_dec_29_2017.common.objects.baseunit.bu_imports import *
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.ser import Ser


class TestSerialObject(unittest.TestCase):

    def setUp(self):
        """ Setting up for the test. """
        # Create a mock variable for a serial connection
        self.mock_serial_conn = mock.MagicMock(spec=serial.Serial)
        self.mock_serial_conn._isOpen = False

        self.mock_sendln = mock.MagicMock(side_effect=None)

        self.mock_waitln = mock.MagicMock(side_effect=None)

        self.mock_controller_reply = mock.MagicMock(side_effect=None)

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def create_test_serial_object(self, _diff_comport=None, _diff_eth_port=None):
        """ Creates a new serial object for use in a unit test """
        # Create a serial object for communicating over the comport
        if _diff_comport is not None:
            ser = Ser(comport=_diff_comport, eth_port=None)

        # Create a serial object for communicating over the ethernet port
        elif _diff_eth_port is not None:
            ser = Ser(comport=None, eth_port=_diff_eth_port)

        # Create a default serial object which defaults to communicating over the ethernet port.
        else:
            ser = Ser(comport=None, eth_port=None)

        ser.serial_conn = self.mock_serial_conn
        ser.unit_testing = True
        return ser

    def test_init_ser_connection_pass_1(self):
        """ Verifies valid serial comport connection init """
        # import serial base class module to mock the built in python package 'serial' import
        import old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.ser as ser_module

        # Create a mock for the serial built in package
        mock_serial_class = mock.MagicMock(spec=serial)
        mock_serial_class.side_effect = None

        # Overwrite the modules python 'serial' import with our mocked class
        ser_module.serial = mock_serial_class

        # Create ser object and assert it is closed on creation
        ser = self.create_test_serial_object(_diff_comport=1)
        self.assertFalse(ser.serial_conn._isOpen)

        # Init the connection and verify that it is open
        ser.init_ser_connection()
        self.assertTrue(ser.serial_conn._isOpen)

        # Close serial connection because we just opened a physical connection
        ser.close_connection()

    def test_init_ser_connection_pass_2(self):
        """ Verifies valid serial ethernet socket connection init """
        # import serial base class module to mock the built in python package 'serial' import
        import old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.ser as ser_module

        # Create a mock for the serial built in package
        mock_ethernet_serial_class = mock.MagicMock(spec=serial)
        mock_ethernet_serial_class.side_effect = None

        # Overwrite the modules python 'serial' import with our mocked class
        ser_module.eth_serial = mock_ethernet_serial_class

        # Create ser object and assert it is closed on creation
        ser = self.create_test_serial_object(_diff_eth_port="socket://10.11.12.240:10002")
        self.assertFalse(ser.serial_conn._isOpen)

        # Init the connection and verify that it is open
        ser.init_ser_connection()
        self.assertTrue(ser.serial_conn._isOpen)

        # Close serial connection because we just opened a physical connection
        ser.close_connection()

    def test_check_if_cn_connected_pass_1(self):
        """ Verifies no exception is raised when communication with controller is successful """
        ser = self.create_test_serial_object()

        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)
        ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        expected_command = "DO,EC=TR"
        ser.check_if_cn_connected()
        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    def test_check_if_cn_connected_fail_1(self):
        """ Verifies exception if communication with controller fails """
        ser = self.create_test_serial_object()

        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=Exception)
        ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        with self.assertRaises(Exception) as context:
            ser.check_if_cn_connected()
        e_msg = "Unable to communicate with controller during serial port initialization"
        self.assertEqual(first=e_msg, second=context.exception.message)

    def test_get_and_wait_for_reply_pass_1(self):
        """ Verifies sending a command and receiving the corresponding key-value pairs with no retry """
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        mock_data = "SN=TSD0001,ZN=1,EN=FA,DS=Test Zone 1"
        mock_read = mock.MagicMock(side_effect=[mock_data, '\r', '\n'])
        ser.serial_conn.read = mock_read

        cmd_to_send = "GET,ZN=1"
        r_key_values = ser.get_and_wait_for_reply(tosend=cmd_to_send)

        self.assertEqual(first=r_key_values, second=status_parser.KeyValues(mock_data))

    def test_get_and_wait_for_reply_pass_2(self):
        """ Verifies sending a command and receiving the corresponding key-value pairs with 1 retry """
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        mock_data = "SN=TSD0001,ZN=1,EN=FA,DS=Test Zone 1"
        mock_read = mock.MagicMock(side_effect=['B', 'C', '\r', '\n', mock_data, '\r', '\n'])
        ser.serial_conn.read = mock_read

        cmd_to_send = "GET,ZN=1"
        r_key_values = ser.get_and_wait_for_reply(tosend=cmd_to_send)

        self.assertEqual(first=r_key_values, second=status_parser.KeyValues(mock_data))

    def test_get_and_wait_for_reply_pass_3(self):
        """ Verifies sending a command and receiving the corresponding key-value pairs with 2 retries """
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        mock_data = "SN=TSD0001,ZN=1,EN=FA,DS=Test Zone 1"
        mock_read = mock.MagicMock(side_effect=['B', 'C', '\r', '\n', 'B', 'C', '\r', '\n', mock_data, '\r', '\n'])
        ser.serial_conn.read = mock_read

        cmd_to_send = "GET,ZN=1"
        r_key_values = ser.get_and_wait_for_reply(tosend=cmd_to_send)

        self.assertEqual(first=r_key_values, second=status_parser.KeyValues(mock_data))

    def test_get_and_wait_for_reply_fail_1(self):
        """ Verifies sending a command and failing after 3 retries and receiving a 'BC' response """
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        mock_read = mock.MagicMock(side_effect=['B', 'C', '\r', '\n', 'B', 'C', '\r', '\n', 'B', 'C', '\r', '\n'])
        ser.serial_conn.read = mock_read

        cmd_to_send = "GET,ZN=1"
        with self.assertRaises(AssertionError) as context:
            ser.get_and_wait_for_reply(tosend=cmd_to_send)

        expected_msg = "BC response received from controller"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_get_and_wait_for_reply_fail_2(self):
        """ Verifies sending a command and receiving a non 'BC' exception from get_key_value_pairs """
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        mock_read = mock.MagicMock(side_effect=['E', 'R', '\r', '\n'])
        ser.serial_conn.read = mock_read

        cmd_to_send = "GET,ZN=1"
        with self.assertRaises(AssertionError) as context:
            ser.get_and_wait_for_reply(tosend=cmd_to_send)

        expected_msg = "ER response received from controller"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_get_key_value_pairs_pass_1(self):
        """ Verifies valid key value pair string object is returned after receiving over serial connection """
        mock_key_value_data = "SS=DN,SN=TSD0001,BS=Ben Shewmake"
        ser = self.create_test_serial_object()
        mock_read = mock.MagicMock(side_effect=[mock_key_value_data, '\r', '\n'])
        ser.serial_conn.read = mock_read
        returned_key_values = ser.get_key_value_pairs()
        self.assertEqual(first=status_parser.KeyValues(mock_key_value_data), second=returned_key_values)

    def test_get_key_value_pairs_fail_1(self):
        """ Verifies valid exception handling after receiving a 'BC' from controller """
        ser = self.create_test_serial_object()
        mock_read = mock.MagicMock(side_effect=['B', 'C', '\r', '\n'])
        ser.serial_conn.read = mock_read
        with self.assertRaises(AssertionError) as context:
            ser.get_key_value_pairs()
        expected_msg = "BC response received from controller"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_get_key_value_pairs_fail_2(self):
        """ Verifies valid exception handling after receiving a 'ER' from controller """
        ser = self.create_test_serial_object()
        mock_read = mock.MagicMock(side_effect=['E', 'R', '\r', '\n'])
        ser.serial_conn.read = mock_read
        with self.assertRaises(AssertionError) as context:
            ser.get_key_value_pairs()
        expected_msg = "ER response received from controller"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_get_key_value_pairs_fail_3(self):
        """ Verifies valid exception handling after receiving a 'NM' from controller """
        ser = self.create_test_serial_object()
        mock_read = mock.MagicMock(side_effect=['N', 'M', '\r', '\n'])
        ser.serial_conn.read = mock_read
        with self.assertRaises(AssertionError) as context:
            ser.get_key_value_pairs()
        expected_msg = "NM No Message Found"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_get_key_value_pairs_fail_4(self):
        """ Verifies valid exception handling after receiving no response from controller """
        ser = self.create_test_serial_object()
        mock_read = mock.MagicMock(side_effect=['', '\r', '\n'])
        ser.serial_conn.read = mock_read
        with self.assertRaises(AssertionError) as context:
            ser.get_key_value_pairs()
        expected_msg = "No response received from controller"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_sendln(self):
        """ Test the sendln function -- this should call the underlying serial port with the input string followed by a
        carriage return / line feed """
        tosend = "This be a test"
        expected = tosend + "\r\n"
        ser = self.create_test_serial_object()
        ser.sendln(tosend)
        self.mock_serial_conn.write.assert_called_once_with(expected)

    def test_waitln(self):
        """ Read lines from the dummy serial port.   Looked-for item is the fourth line received -- verify that we
        did four calls to the serial port readline() before we got what we wanted """
        waitfor = "TARGET"
        ser = self.create_test_serial_object()
        mock_read_line = mock.MagicMock(side_effect=["FIRST", "SECOND", "THIRD", "TARGET", "AFTER"])
        ser.serial_conn.readline = mock_read_line
        ser.waitln(waitfor)
        self.assertEqual(len(mock_read_line.call_args_list), 4)    # readline() called 4 times

    def test_recvln_pass_1(self):
        """ Tests returning an expected string received over the serial port. """
        expected_str = "OK"
        ser = self.create_test_serial_object()
        mock_read = mock.MagicMock(side_effect=["O", "K", "\r", "\n"])
        ser.serial_conn.read = mock_read
        received_str = ser.recvln()
        self.assertEqual(first=expected_str, second=received_str)

    def test_recvln_pass_2(self):
        """ Tests returning 'None' when nothing is received over the serial port. """
        expected_str = None
        ser = self.create_test_serial_object()
        mock_read = mock.MagicMock(return_value="")
        ser.serial_conn.read = mock_read
        received_str = ser.recvln()
        self.assertEqual(first=expected_str, second=received_str)

    def test_recvln_pass_3(self):
        """ Tests returning 'None' when max characters have been read over the serial port. """
        expected_str = None
        ser = self.create_test_serial_object()
        mock_read = mock.MagicMock(return_value="K")
        ser.serial_conn.read = mock_read
        received_str = ser.recvln()
        self.assertEqual(first=expected_str, second=received_str)

    def test_recvln_pass_4(self):
        """ Tests returning 'None' when max characters have been read and the ignore variable is included """
        expected_str = None
        ser = self.create_test_serial_object()
        mock_read = mock.MagicMock(return_value="K")
        ser.serial_conn.read = mock_read
        received_str = ser.recvln(ignore="K")
        self.assertEqual(first=expected_str, second=received_str)

    def test_wait_pass_1(self):
        """ Tests returning a 0 when the first word to wait for was found. """
        expected_index = 0
        waitfors = ["OK", "ER", "BC", "NM"]
        ser = self.create_test_serial_object()
        mock_read = mock.MagicMock(side_effect=["O", "K", "\r", "\n"])
        ser.serial_conn.read = mock_read
        received_index = ser.wait(waitfors=waitfors)
        self.assertEqual(first=expected_index, second=received_index)

    def test_wait_pass_2(self):
        """ Tests returning a -2 when max characters have been read looking for expected response. """
        expected_index = -2
        waitfors = ["OK", "ER", "BC", "NM"]
        ser = self.create_test_serial_object()
        # Use return_value instead of side_effect here to return "K" every call, where side_effect only will return once
        # side_effect could be used, but a list of size max characters containing all K's needs to be it's input
        mock_read = mock.MagicMock(return_value="K")
        ser.serial_conn.read = mock_read
        received_index = ser.wait(waitfors=waitfors)
        self.assertEqual(first=expected_index, second=received_index)

    def test_wait_pass_3(self):
        """ Tests returning a -1 when no response was received from controller. """
        expected_index = -1
        waitfors = ["OK", "ER", "BC", "NM"]
        ser = self.create_test_serial_object()
        mock_read = mock.MagicMock(return_value="")
        ser.serial_conn.read = mock_read
        received_index = ser.wait(waitfors=waitfors)
        self.assertEqual(first=expected_index, second=received_index)

    def test_wait_pass_4(self):
        """ Tests returning a 1 when the second word to wait for was found. """
        expected_index = 1
        waitfors = ["OK", "ER", "BC", "NM"]
        ser = self.create_test_serial_object()
        mock_read = mock.MagicMock(side_effect=["E", "R", "\r", "\n"])
        ser.serial_conn.read = mock_read
        received_index = ser.wait(waitfors=waitfors)
        self.assertEqual(first=expected_index, second=received_index)

    def test_wait_pass_5(self):
        """ Tests returning the correct last index when the last word to wait for was found. """
        expected_index = 3
        waitfors = ["OK", "ER", "BC", "NM"]
        ser = self.create_test_serial_object()
        mock_read = mock.MagicMock(side_effect=["N", "M", "\r", "\n"])
        ser.serial_conn.read = mock_read
        received_index = ser.wait(waitfors=waitfors)
        self.assertEqual(first=expected_index, second=received_index)

    def test_send_and_wait_for_reply_pass_1(self):
        """ Successful communication through serial port to and from controller with no retries """
        test_pass = False
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        # Mock the controller's reply
        mock_read = mock.MagicMock(side_effect=["O", "K", "\r", "\n"])
        ser.serial_conn.read = mock_read

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                ser.send_and_wait_for_reply(tosend="FOO")

        # Catches an Assertion Error from above, meaning 'send_and_wait_for_reply' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    def test_send_and_wait_for_reply_pass_2(self):
        """ Successful communication through serial port to and from controller with 1 retry """
        test_pass = False
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        # Mock the controller's reply
        mock_read = mock.MagicMock(side_effect=["B", "C", "O", "K", "\r", "\n"])
        ser.serial_conn.read = mock_read

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                ser.send_and_wait_for_reply(tosend="FOO")

        # Catches an Assertion Error from above, meaning 'send_and_wait_for_reply' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    def test_send_and_wait_for_reply_pass_3(self):
        """ Successful communication through serial port to and from controller with 2 retries """
        test_pass = False
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        # Mock the controller's reply
        mock_read = mock.MagicMock(side_effect=["B", "C", "B", "C", "O", "K", "\r", "\n"])
        ser.serial_conn.read = mock_read

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                ser.send_and_wait_for_reply(tosend="FOO")

        # Catches an Assertion Error from above, meaning 'send_and_wait_for_reply' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    def test_send_and_wait_for_reply_fail_1(self):
        """ Failed attempt to communicate through serial port to and from controller after 3 retries receiving BCs """
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        # Mock the controller's reply
        mock_read = mock.MagicMock(side_effect=["B", "C", "B", "C", "B", "C", "\r", "\n"])
        ser.serial_conn.read = mock_read

        # .assertRaises raises an Assertion Error if an Exception is not raised
        with self.assertRaises(Exception) as context:
            ser.send_and_wait_for_reply(tosend="FOO")

        expected_message = "BC response received from controller"
        failed_message = "Expected 'AssertionError' to be raised after receiving 'BCs', no exception was raised."
        self.assertEqual(expected_message, context.exception.message, failed_message)

    def test_send_and_wait_for_reply_fail_2(self):
        """ Failed attempt to communicate through serial port to and from controller not receiving a BC """
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        # Mock the controller's reply
        mock_read = mock.MagicMock(side_effect=["E", "R", "\r", "\n"])
        ser.serial_conn.read = mock_read

        # .assertRaises raises an Assertion Error if an Exception is not raised
        with self.assertRaises(Exception) as context:
            ser.send_and_wait_for_reply(tosend="FOO")

        expected_message = "ER response received from controller"
        failed_message = "Expected 'AssertionError' to be raised after receiving 'ER', no exception was raised."
        self.assertEqual(expected_message, context.exception.message, failed_message)

    def test_controller_reply_pass_1(self):
        """ Verifies receiving 'OK' causes no exceptions. """
        test_pass = False

        ser = self.create_test_serial_object()

        # Mock the controller's reply
        mock_read = mock.MagicMock(side_effect=["O", "K", "\r", "\n"])
        ser.serial_conn.read = mock_read

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                ser.controller_reply()

        # Catches an Assertion Error from above, meaning 'send_and_wait_for_reply' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        failed_message = "Expected no Exception to be raised, Exception raised."
        self.assertEqual(test_pass, True, failed_message)

    def test_controller_reply_fail_1(self):
        """ Verifies correct exception is raised with expected wording when receiving a 'ER' """
        ser = self.create_test_serial_object()

        # Mock the controller's reply
        mock_read = mock.MagicMock(side_effect=["E", "R", "\r", "\n"])
        ser.serial_conn.read = mock_read

        # .assertRaises raises an Assertion Error if an Exception is not raised
        with self.assertRaises(AssertionError) as context:
            ser.controller_reply()

        expected_message = "ER response received from controller"
        failed_message = "Expected 'AssertionError' to be raised after receiving 'ER', no exception was raised."
        self.assertEqual(expected_message, context.exception.message, failed_message)
        
    def test_controller_reply_fail_2(self):
        """ Verifies correct exception is raised with expected wording when receiving a 'BC' """
        ser = self.create_test_serial_object()

        # Mock the controller's reply
        mock_read = mock.MagicMock(side_effect=["B", "C", "\r", "\n"])
        ser.serial_conn.read = mock_read

        # .assertRaises raises an Assertion Error if an Exception is not raised
        with self.assertRaises(AssertionError) as context:
            ser.controller_reply()

        expected_message = "BC response received from controller"
        failed_message = "Expected 'AssertionError' to be raised after receiving 'BC', no exception was raised."
        self.assertEqual(expected_message, context.exception.message, failed_message)        
        
    def test_controller_reply_fail_3(self):
        """ Verifies correct exception is raised with expected wording when receiving a 'NM' """
        ser = self.create_test_serial_object()

        # Mock the controller's reply
        mock_read = mock.MagicMock(side_effect=["N", "M", "\r", "\n"])
        ser.serial_conn.read = mock_read

        # .assertRaises raises an Assertion Error if an Exception is not raised
        with self.assertRaises(AssertionError) as context:
            ser.controller_reply()

        expected_message = "NM No Message Found"
        failed_message = "Expected 'AssertionError' to be raised after receiving 'NM', no exception was raised."
        self.assertEqual(expected_message, context.exception.message, failed_message)

    def test_controller_reply_fail_4(self):
        """ Verifies correct exception is raised with expected wording when no response received from controller """
        ser = self.create_test_serial_object()

        # Mock the controller's reply
        mock_read = mock.MagicMock(side_effect=[""])
        ser.serial_conn.read = mock_read

        # .assertRaises raises an Assertion Error if an Exception is not raised
        with self.assertRaises(AssertionError) as context:
            ser.controller_reply()

        expected_message = "No response received from controller"
        failed_message = "Expected 'AssertionError' to be raised after not receiving response, no exception was raised."
        self.assertEqual(expected_message, context.exception.message, failed_message)

    def test_controller_reply_fail_5(self):
        """ Verifies exception when max characters have been read from controller with no known response. """
        ser = self.create_test_serial_object()

        # Mock the controller's reply
        mock_read = mock.MagicMock(return_value="K")
        ser.serial_conn.read = mock_read

        # .assertRaises raises an Assertion Error if an Exception is not raised
        with self.assertRaises(AssertionError) as context:
            ser.controller_reply()

        expected_message = "Max characters reached while receiving response from controller"
        failed_message = "Expected 'AssertionError' to be raised after not receiving response, no exception was raised."
        self.assertEqual(expected_message, context.exception.message, failed_message)

    def test_flush_serial_pass_1(self):
        """ Verifies that if a serial is present, then the buffer is flushed """
        ser = self.create_test_serial_object()
        mock_flush_output = mock.MagicMock(side_effect=None)
        ser.serial_conn.flushOutput = mock_flush_output
        ser.flush_serial()
        mock_flush_output.assert_called_once_with()

    def test_flush_serial_fail_1(self):
        """ Verifies exception and message if a serial connection isn't present when trying to flush """
        ser = self.create_test_serial_object()
        mock_flush_output = mock.MagicMock(side_effect=None)
        ser.serial_conn.flushOutput = mock_flush_output
        ser.serial_conn = None
        with self.assertRaises(ValueError) as context:
            ser.flush_serial()
        expected_msg = "Error in attempting to flush serial: No serial connection was found"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_bu_wait_pass_1(self):
        """ Verifies a BUSerialPacket instance is returned during the 'bu_wait' method execution """
        address = 1
        command = BaseUnitCommands.COMPANY_ID

        # Create a BUSerialPacket to be returned when 'read' is called
        returned_packet = BUSerialPacket(_address_base=address, _command=command)

        # Create ser object
        ser = self.create_test_serial_object()

        # Mock the controller's reply
        # Here, we use a list to return a new value each time ser.serial_conn.read is called
        # first iteration, .read returns a packet
        # second iteration, .read returns a None (breaking out of the while loop)
        mock_read = mock.MagicMock(side_effect=[returned_packet.pack(), None])
        ser.serial_conn.read = mock_read

        # Execute method under test
        packet_received = ser.bu_wait()

        # Verify that a BUSerialPacket instance was returned
        self.assertEqual(first=type(packet_received), second=BUSerialPacket)

    def test_bu_send_and_wait_for_reply_pass_1(self):
        """ Test BaseUnit Send And Wait For Reply Pass Case 1: Status of returned packet is OK
        Create the test object
        Mock: send_ln
            Return: Nothing, we just want to pass over this method
        Mock the 'packet' object that is going to be passed into the method
        Mock the 'packet' object that is going to be returned by the 'bu_wait' method, and give it a 'status' variable
        Mock: bu_wait
            Return: The mocked packet containing a status
        Call the method
        Assert that the object that was mocked was also returned by the method
        """
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln
        ser.unit_testing = True

        new_packet = mock.MagicMock()
        new_packet.pack = mock.MagicMock()

        mock_packet = mock.MagicMock()
        mock_packet.status = BaseErrors.STATUS_OK

        ser.bu_wait = mock.MagicMock(return_value=mock_packet)

        return_packet = ser.bu_send_and_wait_for_reply(new_packet=new_packet)

        self.assertEqual(mock_packet, return_packet)

    def test_bu_send_and_wait_for_reply_pass_2(self):
        """ Test BaseUnit Send And Wait For Reply Pass Case 2: First packet has bad status, but second has an OK status
        Create the test object
        Mock: send_ln
            Return: Nothing, we just want to pass over this method
        Mock a 'packet' object that is going to be passed into the method
        Mock a 'packet' object that will return a bad status, making our method ask for another packet
        Mock a 'packet' object that will return a good status, making our method pass successfully
        Mock: bu_wait
            Return: On first call, mocked packet with bad status. On second call, mocked packet with good status
        Call the method and store the returned packet
        Compare the returned packet with the mocked packet with a good status
        """
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        new_packet = mock.MagicMock()
        new_packet.pack = mock.MagicMock()

        mock_packet_bad_status = mock.MagicMock()
        mock_packet_bad_status.status = BaseErrors.ILLEGAL_COMMAND_BASE

        mock_packet_good_status = mock.MagicMock()
        mock_packet_good_status.status = BaseErrors.STATUS_OK

        ser.bu_wait = mock.MagicMock(side_effect=[mock_packet_bad_status, mock_packet_good_status])

        return_packet = ser.bu_send_and_wait_for_reply(new_packet=new_packet)

        self.assertEqual(mock_packet_good_status, return_packet)

    def test_bu_send_and_wait_for_reply_fail_1(self):
        """ Test BaseUnit Send And Wait For Reply Fail Case 1: Throw an exception when the number of retries runs out
        Create the test object
        Mock: send_ln
            Return: Nothing, we just want to pass over this method
        Mock the 'packet' object that is going to be passed into the method
        Mock a 'packet' object that will return a bad status, making our method ask for another packet
        Mock: bu_wait
            Return: Return a packet with a bad status so that our method keeps retrying the call
        Create the expected message
        Assert that a ValueError will be raised when the method is called and then call the method
        Compare the error message raised by the method to the expected message
        """
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        new_packet = mock.MagicMock()
        new_packet.pack = mock.MagicMock()

        mock_packet_bad_status = mock.MagicMock()
        mock_packet_bad_status.status = BaseErrors.ILLEGAL_COMMAND_BASE

        ser.bu_wait = mock.MagicMock(return_value=mock_packet_bad_status)

        expected_message = "Error occurred waiting for reply from Base Unit: Error Code: {0}".format(
            mock_packet_bad_status.status)

        with self.assertRaises(ValueError) as context:
            ser.bu_send_and_wait_for_reply(new_packet=new_packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_bu_send_with_no_reply_pass_1(self):
        """ Test BaseUnit Send With No Reply Pass Case 1: Send the packet successfully
        Create the test object
        Mock: send_ln
            Return: Nothing, we just want to pass over this method
        Mock a 'packet' object that is going to be passed into the method
        Create a variable that holds the expected value that we expect 'sendln' will be called with
        Call the method
        Assert sendln was called with the command we expected
        """
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        expected_value = "Testing"

        new_packet = mock.MagicMock()
        new_packet.pack = mock.MagicMock(return_value=expected_value)

        ser.bu_send_with_no_reply(new_packet=new_packet)

        ser.sendln.assert_called_with(expected_value)  # TODO This works but find out why it is yellow

    def test_bu_send_with_no_reply_fail_1(self):
        """ Test BaseUnit Send With No Reply Fail Case 1: Packet fails to send and every retry is used
        Create the test object
        Mock: send_ln
            Return: Nothing, we just want to pass over this method
        Mock a 'packet' object that is going to be passed into the method
        Give the mocked 'send_ln' method a side effect that throws an exception everytime it is called
        Call the method and assert that an Exception will be raised
        """
        ser = self.create_test_serial_object()
        ser.sendln = self.mock_sendln

        new_packet = mock.MagicMock()
        new_packet.pack = mock.MagicMock()

        ser.sendln.side_effect = Exception

        with self.assertRaises(Exception):
            ser.bu_send_with_no_reply(new_packet=new_packet)

    # TODO: After figuring out retry process for Base Unit, uncomment below and fix where needed
    # def test_bu_send_and_wait_for_reply_pass_1(self):
    #     """ Successful communication through serial port to and from base unit with no retries """
    #     address = 1
    #     command = BaseUnitCommands.COMPANY_ID
    #
    #     # Create a BUSerialPacket to be returned when 'read' is called
    #     sent_packet = BUSerialPacket(_address_base=address, _command=command)
    #
    #     # Create ser object
    #     ser = self.create_test_serial_object()
    #
    #     # Mock the controller's reply
    #     mock_read = mock.MagicMock(return_value=sent_packet.pack())
    #     ser.serial_conn.read = mock_read
    #     ser.sendln = self.mock_sendln
    #
    #     # Execute method under test
    #     returned_packet = ser.bu_send_and_wait_for_reply(new_packet=sent_packet)
    #
    #     # Verify that a BUSerialPacket instance was returned
    #     self.assertEqual(first=type(returned_packet), second=BUSerialPacket)
    #
    # def test_bu_send_and_wait_for_reply_pass_2(self):
    #     """ Successful communication through serial port to and from base unit with 1 retry """
    #     address = 1
    #     command = BaseUnitCommands.COMPANY_ID
    #
    #     # Create a BUSerialPacket to be returned when 'read' is called
    #     sent_packet = BUSerialPacket(_address_base=address, _command=command)
    #
    #     # Create a BUSerialPacket with a good status
    #     good_status_packet1 = BUSerialPacket(_address_base=address, _command=command)
    #     good_status_packet1.status = BaseErrors.STATUS_OK
    #
    #     # Create a BUSerialPacket with an error status
    #     bad_status_packet1 = BUSerialPacket(_address_base=address, _command=command)
    #     bad_status_packet1.status = BaseErrors.CHECKSUM_BAD
    #
    #     read_returns = [bad_status_packet1.pack(), good_status_packet1.pack()]
    #
    #     # Create ser object
    #     ser = self.create_test_serial_object()
    #
    #     # Mock the controller's reply
    #     mock_read = mock.MagicMock(side_effect=read_returns)
    #     ser.serial_conn.read = mock_read
    #     ser.sendln = self.mock_sendln
    #
    #     # Execute method under test
    #     returned_packet = ser.bu_send_and_wait_for_reply(new_packet=sent_packet)
    #
    #     # Verify that a BUSerialPacket instance was returned
    #     self.assertEqual(first=type(returned_packet), second=BUSerialPacket)
    #     self.assertEqual(len(mock_read.call_args_list), 2)
    #
    # def test_bu_send_and_wait_for_reply_pass_3(self):
    #     """ Successful communication through serial port to and from base unit with 2 retries """
    #     address = 1
    #     command = BaseUnitCommands.COMPANY_ID
    #
    #     # Create a BUSerialPacket to be returned when 'read' is called
    #     sent_packet = BUSerialPacket(_address_base=address, _command=command)
    #
    #     # Create a BUSerialPacket with a good status
    #     good_status_packet1 = BUSerialPacket(_address_base=address, _command=command)
    #     good_status_packet1.status = BaseErrors.STATUS_OK
    #
    #     # Create a BUSerialPacket with an error status
    #     bad_status_packet1 = BUSerialPacket(_address_base=address, _command=command)
    #     bad_status_packet1.status = BaseErrors.CHECKSUM_BAD
    #
    #     bad_status_packet2 = BUSerialPacket(_address_base=address, _command=command)
    #     bad_status_packet2.status = BaseErrors.CHECKSUM_BAD
    #
    #     read_returns = [bad_status_packet1.pack(), bad_status_packet2.pack(), good_status_packet1.pack()]
    #
    #     # Create ser object
    #     ser = self.create_test_serial_object()
    #
    #     # Mock the controller's reply
    #     mock_read = mock.MagicMock(side_effect=read_returns)
    #     ser.serial_conn.read = mock_read
    #     ser.sendln = self.mock_sendln
    #
    #     # Execute method under test
    #     returned_packet = ser.bu_send_and_wait_for_reply(new_packet=sent_packet)
    #
    #     # Verify that a BUSerialPacket instance was returned
    #     self.assertEqual(first=type(returned_packet), second=BUSerialPacket)
    #     self.assertEqual(len(mock_read.call_args_list), 3)
    #
    # def test_bu_send_and_wait_for_reply_fail_1(self):
    #     """ Verifies exception raised after 3 failed attempts  """
    #     address = 1
    #     command = BaseUnitCommands.COMPANY_ID
    #
    #     # Create a BUSerialPacket to be returned when 'read' is called
    #     sent_packet = BUSerialPacket(_address_base=address, _command=command)
    #
    #     # Create a BUSerialPacket with a good status
    #     good_status_packet1 = BUSerialPacket(_address_base=address, _command=command)
    #     good_status_packet1.status = BaseErrors.STATUS_OK
    #
    #     # Create a BUSerialPacket with an error status
    #     bad_status_packet1 = BUSerialPacket(_address_base=address, _command=command)
    #     bad_status_packet1.status = BaseErrors.CHECKSUM_BAD
    #
    #     bad_status_packet2 = BUSerialPacket(_address_base=address, _command=command)
    #     bad_status_packet2.status = BaseErrors.CHECKSUM_BAD
    #
    #     read_returns = [bad_status_packet1.pack(), bad_status_packet2.pack(), good_status_packet1.pack()]
    #
    #     # Create ser object
    #     ser = self.create_test_serial_object()
    #
    #     # Mock the controller's reply
    #     mock_read = mock.MagicMock(side_effect=read_returns)
    #     ser.serial_conn.read = mock_read
    #     ser.sendln = self.mock_sendln
    #
    #     # Execute method under test
    #     returned_packet = ser.bu_send_and_wait_for_reply(new_packet=sent_packet)
    #
    #     # Verify that a BUSerialPacket instance was returned
    #     self.assertEqual(first=type(returned_packet), second=BUSerialPacket)
    #     self.assertEqual(len(mock_read.call_args_list), 3)

if __name__ == "__main__":
    unittest.main()
