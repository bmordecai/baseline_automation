__author__ = 'Brice "Ajo Grande" Garlick'

import unittest

import mock
import serial
import status_parser

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.poc import POC
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.fm import FlowMeter
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.mv import MasterValve
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import flow_meters, master_valves
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages


class TestPOCObject(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.309759

    Master Valve Starting lat & lng:
    latitude = 43.609768
    longitude = -116.310359
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        POC.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        flow_meters[3] = FlowMeter("FMD0001", 3)

        master_valves[2] = MasterValve("MVD0001", 2)

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # when these objects are created in this test, they carry over to any test after this one because they are
        # just pointers and not test variables specific to just THIS module.
        del master_valves[2]
        del flow_meters[3]

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_poc_object(self):
        """ Creates a new Master Valve object for testing purposes """
        test_poc = POC(_ad=1, _ds="Test POC 1", _fm=3, _mv=2, _en="TR", _fl=0, _hf=0, _hs="FA", _uf=0, _us="FA")

        return test_poc

    #################################
    def test_set_description_on_cn_pass1(self):
        """ Set Description On Controller Pass Case 1: Using Default _ds value """
        test_poc = self.create_test_poc_object()

        expected_value = test_poc.ds
        test_poc.set_description_on_cn()

        actual_value = test_poc.ds
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_description_on_cn_pass2(self):
        """ Set Description On Controller Pass Case 2: Setting new _ds value = 6.0 """
        test_poc = self.create_test_poc_object()

        expected_value = "Test Programs 2"
        test_poc.set_description_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = test_poc.ds
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_description_on_cn_pass3(self):
        """ Set Description On Controller Pass Case 3: Command with correct values sent to controller """
        test_poc = self.create_test_poc_object()

        ds_value = str(test_poc.ds)
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.description, str(ds_value))
        test_poc.set_description_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_description_on_cn_fail1(self):
        """ Set Description On Controller Fail Case 1: Failed communication with controller """
        test_poc = self.create_test_poc_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            test_poc.set_description_on_cn()
        e_msg = "Exception occurred trying to set POC {0}'s 'Description' to: '{1}' -> ".format(
                str(test_poc.ad), str(test_poc.ds))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_enable_state_pass1(self):
        """ Set Enable State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.enabled, opcodes.true)
        test_poc = self.create_test_poc_object()
        test_poc.set_enabled_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_state_pass2(self):
        """ Set Enable State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.enabled, opcodes.false)
        test_poc = self.create_test_poc_object()
        test_poc.set_enabled_state_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_state_fail1(self):
        """ Set Enable State On Controller Fail Case 1: Invalid State Argument """
        test_poc = self.create_test_poc_object()
        with self.assertRaises(ValueError) as context:
            test_poc.set_enabled_state_on_cn("Foo")
        e_msg = "Invalid state for POC 1 to set: Foo. Valid states are 'TR' or 'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_enable_state_fail2(self):
        """ Set Enable State On Controller Fail Case 2: Failed Communication With Controller """
        test_poc = self.create_test_poc_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            test_poc.set_enabled_state_on_cn("FA")
        e_msg = "Exception occurred trying to set POC 1's 'Enabled State' to: 'FA' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_target_flow_on_cn_pass1(self):
        """ Set Target Flow On Controller Pass Case 1: Using Default _fl value """
        test_poc = self.create_test_poc_object()

        expected_value = test_poc.fl
        test_poc.set_target_flow_on_cn()

        actual_value = test_poc.fl
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_target_flow_on_cn_pass2(self):
        """ Set Target Flow On Controller Pass Case 2: Setting new _fl value = 6.0 """
        test_poc = self.create_test_poc_object()

        expected_value = 6
        test_poc.set_target_flow_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = test_poc.fl
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_target_flow_on_cn_pass3(self):
        """ Set Target Flow On Controller Pass Case 3: Command with correct values sent to controller """
        test_poc = self.create_test_poc_object()

        fl_value = str(test_poc.fl)
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.target_flow, str(fl_value))
        test_poc.set_target_flow_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_target_flow_on_cn_fail1(self):
        """ Set Target Flow On Controller Fail Case 1: Failed communication with controller """
        test_poc = self.create_test_poc_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            test_poc.set_target_flow_on_cn()
        e_msg = "Exception occurred trying to set POC {0}'s 'Target Flow' to: '{1}' -> ".format(
                str(test_poc.ad), str(test_poc.fl))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_high_flow_limit_on_cn_pass1(self):
        """ Set High Flow Limit On Controller Pass Case 1: Using Default _hf value """
        test_poc = self.create_test_poc_object()

        expected_value = test_poc.hf
        test_poc.set_high_flow_limit_on_cn()

        actual_value = test_poc.hf
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_high_flow_limit_on_cn_pass2(self):
        """ Set High Flow Limit On Controller Pass Case 2: Setting new _hf value = 6.0 """
        test_poc = self.create_test_poc_object()

        expected_value = 6
        test_poc.set_high_flow_limit_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = test_poc.hf
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_high_flow_limit_on_cn_pass3(self):
        """ Set High Flow Limit On Controller Pass Case 3: Command with correct values sent to controller """
        test_poc = self.create_test_poc_object()

        hf_value = str(test_poc.hf)
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.high_flow_limit,
                                                      str(hf_value))
        test_poc.set_high_flow_limit_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_flow_limit_on_cn_fail1(self):
        """ Set High Flow Limit On Controller Fail Case 1: Failed communication with controller """
        test_poc = self.create_test_poc_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            test_poc.set_high_flow_limit_on_cn()
        e_msg = "Exception occurred trying to set POC {0}'s 'High Flow Limit' to: '{1}' -> ".format(
                str(test_poc.ad), str(test_poc.hf))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_shutdown_on_high_flow_state_on_cn_pass1(self):
        """ Set Shutdown on High Flow State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.shutdown_on_high_flow,
                                                      opcodes.false)
        test_poc = self.create_test_poc_object()
        test_poc.set_shutdown_on_high_flow_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_shutdown_on_high_flow_state_on_cn_pass2(self):
        """ Set Shutdown on High Flow State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.shutdown_on_high_flow,
                                                      opcodes.false)
        test_poc = self.create_test_poc_object()
        test_poc.set_shutdown_on_high_flow_state_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_shutdown_on_high_flow_state_on_cn_fail1(self):
        """ Set Shutdown on High Flow State On Controller Fail Case 1: Invalid State Argument """
        test_poc = self.create_test_poc_object()
        with self.assertRaises(ValueError) as context:
            test_poc.set_shutdown_on_high_flow_state_on_cn("Foo")
        e_msg = "Invalid state for POC 1 to set: Foo. Valid states are 'TR' or 'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_shutdown_on_high_flow_state_on_cn_fail2(self):
        """ Set Shutdown on High Flow State On Controller Fail Case 2: Failed Communication With Controller """
        test_poc = self.create_test_poc_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            test_poc.set_shutdown_on_high_flow_state_on_cn("FA")
        e_msg = "Exception occurred trying to set POC 1's 'Shutdown On High Flow' to: 'FA' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_unscheduled_flow_limit_on_cn_pass1(self):
        """ Set Unscheduled Flow Limit On Controller Pass Case 1: Using Default _uf value """
        test_poc = self.create_test_poc_object()

        expected_value = test_poc.uf
        test_poc.set_unscheduled_flow_limit_on_cn()

        actual_value = test_poc.uf
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_unscheduled_flow_limit_on_cn_pass2(self):
        """ Set Unscheduled Flow Limit On Controller Pass Case 2: Setting new _uf value = 6.0 """
        test_poc = self.create_test_poc_object()

        expected_value = 6
        test_poc.set_unscheduled_flow_limit_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = test_poc.uf
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_unscheduled_flow_limit_on_cn_pass3(self):
        """ Set Unscheduled Flow Limit On Controller Pass Case 3: Command with correct values sent to controller """
        test_poc = self.create_test_poc_object()

        uf_value = str(test_poc.uf)
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.unscheduled_flow_limit,
                                                      str(uf_value))
        test_poc.set_unscheduled_flow_limit_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_unscheduled_flow_limit_on_cn_fail1(self):
        """ Set Unscheduled Flow Limit On Controller Fail Case 1: Failed communication with controller """
        test_poc = self.create_test_poc_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            test_poc.set_unscheduled_flow_limit_on_cn()
        e_msg = "Exception occurred trying to set POC {0}'s 'Unscheduled Flow Limit' to: '{1}' -> ".format(
                str(test_poc.ad), str(test_poc.uf))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_shutdown_on_unscheduled_state_on_cn_pass1(self):
        """ Set Shutdown on High Flow State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.shutdown_on_unscheduled,
                                                      opcodes.false)
        test_poc = self.create_test_poc_object()
        test_poc.set_shutdown_on_unscheduled_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_shutdown_on_unscheduled_state_on_cn_pass2(self):
        """ Set Shutdown on High Flow State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.shutdown_on_unscheduled,
                                                      opcodes.false)
        test_poc = self.create_test_poc_object()
        test_poc.set_shutdown_on_unscheduled_state_on_cn("TR")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_shutdown_on_unscheduled_state_on_cn_fail1(self):
        """ Set Shutdown on High Flow State On Controller Fail Case 1: Invalid State Argument """
        test_poc = self.create_test_poc_object()
        with self.assertRaises(ValueError) as context:
            test_poc.set_shutdown_on_unscheduled_state_on_cn("Foo")
        e_msg = "Invalid state for POC 1 to 'SET' for 'Shutdown on Unscheduled': Foo. Valid states are 'TR' or 'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_shutdown_on_unscheduled_state_on_cn_fail2(self):
        """ Set Shutdown on High Flow State On Controller Fail Case 2: Failed Communication With Controller """
        test_poc = self.create_test_poc_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            test_poc.set_shutdown_on_unscheduled_state_on_cn("FA")
        e_msg = "Exception occurred trying to set POC 1's 'Shutdown On Unscheduled' state to: 'FA' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_flow_meter_on_cn_pass1(self):
        """ Set Flow Meter On Controller Pass Case 1: Passed in address of a present Flow Meter """
        test_poc = self.create_test_poc_object()

        expected_value = 3
        test_poc.set_flow_meter_on_cn(expected_value)

        this_obj = test_poc.flow_meter_objects[3]
        actual_value = this_obj.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_flow_meter_on_cn_pass2(self):
        """ Set Flow Meter On Controller Pass Case 2: Command with correct values sent to controller """
        test_poc = self.create_test_poc_object()

        this_obj = test_poc.flow_meter_objects[3]
        sn_value = this_obj.sn
        ad_value = this_obj.ad
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.flow_meter,
                                                      str(sn_value))
        test_poc.set_flow_meter_on_cn(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_flow_meter_on_cn_fail1(self):
        """ Set Flow Meter On Controller Fail Case 1: Pass an address of Flow Meter that is not present """
        test_poc = self.create_test_poc_object()

        new_ad_value = 2
        with self.assertRaises(Exception) as context:
            test_poc.set_flow_meter_on_cn(new_ad_value)
        expected_message = "Invalid Flow Meter address. Verify address exists in object json configuration and/or in " \
                           "current test. Received address: {0}, available addresses: [3]".format(str(new_ad_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_flow_meter_on_cn_fail2(self):
        """ Set Flow Meter On Controller Fail Case 2: Failed communication with controller """
        test_poc = self.create_test_poc_object()
        this_obj = test_poc.flow_meter_objects[3]
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            test_poc.set_flow_meter_on_cn(ad_value)
        e_msg = "Exception occurred trying to set POC {0}'s 'Flow Meter' to: '{1}' ({2})" \
                "-> ".format(str(test_poc.ad), str(this_obj.sn), str(this_obj.ad))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_master_valve_on_cn_pass1(self):
        """ Set Master Valve On Controller Pass Case 1: Passed in address of a present Master Valve """
        test_poc = self.create_test_poc_object()

        expected_value = 2
        test_poc.set_master_valve_on_cn(expected_value)

        this_obj = test_poc.master_valve_objects[2]
        actual_value = this_obj.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_master_valve_on_cn_pass2(self):
        """ Set Master Valve On Controller Pass Case 2: Command with correct values sent to controller """
        test_poc = self.create_test_poc_object()

        this_obj = test_poc.master_valve_objects[2]
        sn_value = this_obj.sn
        ad_value = this_obj.ad
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.master_valve,
                                                      str(sn_value))
        test_poc.set_master_valve_on_cn(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_master_valve_on_cn_fail1(self):
        """ Set Master Valve On Controller Fail Case 1: Pass an address of Master Valve that is not present """
        test_poc = self.create_test_poc_object()

        new_ad_value = 3
        with self.assertRaises(Exception) as context:
            test_poc.set_master_valve_on_cn(new_ad_value)
        expected_message = "Invalid Master Valve address. Verify address exists in object json configuration and/or in " \
                           "current test. Received address: {0}, available addresses: [2]".format(str(new_ad_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_master_valve_on_cn_fail2(self):
        """ Set Master Valve On Controller Fail Case 2: Failed communication with controller """
        test_poc = self.create_test_poc_object()
        this_obj = test_poc.master_valve_objects[2]
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            test_poc.set_master_valve_on_cn(ad_value)
        e_msg = "Exception occurred trying to set POC {0}'s 'Master Valve' to: '{1}' ({2})" \
                "-> ".format(str(test_poc.ad), str(this_obj.sn), str(this_obj.ad))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_message_on_cn_pass1(self):
        """ Set Message On Controller Pass Case 1: Correct command is sent to controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: message_to_set because it goes outside of the method
            Return: The expected message
        Call the method
        """
        pc = self.create_test_poc_object()

        expected_command = "SET,MG,{0}={1},{2}={3}".format(opcodes.point_of_connection, str(pc.ad), opcodes.status_code,
                                                           opcodes.budget_exceeded)
        with mock.patch('common.objects.base_classes.messages.message_to_set', return_value=expected_command):
            pc.set_message_on_cn(_status_code='OK')

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_message_on_cn_fail1(self):
        """ Set Message On Controller Controller Fail Case 1: Incorrect command is sent to the controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: message_to_set because it goes outside of the method
            Return: The expected message
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        """
        pc = self.create_test_poc_object()

        expected_command = "Exception caught in messages.set_message: "

        self.mock_send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_set', side_effect=Exception):
                pc.set_message_on_cn(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.assertEqual(expected_command, context.exception.message)

    #################################
    def test_get_data_pass1(self):
        """ Get Data From Controller Pass Case 1: Correct command is sent to controller """
        test_poc = self.create_test_poc_object()

        expected_command = "GET,{0}={1}".format(opcodes.point_of_connection, str(test_poc.ad))
        test_poc.get_data()
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_data_fail1(self):
        """ Get Data From Controller Fail Case 1: Failed communication with controller """
        test_poc = self.create_test_poc_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_get_and_wait_for_reply.side_effect = AssertionError

        expected_command = "GET,{0}={1}".format(opcodes.point_of_connection, str(test_poc.ad))
        with self.assertRaises(ValueError) as context:
            test_poc.get_data()
        e_msg = "Unable to get data for POC {0} using command: '{1}'. Exception raised: "\
                .format(str(test_poc.ad), expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_get_message_on_cn_pass1(self):
        """ Get Message On Controller Pass Case 1: Correct command is sent to controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: message_to_get because it goes outside of the method
            Return: The expected message
        Call the method
        """
        pc = self.create_test_poc_object()

        expected_command = "GET,MG,{0}={1},{2}={3}".format(opcodes.point_of_connection, str(pc.ad), opcodes.status_code,
                                                           opcodes.budget_exceeded)

        with mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: expected_command}):
            pc.get_message_on_cn(_status_code='OK')

        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_message_on_cn_fail1(self):
        """ Get Message On Controller Controller Fail Case 1: Incorrect command is sent to the controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: message_to_get because it goes outside of the method
            Return: The expected message
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        Assert the created command is the same as the command from the method
        """
        pg = self.create_test_poc_object()

        expected_command = "Exception caught in messages.get_message: "

        self.mock_get_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_get'):
                pg.get_message_on_cn(_status_code='OK')

        self.assertEqual(expected_command, context.exception.message)

    #################################
    def test_clear_message_on_cn_pass1(self):
        """ Clear Message On Controller Pass Case 1: Correct command is sent to controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: The expected message in a dictionary
        Call the method and verify what is was called with

        """
        pc = self.create_test_poc_object()

        expected_command = "GET,MG,{0}={1},{2}={3}".format(opcodes.mainline, str(pc.ad), opcodes.status_code,
                                                           opcodes.budget_exceeded)

        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value=expected_command), \
             mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: expected_command}):
            pc.clear_message_on_cn(_status_code='OK')
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_clear_message_on_cn_fail1(self):
        """ Clear Message On Controller Fail Case 1: Exception is thrown when sending command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: An Exception
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        """
        pc = self.create_test_poc_object()

        expected_msg = "Exception caught in messages.clear_message: "

        self.mock_send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_clear'):
                pc.clear_message_on_cn(_status_code=opcodes.budget_exceeded)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_clear_message_on_cn_fail2(self):
        """ Clear Message On Controller Fail Case 2: Exception is thrown when getting command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: The expected message
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method
        Cannot make an assertion since the exception is caught inside the method
        """
        pc = self.create_test_poc_object()

        expected_msg = "Message cleared, but was unable to verify if the message was gone."

        self.mock_get_and_wait_for_reply.side_effect = Exception

        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value=expected_msg):
            pc.clear_message_on_cn(_status_code=opcodes.budget_exceeded)

    #################################
    def test_clear_message_on_cn_fail3(self):
        """ Clear Message On Controller Fail Case 3: Exception is thrown when getting command, no message was found
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: The expected message
        Mock: Messages to get because it goes outside of the method
            Return: the expected message in a dictionary
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method
        Cannot make an assertion since the exception is caught inside the method
        """
        pc = self.create_test_poc_object()

        no_message_found = "NM No Message Found"

        self.mock_get_and_wait_for_reply.side_effect = Exception(no_message_found)

        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value=no_message_found), \
             mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: no_message_found}):
            pc.clear_message_on_cn(_status_code='OK')

    #################################
    def test_verify_description_on_cn_pass1(self):
        """ Verify Description On Controller Pass Case 1: Exception is not raised """
        test_poc = self.create_test_poc_object()
        test_poc.ds = 'Test POC 2'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test POC 2".format(opcodes.description))
        test_poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                test_poc.verify_description_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_description_on_cn_fail1(self):
        """ Verify Description On Controller Fail Case 1: Value on controller does not match what is
        stored in test_poc.ds """
        test_poc = self.create_test_poc_object()
        test_poc.ds = 'Test Program'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test".format(opcodes.description))
        test_poc.data = mock_data

        expected_message = "Unable to verify POC 1's 'Description'. Received: Test, Expected: Test Program"
        try:
            test_poc.verify_description_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_enabled_state_on_cn_pass1(self):
        """ Verify Enable State On Controller Pass Case 1: Exception is not raised """
        test_poc = self.create_test_poc_object()
        test_poc.en = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
        test_poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                test_poc.verify_enabled_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_enabled_state_on_cn_fail1(self):
        """ Verify Enable State On Controller Fail Case 1: Value on controller does not match what is
        stored in test_poc.en """
        test_poc = self.create_test_poc_object()
        test_poc.en = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
        test_poc.data = mock_data

        expected_message = "Unable verify POC 1's 'Enabled State'. Received: TR, Expected: Tr"
        try:
            test_poc.verify_enabled_state_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_target_flow_on_cn_pass1(self):
        """ Verify Target Flow On Controller Pass Case 1: Exception is not raised """
        test_poc = self.create_test_poc_object()
        test_poc.fl = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.target_flow))
        test_poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                test_poc.verify_target_flow_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_target_flow_on_cn_fail1(self):
        """ Verify Target Flow On Controller Fail Case 1: Value on controller does not match what is
        stored in test_poc.fl """
        test_poc = self.create_test_poc_object()
        test_poc.fl = 6
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.target_flow))
        test_poc.data = mock_data

        expected_message = "Unable verify POC 1's 'Target Flow'. Received: 5.0, Expected: 6"
        try:
            test_poc.verify_target_flow_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_high_flow_limit_on_cn_pass1(self):
        """ Verify High Flow Limit On Controller Pass Case 1: Exception is not raised """
        test_poc = self.create_test_poc_object()
        test_poc.hf = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.high_flow_limit))
        test_poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                test_poc.verify_high_flow_limit_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_high_flow_limit_on_cn_fail1(self):
        """ Verify High Flow Limit On Controller Fail Case 1: Value on controller does not match what is
        stored in test_poc.hf """
        test_poc = self.create_test_poc_object()
        test_poc.hf = 6
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5.0".format(opcodes.high_flow_limit))
        test_poc.data = mock_data

        expected_message = "Unable verify POC 1's 'High Flow Limit'. Received: 5.0, Expected: 6"
        try:
            test_poc.verify_high_flow_limit_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_shutdown_on_high_flow_on_cn_pass1(self):
        """ Verify Shutdown on High Flow State On Controller Pass Case 1: Exception is not raised """
        test_poc = self.create_test_poc_object()
        test_poc.hs = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.shutdown_on_high_flow))
        test_poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                test_poc.verify_shutdown_on_high_flow_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_shutdown_on_high_flow_on_cn_fail1(self):
        """ Verify Shutdown on High Flow State On Controller Fail Case 1: Value on controller does not match what is
        stored in test_poc.hs """
        test_poc = self.create_test_poc_object()
        test_poc.hs = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.shutdown_on_high_flow))
        test_poc.data = mock_data

        expected_message = "Unable verify POC 1's 'Shutdown on High Flow'. Received: TR, Expected: Tr"
        try:
            test_poc.verify_shutdown_on_high_flow_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_unscheduled_flow_limit_on_cn_pass1(self):
        """ Verify Unscheduled Flow Limit On Controller Pass Case 1: Exception is not raised """
        test_poc = self.create_test_poc_object()
        test_poc.uf = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.unscheduled_flow_limit))
        test_poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                test_poc.verify_unscheduled_flow_limit_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_unscheduled_flow_limit_on_cn_fail1(self):
        """ Verify Unscheduled Flow Limit On Controller Fail Case 1: Value on controller does not match what is
        stored in test_poc.uf """
        test_poc = self.create_test_poc_object()
        test_poc.uf = 6
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.unscheduled_flow_limit))
        test_poc.data = mock_data

        expected_message = "Unable verify POC 1's 'Unscheduled Flow Limit'. Received: 5.0, Expected: 6"
        try:
            test_poc.verify_unscheduled_flow_limit_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_shutdown_on_unscheduled_on_cn_pass1(self):
        """ Verify Shutdown on Unscheduled State On Controller Pass Case 1: Exception is not raised """
        test_poc = self.create_test_poc_object()
        test_poc.us = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.shutdown_on_unscheduled))
        test_poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                test_poc.verify_shutdown_on_unscheduled_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_shutdown_on_unscheduled_on_cn_fail1(self):
        """ Verify Shutdown on Unscheduled State On Controller Fail Case 1: Value on controller does not match what is
        stored in test_poc.us """
        test_poc = self.create_test_poc_object()
        test_poc.us = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.shutdown_on_unscheduled))
        test_poc.data = mock_data

        expected_message = "Unable verify POC 1's 'Shutdown on Unscheduled'. Received: TR, Expected: Tr"
        try:
            test_poc.verify_shutdown_on_unscheduled_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_flow_meter_on_cn_pass1(self):
        """ Verify Flow Meter On Controller Pass Case 1: Exception is not raised """
        test_poc = self.create_test_poc_object()
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=FMD0001".format(opcodes.flow_meter))
        test_poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                test_poc.verify_flow_meter_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_flow_meter_on_cn_fail1(self):
        """ Verify Flow Meter On Controller Fail Case 1: Value on controller does not match what is
        stored in serial number for the flow meter """
        test_poc = self.create_test_poc_object()
        this_obj = test_poc.flow_meter_objects[3]
        this_obj.sn = "FMD0005"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=FMD0001".format(opcodes.flow_meter))
        test_poc.data = mock_data

        expected_message = "Unable verify POC 1's 'Flow Meter'. Received: FMD0001, Expected: FMD0005"
        try:
            test_poc.verify_flow_meter_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_master_valve_on_cn_pass1(self):
        """ Verify Master Valve On Controller Pass Case 1: Exception is not raised """
        test_poc = self.create_test_poc_object()
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=MVD0001".format(opcodes.master_valve))
        test_poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                test_poc.verify_master_valve_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_master_valve_on_cn_fail1(self):
        """ Verify Master Valve On Controller Fail Case 1: Value on controller does not match what is
        stored in serial number for the master valve """
        test_poc = self.create_test_poc_object()
        this_obj = test_poc.master_valve_objects[2]
        this_obj.sn = "MVD0005"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=MVD0001".format(opcodes.master_valve))
        test_poc.data = mock_data

        expected_message = "Unable verify POC 1's 'Master Valve'. Received: MVD0001, Expected: MVD0005"
        try:
            test_poc.verify_master_valve_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_status_on_cn_pass1(self):
        """ Verify Status On Controller Pass Case 1: Exception is not raised """
        test_poc = self.create_test_poc_object()
        status_code = opcodes.running
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=RN".format(opcodes.status_code))
        test_poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                test_poc.verify_status_on_cn(status_code)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_status_on_cn_fail1(self):
        """ Verify Status On Controller Fail Case 1: Value on controller does not match what is
        stored in test_poc.ss """
        test_poc = self.create_test_poc_object()
        status_code = opcodes.connecting
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=DS".format(opcodes.status_code))
        test_poc.data = mock_data

        expected_message = "Unable verify POC 1's 'Status'. Received: DS, Expected: CG"
        try:
            test_poc.verify_status_on_cn(status_code)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_message_on_cn_pass1(self):
        """
        verify_message_on_cn pass case 1
        Create test point of connection object
        Mock: get_message_on_cn because it goes outside of the method
            Return: None so it does not fail
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id, and expected_date_time_from_cn
        Mock: build_message_string (note the [opcodes.message_id])
            Return: Four different strings, different than the ones used for expected_message_id, controller_date_time
            expected_date_time, and expected_message_text.
        Run: the test, and pass a status code in
        Compare: All the strings in each dictionary, and make sure it was stored properly in the objects build string
        """
        # Create test mainline object
        pc = self.create_test_poc_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Mock get message on cn
        mock_get_message_on_cn = mock.MagicMock(side_effect=['thing'])
        pc.get_message_on_cn = mock_get_message_on_cn

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, message_date])
        pc.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        pc.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Run test
        pc.verify_message_on_cn(_status_code='OK')

        # Compare actual strings to expected string
        self.assertEqual({'DT': '5/20/15 3:20:11', 'ID': 'blah2', 'TX': 'blah'}, pc.build_message_string)

    #################################
    def test_verify_message_on_cn_pass2(self):
        """ Verify message on controller Pass Case 2: Created DT doesn't match controller DT, but are within 60 minutes
        Create test point of connection object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the point of connection object to return the three strings
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: There is nothing to compare, we just want it to run through successfully since it just verifies
        """
        # Create test mainline object
        pc = self.create_test_poc_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError, but still within 60 minutes)
        wrong_message_dt = '5/20/15 3:59:11'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, wrong_message_dt])
        pc.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        pc.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Run method that will tested
        pc.verify_message_on_cn(_status_code=opcodes.bad_serial)

    #################################
    def test_verify_message_on_cn_fail1(self):
        """ Verify message on controller Fail Case 1: Created ID doesn't match controller ID, raise ValueError
        Create test point of connection object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id (which will be wrong so an
            ValueError is thrown), and expected_date_time_from_cn
        Manually fill up the build_message_string variables in the point of connection object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        pc = self.create_test_poc_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message ID (This is so we can purposefully force the ValueError)
        wrong_message_id = 'blah3'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, wrong_message_id, message_date])
        pc.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        pc.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "Created ID message did not match the ID received from the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           pc.build_message_string[opcodes.message_id],    # {0} The ID that was built
                           wrong_message_id                                # {1} The ID returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            pc.verify_message_on_cn(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_on_cn_fail2(self):
        """ Verify message on controller Fail Case 2: Created DT doesn't match controller DT, raise ValueError
        Create test point of connection object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the point of connection object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        pc = self.create_test_poc_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError)
        wrong_message_dt = '5/11/11 3:59:59'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, wrong_message_dt])
        pc.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        pc.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "The date and time of the message didn't match the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           pc.build_message_string[opcodes.date_time],  # {0} The DT that was built
                           wrong_message_dt                             # {1} The DT returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            pc.verify_message_on_cn(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_on_cn_fail3(self):
        """ Verify message on controller Fail Case 3: Created TX doesn't match controller TX, raise ValueError
        Create test point of connection object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the point of connection object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        pc = self.create_test_poc_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError, but still within 60 minutes)
        wrong_message_tx = 'blah3'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[wrong_message_tx, message_id, message_date])
        pc.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        pc.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "Created TX message did not match the TX received from the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           pc.build_message_string[opcodes.message_text],  # {0} The TX that was built
                           wrong_message_tx                                 # {1} The TX returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            pc.verify_message_on_cn(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    if __name__ == "__main__":
        unittest.main()
