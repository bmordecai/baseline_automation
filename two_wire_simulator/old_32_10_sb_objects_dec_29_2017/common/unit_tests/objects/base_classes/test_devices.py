__author__ = 'Brice "Ajo Grande" Garlick'

import unittest
import mock
import serial
import status_parser

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
import old_32_10_sb_objects_dec_29_2017.common.variables.common as common_vars
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages

class TestDeviceObject(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.309759

    Master Valve Starting lat & lng:
    latitude = 43.609768
    longitude = -116.310359
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        Devices.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        Devices.controller_lat = float(43.609768)
        Devices.controller_long = float(-116.309759)

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_device_object(self, dev_type="ZN", cn_type="32"):
        """ Creates a new Master Valve object for testing purposes """
        dev = Devices(_sn="TSD0001", _ds="Test Device 1", _ad=1, _la=float(43.609768), _lg=float(-116.309759), _ss="EN")

        dev.dv_type = dev_type
        dev.controller_type = cn_type

        return dev

    #################################
    def test_set_address_on_cn_pass1(self):
        """ Set Address On Controller Pass Case 1: Using Default _ad value, check that object variable _ad is set """
        dev = self.create_test_device_object()

        expected_value = dev.ad
        dev.set_address_on_cn()

        actual_value = dev.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_address_on_cn_pass2(self):
        """ Set Address On Controller Pass Case 2: Setting new _ad value = 2, check that object variable _ad is set """
        dev = self.create_test_device_object()

        expected_value = 2
        dev.set_address_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = dev.ad
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_address_on_cn_pass3(self):
        """ Set Address On Controller Pass Case 3: Command with correct values sent to controller """
        dev = self.create_test_device_object()

        ad_value = str(dev.ad)
        expected_command = "DO,AZ=TSD0001,NU={0}".format(str(ad_value))
        dev.set_address_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_address_on_cn_fail1(self):
        """ Set Address On Controller Fail Case 1: Failed communication with controller """
        dev = self.create_test_device_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            dev.set_address_on_cn()
        e_msg = "Exception occurred trying to address ZN 1 with serial: TSD0001 to the controller"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_description_on_cn_pass1(self):
        """ Set Description On Controller Pass Case 1: Using Default _ds value, check that object variable _ds is set"""
        dev = self.create_test_device_object()

        expected_value = dev.ds
        dev.set_description_on_cn()

        actual_value = dev.ds
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_description_on_cn_pass2(self):
        """ Set Description On Controller Pass Case 2: Setting new _ds value = "Test Programs 2", check that
        object variable _ds is set on a ZN"""
        dev = self.create_test_device_object()

        expected_value = "Test Device 2"
        dev.set_description_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = dev.ds
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_description_on_cn_pass3(self):
        """ Set Description On Controller Pass Case 3: Setting new _ds value = "Test Programs 2", check that
        object variable _ds is set on a 3200"""
        dev = self.create_test_device_object(dev_type="CN")

        expected_value = "Test Device 2"
        dev.set_description_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = dev.ds
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_description_on_cn_pass4(self):
        """ Set Description On Controller Pass Case 4: Setting new _ds value = "Test Programs 2", check that
        object variable _ds is set on a 1000"""
        dev = self.create_test_device_object(dev_type="CN", cn_type="10")

        expected_value = "Test Device 2"
        dev.set_description_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = dev.ds
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_description_on_cn_pass5(self):
        """ Set Description On Controller Pass Case 5: Command with correct values sent to controller with a ZN
        dev type"""
        dev = self.create_test_device_object()

        ds_value = str(dev.ds)
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.zone, opcodes.description, str(ds_value))
        dev.set_description_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_description_on_cn_pass6(self):
        """ Set Description On Controller Pass Case 6: Command with correct values sent to controller with an MV
        dev type"""
        dev = self.create_test_device_object(dev_type="MV")

        ds_value = str(dev.ds)
        expected_command = "SET,{0}=TSD0001,{1}={2}".format(opcodes.master_valve, opcodes.description, str(ds_value))
        dev.set_description_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_description_on_cn_fail1(self):
        """ Set Description On Controller Fail Case 1: Failed communication with controller """
        dev = self.create_test_device_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            dev.set_description_on_cn()
        e_msg = "Exception occurred trying to set ZN's ds to: {0}".format(dev.ds)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_description_on_cn_fail2(self):
        """ Set Description On Controller Fail Case 2: Description is too long for controller type 3200 """
        dev = self.create_test_device_object(dev_type="CN")

        new_ds_value = "12345678901234567890123456789012345678901234"

        with self.assertRaises(Exception) as context:
            dev.set_description_on_cn(new_ds_value)
        e_msg = "Attempted to set a description to 3200 that is too long: {0}".format(str(len(new_ds_value)))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_description_on_cn_fail3(self):
        """ Set Description On Controller Fail Case 3: Description is too long for controller type 1000 """
        dev = self.create_test_device_object(dev_type="CN", cn_type="10")

        new_ds_value = "12345678901234567890123456789012"

        with self.assertRaises(Exception) as context:
            dev.set_description_on_cn(new_ds_value)
        e_msg = "Attempted to set a description to 1000 that is too long: {0}".format(str(len(new_ds_value)))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_description_on_cn_fail4(self):
        """ Set Description On Controller Fail Case 4: Incorrect dev type """
        dev = self.create_test_device_object(dev_type="BG")

        with self.assertRaises(Exception) as context:
            dev.set_description_on_cn()
        e_msg = "Invalid device type: BG entered while trying to send ds to cn"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_latitude_on_cn_pass1(self):
        """ Set Latitude On Controller Pass Case 1: Using Default _la value, check that object variable _la is set"""
        dev = self.create_test_device_object()

        expected_value = dev.la
        dev.set_latitude_on_cn()

        actual_value = dev.la
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_latitude_on_cn_pass2(self):
        """ Set Description On Controller Pass Case 2: Setting new _la value = 5.000001, check that
        object variable _la is set on a ZN"""
        dev = self.create_test_device_object()

        expected_value = 5.000001
        dev.set_latitude_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = dev.la
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_latitude_on_cn_pass3(self):
        """ Set Latitude On Controller Pass Case 3: Command with correct values sent to controller with a ZN
        dev type"""
        dev = self.create_test_device_object()

        la_value = 5.000001
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.zone, opcodes.latitude, str(la_value))
        dev.set_latitude_on_cn(la_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_latitude_on_cn_pass4(self):
        """ Set Latitude On Controller Pass Case 4: Command with correct values sent to controller with an MV
        dev type"""
        dev = self.create_test_device_object(dev_type="MV")

        la_value = 5.000001
        expected_command = "SET,{0}=TSD0001,{1}={2}".format(opcodes.master_valve, opcodes.latitude, str(la_value))
        dev.set_latitude_on_cn(la_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_latitude_on_cn_fail1(self):
        """ Set Description On Controller Fail Case 1: Failed communication with controller """
        dev = self.create_test_device_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            dev.set_latitude_on_cn()
        e_msg = "Exception occurred trying to set ZN's latitude to: {0}".format(str(dev.la))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_latitude_on_cn_fail2(self):
        """ Set Latitude On Controller Fail Case 2: Incorrect latitude format passed as argument """
        dev = self.create_test_device_object()

        new_la_value = 6
        placeholder = helper_methods.format_lat_long
        self.mock_format_lat_long = mock.MagicMock(sideeffect=Exception)
        helper_methods.format_lat_long = self.mock_format_lat_long
        self.mock_format_lat_long.side_effect = Exception

        with self.assertRaises(Exception) as context:
            dev.set_latitude_on_cn(new_la_value)
        e_msg = "Exception occurred trying to set latitude for device: ZN\n"
        helper_methods.format_lat_long = placeholder
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_latitude_on_cn_fail3(self):
        """ Set Latitude On Controller Fail Case 3: Incorrect dev type """
        dev = self.create_test_device_object(dev_type="BG")

        with self.assertRaises(Exception) as context:
            dev.set_latitude_on_cn()
        e_msg = "Invalid device type: BG entered while trying to send latitude to cn"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_longitude_on_cn_pass1(self):
        """ Set Longitude On Controller Pass Case 1: Using Default _lg value, check that object variable _lg is set"""
        dev = self.create_test_device_object()

        expected_value = dev.lg
        dev.set_longitude_on_cn()

        actual_value = dev.lg
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_longitude_on_cn_pass2(self):
        """ Set Longitude On Controller Pass Case 2: Setting new _lg value = 5.000001, check that
        object variable _lg is set on a ZN"""
        dev = self.create_test_device_object()

        expected_value = 5.000001
        dev.set_longitude_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = dev.lg
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_longitude_on_cn_pass3(self):
        """ Set Longitude On Controller Pass Case 3: Command with correct values sent to controller with a ZN
        dev type"""
        dev = self.create_test_device_object()

        lg_value = 5.000001
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.zone, opcodes.longitude, str(lg_value))
        dev.set_longitude_on_cn(lg_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_longitude_on_cn_pass4(self):
        """ Set Longitude On Controller Pass Case 4: Command with correct values sent to controller with an MV
        dev type"""
        dev = self.create_test_device_object(dev_type="MV")

        la_value = 5.000001
        expected_command = "SET,{0}=TSD0001,{1}={2}".format(opcodes.master_valve, opcodes.longitude, str(la_value))
        dev.set_longitude_on_cn(la_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_longitude_on_cn_fail1(self):
        """ Set Longitude On Controller Fail Case 1: Failed communication with controller """
        dev = self.create_test_device_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            dev.set_longitude_on_cn()
        e_msg = "Exception occurred trying to set ZN's lg to: {0}".format(str(dev.lg))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_longitude_on_cn_fail2(self):
        """ Set Longitude On Controller Fail Case 2: Incorrect longitude format passed as argument """
        dev = self.create_test_device_object()

        new_lg_value = 6
        placeholder = helper_methods.format_lat_long
        self.mock_format_lat_long = mock.MagicMock(sideeffect=Exception)
        helper_methods.format_lat_long = self.mock_format_lat_long
        self.mock_format_lat_long.side_effect = Exception

        with self.assertRaises(Exception) as context:
            dev.set_longitude_on_cn(new_lg_value)
        e_msg = "Exception occurred trying to set longitude for device: ZN\n"
        helper_methods.format_lat_long = placeholder
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_longitude_on_cn_fail3(self):
        """ Set Longitude On Controller Fail Case 3: Incorrect dev type """
        dev = self.create_test_device_object(dev_type="BG")

        with self.assertRaises(Exception) as context:
            dev.set_longitude_on_cn()
        e_msg = "Invalid device type: BG entered while trying to send longitude to cn"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_message_on_cn_pass1(self):
        """
        set_message_on_cn pass case 1
        Create: Test devices object
        Mock: message_to_set method
        Return: None, so it does not fail
        Mock: ser.send_and_wait_for_reply
        Return: None, so it does not fail
        Run: Test, should not return an error if it passes
        """
        # Create test devices object
        dev = self.create_test_device_object()

        # Mock serial send and wait for reply method
        self.mock_send_and_wait_for_reply.side_effect = None

        # Run test
        with mock.patch('common.objects.base_classes.messages.message_to_set'):
            dev.set_message_on_cn(_status_code='OK')

    #################################
    def test_set_message_on_cn_fail1(self):
        """
        set_message_on_cn fail case 1
        Create: test devices object
        Store: Expected error message
        Mock: message_to_set
        Return: Exception, so the test fails
        Run: Test
        Compare: Expected error message to actual error message
        """
        # Create test devices object
        dev = self.create_test_device_object()

        # Expected error message
        e_msg = "Exception caught in messages.set_message: "

        # Run test
        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_set', side_effect=Exception):
                dev.set_message_on_cn(_status_code='OK')

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_get_data_pass1(self):
        """ Get Data From Controller Pass Case 1: Correct command is sent to controller with ZN dev type """
        dev = self.create_test_device_object()

        expected_command = "GET,{0}={1}".format(opcodes.zone, str(dev.ad))
        dev.get_data()
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_data_pass2(self):
        """ Get Data From Controller Pass Case 2: Correct command is sent to controller with CN dev type """
        dev = self.create_test_device_object(dev_type="CN")

        expected_command = "GET,{0}".format(opcodes.controller)
        dev.get_data()
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_data_pass3(self):
        """ Get Data From Controller Pass Case 3: Correct command is sent to controller with MV dev type """
        dev = self.create_test_device_object(dev_type="MV")

        expected_command = "GET,{0}=TSD0001".format(opcodes.master_valve)
        dev.get_data()
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_data_fail1(self):
        """ Get Data From Controller Fail Case 1: Failed communication with controller """
        dev = self.create_test_device_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_get_and_wait_for_reply.side_effect = AssertionError

        expected_command = "GET,{0}={1}".format(opcodes.zone, str(dev.ad))
        with self.assertRaises(ValueError) as context:
            dev.get_data()
        e_msg = "Unable to get data for device: '{0}' using command: '{1}'. Exception raised: "\
                .format(str(dev.dv_type), expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_get_message_on_cn_pass1(self):
        """
        get_message_on_cn pass case 1
        Create: Test devices object
        Mock: message_to_get method
        Return: None, so it does not fail
        Mock: ser.get_and_wait_for_reply
        Return: None, so it does not fail
        Run: Test, should not return an error if it passes
        """
        # Create test devices object
        dev = self.create_test_device_object()

        # Mock serial get and wait for reply
        self.mock_get_and_wait_for_reply.side_effect = None

        # Run test
        with mock.patch('common.objects.base_classes.messages.message_to_get'):
            dev.get_message_on_cn(_status_code='OK')

    #################################
    def test_get_message_on_cn_fail1(self):
        """
        get_message_on_cn fail case 1
        Create: test devices object
        Store: Expected error message
        Mock: messages_to_get
        Return: Exception, so the test will fail
        Run: test, expect an exception
        Compare: Actual error message to expected error message
        """
        # Create test devices object
        dev = self.create_test_device_object()

        # Store expected error message
        e_msg = "Exception caught in messages.get_message: "

        # Run test, expect an exception
        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_get', side_effect=Exception):
                dev.get_message_on_cn(_status_code='OK')

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_clear_message_on_cn_pass1(self):
        """
        clear_message_on_cn pass case 1:
        Create: Test devices object
        Mock: messages_to_clear
        Return: None, so it does not fail
        Mock: ser.send_and_wait_for_reply
        Return: None, so it does not fail
        Mock: messages_to_get
        Return: None, so it does not fail
        Mock: ser.get_and_wait_for_reply
        Return: Exception, so the "NM No Message Found" error message is triggered
        Run: test
        """
        # Todo: does this method need an exception to catch the error message?
        # Create test devices object
        dev = self.create_test_device_object()

        # Mock send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = None

        # Mock get and wait for reply
        self.mock_get_and_wait_for_reply.side_effect = Exception("NM No Message Found")

        # Run test, expect an exception
        with mock.patch('common.objects.base_classes.messages.message_to_clear'), \
             mock.patch('common.objects.base_classes.messages.message_to_get'):
            dev.clear_message_on_cn(_status_code='OK')

    #################################
    def test_clear_message_on_cn_fail1(self):
        """
        clear_message_on_cn pass case 1:
        Create: Test devices object
        Mock: messages_to_clear
        Return: None, so it does not fail
        Mock: ser.send_and_wait_for_reply
        Return: None, so it does not fail
        Mock: messages_to_get
        Return: None, so it does not fail
        Mock: ser.get_and_wait_for_reply
        Return: Exception, but no "NM No Message Found' with it
        Run: test
        """
        # Todo: does this method need an exception to catch the error message?
        # Create test devices object
        dev = self.create_test_device_object()

        # Mock send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = None

        # Mock get and wait for reply
        self.mock_get_and_wait_for_reply.side_effect = Exception

        # Run test, expect an exception
        with mock.patch('common.objects.base_classes.messages.message_to_clear'), \
             mock.patch('common.objects.base_classes.messages.message_to_get'):
            dev.clear_message_on_cn(_status_code='OK')

    #################################
    def test_clear_message_on_cn_fail2(self):
        """ Clear Message On Controller Fail Case 2: Exception is thrown when sending command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: An Exception
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        """
        dv = self.create_test_device_object()

        expected_msg = "Exception caught in messages.clear_message: "

        self.mock_send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_clear'):
                dv.clear_message_on_cn(_status_code='OK')

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_description_on_cn_pass1(self):
        """ Verify Description On Controller Pass Case 1: Exception is not raised """
        dev = self.create_test_device_object()
        dev.ds = 'Test Device 2'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test Device 2".format(opcodes.description))
        dev.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                dev.verify_description_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_description_on_cn_fail1(self):
        """ Verify Description On Controller Fail Case 1: Value on controller does not match what is
        stored in dev.ds for dev type ZN """
        dev = self.create_test_device_object()
        dev.ds = 'Test Device'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test".format(opcodes.description))
        dev.data = mock_data

        expected_message = "Unable verify ZN: 1's description. Received: Test, Expected: Test Device"
        try:
            dev.verify_description_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_description_on_cn_fail2(self):
        """ Verify Description On Controller Fail Case 2: Value on controller does not match what is
        stored in dev.ds for dev type MV """
        dev = self.create_test_device_object(dev_type="MV")
        dev.ds = 'Test Device'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test".format(opcodes.description))
        dev.data = mock_data

        expected_message = "Unable verify MV: TSD0001's description. Received: Test, Expected: Test Device"
        try:
            dev.verify_description_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_description_on_cn_fail3(self):
        """
        verify_description_on_cn fail case 3:
        Create: test devices object
        Store: expected error message
        Set: dv_type equal to something that is not currently in devices
        Run: test, with ValueError expected
        Compare: expected error to actual error
        """
        # Create test devices object
        dev = self.create_test_device_object()

        # Store expected error message
        e_msg = "Invalid device type: {0} entered while trying to send longitude to cn".format(str('BN'))

        # Set dv type equal to something that is not in device types
        dev.dv_type = 'BN'

        # Run the test with ValueError expected
        with self.assertRaises(ValueError) as context:
            dev.do_self_test()

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_status_on_cn_pass1(self):
        """ Verify Status On Controller Pass Case 1: Exception is not raised """
        dev = self.create_test_device_object()
        expected_ss = opcodes.okay
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=OK".format(opcodes.status_code))
        dev.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                dev.verify_status_on_cn(expected_ss)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_status_on_cn_fail1(self):
        """ Verify Status On Controller Fail Case 1: Value on controller does not match what is
        passed into method for dev type ZN """
        dev = self.create_test_device_object()
        expected_ss = opcodes.disabled
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=OK".format(opcodes.status_code))
        dev.data = mock_data

        expected_message = "Unable verify ZN: 1's status. Received: OK, Expected: DS"
        try:
            dev.verify_status_on_cn(expected_ss)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_status_on_cn_fail2(self):
        """ Verify Status On Controller Fail Case 2: Value on controller does not match what is
        stored in dev.ds for dev type MV """
        dev = self.create_test_device_object(dev_type="MV")
        expected_ss = opcodes.disabled
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=OK".format(opcodes.status_code))
        dev.data = mock_data

        expected_message = "Unable verify MV: TSD0001's status. Received: OK, Expected: DS"
        try:
            dev.verify_status_on_cn(expected_ss)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_status_on_cn_fail3(self):
        """ Verify Status On Controller Fail Case 3: Argument passed in is not a valid status code """
        dev = self.create_test_device_object()
        expected_ss = "BG"
        test_pass = False
        e_msg = ""

        # mock_data = status_parser.KeyValues("SN=TSD0001,{0}=OK".format(opcodes.status_code))
        # dev.data = mock_data

        expected_message = "Unable to verify ZN 1's status. Received invalid expected status: (BG),\n" \
                           " Expected one of the following: ({0})".format(common_vars.dictionary_for_status_codes.keys())
        try:
            dev.verify_status_on_cn(expected_ss)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_latitude_on_cn_pass1(self):
        """ Verify Latitude On Controller Pass Case 1: Exception is not raised """
        dev = self.create_test_device_object()
        dev.la = 5.000001
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5.000001".format(opcodes.latitude))
        dev.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                dev.verify_latitude_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_latitude_on_cn_fail1(self):
        """ Verify Latitude On Controller Fail Case 1: Value on controller does not match what is
        stored in dev.la for dev type ZN """
        dev = self.create_test_device_object()
        dev.la = 5.000001
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5.000002".format(opcodes.latitude))
        dev.data = mock_data

        expected_message = "Unable verify ZN: 1's 'latitude'. Received: 5.000002, Expected: 5.000001"
        try:
            dev.verify_latitude_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_latitude_on_cn_fail2(self):
        """ Verify Latitude On Controller Fail Case 2: Value on controller does not match what is
        stored in dev.la for dev type MV """
        dev = self.create_test_device_object(dev_type="MV")
        dev.la = 5.000001
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5.000002".format(opcodes.latitude))
        dev.data = mock_data

        expected_message = "Unable verify MV: TSD0001's 'latitude'. Received: 5.000002, Expected: 5.000001"
        try:
            dev.verify_latitude_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_longitude_on_cn_pass1(self):
        """ Verify Longitude On Controller Pass Case 1: Exception is not raised """
        dev = self.create_test_device_object()
        dev.lg = 5.000001
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5.000001".format(opcodes.longitude))
        dev.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                dev.verify_longitude_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_longitude_on_cn_fail1(self):
        """ Verify Longitude On Controller Fail Case 1: Value on controller does not match what is
        stored in dev.lg for dev type ZN """
        dev = self.create_test_device_object()
        dev.lg = 5.000001
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5.000002".format(opcodes.longitude))
        dev.data = mock_data

        expected_message = "Unable verify ZN: 1's 'longitude'. Received: 5.000002, Expected: 5.000001"
        try:
            dev.verify_longitude_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_longitude_on_cn_fail2(self):
        """ Verify Longitude On Controller Fail Case 2: Value on controller does not match what is
        stored in dev.lg for dev type MV """
        dev = self.create_test_device_object(dev_type="MV")
        dev.lg = 5.000001
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5.000002".format(opcodes.longitude))
        dev.data = mock_data

        expected_message = "Unable verify MV: TSD0001's 'longitude'. Received: 5.000002, Expected: 5.000001"
        try:
            dev.verify_longitude_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_serial_number_on_cn_pass1(self):
        """ Verify Serial Number On Controller Pass Case 1: Exception is not raised """
        dev = self.create_test_device_object()
        dev.sn = "TSD0002"
        test_pass = False

        mock_data = status_parser.KeyValues("{0}=TSD0002".format(opcodes.serial_number))
        dev.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                dev.verify_serial_number_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_serial_number_on_cn_fail1(self):
        """ Verify Serial Number On Controller Fail Case 1: Value on controller does not match what is
        stored in dev.sn for dev type ZN """
        dev = self.create_test_device_object()
        dev.sn = "TSD0002"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}=TSD0003".format(opcodes.serial_number))
        dev.data = mock_data

        expected_message = "Unable verify ZN: 1's 'Serial Number'. Received: TSD0003, Expected: TSD0002"
        try:
            dev.verify_serial_number_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_message_on_cn_pass1(self):
        """
        verify_message_on_cn pass case 1
        Create test object
        Mock: get_message_on_cn because it goes outside of the method
            Return: None so it does not fail
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id, and expected_date_time_from_cn
        Mock: build_message_string (note the [opcodes.message_id])
            Return: Four different strings, different than the ones used for expected_message_id, controller_date_time
            expected_date_time, and expected_message_text.
        Run: the test, and pass a status code in
        Compare: All the strings in each dictionary, and make sure it was stored properly in the objects build string
        """
        # Create test mainline object
        dv = self.create_test_device_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Mock get message on cn
        mock_get_message_on_cn = mock.MagicMock(side_effect=['thing'])
        dv.get_message_on_cn = mock_get_message_on_cn

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, message_date])
        dv.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        dv.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Run test
        dv.verify_message_on_cn(_status_code='OK')

        # Compare actual strings to expected string
        self.assertEqual({'DT': '5/20/15 3:20:11', 'ID': 'blah2', 'TX': 'blah'}, dv.build_message_string)

    #################################
    def test_verify_message_on_cn_pass2(self):
        """ Verify message on controller Pass Case 2: Created DT doesn't match controller DT, but are within 60 minutes
        Create test object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the object to return the three strings
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: There is nothing to compare, we just want it to run through successfully since it just verifies
        """
        # Create test mainline object
        dv = self.create_test_device_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError, but still within 60 minutes)
        wrong_message_dt = '5/20/15 3:59:11'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, wrong_message_dt])
        dv.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        dv.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Run method that will tested
        dv.verify_message_on_cn(_status_code=opcodes.bad_serial)

    #################################
    def test_verify_message_on_cn_fail1(self):
        """ Verify message on controller Fail Case 1: Created ID doesn't match controller ID, raise ValueError
        Create test object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id (which will be wrong so an
            ValueError is thrown), and expected_date_time_from_cn
        Manually fill up the build_message_string variables in the object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        dv = self.create_test_device_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message ID (This is so we can purposefully force the ValueError)
        wrong_message_id = 'blah3'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, wrong_message_id, message_date])
        dv.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        dv.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "Created ID message did not match the ID received from the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           dv.build_message_string[opcodes.message_id],    # {0} The ID that was built
                           wrong_message_id                                # {1} The ID returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            dv.verify_message_on_cn(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_on_cn_fail2(self):
        """ Verify message on controller Fail Case 2: Created DT doesn't match controller DT, raise ValueError
        Create test object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        dv = self.create_test_device_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError)
        wrong_message_dt = '5/11/11 3:59:59'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, wrong_message_dt])
        dv.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        dv.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "The date and time of the message didn't match the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           dv.build_message_string[opcodes.date_time],  # {0} The DT that was built
                           wrong_message_dt                             # {1} The DT returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            dv.verify_message_on_cn(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_on_cn_fail3(self):
        """ Verify message on controller Fail Case 3: Created TX doesn't match controller TX, raise ValueError
        Create test object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        dv = self.create_test_device_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError, but still within 60 minutes)
        wrong_message_tx = 'blah3'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[wrong_message_tx, message_id, message_date])
        dv.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        dv.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "Created TX message did not match the TX received from the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           dv.build_message_string[opcodes.message_text],  # {0} The TX that was built
                           wrong_message_tx                                 # {1} The TX returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            dv.verify_message_on_cn(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    if __name__ == "__main__":
        unittest.main()
