__author__ = 'Brice "Ajo Grande" Garlick'

import unittest

import mock
import serial
import status_parser
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.programs import Programs
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes


class TestProgramsObject(unittest.TestCase):
    """   """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        Programs.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_programs_object(self):
        """ Creates a new Master Valve object for testing purposes """
        pg = Programs(_ds="Test Programs 1", _ad=1)

        return pg

    #################################
    def test_set_description_on_cn_pass1(self):
        """ Set Description On Controller Pass Case 1: Using Default _ds value """
        pg = self.create_test_programs_object()

        expected_value = pg.ds
        pg.set_description_on_cn()

        actual_value = pg.ds
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_description_on_cn_pass2(self):
        """ Set Description On Controller Pass Case 2: Setting new _ds value = Test Programs 2 """
        pg = self.create_test_programs_object()

        expected_value = "Test Programs 2"
        pg.set_description_on_cn(expected_value)

        actual_value = pg.ds
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_description_on_cn_pass3(self):
        """ Set Description On Controller Pass Case 3: Command with correct values sent to controller """
        pg = self.create_test_programs_object()

        ds_value = str(pg.ds)
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.description, str(ds_value))
        pg.set_description_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_description_on_cn_fail1(self):
        """ Set Description On Controller Fail Case 1: Failed communication with controller """
        pg = self.create_test_programs_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            pg.set_description_on_cn()
        e_msg = "Exception occurred trying to set Program {0}'s description to: {1}".format(
                str(pg.ad), str(pg.ds))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_enable_state_pass1(self):
        """ Set Enable State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.enabled, opcodes.true)
        pg = self.create_test_programs_object()
        pg.set_enable_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_state_pass2(self):
        """ Set Enable State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.enabled, opcodes.false)
        pg = self.create_test_programs_object()
        pg.set_enable_state_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_state_fail1(self):
        """ Set Enable State On Controller Fail Case 1: Invalid State Argument """
        pg = self.create_test_programs_object()
        with self.assertRaises(ValueError) as context:
            pg.set_enable_state_on_cn("Foo")
        e_msg = "Invalid state for program to set: Foo. Valid states are 'TR' or 'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_enable_state_fail2(self):
        """ Set Enable State On Controller Fail Case 2: Failed Communication With Controller """
        pg = self.create_test_programs_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            pg.set_enable_state_on_cn("FA")
        e_msg = "Exception occurred trying to set Program 1's enabled state to: FA"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_water_window_on_cn_pass1(self):
        """ Set Water Window On Controller Pass Case 1: Using Default _ww value """
        pg = self.create_test_programs_object()

        # Expected value is the _ds value set at object Zone object creation
        expected_value = '111111111111111111111111'
        pg.set_water_window_on_cn()

        # _ds value is set during this method and should equal the original value
        actual_value = pg.ww[0]
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_water_window_on_cn_pass2(self):
        """ Set Water Window On Controller Pass Case 2: Using passed in value 111111101111110111111111 for _ww\n
        _is_weekly = true """
        pg = self.create_test_programs_object()

        # Expected value is the _ww value set at object Zone object creation
        expected_value = ['111111101111110111111111']
        pg.set_water_window_on_cn(expected_value)

        # _ww value is set during this method and should equal the original value
        actual_value = pg.ww[0]
        self.assertEqual(first=expected_value[0], second=actual_value)

    #################################
    def test_set_water_window_on_cn_pass3(self):
        """ Set Water Window On Controller Pass Case 2: Using passed in values:\n
            011111111111111111111111\n
            001111111111111111111111\n
            000111111111111111111111\n
            000011111111111111111111\n
            000001111111111111111111\n
            000000111111111111111111\n
            000000011111111111111111\n

        _is_weekly = true """
        pg = self.create_test_programs_object()

        # Expected value is the _ww value set at object Zone object creation
        expected_value = ['011111111111111111111111',
                          '001111111111111111111111',
                          '000111111111111111111111',
                          '000011111111111111111111',
                          '000001111111111111111111',
                          '000000111111111111111111',
                          '000000011111111111111111']
        pg.set_water_window_on_cn(expected_value, _is_weekly=False)

        # _ww value is set during this method and should equal the original value
        actual_value = pg.ww
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_water_window_on_cn_pass4(self):
        """ Set Water Window On Controller Pass Case 4: Command with correct values sent to controller with:\n
            ww value = 111111111111111111111111\n
            _is_weekly = True """
        pg = self.create_test_programs_object()

        ww_value = pg.ww
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.weekly_water_window, str(ww_value[0]))
        pg.set_water_window_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_window_on_cn_pass5(self):
        """ Set Water Window On Controller Pass Case 5: Command with correct values sent to controller with:\n
            ww value =  011111111111111111111111\n
                        001111111111111111111111\n
                        000111111111111111111111\n
                        000011111111111111111111\n
                        000001111111111111111111\n
                        000000111111111111111111\n
                        000000011111111111111111\n
            _is_weekly = False """
        pg = self.create_test_programs_object()

        ww_value = ['011111111111111111111111',
                    '001111111111111111111111',
                    '000111111111111111111111',
                    '000011111111111111111111',
                    '000001111111111111111111',
                    '000000111111111111111111',
                    '000000011111111111111111'
                    ]
        expected_command = "SET,{0}=1,{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14}".format(
            opcodes.program,
            opcodes.sunday, str(ww_value[0]),
            opcodes.monday, str(ww_value[1]),
            opcodes.tuesday, str(ww_value[2]),
            opcodes.wednesday, str(ww_value[3]),
            opcodes.thursday, str(ww_value[4]),
            opcodes.friday, str(ww_value[5]),
            opcodes.saturday, str(ww_value[6]),
        )
        pg.set_water_window_on_cn(_ww=ww_value, _is_weekly=False)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_window_on_cn_fail1(self):
        """ Set Water Window On Controller Fail Case 1: Pass invalid string:\n
                011111111111111111111112 (String can only have 1's and 0's)\n
                _is_weekly = True """
        pg = self.create_test_programs_object()

        new_ww_value = ["011111111111111111111112"]
        with self.assertRaises(Exception) as context:
            pg.set_water_window_on_cn(new_ww_value)
        expected_message = "Invalid water window for program to set at index 0: {0}. Valid water window must " \
                           "be a string of 0's and 1's.".format(new_ww_value[0])
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_water_window_on_cn_fail2(self):
        """ Set Water Window On Controller Fail Case 2: Pass invalid string at index 2:\n
            ww value =  011111111111111111111111\n
                        001111111111111111111111\n
                        000111111111111111111131\n
                        000011111111111111111111\n
                        000001111111111111111111\n
                        000000111111111111111111\n
                        000000011111111111111111\n
            _is_weekly = False  """
        pg = self.create_test_programs_object()

        wrong_index = 2
        new_ww_value = ['011111111111111111111111',
                        '001111111111111111111111',
                        '000111111111111111111131',
                        '000011111111111111111111',
                        '000001111111111111111111',
                        '000000111111111111111111',
                        '000000011111111111111111'
                        ]
        with self.assertRaises(Exception) as context:
            pg.set_water_window_on_cn(new_ww_value, False)
        expected_message = "Invalid water window for program to set at index {0}: {1}. Valid water window must " \
                           "be a string of 0's and 1's.".format(str(wrong_index), new_ww_value[wrong_index])
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_water_window_on_cn_fail3(self):
        """ Set Water Window On Controller Fail Case 3: Pass invalid string at index 6: \n
            ww value =  011111111111111111111111\n
                        001111111111111111111111\n
                        000111111111111111111111\n
                        000011111111111111111111\n
                        000001111111111111111111\n
                        000000111111111111111111\n
                        000000061111111111111111\n
            _is_weekly = False  """
        pg = self.create_test_programs_object()

        wrong_index = 6
        new_ww_value = ['011111111111111111111111',
                        '001111111111111111111111',
                        '000111111111111111111111',
                        '000011111111111111111111',
                        '000001111111111111111111',
                        '000000111111111111111111',
                        '000000061111111111111111'
                        ]
        with self.assertRaises(Exception) as context:
            pg.set_water_window_on_cn(new_ww_value, False)
        expected_message = "Invalid water window for program to set at index {0}: {1}. Valid water window must " \
                           "be a string of 0's and 1's.".format(str(wrong_index), new_ww_value[wrong_index])
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_water_window_on_cn_fail4(self):
        """ Set Water Window On Controller Fail Case 4: Pass invalid string:\n
                0111111111111111111111111 (String can only 24 characters, this has 25)\n
                _is_weekly = True """
        pg = self.create_test_programs_object()

        new_ww_value = ["0111111111111111111111111"]
        with self.assertRaises(Exception) as context:
            pg.set_water_window_on_cn(new_ww_value)
        expected_message = "Invalid water window for program to set at index 0: {0}. Valid water window must be" \
                           "exactly 24 characters long".format(new_ww_value[0])
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_water_window_on_cn_fail5(self):
        """ Set Water Window On Controller Fail Case 5: Pass invalid string:\n
                01111111111111111111111 (String can only 24 characters, this has 23)\n
                _is_weekly = True """
        pg = self.create_test_programs_object()

        new_ww_value = ["01111111111111111111111"]
        with self.assertRaises(Exception) as context:
            pg.set_water_window_on_cn(new_ww_value)
        expected_message = "Invalid water window for program to set at index 0: {0}. Valid water window must be" \
                           "exactly 24 characters long".format(new_ww_value[0])
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_water_window_on_cn_fail6(self):
        """ Set Water Window On Controller Fail Case 6: Pass invalid string at index 3: \n
            ww value =  011111111111111111111111\n
                        001111111111111111111111\n
                        000111111111111111111111\n
                        00001111111111111111111\n
                        000001111111111111111111\n
                        000000111111111111111111\n
                        000000011111111111111111\n
            _is_weekly = False  """
        pg = self.create_test_programs_object()

        wrong_index = 3
        new_ww_value = ['011111111111111111111111',
                        '001111111111111111111111',
                        '000111111111111111111111',
                        '00001111111111111111111',
                        '000001111111111111111111',
                        '000000111111111111111111',
                        '000000011111111111111111'
                        ]
        with self.assertRaises(Exception) as context:
            pg.set_water_window_on_cn(new_ww_value, False)
        expected_message = "Invalid water window for program to set at index {0}: {1}. Valid water window must be" \
                           "exactly 24 characters long".format(str(wrong_index), new_ww_value[wrong_index])
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_water_window_on_cn_fail7(self):
        """ Set Water Window On Controller Fail Case 7: Pass invalid # of elements for water window type string:\n
                0111111111111111111111111
                0011111111111111111111111  (2 elements, should only be 1)
                _is_weekly = True """
        pg = self.create_test_programs_object()

        new_ww_value = ["011111111111111111111111",
                        "001111111111111111111111"
                        ]
        with self.assertRaises(Exception) as context:
            pg.set_water_window_on_cn(new_ww_value)
        expected_message = "Invalid # of list elements for a water window: if weekly water window = true, " \
                           "# of arguments = 1"
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_water_window_on_cn_fail8(self):
        """ Set Water Window On Controller Fail Case 8: Pass invalid # of elements for water window type string:\n
            ww value =  011111111111111111111111\n
                        001111111111111111111111\n
                        000111111111111111111111\n
                        000011111111111111111111\n
                        000001111111111111111111\n
                        000000111111111111111111\n  (6 elements, should be 7)
            _is_weekly = False  """
        pg = self.create_test_programs_object()

        new_ww_value = ['011111111111111111111111',
                        '001111111111111111111111',
                        '000111111111111111111111',
                        '000011111111111111111111',
                        '000001111111111111111111',
                        '000000111111111111111111'
                        ]
        with self.assertRaises(Exception) as context:
            pg.set_water_window_on_cn(new_ww_value, False)
        expected_message = "Invalid # of list elements for a water window: if weekly water window = false, " \
                           "# of arguments = 7"
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_water_window_on_cn_fail9(self):
        """ Set Water Window On Controller Fail Case 8: Exception occured with communication with controller """
        pg = self.create_test_programs_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            pg.set_water_window_on_cn()
        e_msg = "Exception occurred trying to set Program 1's Water Window to: {0}".format(str(pg.ww))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_max_concurrent_zones_on_cn_pass1(self):
        """ Set Max Concurrent Zones On Controller Pass Case 1: Use default _mc value """
        pg = self.create_test_programs_object()

        # Expected value is the _mc value set at object Zone object creation
        expected_value = pg.mc
        pg.set_max_concurrent_zones_on_cn()

        # _mc value is set during this method and should equal the original value
        actual_value = pg.mc
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_max_concurrent_zones_on_cn_pass2(self):
        """ Set Max Concurrent Zones On Controller Pass Case 2: Set 5 as _mc value """
        pg = self.create_test_programs_object()

        # Expected value is the _mc value set at object Zone object creation
        expected_value = 5
        pg.set_max_concurrent_zones_on_cn(expected_value)

        # _mc value is set during this method and should equal the original value
        actual_value = pg.mc
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_max_concurrent_zones_on_cn_pass3(self):
        """ Set Max Concurrent Zones On Controller Pass Case 3: Correct command is sent to controller """
        pg = self.create_test_programs_object()

        mc_value = str(pg.mc)
        expected_command = "SET,{0}={1},{2}={3}".format(opcodes.program, str(pg.ad), opcodes.max_concurrent_zones,
                                                        str(mc_value))
        pg.set_max_concurrent_zones_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_max_concurrent_zones_on_cn_fail1(self):
        """ Set Max Concurrent Zones On Controller Fail Case 1: Pass String value as argument """
        pg = self.create_test_programs_object()

        new_mc_value = "b"
        with self.assertRaises(Exception) as context:
            pg.set_max_concurrent_zones_on_cn(new_mc_value)
        expected_message = "Failed trying to set PG {0} concurrent zones. Invalid type, expected an int, " \
                           "received: {1}".format(str(pg.ad), type(new_mc_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_max_concurrent_zones_on_cn_fail2(self):
        """ Set Max Concurrent Zones On Controller Fail Case 2: Failed communication with controller """
        pg = self.create_test_programs_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            pg.set_max_concurrent_zones_on_cn()
        e_msg = "Exception occurred trying to set Program {0}'s max concurrent zones to: {1}"\
                .format(str(pg.ad), str(pg.mc))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_seasonal_adjust_on_cn_pass1(self):
        """ Set Seasonal Adjust On Controller Pass Case 1: Use default _sa value """
        pg = self.create_test_programs_object()

        # Expected value is the _mc value set at object Zone object creation
        expected_value = pg.sa
        pg.set_seasonal_adjust_on_cn()

        # _mc value is set during this method and should equal the original value
        actual_value = pg.sa
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_seasonal_adjust_on_cn_pass2(self):
        """ Set Seasonal Adjust On Controller Pass Case 2: Set 5 as _sa value """
        pg = self.create_test_programs_object()

        # Expected value is the _mc value set at object Zone object creation
        expected_value = 5
        pg.set_seasonal_adjust_on_cn(expected_value)

        # _mc value is set during this method and should equal the original value
        actual_value = pg.sa
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_seasonal_adjust_on_cn_pass3(self):
        """ Set Seasonal Adjust On Controller Pass Case 3: Correct command is sent to controller """
        pg = self.create_test_programs_object()

        sa_value = str(pg.sa)
        expected_command = "SET,{0}={1},{2}={3}".format(opcodes.program, str(pg.ad), opcodes.seasonal_adjust,
                                                        str(sa_value))
        pg.set_seasonal_adjust_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_seasonal_adjust_on_cn_fail1(self):
        """ Set Seasonal Adjust On Controller Fail Case 1: Pass String value as argument """
        pg = self.create_test_programs_object()

        new_sa_value = "b"
        with self.assertRaises(Exception) as context:
            pg.set_seasonal_adjust_on_cn(new_sa_value)
        expected_message = "Failed trying to set PG {0} seasonal adjust. Invalid type, expected an int, " \
                           "received: {1}".format(str(pg.ad), type(new_sa_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_seasonal_adjust_on_cn_fail2(self):
        """ Set Seasonal Adjust On Controller Fail Case 2: Failed communication with controller """
        pg = self.create_test_programs_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            pg.set_seasonal_adjust_on_cn()
        e_msg = "Exception occurred trying to set Program {0}'s Seasonal Adjust to: {1}".format(str(pg.ad),
                                                                                                str(pg.sa))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_message_on_cn_pass1(self):
        """ Set Message On Controller Pass Case 1: Correct command is sent to controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: message_to_set because it goes outside of the method
            Return: The expected message
        Call the method
        """
        pg = self.create_test_programs_object()

        expected_command = "SET,MG,{0}={1},{2}={3}".format(opcodes.point_of_connection, str(pg.ad), opcodes.status_code,
                                                           opcodes.learn_flow_with_errors)

        with mock.patch('common.objects.base_classes.messages.message_to_set', return_value=expected_command):
            pg.set_message_on_cn(_status_code='OK')
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_message_on_cn_fail1(self):
        """ Set Message On Controller Controller Fail Case 1: Incorrect command is sent to the controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: message_to_set because it goes outside of the method
            Return: The expected message
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        """
        pg = self.create_test_programs_object()

        expected_command = "Exception caught in messages.set_message: "

        self.mock_send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_set', side_effect=Exception):
                pg.set_message_on_cn(_status_code=opcodes.learn_flow_with_errors)

        self.assertEqual(expected_command, context.exception.message)

    #################################
    def test_get_data_pass1(self):
        """ Get Data From Controller Pass Case 1: Correct command is sent to controller """
        pg = self.create_test_programs_object()

        expected_command = "GET,{0}={1}".format(opcodes.program, str(pg.ad))
        pg.get_data()
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_data_fail1(self):
        """ Get Data From Controller Fail Case 1: Failed communication with controller """
        pg = self.create_test_programs_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_get_and_wait_for_reply.side_effect = Exception

        expected_command = "GET,{0}={1}".format(opcodes.program, str(pg.ad))
        with self.assertRaises(Exception) as context:
            pg.get_data()
        e_msg = "Unable to get data for Program: '{0}' using command: '{1}'. Exception raised: "\
                .format(str(pg.ad), expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_get_message_on_cn_pass1(self):
        """ Get Message On Controller Pass Case 1: Correct command is sent to controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: message_to_get because it goes outside of the method
            Return: The expected message
        Call the method
        """
        pg = self.create_test_programs_object()

        expected_command = "GET,MG,{0}={1},{2}={3}".format(opcodes.point_of_connection, str(pg.ad), opcodes.status_code,
                                                           opcodes.learn_flow_with_errors)

        with mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: expected_command}):
            pg.get_message_on_cn(_status_code=opcodes.budget_exceeded)

        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_message_on_cn_fail1(self):
        """ Get Message On Controller Controller Fail Case 1: Incorrect command is sent to the controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: message_to_get because it goes outside of the method
            Return: The expected message
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        Assert the created command is the same as the command from the method
        """
        pg = self.create_test_programs_object()

        expected_command = "Exception caught in messages.get_message: "

        self.mock_get_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_get'):
                pg.get_message_on_cn(_status_code=opcodes.learn_flow_with_errors)

        self.assertEqual(expected_command, context.exception.message)

    #################################
    def test_clear_message_on_cn_pass1(self):
        """ Clear Message On Controller Pass Case 1: Correct command is sent to controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: The expected message in a dictionary
        Call the method and verify what is was called with

        """
        pg = self.create_test_programs_object()

        expected_command = "GET,MG,{0}={1},{2}={3}".format(opcodes.mainline, str(pg.ad), opcodes.status_code,
                                                           opcodes.learn_flow_with_errors)

        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value=expected_command), \
             mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: expected_command}):
            pg.clear_message_on_cn(_status_code=opcodes.budget_exceeded)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_clear_message_on_cn_fail1(self):
        """ Clear Message On Controller Fail Case 1: Exception is thrown when sending command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: An Exception
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        """
        pg = self.create_test_programs_object()

        expected_msg = "Exception caught in messages.clear_message: "

        self.mock_send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_clear'):
                pg.clear_message_on_cn(_status_code=opcodes.learn_flow_with_errors)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_clear_message_on_cn_fail2(self):
        """ Clear Message On Controller Fail Case 2: Exception is thrown when getting command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: The expected message
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method
        Cannot make an assertion since the exception is caught inside the method
        """
        pg = self.create_test_programs_object()

        expected_msg = "Message cleared, but was unable to verify if the message was gone."

        self.mock_get_and_wait_for_reply.side_effect = Exception

        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value=expected_msg):
            pg.clear_message_on_cn(_status_code=opcodes.learn_flow_with_errors)

    #################################
    def test_clear_message_on_cn_fail3(self):
        """ Clear Message On Controller Fail Case 3: Exception is thrown when getting command, no message was found
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: The expected message
        Mock: Messages to get because it goes outside of the method
            Return: the expected message in a dictionary
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method
        Cannot make an assertion since the exception is caught inside the method
        """
        pg = self.create_test_programs_object()

        no_message_found = "NM No Message Found"

        self.mock_get_and_wait_for_reply.side_effect = Exception(no_message_found)

        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value=no_message_found), \
             mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: no_message_found}):
            pg.clear_message_on_cn(_status_code=opcodes.budget_exceeded)

    #################################
    def test_verify_description_on_cn_pass1(self):
        """ Verify Description On Controller Pass Case 1: Exception is not raised """
        pg = self.create_test_programs_object()
        pg.ds = 'Test Program 2'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test Program 2".format(opcodes.description))
        pg.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                pg.verify_description_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_description_on_cn_fail1(self):
        """ Verify Description On Controller Fail Case 1: Value on controller does not match what is
        stored in pg.ds """
        pg = self.create_test_programs_object()
        pg.ds = 'Test Program'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test".format(opcodes.description))
        pg.data = mock_data

        expected_message = "Unable verify Program: 1's Description. Received: Test, Expected: Test Program"
        try:
            pg.verify_description_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_enabled_state_on_cn_pass1(self):
        """ Verify Enable State On Controller Pass Case 1: Exception is not raised """
        pg = self.create_test_programs_object()
        pg.en = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
        pg.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                pg.verify_enabled_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_enabled_state_on_cn_fail1(self):
        """ Verify Enable State On Controller Fail Case 1: Value on controller does not match what is
        stored in pg.en """
        pg = self.create_test_programs_object()
        pg.en = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
        pg.data = mock_data

        expected_message = "Unable verify Program: 1's Enabled State. Received: TR, Expected: Tr"
        try:
            pg.verify_enabled_state_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_water_window_on_cn_pass1(self):
        """ Verify Water Window On Controller Pass Case 1: Exception is not raised """
        pg = self.create_test_programs_object()
        pg.ww = ['011111111111111111111111']
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=011111111111111111111111"
                                            .format(opcodes.weekly_water_window))
        pg.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                pg.verify_water_window_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_water_window_on_cn_pass2(self):
        """ Verify Water Window On Controller Pass Case 2: Exception is not raised """
        pg = self.create_test_programs_object()
        pg.ww = ['011111111111111111111111',
                 '001111111111111111111111',
                 '000111111111111111111111',
                 '000011111111111111111111',
                 '000001111111111111111111',
                 '000000111111111111111111',
                 '000000011111111111111111'
                 ]
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,"
                                            "{0}=011111111111111111111111,"
                                            "{1}=001111111111111111111111,"
                                            "{2}=000111111111111111111111,"
                                            "{3}=000011111111111111111111,"
                                            "{4}=000001111111111111111111,"
                                            "{5}=000000111111111111111111,"
                                            "{6}=000000011111111111111111".format(
                                                opcodes.sunday,
                                                opcodes.monday,
                                                opcodes.tuesday,
                                                opcodes.wednesday,
                                                opcodes.thursday,
                                                opcodes.friday,
                                                opcodes.saturday
                                                ))
        pg.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                pg.verify_water_window_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_water_window_on_cn_fail1(self):
        """ Verify Water Window On Controller Fail Case 1: Value on controller does not match what is
        stored in pg.en """
        pg = self.create_test_programs_object()
        pg.ww = ['021111111111111111111111']
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=011111111111111111111111"
                                            .format(opcodes.weekly_water_window))
        pg.data = mock_data

        expected_message = "Unable verify Program: 1's Water Window. Received: ['011111111111111111111111'], " \
                           "Expected: ['021111111111111111111111']"
        try:
            pg.verify_water_window_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_max_concurrent_zones_on_cn_pass1(self):
        """ Verify Max Concurrent Zones On Controller Pass Case 1: Exception is not raised """
        pg = self.create_test_programs_object()
        pg.mc = 5.0
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.max_concurrent_zones))
        pg.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                pg.verify_max_concurrent_zones_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_max_concurrent_zones_on_cn_fail1(self):
        """ Verify MAx Concurrent Zones On Controller Fail Case 1: Value on controller does not match what is
        stored in controller.mc """
        pg = self.create_test_programs_object()
        pg.mc = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.max_concurrent_zones))
        pg.data = mock_data

        expected_message = "Unable verify Program: 1's Max Concurrent Zones. Received: 6, Expected: 5"
        try:
            pg.verify_max_concurrent_zones_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_seasonal_adjust_on_cn_pass1(self):
        """ Verify Seasonal Adjust On Controller Pass Case 1: Exception is not raised """
        pg = self.create_test_programs_object()
        pg.sa = 5.0
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.seasonal_adjust))
        pg.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                pg.verify_seasonal_adjust_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_seasonal_adjust_on_cn_fail1(self):
        """ Verify Seasonal Adjust On Controller Fail Case 1: Value on controller does not match what is
        stored in controller.mc """
        pg = self.create_test_programs_object()
        pg.sa = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.seasonal_adjust))
        pg.data = mock_data

        expected_message = "Unable verify Program: 1's Seasonal Adjust. Received: 6, Expected: 5"
        try:
            pg.verify_seasonal_adjust_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_status_on_cn_pass1(self):
        """ Verify Status On Controller Pass Case 1: Exception is not raised """
        pg = self.create_test_programs_object()
        status_code = opcodes.running
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=RN".format(opcodes.status_code))
        pg.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                pg.verify_status_on_cn(status_code)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_status_on_cn_fail1(self):
        """ Verify Status On Controller Fail Case 1: Value on controller does not match what is
        stored in pg.ss """
        pg = self.create_test_programs_object()
        status_code = opcodes.connecting
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=DS".format(opcodes.status_code))
        pg.data = mock_data

        expected_message = "Unable verify Program: 1's status. Received: DS, Expected: CG"
        try:
            pg.verify_status_on_cn(status_code)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_message_on_cn_pass1(self):
        """
        verify_message_on_cn pass case 1
        Create test object
        Mock: get_message_on_cn because it goes outside of the method
            Return: None so it does not fail
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id, and expected_date_time_from_cn
        Mock: build_message_string (note the [opcodes.message_id])
            Return: Four different strings, different than the ones used for expected_message_id, controller_date_time
            expected_date_time, and expected_message_text.
        Run: the test, and pass a status code in
        Compare: All the strings in each dictionary, and make sure it was stored properly in the objects build string
        """
        # Create test mainline object
        pg = self.create_test_programs_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Mock get message on cn
        mock_get_message_on_cn = mock.MagicMock(side_effect=['thing'])
        pg.get_message_on_cn = mock_get_message_on_cn

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, message_date])
        pg.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        pg.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Run test
        pg.verify_message_on_cn(_status_code='OK')

        # Compare actual strings to expected string
        self.assertEqual({'DT': '5/20/15 3:20:11', 'ID': 'blah2', 'TX': 'blah'}, pg.build_message_string)

    #################################
    def test_verify_message_on_cn_pass2(self):
        """ Verify message on controller Pass Case 2: Created DT doesn't match controller DT, but are within 60 minutes
        Create test object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the object to return the three strings
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: There is nothing to compare, we just want it to run through successfully since it just verifies
        """
        # Create test mainline object
        pg = self.create_test_programs_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError, but still within 60 minutes)
        wrong_message_dt = '5/20/15 3:59:11'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, wrong_message_dt])
        pg.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        pg.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Run method that will tested
        pg.verify_message_on_cn(_status_code=opcodes.bad_serial)

    #################################
    def test_verify_message_on_cn_fail1(self):
        """ Verify message on controller Fail Case 1: Created ID doesn't match controller ID, raise ValueError
        Create test object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id (which will be wrong so an
            ValueError is thrown), and expected_date_time_from_cn
        Manually fill up the build_message_string variables in the object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        pg = self.create_test_programs_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message ID (This is so we can purposefully force the ValueError)
        wrong_message_id = 'blah3'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, wrong_message_id, message_date])
        pg.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        pg.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "Created ID message did not match the ID received from the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           pg.build_message_string[opcodes.message_id],    # {0} The ID that was built
                           wrong_message_id                                # {1} The ID returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            pg.verify_message_on_cn(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_on_cn_fail2(self):
        """ Verify message on controller Fail Case 2: Created DT doesn't match controller DT, raise ValueError
        Create test object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        pg = self.create_test_programs_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError)
        wrong_message_dt = '5/11/11 3:59:59'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, wrong_message_dt])
        pg.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        pg.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "The date and time of the message didn't match the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           pg.build_message_string[opcodes.date_time],  # {0} The DT that was built
                           wrong_message_dt                             # {1} The DT returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            pg.verify_message_on_cn(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_on_cn_fail3(self):
        """ Verify message on controller Fail Case 3: Created TX doesn't match controller TX, raise ValueError
        Create test object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        pg = self.create_test_programs_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError, but still within 60 minutes)
        wrong_message_tx = 'blah3'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[wrong_message_tx, message_id, message_date])
        pg.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        pg.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "Created TX message did not match the TX received from the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           pg.build_message_string[opcodes.message_text],  # {0} The TX that was built
                           wrong_message_tx                                 # {1} The TX returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            pg.verify_message_on_cn(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    if __name__ == "__main__":
        unittest.main()
