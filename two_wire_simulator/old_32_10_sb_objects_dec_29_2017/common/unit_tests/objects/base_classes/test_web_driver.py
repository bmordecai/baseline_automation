import os
import unittest
import mock

from selenium.common.exceptions import *
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.event_firing_webdriver import EventFiringWebDriver as EFWD
from selenium.webdriver.remote.webdriver import WebDriver as Driver
from selenium.webdriver.firefox import webdriver as ff_webdriver
from selenium.webdriver.chrome import webdriver as ch_webdriver
from selenium.webdriver.ie import webdriver as ie_webdriver


from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.locators import *
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import WebDriver
import old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver as wd
from old_32_10_sb_objects_dec_29_2017.common.user_configuration import UserConfiguration

__author__ = 'baseline'


class TestWebDriverObject(unittest.TestCase):

    def setUp(self):
        """ Setting up for the test. """
        # This allows us to have an absolute file path through all computers
        path = os.path.abspath(__file__).partition('old_32_10_sb_objects_dec_29_2017')
        self.conf = UserConfiguration(os.path.join(path[0], 'old_32_10_sb_objects_dec_29_2017/common/user_credentials/user_credentials_joe.json'))
        self.conf.unit_testing = True
        self.browser = WebDriver(self.conf)

        self.mock_web_driver = mock.MagicMock(spec=webdriver)
        self.browser.web_driver = self.mock_web_driver

        self.mock_wait_for_element_visible = mock.MagicMock(side_effect=None)
        self.mock_until = mock.MagicMock(side_effect=None)
        self.mock_web_driver_wait = mock.MagicMock(side_effect=None)
        self.mock_find_element = mock.MagicMock(side_effect=None)

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        self.browser.teardown_driver()
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_open_pass_1(self):
        """ Test Open Pass Case 1: Run through the method
        Mock: init_driver
            Return: Nothing we just want to pass over the method
        Mock: web_driver.capabilities
            Return: String to print out
        Mock: web_driver.maximize_window
            Return: Nothing we just want to pass over the method
        Mock: web_driver.get
            Return: Nothing we just want to pass over the method
        Call the method
        Assert the correct methods are called with the expected parameters
        """
        mock_init_driver = mock.MagicMock()
        self.browser.init_driver = mock_init_driver
        self.browser.web_driver.capabilities = mock.MagicMock(return_value="Testing Print")
        self.browser.web_driver.maximize_window = mock.MagicMock()
        self.browser.web_driver.get = mock.MagicMock()
        self.browser.web_driver.switch_to = mock.MagicMock()
        self.browser.web_driver.window = mock.MagicMock()
        self.browser.web_driver.window_handles = mock.MagicMock()

        self.browser.open(_browser_name="FireFox")

        mock_init_driver.assert_called_with(_browser="FireFox")

    def test_verify_file_path_fail_1(self):
        """ Verifies exception message raised when the file path doesn't exist. """
        bad_file_path = "//Ben/Foo/Bar"
        with self.assertRaises(ValueError) as context:
            self.browser.verify_file_path(file_path=bad_file_path)
        expected_msg = "Either file is missing, is not readable or wrong file path specified in user .json file." \
                       "\nReceived PATH: '//Ben/Foo/Bar'. For more information about web_drivers and expected paths " \
                       "please visit: 'https://seleniumhq.github.io/docs/wd.html'"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_init_driver_pass_1(self):
        """ Verifies a valid Google Chrome webdriver initialized on a mac """
        _browser_under_test = "chrome"
        mock_platform = mock.MagicMock(return_value="Darwin")
        previous_lib = wd.platform.system
        wd.platform.system = mock_platform
        mock_chrome_driver = mock.MagicMock(spec=webdriver.Chrome)
        mock_init_chrome_webdriver = mock.MagicMock(return_value=mock_chrome_driver)
        self.browser.init_chrome_webdriver = mock_init_chrome_webdriver
        self.browser.init_driver(_browser=_browser_under_test)
        fail_msg = "Expected a Google Chrome webdriver instance."
        self.assertTrue(expr=isinstance(self.browser.web_driver, webdriver.Chrome), msg=fail_msg)
        wd.platform.system = previous_lib

    def test_init_driver_pass_2(self):
        """ Verifies a valid Google Chrome webdriver initialized on windows """
        _browser_under_test = "chrome"
        mock_platform = mock.MagicMock(return_value="Windows")
        previous_lib = wd.platform.system
        wd.platform.system = mock_platform
        mock_chrome_driver = mock.MagicMock(spec=webdriver.Chrome)
        mock_init_chrome_webdriver = mock.MagicMock(return_value=mock_chrome_driver)
        self.browser.init_chrome_webdriver = mock_init_chrome_webdriver
        self.browser.init_driver(_browser=_browser_under_test)
        fail_msg = "Expected a Google Chrome webdriver instance."
        self.assertTrue(expr=isinstance(self.browser.web_driver, webdriver.Chrome), msg=fail_msg)
        wd.platform.system = previous_lib

    def test_init_driver_pass_3(self):
        """ Verifies a valid Google Chrome webdriver initialized on linux """
        _browser_under_test = "chrome"
        mock_platform = mock.MagicMock(return_value="Linux")
        previous_lib = wd.platform.system
        wd.platform.system = mock_platform
        mock_chrome_driver = mock.MagicMock(spec=webdriver.Chrome)
        mock_init_chrome_webdriver = mock.MagicMock(return_value=mock_chrome_driver)
        self.browser.init_chrome_webdriver = mock_init_chrome_webdriver
        self.browser.init_driver(_browser=_browser_under_test)
        fail_msg = "Expected a Google Chrome webdriver instance."
        self.assertTrue(expr=isinstance(self.browser.web_driver, webdriver.Chrome), msg=fail_msg)
        wd.platform.system = previous_lib

    def test_init_driver_pass_4(self):
        """ Verifies a valid Firefox webdriver initialized on Mac """
        _browser_under_test = "firefox"
        mock_platform = mock.MagicMock(return_value="Darwin")
        previous_lib = wd.platform.system
        wd.platform.system = mock_platform
        mock_firefox_driver = mock.MagicMock(spec=webdriver.Firefox)
        mock_init_firefox_webdriver = mock.MagicMock(return_value=mock_firefox_driver)
        self.browser.init_firefox_webdriver = mock_init_firefox_webdriver
        self.browser.init_driver(_browser=_browser_under_test)
        fail_msg = "Expected a Firefox webdriver instance."
        self.assertTrue(expr=isinstance(self.browser.web_driver, webdriver.Firefox), msg=fail_msg)
        wd.platform.system = previous_lib

    def test_init_driver_pass_5(self):
        """ Verifies a valid Firefox webdriver initialized on Mac """
        _browser_under_test = "firefox"
        mock_platform = mock.MagicMock(return_value="Windows")
        previous_lib = wd.platform.system
        wd.platform.system = mock_platform
        mock_firefox_driver = mock.MagicMock(spec=webdriver.Firefox)
        mock_init_firefox_webdriver = mock.MagicMock(return_value=mock_firefox_driver)
        self.browser.init_firefox_webdriver = mock_init_firefox_webdriver
        self.browser.init_driver(_browser=_browser_under_test)
        fail_msg = "Expected a Firefox webdriver instance."
        self.assertTrue(expr=isinstance(self.browser.web_driver, webdriver.Firefox), msg=fail_msg)
        wd.platform.system = previous_lib

    def test_init_driver_pass_6(self):
        """ Verifies a valid Firefox webdriver initialized on Mac """
        _browser_under_test = "firefox"
        mock_platform = mock.MagicMock(return_value="Linux")
        previous_lib = wd.platform.system
        wd.platform.system = mock_platform
        mock_firefox_driver = mock.MagicMock(spec=webdriver.Firefox)
        mock_init_firefox_webdriver = mock.MagicMock(return_value=mock_firefox_driver)
        self.browser.init_firefox_webdriver = mock_init_firefox_webdriver
        self.browser.init_driver(_browser=_browser_under_test)
        fail_msg = "Expected a Firefox webdriver instance."
        self.assertTrue(expr=isinstance(self.browser.web_driver, webdriver.Firefox), msg=fail_msg)
        wd.platform.system = previous_lib
        
    def test_init_driver_pass_7(self):
        """ Verifies a valid Internet Explorer webdriver initialized on Windows """
        _browser_under_test = "ie"
        mock_platform = mock.MagicMock(return_value="Windows")
        previous_lib = wd.platform.system
        wd.platform.system = mock_platform
        mock_ie_driver = mock.MagicMock(spec=webdriver.Ie)
        mock_init_ie_webdriver = mock.MagicMock(return_value=mock_ie_driver)
        self.browser.init_ie_webdriver = mock_init_ie_webdriver
        self.browser.init_driver(_browser=_browser_under_test)
        fail_msg = "Expected a Internet Explorer webdriver instance."
        self.assertTrue(expr=isinstance(self.browser.web_driver, webdriver.Ie), msg=fail_msg)
        wd.platform.system = previous_lib

    def test_init_driver_fail_1(self):
        """ Verifies exception and message when unsupported platform is trying to init a Internet Explorer webdriver """
        _browser_under_test = "ie"
        # Here we could use 'Linux' instead of 'Darwin', both expect the same outcome
        mock_platform = mock.MagicMock(return_value="Darwin")
        previous_lib = wd.platform.system
        wd.platform.system = mock_platform
        mock_ie_driver = mock.MagicMock(spec=webdriver.Ie)
        mock_init_ie_webdriver = mock.MagicMock(return_value=mock_ie_driver)
        self.browser.init_ie_webdriver = mock_init_ie_webdriver
        with self.assertRaises(ValueError) as context:
            self.browser.init_driver(_browser=_browser_under_test)
        expected_msg = "Internet Explorer web driver is not supported on both Linux and Macintosh Systems."
        self.assertEqual(first=expected_msg, second=context.exception.message)
        wd.platform.system = previous_lib

    def test_init_driver_fail_2(self):
        """ Verifies exception and message when user tries to init an unsupported webdriver """
        _browser_under_test = "Ben"
        with self.assertRaises(ValueError) as context:
            self.browser.init_driver(_browser=_browser_under_test)
        expected_msg = "Webdriver for browser: ben is currently not supported. Supported browsers: " \
                       "['chrome', 'google chrome', 'firefox', 'ie']"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_init_firefox_webdriver_pass_1(self):
        """ Test Init Firefox Webdriver Pass Case 1: Successful Firefox webdriver init and Firefox instance returned
        Mock: WebDriver.__init__ so we don't open up a browser
            Return: None since inits cannot return anything besides None, even when they are mocked apparently
        Mock: WebDriver.implicitly_wait
            Return: Nothing we just need to pass over it
        Call the method
        Assert that what was returned is of the correct type
        """
        mock_firefox = mock.MagicMock(return_value=None)
        ff_webdriver.WebDriver.__init__ = mock_firefox

        mock_implicitly_wait = mock.MagicMock(return_value=None)
        Driver.implicitly_wait = mock_implicitly_wait

        returned_firefox_driver = self.browser.init_firefox_webdriver()
        self.assertTrue(isinstance(returned_firefox_driver, EFWD))

    def test_init_firefox_webdriver_fail_1(self):
        """ Test Init Firefox Webdriver Fail Case 1: Exception when initializing WebDriver
        Mock: WebDriver.__init__ so we don't open up a browser
            Throws: Exception
        Create the expected error message
        Call the method and assert that an exception is thrown
        Assert the message attached to the exception is equal to what was expected
        """
        mock_firefox = mock.MagicMock(side_effect=Exception("Test Error Message"))
        ff_webdriver.WebDriver.__init__ = mock_firefox

        expected_msg = "Exception occurred initializing FireFox webdriver. Exception received: Test Error Message"

        with self.assertRaises(Exception) as context:
            self.browser.init_firefox_webdriver()
        self.assertEqual(expected_msg, context.exception.message)

    def test_init_chrome_webdriver_pass_1(self):
        """ Test Init Chrome Webdriver Pass Case 1: Successful Chrome webdriver init and Chrome instance returned
        Mock: WebDriver.__init__ so we don't open up a browser
            Return: None since inits cannot return anything besides None, even when they are mocked apparently
        Mock: WebDriver.implicitly_wait
            Return: Nothing we just need to pass over it
        Call the method
        Assert that what was returned is of the correct type
        """
        mock_chrome = mock.MagicMock(return_value=None)
        ch_webdriver.WebDriver.__init__ = mock_chrome

        mock_implicitly_wait = mock.MagicMock(return_value=None)
        Driver.implicitly_wait = mock_implicitly_wait

        returned_chrome_driver = self.browser.init_chrome_webdriver()

        self.assertTrue(isinstance(returned_chrome_driver, EFWD))

    def test_init_chrome_webdriver_fail_1(self):
        """ Verifies exception and message raised when unable to verify chrome file path """
        with self.assertRaises(Exception) as context:
            returned_chrome_driver = self.browser.init_chrome_webdriver(path_to_driver="Foo")
        expected_msg = "Exception occurred initializing Google Chrome webdriver. Exception received: Either file is " \
                       "missing, is not readable or wrong file path specified in user .json file.\nReceived PATH: " \
                       "'Foo'. For more information about web_drivers and expected paths please visit: " \
                       "'https://seleniumhq.github.io/docs/wd.html'"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    # TODO: Currently fails on Windows, but works on Mac. Seems to be a OS issue when trying to mock OS's
    # TODO: Exception occurs when the ChromeDriver tries to open, in 'service.py' -> 'self.service.start()'
    # def test_init_chrome_webdriver_fail_2(self):
    #     """ Verifies exception and message raised when unable to initialize the driver after valid file path """
    #     mock_verify_file_path = mock.MagicMock(side_effect=None)
    #     self.browser.verify_file_path = mock_verify_file_path
    #     with self.assertRaises(Exception) as context:
    #         returned_chrome_driver = self.browser.init_chrome_webdriver(path_to_driver="Some valid path")
    #     expected_msg = "Exception occurred initializing Google Chrome webdriver. Exception received: " \
    #                    "'Some valid path' executable needs to be in PATH. Please see " \
    #                    "https://sites.google.com/a/chromium.org/chromedriver/home"
    #     self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_init_ie_webdriver_pass_1(self):
        """ Test Init IE Webdriver Pass Case 1: Successful IE webdriver init and IE instance returned
        Mock: verify_file_path
            Return: Nothing since we just want to pass over it
        Mock: WebDriver.__init__ so we don't open up a browser
            Return: None since inits cannot return anything besides None, even when they are mocked apparently
        Mock: WebDriver.implicitly_wait
            Return: Nothing we just need to pass over it
        Call the method
        Assert that what was returned is of the correct type
        """
        mock_verify_file_path = mock.MagicMock()
        self.browser.verify_file_path = mock_verify_file_path

        mock_chrome = mock.MagicMock(return_value=None)
        ie_webdriver.WebDriver.__init__ = mock_chrome

        mock_implicitly_wait = mock.MagicMock(return_value=None)
        Driver.implicitly_wait = mock_implicitly_wait

        returned_chrome_driver = self.browser.init_ie_webdriver(path_to_driver="hi")

        self.assertTrue(isinstance(returned_chrome_driver, EFWD))

    # TODO: Currently fails on Windows, but works on Mac. Seems to be a OS issue when trying to mock OS's
    # TODO: Exception occurs when the ChromeDriver tries to open, in 'service.py' -> 'self.service.start()'
    def test_init_ie_webdriver_fail_1(self):
        """ Verifies exception and message raised when unable to verify ie file path """
        with self.assertRaises(Exception) as context:
            returned_ie_driver = self.browser.init_ie_webdriver(path_to_driver="Foo")
        expected_msg = "Exception occurred initializing Internet Explorer webdriver. Exception received: Either file " \
                       "is missing, is not readable or wrong file path specified in user .json file.\nReceived PATH: " \
                       "'Foo'. For more information about web_drivers and expected paths please visit: " \
                       "'https://seleniumhq.github.io/docs/wd.html'"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    # TODO: Currently fails on Windows, but works on Mac. Seems to be a OS issue when trying to mock OS's
    # TODO: Exception occurs when the ChromeDriver tries to open, in 'service.py' -> 'self.service.start()'
    # def test_init_ie_webdriver_fail_2(self):
    #     """ Verifies exception and message raised when unable to initialize the driver after valid file path """
    #     mock_verify_file_path = mock.MagicMock(side_effect=None)
    #     self.browser.verify_file_path = mock_verify_file_path
    #     with self.assertRaises(Exception) as context:
    #         returned_ie_driver = self.browser.init_ie_webdriver(path_to_driver="Some valid path")
    #     expected_msg = "Exception occurred initializing Internet Explorer webdriver. Exception received: IEDriver " \
    #                    "executable needs to be available in the path. Please download from " \
    #                    "http://selenium-release.storage.googleapis.com/index.html and read up at " \
    #                    "http://code.google.com/p/selenium/wiki/InternetExplorerDriver"
    #     self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_get_text_from_web_element_pass_1(self):
        """ Verifies getting text from a web element without having to access the 'innerHTML' attribute. """
        mock_web_element = mock.MagicMock(spec=WebElement)
        mock_web_element.text = 'Unit Test'
        self.mock_web_driver.find_element = mock.MagicMock(return_value=mock_web_element)
        self.browser.wait_for_element_visible = self.mock_wait_for_element_visible
        text_received = self.browser.get_text_from_web_element(_locator="FauxLocator")
        expected_text = 'Unit Test'
        self.assertEqual(first=text_received, second=expected_text)

    def test_get_text_from_web_element_pass_2(self):
        """ Verifies getting text from a web element with having to access the 'innerHTML' attribute. """
        mock_web_element = mock.MagicMock(spec=WebElement, innerHTML='Unit Test', text='Unit Test')
        del mock_web_element.text
        mock_web_element.get_attribute.return_value = 'Unit Test'
        self.mock_web_driver.find_element = mock.MagicMock(return_value=mock_web_element)
        self.browser.wait_for_element_visible = self.mock_wait_for_element_visible
        text_received = self.browser.get_text_from_web_element(_locator="FauxLocator")
        expected_text = 'Unit Test'
        self.assertEqual(first=text_received, second=expected_text)

    def test_get_text_from_web_element_fail_1(self):
        """ Verifies exception and message when unable to locate an element. """
        _locator = "FauxLocator"
        self.mock_web_driver.find_element = mock.MagicMock(side_effect=NoSuchElementException)
        self.browser.wait_for_element_visible = self.mock_wait_for_element_visible
        with self.assertRaises(NoSuchElementException) as context:
            self.browser.get_text_from_web_element(_locator=_locator)
        expected_msg = "Unable to locate web element [id={0}]".format(_locator)
        self.assertEqual(first=expected_msg, second=context.exception.msg)

    def test_get_text_from_web_element_fail_2(self):
        """ Verifies exception and message when able to locate an element but doesn't have an innerHTML text field. """
        _locator = "FauxLocator"
        mock_web_element = mock.MagicMock(spec=WebElement, innerHTML='Unit Test')
        del mock_web_element.text
        mock_web_element.get_attribute.side_effect = AttributeError
        self.mock_web_driver.find_element = mock.MagicMock(return_value=mock_web_element)
        self.browser.wait_for_element_visible = self.mock_wait_for_element_visible
        with self.assertRaises(AttributeError) as context:
            self.browser.get_text_from_web_element(_locator=_locator)
        expected_msg = "Unable to get 'innerHTML' text from web element [id={0}]".format(_locator)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_send_text_to_element_pass_1(self):
        """ Verifies ability to locate a web element, and send text to the element. """
        _locator = (By.ID, "Ben")
        _text = "Unit Test"
        _test_pass = False

        mock_web_element = mock.MagicMock(spec=WebElement)
        self.mock_web_driver.find_element = mock.MagicMock(return_value=mock_web_element)
        self.browser.wait_for_element_visible = self.mock_wait_for_element_visible
        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                self.browser.send_text_to_element(_locator=_locator, text_to_send=_text)

        # Catches an Assertion Error from above, meaning 'send_text_to_element' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                _test_pass = True

        failed_message = "Expected no Exception to be raised, Exception raised."
        self.assertEqual(_test_pass, True, failed_message)

    def test_send_text_to_element_pass_2(self):
        """ Verifies ability to locate a web element, clear the input field and send text to the element. """
        _locator = (By.ID, "Ben")
        _text = "Unit Test"
        _test_pass = False

        mock_web_element = mock.MagicMock(spec=WebElement)
        self.mock_web_driver.find_element = mock.MagicMock(return_value=mock_web_element)
        self.browser.wait_for_element_visible = self.mock_wait_for_element_visible
        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                self.browser.send_text_to_element(_locator=_locator, text_to_send=_text, clear_field=True)

        # Catches an Assertion Error from above, meaning 'send_text_to_element' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                _test_pass = True

        failed_message = "Expected no Exception to be raised, Exception raised."
        self.assertEqual(_test_pass, True, failed_message)

    def test_send_text_to_element_pass_3(self):
        """ Verifies ability to retry locate a web element once, clear the input field and send text to the element. """
        _locator = (By.ID, "Ben")
        _text = "Unit Test"
        _test_pass = False

        mock_web_element = mock.MagicMock(spec=WebElement)
        self.mock_web_driver.find_element = mock.MagicMock(side_effect=[NoSuchElementException, mock_web_element])
        self.browser.wait_for_element_visible = self.mock_wait_for_element_visible
        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                self.browser.send_text_to_element(_locator=_locator, text_to_send=_text, clear_field=True)

        # Catches an Assertion Error from above, meaning 'send_text_to_element' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                _test_pass = True

        failed_message = "Expected no Exception to be raised, Exception raised."
        self.assertEqual(_test_pass, True, failed_message)

    def test_send_text_to_element_pass_4(self):
        """ Verifies ability to retry locate a web element twice, clear the input field and send text to the element."""
        _locator = (By.ID, "Ben")
        _text = "Unit Test"
        _test_pass = False

        mock_web_element = mock.MagicMock(spec=WebElement)
        _side_effects = [NoSuchElementException, NoSuchElementException, mock_web_element]
        self.mock_web_driver.find_element = mock.MagicMock(side_effect=_side_effects)
        self.browser.wait_for_element_visible = self.mock_wait_for_element_visible
        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                self.browser.send_text_to_element(_locator=_locator, text_to_send=_text, clear_field=True)

        # Catches an Assertion Error from above, meaning 'send_text_to_element' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                _test_pass = True

        failed_message = "Expected no Exception to be raised, Exception raised."
        self.assertEqual(_test_pass, True, failed_message)

    def test_send_text_to_element_pass_5(self):
        """ Verifies ability to clear the web element, and make sure no errors are thrown. """
        _locator = (By.ID, "Ben")
        _text = "Unit Test"
        _test_pass = False

        mock_web_element = mock.MagicMock(spec=WebElement)
        self.mock_web_driver.find_element = mock.MagicMock(return_value=mock_web_element)
        self.browser.wait_for_element_visible = self.mock_wait_for_element_visible
        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                self.browser.send_text_to_element(_locator=_locator, text_to_send=_text, clear_field=True, drop_down_element=True)

        # Catches an Assertion Error from above, meaning 'send_text_to_element' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                _test_pass = True

        failed_message = "Expected no Exception to be raised, Exception raised."
        self.assertEqual(_test_pass, True, failed_message)

    def test_send_text_to_element_fail_1(self):
        """ Verifies exception and message when unable to locate element to send text to after 3 tries. """
        _locator = (By.ID, "Ben")
        _text = "Unit Test"

        _side_effects = [NoSuchElementException, NoSuchElementException, NoSuchElementException]
        self.mock_web_driver.find_element = mock.MagicMock(side_effect=_side_effects)
        self.browser.wait_for_element_visible = self.mock_wait_for_element_visible
        with self.assertRaises(ValueError) as context:
            self.browser.send_text_to_element(_locator=_locator, text_to_send=_text)
        expected_msg = "Failed to send text on third attempt to web element: {0}. ''".format(_locator)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_find_element_then_click_pass_1(self):
        """ Successfully finding an element on the page, moving to it and clicking it using a locator """
        _locator = (By.ID, "Ben")
        _test_pass = False

        mock_web_element = mock.MagicMock(spec=WebElement)
        self.mock_web_driver.find_element = mock.MagicMock(return_value=mock_web_element)
        self.browser.wait_for_element_visible = self.mock_wait_for_element_visible

        # Mock action chains class
        mock_action_chains = mock.MagicMock(spec=ActionChains)
        mock_action_chains.perform.side_effect = None

        # Set the built in python ActionChains import to our mocked ActionChains
        previous = wd.ActionChains
        wd.ActionChains = mock_action_chains

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                self.browser.find_element_then_click(_locator=_locator)

        # Catches an Assertion Error from above, meaning 'send_text_to_element' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                _test_pass = True

        failed_message = "Expected no Exception to be raised, Exception raised."
        self.assertEqual(_test_pass, True, failed_message)
        wd.ActionChains = previous

    def test_find_element_then_click_fail_1(self):
        """ Verifies exception and message handling when webdriver is unable to find the element. """
        _locator = (By.ID, "Ben")

        self.mock_web_driver.find_element = mock.MagicMock(side_effect=NoSuchElementException)
        self.browser.wait_for_element_visible = self.mock_wait_for_element_visible

        with self.assertRaises(Exception) as context:
            self.browser.find_element_then_click(_locator=_locator)

        expected_msg = "Unable to locate element to click: {0}. Exception: {1}".format(_locator, '')
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_find_element_then_click_fail_2(self):
        """ Verifies exception and message handling when web element becomes stale (page refresh). """
        _locator = (By.ID, "Ben")

        wait_for_ele_exception_msg = "Webdriver unable to locate element: '{0}'.".format(_locator)
        self.mock_wait_for_element_visible.side_effect = WebDriverException(wait_for_ele_exception_msg)
        self.mock_find_element.side_effect = WebDriverException(wait_for_ele_exception_msg)
        self.browser.web_driver.find_element = self.mock_find_element

        with self.assertRaises(Exception) as context:
            self.browser.find_element_then_click(_locator=_locator)

        expected_msg = "Unable to locate element to click: {0}. Exception: {1}".format(_locator,
                                                                                       wait_for_ele_exception_msg)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_get_status_pass_1(self):
        """ Verifies a status is returned which corresponds to the current status of the Base manager web element """
        _id_locator = (By.ID, "Bens ID")

        mock_web_element = mock.MagicMock(spec=WebElement)
        mock_web_element.value_of_css_property.return_value = (112, 191, 65)

        self.mock_web_driver.find_element = mock.MagicMock(return_value=mock_web_element)
        hex_returned = self.browser.get_status(_locator=_id_locator)

        expected_return_value = "Done"
        self.assertEqual(first=hex_returned, second=expected_return_value)

    def test_get_status_fail_1(self):
        """ Verifies correct exception and message when unable to locate the web element """
        _id_locator = (By.ID, "Bens ID")

        mock_web_element = mock.MagicMock(spec=WebElement)
        mock_web_element.value_of_css_property.return_value = (112, 191, 65)

        self.mock_web_driver.find_element = mock.MagicMock(side_effect=NoSuchElementException("Foo"))

        with self.assertRaises(NoSuchElementException) as context:
            self.browser.get_status(_locator=_id_locator)

        expected_msg = "Unable to get status for element: {0}. {1}".format(_id_locator, "Foo")
        self.assertEqual(first=expected_msg, second=context.exception.msg)

    def test_get_status_fail_2(self):
        """ Verifies correct exception and message when the web element doesn't have the css property we need """
        _id_locator = (By.ID, "Bens ID")

        mock_web_element = mock.MagicMock(spec=WebElement)
        mock_web_element.value_of_css_property.return_value = (112, 191, 65)

        self.mock_web_driver.find_element = mock.MagicMock(side_effect=NoSuchAttributeException("Foo"))

        with self.assertRaises(NoSuchAttributeException) as context:
            self.browser.get_status(_locator=_id_locator)

        expected_msg = "Unable to get status for element: {0}. {1}".format(_id_locator, "Foo")
        self.assertEqual(first=expected_msg, second=context.exception.msg)

    def test_get_status_fail_3(self):
        """ Verifies correct exception and message when an unknown status is received """
        _id_locator = (By.ID, "Bens ID")

        mock_web_element = mock.MagicMock(spec=WebElement)
        mock_web_element.value_of_css_property.return_value = (112, 191, 61)

        self.mock_web_driver.find_element = mock.MagicMock(return_value=mock_web_element)

        with self.assertRaises(KeyError) as context:
            self.browser.get_status(_locator=_id_locator)

        expected_msg = "Color from browser not recognized: #70bf3d. Known colors status: ['', '#c82605', 'Rain Delay'," \
                       " 'Soaking', 'Watering', '#70bf41', 'Waiting', '#ff9b34', 'Online', '#f6d328', 'Paused', 'Okay'," \
                       " 'Off', '#0155a4', 'Error', '#ff2f92', '#a349a41', 'Disabled', 'Unassigned', 'Done', " \
                       "'No Connection', '#a6aaa9', '#57a7f9']"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_wait_for_element_text_change_pass_1(self):
        """ Verifies a successful attempt at waiting for a web element to contain specific text """
        _locator = (By.ID, "Bens ID")
        _text = "Brice"

        # Mock the python built-in WebDriverWait's output
        self.mock_until.return_value = True
        self.mock_web_driver.find_element = self.mock_find_element

        # NOTE: previous_until = wd.WebDriverWait is needed because if we don't reassign wd.WebDriverWait at the end,
        # the next unit test that uses wd.WebDriverWait will use this mock instead of the correct one,
        # causing failures when there shouldn't be
        previous_until = wd.WebDriverWait.until
        wd.WebDriverWait.until = self.mock_until

        found = self.browser.wait_for_element_text_change(_locator, _text)
        self.assertTrue(found)
        # re-assign previous until pointer
        wd.WebDriverWait.until = previous_until

    def test_wait_for_element_text_change_fail_1(self):
        """ Verifies exception and message when unable to wait for the text to be present in the element (timeout) """
        _locator = (By.ID, "Bens ID")
        _text1 = "Brice"
        _text2 = "Foo"

        mock_web_element = mock.MagicMock(side_effect=None)
        mock_web_element.text = _text2
        self.mock_find_element.return_value = mock_web_element

        # Mock the python built-in WebDriverWait's output
        self.mock_web_driver_wait.side_effect = TimeoutException(_text2)
        self.mock_web_driver.find_element = self.mock_find_element

        # NOTE: previous = wd.WebDriverWait is needed because if we don't reassign wd.WebDriverWait at the end,
        # the next unit test that uses wd.WebDriverWait will use this mock instead of the correct one,
        # causing failures when there shouldn't be
        # Save WebDriverWait library to 'previous' variable so we can overwrite with a mock version for test
        previous = wd.WebDriverWait
        wd.WebDriverWait = self.mock_web_driver_wait

        with self.assertRaises(TimeoutException) as context:
            self.browser.wait_for_element_text_change(_locator, _text1)

        expected_msg = "Timed Out: Webdriver unable to wait for element text to change to: {0}. Current text: {" \
                       "1}. Exception: {2}".format(
            _text1,
            _text2,
            _text2
        )
        self.assertEqual(first=expected_msg, second=context.exception.msg)

        # Re-assign python library from mock to built-in
        wd.WebDriverWait = previous

    def test_handle_dialog_box_pass_1(self):
        """ Verifies a successful attempt at handling the 'loading' info dialog box. """
        _test_pass = False

        # Use side_effect with true first to make iteration go inside the while loop once
        mock_is_visible = mock.MagicMock(side_effect=[True, True, False])
        self.browser.is_visible = mock_is_visible
        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                self.browser.handle_dialog_box()

        # Catches an Assertion Error from above, meaning 'send_text_to_element' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                _test_pass = True

        failed_message = "Expected no Exception to be raised, Exception raised."
        self.assertEqual(_test_pass, True, failed_message)
        mock_is_visible.assert_any_call(DialogBoxLocators.INFO_DIALOG)

    def test_handle_dialog_box_fail_1(self):
        """ Verifies execution tries to continue after an unsuccessful attempt at locating the dialog box """
        _test_pass = False

        is_visible_returns = [False, False, False, False, False, False, False, False, False, False, False]

        # Use side_effect with false first to make iteration go inside the while loop once
        mock_is_visible = mock.MagicMock(side_effect=is_visible_returns)
        self.browser.is_visible = mock_is_visible
        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                self.browser.handle_dialog_box()

        # Catches an Assertion Error from above, meaning 'send_text_to_element' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                _test_pass = True

        failed_message = "Expected no Exception to be raised, Exception raised."
        self.assertEqual(_test_pass, True, failed_message)
        mock_is_visible.assert_any_call(DialogBoxLocators.INFO_DIALOG)

    def test_is_visible_pass_1(self):
        """ Verifies a successful attempt at waiting for a web element to become visible for the user """
        _test_locator = (By.ID, "Bens ID")
        # NOTE: previous = wd.WebDriverWait is needed because if we don't reassign wd.WebDriverWait at the end,
        # the next unit test that uses wd.WebDriverWait will use this mock instead of the correct one,
        # causing failures when there shouldn't be
        self.mock_until.return_value = True
        previous = wd.WebDriverWait.until
        wd.WebDriverWait.until = self.mock_until
        self.assertTrue(self.browser.is_visible(_locator=_test_locator))
        wd.WebDriverWait.until = previous

    def test_is_visible_fail_1(self):
        """ Verifies a False return value when unable to locate the element or timed out waiting for element """
        _test_locator = (By.ID, "Bens ID")
        # NOTE: previous = wd.WebDriverWait is needed because if we don't reassign wd.WebDriverWait at the end,
        # the next unit test that uses wd.WebDriverWait will use this mock instead of the correct one,
        # causing failures when there shouldn't be
        self.mock_web_driver_wait.side_effect = TimeoutException
        previous = wd.WebDriverWait
        wd.WebDriverWait = self.mock_web_driver_wait
        self.assertFalse(self.browser.is_visible(_locator=_test_locator))
        wd.WebDriverWait = previous

    def test_is_clickable_pass_1(self):
        """ Verifies a successful attempt at waiting for a web element to become click-able for the user """
        _test_locator = (By.ID, "Bens ID")
        # NOTE: previous = wd.WebDriverWait is needed because if we don't reassign wd.WebDriverWait at the end,
        # the next unit test that uses wd.WebDriverWait will use this mock instead of the correct one,
        # causing failures when there shouldn't be
        self.mock_until.return_value = True
        previous = wd.WebDriverWait.until
        wd.WebDriverWait.until = self.mock_until
        self.assertTrue(self.browser.is_clickable(_locator=_test_locator))
        wd.WebDriverWait.until = previous

    def test_is_clickable_fail_1(self):
        """ Verifies a False return value when unable to locate the element or timed out waiting for element """
        _test_locator = (By.ID, "Bens ID")
        # NOTE: previous = wd.WebDriverWait is needed because if we don't reassign wd.WebDriverWait at the end,
        # the next unit test that uses wd.WebDriverWait will use this mock instead of the correct one,
        # causing failures when there shouldn't be
        self.mock_web_driver_wait.side_effect = TimeoutException
        previous = wd.WebDriverWait
        wd.WebDriverWait = self.mock_web_driver_wait
        self.assertFalse(self.browser.is_clickable(_locator=_test_locator))
        wd.WebDriverWait = previous

    def test_wait_for_element_clickable_pass_1(self):
        """ Verifies a successful attempt at waiting for a web element to become click-able for the user """
        _test_locator = (By.ID, "Bens ID")

        # NOTE: previous = wd.WebDriverWait is needed because if we don't reassign wd.WebDriverWait at the end,
        # the next unit test that uses wd.WebDriverWait will use this mock instead of the correct one,
        # causing failures when there shouldn't be
        self.mock_until.return_value = True
        previous = wd.WebDriverWait.until
        wd.WebDriverWait.until = self.mock_until

        found = self.browser.wait_for_element_clickable(_locator=_test_locator)
        self.assertTrue(found)
        wd.WebDriverWait.until = previous

    def test_wait_for_element_clickable_fail_1(self):
        """ Verifies exception and message when unable to wait for the web element to become click-able for the user """
        _test_locator = (By.ID, "Bens ID")
        _exception_text = "Foo"

        # NOTE: previous = wd.WebDriverWait is needed because if we don't reassign wd.WebDriverWait at the end,
        # the next unit test that uses wd.WebDriverWait will use this mock instead of the correct one,
        # causing failures when there shouldn't be
        self.mock_web_driver_wait.side_effect = TimeoutException(_exception_text)
        previous = wd.WebDriverWait
        wd.WebDriverWait = self.mock_web_driver_wait

        with self.assertRaises(TimeoutException) as context:
            self.browser.wait_for_element_clickable(_locator=_test_locator)

        expected_msg = "Webdriver timed out waiting for: '{0}' to be clickable. {1}".format(
            _test_locator,
            _exception_text
        )
        self.assertEqual(first=expected_msg, second=context.exception.msg)
        wd.WebDriverWait = previous

    def test_wait_for_element_visible_pass_1(self):
        """ Verifies a successful attempt at waiting for a web element to become visible to the user """
        _test_locator = (By.ID, "Bens ID")

        # NOTE: previous = wd.WebDriverWait is needed because if we don't reassign wd.WebDriverWait at the end,
        # the next unit test that uses wd.WebDriverWait will use this mock instead of the correct one,
        # causing failures when there shouldn't be
        self.mock_until.return_value = True
        previous = wd.WebDriverWait.until
        wd.WebDriverWait.until = self.mock_until

        found = self.browser.wait_for_element_visible(_locator=_test_locator)
        self.assertTrue(found)
        wd.WebDriverWait.until = previous

    def test_wait_for_element_visible_fail_1(self):
        """ Verifies exception and message when unable to wait for the web element to become visible to the user """
        _test_locator = (By.ID, "Bens ID")
        _exception_text = "Foo"

        # NOTE: previous = wd.WebDriverWait is needed because if we don't reassign wd.WebDriverWait at the end,
        # the next unit test that uses wd.WebDriverWait will use this mock instead of the correct one,
        # causing failures when there shouldn't be
        self.mock_web_driver_wait.side_effect = TimeoutException(_exception_text)
        previous = wd.WebDriverWait
        wd.WebDriverWait = self.mock_web_driver_wait

        with self.assertRaises(TimeoutException) as context:
            self.browser.wait_for_element_visible(_locator=_test_locator)

        expected_msg = "Webdriver unable to locate element: '{0}'. {1}".format(
            _test_locator,
            _exception_text
        )
        self.assertEqual(first=expected_msg, second=context.exception.msg)
        wd.WebDriverWait = previous




