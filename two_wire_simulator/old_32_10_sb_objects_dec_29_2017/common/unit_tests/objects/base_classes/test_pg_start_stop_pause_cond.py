__author__ = 'Eldinner Plate'

import unittest
import mock
import serial

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes, types

from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.pg_start_stop_pause_cond import BaseStartStopPause

from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr


class TestBaseStartStopPause(unittest.TestCase):
    """
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        BaseStartStopPause.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_start_stop_pause_object(self, event_type, cn_type):
        """ Creates a new start stop pause object for testing purposes """
        ssp = BaseStartStopPause(program_ad=1, event_type=event_type, cn_type=cn_type)

        return ssp

    def test_set_moisture_condition_on_pg_pass_1(self):
        """ Test Set Moisture Condition On Program Pass Case 1: Pass in a valid upper limit start condition
        Create the test object
        Create variables that will be passed into the method
        Create an expected command with the variables that are going to be passed in
        Call the method
        Assert that the mocked send_and_wait_for_reply is called with the expected command
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_start_condition, cn_type="10")
        pg_ad = 1
        ms_ser = "foo"
        threshold = 10

        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13}".format(
                opcodes.program_start_condition,
                str(pg_ad),
                opcodes.type,
                opcodes.moisture_sensor,
                opcodes.moisture_sensor,
                ms_ser,
                opcodes.moisture_mode,
                opcodes.upper_limit,
                opcodes.moisture_trigger_threshold,
                str(threshold),
                opcodes.date_time,
                opcodes.true,
                opcodes.calibrate_cycle,
                opcodes.program_start_condition
            )

        ssp.set_moisture_condition_on_pg(serial_number=ms_ser, mode=opcodes.upper_limit, threshold=threshold, dt=opcodes.true,
                                         cc=opcodes.program_start_condition)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_moisture_condition_on_pg_pass_2(self):
        """ Test Set Moisture Condition On Program Pass Case 2: Pass in a valid lower limit pause condition
        Create the test object
        Create variables that will be passed into the method
        Create an expected command with the variables that are going to be passed in
        Call the method
        Assert that the mocked send_and_wait_for_reply is called with the expected command
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_pause_condition, cn_type="32")
        pg_ad = 1
        ms_ser = "foo"
        threshold = 10
        ms_pause_time = 60

        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9}".format(
                opcodes.program_pause_condition,
                str(pg_ad),
                opcodes.moisture_sensor,
                ms_ser,
                opcodes.moisture_mode,
                opcodes.lower_limit,
                opcodes.moisture_trigger_threshold,
                str(threshold),
                opcodes.moisture_pause_time,
                str(ms_pause_time)
            )

        ssp.set_moisture_condition_on_pg(serial_number=ms_ser, mode=opcodes.lower_limit, threshold=threshold,
                                         pt=ms_pause_time)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_moisture_condition_on_pg_pass_3(self):
        """ Test Set Moisture Condition On Program Pass Case 3: Pass in a valid stop condition that has a mode 'off'
        Create the test object
        Create variables that will be passed into the method
        Create an expected command with the variables that are going to be passed in
        Call the method
        Assert that the mocked send_and_wait_for_reply is called with the expected command
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="32")
        pg_ad = 1
        ms_ser = "foo"

        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7}".format(
                opcodes.program_stop_condition,
                str(pg_ad),
                opcodes.moisture_sensor,
                ms_ser,
                opcodes.moisture_mode,
                opcodes.off,
                opcodes.stop_immediately,
                opcodes.true
            )

        ssp.set_moisture_condition_on_pg(serial_number=ms_ser, mode=opcodes.off, si=opcodes.true)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_moisture_condition_on_pg_fail_1(self):
        """ Test Set Moisture Condition On Program Fail Case 1: Pass in an upper limit command without a threshold
        Create the test object
        Create variables that will be passed into the method
        Create an expected error message that we will compare against what the method returns
        Call the method and expect an error
        Compare the error message we created with the actual error message that was raised
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="32")
        ms_ser = "foo"

        expected_msg = "Since the mode that was passed in was '{0}', a threshold value is expected. Please pass in a " \
                       "threshold value when using this method.".format(opcodes.upper_limit)

        with self.assertRaises(ValueError) as context:
            ssp.set_moisture_condition_on_pg(serial_number=ms_ser, mode=opcodes.upper_limit, si=opcodes.true)

        self.assertEqual(expected_msg, context.exception.message)

    def test_set_moisture_condition_on_pg_fail_2(self):
        """ Test Set Moisture Condition On Program Fail Case 2: Pass in an invalid 'mode'
        Create the test object
        Create variables that will be passed into the method
        Create an expected error message that we will compare against what the method returns
        Call the method and expect an error
        Compare the error message we created with the actual error message that was raised
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="32")
        ms_ser = "foo"
        invalid_mode = 'abc'

        expected_msg = "The 'mode' variable that was passed in is not valid. Expected: '{0}', '{1}', or '{2}'. " \
                       "Received: '{3}'".format(opcodes.off, opcodes.upper_limit, opcodes.lower_limit, invalid_mode)

        with self.assertRaises(ValueError) as context:
            ssp.set_moisture_condition_on_pg(serial_number=ms_ser, mode=invalid_mode, si=opcodes.true)

        self.assertEqual(expected_msg, context.exception.message)

    def test_set_moisture_condition_on_pg_fail_3(self):
        """ Test Set Moisture Condition On Program Fail Case 3: send_and_wait_for_reply raises an exception
        Create the test object
        Create variables that will be passed into the method
        Make the mock_send_and_wait_for_reply method throw an Exception when it is called
        Create the base string that will be used in the error message
        Call the method and expect an error
        Compare the error message we created with the actual error message that was raised
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="32")
        pg_ad = 1
        ms_ser = "foo"

        self.mock_send_and_wait_for_reply.side_effect = Exception

        base_str = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.program_stop_condition,
            str(pg_ad),
            opcodes.moisture_sensor,
            ms_ser,
            opcodes.moisture_mode,
            opcodes.off
        )

        expected_msg = "Exception occurred trying to set moisture condition for\nCN: {0}\nevent type: {1}" \
                       "\ncommand: {2}".format(
                           ssp._cn_type,
                           ssp._event_type,
                           base_str
                       )

        with self.assertRaises(Exception) as context:
            ssp.set_moisture_condition_on_pg(serial_number=ms_ser, mode=opcodes.off)

        self.assertEqual(expected_msg, context.exception.message)

    def test_set_temperature_condition_on_pg_pass_1(self):
        """ Test Set Temperature Condition On Program Pass Case 1: Pass in a valid upper limit start condition
        Create the test object
        Create variables that will be passed into the method
        Create an expected command with the variables that are going to be passed in
        Call the method
        Assert that the mocked send_and_wait_for_reply is called with the expected command
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_start_condition, cn_type="10")
        pg_ad = 1
        ts_ser = "foo"
        threshold = 10

        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11}".format(
                opcodes.program_start_condition,
                str(pg_ad),
                opcodes.type,
                opcodes.temperature_sensor,
                opcodes.temperature_sensor,
                ts_ser,
                opcodes.temperature_sensor_mode,
                opcodes.upper_limit,
                opcodes.temperature_trigger_threshold,
                str(threshold),
                opcodes.date_time,
                opcodes.true
            )

        ssp.set_temperature_condition_on_pg(serial_number=ts_ser, mode=opcodes.upper_limit, threshold=threshold,
                                            dt=opcodes.true)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_temperature_condition_on_pg_pass_2(self):
        """ Test Set Temperature Condition On Program Pass Case 2: Pass in a valid lower limit pause condition
        Create the test object
        Create variables that will be passed into the method
        Create an expected command with the variables that are going to be passed in
        Call the method
        Assert that the mocked send_and_wait_for_reply is called with the expected command
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_pause_condition, cn_type="32")
        pg_ad = 1
        ts_ser = "foo"
        threshold = 10
        ts_pause_time = 60

        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9}".format(
                opcodes.program_pause_condition,
                str(pg_ad),
                opcodes.temperature_sensor,
                ts_ser,
                opcodes.temperature_sensor_mode,
                opcodes.lower_limit,
                opcodes.temperature_trigger_threshold,
                str(threshold),
                opcodes.temperature_pause_time,
                str(ts_pause_time * 60)
            )

        ssp.set_temperature_condition_on_pg(serial_number=ts_ser, mode=opcodes.lower_limit, threshold=threshold,
                                            pt=ts_pause_time)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_temperature_condition_on_pg_pass_3(self):
        """ Test Set Temperature Condition On Program Pass Case 3: Pass in a valid stop condition that has a mode 'off'
        Create the test object
        Create variables that will be passed into the method
        Create an expected command with the variables that are going to be passed in
        Call the method
        Assert that the mocked send_and_wait_for_reply is called with the expected command
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="32")
        pg_ad = 1
        ts_ser = "foo"

        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7}".format(
                opcodes.program_stop_condition,
                str(pg_ad),
                opcodes.temperature_sensor,
                ts_ser,
                opcodes.temperature_sensor_mode,
                opcodes.off,
                opcodes.stop_immediately,
                opcodes.true
            )

        ssp.set_temperature_condition_on_pg(serial_number=ts_ser, mode=opcodes.off, si=opcodes.true)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_temperature_condition_on_pg_fail_1(self):
        """ Test Set Temperature Condition On Program Fail Case 1: Pass in an upper limit command without a threshold
        Create the test object
        Create variables that will be passed into the method
        Create an expected error message that we will compare against what the method returns
        Call the method and expect an error
        Compare the error message we created with the actual error message that was raised
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="32")
        ts_ser = "foo"

        expected_msg = "Since the mode that was passed in was '{0}', a threshold value is expected. Please pass in a " \
                       "threshold value when using this method.".format(opcodes.upper_limit)

        with self.assertRaises(ValueError) as context:
            ssp.set_temperature_condition_on_pg(serial_number=ts_ser, mode=opcodes.upper_limit, si=opcodes.true)

        self.assertEqual(expected_msg, context.exception.message)

    def test_set_temperature_condition_on_pg_fail_2(self):
        """ Test Set Temperature Condition On Program Fail Case 2: Pass in an invalid 'mode'
        Create the test object
        Create variables that will be passed into the method
        Create an expected error message that we will compare against what the method returns
        Call the method and expect an error
        Compare the error message we created with the actual error message that was raised
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="32")
        ts_ser = "foo"
        invalid_mode = 'abc'

        expected_msg = "The 'mode' variable that was passed in is not valid. Expected: '{0}', '{1}', or '{2}'. " \
                       "Received: '{3}'".format(opcodes.off, opcodes.upper_limit, opcodes.lower_limit, invalid_mode)

        with self.assertRaises(ValueError) as context:
            ssp.set_temperature_condition_on_pg(serial_number=ts_ser, mode=invalid_mode, si=opcodes.true)

        self.assertEqual(expected_msg, context.exception.message)

    def test_set_temperature_condition_on_pg_fail_3(self):
        """ Test Set Temperature Condition On Program Fail Case 3: send_and_wait_for_reply raises an exception
        Create the test object
        Create variables that will be passed into the method
        Make the mock_send_and_wait_for_reply method throw an Exception when it is called
        Create the base string that will be used in the error message
        Call the method and expect an error
        Compare the error message we created with the actual error message that was raised
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="32")
        pg_ad = 1
        ts_ser = "foo"

        self.mock_send_and_wait_for_reply.side_effect = Exception

        base_str = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.program_stop_condition,
            str(pg_ad),
            opcodes.temperature_sensor,
            ts_ser,
            opcodes.temperature_sensor_mode,
            opcodes.off
        )

        expected_msg = "Exception occurred trying to set temperature condition for\nCN: {0}\nevent type: {1}" \
                       "\ncommand: {2}".format(
                           ssp._cn_type,
                           ssp._event_type,
                           base_str
                       )

        with self.assertRaises(Exception) as context:
            ssp.set_temperature_condition_on_pg(serial_number=ts_ser, mode=opcodes.off)

        self.assertEqual(expected_msg, context.exception.message)

    def test_set_event_switch_condition_on_pg_pass_1(self):
        """ Test Set Event Switch Condition On Program Pass Case 1: Pass in a valid open start condition
        Create the test object
        Create variables that will be passed into the method
        Create an expected command with the variables that are going to be passed in
        Call the method
        Assert that the mocked send_and_wait_for_reply is called with the expected command
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_start_condition, cn_type="10")
        pg_ad = 1
        sw_ser = "foo"

        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9}".format(
                opcodes.program_start_condition,
                str(pg_ad),
                opcodes.type,
                opcodes.event_switch,
                opcodes.event_switch,
                sw_ser,
                opcodes.switch_mode,
                opcodes.contacts_open,
                opcodes.date_time,
                opcodes.true
            )

        ssp.set_event_switch_condition_on_pg(serial_number=sw_ser, mode=opcodes.contacts_open, dt=opcodes.true)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_event_switch_condition_on_pg_pass_2(self):
        """ Test Set Event Switch Condition On Program Pass Case 2: Pass in a valid close pause condition
        Create the test object
        Create variables that will be passed into the method
        Create an expected command with the variables that are going to be passed in
        Call the method
        Assert that the mocked send_and_wait_for_reply is called with the expected command
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_pause_condition, cn_type="32")
        pg_ad = 1
        sw_ser = "foo"
        sw_pause_time = 60

        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7}".format(
                opcodes.program_pause_condition,
                str(pg_ad),
                opcodes.event_switch,
                sw_ser,
                opcodes.switch_mode,
                opcodes.closed,
                opcodes.switch_pause_time,
                str(sw_pause_time * 60)
            )

        ssp.set_event_switch_condition_on_pg(serial_number=sw_ser, mode=opcodes.contacts_closed, pt=sw_pause_time)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_event_switch_condition_on_pg_pass_3(self):
        """ Test Set Event Switch Condition On Program Pass Case 3: Pass in a valid stop condition that has a mode 'off'
        Create the test object
        Create variables that will be passed into the method
        Create an expected command with the variables that are going to be passed in
        Call the method
        Assert that the mocked send_and_wait_for_reply is called with the expected command
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="32")
        pg_ad = 1
        sw_ser = "foo"

        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7}".format(
                opcodes.program_stop_condition,
                str(pg_ad),
                opcodes.event_switch,
                sw_ser,
                opcodes.switch_mode,
                opcodes.off,
                opcodes.stop_immediately,
                opcodes.true
            )

        ssp.set_event_switch_condition_on_pg(serial_number=sw_ser, mode=opcodes.off, si=opcodes.true)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_event_switch_condition_on_pg_fail_1(self):
        """ Test Set Event Switch Condition On Program Fail Case 1: Pass in an invalid 'mode'
        Create the test object
        Create variables that will be passed into the method
        Create an expected error message that we will compare against what the method returns
        Call the method and expect an error
        Compare the error message we created with the actual error message that was raised
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="32")
        sw_ser = "foo"
        invalid_mode = 'abc'

        expected_msg = "The 'mode' variable that was passed in is not valid. Expected: '{0}', '{1}', or '{2}'. " \
                       "Received: '{3}'".format(opcodes.off, opcodes.contacts_open, opcodes.contacts_closed,
                                                invalid_mode)

        with self.assertRaises(ValueError) as context:
            ssp.set_event_switch_condition_on_pg(serial_number=sw_ser, mode=invalid_mode, si=opcodes.true)

        self.assertEqual(expected_msg, context.exception.message)

    def test_set_event_switch_condition_on_pg_fail_2(self):
        """ Test Set Event Switch Condition On Program Fail Case 2: send_and_wait_for_reply raises an exception
        Create the test object
        Create variables that will be passed into the method
        Make the mock_send_and_wait_for_reply method throw an Exception when it is called
        Create the base string that will be used in the error message
        Call the method and expect an error
        Compare the error message we created with the actual error message that was raised
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="32")
        pg_ad = 1
        sw_ser = "foo"

        self.mock_send_and_wait_for_reply.side_effect = Exception

        base_str = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.program_stop_condition,
            str(pg_ad),
            opcodes.event_switch,
            sw_ser,
            opcodes.switch_mode,
            opcodes.off
        )

        expected_msg = "Exception occurred trying to set event switch condition for\nCN: {0}\nevent type: {1}" \
                       "\ncommand: {2}".format(
                           ssp._cn_type,
                           ssp._event_type,
                           base_str
                       )

        with self.assertRaises(Exception) as context:
            ssp.set_event_switch_condition_on_pg(serial_number=sw_ser, mode=opcodes.off)

        self.assertEqual(expected_msg, context.exception.message)

    def test_get_data_pass_1(self):
        """ Test Get Data Pass Case 1: 1000 controller successfully gets data
        Create the test object
        Create a value we expect will be returned
        Create the command we expect to be passed into the mocked get_and_wait_for_reply method
        Make the mock get_and_wait_for_reply method to return what we expect
        Assert that the method was called correctly and that the data stored was what we expected
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = "Testing"

        expected_command = "GET,{0}={1},{2}={3}".format(
                                ssp._event_type,  # {0}
                                ssp._program_ad,  # {1}
                                opcodes.type,     # {2}
                                ssp._device_type  # {3}
                            )

        self.mock_get_and_wait_for_reply.return_value = return_value

        ssp.get_data()

        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)
        self.assertEqual(ssp.data, return_value)

    def test_get_data_pass_2(self):
        """ Test Get Data Pass Case 2: 3200 controller successfully gets data
        Create the test object
        Create a value we expect will be returned
        Create the command we expect to be passed into the mocked get_and_wait_for_reply method
        Make the mock get_and_wait_for_reply method to return what we expect
        Assert that the method was called correctly and that the data stored was what we expected
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="32")
        return_value = "Testing"
        expected_command = "GET,{0}={1},{2}={3}".format(
                                ssp._event_type,  # {0}
                                ssp._program_ad,  # {1}
                                'None',     # {2}
                                ssp._device_type  # {3}
                            )
        # expected_command = "GET,{0}={1}".format(
        #                         ssp._event_type,  # {0}
        #                         ssp._program_ad,  # {1}
        #                     )

        self.mock_get_and_wait_for_reply.return_value = return_value

        ssp.get_data()

        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)
        self.assertEqual(ssp.data, return_value)

    def test_get_data_fail_1(self):
        """ Test Get Data Fail Case 1: 3200 controller fails to get a reply from the controller
        Create the test object
        Create the error message that will be raised
        Create the command we expect to be passed into the mocked get_and_wait_for_reply method
        Create the error message we expect to be raised
        Make the mock get_and_wait_for_reply method raise an Exception
        Assert that the method raised an Exception when it was called
        Assert that the error message we expected was the same as the one that was actually returned
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="32")
        error_msg = "Testing Error Message"

        command = "GET,{0}={1},{2}={3}".format(
                      ssp._event_type,    # {0}
                      ssp._program_ad,    # {1}
                      'None',             # {2}
                      ssp._device_type    # {3}
                  )

        expected_msg = "Unable to get data for Program: '{0}' using command: '{1}'. Exception raised: {2}".format(
                            str(ssp._program_ad),   # {0}
                            command,                # {1}
                            error_msg               # {2}
                        )

        self.mock_get_and_wait_for_reply.side_effect = Exception(error_msg)

        with self.assertRaises(Exception) as context:
            ssp.get_data()

        self.assertEqual(expected_msg, context.exception.message)

    def test_verify_device_serial_on_cn_pass_1(self):
        """ Test Verify Device Serial On Controller Pass Case 1: Successfully verify the serial number of an ms
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = "foo"

        ssp._device_serial = return_value
        ssp._device_type = opcodes.moisture_sensor

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        ssp.verify_device_serial_on_cn()

        self.assertEqual(return_value, ssp._device_serial)

    def test_verify_device_serial_on_cn_pass_2(self):
        """ Test Verify Device Serial On Controller Pass Case 2: Successfully verify the serial number of an ts
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = "foo"

        ssp._device_serial = return_value
        ssp._device_type = opcodes.temperature_sensor

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        ssp.verify_device_serial_on_cn()

        self.assertEqual(return_value, ssp._device_serial)

    def test_verify_device_serial_on_cn_pass_3(self):
        """ Test Verify Device Serial On Controller Pass Case 3: Successfully verify the serial number of an sw
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = "foo"

        ssp._device_serial = return_value
        ssp._device_type = opcodes.event_switch

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        ssp.verify_device_serial_on_cn()

        self.assertEqual(return_value, ssp._device_serial)

    def test_verify_device_serial_on_cn_fail_1(self):
        """ Test Verify Device Serial On Controller Fail Case 1: The serial number stored in the object doesn't match cn
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = "foo"

        ssp._device_serial = "bar"
        ssp._device_type = opcodes.moisture_sensor

        expected_message = "Unable to verify StartStopPause({0}) Condition: {1}'s device serial number. Received: " \
                           "{2}, Expected: {3}".format(
                               str(ssp._event_type),      # {0}
                               str(ssp._program_ad),      # {1}
                               str(return_value),         # {2}
                               str(ssp._device_serial))   # {3}

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        with self.assertRaises(ValueError) as context:
            ssp.verify_device_serial_on_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_mode_on_cn_pass_1(self):
        """ Test Verify Mode On Controller Pass Case 1: Successfully verify the mode of a ms
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = "foo"

        ssp._mode = return_value
        ssp._device_type = opcodes.moisture_sensor

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        ssp.verify_mode_on_cn()

        self.assertEqual(return_value, ssp._mode)

    def test_verify_mode_on_cn_pass_2(self):
        """ Test Verify Mode On Controller Pass Case 2: Successfully verify the mode of a ts
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = "foo"

        ssp._mode = return_value
        ssp._device_type = opcodes.temperature_sensor

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        ssp.verify_mode_on_cn()

        self.assertEqual(return_value, ssp._mode)

    def test_verify_mode_on_cn_pass_3(self):
        """ Test Verify Mode On Controller Pass Case 3: Successfully verify the mode of a sw
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = "foo"

        ssp._mode = return_value
        ssp._device_type = opcodes.event_switch

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        ssp.verify_mode_on_cn()

        self.assertEqual(return_value, ssp._mode)

    def test_verify_mode_on_cn_fail_1(self):
        """ Test Verify Mode On Controller Fail Case 1: The serial number stored in the object doesn't match cn
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = "foo"

        ssp._mode = "bar"
        ssp._device_type = opcodes.moisture_sensor

        expected_message = "Unable to verify StartStopPause({0}) Condition: {1}'s mode. Received: " \
                           "{2}, Expected: {3}".format(
                               str(ssp._event_type),      # {0}
                               str(ssp._program_ad),      # {1}
                               str(return_value),         # {2}
                               str(ssp._mode))   # {3}

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        with self.assertRaises(ValueError) as context:
            ssp.verify_mode_on_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_mode_on_cn_fail_2(self):
        """ Test Verify Mode On Controller Fail Case 2: The device type in the StartStopPause object doesn't exist
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Call the method and assert that it raises a value error
        Check to make sure the error message returned from the method matches the expected error message
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")

        ssp._device_serial = "bar"
        ssp._device_type = "Invalid type"

        expected_message = "Unable to verify StartStopPause Condition: {0}'s mode. No device type was found. " \
                           "Make sure to use one of the set methods before trying to verify.".format(ssp._program_ad)

        with self.assertRaises(ValueError) as context:
            ssp.verify_mode_on_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_threshold_on_cn_pass_1(self):
        """ Test Verify Threshold On Controller Pass Case 1: Successfully verify the threshold of a ms
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = 123

        ssp._threshold = return_value
        ssp._device_type = opcodes.moisture_sensor

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        ssp.verify_threshold_on_cn()

        self.assertEqual(return_value, ssp._threshold)

    def test_verify_threshold_on_cn_pass_2(self):
        """ Test Verify Threshold On Controller Pass Case 2: Successfully verify the threshold of a ts
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = 123

        ssp._threshold = return_value
        ssp._device_type = opcodes.temperature_sensor

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        ssp.verify_threshold_on_cn()

        self.assertEqual(return_value, ssp._threshold)

    def test_verify_threshold_on_cn_fail_1(self):
        """ Test Verify Threshold On Controller Fail Case 1: The serial number stored in the object doesn't match cn
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = 123.0

        ssp._threshold = 456
        ssp._device_type = opcodes.moisture_sensor

        expected_message = "Unable to verify StartStopPause({0}) Condition: {1}'s threshold. Received: " \
                           "{2}, Expected: {3}".format(
                               str(ssp._event_type),      # {0}
                               str(ssp._program_ad),      # {1}
                               str(return_value),         # {2}
                               str(ssp._threshold))   # {3}

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        with self.assertRaises(ValueError) as context:
            ssp.verify_threshold_on_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_threshold_on_cn_fail_2(self):
        """ Test Verify Threshold On Controller Fail Case 2: The device type in the StartStopPause object doesn't exist
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Call the method and assert that it raises a value error
        Check to make sure the error message returned from the method matches the expected error message
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")

        ssp._device_type = "Invalid type"

        expected_message = "Unable to verify StartStopPause Condition: {0}'s threshold. No device type was found. " \
                           "Make sure to use one of the set methods before trying to verify.".format(ssp._program_ad)

        with self.assertRaises(ValueError) as context:
            ssp.verify_threshold_on_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_pause_time_on_cn_pass_1(self):
        """ Test Verify Pause Time On Controller Pass Case 1: Successfully verify the pause time of a ms
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = 123.0

        ssp._pause_time = return_value
        ssp._device_type = opcodes.moisture_sensor

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value*60)

        ssp.verify_pause_time_on_cn()

        self.assertEqual(return_value, ssp._pause_time)

    def test_verify_pause_time_on_cn_pass_2(self):
        """ Test Verify Pause Time On Controller Pass Case 2: Successfully verify the pause time of a ts
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = 123.0

        ssp._pause_time = return_value
        ssp._device_type = opcodes.temperature_sensor

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value*60)

        ssp.verify_pause_time_on_cn()

        self.assertEqual(return_value, ssp._pause_time)

    def test_verify_pause_time_on_cn_pass_3(self):
        """ Test Verify Pause Time On Controller Pass Case 3: Successfully verify the pause time of a sw
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = 123.0

        ssp._pause_time = return_value
        ssp._device_type = opcodes.event_switch

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value*60)

        ssp.verify_pause_time_on_cn()

        self.assertEqual(return_value, ssp._pause_time)

    def test_verify_pause_time_on_cn_fail_1(self):
        """ Test Verify Pause Time On Controller Fail Case 1: The serial number stored in the object doesn't match cn
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = 123.0

        ssp._pause_time = "bar"
        ssp._device_type = opcodes.moisture_sensor

        expected_message = "Unable to verify StartStopPause({0}) Condition: {1}'s pause time. Received: " \
                           "{2}, Expected: {3}".format(
                               str(ssp._event_type),      # {0}
                               str(ssp._program_ad),      # {1}
                               str(return_value),         # {2}
                               str(ssp._pause_time))   # {3}

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        with self.assertRaises(ValueError) as context:
            ssp.verify_pause_time_on_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_pause_time_on_cn_fail_2(self):
        """ Test Verify Pause Time On Controller Fail Case 2: The device type in the StartStopPause object doesn't exist
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Call the method and assert that it raises a value error
        Check to make sure the error message returned from the method matches the expected error message
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")

        ssp._device_serial = "bar"
        ssp._device_type = "Invalid type"

        expected_message = "Unable to verify StartStopPause Condition: {0}'s pause time. No device type was found. " \
                           "Make sure to use one of the set methods before trying to verify.".format(ssp._program_ad)

        with self.assertRaises(ValueError) as context:
            ssp.verify_pause_time_on_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_stop_immediately_on_cn_pass_1(self):
        """ Test Verify Pause Time On Controller Pass Case 1: Successfully verify the pause time of a ms
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = 'foo'

        ssp._si = return_value
        ssp._device_type = opcodes.moisture_sensor

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        ssp.verify_stop_immediately_on_cn()

        self.assertEqual(return_value, ssp._si)

    def test_verify_stop_on_cn_fail_1(self):
        """ Test Verify Pause Time On Controller Fail Case 1: The serial number stored in the object doesn't match cn
        Create the test object
        Create an arbitrary variable to use throughout the method as a return value
        Assign values to the StartStopPause object so it goes through the corrects parts of the method
        Mock: data so we don't have to worry about instantiating it
            Return: nothing
        Mock: get_value_string_by_key
            Return: 'foo' it doesn't matter what we return as long as we can verify it
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        return_value = "foo"

        ssp._si = "bar"
        ssp._device_type = opcodes.moisture_sensor

        expected_message = "Unable to verify StartStopPause({0}) Condition: {1}'s stop immediately. Received: " \
                           "{2}, Expected: {3}".format(
                               str(ssp._event_type),      # {0}
                               str(ssp._program_ad),      # {1}
                               str(return_value),         # {2}
                               str(ssp._si))      # {3}

        ssp.data = mock.MagicMock()
        ssp.data.get_value_string_by_key = mock.MagicMock(return_value=return_value)

        with self.assertRaises(ValueError) as context:
            ssp.verify_stop_immediately_on_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_who_i_am_pass_1(self):
        """ Test Verify Who I Am Pass Case 1: Verify that it runs through the method cleanly
        Create the test object
        Assign the variables that will be used in the method
        Mock all the methods because we test each of them individually and we don't want to jump outside of the method
        Call the method
        Since nothing is returned or changed we don't have anything new to assert
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        ssp._threshold = 123
        ssp._pause_time = 123
        ssp._si = 'foo'

        ssp.get_data = mock.MagicMock()

        ssp.verify_device_serial_on_cn = mock.MagicMock()
        ssp.verify_mode_on_cn = mock.MagicMock()
        ssp.verify_threshold_on_cn = mock.MagicMock()
        ssp.verify_pause_time_on_cn = mock.MagicMock()
        ssp.verify_stop_immediately_on_cn = mock.MagicMock()

        ssp.verify_who_i_am()

    def test_verify_who_i_am_pass_2(self):
        """ Test Verify Who I Am Pass Case 2: When the mode is off, verify that only one verify method runs
        Create the test object
        Assign the variables to be used in the method
        Mock the methods we don't want to dive into
        Call the method
        Since nothing is returned or changed we don't have anything new to assert
        """
        ssp = self.create_test_start_stop_pause_object(event_type=opcodes.program_stop_condition, cn_type="10")
        ssp._mode = opcodes.off

        ssp.get_data = mock.MagicMock()
        ssp.verify_mode_on_cn = mock.MagicMock()

        ssp.verify_who_i_am()

