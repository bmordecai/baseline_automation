__author__ = 'baseline'

import unittest
from old_32_10_sb_objects_dec_29_2017.common.objects.baseunit import bu_helpers, bu_imports
import json_handler
import os


class TestBUHelpers(unittest.TestCase):

    # SN_IN_USE_FP = '../../../objects/baseunit/json_config_files/serial_numbers_in_use.json'
    SN_IN_USE_FP = os.path.join("../../../objects/baseunit/json_config_files", "serial_numbers_in_use.json")

    def setUp(self):
        exists = os.path.exists(self.SN_IN_USE_FP)
        print exists
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """

        # Reset Serial Number's in Use file
        # fn = os.path.join("unit_test_jsons", "serial_numbers_in_use.json")
        fn = self.SN_IN_USE_FP
        tmp = bu_imports.JSONFileTemplates.SERIAL_NUMBERS_IN_USE
        bu_helpers.reset_file_with_template(_file_name=fn, _template=tmp)

        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_get_next_sn_pass1(self):
        """ Verifies valid next serial number from default serial number for Baseline Single-Valve Decoder """
        c = "BL"
        t = bu_imports.DeviceTypes.SINGLE_VALVE_STR
        expected_sn = "00D0001"
        received_sn = bu_helpers.get_next_sn(_company=c, _type=t)
        self.assertEqual(first=received_sn, second=expected_sn)

    def test_get_next_sn_pass2(self):
        """ Verifies valid next serial number from default serial number for Baseline Dual-Valve Decoder """
        c = "BL"
        t = bu_imports.DeviceTypes.DUAL_VALVE_STR
        expected_sn = "00E0010"
        received_sn = bu_helpers.get_next_sn(_company=c, _type=t)
        self.assertEqual(first=received_sn, second=expected_sn)

    def test_get_next_sn_pass3(self):
        """ Verifies valid next serial number from default serial number for Baseline Quad-Valve Decoder """
        c = "BL"
        t = bu_imports.DeviceTypes.QUAD_VALVE_STR
        expected_sn = "00Q0010"
        received_sn = bu_helpers.get_next_sn(_company=c, _type=t)
        self.assertEqual(first=received_sn, second=expected_sn)

    def test_get_next_sn_pass4(self):
        """ Verifies valid next serial number from default serial number for Baseline Twelve-Valve Decoder """
        c = "BL"
        t = bu_imports.DeviceTypes.TWELVE_VALVE_STR
        expected_sn = "VA00101"
        received_sn = bu_helpers.get_next_sn(_company=c, _type=t)
        self.assertEqual(first=received_sn, second=expected_sn)

    def test_reset_file_with_template_pass1(self):
        """ Verifies Json file is reset with default template """
        # fn = "unit_test_jsons/serial_numbers_in_use.json"
        fn = bu_imports.FileNamePaths.SERIAL_NUMBERS_IN_USE
        tmpl = bu_imports.JSONFileTemplates.SERIAL_NUMBERS_IN_USE
        bu_helpers.reset_file_with_template(_file_name=fn, _template=tmpl)
        current_data = json_handler.get_data(file_name=fn)
        self.assertEqual(first=current_data, second=tmpl)
        print "yay"

    def test_put_sn_to_file_pass1(self):
        """ Verifies successful addition of a serial number to the respective list for valve type in the json file """
        c = bu_imports.CompanyNames.BASELINE
        t = bu_imports.DeviceTypes.SINGLE_VALVE_STR
        s = "TSD0001"
        bu_helpers.put_sn_to_file(_company=c, _type=t, _sn=s)
        current_data = json_handler.get_data(file_name=bu_imports.FileNamePaths.SERIAL_NUMBERS_IN_USE)
        self.assertTrue(s in current_data[c][t])

    def test_is_sn_used_pass1(self):
        """ Verifies a True boolean value is returned when a serial is found """
        expected_result = True
        cmpny = bu_imports.CompanyNames.BASELINE
        typ = bu_imports.DeviceTypes.SINGLE_VALVE_STR
        sn = "TSD0001"
        # bu_helpers.put_sn_to_file(_company=cmpny, _type=)

    def test_convert_32bit_number_to_4_8bit_numbers(self):
        """ Verifies correct division of 32bit unsigned int into 4 8bit unsigned ints """
        expected_result = (13, 0, 57, 135)
        actual_result = bu_helpers.convert_sn_from_32bit_int_to_4_8bit_ints(arg0=0xD003987)
        self.assertEqual(first=actual_result, second=expected_result)

    def test_convert_4_8bit_numbers_to_32bit_number(self):
        """ Verifies correct merge/bit shifting to attain a 32bit unsigned int value from 4 8bit unsigned ints """
        expected_result = '0xD003987'
        actual_result = bu_helpers.convert_4_8bit_numbers_to_32bit_number(arg0=13, arg1=0, arg2=57, arg3=135)
        ar = hex(actual_result)[2:].upper()
        self.assertEqual(first=ar, second=expected_result[2:])

    def test_convert_sn_string_to_int_pass1(self):
        """ Verifies correct integer value returned from serial number """
        expected_result = 238133987
        actual_result = bu_helpers.convert_sn_string_to_int(sn_as_string="D003987")
        self.assertEqual(first=actual_result, second=expected_result)

    def test_convert_int_to_sn_string_pass1(self):
        """ Verifies correct serial number from integer """
        expected_result = "D003987"
        actual_result = bu_helpers.convert_int_to_sn_string(sn_as_int=238133987)
        self.assertEqual(first=actual_result, second=expected_result)
