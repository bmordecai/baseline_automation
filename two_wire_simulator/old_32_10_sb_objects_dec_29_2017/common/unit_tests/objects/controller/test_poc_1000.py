__author__ = 'Brice "Ajo Grande" Garlick'

import unittest
import mock
import serial
import status_parser

import old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.poc as poc
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_1000 import POC1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.mv import MasterValve
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.fm import FlowMeter
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ms import MoistureSensor
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.sw import EventSwitch
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import *


class TestPOC1000Object(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.309759

    Master Valve Starting lat & lng:
    latitude = 43.609768
    longitude = -116.310359
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        Devices.ser = self.mock_ser
        poc.POC.ser = self.mock_ser
        Mainline.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        master_valves[3] = MasterValve("MVD0001", 3)

        flow_meters[5] = FlowMeter("FMD0001", 5)

        # 1000 POC Doesn't have a Mainline
        # mainlines32[1] = Mainline(1)

        moisture_sensors[4] = MoistureSensor("MSD0001", 4)

        event_switches[2] = EventSwitch("ESD0001", 2)

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # when these objects are created in this test, they carry over to any test after this one because they are
        # just pointers and not test variables specific to just THIS module.
        del master_valves[3]
        del flow_meters[5]
        del moisture_sensors[4]
        del event_switches[2]

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_poc_1000_object(self):
        """ Creates a new Master Valve object for testing purposes """
        poc = POC1000(_address=1, _flow_meter_address=5, _master_valve_address=3)

        return poc

    #################################
    def test_send_programming_to_cn_pass1(self):
        """ Send Programming To Controller Pass Case 1: Correct Command Sent """
        expected_command = "SET," \
                           "PC=1," \
                           "DS=Test Water Source 1," \
                           "EN=TR," \
                           "FL=1000.0," \
                           "HF=0.0," \
                           "HS=FA," \
                           "UF=0.0," \
                           "US=FA," \
                           "FM=FMD0001," \
                           "MV=MVD0001," \
                           "LF=FA," \
                           "FT=1," \
                           "VF=10," \
                           "VE=FA," \
                           "FF=FA"
        poc = self.create_test_poc_1000_object()

        poc.send_programming_to_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_to_cn_fail1(self):
        """ Send Programming To Controller Fail Case 1: Failed communication with controller """
        expected_command = "SET," \
                           "PC=1," \
                           "DS=Test Water Source 1," \
                           "EN=TR," \
                           "FL=1000.0," \
                           "HF=0.0," \
                           "HS=FA," \
                           "UF=0.0," \
                           "US=FA," \
                           "FM=FMD0001," \
                           "MV=MVD0001," \
                           "LF=FA," \
                           "FT=1," \
                           "VF=10," \
                           "VE=FA," \
                           "FF=FA"
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            poc = self.create_test_poc_1000_object()
            poc.send_programming_to_cn()
        e_msg = "Exception occurred trying to 'SET' Water Source 1's 'Values' to: '{0}'\n -> ".format(expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_limit_concurrent_to_target_state_on_cn_pass1(self):
        """ Set Limit Concurrent To Target State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.limit_concurrent_to_target,
                                                      opcodes.true)
        poc = self.create_test_poc_1000_object()
        poc.set_limit_concurrent_to_target_state_on_cn("TR")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_limit_concurrent_to_target_state_on_cn_pass2(self):
        """ Set Limit Concurrent To Target State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.limit_concurrent_to_target,
                                                      opcodes.false)
        poc = self.create_test_poc_1000_object()
        poc.set_limit_concurrent_to_target_state_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_limit_concurrent_to_target_state_on_cn_fail1(self):
        """ Set Limit Concurrent To Target State On Controller Fail Case 1: Invalid State Argument """
        poc = self.create_test_poc_1000_object()
        with self.assertRaises(ValueError) as context:
            poc.set_limit_concurrent_to_target_state_on_cn("Foo")
        e_msg = "Invalid state entered for 'SET' 'Limit Concurrent To Target State' for 1000 Water Source 1. " \
                "Expects: TR | FA, received: Foo"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_limit_concurrent_to_target_state_on_cn_fail2(self):
        """ Set Limit Concurrent To Target State On Controller Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_1000_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_limit_concurrent_to_target_state_on_cn("FA")
        e_msg = "Exception occurred trying to set 1000's Water Source 1's 'Limit Concurrent To Target State' " \
                "to: 'FA' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_fill_time_in_minutes_on_cn_pass1(self):
        """ Set Fill Time In Minutes On Controller Pass Case 1: Setting new _ft value = 6.0, check that object
        variable _ft is set """
        poc = self.create_test_poc_1000_object()

        new_value = 6
        poc.set_fill_time_in_minutes_on_cn(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = poc.ft
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_fill_time_in_minutes_on_cn_pass2(self):
        """ Set Fill Time In Minutest On Controller Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_1000_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.fill_time,
                                                      str(new_value))
        poc.set_fill_time_in_minutes_on_cn(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_fill_time_in_minutes_on_cn_fail1(self):
        """ Set Fill Time In Minutes On Controller Fail Case 1: Failed communication with controller """
        poc = self.create_test_poc_1000_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_fill_time_in_minutes_on_cn(new_value)
        e_msg = "Exception occurred trying to set 1000's Water Source 1's 'Fill Time' to: '6' -> ".format(
                str(poc.ad), str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_flow_variance_percent_on_cn_pass1(self):
        """ Set Flow Variance Percent On Controller Pass Case 1: Setting new _vf value = 6.0, check that object
        variable _vf is set """
        poc = self.create_test_poc_1000_object()

        new_value = 6
        poc.set_flow_variance_percent_on_cn(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = poc.vf
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_flow_variance_percent_on_cn_pass2(self):
        """ Set Flow Variance Percent On Controller Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_1000_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.flow_variance,
                                                      str(new_value))
        poc.set_flow_variance_percent_on_cn(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_flow_variance_percent_on_cn_fail1(self):
        """ Set Flow Variance Percent On Controller Fail Case 1: Failed communication with controller """
        poc = self.create_test_poc_1000_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_flow_variance_percent_on_cn(new_value)
        e_msg = "Exception occurred trying to set 1000's Water Source 1's 'Flow Variance' to: '6' -> ".format(
                str(poc.ad), str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_flow_variance_enable_state_on_cn_pass1(self):
        """ Set Flow Variance Enable State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.flow_variance_enable,
                                                      opcodes.true)
        poc = self.create_test_poc_1000_object()
        poc.set_flow_variance_enable_state_on_cn("TR")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_flow_variance_enable_state_on_cn_pass2(self):
        """ Set Flow Variance Enable State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.flow_variance_enable,
                                                      opcodes.false)
        poc = self.create_test_poc_1000_object()
        poc.set_flow_variance_enable_state_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_flow_variance_enable_state_on_cn_fail1(self):
        """ Set Flow Variance Enable State On Controller Fail Case 1: Invalid State Argument """
        poc = self.create_test_poc_1000_object()
        with self.assertRaises(ValueError) as context:
            poc.set_flow_variance_enable_state_on_cn("Foo")
        e_msg = "Invalid state entered for 'SET' 'Flow Variance Enable State' for 1000 Water Source 1. " \
                "Expects: TR | FA, received: Foo"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_flow_variance_enable_state_on_cn_fail2(self):
        """ Set Flow Variance Enable State On Controller Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_1000_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_flow_variance_enable_state_on_cn("FA")
        e_msg = "Exception occurred trying to set 1000's Water Source 1's 'Flow Variance Enable State' " \
                "to: 'FA' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_flow_fault_state_on_cn_pass1(self):
        """ Set Flow Fault State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.flow_fault,
                                                      opcodes.true)
        poc = self.create_test_poc_1000_object()
        poc.set_flow_fault_state_on_cn("TR")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_flow_fault_state_on_cn_pass2(self):
        """ Set Flow Fault State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.flow_fault,
                                                      opcodes.false)
        poc = self.create_test_poc_1000_object()
        poc.set_flow_fault_state_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_flow_fault_state_on_cn_fail1(self):
        """ Set Flow Fault State On Controller Fail Case 1: Invalid State Argument """
        poc = self.create_test_poc_1000_object()
        with self.assertRaises(ValueError) as context:
            poc.set_flow_fault_state_on_cn("Foo")
        e_msg = "Invalid state entered for 'SET' 'Flow Fault State' for 1000 Water Source 1. " \
                "Expects: TR | FA, received: Foo"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_flow_fault_state_on_cn_fail2(self):
        """ Set Flow Fault State On Controller Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_1000_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_flow_fault_state_on_cn("FA")
        e_msg = "Exception occurred trying to set 1000's Water Source 1's 'Flow Fault State' " \
                "to: 'FA' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_limit_concurrent_to_target_on_cn_pass1(self):
        """ Verify Limit Concurrent To Target On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_1000_object()
        poc.lf = "FA"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=FA".format(opcodes.limit_concurrent_to_target))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_limit_concurrent_to_target_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_limit_concurrent_to_target_on_cn_fail1(self):
        """ Verify Limit Concurrent To Target On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.lf """
        poc = self.create_test_poc_1000_object()
        poc.lf = "TR"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Tr".format(opcodes.limit_concurrent_to_target))
        poc.data = mock_data

        expected_message = "Unable to verify 1000's Water Source 1's 'Limit Concurrent To Target State'. Received: " \
                           "Tr, Expected: TR"
        try:
            poc.verify_limit_concurrent_to_target_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_fill_time_on_cn_pass1(self):
        """ Verify Fill Time On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_1000_object()
        poc.ft = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.fill_time))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_fill_time_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_fill_time_on_cn_fail1(self):
        """ Verify Fill Time On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.ft """
        poc = self.create_test_poc_1000_object()
        poc.ft = 6
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.fill_time))
        poc.data = mock_data

        expected_message = "Unable to verify 1000's Water Source 1's 'Fill Time'. Received: 5, Expected: 6"
        try:
            poc.verify_fill_time_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_flow_variance_on_cn_pass1(self):
        """ Verify Flow Variance On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_1000_object()
        poc.vf = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.flow_variance))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_flow_variance_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_flow_variance_on_cn_fail1(self):
        """ Verify Flow Variance On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.vf """
        poc = self.create_test_poc_1000_object()
        poc.vf = 6.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.flow_variance))
        poc.data = mock_data

        expected_message = "Unable to verify 1000's Water Source 1's 'Flow Variance'. Received: 5.0, Expected: 6.0"
        try:
            poc.verify_flow_variance_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_flow_variance_enable_on_cn_pass1(self):
        """ Verify Flow Variance Enable State On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_1000_object()
        poc.ve = "FA"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=FA".format(opcodes.flow_variance_enable))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_flow_variance_enable_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_flow_variance_enable_on_cn_fail1(self):
        """ Verify Flow Variance Enable State On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.ve """
        poc = self.create_test_poc_1000_object()
        poc.ve = "TR"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Tr".format(opcodes.flow_variance_enable))
        poc.data = mock_data

        expected_message = "Unable to verify 1000's Water Source 1's 'Flow Variance Enable State'. Received: " \
                           "Tr, Expected: TR"
        try:
            poc.verify_flow_variance_enable_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_flow_fault_state_on_cn_pass1(self):
        """ Verify Flow Fault State On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_1000_object()
        poc.ff = "FA"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=FA".format(opcodes.flow_fault))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_flow_fault_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_flow_fault_state_on_cn_fail1(self):
        """ Verify Flow Fault State On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.ve """
        poc = self.create_test_poc_1000_object()
        poc.ff = "TR"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Tr".format(opcodes.flow_fault))
        poc.data = mock_data

        expected_message = "Unable to verify 1000's Water Source 1's 'Flow Fault State'. Received: " \
                           "Tr, Expected: TR"
        try:
            poc.verify_flow_fault_state_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_who_i_am_pass1(self):
        poc = self.create_test_poc_1000_object()

        mock_data = status_parser.KeyValues("PC=1,"
                                            "DS=Test Water Source 1,"
                                            "EN=TR,"
                                            "FL=1000,"
                                            "HF=0,"
                                            "HS=FA,"
                                            "UF=0,"
                                            "US=FA,"
                                            "FM=FMD0001,"
                                            "MV=MVD0001,"
                                            "LF=FA,"
                                            "FT=1,"
                                            "VF=10,"
                                            "VE=FA,"
                                            "FF=FA,"
                                            "MV=3,"
                                            "FM=5")
        poc.data = mock_data
        poc.get_data = mock.MagicMock(sideeffect=None)
        poc.verify_who_i_am()

    if __name__ == "__main__":
        unittest.main()