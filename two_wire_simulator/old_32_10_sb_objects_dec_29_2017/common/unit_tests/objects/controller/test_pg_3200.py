__author__ = 'Brice "Ajo Grande" Garlick'

import unittest

import mock
import serial
import status_parser
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
import old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.poc as poc
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.programs import Programs
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.mv import MasterValve
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import *


class TestPG3200Object(unittest.TestCase):
    """   """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        Programs.ser = self.mock_ser
        poc.POC.ser = self.mock_ser
        Mainline.ser = self.mock_ser

        mainlines32[5] = Mainline(5)

        master_valves[3] = MasterValve("MVD0001", 3)

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # when these objects are created in this test, they carry over to any test after this one because they are
        # just pointers and not test variables specific to just THIS module.
        del master_valves[3]
        del mainlines32[5]

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_prog_3200_object(self, _ww=1, _ci="WD"):
        """ Creates a new Master Valve object for testing purposes """
        # creates the program object to be used in all the tests
        if _ww == 1:
            prog = PG3200(_ad=1, _ww=['111111111111111111111111'], _ci=_ci, _ml=5, _bp=3)
        else:
            ww_value = ['011111111111111111111111',
                        '001111111111111111111111',
                        '000111111111111111111111',
                        '000011111111111111111111',
                        '000001111111111111111111',
                        '000000111111111111111111',
                        '000000011111111111111111'
                        ]
            prog = PG3200(_ad=1, _ww=ww_value, _ci=_ci, _ml=5, _bp=3)

        return prog

    #################################
    def test_create_full_prog_3200_object_pass1(self):
        """ Test creating prog_3200 object with all arguments passed-in """
        # creates a program 3200 object with all passed in values to be sure constructor works properly

        prog = PG3200(
            _ad=1,
            _en="TR",
            _ww=['111111111111111111111111'],
            _mc=1,
            _sa=100,
            _ci="WD",
            _di=5,
            _wd=[
                1,          # Sunday
                1,          # Monday
                1,          # Tuesday
                1,          # Wednesday
                1,          # Thursday
                1,          # Friday
                1           # Saturday
                ],
            _sm=[
                1, 1,       # January 1-15, January 16-31
                1, 1,       # February 1-15, February 16-End of month
                1, 1,       # March 1-15, March 16-31
                1, 1,       # April 1-15, April 16-30
                1, 1,       # May 1-15, May 16-31
                1, 1,       # June 1-15, June 16-30
                1, 1,       # July 1-15, July 16-31
                1, 1,       # August 1-15, August 16-31
                1, 1,       # September 1-15, September 16-30
                1, 1,       # October 1-15, October 16-31
                1, 1,       # November 1-15, November 16-30
                1, 1        # December 1-15, December 16-31
                ],
            _st=[480],
            _pr=1,
            _ml=5,
            _bp=3
        )

        return prog

    #################################
    def test_send_programming_to_cn_pass1(self):
        """ Send Programming To Controller Pass Case 1: Correct Command Sent with 1 water window value using
        defaults, _ci = EV """
        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "WW=111111111111111111111111," \
                           "MC=1," \
                           "SA=100," \
                           "CI=EV," \
                           "ST=480," \
                           "PR=2," \
                           "ML=5," \
                           "BP=3"
        prog = self.create_test_prog_3200_object(_ci="EV")

        prog.send_programming_to_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_to_cn_pass2(self):
        """ Send Programming To Controller Pass Case 2: Correct Command Sent with 1 water window value using
        defaults, _ci = WD """
        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "WW=111111111111111111111111," \
                           "MC=1," \
                           "SA=100," \
                           "CI=WD," \
                           "WD=1111111," \
                           "ST=480," \
                           "PR=2," \
                           "ML=5," \
                           "BP=3"
        prog = self.create_test_prog_3200_object()

        prog.send_programming_to_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_to_cn_pass3(self):
        """ Send Programming To Controller Pass Case 3: Correct Command Sent with 1 water window value using
        defaults, _ci = ID """
        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "WW=111111111111111111111111," \
                           "MC=1," \
                           "SA=100," \
                           "CI=ID," \
                           "DI=1," \
                           "ST=480," \
                           "PR=2," \
                           "ML=5," \
                           "BP=3"
        prog = self.create_test_prog_3200_object(_ci="ID")

        prog.send_programming_to_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_to_cn_pass4(self):
        """ Send Programming To Controller Pass Case 4: Correct Command Sent with 1 water window value using
        defaults, _ci = ET """
        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "WW=111111111111111111111111," \
                           "MC=1," \
                           "SA=100," \
                           "CI=ET," \
                           "SM=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1," \
                           "ST=480," \
                           "PR=2," \
                           "ML=5," \
                           "BP=3"
        prog = self.create_test_prog_3200_object(_ci="ET")

        prog.send_programming_to_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_to_cn_pass5(self):
        """ Send Programming To Controller Pass Case 5: Correct Command Sent with 7 water window values """
        ww_value = ['011111111111111111111111',
                    '001111111111111111111111',
                    '000111111111111111111111',
                    '000011111111111111111111',
                    '000001111111111111111111',
                    '000000111111111111111111',
                    '000000011111111111111111'
                    ]

        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13}," \
                           "MC=1," \
                           "SA=100," \
                           "CI=WD," \
                           "WD=1111111," \
                           "ST=480," \
                           "PR=2," \
                           "ML=5," \
                           "BP=3".format(opcodes.sunday, str(ww_value[0]),
                                         opcodes.monday, str(ww_value[1]),
                                         opcodes.tuesday, str(ww_value[2]),
                                         opcodes.wednesday, str(ww_value[3]),
                                         opcodes.thursday, str(ww_value[4]),
                                         opcodes.friday, str(ww_value[5]),
                                         opcodes.saturday, str(ww_value[6])
                                         )
        prog = self.create_test_prog_3200_object(_ww=7)

        prog.send_programming_to_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_to_cn_fail1(self):
        """ Send Programming To Controller Fail Case 1: Failed communication with controller """
        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "WW=111111111111111111111111," \
                           "MC=1," \
                           "SA=100," \
                           "CI=WD," \
                           "WD=1111111," \
                           "ST=480," \
                           "PR=2," \
                           "ML=5," \
                           "BP=3"
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            prog = self.create_test_prog_3200_object()
            prog.send_programming_to_cn()
        e_msg = "Exception occurred trying to set Program 1's 'Values' to: '{0}' -> ".format(expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_calendar_interval_on_cn_pass1(self):
        """ Set Calendar Interval On Controller Pass Case 1: Setting new _ci value = "ET", check that object
        variable _ci is set """
        prog = self.create_test_prog_3200_object()

        new_value = "ET"
        # ci type is SM
        ci_type_value = [
            1, 1,       # January 1-15, January 16-31
            1, 1,       # February 1-15, February 16-End of month
            1, 1,       # March 1-15, March 16-31
            1, 1,       # April 1-15, April 16-30
            1, 1,       # May 1-15, May 16-31
            1, 1,       # June 1-15, June 16-30
            1, 1,       # July 1-15, July 16-31
            1, 1,       # August 1-15, August 16-31
            1, 1,       # September 1-15, September 16-30
            1, 1,       # October 1-15, October 16-31
            1, 1,       # November 1-15, November 16-30
            1, 1        # December 1-15, December 16-31
            ]
        prog.set_calendar_interval_on_cn(_ci=new_value, _sm=ci_type_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = prog.ci
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_calendar_interval_on_cn_pass2(self):
        """ Set Calendar Interval On Controller Pass Case 2: Command with correct values is sent to controller,
        _ci = ET """
        prog = self.create_test_prog_3200_object()

        new_value = "ET"
        # ci type is SM
        ci_type_value = [
            1, 1,       # January 1-15, January 16-31
            1, 1,       # February 1-15, February 16-End of month
            1, 1,       # March 1-15, March 16-31
            1, 1,       # April 1-15, April 16-30
            1, 1,       # May 1-15, May 16-31
            1, 1,       # June 1-15, June 16-30
            1, 1,       # July 1-15, July 16-31
            1, 1,       # August 1-15, August 16-31
            1, 1,       # September 1-15, September 16-30
            1, 1,       # October 1-15, October 16-31
            1, 1,       # November 1-15, November 16-30
            1, 1        # December 1-15, December 16-31
            ]
        expected_command = "SET,{0}=1,{1}={2},{3}=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1".format(
            opcodes.program,
            opcodes.calendar_interval,
            str(new_value),
            opcodes.program_semi_month_interval
            )

        prog.set_calendar_interval_on_cn(_ci=new_value, _sm=ci_type_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_calendar_interval_on_cn_pass3(self):
        """ Set Calendar Interval On Controller Pass Case 3: Setting new _ci value = "ID", check that object
        variable _ci is set """
        prog = self.create_test_prog_3200_object()

        new_value = "ID"
        # ci type is DI
        ci_type_value = 5
        prog.set_calendar_interval_on_cn(_ci=new_value, _di=ci_type_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = prog.ci
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_calendar_interval_on_cn_pass4(self):
        """ Set Calendar Interval On Controller Pass Case 4: Command with correct values is sent to controller,
        _ci = ID """
        prog = self.create_test_prog_3200_object()

        new_value = "ID"
        # ci type is DI
        ci_type_value = 5
        expected_command = "SET,{0}=1,{1}={2},{3}=5".format(
            opcodes.program,
            opcodes.calendar_interval,
            str(new_value),
            opcodes.day_interval
            )

        prog.set_calendar_interval_on_cn(_ci=new_value, _di=ci_type_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_calendar_interval_on_cn_pass5(self):
        """ Set Calendar Interval On Controller Pass Case 5: Setting new _ci value = "WD", check that object
        variable _ci is set """
        prog = self.create_test_prog_3200_object()

        new_value = "WD"
        # ci type is WD
        ci_type_value = [
            1,          # Sunday
            1,          # Monday
            1,          # Tuesday
            1,          # Wednesday
            1,          # Thursday
            1,          # Friday
            1           # Saturday
            ]
        prog.set_calendar_interval_on_cn(_ci=new_value, _wd=ci_type_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = prog.ci
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_calendar_interval_on_cn_pass6(self):
        """ Set Calendar Interval On Controller Pass Case 6: Command with correct values is sent to controller,
        _ci = WD """
        prog = self.create_test_prog_3200_object()

        new_value = "WD"
        # ci type is WD
        ci_type_value = [
            1,          # Sunday
            1,          # Monday
            1,          # Tuesday
            1,          # Wednesday
            1,          # Thursday
            1,          # Friday
            1           # Saturday
            ]
        expected_command = "SET,{0}=1,{1}={2},{3}=1111111".format(
            opcodes.program,
            opcodes.calendar_interval,
            str(new_value),
            opcodes.week_days
            )

        prog.set_calendar_interval_on_cn(_ci=new_value, _wd=ci_type_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_calendar_interval_on_cn_pass7(self):
        """ Set Calendar Interval On Controller Pass Case 7: Setting new _ci value = "EV", check that object
        variable _ci is set """
        prog = self.create_test_prog_3200_object()

        new_value = "EV"
        prog.set_calendar_interval_on_cn(_ci=new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = prog.ci
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_calendar_interval_on_cn_pass8(self):
        """ Set Calendar Interval On Controller Pass Case 8: Command with correct values is sent to controller,
        _ci = EV """
        prog = self.create_test_prog_3200_object()

        new_value = "EV"
        expected_command = "SET,{0}=1,{1}={2}".format(
            opcodes.program,
            opcodes.calendar_interval,
            str(new_value)
            )

        prog.set_calendar_interval_on_cn(_ci=new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_calendar_interval_on_cn_fail1(self):
        """ Set Calendar Interval On Controller Fail Case 1: Failed communication with controller """
        prog = self.create_test_prog_3200_object()
        new_value = "EV"

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        expected_command = "SET,{0}=1,{1}={2}".format(
            opcodes.program,
            opcodes.calendar_interval,
            str(new_value)
            )

        with self.assertRaises(Exception) as context:
            prog.set_calendar_interval_on_cn(_ci=new_value)
        e_msg = "Exception occurred trying to set Program 1's 'Calendar Interval' to: {0} -> ".format(
                expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_cycle_count_on_cn_fail2(self):
        """ Set Calendar Interval On Controller Fail Case 2: Trying to set _ci value that is not an acceptable value """
        prog = self.create_test_prog_3200_object()
        new_value = "BG"

        with self.assertRaises(Exception) as context:
            prog.set_calendar_interval_on_cn(_ci=new_value)
        e_msg = "Invalid calendar interval type specified for Program 1. Expected: ('EV','OD','OS','ET'," \
                "'ID','WD'), but received: {0}".format(str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_start_times_on_cn_pass1(self):
        """ Set Start Times On Controller Pass Case 1: Setting new _st value = [60, 120] check that object
        variable _st is set """
        prog = self.create_test_prog_3200_object()

        new_value = [60, 120]
        prog.set_start_times_on_cn(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = prog.st
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_start_times_on_cn_pass2(self):
        """ Set Start Times On Controller Pass Case 2: Command with correct values is sent to controller """
        prog = self.create_test_prog_3200_object()

        new_value = [60, 120]
        expected_command = "SET,{0}=1,{1}=60=120".format(
            opcodes.program,
            opcodes.start_times
            )

        prog.set_start_times_on_cn(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_start_times_on_cn_fail1(self):
        """ Set Start Times On Controller Fail Case 1: Failed communication with controller """
        prog = self.create_test_prog_3200_object()
        new_value = [60]

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.set_start_times_on_cn(new_value)
        e_msg = "Exception occurred trying to set Program 1's 'Start Times' to: 60 -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_priority_level_on_cn_pass1(self):
        """ Set Priority Level On Controller Pass Case 1: Setting new _pr value = 3 check that object
        variable _pr is set """
        prog = self.create_test_prog_3200_object()

        new_value = 3
        prog.set_priority_level_on_cn(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = prog.pr
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_priority_level_on_cn_pass2(self):
        """ Set Priority Level On Controller Pass Case 2: Command with correct values is sent to controller """
        prog = self.create_test_prog_3200_object()

        new_value = 3
        expected_command = "SET,{0}=1,{1}=3".format(
            opcodes.program,
            opcodes.priority
            )

        prog.set_priority_level_on_cn(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_priority_level_on_cn_fail1(self):
        """ Set Priority Level On Controller Fail Case 1: Failed communication with controller """
        prog = self.create_test_prog_3200_object()
        new_value = 1

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.set_priority_level_on_cn(new_value)
        e_msg = "Exception occurred trying to set Program 1's 'Priority Level' to: 1 -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_mainline_on_cn_pass1(self):
        """ Set Mainline On Controller Pass Case 1: Passed in address of a present Mainline """
        prog = self.create_test_prog_3200_object()

        expected_value = 5
        prog.set_mainline_on_cn(expected_value)

        this_obj = prog.mainline_objects[5]
        actual_value = this_obj.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_mainline_on_cn_pass2(self):
        """ Set Mainline On Controller Pass Case 2: Command with correct values sent to controller """
        prog = self.create_test_prog_3200_object()

        this_obj = prog.mainline_objects[5]
        # sn_value = this_obj.sn
        ad_value = this_obj.ad
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.mainline,
                                                      str(ad_value))

        prog.set_mainline_on_cn(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_mainline_on_cn_fail1(self):
        """ Set Mainline On Controller Fail Case 1: Failed communication with controller """
        prog = self.create_test_prog_3200_object()
        this_obj = prog.mainline_objects[5]
        # sn_value = this_obj.sn
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.set_mainline_on_cn(ad_value)
        e_msg = "Exception occurred trying to set Program 1's 'Mainline' to: {0} " \
                "-> ".format(str(ad_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_booster_pump_on_cn_pass1(self):
        """ Set Booster Pump On Controller Pass Case 1: Passed in address of a present Booster Pump """
        prog = self.create_test_prog_3200_object()

        expected_value = 3
        prog.set_booster_pump_on_cn(expected_value)

        this_obj = prog.mv_objects[3]
        actual_value = this_obj.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_booster_pump_on_cn_pass2(self):
        """ Set Booster Pump On Controller Pass Case 2: Command with correct values sent to controller """
        prog = self.create_test_prog_3200_object()

        this_obj = prog.mv_objects[3]
        sn_value = this_obj.sn
        ad_value = this_obj.ad
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.booster_pump,
                                                      str(sn_value))

        prog.set_booster_pump_on_cn(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_booster_pump_on_cn_fail1(self):
        """ Set Booster Pump On Controller Fail Case 1: Failed communication with controller """
        prog = self.create_test_prog_3200_object()
        this_obj = prog.mv_objects[3]
        sn_value = this_obj.sn
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.set_booster_pump_on_cn(ad_value)
        e_msg = "Exception occurred trying to set Program 1's 'Booster Pump' to: {0}({1}) " \
                "-> ".format(sn_value, str(ad_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_start_times_on_cn_pass1(self):
        """ Verify Start Times On Controller Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        prog.st = [60]
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=60".format(opcodes.start_times))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_start_times_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_start_times_on_cn_fail1(self):
        """ Verify Start Times On Controller Fail Case 1: Value on controller does not match what is
        stored in prog.st """
        prog = self.create_test_prog_3200_object()
        prog.st = [60]
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.start_times))
        prog.data = mock_data

        expected_message = "Unable to verify 3200 Program 1's 'Start Times'. Received: [6], Expected: [60]"
        try:
            prog.verify_start_times_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_calendar_interval_on_cn_pass1(self):
        """ Verify Calendar Interval On Controller Pass Case 1: Exception is not raised, _ci=ET, class variables
        match values on the controller """
        prog = self.create_test_prog_3200_object()
        prog.ci = "ET"
        prog.sm = [
            1, 1,       # January 1-15, January 16-31
            1, 1,       # February 1-15, February 16-End of month
            1, 1,       # March 1-15, March 16-31
            1, 1,       # April 1-15, April 16-30
            1, 1,       # May 1-15, May 16-31
            1, 1,       # June 1-15, June 16-30
            1, 1,       # July 1-15, July 16-31
            1, 1,       # August 1-15, August 16-31
            1, 1,       # September 1-15, September 16-30
            1, 1,       # October 1-15, October 16-31
            1, 1,       # November 1-15, November 16-30
            1, 1        # December 1-15, December 16-31
            ]
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}={1},"
                                            "{2}=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1"
                                            .format(opcodes.calendar_interval,
                                                    opcodes.historical_calendar,
                                                    opcodes.program_semi_month_interval))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_calendar_interval_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            print("**** Msg = {0}".format(e_msg))
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_calendar_interval_on_cn_pass2(self):
        """ Verify Calendar Interval On Controller Pass Case 2: Exception is not raised, _ci=WD, class variables
        match values on the controller """
        prog = self.create_test_prog_3200_object()
        prog.ci = "WD"
        prog.wd = [
            1,          # Sunday
            1,          # Monday
            1,          # Tuesday
            1,          # Wednesday
            1,          # Thursday
            1,          # Friday
            1           # Saturday
            ]
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}={1},"
                                            "{2}=1111111"
                                            .format(opcodes.calendar_interval,
                                                    opcodes.week_days,
                                                    opcodes.week_days))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_calendar_interval_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_calendar_interval_on_cn_pass3(self):
        """ Verify Calendar Interval On Controller Pass Case 3: Exception is not raised, _ci=ID, class variables
        match values on the controller """
        prog = self.create_test_prog_3200_object()
        prog.ci = "ID"
        prog.di = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}={1},"
                                            "{2}=5"
                                            .format(opcodes.calendar_interval,
                                                    opcodes.interval_days,
                                                    opcodes.day_interval))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_calendar_interval_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_calendar_interval_on_cn_pass4(self):
        """ Verify Calendar Interval On Controller Pass Case 4: Exception is not raised, _ci=EV, class variables
        match values on the controller """
        prog = self.create_test_prog_3200_object()
        prog.ci = "EV"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}={1},".format(opcodes.calendar_interval,
                                                                                 opcodes.even_day))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_calendar_interval_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_calendar_interval_on_cn_fail1(self):
        """ Verify Calendar Interval On Controller Fail Case 1: prog.ci=ET, sm value is None """
        prog = self.create_test_prog_3200_object()
        prog.ci = "ET"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=ET,{1}=None"
                                            .format(opcodes.calendar_interval, opcodes.program_semi_month_interval))
        prog.data = mock_data

        expected_message = "Unable to verify 'Calendar Interval' type: ET for current program. Unable to find a value " \
                           "associated with key: 'SM'. Current program doesn't have the expected 'Calendar Interval'"
        try:
            prog.verify_calendar_interval_on_cn(unit_test_value=None)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_calendar_interval_on_cn_fail2(self):
        """ Verify Calendar Interval On Controller Fail Case 2: Value on controller does not match what is
        stored in prog.di where prog.ci=ID """
        prog = self.create_test_prog_3200_object()
        prog.ci = "ID"
        prog.di = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=ID,{1}=6"
                                            .format(opcodes.calendar_interval, opcodes.day_interval))
        prog.data = mock_data

        expected_message = "Unable to verify 'Calendar Interval' type: ID, for current program. Expected:"\
                           "(Key: DI, Value: 5), Received: (Key: DI, Value: 6)"
        try:
            prog.verify_calendar_interval_on_cn(unit_test_value=None)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_priority_level_on_cn_pass1(self):
        """ Verify Priority Level On Controller Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        prog.pr = 2
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=2".format(opcodes.priority))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_priority_level_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_priority_level_on_cn_fail1(self):
        """ Verify Priority Level On Controller Fail Case 1: Value on controller does not match what is
        stored in prog.pr """
        prog = self.create_test_prog_3200_object()
        prog.pr = 2
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=1".format(opcodes.priority))
        prog.data = mock_data

        expected_message = "Unable to verify 3200 Program 1's 'Priority Level'. Received: 1, Expected: 2"
        try:
            prog.verify_priority_level_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_mainline_on_cn_pass1(self):
        """ Verify Mainline On Controller Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.mainline))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_mainline_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_mainline_on_cn_fail1(self):
        """ Verify Mainline On Controller Fail Case 1: Value on controller does not match what is
        stored in ad for the POC """
        prog = self.create_test_prog_3200_object()
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=3".format(opcodes.mainline))
        prog.data = mock_data

        expected_message = "Unable to verify 3200 Program 1's 'Mainline'. Received: 3, Expected: 5"
        try:
            prog.verify_mainline_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_booster_pump_on_cn_pass1(self):
        """ Verify Booster Pump On Controller Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        sn_value = prog.mv_objects[3].sn
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.booster_pump, sn_value))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_booster_pump_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_booster_pump_on_cn_fail1(self):
        """ Verify Booster Pump On Controller Fail Case 1: Value on controller does not match what is
        stored in ad for the POC """
        prog = self.create_test_prog_3200_object()
        sn_value = prog.mv_objects[3].sn
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=2".format(opcodes.booster_pump))
        prog.data = mock_data

        expected_message = "Unable to verify 3200 Program 1's 'Booster Pump'. Received: 2, Expected: {0}"\
            .format(sn_value)
        try:
            prog.verify_booster_pump_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_semi_month_interval_list_pass1(self):
        """ Verify Valid Semi Month Interval List Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_valid_semi_month_interval_list(test_value)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_valid_semi_month_interval_list_fail1(self):
        """ Verify Valid Semi Month Interval List Fail Case 1: value passed is more than 24 """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        test_pass = False
        e_msg = ""

        expected_message = "Invalid semi month interval argument passed in. List must contain 24 comma separated " \
                           "integers, instead found: 25"
        try:
            prog.verify_valid_semi_month_interval_list(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_semi_month_interval_list_fail2(self):
        """ Verify Valid Semi Month Interval List Fail Case 2: value passed is less than 24 """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        test_pass = False
        e_msg = ""

        expected_message = "Invalid semi month interval argument passed in. List must contain 24 comma separated " \
                           "integers, instead found: 23"
        try:
            prog.verify_valid_semi_month_interval_list(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_week_day_list_pass1(self):
        """ Verify Valid Week Day List Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1, 1]
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_valid_week_day_list(test_value)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_valid_week_day_list_fail1(self):
        """ Verify Valid Week Day List Fail Case 1: value passed is more than 7 """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1, 1, 1]
        test_pass = False
        e_msg = ""

        expected_message = "Invalid week day water schedule argument passed in. List must contain 7 comma separated " \
                           "integers, instead found: 8"
        try:
            prog.verify_valid_week_day_list(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_week_day_list_fail2(self):
        """ Verify Valid Week Day List Fail Case 2: value passed is less than 7 """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1]
        test_pass = False
        e_msg = ""

        expected_message = "Invalid week day water schedule argument passed in. List must contain 7 comma separated " \
                           "integers, instead found: 6"
        try:
            prog.verify_valid_week_day_list(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_week_day_list_fail3(self):
        """ Verify Valid Week Day List Fail Case 3: value passed contains ints other than 1 or 0 """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1, 2]
        test_pass = False
        e_msg = ""

        expected_message = "Invalid list of week day values for Program 1. Expects only 1's and/or 0's"
        try:
            prog.verify_valid_week_day_list(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_start_times_pass1(self):
        """ Verify Valid Start Times Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        test_value = [60, 120, 480]
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_valid_start_times(test_value)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_valid_start_times_fail1(self):
        """ Verify Valid Start Times Fail Case 1: value passed is more than 8 """
        prog = self.create_test_prog_3200_object()
        test_value = [60, 120, 480, 1, 1, 1, 1, 1, 1]
        test_pass = False
        e_msg = ""

        expected_message = "Invalid number of start times to set for Program 1. Expects 1 to 8 start times, " \
                           "received: 9"
        try:
            prog.verify_valid_start_times(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_start_times_fail2(self):
        """ Verify Valid Start Times Fail Case 2: value passed is less than 1 """
        prog = self.create_test_prog_3200_object()
        test_value = []
        test_pass = False
        e_msg = ""

        expected_message = "Invalid number of start times to set for Program 1. Expects 1 to 8 start times, " \
                           "received: 0"
        try:
            prog.verify_valid_start_times(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_booster_pump_pass1(self):
        """ Verify Valid Booster Pump Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        test_value = 3
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_valid_booster_pump(test_value)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_valid_booster_pump_fail1(self):
        """ Verify Valid Booster Pump Fail Case 1: Value passed is not an available address """
        prog = self.create_test_prog_3200_object()
        test_value = 2
        test_pass = False
        e_msg = ""

        expected_message = "Invalid Booster Pump address for 3200 Program. Valid address are: {0}"\
            .format(prog.mv_objects.keys())
        try:
            prog.verify_valid_booster_pump(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_mainline_pass1(self):
        """ Verify Valid Mainline Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        test_value = 5
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_valid_mainline(test_value)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_valid_mainline_fail1(self):
        """ Verify Valid Mainline Fail Case 1: Value passed is not an available address """
        prog = self.create_test_prog_3200_object()
        test_value = 2
        test_pass = False
        e_msg = ""

        expected_message = "Invalid Mainline address for 3200 Program. Valid address are: {0}"\
            .format(prog.mainline_objects.keys())
        try:
            prog.verify_valid_mainline(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_priority_level_pass1(self):
        """ Verify Valid Priority Level Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        test_value = 2
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_valid_priority_level(test_value)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_valid_priority_level_fail1(self):
        """ Verify Valid Priority Level Fail Case 1: value passed is not 1, 2, or 3 """
        prog = self.create_test_prog_3200_object()
        test_value = 5
        test_pass = False
        e_msg = ""

        expected_message = "Invalid '_priority' (Priority Level) value for 'Set' function for Program 1: 5."\
                           " Accepted values are: 1=high | 2=medium | 3=low"
        try:
            prog.verify_valid_priority_level(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_who_i_am_pass1(self):
        prog = self.create_test_prog_3200_object()

        mock_data = status_parser.KeyValues("PG=1,"
                                            "EN=TR,"
                                            "DS=Test Program 1,"
                                            "WW=111111111111111111111111,"
                                            "MC=1,"
                                            "SA=100,"
                                            "CI=WD,"
                                            "WD=1111111,"
                                            "ST=480,"
                                            "PR=2,"
                                            "ML=5,"
                                            "BP=MVD0001")
        prog.data = mock_data
        prog.get_data = mock.MagicMock(sideeffect=None)
        prog.verify_who_i_am()

    if __name__ == "__main__":
        unittest.main()