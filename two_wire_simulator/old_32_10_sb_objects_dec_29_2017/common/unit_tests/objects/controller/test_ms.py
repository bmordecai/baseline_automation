import unittest
import mock
import serial
import status_parser

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
# you have to set the lat and long after the devices class is called so that they don't get skipped over
Devices.controller_lat = float(43.609768)
Devices.controller_long = float(-116.310569)

from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ms import MoistureSensor


__author__ = 'Brice "Ajo Grande" Garlick'


class TestMoistureSensorObject(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.309759

    Moisture Sensor Starting lat & lng:
    latitude = 43.609768
    longitude = -116.309354
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """

        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)
        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        Devices.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_moisture_sensor_object(self, _diffserial=None, _diffaddress=None):
        """ Creates a new Moisture Sensor object for testing purposes """
        if _diffserial is not None:
            ms = MoistureSensor(_serial="TSD0001", _address=_diffaddress)
        elif _diffaddress is not None:
            ms = MoistureSensor(_serial=_diffserial, _address=1)
        else:
            ms = MoistureSensor(_serial="TSD0001", _address=1)

        return ms

    #################################
    def test_set_default_values_pass1(self):
        """ Set Default Values On Moisture Sensor Pass Case 1: Correct Command Sent """
        expected_command = "SET," \
                           "MS=TSD0001," \
                           "DS=TSD0001 Test Moisture Sensor 1," \
                           "LA=43.609773," \
                           "LG=-116.310064," \
                           "VP=21.3," \
                           "VD=71.5," \
                           "VT=0.0"
        ms = self.create_test_moisture_sensor_object()

        ms.set_default_values()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values On Moisture Sensor Fail Case 1: Failed communication with controller """
        expected_command = "SET," \
                           "MS=TSD0001," \
                           "DS=TSD0001 Test Moisture Sensor 1," \
                           "LA=43.609773," \
                           "LG=-116.310064," \
                           "VP=21.3," \
                           "VD=71.5," \
                           "VT=0.0"
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            ms = self.create_test_moisture_sensor_object()
            ms.set_default_values()
        e_msg = "Exception occurred trying to set Moisture Sensor TSD0001 (1)'s 'Default values' to: '{0}'".format(
                expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_moisture_percent_on_cn_pass1(self):
        """ Set Moisture Percent On Controller Pass Case 1: Using Default _vp value """
        ms = self.create_test_moisture_sensor_object()

        #Expected value is the _vp value set at object Zone object creation
        expected_value = ms.vp
        ms.set_moisture_percent_on_cn()

        #_vp value is set during this method and should equal the original value
        actual_value = ms.vp
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_moisture_percent_on_cn_pass2(self):
        """ Set Moisture Percent On Controller Pass Case 2: Setting new _vp value = 6.0 """
        ms = self.create_test_moisture_sensor_object()

        #Expected _vp value is 6
        expected_value = 6.0
        ms.set_moisture_percent_on_cn(expected_value)

        #_vp value is set during this method and should equal the value passed into the method
        actual_value = ms.vp
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_moisture_percent_on_cn_pass3(self):
        """ Set Moisture Percent On Controller Pass Case 3: Command with correct values sent to controller """
        ms = self.create_test_moisture_sensor_object()

        ms_value = str(ms.vp)
        expected_command = "SET,MS=TSD0001,{0}={1}".format(opcodes.moisture_percent, ms_value)
        ms.set_moisture_percent_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_moisture_percent_on_cn_fail1(self):
        """ Set Moisture Percent On Controller Fail Case 1: Pass String value as argument """
        ms = self.create_test_moisture_sensor_object()

        new_vp_value = "b"
        with self.assertRaises(Exception) as context:
            ms.set_moisture_percent_on_cn(new_vp_value)
        expected_message = "Failed trying to set MS {0} ({1}) moisture percent. Invalid argument type, " \
                           "expected an int or float, received: {2}".format(str(ms.sn), str(ms.ad), type(new_vp_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_moisture_percent_on_cn_fail2(self):
        """ Set Moisture Percent On Controller Fail Case 2: Failed communication with controller """
        ms = self.create_test_moisture_sensor_object()

        #A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ms.set_moisture_percent_on_cn()
        e_msg = "Exception occurred trying to set Moisture Sensor {0} ({1})'s 'Moisture Percent' to: '{2}'".format(
                ms.sn, str(ms.ad), str(ms.vp))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_temperature_reading_on_cn_pass1(self):
        """ Set Temperature Reading On Controller Pass Case 1: Using Default _vd value """
        ms = self.create_test_moisture_sensor_object()

        #Expected value is the _vd value set at object Zone object creation
        expected_value = ms.vd
        ms.set_temperature_reading_on_cn()

        #_vd value is set during this method and should equal the original value
        actual_value = ms.vd
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_temperature_reading_on_cn_pass2(self):
        """ Set Temperature Reading On Controller Pass Case 2: Setting new _vd value = 6.0 """
        ms = self.create_test_moisture_sensor_object()

        #Expected _vd value is 6
        expected_value = 6.0
        ms.set_temperature_reading_on_cn(expected_value)

        #_vd value is set during this method and should equal the value passed into the method
        actual_value = ms.vd
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_temperature_reading_on_cn_pass3(self):
        """ Set Temperature Reading On Controller Pass Case 3: Command with correct values sent to controller """
        ms = self.create_test_moisture_sensor_object()

        ms_value = str(ms.vd)
        expected_command = "SET,MS=TSD0001,{0}={1}".format(opcodes.temperature_value, ms_value)
        ms.set_temperature_reading_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_temperature_reading_on_cn_fail1(self):
        """ Set Temperature Reading On Controller Fail Case 1: Pass String value as argument """
        ms = self.create_test_moisture_sensor_object()

        new_vd_value = "b"
        with self.assertRaises(Exception) as context:
            ms.set_temperature_reading_on_cn(new_vd_value)
        expected_message = "Failed trying to set MS {0} ({1}) temperature reading. Invalid argument type, " \
                           "expected an int or float, received: {2}".format(str(ms.sn), str(ms.ad), type(new_vd_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_temperature_reading_on_cn_fail2(self):
        """ Set Temperature Reading On Controller Fail Case 2: Failed communication with controller """
        ms = self.create_test_moisture_sensor_object()

        #A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ms.set_temperature_reading_on_cn()
        e_msg = "Exception occurred trying to set Moisture Sensor {0} ({1})'s 'Temperature Reading' to: '{2}'".format(
                ms.sn, str(ms.ad), str(ms.vd))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass1(self):
        """ Set Two Wire Drop Value On Controller Pass Case 1: Using Default _vt value """
        ms = self.create_test_moisture_sensor_object()

        #Expected value is the _vt value set at object Zone object creation
        expected_value = ms.vt
        ms.set_two_wire_drop_value_on_cn()

        #_vt value is set during this method and should equal the original value
        actual_value = ms.vt
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass2(self):
        """ Set Two Wire Drop Value On Controller Pass Case 2: Setting new _vd value = 6.0 """
        ms = self.create_test_moisture_sensor_object()

        #Expected _vt value is 6
        expected_value = 6.0
        ms.set_two_wire_drop_value_on_cn(expected_value)

        #_vt value is set during this method and should equal the value passed into the method
        actual_value = ms.vt
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass3(self):
        """ Set Two Wire Drop Value On Controller Pass Case 3: Command with correct values sent to controller """
        ms = self.create_test_moisture_sensor_object()

        vt_value = str(ms.vt)
        expected_command = "SET,MS=TSD0001,{0}={1}".format(opcodes.two_wire_drop, vt_value)
        ms.set_two_wire_drop_value_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_two_wire_drop_value_on_cn_fail1(self):
        """ Set Two Wire Drop Value On Controller Fail Case 1: Pass String value as argument """
        ms = self.create_test_moisture_sensor_object()

        new_vt_value = "b"
        with self.assertRaises(Exception) as context:
            ms.set_two_wire_drop_value_on_cn(new_vt_value)
        expected_message = "Failed trying to set MS {0} ({1}) two wire drop. Invalid argument type, " \
                           "expected an int or float, received: {2}".format(str(ms.sn), str(ms.ad), type(new_vt_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_two_wire_drop_value_on_cn_fail2(self):
        """ Set Two Wire Drop Value On Controller Fail Case 2: Failed communication with controller """
        ms = self.create_test_moisture_sensor_object()

        #A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ms.set_two_wire_drop_value_on_cn()
        e_msg = "Exception occurred trying to set Moisture Sensor {0} ({1})'s 'Two Wire Drop Value' to: '{2}'".format(
                ms.sn, str(ms.ad), str(ms.vt))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_moisture_percent_on_cn_pass1(self):
        """ Verify Moisture Percent On Controller Pass Case 1: Exception is not raised """
        ms = self.create_test_moisture_sensor_object()
        ms.vp = 22.4
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=22.4".format(opcodes.moisture_percent))
        ms.data = mock_data

        try:
            #.assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ms.verify_moisture_percent_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_moisture_percent_on_cn_fail1(self):
        """ Verify Moisture Percent On Controller Fail Case 1: Value on controller does not match what is
        stored in ms.vp """
        ms = self.create_test_moisture_sensor_object()
        ms.vp = 22.4
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=23.2".format(opcodes.moisture_percent))
        ms.data = mock_data

        expected_message = "Unable verify Moisture Sensor TSD0001 (1)s moisture percent reading. Received: 23.2, " \
                           "Expected: 22.4"
        try:
            ms.verify_moisture_percent_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_temperature_reading_on_cn_pass1(self):
        """ Verify Temperature Reading On Controller Pass Case 1: Exception is not raised """
        ms = self.create_test_moisture_sensor_object()
        ms.vd = 22.4
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=22.4".format(opcodes.temperature_value))
        ms.data = mock_data

        try:
            #.assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ms.verify_temperature_reading_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_temperature_reading_on_cn_fail1(self):
        """ Verify Temperature Reading On Controller Fail Case 1: Value on controller does not match what is
        stored in ms.vd """
        ms = self.create_test_moisture_sensor_object()
        ms.vd = 22.4
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=23.2".format(opcodes.temperature_value))
        ms.data = mock_data

        expected_message = "Unable verify Moisture Sensor TSD0001 (1)'s temperature reading. Received: 23.2, " \
                           "Expected: 22.4"
        try:
            ms.verify_temperature_reading_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_two_wire_drop_value_on_cn_pass1(self):
        """ Verify Two Wire Drop Value On Controller Pass Case 1: Exception is not raised """
        ms = self.create_test_moisture_sensor_object()
        ms.vt = 22.4
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=22.4".format(opcodes.two_wire_drop))
        ms.data = mock_data

        try:
            #.assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ms.verify_two_wire_drop_value_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_two_wire_drop_value_on_cn_fail1(self):
        """ Verify Two Wire Drop Value On Controller Fail Case 1: Value on controller does not match what is
        stored in ms.vt """
        ms = self.create_test_moisture_sensor_object()
        ms.vt = 22.4
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=23.2".format(opcodes.two_wire_drop))
        ms.data = mock_data

        expected_message = "Unable verify Moisture Sensor TSD0001 (1)'s two wire drop value. Received: 23.2, " \
                           "Expected: 22.4"
        try:
            ms.verify_two_wire_drop_value_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_self_test_and_update_object_attributes_pass1(self):
        """ Verify Self Test and Update Object Attributes Pass Case 1: Get data and then assign the new values to the objects attributes
        Create test moisture sensor object
        Create the variables required to test the method
        Mock: do_self_test because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: get_data because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: data and get_value_string_by_key so we can output the values we want
            Return: Three values, one each for moisture_percent, temperature_value, and two_wire_drop
        Call the method
        """
        # Create the moisture sensor object
        ms = self.create_test_moisture_sensor_object()

        vp_value = 2.5
        vd_value = 5
        vt_value = 26.5

        # Mock the do_self_test method
        mock_do_self_test = mock.MagicMock()
        ms.do_self_test = mock_do_self_test

        # Mock the get_data method
        mock_get_data = mock.MagicMock()
        ms.get_data = mock_get_data

        # Mock get_value_string_by_key and the data object that is in the moisture sensor object
        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(side_effect=[vp_value, vd_value, vt_value])
        ms.data = mock_data

        # Call the method
        ms.self_test_and_update_object_attributes()

        # Verify the values were set
        self.assertEqual(vp_value, ms.vp)
        self.assertEqual(vd_value, ms.vd)
        self.assertEqual(vt_value, ms.vt)

    #################################
    def test_self_test_and_update_object_attributes_fail1(self):
        """ Verify Self Test and Update Object Attributes Fail Case 1: Catch Exception that is thrown
        Create test moisture sensor object
        Create the variables required to test the method
        Mock: do_self_test because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: get_data because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: data and get_value_string_by_key so we can output the values we want
            Return: Three values, one each for moisture_percent, temperature_value, and two_wire_drop
        Create the expected message
        Call the method
        Compare the expected error message with the actual error message
        """
        # Create the moisture sensor object
        ms = self.create_test_moisture_sensor_object()

        # Mock the do_self_test method
        mock_do_self_test = mock.MagicMock()
        ms.do_self_test = mock_do_self_test

        # Mock the get_data method
        mock_get_data = mock.MagicMock()
        ms.get_data = mock_get_data

        # Mock get_value_string_by_key and the data object that is in the moisture sensor object
        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(side_effect=Exception)
        ms.data = mock_data

        # Create the expected message
        expected_msg = "Exception occurred trying to updating attributes of the moisture sensor {0} object." \
                       " Moisture Value: '{1}'," \
                       " Temperature Value: '{2}'," \
                       " Two Wire Drop Value: '{3}'" \
                       .format(
                           str(ms.ad),  # {0}
                           str(ms.vp),  # {1}
                           str(ms.vd),  # {2}
                           str(ms.vt),  # {3}
                       )

        # Run method that will tested
        with self.assertRaises(Exception) as context:
            ms.self_test_and_update_object_attributes()

        # Compare the expected with the actual
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_who_i_am(self):
        """ Verify Who I Am Pass Case 1: Runs through the method successfully
        Create test moisture sensor object
        Mock: get_data because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_description_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_serial_number_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_latitude_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_longitude_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_status_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_moisture_percent_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_temperature_reading_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Call the method and make sure it runs through successfully
        """
        # Creates the moisture sensor object
        ms = self.create_test_moisture_sensor_object()

        # Mock the get_data method
        mock_get_data = mock.MagicMock()
        ms.get_data = mock_get_data

        # Mock the verify_description_on_cn method
        mock_verify_description_on_cn = mock.MagicMock()
        ms.verify_description_on_cn = mock_verify_description_on_cn

        # Mock the verify_serial_number_on_cn method
        mock_verify_serial_number_on_cn = mock.MagicMock()
        ms.verify_serial_number_on_cn = mock_verify_serial_number_on_cn

        # Mock the verify_latitude_on_cn method
        mock_verify_latitude_on_cn = mock.MagicMock()
        ms.verify_latitude_on_cn = mock_verify_latitude_on_cn

        # Mock the verify_longitude_on_cn method
        mock_verify_longitude_on_cn = mock.MagicMock()
        ms.verify_longitude_on_cn = mock_verify_longitude_on_cn

        # Mock the verify_status_on_cn method
        mock_verify_status_on_cn = mock.MagicMock()
        ms.verify_status_on_cn = mock_verify_status_on_cn

        # Mock the verify_moisture_percent_on_cn method
        mock_verify_moisture_percent_on_cn = mock.MagicMock()
        ms.verify_moisture_percent_on_cn = mock_verify_moisture_percent_on_cn

        # Mock the verify_temperature_reading_on_cn method
        mock_verify_temperature_reading_on_cn = mock.MagicMock()
        ms.verify_temperature_reading_on_cn = mock_verify_temperature_reading_on_cn

        # There is nothing to assert in this method call
        ms.verify_who_i_am(_expected_status=opcodes.bad_serial)

    if __name__ == "__main__":
        unittest.main()
