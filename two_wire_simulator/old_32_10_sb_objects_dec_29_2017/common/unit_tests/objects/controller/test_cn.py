__author__ = 'Brice "Ajo Grande" Garlick'

import unittest

import mock
import serial
import status_parser
import datetime

from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.cn import Controller
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# Unittest the following methods:
# Todo: set_ai_for_cn
# Todo: verify_ip_address_state
# Todo: init_cn
# Todo: do_increment_clock
# Todo: do_reboot_controller


class TestControllerObject(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.310569
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Set serial instance to mock serial
        Devices.ser = self.mock_ser

        Devices.controller_lat = float(43.609768)
        Devices.controller_long = float(-116.310569)

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """ Cleaning up after the test. """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_controller_object(self, _difftype=None, _diffmac=None, _diffdescription=None):
        """ Creates a new controller object for use in a unit test """
        if _difftype is not None:
            controller = Controller(_type=_difftype, _mac="0008EE218C85", _description="TSD0001 Test Controller")
        elif _diffmac is not None:
            controller = Controller(_type="32", _mac=_diffmac, _description="TSD0001 Test Controller")
        elif _diffdescription is not None:
            controller = Controller(_type="32", _mac="0008EE218C85", _description=_diffdescription)
        else:
            controller = Controller(_type="32", _mac="0008EE218C85", _description="TSD0001 Test Controller")
        return controller

    #################################
    def test_set_default_values_pass1(self):
        """ Set Default Values On 1000 Controller Pass Case 1: Correct Command Sent """
        Devices.controller_type = "10"
        expected_command = "SET," \
                           "CN," \
                           "SN=1K10001," \
                           "DS=TSD0001 Test Controller," \
                           "LA=43.609768," \
                           "LG=-116.310569," \
                           "MC=1," \
                           "RP=0," \
                           "JR=CL"
        controller = Controller(_type="10", _mac="0008EE218C85", _description="TSD0001 Test Controller")
        self.mock_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values On 1000 Controller Fail Case 1: Handle Exception Raised From Serial Object """
        Devices.controller_type = "10"
        expected_command = "SET," \
                           "CN," \
                           "SN=3K10001," \
                           "DS=TSD0001 Test Controller," \
                           "LA=43.609768," \
                           "LG=-116.310569," \
                           "MC=1," \
                           "RP=0," \
                           "JR=CL," \
                           "JF=CL," \
                           "JP=CL"
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            self.create_test_controller_object()
        e_msg = "Exception occurred trying to set Controller 0008EE218C85's 'Default values' to: '{0}'. " \
                "Exception received: ".format(expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test___str___pass1(self):
        """  Returns string representation of the controller object pass case 1: tests that the value returned is the
        same as the value entered. """
        # Create controller object and a devices object

        controller = self.create_test_controller_object()
        devices = self.create_test_controller_object()

        # Store values for each string representation

        controller.ty = "32"
        controller.sn = "3K10001"
        controller.mac = '12345678901234'
        controller.ds = 'TSD001 test controller'
        controller.la = '43.1234613'
        controller.lg = '-123.0987807'
        controller.mc = '1'
        controller.rp = '12'
        controller.vr = '3'
        controller.ss = 'All ready'
        controller.jr = 'OK'
        controller.jf = 'CL'
        controller.jp = 'CL'
        devices.et = '0.0'
        devices.ra = '0'
        controller.ed = '000000000'
        controller.ei = '34'

        # Dive into the method to test

        returned_string = controller.__str__()

        # Compare each returned string with the expected string (formatting from the original method is copied over)

        self.assertEqual(returned_string,
                         "\n-----------------------------------------\n" \
                         "Controller Object:\n" \
                         "Type:                     {0}\n" \
                         "Serial Number:            {1}\n" \
                         "Mac Address:              {2}\n" \
                         "Description:              {3}\n" \
                         "Latitude:                 {4}\n" \
                         "Longitude:                {5}\n" \
                         "Max Concurrent Zones:     {6}\n" \
                         "Rain Pause Days:          {7} days\n" \
                         "Code Version:             {8}\n" \
                         "Status:                   {9}\n" \
                         "Rain Jumper State:        {10}\n" \
                         "Flow Jumper State:        {11}\n" \
                         "Pause Jumper State:       {12}\n" \
                         "Controller ETo Value:     {13}\n" \
                         "Controller rain value:    {14}\n" \
                         "Controller ETo Date:      {15}\n" \
                         "Initial Controller ETo:   {16}\n" \
                         "Memory Usage:             {17}\n" \
                         "-----------------------------------------\n".format(
                             controller.ty,         # {0}
                             controller.sn,         # {1}
                             controller.mac,        # {2}
                             controller.ds,         # {3}
                             str(controller.la),    # {4}
                             str(controller.lg),    # {5}
                             str(controller.mc),    # {6}
                             str(controller.rp),    # {7}
                             str(controller.vr),    # {8}
                             controller.ss,         # {9}
                             str(controller.jr),    # {10}
                             str(controller.jf),    # {11}
                             str(controller.jp),    # {12}
                             str(devices.et),       # {13}
                             str(devices.ra),       # {14}
                             str(controller.ed),    # (15}
                             str(controller.ei),    # {16}
                             str(controller.mu)     # {17}
                         ))


    #################################
    def test_is1000_pass1(self):
        """ Is 1000 Pass Case 1:  Returns True if Controller is 1000, else returns False """
        controller = self.create_test_controller_object(_difftype="10")

        is1000 = controller.is1000()
        self.assertTrue(is1000)

        controller.ty = "32"
        is1000 = controller.is1000()
        self.assertFalse(is1000)

    #################################
    def test_is3200_pass1(self):
        """ Is 3200 Pass Case 1:  Returns True if Controller is 3200, else returns False """
        controller = self.create_test_controller_object(_difftype="32")

        is3200 = controller.is3200()
        self.assertTrue(is3200)

        controller.ty = "10"
        is3200 = controller.is3200()
        self.assertFalse(is3200)

    #################################
    def test_set_max_concurrent_zones_on_cn_pass1(self):
        """ Set Max Concurrent Zones On Controller Pass Case 1: Use default _mc value """
        controller = self.create_test_controller_object()

        # Expected value is the _mc value set at object Zone object creation
        expected_value = controller.mc
        controller.set_max_concurrent_zones_on_cn()

        # _mc value is set during this method and should equal the original value
        actual_value = controller.mc
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_max_concurrent_zones_on_cn_pass2(self):
        """ Set Max Concurrent Zones On Controller Pass Case 2: Set 5 as _mc value """
        controller = self.create_test_controller_object()

        # Expected value is the _mc value set at object Zone object creation
        expected_value = 5
        controller.set_max_concurrent_zones_on_cn(expected_value)
        # _mc value is set during this method and should equal the original value
        actual_value = controller.mc
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_max_concurrent_zones_on_cn_pass3(self):
        """ Set Max Concurrent Zones On Controller Pass Case 3: Correct command is sent to controller """
        controller = self.create_test_controller_object()

        mc_value = str(controller.mc)
        expected_command = "SET,CN,MC=" + mc_value
        controller.set_max_concurrent_zones_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_max_concurrent_zones_on_cn_fail1(self):
        """ Set Max Concurrent Zones On Controller Fail Case 1: Pass String value as argument """
        controller = self.create_test_controller_object()

        new_mc_value = "b"
        with self.assertRaises(Exception) as context:
            controller.set_max_concurrent_zones_on_cn(new_mc_value)
        expected_message = "Failed trying to set max concurrent zones for controller. Invalid type passed in, " \
                           "expected integer.  Type Received: {0}".format(type(new_mc_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_max_concurrent_zones_on_cn_fail2(self):
        """ Set Max Concurrent Zones On Controller Fail Case 2: Failed communication with controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.set_max_concurrent_zones_on_cn()
        e_msg = "Exception occurred trying to set Controller's 'Max Concurrent Zones' to: '{0}'" \
                .format(str(controller.mc))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_serial_number_on_cn_pass1(self):
        """ Sets the serial number of the controller pass case 1: able to set serial number"""
        # Create controller object
        cn = self.create_test_controller_object()

        # Because the method strips off the first to character and appends it to 3K the error message is as follows
        # Store intended serial number
        ser_num = '3K34567'

        # Go into method, overwrite should be intended, thus serial number should be a string
        # Number of characters in string needs to be 7
        cn.set_serial_number_on_cn(_serial_num=ser_num)

        # Check that serial number equals the new serial number
        self.assertEqual(ser_num, cn.sn)

    #################################
    def test_set_serial_number_on_cn_fail1(self):
        """ Sets the serial number of the controller fail case 1: The controller needs a seven digit string passed
        in as a serial number """
        # Create controller object
        cn = self.create_test_controller_object()

        # Stores serial number
        bad_ser_num = '1234'

        # Because the method strips off the first to character and appends it to 3K the error message is as follows
        # Store the expected error message
        e_msg = "controller serial number must be a seven digit string 3K34"

        # Go into method and pass in something besides a seven digit serial number as the serial number parameter
        # Prepare to have a value error
        with self.assertRaises(ValueError) as context:
            cn.set_serial_number_on_cn(_serial_num=bad_ser_num)

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_serial_number_on_cn_fail2(self):
        """
        set_serial_number_on_cn fail case 2
        Create: Test controller object
        Store: Expected error message
        Run: Test, with _serial_num equal to an integer, expect a type error as it expects a string
        Compare: Expected error message to actual error message
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error message
        e_msg = "Serial number must be in a string format '3'"

        # Run the test, expect a type error
        with self.assertRaises(TypeError) as context:
            cn.set_serial_number_on_cn(_serial_num=3)

        # Compare the actual error message to the expected error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_serial_number_on_cn_fail3(self):
        """
        set_serial_number_on_cn fail case 3
        force the exception
        Create: Test controller object
        Store: Expected error message
        Mock: send_and_wait_for_reply
        Return: Exception, so it will fail
        Run: Test, with exception expected
        Compare: Actual error message to expected error message
        """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store alternate bad serial number
        bad_ser_num = '1234567'
        # the method strips off the first to characters passed in and than appends that to 3k therefor the message
        # would be the following
        # Store expected error message
        e_msg = "Exception occurred trying to set Controller's 'Serial number' to: '3K34567'"

        # Mock an exception on serial send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Go into the method and store the serial number as something besides a string, prepare to raise an exception
        with self.assertRaises(Exception) as context:
            cn.set_serial_number_on_cn(_serial_num=bad_ser_num)

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_rain_delay_on_cn_pass1(self):
        """ Set Rain Delay On Controller Pass Case 1: Use default _rp value """
        controller = self.create_test_controller_object()

        # Expected value is the _rp value set at object Zone object creation
        expected_value = controller.rp
        controller.set_rain_delay_on_cn()

        # _rp value is set during this method and should equal the original value
        actual_value = controller.rp
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_rain_delay_on_cn_pass2(self):
        """ Set Rain Delay On Controller Pass Case 2: Set 5 as _rp value """
        controller = self.create_test_controller_object()

        # Expected value is the _rp value set at object Zone object creation
        expected_value = 5
        controller.set_rain_delay_on_cn(expected_value)

        # _rp value is set during this method and should equal the original value
        actual_value = controller.rp
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_rain_delay_on_cn_pass3(self):
        """ Set Rain Delay On Controller Pass Case 3: Correct command is sent to controller """
        controller = self.create_test_controller_object()

        rp_value = str(controller.rp)
        expected_command = "SET,CN,RP=" + rp_value
        controller.set_rain_delay_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_rain_delay_on_cn_fail1(self):
        """ Set Rain Delay On Controller Fail Case 1: Pass String value as argument """
        controller = self.create_test_controller_object()

        new_rp_value = "b"
        with self.assertRaises(Exception) as context:
            controller.set_rain_delay_on_cn(new_rp_value)
        expected_message = "Failed trying to set rain delay for controller. Invalid type passed in, " \
                           "expected integer. Type Received: {0}".format(type(new_rp_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_rain_delay_on_cn_fail2(self):
        """ Set Rain Delay On Controller Fail Case 2: Failed communication with controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.set_rain_delay_on_cn()
        e_msg = "Exception occurred trying to set Controller's 'Rain Delay' to: '{0}'" \
                .format(str(controller.rp))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_rain_jumper_state_on_cn_pass1(self):
        """ Set Rain Jumper State On Controller Pass Case 1: Use default _jr value """
        controller = self.create_test_controller_object()

        # Expected value is the _jr value set at object Zone object creation
        expected_value = controller.jr
        controller.set_rain_jumper_state_on_cn()

        # _jr value is set during this method and should equal the original value
        actual_value = controller.jr
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_rain_jumper_state_on_cn_pass2(self):
        """ Set Rain Jumper State On Controller Pass Case 2: Set OP as _jr value """
        controller = self.create_test_controller_object()

        # Expected value is the _jr value set at object Zone object creation
        expected_value = "OP"
        controller.set_rain_jumper_state_on_cn(expected_value)

        # _jr value is set during this method and should equal the original value
        actual_value = controller.jr
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_rain_jumper_state_on_cn_pass3(self):
        """ Set Rain Jumper State On Controller Pass Case 3: Correct command is sent to controller """
        controller = self.create_test_controller_object()

        jr_value = controller.jr
        expected_command = "SET,CN,JR=" + jr_value
        controller.set_rain_jumper_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_rain_jumper_state_on_cn_fail1(self):
        """ Set Rain Jumper State On Controller Fail Case 1: Pass value other than 'OP' or 'CL' as argument """
        controller = self.create_test_controller_object()

        new_jr_value = "open"
        with self.assertRaises(Exception) as context:
            controller.set_rain_jumper_state_on_cn(new_jr_value)
        expected_message = "Exception occurred attempting to set incorrect 'Rain Jumper State' value for controller: " \
                           "'{0}', Expects 'CL' | 'OP'.".format(new_jr_value)
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_rain_jumper_state_on_cn_fail2(self):
        """ Set Rain Jumper State On Controller Fail Case 2: Failed communication with controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.set_rain_jumper_state_on_cn()
        e_msg = "Exception occurred trying to set Controller's 'Rain Jumper State' to: '{0}'" \
                .format(str(controller.jr))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_flow_rain_jumper_state_on_cn_pass1(self):
        """ Set flow Jumper State On Controller Pass Case 1: Use default _jf value """
        controller = self.create_test_controller_object()

        # Expected value is the _jf value set at object Zone object creation
        expected_value = controller.jf
        controller.set_flow_jumper_state_on_cn()

        # _jf value is set during this method and should equal the original value
        actual_value = controller.jf
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_flow_jumper_state_on_cn_pass2(self):
        """ Set Flow Jumper State On Controller Pass Case 2: Set OP as _jf value """
        controller = self.create_test_controller_object()

        # Expected value is the _jf value set at object Zone object creation
        expected_value = "OP"
        controller.set_flow_jumper_state_on_cn(expected_value)

        # _jf value is set during this method and should equal the original value
        actual_value = controller.jf
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_flow_jumper_state_on_cn_pass3(self):
        """ Set Flow Jumper State On Controller Pass Case 3: Correct command is sent to controller """
        controller = self.create_test_controller_object()

        jf_value = controller.jf
        expected_command = "SET,CN,JF=" + jf_value
        controller.set_flow_jumper_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_flow_jumper_state_on_cn_fail1(self):
        """ Set Flow Jumper State On Controller Fail Case 1: Pass value other than 'OP' or 'CL' as argument """
        controller = self.create_test_controller_object()

        new_jf_value = "open"
        with self.assertRaises(Exception) as context:
            controller.set_flow_jumper_state_on_cn(new_jf_value)
        expected_message = "Exception occurred attempting to set incorrect 'Flow Jumper State' value for controller: " \
                           "'{0}', Expects 'CL' | 'OP'.".format(new_jf_value)
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_flow_jumper_state_on_cn_fail2(self):
        """ Set Flow Jumper State On Controller Fail Case 2: Failed communication with controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.set_flow_jumper_state_on_cn()
        e_msg = "Exception occurred trying to set Controller's 'Flow Jumper State' to: '{0}'" \
                .format(str(controller.jr))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_flow_jumper_state_on_cn_fail3(self):
        """
        set_flow_jumper_state_on_cn fail case 3
        Create: Test controller object
        Store: Expected error message
        Mock: is3200 method
        Return: False, as the is3200 method will return false if the controller is not a 3200
        Run: Test, and prepare to raise a ValueError
        Compare: Expected error message to actual error message
        """
        # Create the test controller object
        cn = self.create_test_controller_object()

        # Store expected error message
        e_msg = "Attempting to set flow jumper value to a 1000, which is currently not supported."

        # Set the controller type to 1000
        mock_is3200 = mock.MagicMock(return_value=False)
        cn.is3200 = mock_is3200

        # Run test, expect a ValueError
        with self.assertRaises(ValueError) as context:
            cn.set_flow_jumper_state_on_cn()

        # Compare actual error message to expected error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_pause_rain_jumper_state_on_cn_pass1(self):
        """ Set Pause Jumper State On Controller Pass Case 1: Use default _jp value """
        controller = self.create_test_controller_object()

        # Expected value is the _jp value set at object Zone object creation
        expected_value = controller.jp
        controller.set_pause_jumper_state_on_cn()

        # _jp value is set during this method and should equal the original value
        actual_value = controller.jp
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_pause_jumper_state_on_cn_pass2(self):
        """ Set Pause Jumper State On Controller Pass Case 2: Set OP as _jp value """
        controller = self.create_test_controller_object()

        # Expected value is the _jp value set at object Zone object creation
        expected_value = "OP"
        controller.set_pause_jumper_state_on_cn(expected_value)

        # _jp value is set during this method and should equal the original value
        actual_value = controller.jp
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_pause_jumper_state_on_cn_pass3(self):
        """ Set Pause Jumper State On Controller Pass Case 3: Correct command is sent to controller """
        controller = self.create_test_controller_object()

        jp_value = controller.jp
        expected_command = "SET,CN,JP=" + jp_value
        controller.set_pause_jumper_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_pause_jumper_state_on_cn_fail1(self):
        """ Set Pause Jumper State On Controller Fail Case 1: Pass value other than 'OP' or 'CL' as argument """
        controller = self.create_test_controller_object()

        new_jp_value = "open"
        with self.assertRaises(Exception) as context:
            controller.set_pause_jumper_state_on_cn(new_jp_value)
        expected_message = "Exception occurred attempting to set incorrect 'Pause Jumper State' value for " \
                           "controller: '{0}', Expects 'CL' | 'OP'.".format(new_jp_value)
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_pause_jumper_state_on_cn_fail2(self):
        """ Set Pause Jumper State On Controller Fail Case 2: Failed communication with controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.set_pause_jumper_state_on_cn()
        e_msg = "Exception occurred trying to set Controller's 'Pause Jumper State' to: '{0}'" \
                .format(str(controller.jp))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_pause_jumper_state_on_cn_fail3(self):
        """ Set Pause Jumper State On Controller Fail Case 2: Attempted on 1000 which is not supported """
        controller = self.create_test_controller_object(_difftype="10")

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.set_pause_jumper_state_on_cn()
        e_msg = "Attempting to set pause jumper value to a 1000, which is currently not supported."
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_date_and_time_on_cn_pass1(self):
        """ Set Date and Time On Controller Pass Case 1: Pass in 01/01/2015 and 10:30:00 """
        controller = self.create_test_controller_object()

        date = '01/01/2015'
        new_data = '10:30:00'
        expected_command = 'SET,DT=' + str(date) + ' ' + str(new_data)
        controller.set_date_and_time_on_cn(date, new_data)
        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    #################################
    def test_set_date_and_time_on_cn_fail1(self):
        """ Set Date and Time On Controller Fail Case 1: Use incorrect date format - 01-01-2015 """
        controller = self.create_test_controller_object()

        date = '01-01-2015'
        new_data = '10:30:00'
        with self.assertRaises(Exception) as context:
            controller.set_date_and_time_on_cn(date, new_data)
        expected_message = "Incorrect data format, should be MM/DD/YYYY"
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_date_and_time_on_cn_fail2(self):
        """ Set Date and Time On Controller Fail Case 2: Use incorrect time format - 10.30.00 """
        controller = self.create_test_controller_object()

        date = '01/01/2015'
        new_data = '10.30.00'
        with self.assertRaises(Exception) as context:
            controller.set_date_and_time_on_cn(date, new_data)
        expected_message = "Incorrect data format, should be HH:MM:SS"
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_date_and_time_on_cn_fail3(self):
        """ Set Date and Time On Controller Fail Case 3: Failed Communication with Controller """
        controller = self.create_test_controller_object()

        date = '01/01/2015'
        new_data = '10:30:00'

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.set_date_and_time_on_cn(date, new_data)
        e_msg = "Exception occurred trying to set Controller's Date and Time to: '{0}' '{1}'" \
                .format(str(date), str(new_data))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_sim_mode_to_on_pass1(self):
        """ Set Sim Mode To On Pass Case 1: Controller is successfully set to sim mode """
        controller = self.create_test_controller_object()

        expected_command = 'DO,SM=TR'
        controller.set_sim_mode_to_on()
        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    #################################
    def test_set_sim_mode_to_on_fail1(self):
        """ Set Sim Mode To On Fail Case 1: Failed Communication with Controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.set_sim_mode_to_on()
        e_msg = "Turn On Sim Mode Command Failed: "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_sim_mode_to_off_pass1(self):
        """ Set Sim Mode To Off Pass Case 1: Controller is successfully set to sim mode """
        controller = self.create_test_controller_object()

        expected_command = 'DO,SM=FA'
        controller.set_sim_mode_to_off()
        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    #################################
    def test_set_sim_mode_to_off_fail1(self):
        """ Set Sim Mode To Off Fail Case 1: Failed Communication with Controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.set_sim_mode_to_off()
        e_msg = "Turn Off Sim Mode Command Failed: "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_controller_to_run_pass1(self):
        """ Sets the controller to run pass case 1: Controller type is 1000 """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store the expected serial send and wait for reply value
        expected_value = 'KEY,DL=5'

        # Set controller type to 1000
        cn.ty = "10"

        # Increment the clock
        clock_incrementation = mock.MagicMock(return_value=1)
        cn.do_increment_clock = clock_incrementation

        # Go into method to test
        cn.set_controller_to_run()

        # Check if the serial send and wait for reply's value matches the expected value
        # self.assertEqual(expected_value, expected_value)

    #################################
    def test_set_controller_to_run_pass2(self):
        """ Sets the controller to run pass case 2: Controller type is set to default """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store the expected serial send and wait for reply value
        expected_value = 'KEY,DL=15'

        # Increment the clock
        clock_incrementation = mock.MagicMock(return_value=1)
        cn.do_increment_clock = clock_incrementation

        # Go into method to test
        cn.set_controller_to_run()

        # Check if the serial send and wait for reply's value matches the expected value
        # self.assertEqual(expected_value, expected_value)

    #################################
    def test_set_controller_to_run_fail1(self):
        """ Sets the controller to run fail case 1: Controller type is 1000, method raises an exception """
        cn = self.create_test_controller_object()

        # Store the expected serial send and wait for reply value
        expected_value = 'KEY,DL=5'

        # Store expected error message
        e_msg = "Setting Controller to Run Position Command Failed: "

        # Set controller type to 1000
        cn.ty = "10"

        # Increment the clock
        clock_incrementation = mock.MagicMock(return_value=1)
        cn.do_increment_clock = clock_incrementation

        # Mock a side effect throwing an exception
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Go into method to test
        with self.assertRaises(Exception) as context:
            cn.set_controller_to_run()

        # Compare expected error message against actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_controller_to_run_fail2(self):
        """ Sets the controller to run fail case 2: Controller type is default, method raises an exception """
        cn = self.create_test_controller_object()

        # Store the expected serial send and wait for reply value
        expected_value = 'KEY,DL=15'

        # Store expected error message
        e_msg = "Setting Controller to Run Position Command Failed: "

        # Increment the clock
        clock_incrementation = mock.MagicMock(return_value=1)
        cn.do_increment_clock = clock_incrementation

        # Mock a side effect throwing an exception
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Go into method to test
        with self.assertRaises(Exception) as context:
            cn.set_controller_to_run()

        # Compare expected error message against actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    # def test_set_ai_for_cn_pass1(self):
    #     """ Set AI For Controller Pass Case 1: Sends correct commands to controller to disconnect from and ping
    #      Base manager. """
    #     controller = self.create_test_controller_object()
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,SS=CN")
    #
    #     self.mock_get_and_wait_for_reply = mock.MagicMock(return_value=mock_data)
    #     self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply
    #
    #     disc_bm = 'DO,BM=DC'
    #     set_ip_msg = 'SET,BM,AI=129.0.0.0,UA=TR'
    #     ping_bm = 'DO,BM=PN'
    #
    #     controller.set_ai_for_cn(ip_address="129.0.0.0", unit_testing=True)
    #     send_error = ""
    #
    #     try:
    #         self.mock_send_and_wait_for_reply.assert_called_with(disc_bm)
    #     except AssertionError as ae:
    #         print(ae.message)
    #         send_error = disc_bm
    #         try:
    #             self.mock_send_and_wait_for_reply.assert_called_with(set_ip_msg)
    #         except AssertionError as ae2:
    #             print(ae2.message)
    #             send_error = set_ip_msg
    #         else:
    #             try:
    #                 self.mock_send_and_wait_for_reply.assert_called_with(ping_bm)
    #             except AssertionError as ae3:
    #                 print(ae3.message)
    #                 send_error = ping_bm
    #             else:
    #                 test_pass = True
    #     else:
    #         try:
    #             self.mock_send_and_wait_for_reply.assert_called_with(set_ip_msg)
    #         except AssertionError as ae2:
    #             print(ae2.message)
    #             send_error = set_ip_msg
    #         else:
    #             try:
    #                 self.mock_send_and_wait_for_reply.assert_called_with(ping_bm)
    #             except AssertionError as ae3:
    #                 print(ae3.message)
    #                 send_error = ping_bm
    #             else:
    #                 test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised. Error in sending {0} to "
    #                                       "controller, not in correct format".format(send_error))

    #################################
    def test_set_controller_to_off_pass1(self):
        """
        set_controller_to_off pass case 1:
        Create: Test controller object
        Set: Controller type to 1000
        Mock: Serial send and wait for reply
        Return: None, so it does not fail
        Mock: Do increment clock
        Return: None, so it does not fail
        Test return: Nothing, if it runs through the code and does not fail, then it passes
        Run: Test
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Set the controller type to the 1000
        cn.ty = "10"

        # Mock serial send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = None

        # Mock do increment clock
        mock_increment_clock = mock.MagicMock(side_effect=None)
        cn.do_increment_clock = mock_increment_clock

        # Run method
        cn.set_controller_to_off()

    #################################
    def test_set_controller_to_off_pass2(self):
        """
        set_controller_to_off pass case 2:
        Create: Test controller object
        Mock: Serial send and wait for reply
        Return: None, so it does not fail
        Mock: Do increment clock
        Return: None, so it does not fail
        Test return: Nothing, if it runs through the code and does not fail, then it passes
        Run: Test
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Mock serial send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = None

        # Mock do increment clock
        mock_increment_clock = mock.MagicMock(side_effect=None)
        cn.do_increment_clock = mock_increment_clock

        # Run method
        cn.set_controller_to_off()

    #################################
    def test_set_controller_to_off_fail1(self):
        """
        set_controller_to_off pass case 1:
        Create: Test controller object
        Store: Expected error message
        Set: Controller type to 1000
        Mock: Serial send and wait for reply
        Return: Exception, so it fails
        Run: Test, with an exception expected
        Compare: Expected error message to actual error message
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error message
        e_msg = "Setting Controller to Off Position Command Failed: "

        # Set the controller type to the 1000
        cn.ty = "10"

        # Mock serial send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Run test with exception expected
        with self.assertRaises(Exception) as context:
            cn.set_controller_to_off()

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_controller_to_off_fail2(self):
        """
        set_controller_to_off pass case 2:
        Create: Test controller object
        Store: Expected error message
        Mock: Serial send and wait for reply
        Return: Exception, so it fails
        Run: Test, with an exception expected
        Compare: Expected error message to actual error message
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error message
        e_msg = "Setting Controller to Off Position Command Failed: "

        # Mock serial send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Run test with exception expected
        with self.assertRaises(Exception) as context:
            cn.set_controller_to_off()

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_eto_and_date_stamp_on_cn_pass1(self):
        """
        set_eto_and_date_stamp_on_cn pass case 1
        Create: Controller object
        Store: expected et_value, rain_value, and controller_date in variables
        Mock: Verify_value_is_string method
        Return: True, so the test skips over the error
        Run: Test, with the controller values passed into the test
        Compare: The et, ed, and ra values to their expected values
        """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store expected values
        et_value = 3.00
        rain_value = 3.22
        controller_date = '20150321'

        # Mock mock verify value is string
        method_holder = helper_methods.verify_value_is_string
        mock_verify_value_is_string = mock.MagicMock(return_value=True)
        helper_methods.verify_value_is_string = mock_verify_value_is_string

        # Run test
        cn.set_eto_and_date_stamp_on_cn(_controller_et_value=et_value, _controller_rain_value=3.22,
                                        _controller_date=controller_date)

        helper_methods.verify_value_is_string = method_holder

        # Compare ET ED and RA values to their expected values
        self.assertEqual(Devices.et, et_value)
        self.assertEqual(cn.ed, controller_date)
        self.assertEqual(Devices.ra, rain_value)

    #################################
    def test_set_eto_and_date_stamp_on_cn_fail1(self):
        """ Set an eto and rain value on the controller fail case 1: Exception occurred while trying to set the ET
        value"""
        # Create the controller object
        cn = self.create_test_controller_object()

        # Store bad expected ET value
        expected_et = 6324

        # Store expected error
        e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was and Invalid " \
                "argument type, expected a float, received: {1}" \
                .format(expected_et, type(expected_et))

        # Set controller et value to something different than a float
        # Run through the method
        with self.assertRaises(TypeError) as context:
            cn.set_eto_and_date_stamp_on_cn(_controller_et_value=expected_et, _controller_rain_value=3.22,
                                            _controller_date='20150321')

        # compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_eto_and_date_stamp_on_cn_fail2(self):
        """ Set an eto and rain value on the controller fail case 2: Exception occurs while setting the eto value under
         the expected limit """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Store the expected eto value that's below the expected range in a variable
        under_et = -3.00

        # Store the expected message
        e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was not between" \
                "(0.00 and 5.00)" \
                .format(under_et)

        # Set the values for the runtime, and catch the value error
        with self.assertRaises(ValueError) as context:
            cn.set_eto_and_date_stamp_on_cn(_controller_et_value=under_et, _controller_date='20150321',
                                            _controller_rain_value=3.00)

        # Compare the actual error message with the expected error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_eto_and_date_stamp_on_cn_fail3(self):
        """ Set an eto and rain value on the controller fail case 3: Value is set above expected for the eto value """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Store the bad eto value that's above the expected eto value range
        over_et = 30.00

        # Store the expected error message
        e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was not between" \
                "(0.00 and 5.00)" \
                .format(over_et)

        # Run the test, setting the correct values within the test
        with self.assertRaises(ValueError) as context:
            cn.set_eto_and_date_stamp_on_cn(_controller_et_value=over_et, _controller_rain_value=3.00,
                                            _controller_date='20150423')

        # Compare the expected error message to the actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_eto_and_date_stamp_on_cn_fail4(self):
        """ Set an eto and rain value on the controller fail case 4: Exception occurred while setting the rain value
        under the expected limit """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store bad rain value, something outside the range of 0-5
        under_rain_value = -4.00

        # Store expected message
        e_msg = "Exception occurred trying to set the controller Rain value. Value received {0} was not between" \
                "(0.00 and 5.00)" \
                .format(under_rain_value)

        # Set the rest of the values for the run, and catch value error
        with self.assertRaises(ValueError) as context:
            cn.set_eto_and_date_stamp_on_cn(_controller_et_value=4.00, _controller_rain_value=under_rain_value,
                                            _controller_date='20150321')

    #################################
    def test_set_eto_and_date_stamp_on_cn_fail5(self):
        """ Set an eto and rain value on the controller fail case 5: Exception occurred while setting the rain value
        over the expected limit """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store bad rain value, something outside the range of 0-5
        over_rain_value = 75.00

        # Store expected message
        e_msg = "Exception occurred trying to set the controller Rain value. Value received {0} was not between" \
                "(0.00 and 5.00)"\
                .format(over_rain_value)

        # Set the rest of the values for the run, and catch value error
        with self.assertRaises(ValueError) as context:
            cn.set_eto_and_date_stamp_on_cn(_controller_et_value=4.00, _controller_rain_value=over_rain_value,
                                            _controller_date='20150321')

        # Compare error message on actual with expected error message
        self.assertEqual(e_msg, context.exception.message)

    ################################
    def test_set_eto_and_date_stamp_on_cn_fail6(self):
        """ Set eto and date stamp values on the controller fail case 6: Date was not a string """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store bad date value
        bad_date = 363153

        # store expected error message
        e_msg = "Exception occurred trying to set the controller ET Date. Value received {0} was and Invalid " \
                "argument type, expected a str YYYYMMDD" \
                .format(bad_date, type(cn.ed))

        # Raise a type error because the ide won't let you set the date as anything besides a string
        self.mock_send_and_wait_for_reply.side_effect = TypeError

        # Set the values for the rest of the run, and catch the value error
        with self.assertRaises(TypeError) as context:
            cn.set_eto_and_date_stamp_on_cn(_controller_et_value=4.00, _controller_date=bad_date,
                                            _controller_rain_value=3.00)

        # Compare the actual and expected errors
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_eto_and_date_stamp_on_cn_fail7(self):
        """ Set the date and eto values on the controller fail case 7: Controller cannot set the et and date stamp
         values onto itself, problem in sending """
        # Set up the controller object
        cn = self.create_test_controller_object()

        # Store expected values
        et_value = 3.00
        rain_value = 3.22
        controller_date = '20150321'

        # Store the expected exception
        e_msg = "Exception occurred trying to set the controller ET and date stamp ET and rain fall values. " \
                "ET = '{0}'Date Stamp = '{1}'and Rain Fall = '{2}'".format(et_value, controller_date, rain_value)

        # Create an exception through side effect
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Run through the program with the correct values, prepare to catch an exception
        with self.assertRaises(Exception) as context:
            cn.set_eto_and_date_stamp_on_cn(_controller_et_value=et_value, _controller_rain_value=rain_value,
                                            _controller_date=controller_date)

        # Compare the expected exception with the actual exception
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_eto_and_date_stamp_on_cn_fail8(self):
        """
        set_eto_and_date_stamp_on_cn fail case 8
        Create: Test controller object
        Store: Expected values for controller date and expected error message
        Mock: Verify value is a string
        Return: Exception so it will fail
        Run: Test, and prepare to catch an exception
        Compare: Actual exception message with expected exception message
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Controller date (verify_value_is_string method is mocked, so it does not necessarily need to be a bad value)
        controller_date = '20150321'

        # Mock mock verify value is string
        method_holder = helper_methods.verify_value_is_string
        mock_verify_value_is_string = mock.MagicMock(return_value=False)
        helper_methods.verify_value_is_string = mock_verify_value_is_string

        # Expected error message
        e_msg = "Exception occurred trying to set the controller ET Date. Value received {0} was and invalid " \
                "length, expected a {1} characters string (YYYYMMDD)" \
            .format(controller_date, 8)

        # Run test, prepare to catch an exception
        with self.assertRaises(TypeError) as context:
            cn.set_eto_and_date_stamp_on_cn(_controller_et_value=3.21, _controller_rain_value=1.23,
                                            _controller_date=controller_date)

        helper_methods.verify_value_is_string = method_holder

        # Compare actual exception message with real expected exception message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_initial_eto_value_cn_pass1(self):
        """ Sets an initial eto value on the controller pass case 1: Able to set the initial eto value on the controller
        and all of its zones """
        # Creates the controller object
        cn = self.create_test_controller_object()

        # Expected et value
        expected_value = 3.00

        # Set the initial et value somewhere in between 0-5 and run the test
        cn.set_initial_eto_value_cn(_initial_et_value=expected_value)

        # Compare actual et value to the expected et value
        self.assertEqual(expected_value, cn.ei)

    #################################
    def test_set_initial_eto_value_cn_fail1(self):
        """ Sets an initial eto value on the controller fail case 1: Value received is not a float """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Sets the initial et value (make sure this is not a float)
        incorrect_et_value = 3

        # Set expected error
        e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was and Invalid " \
                "argument type, expected a float, received: {1}" \
            .format(incorrect_et_value, type(incorrect_et_value))

        # Runs with the et value set to something besides a float, typically something besides an integer
        with self.assertRaises(TypeError) as context:
            cn.set_initial_eto_value_cn(_initial_et_value=incorrect_et_value)

        # Compare expected error with actual error
        self.assertEqual(e_msg, context.exception.message)

        #################################

    def test_set_initial_eto_value_cn_fail2(self):
        """ Sets the initial eto value on the controller fail case 2: Value was below 0, needs to be between 0 and 5 """
        # Create controller object
        cn = self.create_test_controller_object()

        # Set et value below 0, but keep it as a float
        expected_value = -2.00

        # Set expected error
        e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was not between" \
                "(0.00 and 5.00)" \
            .format(expected_value)

        # Run the test with the bad et value
        with self.assertRaises(ValueError) as context:
            cn.set_initial_eto_value_cn(_initial_et_value=expected_value)

        # Compare the expected error with the actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_initial_eto_value_cn_fail3(self):
        """ Sets the initial eto value on the controller fail case 2: Value was above 5, needs to be between 0 and 5 """
        # Create controller object
        cn = self.create_test_controller_object()

        # Set et value below 0, but keep it as a float
        expected_value = 7.00

        # Set expected error
        e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was not between" \
                "(0.00 and 5.00)" \
            .format(expected_value)

        # Run the test with the bad et value
        with self.assertRaises(ValueError) as context:
            cn.set_initial_eto_value_cn(_initial_et_value=expected_value)

        # Compare the expected error with the actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_initial_eto_value_cn_fail4(self):
        """ Sets initial eto value on the controller: Error sending values to the controller """
        # Creates controller object
        cn = self.create_test_controller_object()

        # Sets a correct et value
        et_value = 3.00

        # Sets expected error
        e_msg = "Exception occurred trying to set the controller ETo = '{0}' " \
                "and Date Stamp = '{1}'".format(et_value, cn.ed)

        # Creates an exception using a side effect
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Sets the expected et value and runs the test
        with self.assertRaises(Exception) as context:
            cn.set_initial_eto_value_cn(_initial_et_value=et_value)

        # Compares the error of the test with the expected error
        self.assertEqual(e_msg, context.exception.message)

    ################################
    def test_set_learn_flow_enabled_pass1(self):
        """ Turns on the learn flow for a particular program or zone pass case 1: Is able to turn on learn flow """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store values for program address, zone address, and time delay as integers
        program_address = 3
        zn_address = 6
        time_delay = 11

        # Stores message from print statement
        print_statement = "Successfully set the learn flow on program '{0}' and zone '{1}'.".format(program_address,
                                                                                                    zn_address)

        # Set _pg_ad to an integer
        # Set _zn_ad to an integer
        # Set _time_delay to an integer
        # Run program with above parameters set
        cn.set_learn_flow_enabled(_pg_ad=program_address, _zn_ad=zn_address, _time_delay=time_delay)

    #     # Compare program address and zone address to expected values
    #     self.assertEqual(cn.set_learn_flow_enabled(_pg_ad), program_address)

    #################################
    def test_set_learn_flow_enabled_fail1(self):
        """ Turns on the learn flow for a particular program or zone pass case 1: Fails when setting the program
         address """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store program address as something (that's not an int)
        program_address = 3.14

        # Store the expected error message
        e_msg = "Exception occurred trying to enable learn flow. Argument passed in was an Invalid " \
                "argument type, expected an int, received: {0}." \
                .format(type(program_address))

        # Set pg_ad to something besides an integer
        # Run the program, and prepare to catch a type error
        with self.assertRaises(TypeError) as context:
            cn.set_learn_flow_enabled(_pg_ad=program_address)

        # Verify the expected error message versus the actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_learn_flow_enabled_fail2(self):
        """ Turns on the learn flow for a particular program or zone pass case 2: Fails when setting the zone
            address """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store bad zone address
        zone_address = 32.14

        # Store expected error message
        e_msg = "Exception occurred trying to enable learn flow. Argument passed in was an Invalid " \
                "argument type, expected an int, received: {0}" \
                .format(type(zone_address))

        # Run program with program address and zone address inserted, prepare for a type error to be raised
        with self.assertRaises(TypeError) as context:
            cn.set_learn_flow_enabled(_pg_ad=6, _zn_ad=zone_address, _time_delay=3)

        # Compare expected error and actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_learn_flow_enabled_fail3(self):
        """ Turns on the learn flow for a particular program or zone pass case 3: Fails when setting the time
            delay """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store time delay
        time_delay = 31.42

        # Store the expected error message
        e_msg = "Exception occurred trying to enable learn flow. Time delay passed in was an Invalid " \
                "argument type, expected an int, received: {0}" \
                .format(type(time_delay))

        # Run the method while setting all values (including time delay as an incorrect value), expect a type error
        with self.assertRaises(TypeError) as context:
            cn.set_learn_flow_enabled(_pg_ad=3, _zn_ad=6, _time_delay=time_delay)

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_learn_flow_enabled_fail4(self):
        """ Turns on the learn flow for a particular program or zone pass case 4: Exception occurs when sending the
        message to the controller """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store program address and zone address
        zone_address = 3
        program_address = 5

        # Store expected error message
        e_msg = "Exception occurred trying to set learn flow on program '{0}' and zone '{1}'.".format(
                program_address,
                zone_address)

        # Raise an exception as a side effect
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Run program with all values correctly inserted, prepare for an exception
        with self.assertRaises(Exception) as context:
            cn.set_learn_flow_enabled(_pg_ad=program_address, _zn_ad=zone_address, _time_delay=5)

        # Compare expected error message to the actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_program_start_stop_pass1(self):
        """ Set a program to either start or stop pass case 1: Able to run through the method without hitting an
         exception """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Run the program
        cn.set_program_start_stop(_pg_ad=3, _function='SR')

    #################################
    def test_set_program_start_stop_fail1(self):
        """ Sets a program to either start or stop fail case 1: program address is not an integer """
        # Create controller object
        cn = self.create_test_controller_object()

        # Set the program address to something other than an integer
        program_address = 3.21

        # Store the expected error message as an integer
        e_msg = "Exception occurred trying to set program's start/stop. Argument passed in was an Invalid " \
                "argument type, expected an int, received: {0}." \
                .format(type(program_address))

        # Set the necessary values and run the program, prepare for a type error
        with self.assertRaises(TypeError) as context:
            cn.set_program_start_stop(_pg_ad=program_address, _function='SP')

        # Compare the expected error message to the actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_program_start_stop_fail2(self):
        """ Sets a program to either start or stop fail case 2: function is not a correct string """
        # Create controller object
        cn = self.create_test_controller_object()

        # Set function to an incorrect string
        func = "OK"

        # Store expected error message
        e_msg = "Exception occurred trying to set the program's start/stop condition. Value received {0} was and " \
                "Invalid argument type, expected either string 'SR' or 'SP', got {0}" \
                .format(func)

        # Set function to something that's not a search for devices or a string, prepare to catch a type error
        with self.assertRaises(TypeError) as context:
            cn.set_program_start_stop(_pg_ad=3, _function=func)

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_program_start_stop_fail3(self):
        """ Sets a program to either start or stop fail case 3: Incorrect command sent to serial """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store program address and function
        program_address = 3
        func = "SP"

        # Store expected error message
        e_msg = "Exception occurred trying to start/stop program '{0}' with opcode '{1}'".format(program_address,
                                                                                                 func)

        # Raise exception by using a side effect
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Run method with the correct program address and function, catch an exception
        with self.assertRaises(Exception) as context:
            cn.set_program_start_stop(_pg_ad=program_address, _function=func)

        # Compare the expected error and actual error messages
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_address_for_objects_pass1(self):
        """
        set_address_for_objects pass case 1:
        Create: Test controller object
        Mock: flow meter object
        Return: None, so it does not fail
        Create: An expected call list
        Run: Test, with object dictionary and address range filled
        Check: That the call was made
        """
        # Create controller object
        cn = self.create_test_controller_object()

        # Mock a new flow meter object
        mock_fm_obj = mock.MagicMock(side_effect=None)

        # Create an expected call list
        expected_call_list = [mock.call.set_address_on_cn()]

        # Set the object dictionary and address range and run the method
        cn.set_address_for_objects(object_dict={1: mock_fm_obj}, ad_range=[1])

        # Checks which calls should be made, and in what order they should be made
        mock_fm_obj.assert_has_calls(expected_call_list)

    #################################
    def test_set_address_for_zones_pass1(self):
        """
        set_address_for_zones pass case 1:
        Create: Test controller object
        Mock: flow meter object
        Return: None, so it does not fail
        Create: An expected call list
        Run: Test, with object dictionary and address range filled
        Check: That the call was made
        """
        # Create controller object
        cn = self.create_test_controller_object()

        # Mock a new flow meter object
        mock_fm_obj = mock.MagicMock(side_effect=None)

        # Create an expected call list
        expected_call_list = [mock.call.set_address_on_cn(), mock.call.set_default_values()]

        # Set the object dictionary and address range and run the method
        cn.set_address_and_default_values_for_zn(zn_object_dict={1: mock_fm_obj}, zn_ad_range=[1])

        # Checks which calls should be made, and in what order they should be made
        mock_fm_obj.assert_has_calls(expected_call_list)

    #################################
    def test_set_address_and_default_values_for_ms_pass1(self):
        """
        set_address_and_default_values_for_ms pass case 1:
        Create: Test controller object
        Set: Controller type to 1000
        Mock: flow meter object
        Return: None, so it does not fail
        Create: An expected call list
        Run: Test, with object dictionary and address range filled
        Check: That the call was made
        """
        # Create controller object
        cn = self.create_test_controller_object()

        # Set controller type to 1000
        cn.ty = '10'

        # Mock a new flow meter object
        mock_fm_obj = mock.MagicMock(side_effect=None)

        # Create an expected call list
        expected_call_list = [mock.call.set_address_on_cn(), mock.call.set_default_values()]

        # Set the object dictionary and address range and run the method
        cn.set_address_and_default_values_for_ms(ms_object_dict={1: mock_fm_obj}, ms_ad_range=[1])

        # Checks which calls should be made, and in what order they should be made
        mock_fm_obj.assert_has_calls(expected_call_list)

    #################################
    def test_set_address_and_default_values_for_ms_pass2(self):
        """
        set_address_and_default_values_for_ms pass case 2:
        Create: Test controller object
        Mock: flow meter object
        Return: None, so it does not fail
        Create: An expected call list
        Run: Test, with object dictionary and address range filled
        Check: That the call was made
        """
        # Create controller object
        cn = self.create_test_controller_object()

        # Mock a new flow meter object
        mock_fm_obj = mock.MagicMock(side_effect=None)

        # Create an expected call list
        expected_call_list = [mock.call.set_default_values()]

        # Set the object dictionary and address range and run the method
        cn.set_address_and_default_values_for_ms(ms_object_dict={1: mock_fm_obj}, ms_ad_range=[1])

        # Checks which calls should be made, and in what order they should be made
        mock_fm_obj.assert_has_calls(expected_call_list)

    #################################
    def test_set_address_and_default_values_for_fm_pass1(self):
        """
        set_address_and_default_values_for_fm pass case 1
        Create: Test controller object
        Set: Controller type to the 1000
        Mock: set_address_on_cn
        Return: None, so it does not fail
        Mock: set_default_values
        Return: None, so it does not fail
        Mock: a new flow meter object
        Return: None, so the test does not fail
        Create: A new expected call list, detailing which methods are called, and in which order they are called
        Run: test
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Set the controller type to 1000
        cn.ty = "10"

        # Mock set address on controller
        mock_set_address_on_controller = mock.MagicMock(side_effect=None)
        cn.set_address_on_cn = mock_set_address_on_controller

        # Mock set default values
        mock_set_default_values = mock.MagicMock(side_effect=None)
        cn.set_default_values = mock_set_default_values

        # Create a mock flow meter object
        mock_fm_obj = mock.MagicMock(side_effect=None)

        # Create an expected call list
        expected_call_list = [mock.call.set_address_on_cn(), mock.call.set_default_values()]

        # Run test
        cn.set_address_and_default_values_for_fm(fm_object_dict={1: mock_fm_obj}, fm_ad_range=[1])

        # Checks which calls should be made, and in what order they should be made
        mock_fm_obj.assert_has_calls(expected_call_list)

    #################################
    def test_set_address_and_default_values_for_fm_pass2(self):
        """
        set_address_and_default_values_for_fm pass case 2
        Create: Test controller object
        Mock: set_address_on_cn
        Return: None, so it does not fail
        Mock: set_default_values
        Return: None, so it does not fail
        Mock: a new flow meter object
        Return: None, so the test does not fail
        Create: A new expected call list, detailing which methods are called, and in which order they are called
        Run: test
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Mock set address on controller
        mock_set_address_on_controller = mock.MagicMock(side_effect=None)
        cn.set_address_on_cn = mock_set_address_on_controller

        # Mock set default values
        mock_set_default_values = mock.MagicMock(side_effect=None)
        cn.set_default_values = mock_set_default_values

        # Create a mock flow meter object
        mock_fm_obj = mock.MagicMock(side_effect=None)

        # Create an expected call list
        expected_call_list = [mock.call.set_default_values()]

        # Run test
        cn.set_address_and_default_values_for_fm(fm_object_dict={1: mock_fm_obj}, fm_ad_range=[1])

        # Checks which calls should be made, and in what order they should be made
        mock_fm_obj.assert_has_calls(expected_call_list)

    #################################
    def test_set_address_and_default_values_for_ts_pass1(self):
        """
        set_address_and_default_values_for_ts pass case 1
        Create: Test controller object
        Set: Controller type to the 1000
        Mock: set_address_on_cn
        Return: None, so it does not fail
        Mock: set_default_values
        Return: None, so it does not fail
        Mock: a new flow meter object
        Return: None, so the test does not fail
        Create: A new expected call list, detailing which methods are called, and in which order they are called
        Run: test
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Set controller type to 1000
        cn.ty = "10"

        # Mock set address on controller
        mock_set_address_on_controller = mock.MagicMock(side_effect=None)
        cn.set_address_on_cn = mock_set_address_on_controller

        # Mock set default values
        mock_set_default_values = mock.MagicMock(side_effect=None)
        cn.set_default_values = mock_set_default_values

        # Create a mock flow meter object
        mock_fm_obj = mock.MagicMock(side_effect=None)

        # Create an expected call list
        expected_call_list = [mock.call.set_address_on_cn(), mock.call.set_default_values()]

        # Run test
        cn.set_address_and_default_values_for_ts(ts_object_dict={1: mock_fm_obj}, ts_ad_range=[1])

        # Checks which calls should be made, and in what order they should be made
        mock_fm_obj.assert_has_calls(expected_call_list)

    #################################
    def test_set_address_and_default_values_for_ts_pass2(self):
        """
        set_address_and_default_values_for_ts pass case 2
        Create: Test controller object
        Mock: set_address_on_cn
        Return: None, so it does not fail
        Mock: set_default_values
        Return: None, so it does not fail
        Mock: a new flow meter object
        Return: None, so the test does not fail
        Create: A new expected call list, detailing which methods are called, and in which order they are called
        Run: test
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Mock set address on controller
        mock_set_address_on_controller = mock.MagicMock(side_effect=None)
        cn.set_address_on_cn = mock_set_address_on_controller

        # Mock set default values
        mock_set_default_values = mock.MagicMock(side_effect=None)
        cn.set_default_values = mock_set_default_values

        # Create a mock flow meter object
        mock_fm_obj = mock.MagicMock(side_effect=None)

        # Create an expected call list
        expected_call_list = [mock.call.set_default_values()]

        # Run test
        cn.set_address_and_default_values_for_ts(ts_object_dict={1: mock_fm_obj}, ts_ad_range=[1])

        # Checks which calls should be made, and in what order they should be made
        mock_fm_obj.assert_has_calls(expected_call_list)

    #################################
    def test_set_address_and_default_values_for_sw_pass1(self):
        """
        set_address_and_default_values_for_sw pass case 1
        Create: Test controller object
        Set: Controller type to the 1000
        Mock: set_address_on_cn
        Return: None, so it does not fail
        Mock: set_default_values
        Return: None, so it does not fail
        Mock: a new flow meter object
        Return: None, so the test does not fail
        Create: A new expected call list, detailing which methods are called, and in which order they are called
        Run: test
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Set controller type to 1000
        cn.ty = '10'

        # Mock set address on controller
        mock_set_address_on_controller = mock.MagicMock(side_effect=None)
        cn.set_address_on_cn = mock_set_address_on_controller

        # Mock set default values
        mock_set_default_values = mock.MagicMock(side_effect=None)
        cn.set_default_values = mock_set_default_values

        # Create a mock flow meter object
        mock_fm_obj = mock.MagicMock(side_effect=None)

        # Create an expected call list
        expected_call_list = [mock.call.set_address_on_cn(), mock.call.set_default_values()]

        # Run test
        cn.set_address_and_default_values_for_sw(sw_object_dict={1: mock_fm_obj}, sw_ad_range=[1])

        # Checks which calls should be made, and in what order they should be made
        mock_fm_obj.assert_has_calls(expected_call_list)

    #################################
    def test_set_address_and_default_values_for_sw_pass2(self):
        """
        set_address_and_default_values_for_sw pass case 2
        Create: Test controller object
        Mock: set_address_on_cn
        Return: None, so it does not fail
        Mock: set_default_values
        Return: None, so it does not fail
        Mock: a new flow meter object
        Return: None, so the test does not fail
        Create: A new expected call list, detailing which methods are called, and in which order they are called
        Run: test
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Mock set address on controller
        mock_set_address_on_controller = mock.MagicMock(side_effect=None)
        cn.set_address_on_cn = mock_set_address_on_controller

        # Mock set default values
        mock_set_default_values = mock.MagicMock(side_effect=None)
        cn.set_default_values = mock_set_default_values

        # Create a mock flow meter object
        mock_fm_obj = mock.MagicMock(side_effect=None)

        # Create an expected call list
        expected_call_list = [mock.call.set_default_values()]

        # Run test
        cn.set_address_and_default_values_for_sw(sw_object_dict={1: mock_fm_obj}, sw_ad_range=[1])

        # Checks which calls should be made, and in what order they should be made
        mock_fm_obj.assert_has_calls(expected_call_list)

    #################################
    def test_set_serial_port_timeout_pass1(self):
        """ Resets the length of time before the serial port times out pass case 1: Pass in a new value for timeout """
        # Create controller object
        cn = self.create_test_controller_object()

        # Mock the serial connection
        serial_conn = mock.MagicMock()
        cn.ser.serial_conn = serial_conn
        settimeout = mock.MagicMock()
        cn.ser.serial_conn.setTimeout = settimeout

        # Pass in timeout value and run method
        cn.set_serial_port_timeout(timeout=300)

    #################################
    def test_get_date_and_time_pass1(self):
        """ Gets the date and the time pass case 1: Successfully sends a GET,DT string """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Mock the get and wait for reply
        cn_date_time = mock.MagicMock()
        cn.ser.get_and_wait_for_reply = cn_date_time

        # Run the method
        cn.get_date_and_time()

    #################################
    def test_get_date_and_time_fail1(self):
        """ Gets the date and the time fail case 1: Fails to send a GET, DT string """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Store expected message
        e_msg = "Getting controller date time command failed"

        # Run the method without mocking the get prepare for an exception to be thrown
        with self.assertRaises(Exception) as context:
            cn.get_date_and_time()

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_date_and_time_pass1(self):
        """ Verifies the date and the time pass case 1: Able to successfully verify the date and time """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Assign controller date and time
        date_mngr.controller_datetime.obj = datetime.datetime.strptime('12/30/2011 10:12:32',
                                                                       "%m/%d/%Y %H:%M:%S").date()
        date_mngr.controller_datetime.time_obj.obj = datetime.datetime.strptime('12/30/2011 10:12:32',
                                                                                "%m/%d/%Y %H:%M:%S").time()

        # Mock get date and time
        get_date_and_time = mock.MagicMock()
        get_date_and_time.get_value_string_by_key = mock.MagicMock(return_value='12/30/2011 10:12:32')
        cn.get_date_and_time = mock.MagicMock(return_value=get_date_and_time)

        # Run method
        cn.verify_date_and_time()

    #################################
    def test_verify_date_and_time_pass2(self):
        """ Verifies the date and the time pass case 2: Controller times are within 60 seconds of each other """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Assign controller date and time, only have them a minute apart maximum
        date_mngr.controller_datetime.obj = datetime.datetime.strptime('12/30/2011 10:12:32',
                                                                       "%m/%d/%Y %H:%M:%S").date()
        date_mngr.controller_datetime.time_obj.obj = datetime.datetime.strptime('12/30/2011 10:12:35',
                                                                                "%m/%d/%Y %H:%M:%S").time()

        # Mock get date and time
        get_date_and_time = mock.MagicMock()
        get_date_and_time.get_value_string_by_key = mock.MagicMock(return_value='12/30/2011 10:12:32')
        cn.get_date_and_time = mock.MagicMock(return_value=get_date_and_time)

        # Run method
        cn.verify_date_and_time()


    #################################
    def test_verify_date_and_time_fail1(self):
        """ Verifies the date and the time fail case 1: Controller times are outside 60 seconds of each other """
        # Create the controller object
        cn = self.create_test_controller_object()

        expected_datetime = datetime.datetime.strptime('12/30/2011 10:12:32', "%m/%d/%Y %H:%M:%S")
        actual_datetime = datetime.datetime.strptime('12/30/2011 10:42:32', "%m/%d/%Y %H:%M:%S")

        # Store the date and time objects
        date_mngr.controller_datetime.obj = datetime.datetime.strptime('12/30/2011 10:12:32',
                                                                       "%m/%d/%Y %H:%M:%S").date()
        date_mngr.controller_datetime.time_obj.obj = datetime.datetime.strptime('12/30/2011 10:42:32',
                                                                                "%m/%d/%Y %H:%M:%S").time()

        # Store the expected error in a variable
        e_msg = "The date and time of the controller didn't match the controller object:\n" \
                "\tController Object Date Time:       \t\t'{0}'\n" \
                "\tDate Time Received From Controller:\t\t'{1}'\n".format(
                actual_datetime,  # {0} The date of the controller object
                expected_datetime  # {1} The date the controller has
        )

        # Mock get date and time
        get_date_and_time = mock.MagicMock()
        get_date_and_time.get_value_string_by_key = mock.MagicMock(return_value='12/30/2011 10:12:32')
        cn.get_date_and_time = mock.MagicMock(return_value=get_date_and_time)

        # Run method, and prepare for a value error
        with self.assertRaises(ValueError) as context:
            cn.verify_date_and_time()

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_max_concurrent_zones_on_cn_pass1(self):
        """ Verify Max Concurrent Zones On Controller Pass Case 1: Exception is not raised """
        controller = self.create_test_controller_object()
        controller.mc = 5.0
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,MC=5")
        controller.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                controller.verify_max_concurrent_zones_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_max_concurrent_zones_on_cn_fail1(self):
        """ Verify MAx Concurrent Zones On Controller Fail Case 1: Value on controller does not match what is
        stored in controller.mc """
        controller = self.create_test_controller_object()
        controller.mc = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,MC=6")
        controller.data = mock_data

        expected_message = "Unable to verify Controller's 'Max Concurrent Zones'. Received: 6, Expected: 5"
        try:
            controller.verify_max_concurrent_zones_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_code_version_on_cn_pass1(self):
        """ Verify Code Version On Controller Pass Case 1: Exception is not raised """
        controller = self.create_test_controller_object()
        controller.vr = "2.1"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,VR=2.1")
        controller.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                controller.verify_code_version_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_code_version_on_cn_fail1(self):
        """ Verify Code Version On Controller Fail Case 1: Value on controller does not match what is
        passed in as argument """
        controller = self.create_test_controller_object()
        controller.vr = "2.1"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,VR=2.2")
        controller.data = mock_data

        expected_message = "Unable to verify Controller's 'Code Version'. Received: 2.2, Expected: 2.1"
        try:
            controller.verify_code_version_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_rain_jumper_state_on_cn_pass1(self):
        """ Verify Rain Jumper State On Controller Pass Case 1: Exception is not raised """
        controller = self.create_test_controller_object()
        controller.jr = "CL"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,JR=CL")
        controller.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                controller.verify_rain_jumper_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_rain_jumper_state_on_cn_fail1(self):
        """ Verify Rain Jumper State On Controller Fail Case 1: Value on controller does not match what is
        saved in controller.jr """
        controller = self.create_test_controller_object()
        controller.jr = "CL"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,JR=OP")
        controller.data = mock_data

        expected_message = "Unable to verify Controller's 'Rain Jumper State'. Received: OP, Expected: CL"
        try:
            controller.verify_rain_jumper_state_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_flow_jumper_state_on_cn_pass1(self):
        """ Verify Flow Jumper State On Controller Pass Case 1: Exception is not raised """
        controller = self.create_test_controller_object()
        controller.jf = "CL"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,JF=CL")
        controller.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                controller.verify_flow_jumper_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_flow_jumper_state_on_cn_fail1(self):
        """ Verify Flow Jumper State On Controller Fail Case 1: Value on controller does not match what is
        saved in controller.jr """
        controller = self.create_test_controller_object()
        controller.jf = "CL"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,JF=OP")
        controller.data = mock_data

        expected_message = "Unable to verify Controller's 'Flow Jumper State'. Received: OP, Expected: CL"
        try:
            controller.verify_flow_jumper_state_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_flow_jumper_state_on_cn_fail2(self):
        """
        verify_flow_jumper_state_on_cn fail case 2
        Create: The test controller object
        Set: The controller type to the 1000
        Store: Expected error message
        Run: Test, and expect a ValueError
        Compare: Expected error message to actual error message
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Set the controller type to the 1000
        cn.ty = "10"

        # Store the expected error message
        e_msg = "Attempting to verify a 'Pause Jumper' state on a 1000 which is not supported."

        # Run the test, and expect a ValueError
        with self.assertRaises(ValueError) as context:
            cn.verify_flow_jumper_state_on_cn()

        # Compare the expected error message to the actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_pause_jumper_state_on_cn_pass1(self):
        """ Verify Pause Jumper State On Controller Pass Case 1: Exception is not raised """
        controller = self.create_test_controller_object()
        controller.jp = "CL"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,JP=CL")
        controller.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                controller.verify_pause_jumper_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_pause_jumper_state_on_cn_fail1(self):
        """ Verify Pause Jumper State On Controller Fail Case 1: Value on controller does not match what is
        saved in controller.jr """
        controller = self.create_test_controller_object()
        controller.jp = "CL"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,JP=OP")
        controller.data = mock_data

        expected_message = "Unable to verify Controller's 'Pause Jumper State'. Received: OP, Expected: CL"
        try:
            controller.verify_pause_jumper_state_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_pause_jumper_state_on_cn_fail2(self):
        """
        verify_pause_jumper_state_on_cn fail case 2
        Create: The test controller object
        Set: The controller type to the 1000
        Store: Expected error message
        Run: Test, and expect a ValueError
        Compare: Expected error message to actual error message
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Set the controller type to the 1000
        cn.ty = "10"

        # Store the expected error message
        e_msg = "Attempting to verify a 'Pause Jumper' state on a 1000 which is not supported."

        # Run the test, and expect a ValueError
        with self.assertRaises(ValueError) as context:
            cn.verify_pause_jumper_state_on_cn()

        # Compare the expected error message to the actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_description_length_pass1(self):
        """ Verify Description Length Pass Case 1: Exception is not raised for description:
        '123456789012345678901234567890123456789012' of length 42, on 3200 controller """
        new_desc = "123456789012345678901234567890123456789012"
        controller = self.create_test_controller_object()
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                controller.verify_description_length(_type=controller.ty, _desc=new_desc)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_description_length_pass2(self):
        """ Verify Description Length Pass Case 2: Exception is not raised for description:
        '12345678901234567890123456789012' of length 32, on 1000 controller """
        new_desc = "12345678901234567890123456789012"
        controller = self.create_test_controller_object(_difftype="10")
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                controller.verify_description_length(_type=controller.ty, _desc=new_desc)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_description_length_fail1(self):
        """ Verify Description Length Fail Case 1: Exception is raised for description:
        '1234567890123456789012345678901234567890123' of length 43 (too long), on 3200 controller """
        new_desc = "1234567890123456789012345678901234567890123"
        controller = self.create_test_controller_object()
        test_pass = False
        e_msg = ""

        expected_message = "Controller description '1234567890123456789012345678901234567890123' is too long. " \
                           "Max description length for 3200: 42, Current length: {0}".format(len(new_desc))
        try:
            controller.verify_description_length(_type=controller.ty, _desc=new_desc)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_description_length_fail2(self):
        """ Verify Description Length Fail Case 2: Exception is raised for description:
        '123456789012345678901234567890123' of length 33 (too long), on 1000 controller """
        new_desc = "123456789012345678901234567890123"
        controller = self.create_test_controller_object(_difftype="10")
        test_pass = False
        e_msg = ""

        expected_message = "Controller description '123456789012345678901234567890123' is too long. " \
                           "Max description length for 1000: 32, Current length: {0}".format(len(new_desc))
        try:
            controller.verify_description_length(_type=controller.ty, _desc=new_desc)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Verifies all attributes for the controller object pass case 1: Verifies everything, should run through
         all tests as the controller will be set to a 3200. The 1000 does not verify flow jumper or pause jumper
         state. Expected status should be something other than none """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Set controller type to 3200
        cn.controller_type = "32"

        # Verify get data
        get_data = mock.MagicMock()
        cn.get_data = get_data

        # Verify date and time
        date_and_time_verifier = mock.MagicMock()
        cn.verify_date_and_time = date_and_time_verifier

        # Verify description
        description_verifier = mock.MagicMock()
        cn.verify_description_on_cn = description_verifier

        # Verify serial number
        serial_number = mock.MagicMock()
        cn.verify_serial_number_on_cn = serial_number

        # Verify latitude
        latitude = mock.MagicMock()
        cn.verify_latitude_on_cn = latitude

        # Verify longitude
        longitude = mock.MagicMock()
        cn.verify_longitude_on_cn = longitude

        # Verify status
        status_verifier = mock.MagicMock()
        cn.verify_status_on_cn = status_verifier

        # Verify code version
        code_version = mock.MagicMock()
        cn.verify_code_version_on_cn = code_version

        # Verify rain jumper state
        rain_jumper = mock.MagicMock()
        cn.verify_rain_jumper_state_on_cn = rain_jumper

        # Verify flow jumper state
        flow_jumper = mock.MagicMock()
        cn.verify_flow_jumper_state_on_cn = flow_jumper

        # Verify pause jumper state
        pause_jumper = mock.MagicMock()
        cn.verify_pause_jumper_state_on_cn = pause_jumper

        # Run program while setting expected status to something other than none
        cn.verify_who_i_am(_expected_status='OK')

    #################################
    def test_init_cn_pass1(self):
        """
        init_cn pass case 1:
        Mock: assertion error for turn of echo
        Raise: AssertionError for turn on echo failure
        Return Message for main AssertionError and turn on echo AssertionError
        """
        # Create controller object
        cn = self.create_test_controller_object()
        mock_turn_on_echo = mock.MagicMock(side_effect=AssertionError)
        cn.turn_on_echo = mock_turn_on_echo
        with self.assertRaises(AssertionError) as context:
            cn.init_cn()
        e_msg = "Initiate Controller Command Failed: Turn on Echo Command Failed: "
        print context.exception.message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_init_cn_fail1(self):
        """
        init_cn fail case 1:
        Mock: turn off echo
        Return: None, so it doesnt fail
        Raise: assertion error, for set_sim_mode_to_on
        Return: Message for main AssertionError and AssertionError for set_sim_mode_to_on failure
        """
        # Create controller object
        cn = self.create_test_controller_object()
        mock_turn_on_echo = mock.MagicMock(side_effect=None)
        cn.turn_on_echo = mock_turn_on_echo
        mock_set_sim_mode_to_on = mock.MagicMock(side_effect=AssertionError)
        cn.set_sim_mode_to_on = mock_set_sim_mode_to_on
        with self.assertRaises(AssertionError) as context:
            cn.init_cn()
        e_msg = "Initiate Controller Command Failed: Turn on sim Mode to controller failed: "
        print context.exception.message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_init_cn_fail2(self):
        """
        init cn fail case 2:
        Mock: Turn on echo
        Return: None, so it does not fail
        Mock: Turn sim mode to on
        Return: None, so it does not fail
        Raise: AssertionError for stop_clock
        Return: Message for main AssertionError and AssertionError for stop_clock on failure
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store the expected error
        e_msg = "Initiate Controller Command Failed: stop Command Failed: "

        # Mock turn on echo, no side effect
        mock_turn_on_echo = mock.MagicMock(side_effect=None)
        cn.turn_on_echo = mock_turn_on_echo

        # Mock set sim mode to on, no side effect
        mock_set_sim_mode_to_on = mock.MagicMock(side_effect=None)
        cn.set_sim_mode_to_on = mock_set_sim_mode_to_on

        # Mock stop clock on the controller, return an assertion error
        mock_stop_clock = mock.MagicMock(side_effect=AssertionError)
        cn.stop_clock = mock_stop_clock

        # Run the test and prepare to catch an assertion error
        with self.assertRaises(AssertionError) as context:
            cn.init_cn()

        # print the exception
        print context.exception.message

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_init_cn_fail3(self):
        """
        init cn fail case 3:
        Mock: turn on echo
        Return: None, so it does not fail
        Mock: set sim mode to on
        Return: None, so it does not fail
        Mock: stop clock
        Return: None, so it does not fail
        Raise: AssertionError for set_date_and_time
        Return: Message for AssertionError for main and AssertionError for set date and time on controller
        """

        # Create test controller object
        cn = self.create_test_controller_object()

        # Store the expected error
        e_msg = "Initiate Controller Command Failed: stop Command Failed: "

        # Mock turn on echo, no side effect
        mock_turn_on_echo = mock.MagicMock(side_effect=None)
        cn.turn_on_echo = mock_turn_on_echo

        # Mock set sim mode to on, no side effect
        mock_set_sim_mode_to_on = mock.MagicMock(side_effect=None)
        cn.set_sim_mode_to_on = mock_set_sim_mode_to_on

        # Mock stop clock on the controller, no side effect
        mock_stop_clock = mock.MagicMock(side_effect=None)
        cn.stop_clock = mock_stop_clock

        # Mock set date and time on controller, return an assertion error
        mock_set_date_and_time_on_cn = mock.MagicMock(side_effect=AssertionError)
        cn.set_date_and_time_on_cn = mock_set_date_and_time_on_cn

        # Run the test, and catch an assertion error
        with self.assertRaises(AssertionError) as context:
            cn.init_cn()

        print context.exception.message

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_init_cn_fail4(self):
        """
        init cn fail case 4:
        Create: Test controller object, so the test does not need to use the actual controller
        Mock: turn on echo
        Return: None, so it does not fail
        Mock: set sim mode to on
        Return: None, so it does not fail
        Mock: stop clock
        Return: None, so it does not fail
        Mock: set date and time on controller
        Return: None, so it does not fail
        Mock: Turn on fauxIO
        Raise: AssertionError for fauxIO so the test will fail
        Return: Message for AssertionError for main and AssertionError for fauxIO on controller
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error message
        e_msg = "Initiate Controller Command Failed: Turn on FauxIO Command Failed: "

        # Mock turn on echo, no side effect
        mock_turn_on_echo = mock.MagicMock(side_effect=None)
        cn.turn_on_echo = mock_turn_on_echo

        # Mock set sim mode to on, no side effect
        mock_set_sim_mode_to_on = mock.MagicMock(side_effect=None)
        cn.set_sim_mode_to_on = mock_set_sim_mode_to_on

        # Mock stop clock on the test controller object, no side effect
        mock_stop_clock = mock.MagicMock(side_effect=None)
        cn.stop_clock = mock_stop_clock

        # Mock date and time on the test controller object, no side effect
        mock_set_date_and_time_on_cn = mock.MagicMock(side_effect=None)
        cn.set_date_and_time_on_cn = mock_set_date_and_time_on_cn

        # Mock FauxIO on the test controller object, raise an assertion error
        mock_turn_on_fauxio = mock.MagicMock(side_effect=AssertionError)
        cn.turn_on_faux_io = mock_turn_on_fauxio

        # Catch an AssertionError and run the test
        with self.assertRaises(AssertionError) as context:
            cn.init_cn()

        print context.exception.message

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_init_cn_fail5(self):
        """
        init cn fail case 5:
        Create: Test controller object, so the test does not need to use the actual controller
        Store: The error message for later comparison
        Mock: turn on echo
        Return: None, so it does not fail
        Mock: set sim mode to on
        Return: None, so it does not fail
        Mock: stop clock
        Return: None, so it does not fail
        Mock: set date and time on controller
        Return: None, so it does not fail
        Mock: Turn on fauxIO
        Return: None, so it will not fail
        Mock: serial send and wait for reply for devices
        Raise: ser.send_and_wait_for_reply for devices assertion error
        Return: Message for AssertionError for main and AssertionError for ser.send_and_wait_for_reply on controller
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error message
        e_msg = "Initiate Controller Command Failed: Clear All Devices Command Failed: "

        # Mock turn on echo, no side effect
        mock_turn_on_echo = mock.MagicMock(side_effect=None)
        cn.turn_on_echo = mock_turn_on_echo

        # Mock set sim mode to on, no side effect
        mock_set_sim_mode_to_on = mock.MagicMock(side_effect=None)
        cn.set_sim_mode_to_on = mock_set_sim_mode_to_on

        # Mock stop clock on the test controller object, no side effect
        mock_stop_clock = mock.MagicMock(side_effect=None)
        cn.stop_clock = mock_stop_clock

        # Mock date and time on the test controller object, no side effect
        mock_set_date_and_time_on_cn = mock.MagicMock(side_effect=None)
        cn.set_date_and_time_on_cn = mock_set_date_and_time_on_cn

        # Mock FauxIO on the test controller object, no side effect
        mock_turn_on_fauxio = mock.MagicMock(side_effect=None)
        cn.turn_on_faux_io = mock_turn_on_fauxio

        # Mock serial send and wait for reply, AssertionError as a side effect
        self.mock_send_and_wait_for_reply.side_effect = AssertionError

        # Catch an AssertionError and run the test
        with self.assertRaises(AssertionError) as context:
            cn.init_cn()

        print context.exception.message

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_init_cn_fail6(self):
        """
        init cn fail case 6:
        Create: Test controller object, so the test does not need to use the actual controller
        Store: The error message for later comparison
        Mock: turn on echo
        Return: None, so it does not fail
        Mock: set sim mode to on
        Return: None, so it does not fail
        Mock: stop clock
        Return: None, so it does not fail
        Mock: set date and time on controller
        Return: None, so it does not fail
        Mock: Turn on fauxIO
        Return: None, so it will not fail
        Mock: serial send and wait for reply (clear all for devices)
        Return: None, so it will not fail
        Mock: serial send and wait for reply (clear all for controller)
        Raise: ser.send_and_wait_for_reply assertion error
        Return: Message for AssertionError for main and AssertionError for ser.send_and_wait_for_reply on controller
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error message
        e_msg = "Initiate Controller Command Failed: Clear All Programming Command Failed: "

        # Mock turn on echo, no side effect
        mock_turn_on_echo = mock.MagicMock(side_effect=None)
        cn.turn_on_echo = mock_turn_on_echo

        # Mock set sim mode to on, no side effect
        mock_set_sim_mode_to_on = mock.MagicMock(side_effect=None)
        cn.set_sim_mode_to_on = mock_set_sim_mode_to_on

        # Mock stop clock on the test controller object, no side effect
        mock_stop_clock = mock.MagicMock(side_effect=None)
        cn.stop_clock = mock_stop_clock

        # Mock date and time on the test controller object, no side effect
        mock_set_date_and_time_on_cn = mock.MagicMock(side_effect=None)
        cn.set_date_and_time_on_cn = mock_set_date_and_time_on_cn

        # Mock FauxIO on the test controller object, no side effect
        mock_turn_on_fauxio = mock.MagicMock(side_effect=None)
        cn.turn_on_faux_io = mock_turn_on_fauxio

        # Mock serial send and wait for reply, None for the first side effect, then AssertionError as a second
        # side effect
        self.mock_send_and_wait_for_reply.side_effect = [None, AssertionError]

        # Catch an AssertionError and run the test
        with self.assertRaises(AssertionError) as context:
            cn.init_cn()

        print context.exception.message

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    ################################
    def test_do_enable_packet_sending_to_bm_pass1(self):
        """ Packet communication for 100 between BaseManager and controller pass case 1: Able to send packets """
        # Create controller object
        cn = self.create_test_controller_object()

        # Do increment clock
        clock_mock = mock.MagicMock(return_value=5)
        cn.do_increment_clock = clock_mock

        # Set sim mode to off
        cn.set_sim_mode_to_off()

        # Do start clock
        cn.start_clock()

        # Mock serial and wait for reply
        self.mock_send_and_wait_for_reply('DO,BM=PN')

        # Run method
        cn.do_enable_packet_sending_to_bm()

    ################################
    def test_do_enable_packet_sending_to_bm_fail1(self):
        """ Packet communication for 100 between BaseManager and controller fail case 1: Not able to send packets """
        # Create controller object
        cn = self.create_test_controller_object()

        # Do increment clock
        clock_mock = mock.MagicMock(return_value=5)
        cn.do_increment_clock = clock_mock

        # Store expected exception
        err_msg = "Exception occurred initializing 1000 controller packet sending to BM: " \
                  "Turn Off Sim Mode Command Failed: "

        # Raise an exception
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Mock serial and wait for reply
        with self.assertRaises(Exception) as context:
            cn.do_enable_packet_sending_to_bm()

        # Compare the expected error message to the actual error message
        self.assertEqual(err_msg, context.exception.message)

    #################################
    def test_do_search_for_dv_pass1(self):
        """ Do Search for Device Type Pass Case 1: Exception is not raised for correct controller type: 'ZN' """
        dv_type = 'ZN'
        controller = self.create_test_controller_object()

        expected_command = "DO,SR=ZN"
        controller.do_search_for_dv(dv_type)
        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    #################################
    def test_do_search_for_dv_fail1(self):
        """ Do Search for Device Type Fail Case 1: Failed communication with controller """
        dv_type = 'ZN'
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.do_search_for_dv(dv_type)
        e_msg = "Searching for two-wire devices failed.. "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_do_search_for_dv_fail2(self):
        """ Do Search for Device Type Fail Case 1: Incorrect controller type passed as argument """
        dv_type = 'Zone'
        controller = self.create_test_controller_object()

        with self.assertRaises(Exception) as context:
            controller.do_search_for_dv(dv_type)
        e_msg = "[CONTROLLER] incorrect two-wire device type passed in: Zone"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_load_all_dv_to_cn_pass1(self):
        """
        load_all_dv_to_cn pass case 1:
        Mock: load dv to cn d1
        Return: None because so it does not fail
        Mock: load dv to cn mv d1
        Return: None because so it does not fail
        Mock: load dv to cn d2
        Return: None because so it does not fail
        Mock: load dv to cn mv d2
        Return: None because so it does not fail
        Mock: load dv to cn mv d2
        Return: None because so it does not fail
        Mock: load dv to cn d4
        Return: None because so it does not fail
        Mock: load dv to cn DD
        Return: None because so it does not fail
        Mock: load dv to cn MS
        Return: None because so it does not fail
        Mock: load dv to cn FM
        Return: None because so it does not fail
        Mock: load dv to cn TS
        Return: None because so it does not fail
        Mock: load dv to cn SW
        Return: None because so it does not fail
        Run test: Have values for all ten decoders loaded onto controller
        Return: Should not return anything, as this is the case where it does not return an exception.
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Mock each of the load dv to cn methods
        mock_load_dv_to_cn_d1 = mock.MagicMock(side_effect=None)
        cn.load_dv_to_cn = mock_load_dv_to_cn_d1
        mock_load_dv_to_cn_mv_d1 = mock.MagicMock(side_effect=None)
        cn.load_dv_to_cn = mock_load_dv_to_cn_mv_d1
        mock_load_dv_to_cn_d2 = mock.MagicMock(side_effect=None)
        cn.load_dv_to_cn = mock_load_dv_to_cn_d2
        mock_load_dv_to_cn_mv_d2 = mock.MagicMock(side_effect=None)
        cn.load_dv_to_cn = mock_load_dv_to_cn_mv_d2
        mock_load_dv_to_cn_d4 = mock.MagicMock(side_effect=None)
        cn.load_dv_to_cn = mock_load_dv_to_cn_d4
        mock_load_dv_to_cn_dd = mock.MagicMock(side_effect=None)
        cn.load_dv_to_cn = mock_load_dv_to_cn_dd
        mock_load_dv_to_cn_ms = mock.MagicMock(side_effect=None)
        cn.load_dv_to_cn = mock_load_dv_to_cn_ms
        mock_load_dv_to_cn_fm = mock.MagicMock(side_effect=None)
        cn.load_dv_to_cn = mock_load_dv_to_cn_fm
        mock_load_dv_to_cn_ts = mock.MagicMock(side_effect=None)
        cn.load_dv_to_cn = mock_load_dv_to_cn_ts
        mock_load_dv_to_cn_sw = mock.MagicMock(side_effect=None)
        cn.load_dv_to_cn = mock_load_dv_to_cn_sw

        # Run method with all of the lists filled correctly
        cn.load_all_dv_to_cn(d1_list=["TSD0001"],
                             mv_d1_list=["TSD0001"],
                             d2_list=["TSD0001"],
                             mv_d2_list=["TSD0001"],
                             d4_list=["TSD0001"],
                             dd_list=["TSD0001"],
                             ms_list=["TSD0001"],
                             fm_list=["TSD0001"],
                             ts_list=["TSD0001"],
                             sw_list=["TSD0001"])

    #################################
    def test_load_all_dv_to_cn_fail1(self):
        """
        load_all_dv_to_cn fail case 1:
        Mock: load dv to cn d1
        Return: An exception so it will fail
        Store: An expected error message
        Run: Test with all of the lists filled in (not necessarily with correct values)
        Compare: The expected error message with the actual error message
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Expected error message
        e_msg = "Exception occurred loading all available devices to controller: "

        # Mock failed load dv to cn method
        mock_load_dv_to_cn_d1 = mock.MagicMock(side_effect=Exception)
        cn.load_dv_to_cn = mock_load_dv_to_cn_d1

        # Run method with all of the lists filled correctly
        with self.assertRaises(Exception) as context:
            cn.load_all_dv_to_cn(d1_list=["TSD0001"],
                                 mv_d1_list=["TSD0001"],
                                 d2_list=["TSD0001"],
                                 mv_d2_list=["TSD0001"],
                                 d4_list=["TSD0001"],
                                 dd_list=["TSD0001"],
                                 ms_list=["TSD0001"],
                                 fm_list=["TSD0001"],
                                 ts_list=["TSD0001"],
                                 sw_list=["TSD0001"])

        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_load_dv_to_cn_pass1(self):
        """ Load Device to Controller Pass Case 1: Exception is not raised for correct controller type:
         Single Valve Decoder"""
        dv_type = opcodes.single_valve_decoder
        dv_list = ["TSD0001"]
        controller = self.create_test_controller_object()

        expected_command = "DEV,{0}=TSD0001".format(dv_type)
        controller.load_dv_to_cn(dv_type, dv_list)
        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    #################################
    def test_load_dv_to_cn_pass2(self):
        """ Load Device to Controller Pass Case 2: Controller accepts multiple serial numbers """
        dv_type = opcodes.single_valve_decoder
        dv_list = ["TSD0001", "TSD0002", "TSD0003"]
        controller = self.create_test_controller_object()
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                controller.load_dv_to_cn(dv_type, dv_list)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_load_dv_to_cn_fail1(self):
        """ Load Device to Controller Fail Case 1: Incorrect device type passed as argument """
        dv_type = opcodes.booster_pump
        dv_list = ["TSD0001"]
        controller = self.create_test_controller_object()

        with self.assertRaises(Exception) as context:
            controller.load_dv_to_cn(dv_type, dv_list)
        e_msg = "[CONTROLLER] incorrect decoder type: {0}".format(str(dv_type))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_load_dv_to_cn_fail2(self):
        """ Load Device to Controller Fail Case 2: Incorrect serial number size passed as argument """
        dv_type = opcodes.single_valve_decoder
        dv_list = ["TSD0001", "TSD002"]
        controller = self.create_test_controller_object()

        with self.assertRaises(Exception) as context:
            controller.load_dv_to_cn(dv_type, dv_list)
        e_msg = "Device id is not a valid serial number: TSD002"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_load_dv_to_cn_fail3(self):
        """ Load Device to Controller Fail Case 3: Failed Communication with the controller """
        dv_type = opcodes.single_valve_decoder
        dv_list = ["TSD0001", "TSD0002"]
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.load_dv_to_cn(dv_type, dv_list)
        e_msg = "Load device(s) failed.. "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_turn_on_faux_io_pass1(self):
        """ Turn On Faux IO Pass Case 1: Communication with controller is successful """
        controller = self.create_test_controller_object()

        expected_command = "DO,FX=TR"
        controller.turn_on_faux_io()
        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    #################################
    def test_turn_on_faux_io_fail1(self):
        """ Turn On Faux IO Fail Case 3: Failed Communication with the controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.turn_on_faux_io()
        e_msg = "Turn on FauxIO Command Failed: "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_turn_on_echo_pass1(self):
        """
        turn_on_echo pass case 1:
        Create: Test controller object
        Store: Expected command
        Run: Test
        Mock: Serial send and wait for reply
        Return: Nothing, test only shows that expected command was sent
        """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store the expected command
        expected_command = 'DO,EC=TR'

        # Run the test
        cn.turn_on_echo()

        # Mock serial and wait for reply
        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    #################################
    def test_turn_on_echo_fail1(self):
        """
        turn_on_echo fail case 1:
        Create: Test controller object
        Store: Expected exception
        Mock: Serial send and wait for reply, have a side effect of exception
        Run: Test
        Compare: Expected exception to actual exception
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected exception
        e_msg = "Turning on Echo Command Failed: "

        # Mock serial
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Run test, and expect an exception
        with self.assertRaises(Exception) as context:
            cn.turn_on_echo()

        # Compare actual exception to expected exception
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_turn_off_echo_pass1(self):
        """
        turn_off_echo pass case 1:
        Create: Test controller object
        Store: Expected command
        Run: Test
        Mock: Serial send and wait for reply
        Return: Nothing, test only shows that expected command was sent
        """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store the expected command
        expected_command = 'DO,EC=FA'

        # Run the test
        cn.turn_off_echo()

        # Mock serial and wait for reply
        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    #################################
    def test_turn_off_echo_fail1(self):
        """
        turn_off_echo fail case 1:
        Create: Test controller object
        Store: Expected exception
        Mock: Serial send and wait for reply, have a side effect of exception
        Run: Test
        Compare: Expected exception to actual exception
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected exception
        e_msg = "Turning off Echo Command Failed: "

        # Mock serial
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Run test, and expect an exception
        with self.assertRaises(Exception) as context:
            cn.turn_off_echo()

        # Compare actual exception to expected exception
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_stop_clock_pass1(self):
        """ Stop Clock Pass Case 1: Communication with controller is successful """
        controller = self.create_test_controller_object()

        expected_command = "DO,CK,TM=0"
        controller.stop_clock()
        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    #################################
    def test_stop_clock_fail1(self):
        """ Stop Clock Fail Case 1: Failed Communication with the controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.stop_clock()
        e_msg = "Stop Clock Command Failed: "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_start_clock_pass1(self):
        """ Start Clock Pass Case 1: Communication with controller is successful """
        controller = self.create_test_controller_object()

        expected_command = "DO,CK,TM=-1"
        controller.start_clock()
        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    #################################
    def test_start_clock_fail1(self):
        """ Start Clock Fail Case 1: Failed Communication with the controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.start_clock()
        e_msg = "Start Clock Command Failed: "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_do_increment_clock_pass1(self):
        """
        do_increment_clock pass case 1:
        Create: Test controller object
        Mock: increment_date_time
        Run: Test
        Pass in: Hours as an integer, minutes as an integer, and seconds as an integer, all between 0-60
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Mock increment date time
        mock_increment_date_time = mock.MagicMock()
        date_mngr.controller_datetime.increment_date_time = mock_increment_date_time
        date_mngr.curr_day.increment_date_time = mock_increment_date_time

        # Run test with hours between 0-24, minutes between 0-60, and seconds between 0-60, all as integers
        cn.do_increment_clock(hours=5, minutes=3, seconds=22)

    #################################
    def test_do_increment_clock_pass2(self):
        """
        do_increment_clock pass case 2:
        Create: Test controller object
        Change: Controller type to 1000
        Mock: increment_date_time
        Run: Test
        Pass in: Hours as an integer, minutes as an integer, and seconds as an integer, all between 0-60
        """

        # Create test controller object
        cn = self.create_test_controller_object()

        # Change controller type to 1000
        cn.ty = '10'

        # Mock increment_date_time
        mock_increment_date_time = mock.MagicMock()
        date_mngr.controller_datetime.increment_date_time = mock_increment_date_time
        date_mngr.curr_day.increment_date_time = mock_increment_date_time

        # Run the test
        cn.do_increment_clock(hours=3, minutes=2, seconds=1)

    #################################
    def test_do_increment_clock_pass3(self):
        """
        do_increment_clock pass case 3:
        - Test hours and minutes set to 0 while seconds still have a value
        Create: Test controller object
        Change: Controller type to 1000
        Mock: increment_date_time
        Run: Test
        Pass in: Hours and minutes to 0, and seconds as an integer between 0-60
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Change controller type to 1000
        cn.ty = '10'

        # Mock increment_date_time
        mock_increment_date_time = mock.MagicMock()
        date_mngr.controller_datetime.increment_date_time = mock_increment_date_time

        # Run test, expect a value error
        cn.do_increment_clock(hours=0, minutes=0, seconds=20)

    #################################
    def test_do_increment_clock_pass4(self):
        """
        do_increment_clock pass case 4:
        - Test seconds set to 0 while hours and minutes still have proper integer values
        Create: Test controller object
        Change: Controller type to 1000
        Mock: increment_date_time
        Run: Test
        Pass in: Seconds to 0, and minutes and hours as integers between 0-60
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Change controller type to 1000
        cn.ty = '10'

        # Mock increment_date_time
        mock_increment_date_time = mock.MagicMock()
        date_mngr.controller_datetime.increment_date_time = mock_increment_date_time

        # Run test, expect a value error
        cn.do_increment_clock(hours=01, minutes=05, seconds=0)

    #################################
    def test_do_increment_clock_fail(self):
        """
        do_increment_clock fail 1:
        Create: Test controller object
        Store: Expected error message
        Run: Test, expect ValueError
        Pass in: Hours, minutes, and seconds all as 0
        Compare: Expected error to actual error
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error
        e_msg = "Failed trying to increment clock. Invalid type passed in for hours. Received: {0}, " \
                "Expected: int ".format(str(0))

        # Run the test, passing in all 0's as values, expect a value error
        with self.assertRaises(ValueError) as context:
            cn.do_increment_clock(hours=0, minutes=0, seconds=0)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################

    def test_do_increment_clock_fail2(self):
        """
        do_increment_clock fail 2:
        Create: Test controller object
        Store: Expected error message
        Run: Test, expect TypeError
        Pass in: Minutes correct and correct seconds as 0, pass in a bad value for hours as 24.5 so that it's not an integer
        :raises type error for hours expeceted an in pass in float
        """


        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error
        e_msg = "Failed trying to increment clock. Invalid type passed in for hours. Received: {0}, " \
                "Expected: int ".format(str(24.5))

        # Run the test, passing in all 0's as values, expect a value error
        with self.assertRaises(TypeError) as context:
            cn.do_increment_clock(hours=24.5, minutes=0, seconds=0)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_do_increment_clock_fail3(self):
        """
        do_increment_clock fail 3:
        Create: Test controller object
        Store: Expected error message
        Run: Test, expect ValueError
        Pass in: Minutes and seconds  as 0, pass in hours as 25 so it is above the range of 1-24
        Compare: Expected error to actual error
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error
        e_msg = "Failed trying to increment clock. Invalid range for hours. Expected a number between " \
                "0 - 24, Received: {0},".format(str(24))

        # Run test, expect a value error
        with self.assertRaises(ValueError) as context:
            cn.do_increment_clock(hours=24, minutes=0, seconds=0)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_do_increment_clock_fail4(self):
        """
        do_increment_clock fail 4:
        Create: Test controller object
        Store: Expected error message
        Run: Test, expect TypeError
        Pass in: Seconds as 0, minutes as a float, and hours inside 0-24
        Compare: Expected error to actual error
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error
        e_msg = "Failed trying to increment clock. Invalid type passed in for minutes. Received: {0}, " \
                "Expected: int ".format(str(22.1))

        # Run test, expect a value error
        with self.assertRaises(TypeError) as context:
            cn.do_increment_clock(hours=22, minutes=22.1, seconds=0)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_do_increment_clock_fail5(self):
        """
        do_increment_clock fail 5:
        Create: Test controller object
        Store: Expected error message
        Run: Test, expect TypeError
        Pass in: Seconds as 0, minutes as 61 so it's outside the range of 0-60, and hours inside 0-24
        Compare: Expected error to actual error
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error
        e_msg = "Failed trying to increment clock. Invalid range for minutes. Expected a number between " \
                "0 - 60, Received: {0},".format(str(61))

        # Run test, expect a value error
        with self.assertRaises(ValueError) as context:
            cn.do_increment_clock(hours=22, minutes=61, seconds=0)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_do_increment_clock_fail6(self):
        """
        do_increment_clock fail 6:
        Create: Test controller object
        Store: Expected error message
        Run: Test, expect TypeError
        Pass in: Seconds as 23.1, minutes between 0-60, and hours inside 0-24
        pass in seconds as float instead of int
        Compare: Expect a type error to actual error
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error
        e_msg = "Failed trying to increment clock. Invalid type passed in for seconds. Received: {0}, " \
                        "Expected: int ".format(str(23.1))

        # Run test, expect a value error
        with self.assertRaises(TypeError) as context:
            cn.do_increment_clock(hours=22, minutes=59, seconds=23.1)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_do_increment_clock_fail7(self):
        """
        do_increment_clock fail 7:
        Create: Test controller object
        Store: Expected error message
        Run: Test, expect TypeError
        Pass in: Seconds as 62, minutes between 0-60, and hours between 0-24
        Compare: Expected error to actual error
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error
        e_msg = "Failed trying to increment clock. Invalid range for seconds. Expected a number between " \
                        "0 - 60, Received: {0},".format(str(62))

        # Run test, expect a value error
        with self.assertRaises(ValueError) as context:
            cn.do_increment_clock(hours=22, minutes=59, seconds=62)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_do_increment_clock_fail8(self):
        """
        do_increment_clock fail 8:
        Create: Test controller object
        Store: Expected error message
        Set: controller type to 1000
        Mock: A ValueError on increment_date_time
        Run: Test, expect ValueError
        Pass in: Seconds between 0-60, minutes between 0-60, and hours between 0-24
        pass in correct values so that we can get to method with triggering other value erros or type errors
        Verify the that value error is raised
        """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store expected error message
        e_msg = "Failed trying to increment clock. date time values are incorrect. Received: {0}, " \
                "Expected: int ".format(str(12))

        # Set controller type to 1000
        cn.ty = "10"

        # Mock increment_date_time for 1000
        mock_increment_date_time = mock.MagicMock(side_effect=ValueError)
        cn.date_mngr.controller_datetime.increment_date_time = mock_increment_date_time

        # Run test with correct values passed in and a ValueError expected
        with self.assertRaises(ValueError) as context:
            cn.do_increment_clock(hours=12, minutes=5, seconds=1)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_do_increment_clock_fail9(self):
        """
        do_increment_clock fail 8:
        Create: Test controller object
        Store: Expected error message
        Set: controller type to 3200
        Mock: A ValueError on increment_date_time
        Run: Test, expect TypeError
        Pass in: Seconds between 0-60, minutes between 0-60, and hours between 0-24
        Compare: Expected error to actual error
        """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store expected error message
        e_msg = "Failed trying to increment clock. date time values are incorrect. Received: {0}, " \
                "Expected: int ".format(str(12))

        # Set controller type to 3200
        cn.ty = "32"

        # Mock increment_date_time for 1000
        mock_increment_date_time = mock.MagicMock(side_effect=ValueError)
        cn.date_mngr.controller_datetime.increment_date_time = mock_increment_date_time

        # Run test with correct values passed in and a ValueError expected
        with self.assertRaises(ValueError) as context:
            cn.do_increment_clock(hours=12, minutes=5, seconds=1)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_wait_for_controller_after_reboot_pass1(self):
        """
        wait_for_controller_after_reboot pass case 1:
        Create: The test controller object
        Mock: Set_serial_port_timeout because it goes outside the method
        Return: None, so it does not fail
        Mock: ser.wait so it does not go outside the method
        Return: 0, so result is 0
        Mock: Ser.working
        Return: Nothing, if the test passes after running through the code, then it works
        Run: Test
        """
        # Create the test controller object
        cn = self.create_test_controller_object()

        # Mock the serial port timeout
        mock_serial_port_timeout = mock.MagicMock(side_effect=[None, None])
        cn.set_serial_port_timeout = mock_serial_port_timeout

        # Mock the ser.wait
        mock_ser_wait = mock.MagicMock(return_value=0)
        cn.ser.wait = mock_ser_wait

        # Mock the ser.working
        mock_ser_working = mock.MagicMock(side_effect=None)
        cn.ser.working = mock_ser_working

        # Run test
        cn.wait_for_controller_after_reboot()

    #################################
    def test_wait_for_controller_after_reboot_fail1(self):
        """
        wait_for_controller_after_reboot pass case 1:
        Create: The test controller object
        Store: Expected error message
        Mock: Set_serial_port_timeout because it goes outside the method
        Return: Nothing, so it does not fail with the incorrect type of error
        Mock: ser.wait so it does not go outside the method
        Return: 25, so a value error, and not a type error is raised
        Run: Test
        Compare: Actual error message to expected error message
        """
        # Create the test controller object
        cn = self.create_test_controller_object()

        # Store an expected error, an empty string because
        e_msg = "Reconnection to controller failed"

        # Mock the serial port timeout
        mock_serial_port_timeout = mock.MagicMock(side_effect=[None, None])
        cn.set_serial_port_timeout = mock_serial_port_timeout

        # Mock the ser.wait
        mock_ser_wait = mock.MagicMock(return_value=25)
        cn.ser.wait = mock_ser_wait

        # Run test, prepare for a value error
        with self.assertRaises(ValueError) as context:
            cn.wait_for_controller_after_reboot()

        # Compare actual error message to expected error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_do_reboot_controller_pass1(self):
        """
        do_reboot_controller pass case 1:
        Mock: Ser.send_and_wait_for_reply
        Return: None, so it does not fail
        Set: Controller type equal to 3200
        Mock: Wait_for_controller_after_reboot
        Return: None so it does not fail
        Run: Test
        """
        # Create the test controller object
        cn = self.create_test_controller_object()

        # Mock send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = None

        # Set controller type to 3200
        cn.controller_type = '32'

        # Mock wait for controller after reboot
        mock_wait_for_controller_after_reboot = mock.MagicMock(side_effect=None)
        cn.wait_for_controller_after_reboot = mock_wait_for_controller_after_reboot

        # Run test
        cn.do_reboot_controller()

    if __name__ == "__main__":
        unittest.main()
