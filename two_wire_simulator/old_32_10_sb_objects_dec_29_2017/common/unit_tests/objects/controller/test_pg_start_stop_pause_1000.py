__author__ = 'Eldinner Plate'

import unittest
import mock
import serial

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes, types

from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.pg_start_stop_pause_cond import BaseStartStopPause

from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_1000 import StartConditionFor1000, StopConditionFor1000, PauseConditionFor1000


class TestStartStopPause1000(unittest.TestCase):
    """
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        BaseStartStopPause.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_start_condition_for_1000_object(self):
        """ Creates a new Start condition object for testing purposes """
        ssp = StartConditionFor1000(program_ad=1)
        return ssp

    #################################
    def create_test_stop_condition_for_1000_object(self):
        """ Creates a new Pause condition object for testing purposes """
        ssp = StopConditionFor1000(program_ad=1)
        return ssp

    #################################
    def create_test_pause_condition_for_1000_object(self):
        """ Creates a new Pause condition object for testing purposes """
        ssp = PauseConditionFor1000(program_ad=1)
        return ssp

    def test_initializing_objects(self):
        """ Test Initializing Objects: Create instances of the three different objects """
        start = self.create_test_start_condition_for_1000_object()
        stop = self.create_test_stop_condition_for_1000_object()
        pause = self.create_test_pause_condition_for_1000_object()

    def test_set_day_time_start_pass_1(self):
        """ Test Set Day Time Start Pass Case 1: Test the calendar interval type
        Create the test object
        Assign values to the variables that will be used in the method
        Create an expected command that will be used to check if the test passed
        Mock: verify_valid_start_times
            Return: Nothing we just want to pass over it
        Mock: build_start_time_string
            Return: start_times which will help the method build the correct command string
        Call the method with the appropriate parameters
        Assert that the mocked send_and_wait_for_reply was called with the expected command
        """
        start = self.create_test_start_condition_for_1000_object()
        start_times = '480,600,780'

        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9}".format(
            opcodes.program_start_condition,
            start._program_ad,
            opcodes.type,
            opcodes.date_time,
            opcodes.date_time,
            opcodes.true,
            opcodes.start_times,
            start_times,
            opcodes.calendar_interval,
            opcodes.even_day
        )

        start.verify_valid_start_times = mock.MagicMock()
        start.build_start_time_string = mock.MagicMock(return_value=start_times)

        start.set_day_time_start(_st_list=[480, 600, 780], _interval_type=opcodes.calendar_interval,
                                 _interval_args=opcodes.even_day)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_day_time_start_pass_2(self):
        """ Test Set Day Time Start Pass Case 2: Test the day interval type
        Create the test object
        Create an expected command that will be used to check if the test passed
        Call the method with the appropriate parameters
        Assert that the mocked send_and_wait_for_reply was called with the expected command
        """
        start = self.create_test_start_condition_for_1000_object()

        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7}".format(
            opcodes.program_start_condition,
            start._program_ad,
            opcodes.type,
            opcodes.date_time,
            opcodes.date_time,
            opcodes.true,
            opcodes.day_interval,
            '1'
        )

        start.set_day_time_start(_interval_type=opcodes.day_interval, _interval_args=1)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_day_time_start_pass_3(self):
        """ Test Set Day Time Start Pass Case 3: Test the week day interval type
        Create the test object
        Assign values to the variables that will be used in the method
        Create an expected command that will be used to check if the test passed
        Call the method with the appropriate parameters
        Assert that the mocked send_and_wait_for_reply was called with the expected command
        """
        start = self.create_test_start_condition_for_1000_object()
        week_days = [1, 1, 1, 1, 1, 1, 1]

        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7}".format(
            opcodes.program_start_condition,
            start._program_ad,
            opcodes.type,
            opcodes.date_time,
            opcodes.date_time,
            opcodes.true,
            opcodes.week_days,
            '1111111'
        )

        start.set_day_time_start(_interval_type=opcodes.week_days, _interval_args=week_days)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_day_time_start_pass_4(self):
        """ Test Set Day Time Start Pass Case 4: Test the semi-month interval type
        Create the test object
        Assign values to the variables that will be used in the method
        Create an expected command that will be used to check if the test passed
        Call the method with the appropriate parameters
        Assert that the mocked send_and_wait_for_reply was called with the expected command
        """
        start = self.create_test_start_condition_for_1000_object()
        semi_monthly = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7}".format(
            opcodes.program_start_condition,
            start._program_ad,
            opcodes.type,
            opcodes.date_time,
            opcodes.date_time,
            opcodes.true,
            opcodes.semi_month_interval,
            '1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1'
        )

        start.set_day_time_start(_interval_type=opcodes.semi_month_interval, _interval_args=semi_monthly)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_day_time_start_fail_1(self):
        """ Test Set Day Time Start Fail Case 1: invalid _dt passed in
        Create the test object
        Create variables that will be used in the method
        Create the string that we expect the method to raise
        Call the method while asserting that it will throw an error
        Compare the message raised by the error to the message that we created
        """
        start= self.create_test_start_condition_for_1000_object()
        invalid_param = 'invalid'

        expected_message = "The 'dt' value passed in is not valid. Expected either '{0}' or '{1}', got '{2}'.".format(
            opcodes.true,
            opcodes.false,
            invalid_param
        )

        with self.assertRaises(ValueError) as context:
            start.set_day_time_start(_dt=invalid_param)

        self.assertEqual(expected_message, context.exception.message)

    def test_set_day_time_start_fail_2(self):
        """ Test Set Day Time Start Fail Case 2: invalid calendar_interval argument passed in
        Create the test object
        Create variables that will be used in the method
        Create the string that we expect the method to raise
        Call the method while asserting that it will throw an error
        Compare the message raised by the error to the message that we created
        """
        start = self.create_test_start_condition_for_1000_object()
        invalid_param = 'invalid'

        expected_message = "Invalid calendar interval arg for start time: {entered}. Valid arguments: " \
                           "'EV' | 'OD' | 'OS'".format(entered=invalid_param)

        with self.assertRaises(ValueError) as context:
            start.set_day_time_start(_interval_type=opcodes.calendar_interval, _interval_args=invalid_param)

        self.assertEqual(expected_message, context.exception.message)

    def test_set_day_time_start_fail_3(self):
        """ Test Set Day Time Start Fail Case 3: invalid day interval parameter passed in
        Create the test object
        Create variables that will be used in the method
        Create the string that we expect the method to raise
        Call the method while asserting that it will throw an error
        Compare the message raised by the error to the message that we created
        """
        start = self.create_test_start_condition_for_1000_object()
        invalid_param = 0

        expected_message = "Invalid day interval arg for start time: {entered}. Valid arguments: 1 to xx days"\
            .format(entered=invalid_param)

        with self.assertRaises(ValueError) as context:
            start.set_day_time_start(_interval_type=opcodes.day_interval, _interval_args=invalid_param)

        self.assertEqual(expected_message, context.exception.message)

    def test_set_day_time_start_fail_4(self):
        """ Test Set Day Time Start Fail Case 4: send_and_wait_for_reply raises an Exception
        Create the test object
        Create variables that will be used in the method
        Create the string that we expect the method to raise
        Call the method while asserting that it will throw an error
        Compare the message raised by the error to the message that we created
        """
        start = self.create_test_start_condition_for_1000_object()

        self.mock_send_and_wait_for_reply.side_effect = Exception

        expected_message = "Exception occurred when trying to set a date/time condition. Command: SET,PT=1,TY=DT,DT=TR"

        with self.assertRaises(Exception) as context:
            start.set_day_time_start()

        self.assertEqual(expected_message, context.exception.message)

    def test_build_start_time_string_pass_1(self):
        """ Test Build Start Time String Pass Case 1: Verify the correct string is built
        Create the test object
        Create the variable to be passed into the method
        Create the value we expect to be returned
        Call the method and store it's returned value in a variable
        Compare the returned value with our expected value to see if the test passes
        """
        start = self.create_test_start_condition_for_1000_object()
        start_time_list = [480, 600, 780]
        expected_value = "480=600=780"

        actual_value = start.build_start_time_string(_st_list=start_time_list)

        self.assertEqual(expected_value, actual_value)

    def test_build_semi_month_interval_string_pass_1(self):
        """ Test Build Semi Month Interval String Pass Case 1: Verify the correct string is built
        Create the test object
        Create the variable to be passed into the method
        Create the value we expect to be returned
        Call the method and store it's returned value in a variable
        Compare the returned value with our expected value to see if the test passes
        """
        start = self.create_test_start_condition_for_1000_object()
        semi_month_list = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        expected_value = "1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1"

        actual_value = start.build_semi_month_interval_string(_sm_list=semi_month_list)

        self.assertEqual(expected_value, actual_value)

    def test_build_week_day_string_pass_1(self):
        """ Test Build Week day Interval String Pass Case 1: Verify the correct string is built
        Create the test object
        Create the variable to be passed into the method
        Create the value we expect to be returned
        Call the method and store it's returned value in a variable
        Compare the returned value with our expected value to see if the test passes
        """
        start = self.create_test_start_condition_for_1000_object()
        wd_list = [1, 1, 1, 1, 1, 1, 1]
        expected_value = "1111111"

        actual_value = start.build_week_day_string(_wd_list=wd_list)

        self.assertEqual(expected_value, actual_value)

    def test_verify_valid_week_day_list_pass_1(self):
        """ Test Verify Valid Week Day List Pass Case 1: Runs through the method without errors
        Create test object
        Create the variable that will be passed into the method
        Create the expected value
        Call the method and store the value it returns
        Assert that the returned value is equal to the expected value
        """
        start = self.create_test_start_condition_for_1000_object()
        wd_list = [1, 1, 1, 1, 1, 1, 1]

        expected_value = wd_list

        actual_value = start.verify_valid_week_day_list(_week_day_list=wd_list)

        self.assertEqual(expected_value, actual_value)

    def test_verify_valid_week_day_list_fail_1(self):
        """ Test Verify Valid Week Day List Fail Case 1: Pass in a list with more than 7 integers
        Create test object
        Create the variables that will be passed into the method
        Create the message we expect will be returned
        Call the method and assert that a ValueError will be raised
        Assert that the error message that was raised by the method is the same as what we expected
        """
        start = self.create_test_start_condition_for_1000_object()
        wd_list = [1, 1, 1, 1, 1, 1, 1, 1]

        expected_message = "Invalid week day water schedule argument passed in. List must contain 7 comma separated " \
                           "integers, instead found: {0}".format(
                               str(len(wd_list))  # {0}
                           )

        with self.assertRaises(ValueError) as context:
            start.verify_valid_week_day_list(_week_day_list=wd_list)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_valid_week_day_list_fail_2(self):
        """ Test Verify Valid Week Day List Fail Case 2: Pass in a list that contains an integer that isn't '1' or '0'
        Create test object
        Create the variables that will be passed into the method
        Create the message we expect will be returned
        Call the method and assert that a ValueError will be raised
        Assert that the error message that was raised by the method is the same as what we expected
        """
        start = self.create_test_start_condition_for_1000_object()
        wd_list = [1, 1, 1, 1, 1, 1, 5]

        expected_message = "Invalid list of week day values for Program {0}. Expects only 1's and/or 0's".format(
            str(start._program_ad))

        with self.assertRaises(ValueError) as context:
            start.verify_valid_week_day_list(_week_day_list=wd_list)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_valid_semi_month_interval_list_pass_1(self):
        """ Test Verify Valid Semi Month Interval List Pass Case 1: Runs through the method and verifies the list
        Create the test object
        Create the variable to be passed into the method
        Create the value we expect to be returned
        Call the method and store the value it returns
        Assert that the returned value is equal to the expected value
        """
        start = self.create_test_start_condition_for_1000_object()
        sm_list = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

        expected_value = True

        actual_value = start.verify_valid_semi_month_interval_list(_semi_month_interval_list=sm_list)

        self.assertEqual(expected_value, actual_value)

    def test_verify_valid_semi_month_interval_list_fail_1(self):
        """ Test Verify Valid Semi Month List Fail Case 1: Pass in a list with more than 24 integers
        Create test object
        Create the variables that will be passed into the method
        Create the message we expect will be returned
        Call the method and assert that a ValueError will be raised
        Assert that the error message that was raised by the method is the same as what we expected
        """
        start = self.create_test_start_condition_for_1000_object()
        sm_list = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

        expected_message = "Invalid semi month interval argument passed in. List must contain 24 comma separated " \
                           "integers, instead found: {0}".format(
                               str(len(sm_list))  # {0}
                           )

        with self.assertRaises(ValueError) as context:
            start.verify_valid_semi_month_interval_list(_semi_month_interval_list=sm_list)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_valid_start_times_pass_1(self):
        """ Test Verify Valid Start Times Pass Case 1: Runs through the method and verifies the list
        Create the test object
        Create the variable to be passed into the method
        Create the value we expect to be returned
        Call the method and store the value it returns
        Assert that the returned value is equal to the expected value
        """
        start = self.create_test_start_condition_for_1000_object()
        start_times_list = [1, 1, 1, 1, 1, 1, 1, 1]

        expected_value = True

        actual_value = start.verify_valid_start_times(_list_of_times=start_times_list)

        self.assertEqual(expected_value, actual_value)

    def test_verify_valid_start_times_fail_1(self):
        """ Test Verify Valid Start Times Fail Case 1: Pass in a list with more than 8 integers
        Create test object
        Create the variables that will be passed into the method
        Create the message we expect will be returned
        Call the method and assert that a ValueError will be raised
        Assert that the error message that was raised by the method is the same as what we expected
        """
        start = self.create_test_start_condition_for_1000_object()
        start_times_list = [1, 1, 1, 1, 1, 1, 1, 1, 1]

        expected_message = "Invalid number of start times to set for Start condition: {0}. Expects 1 to 8 start " \
                           "times, received: {1}".format(
                               str(start._program_ad),  # {0}
                               str(len(start_times_list))  # {1}
                           )

        with self.assertRaises(ValueError) as context:
            start.verify_valid_start_times(_list_of_times=start_times_list)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_interval_on_cn_pass_1(self):
        """ Test Verify Interval On Controller Pass Case 1: Method runs through with calendar interval
        Create test object
        Create the variables that will be used in the method
        Mock: data's get_value_string_by_key method
            Return: opcodes.even_day so the method goes through the correct if statement
        Call the method
        Cannot assert anything as the method doesn't return anything and it doesn't change any values
        """
        start = self.create_test_start_condition_for_1000_object()
        start._interval_ty = opcodes.calendar_interval
        start._ci = opcodes.even_day

        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(return_value=opcodes.even_day)
        start.data = mock_data

        start.verify_interval_on_cn()

    def test_verify_interval_on_cn_pass_2(self):
        """ Test Verify Interval On Controller Pass Case 2: Method runs through with day interval
        Create test object
        Create the variables that will be used in the method
        Mock: data's get_value_string_by_key method
            Return: 1 so the method goes through the correct if statement
        Call the method
        Cannot assert anything as the method doesn't return anything and it doesn't change any values
        """
        start = self.create_test_start_condition_for_1000_object()
        start._interval_ty = opcodes.day_interval
        start._di = 1

        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(return_value=1)
        start.data = mock_data

        start.verify_interval_on_cn()

    def test_verify_interval_on_cn_pass_3(self):
        """ Test Verify Interval On Controller Pass Case 3: Method runs through with week days
        Create test object
        Create the variables that will be used in the method
        Mock: data's get_value_string_by_key method
            Return: '1111111' so the method goes through the correct if statement
        Call the method
        Cannot assert anything as the method doesn't return anything and it doesn't change any values
        """
        start = self.create_test_start_condition_for_1000_object()
        start._interval_ty = opcodes.week_days
        start._wd = '1111111'

        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(return_value='1111111')
        start.data = mock_data

        start.verify_interval_on_cn()

    def test_verify_interval_on_cn_pass_4(self):
        """ Test Verify Interval On Controller Pass Case 4: Method runs through with semi month
        Create test object
        Create the variables that will be used in the method
        Mock: data's get_value_string_by_key method
            Return: '111111111111111111111111' so the method goes through the correct if statement
        Call the method
        Cannot assert anything as the method doesn't return anything and it doesn't change any values
        """
        start = self.create_test_start_condition_for_1000_object()
        start._interval_ty = opcodes.semi_month_interval
        start._smi = '111111111111111111111111'

        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(return_value='111111111111111111111111')
        start.data = mock_data

        start.verify_interval_on_cn()

    def test_verify_interval_on_cn_fail_1(self):
        """ Test Verify Interval On Controller Fail Case 1: Have the method run with an invalid interval type
        Create test object
        Create the variables that will be used in the method
        Mock: data's get_value_string_by_key method
            Return: 'foo' so the method goes through the correct if statement
        Call the method
        Create an expected message
        Call the method and assert that a ValueError will be raised
        Assert that the error message that was raised by the method is the same as what we expected
        """
        start = self.create_test_start_condition_for_1000_object()
        start._interval_ty = 'Invalid'

        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(return_value='foo')
        start.data = mock_data

        expected_message = "The interval type '{0} is not valid. The valid interval types are ({1}, {2}, {3}, {4}). ".format(
                start._interval_ty,
                opcodes.calendar_interval,
                opcodes.day_interval,
                opcodes.week_days,
                opcodes.semi_month_interval
            )

        with self.assertRaises(ValueError) as context:
            start.verify_interval_on_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_interval_on_cn_fail_2(self):
        """ Test Verify Interval On Controller Fail Case 2: Controller value doens't match object variable
        Create test object
        Create the variables that will be used in the method
        Mock: data's get_value_string_by_key method
            Return: opcodes.even_day so the method goes through the correct if statement
        Call the method
        Create an expected message
        Call the method and assert that a ValueError will be raised
        Assert that the error message that was raised by the method is the same as what we expected
        """
        start = self.create_test_start_condition_for_1000_object()
        start._interval_ty = opcodes.calendar_interval
        start._ci = 'invalid'

        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(return_value=opcodes.even_day)
        start.data = mock_data

        expected_message = "Unable to verify StartStopPause Condition: {0}'s interval. Received: {1}, " \
                           "Expected: {2}".format(
                               str(start._program_ad),      # {0}
                               str(opcodes.even_day),       # {1}
                               str(start._ci))              # {2}

        with self.assertRaises(ValueError) as context:
            start.verify_interval_on_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_date_time_enabled_on_cn_pass_1(self):
        """ Test Verify Date Time Enabled On Controller Pass Case 1: Method runs through without errors
        Create test object
        Create the variables that will be used in the method
        Mock: data's get_value_string_by_key method
            Return: opcodes.true so the method goes through the correct if statement
        Call the method
        Cannot assert anything as the method doesn't return anything and it doesn't change any values
        """
        start = self.create_test_start_condition_for_1000_object()
        start._dt = opcodes.true

        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(return_value=opcodes.true)
        start.data = mock_data

        start.verify_date_time_enabled_on_cn()

    def test_verify_date_time_enabled_on_cn_fail_1(self):
        """ Test Verify Date Time Enabled On Controller Fail Case 1: Controller value doesn't match object variable
        Create test object
        Create the variables that will be used in the method
        Mock: data's get_value_string_by_key method
            Return: opcodes.true so the method goes through the correct if statement
        Create the expected message
        Call the method and assert that a ValueError will be raised
        Assert that the error message that was raised by the method is the same as what we expected
        """
        start = self.create_test_start_condition_for_1000_object()
        start._dt = opcodes.false

        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(return_value=opcodes.true)
        start.data = mock_data

        expected_message = "Unable to verify StartStopPause({0}) Condition: {1}'s date time. Received: {2}, " \
                               "Expected: {3}".format(
                                   str(start._event_type),      # {0}
                                   str(start._program_ad),      # {1}
                                   str(opcodes.true),           # {2}
                                   str(start._dt))              # {3}

        with self.assertRaises(ValueError) as context:
            start.verify_date_time_enabled_on_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_start_times_on_cn_pass_1(self):
        """ Test Verify Start Times On Controller Pass Case 1: Method runs through without errors
        Create test object
        Create the variables that will be used in the method
        Mock: data's get_value_string_by_key method
            Return: [480, 600, 780] so the method goes through the correct if statement
        Call the method
        Cannot assert anything as the method doesn't return anything and it doesn't change any values
        """
        start = self.create_test_start_condition_for_1000_object()
        start._st = [480, 600, 780]

        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(return_value=[480, 600, 780])
        start.data = mock_data

        start.verify_start_times_on_cn()

    def test_verify_start_times_on_cn_fail_1(self):
        """ Test Verify Start Times On Controller Fail Case 1: Controller value doesn't match object variable
        Create test object
        Create the variables that will be used in the method
        Mock: data's get_value_string_by_key method
            Return: 'invalid' so the method goes through the correct if statement
        Create the expected message
        Call the method and assert that a ValueError will be raised
        Assert that the error message that was raised by the method is the same as what we expected
        """
        start = self.create_test_start_condition_for_1000_object()
        start_values = [480, 600, 780]
        start._st = start_values

        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(return_value='invalid')
        start.data = mock_data

        expected_message = "Unable to verify StartStopPause({0}) Condition: {1}'s start times. Received: {2}, " \
                           "Expected: {3}".format(
                               str(start._event_type),      # {0}
                               str(start._program_ad),      # {1}
                               str('invalid'),              # {2}
                               str(start._st))              # {3}

        with self.assertRaises(ValueError) as context:
            start.verify_start_times_on_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_calibration_cycle_on_cn_pass_1(self):
        """ Test Verify Calibration Cycle On Controller Pass Case 1: Method runs through without errors
        Create test object
        Create the variables that will be used in the method
        Mock: data's get_value_string_by_key method
            Return: [480, 600, 780] so the method goes through the correct if statement
        Call the method
        Cannot assert anything as the method doesn't return anything and it doesn't change any values
        """
        start = self.create_test_start_condition_for_1000_object()
        start._cc = 'foo'

        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(return_value='foo')
        start.data = mock_data

        start.verify_calibration_cycle_on_cn()

    def test_verify_calibration_cycle_on_cn_fail_1(self):
        """ Test Verify Calibration Cycle On Controller Fail Case 1: Controller value doesn't match object variable
        Create test object
        Create the variables that will be used in the method
        Mock: data's get_value_string_by_key method
            Return: 'invalid' so the method goes through the correct if statement
        Create the expected message
        Call the method and assert that a ValueError will be raised
        Assert that the error message that was raised by the method is the same as what we expected
        """
        start = self.create_test_start_condition_for_1000_object()
        start._cc = 'foo'

        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(return_value='invalid')
        start.data = mock_data

        expected_message = "Unable to verify StartStopPause({0}) Condition: {1}'s calibration cycle. Received: {2}, " \
                           "Expected: {3}".format(
                               str(start._event_type),      # {0}
                               str(start._program_ad),      # {1}
                               str('invalid'),              # {2}
                               str(start._cc))              # {3}

        with self.assertRaises(ValueError) as context:
            start.verify_calibration_cycle_on_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_who_i_am_pass_1(self):
        """ Test Verify Who I Am Pass Case 1: Verify the method runs through properly
        Create test object
        Assign the variables that are necessary to run through the method
        Mock: 'verify' methods because we test each one of them above and we don't want to test outside of this method
        Have nothing to compare against because the method doesn't return anything or change any variables
        """
        start = self.create_test_start_condition_for_1000_object()
        start._dt = 'foo'
        start._st = 'bar'
        start._cc = 'spam'
        start._interval_ty = 'alpha'
        start._device_serial = 'beta'
        start._mode = 'delta'
        start._threshold = 'omega'
        start._pause_time = 'gamma'
        start._si = 'xi'

        start.verify_date_time_enabled_on_cn = mock.MagicMock()
        start.verify_start_times_on_cn = mock.MagicMock()
        start.verify_calibration_cycle_on_cn = mock.MagicMock()
        start.verify_interval_on_cn = mock.MagicMock()
        start.verify_device_serial_on_cn = mock.MagicMock()
        start.verify_mode_on_cn = mock.MagicMock()
        start.verify_threshold_on_cn = mock.MagicMock()
        start.verify_pause_time_on_cn = mock.MagicMock()
        start.verify_stop_immediately_on_cn = mock.MagicMock()

        start.verify_who_i_am()
