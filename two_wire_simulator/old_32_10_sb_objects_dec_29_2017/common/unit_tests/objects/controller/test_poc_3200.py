__author__ = 'Brice "Ajo Grande" Garlick'

import unittest
import mock
import serial
import status_parser

import old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.poc as poc
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.mv import MasterValve
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.fm import FlowMeter
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ms import MoistureSensor
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.sw import EventSwitch
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import *


class TestPOC3200Object(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.309759

    Master Valve Starting lat & lng:
    latitude = 43.609768
    longitude = -116.310359
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        Devices.ser = self.mock_ser
        poc.POC.ser = self.mock_ser
        Mainline.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        master_valves[3] = MasterValve("MVD0001", 3)

        flow_meters[5] = FlowMeter("FMD0001", 5)

        mainlines32[1] = Mainline(1)

        moisture_sensors[4] = MoistureSensor("MSD0001", 4)

        event_switches[2] = EventSwitch("ESD0001", 2)

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # when these objects are created in this test, they carry over to any test after this one because they are
        # just pointers and not test variables specific to just THIS module.
        del master_valves[3]
        del flow_meters[5]
        del mainlines32[1]
        del moisture_sensors[4]
        del event_switches[2]

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_poc_3200_object(self):
        """ Creates a new Master Valve object for testing purposes """
        poc = POC3200(_ad=1)

        return poc

    #################################
    def test_send_programming_to_cn_pass1(self):
        """ Send Programming To Controller Pass Case 1: Correct Command Sent """
        expected_command = "SET," \
                           "PC=1," \
                           "DS=Test POC 1," \
                           "EN=TR," \
                           "FL=0," \
                           "HF=0," \
                           "HS=FA," \
                           "UF=0.0," \
                           "US=FA," \
                           "PR=2," \
                           "WS=FA," \
                           "WR=FA," \
                           "ML=1," \
                           "ME=25," \
                           "MN=FA," \
                           "EW=5," \
                           "SE=CL," \
                           "SN=FA"
        poc = self.create_test_poc_3200_object()

        poc.send_programming_to_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_to_cn_fail1(self):
        """ Send Programming To Controller Fail Case 1: Failed communication with controller """
        expected_command = "SET," \
                           "PC=1," \
                           "DS=Test POC 1," \
                           "EN=TR," \
                           "FL=0," \
                           "HF=0," \
                           "HS=FA," \
                           "UF=0.0," \
                           "US=FA," \
                           "PR=2," \
                           "WS=FA," \
                           "WR=FA," \
                           "ML=1," \
                           "ME=25," \
                           "MN=FA," \
                           "EW=5," \
                           "SE=CL," \
                           "SN=FA"
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            poc = self.create_test_poc_3200_object()
            poc.send_programming_to_cn()
        e_msg = "Exception occurred trying to 'SET' POC 1's 'Values' to: '{0}'\n -> ".format(expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_priority_on_cn_pass1(self):
        """ Set Priority On Controller Pass Case 1: Setting new _pr value = 6.0, check that object
        variable _pr is set """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        poc.set_priority_on_cn(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = poc.pr
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_priority_on_cn_pass2(self):
        """ Set Priority On Controller Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.priority, str(new_value))
        poc.set_priority_on_cn(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_priority_on_cn_fail1(self):
        """ Set Priority On Controller Fail Case 1: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_priority_on_cn(new_value)
        e_msg = "Exception occurred trying to set POC {0}'s 'Priority' to: '{1}' -> ".format(
                str(poc.ad), str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_monthly_water_budget_on_cn_pass1(self):
        """ Set Monthly Water Budget On Controller Pass Case 1: Setting new _pr value = 6.0, check that object
        variable _wb is set """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        poc.set_monthly_water_budget_on_cn(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = poc.wb
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_monthly_water_budget_on_cn_pass2(self):
        """ Set Monthly Water Budget On Controller Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.monthly_water_budget,
                                                      str(new_value))
        poc.set_monthly_water_budget_on_cn(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_monthly_water_budget_on_cn_fail1(self):
        """ Set Monthly Water Budget On Controller Fail Case 1: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_monthly_water_budget_on_cn(new_value)
        e_msg = "Exception occurred trying to set POC {0}'s 'Monthly Water Budget' to: '{1}' -> ".format(
                str(poc.ad), str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_shutdown_on_over_budget_state_on_cn_pass1(self):
        """ Set Shutdown on Over Budget State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.shutdown_on_over_budget,
                                                      opcodes.true)
        poc = self.create_test_poc_3200_object()
        poc.set_shutdown_on_over_budget_state_on_cn("TR")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_shutdown_on_over_budget_state_on_cn_pass2(self):
        """ Set Shutdown on Over Budget State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.shutdown_on_over_budget,
                                                      opcodes.false)
        poc = self.create_test_poc_3200_object()
        poc.set_shutdown_on_over_budget_state_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_shutdown_on_over_budget_state_on_cn_fail1(self):
        """ Set Shutdown on Over Budget State On Controller Fail Case 1: Invalid State Argument """
        poc = self.create_test_poc_3200_object()
        with self.assertRaises(ValueError) as context:
            poc.set_shutdown_on_over_budget_state_on_cn("Foo")
        e_msg = "Invalid 'Shutdown On Over Budget' state for POC 1 to set: Foo. Valid states are 'TR' or 'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_shutdown_on_over_budget_state_on_cn_fail2(self):
        """ Set Shutdown on Over Budget State On Controller Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_shutdown_on_over_budget_state_on_cn("FA")
        e_msg = "Exception occurred trying to set POC 1's 'Shutdown On Over Budget' to: 'FA' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_water_rationing_enable_state_on_cn_pass1(self):
        """ Set Water Rationing Enable State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.water_rationing_enable,
                                                      opcodes.true)
        poc = self.create_test_poc_3200_object()
        poc.set_water_rationing_enable_state_on_cn("TR")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_rationing_enable_state_on_cn_pass2(self):
        """ Set Water Rationing Enable State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.water_rationing_enable,
                                                      opcodes.false)
        poc = self.create_test_poc_3200_object()
        poc.set_water_rationing_enable_state_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_rationing_enable_state_on_cn_fail1(self):
        """ Set Water Rationing Enable State On Controller Fail Case 1: Invalid State Argument """
        poc = self.create_test_poc_3200_object()
        with self.assertRaises(ValueError) as context:
            poc.set_water_rationing_enable_state_on_cn("Foo")
        e_msg = "Invalid 'Water Rationing Enable' state for POC 1 to set: Foo. Valid states are 'TR' or 'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_water_rationing_enable_state_on_cn_fail2(self):
        """ Set Water Rationing Enable State On Controller Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_water_rationing_enable_state_on_cn("FA")
        e_msg = "Exception occurred trying to set POC 1's 'Water Rationing Enable State' to: 'FA' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_mainline_on_cn_pass1(self):
        """ Set Mainline On Controller Pass Case 1: Passed in address of a present Mainline """
        poc = self.create_test_poc_3200_object()

        expected_value = 1
        poc.set_mainline_on_cn(expected_value)

        this_obj = poc.mainline_objects[1]
        actual_value = this_obj.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_mainline_on_cn_pass2(self):
        """ Set Mainline On Controller Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        this_obj = poc.mainline_objects[1]
        ad_value = this_obj.ad
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.mainline,
                                                      str(poc.ml))
        poc.set_mainline_on_cn(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_mainline_on_cn_fail1(self):
        """ Set Mainline On Controller Fail Case 1: Pass an address of Mainline that is below available addresses """
        poc = self.create_test_poc_3200_object()

        new_ad_value = 0
        with self.assertRaises(Exception) as context:
            poc.set_mainline_on_cn(new_ad_value)
        expected_message = "Invalid 'Mainline' address entered for POC {0}. Available mainlines are 1 - 8, " \
                           "user entered: {1}".format(str(poc.ad), str(new_ad_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_mainline_on_cn_fail2(self):
        """ Set Mainline On Controller Fail Case 2: Pass an address of Mainline that is above available addresses """
        poc = self.create_test_poc_3200_object()

        new_ad_value = 9
        with self.assertRaises(Exception) as context:
            poc.set_mainline_on_cn(new_ad_value)
        expected_message = "Invalid 'Mainline' address entered for POC {0}. Available mainlines are 1 - 8, " \
                           "user entered: {1}".format(str(poc.ad), str(new_ad_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_mainline_on_cn_fail3(self):
        """ Set Mainline On Controller Fail Case 3: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        this_obj = poc.mainline_objects[1]
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_mainline_on_cn(ad_value)
        e_msg = "Exception occurred trying to set POC {0}'s 'Mainline' to: '{1}' " \
                "-> ".format(str(poc.ad), str(this_obj.ad))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_moisture_sensor_on_cn_pass1(self):
        """ Set Moisture Sensor On Controller Pass Case 1: Passed in address of a present Mainline """
        poc = self.create_test_poc_3200_object()

        expected_value = 4
        poc.set_moisture_sensor_on_cn(expected_value)

        this_obj = poc.moisture_sensor_objects[4]
        actual_value = this_obj.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_moisture_sensor_on_cn_pass2(self):
        """ Set Moisture Sensor On Controller Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        this_obj = poc.moisture_sensor_objects[4]
        sn_value = this_obj.sn
        ad_value = this_obj.ad
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.moisture_sensor,
                                                      str(sn_value))

        poc.set_moisture_sensor_on_cn(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_moisture_sensor_on_cn_fail1(self):
        """ Set Moisture Sensor On Controller Fail Case 1: Pass an address of Moisture Sensor that is not present """
        poc = self.create_test_poc_3200_object()

        new_ad_value = 2
        with self.assertRaises(Exception) as context:
            poc.set_moisture_sensor_on_cn(new_ad_value)
        expected_message = "Invalid Moisture Sensor address. Verify address exists in object json configuration and/or" \
                           " in current test. Received address: {0}, available addresses: {1}"\
            .format(str(new_ad_value), str(poc.moisture_sensor_objects.keys()))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_moisture_sensor_on_cn_fail2(self):
        """ Set Moisture Sensor On Controller Fail Case 2: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        this_obj = poc.moisture_sensor_objects[4]
        sn_value = this_obj.sn
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_moisture_sensor_on_cn(ad_value)
        e_msg = "Exception occurred trying to set POC {0}'s 'Moisture Sensor' to: '{1}({2})' " \
                "-> ".format(str(poc.ad), str(this_obj.ad), sn_value)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_moisture_empty_limit_on_cn_pass1(self):
        """ Set Moisture Empty Limit On Controller Pass Case 1: Setting new _me value = 6.0, check that object
        variable _me is set """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        poc.set_moisture_empty_limit_on_cn(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = poc.me
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_moisture_empty_limit_on_cn_pass2(self):
        """ Set Moisture Empty Limit On Controller Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.moisture_empty_limit,
                                                      str(new_value))
        poc.set_moisture_empty_limit_on_cn(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_moisture_empty_limit_on_cn_fail1(self):
        """ Set Moisture Empty Limit On Controller Fail Case 1: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_moisture_empty_limit_on_cn(new_value)
        e_msg = "Exception occurred trying to set POC {0}'s 'Moisture Empty Limit' to: '{1}' -> ".format(
                str(poc.ad), str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_moisture_sensor_empty_enable_state_on_cn_pass1(self):
        """ Set Moisture Sensor Empty Enable State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.moisture_sensor_empty_enable,
                                                      opcodes.true)
        poc = self.create_test_poc_3200_object()
        poc.set_moisture_sensor_empty_enable_state_on_cn("TR")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_moisture_sensor_empty_enable_state_on_cn_pass2(self):
        """ Set Moisture Sensor Empty Enable State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.moisture_sensor_empty_enable,
                                                      opcodes.false)
        poc = self.create_test_poc_3200_object()
        poc.set_moisture_sensor_empty_enable_state_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_moisture_sensor_empty_enable_state_on_cn_fail1(self):
        """ Set Moisture Sensor Empty Enable State On Controller Fail Case 1: Invalid State Argument """
        poc = self.create_test_poc_3200_object()
        with self.assertRaises(ValueError) as context:
            poc.set_moisture_sensor_empty_enable_state_on_cn("Foo")
        e_msg = "Invalid 'Moisture Sensor Empty' state for POC 1 to set: Foo. Valid states are 'TR' or 'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_moisture_sensor_empty_enable_state_on_cn_fail2(self):
        """ Set Moisture Sensor Empty Enable State On Controller Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_moisture_sensor_empty_enable_state_on_cn("FA")
        e_msg = "Exception occurred trying to set POC 1's 'Moisture Sensor Empty Enable State' to: 'FA' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_empty_wait_time_on_cn_pass1(self):
        """ Set Empty Wait Time On Controller Pass Case 1: Setting new _me value = 6.0, check that object
        variable _me is set """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        poc.set_empty_wait_time_on_cn(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = poc.ew
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_empty_wait_time_on_cn_pass2(self):
        """ Set Empty Wait Time On Controller Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.empty_wait_time,
                                                      str(new_value))
        poc.set_empty_wait_time_on_cn(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_empty_wait_time_on_cn_fail1(self):
        """ Set Empty Wait Time On Controller Fail Case 1: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_empty_wait_time_on_cn(new_value)
        e_msg = "Exception occurred trying to set POC {0}'s 'Empty Wait Time' to: '{1}' -> ".format(
                str(poc.ad), str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_event_switch_empty_on_cn_pass1(self):
        """ Set Event Switch Empty On Controller Pass Case 1: Passed in address of a present Mainline """
        poc = self.create_test_poc_3200_object()

        expected_value = 2
        poc.set_event_switch_empty_on_cn(expected_value)

        this_obj = poc.event_switch_objects[2]
        actual_value = this_obj.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_event_switch_empty_on_cn_pass2(self):
        """ Set Event Switch Empty On Controller Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        this_obj = poc.event_switch_objects[2]
        sn_value = this_obj.sn
        ad_value = this_obj.ad
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.event_switch,
                                                      str(sn_value))

        poc.set_event_switch_empty_on_cn(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_event_switch_empty_on_cn_fail1(self):
        """ Set Event Switch Empty On Controller Fail Case 1: Pass an address of Moisture Sensor that is not present """
        poc = self.create_test_poc_3200_object()

        new_ad_value = 3
        with self.assertRaises(Exception) as context:
            poc.set_event_switch_empty_on_cn(new_ad_value)
        expected_message = "Invalid Event Switch address. Verify address exists in object json configuration and/or" \
                           " in current test. Received address: {0}, available addresses: {1}"\
            .format(str(new_ad_value), str(poc.event_switch_objects.keys()))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_event_switch_empty_on_cn_fail2(self):
        """ Set Event Switch Empty On Controller Fail Case 2: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        this_obj = poc.event_switch_objects[2]
        sn_value = this_obj.sn
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_event_switch_empty_on_cn(ad_value)
        e_msg = "Exception occurred trying to set POC {0}'s 'Event Switch Empty SN' to: '{2}' ({1}) " \
                "-> ".format(str(poc.ad), str(this_obj.ad), sn_value)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_switch_empty_condition_state_on_cn_pass1(self):
        """ Set Switch Empty Condition On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.switch_empty_condition,
                                                      opcodes.open)
        poc = self.create_test_poc_3200_object()
        poc.set_switch_empty_condition_state_on_cn("OP")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_switch_empty_condition_state_on_cn_pass2(self):
        """ Set Switch Empty Condition On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.switch_empty_condition,
                                                      opcodes.closed)
        poc = self.create_test_poc_3200_object()
        poc.set_switch_empty_condition_state_on_cn("CL")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_switch_empty_condition_state_on_cn_fail1(self):
        """ Set Switch Empty Condition On Controller Fail Case 1: Invalid State Argument """
        poc = self.create_test_poc_3200_object()
        with self.assertRaises(ValueError) as context:
            poc.set_switch_empty_condition_state_on_cn("Foo")
        e_msg = "Invalid 'Event Switch Condition' state for POC 1 to set: Foo. Valid states are 'OP' or 'CL'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_switch_empty_condition_state_on_cn_fail2(self):
        """ Set Switch Empty Condition On Controller Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_switch_empty_condition_state_on_cn("CL")
        e_msg = "Exception occurred trying to set POC 1's 'Event Switch Condition State' to: 'CL' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_switch_empty_enable_state_on_cn_pass1(self):
        """ Set Switch Empty Enable State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.switch_empty_enable,
                                                      opcodes.true)
        poc = self.create_test_poc_3200_object()
        poc.set_switch_empty_enable_state_on_cn("TR")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_switch_empty_enable_state_on_cn_pass2(self):
        """ Set Switch Empty Enable State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.switch_empty_enable,
                                                      opcodes.false)
        poc = self.create_test_poc_3200_object()
        poc.set_switch_empty_enable_state_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_switch_empty_enable_state_on_cn_fail1(self):
        """ Set Switch Empty Enable State On Controller Fail Case 1: Invalid State Argument """
        poc = self.create_test_poc_3200_object()
        with self.assertRaises(ValueError) as context:
            poc.set_switch_empty_enable_state_on_cn("Foo")
        e_msg = "Invalid 'Event Switch Empty' state for POC 1 to set: Foo. Valid states are 'TR' or 'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_switch_empty_enable_state_on_cn_fail2(self):
        """ Set Switch Empty Enable State On Controller Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_switch_empty_enable_state_on_cn("FA")
        e_msg = "Exception occurred trying to set POC 1's 'Event Switch Empty Enable State' to: 'FA' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_priority_on_cn_pass1(self):
        """ Verify Priority On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.pr = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.priority))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_priority_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_priority_on_cn_fail1(self):
        """ Verify Priority On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.pr """
        poc = self.create_test_poc_3200_object()
        poc.pr = 6
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.priority))
        poc.data = mock_data

        expected_message = "Unable to verify POC 1's 'Priority'. Received: 5, Expected: 6"
        try:
            poc.verify_priority_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_monthly_water_budget_on_cn_pass1(self):
        """ Verify Monthly Water Budget On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.wb = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.monthly_water_budget))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_monthly_water_budget_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_monthly_water_budget_on_cn_fail1(self):
        """ Verify Monthly Water Budget On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.wb """
        poc = self.create_test_poc_3200_object()
        poc.wb = 6
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.monthly_water_budget))
        poc.data = mock_data

        expected_message = "Unable to verify POC 1's 'Monthly Water Budget'. Received: 5, Expected: 6"
        try:
            poc.verify_monthly_water_budget_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_shutdown_on_over_budget_on_cn_pass1(self):
        """ Verify Shutdown on Over Budget On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.ws = "FA"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=FA".format(opcodes.shutdown_on_over_budget))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_shutdown_on_over_budget_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_shutdown_on_over_budget_on_cn_fail1(self):
        """ Verify Shutdown on Over Budget On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.ws """
        poc = self.create_test_poc_3200_object()
        poc.ws = "TR"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Tr".format(opcodes.shutdown_on_over_budget))
        poc.data = mock_data

        expected_message = "Unable to verify POC 1's 'Shutdown On Over Budget State'. Received: Tr, Expected: TR"
        try:
            poc.verify_shutdown_on_over_budget_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_water_rationing_enable_state_on_cn_pass1(self):
        """ Verify Water Rationing Enable on Over Budget On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.wr = "FA"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=FA".format(opcodes.water_rationing_enable))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_water_rationing_enable_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_water_rationing_enable_state_on_cn_fail1(self):
        """ Verify Water Rationing Enable On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.wr """
        poc = self.create_test_poc_3200_object()
        poc.wr = "TR"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Tr".format(opcodes.water_rationing_enable))
        poc.data = mock_data

        expected_message = "Unable to verify POC 1's 'Water Rationing Enable State'. Received: Tr, Expected: TR"
        try:
            poc.verify_water_rationing_enable_state_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_mainline_on_cn_pass1(self):
        """ Verify Mainline On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=1".format(opcodes.mainline))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_mainline_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_mainline_on_cn_fail1(self):
        """ Verify Mainline On Controller Fail Case 1: Value on controller does not match what is
        stored in serial number for the mainline """
        poc = self.create_test_poc_3200_object()
        this_obj = poc.mainline_objects[1]
        this_obj.ad = 1
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=2".format(opcodes.mainline))
        poc.data = mock_data

        expected_message = "Unable to verify POC 1's 'Mainline'. Received: 2, Expected: 1"
        try:
            poc.verify_mainline_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_moisture_sensor_on_cn_pass1(self):
        """ Verify Moisture Sensor On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.ms = 4
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=MSD0001".format(opcodes.moisture_sensor))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_moisture_sensor_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_moisture_sensor_on_cn_fail1(self):
        """ Verify Moisture Sensor On Controller Fail Case 1: Value on controller does not match what is
        stored in serial number for the Moisture Sensor """
        poc = self.create_test_poc_3200_object()
        poc.ms = 4
        this_obj = poc.moisture_sensor_objects[4]
        this_obj.sn = "MSD0005"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=MSD0001".format(opcodes.moisture_sensor))
        poc.data = mock_data

        expected_message = "Unable to verify POC 1's 'Moisture Sensor'. Received: MSD0001, Expected: MSD0005"
        try:
            poc.verify_moisture_sensor_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_moisture_empty_limit_on_cn_pass1(self):
        """ Verify Moisture Empty Limit On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.me = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.moisture_empty_limit))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_moisture_empty_limit_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_moisture_empty_limit_on_cn_fail1(self):
        """ Verify Moisture Empty Limit On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.me """
        poc = self.create_test_poc_3200_object()
        poc.me = 6.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.moisture_empty_limit))
        poc.data = mock_data

        expected_message = "Unable to verify POC 1's 'Moisture Empty Limit'. Received: 5.0, Expected: 6.0"
        try:
            poc.verify_moisture_empty_limit_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_moisture_empty_enable_state_on_cn_pass1(self):
        """ Verify Moisture Empty Enable on Over Budget On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.mn = "FA"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=FA".format(opcodes.moisture_sensor_empty_enable))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_moisture_empty_enable_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_moisture_empty_enable_state_on_cn_fail1(self):
        """ Verify Moisture Empty Enable On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.mn """
        poc = self.create_test_poc_3200_object()
        poc.mn = "TR"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Tr".format(opcodes.moisture_sensor_empty_enable))
        poc.data = mock_data

        expected_message = "Unable to verify POC 1's 'Moisture Empty Enable State'. Received: Tr, Expected: TR"
        try:
            poc.verify_moisture_empty_enable_state_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_empty_wait_time_on_cn_pass1(self):
        """ Verify Empty Wait Time On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.ew = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.empty_wait_time))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_empty_wait_time_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_empty_wait_time_on_cn_fail1(self):
        """ Verify Empty Wait Time On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.ew """
        poc = self.create_test_poc_3200_object()
        poc.ew = 6
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5".format(opcodes.empty_wait_time))
        poc.data = mock_data

        expected_message = "Unable to verify POC 1's 'Empty Wait Time'. Received: 5, Expected: 6"
        try:
            poc.verify_empty_wait_time_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_event_switch_on_cn_pass1(self):
        """ Verify Event Switch On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.sw = 2
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=ESD0001".format(opcodes.event_switch))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_event_switch_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg == expected_message:
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_event_switch_on_cn_fail1(self):
        """ Verify Event Switch On Controller Fail Case 1: Value on controller does not match what is
        stored in serial number for the Event Switch """
        poc = self.create_test_poc_3200_object()
        poc.sw = 2
        this_obj = poc.event_switch_objects[2]
        this_obj.sn = "ESD0005"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=ESD0001".format(opcodes.event_switch))
        poc.data = mock_data

        expected_message = "Unable to verify POC 1's 'Event Switch'. Received: ESD0001, Expected: ESD0005"
        try:
            poc.verify_event_switch_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg == expected_message:
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_switch_empty_condition_on_cn_pass1(self):
        """ Verify Switch Empty Condition on Over Budget On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.se = "OP"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=OP".format(opcodes.switch_empty_condition))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_switch_empty_condition_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_switch_empty_condition_on_cn_fail1(self):
        """ Verify Switch Empty Condition On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.se """
        poc = self.create_test_poc_3200_object()
        poc.se = "CL"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Cl".format(opcodes.switch_empty_condition))
        poc.data = mock_data

        expected_message = "Unable to verify POC 1's 'Switch Empty Condition'. Received: Cl, Expected: CL"
        try:
            poc.verify_switch_empty_condition_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_switch_empty_enable_on_cn_pass1(self):
        """ Verify Switch Empty Enable on Over Budget On Controller Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.sn = "FA"
        test_pass = False

        mock_data = status_parser.KeyValues("{0}=FA".format(opcodes.switch_empty_enable))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_switch_empty_enable_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_switch_empty_enable_on_cn_fail1(self):
        """ Verify Switch Empty Enable On Controller Fail Case 1: Value on controller does not match what is
        stored in poc.sn """
        poc = self.create_test_poc_3200_object()
        poc.sn = "TR"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}=Tr".format(opcodes.switch_empty_enable))
        poc.data = mock_data

        expected_message = "Unable to verify POC 1's 'Switch Empty Enable State'. Received: Tr, Expected: TR"
        try:
            poc.verify_switch_empty_enable_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_who_i_am_pass1(self):
        poc = self.create_test_poc_3200_object()

        mock_data = status_parser.KeyValues("PC=1,"
                                            "DS=Test POC 1,"
                                            "EN=TR,"
                                            "FL=0,"
                                            "HF=0,"
                                            "HS=FA,"
                                            "UF=0,"
                                            "US=FA,"
                                            "PR=2,"
                                            "WB=0,"
                                            "WS=FA,"
                                            "WR=FA,"
                                            "ML=1,"
                                            "ME=25,"
                                            "MN=FA,"
                                            "EW=5,"
                                            "SE=CL,"
                                            "SN=FA,"
                                            "MV=3,"
                                            "FM=5,"
                                            "ML=1,"
                                            "MS=4,"
                                            "SW=2")
        poc.data = mock_data
        poc.get_data = mock.MagicMock(sideeffect=None)
        poc.verify_who_i_am()

    if __name__ == "__main__":
        unittest.main()