import unittest
import mock
import serial
import status_parser

from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
# you have to set the lat and long after the devices class is called so that they don't get skipped over
Devices.controller_lat = float(43.609768)
Devices.controller_long = float(-116.310569)

from old_32_10_sb_objects_dec_29_2017.common.epa_package import equations
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zn import Zone

__author__ = 'Brice "Ajo Grande" Garlick'


class TestZoneObject(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.310569

    Zone Starting lat & lng:
    latitude = 43.609768
    longitude = -116.310869
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        self.zone_list = []

        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        Devices.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_zone_object(self, _diffserial=None, _diffaddress=None):
        """
        Creates a new zone object for use in a unit test
        """
        # if _diffSerial is None and _diffAddress is None:
        if _diffserial is not None:
            zone = Zone(_serial="TSD0001", _address=_diffaddress)
        elif _diffaddress is not None:
            zone = Zone(_serial=_diffserial, _address=1)
        else:
            zone = Zone(_serial="TSD0001", _address=1)
        return zone

    def test_zone_lat_long_floats(self):
        """ """
        count = 10
        for address in range(1, count):
            self.zone_list.append(self.create_test_zone_object(_diffserial=address, _diffaddress=address))

        print self.zone_list

    #################################
    def test_set_default_values_pass1(self):
        """ Set Default Values On 1000 Controller Pass Case 1: Correct Command Sent """
        Devices.controller_type = "10"
        expected_command = "SET," \
                           "ZN=1," \
                           "EN=TR," \
                           "DS=TSD0001 Test Zone 1," \
                           "LA=43.609773," \
                           "LG=-116.310264," \
                           "DF=0.0," \
                           "MR=0," \
                           "MV=1," \
                           "FF=FA," \
                           "KC=0.0," \
                           "PR=0.0," \
                           "EF=0.0," \
                           "RZ=0.0," \
                           "VA=0.23," \
                           "VV=28.7," \
                           "VT=1.7"

        zone = Zone(_serial="TSD0001", _address=1)
        zone.set_default_values()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_pass2(self):
        """ Set Default Values On 3200 Controller Pass Case 2: Correct Command Sent """
        Devices.controller_type = "32"
        expected_command = "SET," \
                           "ZN=1," \
                           "EN=TR," \
                           "DS=TSD0001 Test Zone 1," \
                           "LA=43.609773," \
                           "LG=-116.310264," \
                           "DF=0.0," \
                           "KC=0.0," \
                           "PR=0.0," \
                           "EF=0.0," \
                           "RZ=0.0," \
                           "VA=0.23," \
                           "VV=28.7," \
                           "VT=1.7"

        zone = Zone(_serial="TSD0001", _address=1)
        zone.set_default_values()
        self.mock_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values On 1000 Controller Fail Case 1: Handle Exception Raised From Serial Object """
        Devices.controller_type = "10"
        expected_command = "SET," \
                           "ZN=1," \
                           "EN=TR," \
                           "DS=TSD0001 Test Zone 1," \
                           "LA=43.609773," \
                           "LG=-116.310264," \
                           "DF=0.0," \
                           "MR=0," \
                           "MV=1," \
                           "FF=FA," \
                           "KC=0.0," \
                           "PR=0.0," \
                           "EF=0.0," \
                           "RZ=0.0," \
                           "VA=0.23," \
                           "VV=28.7," \
                           "VT=1.7"

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            zone = Zone(_serial="TSD0001", _address=1)
            zone.set_default_values()
        e_msg = "Exception occurred trying to set Zone {0}'s 'Default values' to: '{1}'".format(
            "1", expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_enable_state_pass1(self):
        """ Set Enable State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,ZN=1,EN=TR"
        zone = Zone(_serial="TSD0001", _address=1)
        zone.set_enable_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_state_pass2(self):
        """ Set Enable State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,ZN=1,EN=FA"
        zone = Zone(_serial="TSD0001", _address=1)
        zone.set_enable_state_on_cn(_state="FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_state_fail1(self):
        """ Set Enable State On Controller Fail Case 1: Invalid State Argument """
        zone = Zone(_serial="TSD0001", _address=1)
        with self.assertRaises(ValueError) as context:
            zone.set_enable_state_on_cn(_state="Foo")
        e_msg = "Invalid enabled state entered for Zone 1. Received: Foo, Expected: 'TR' or 'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_enable_state_fail2(self):
        """ Set Enable State On Controller Fail Case 2: Failed Communication With Controller """
        zone = Zone(_serial="TSD0001", _address=1)

        # Set the send_and_wait_for_reply method to raise an 'Exception' after zone instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            zone.set_enable_state_on_cn(_state="FA")
        e_msg = "Exception occurred trying to set Enabled State for Zone 1"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_design_flow_on_cn_pass1(self):
        """ Set Design Flow On Controller Pass Case 1: Using Default _df value """
        zone = self.create_test_zone_object()

        # Expected value is the _df value set at object Zone object creation
        expected_value = zone.df
        zone.set_design_flow_on_cn()

        # _df value is set during this method and should equal the original value
        actual_value = zone.df
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_design_flow_on_cn_pass2(self):
        """ Set Design Flow On Controller Pass Case 2: Setting new _df value = 6 """
        zone = self.create_test_zone_object()

        # Expected _df value is 6
        expected_value = 6
        zone.set_design_flow_on_cn(_df=expected_value)

        # _df value is set during this method and should equal the value passed into the method
        actual_value = zone.df
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_design_flow_on_cn_pass3(self):
        """ Set Design Flow On Controller Pass Case 3: Command with correct values sent to controller """
        zone = self.create_test_zone_object()

        df_value = str(zone.df)
        expected_command = "SET,ZN=1,DF=" + df_value
        zone.set_design_flow_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_design_flow_on_cn_fail1(self):
        """ Set Design Flow On Controller Fail Case 1: Pass String value as argument """
        zone = self.create_test_zone_object()

        new_df_value = "b"
        with self.assertRaises(Exception) as context:
            zone.set_design_flow_on_cn(new_df_value)
        expected_message = "Failed trying to set ZN 1 design flow. Invalid design flow type, Received type: {0}, " \
                           "Expected: int or float.".format(type(new_df_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_design_flow_on_cn_fail2(self):
        """ Set Design Flow On Controller Fail Case 2: Failed communication with controller """
        zone = self.create_test_zone_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            zone.set_design_flow_on_cn()
        e_msg = "Exception occurred trying to set Zone 1's 'DF' to: '{0}'".format(str(zone.df))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_solenoid_current_on_cn_pass1(self):
        """ Set Solenoid Current On Controller Pass Case 1: Using Default _va value """
        zone = self.create_test_zone_object()

        # Expected value is the _va value set at object Zone object creation
        expected_value = zone.va
        zone.set_solenoid_current_on_cn()

        # _va value is set during this method and should equal the original value
        actual_value = zone.va
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_solenoid_current_on_cn_pass2(self):
        """ Set Solenoid Current On Controller Pass Case 2: Using 6 as passed in value for _va """
        zone = self.create_test_zone_object()

        # Expected value is the _va value set at object Zone object creation
        expected_value = 6
        zone.set_solenoid_current_on_cn(expected_value)

        # _va value is set during this method and should equal the original value
        actual_value = zone.va
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_solenoid_current_on_cn_pass3(self):
        """ Set Solenoid Current On Controller Pass Case 3: Command with correct values sent to controller """
        zone = self.create_test_zone_object()

        va_value = str(zone.va)
        expected_command = "SET,ZN=1,VA=" + va_value
        zone.set_solenoid_current_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_solenoid_current_on_cn_fail1(self):
        """ Set Solenoid Current On Controller Fail Case 1: Pass String value as argument """
        zone = self.create_test_zone_object()

        new_df_value = "b"
        with self.assertRaises(Exception) as context:
            zone.set_solenoid_current_on_cn(new_df_value)
        expected_message = "Failed trying to set ZN 1's solenoid current. Invalid type passed in, expected int or " \
                           "float. Received type: {0}".format(type(new_df_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_solenoid_current_flow_on_cn_fail2(self):
        """ Set Solenoid Current On Controller Fail Case 2: Failed communication with controller """
        zone = self.create_test_zone_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            zone.set_solenoid_current_on_cn()
        e_msg = "Exception occurred trying to set Zone 1's 'Solenoid Current' to: '{0}'".format(zone.va)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_solenoid_voltage_on_cn_pass1(self):
        """ Set Solenoid Voltage On Controller Pass Case 1: Using Default _vv value """
        zone = self.create_test_zone_object()

        # Expected value is the _va value set at object Zone object creation
        expected_value = zone.vv
        zone.set_solenoid_voltage_on_cn()

        # _va value is set during this method and should equal the original value
        actual_value = zone.vv
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_solenoid_voltage_on_cn_pass2(self):
        """ Set Solenoid Voltage On Controller Pass Case 2: Using 6 as passed in value for _vv """
        zone = self.create_test_zone_object()

        # Expected value is the _va value set at object Zone object creation
        expected_value = 6
        zone.set_solenoid_voltage_on_cn(expected_value)

        # _va value is set during this method and should equal the original value
        actual_value = zone.vv
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_solenoid_voltage_on_cn_pass3(self):
        """ Set Solenoid Voltage On Controller Pass Case 3: Command with correct values sent to controller """
        zone = self.create_test_zone_object()

        vv_value = str(zone.vv)
        expected_command = "SET,ZN=1,VV=" + vv_value
        zone.set_solenoid_voltage_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_solenoid_voltage_on_cn_fail1(self):
        """ Set Solenoid Voltage On Controller Fail Case 1: Pass String value as argument """
        zone = self.create_test_zone_object()

        new_vv_value = "b"
        with self.assertRaises(Exception) as context:
            zone.set_solenoid_voltage_on_cn(new_vv_value)
        expected_message = "Failed trying to set ZN 1's solenoid voltage. Invalid type passed in, expected int or " \
                           "float. Received type: {0}".format(type(new_vv_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_solenoid_voltage_flow_on_cn_fail2(self):
        """ Set Solenoid Voltage On Controller Fail Case 2: Failed communication with controller """
        zone = self.create_test_zone_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            zone.set_solenoid_voltage_on_cn()
        e_msg = "Exception occurred trying to set Zone 1's 'Solenoid Voltage' to: '{0}'".format(zone.vv)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass1(self):
        """ Set Two Wore Drop Value On Controller Pass Case 1: Using Default _vt value """
        zone = self.create_test_zone_object()

        # Expected value is the _va value set at object Zone object creation
        expected_value = zone.vt
        zone.set_two_wire_drop_value_on_cn()

        # _va value is set during this method and should equal the original value
        actual_value = zone.vt
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass2(self):
        """ Set Two Wore Drop Value On Controller Pass Case 2: Using 6 as passed in value for _vt """
        zone = self.create_test_zone_object()

        # Expected value is the _va value set at object Zone object creation
        expected_value = 6
        zone.set_two_wire_drop_value_on_cn(expected_value)

        # _va value is set during this method and should equal the original value
        actual_value = zone.vt
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass3(self):
        """ Set Two Wore Drop Value Controller Pass Case 3: Command with correct values sent to controller """
        zone = self.create_test_zone_object()

        vt_value = str(zone.vt)
        expected_command = "SET,ZN=1,VT=" + vt_value
        zone.set_two_wire_drop_value_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_two_wire_drop_value_on_cn_fail1(self):
        """ Set Two Wore Drop Value On Controller Fail Case 1: Pass String value as argument """
        zone = self.create_test_zone_object()

        new_vt_value = "b"
        with self.assertRaises(Exception) as context:
            zone.set_two_wire_drop_value_on_cn(new_vt_value)
        expected_message = "Failed trying to set ZN 1's two wire drop value. Invalid type passed in, expected int or " \
                           "float. Received type: {0}".format(type(new_vt_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_two_wire_drop_value_flow_on_cn_fail2(self):
        """ Set Two Wore Drop Value On Controller Fail Case 2: Failed communication with controller """
        zone = self.create_test_zone_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            zone.set_two_wire_drop_value_on_cn()
        e_msg = "Exception occurred trying to set Zone 1's 'Two Wire Drop Value' to: '{0}'".format(zone.vt)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_mirror_zone_number_on_cn_pass1(self):
        """ Set Mirror Zone Number On Controller Pass Case 1: Using 6 as passed in value for _mr """
        Devices.controller_type = "10"
        zone = self.create_test_zone_object()

        # Expected value is the _va value set at object Zone object creation
        expected_value = 6
        zone.set_mirror_zone_number_on_cn(expected_value)

        # _va value is set during this method and should equal the original value
        actual_value = zone.mr
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_mirror_zone_number_on_cn_pass2(self):
        """ Set Mirror Zone Number Controller Pass Case 2: Command with correct values sent to controller """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        expected_value = 6
        expected_command = "SET,ZN=1,MR=" + str(expected_value)
        zone.set_mirror_zone_number_on_cn(expected_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_mirror_zone_number_on_cn_fail1(self):
        """ Set Mirror Zone Number On Controller Fail Case 1: Pass String value as argument """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        new_mr_value = "b"
        with self.assertRaises(Exception) as context:
            zone.set_mirror_zone_number_on_cn(new_mr_value)
        expected_message = "Failed trying to set ZN 1 to mirror a zone. Not an integer passed in. Received type: " \
                           "{0}".format(type(new_mr_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_mirror_zone_number_flow_on_cn_fail2(self):
        """ Set Mirror Zone Number On Controller Fail Case 2: Failed communication with controller """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        # A contrived Exception is thrown when communicating with the mock serial port
        new_mr_value = 6
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            zone.set_mirror_zone_number_on_cn(new_mr_value)
        e_msg = "Exception occurred trying to set Zone 1 to Mirror Zone: {0}".format(new_mr_value)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_mirror_zone_number_flow_on_cn_fail3(self):
        """ Set Mirror Zone Number On Controller Fail Case 3: Attempt method with a 3200 controller """
        zone = self.create_test_zone_object()
        Devices.controller_type = "32"

        # A contrived Exception is thrown when communicating with the mock serial port
        new_mr_value = 6
        with self.assertRaises(Exception) as context:
            zone.set_mirror_zone_number_on_cn(new_mr_value)
        expected_message = "Attempting to Mirror a Zone on a 3200, which is currently not supported."
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_master_valve_pointer_on_cn_pass1(self):
        """ Set Master Valve Pointer On Controller Pass Case 1: Using 6 as passed in value for _mv """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        # Expected value is the _va value set at object Zone object creation
        expected_value = 6
        zone.set_master_valve_pointer_on_cn(expected_value)

        # _va value is set during this method and should equal the original value
        actual_value = zone.mv
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_master_valve_pointer_on_cn_pass2(self):
        """ Set Master Valve Pointer Controller Pass Case 2: Command with correct values sent to controller """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        expected_value = 6
        expected_command = "SET,ZN=1,MV=" + str(expected_value)
        zone.set_master_valve_pointer_on_cn(expected_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_master_valve_pointer_on_cn_fail1(self):
        """ Set Master Valve Pointer On Controller Fail Case 1: Pass String value as argument """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        new_mv_value = "b"
        with self.assertRaises(Exception) as context:
            zone.set_master_valve_pointer_on_cn(new_mv_value)
        expected_message = "Failed trying to set ZN 1's master valve. Not an integer passed in. Received type: " \
                           "{0}".format(type(new_mv_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_master_valve_pointer_on_cn_fail2(self):
        """ Set Master Valve Pointer On Controller Fail Case 2: Failed communication with controller """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        # A contrived Exception is thrown when communicating with the mock serial port
        new_mv_value = 6
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            zone.set_master_valve_pointer_on_cn(new_mv_value)
        e_msg = "Exception occurred trying to set Zone 1's MV to: MV {0}".format(new_mv_value)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_master_valve_pointer_on_cn_fail3(self):
        """ Set Master Valve Pointer On Controller Fail Case 3: Attempt method with a 3200 controller """
        zone = self.create_test_zone_object()
        Devices.controller_type = "32"

        # A contrived Exception is thrown when communicating with the mock serial port
        new_mv_value = 6
        with self.assertRaises(Exception) as context:
            zone.set_master_valve_pointer_on_cn(new_mv_value)
        expected_message = "Attempting to assign MV to a ZN on a 3200, which is currently not supported."
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_master_valve_pointer_on_cn_fail4(self):
        """ Set Master Valve Pointer On Controller Fail Case 4: Pass Master Valve that is below 1 """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        new_mv_value = 0
        with self.assertRaises(Exception) as context:
            zone.set_master_valve_pointer_on_cn(new_mv_value)
        expected_message = "Attempting to set reference to a master valve outside of available MV addresses: " \
                           "{0}".format(str(new_mv_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_master_valve_pointer_on_cn_fail5(self):
        """ Set Master Valve Pointer On Controller Fail Case 5: Pass Master Valve that is above 7 """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        new_mv_value = 8
        with self.assertRaises(Exception) as context:
            zone.set_master_valve_pointer_on_cn(new_mv_value)
        expected_message = "Attempting to set reference to a master valve outside of available MV addresses: " \
                           "{0}".format(str(new_mv_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_high_flow_variance_on_cn_pass1(self):
        """ Set High Flow Variance On Controller Pass Case 1: Using Default _ff value """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        # Expected value is the _va value set at object Zone object creation
        expected_value = zone.ff
        zone.set_high_flow_variance_on_cn()

        # _va value is set during this method and should equal the original value
        actual_value = zone.ff
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_high_flow_variance_on_cn_pass2(self):
        """ Set High Flow Variance On Controller Pass Case 2: Using "TR" as passed in value for _ff """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        # Expected value is the _va value set at object Zone object creation
        expected_value = "TR"
        zone.set_high_flow_variance_on_cn('TR')

        # _va value is set during this method and should equal the original value
        actual_value = zone.ff
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_high_flow_variance_on_cn_pass3(self):
        """ Set High Flow Variance On Controller Pass Case 3: Command with correct values sent to controller """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        expected_command = "SET,ZN=1,FF=FA"
        zone.set_high_flow_variance_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_flow_variance_on_cn_fail1(self):
        """ Set High Flow Variance On Controller Fail Case 1: Pass Int value as argument """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        new_ff_value = 6
        with self.assertRaises(Exception) as context:
            zone.set_high_flow_variance_on_cn(new_ff_value)
        expected_message = "Failed trying to set ZN 1's high flow variance. Not a string passed in. Received " \
                           "type: {0}".format(type(new_ff_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_high_flow_variance_on_cn_fail2(self):
        """ Set High Flow Variance On Controller Fail Case 2: Failed communication with controller """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            zone.set_high_flow_variance_on_cn()
        e_msg = "Exception occurred trying to set Zone 1's high flow variance to: {0}".format(zone.ff)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_high_flow_variance_on_cn_fail3(self):
        """ Set High Flow Variance On Controller Fail Case 3: Attempt method with a 3200 controller """
        zone = self.create_test_zone_object()
        Devices.controller_type = "32"

        # A contrived Exception is thrown when communicating with the mock serial port
        with self.assertRaises(Exception) as context:
            zone.set_high_flow_variance_on_cn()
        expected_message = "Attempting to set high flow variance to a ZN on a 3200, which is currently not supported."
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_high_flow_variance_on_cn_fail4(self):
        """ Set High Flow Variance On Controller Fail Case 4: Pass string that is not 'TR' or 'FA' """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"

        new_ff_value = "FALSE"
        with self.assertRaises(Exception) as context:
            zone.set_high_flow_variance_on_cn(new_ff_value)
        expected_message = "Exception occurred attempting to set incorrect 'High Flow Variance' value for " \
                           "controller: {0}. Expects 'TR' or 'FA'.".format(new_ff_value)
        self.assertEqual(expected_message, context.exception.message)

    # TODO finish testing this method
    #################################
    def test_set_crop_coefficient_value_on_cn_pass1(self):
        """ Set Crop Coefficient Value On Controller Pass Case 1: Pass in all values between 0-5, except kc"""
        zone = self.create_test_zone_object()
        expected_value = 1

        # Mock the calculated_landscape_coefficient method and assign it to what it mocks
        mock_calculate_landscape_coefficient = mock.MagicMock(return_value=expected_value)
        equations.calculate_landscape_coefficient = mock_calculate_landscape_coefficient

        zone.set_crop_coefficient_value_on_cn(_ks_value=2, _kd_value=2, _kmc_value=2)
        actual_value = zone.kc

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_crop_coefficient_value_on_cn_pass2(self):
        """ Set Crop Coefficient Value On Controller Pass Case 2: set KC value to 4.00 """
        # Create zone object
        zone = self.create_test_zone_object()

        expected_value = 4.00

        # Verify that exception is raised
        zone.set_crop_coefficient_value_on_cn(_kc_value=expected_value)

        # Compare expected error message to actual error message
        self.assertEqual(expected_value, zone.kc)

    #################################
    def test_set_crop_coefficient_value_on_cn_fail1(self):
        """ Set Crop Coefficient Value On Controller Fail Case 1: Pass in all values outside 0-5, except kc"""
        zone = self.create_test_zone_object()

        e_msg = "Exception occurred trying to set the crop coefficient value. \n" \
                "Species factor received was and Invalid was not between (0.00 and 5.00) received: {0}. \n" \
                "Density factor received was and Invalid was not between (0.00 and 5.00) received: {1}. \n" \
                "Micro Climate received was and Invalid was not between (0.00 and 5.00) received: {2}.\n" \
                .format(
                6,  # {0}
                6,  # {1}
                6,  # {2}
                )

        with self.assertRaises(ValueError) as context:
            zone.set_crop_coefficient_value_on_cn(_ks_value=6, _kd_value=6, _kmc_value=6)

        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_crop_coefficient_value_on_cn_fail2(self):
        """ Set Crop Coefficient Value On Controller Fail Case 2: set KC value to 9.00 """
        # Create zone object
        zone = self.create_test_zone_object()

        # Create expected message
        expected_message = "Exception occurred trying to set the crop coefficient value. \n" \
                           "crop coefficient received was and Invalid was not between (0.00 and 5.00)" \
                           " received: {0}. \n" \
            .format(
            9.00,  # {0}
        )

        # Verify that exception is raised
        with self.assertRaises(ValueError) as context:
            zone.set_crop_coefficient_value_on_cn(_kc_value=9.00)

        # Compare expected error message to actual error message
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_crop_coefficient_value_on_cn_fail3(self):
        """Set crop coefficient value Fail Case 3: Exception while sending value to the controller, invalid value"""
        # Create zone object
        zone = self.create_test_zone_object()

        # Get an exception from the try statement
        self.mock_send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            zone.set_crop_coefficient_value_on_cn(_kc_value=4.00)

        # Create exception message
        expected_message = "Exception occurred trying to set the crop coefficient value on the zone: value = '{0}' " \
            .format(zone.kc)

        # Compare the exception messages
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_precipitation_rate_value_on_cn_pass1(self):
        """ Sets the precipitation rate value on both the controller Pass Case 1: Sets precipitation rate at 3.00"""
        # Creates zone object
        zone = self.create_test_zone_object()

        # Create expected value variable
        expected_value = 3.00

        # Calls the method
        zone.set_precipitation_rate_value_on_cn(_pr_value=expected_value)

        # Verifies that self.pr is set in the zone object
        self.assertEqual(zone.pr, expected_value)

    #################################
    def test_set_precipitation_rate_value_on_cn_fail1(self):
        """ Sets the precipitation rate value on the controller Fail Case 1: Sets invalid precipitation rate at 6.00"""
        # Creates zone object
        zone = self.create_test_zone_object()

        # Stores expected value in variable
        value = 6.00

        # Stores expected error message in variable
        e_msg = "Exception occurred trying to set the precipitation rate value. Value received {0} was not between" \
                "(0.00 and 5.00)".format(value)

        # Calls the method, and raises a value error
        with self.assertRaises(ValueError) as context:
            zone.set_precipitation_rate_value_on_cn(value)

        # Checks that the expected error message is equal to the error message in the code
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_precipitation_rate_value_on_cn_fail2(self):
        """ Sets the precipitation rate value on the controller fail case 2: Raises an exception when the built string
         is sent to the controller"""
        # Create zone object
        zone = self.create_test_zone_object()

        # Sets _pr_value at 3.00
        value = 3.00

        # Stores expected exception
        expected_error = "Exception occurred trying to set the precipitation rate value on the zone " \
                         "value = '{0}' ".format(value)

        # Raises an exception as a side effect when string is sent to the controller
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Raises the exception while in the zone object
        with self.assertRaises(Exception) as context:
            zone.set_precipitation_rate_value_on_cn(_pr_value=value)

        # Compares the expected exception to the exception given
        self.assertEqual(expected_error, context.exception.message)

    #################################
    def test_set_distribution_uniformity_value_on_cn_pass1(self):
        """ Sets a distribution uniformity value on the controller pass case 1: Sets the distribution uniformity
        value at 140"""

        # Creates zone object
        zone = self.create_test_zone_object()

        # Stores the expected value in a variable
        expected_value = 140

        # Dives into method
        zone.set_distribution_uniformity_value_on_cn(_du_value=expected_value)

        # Compares the sent value to the expected value
        self.assertEqual(expected_value, zone.du)

    #################################
    def test_set_distribution_uniformity_value_on_cn_fail1(self):
        """ Sets a distribution uniformity value on the controller fail case 1: Sets the distribution uniformity
         value at 300 """
        # Creates the zone object
        zone = self.create_test_zone_object()

        # Stores value in a variable
        value = 300

        # Stores the error message in a variable
        e_msg = "Exception occurred trying to set the Distribution Uniformity value. Value received {0} was not " \
                "between(1 and 100) ".format(value)

        # Goes into the test, and raises a value error
        with self.assertRaises(ValueError) as context:
            zone.set_distribution_uniformity_value_on_cn(value)

        # Compares the exception raised to the actual value error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_distribution_uniformity_value_on_cn_fail2(self):
        """ Sets a distribution uniformity value on the zone on the controller pass case 2: Raises exception when
        built string is sent to the controller"""
        # Creates zone object
        zone = self.create_test_zone_object()

        # Stores distribution uniformity value
        value = 140

        # Stores the error message in a variable
        e_msg = "Exception occurred trying to set the Distribution Uniformity value on the zone " \
                "value = '{0}' ".format(value)

        # Raises an exception using side effect
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Goes into the test, waits for the exception
        with self.assertRaises(Exception) as context:
            zone.set_distribution_uniformity_value_on_cn(value)

        # Compares the expected exception with the actual exception
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_root_zone_working_storage_capacity_on_cn_pass1(self):
        """ Sets a root zone holding capacity value on controller pass case 1: Sets initial root zone water holding
         capacity successfully """
        # Creates zone object
        zone = self.create_test_zone_object()

        # Mocks the equations return values
        equations_mock = mock.MagicMock(return_value=0.35)
        equations.return_calculate_root_zone_working_water_storage = equations_mock

        # Stores the value for the root zone holding capacity
        value = 0.35

        # Goes into the test
        zone.set_root_zone_working_storage_capacity_on_cn()

        # Compares value within the test to the expected value
        self.assertEqual(zone.rz, value)

    #################################
    def test_set_root_zone_working_storage_capacity_on_cn_fail1(self):
        """ Sets a root zone water holding capacity value on controller fail case 1: Exception given while sending
         built string to controller"""
        # Creates zone object
        zone = self.create_test_zone_object()

        # Stores the value for the root zone holding capacity
        value = 0.65

        # Mocks the equation return values
        equation_mock = mock.MagicMock(return_value=value)
        equations.return_calculate_root_zone_working_water_storage = equation_mock

        # Stores expected error message
        e_msg = "Exception occurred trying to set the Root Zone Water Holding Capacity value on the zone " \
                "value = '{0}' ".format(value)

        # Raises an exception while sending built string to controller as a side effect
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Dives into actual method while expecting an exception
        with self.assertRaises(Exception) as context:
            zone.set_root_zone_working_storage_capacity_on_cn()

        # Compares exception error to the expected exception
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_cycle_time_on_cn_pass1(self):
        """ Sets the cycle time for the zone program on the controller pass case 1: Successfully sets cycle time """
        # Create zone object
        zone = self.create_test_zone_object()

        # sets controller type to the 1000
        zone.controller_type = "10"

        # Creates variable for cycle time
        cycle_time = 60

        # Mocks getting calculated cycle time
        cycle_time_mock = mock.MagicMock(return_value=cycle_time)
        zone.get_calculated_cycle_time = cycle_time_mock

        # Set 'cycle time' and 'use calculated cycle time'
        zone.set_cycle_time_on_cn(_cycle_time=cycle_time, _use_calculated_cycle_time=True)

        # Compares value of cycle time to expected value of cycle time
        self.assertEqual(zone.ct, cycle_time)

    #################################
    def test_set_cycle_time_on_cn_fail1(self):
        """ Sets the cycle time for zone program on the controller fail case 1: Raises an exception when the built
         string is sent to the controller"""

        # Creates zone object
        zone = self.create_test_zone_object()

        # Set controller type to the 1000
        zone.controller_type = "10"

        # Creates a variable to store cycle time
        cycle_time = 120

        # Store the expected error message
        e_msg = "Exception occurred trying to set (Zone {0}, 'Cycle Time' to: {1}\n " \
                "{2}".format(
            str(zone.ad),  # {0}
            str(cycle_time),  # {1}
            str('')  # {2}
        )

        # Raise an Assertion Error when sending string to controller by side effect
        self.mock_send_and_wait_for_reply.side_effect = AssertionError

        # Run through the method with an assertion error ready to be raised
        with self.assertRaises(Exception) as context:
            zone.set_cycle_time_on_cn(_cycle_time=cycle_time, _use_calculated_cycle_time=False)

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_cycle_time_on_cn_fail2(self):
        """ Sets the cycle time for the zone program on the controller fail case 2: attempts to set the zone cycle
         time for the 3200. Note that the 3200 is the default controller time, and thus does not need to be set as it
         is set automatically. """
        # Create zone object
        zone = self.create_test_zone_object()

        # Stores the expected message
        e_msg = "Attempting to set Zone Cycle Time not Zone Program Cycle Time for 3200 which is NOT SUPPORTED"

        # Run through the test with the expected assertion error
        with self.assertRaises(ValueError) as context:
            zone.set_cycle_time_on_cn()

        # Compare actual value error to expected value error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_soak_time_on_cn_pass1(self):
        """ Sets the soak time for the zone program on the controller pass case 1: Successfully sets soak time """
        # Create zone object
        zone = self.create_test_zone_object()

        # sets controller type to the 1000
        zone.controller_type = "10"

        # Creates variable for cycle time
        soak_time = 60

        # Mocks getting calculated cycle time
        soak_time_mock = mock.MagicMock(return_value=soak_time)
        zone.get_calculated_soak_time = soak_time_mock

        # Set 'cycle time' and 'use calculated cycle time'
        zone.set_soak_time_on_cn(_soak_time=soak_time, _use_calculated_soak_time=True)

        # Compares value of cycle time to expected value of cycle time
        self.assertEqual(zone.so, soak_time)

    #################################
    def test_set_soak_time_on_cn_pass2(self):
        """ Sets soak time for the zone program on the controller fail case 3: soak time is equal to 0"""

        # Create the zone object
        zone = self.create_test_zone_object()

        # Set controller type to the 1000
        zone.controller_type = "10"

        # Create an expected soak time variable equal to 0
        expected_soak_time = 0

        # Set soak time equal to 0
        zone.set_soak_time_on_cn(_soak_time=0)

        # Check that the soak time and expected soak time are equal
        self.assertEqual(expected_soak_time, zone.so)

    #################################
    def test_set_soak_time_on_cn_fail1(self):
        """ Sets the soak time for zone program on the controller fail case 1: Raises an exception when the built
         string is sent to the controller"""

        # Creates zone object
        zone = self.create_test_zone_object()

        # Set controller type to the 1000
        zone.controller_type = "10"

        # Creates a variable to store cycle time
        soak_time = 120.0

        # Store the expected error message
        e_msg = "Exception occurred trying to set (Zone {0}, 'Soak Time' to: {1}\n " \
                "{2}".format(
                    str(zone.ad),  # {0}
                    str(soak_time),  # {1}
                    str('')  # {2}
                )

        # Raise an Assertion Error when sending string to controller by side effect
        self.mock_send_and_wait_for_reply.side_effect = AssertionError

        # Run through the method with an assertion error ready to be raised
        with self.assertRaises(Exception) as context:
            zone.set_soak_time_on_cn(_soak_time=soak_time, _use_calculated_soak_time=False)

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_soak_time_on_cn_fail2(self):
        """ Sets the soak time for the zone program on the controller fail case 2: attempts to set the zone soak
         time for the 3200. Note that the 3200 is the default controller time, and thus does not need to be set as it
         is set automatically. """
        # Create zone object
        zone = self.create_test_zone_object()

        # Set the controller to the 3200
        zone.controller_type = "32"

        # Stores the expected message
        e_msg = "Attempting to set zone Soak Time for a Zone for 3200 which is NOT SUPPORTED"

        # Run through the test with the expected assertion error
        with self.assertRaises(ValueError) as context:
            zone.set_soak_time_on_cn()

        # Compare actual value error to expected value error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_get_calculated_cycle_time_pass1(self):
        """ Checks that the returned calculated cycle time is equal to the expected calculated cycle time"""
        # Create the zone object
        zone = self.create_test_zone_object()

        # Creates a variable to store a value
        returned_value = 30

        # Mock the equations.return_calculated_cycle_time
        equations_mock = mock.MagicMock(return_value=returned_value)
        equations.return_calculated_cycle_time = equations_mock

        # Call the method
        actual_value = zone.get_calculated_cycle_time()

        # compare the returned value of the method to the returned value of the mock
        self.assertEqual(returned_value, actual_value)

    #################################
    def test_get_calculated_soak_time_pass1(self):
        """ Checks that the returned calculated soak time is equal to the expected calculated soak time"""
        # Create the zone object
        zone = self.create_test_zone_object()

        # Creates a variable to store a value
        returned_value = 30

        # Mock the equations.return_calculated_cycle_time
        equations_mock = mock.MagicMock(return_value=returned_value)
        equations.return_calculated_soak_time = equations_mock

        # Call the method
        actual_value = zone.get_calculated_soak_time()

        # compare the returned value of the method to the returned value of the mock
        self.assertEqual(returned_value, actual_value)

    #################################
    def test_verify_crop_coefficient_value_on_cn_pass1(self):
        """ verifies crop coefficient for zone on the controller pass case 1: Tests to see if the crop coefficient
        can be verified """
        # Create zone object
        zone = self.create_test_zone_object()

        # Create expected value
        expected_value = 2.00

        # KC value is expected value
        zone.kc = expected_value

        # Mock the get data value string
        data_value = mock.MagicMock(return_value=expected_value)
        zone.data.get_value_string_by_key = data_value

        # Call the method
        zone.verify_crop_coefficient_value_on_cn()

        # Verify if the mocked crop coefficient is equal to the actual crop coefficient
        self.assertEqual(expected_value, zone.kc)

    #################################
    def test_verify_crop_coefficient_value_on_cn_fail1(self):
        """ verifies crop coefficient for zone on the controller fail case 1: Unable to verify the crop coefficient """
        # Create the zone object
        zone = self.create_test_zone_object()

        # Store zn.kc's value
        zone.kc = 3.00

        # Store expected return value from mock
        expected_value = 2.00

        # Mock the get value string
        value_string = mock.MagicMock(return_value=expected_value)
        zone.data.get_value_string_by_key = value_string

        # Create the expected exception
        e_msg = "Unable to verify Zone {0}'s crop coefficient. Received: {1}, Expected: {2}".format(
            str(zone.ad),  # {0}
            str(expected_value),  # {1}
            str(zone.kc)  # {2}
        )

        # Run through the test
        with self.assertRaises(ValueError) as context:
            zone.verify_crop_coefficient_value_on_cn()

        # Check that the error message is equal to the expected error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_precipitation_rate_value_on_cn_pass1(self):
        """ Verifies the precipitation rate on the controller pass case 1: Controller precipitation rate is able to be
        set correctly """
        # Create zone object
        zone = self.create_test_zone_object()

        # Create expected value
        expected_value = 2.00

        # KC value is expected value
        zone.pr = expected_value

        # Mock the get data value string
        data_value = mock.MagicMock(return_value=expected_value)
        zone.data.get_value_string_by_key = data_value

        # Call the method
        zone.verify_precipitation_rate_value_on_cn()

        # Verify if the mocked crop coefficient is equal to the actual crop coefficient
        self.assertEqual(expected_value, zone.pr)

    #################################
    def test_verify_precipitation_rate_value_on_cn_fail1(self):
        """ verifies precipitation rate for zone on the controller fail case 1: Unable to verify the precipitation
        rate """
        # Create the zone object
        zone = self.create_test_zone_object()

        # Store zn.kc's value
        zone.pr = 3.00

        # Store expected return value from mock
        expected_value = 2.00

        # Mock the get value string
        value_string = mock.MagicMock(return_value=expected_value)
        zone.data.get_value_string_by_key = value_string

        # Create the expected exception
        e_msg = "Unable to verify Zone {0}'s precipitation rate. Received: {1}, Expected: {2}".format(
            str(zone.ad),  # {0}
            str(expected_value),  # {1}
            str(zone.pr)  # {2}
        )

        # Run through the test
        with self.assertRaises(ValueError) as context:
            zone.verify_precipitation_rate_value_on_cn()

        # Check that the error message is equal to the expected error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_distribution_uniformity_value_on_cn_pass1(self):
        """ Verifies the distribution uniformity on the controller pass case 1: Controller distribution uniformity is
        able to be set correctly """
        # Create zone object
        zone = self.create_test_zone_object()

        # Create expected value
        expected_value = 2.00

        # KC value is expected value
        zone.du = expected_value

        # Mock the get data value string
        data_value = mock.MagicMock(return_value=expected_value)
        zone.data.get_value_string_by_key = data_value

        # Call the method
        zone.verify_distribution_uniformity_value_on_cn()

        # Verify if the mocked crop coefficient is equal to the actual crop coefficient
        self.assertEqual(expected_value, zone.du)

    #################################
    def test_verify_distribution_uniformity_value_on_cn_fail1(self):
        """ verifies distribution uniformity for zone on the controller fail case 1: Unable to verify the distribution
        uniformity rate """
        # Create the zone object
        zone = self.create_test_zone_object()

        # Store zn.kc's value
        zone.du = 3.00

        # Store expected return value from mock
        expected_value = 2.00

        # Mock the get value string
        value_string = mock.MagicMock(return_value=expected_value)
        zone.data.get_value_string_by_key = value_string

        # Create the expected exception
        e_msg = "Unable to verify Zone {0}'s Distribution Uniformity. Received: {1}, Expected: {2}".format(
            str(zone.ad),  # {0}
            str(expected_value),  # {1}
            str(zone.du)  # {2}
        )

        # Run through the test
        with self.assertRaises(ValueError) as context:
            zone.verify_distribution_uniformity_value_on_cn()

        # Check that the error message is equal to the expected error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_root_zone_working_storage_capacity_on_cn_pass1(self):
        """ Verifies the root zone working storage capacity on the controller pass case 1: Controller
        root zone working storage capacity is able to be set correctly """
        # Create zone object
        zone = self.create_test_zone_object()

        # Create expected value
        expected_value = 2.00

        # KC value is expected value
        zone.rz = expected_value

        # Mock the get data value string
        data_value = mock.MagicMock(return_value=expected_value)
        zone.data.get_value_string_by_key = data_value

        # Call the method
        zone.verify_root_zone_working_storage_capacity_on_cn()

        # Verify if the mocked crop coefficient is equal to the actual crop coefficient
        self.assertEqual(expected_value, zone.rz)

    #################################
    def test_verify_root_zone_working_storage_capacity_value_on_cn_fail1(self):
        """ verifies root zone working storage capacity for zone on the controller fail case 1: Unable to verify the
        root zone working storage capacity """
        # Create the zone object
        zone = self.create_test_zone_object()

        # Store zn.kc's value
        zone.rz = 3.00

        # Store expected return value from mock
        expected_value = 2.00

        # Mock the get value string
        value_string = mock.MagicMock(return_value=expected_value)
        zone.data.get_value_string_by_key = value_string

        # Create the expected exception
        e_msg = "Unable to verify Zone {0}'s Root Zone Water Holding Capacity . Received: {1}, Expected: {2}" \
            .format(
            str(zone.ad),  # {0}
            str(expected_value),  # {1}
            str(zone.rz)  # {2}
        )

        # Run through the test
        with self.assertRaises(ValueError) as context:
            zone.verify_root_zone_working_storage_capacity_on_cn()

        # Check that the error message is equal to the expected error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_moisture_balance_on_cn_pass1(self):
        """ Verifies the moisture balance set for zone on the controller pass case 1: Verifies zones' moisture balance
        against each other """
        # Creates zone object
        zone = self.create_test_zone_object()

        # Create variable for expected moisture balance
        expected_moisture_balance = 30

        # Mock equations moisture balance
        moisture_balance_mock = mock.MagicMock(return_value=expected_moisture_balance)
        equations.return_todays_calculated_moisture_balance = moisture_balance_mock

        # Mock get value string by key
        value_string = mock.MagicMock(return_value=expected_moisture_balance)
        zone.data.get_value_string_by_key = value_string

        # Go into method
        zone.verify_moisture_balance_on_cn()

        # Check that actual moisture balance equals expected moisture balance
        self.assertEqual(zone.mb, expected_moisture_balance)

    #################################
    def test_verify_moisture_balance_on_cn_fail1(self):
        """ Verifies the moisture balance set for the zone on the controller fail case 1: Unable to verify the
         moisture balance on the zone """
        # Creates zone object
        zone = self.create_test_zone_object()

        # Create variable for expected moisture balance
        expected_moisture_balance = 30.0

        # Create second expected moisture balance (that's different than the first)
        second_expected_moisture_balance = 45.0

        # # Store expected error message in a variable
        e_msg = "Unable to verify Zone {0}'s daily moisture balance. Received: {1}, Expected: {2}".format(
            str(zone.ad),  # {0}
            str(second_expected_moisture_balance),  # {1}
            str(expected_moisture_balance)  # {2}
        )

        # Mock equations moisture balance
        moisture_balance_mock = mock.MagicMock(return_value=expected_moisture_balance)
        equations.return_todays_calculated_moisture_balance = moisture_balance_mock

        # Mock get value string by key
        value_string = mock.MagicMock(return_value=second_expected_moisture_balance)
        zone.data.get_value_string_by_key = value_string

        # Go into method
        with self.assertRaises(ValueError) as context:
            zone.verify_moisture_balance_on_cn()

        # Check that actual moisture balance equals expected moisture balance
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_design_flow_on_cn_pass1(self):
        """ Verify Design Flow On Controller Pass Case 1: Exception is not raised """
        zone = self.create_test_zone_object()
        zone.df = 5.0
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,DF=5")
        zone.data = mock_data

        try:
            # assertRaises raises an Assertion Error if an Exception is not raised in the verify_high_flow... method
            with self.assertRaises(Exception):
                zone.verify_design_flow_on_cn()

        # Catches an Assertion Error from above, meaning the verify_high_flow... method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_design_flow_on_cn_fail1(self):
        """ Verify Design Flow On Controller Fail Case 1: Value on controller does not match what is
        stored in zone.df """
        zone = self.create_test_zone_object()
        zone.df = 5.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,DF=6")
        zone.data = mock_data

        expected_message = "Unable to verify Zone 1's design flow. Received: 6.0, Expected: 5.0"
        try:
            zone.verify_design_flow_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_solenoid_current_on_cn_pass1(self):
        """ Verify Solenoid Current On Controller Pass Case 1: Exception is not raised """
        zone = self.create_test_zone_object()
        zone.va = 5.0
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,VA=5")
        zone.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the verify_high_flow... method
            with self.assertRaises(Exception):
                zone.verify_solenoid_current_on_cn()

        # Catches an Assertion Error from above, meaning the verify_high_flow... method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_solenoid_current_on_cn_fail1(self):
        """ Verify Solenoid Current On Controller Fail Case 1: Value on controller does not match what is
        stored in zone.va """
        zone = self.create_test_zone_object()
        zone.va = 5.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,VA=6")
        zone.data = mock_data

        expected_message = "Unable to verify Zone 1's solenoid current. Received: 6.0, Expected: 5.0"
        try:
            zone.verify_solenoid_current_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_solenoid_voltage_on_cn_pass1(self):
        """ Verify Solenoid Voltage On Controller Pass Case 1: Exception is not raised """
        zone = self.create_test_zone_object()
        zone.vv = 5.0
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,VV=5")
        zone.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the verify_high_flow... method
            with self.assertRaises(Exception):
                zone.verify_solenoid_voltage_on_cn()

        # Catches an Assertion Error from above, meaning the verify_high_flow... method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_solenoid_voltage_on_cn_fail1(self):
        """ Verify Solenoid Voltage On Controller Fail Case 1: Value on controller does not match what is
        stored in zone.vv """
        zone = self.create_test_zone_object()
        zone.vv = 5.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,VV=6")
        zone.data = mock_data

        expected_message = "Unable to verify Zone 1's solenoid voltage. Received: 6.0, Expected: 5.0"
        try:
            zone.verify_solenoid_voltage_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_two_wire_drop_value_on_cn_pass1(self):
        """ Verify Solenoid Voltage On Controller Pass Case 1: Exception is not raised """
        zone = self.create_test_zone_object()
        zone.vt = 5.0
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,VT=5")
        zone.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the verify_high_flow... method
            with self.assertRaises(Exception):
                zone.verify_two_wire_drop_value_on_cn()

        # Catches an Assertion Error from above, meaning the verify_high_flow... method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_two_wire_drop_value_on_cn_fail1(self):
        """ Verify Solenoid Voltage On Controller Fail Case 1: Value on controller does not match what is
        stored in zone.vt """
        zone = self.create_test_zone_object()
        zone.vt = 5.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,VT=6")
        zone.data = mock_data

        expected_message = "Unable to verify Zone 1's two wire drop value. Received: 6.0, Expected: 5.0"
        try:
            zone.verify_two_wire_drop_value_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_mirrored_zone_on_cn_pass1(self):
        """ Verify Mirrored Zone On Controller Pass Case 1: Exception is not raised """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"
        zone.mr = 5.0
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,MR=5")
        zone.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the verify_high_flow... method
            with self.assertRaises(Exception):
                zone.verify_mirrored_zone_on_cn()

        # Catches an Assertion Error from above, meaning the verify_high_flow... method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_mirrored_zone_on_cn_fail1(self):
        """ Verify Mirrored Zone On Controller Fail Case 1: Value on controller does not match what is
        stored in zone.vt """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"
        zone.mr = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,MR=6")
        zone.data = mock_data

        expected_message = "Unable to verify Zone 1's mirrored zone. Received: 6, Expected: 5"
        try:
            zone.verify_mirrored_zone_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_mirrored_zone_on_cn_fail2(self):
        """ Verify Mirrored Zone On Controller Fail Case 2: Controller type is not a 1000 """
        zone = self.create_test_zone_object()
        Devices.controller_type = "32"
        zone.mr = 6.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,MR=6")
        zone.data = mock_data

        expected_message = "Attempting to verify mirrored zone for a zone on a 3200, which is currently not " \
                           "supported."
        try:
            zone.verify_mirrored_zone_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_master_valve_assigned_on_cn_pass1(self):
        """ Verify Master Valve Assigned On Controller Pass Case 1: Exception is not raised """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"
        zone.mv = 5.0
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,MV=5")
        zone.data = mock_data

        try:
            # assertRaises raises an Assertion Error if an Exception is not raised in the verify_high_flow... method
            with self.assertRaises(Exception):
                zone.verify_master_valve_assigned_on_cn()

        # Catches an Assertion Error from above, meaning the verify_high_flow... method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_master_valve_assigned_on_cn_pass2(self):
        """ Verify Master Valve assigned on controller pass case 2: Controller is set to none """
        # Create zone object
        zone = self.create_test_zone_object()

        # Set controller type to the 1000
        zone.controller_type = "10"

        # Mock assigned master valve zone value to get down to the pass case
        expected_value = mock.MagicMock(return_value=None)
        zone.data.get_value_string_by_key = expected_value

        # The master valve value is set equal to the expected value's return value
        zone.mv = expected_value.return_value

        # Dive into the method
        zone.verify_master_valve_assigned_on_cn()

        # Check that mv is equal to expected value
        self.assertEqual(zone.mv, None)

    #################################
    def test_verify_master_valve_assigned_on_cn_fail1(self):
        """ Verify Master Valve Assigned On Controller Fail Case 1: Value on controller does not match what is
        stored in zone.vt """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"
        zone.mv = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,MV=6")
        zone.data = mock_data

        expected_message = "Unable to verify Zone 1's Master Valve pointer. Received: 6, Expected: 5"
        try:
            zone.verify_master_valve_assigned_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_master_valve_assigned_on_cn_fail2(self):
        """ Verify Master Valve Assigned On Controller Fail Case 2: Controller type is not a 1000 """
        zone = self.create_test_zone_object()
        Devices.controller_type = "32"
        zone.mv = 6
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,MV=6")
        zone.data = mock_data

        expected_message = "Attempting to verify Master valve assignment for a zone on a 3200, which is currently " \
                           "not supported."
        try:
            zone.verify_master_valve_assigned_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_high_flow_variance_val_on_cn_pass1(self):
        """ Verify High Flow Variance On Controller Pass Case 1: Exception is not raised """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"
        zone.ff = 5.0
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,FF=5")
        zone.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the verify_high_flow... method
            with self.assertRaises(Exception):
                zone.verify_high_flow_variance_val_on_cn()

        # Catches an Assertion Error from above, meaning the verify_high_flow... method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_high_flow_variance_val_on_cn_pass2(self):
        """ Verifies the high flow variance on the controller pass case 2: High flow variance value is currently
        set to false """
        # Create zone object
        zone = self.create_test_zone_object()

        # Set the controller tye to the 1000
        zone.controller_type = "10"

        # Store a false value (for return and comparison)
        false_value = 'FA'

        # Set the high flow variance to false through mock
        high_flow_variance = mock.MagicMock(return_value=false_value)
        zone.data.get_value_string_by_key = high_flow_variance

        # Go into the method
        zone.verify_high_flow_variance_val_on_cn()

        # Compare the flow variance against the expected flow variance
        self.assertEqual(zone.ff, false_value)

    #################################
    def test_verify_high_flow_variance_val_on_cn_fail1(self):
        """ Verify High Flow Variance On Controller Fail Case 1: Value on controller does not match what is
        stored in zone.vt """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"
        zone.ff = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,FF=6")
        zone.data = mock_data

        expected_message = "Unable to verify Zone 1's high flow variance. Received: 6, Expected: 5"
        try:
            zone.verify_high_flow_variance_val_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_high_flow_variance_val_on_cn_fail2(self):
        """ Verify High Flow Variance On Controller Fail Case 2: Controller type is not a 1000 """
        zone = self.create_test_zone_object()
        Devices.controller_type = "32"
        zone.ff = 6.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,FF=6")
        zone.data = mock_data

        expected_message = "Attempting to verify high flow variance for a zone on a 3200, which is currently " \
                           "not supported."
        try:
            zone.verify_high_flow_variance_val_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_enable_state_on_cn_pass1(self):
        """ Verify Enable State On Controller Pass Case 1: Exception is not raised """
        zone = self.create_test_zone_object()
        Devices.controller_type = "10"
        zone.en = "FA"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,EN=FA")
        zone.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the verify_high_flow... method
            with self.assertRaises(Exception):
                zone.verify_enable_state_on_cn()

        # Catches an Assertion Error from above, meaning the verify_high_flow... method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_enable_state_on_cn_fail1(self):
        """ Verify Enable State On Controller Fail Case 1: Value on controller does not match what is
        stored in zone.vt """
        zone = self.create_test_zone_object()
        zone.controller_type = "10"
        zone.en = "TR"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,EN=FA")
        zone.data = mock_data

        expected_message = "Unable to verify Zone 1's enabled state. Received: FA, Expected: TR"
        try:
            zone.verify_enable_state_on_cn()

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_self_test_and_update_object_attributes_pass1(self):
        """ Tests the device and then resets the attributes of the device to match the controller pass case 1:
        Successfully updates the attributes of the zone object """
        # Creates zone object
        zone = self.create_test_zone_object()

        # Stores solenoid current return value, solenoid voltage return value, and two wire drop value in variables
        solenoid_current = 1.0
        solenoid_voltage = 1.0
        two_wire_drop = 1.0

        # Mocks solenoid current
        solenoid_current_mock = mock.MagicMock(return_value=solenoid_current)
        zone.data.get_value_string_by_key = solenoid_current_mock

        # Mocks solenoid voltage
        solenoid_voltage_mock = mock.MagicMock(return_value=solenoid_voltage)
        zone.data.get_value_string_by_key = solenoid_voltage_mock

        # Mocks two wire drop
        two_wire_drop_mock = mock.MagicMock(return_value=two_wire_drop)
        zone.data.get_value_string_by_key = two_wire_drop_mock

        # Go into the method
        zone.self_test_and_update_object_attributes()

        # Compare each of the values to the expected values
        self.assertEqual(solenoid_current, zone.va)
        self.assertEqual(solenoid_voltage, zone.vv)
        self.assertEqual(two_wire_drop, zone.vt)

        ##################################

    def test_self_test_and_update_object_attributes_fail1(self):
        """ Tests the device and then resets the attributes of the device to match the controller fail case 1:
        Exception occurred while trying to update attributes of the zone"""
        # Creates zone object
        zone = self.create_test_zone_object()

        # Stores solenoid current return value, solenoid voltage return value, and two wire drop value in variables
        solenoid_current = 1.0
        solenoid_voltage = 1.0
        two_wire_drop = 1.0

        # Store expected error message

        e_msg = "Exception occurred trying to update attributes of the zone {0} object." \
                " Solenoid Current Value: '{1}'," \
                " Solenoid Voltage'Value: '{2}'," \
                " Two Wire Drop Value: '{3}'" \
                .format(
                    str(zone.ad),  # {0}
                    str(zone.va),  # {1}
                    str(zone.vv),  # {2}
                    str(zone.vt)  # {3}
                )

        # Mocks solenoid current
        solenoid_current_mock = mock.MagicMock(return_value=solenoid_current)
        zone.data.get_value_string_by_key = solenoid_current_mock

        # Mocks solenoid voltage
        solenoid_voltage_mock = mock.MagicMock(return_value=solenoid_voltage)
        zone.data.get_value_string_by_key = solenoid_voltage_mock

        # Mocks two wire drop
        two_wire_drop_mock = mock.MagicMock(return_value=two_wire_drop)
        zone.data.get_value_string_by_key = two_wire_drop_mock

        # Create an exception using side effect
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Raise an exception while running through the code
        with self.assertRaises(Exception) as context:
            zone.self_test_and_update_object_attributes()

        # Compare expected error message with actual error message
        self.assertEqual(e_msg, context.exception.message)


    #################################
    # def test_who_i_am_pass1(self):
    #     """ Verifies who I am on the controller pass case 1: _expected_status is not none. Verify base attributes, and
    #     zone specific attributes """
        # Create zone object
        # zone = self.create_test_zone_object()
        #
        # # Mock base attributes
        # description = mock.MagicMock()
        # zone.verify_description_on_cn = description
        #
        # latitude = mock.MagicMock()
        # zone.verify_latitude_on_cn = latitude
        #
        # longitude = mock.MagicMock()
        # zone.verify_longitude_on_cn = longitude
        #
        # status = mock.MagicMock(return_value='OK')
        # zone.verify_status_on_cn = status
        #
        # # Verify zone specific attributes
        # serial_number = mock.MagicMock(return_value='TSD0001')
        # zone.verify_serial_number_on_cn = serial_number
        #
        # enabled_state = mock.MagicMock(return_value='TR')
        # zone.verify_enable_state_on_cn = enabled_state
        #
        # design_flow = mock.MagicMock(return_value=1.0)
        # zone.verify_design_flow_on_cn = design_flow
        #
        # solenoid_current = mock.MagicMock(return_value=0.23)
        # zone.verify_solenoid_current_on_cn = solenoid_current
        #
        # solenoid_voltage = mock.MagicMock()
        # zone.verify_solenoid_voltage_on_cn = solenoid_voltage
        #
        # two_wire_drop_value = mock.MagicMock()
        # zone.verify_two_wire_drop_value_on_cn = two_wire_drop_value
        # #TODO the controller type is not being cleared from unit test before this one
        # # # Set controller to 3200
        # # zone.controller_type = "32"
        #
        # # Goes into the method, make sure to set expected status to something other than none
        # zone.verify_who_i_am(_expected_status='OK')

    #################################
    def test_who_i_am_pass2(self):
        """ Verifies who I am on the controller pass case 1: Controller type is 1000 and enable_et is true"""
        # Create zone object
        zone = self.create_test_zone_object()

        # Mock base attributes
        description = mock.MagicMock()
        zone.verify_description_on_cn = description

        latitude = mock.MagicMock()
        zone.verify_latitude_on_cn = latitude

        longitude = mock.MagicMock()
        zone.verify_longitude_on_cn = longitude

        status = mock.MagicMock(return_value='OK')
        zone.verify_status_on_cn = status

        # Verify zone specific attributes
        serial_number = mock.MagicMock(return_value='TSD0001')
        zone.verify_serial_number_on_cn = serial_number

        enabled_state = mock.MagicMock(return_value='TR')
        zone.verify_enable_state_on_cn = enabled_state

        design_flow = mock.MagicMock(return_value=1.0)
        zone.verify_design_flow_on_cn = design_flow

        solenoid_current = mock.MagicMock(return_value=0.23)
        zone.verify_solenoid_current_on_cn = solenoid_current

        solenoid_voltage = mock.MagicMock()
        zone.verify_solenoid_voltage_on_cn = solenoid_voltage

        two_wire_drop_value = mock.MagicMock()
        zone.verify_two_wire_drop_value_on_cn = two_wire_drop_value

        # Set controller to 1000
        zone.controller_type = "10"

        # Verify mirrored zone
        mirrored_zone = mock.MagicMock()
        zone.verify_mirrored_zone_on_cn = mirrored_zone

        # Verify master valve assignment
        master_valve_assignment = mock.MagicMock()
        zone.verify_master_valve_assigned_on_cn = master_valve_assignment

        # Verify high flow variance value
        high_flow_variance = mock.MagicMock()
        zone.verify_high_flow_variance_val_on_cn = high_flow_variance

        # Set enable_et to true (just something other than opcodes.false, not necessarily opcodes.true or true)
        zone.enable_et = True

        # Verify etc valve
        etc_valve = mock.MagicMock()
        zone.verify_etc_value_on_cn = etc_valve

        # Verify crop coefficient
        crop_coefficient = mock.MagicMock()
        zone.verify_crop_coefficient_value_on_cn = crop_coefficient

        # Verify precipitation rate value
        precipitation_rate = mock.MagicMock()
        zone.verify_precipitation_rate_value_on_cn = precipitation_rate

        # Verify distribution uniformity value
        distribution_uniformity_value = mock.MagicMock()
        zone.verify_distribution_uniformity_value_on_cn = distribution_uniformity_value

        # Verify root zone working storage capacity
        root_zone_working_storage_capacity = mock.MagicMock()
        zone.verify_root_zone_working_storage_capacity_on_cn = root_zone_working_storage_capacity

        # Verify moisture balance value
        moisture_balance_value = mock.MagicMock()
        zone.verify_moisture_balance_on_cn = moisture_balance_value

        # Goes into the method
        zone.verify_who_i_am()

if __name__ == "__main__":
    unittest.main()
