import unittest
import mock
import serial
import status_parser

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
# you have to set the lat and long after the devices class is called so that they don't get skipped over
Devices.controller_lat = float(43.609768)
Devices.controller_long = float(-116.310569)

from old_32_10_sb_objects_dec_29_2017.common.objects.controller.sw import EventSwitch
__author__ = 'Brice "Ajo Grande" Garlick'


class TestEventSwitchObject(unittest.TestCase):

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        EventSwitch.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_event_switch_object(self):
        """ Creates a new Event Switch object for testing purposes """
        switch = EventSwitch(_serial="TSD0001", _address=1)

        return switch

    #################################
    def test_set_default_values_pass1(self):
        """ Set Default Values On Controller Pass Case 1: Correct Command Sent """
        switch = self.create_test_event_switch_object()
        expected_command = "SET," \
                           "SW=TSD0001," \
                           "EN=TR," \
                           "DS=TSD0001 Test Event Switch 1," \
                           "LA=43.609773," \
                           "LG=-116.309864," \
                           "VC=CL," \
                           "VT=1.7"

        switch.set_default_values()
        self.mock_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values On Controller Fail Case 1: Handle Exception Raised From Controller """
        switch = self.create_test_event_switch_object()
        expected_command = "SET," \
                           "SW=TSD0001," \
                           "EN=TR," \
                           "DS=TSD0001 Test Event Switch 1," \
                           "LA=43.609773," \
                           "LG=-116.309864," \
                           "VC=CL," \
                           "VT=1.7"

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            switch.set_default_values()
        e_msg = "Exception occurred trying to set Event Switch TSD0001 (1)'s 'Default values' to: '{0}'"\
            .format(expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_enable_state_pass1(self):
        """ Set Enable State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=TSD0001,{1}=TR".format(opcodes.event_switch, opcodes.enabled)
        switch = self.create_test_event_switch_object()
        switch.set_enable_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_state_pass2(self):
        """ Set Enable State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=TSD0001,{1}=FA".format(opcodes.event_switch, opcodes.enabled)
        switch = self.create_test_event_switch_object()
        switch.set_enable_state_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_state_fail1(self):
        """ Set Enable State On Controller Fail Case 1: Invalid State Argument """
        switch = self.create_test_event_switch_object()
        with self.assertRaises(ValueError) as context:
            switch.set_enable_state_on_cn("Foo")
        e_msg = "Invalid enabled state entered for Event Switch TSD0001 (1). Received: Foo, Expected: 'TR' or " \
                "'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_enable_state_fail2(self):
        """ Set Enable State On Controller Fail Case 2: Failed Communication With Controller """
        switch = self.create_test_event_switch_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            switch.set_enable_state_on_cn("FA")
        e_msg = "Exception occurred trying to set Event Switch TSD0001 (1)'s Enable State"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_contact_state_on_cn_pass1(self):
        """ Set Contact State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=TSD0001,{1}=CL".format(opcodes.event_switch, opcodes.contacts_state)
        switch = self.create_test_event_switch_object()
        switch.set_contact_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_contact_state_on_cn_pass2(self):
        """ Set Contact State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=TSD0001,{1}=OP".format(opcodes.event_switch, opcodes.contacts_state)
        switch = self.create_test_event_switch_object()
        switch.set_contact_state_on_cn("OP")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_contact_state_on_cn_fail1(self):
        """ Set Contact State On Controller Fail Case 1: Invalid State Argument """
        switch = self.create_test_event_switch_object()
        with self.assertRaises(ValueError) as context:
            switch.set_contact_state_on_cn("Foo")
        e_msg = "Unable to set Event Switch TSD0001 (1)'s Contact State. Received: Foo, needs to be FA or TR"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_contact_state_on_cn_fail2(self):
        """ Set Contact State On Controller Fail Case 2: Failed Communication With Controller """
        switch = self.create_test_event_switch_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            switch.set_contact_state_on_cn("OP")
        e_msg = "Exception occurred trying to set Event Switch TSD0001 (1)'s 'Contact State' to: 'OP'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass1(self):
        """ Set Two Wire Drop Value On Controller Pass Case 1: Using Default _vt value """
        switch = self.create_test_event_switch_object()

        # Expected value is the _vt value set at object Zone object creation
        expected_value = switch.vt
        switch.set_two_wire_drop_value_on_cn()

        # _vt value is set during this method and should equal the original value
        actual_value = switch.vt
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass2(self):
        """ Set Two Wire Drop Value On Controller Pass Case 2: Setting new _vt value = 6.0 """
        switch = self.create_test_event_switch_object()

        # Expected _vt value is 6
        expected_value = 6.0
        switch.set_two_wire_drop_value_on_cn(expected_value)

        # _vt value is set during this method and should equal the value passed into the method
        actual_value = switch.vt
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass3(self):
        """ Set Two Wire Drop Value On Controller Pass Case 3: Command with correct values sent to controller """
        switch = self.create_test_event_switch_object()

        vt_value = str(switch.vt)
        expected_command = "SET,{0}=TSD0001,{1}={2}".format(opcodes.event_switch, opcodes.two_wire_drop, vt_value)
        switch.set_two_wire_drop_value_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_two_wire_drop_value_on_cn_fail1(self):
        """ Set Two Wire Drop Value On Controller Fail Case 1: Pass String value as argument """
        switch = self.create_test_event_switch_object()

        new_vt_value = "b"
        with self.assertRaises(Exception) as context:
            switch.set_two_wire_drop_value_on_cn(new_vt_value)
        expected_message = "Failed trying to set Event Switch TSD0001 (1) two wire drop. Invalid argument type, " \
                           "expected an int or float, received: {0}".format(type(new_vt_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_two_wire_drop_value_on_cn_fail2(self):
        """ Set Two Wire Drop Value On Controller Fail Case 2: Unable to send command to controller """
        switch = self.create_test_event_switch_object()
        _vt = 2
        self.mock_send_and_wait_for_reply.side_effect = Exception
        switch.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply
        with self.assertRaises(Exception) as context:
            switch.set_two_wire_drop_value_on_cn(_value=_vt)
        expected_msg = "Exception occurred trying to set Event Switch {0} ({1})'s 'Two Wire Drop Value' to: '{2}'"\
            .format(switch.sn, switch.ad, _vt)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_verify_contact_state_on_cn_pass1(self):
        """ Verify Contact State On Controller Pass Case 1: Exception is not raised """
        switch = self.create_test_event_switch_object()
        switch.vc = 'OP'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=OP".format(opcodes.contacts_state))
        switch.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                switch.verify_contact_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_contact_state_on_cn_fail1(self):
        """ Verify Contact State On Controller Fail Case 1: Value on controller does not match what is
        stored in switch.vc """
        switch = self.create_test_event_switch_object()
        switch.vc = 'Cl'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=CL".format(opcodes.contacts_state))
        switch.data = mock_data

        expected_message = "Unable verify Event Switch TSD0001 (1)'s contact state. Received: CL, " \
                           "Expected: Cl"
        try:
            switch.verify_contact_state_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_two_wire_drop_value_on_cn_pass1(self):
        """ Verify Two Wire Drop Value On Controller Pass Case 1: Exception is not raised """
        switch = self.create_test_event_switch_object()
        switch.vt = 5.0
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5.0".format(opcodes.two_wire_drop))
        switch.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                switch.verify_two_wire_drop_value_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_two_wire_drop_value_on_cn_fail1(self):
        """ Verify Two Wire Drop Value On Controller Fail Case 1: Value on controller does not match what is
        stored in switch.vt """
        switch = self.create_test_event_switch_object()
        switch.vt = 5.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.two_wire_drop))
        switch.data = mock_data

        expected_message = "Unable verify Event Switch TSD0001 (1)'s two wire drop value. Received: 6.0, Expected: 5.0"
        try:
            switch.verify_two_wire_drop_value_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_enable_state_on_cn_pass1(self):
        """ Verify Enable State On Controller Pass Case 1: Exception is not raised """
        switch = self.create_test_event_switch_object()
        switch.en = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
        switch.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                switch.verify_enable_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_enable_state_on_cn_fail1(self):
        """ Verify Enable State On Controller Fail Case 1: Value on controller does not match what is
        stored in switch.en """
        switch = self.create_test_event_switch_object()
        switch.en = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
        switch.data = mock_data

        expected_message = "Unable verify Event Switch TSD0001 (1)'s enabled state. Received: TR, " \
                           "Expected: {0}".format(str(switch.en))
        try:
            switch.verify_enable_state_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_self_test_and_update_object_attributes_pass1(self):
        """ Does a self test on the device, then does a get date a resets the attributes of the device to match the
        controller pass case 1: Able to successfully set the attributes of the event switch object """
        # Create an event switch object
        sw = self.create_test_event_switch_object()

        # Mock do self test
        self_test = mock.MagicMock()
        sw.do_self_test = self_test

        # Mock get data
        data_get = mock.MagicMock()
        sw.get_data = data_get

        # Set expected contact state and two wire drop values
        contact_state_value = 'OK'
        two_wire_drop_value = 1.9

        # Mock return value for contact state and two wire drop as they both use the get_value_string_by_key method
        contact_and_drop_values = mock.MagicMock()
        sw.data.get_value_string_by_key = contact_and_drop_values
        contact_and_drop_values.side_effect = ['OK', 1.9]

        # Go into method
        sw.self_test_and_update_object_attributes()

        # Compare expected and actual contact_state and two wire drop values
        self.assertEqual(contact_state_value, sw.vc)
        self.assertEqual(two_wire_drop_value, sw.vt)

    #################################
    def test_self_test_and_update_object_attributes_fail1(self):
        """ Does a self test on the device, then does a get date a resets the attributes of the device to match the
        controller pass case 1: Raises an exception when updating attributes of the event switch"""
        # Create event switch sensor object
        sw = self.create_test_event_switch_object()

        # Store expected error message
        e_msg = "Exception occurred trying to update attributes of the event switch {0} object." \
                " Contact State Value: '{1}'," \
                " Two Wire Drop Value: '{2}'," \
                .format(
                    str(sw.ad),  # {0}
                    str(sw.vc),  # {1}
                    str(sw.vt),  # {2}
                )

        # Mock event switch value
        event_switch = mock.MagicMock()
        sw.data.get_value_string_by_key = event_switch

        # Mock two wire drop value
        two_wire_drop = mock.MagicMock()
        sw.data.get_value_string_by_key = two_wire_drop

        # Raise an exception as a side effect

        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Run the method expecting an exception
        with self.assertRaises(Exception) as context:
            sw.self_test_and_update_object_attributes()

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Test Verify Who I Am Pass Case 1: Run through the method
        Create test mainline object
        Mock: self.get_data because it goes outside of the method
            Return: None, so it does not fail
        Mock: self.verify_description_on_cn because it goes outside of the method
            Return: None, so it does not fail
        Mock: self.verify_serial_number_on_cn because it goes outside of the method
            Return: None, so it does not fail
        Mock: self.verify_latitude_on_cn because it goes outside of the method
            Return: None, so it does not fail
        Mock: self.verify_longitude_on_cn because it goes outside of the method
            Return: None, so it does not fail
        Mock: self.verify_status_on_cn because it goes outside of the method
            Return: None, so it does not fail
        Mock: self.verify_enable_state_on_cn because it goes outside of the method
            Return: None, so it does not fail
        Mock: self.verify_contact_state_on_cn because it goes outside of the method
            Return: None, so it does not fail
        Mock: self.verify_two_wire_drop_value_on_cn because it goes outside of the method
            Return: None, so it does not fail
        Run: Test, and if the test passes, it has successfully run through the code. There is no need to compare
        any values for this particular case.
        """
        # Create test mainline object
        sw = self.create_test_event_switch_object()

        # Mock get_data
        mock_get_data = mock.MagicMock(side_effect=None)
        sw.get_data = mock_get_data

        # Mock verify_description_on_cn
        mock_verify_description_on_cn = mock.MagicMock(side_effect=None)
        sw.verify_description_on_cn = mock_verify_description_on_cn

        # Mock verify_serial_number_on_cn
        mock_verify_serial_number_on_cn = mock.MagicMock(side_effect=None)
        sw.verify_serial_number_on_cn = mock_verify_serial_number_on_cn

        # Mock verify_latitude_on_cn
        mock_verify_latitude_on_cn = mock.MagicMock(side_effect=None)
        sw.verify_latitude_on_cn = mock_verify_latitude_on_cn

        # Mock verify_longitude_on_cn
        mock_verify_longitude_on_cn = mock.MagicMock(side_effect=None)
        sw.verify_longitude_on_cn = mock_verify_longitude_on_cn

        # Mock verify_status_on_cn
        mock_verify_status_on_cn = mock.MagicMock(side_effect=None)
        sw.verify_status_on_cn = mock_verify_status_on_cn

        # Mock verify_enable_state_on_cn
        mock_verify_enable_state_on_cn = mock.MagicMock(side_effect=None)
        sw.verify_enable_state_on_cn = mock_verify_enable_state_on_cn

        # Mock verify_contact_state_on_cn
        mock_verify_contact_state_on_cn = mock.MagicMock(side_effect=None)
        sw.verify_contact_state_on_cn = mock_verify_contact_state_on_cn

        # Mock verify_two_wire_drop_value_on_cn
        mock_verify_two_wire_drop_value_on_cn = mock.MagicMock(side_effect=None)
        sw.verify_two_wire_drop_value_on_cn = mock_verify_two_wire_drop_value_on_cn

        # Run test
        sw.verify_who_i_am(_expected_status=opcodes.bad_serial)

    if __name__ == "__main__":
        unittest.main()
