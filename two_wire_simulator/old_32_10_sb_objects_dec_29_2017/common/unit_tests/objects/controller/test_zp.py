import unittest
import mock
import serial
import status_parser

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.epa_package import equations
from old_32_10_sb_objects_dec_29_2017.common.epa_package.wbw_imports import *
from old_32_10_sb_objects_dec_29_2017.common.objects.object_bucket import *

from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
# you have to set the lat and long after the devices class is called so that they don't get skipped over
Devices.controller_lat = float(43.609768)
Devices.controller_long = float(-116.310569)

from old_32_10_sb_objects_dec_29_2017.common.objects.controller import zp, zn, ms
from old_32_10_sb_objects_dec_29_2017.common.objects.controller import pg_1000, pg_3200, poc_1000, poc_3200
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages

__author__ = 'Brice "Ajo Grande" Garlick'


class TestZoneProgramObject(unittest.TestCase):

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        self.mock_general_method = mock.MagicMock(side_effect=None)

        # Set serial instance to mock serial
        zp.ZoneProgram.ser = self.mock_ser
        zn.Zone.ser = self.mock_ser
        pg_1000.PG1000.ser = self.mock_ser
        pg_3200.PG3200.ser = self.mock_ser
        poc_1000.POC1000.ser = self.mock_ser
        poc_3200.POC3200.ser = self.mock_ser
        ms.MoistureSensor.ser = self.mock_ser

        zones[1] = zn.Zone(_serial="ZND0001", _address=1)
        zones[2] = zn.Zone(_serial="ZND0002", _address=2)

        poc32[1] = poc_3200.POC3200(_ad=1)
        poc10[1] = poc_1000.POC1000(_address=1)
        programs32[1] = pg_3200.PG3200(_ad=1)
        programs10[1] = pg_1000.PG1000(_address=1)
        moisture_sensors[1] = ms.MoistureSensor(_serial="MSD0001", _address=1)

        zp.ZoneProgram.controller_type = "32"

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # when these objects are created in this test, they carry over to any test after this one because they are
        # just pointers and not test variables specific to just THIS module.
        del zones[1]
        del poc32[1]
        del poc10[1]
        del programs32[1]
        del programs10[1]

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_zone_program_object(self, con_type="32"):
        """ Creates a new Event Switch object for testing purposes """
        # method_mock = mock.MagicMock(return_value=60)
        # zp.ZoneProgram.calculate_3200_runtime = method_mock
        # zp.ZoneProgram.calculate_3200_cycletime = method_mock
        # zp.ZoneProgram.calculate_3200_soaktime = method_mock

        if con_type is not "32":
            zp.ZoneProgram.controller_type = "10"

        zprog = zp.ZoneProgram(zone_obj=zones[1], prog_obj=programs32[1], _rt=240)

        # zprog.zone_objects = zones
        return zprog

    #################################
    def create_test_zone_program_object_all_args(self):
        """ Creates a new Event Switch object for testing purposes """
        zprog = zp.ZoneProgram(zone_obj=zones[1], prog_obj=programs32[1], _rt=240, _ct=60, _so=100, _pz=1, _ra=100,
                               _ws="LL", _ms=1, _ll=30, _ul=70, _cc="NV")
        print zprog

        return zprog

    #################################
    def test_calculate_3200_runtime_pass_1(self):
        """ Calculate 3200 Runtime Pass Case 1: Correct Value Returned When ZP is a Linked Zone """
        primaryzp = self.create_test_zone_program_object()
        zone_programs[1] = primaryzp
        linkedzp = zp.ZoneProgram(zone_obj=zones[2], prog_obj=programs32[1], _rt=240, _ct=60, _so=100, _pz=1, _ra=100,
                                  _ws="LL", _ms=1, _ll=30, _ul=70, _cc="NV")
        linkedzp.linked = True
        ret_val = 240  # return value

        mock_rt_value = mock.MagicMock(return_value=30)

        # The runtime will be 240 because "linkedzp" is a linked zone and the primary zone has a rt of 240
        self.assertEqual(first=linkedzp.calculate_3200_runtime(runtime=30), second=ret_val)

    #################################
    def test_calculate_3200_runtime_pass_2(self):
        """ Calculate 3200 Runtime Pass Case 2: Correct Value Returned When ZP is not a Linked Zone """
        zprog = self.create_test_zone_program_object()
        ret_val = 120  # return value

        self.assertEqual(first=zprog.calculate_3200_runtime(runtime=120), second=ret_val)

    #################################
    def test_calculate_3200_runtime_pass_3(self):
        """ Calculate 3200 Runtime Pass Case 3: Correct Value Returned When ZP is not a Linked Zone but ET is enabled"""
        zprog = self.create_test_zone_program_object()
        zprog.ee = opcodes.true
        ret_val = 160  # return value

        self.assertEqual(first=zprog.calculate_3200_runtime(runtime=110), second=ret_val)

    #################################
    def test_calculate_3200_cycle_time_pass_1(self):
        """ Calculate 3200 Cycle Time Pass Case 1: Correct Value Returned When ZP is a Linked Zone and EP is enabled """
        primaryzp = self.create_test_zone_program_object()
        primaryzp.ct = 30
        zone_programs[1] = primaryzp
        linkedzp = zp.ZoneProgram(zone_obj=zones[2], prog_obj=programs32[1], _rt=240, _ct=30, _so=100, _pz=1, _ra=100,
                                  _ws="LL", _ms=1, _ll=30, _ul=70, _cc="NV")
        linkedzp.linked = True
        linkedzp.ee = opcodes.true
        ret_val = 60  # return value

        self.assertEqual(first=linkedzp.calculate_3200_cycletime(cycletime=30), second=ret_val)

    #################################
    def test_calculate_3200_cycle_time_pass_2(self):
        """ Calculate 3200 Cycle Time Pass Case 2: Correct Value Returned When Cycle Time is Divisible by 60 """
        zprog = self.create_test_zone_program_object()
        ret_val = 120  # return value

        self.assertEqual(first=zprog.calculate_3200_cycletime(cycletime=120), second=ret_val)

    #################################
    def test_calculate_3200_cycle_time_pass_3(self):
        """ Calculate 3200 Cycle Time Pass Case 3: Correct Value Returned When Cycle Time is not Divisible by 60 """
        zprog = self.create_test_zone_program_object()
        ret_val = 160  # return value

        self.assertEqual(first=zprog.calculate_3200_cycletime(cycletime=110), second=ret_val)

    def test_build_obj_configuration_for_send_pass_1(self):
        """ Build Object Configuration For Send Pass Case 1: Create the SET string for a linked zone """
        zprog = self.create_test_zone_program_object_all_args()
        # Linked zone is set to true
        zprog.linked = True

        expected_command = "SET,PZ=1,PG=1,RA=100,PZ=1"

        zprog.zone_program_objects[1] = zprog

        # Build the command using the zone program's method
        actual_command = zprog.build_obj_configuration_for_send()

        self.assertEqual(expected_command, actual_command)

    def test_build_obj_configuration_for_send_fail_1(self):
        """ Build Object Configuration For Send Fail Case 1: Create the SET string for a linked zone """
        zprog = self.create_test_zone_program_object_all_args()
        zprog.linked = True

        mock_zp = mock.MagicMock()

        zprog.zone_program_objects[1] = mock_zp

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            zprog.build_obj_configuration_for_send()

        e_msg = "Zone must be in the same program as the primary zone"

        # TODO Start here
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_send_programming_to_cn_pass1(self):
        """ Set Send Programming to Controller Pass Case 1: Correct Command Sent to 1000 controller """
        zprog = self.create_test_zone_program_object(con_type="10")
        expected_command = "SET," \
                           "PZ=1," \
                           "PG=1," \
                           "RT=240"

        zprog.send_programming_to_cn()
        self.mock_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_to_cn_pass2(self):
        """ Set Send Programming to Controller Pass Case 2: Correct Command Sent to 3200 controller """
        zprog = self.create_test_zone_program_object()
        expected_command = "SET," \
                           "PZ=1," \
                           "PG=1," \
                           "RT=240," \
                           "EE=FA,"\
                           "RA=100," \
                           "WS=TM," \
                           "LL=26.0," \
                           "UL=30.0,"\
                           "CC=NV"
        zprog.send_programming_to_cn()
        self.mock_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_to_cn_fail1(self):
        """ Set Send Programming to Controller Fail Case 1: Handle Exception Raised From Controller """
        ts = self.create_test_zone_program_object(con_type="10")
        expected_command = "SET," \
                           "PZ=1," \
                           "PG=1," \
                           "RT=240"

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            ts.send_programming_to_cn()
        e_msg = "Exception occurred trying to set Zone Program 1 for Program 1's 'Values' to: '{0}'\n -> " \
                .format(expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_run_time_on_cn_pass1(self):
        """ Set Run Time On Controller Pass Case 1: Using Default _rt value """
        zprog = self.create_test_zone_program_object()

        # Expected value is the value set at Zone program object creation
        expected_value = zprog.rt
        zprog.set_run_time_on_cn()

        #  value is set during this method and should equal the original value
        actual_value = zprog.rt
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_run_time_on_cn_pass2(self):
        """ Set Run Time On Controller Pass Case 2: Setting new _rt value = 6.0 """
        zprog = self.create_test_zone_program_object()

        # Expected value is 6
        expected_value = 6
        zprog.set_run_time_on_cn(expected_value)

        #  value is set during this method and should equal the value passed into the method
        actual_value = zprog.rt
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_run_time_on_cn_pass3(self):
        """ Set Run Time On Controller Pass Case 3: Command with correct values sent to controller """
        zprog = self.create_test_zone_program_object()

        rt_value = str(zprog.rt)
        expected_command = "SET,{0}=1,{1}=1,{2}={3}".format(opcodes.zone_program, opcodes.program,
                                                            opcodes.run_time, rt_value)
        zprog.set_run_time_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_run_time_on_cn_fail1(self):
        """ Set Run Time On Controller Fail Case 1: Unable to send command to controller """
        zprog = self.create_test_zone_program_object()
        _rt = 2

        self.mock_send_and_wait_for_reply.side_effect = AssertionError
        zprog.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        with self.assertRaises(Exception) as context:
            zprog.set_run_time_on_cn(_rt)

        expected_msg = "Exception occurred trying to set (Zone Program 1, Program 1)'s 'Run Time' to: {0}\n "\
            .format(_rt)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    # TODO reference
    #################################
    def test_set_cycle_time_on_cn_pass1(self):
        """ Set Cycle Time On Controller Pass Case 1: Using Default _ct value """
        zprog = self.create_test_zone_program_object()

        # Expected value is the  value set at zone program object's creation
        expected_value = zprog.ct
        zprog.set_cycle_time_on_cn()

        #  value is set during this method and should equal the original value
        actual_value = zprog.ct
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_cycle_time_on_cn_pass2(self):
        """ Set Cycle Time On Controller Pass Case 2: Setting new _ct value = 60 """
        zprog = self.create_test_zone_program_object()

        # Expected  value is 60
        expected_value = 60
        zprog.set_cycle_time_on_cn(90)

        #  value is set during this method and should equal the value passed into the method
        actual_value = zprog.ct
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_cycle_time_on_cn_pass3(self):
        """ Set Cycle Time On Controller Pass Case 3: Command with correct values sent to controller """
        zprog = self.create_test_zone_program_object()

        ct_value = str(zprog.ct)
        expected_command = "SET,{0}=1,{1}=1,{2}={3}".format(opcodes.zone_program, opcodes.program,
                                                            opcodes.cycle_time, ct_value)
        zprog.set_cycle_time_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_cycle_time_on_cn_pass4(self):
        """ Set Cycle Time On Controller Pass Case 4: Use calculated cycle time to send _ct to controller """
        zprog = self.create_test_zone_program_object()

        # Expected value that is passed into the mocked method
        expected_value = 60

        # Create a mock get_calculated_cycle_time method that returns a cycle time and assign it to the real method
        mock_get_calculated_cycle_time = mock.MagicMock(return_value=90)
        zprog.zone.get_calculated_cycle_time = mock_get_calculated_cycle_time

        zprog.set_cycle_time_on_cn(_use_calculated_cycle_time=True)

        #  value is set during this method and should equal the value passed into the method
        actual_value = zprog.ct
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_cycle_time_on_cn_fail1(self):
        """ Set Cycle Time On Controller Fail Case 1: Unable to send command to controller """
        zprog = self.create_test_zone_program_object()
        _ct = 20
        expected_value = 0  # Cycle time always cuts out the remainder of seconds that don't make up a full minute

        self.mock_send_and_wait_for_reply.side_effect = AssertionError
        zprog.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        with self.assertRaises(Exception) as context:
            zprog.set_cycle_time_on_cn(_ct)

        expected_msg = "Exception occurred trying to set (Zone Program 1, Program 1)'s 'Cycle Time' to: {0}\n "\
            .format(expected_value)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_cycle_time_on_cn_fail2(self):
        """ Set Cycle Time On Controller Fail Case 2: Try command on 1000 controller """
        zprog = self.create_test_zone_program_object(con_type="10")

        with self.assertRaises(ValueError) as context:
            zprog.set_cycle_time_on_cn(2)

        expected_msg = "Attempting to set Cycle Time for a Zone Program for 1000 which is NOT SUPPORTED"

        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_soak_time_on_cn_pass1(self):
        """ Set Soak Time On Controller Pass Case 1: Using Default _so value """
        zprog = self.create_test_zone_program_object()

        # Expected value is the  value set at zone program object's creation
        expected_value = zprog.so
        zprog.set_soak_time_on_cn()

        #  value is set during this method and should equal the original value
        actual_value = zprog.so
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_soak_time_on_cn_pass2(self):
        """ Set Soak Time On Controller Pass Case 2: Setting new _so value = 60 """
        zprog = self.create_test_zone_program_object()

        # Expected  value is 6
        expected_value = 60
        set_value = 6
        zprog.set_soak_time_on_cn(set_value)

        #  value is set during this method and should equal the value passed into the method
        actual_value = zprog.so
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_soak_time_on_cn_pass3(self):
        """ Set Soak Time On Controller Pass Case 3: Command with correct values sent to controller """
        zprog = self.create_test_zone_program_object()

        so_value = str(zprog.so)
        expected_command = "SET,{0}=1,{1}=1,{2}={3}".format(opcodes.zone_program, opcodes.program,
                                                            opcodes.soak_cycle, so_value)
        zprog.set_soak_time_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_soak_time_on_cn_pass4(self):
        """ Set Cycle Time On Controller Pass Case 4: Use calculated cycle time to send _ct to controller """
        zprog = self.create_test_zone_program_object()

        # Expected value that is passed into the mocked method
        expected_value = 60

        # Create a mock get_calculated_cycle_time method that returns a cycle time and assign it to the real method
        mock_get_calculated_soak_time = mock.MagicMock(return_value=30)
        zprog.zone.get_calculated_soak_time = mock_get_calculated_soak_time

        zprog.set_soak_time_on_cn(_use_calculated_soak_time=True)

        #  value is set during this method and should equal the value passed into the method
        actual_value = zprog.so
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_soak_time_on_cn_pass5(self):
        """ Set Cycle Time On Controller Pass Case 5: Use calculated cycle time to send _ct to controller as 0 """
        zprog = self.create_test_zone_program_object()

        # Expected value that is passed into the mocked method
        expected_value = 0

        zprog.set_soak_time_on_cn(_soak_time=0, _use_calculated_soak_time=False)

        actual_value = zprog.so

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_soak_time_on_cn_fail1(self):
        """ Set Soak Time On Controller Fail Case 1: Unable to send command to controller """
        zprog = self.create_test_zone_program_object()
        _so = 30
        expected_value = 60.0 # Soak time always rounds up to the nearest minute(in seconds)

        self.mock_send_and_wait_for_reply.side_effect = AssertionError
        zprog.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        with self.assertRaises(Exception) as context:
            zprog.set_soak_time_on_cn(_so)

        expected_msg = "Exception occurred trying to set (Zone Program 1, Program 1)'s 'Soak Time' to: {0}\n "\
            .format(expected_value)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_soak_time_on_cn_fail2(self):
        """ Set Soak Time On Controller Fail Case 2: Try command on 1000 controller """
        zprog = self.create_test_zone_program_object(con_type="10")

        with self.assertRaises(ValueError) as context:
            zprog.set_soak_time_on_cn(2)

        expected_msg = "Attempting to set Soak Time for a Zone Program for 1000 which is NOT SUPPORTED"

        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_zone_mode_on_cn_pass1(self):
        """ Set Zone Mode On Controller Pass Case 1: Using timed mode """
        zprog = self.create_test_zone_program_object()

        # Expected value is the value set at zone program object's creation
        expected_value = 0
        zprog.set_zone_mode_on_cn(opcodes.timed_mode)

        # value is set during this method and should equal the original value
        actual_value = zprog.pz
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_zone_mode_on_cn_pass2(self):
        """ Set Zone Mode On Controller Pass Case 2: Using primary mode """
        zprog = self.create_test_zone_program_object()

        # Expected value is the value set at zone program object's creation
        expected_value = 1
        zprog.set_zone_mode_on_cn(opcodes.primary_mode)

        # value is set during this method and should equal the original value
        actual_value = zprog.pz
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_zone_mode_on_cn_pass3(self):
        """ Set Zone Mode On Controller Pass Case 3: Using linked mode """
        zprog = self.create_test_zone_program_object()

        # Expected value is the value set at zone program object's creation
        expected_value = 1
        zprog.set_zone_mode_on_cn(opcodes.linked_mode, expected_value)

        # value is set during this method and should equal the original value
        actual_value = zprog.pz
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_zone_mode_on_cn_pass4(self):
        """ Set Zone Mode On Controller Pass Case 4: Command with correct values sent to controller """
        zprog = self.create_test_zone_program_object()

        pz_value = str(1)
        expected_command = "SET,{0}=1,{1}=1,{2}={3}".format(opcodes.zone_program, opcodes.program,
                                                            opcodes.primary_zone_number, pz_value)
        zprog.set_zone_mode_on_cn(opcodes.primary_mode)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_zone_mode_on_cn_fail1(self):
        """ Set Zone Mode On Controller Fail Case 1: Unable to send command to controller """
        zprog = self.create_test_zone_program_object()
        _pz = 1

        self.mock_send_and_wait_for_reply.side_effect = AssertionError
        zprog.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        with self.assertRaises(Exception) as context:
            zprog.set_zone_mode_on_cn(opcodes.primary_mode)

        expected_msg = "Exception occurred trying to set (Zone Program 1, Program 1)'s 'Primary Zone' to: {0}\n "\
            .format(_pz)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_zone_mode_on_cn_fail2(self):
        """ Set Zone Mode On Controller Fail Case 2: Try command on 1000 controller """
        zprog = self.create_test_zone_program_object(con_type="10")

        with self.assertRaises(ValueError) as context:
            zprog.set_zone_mode_on_cn(2)

        expected_msg = "Attempting to set Primary Zone for a Zone Program for 1000 which is NOT SUPPORTED"

        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_zone_mode_on_cn_fail3(self):
        """ Set Zone Mode On Controller Fail Case 3: Invalid mode 'CT' passed """
        zprog = self.create_test_zone_program_object()

        with self.assertRaises(Exception) as context:
            zprog.set_zone_mode_on_cn('CT')

        expected_msg = "Invalid zone 'Mode' entered for (Zone Program 1, Program 1. Received: CT, " \
                       "Expected 'opcodes.timed_mode', 'opcodes.linked_mode' or 'opcodes.primary_mode'."
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_zone_mode_on_cn_fail4(self):
        """ Set Zone Mode On Controller Fail Case 4: No linked address for linked mode """
        zprog = self.create_test_zone_program_object()

        with self.assertRaises(Exception) as context:
            zprog.set_zone_mode_on_cn(opcodes.linked_mode)

        expected_msg = "Missing arguments. Specified a 'LINKED' mode for (Zone Program 1, Program 1) but didn't " \
                       "provide a Zone address to link to as a second argument to the function call."
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_runtime_ratio_on_cn_pass1(self):
        """ Set Soak Time On Controller Pass Case 1: Using Default _ra value """
        zprog = self.create_test_zone_program_object()

        # Expected value is the value set at zone program object's creation
        expected_value = zprog.ra
        zprog.set_runtime_ratio_on_cn()

        # value is set during this method and should equal the original value
        actual_value = zprog.ra
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_runtime_ratio_on_cn_pass2(self):
        """ Set Soak Time On Controller Pass Case 2: Setting new _ra value = 6.0 """
        zprog = self.create_test_zone_program_object()

        # Expected value is 6
        expected_value = 6
        zprog.set_runtime_ratio_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = zprog.ra
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_runtime_ratio_on_cn_pass3(self):
        """ Set Soak Time On Controller Pass Case 3: Command with correct values sent to controller """
        zprog = self.create_test_zone_program_object()

        ra_value = str(zprog.ra)
        expected_command = "SET,{0}=1,{1}=1,{2}={3}".format(opcodes.zone_program, opcodes.program,
                                                            opcodes.runtime_tracking_ratio, ra_value)
        zprog.set_runtime_ratio_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_runtime_ratio_on_cn_fail1(self):
        """ Set Soak Time On Controller Fail Case 1: Unable to send command to controller """
        zprog = self.create_test_zone_program_object()
        _ra = 2

        self.mock_send_and_wait_for_reply.side_effect = AssertionError
        zprog.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        with self.assertRaises(Exception) as context:
            zprog.set_runtime_ratio_on_cn(_ra)

        expected_msg = "Exception occurred trying to set (Zone Program 1, Program 1)'s 'Runtime Ratio' to: {0}\n"\
            .format(_ra)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_runtime_ratio_on_cn_fail2(self):
        """ Set Soak Time On Controller Fail Case 2: Try command on 1000 controller """
        zprog = self.create_test_zone_program_object(con_type="10")

        with self.assertRaises(ValueError) as context:
            zprog.set_runtime_ratio_on_cn(2)

        expected_msg = "Attempting to set Runtime Ratio for a Zone Program for 1000 which is NOT SUPPORTED"

        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_water_strategy_on_cn_pass1(self):
        """ Set Soak Time On Controller Pass Case 1: Using Default _ws value """
        zprog = self.create_test_zone_program_object()

        # Expected value is the value set at zone program object's creation
        expected_value = zprog.ws
        zprog.set_water_strategy_on_cn()

        # value is set during this method and should equal the original value
        actual_value = zprog.ws
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_water_strategy_on_cn_pass2(self):
        """ Set Soak Time On Controller Pass Case 2: Setting new _ws value = LL """
        zprog = self.create_test_zone_program_object()

        # Expected value is LL
        expected_value = "LL"
        zprog.set_water_strategy_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = zprog.ws
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_water_strategy_on_cn_pass3(self):
        """ Set Soak Time On Controller Pass Case 3: Command with correct values sent to controller """
        zprog = self.create_test_zone_program_object()

        ws_value = opcodes.lower_limit
        expected_command = "SET,{0}=1,{1}=1,{2}={3}".format(opcodes.zone_program, opcodes.program,
                                                            opcodes.water_strategy, ws_value)
        zprog.set_water_strategy_on_cn("LL")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_strategy_on_cn_fail1(self):
        """ Set Soak Time On Controller Fail Case 1: Unable to send command to controller """
        zprog = self.create_test_zone_program_object()
        _ws = "LL"

        self.mock_send_and_wait_for_reply.side_effect = AssertionError
        zprog.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        with self.assertRaises(Exception) as context:
            zprog.set_water_strategy_on_cn(_ws)

        expected_msg = "Exception occurred trying to set (Zone Program 1, Program 1)'s 'Watering Strategy' to: {0}\n"\
            .format(_ws)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_water_strategy_on_cn_fail2(self):
        """ Set Soak Time On Controller Fail Case 2: Try command on 1000 controller """
        zprog = self.create_test_zone_program_object(con_type="10")

        with self.assertRaises(ValueError) as context:
            zprog.set_water_strategy_on_cn("LL")

        expected_msg = "Attempting to set 'Watering Strategy' for a Zone Program for 1000 which is NOT SUPPORTED"

        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_water_strategy_on_cn_fail3(self):
        """ Set Zone Mode On Controller Fail Case 3: Invalid mode 'CT' passed """
        zprog = self.create_test_zone_program_object()

        with self.assertRaises(Exception) as context:
            zprog.set_water_strategy_on_cn('CT')

        expected_msg = "Invalid 'Watering Strategy' value for 'SET' for Zone Program : 1, Program 1. Expects "\
                       "'TM', 'LL', 'UL', received: CT"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_primary_zone_moisture_sensor_on_cn_pass1(self):
        """ Set Primary Zone Moisture Sensor On Controller Pass Case 1: Passed in address of a present Mainline """
        zprog = self.create_test_zone_program_object()

        expected_value = 1
        zprog.set_primary_zone_moisture_sensor_on_cn(expected_value)

        this_obj = zprog.moisture_sensor_objects[1]
        actual_value = this_obj.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_primary_zone_moisture_sensor_on_cn_pass2(self):
        """ Set Primary Zone Moisture Sensor On Controller Pass Case 2: Command with correct values sent to cn """
        zprog = self.create_test_zone_program_object()

        this_obj = zprog.moisture_sensor_objects[1]
        sn_value = this_obj.sn
        ad_value = this_obj.ad
        expected_command = "SET,{0}=1,{1}=1,{2}={3}".format(opcodes.zone_program, opcodes.program,
                                                            opcodes.moisture_sensor, sn_value)

        zprog.set_primary_zone_moisture_sensor_on_cn(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_primary_zone_moisture_sensor_on_cn_fail1(self):
        """ Set Primary Zone Moisture Sensor On Controller Fail Case 1: Failed communication with controller """
        zprog = self.create_test_zone_program_object()
        this_obj = zprog.moisture_sensor_objects[1]
        sn_value = this_obj.sn
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = AssertionError

        with self.assertRaises(Exception) as context:
            zprog.set_primary_zone_moisture_sensor_on_cn(ad_value)
        e_msg = "Exception occurred trying to set (Zone Program 1, Program 1)'s 'Moisture Sensor Serial " \
                "Number' to: {0} ({1})\n".format(str(sn_value), str(ad_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_primary_zone_moisture_sensor_on_cn_fail2(self):
        """ Set Primary Zone Moisture Sensor On Controller Fail Case 2: Invalid moisture sensor address '5' passed """
        zprog = self.create_test_zone_program_object()

        with self.assertRaises(Exception) as context:
            zprog.set_primary_zone_moisture_sensor_on_cn(5)

        expected_msg = "Invalid Moisture Sensor address. " \
                       "Verify address exists in object json configuration and/or in " \
                       "current test. Received address: 5, available addresses: [1]"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_primary_zone_moisture_sensor_on_cn_fail3(self):
        """ Set Primary Zone Moisture Sensor On Controller Fail Case 3: Try command on 1000 controller """
        zprog = self.create_test_zone_program_object(con_type="10")

        with self.assertRaises(ValueError) as context:
            zprog.set_primary_zone_moisture_sensor_on_cn(1)

        expected_msg = "Attempting to set Moisture Sensor Serial Number for a Zone Program for 1000 which is " \
                       "NOT SUPPORTED"

        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_lower_limit_threshold_on_cn_pass1(self):
        """ Set Lower Limit Threshold On Controller Pass Case 1: Using Default _ll value """
        zprog = self.create_test_zone_program_object()

        # Expected value is the  value set at zone program object's creation
        expected_value = zprog.ll
        zprog.set_lower_limit_threshold_on_cn()

        #  value is set during this method and should equal the original value
        actual_value = zprog.ll
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_lower_limit_threshold_on_cn_pass2(self):
        """ Set Lower Limit Threshold On Controller Pass Case 2: Setting new _ll value = 6.0 """
        zprog = self.create_test_zone_program_object()

        # Expected value is 6
        expected_value = 6
        zprog.set_lower_limit_threshold_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = zprog.ll
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_lower_limit_threshold_on_cn_pass3(self):
        """ Set Lower Limit Threshold On Controller Pass Case 3: Command with correct values sent to controller """
        zprog = self.create_test_zone_program_object()

        ll_value = str(zprog.ll)
        expected_command = "SET,{0}=1,{1}=1,{2}={3}".format(opcodes.zone_program, opcodes.program,
                                                            opcodes.lower_limit, ll_value)
        zprog.set_lower_limit_threshold_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_lower_limit_threshold_on_cn_fail1(self):
        """ Set Lower Limit Threshold On Controller Fail Case 1: Unable to send command to controller """
        zprog = self.create_test_zone_program_object()
        _ll = 2

        self.mock_send_and_wait_for_reply.side_effect = AssertionError
        zprog.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        with self.assertRaises(Exception) as context:
            zprog.set_lower_limit_threshold_on_cn(_ll)

        expected_msg = "Exception occurred trying to set (Zone Program 1, Program 1)'s 'Lower Limit Threshold' " \
                       "to: {0}\n".format(_ll)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_lower_limit_threshold_on_cn_fail2(self):
        """ Set Lower Limit Threshold On Controller Fail Case 2: Try command on 1000 controller """
        zprog = self.create_test_zone_program_object(con_type="10")

        with self.assertRaises(ValueError) as context:
            zprog.set_lower_limit_threshold_on_cn(2)

        expected_msg = "Attempting to set Lower Limit Threshold for a Zone Program for 1000 which is NOT SUPPORTED"

        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_upper_limit_threshold_on_cn_pass1(self):
        """ Set Upper Limit Threshold On Controller Pass Case 1: Using Default _ul value """
        zprog = self.create_test_zone_program_object()

        # Expected value is the  value set at zone program object's creation
        expected_value = zprog.ul
        zprog.set_upper_limit_threshold_on_cn()

        #  value is set during this method and should equal the original value
        actual_value = zprog.ul
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_upper_limit_threshold_on_cn_pass2(self):
        """ Set Upper Limit Threshold On Controller Pass Case 2: Setting new _ul value = 6.0 """
        zprog = self.create_test_zone_program_object()

        # Expected value is 6
        expected_value = 6.0
        zprog.set_upper_limit_threshold_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = zprog.ul
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_upper_limit_threshold_on_cn_pass3(self):
        """ Set Upper Limit Threshold On Controller Pass Case 3: Command with correct values sent to controller """
        zprog = self.create_test_zone_program_object()

        ul_value = str(zprog.ul)
        expected_command = "SET,{0}=1,{1}=1,{2}={3}".format(opcodes.zone_program, opcodes.program,
                                                            opcodes.upper_limit, ul_value)
        zprog.set_upper_limit_threshold_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_upper_limit_threshold_on_cn_fail1(self):
        """ Set Upper Limit Threshold On Controller Fail Case 1: Unable to send command to controller """
        zprog = self.create_test_zone_program_object()
        _ul = 2.0

        self.mock_send_and_wait_for_reply.side_effect = AssertionError
        zprog.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        with self.assertRaises(Exception) as context:
            zprog.set_upper_limit_threshold_on_cn(_ul)

        expected_msg = "Exception occurred trying to set (Zone Program 1, Program 1)'s 'Upper Limit Threshold' " \
                       "to: {0}\n".format(_ul)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_upper_limit_threshold_on_cn_fail2(self):
        """ Set Upper Limit Threshold On Controller Fail Case 2: Try command on 1000 controller """
        zprog = self.create_test_zone_program_object(con_type="10")

        with self.assertRaises(ValueError) as context:
            zprog.set_upper_limit_threshold_on_cn(2)

        expected_msg = "Attempting to set Upper Limit Threshold for a Zone Program for 1000 which is NOT SUPPORTED"

        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_calibration_cycle_on_cn_pass1(self):
        """ Set Calibration Cycle On Controller Pass Case 1: Using Default _cc value """
        zprog = self.create_test_zone_program_object()

        # Expected value is the value set at zone program object's creation
        expected_value = zprog.cc
        zprog.set_calibration_cycle_on_cn()

        # value is set during this method and should equal the original value
        actual_value = zprog.cc
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_calibration_cycle_on_cn_pass2(self):
        """ Set Calibration Cycle On Controller Pass Case 2: Setting new _cc value = LL """
        zprog = self.create_test_zone_program_object()

        expected_value = "NV"
        zprog.set_calibration_cycle_on_cn(expected_value)

        actual_value = zprog.cc
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_calibration_cycle_on_cn_pass3(self):
        """ Set Calibration Cycle On Controller Pass Case 3: Command with correct values sent to controller """
        zprog = self.create_test_zone_program_object()

        cc_value = opcodes.calibrate_never
        expected_command = "SET,{0}=1,{1}=1,{2}={3}".format(opcodes.zone_program, opcodes.program,
                                                            opcodes.calibrate_cycle, cc_value)
        zprog.set_calibration_cycle_on_cn("NV")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_calibration_cycle_on_cn_fail1(self):
        """ Set Calibration Cycle On Controller Fail Case 1: Unable to send command to controller """
        zprog = self.create_test_zone_program_object()
        _cc = "NV"

        self.mock_send_and_wait_for_reply.side_effect = AssertionError
        zprog.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        with self.assertRaises(Exception) as context:
            zprog.set_calibration_cycle_on_cn(_cc)

        expected_msg = "Exception occurred trying to set (Zone Program 1, Program 1)'s 'Calibration Cycle' to: {0}\n"\
            .format(_cc)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_calibration_cycle_on_cn_fail2(self):
        """ Set Calibration Cycle On Controller Fail Case 2: Try command on 1000 controller """
        zprog = self.create_test_zone_program_object(con_type="10")

        with self.assertRaises(ValueError) as context:
            zprog.set_calibration_cycle_on_cn("LL")

        expected_msg = "Attempting to set Calibration Cycle for a Zone Program for 1000 which is NOT SUPPORTED"

        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_calibration_cycle_on_cn_fail3(self):
        """ Set Calibration Cycle On Controller Fail Case 3: Invalid mode 'CT' passed """
        zprog = self.create_test_zone_program_object()

        with self.assertRaises(Exception) as context:
            zprog.set_calibration_cycle_on_cn('CT')

        expected_msg = "Invalid 'Calibration Cycle' value for 'SET' for Zone Program : 1, Program 1. Expects 'NV', " \
                       "'SG' or 'MO', received: CT"
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_enable_et_state_on_cn_pass1(self):
        """ Set Enable State For ET on Controller Pass Case 1: Using default _state value """
        zprog = self.create_test_zone_program_object()

        # Expected value is the  value set at zone program object's creation
        expected_value = "FA"
        zprog.set_enable_et_state_on_cn()

        #  value is set during this method and should equal the original value
        actual_value = zprog.ee
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_enable_et_state_on_cn_pass2(self):
        """ Set Enable State For ET on Controller Pass Case 2: Using _state="TR" value """
        zprog = self.create_test_zone_program_object()

        # Expected value is the  value set at zone program object's creation
        expected_value = "TR"
        zprog.set_enable_et_state_on_cn(expected_value)

        #  value is set during this method and should equal the original value
        actual_value = zprog.ee
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_enable_et_state_on_cn_pass3(self):
        """ Set Enable State For ET on Controller Pass Case 3: Command with correct values sent to controller """
        zprog = self.create_test_zone_program_object()

        et_value = zprog.ee
        expected_command = "SET,{0}=1,{1}=1,{2}={3}".format(
            opcodes.zone_program,                   # {0}
            opcodes.program,                        # {1}
            WaterSenseCodes.Enable_ET_Runtime,      # {2}
            zprog.ee                                # {3}
        )
        zprog.set_enable_et_state_on_cn(et_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_et_state_on_cn_fail1(self):
        """ Set Enable State For ET on Controller Fail Case 1: Try command on 1000 controller"""
        zprog = self.create_test_zone_program_object(con_type="10")

        with self.assertRaises(ValueError) as context:
            zprog.set_enable_et_state_on_cn()

        expected_msg = "Attempting to set Enable ET Runtime for a Zone Program for 1000 which is NOT SUPPORTED"

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_enable_et_state_on_cn_fail2(self):
        """ Set Enable State For ET on Controller Fail Case 2: Unable to send command to controller """
        zprog = self.create_test_zone_program_object()
        ee = opcodes.true

        self.mock_send_and_wait_for_reply.side_effect = AssertionError
        zprog.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        with self.assertRaises(Exception) as context:
            zprog.set_enable_et_state_on_cn(ee)

        expected_msg = "Exception occurred trying to set (Zone Program 1, Program 1)'s 'Enable ET Runtime' " \
                       "to: {0}\n".format(ee)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_enable_et_state_on_cn_fail3(self):
        """ Set Enable State For ET on Controller Fail Case 3: Invalid ee value"""
        zprog = self.create_test_zone_program_object()
        invalid_ee = "123"
        zprog.ee = invalid_ee

        with self.assertRaises(ValueError) as context:
            zprog.set_enable_et_state_on_cn()

        expected_msg = "Invalid 'Enable ET Runtime state' value for 'SET' for Zone Program : 1, Program 1. " \
                       "Expects 'TR',or 'FA', received: {0}".format(invalid_ee)
        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_enable_et_state_on_cn_fail4(self):
        """ Set Enable State For ET on Controller Fail Case 4: Invalid state value"""
        zprog = self.create_test_zone_program_object()
        invalid_state = "123"

        with self.assertRaises(ValueError) as context:
            zprog.set_enable_et_state_on_cn(invalid_state)

        expected_msg = "Invalid enabled state entered for Zone 1. Received: {0}, Expected: 'TR' or 'FA'".format(
            invalid_state)

        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_set_message_on_cn_pass1(self):
        """Set message on controller Pass Case 1: build valid string"""
        zprog = self.create_test_zone_program_object()
        expected_command = 'SET,MG,ZN=1,SS=CS,PG=1,DV=SB01429'

        with mock.patch('common.objects.base_classes.messages.message_to_set', return_value=expected_command):
            zprog.set_message_on_cn(_status_code='CS')

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_message_on_cn_fail1(self):
        """ Set message on controller Fail Case 1: Don't build valid string """
        zprog = self.create_test_zone_program_object()
        unexpected_command = 'SET,MG,ZN=1,SS=CS,PG=1,DV=SB01429'

        self.mock_send_and_wait_for_reply.side_effect = Exception

        expected_message = "Exception caught in messages.set_message: "

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_set', return_value=unexpected_command):
                zprog.set_message_on_cn(_status_code='CS')

        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_get_message_on_cn_pass1(self):
        """ Get message on controller Pass Case 1: Sets message on Controller """
        zprog = self.create_test_zone_program_object()
        expected_command = 'SET,MG,ZN=1,SS=CS,PG=1,DV=SB01429'

        self.mock_get_and_wait_for_reply.return_value = "Hello"

        with mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: expected_command}):
            ret_val = zprog.get_message_on_cn(_status_code='CS')

        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)
        self.assertEqual("Hello", ret_val)

    #################################
    def test_get_message_on_cn_fail1(self):
        """ Get message on controller Fail Case 1: Brings up error when getting message """
        zprog = self.create_test_zone_program_object()
        expected_command = 'SET,MG,ZN=1,SS=CS,PG=1,DV=SB01429'

        self.mock_get_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: expected_command}):
                zprog.get_message_on_cn(_status_code='CS')

        expected_message = "Exception caught in messages.get_message: "

        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_clear_message_on_cn_pass1(self):
        """ Clears message on the controller Pass Case 1: Verifies the message was cleared successfully """
        zprog = self.create_test_zone_program_object()  # create zone program object
        expected_command = 'DO,MG,Zn=1,SS=CF,PG=1,Dv=SB01429,V1=29.7'  # passed into the send and get methods

        # Raises an exception 'NM No Message Found' when mock method is called
        self.mock_get_and_wait_for_reply.side_effect = Exception("NM No Message Found")

        # Dives into the original method in zp (zone programs), runs the method
        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value=expected_command), \
             mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: expected_command}):
            zprog.clear_message_on_cn(_status_code=opcodes.calibrate_successful)

        # Checks and sees whether the expected command is within the arguments
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

        # Checks whether the expected command was passed in as a method argument
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_clear_message_on_cn_pass2(self):
        """ Clears message on the controller Pass Case 2: Verifies the message was not cleared """
        zprog = self.create_test_zone_program_object()

        self.mock_get_and_wait_for_reply.side_effect = Exception("Arbitrary String")

        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value="DO,MG,Zn=1,SS=CF,PG=1,Dv=SB01429,V1=29.7"), \
             mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: "DO,MG,Zn=1,SS=CF,PG=1,Dv=SB01429,V1=29.7"}):
            zprog.clear_message_on_cn(_status_code=opcodes.calibrate_successful)

        self.assertRaises(Exception, self.mock_get_and_wait_for_reply)

    #################################
    def test_clear_message_on_cn_fail1(self):
        """ Clear Message On Controller Fail Case 1: message_to_clear throws an exception """
        zprog = self.create_test_zone_program_object()

        expected_message = "Exception caught in messages.clear_message: "
        self.mock_send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value="DO,MG,Zn=1,SS=CF,PG=1,Dv=SB01429,V1=29.7"):
                zprog.clear_message_on_cn(_status_code='CF')

        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_get_data_pass1(self):
        """ Set Get Data Pass Case 1: Command with correct values sent to controller """
        zprog = self.create_test_zone_program_object()

        expected_command = "GET,{0}=1,{1}=1".format(opcodes.zone_program, opcodes.program)
        zprog.get_data()
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_data_fail1(self):
        """ Set Calibration Cycle On Controller Fail Case 1: Unable to send command to controller """
        zprog = self.create_test_zone_program_object()

        expected_command = "GET,{0}=1,{1}=1".format(opcodes.zone_program, opcodes.program)

        self.mock_get_and_wait_for_reply.side_effect = AssertionError
        zprog.ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        with self.assertRaises(Exception) as context:
            zprog.get_data()

        expected_msg = "Unable to get data for (Zone Program: 1, Program 1) using command: '{0}'. " \
                       "Exception raised: ".format(expected_command)

        self.assertEqual(first=expected_msg, second=context.exception.message)

    #################################
    def test_verify_runtime_on_cn_pass1(self):
        """ Verify Runtime On Controller Pass Case 1: Exception is not raised """
        zprog = self.create_test_zone_program_object()
        zprog.rt = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.run_time))
        zprog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                zprog.verify_runtime_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_runtime_on_cn_pass2(self):
        """ Verify Runtime On Controller Pass Case 2: Exception is not raised and value is calculated with equations"""
        zprog = self.create_test_zone_program_object()
        zprog.ee = opcodes.true
        expected_value = 60

        # Mock the equations method and assign it to what it is mocking
        mock_return_todays_calculated_runtime = mock.MagicMock(return_value=expected_value)
        equations.return_todays_calculated_runtime = mock_return_todays_calculated_runtime

        # Mock data and assign it to what it mocks
        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=60".format(opcodes.run_time))
        zprog.data = mock_data

        zprog.verify_runtime_on_cn()
        actual_value = zprog.rt
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_verify_runtime_on_cn_pass3(self):
        """ Verify Runtime On Controller Pass Case 3: Exception is not raised for bs1000"""
        zprog = self.create_test_zone_program_object(con_type="10")
        zprog.program.ee = opcodes.true
        expected_value = 60

        # Mock the equations method and assign it to what it is mocking
        mock_return_todays_calculated_runtime = mock.MagicMock(return_value=expected_value)
        equations.return_todays_calculated_runtime = mock_return_todays_calculated_runtime

        # Mock data and assign it to what it mocks
        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=60".format(opcodes.run_time))
        zprog.data = mock_data

        zprog.verify_runtime_on_cn()
        actual_value = zprog.rt
        self.assertEqual(expected_value, actual_value)


    #################################
    def test_verify_runtime_on_cn_fail1(self):
        """ Verify Runtime On Controller Pass Case 1: Calculated runtime not equal to runtime from controller"""
        zprog = self.create_test_zone_program_object()
        zprog.ee = opcodes.true
        expected_value = 60

        # Mock the equations method and assign it to what it is mocking
        mock_return_todays_calculated_runtime = mock.MagicMock(return_value=expected_value)
        equations.return_todays_calculated_runtime = mock_return_todays_calculated_runtime

        # Mock data and assign it to what it mocks
        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=50".format(opcodes.run_time))
        zprog.data = mock_data

        with self.assertRaises(ValueError) as context:
            zprog.verify_runtime_on_cn()

        expected_msg = "Unable to verify (Zone Program 1, Program 1)'s 'Runtime' value. Received: {0}, " \
                       "Expected: {1}".format(str(50), str(expected_value))

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_cycle_time_on_cn_pass1(self):
        """ Verify Cycle Time On Controller Pass Case 1: Exception is not raised """
        zprog = self.create_test_zone_program_object()
        zprog.ct = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.cycle_time))
        zprog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                zprog.verify_cycle_time_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_cycle_time_on_cn_pass2(self):
        """ Verify Cycle Time On Controller Pass Case 2: TODO """
        zprog = self.create_test_zone_program_object()
        zprog.ee = opcodes.true
        zprog.ct = 150
        expected_value = 240

        # Mock data(cycle_time is 240) and assign it to what it mocks
        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=240".format(opcodes.cycle_time))
        zprog.data = mock_data

        zprog.verify_cycle_time_on_cn()

        self.assertEqual(expected_value, 240)

    #################################
    def test_verify_cycle_time_on_cn_fail1(self):
        """ Verify Cycle Time On Controller Fail Case 1: Value on controller does not match what is
        stored in zprog.ct """
        zprog = self.create_test_zone_program_object()
        zprog.ct = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.cycle_time))
        zprog.data = mock_data

        expected_message = "Unable to verify (Zone Program 1, Program 1)'s 'Cycle Time' value. Received: 6, " \
                           "Expected: 5"
        try:
            zprog.verify_cycle_time_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_soak_time_on_cn_pass1(self):
        """ Verify Soak Time On Controller Pass Case 1: Exception is not raised """
        zprog = self.create_test_zone_program_object()
        zprog.so = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.soak_cycle))
        zprog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                zprog.verify_soak_time_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_soak_time_on_cn_fail1(self):
        """ Verify Soak Time On Controller Fail Case 1: Value on controller does not match what is
        stored in zprog.so """
        zprog = self.create_test_zone_program_object()
        zprog.so = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.soak_cycle))
        zprog.data = mock_data

        expected_message = "Unable to verify (Zone Program 1, Program 1)'s 'Soak Time' value. Received: 6, " \
                           "Expected: 5"
        try:
            zprog.verify_soak_time_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_primary_zone_number_on_cn_pass1(self):
        """ Verify Soak Time On Controller Pass Case 1: Exception is not raised """
        zprog = self.create_test_zone_program_object()
        zprog.pz = 1
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=1".format(opcodes.zone_program))
        zprog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                zprog.verify_primary_zone_number_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_primary_zone_number_on_cn_fail1(self):
        """ Verify Soak Time On Controller Fail Case 1: Value on controller does not match what is
        stored in zprog.pz """
        zprog = self.create_test_zone_program_object()
        zprog.pz = 1
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=2".format(opcodes.zone_program))
        zprog.data = mock_data

        expected_message = "Unable to verify (Zone Program 1, Program 1)'s 'Primary Zone Number' value. Received: 2, " \
                           "Expected: 1"
        try:
            zprog.verify_primary_zone_number_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_runtime_tracking_ratio_on_cn_pass1(self):
        """ Verify Soak Time On Controller Pass Case 1: Exception is not raised """
        zprog = self.create_test_zone_program_object()
        zprog.ra = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.runtime_tracking_ratio))
        zprog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                zprog.verify_runtime_tracking_ratio_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_runtime_tracking_ratio_on_cn_fail1(self):
        """ Verify Soak Time On Controller Fail Case 1: Value on controller does not match what is
        stored in zprog.ra """
        zprog = self.create_test_zone_program_object()
        zprog.ra = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.runtime_tracking_ratio))
        zprog.data = mock_data

        expected_message = "Unable to verify (Zone Program 1, Program 1)'s 'Runtime Tracking Ratio' value. " \
                           "Received: 6, Expected: 5"
        try:
            zprog.verify_runtime_tracking_ratio_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_water_strategy_on_cn_pass1(self):
        """ Verify Water Strategy On Controller Pass Case 1: Exception is not raised """
        zprog = self.create_test_zone_program_object()
        zprog.ws = "LL"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=LL".format(opcodes.water_strategy))
        zprog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                zprog.verify_water_strategy_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_water_strategy_on_cn_fail1(self):
        """ Verify Water Strategy On Controller Fail Case 1: Value on controller does not match what is
        stored in zprog.ws """
        zprog = self.create_test_zone_program_object()
        zprog.ws = "LL"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Ll".format(opcodes.water_strategy))
        zprog.data = mock_data

        expected_message = "Unable to verify (Zone Program 1, Program 1)'s 'Water Strategy' value. Received: Ll" \
                           ", Expected: LL"
        try:
            zprog.verify_water_strategy_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_primary_zone_moisture_sensor_on_cn_pass1(self):
        """ Verify Primary Zone Moisture Sensor On Controller Pass Case 1: Exception is not raised """
        zprog = self.create_test_zone_program_object()
        zprog.ms = 1
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=MSD0001".format(opcodes.moisture_sensor))
        zprog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                zprog.verify_primary_zone_moisture_sensor_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_primary_zone_moisture_sensor_on_cn_fail1(self):
        """ Verify Primary Zone Moisture Sensor On Controller Fail Case 1: Value on controller does not match what is
        stored in sn for the Zone Program """
        zprog = self.create_test_zone_program_object()
        zprog.ms = 1
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=MSD0002".format(opcodes.moisture_sensor))
        zprog.data = mock_data

        expected_message = "Unable to verify (Zone Program 1, Program 1)'s 'Primary Zone Moisture Sensor' value. " \
                           "Received: MSD0002, Expected: MSD0001"
        try:
            zprog.verify_primary_zone_moisture_sensor_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_lower_limit_threshold_on_cn_pass1(self):
        """ Verify Lower Limit On Controller Pass Case 1: Exception is not raised """
        zprog = self.create_test_zone_program_object()
        zprog.ll = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.lower_limit))
        zprog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                zprog.verify_lower_limit_threshold_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_lower_limit_threshold_on_cn_fail1(self):
        """ Verify Lower Limit On Controller Fail Case 1: Value on controller does not match what is
        stored in zprog.ll """
        zprog = self.create_test_zone_program_object()
        zprog.ll = 5.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.lower_limit))
        zprog.data = mock_data

        expected_message = "Unable to verify (Zone Program 1, Program 1)'s 'Lower Limit Threshold' value. " \
                           "Received: 6.0, Expected: 5.0"
        try:
            zprog.verify_lower_limit_threshold_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_upper_limit_threshold_on_cn_pass1(self):
        """ Verify Upper Limit On Controller Pass Case 1: Exception is not raised """
        zprog = self.create_test_zone_program_object()
        zprog.ul = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.upper_limit))
        zprog.data = mock_data

        try:
            # assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                zprog.verify_upper_limit_threshold_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_upper_limit_threshold_on_cn_fail1(self):
        """ Verify Upper Limit On Controller Fail Case 1: Value on controller does not match what is
        stored in zprog.ul """
        zprog = self.create_test_zone_program_object()
        zprog.ul = 5.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.upper_limit))
        zprog.data = mock_data

        expected_message = "Unable to verify (Zone Program 1, Program 1)'s 'Upper Limit Threshold' value. " \
                           "Received: 6.0, Expected: 5.0"
        try:
            zprog.verify_upper_limit_threshold_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_calibrate_cycle_on_cn_pass1(self):
        """ Verify Calibrate Cycle On Controller Pass Case 1: Exception is not raised """
        zprog = self.create_test_zone_program_object()
        zprog.cc = "NV"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=NV".format(opcodes.calibrate_cycle))
        zprog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                zprog.verify_calibrate_cycle_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_calibrate_cycle_on_cn_fail1(self):
        """ Verify Calibrate Cycle On Controller Fail Case 1: Value on controller does not match what is
        stored in zprog.cc """
        zprog = self.create_test_zone_program_object()
        zprog.cc = "NV"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Nv".format(opcodes.calibrate_cycle))
        zprog.data = mock_data

        expected_message = "Unable to verify (Zone Program 1, Program 1)'s 'Calibrate Cycle' value. Received: Nv" \
                           ", Expected: NV"
        try:
            zprog.verify_calibrate_cycle_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))


    #################################
    def test_verify_enable_et_state_on_cn_pass1(self):
        """ Verify Enable ET state On Controller Pass Case 1: Exception is not raised - Verified successfully"""
        zprog = self.create_test_zone_program_object()
        zprog.ee = opcodes.true
        test_pass = False

        mock_data = status_parser.KeyValues("RT=900,CT=300,SO=300,EE=TR")
        zprog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                zprog.verify_enable_et_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")


    #################################
    def test_verify_enable_et_state_on_cn_fail1(self):
        """ Verify Calibrate Cycle On Controller Fail Case 1: Value on controller does not match what is
        stored in zprog.cc """
        zprog = self.create_test_zone_program_object()
        zprog.ee = opcodes.true
        test_pass = False

        mock_data = status_parser.KeyValues("RT=900,CT=300,SO=300,EE=FA")
        zprog.data = mock_data

        expected_message = "Unable to verify (Zone Program 1, Program 1)'s 'Enable Et Runtime State' value. Received: FA" \
                           ", Expected: TR"

        e_msg = ""

        with self.assertRaises(ValueError) as context:
            zprog.verify_enable_et_state_on_cn()

        self.assertEqual(first=context.exception.message, second=expected_message)

    # #################################
    def test_verify_message_on_cn_pass1(self):
        """ Verify Message On Controller Pass 1: Verifies the correct id, tx, and date strings are returned """
        # Create zone program object
        zprog = self.create_test_zone_program_object()
        dict_of_values = {opcodes.message_text: "Message TX",
                          opcodes.message_id: "Message ID",
                          opcodes.date_time: "06/29/96 03:54:51"}
        # Mock the get value string method using side_effect to mock multiple outcomes from the same method
        def side_effect(*args, **kwargs):
            return dict_of_values[args[0]]

        mg_on_cn_mock = mock.MagicMock()
        key_value_mock = mock.MagicMock()
        key_value_mock.side_effect = side_effect
        mg_on_cn_mock.get_value_string_by_key = key_value_mock

        # Mocks the get message on controller method, and assign it to return another mock object
        get_message_mock = mock.MagicMock(return_value=mg_on_cn_mock)
        zprog.get_message_on_cn = get_message_mock

        # Initialize 'build_message_string' since it isn't initialized in the method due to our mock
        zprog.build_message_string = dict_of_values

        # Call the method we are testing
        zprog.verify_message_on_cn(_status_code=opcodes.calibrate_successful)

    def test_verify_message_on_cn_fail1(self):
        """ Verify the message on the controller Fail Case 1: Controller ID string does not match built string """
        # Create zone program object

        zprog = self.create_test_zone_program_object()

        # Mock all of the 'get value string by key's

        dict_of_values = {opcodes.message_text: "Message TX",
                          opcodes.message_id: "Message ID",
                          opcodes.date_time: "06/29/96 03:54:51"}

        # Mock the get value string method using side_effect to mock multiple outcomes from the same method
        def side_effect(*args, **kwargs):
            return dict_of_values[args[0]]

        mg_on_cn_mock = mock.MagicMock()
        key_value_mock = mock.MagicMock()
        key_value_mock.side_effect = side_effect
        mg_on_cn_mock.get_value_string_by_key = key_value_mock

        # Mocks the get message on controller method, and assign it to return another mock object
        get_message_mock = mock.MagicMock(return_value=mg_on_cn_mock)
        zprog.get_message_on_cn = get_message_mock

        # Initialize 'build_message_string' since it isn't initialized in the method due to our mock
        zprog.build_message_string = {opcodes.message_text: "Message TX",
                                      opcodes.message_id: "Wrong Message ID",
                                      opcodes.date_time: "06/29/96 03:54:51"}

        # Assign error message to match error message in original program

        expected_message = "Created ID message did not match the ID received from the controller:\n" \
                            "\tCreated: \t\t'{0}'\n" \
                            "\tReceived:\t\t'{1}'\n".format(
                                "Wrong Message ID",  # {0} The ID that was built
                                "Message ID"         # {1} The ID returned from controller
                            )

        # Check to see that this error message equals expected error message
        with self.assertRaises(ValueError) as context:
            zprog.verify_message_on_cn(_status_code=opcodes.calibrate_successful)

        self.assertEqual(context.exception.message, expected_message)

    # TODO Finish the fail cases and relook at the pass case

    #################################
    def test_who_i_am_pass1(self):
        """
        Test Who I Am with PZ == zone_ad_ptr
        """
        zprog = zp.ZoneProgram(zone_obj=zones[1], prog_obj=programs32[1], _rt=240, _ct=60, _so=100, _pz=1, _ra=100, _ws="LL",
                               _ms=1, _ll=30, _ul=70, _cc="NV")

        mock_data = status_parser.KeyValues("SET,"
                                            "PZ=1,"
                                            "PG=1,"
                                            "RT=240,"
                                            "RA=100,"
                                            "WS=LL,"
                                            "CC=NV,"
                                            "CT=60,"
                                            "SO=100,"
                                            "LL=30,"
                                            "UL=70,"
                                            "MS=MSD0001,"
                                            "EE=FA")

        zprog.ser.get_and_wait_for_reply = mock.MagicMock(side_effect=[mock_data])
        zprog.verify_who_i_am()

    if __name__ == "__main__":
        unittest.main()