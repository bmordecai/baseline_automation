import unittest
import mock
import serial
import status_parser

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
# you have to set the lat and long after the devices class is called so that they don't get skipped over
Devices.controller_lat = float(43.609768)
Devices.controller_long = float(-116.310569)
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.mv import MasterValve
__author__ = 'Brice "Ajo Grande" Garlick'


class TestMasterValveObject(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.309759

    Master Valve Starting lat & lng:
    latitude = 43.609768
    longitude = -116.310359
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        Devices.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_master_valve_object(self, _diffserial=None, _diffaddress=None):
        """ Creates a new Master Valve object for testing purposes """
        if _diffserial is not None:
            mv = MasterValve(_serial="TSD0001", _address=_diffaddress)
        elif _diffaddress is not None:
            mv = MasterValve(_serial=_diffserial, _address=1)
        else:
            mv = MasterValve(_serial="TSD0001", _address=1)

        return mv

    #################################
    def test_set_default_values_pass1(self):
        """ Set Default Values On Moisture Sensor Pass Case 1: Correct Command Sent """
        expected_command = "SET," \
                           "MV=TSD0001," \
                           "EN=TR," \
                           "DS=TSD0001 Test Master Valve 1," \
                           "LA=43.609773," \
                           "LG=-116.309964," \
                           "NO=FA," \
                           "VT=0.0"
        mv = self.create_test_master_valve_object()
        mv.set_default_values()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_pass2(self):
        """ Set Default Values On Moisture Sensor Pass Case 1: Correct Command Sent on a 3200 """
        expected_command = "SET," \
                           "MV=TSD0001," \
                           "EN=TR," \
                           "DS=TSD0001 Test Master Valve 1," \
                           "LA=43.609773," \
                           "LG=-116.309964," \
                           "NO=FA," \
                           "VT=0.0," \
                           "BP=FA"
        mv = self.create_test_master_valve_object()
        MasterValve.controller_type = "32"
        mv.set_default_values()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values On Moisture Sensor Fail Case 1: Failed communication with controller """
        expected_command = "SET," \
                           "MV=TSD0001," \
                           "EN=TR," \
                           "DS=TSD0001 Test Master Valve 1," \
                           "LA=43.609773," \
                           "LG=-116.309964," \
                           "NO=FA," \
                           "VT=0.0"
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            ms = self.create_test_master_valve_object()
            ms.set_default_values()
        e_msg = "Exception occurred trying to set Master Valve TSD0001 (1)'s 'Default values' to: '{0}'".format(
                expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_enable_state_pass1(self):
        """ Set Enable State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=TSD0001,{1}=TR".format(opcodes.master_valve, opcodes.enabled)
        mv = self.create_test_master_valve_object()
        mv.set_enable_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_state_pass2(self):
        """ Set Enable State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=TSD0001,{1}=FA".format(opcodes.master_valve, opcodes.enabled)
        mv = self.create_test_master_valve_object()
        mv.set_enable_state_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_state_fail1(self):
        """ Set Enable State On Controller Fail Case 1: Invalid State Argument """
        mv = self.create_test_master_valve_object()
        with self.assertRaises(ValueError) as context:
            mv.set_enable_state_on_cn("Foo")
        e_msg = "Invalid enabled state for Master Valve TSD0001 (1). Received: Foo, expected: 'TR' or 'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_enable_state_fail2(self):
        """ Set Enable State On Controller Fail Case 2: Failed Communication With Controller """
        mv = self.create_test_master_valve_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            mv.set_enable_state_on_cn("FA")
        e_msg = "Exception occurred trying to set enabled state for master valve TSD0001 (1)"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_normally_open_state_on_cn_pass1(self):
        """ Set Normally Open State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=TSD0001,{1}=FA".format(opcodes.master_valve, opcodes.normally_open)
        mv = self.create_test_master_valve_object()
        mv.set_normally_open_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_normally_open_state_on_cn_pass2(self):
        """ Set Normally Open State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=TSD0001,{1}=TR".format(opcodes.master_valve, opcodes.normally_open)
        mv = self.create_test_master_valve_object()
        mv.set_normally_open_state_on_cn("TR")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_normally_open_state_on_cn_fail1(self):
        """ Set Normally Open State On Controller Fail Case 1: Invalid State Argument """
        mv = self.create_test_master_valve_object()
        with self.assertRaises(ValueError) as context:
            mv.set_normally_open_state_on_cn("Foo")
        e_msg = "Invalid normally open state for Master Valve TSD0001 (1). Received: Foo, expected: 'TR' or 'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_normally_open_state_on_cn_fail2(self):
        """ Set Normally Open State On Controller Fail Case 2: Failed Communication With Controller """
        mv = self.create_test_master_valve_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            mv.set_normally_open_state_on_cn("FA")
        e_msg = "Exception occurred trying to set Master Valve TSD0001 (1)'s normally open to FA"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass1(self):
        """ Set Two Wire Drop Value On Controller Pass Case 1: Using Default _vt value """
        mv = self.create_test_master_valve_object()

        # Expected value is the _vt value set at object Zone object creation
        expected_value = mv.vt
        mv.set_two_wire_drop_value_on_cn()

        # _vt value is set during this method and should equal the original value
        actual_value = mv.vt
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass2(self):
        """ Set Two Wire Drop Value On Controller Pass Case 2: Setting new _vt value = 6.0 """
        mv = self.create_test_master_valve_object()

        # Expected _vt value is 6
        expected_value = 6.0
        mv.set_two_wire_drop_value_on_cn(expected_value)

        # _vt value is set during this method and should equal the value passed into the method
        actual_value = mv.vt
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass3(self):
        """ Set Two Wire Drop Value On Controller Pass Case 3: Command with correct values sent to controller """
        mv = self.create_test_master_valve_object()

        vt_value = str(mv.vt)
        expected_command = "SET,{0}=TSD0001,{1}={2}".format(opcodes.master_valve, opcodes.two_wire_drop, vt_value)
        mv.set_two_wire_drop_value_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_two_wire_drop_value_on_cn_fail1(self):
        """ Set Two Wire Drop Value On Controller Fail Case 1: Pass String value as argument """
        mv = self.create_test_master_valve_object()

        new_vt_value = "b"
        with self.assertRaises(Exception) as context:
            mv.set_two_wire_drop_value_on_cn(new_vt_value)
        expected_message = "Failed trying to set Master Valve TSD0001 (1)'s two wire drop. Invalid number type, " \
                           "expected an int or float, received: ({0})".format(type(new_vt_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_two_wire_drop_value_on_cn_fail2(self):
        """ Set Two Wire Drop Value On Controller Fail Case 2: Failed communication with controller """
        mv = self.create_test_master_valve_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            mv.set_two_wire_drop_value_on_cn()
        e_msg = "Exception occurred trying to set Master Valve TSD0001 (1)'s 'Two Wire Drop Value' to: '{0}'".format(
                str(mv.vt))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_booster_enable_state_on_cn_pass1(self):
        """ Set Booster Enable State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=TSD0001,{1}=FA".format(opcodes.master_valve, opcodes.booster_pump)
        mv = self.create_test_master_valve_object()
        mv.set_booster_enable_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_booster_enable_state_on_cn_pass2(self):
        """ Set Booster Enable State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=TSD0001,{1}=TR".format(opcodes.master_valve, opcodes.booster_pump)
        mv = self.create_test_master_valve_object()
        mv.set_booster_enable_state_on_cn("TR")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_booster_enable_state_on_cn_fail1(self):
        """ Set Booster Enable State On Controller Fail Case 1: Invalid State Argument """
        mv = self.create_test_master_valve_object()
        with self.assertRaises(ValueError) as context:
            mv.set_booster_enable_state_on_cn("Foo")
        e_msg = "Invalid booster pump enable state for Master Valve TSD0001 (1). Received: Foo, expected: 'TR' or 'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_booster_enable_state_on_cn_fail2(self):
        """ Set Booster Enable State On Controller Fail Case 2: Failed Communication With Controller """
        mv = self.create_test_master_valve_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            mv.set_booster_enable_state_on_cn("FA")
        e_msg = "Exception occurred trying to set Master Valve TSD0001 (1)'s 'Booster Pump State' to: FA"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_normally_open_state_on_cn_pass1(self):
        """ Verify Normally Open State On Controller Pass Case 1: Exception is not raised """
        mv = self.create_test_master_valve_object()
        mv.no = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.normally_open))
        mv.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                mv.verify_normally_open_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_normally_open_state_on_cn_fail1(self):
        """ Verify Normally Open State On Controller Fail Case 1: Value on controller does not match what is
        stored in mv.no """
        mv = self.create_test_master_valve_object()
        mv.no = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.normally_open))
        mv.data = mock_data

        expected_message = "Unable verify Master Valve TSD0001 (1)'s normally open state. Received: TR, " \
                           "Expected: {0}".format(str(mv.no))
        try:
            mv.verify_normally_open_state_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_enable_state_on_cn_pass1(self):
        """ Verify Enable State On Controller Pass Case 1: Exception is not raised """
        mv = self.create_test_master_valve_object()
        mv.en = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
        mv.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                mv.verify_enable_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_enable_state_on_cn_fail1(self):
        """ Verify Enable State On Controller Fail Case 1: Value on controller does not match what is
        stored in mv.en """
        mv = self.create_test_master_valve_object()
        mv.en = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
        mv.data = mock_data

        expected_message = "Unable verify master valve TSD0001 (1)'s enabled state. Received: TR, " \
                           "Expected: {0}".format(str(mv.en))
        try:
            mv.verify_enable_state_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_booster_enable_state_on_cn_pass1(self):
        """ Verify Booster Pump Enable State On Controller Pass Case 1: Exception is not raised """
        mv = self.create_test_master_valve_object()
        mv.bp = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.booster_pump))
        mv.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                mv.verify_booster_enable_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_booster_enable_state_on_cn_fail1(self):
        """ Verify Booster Pump Enable State On Controller Fail Case 1: Value on controller does not match what is
        stored in mv.bp """
        mv = self.create_test_master_valve_object()
        mv.bp = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.booster_pump))
        mv.data = mock_data

        expected_message = "Unable verify Master Valve TSD0001 (1)'s Booster pump enabled state. Received: TR, " \
                           "Expected: {0}".format(str(mv.bp))
        try:
            mv.verify_booster_enable_state_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_two_wire_drop_value_on_cn_pass1(self):
        """ Verify Two Wire Drop Value On Controller Pass Case 1: Exception is not raised """
        mv = self.create_test_master_valve_object()
        mv.vt = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.two_wire_drop))
        mv.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                mv.verify_two_wire_drop_value_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_two_wire_drop_value_on_cn_fail1(self):
        """ Verify Two Wire Drop Value On Controller Fail Case 1: Value on controller does not match what is
        stored in fm.vt """
        fm = self.create_test_master_valve_object()
        fm.vt = 5.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.two_wire_drop))
        fm.data = mock_data

        expected_message = "Unable verify master valve TSD0001 (1)'s two wire drop value. Received: 6.0, Expected: 5.0"
        try:
            fm.verify_two_wire_drop_value_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_self_test_and_update_object_attributes_pass1(self):
        """ Verify Self Test and Update Object Attributes Pass Case 1: Get data and then assign the new value to the object's attribute
        Create test master valve object
        Create the variable required to test the method
        Mock: do_self_test because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: get_data because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: data and get_value_string_by_key so we can output the values we want
            Return: Two wire drop value
        Call the method
        """
        # Create the moisture sensor object
        mv = self.create_test_master_valve_object()

        vt_value = 2

        # Mock the do_self_test method
        mock_do_self_test = mock.MagicMock()
        mv.do_self_test = mock_do_self_test

        # Mock the get_data method
        mock_get_data = mock.MagicMock()
        mv.get_data = mock_get_data

        # Mock get_value_string_by_key and the data object that is in the moisture sensor object
        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(return_value=vt_value)
        mv.data = mock_data

        # Call the method
        mv.self_test_and_update_object_attributes()

        # Verify the value was set
        self.assertEqual(vt_value, mv.vt)

    #################################
    def test_self_test_and_update_object_attributes_fail1(self):
        """ Verify Self Test and Update Object Attributes Fail Case 1: Catch Exception that is thrown
        Create test master valve object
        Create the variables required to test the method
        Mock: do_self_test because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: get_data because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: data and get_value_string_by_key so we can output the values we want
            Return: Three values, one each for moisture_percent, temperature_value, and two_wire_drop
        Create the expected message
        Call the method
        Compare the expected error message with the actual error message
        """
        # Create the moisture sensor object
        mv = self.create_test_master_valve_object()

        # Mock the do_self_test method
        mock_do_self_test = mock.MagicMock()
        mv.do_self_test = mock_do_self_test

        # Mock the get_data method
        mock_get_data = mock.MagicMock()
        mv.get_data = mock_get_data

        # Mock get_value_string_by_key and the data object that is in the moisture sensor object
        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(side_effect=Exception)
        mv.data = mock_data

        # Create the expected message
        expected_msg = "Exception occurred trying to updating attributes of the master valve {0} object." \
                       " Two Wire Drop Value: '{1}'" \
                       .format(
                           str(mv.ad),    # {0}
                           str(mv.vt)     # {1}
                       )

        # Run method that will tested
        with self.assertRaises(Exception) as context:
            mv.self_test_and_update_object_attributes()

        # Compare the expected with the actual
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_who_i_am(self):
        """ Verify Who I Am Pass Case 1: Runs through the method successfully
        Create test moisture sensor object
        Mock: get_data because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_description_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_serial_number_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_latitude_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_longitude_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_two_wire_drop_value_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_status_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_enable_state_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_normally_open_state_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_booster_enable_state_on_cn because it goes outside the method
            Return: Nothing we just want to pass over it
        Call the method and make sure it runs through successfully
        """
        # Creates the moisture sensor object
        mv = self.create_test_master_valve_object()
        mv.controller_type = "32"

        # Mock the get_data method
        mock_get_data = mock.MagicMock()
        mv.get_data = mock_get_data

        # Mock the verify_description_on_cn method
        mock_verify_description_on_cn = mock.MagicMock()
        mv.verify_description_on_cn = mock_verify_description_on_cn

        # Mock the verify_serial_number_on_cn method
        mock_verify_serial_number_on_cn = mock.MagicMock()
        mv.verify_serial_number_on_cn = mock_verify_serial_number_on_cn

        # Mock the verify_latitude_on_cn method
        mock_verify_latitude_on_cn = mock.MagicMock()
        mv.verify_latitude_on_cn = mock_verify_latitude_on_cn

        # Mock the verify_longitude_on_cn method
        mock_verify_longitude_on_cn = mock.MagicMock()
        mv.verify_longitude_on_cn = mock_verify_longitude_on_cn

        # Mock the verify_two_wire_drop_value_on_cn method
        mock_verify_two_wire_drop_value_on_cn = mock.MagicMock()
        mv.verify_two_wire_drop_value_on_cn = mock_verify_two_wire_drop_value_on_cn

        # Mock the verify_status_on_cn method
        mock_verify_status_on_cn = mock.MagicMock()
        mv.verify_status_on_cn = mock_verify_status_on_cn

        # Mock the verify_status_on_cn method
        mock_verify_status_on_cn = mock.MagicMock()
        mv.verify_status_on_cn = mock_verify_status_on_cn

        # Mock the verify_enable_state_on_cn method
        mock_verify_enable_state_on_cn = mock.MagicMock()
        mv.verify_enable_state_on_cn = mock_verify_enable_state_on_cn

        # Mock the verify_normally_open_state_on_cn method
        mock_verify_normally_open_state_on_cn = mock.MagicMock()
        mv.verify_normally_open_state_on_cn = mock_verify_normally_open_state_on_cn

        # Mock the verify_booster_enable_state_on_cn method
        mock_verify_booster_enable_state_on_cn = mock.MagicMock()
        mv.verify_booster_enable_state_on_cn = mock_verify_booster_enable_state_on_cn

        # There is nothing to assert in this method call
        mv.verify_who_i_am(_expected_status=opcodes.bad_serial)

    if __name__ == "__main__":
        unittest.main()

