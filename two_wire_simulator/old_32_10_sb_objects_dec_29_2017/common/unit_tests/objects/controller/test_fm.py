import unittest
import mock
import serial
import status_parser

# from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.ser import Ser
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.devices import Devices
# you have to set the lat and long after the devices class is called so that they don't get skipped over
Devices.controller_lat = float(43.609768)
Devices.controller_long = float(-116.310569)

from old_32_10_sb_objects_dec_29_2017.common.objects.controller.fm import FlowMeter


__author__ = 'Brice "Ajo Grande" Garlick'


class TestFlowMeterObject(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.309759

    Flow Meter Starting lat & lng:
    latitude = 43.609768
    longitude = -116.309354
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        Devices.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_flow_meter_object(self, _diffserial=None, _diffaddress=None):
        """ Creates a new Flow Meter object for testing purposes """
        if _diffserial is not None:
            fm = FlowMeter(_serial="TSD0001", _address=_diffaddress)
        elif _diffaddress is not None:
            fm = FlowMeter(_serial=_diffserial, _address=1)
        else:
            fm = FlowMeter(_serial="TSD0001", _address=1)

        return fm

    #################################
    def test_set_default_values_pass1(self):
        """ Set Default Values On Flow Meter Pass Case 1: Correct Command Sent """
        expected_command = "SET," \
                           "FM=TSD0001," \
                           "EN=TR," \
                           "DS=TSD0001 Test Flow Meter 1," \
                           "LA=43.609773," \
                           "LG=-116.310164," \
                           "KV=2.0," \
                           "VR=0.0," \
                           "VG=0.0," \
                           "VT=0.0"
        fm = self.create_test_flow_meter_object()

        fm.set_default_values()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values On Flow Meter Fail Case 1: Failed communication with controller """
        expected_command = "SET," \
                           "FM=TSD0001," \
                           "EN=TR," \
                           "DS=TSD0001 Test Flow Meter 1," \
                           "LA=43.609773," \
                           "LG=-116.310164," \
                           "KV=2.0," \
                           "VR=0.0," \
                           "VG=0.0," \
                           "VT=0.0"
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            fm = self.create_test_flow_meter_object()
            fm.set_default_values()
        e_msg = "Exception occurred trying to set Flow Meter TSD0001 (1)'s 'Default values' to: '{0}'".format(
                expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_enable_state_pass1(self):
        """ Set Enable State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,FM=TSD0001,EN=TR"
        fm = self.create_test_flow_meter_object()
        fm.set_enable_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_state_pass2(self):
        """ Set Enable State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,FM=TSD0001,EN=FA"
        fm = self.create_test_flow_meter_object()
        fm.set_enable_state_on_cn(_state="FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_state_pass3(self):
        """ Set Enable State On Controller Pass Case 3: Command with correct values sent to controller """
        fm = self.create_test_flow_meter_object()

        en_value = str(fm.en)
        expected_command = "SET,FM=TSD0001,EN=" + en_value
        fm.set_enable_state_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_state_fail1(self):
        """ Set Enable State On Controller Fail Case 1: Invalid State Argument """
        fm = self.create_test_flow_meter_object()
        with self.assertRaises(ValueError) as context:
            fm.set_enable_state_on_cn(_state="Foo")
        e_msg = "Invalid enabled state entered for Flow Meter TSD0001 (1). Received: Foo, Expected: 'TR' or 'FA'"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_enable_state_fail2(self):
        """ Set Enable State On Controller Fail Case 2: Failed Communication With Controller """
        fm = self.create_test_flow_meter_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after zone instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            fm.set_enable_state_on_cn(_state="FA")
        e_msg = "Exception occurred trying to enable flow meter TSD0001"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_k_value_on_cn_pass1(self):
        """ Set K Value On Controller Pass Case 1: Using Default _kv value """
        fm = self.create_test_flow_meter_object()

        #Expected value is the _df value set at object Zone object creation
        expected_value = fm.kv
        fm.set_k_value_on_cn()

        #_df value is set during this method and should equal the original value
        actual_value = fm.kv
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_k_value_on_cn_pass2(self):
        """ Set K Value On Controller Pass Case 2: Setting new _kv value = 6.0 """
        fm = self.create_test_flow_meter_object()

        #Expected _df value is 6
        expected_value = 6.0
        fm.set_k_value_on_cn(expected_value)

        #_df value is set during this method and should equal the value passed into the method
        actual_value = fm.kv
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_k_value_on_cn_pass3(self):
        """ Set K Value On Controller Pass Case 3: Command with correct values sent to controller """
        fm = self.create_test_flow_meter_object()

        df_value = str(fm.kv)
        expected_command = "SET,FM=TSD0001,KV=" + df_value
        fm.set_k_value_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_k_value_on_cn_fail1(self):
        """ Set K Value On Controller Fail Case 1: Pass String value as argument """
        fm = self.create_test_flow_meter_object()

        new_kv_value = "b"
        with self.assertRaises(Exception) as context:
            fm.set_k_value_on_cn(new_kv_value)
        expected_message = "Failed trying to set FM {0} ({1})'s k value. Invalid argument type, expected a float, " \
                           "received: {2}".format(str(fm.sn), str(fm.ad), type(new_kv_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_k_value_on_cn_fail2(self):
        """ Set K Value On Controller Fail Case 2: Failed communication with controller """
        fm = self.create_test_flow_meter_object()

        #A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            fm.set_k_value_on_cn()
        e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'K Value' to: '{2}'".format(
                fm.sn, str(fm.ad), str(fm.kv))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_flow_rate_on_cn_pass1(self):
        """ Set Flow Rate On Controller Pass Case 1: Using Default _vr value """
        fm = self.create_test_flow_meter_object()

        #Expected value is the _df value set at object Zone object creation
        expected_value = fm.vr
        fm.set_flow_rate_on_cn()

        #_df value is set during this method and should equal the original value
        actual_value = fm.vr
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_flow_rate_on_cn_pass2(self):
        """ Set Flow Rate On Controller Pass Case 2: Setting new _vr value = 6.0 """
        fm = self.create_test_flow_meter_object()

        #Expected _df value is 6
        expected_value = 6.0
        fm.set_flow_rate_on_cn(expected_value)

        #_df value is set during this method and should equal the value passed into the method
        actual_value = fm.vr
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_flow_rate_on_cn_pass3(self):
        """ Set Flow Rate On Controller Pass Case 3: Command with correct values sent to controller """
        fm = self.create_test_flow_meter_object()

        vr_value = "0.0"
        expected_command = "SET,FM=TSD0001,VR=" + vr_value
        fm.set_flow_rate_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_flow_rate_on_cn_fail1(self):
        """ Set Flow Rate On Controller Fail Case 1: Pass String value as argument """
        fm = self.create_test_flow_meter_object()

        new_vr_value = "b"
        with self.assertRaises(Exception) as context:
            fm.set_flow_rate_on_cn(new_vr_value)
        expected_message = "Failed trying to set FM {0} ({1})'s Flow Rate, invalid argument type, expected a int " \
                           "or float, received: {2}".format(fm.sn, str(fm.ad), type(new_vr_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_flow_rate_on_cn_fail2(self):
        """ Set Flow Rate On Controller Fail Case 2: Failed communication with controller """
        fm = self.create_test_flow_meter_object()

        #A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            fm.set_flow_rate_on_cn()
        e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'Flow Rate' to: '({2})'".format(
                fm.sn, str(fm.ad), str(fm.vr))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_water_usage_on_cn_pass1(self):
        """ Set Water Usage On Controller Pass Case 1: Using Default _vg value """
        fm = self.create_test_flow_meter_object()

        #Expected value is the _df value set at object Zone object creation
        expected_value = fm.vg
        fm.set_water_usage_on_cn()

        #_df value is set during this method and should equal the original value
        actual_value = fm.vg
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_water_usage_on_cn_pass2(self):
        """ Set Water Usage On Controller Pass Case 2: Setting new _vg value = 6.0 """
        fm = self.create_test_flow_meter_object()

        #Expected _df value is 6
        expected_value = 6.0
        fm.set_water_usage_on_cn(expected_value)

        #_df value is set during this method and should equal the value passed into the method
        actual_value = fm.vg
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_water_usage_on_cn_pass3(self):
        """ Set Water Usage On Controller Pass Case 3: Command with correct values sent to controller """
        fm = self.create_test_flow_meter_object()

        vg_value = str(fm.vg)
        expected_command = "SET,FM=TSD0001,VG=" + vg_value
        fm.set_water_usage_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_usage_on_cn_fail1(self):
        """ Set Water Usage On Controller Fail Case 1: Pass String value as argument """
        fm = self.create_test_flow_meter_object()

        new_vg_value = "b"
        with self.assertRaises(Exception) as context:
            fm.set_water_usage_on_cn(new_vg_value)
        expected_message = "Failed trying to set FM {0} ({1})'s Water Usage, invalid argument type, expected a int " \
                           "or float, received: {2}".format(fm.sn, str(fm.ad), type(new_vg_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_water_usage_on_cn_fail2(self):
        """ Set Water Usage On Controller Fail Case 2: Failed communication with controller """
        fm = self.create_test_flow_meter_object()
        new_vg_value = 6.0

        #A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            fm.set_water_usage_on_cn(new_vg_value)
        e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'water usage' to: '{2}'".format(
                fm.sn, str(fm.ad), str(new_vg_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass1(self):
        """ Set Two Wire Drop Value On Controller Pass Case 1: Using Default _vt value """
        fm = self.create_test_flow_meter_object()

        #Expected value is the _va value set at object Zone object creation
        expected_value = fm.vt
        fm.set_two_wire_drop_value_on_cn()

        #_va value is set during this method and should equal the original value
        actual_value = fm.vt
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass2(self):
        """ Set Two Wire Drop Value On Controller Pass Case 2: Using 6 as passed in value for _vt """
        fm = self.create_test_flow_meter_object()

        #Expected value is the _va value set at object Zone object creation
        expected_value = 6
        fm.set_two_wire_drop_value_on_cn(expected_value)

        #_va value is set during this method and should equal the original value
        actual_value = fm.vt
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_two_wire_drop_value_on_cn_pass3(self):
        """ Set Two Wire Drop Value Controller Pass Case 3: Command with correct values sent to controller """
        fm = self.create_test_flow_meter_object()

        vt_value = str(fm.vt)
        expected_command = "SET,FM=TSD0001,VT=" + vt_value
        fm.set_two_wire_drop_value_on_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_two_wire_drop_value_on_cn_fail1(self):
        """ Set Two Wire Drop Value On Controller Fail Case 1: Pass String value as argument """
        fm = self.create_test_flow_meter_object()

        new_vt_value = "b"
        with self.assertRaises(Exception) as context:
            fm.set_two_wire_drop_value_on_cn(new_vt_value)
        expected_message = "Failed trying to set FM {0} ({1})'s Two Wire Drop, invalid argument type, expected a " \
                           "int or float, received: {2}".format(fm.sn, str(fm.ad), type(new_vt_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_two_wire_drop_value_flow_on_cn_fail2(self):
        """ Set Two Wire Drop Value On Controller Fail Case 2: Failed communication with controller """
        fm = self.create_test_flow_meter_object()

        #A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            fm.set_two_wire_drop_value_on_cn()
        e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'Two Wire Drop Value' to: '{2}'".format(
                fm.sn, str(fm.ad), str(fm.vt))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_k_value_on_cn_pass1(self):
        """ Verify K Value On Controller Pass Case 1: Exception is not raised """
        fm = self.create_test_flow_meter_object()
        fm.kv = 5.0
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.k_value))
        fm.data = mock_data

        try:
            #.assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                fm.verify_k_value_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_k_value_on_cn_fail1(self):
        """ Verify K Value On Controller Fail Case 1: Value on controller does not match what is
        stored in fm.kv """
        fm = self.create_test_flow_meter_object()
        fm.kv = 5.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.k_value))
        fm.data = mock_data

        expected_message = "Unable verify Flow Meter TSD0001 (1)'s k_value. Received: 6.0, Expected: 5.0"
        try:
            fm.verify_k_value_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_flow_rate_on_cn_pass1(self):
        """ Verify Flow Rate On Controller Pass Case 1: Exception is not raised """
        fm = self.create_test_flow_meter_object()
        fm.vr = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.flow_rate))
        fm.data = mock_data

        try:
            #.assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                fm.verify_flow_rate_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_flow_rate_on_cn_fail1(self):
        """ Verify Flow Rate On Controller Fail Case 1: Value on controller does not match what is stored in fm.vr """
        fm = self.create_test_flow_meter_object()
        fm.vr = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.flow_rate))
        fm.data = mock_data

        expected_message = "Unable verify Flow Meter TSD0001 (1)'s flow rate. Received: 6.0, Expected: 5"
        try:
            fm.verify_flow_rate_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_water_usage_on_cn_pass1(self):
        """ Verify Water Usage On Controller Pass Case 1: Exception is not raised """
        fm = self.create_test_flow_meter_object()
        fm.vg = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.total_usage))
        fm.data = mock_data

        try:
            #.assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                fm.verify_water_usage_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_water_usage_on_cn_fail1(self):
        """ Verify Water Usage On Controller Fail Case 1: Value on controller does not match what is
        stored in fm.vg """
        fm = self.create_test_flow_meter_object()
        fm.vg = 5.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.total_usage))
        fm.data = mock_data

        expected_message = "Unable verify Flow Meter TSD0001 (1)'s water usage. Received: 6.0, Expected: 5.0"
        try:
            fm.verify_water_usage_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_two_wire_drop_value_on_cn_pass1(self):
        """ Verify Two Wire Drop Value On Controller Pass Case 1: Exception is not raised """
        fm = self.create_test_flow_meter_object()
        fm.vt = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.two_wire_drop))
        fm.data = mock_data

        try:
            #.assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                fm.verify_two_wire_drop_value_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_two_wire_drop_value_on_cn_fail1(self):
        """ Verify Two Wire Drop Value On Controller Fail Case 1: Value on controller does not match what is
        stored in fm.vt """
        fm = self.create_test_flow_meter_object()
        fm.vt = 5.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.two_wire_drop))
        fm.data = mock_data

        expected_message = "Unable verify Flow Meter TSD0001 (1)'s two wire drop value. Received: 6.0, Expected: 5.0"
        try:
            fm.verify_two_wire_drop_value_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_enable_state_on_cn_pass1(self):
        """ Verify Enable State On Controller Pass Case 1: Exception is not raised """
        fm = self.create_test_flow_meter_object()
        fm.en = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
        fm.data = mock_data

        try:
            #.assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                fm.verify_enable_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_enable_state_on_cn_fail1(self):
        """ Verify Enable State On Controller Fail Case 1: Value on controller does not match what is
        stored in fm.en """
        fm = self.create_test_flow_meter_object()
        fm.en = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
        fm.data = mock_data

        expected_message = "Unable verify Flow Meter TSD0001 (1)'s enabled state. Received: TR, " \
                           "Expected: {0}".format(str(fm.en))
        try:
            fm.verify_enable_state_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_self_test_and_update_object_attributes_pass1(self):
        """
        self_test_and_update_object_attributes pass case 1:
        Create test flow meter object
        Store: Expected values that can be compared against actual values to make sure they can be reset
        Mock: do_self_test because it goes outside of the method
        Return: None so it does not fail
        Mock: get_data because it goes outside of the method
        Return: None, so it does not fail
        Mock: data.get_value_string_by_key because it goes outside of the method
        Return: Three integers, so they can be converted into floats
        Run: Test, with nothing passed into it
        Compare: expected values, such as self.ad or self.vr, to actual values stored in test
        """
        # Create test flow meter object
        fm = self.create_test_flow_meter_object()

        # Store expected values in test
        first_value = 3.0
        second_value = 4.0
        third_value = 5.0

        # Mock do_self_test
        mock_do_self_test = mock.MagicMock(side_effect=None)
        fm.do_self_test = mock_do_self_test

        # Mock get_data
        mock_get_data = mock.MagicMock(side_effect=None)
        fm.get_data = mock_get_data

        # Mock data.get_value_string_by_key, return three integers
        mock_get_value_string_by_key = mock.MagicMock(side_effect=[first_value, second_value, third_value])
        fm.data.get_value_string_by_key = mock_get_value_string_by_key

        # Run the test
        fm.self_test_and_update_object_attributes()

        # Compare expected values to actual values. Use assertIs instead of assertEquals in order to compare the type
        # (float to float) as well as the value that assertEqual would normally compare
        self.assertIs(first_value, fm.vr)
        self.assertIs(second_value, fm.vg)
        self.assertIs(third_value, fm.vt)

    #################################
    def test_self_test_and_update_object_attributes_fail1(self):
        """
        self_test_and_update_object_attributes fail case 1:
        Create: test flow meter object
        Mock: do_self_test because it goes outside of the method
        Return: None so it does not fail at this point in the method
        Mock: get_data because it goes outside of the method
        Return: None, so it does not fail at this point in the method
        Mock: All three get_value_string_by_key methods
        Return: Strings for all of them, this way they cannot be type casted into floats, and will raise an exception
        Run: The method, and catch an exception, which will let us access the error message
        """
        # Create test flow meter object
        fm = self.create_test_flow_meter_object()

        # Store incorrect expected value in test, because the test has the get_value_string_by_key as strings,
        # the test cannot run with them.
        incorrect_value = 0.0

        e_msg = "Exception occurred trying to updating attributes of the flow meter {0} object." \
                " Flow Rate Value: '{1}'," \
                " Total Usage Value: '{2}'," \
                " Two Wire Drop Value: '{3}'" \
                .format(
                str(1),  # {0}
                str(incorrect_value),  # {1}
                str(incorrect_value),  # {2}
                str(incorrect_value),  # {3}
                )

        # Mock do_self_test
        mock_do_self_test = mock.MagicMock(side_effect=None)
        fm.do_self_test = mock_do_self_test

        # Mock get_data
        mock_get_data = mock.MagicMock(side_effect=None)
        fm.get_data = mock_get_data

        # Mock self.data.get_value_string_by_key
        mock_get_value_string_by_key = mock.MagicMock(side_effect=['this', 'should', 'fail'])
        fm.data.get_value_string_by_key = mock_get_value_string_by_key

        # Run the method, and catch an exception
        with self.assertRaises(Exception) as context:
            fm.self_test_and_update_object_attributes()

        # Compare actual error message to expected error message in order to test that the correct message was reached
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_who_i_am_pass1(self):
        """
        verify_who_i_am pass case 1:
        Create: test flow meter object
        Mock: get_data because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_description_on_cn because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_serial_number_on_cn because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_latitude_on_cn because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_longitude_on_cn because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_status_on_cn because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_enable_state_on_cn because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_k_value_on_cn because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_flow_rate_on_cn because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_water_usage_on_cn because it goes outside the method
        Return: None, so it does not fail
        Run: Test, passing _expected_status in as something besides None
        Passes: If all the code runs through without any exceptions. This merely tests the flow, not any of the verified
        methods inside of the code.
        """
        # Create test flow meter object
        fm = self.create_test_flow_meter_object()

        # Mock get_data
        mock_get_data = mock.MagicMock(side_effect=None)
        fm.get_data = mock_get_data

        # Mock verify_description_on_cn
        mock_verify_description_on_cn = mock.MagicMock(side_effect=None)
        fm.verify_description_on_cn = mock_verify_description_on_cn

        # Mock verify_serial_number_on_cn
        mock_verify_serial_number_on_cn = mock.MagicMock(side_effect=None)
        fm.verify_serial_number_on_cn = mock_verify_serial_number_on_cn

        # Mock verify_latitude_on_cn
        mock_verify_latitude_on_cn = mock.MagicMock(side_effect=None)
        fm.verify_latitude_on_cn = mock_verify_latitude_on_cn

        # Mock verify_longitude_on_cn
        mock_verify_longitude_on_cn = mock.MagicMock(side_effect=None)
        fm.verify_longitude_on_cn = mock_verify_longitude_on_cn

        # Mock verify_status_on_cn
        mock_verify_status_on_cn = mock.MagicMock(side_effect=None)
        fm.verify_status_on_cn = mock_verify_status_on_cn

        # Mock verify_enable_state_on_cn
        mock_verify_enable_state_on_cn = mock.MagicMock(side_effect=None)
        fm.verify_enable_state_on_cn = mock_verify_enable_state_on_cn

        # Mock verify_k_value_on_cn
        mock_verify_k_value_on_cn = mock.MagicMock(side_effect=None)
        fm.verify_k_value_on_cn = mock_verify_k_value_on_cn

        # Mock verify_flow_rate_on_cn
        mock_verify_flow_rate_on_cn = mock.MagicMock(side_effect=None)
        fm.verify_flow_rate_on_cn = mock_verify_flow_rate_on_cn

        # Mock verify_water_usage_on_cn
        mock_verify_water_usage_on_cn = mock.MagicMock(side_effect=None)
        fm.verify_water_usage_on_cn = mock_verify_water_usage_on_cn

        # Run test
        fm.verify_who_i_am(_expected_status='OK')

    if __name__ == "__main__":
        unittest.main()
