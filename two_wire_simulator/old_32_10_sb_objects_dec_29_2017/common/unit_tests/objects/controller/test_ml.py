__author__ = 'Brice "Ajo Grande" Garlick'

import unittest

import mock
import serial
import status_parser
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes import messages


class TestMainLineObject(unittest.TestCase):

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        Mainline.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_mainline_object(self):
        """ Creates a new Mainline object for testing purposes """
        ml = Mainline(_ad=1)

        return ml

    #################################
    def test_send_programming_to_cn_pass1(self):
        """ Send Programming To Controller Pass Case 1: Correct Command Sent """
        expected_command = "SET," \
                           "ML=1," \
                           "DS=Test Mainline 1," \
                           "FT=120," \
                           "FL=0.0," \
                           "LC=FA," \
                           "HV=0," \
                           "HS=FA," \
                           "LV=0," \
                           "LS=FA"
        ml = self.create_test_mainline_object()

        ml.send_programming_to_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    #################################
    def test_send_programming_to_cn_fail1(self):
        """ Send Programming To Controller Fail Case 1: Failed communication with controller """
        expected_command = "SET," \
                           "ML=1," \
                           "DS=Test Mainline 1," \
                           "FT=120," \
                           "FL=0.0," \
                           "LC=FA," \
                           "HV=0," \
                           "HS=FA," \
                           "LV=0," \
                           "LS=FA"
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            ml = self.create_test_mainline_object()
            ml.send_programming_to_cn()
        e_msg = "Exception occurred trying to set Mainline 1's 'Default Values' to: '{0}' -> ".format(
                expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_description_on_cn_pass1(self):
        """ Set Description On Controller Pass Case 1: Setting new _ds value = Test Mainline 2 """
        ml = self.create_test_mainline_object()

        expected_value = "Test Mainline 2"
        ml.set_description_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = ml.ds
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_description_on_cn_pass2(self):
        """ Set Description On Controller Pass Case 2: Command with correct values sent to controller """
        ml = self.create_test_mainline_object()

        ds_value = str(ml.ds)
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.description, str(ds_value))
        ml.set_description_on_cn(ds_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_description_on_cn_fail1(self):
        """ Set Description On Controller Fail Case 1: Failed communication with controller """
        ml = self.create_test_mainline_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ml.set_description_on_cn(ml.ds)
        e_msg = "Exception occurred trying to set Mainline 1's 'Description' to: '{0}' -> ".format(str(ml.ds))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_pipe_fill_time_on_cn_pass1(self):
        """ Set Pipe Fill Time On Controller Pass Case 1: Setting new _ft value = 5 """
        ml = self.create_test_mainline_object()

        expected_value = 5
        ml.set_pipe_fill_time_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = ml.ft
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_pipe_fill_time_on_cn_pass2(self):
        """ Set Pipe Fill Time On Controller Pass Case 2: Command with correct values sent to controller """
        ml = self.create_test_mainline_object()

        ft_value = 5
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.fill_time, str(ft_value*60))
        ml.set_pipe_fill_time_on_cn(ft_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_pipe_fill_time_on_cn_fail1(self):
        """ Set Pipe Fill Time On Controller Fail Case 1: Failed communication with controller """
        ml = self.create_test_mainline_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ml.set_pipe_fill_time_on_cn(ml.ft)
        e_msg = "Exception occurred trying to set Mainline 1's 'Pipe Fill Time' to: '{0}' -> ".format(str(ml.ft))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_target_flow_on_cn_pass1(self):
        """ Set Target Flow On Controller Pass Case 1: Setting new _fl value = 6.0 """
        ml = self.create_test_mainline_object()

        expected_value = 6
        ml.set_target_flow_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = ml.fl
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_target_flow_on_cn_pass2(self):
        """ Set Target Flow On Controller Pass Case 2: Command with correct values sent to controller """
        ml = self.create_test_mainline_object()

        fl_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.target_flow, str(fl_value))
        ml.set_target_flow_on_cn(fl_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_target_flow_on_cn_fail1(self):
        """ Set Target Flow On Controller Fail Case 1: Failed communication with controller """
        ml = self.create_test_mainline_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ml.set_target_flow_on_cn(6)
        e_msg = "Exception occurred trying to set Mainline 1's 'Target Flow' to: '{0}' -> ".format(str(ml.fl))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_limit_zones_by_flow_state_on_cn_pass1(self):
        """ Set Limit Zones by Flow State On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.limit_zones_by_flow,
                                                      opcodes.false)
        ml = self.create_test_mainline_object()
        ml.set_limit_zones_by_flow_state_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_limit_zones_by_flow_state_on_cn_pass2(self):
        """ Set Limit Zones by Flow State On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.limit_zones_by_flow,
                                                      opcodes.true)
        ml = self.create_test_mainline_object()
        ml.set_limit_zones_by_flow_state_on_cn("TR")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_limit_zones_by_flow_state_on_cn_fail1(self):
        """ Set Limit Zones by Flow State On Controller Fail Case 1: Invalid State Argument """
        ml = self.create_test_mainline_object()
        with self.assertRaises(ValueError) as context:
            ml.set_limit_zones_by_flow_state_on_cn("Foo")
        e_msg = "Invalid 'Limit Zones By Flow' state entered for Mainline 1. Expects 'TR' or 'FA'. " \
                "Received: Foo"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_limit_zones_by_flow_state_on_cn_fail2(self):
        """ Set Limit Zones by Flow State On Controller Fail Case 2: Failed Communication With Controller """
        ml = self.create_test_mainline_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ml.set_limit_zones_by_flow_state_on_cn("FA")
        e_msg = "Exception occurred trying to set Mainline 1's 'Limit Zones By Flow' to: 'FA' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_high_variance_limit_on_cn_pass1(self):
        """ Set High Flow Limit On Controller Pass Case 1: Setting new _hv value = 6.0 """
        ml = self.create_test_mainline_object()

        expected_value = 6
        ml.set_high_variance_limit_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = ml.hv
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_high_variance_limit_on_cn_pass2(self):
        """ Set High Flow Limit On Controller Pass Case 2: Command with correct values sent to controller """
        ml = self.create_test_mainline_object()

        hv_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.high_variance_limit,
                                                      str(hv_value))
        ml.set_high_variance_limit_on_cn(hv_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_flow_limit_on_cn_fail1(self):
        """ Set High Flow Limit On Controller Fail Case 1: Failed communication with controller """
        ml = self.create_test_mainline_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ml.set_high_variance_limit_on_cn(6)
        e_msg = "Exception occurred trying to set Mainline 1's 'High Variance Limit' to: '6' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_high_variance_shut_down_on_cn_pass1(self):
        """ Set High Variance Shutdown On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.high_flow_variance_shutdown,
                                                      opcodes.false)
        ml = self.create_test_mainline_object()
        ml.set_high_variance_shut_down_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_variance_shut_down_on_cn_pass2(self):
        """ Set High Variance Shutdown On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.high_flow_variance_shutdown,
                                                      opcodes.true)
        ml = self.create_test_mainline_object()
        ml.set_high_variance_shut_down_on_cn("TR")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_variance_shut_down_on_cn_fail1(self):
        """ Set High Variance Shutdown On Controller Fail Case 1: Invalid State Argument """
        ml = self.create_test_mainline_object()
        with self.assertRaises(ValueError) as context:
            ml.set_high_variance_shut_down_on_cn("Foo")
        e_msg = "Invalid 'High Variance Shut Down' state entered for Mainline 1. Expects 'TR' or 'FA'. " \
                "Received: Foo"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_high_variance_shut_down_on_cn_fail2(self):
        """ Set High Variance Shutdown On Controller Fail Case 2: Failed Communication With Controller """
        ml = self.create_test_mainline_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ml.set_high_variance_shut_down_on_cn("FA")
        e_msg = "Exception occurred trying to set Mainline 1's 'High Variance Shut Down' to: 'FA' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_low_variance_limit_on_cn_pass1(self):
        """ Set Low Flow Limit On Controller Pass Case 1: Setting new _hv value = 6.0 """
        ml = self.create_test_mainline_object()

        expected_value = 6
        ml.set_low_variance_limit_on_cn(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = ml.lv
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_low_variance_limit_on_cn_pass2(self):
        """ Set Low Flow Limit On Controller Pass Case 2: Command with correct values sent to controller """
        ml = self.create_test_mainline_object()

        hv_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.low_variance_limit,
                                                      str(hv_value))
        ml.set_low_variance_limit_on_cn(hv_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_low_flow_limit_on_cn_fail1(self):
        """ Set Low Flow Limit On Controller Fail Case 1: Failed communication with controller """
        ml = self.create_test_mainline_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ml.set_low_variance_limit_on_cn(6)
        e_msg = "Exception occurred trying to set Mainline 1's 'Low Variance Limit' to: '6' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_low_variance_shut_down_on_cn_pass1(self):
        """ Set Low Variance Shutdown On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.low_variance_shut_down,
                                                      opcodes.false)
        ml = self.create_test_mainline_object()
        ml.set_low_variance_shut_down_on_cn("FA")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_low_variance_shut_down_on_cn_pass2(self):
        """ Set Low Variance Shutdown On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.low_variance_shut_down,
                                                      opcodes.true)
        ml = self.create_test_mainline_object()
        ml.set_low_variance_shut_down_on_cn("TR")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_low_variance_shut_down_on_cn_fail1(self):
        """ Set Low Variance Shutdown On Controller Fail Case 1: Invalid State Argument """
        ml = self.create_test_mainline_object()
        with self.assertRaises(ValueError) as context:
            ml.set_low_variance_shut_down_on_cn("Foo")
        e_msg = "Invalid 'Low Variance Shut Down' state entered for Mainline 1. Expects 'TR' or 'FA'. " \
                "Received: Foo"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_low_variance_shut_down_on_cn_fail2(self):
        """ Set Low Variance Shutdown On Controller Fail Case 2: Failed Communication With Controller """
        ml = self.create_test_mainline_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ml.set_low_variance_shut_down_on_cn("FA")
        e_msg = "Exception occurred trying to set Mainline 1's 'Low Variance Shut Down' to: 'FA' -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_message_on_cn_pass1(self):
        """ Set Message On Controller Pass Case 1: Correct command is sent to controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to set because it goes outside of the method
            Return: The expected message
        Call the method"""
        ml = self.create_test_mainline_object()

        expected_command = "SET,MG,{0}={1},{2}={3}".format(opcodes.mainline, str(ml.ad), opcodes.status_code,
                                                           opcodes.learn_flow_fail_flow_biCoders_disabled)

        with mock.patch('common.objects.base_classes.messages.message_to_set', return_value=expected_command):
            ml.set_message_on_cn(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_message_on_cn_fail1(self):
        """ Set Message On Controller Controller Fail Case 1: Exception is thrown when sending command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to set because it goes outside of the method
            Return: The expected message
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        """
        ml = self.create_test_mainline_object()

        expected_command = "SET,MG,{0}={1},{2}={3}".format(opcodes.mainline, str(ml.ad), opcodes.status_code,
                                                           opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.mock_send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_set', return_value=expected_command):
                ml.set_message_on_cn(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_data_pass1(self):
        """ Get Data From Controller Pass Case 1: Correct command is sent to controller """
        ml = self.create_test_mainline_object()

        expected_command = "GET,{0}={1}".format(opcodes.mainline, str(ml.ad))
        ml.get_data()
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_data_fail1(self):
        """ Get Data From Controller Fail Case 1: Failed communication with controller """
        ml = self.create_test_mainline_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_get_and_wait_for_reply.side_effect = AssertionError

        expected_command = "GET,{0}={1}".format(opcodes.mainline, str(ml.ad))
        with self.assertRaises(ValueError) as context:
            ml.get_data()
        e_msg = "Unable to get data for Mainline {0} using command: '{1}'. Exception raised: "\
                .format(str(ml.ad), expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_get_message_on_cn_pass1(self):
        """ Get Message On Controller Pass Case 1: Correct command is sent to controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to get because it goes outside of the method
            Return: The expected message in a dictionary
        Call the method
        """
        ml = self.create_test_mainline_object()

        expected_command = "GET,MG,{0}={1},{2}={3}".format(opcodes.mainline, str(ml.ad), opcodes.status_code,
                                                           opcodes.learn_flow_fail_flow_biCoders_disabled)

        with mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: expected_command}):
            ml.get_message_on_cn(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_message_on_cn_fail1(self):
        """ Get Message On Controller Fail Case 1: Exception is thrown when sending command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to get because it goes outside of the method
            Return: The expected message
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        """
        ml = self.create_test_mainline_object()

        expected_command = "GET,MG,{0}={1},{2}={3}".format(opcodes.mainline, str(ml.ad), opcodes.status_code,
                                                           opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.mock_get_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: expected_command}):
                ml.get_message_on_cn(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_clear_message_on_cn_pass1(self):
        """ Clear Message On Controller Pass Case 1: Correct command is sent to controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: The expected message in a dictionary
        Call the method and verify what is was called with

        """
        ml = self.create_test_mainline_object()

        expected_command = "GET,MG,{0}={1},{2}={3}".format(opcodes.mainline, str(ml.ad), opcodes.status_code,
                                                           opcodes.learn_flow_fail_flow_biCoders_disabled)

        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value=expected_command), \
             mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: expected_command}):
            ml.clear_message_on_cn(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_clear_message_on_cn_fail1(self):
        """ Clear Message On Controller Fail Case 1: Exception is thrown when sending command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: An Exception
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        """
        ml = self.create_test_mainline_object()

        expected_msg = "Exception caught in messages.clear_message: "

        self.mock_send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_clear'):
                ml.clear_message_on_cn(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_clear_message_on_cn_fail2(self):
        """ Clear Message On Controller Fail Case 2: Exception is thrown when getting command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: The expected message
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method
        Cannot make an assertion since the exception is caught inside the method
        """
        ml = self.create_test_mainline_object()

        expected_msg = "Message cleared, but was unable to verify if the message was gone."

        self.mock_get_and_wait_for_reply.side_effect = Exception

        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value=expected_msg):
            ml.clear_message_on_cn(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

    #################################
    def test_clear_message_on_cn_fail3(self):
        """ Clear Message On Controller Fail Case 3: Exception is thrown when getting command, no message was found
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: The expected message
        Mock: Messages to get because it goes outside of the method
            Return: the expected message in a dictionary
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method
        Cannot make an assertion since the exception is caught inside the method
        """
        ml = self.create_test_mainline_object()

        no_message_found = "NM No Message Found"

        self.mock_get_and_wait_for_reply.side_effect = Exception(no_message_found)

        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value=no_message_found), \
             mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: no_message_found}):
            ml.clear_message_on_cn(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

    #################################
    def test_verify_description_on_cn_pass1(self):
        """ Verify Description On Controller Pass Case 1: Exception is not raised """
        ml = self.create_test_mainline_object()
        ml.ds = 'Test Mainline 2'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test Mainline 2".format(opcodes.description))
        ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ml.verify_description_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_description_on_cn_fail1(self):
        """ Verify Description On Controller Fail Case 1: Value on controller does not match what is
        stored in ml.ds """
        ml = self.create_test_mainline_object()
        ml.ds = 'Test Program'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test".format(opcodes.description))
        ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'Description' value. Received: Test, Expected: Test Program"
        try:
            ml.verify_description_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_pipe_fill_time_on_cn_pass1(self):
        """ Verify Pipe Fill Time On Controller Pass Case 1: Exception is not raised """
        ml = self.create_test_mainline_object()
        ml.ft = 6
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.fill_time))
        ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ml.verify_pipe_fill_time_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_pipe_fill_time_on_cn_fail1(self):
        """ Verify Pipe Fille Time On Controller Fail Case 1: Value on controller does not match what is
        stored in ml.ft """
        ml = self.create_test_mainline_object()
        ml.ft = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.fill_time))
        ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'Pipe Fill Time' value. Received: 6, Expected: 5"
        try:
            ml.verify_pipe_fill_time_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_target_flow_on_cn_pass1(self):
        """ Verify Target Flow On Controller Pass Case 1: Exception is not raised """
        ml = self.create_test_mainline_object()
        ml.fl = 6
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.target_flow))
        ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ml.verify_target_flow_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_target_flow_on_cn_fail1(self):
        """ Verify Target Flow On Controller Fail Case 1: Value on controller does not match what is
        stored in ml.fl """
        ml = self.create_test_mainline_object()
        ml.fl = 5.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.target_flow))
        ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'Target Flow' value. Received: 6.0, Expected: 5.0"
        try:
            ml.verify_target_flow_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_limit_zones_by_flow_state_on_cn_pass1(self):
        """ Verify Limit Zones By Flow State On Controller Pass Case 1: Exception is not raised """
        ml = self.create_test_mainline_object()
        ml.lc = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=TR".format(opcodes.limit_zones_by_flow))
        ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ml.verify_limit_zones_by_flow_state_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_limit_zones_by_flow_state_on_cn_fail1(self):
        """ Verify Limit Zones By Flow State On Controller Fail Case 1: Value on controller does not match what is
        stored in ml.lc """
        ml = self.create_test_mainline_object()
        ml.lc = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=TR".format(opcodes.limit_zones_by_flow))
        ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'Limit Zones By Flow State' value. Received: TR, Expected: Tr"
        try:
            ml.verify_limit_zones_by_flow_state_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_high_variance_limit_on_cn_pass1(self):
        """ Verify High Variance Limit On Controller Pass Case 1: Exception is not raised """
        ml = self.create_test_mainline_object()
        ml.hv = 6
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.high_variance_limit))
        ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ml.verify_high_variance_limit_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_high_variance_limit_on_cn_fail1(self):
        """ Verify High Variance Limit On Controller Fail Case 1: Value on controller does not match what is
        stored in ml.hv """
        ml = self.create_test_mainline_object()
        ml.hv = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.high_variance_limit))
        ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'High Variance Limit' value. Received: 6, Expected: 5"
        try:
            ml.verify_high_variance_limit_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_high_variance_shut_down_on_cn_pass1(self):
        """ Verify High Variance Shutdown On Controller Pass Case 1: Exception is not raised """
        ml = self.create_test_mainline_object()
        ml.hs = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=TR".format(opcodes.high_flow_variance_shutdown))
        ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ml.verify_high_variance_shut_down_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_high_variance_shut_down_on_cn_fail1(self):
        """ Verify High Variance Shutdown On Controller Fail Case 1: Value on controller does not match what is
        stored in ml.hs """
        ml = self.create_test_mainline_object()
        ml.hs = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=TR".format(opcodes.high_flow_variance_shutdown))
        ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'High Variance Shutdown' value. Received: TR, Expected: Tr"
        try:
            ml.verify_high_variance_shut_down_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_low_variance_limit_on_cn_pass1(self):
        """ Verify Low Variance Limit On Controller Pass Case 1: Exception is not raised """
        ml = self.create_test_mainline_object()
        ml.lv = 6
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.low_variance_limit))
        ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ml.verify_low_variance_limit_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_low_variance_limit_on_cn_fail1(self):
        """ Verify Low Variance Limit On Controller Fail Case 1: Value on controller does not match what is
        stored in ml.lv """
        ml = self.create_test_mainline_object()
        ml.lv = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.low_variance_limit))
        ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'Low Variance Limit' value. Received: 6, Expected: 5"
        try:
            ml.verify_low_variance_limit_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_low_variance_shut_down_on_cn_pass1(self):
        """ Verify Low Variance Shutdown On Controller Pass Case 1: Exception is not raised """
        ml = self.create_test_mainline_object()
        ml.ls = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=TR".format(opcodes.low_variance_shut_down))
        ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ml.verify_low_variance_shut_down_on_cn()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_low_variance_shut_down_on_cn_fail1(self):
        """ Verify Low Variance Shutdown On Controller Fail Case 1: Value on controller does not match what is
        stored in ml.ls """
        ml = self.create_test_mainline_object()
        ml.ls = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=TR".format(opcodes.low_variance_shut_down))
        ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'Low Variance Shutdown' value. Received: TR, Expected: Tr"
        try:
            ml.verify_low_variance_shut_down_on_cn()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_message_on_cn_pass1(self):
        """
        verify_message_on_cn pass case 1
        Create test mainline object
        Mock: get_message_on_cn because it goes outside of the method
        Store: Expected values each build message string, so the values can be mocked
        Return: None so it does not fail
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
        Return: Three separate strings, for expected_message_text, expected_message_id, and expected_date_time_from_cn
        Mock: build_message_string (note the [opcodes.message_id])
        Return: Four different strings, different than the ones used for expected_message_id, controller_date_time
        expected_date_time, and expected_message_text.
        Run: the test, and pass a status code in
        Compare: Actual strings from the build_message_string methods to the first expected string. All three strings
        are the same, so just comparing the first is okay.
        """
        # Create test mainline object
        ml = self.create_test_mainline_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Mock get message on cn
        mock_get_message_on_cn = mock.MagicMock(side_effect=['thing'])
        ml.get_message_on_cn = mock_get_message_on_cn

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, message_date])
        ml.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        # Mock build_message_string
        # mock_build_message_string = mock.MagicMock(return_value='diffstring')
        # ml.build_message_string = mock_build_message_string
        # print "build message string is" + ml.build_message_string
        ml.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Run test
        ml.verify_message_on_cn(_status_code='OK')

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual({'DT': '5/20/15 3:20:11', 'ID': 'blah2', 'TX': 'blah'}, ml.build_message_string)

    #################################
    def test_verify_message_on_cn_fail1(self):
        """ Verify message on controller Fail Case 1: Created ID doesn't match controller ID, raise ValueError
        Create test mainline object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id (which will be wrong so an
            ValueError is thrown), and expected_date_time_from_cn
        Manually fill up the build_message_string variables in the mainline object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        ml = self.create_test_mainline_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message ID (This is so we can purposefully force the ValueError)
        wrong_message_id = 'blah3'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, wrong_message_id, message_date])
        ml.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        ml.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "Created ID message did not match the ID received from the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           ml.build_message_string[opcodes.message_id],    # {0} The ID that was built
                           wrong_message_id                                # {1} The ID returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            ml.verify_message_on_cn(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_on_cn_fail2(self):
        """ Verify message on controller Fail Case 2: Created DT doesn't match controller DT, raise ValueError
        Create test mainline object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the mainline object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        ml = self.create_test_mainline_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError)
        wrong_message_dt = '5/11/11 3:59:59'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, wrong_message_dt])
        ml.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        ml.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "The date and time of the message didn't match the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           ml.build_message_string[opcodes.date_time],  # {0} The DT that was built
                           wrong_message_dt                             # {1} The DT returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            ml.verify_message_on_cn(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_on_cn_fail3(self):
        """ Verify message on controller Fail Case 3: Created DT doesn't match controller DT, but are within 60 minutes, raise ValueError
        Create test mainline object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the mainline object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        ml = self.create_test_mainline_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError, but still within 60 minutes)
        wrong_message_dt = '5/20/15 3:59:11'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, wrong_message_dt])
        ml.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        ml.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "The date and time of the message didn't match the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           ml.build_message_string[opcodes.date_time],  # {0} The DT that was built
                           wrong_message_dt                             # {1} The DT returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            ml.verify_message_on_cn(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_on_cn_fail4(self):
        """ Verify message on controller Fail Case 4: Created TX doesn't match controller TX, raise ValueError
        Create test mainline object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message_on_cn because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg_on_cn.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the mainline object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        ml = self.create_test_mainline_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError, but still within 60 minutes)
        wrong_message_tx = 'blah3'

        # Mock get_value_string_by_key
        mock_mg_on_cn = mock.MagicMock()
        mock_mg_on_cn.get_value_string_by_key = mock.MagicMock(side_effect=[wrong_message_tx, message_id, message_date])
        ml.get_message_on_cn = mock.MagicMock(return_value=mock_mg_on_cn)

        ml.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "Created TX message did not match the TX received from the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           ml.build_message_string[opcodes.message_text],  # {0} The TX that was built
                           wrong_message_tx                                 # {1} The TX returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            ml.verify_message_on_cn(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_who_i_am_pass1(self):
        """
        Create test mainline object
        Mock: self.get_data because it goes outside of the method
        Return: None, so it does not fail
        Mock: self.verify_description_on_cn because it goes outside of the method
        Return: None, so it does not fail
        Mock: self.verify_pipe_fill_time_on_cn because it goes outside of the method
        Return: None, so it does not fail
        Mock: self.verify_target_flow_on_cn because it goes outside of the method
        Return: None, so it does not fail
        Mock: self.verify_limit_zones_by_flow_state_on_cn because it goes outside of the method
        Return: None, so it does not fail
        Mock: self.verify_high_variance_limit_on_cn because it goes outside of the method
        Return: None, so it does not fail
        Mock: self.verify_high_variance_shut_down_on_cn because it goes outside of the method
        Return: None, so it does not fail
        Mock: self.verify_low_variance_limit_on_cn because it goes outside of the method
        Return: None, so it does not fail
        Mock: self.verify_low_variance_shut_down_on_cn because it goes outside of the method
        Return: None, so it does not fail
        Run: Test, and if the test passes, it has successfully run through the code. There is no need to compare
        any values for this particular case.
        """
        # Create test mainline object
        ml = self.create_test_mainline_object()

        # Mock get_data
        mock_get_data = mock.MagicMock(side_effect=None)
        ml.get_data = mock_get_data

        # Mock verify_description_on_cn
        mock_verify_description_on_cn = mock.MagicMock(side_effect=None)
        ml.verify_description_on_cn = mock_verify_description_on_cn

        # Mock pipe_fill_time_on_cn
        mock_verify_pipe_fill_time_on_cn = mock.MagicMock(side_effect=None)
        ml.verify_pipe_fill_time_on_cn = mock_verify_pipe_fill_time_on_cn

        # Mock verify_target_flow_on_cn
        mock_verify_target_flow_on_cn = mock.MagicMock(side_effect=None)
        ml.verify_target_flow_on_cn = mock_verify_target_flow_on_cn

        # Mock verify_limit_zones_by_flow_state_on_cn
        mock_verify_limit_zones_by_flow_state_on_cn = mock.MagicMock(side_effect=None)
        ml.verify_limit_zones_by_flow_state_on_cn = mock_verify_limit_zones_by_flow_state_on_cn

        # Mock verify_high_variance_limit_on_cn
        mock_verify_high_variance_limit_on_cn = mock.MagicMock(side_effect=None)
        ml.verify_high_variance_limit_on_cn = mock_verify_high_variance_limit_on_cn

        # Mock verify_high_variance_shut_down_on_cn
        mock_verify_high_variance_shut_down_on_cn = mock.MagicMock(side_effect=None)
        ml.verify_high_variance_shut_down_on_cn = mock_verify_high_variance_shut_down_on_cn

        # Mock verify_low_variance_limit_on_cn
        mock_verify_low_variance_limit_on_cn = mock.MagicMock(side_effect=None)
        ml.verify_low_variance_limit_on_cn = mock_verify_low_variance_limit_on_cn

        # Mock verify_low_variance_shut_down_on_cn
        mock_verify_low_variance_shut_down_on_cn = mock.MagicMock(side_effect=None)
        ml.verify_low_variance_shut_down_on_cn = mock_verify_low_variance_shut_down_on_cn

        # Run test
        ml.verify_who_i_am()

    if __name__ == "__main__":
        unittest.main()
