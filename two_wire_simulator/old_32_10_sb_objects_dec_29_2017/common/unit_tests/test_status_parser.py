import unittest
from unittest import TestCase
import mock
from mock import call
import status_parser


__author__ = 'ahill'


class TestKeyValue(TestCase):
    """
    A KeyValue is a string Key followed by a Value, of the form
         foo=bar
    although the Value can also be a semicolon-separated list of strings, e.g.
         foo=bar1;bar2;bar3;bar4
    Both the = and the ; are legal characters in the Key and Value, although in
    that case they must be escaped with a \, e.g.
        f\=\;00=bar1;ba\;\=r2
    """

    def test_showme(self):
        my_mock = mock.MagicMock()
        f_mock = mock.MagicMock()
        # Patch sys.stdout.write so we can tests for calls to print().   Each call to print()
        # results in two calls to write() -- the first is the string to be printed, and the second
        # is a newline
        my_mock.write = f_mock
        key = 'foo'
        value = 'bar'
        calls = [call('KeyValue '),
                 call('\n'),
                 call('\t\t--------- KeyValue key:' + key),
                 call('\n'),
                 call(' \t\t\t\tvalue is ' + value),
                 call('\n')]
        with mock.patch('sys.stdout', my_mock):
            uut = status_parser.KeyValue(key + '=' + value)
            uut.showme()
        f_mock.assert_has_calls(calls)

    def test_showme_with_fromstring(self):
        my_mock = mock.MagicMock()
        f_mock = mock.MagicMock()
        # Patch sys.stdout.write so we can tests for calls to print().   Each call to print()
        # results in two calls to write() -- the first is the string to be printed, and the second
        # is a newline
        my_mock.write = f_mock
        key = 'foo'
        value = 'bar'
        fromstr = 'Miss Piggy'
        calls = [call('KeyValue From: \t' + fromstr),
                 call('\n'),
                 call('\t\t--------- KeyValue key:' + key),
                 call('\n'),
                 call(' \t\t\t\tvalue is ' + value),
                 call('\n')]
        with mock.patch('sys.stdout', my_mock):
            uut = status_parser.KeyValue(key + '=' + value)
            uut.showme(fromstr)
        f_mock.assert_has_calls(calls)

    def test_eq_false(self):
        # Exercise the _eq_ path where comparing to a non-KeyValue class
        first = status_parser.KeyValue('foo=bar')
        second = 'foo=bar'
        self.assertFalse(first == second)

    def test_neq(self):
        # Exercise the _neq_ operator
        first = status_parser.KeyValue('foo=bar')
        second = status_parser.KeyValue('foo=var')
        self.assertTrue(first != second)

    def test_clean_init(self):
        uut = status_parser.KeyValue()
        self.assertEqual(uut.key, None)
        self.assertEqual(uut.value, None)

    def test_single(self):
        uut = status_parser.KeyValue('foo=bar')
        self.assertEqual(uut.key, 'foo')
        expected = status_parser.Value('bar')
        self.assertEqual(uut.value, expected)

    def test_single_multiple_equals(self):
        uut = status_parser.KeyValue('foo=bar=baz=whatsis')
        self.assertEqual(uut.key, 'foo')
        expected = status_parser.Value('bar=baz=whatsis')
        self.assertEqual(uut.value, expected)

    def test_null_value(self):
        uut = status_parser.KeyValue('foo=')
        self.assertEqual(uut.key, 'foo')
        expected = status_parser.Value('')
        self.assertEqual(uut.value, expected)

    def test_multiple(self):
        uut = status_parser.KeyValue('foo=bar;whatsis;howzat?')
        self.assertEqual(uut.key, 'foo')
        expected = status_parser.Value('bar;whatsis;howzat?')
        self.assertEqual(uut.value, expected)

    def test_single_escaped(self):
        uut = status_parser.KeyValue('f\;o\=o=b\=a\;r')
        self.assertEqual(uut.key, 'f\;o\=o')
        expected = status_parser.Value('b\=a\;r')
        self.assertEqual(uut.value, expected)

    def test_multiple_escaped(self):
        uut = status_parser.KeyValue('f\=o\;o=b\=ar;wh\;\=atsis;h\=\;\=owzat?')
        self.assertEqual(uut.key, 'f\=o\;o')
        expected = status_parser.Value('b\=ar;wh\;\=atsis;h\=\;\=owzat?')
        self.assertEqual(uut.value, expected)

    def test_get_value_string(self):
        uut = status_parser.KeyValue('foo=bar;whatsis;howzat?')
        val = uut.get_value_string()
        self.assertEqual(val, 'bar;whatsis;howzat?')


class TestKeyValues(TestCase):
    """
    KeyValues look just like a KeyValue (a Key followed by a Value).   The only difference is that they exist
    only as a part of a KeyValue.   They are separated from other parts of the KeyValue (and each other, if there are
    more than one), by the comma (",") symbol
    """
    def test_showme(self):
        my_mock = mock.MagicMock()
        f_mock = mock.MagicMock()
        # Patch sys.stdout.write so we can tests for calls to print().   Each call to print()
        # results in two calls to write() -- the first is the string to be printed, and the second
        # is a newline
        my_mock.write = f_mock
        key1 = 'foo'
        value1 = 'bar'
        key2 = 'whatsis'
        value2 = 'itnot'
        calls = [call('KeyValue '),
                 call('\n'),
                 call('\t\t--------- KeyValue key:' + key1),
                 call('\n'),
                 call(' \t\t\t\tvalue is ' + value1),
                 call('\n'),
                 call('KeyValue '),
                 call('\n'),
                 call('\t\t--------- KeyValue key:' + key2),
                 call('\n'),
                 call(' \t\t\t\tvalue is ' + value2),
                 call('\n')]
        with mock.patch('sys.stdout', my_mock):
            uut = status_parser.KeyValues(key1 + '=' + value1 + ',' + key2 + '=' + value2)
            uut.showme()
        f_mock.assert_has_calls(calls)

    def test_eq_false(self):
        # Exercise the _eq_ path where comparing to a non-KeyValues class
        first = status_parser.KeyValues('foo=bar,whatsis=itsis')
        second = 'foo=bar'
        self.assertFalse(first == second)

    def test_neq(self):
        # Exercise the _ne_ operator
        first = status_parser.KeyValues('foo=bar,whatsis=itsis')
        second = status_parser.KeyValues('foo=bar,whatsis=itnot')
        self.assertTrue(first != second)

    def test_clean_init(self):
        uut = status_parser.KeyValues()
        self.assertEqual(len(uut.get_key_values()), 0)

    def test_single_keyvalue_single_value(self):
        uut = status_parser.KeyValues('foo=bar')
        keyvalues = uut.get_key_values()
        self.assertEqual(len(keyvalues), 1)
        expected = status_parser.KeyValue('foo=bar')
        self.assertEqual(keyvalues[0], expected)

    def test_single_keyvalue_multiple_values(self):
        uut = status_parser.KeyValues('foo=bar;whatsis;howzat?')
        keyvalues = uut.get_key_values()
        self.assertEqual(len(keyvalues), 1)
        expected = status_parser.KeyValue('foo=bar;whatsis;howzat?')
        self.assertEqual(keyvalues[0], expected)

    def test_multiple_keyvalues_single_values(self):
        uut = status_parser.KeyValues('foo=bar,whatsis=itsis')
        keyvalues = uut.get_key_values()
        self.assertEqual(len(keyvalues), 2)
        expected = status_parser.KeyValue('foo=bar')
        self.assertEqual(keyvalues[0], expected)
        expected = status_parser.KeyValue('whatsis=itsis')
        self.assertEqual(keyvalues[1], expected)

    def test_multiple_keyvalues_single_values_2(self):
        uut = status_parser.KeyValues('SS=DN,VA=NA,VV=NA,VT=1.7')
        keyvalues = uut.get_key_values()
        self.assertEqual(len(keyvalues), 4)
        expected = status_parser.KeyValue('SS=DN')
        self.assertEqual(keyvalues[0], expected)
        expected = status_parser.KeyValue('VA=NA')
        self.assertEqual(keyvalues[1], expected)
        expected = status_parser.KeyValue('VV=NA')
        self.assertEqual(keyvalues[2], expected)
        expected = status_parser.KeyValue('VT=1.7')
        self.assertEqual(keyvalues[3], expected)
        # self.assertEqual(uut.getKeyValueByKey('VT'), 'VT=1.7')
        self.assertEqual(uut.get_value_string_by_key('VT'), '1.7')

    def test_multiple_keyvalues_single_values_3(self):
        testdata = 'DS=Test Program 1,EN=TR,SS=DN,CI=WD,WD=0101010,WW=011111100001111111111110,ST=480=540=600=660,PR=2'
        uut = status_parser.KeyValues(testdata)
        keyvalues = uut.get_key_values()
        self.assertEqual(len(keyvalues), 8)
        expected = status_parser.KeyValue('DS=Test Program 1')
        self.assertEqual(keyvalues[0], expected)
        expected = status_parser.KeyValue('EN=TR')
        self.assertEqual(keyvalues[1], expected)
        expected = status_parser.KeyValue('SS=DN')
        self.assertEqual(keyvalues[2], expected)
        expected = status_parser.KeyValue('CI=WD')
        self.assertEqual(keyvalues[3], expected)
        expected = status_parser.KeyValue('WD=0101010')
        self.assertEqual(keyvalues[4], expected)
        expected = status_parser.KeyValue('WW=011111100001111111111110')
        self.assertEqual(keyvalues[5], expected)
        expected = status_parser.KeyValue('ST=480=540=600=660')
        self.assertEqual(keyvalues[6], expected)
        expected = status_parser.KeyValue('PR=2')
        self.assertEqual(keyvalues[7], expected)

    def test_multiple_keyvalues_calibration_result(self):
        dtkeyvalue = 'DT=08/28/14 22:00:00'
        txkeyvalue = 'TX=-------------------------\n'
        txkeyvalue += 'biSensor: Test moisture sensor 1\n'
        txkeyvalue += '08/28/14 10:00 PM\, Priority: None\n'
        txkeyvalue += '-------------------------\n'
        txkeyvalue += 'Moisture Sensor Calibration Done:\n'
        txkeyvalue += 'Watering limits changed.\n'
        txkeyvalue += 'Lower Limit = 20.875\n'
        txkeyvalue += 'Upper Limit = 25.875'
        prkeyvalue = 'PR=NN'
        v1keyvalue = 'V1=20.875'
        v2keyvalue = 'V2=25.875'
        pckeyvalue = 'PC=1'
        pgkeyvalue = 'PG=1'
        znkeyvalue = 'ZN=1'
        testdata = dtkeyvalue + ',' + txkeyvalue + ',' + prkeyvalue + ',' + v1keyvalue + ',' + v2keyvalue + ','
        testdata += pckeyvalue + ',' + pgkeyvalue + ',' + znkeyvalue
        uut = status_parser.KeyValues(testdata)
        keyvalues = uut.get_key_values()
        self.assertEqual(len(keyvalues), 8)
        expected = status_parser.KeyValue(dtkeyvalue)
        self.assertEqual(keyvalues[0], expected)
        expected = status_parser.KeyValue(txkeyvalue)
        self.assertEqual(keyvalues[1], expected)
        expected = status_parser.KeyValue(prkeyvalue)
        self.assertEqual(keyvalues[2], expected)
        expected = status_parser.KeyValue(v1keyvalue)
        self.assertEqual(keyvalues[3], expected)
        expected = status_parser.KeyValue(v2keyvalue)
        self.assertEqual(keyvalues[4], expected)
        expected = status_parser.KeyValue(pckeyvalue)
        self.assertEqual(keyvalues[5], expected)
        expected = status_parser.KeyValue(pgkeyvalue)
        self.assertEqual(keyvalues[6], expected)
        expected = status_parser.KeyValue(znkeyvalue)
        self.assertEqual(keyvalues[7], expected)

    # def test_multiple_keyvalues_calibration_result2(self):
    #     dtkeyvalue = 'DT=08/28/14 22:00:00'
    #     txkeyvalue = 'TX=-------------------------\\\n'
    #     txkeyvalue += 'biSensor\: Test moisture sensor 1\\\n'
    #     txkeyvalue += '08\/28\/14 10:00 PM\, Priority\: None\\\n'
    #     txkeyvalue += '-------------------------\\\n'
    #     txkeyvalue += 'Moisture Sensor Calibration Done\:\\\n'
    #     txkeyvalue += 'Watering limits changed.\\\n'
    #     txkeyvalue += 'Lower Limit \= 20.875\\\n'
    #     txkeyvalue += 'Upper Limit \= 25.875'
    #     prkeyvalue = 'PR=NN'
    #     v1keyvalue = 'V1=20.875'
    #     v2keyvalue = 'V2=25.875'
    #     pckeyvalue = 'PC=1'
    #     pgkeyvalue = 'PG=1'
    #     znkeyvalue = 'ZN=1'
    #     testdata = dtkeyvalue + ',' + txkeyvalue + ',' + prkeyvalue + ',' + v1keyvalue + ',' + v2keyvalue + ','
    #     testdata += pckeyvalue + ',' + pgkeyvalue + ',' + znkeyvalue
    #     uut = status_parser.KeyValues(testdata)
    #     keyvalues = uut.get_key_values()
    #     self.assertEqual(len(keyvalues), 8)
    #     expected = status_parser.KeyValue(dtkeyvalue)
    #     self.assertEqual(keyvalues[0], expected)
    #     expected = status_parser.KeyValue(txkeyvalue)
    #     self.assertEqual(keyvalues[1], expected)
    #     expected = status_parser.KeyValue(prkeyvalue)
    #     self.assertEqual(keyvalues[2], expected)
    #     expected = status_parser.KeyValue(v1keyvalue)
    #     self.assertEqual(keyvalues[3], expected)
    #     expected = status_parser.KeyValue(v2keyvalue)
    #     self.assertEqual(keyvalues[4], expected)
    #     expected = status_parser.KeyValue(pckeyvalue)
    #     self.assertEqual(keyvalues[5], expected)
    #     expected = status_parser.KeyValue(pgkeyvalue)
    #     self.assertEqual(keyvalues[6], expected)
    #     expected = status_parser.KeyValue(znkeyvalue)
    #     self.assertEqual(keyvalues[7], expected)

    def test_multiple_keyvalues_multiple_values(self):
        uut = status_parser.KeyValues('foo=bar;whatsis;howzat?,whatsis=bar1;whatsis2;howzat?3')
        keyvalues = uut.get_key_values()
        self.assertEqual(len(keyvalues), 2)
        expected = status_parser.KeyValue('foo=bar;whatsis;howzat?')
        self.assertEqual(keyvalues[0], expected)
        expected = status_parser.KeyValue('whatsis=bar1;whatsis2;howzat?3')
        self.assertEqual(keyvalues[1], expected)

    def test_get_keyvalue_by_id(self):
        uut = status_parser.KeyValues('foo=bar;whatsis;howzat?,whatsis=bar1;whatsis2;howzat?3')
        prop = uut.get_key_value_by_key('whatsis')
        self.assertEqual(prop.value, status_parser.Value('bar1;whatsis2;howzat?3'))

    def test_keyvalue_by_id_not_found(self):
        # Searching keyvalues for a missing key should return None
        uut = status_parser.KeyValues('foo=bar;whatsis;howzat?,whatsis=bar1;whatsis2;howzat?3')
        prop = uut.get_key_value_by_key('whosis')
        self.assertEqual(prop, None)

    def test_replace_keyvalue_by_id(self):
        uut = status_parser.KeyValues('foo=bar;whatsis;howzat?,whatsis=bar1;whatsis2;howzat?3')
        uut.replace_value_string_by_key('whatsis', 'new;newer;newest')
        prop = uut.get_key_value_by_key('whatsis')
        self.assertEqual(prop.value, status_parser.Value('new;newer;newest'))

    def test_replace_keyvalue_by_id_not_found(self):
        # Trying to replace a keyvalue for a missing key should return None
        uut = status_parser.KeyValues('foo=bar;whatsis;howzat?,whatsis=bar1;whatsis2;howzat?3')
        result = uut.replace_value_string_by_key('whosis', 'new;newer;newest')
        self.assertEqual(result, None)

    def test_get_keyvalue_value_string_by_id(self):
        uut = status_parser.KeyValues('foo=bar;whatsis;howzat?,whatsis=bar1;whatsis2;howzat?3')
        val = uut.get_value_string_by_key('whatsis')
        self.assertEqual(val, 'bar1;whatsis2;howzat?3')

    def test_get_keyvalue_value_string_by_id_not_found(self):
        # Getting a keyvalue value string for a missing key should return None
        uut = status_parser.KeyValues('foo=bar;whatsis;howzat?,whatsis=bar1;whatsis2;howzat?3')
        val = uut.get_value_string_by_key('whosis')
        self.assertEqual(val, None)


class TestValue(TestCase):
    """
    A Value parses one or more semicolon-seperated strings
    (i.e., foo, or foo;bar;foobar)
    Strings can contain the semicolon, but must be escaped with the \ character
    """

    def test_showme(self):
        my_mock = mock.MagicMock()
        f_mock = mock.MagicMock()
        # Patch sys.stdout.write so we can tests for calls to print().   Each call to print()
        # results in two calls to write() -- the first is the string to be printed, and the second
        # is a newline
        my_mock.write = f_mock
        value = 'hoodle'
        calls = [call(' \t\t\t\tvalue is ' + value),
                 call('\n')]
        with mock.patch('sys.stdout', my_mock):
            uut = status_parser.Value(value)
            uut.showme()
        f_mock.assert_has_calls(calls)

    def test_eq_false(self):
        # Exercise the _eq_ path where comparing to a non-Value class
        first = status_parser.Value('bar')
        second = 'bar'
        self.assertFalse(first == second)

    def test_neq(self):
        # Exercise the _neq_ operator
        first = status_parser.Value('bar')
        second = status_parser.Value('var')
        self.assertTrue(first != second)

    def test_clean_init(self):
        uut = status_parser.Value()
        self.assertEqual(len(uut.value_list), 0)

    def test_parse_single(self):
        uut = status_parser.Value('foo')
        self.assertEqual(uut.value_list[0], 'foo')

    def test_parse_empty(self):
        uut = status_parser.Value('')
        self.assertEqual(uut.value_list, [])

    def test_parse_null(self):
        uut = status_parser.Value(None)
        self.assertEqual(uut.value_list, [])

    def test_parse_multiple(self):
        uut = status_parser.Value('foo;bar;whatsis')
        self.assertEqual(len(uut.value_list), 3)
        self.assertEqual(uut.value_list[0], 'foo')
        self.assertEqual(uut.value_list[1], 'bar')
        self.assertEqual(uut.value_list[2], 'whatsis')

    def test_parse_single_escaped(self):
        uut = status_parser.Value('f\;oo\;bar')
        self.assertEqual(uut.value_list[0], 'f;oo;bar')

    def test_parse_multiple_escaped(self):
        uut = status_parser.Value('f\;oo;ba\;r;what\;sis')
        self.assertEqual(len(uut.value_list), 3)
        self.assertEqual(uut.value_list[0], 'f;oo')
        self.assertEqual(uut.value_list[1], 'ba;r')
        self.assertEqual(uut.value_list[2], 'what;sis')

    def test_get_value_string(self):
        uut = status_parser.Value('foo;bar;whatsis;whosis')
        self.assertEqual(uut.get_value_string(), 'foo;bar;whatsis;whosis')


if __name__ == '__main__':
    unittest.main()