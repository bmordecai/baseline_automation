from old_32_10_sb_objects_dec_29_2017.common.logging_handler import LoggingHandler
from automated_tests_wrapper import run_use_cases
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes


########################################################################################################################
#                                                USER CONFIGURATION                                                    #
########################################################################################################################
"""
This section currently contains:
    - A reference to the file path that contains:
        - Your username and password to BaseManager, as well as the url, site name, and company.
        - MAC Address and socket port of your controllers and substations.
        - Path of your browser drivers for selenium testing.
"""
# Load in user configured items from text file (necessary for serial connection)
user_configuration_file_name = 'user_credentials_eldin.json'

########################################################################################################################
#                                                  HARDWARE SPECS                                                      #
########################################################################################################################
"""
This section currently contains:
    - Serial Number for the 3200 Controller.
    - Firmware versions for the 3200, 1000, or substation that you will be running the tests against.
"""
# 3200 Serial number and firmware version
controller_3200_serial_num = '3K10001'
controller_3200_firm_version = '12.32'

# 1000 firmware version
controller_1000_firm_version = '0.171'

# Substation firmware version
substation_firm_version = "1.0.21"

########################################################################################################################
#                                                CONTROL VARIABLES                                                     #
########################################################################################################################
"""
This section currently contains:
    - The numbers of the use cases you would like to run.
        Ex: 'tests_to_run_3200 = [1, 2, 3]' will run 3200 use cases 1, 2, and 3.
        Ex: 'tests_to_run_3200 = opcodes.run_all_use_cases' will run all 3200 use cases.
        EX: 'tests_to_run_3200 = []' will not run any 3200 use cases.
    - A method that allows you to specify if you want the program to terminate on errors, or if you want to log them.
    - The location/name of your log file.
"""
# These lines specify what tests you want to run.
# If you want to run all tests under a given category, put 'opcodes.run_all_use_cases' after the equal sign.
# If you want to run individual tests, simply put the number of the use case in the square braces, separated by commas.
tests_to_run_1000 = [1, 2, 3]
tests_to_run_3200 = []
tests_to_run_jade = []
tests_to_run_bacnet = []
tests_to_run_substations = opcodes.run_all_use_cases
tests_to_run_base_manager = []

# True - Run through your use cases without stopping on errors. We will log any errors along with starts/passes/fails
# False - Run through your use cases and crash on the first error you encounter.
LoggingHandler.enable_continuous_run_and_logging_for_use_cases(enabled=False)

# The location of the log file if you enable logging above this
LoggingHandler.LOG_FILENAME = "_eldin_run_automated_test_suite_log.txt"

# Pass in all the variables to run the use cases you selected
run_use_cases(firmware_version_1000=controller_1000_firm_version,
              firmware_version_3200=controller_3200_firm_version,
              firmware_version_substation=substation_firm_version,
              user_configuration_file_name=user_configuration_file_name,
              tests_to_run_1000=tests_to_run_1000,
              tests_to_run_jade=tests_to_run_jade,
              tests_to_run_bacnet=tests_to_run_bacnet,
              tests_to_run_base_manager=tests_to_run_base_manager,
              tests_to_run_3200=tests_to_run_3200,
              tests_to_run_substation=tests_to_run_substations,
              serial_number_3200_controller=controller_3200_serial_num,)
