__author__ = 'baseline'
import sys
import time

import old_32_10_sb_objects_dec_29_2017.common.configuration as test_conf
import old_32_10_sb_objects_dec_29_2017.common.user_configuration as user_conf
import old_32_10_sb_objects_dec_29_2017.page_factory as factory
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from datetime import datetime, timedelta
from selenium.webdriver.common.keys import Keys

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler


class BaseManagerUseCase6(object):
    """
    Purpose:
        This test verifies the following BaseManager functionality for the Subscription for BaseStation 1000's: \n
        - load a controller with devices
        - Assign each device to an address/serial number and give descriptions
        - verify that if a subscriptions is greater than 15 day past the renewal date for a basic subscription and 30
          days for advanced, it only shows LiveView

    Coverage Area: \n
        - assuming controller is already a part of Basemanager
        - subscriptions type with different renewal dates
            - Basic
                - 15 days past renewal date
            - advanced
                - 30 days past renewal date

    Not Covered: \n
        - going from a non-subscriber to a subscriber

    """
    # ~~~~ Local Variables ~~~~ #

    def __init__(self, controller_type, test_name, user_configuration_instance, json_configuration_file):
        """
        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = test_conf.Configuration(cn_type=controller_type,
                                              test_name=test_name,
                                              user_conf_file=user_configuration_instance,
                                              data_json_file=json_configuration_file)

        # Get web driver instance from config
        self.web_driver = None

        # Create page instances to use for Client use
        self.main_menu = None

        # Main Tab Page Objects
        self.login_page = None
        self.main_page = None
        self.quick_view_tab = None
        self.live_view_tab = None

        # Other page objects
        self.subscriptions_page = None

    def run_use_case(self):
        """
        The following steps are done on all browser specified in the user configuration: \n

        Step 1:
            - Initialize the controller
                1. Clear all programming
                2. Load all necessary devices from json file
                3. Do search for all devices

        Step 2:
            - initialize & open browser

        step 3:
            - login as company admin & verify successful logged in

        Step 4:
            - select site
            - set the values of both controllers to default up to date subscriptions
            - save changes
            - log out of admin

        Step 5:
            - login as super user & verify successful logged in

        Step 6:
            - Select Company
            - select site
            - select 1000 controller
            - verify all features are available
            - logout

        Step 7:
            - login as super user & verify successfully logged in

        step 8:
            - select site
            - set the renewal date value of the 1000 controller(basic subscription) to be 15 days before today's date
            - save changes
            - log out of super user

        step 9:
            - rerun step 5 (logging in as normal user)
            - select company
            - select site
            - select 1000 controller
            - verify only LiveView is available

        step 3 & 4:
            - we run step 4 again so that our controller subscriptions are usable again, ensuring other tests don't run
              into problems with lack of features.

        """
        try:
            # browsers_for_test = ["chrome", "firefox"]
            browsers_for_test = ["chrome"]
            for run_number, browser in enumerate(browsers_for_test):
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test()
                self.step_1()
                self.step_2(_browser=browser)
                self.step_3()
                self.step_4()
                self.step_5()
                self.step_6()
                self.step_7()
                self.step_8()
                self.step_9()
                self.step_3()
                self.step_4()

                # Close the first webdriver to start the second web driver
                if run_number == 0 and len(browsers_for_test) > 1:
                    self.config.resource_handler.web_driver.web_driver.quit()

                else:
                    print "Successfully executed in: '{browser}'".format(browser=browser)
                    time.sleep(5)

        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        else:
            helper_methods.print_test_passed(self.config.test_name)

        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)

    def step_1(self):
        """
        Initialize the controller:
        1. Clear programming
        2. Load devices
        3. Do Search for all devices
        """

        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        try:
            # Clear programming
            self.config.controllers[1].init_cn()

            # Load all devices
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            # Address Zones
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.zones,
                                                               ad_range=self.config.zn_ad_range)

            # Address Master Valves
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.master_valves,
                                                               ad_range=self.config.mv_ad_range)

            # Address Moisture Sensors
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.moisture_sensors,
                                                               ad_range=self.config.ms_ad_range)

            # Address Temperature Sensors
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.temperature_sensors,
                                                               ad_range=self.config.ts_ad_range)

            # Address Event Switches
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.event_switches,
                                                               ad_range=self.config.sw_ad_range)

            # Address Flow Meters
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.flow_meters,
                                                               ad_range=self.config.fm_ad_range)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

        print "----"
        print "Successfully finished Step 1 in BaseManager 1000 Subscription Test"
        print "-> Controller Init: Cleared programming, loaded all necessary devices, searched for all devices"
        print "----"

    def step_2(self, _browser):
        """
        Initializes the web driver to the correct browser executor
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        # Get web driver instance from config
        self.web_driver = self.config.resource_handler.web_driver

        # Create page instances to use for Client use
        self.main_menu = factory.get_main_menu_object(_webdriver=self.web_driver)

        # Main Tab Page Objects
        self.login_page = factory.get_login_page_object(_webdriver=self.web_driver)
        self.main_page = factory.get_main_page_object(_webdriver=self.web_driver)
        self.quick_view_tab = factory.get_quick_view_page_object(_webdriver=self.web_driver)
        self.live_view_tab = factory.get_live_view_page_object(_webdriver=self.web_driver)

        # Other page objects
        self.subscriptions_page = factory.get_subscription_page_object(_webdriver=self.web_driver)

        # Open browser
        self.config.resource_handler.web_driver.open(_browser)
        print "----"
        print "Successfully finished Step 2 in BaseManager 1000 Subscription Test"
        print "-> Opened Web Browser"
        print "----"

    def step_3(self):
        """
        -login as super user
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        try:
            # self.login_page.enter_login_info(_user_name=self.config.user_conf.user_name, _password=self.config.user_conf.user_password)
            self.login_page.enter_login_info(_user_name='SuperAutoTests', _password='SuperTest@10259')
            self.login_page.click_login_button()
            self.main_page.verify_open()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)
        print "----"
        print "Successfully finished Step 3 in BaseManager 1000 Subscription Test"
        print "-> Logged In as Super User"
        print "----"

    def step_4(self):
        """
        - Set default values for both controllers subscriptions
        - Select administration
        - Select company
        - Select site
        - Set subscription to basic on controller 1000
        - Change renewal date be up to date on both controllers
        - Exit out of the subscriptions tab
        - Logout
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        try:
            # select the subscription menu
            self.main_page.select_main_menu()
            self.main_menu.select_administration()
            self.main_menu.select_subscriptions()

            # This should send us to our new tab
            # self.web_driver.web_driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.TAB)
            allTabs = self.web_driver.web_driver.window_handles
            self.main_menu.driver.web_driver.switch_to_window(allTabs[1])

            # this is when a new tab opens up
            self.subscriptions_page.select_main_menu()
            self.subscriptions_page.select_a_company()

            # set the subscription to basic for a 1000 and advanced for a 3200
            self.subscriptions_page.click_edit_button()
            self.subscriptions_page.select_bm_subscription(mac_address=self.web_driver.conf.mac_address_for_1000)

            # change the renewal date to the last day of the current year for both controllers
            d = datetime.today()
            time_str = d.strftime("12/31/%y").replace('/0', '/')
            self.subscriptions_page.set_renewal_date(time_str, mac_address=self.web_driver.conf.mac_address_for_1000)
            self.subscriptions_page.click_save_button()

            # Exits us out of the tab
            self.subscriptions_page.driver.web_driver.close()
            self.subscriptions_page.driver.web_driver.switch_to_window(allTabs[0])

            # logout
            self.main_page.select_main_menu()
            self.main_menu.click_sign_out_button()
        except Exception, e:
            e_msg = e.msg if e.msg else e.message
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e_msg)
        print "----"
        print "Successfully finished Step 4 in BaseManager 1000 Subscription Test"
        print "-> Logged In"
        print "----"

    def step_5(self):
        """
        - Log in a normal user
        - Verify that the main page is open
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        # enter the user name and password selects login button
        self.login_page.enter_login_info()
        self.login_page.click_login_button()
        self.main_page.verify_open()
        print "----"
        print "Successfully finished Step 5 in BaseManager 1000 Subscription Test"
        print "-> Logged In as Basic User"
        print "----"

    def step_6(self):
        """
        - Selects a site and controller
        - Verifies QuickView Tab is opened
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        # select the 1000 controller and verify we can access all of its features
        self.main_page.select_main_menu()
        self.main_menu.select_site()
        self.main_menu.select_a_controller(mac_address=self.web_driver.conf.mac_address_for_1000)
        self.main_menu.wait_for_main_menu_close()
        self.quick_view_tab.verify_open()

        # log out
        self.main_page.select_main_menu()
        self.main_menu.click_sign_out_button()

        print "----"
        print "Successfully finished Step 6 in BaseManager 1000 Subscription Test"
        print "-> Selected Site and Controller"
        print "-> Verified both Controllers Have Full Access to BaseManager"
        print "----"

    def step_7(self):
        """
        - login as super user & verify successful logged in
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.login_page.enter_login_info(_user_name='SuperAutoTests', _password='SuperTest@10259')
            self.login_page.click_login_button()
            self.main_page.verify_open()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)
        print "----"
        print "Successfully finished Step 7 in BaseManager 1000 Subscription Test"
        print "-> Logged In as Super User"
        print "----"

    def step_8(self):
        """
        - Select administration
        - Select company
        - Select site
        - set subscription to basic on controller
        - Change renewal date to 15 days past due in relationship to the computer date
        - Logout
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        try:
            # select the subscription menu
            self.main_page.select_main_menu()
            self.main_menu.select_administration()
            self.main_menu.select_subscriptions()

            # This should send us to our new tab
            # self.web_driver.web_driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.TAB)
            allTabs = self.web_driver.web_driver.window_handles
            self.main_menu.driver.web_driver.switch_to_window(allTabs[1])

            # select the correct company and open up the edit screen
            self.subscriptions_page.select_main_menu()
            self.subscriptions_page.select_a_company()
            self.subscriptions_page.click_edit_button()

            # change the renewal date for 1000
            d = datetime.today() - timedelta(days=15)
            time_str = d.strftime("%m/%d/%y").replace('/0', '/')
            self.subscriptions_page.set_renewal_date(time_str, mac_address=self.web_driver.conf.mac_address_for_1000)
            self.subscriptions_page.click_save_button()

            # Exits us out of the tab
            self.subscriptions_page.driver.web_driver.close()
            self.subscriptions_page.driver.web_driver.switch_to_window(allTabs[0])

            # logout
            self.main_page.select_main_menu()
            self.main_menu.click_sign_out_button()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)
        print "----"
        print "Successfully finished Step 8 in BaseManager 1000 Subscription Test"
        print "-> Selected Controller"
        print "-> Set Renewal Date for both Controllers to be past due"
        print "----"

    def step_9(self):
        """
        - rerun step 5 (logging in as normal user)
        - select site
        - select 1000 controller
        - verify QuickView tab is not present
        - verify liveview tab is open
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.step_5()

            # Select 1000 controller
            self.main_page.select_main_menu()
            self.main_menu.select_site()
            self.main_menu.select_a_controller(mac_address=self.web_driver.conf.mac_address_for_1000)
            self.main_menu.wait_for_main_menu_close()

            # selecting a controller returns you to the quick view tab
            self.live_view_tab.verify_open()
            
            # logout
            self.main_page.select_main_menu()
            self.main_menu.click_sign_out_button()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)
        print "----"
        print "Successfully finished Step 9 in BaseManager 1000 Subscription Test"
        print "Logged in as Basic User"
        print "-> Selected Site and Controller"
        print "-> Verified only LiveView is Present in 1000"
        print "----"
