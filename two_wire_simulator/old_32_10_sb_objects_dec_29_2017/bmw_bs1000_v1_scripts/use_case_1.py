__author__ = 'baseline'
import sys
import old_32_10_sb_objects_dec_29_2017.common.configuration as test_conf
import old_32_10_sb_objects_dec_29_2017.common.user_configuration as user_conf
import old_32_10_sb_objects_dec_29_2017.page_factory as factory
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
import time

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler


class BaseManagerUseCase1(object):
    """
    Purpose:
        Verify 1000 Programming model on BaseManager for BaseStation 1000's v1.19 firmware version. Main purpose is to
        verify JIRA Bug: BM-1931 (v1.19 - 1000 - Unable to save ET enabled programs on P3)

    Coverage Area: \n
        - Log in to BM
        - Selects 1000 controller
        - Verify quickview displays
        - Select Programs tab
        - Verify programs list displays
        - Adds new program with the following changes:
            - ET Watering enabled
            - Added 1 zone
        - Verifies ability to save ET enabled Program on 1000

    Areas not Covered: \n

    Bugs Addressed: \n
        (1) BM-1931
    """
    # ~~~~ Local Variables ~~~~ #

    def __init__(self, controller_type, test_name, user_configuration_instance, json_configuration_file):
        """
        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = test_conf.Configuration(cn_type=controller_type,
                                              test_name=test_name,
                                              user_conf_file=user_configuration_instance,
                                              data_json_file=json_configuration_file)

        # Get web driver instance from config
        self.web_driver = None

        # Create page instances to use for Client use
        self.main_menu = None

        # Main Tab Page Objects
        self.login_page = None
        self.main_page = None
        """:type: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page.MainPage"""
        self.quick_view_tab = None
        self.programs_tab = None
        """:type: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.pg_tab.ProgramsTab"""

    #################################
    def run_use_case(self):
        """
        The following steps are done on all browser specified in the user configuration: \n

        Step 1:
            - Initialize the controller
                1. Clear all programming
                2. Load all necessary devices from json file
                3. Do search for all devices

        Step 2:
            - initialize & open browser

        step 3:
            - login & verify successful logged in

        Step 4:
            - select site
            - select controller
            - verify QuickView tab opens

        Step 5:
            - Selected Programs Tab
            - Verify Programs Tab opened
        """
        try:
            # browsers_for_test = ["chrome", "firefox"]
            browsers_for_test = ["chrome"]
            # browsers_for_test = ["firefox"]
            for run_number, browser in enumerate(browsers_for_test):
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test()
                self.step_1()
                self.step_2(_browser=browser)
                self.step_3()
                self.step_4()
                self.step_5()
                self.step_6()
                self.step_7()

                # Close the first webdriver to start the second web driver
                if run_number == 0 and len(browsers_for_test) > 1:
                    self.config.resource_handler.web_driver.web_driver.quit()

                else:
                    print "Successfully executed in: '{browser}'".format(browser=browser)
                    time.sleep(5)

        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        else:
            helper_methods.print_test_passed(self.config.test_name)

        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)

    #################################
    def step_1(self):
        """
        Initialize the controller:
        1. Clear programming
        2. Load devices
        3. Do Search for all devices
        """
        helper_methods.print_method_name()
        try:
            # Clear programming
            self.config.controllers[1].init_cn()

            # Verify controller is connected to BaseManger
            self.config.basemanager_connection[1].verify_ip_address_state()

            # Load all devices
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            # Address Zones
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.zones,
                                                               ad_range=self.config.zn_ad_range)

            # Address Master Valves
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.master_valves,
                                                               ad_range=self.config.mv_ad_range)

            # Address Moisture Sensors
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.moisture_sensors,
                                                               ad_range=self.config.ms_ad_range)

            # Address Temperature Sensors
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.temperature_sensors,
                                                               ad_range=self.config.ts_ad_range)

            # Address Event Switches
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.event_switches,
                                                               ad_range=self.config.sw_ad_range)

            # Address Flow Meters
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.flow_meters,
                                                               ad_range=self.config.fm_ad_range)

            # 1000 Specific
            self.config.controllers[1].do_enable_packet_sending_to_bm()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Controller Init: Cleared programming, loaded all necessary devices, searched for all devices"
            )

    #################################
    def step_2(self, _browser):
        """
        Initializes the web driver to the correct browser executor
        """
        helper_methods.print_method_name()
        try:
            # Get web driver instance from config
            self.web_driver = self.config.resource_handler.web_driver

            # Create page instances to use for Client use
            self.main_menu = factory.get_main_menu_object(_webdriver=self.web_driver)

            # Main Tab Page Objects
            self.login_page = factory.get_login_page_object(_webdriver=self.web_driver)
            self.main_page = factory.get_main_page_object(_webdriver=self.web_driver)
            self.quick_view_tab = factory.get_quick_view_page_object(_webdriver=self.web_driver)
            self.programs_tab = factory.get_programs_page_object(_webdriver=self.web_driver)

            # Open browser
            self.config.resource_handler.web_driver.open(_browser)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Opened Web Browser"
            )

    #################################
    def step_3(self):
        """
        """
        helper_methods.print_method_name()
        try:
            # enter the user name and password selects login button
            self.login_page.enter_login_info()
            self.login_page.click_login_button()
            self.main_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Logged In"
            )

    #################################
    def step_4(self):
        """
        - Selects a site and controller
        - Verifies QuickView Tab is opened
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.main_page.select_main_menu()
            self.main_menu.select_site()
            self.main_menu.select_a_controller(mac_address=self.config.mac)
            self.main_menu.wait_for_main_menu_close()

            # selecting a controller returns you to the quick view tab
            self.quick_view_tab.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected Site and Controller"
            )

    #################################
    def step_5(self):
        """
        - Selects Programs Tab.
        - Verify programs tab opens.
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.main_page.select_programs_tab()
            self.programs_tab.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected Programs tab"
            )

    #################################
    def step_6(self):
        """
        - Select add program row.
        - Verify programs add view opens.
        - Select Enable ET Watering.
        - Add Zone
        - Save new ET Program
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.programs_tab.select_add_program()
            self.programs_tab.verify_program_add_or_detail_view_displayed()
            self.programs_tab.select_enable_et_for_1000_program()
            self.programs_tab.select_add_zone()
            self.programs_tab.save_new_program()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected 'Add Program' row and verified Add Program view displayed."
            )

    #################################
    def step_7(self):
        """
        Verify new program added to Program List
            -> This verifies JIRA Bug: BM-19xx () - Unable to save a new ET Program for 1000
        """
        helper_methods.print_method_name()
        try:
            self.programs_tab.verify_newly_added_program(new_pg_number=1, new_pg_description='New Program')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected 'Add Program' row and verified Add Program view displayed."
            )
