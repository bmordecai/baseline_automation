__author__ = 'baseline'

import sys
from old_32_10_sb_objects_dec_29_2017.common.imports import *
# from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
import old_32_10_sb_objects_dec_29_2017.common.configuration as test_conf
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
# from old_32_10_sb_objects_dec_29_2017.common.user_configuration import UserConfiguration

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Browser pages used
# from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page import *
# from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.login_page import LoginPage
# from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page import MainPage
# import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page as bm_main_page
# import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.login_page as bm_login_page
# import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.qv_tab as bm_qv_tab
# import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.maps_tab as bm_maps_tab
# import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.pg_tab as bm_pg_tab
# import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.water_sources_tab_1000 as bm_ws_tab
# import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.live_view_tab as bm_live_tab
import old_32_10_sb_objects_dec_29_2017.page_factory as factory

# Mobile pages used


class BaseManagerUseCase2(object):
    """
    Purpose:
        This test verifies all Base Manager tabs for a 1000 controller. \n
            - verify browser opens:
                - Firefox
                - Chrome
            - verify controller is online
            - verify each tab opens successfully
                - Maps Tab
                - QuickView Tab
                - Programs Tab
                - Devices Tab
                - Water Sources
                - LiveView
    """
    # ~~~~ Local Variables ~~~~ #

    def __init__(self, controller_type, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = test_conf.Configuration(cn_type=controller_type,
                                              test_name=test_name,
                                              user_conf_file=user_configuration_instance,
                                              data_json_file=json_configuration_file)

        # Get web driver instance from config
        self.web_driver = None

        # Create page instances to use for Client use
        self.main_menu = None

        # Main Tab Page Objects
        self.login_page = None
        self.main_page = None
        self.maps_tab = None
        self.quick_view_tab = None
        self.live_view_tab = None
        self.programs_tab = None

        # Device page Objects
        self.zones_tab = None
        self.moisture_sensor_tab = None
        self.master_valves_tab = None
        self.flow_meters_tab = None
        self.temperature_sensors_tab = None
        self.event_switches_tab = None

        # 1000 Specific page objects
        self.water_sources_tab = None

    def run_use_case(self):
        """
        The following steps are done on all browser specified in the user configuration: \n

        Step 1:
            - initialize & open browser
        step 2:
            - login & verify successful logged in
        Step 3:
            - select site
            - select controller
            - verify QuickView tab opens
        Step 4:
            - select Maps
            - verify it opened
        Step 5:
            - Select Quick View tab
            - Verify it opened
        Step 6:
            - Select Programs Tab
            - Verify it opened
        step 7:
            - Verify all device tabs:
                - select Zones & verify it opens
                - select Moisture Sensors & verify it opens
                - select Flow Meters & verify it opens
                - select Temperature Sensors & verify it opens
                - select Master Valves & verify it opens
                - select Event Switch & verify it opens
        step 8:
            - select Water Sources
            - verify it opened
        step 9:
            - select LiveView
            - verify it opens

        Not Covered:
            - Selecting the different map views and verifying each view opens
        """
        try:
            browsers_for_test = ["chrome"]
            # browsers_for_test = ["chrome", "firefox"]
            for run_number, browser in enumerate(browsers_for_test):
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test()
                self.step_1()
                self.step_2(_browser=browser)
                self.step_3()
                self.step_4()
                self.step_5()
                self.step_6()
                self.step_7()
                self.step_8()
                self.step_9()
                self.step_10()

                # Close the first webdriver to start the second web driver
                if run_number == 0 and len(browsers_for_test) > 1:
                    self.config.resource_handler.web_driver.web_driver.quit()

        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        else:
            helper_methods.print_test_passed(self.config.test_name)

        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)
            
    def step_1(self):
        """
        - Initialize 1000 to known state
        - Verify 1000 connected to BM
        - Init all web pages needed
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        # Initialize controller to known state
        self.config.controllers[1].init_cn()
        
        # Verify controller is connected to BaseManger
        self.config.basemanager_connection[1].verify_ip_address_state()

        # Get web driver instance from config
        self.web_driver = self.config.resource_handler.web_driver

        # Create page instances to use for Client use
        self.main_menu = factory.get_main_menu_object(_webdriver=self.web_driver)

        # Main Tab Page Objects
        self.login_page = factory.get_login_page_object(_webdriver=self.web_driver)
        self.main_page = factory.get_main_page_object(_webdriver=self.web_driver)
        self.maps_tab = factory.get_maps_page_object(_webdriver=self.web_driver)
        self.quick_view_tab = factory.get_quick_view_page_object(_webdriver=self.web_driver)
        self.live_view_tab = factory.get_live_view_page_object(_webdriver=self.web_driver)
        self.programs_tab = factory.get_programs_page_object(_webdriver=self.web_driver)

        # Device page Objects
        self.zones_tab = factory.get_zones_page_object(_webdriver=self.web_driver)
        self.moisture_sensor_tab = factory.get_moisture_sensors_page_object(_webdriver=self.web_driver)
        self.master_valves_tab = factory.get_master_valves_page_object(_webdriver=self.web_driver)
        self.flow_meters_tab = factory.get_flow_meter_page_object(_webdriver=self.web_driver)
        self.temperature_sensors_tab = factory.get_temperature_sensors_page_object(_webdriver=self.web_driver)
        self.event_switches_tab = factory.get_event_switch_page_object(_webdriver=self.web_driver)

        # 1000 Specific page objects
        self.water_sources_tab = factory.get_1000_water_sources_tab(_webdriver=self.web_driver)

    def step_2(self, _browser):
        """
        Initializes the web driver to the correct browser executor
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        # Open browser
        self.config.resource_handler.web_driver.open(_browser)
        print "----"
        print "Successfully finished Step 2 in BaseManager Menu Tab Test"
        print "-> Opened Web Browser"
        print "----"

    def step_3(self):
        """
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        # enter the user name and password selects login button
        self.login_page.enter_login_info()
        self.login_page.click_login_button()
        self.main_page.verify_open()
        print "----"
        print "Successfully finished Step 3 in BaseManager Menu Tab Test"
        print "-> Logged In"
        print "----"

    def step_4(self):
        """
        - Selects a site and controller
        - Verifies QuickView Tab is opened
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_main_menu()
        self.main_menu.select_site()
        self.main_menu.select_a_controller(mac_address=self.config.mac)
        self.main_menu.wait_for_main_menu_close()

        # selecting a controller returns you to the quick view tab
        self.quick_view_tab.verify_open()
        print "----"
        print "Successfully finished Step 4 in BaseManager Menu Tab Test"
        print "-> Selected Site and Controller"
        print "----"

    def step_5(self):
        """
        Selects the map tab and verifies it is open.
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_maps_tab(map_view="company")
        self.maps_tab.verify_open()
        print "----"
        print "Successfully finished Step 5 in BaseManager Menu Tab Test"
        print "-> Selected Maps Tab and Verified it opened"
        print "----"

    def step_6(self):
        """
        Selects the Quick View tab and verifies it opens.
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_quick_view_tab()
        self.quick_view_tab.verify_open()
        print "----"
        print "Successfully finished Step 6 in BaseManager Menu Tab Test"
        print "-> Selected QuickView Tab and Verified it opened"
        print "----"

    def step_7(self):
        """
        Selects the program's tab and verifies it opens
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_programs_tab()
        self.programs_tab.verify_open()
        print "----"
        print "Successfully finished Step 7 in BaseManager Menu Tab Test"
        print "-> Selected Programs Tab and Verified it opened"
        print "----"

    def step_8(self):
        """
        - Verify all device tabs:
            - select Zones & verify it opens
            - select Moisture Sensors & verify it opens
            - select Flow Meters & verify it opens
            - select Temperature Sensors & verify it opens
            - select Master Valves & verify it opens
            - select Event Switch & verify it opens
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_devices_tab(device_type="ZN")
        self.zones_tab.verify_is_open()

        self.main_page.select_devices_tab(device_type="MS")
        self.moisture_sensor_tab.verify_is_open()

        self.main_page.select_devices_tab(device_type="FM")
        self.flow_meters_tab.verify_is_open()

        self.main_page.select_devices_tab(device_type="MV")
        self.master_valves_tab.verify_is_open()

        self.main_page.select_devices_tab(device_type="TS")
        self.temperature_sensors_tab.verify_is_open()

        self.main_page.select_devices_tab(device_type="SW")
        self.event_switches_tab.verify_is_open()
        print "----"
        print "Successfully finished Step 8 in BaseManager Menu Tab Test"
        print "-> Selected and verified all Device Tabs and verified it opened"
        print "----"

    def step_9(self):
        """
        Select and verify the Water Sources tab opens
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_water_sources_tab()
        self.water_sources_tab.verify_open()
        print "----"
        print "Successfully finished Step 9 in BaseManager Menu Tab Test"
        print "-> Selected Water Sources Tab and Verified it opened"
        print "----"

    def step_10(self):
        """
        Selects and verifies the LiveView tab opens
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_live_view_tab()
        self.live_view_tab.verify_open()
        print "----"
        print "Successfully finished Step 10 in BaseManager Menu Tab Test"
        print "-> Selected LiveView Tab and Verified it opened"
        print "----"
