import sys
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from datetime import time

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_1000 import POC1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from time import sleep

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

from old_32_10_sb_objects_dec_29_2017.common.bacnetpypes.bacnet_request_properties import BacnetClient
# Browser pages used
import page_factory

__author__ = 'Tige'


class ControllerUseCase2(object):


    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance,
                 json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "10"=1000, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

        self.now = datetime.now()

    def run_use_case(self):
        """
        Step 1:
            - configure controller:
                -  initiate controller to a known state so that it doesnt have a configuration or any devices loaded
                - turn on echo so the commands are displayed in the console
                - turn on sim mode so the clock can be stopped
                - stop the clock
                - Set the date and time so  that the controller is in a known state
                - turn on faux IO
                - clear all devices
                - clear all programming

            - configure basemanager: \n
                - verify the controller is connected to basemanager \n

        step 2:
            - setting up devices:
                - Loading devices into controller
                - Searching for devices so that they can be addressed
                - Address zones and master valves
                - Set all devices:
                    - descriptions
                    - locations
                    - default parameters
        Step 3:
            - setting up programming:
                - set_up_programs
                - assign zones to programs \n
                - set up primary linked zones \n
                - give each zone a run time of 1 hour and 30 minutes \n
                - give each program a start time of 8:00 A.M. \n
        step 4:
            - setup zone programs: \n
                - must make zone 200 a primary zone before you can link zones to it \n
                - assign sensor to primary zone 1
                - assign moisture sensors to a primary zone 1 and set to lower limit watering

        Step 6:

        Step 7:

        step 8:
            - SEt Messates:
                -
        Step 9:
            - Bacnet verify the messagte
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()

                    self.step_6()
                    self.step_7()
                    self.step_8()

                    self.step_bacnetVerify()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - sets up the controller \n
        - verify basemanager connection
        """
        self.config.controllers[1].init_cn()

        # only need this for BaseManager
        # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock for
        # the reboot process.
        # self.config.controllers[1].set_sim_mode_to_off()
        self.config.basemanager_connection[1].verify_ip_address_state()
        # setup controller
        # Stop clock
        # enable faux IO

    def step_2(self):
        # """
        # - sets the devices that will be used in the configuration of the controller \n
        # - search and address the devices:
        #     - zones                 {zn}
        #     - Master Valves         {mv}
        #     - Moisture Sensors      {ms}
        #     - Temperature Sensors   {ts}
        #     - Event Switches        {sw}
        #     - Flow Meter            {fm}
        # - once the devices are found they can be addressed so that they can be used in the programming
        #     - zones can use addresses {1-200}
        #     - Master Valves can use address {1-8}
        # - the 1000 auto address certain devices in the order it receives them:
        #     - Master Valves         {mv}
        #     - Moisture Sensors      {ms}
        #     - Temperature Sensors   {ts}
        #     - Event Switches        {sw}
        #     - Flow Meter            {fm}
        # """
        # # load all devices need into the controller so that they are available for use in the configuration
        self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                     mv_d1_list=self.config.mv_d1,
                                                     d2_list=self.config.d2,
                                                     mv_d2_list=self.config.mv_d2,
                                                     d4_list=self.config.d4,
                                                     dd_list=self.config.dd,
                                                     ms_list=self.config.ms,
                                                     fm_list=self.config.fm,
                                                     ts_list=self.config.ts,
                                                     sw_list=self.config.sw)

        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
        # assign zones an address between 1-200
        self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                         zn_ad_range=self.config.zn_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
        self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                         mv_ad_range=self.config.mv_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                         ms_ad_range=self.config.ms_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                         ts_ad_range=self.config.ts_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
        self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                         sw_ad_range=self.config.sw_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
        self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                         fm_ad_range=self.config.fm_ad_range)
        self.config.create_1000_poc_objects()

    def step_3(self):
        """
        set_up_programs
        assign zones to programs \n
        set up primary linked zones \n
        give each zone a run time of 1 hour and 30 minutes \n
        give each program a start time of 8:00 A.M. \n
        must make zone 200 a primary zone before you can link zones to it \n
        assign sensor to primary zone 1
        #assign moisture sensors to a primary zone 1 and set to lower limit watering
        """
        # this is set in the PG1000 object
        # TODO need to have concurrent zones per program added

        program_8am_start_time = [480]
        mon_wed_fri_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
        every_day_watering_days = [1, 1, 1, 1, 1, 1, 1]  # run every day
        program_number_1_water_windows = ['011111100001111111111110']
        program_full_open_water_windows = ['111111111111111111111111']
        program_number_closed_water_windows = ['000000000000000000000000']

        self.config.programs[1] = PG1000(_address=1,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)

        self.config.programs[2] = PG1000(_address=2,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[3] = PG1000(_address=3,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[4] = PG1000(_address=4,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[5] = PG1000(_address=5,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[6] = PG1000(_address=6,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[7] = PG1000(_address=7,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[8] = PG1000(_address=8,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[9] = PG1000(_address=9,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[10] = PG1000(_address=10,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[11] = PG1000(_address=11,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[12] = PG1000(_address=12,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[13] = PG1000(_address=13,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[14] = PG1000(_address=14,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[15] = PG1000(_address=15,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[16] = PG1000(_address=16,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[17] = PG1000(_address=17,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[18] = PG1000(_address=18,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[19] = PG1000(_address=19,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)

        self.config.programs[20] = PG1000(_address=20,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[21] = PG1000(_address=21,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[22] = PG1000(_address=22,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[23] = PG1000(_address=23,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[24] = PG1000(_address=24,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[25] = PG1000(_address=25,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[26] = PG1000(_address=26,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[27] = PG1000(_address=27,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[28] = PG1000(_address=28,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[29] = PG1000(_address=29,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[30] = PG1000(_address=30,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)

        self.config.programs[31] = PG1000(_address=31,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)

    def step_4(self):
        # Zone Programs
        self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                   prog_obj=self.config.programs[1],
                                                   _rt=900)

        self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                   prog_obj=self.config.programs[2],
                                                   _rt=900)

        self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                   prog_obj=self.config.programs[3],
                                                   _rt=900)

        self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                   prog_obj=self.config.programs[4],
                                                   _rt=900)

        self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                   prog_obj=self.config.programs[5],
                                                   _rt=900)

        self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[6],
                                                   prog_obj=self.config.programs[6],
                                                   _rt=900)

        self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[7],
                                                   prog_obj=self.config.programs[7],
                                                   _rt=900)

        self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[8],
                                                   prog_obj=self.config.programs[8],
                                                   _rt=900)
        # Zone Programs
        self.config.zone_programs[9] = ZoneProgram(zone_obj=self.config.zones[9],
                                                   prog_obj=self.config.programs[9],
                                                   _rt=900)

        self.config.zone_programs[10] = ZoneProgram(zone_obj=self.config.zones[10],
                                                    prog_obj=self.config.programs[10],
                                                    _rt=900)

        self.config.zone_programs[11] = ZoneProgram(zone_obj=self.config.zones[11],
                                                    prog_obj=self.config.programs[11],
                                                    _rt=900)

        self.config.zone_programs[12] = ZoneProgram(zone_obj=self.config.zones[12],
                                                    prog_obj=self.config.programs[12],
                                                    _rt=900)

        self.config.zone_programs[13] = ZoneProgram(zone_obj=self.config.zones[13],
                                                    prog_obj=self.config.programs[13],
                                                    _rt=900)

        self.config.zone_programs[14] = ZoneProgram(zone_obj=self.config.zones[14],
                                                    prog_obj=self.config.programs[14],
                                                    _rt=900)

        self.config.zone_programs[15] = ZoneProgram(zone_obj=self.config.zones[15],
                                                    prog_obj=self.config.programs[15],
                                                    _rt=900)

        self.config.zone_programs[16] = ZoneProgram(zone_obj=self.config.zones[16],
                                                    prog_obj=self.config.programs[16],
                                                    _rt=900)

        self.config.zone_programs[17] = ZoneProgram(zone_obj=self.config.zones[17],
                                                    prog_obj=self.config.programs[17],
                                                    _rt=900)

        self.config.zone_programs[18] = ZoneProgram(zone_obj=self.config.zones[18],
                                                    prog_obj=self.config.programs[18],
                                                    _rt=900)
        # Zone Programs
        self.config.zone_programs[19] = ZoneProgram(zone_obj=self.config.zones[19],
                                                    prog_obj=self.config.programs[19],
                                                    _rt=900)

        self.config.zone_programs[20] = ZoneProgram(zone_obj=self.config.zones[20],
                                                    prog_obj=self.config.programs[19],
                                                    _rt=900)

        self.config.zone_programs[21] = ZoneProgram(zone_obj=self.config.zones[21],
                                                    prog_obj=self.config.programs[19],
                                                    _rt=900)

        self.config.zone_programs[22] = ZoneProgram(zone_obj=self.config.zones[22],
                                                    prog_obj=self.config.programs[19],
                                                    _rt=900)

        self.config.zone_programs[23] = ZoneProgram(zone_obj=self.config.zones[23],
                                                    prog_obj=self.config.programs[19],
                                                    _rt=900)

        self.config.zone_programs[24] = ZoneProgram(zone_obj=self.config.zones[24],
                                                    prog_obj=self.config.programs[19],
                                                    _rt=900)

    def step_6(self):
        """
        set_poc_1000
        set up POC 1 \n
            enable POC 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            assign POC 1 a target flow of 500 \n
            assign POC 1 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 2 \n
            enable POC 2 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 2 \n
            assign POC 2 a target flow of 500 \n
            assign POC 2 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 3 \n
            enable POC 3 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 3 \n
            assign POC 3 a target flow of 500 \n
            assign POC 3 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n

        """
        self.config.poc[1] = POC1000(_address=1,
                                     _flow_meter_address=1,
                                     _master_valve_address=1,
                                     _enabled_state=opcodes.true,
                                     _target_flow=1000.0,
                                     _high_flow_limit=0.0,
                                     _shutdown_on_high_flow=opcodes.false,
                                     _unscheduled_flow_limit=0.0,
                                     _shutdown_on_unscheduled=opcodes.false,
                                     _limit_concurrent_zones_to_target=opcodes.false,
                                     _fill_time=1,
                                     _flow_variance_percent=10,
                                     _flow_variance_enable=opcodes.false,
                                     _flow_fault=opcodes.false
                                     )

        self.config.poc[2] = POC1000(_address=2,
                                     _flow_meter_address=2,
                                     _master_valve_address=2,
                                     _enabled_state=opcodes.true,
                                     _target_flow=1000.0,
                                     _high_flow_limit=0.0,
                                     _shutdown_on_high_flow=opcodes.false,
                                     _unscheduled_flow_limit=0.0,
                                     _shutdown_on_unscheduled=opcodes.false,
                                     _limit_concurrent_zones_to_target=opcodes.false,
                                     _fill_time=1,
                                     _flow_variance_percent=10,
                                     _flow_variance_enable=opcodes.false,
                                     _flow_fault=opcodes.false
                                     )
        self.config.poc[3] = POC1000(_address=3,
                                     _flow_meter_address=0,
                                     _master_valve_address=0,
                                     _enabled_state=opcodes.true,
                                     _target_flow=1000.0,
                                     _high_flow_limit=0.0,
                                     _shutdown_on_high_flow=opcodes.false,
                                     _unscheduled_flow_limit=0.0,
                                     _shutdown_on_unscheduled=opcodes.false,
                                     _limit_concurrent_zones_to_target=opcodes.false,
                                     _fill_time=1,
                                     _flow_variance_percent=10,
                                     _flow_variance_enable=opcodes.false,
                                     _flow_fault=opcodes.false
                                     )

    def step_7(self):
        """
        :return:
        :rtype:
        """
        self.config.controllers[1].do_increment_clock(minutes=1)
        # time.sleep(20)
        self.config.verify_full_configuration()
        # this turns off fast sim mode and start the clock
        self.config.controllers[1].set_sim_mode_to_off()
        self.config.controllers[1].start_clock()
        # this press the run key so that the controller will come out of the paused state
        self.config.controllers[1].set_controller_to_run()

        self.config.controllers[1].turn_off_echo()

        date_mngr.set_current_date_to_match_computer()
        # By setting a time on the controller we can know what time each message has been set
        self.config.controllers[1].set_date_and_time_on_cn(
            _date=date_mngr.curr_computer_date.date_string_for_controller(),
            _time=date_mngr.curr_computer_date.time_string_for_controller())

    def step_8(self):
        """
        Set Bacnet Alarm Messages for 1000
        """

        # Binary 170 3200 Only
        # Binary 270
        self.config.programs[19].set_message_on_cn(opcodes.stop, opcodes.rain_switch)
        self.config.programs[19].verify_message_on_cn(opcodes.stop, opcodes.rain_switch)

        # Binary 370
        self.config.controllers[1].set_message_on_cn('OC')
        self.config.controllers[1].verify_message_on_cn('OC')

        # Binary 470
        self.config.zones[4].set_message_on_cn(opcodes.no_response)
        self.config.zones[4].verify_message_on_cn(opcodes.no_response)

        # No used
        # Binary 570
        # Binayr 670

        # Binary 770
        self.config.zones[8].set_message_on_cn(opcodes.open_circuit)
        self.config.zones[8].verify_message_on_cn(opcodes.open_circuit)

        #870, 970, 1070, ?1170, 1270,1370,1470  are all 3200 only alarms

        # Binary 1570
        self.config.poc[3].set_message_on_cn(opcodes.unscheduled_flow_shutdown)
        self.config.poc[3].verify_message_on_cn(opcodes.unscheduled_flow_shutdown)

        # Binary 1670
        self.config.poc[2].set_message_on_cn(opcodes.high_flow_shutdown)
        self.config.poc[2].verify_message_on_cn(opcodes.high_flow_shutdown)

        # No used
        # Binary 1770

        # Binary 1870
        self.config.zones[7].set_message_on_cn(opcodes.contacts_open)
        self.config.zones[7].verify_message_on_cn(opcodes.contacts_open)

        # not used
        # Binary 1970
        self.config.zones[7].set_message_on_cn(opcodes.exceeds_design_flow_1000)
        self.config.zones[7].verify_message_on_cn(opcodes.exceeds_design_flow_1000)


    #################################
    def step_bacnetVerify(self):
        """
        - sets up the bacnet client \n
        - Get binary input presentvalue and verifies against expected values
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            print "Waiting for 60 seconds so Bacnet objects update"
            time.sleep(60)

            # Create the bacnet client,  opens port and start listening for requests
            self.bacnet_client = BacnetClient()

            # these are the binary input points and the expected value at these points
            expectedValuesDict = {170: 0L, 270: 0L, 370: 1L, 470: 1L, 570: 0L,
                                  670: 0L, 770: 1L, 870: 0L, 970: 0L, 1070: 0L,
                                  1170: 0L, 1270: 0L, 1370: 0L, 1470: 0L, 1570: 1L,
                                  1670: 1L, 1770: 0L, 1870: 1L, 1970: 1L}

            # set the expected values
            self.bacnet_client.setExpectedValueDict(expectedValuesDict)

            # Set the target device
            self.bacnet_client.setTargetDevice(self.config.test_name,
                                               self.config.user_conf.mac_address_for_1000,
                                               self.config.user_conf.bacnet_server_ip)

            print "Looking for Controller ", self.bacnet_client.targetdevicename

            # set the type of test we are running
            self.bacnet_client.setTestToBinaryInput()

            # Start the application running
            self.bacnet_client.runloop()

            # verify the results
            self.bacnet_client.verifyExpectedValues()

            # did we pass yay if not fail
            if self.bacnet_client.results:
                print "BACnet Properties Passed Verification "
            else:
                msg = "BACnet Properties Failed Verification "
                print msg
                exception_message = msg, self.bacnet_client.targetdevicebacnetip, self.bacnet_client.targetdevicebacnetport
                raise AssertionError(exception_message)


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

