import csv
import os
import logging
import sys

from csv_handler import CSVWriter
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes, csv
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *

# this import allows us to keep track of time
from datetime import time, timedelta, datetime, date
from time import sleep

__author__ = 'Tige'


class ControllerUseCase2(object):
    """
    Test Name:
        - EPA Test Configuration
    purpose:
        - set up a the EPA test
            - setup 6 different zones using Weather based watering
            - verify that teh ETc runtime values change when the eto input value changes
            - verify that correct cycle and soak are performed
            - verify that controller starts watering when deficit is below 50%
            - verify that the controller meets a 80% efficacy rating

    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
        - setting programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign zones to programs \n
            - set each zone to run with ETc run times \n
            - set each zone to have a cycle and soak based on zone properties\n
    Date References:
        - configuration for script is located common\configuration_files\update_cn.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance,
                 json_configuration_file):
        """
        Initialize 'UseCase2' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id
        # initializes the csv writer object so it can be used in the remainder of the use_case
        self.csv_writer = CSVWriter(file_name=test_name + controller_type,
                                    relative_path='bs1000_scripts',
                                    delimiter=',',
                                    line_terminator='\n')

        # if the file isn't already in use, the writer is opened. Otherwise an exception is thrown
        try:
            self.csv_writer.open()
        except IOError:
            e_msg = self.csv_writer.fname + ' file is in use'
            raise IOError(e_msg)

        # self.hydrozone_information = csv.EPA.hydro_zone_headers
        self.logger = logging.getLogger()
        self.hdlr = logging.FileHandler(str(os.path.dirname(os.path.abspath(__file__))) + '/' + test_name +
                                        controller_type + ".txt", mode="w")
        self.formatter = logging.Formatter('%(levelname)s: %(message)s')
        self.hdlr.setFormatter(self.formatter)
        self.logger.addHandler(self.hdlr)
        self.logger.setLevel(logging.INFO)

    def run_use_case(self):
        ###############################################################################################################
        # TODO: EXAMPLE USAGE WITH NEW WEATHER_BASE_WATERING 'PACKAGE'
        #
        # TODO: 1. USE THIS IMPORT AT THE TOP OF THE SCRIPT
        #   from old_32_10_sb_objects_dec_29_2017.common.epa_package import wbw_imports, ir_asa_resources
        #
        # TODO: 2. GETTING A SOIL TYPE
        #   clay_soil_type = wbw_imports.SoilTextureTypes.CLAY
        #
        # TODO: 3. ASA VALUE FOR CLAY WITH A 5% SLOPE:
        #   asa_value = ir_asa_resources.dict_of_allowable_surface_accumulation[clay_soil_type][0.05]
        # or
        #   asa_value = ir_asa_resources.dict_of_allowable_surface_accumulation[wbw_imports.SoilTextureTypes.CLAY][0.05]
        ###############################################################################################################
        """
        Step 1:
            - configure controller:
                - initiate controller to a known state so that it doesnt have a configuration or any devices loaded
                - turn on echo so the commands are displayed in the console
                - turn on sim mode so the clock can be stopped
                - stop the clock
                - Set the date and time so  that the controller is in a known state
                - turn on faux IO
                - clear all devices
                - clear all programming

        step 2:
            - setting up devices:
                - Loading devices into controller
                - Searching for devices so that they can be addressed
                - Address zones and master valves
                - Set all devices:
                    - descriptions
                    - default parameters
        Step 3:
            - setting up programming:
                - set_up_programs
                - assign zones to programs \n
                - give each zone a run time of 1 hour and 30 minutes \n
                - give each program a start time of 8:00 A.M. \n
        step 4:
            - eto setup
        Step 5:
            - reboot the controller:
                - verify that all attributes for each device did not change
                - because the clock was increment we can verify status and verify that the zone didn't start \n
                  watering after the reboot \n
         """

        # TODO: Need to handle opening and closing of file correctly.
        try:
            number_of_retries = 1
            retries = 0

            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    # TODO need to add close serial port
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            self.csv_writer.close()
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - sets up the controller \n
        - verify basemanager connection
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        self.config.controllers[1].init_cn()
        self.logger.info("Successfully initiated controller")

        # only need this for BaseManager
        # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock for
        # the reboot process.
        # self.config.controllers[1].set_sim_mode_to_off()
        # self.config.basemanager_connection[1].verify_ip_address_state()
        # setup controller
        # Stop clock
        # enable faux IO
        self.logger.info("Step 1 complete!")

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        # load all devices need into the controller so that they are available for use in the configuration
        self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                     mv_d1_list=self.config.mv_d1,
                                                     d2_list=self.config.d2,
                                                     mv_d2_list=self.config.mv_d2,
                                                     d4_list=self.config.d4,
                                                     dd_list=self.config.dd,
                                                     ms_list=self.config.ms,
                                                     fm_list=self.config.fm,
                                                     ts_list=self.config.ts,
                                                     sw_list=self.config.sw)

        self.logger.info("[SUCCESS] Loaded all available devices to controller.")
        self.config.load_eto_and_rain_fall_values()

        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
        # assign zones an address between 1-200
        self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                         zn_ad_range=self.config.zn_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
        self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                         mv_ad_range=self.config.mv_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                         ms_ad_range=self.config.ms_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                         ts_ad_range=self.config.ts_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
        self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                         sw_ad_range=self.config.sw_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
        self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                         fm_ad_range=self.config.fm_ad_range)

        self.config.create_1000_poc_objects()

        self.logger.info("Default values set on controller")
        self.logger.info(sys._getframe().f_code.co_name + " complete!")

    def step_3(self):
        """
        - Set all attribute for a zone the a client could specify
        - Set the new descriptions to match EPA test for each zone
            - set the zone attribute for soil type for each zone
            - set the zone attribute for slope for each zone
            - set the zone attribute for sun exposure for each zone
            - set the zone attribute for allowable depletion for each zone
            - set the zone attribute for root depth for each zone
            - set the zone precipitation rates for each zone for each zone
            - set the zone distribution uniformity for each zone for each zone
            - set the zone attribute for Gross are a for each zone
        - set crop coefficient for each zone
            - Set zone 1 is attribute for sun exposure shade tall Fescue
            - Set zone 2 is attribute for sun exposure Bermuda
            - Set zone 6 is attribute for sun exposure Bermuda
        - set the root water holding capacity for each zone
            - this uses an equation to get the root zone water holding capacity and then sends the value \n
              to the controller \n
        """
        #######################################
        # user input fields simulating coming from the client
        #######################################
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        # set descriptions to match EPA test for each zone
        self.config.zones[1].set_description_on_cn(_ds='Loam Soil')
        self.config.zones[2].set_description_on_cn(_ds='Silty Clay Soil')
        self.config.zones[3].set_description_on_cn(_ds='Loamy Sand Soil')
        self.config.zones[4].set_description_on_cn(_ds='Sandy Loam Soil')
        self.config.zones[5].set_description_on_cn(_ds='Clay Loam soil')
        self.config.zones[6].set_description_on_cn(_ds='Clay soil')

        # set the zone attribute for soil type
        self.config.zones[1].soil_type = "Loam"
        self.config.zones[2].soil_type = "Silty Clay"
        self.config.zones[3].soil_type = "Loamy Sand"
        self.config.zones[4].soil_type = "Sandy Loam"
        self.config.zones[5].soil_type = "Clay Loam"
        self.config.zones[6].soil_type = "Clay"

        # set the zone attribute for slope
        self.config.zones[1].slope = .06
        self.config.zones[2].slope = .10
        self.config.zones[3].slope = .08
        self.config.zones[4].slope = .12
        self.config.zones[5].slope = .02
        self.config.zones[6].slope = .20

        # set the zone attribute for sun exposure
        self.config.zones[1].sun_exposure = .75
        self.config.zones[2].sun_exposure = 1.00
        self.config.zones[3].sun_exposure = 1.00
        self.config.zones[4].sun_exposure = .50
        self.config.zones[5].sun_exposure = 1.00
        self.config.zones[6].sun_exposure = 1.00

        # set the zone attribute for allowable depletion
        self.config.zones[1].allowable_depletion = .50
        self.config.zones[2].allowable_depletion = .40
        self.config.zones[3].allowable_depletion = .50
        self.config.zones[4].allowable_depletion = .55
        self.config.zones[5].allowable_depletion = .50
        self.config.zones[6].allowable_depletion = .35

        # set the zone attribute for root depth
        self.config.zones[1].root_depth = 10.0
        self.config.zones[2].root_depth = 8.1
        self.config.zones[3].root_depth = 20.0
        self.config.zones[4].root_depth = 28.0
        self.config.zones[5].root_depth = 25.0
        self.config.zones[6].root_depth = 9.2

        # set zone precipitation rates for each zone
        self.config.zones[1].set_precipitation_rate_value_on_cn(_pr_value=1.60)
        self.config.zones[2].set_precipitation_rate_value_on_cn(_pr_value=1.60)
        self.config.zones[3].set_precipitation_rate_value_on_cn(_pr_value=1.40)
        self.config.zones[4].set_precipitation_rate_value_on_cn(_pr_value=1.40)
        self.config.zones[5].set_precipitation_rate_value_on_cn(_pr_value=0.20)
        self.config.zones[6].set_precipitation_rate_value_on_cn(_pr_value=0.35)

        # set the zone attribute for distribution uniformity
        self.config.zones[1].set_distribution_uniformity_value_on_cn(_du_value=55)
        self.config.zones[2].set_distribution_uniformity_value_on_cn(_du_value=60)
        self.config.zones[3].set_distribution_uniformity_value_on_cn(_du_value=70)
        self.config.zones[4].set_distribution_uniformity_value_on_cn(_du_value=75)
        self.config.zones[5].set_distribution_uniformity_value_on_cn(_du_value=80)
        self.config.zones[6].set_distribution_uniformity_value_on_cn(_du_value=65)

        # set the zone attribute for Gross area
        self.config.zones[1].gross_area = 1000
        self.config.zones[2].gross_area = 1200
        self.config.zones[3].gross_area = 800
        self.config.zones[4].gross_area = 500
        self.config.zones[5].gross_area = 650
        self.config.zones[6].gross_area = 1600

        # this has to come from some table associated back to a date
        self.config.zones[1].set_crop_coefficient_value_on_cn(_kc_value=0.50)

        # Set zone 2 is full sun Bermuda
        self.config.zones[2].set_crop_coefficient_value_on_cn(_kc_value=0.60)
        self.config.zones[3].set_crop_coefficient_value_on_cn(_ks_value=0.50, _kd_value=1.00, _kmc_value=1.10)
        self.config.zones[4].set_crop_coefficient_value_on_cn(_ks_value=0.50, _kd_value=1.00, _kmc_value=.80)
        self.config.zones[5].set_crop_coefficient_value_on_cn(_ks_value=0.50, _kd_value=1.10, _kmc_value=1.10)
        # Set zone 6 is full sun Bermuda
        self.config.zones[6].set_crop_coefficient_value_on_cn(_kc_value=0.73)

        # set root zone working water holding storage this uses an equation to get the root zone water holding capacity
        # and then sends the value to the controller
        for zone in self.config.zn_ad_range:
            self.config.zones[zone].set_root_zone_working_storage_capacity_on_cn()

        self.logger.info("Set all of the zone's attributes.")
        self.logger.info(sys._getframe().f_code.co_name + " complete!")

    def step_4(self):
        """
        setup Program
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        program_number_1_start_times = 120  # 2:00 am start time
        program_number_1_watering_days = 1111111  # runs everyday
        program_number_1_water_windows = ['111111111111111111111111']

        self.config.programs[1] = PG1000(_address=1,
                                         _description='EPA Test Program',
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _point_of_connection_address=[1],
                                         _soak_cycle_mode=opcodes.zone_soak_cycles,
                                         _ee=opcodes.true)
        self.config.controllers[1].ser.send_and_wait_for_reply('SET,'
                                                               'PT=1,'
                                                               'TY=DT,'
                                                               'DT=TR,'
                                                               'WD='+str(program_number_1_watering_days)+',' +
                                                               'ST='+str(program_number_1_start_times)
                                                               )

        self.logger.info("Program has been setup.")
        self.logger.info(sys._getframe().f_code.co_name + " complete!")

    def step_5(self):
        """
        - Set a cycle time for each zone set the new cycle time values
            - this uses an equation to get the cycle time and then sends the value \n
              to the controller \n
        - Set a soak time for each zone set the new cycle time values
            - this uses an equation to get the soak time and then sends the value \n
              to the controller \n
        :return:
        :rtype:
        """
        # Set the new cycle time values
        # Need this to come for the answer to the wrapper

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        for zone in self.config.zn_ad_range:
            self.config.zones[zone].set_cycle_time_on_cn(_use_calculated_cycle_time=True)

        # Set the new soak time values
        # Need this to come for the answer to the wrapper
        for zone in self.config.zn_ad_range:
            self.config.zones[zone].set_soak_time_on_cn(_use_calculated_soak_time=True)

        self.csv_writer.write_dict_of_obj(dict_of_objs=self.config.zones,
                                          template=csv.EPA.hydro_zone_headers_zn_var_names)

        self.csv_writer.write_dict_of_obj(dict_of_objs=self.config.zone_programs,
                                          template=csv.EPA.hydro_zone_headers_zp_var_names)

        self.logger.info(sys._getframe().f_code.co_name + " complete!")

    def step_6(self):
        """
        enable the ET runtime for each zone by assigning it to a program and then enabling it
        :return:
        :rtype:
        """
        # enable each zone to run  on ET run times
        # set _rt = 1 so that there was just a value
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        for zone in self.config.zn_ad_range:
            self.config.zone_programs[zone] = ZoneProgram(zone_obj=self.config.zones[zone],
                                                          prog_obj=self.config.programs[1], _rt=60)

        self.logger.info(sys._getframe().f_code.co_name + " complete!")

    def step_7(self):
        """
         Verify all attribute values for all objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        self.config.verify_full_configuration()

        self.logger.info("Successfully verified the values for all objects in the configuration.")
        self.logger.info(sys._getframe().f_code.co_name + " complete!")
    def step_8(self):

        """
        input 30 days of eto
        input 30 days of rain fall

        Run controller for x number of days adjusting the Eto and rain fall value for each day


        Build a list of dates by passing in a date range
        build a list of ETO and rain fall data points by accessing the list of eto and rain fall values
        using date manager select the first day
        for each date assign an eto and rain fall value
        start by setting controller time to 11:55pm of the first date of the date range
        increment clock to 12:03  this will allow the controller to do all necessary operations
        Set clock to 12:45 am
        Send Eto and Rain fail data to the controller
        increment clock to 1:01 am this will allow controller to make all necessary calculations
        Compare the calculated moisture balance from the test to the moisture balance in the controller
        Compare the calculated Run time from the test to the Run time in the controller
        Compare the calculated Soak and Cycle Time balance from the test to the Soak and Cycle Time in the controller
        run program until all zones are done
        Compare minute my minute what is happening

        :return:
        :rtype:
        """
        # while date_mngr.reached_last_date() == False:
        #   1. Start by setting controller time to 11:55pm of the first date of the date range
        #   2. While there are days left to complete: (for each date)
        #   3. .... Assign an eto and rain fall value
        #   4. .... Increment clock to 12:03am (allow controller to do calculations)
        #   5. .... Set clock to 12:45am
        #   6. .... Send Eto and Rain fail data to the controller
        #   7. .... Increment clock to 1:01 am this will allow controller to make all necessary calculations
        #   8. .... Compare the calculated moisture balance from the test to the moisture balance in the controller
        #   9. .... Compare the calculated Run time from the test to the Run time in the controller
        #   10..... Compare the calculated Soak and Cycle Time balance from the test to the Soak and Cycle Time in
        #           the controller
        #   11..... Run program until all zones are done
        #   12..... Compare minute my minute what is happening

        # START
        # This will be at the beginning of the script before step one
        # when it wants to run seven minutes it drops the last minute

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        date_mngr.set_dates_from_date_range(start_date='12-01-2015', end_date='12-28-2015')
        self.csv_writer.writerow(["Date range of test: ", "", str(date_mngr.begin_date) + " to " + str(date_mngr.end_date)])
        # With current eto/rainfall list:
        print "Day Before Test Starts", date_mngr.prev_day.date_string_for_controller()
        print "First Day of Test", date_mngr.curr_day.date_string_for_controller()

        self.logger.info("Day Before Test Starts: " + date_mngr.prev_day.date_string_for_controller())
        self.logger.info("First Day of Test: " + date_mngr.curr_day.date_string_for_controller())

        # set date and time on the controller to be 2 minutes before midnight than increment the clock past
        # midnight so the controller can make of of its decisions for the new day

        self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.prev_day.date_string_for_controller(),
                                                           _time='23:59:00')
        # increment clock past midnight this will allow the controller to run all of its own calculations on things like
        # day intervals
        self.config.controllers[1].do_increment_clock(minutes=2)
        self.config.controllers[1].verify_date_and_time()

        # First time through test set default values on et to zero
        first_time = True

        # -------------------------------------------------------------------------------------------------------------

        # Start of MAIN LOOP: This loop runs through each day of the test
        while not date_mngr.reached_last_date():

            # set controller to current date or first day of test
            # set the controller to 12:45am
            # the controller takes in all of its eto data at 12:45 and than does all of its eto calculations for the day
            self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='00:45:00')
            index = date_mngr.get_current_day_index()
            eto = self.config.daily_eto_value[index]
            rain_fall = self.config.daily_rainfall_value[index]

            # if first day, set initial eto value on all zones to eto (zero for first time through)
            if first_time:
                self.config.controllers[1].set_initial_eto_value_cn(_initial_et_value=eto)
                first_time = False

            # else, set controller et and rain value for current day
            else:
                    self.config.controllers[1].set_eto_and_date_stamp_on_cn(_controller_et_value=eto,
                                                                            _controller_rain_value=rain_fall,
                                                                            _controller_date=date_mngr.curr_day.
                                                                            formatted_date_string('%Y%m%d'))

            self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='00:59:00')
            self.config.controllers[1].do_increment_clock(minutes=2)
            self.config.controllers[1].verify_date_and_time()

            # outputs the date to the log file
            self.logger.info(date_mngr.curr_day.date_string_for_controller())

            # outputting the date to a csv file
            self.csv_writer.writerow("\n")
            self.csv_writer.writerow(["Date: " + str(date_mngr.curr_day.date_string_for_controller())])
            self.csv_writer.writerow(["ETO Value: " + str(eto)])
            self.csv_writer.writerow(["Rain Fall Value: " + str(rain_fall)])

            # ----------------------------------------------------------------------------------------------------------
            # for each zone:
            #   1. get current data from controller
            #   2. verify zone mb
            #   3. get current data for zone program from controller
            #   4. verify zone program runtime
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_moisture_balance_on_cn()

                self.config.zone_programs[zone].get_data()
                self.config.zone_programs[zone].verify_runtime_on_cn()
                # output to log file
                logging.info(opcodes.zone + ": " + str(zone) +
                             ", Calculated MB: " + str(self.config.zones[zone].mb) +
                             ", Controller MB: " + self.config.zones[zone].data.get_value_string_by_key(
                              opcodes.daily_moisture_balance) +
                             ", Calculated RT: " + str(self.config.zone_programs[zone].rt) +
                             ", Controller RT: " + self.config.zone_programs[zone].data.get_value_string_by_key(
                              opcodes.runtime_total))

                # takes in each value of the key_value_list from each zone program and stores it in a list to be output

            # ----- END FOR --------------------------------------------------------------------------------------------

            self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='01:59:00')
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()

            self.csv_writer.write_dict_of_obj(self.config.zones)
            self.csv_writer.write_dict_of_obj(self.config.zone_programs, template=csv.EPA.hydro_zone_headers_zp_var_names)
            self.csv_writer.writerow(["\n"])

            clock = time(hour=2, minute=0, second=15)

            # this should go minute by minute until all zones are done

            #  need to clear out values for accumulating the new day
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].seconds_zone_ran = 0
                self.config.zones[zone].seconds_zone_soaked = 0
                self.config.zones[zone].seconds_zone_waited = 0
            # ----- END FOR ----------------------------------------------------------------------------------------

            not_done = True

            while not_done:

                zones_still_running = False
                log_string = "Time: " + str(clock.isoformat())

                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone in self.config.zn_ad_range:
                    self.config.zones[zone].get_data()
                    _zone_status = self.config.zones[zone].data.get_value_string_by_key(opcodes.status_code)
                    if _zone_status == opcodes.watering:
                        self.config.zones[zone].seconds_zone_ran += 60  # using 60 so that we are in seconds
                    elif _zone_status == opcodes.soak_cycle:
                        self.config.zones[zone].seconds_zone_soaked += 60
                    elif _zone_status == opcodes.waiting_to_water:
                        self.config.zones[zone].seconds_zone_waited += 60
                    # set flag not all zones are done
                    if _zone_status != opcodes.done_watering:
                        zones_still_running = True

                    # Prepare a string for logging
                    log_string += ", " + opcodes.status_code + "=" + str(_zone_status)
                    # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------

                self.logger.info(log_string)
                log_string = ""
                # this could say if zones_still_running:
                if zones_still_running:
                    not_done = True
                    self.config.controllers[1].do_increment_clock(minutes=1)
                    self.config.controllers[1].verify_date_and_time()
                    try:
                        dummy_date = date(1, 1, 1)
                        full_datetime = datetime.combine(dummy_date, clock)
                        added_datetime = full_datetime + timedelta(minutes=1)
                        clock = added_datetime.time()
                    except Exception, ae:
                        raise Exception("catch clock error: " + ae.message)
                else:
                    not_done = False

            # ----- END WHILE ------------------------------------------------------------------------------------------
    # TODO need to log the values for each runtime soaktime and cycle time below
            # increment controller clock to the next day 2 minutes before midnight than increment the clock past
            # midnight so the controller can make of of its decisions for the new day
            self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='23:59:00')
            self.config.controllers[1].do_increment_clock(minutes=2)
            self.config.controllers[1].verify_date_and_time()

            # move date manager to the next day to remain in sync with controller's current date
            date_mngr.skip_to_next_day()

            output = [["Seconds ran:"], ["Expected Seconds ran:"], ["Seconds soaked:"], ["Seconds Waited:"],["e_msg"]]
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].yesterdays_mb = self.config.zones[zone].mb
                # runtime is the expected time calculated for the controller to run
                runtime = 60*(int(self.config.zone_programs[zone].rt/60))
                if runtime <= 180:
                    runtime = 0
                if 239 >= runtime > 180:
                    runtime = 240
                # Outputs the seconds ran, soaked, and cycled to the CSV
                # TODO tune all of these into the log file
                # log_string += ", " + opcodes.runtime + "=" + str(.rt)
                #log_string += (str(self.config.zones[zone].seconds_zone_ran))
                # output[1].append(str(runtime))
                # output[2].append(str(self.config.zones[zone].seconds_zone_soaked))
                # output[3].append(str(self.config.zones[zone].seconds_zone_waited))

                if self.config.zones[zone].seconds_zone_ran == runtime:
                    e_msg = "Zone '{0}' ran for '{1}' seconds which match expected runtime of '{2}' seconds, " \
                            "it soaked for '{3}' seconds, and it waited for '{4}' seconds"\
                        .format(
                            str(zone),                                          # {0}
                            str(self.config.zones[zone].seconds_zone_ran),      # {1}
                            str(runtime),                                       # {2}
                            str(self.config.zones[zone].seconds_zone_soaked),   # {3}
                            str(self.config.zones[zone].seconds_zone_waited)    # {4}}

                        )
                    print(e_msg)
                    self.logger.info(e_msg)
                elif self.config.zones[zone].seconds_zone_ran > runtime and \
                        abs(self.config.zones[zone].seconds_zone_ran - runtime) <= 120:
                    e_msg = "Zone '{0}' ran for a longer period of time than was expected: Zone '{0}' " \
                            "It was expected to run for '{1}' but it ran for " \
                            "'{2}' seconds, it soaked for '{3}' seconds, and it waited for '{4}' seconds"\
                        .format(
                            str(zone),                                          # {0}
                            str(runtime),                                       # {1}
                            str(self.config.zones[zone].seconds_zone_ran),      # {2}
                            str(self.config.zones[zone].seconds_zone_soaked),   # {3}
                            str(self.config.zones[zone].seconds_zone_waited)    # {4}
                        )
                    print(e_msg)
                    self.logger.info(e_msg)
                elif self.config.zones[zone].seconds_zone_ran < runtime and \
                        abs(self.config.zones[zone].seconds_zone_ran - runtime) <= 120:
                    e_msg = "Zone '{0}' ran for a shorter period of time than was expected: Zone '{0}'" \
                            " It was expected to run for '{1}' but it " \
                            "ran for '{2}' seconds, it soaked for '{3}' seconds, and it waited for '{4}' seconds"\
                        .format(
                            str(zone),                                          # {0}
                            str(runtime),                                       # {1}
                            str(self.config.zones[zone].seconds_zone_ran),      # {2}
                            str(self.config.zones[zone].seconds_zone_soaked),   # {3}
                            str(self.config.zones[zone].seconds_zone_waited)    # {4}
                        )
                    print(e_msg)
                    self.logger.info(e_msg)
                else:
                    e_msg = "Zone '{0}' ran for '{1}' seconds and was expected run for '{2}' seconds, " \
                            "it soaked for '{3}' seconds, and it waited for '{4}' seconds" \
                        .format(
                            str(zone),                                          # {0}
                            str(self.config.zones[zone].seconds_zone_ran),      # {1}
                            str(runtime),                                       # {2}
                            str(self.config.zones[zone].seconds_zone_soaked),   # {3}
                            str(self.config.zones[zone].seconds_zone_waited)    # {4}
                        )
                    self.logger.info("Value Error: " + e_msg)
                    raise ValueError(e_msg)
                self.config.zones[zone].yesterdays_rt = self.config.zones[zone].seconds_zone_ran

            self.csv_writer.writerows(output)