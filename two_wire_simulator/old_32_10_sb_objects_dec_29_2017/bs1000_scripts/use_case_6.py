import sys

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_1000 import StartConditionFor1000, StopConditionFor1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

# this import allows us to keep track of time
from datetime import time, timedelta, datetime, date
from time import sleep

__author__ = 'Tige'


class ControllerUseCase6(object):
    """
    Test Name:
        - Nelson Feature Master Vales With Zones
    purpose:
        - set up the Nelson feature
            - verify that individual master valve can be assigned to zones

    Coverage area: \n
        - master valves \n
            - Individual master valve can be assigned to individual zones \n
            - master valves turn on with zones
            - master valves follow zone concurrency

    Things that are not covered: \n
        - verifying master valves on programs \n
        - verifying master valves turn on with manual runs \n

    Date References:
        - configuration for script is located common\configuration_files\nelson_features.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance,
                 json_configuration_file):
        """
        Initialize 'UseCase2' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        """

        # TODO: Need to handle opening and closing of file correctly.
        try:
            number_of_retries = 1
            retries = 0

            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    # TODO need to add close serial port
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - sets up the controller \n
        - set max concurrent zones of the controller to 50
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        self.config.controllers[1].init_cn()

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - master valves         {mv}
            - event switch          {sw}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-100}
            - master valves can use address {1-100}
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        # load all devices need into the controller so that they are available for use in the configuration
        try:
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(
                ms_object_dict=self.config.moisture_sensors,
                ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(
                ts_object_dict=self.config.temperature_sensors,
                ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)

            self.config.create_1000_poc_objects()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_3(self):
        """
        - Set up program \n
            - create program object \n
                - program 1\n
                    - create start stop pause object
                        - start time 8:00am \n
                        - full open water window \n
                        - Water every day \n
                        - add zone 1-10 to program
                            -  give each zone a 10 minute runtime
                        - assign each zone a master valve

        """
        program_8_am_start_times = [480]  # 8:00 am start time
        program_waters_every_day = [1, 1, 1, 1, 1, 1, 1]  # runs everyday
        program_fully_open_water_windows = ['111111111111111111111111']

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # create program 1
            self.config.programs[1] = PG1000(_address=1,
                                             _description='master valves',
                                             _enabled_state=opcodes.true,
                                             _water_window=program_fully_open_water_windows,
                                             _point_of_connection_address=[1],
                                             _soak_cycle_mode=opcodes.disabled
                                             )
            self.config.program_start_conditions[1] = StartConditionFor1000(program_ad=1)
            self.config.program_start_conditions[1].set_day_time_start(_dt=opcodes.true,
                                                                       _st_list=program_8_am_start_times,
                                                                       _interval_type=opcodes.week_days,
                                                                       _interval_args=program_waters_every_day)
            for zone in range(1, 7):
                self.config.zone_programs[zone] = ZoneProgram(zone_obj=self.config.zones[zone],
                                                              prog_obj=self.config.programs[1], _rt=600)

            for zone in range(1, 7):
                self.config.zones[zone].set_master_valve_pointer_on_cn(_mv_number=zone)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_4(self):
        """
         Verify all attribute values for all objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_5(self):
        """
        Testing zones assign to master valves
        - set controller to allow 6 zones to run concurrent
        - set program 1 to allow 6 zones to run concurrent
        - set clock on controller to 7:59am
        - increment clock to 8:00
        - program 1 should start
        - because program 1 can run 6 zones and the controller can run 6 all zones should run
        - verify that zones 1 - 6 are watering
        - verify that all 6 master valves run
        - because the runtime is set to 11 minutes increment clock 11 minutes
        - verify all zones and master valves are set to done
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=6)
            self.config.programs[1].set_max_concurrent_zones_on_cn(_number_of_zones=6)

            date_mngr.set_current_date_to_match_computer()
            self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='07:59:01')

            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            for master_valve in self.config.mv_ad_range:
                self.config.master_valves[master_valve].get_data()

            for zone in range(1, 7):
                self.config.zones[zone].verify_status_on_cn(status=opcodes.watering)
            for master_valves in range(1, 7):
                self.config.zones[master_valves].verify_status_on_cn(status=opcodes.watering)

            self.config.controllers[1].do_increment_clock(minutes=11)

            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            for master_valve in self.config.mv_ad_range:
                self.config.master_valves[master_valve].get_data()

            for zone in range(1, 7):
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
            for master_valves in range(1, 7):
                self.config.zones[master_valves].verify_status_on_cn(status=opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)


