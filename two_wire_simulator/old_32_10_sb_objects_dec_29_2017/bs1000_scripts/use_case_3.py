
# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from datetime import time

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_1000 import POC1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from time import sleep

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

__author__ = 'Tige'


class ControllerUseCase3():
    """
    Test Name:
        - Set Messages
    purpose:
        - set up a full configuration on the controller
            - verify that no programming is lost when you:
                - reboot the controller
                - update the firmware of the controller
                - replace a device
            - verify that after a reboot the controller restarts
                - program return to current watering state
                - zones return to current watering state
                - device return to current conditions (event paused stays paused)
            - verify that firmware update take effects:
                - even with certain devices or attributes are disabled

    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n

        - setting programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign programs to water sources \n
            - assign zones to programs \n
            - set up primary linked zones \n
            - give each zone a run time of 1 hour and 30 minutes \n
            - assign moisture sensors to a primary zone 1 and set to lower limit watering

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow\n
                - high variance limit
                - high variance shut down \n
                - low variance limit
                - low variance shut down \n

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n
                - priority
                    - low
                    - medium
                    - high\n
            - set high flow limit
            - high flow shut down \n
            - set unscheduled flow limit
            - enable unscheduled flow shut down \n
            - set water budget
            - enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions
                - event switch\n
                - set switch empty condition to closed \n
                - set empty wait time\n

        - Reboot the controller:
            - verify all setting \n
                - zone decoder
                    - verify all settings were not lost \n
                - moisture sensor
                    - verify all settings were not lost \n
                - flow decoder
                    - verify all settings were not lost \n
                - event decoder
                    - verify all settings were not lost  \n
                - temperature decoder
                    - verify all settings were not lost \n
        - Replace devices: \n
            - zone decoder
                - verify all settings were not lost \n
            - moisture sensor
                - verify the primary zone setting were not lost \n
            - flow decoder
                - verify all setting were deleted\n
            - event decoder
                - verify all setting were deleted \n
            - temperature decoder
                - verify all setting were delete\n
        - firmware update: \n
            - disable
                - all one of each device type \n
                - program
                - mainline
                - poc
            - verify configuration all stayed \n
    Date References:
        - configuration for script is located common\configuration_files\EPA_test_configuration.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance,
                 json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "10"=1000, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

        self.now = datetime.now()

    def run_use_case(self):
        """
        Step 1:
            - configure controller:
                -  initiate controller to a known state so that it doesnt have a configuration or any devices loaded
                - turn on echo so the commands are displayed in the console
                - turn on sim mode so the clock can be stopped
                - stop the clock
                - Set the date and time so  that the controller is in a known state
                - turn on faux IO
                - clear all devices
                - clear all programming

            - configure basemanager: \n
                - verify the controller is connected to basemanager \n

        step 2:
            - setting up devices:
                - Loading devices into controller
                - Searching for devices so that they can be addressed
                - Address zones and master valves
                - Set all devices:
                    - descriptions
                    - locations
                    - default parameters
        Step 3:
            - setting up programming:
                - set_up_programs
                - assign zones to programs \n
                - set up primary linked zones \n
                - give each zone a run time of 1 hour and 30 minutes \n
                - give each program a start time of 8:00 A.M. \n
        step 4:
            - setup zone programs: \n
                - must make zone 200 a primary zone before you can link zones to it \n
                - assign sensor to primary zone 1
                - assign moisture sensors to a primary zone 1 and set to lower limit watering
        Step 5:
            - setting up mainlines: \n
                - set up main line 1 \n
                    set limit zones by flow to true \n
                    set the pipe fill time to 4 minutes \n
                    set the target flow to 500 \n
                    set the high variance limit to 5% and enable the high variance shut down \n
                    set the low variance limit to 20% and enable the low variance shut down \n
                \n
                - set up main line 8 \n
                    set limit zones by flow to true \n
                    set the pipe fill time to 1 minute \n
                    set the target flow to 50 \n
                    set the high variance limit to 20% and disable the high variance shut down \n
                    set the low variance limit to 5% and disable the low variance shut down \n
        Step 6:
            - setting up POCs: \n
                - set up POC 1 \n
                    - enable POC 1 \n
                    - assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
                    - assign POC 1 a target flow of 500 \n
                    - assign POC 1 to main line 1 \n
                    - set POC priority to 2-medium \n
                    - set high flow limit to 550 and enable high flow shut down \n
                    - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                    - set water budget to 100000 and enable the water budget shut down \n
                    - enable water rationing \n
                \n
                - set up POC 8 \n
                    - enable POC 8 \n
                    - assign master valve TMV0004 and flow meter TWF0004 to POC 8 \n
                    - assign POC 8 a target flow of 50 \n
                    - assign POC 8 to main line 8 \n
                    - set POC priority to 3-low \n
                    - set high flow limit to 75 and disable high flow shut down \n
                    - set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
                    - set water budget to 1000 and disable water budget shut down \n
                    - disable water rationing \n
                    - assign event switch TPD0001 to POC 8 \n
                    - set switch empty condition to closed \n
                    - set empty wait time to 540 minutes \n
        Step 7:
            - reboot the controller:
                - verify that all attributes for each device did not change
                - because the clock was increment we can verify status and verify that the zone didn't start \n
                  watering after the reboot \n
        step 8:
            - replace devices:
                - Loading new devices into controller
                - Searching for devices so that they can be addressed
                - Address zones and master valves to exciting addresses
                - set new moisture sensor to exciting primary zone
                - Set default values for:
                    - moisture sensor
                    - flow meters
                    - event switch
                    - temperature sensor
                - verify that all attributes for each device did not change
                - because the clock was increment we can verify status and verify that the zone didn't start \n
                  watering after the reboot \n
        Step 9:
            - disable able devices:
            - update firmware:
                - verify that all attributes for each device did not change
                - because the clock was increment we can verify status and verify that the zone didn't start \n
                  watering after the reboot \n
        """
        try:
            number_of_retries = 2
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    # self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + error_txt
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - sets up the controller \n
        - verify basemanager connection
        """
        self.config.controllers[1].init_cn()

        # only need this for BaseManager
        # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock for
        # the reboot process.
        # self.config.controllers[1].set_sim_mode_to_off()
        self.config.basemanager_connection[1].verify_ip_address_state()
        # setup controller
        # Stop clock
        # enable faux IO

    def step_2(self):
        # """
        # - sets the devices that will be used in the configuration of the controller \n
        # - search and address the devices:
        #     - zones                 {zn}
        #     - Master Valves         {mv}
        #     - Moisture Sensors      {ms}
        #     - Temperature Sensors   {ts}
        #     - Event Switches        {sw}
        #     - Flow Meter            {fm}
        # - once the devices are found they can be addressed so that they can be used in the programming
        #     - zones can use addresses {1-200}
        #     - Master Valves can use address {1-8}
        # - the 1000 auto address certain devices in the order it receives them:
        #     - Master Valves         {mv}
        #     - Moisture Sensors      {ms}
        #     - Temperature Sensors   {ts}
        #     - Event Switches        {sw}
        #     - Flow Meter            {fm}
        # """
        # # load all devices need into the controller so that they are available for use in the configuration
        self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                     mv_d1_list=self.config.mv_d1,
                                                     d2_list=self.config.d2,
                                                     mv_d2_list=self.config.mv_d2,
                                                     d4_list=self.config.d4,
                                                     dd_list=self.config.dd,
                                                     ms_list=self.config.ms,
                                                     fm_list=self.config.fm,
                                                     ts_list=self.config.ts,
                                                     sw_list=self.config.sw)

        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
        # assign zones an address between 1-200
        self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                         zn_ad_range=self.config.zn_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
        self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                         mv_ad_range=self.config.mv_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                         ms_ad_range=self.config.ms_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                         ts_ad_range=self.config.ts_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
        self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                         sw_ad_range=self.config.sw_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
        self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                         fm_ad_range=self.config.fm_ad_range)
        self.config.create_1000_poc_objects()

    def step_3(self):
        """
        set_up_programs
        assign zones to programs \n
        set up primary linked zones \n
        give each zone a run time of 1 hour and 30 minutes \n
        give each program a start time of 8:00 A.M. \n
        must make zone 200 a primary zone before you can link zones to it \n
        assign sensor to primary zone 1
        #assign moisture sensors to a primary zone 1 and set to lower limit watering
        """
        # this is set in the PG1000 object
        # TODO need to have concurrent zones per program added

        program_8am_start_time = [480]
        mon_wed_fri_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
        every_day_watering_days = [1, 1, 1, 1, 1, 1, 1]  # run every day
        program_number_1_water_windows = ['011111100001111111111110']
        program_full_open_water_windows = ['111111111111111111111111']
        program_number_closed_water_windows = ['000000000000000000000000']

        self.config.programs[1] = PG1000(_address=1,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)

        self.config.programs[2] = PG1000(_address=2,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[3] = PG1000(_address=3,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[4] = PG1000(_address=4,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[5] = PG1000(_address=5,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[6] = PG1000(_address=6,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[7] = PG1000(_address=7,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[8] = PG1000(_address=8,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[9] = PG1000(_address=9,
                                         _enabled_state=opcodes.true,
                                         _water_window=program_number_1_water_windows,
                                         _max_concurrent_zones=1,
                                         _seasonal_adjust=100,
                                         _point_of_connection_address=[1],
                                         _master_valve_address=1,
                                         _soak_cycle_mode=opcodes.disabled,
                                         _cycle_count=2,
                                         _soak_time=600,
                                         _program_cycles=1)
        self.config.programs[10] = PG1000(_address=10,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[11] = PG1000(_address=11,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[12] = PG1000(_address=12,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[13] = PG1000(_address=13,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[14] = PG1000(_address=14,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[15] = PG1000(_address=15,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[16] = PG1000(_address=16,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[17] = PG1000(_address=17,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[18] = PG1000(_address=18,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[19] = PG1000(_address=19,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)

        self.config.programs[20] = PG1000(_address=20,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[21] = PG1000(_address=21,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[22] = PG1000(_address=22,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[23] = PG1000(_address=23,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[24] = PG1000(_address=24,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[25] = PG1000(_address=25,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[26] = PG1000(_address=26,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[27] = PG1000(_address=27,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[28] = PG1000(_address=28,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[29] = PG1000(_address=29,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)
        self.config.programs[30] = PG1000(_address=30,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)

        self.config.programs[31] = PG1000(_address=31,
                                          _enabled_state=opcodes.true,
                                          _water_window=program_number_1_water_windows,
                                          _max_concurrent_zones=1,
                                          _seasonal_adjust=100,
                                          _point_of_connection_address=[1],
                                          _master_valve_address=1,
                                          _soak_cycle_mode=opcodes.disabled,
                                          _cycle_count=2,
                                          _soak_time=600,
                                          _program_cycles=1)

    def step_4(self):
        # Zone Programs
        self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                   prog_obj=self.config.programs[1],
                                                   _rt=900)

        self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                   prog_obj=self.config.programs[2],
                                                   _rt=900)

        self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                   prog_obj=self.config.programs[3],
                                                   _rt=900)

        self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                   prog_obj=self.config.programs[4],
                                                   _rt=900)

        self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                   prog_obj=self.config.programs[5],
                                                   _rt=900)

        self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[6],
                                                   prog_obj=self.config.programs[6],
                                                   _rt=900)

        self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[7],
                                                   prog_obj=self.config.programs[7],
                                                   _rt=900)

        self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[8],
                                                   prog_obj=self.config.programs[8],
                                                   _rt=900)
        # Zone Programs
        self.config.zone_programs[9] = ZoneProgram(zone_obj=self.config.zones[9],
                                                   prog_obj=self.config.programs[9],
                                                   _rt=900)

        self.config.zone_programs[10] = ZoneProgram(zone_obj=self.config.zones[10],
                                                    prog_obj=self.config.programs[10],
                                                    _rt=900)

        self.config.zone_programs[11] = ZoneProgram(zone_obj=self.config.zones[11],
                                                    prog_obj=self.config.programs[11],
                                                    _rt=900)

        self.config.zone_programs[12] = ZoneProgram(zone_obj=self.config.zones[12],
                                                    prog_obj=self.config.programs[12],
                                                    _rt=900)

        self.config.zone_programs[13] = ZoneProgram(zone_obj=self.config.zones[13],
                                                    prog_obj=self.config.programs[13],
                                                    _rt=900)

        self.config.zone_programs[14] = ZoneProgram(zone_obj=self.config.zones[14],
                                                    prog_obj=self.config.programs[14],
                                                    _rt=900)

        self.config.zone_programs[15] = ZoneProgram(zone_obj=self.config.zones[15],
                                                    prog_obj=self.config.programs[15],
                                                    _rt=900)

        self.config.zone_programs[16] = ZoneProgram(zone_obj=self.config.zones[16],
                                                    prog_obj=self.config.programs[16],
                                                    _rt=900)

        self.config.zone_programs[17] = ZoneProgram(zone_obj=self.config.zones[17],
                                                    prog_obj=self.config.programs[17],
                                                    _rt=900)

        self.config.zone_programs[18] = ZoneProgram(zone_obj=self.config.zones[18],
                                                    prog_obj=self.config.programs[18],
                                                    _rt=900)
        # Zone Programs
        self.config.zone_programs[19] = ZoneProgram(zone_obj=self.config.zones[19],
                                                    prog_obj=self.config.programs[19],
                                                    _rt=900)

        self.config.zone_programs[20] = ZoneProgram(zone_obj=self.config.zones[20],
                                                    prog_obj=self.config.programs[19],
                                                    _rt=900)

        self.config.zone_programs[21] = ZoneProgram(zone_obj=self.config.zones[21],
                                                    prog_obj=self.config.programs[19],
                                                    _rt=900)

        self.config.zone_programs[22] = ZoneProgram(zone_obj=self.config.zones[22],
                                                    prog_obj=self.config.programs[19],
                                                    _rt=900)

        self.config.zone_programs[23] = ZoneProgram(zone_obj=self.config.zones[23],
                                                    prog_obj=self.config.programs[19],
                                                    _rt=900)

        self.config.zone_programs[24] = ZoneProgram(zone_obj=self.config.zones[24],
                                                    prog_obj=self.config.programs[19],
                                                    _rt=900)

    def step_6(self):
        """
        set_poc_1000
        set up POC 1 \n
            enable POC 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            assign POC 1 a target flow of 500 \n
            assign POC 1 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 2 \n
            enable POC 2 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 2 \n
            assign POC 2 a target flow of 500 \n
            assign POC 2 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 3 \n
            enable POC 3 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 3 \n
            assign POC 3 a target flow of 500 \n
            assign POC 3 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n

        """
        self.config.poc[1] = POC1000(_address=1,
                                     _flow_meter_address=1,
                                     _master_valve_address=1,
                                     _enabled_state=opcodes.true,
                                     _target_flow=1000.0,
                                     _high_flow_limit=0.0,
                                     _shutdown_on_high_flow=opcodes.false,
                                     _unscheduled_flow_limit=0.0,
                                     _shutdown_on_unscheduled=opcodes.false,
                                     _limit_concurrent_zones_to_target=opcodes.false,
                                     _fill_time=1,
                                     _flow_variance_percent=10,
                                     _flow_variance_enable=opcodes.false,
                                     _flow_fault=opcodes.false
                                     )

        self.config.poc[2] = POC1000(_address=2,
                                     _flow_meter_address=2,
                                     _master_valve_address=2,
                                     _enabled_state=opcodes.true,
                                     _target_flow=1000.0,
                                     _high_flow_limit=0.0,
                                     _shutdown_on_high_flow=opcodes.false,
                                     _unscheduled_flow_limit=0.0,
                                     _shutdown_on_unscheduled=opcodes.false,
                                     _limit_concurrent_zones_to_target=opcodes.false,
                                     _fill_time=1,
                                     _flow_variance_percent=10,
                                     _flow_variance_enable=opcodes.false,
                                     _flow_fault=opcodes.false
                                     )
        self.config.poc[3] = POC1000(_address=3,
                                     _flow_meter_address=0,
                                     _master_valve_address=0,
                                     _enabled_state=opcodes.true,
                                     _target_flow=1000.0,
                                     _high_flow_limit=0.0,
                                     _shutdown_on_high_flow=opcodes.false,
                                     _unscheduled_flow_limit=0.0,
                                     _shutdown_on_unscheduled=opcodes.false,
                                     _limit_concurrent_zones_to_target=opcodes.false,
                                     _fill_time=1,
                                     _flow_variance_percent=10,
                                     _flow_variance_enable=opcodes.false,
                                     _flow_fault=opcodes.false
                                     )

    def step_7(self):
        """
        :return:
        :rtype:
        """
        self.config.controllers[1].do_increment_clock(minutes=1)
        # time.sleep(20)
        self.config.verify_full_configuration()
        # this turns off fast sim mode and start the clock
        self.config.controllers[1].set_sim_mode_to_off()
        self.config.controllers[1].start_clock()
        # this press the run key so that the controller will come out of the paused state
        self.config.controllers[1].set_controller_to_run()

        self.config.controllers[1].turn_off_echo()

        date_mngr.set_current_date_to_match_computer()
        # By setting a time on the controller we can know what time each message has been set
        self.config.controllers[1].set_date_and_time_on_cn(
            _date=date_mngr.curr_computer_date.date_string_for_controller(),
            _time=date_mngr.curr_computer_date.time_string_for_controller())

    def step_8(self):
        """
        This sets the zone messages
        """
        self.config.zones[1].set_message_on_cn(opcodes.checksum)
        self.config.zones[1].verify_message_on_cn(opcodes.checksum)
        self.config.zones[2].set_message_on_cn(opcodes.no_response)
        self.config.zones[2].verify_message_on_cn(opcodes.no_response)
        self.config.zones[3].set_message_on_cn(opcodes.bad_serial)
        self.config.zones[3].verify_message_on_cn(opcodes.bad_serial)
        self.config.zones[4].set_message_on_cn(opcodes.low_voltage)
        self.config.zones[4].verify_message_on_cn(opcodes.low_voltage)
        self.config.zones[5].set_message_on_cn(opcodes.open_circuit)
        self.config.zones[5].verify_message_on_cn(opcodes.open_circuit)
        self.config.zones[6].set_message_on_cn('OC')
        self.config.zones[6].verify_message_on_cn('OC')
        self.config.zones[7].set_message_on_cn(opcodes.exceeds_design_flow_1000)
        self.config.zones[7].verify_message_on_cn(opcodes.exceeds_design_flow_1000)

    def step_9(self):
        """
        set messages for the temperature sensor
        :return:
        :rtype:
        """
        self.config.temperature_sensors[1].set_message_on_cn(opcodes.checksum)
        self.config.temperature_sensors[1].verify_message_on_cn(opcodes.checksum)
        self.config.temperature_sensors[2].set_message_on_cn(opcodes.no_response)
        self.config.temperature_sensors[2].verify_message_on_cn(opcodes.no_response)
        self.config.temperature_sensors[3].set_message_on_cn(opcodes.bad_serial)
        self.config.temperature_sensors[3].verify_message_on_cn(opcodes.bad_serial)

    def step_10(self):
        """
        set messages for the flow meter
        :return:
        :rtype:
        """
        self.config.flow_meters[1].set_message_on_cn(opcodes.checksum)
        self.config.flow_meters[1].verify_message_on_cn(opcodes.checksum)
        self.config.flow_meters[2].set_message_on_cn(opcodes.no_response)
        self.config.flow_meters[2].verify_message_on_cn(opcodes.no_response)
        self.config.flow_meters[3].set_message_on_cn(opcodes.bad_serial)
        self.config.flow_meters[3].verify_message_on_cn(opcodes.bad_serial)

    def step_11(self):
        """
        set messages for event switch
        :return:
        :rtype:
        """
        self.config.event_switches[1].set_message_on_cn(opcodes.checksum)
        self.config.event_switches[1].verify_message_on_cn(opcodes.checksum)
        self.config.event_switches[2].set_message_on_cn(opcodes.no_response)
        self.config.event_switches[2].verify_message_on_cn(opcodes.no_response)
        self.config.event_switches[3].set_message_on_cn(opcodes.bad_serial)
        self.config.event_switches[3].verify_message_on_cn(opcodes.bad_serial)

    def step_12(self):
        """
        set messages for master valves
        :return:
        :rtype:
        """
        self.config.master_valves[1].set_message_on_cn(opcodes.checksum)
        self.config.master_valves[1].verify_message_on_cn(opcodes.checksum)
        self.config.master_valves[2].set_message_on_cn(opcodes.no_response)
        self.config.master_valves[2].verify_message_on_cn(opcodes.no_response)
        self.config.master_valves[3].set_message_on_cn(opcodes.bad_serial)
        self.config.master_valves[3].verify_message_on_cn(opcodes.bad_serial)
        self.config.master_valves[4].set_message_on_cn(opcodes.low_voltage)
        self.config.master_valves[4].verify_message_on_cn(opcodes.low_voltage)
        self.config.master_valves[5].set_message_on_cn("OP")
        self.config.master_valves[5].verify_message_on_cn("OP")
        self.config.master_valves[6].set_message_on_cn("OC")
        self.config.master_valves[6].verify_message_on_cn("OC")

    def step_13(self):
        """
        set messages for controller
        :return:
        :rtype:
        """
        self.config.controllers[1].set_message_on_cn(opcodes.two_wire_over_current)
        self.config.controllers[1].verify_message_on_cn(opcodes.two_wire_over_current)

    def step_14(self):
        pass

    def step_15(self):
        """
        set messages for moisture sensors
        :return:
        :rtype:
        """
        self.config.moisture_sensors[1].set_message_on_cn(opcodes.checksum)
        self.config.moisture_sensors[1].verify_message_on_cn(opcodes.checksum)
        self.config.moisture_sensors[2].set_message_on_cn(opcodes.no_response)
        self.config.moisture_sensors[2].verify_message_on_cn(opcodes.no_response)
        self.config.moisture_sensors[3].set_message_on_cn(opcodes.bad_serial)
        self.config.moisture_sensors[3].verify_message_on_cn(opcodes.bad_serial)

    def step_16(self):
        """
        set messages for point of connection
        :return:
        :rtype:
        """
        self.config.poc[1].set_message_on_cn(opcodes.device_error)
        self.config.poc[1].verify_message_on_cn(opcodes.device_error)
        self.config.poc[2].set_message_on_cn(opcodes.high_flow_shutdown)
        self.config.poc[2].verify_message_on_cn(opcodes.high_flow_shutdown)
        self.config.poc[3].set_message_on_cn(opcodes.unscheduled_flow_shutdown)
        self.config.poc[3].verify_message_on_cn(opcodes.unscheduled_flow_shutdown)

    def step_17(self):
        """
        set messages for programs
        :return:
        :rtype:
        """
        self.config.programs[1].set_message_on_cn(opcodes.over_run_start_event)
        self.config.programs[1].verify_message_on_cn(opcodes.over_run_start_event)
        self.config.programs[2].set_message_on_cn(opcodes.learn_flow_with_errors)
        self.config.programs[2].verify_message_on_cn(opcodes.learn_flow_with_errors)
        self.config.programs[3].set_message_on_cn(opcodes.calibrate_failure_no_change)
        self.config.programs[3].verify_message_on_cn(opcodes.calibrate_failure_no_change)
        self.config.programs[4].set_message_on_cn(opcodes.start, opcodes.basemanager)
        self.config.programs[4].verify_message_on_cn(opcodes.start, opcodes.basemanager)
        self.config.programs[5].set_message_on_cn(opcodes.start, opcodes.user)
        self.config.programs[5].verify_message_on_cn(opcodes.start, opcodes.user)
        self.config.programs[6].set_message_on_cn(opcodes.start, opcodes.operator)
        self.config.programs[6].verify_message_on_cn(opcodes.start, opcodes.operator)
        self.config.programs[7].set_message_on_cn(opcodes.start, opcodes.programmer)
        self.config.programs[7].verify_message_on_cn(opcodes.start, opcodes.programmer)
        self.config.programs[8].set_message_on_cn(opcodes.start, opcodes.admin)
        self.config.programs[8].verify_message_on_cn(opcodes.start, opcodes.admin)
        self.config.programs[9].set_message_on_cn(opcodes.start, opcodes.moisture)
        self.config.programs[9].verify_message_on_cn(opcodes.start, opcodes.moisture)
        self.config.programs[10].set_message_on_cn(opcodes.start, opcodes.event_switch)
        self.config.programs[10].verify_message_on_cn(opcodes.start, opcodes.event_switch)
        self.config.programs[11].set_message_on_cn(opcodes.start, opcodes.temperature_sensor)
        self.config.programs[11].verify_message_on_cn(opcodes.start, opcodes.temperature_sensor)
        self.config.programs[12].set_message_on_cn(opcodes.start, opcodes.date_time)
        self.config.programs[12].verify_message_on_cn(opcodes.start, opcodes.date_time)
        self.config.programs[13].set_message_on_cn(opcodes.pause, opcodes.system)
        self.config.programs[13].verify_message_on_cn(opcodes.pause, opcodes.system)
        self.config.programs[14].set_message_on_cn(opcodes.pause, opcodes.moisture)
        self.config.programs[14].verify_message_on_cn(opcodes.pause, opcodes.moisture)
        self.config.programs[15].set_message_on_cn(opcodes.pause, opcodes.event_switch)
        self.config.programs[15].verify_message_on_cn(opcodes.pause, opcodes.event_switch)
        self.config.programs[16].set_message_on_cn(opcodes.pause, opcodes.temperature_sensor)
        self.config.programs[16].verify_message_on_cn(opcodes.pause, opcodes.temperature_sensor)
        self.config.programs[17].set_message_on_cn(opcodes.pause, opcodes.water_window_paused)
        self.config.programs[17].verify_message_on_cn(opcodes.pause, opcodes.water_window_paused)
        self.config.programs[18].set_message_on_cn(opcodes.pause, opcodes.event_day)
        self.config.programs[18].verify_message_on_cn(opcodes.pause, opcodes.event_day)
        self.config.programs[19].set_message_on_cn(opcodes.stop, opcodes.basemanager)
        self.config.programs[19].verify_message_on_cn(opcodes.stop, opcodes.basemanager)
        self.config.programs[20].set_message_on_cn(opcodes.stop, opcodes.user)
        self.config.programs[20].verify_message_on_cn(opcodes.stop, opcodes.user)
        self.config.programs[21].set_message_on_cn(opcodes.stop, opcodes.operator)
        self.config.programs[21].verify_message_on_cn(opcodes.stop, opcodes.operator)
        self.config.programs[22].set_message_on_cn(opcodes.stop, opcodes.programmer)
        self.config.programs[22].verify_message_on_cn(opcodes.stop, opcodes.programmer)
        self.config.programs[23].set_message_on_cn(opcodes.stop, opcodes.admin)
        self.config.programs[23].verify_message_on_cn(opcodes.stop, opcodes.admin)
        self.config.programs[24].set_message_on_cn(opcodes.stop, opcodes.moisture)
        self.config.programs[24].verify_message_on_cn(opcodes.stop, opcodes.moisture)
        self.config.programs[25].set_message_on_cn(opcodes.stop, opcodes.event_switch)
        self.config.programs[25].verify_message_on_cn(opcodes.stop, opcodes.event_switch)
        self.config.programs[26].set_message_on_cn(opcodes.stop, opcodes.temperature_sensor)
        self.config.programs[26].verify_message_on_cn(opcodes.stop, opcodes.temperature_sensor)
        self.config.programs[27].set_message_on_cn(opcodes.stop, opcodes.date_time)
        self.config.programs[27].verify_message_on_cn(opcodes.stop, opcodes.date_time)
        self.config.programs[28].set_message_on_cn(opcodes.stop, opcodes.system_off)
        self.config.programs[28].verify_message_on_cn(opcodes.stop, opcodes.system_off)
        self.config.programs[29].set_message_on_cn(opcodes.stop, opcodes.rain_switch)
        self.config.programs[29].verify_message_on_cn(opcodes.stop, opcodes.rain_switch)
        self.config.programs[30].set_message_on_cn(opcodes.stop, opcodes.rain_delay)
        self.config.programs[30].verify_message_on_cn(opcodes.stop, opcodes.rain_delay)

        date_mngr.set_date_to_default()

