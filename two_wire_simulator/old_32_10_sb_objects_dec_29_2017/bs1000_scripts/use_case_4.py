import sys

from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_1000 import PG1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_1000 import StartConditionFor1000, StopConditionFor1000
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

# this import allows us to keep track of time
from datetime import time, timedelta, datetime, date
from time import sleep

__author__ = 'Tige'


class ControllerUseCase4(object):
    """
    Test Name:
        - Nelson Feature Program Cycles
    purpose:
        - set up the nelson feature
            - verify that program cycles work

    Coverage area: \n
        - programs: \n
            - program continues cycles \n
                - if water window closes program pause and than continues \n
                - if day interval doesnt allow continues cycles \n
                - stop program using an event switch
    Things that are not covered: \n
        - programs: \n
            - program continues cycles \n
                - if day interval doesnt allow continues cycles \n
    Date References:
        - configuration for script is located common\configuration_files\nelson_features.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, controller_type, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance,
                 json_configuration_file):
        """
        Initialize 'UseCase2' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        """

        # TODO: Need to handle opening and closing of file correctly.
        try:
            number_of_retries = 1
            retries = 0

            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    # TODO need to add close serial port
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - sets up the controller \n
        - set max concurrent zones of the controller to 50
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        self.config.controllers[1].init_cn()

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - master valves         {mv}
            - event switch          {sw}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-100}
            - master valves can use address {1-100}
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        # load all devices need into the controller so that they are available for use in the configuration
        try:
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(
                ms_object_dict=self.config.moisture_sensors,
                ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(
                ts_object_dict=self.config.temperature_sensors,
                ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)

            self.config.create_1000_poc_objects()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_3(self):
        """
        - Set up program \n
            - create program object
                - program 1\n
                    - create start stop pause object
                        - start time 8:00am \n
                        - full open water window \n
                        - Water every day \n
                        - add zone 1-5 to program
                            -  give each zone a 10 minute runtime


        """
        program_8_am_start_times = [480]  # 8:00 am start time
        program_waters_every_day = [1, 1, 1, 1, 1, 1, 1]  # runs everyday
        program_waters_mon_wed_fri = [0, 1, 0, 1, 0, 1, 0]  # runs Monday, Wednesday, Friday
        program_fully_open_water_windows = ['111111111111111111111111']
        program_water_windows_closed_11am_to_12_pm = ['111111111101111111111111']

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # create program 1
            self.config.programs[1] = PG1000(_address=1,
                                             _description='Continuous Program Cycles',
                                             _enabled_state=opcodes.true,
                                             _water_window=program_fully_open_water_windows,
                                             _point_of_connection_address=[1],
                                             _soak_cycle_mode=opcodes.disabled
                                             )
            self.config.program_start_conditions[1] = StartConditionFor1000(program_ad=1)
            self.config.program_start_conditions[1].set_day_time_start(_dt=opcodes.true,
                                                                       _st_list=program_8_am_start_times,
                                                                       _interval_type=opcodes.week_days,
                                                                       _interval_args=program_waters_every_day)
            self.config.program_stop_conditions[1] = StopConditionFor1000(program_ad=1)
            self.config.program_stop_conditions[1].set_event_switch_condition_on_pg(self.config.event_switches[1].sn,
                                                                                    mode=opcodes.contacts_open,
                                                                                    si=opcodes.true)

            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1], _rt=600)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1], _rt=600)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1], _rt=600)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1], _rt=600)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[1], _rt=600)

            # create program 5
            self.config.programs[5] = PG1000(_address=5,
                                             _description='Master Valves Assigned to Zones',
                                             _enabled_state=opcodes.true,
                                             _water_window=program_water_windows_closed_11am_to_12_pm,
                                             _point_of_connection_address=[1],
                                             _soak_cycle_mode=opcodes.disabled
                                             )
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_4(self):
        """
         Verify all attribute values for all objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_5(self):
        """
        Testing Continuous cycles for a program with a fully open water window
        - set program 1 cycles to continuous
        - set max concurrent zones on the program to 5
        - set max concurrent zones on the controller to 5
        - this will have all the zones run at one time
        - set clock on controller to 7:59am
        - increment clock to 8:00
        - program 1 should start
        - increment time for 48 hours in 9 minute interval's program should continuously run 9 minutes is off all runtime boundaries so I can verify still running
        - also this will verify that we can cross midnight and still keep running
        - verify every 9 minutes that all zones are running
        - after 48 hours stop program
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # date_mngr.set_dates_from_date_range(start_date='12-01-2015', end_date='12-28-2015')
            date_mngr.set_current_date_to_match_computer()
            # set program to be continuous program cycles 16 = continous
            self.config.programs[1].set_program_cycles_on_cn(_number_of_cycles=26)

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=5)
            self.config.programs[1].set_max_concurrent_zones_on_cn(_number_of_zones=5)

            date_mngr.set_current_date_to_match_computer()
            self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='07:59:00')
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].do_increment_clock(minutes=1)
            # TODO next line is throughing an error cant cast a string to a date object
            # date_mngr.set_dates_from_date_range(start_date=date_mngr.curr_day.date_string_for_controller(),end_date=date_mngr.curr_day.obj + timedelta(days=2))
            date_mngr.end_date = date_mngr.curr_day.obj + timedelta(days=2)
            # loops through the program zones looks at the program and finds the zones in that program
            while not date_mngr.reached_last_date():
                for zone_program in self.config.zone_programs.values():
                    if zone_program.program.ad == 1:
                        self.config.zones[zone_program.zone.ad].get_data()
                        self.config.zones[zone_program.zone.ad].verify_status_on_cn(status=opcodes.watering)
                self.config.controllers[1].do_increment_clock(minutes=9)

            self.config.controllers[1].set_program_start_stop(_pg_ad=1,_function=opcodes.stop)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_6(self):
        """
        Testing Continuous cycles for a program with a water window that opens and closes
        - disable programs 5,10,40 so that they don't effect the test
        - set program 1 cycles to continuous
        - set max concurrent zones on the program to 5
        - set max concurrent zones on the controller to 5
        - this will have all the zones run at one time
        - set clock on controller to 7:59am
        - increment clock to 8:00
        - program 1 should start
        using date manager set end date of test 2 days later
        - increment time for 2 days  in 9 minute interval's program should continuously run 9 minutes is off all
        runtime boundaries so I can verify still running
        - also this will verify that we can cross midnight and still keep running
        - verify every 9 minutes that all zones are running
        - verify that when water window close the zone pause and when it open back up they start watering again
        - after 48 hours stop program with event switch
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        program_water_windows_closed_11am_to_12_pm = ['111111111110111111111111']
        try:
            self.config.programs[1].set_water_window_on_cn(_ww=program_water_windows_closed_11am_to_12_pm)
            water_window_closed = datetime.strptime("11:00:00", '%H:%M:%S')
            water_window_open = datetime.strptime("11:59:00", '%H:%M:%S')

            #set program to be continuous program cycles 16 = continous
            self.config.programs[1].set_program_cycles_on_cn(_number_of_cycles=26)

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=5)
            self.config.programs[1].set_max_concurrent_zones_on_cn(_number_of_zones=5)
            self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='07:59:00')
            self.config.controllers[1].set_controller_to_run()
            self.config.controllers[1].do_increment_clock(minutes=1)

            date_mngr.end_date = date_mngr.curr_day.obj + timedelta(days=2)
            # loops through the program zones looks at the program and finds the zones in that program
            while not date_mngr.reached_last_date():
                for zone_program in self.config.zone_programs.values():
                    if zone_program.program.ad == 1:
                        self.config.zones[zone_program.zone.ad].get_data()
                        if water_window_closed.time() <= date_mngr.curr_day.time_obj.obj <= water_window_open.time():
                            self.config.zones[zone_program.zone.ad].verify_status_on_cn(status=opcodes.paused)
                        else:
                            self.config.zones[zone_program.zone.ad].verify_status_on_cn(status=opcodes.watering)
                self.config.controllers[1].do_increment_clock(minutes=9)

            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.contacts_open)
            # stop program verify all zones are set to done
            self.config.controllers[1].do_increment_clock(minutes=5)
            for zone_program in self.config.zone_programs.values():
                if zone_program.program.ad == 1:
                    self.config.zones[zone_program.zone.ad].get_data()
                    self.config.zones[zone_program.zone.ad].verify_status_on_cn(status=opcodes.done_watering)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)