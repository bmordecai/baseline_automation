import csv
import os
import logging
import sys

from csv_handler import CSVWriter
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes, csv
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

from datetime import time, timedelta, datetime, date
from time import sleep

__author__ = 'Tige'


class ControllerUseCase9(object):
    """
    Test name:
        - 3200 RAM Test
    purpose:
        - set up a test that will max out the Ram on the 3200
            - setup 6 different zones using Weather based watering
            - verify that teh ETc runtime values change when the eto input value changes
            - verify that correct cycle and soak are performed
            - verify that controller starts watering when deficit is below 50%
            - verify that the controller meets a 80% efficacy rating

    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
        - setting programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign zones to programs \n
        - create 99 Programs
        - create 200 Zones
        - Add zones to programs until the 3200 runs out of ram
            - Create a program
            - add zone
            - look for message say low memory
    Date References:
        - configuration for script is located common\configuration_files\ram_test_3200.json
        - the devices and addresses range is read from the .json file

    """
# program_range = range(1, 100)
    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id
        self.program_range = range(1, 10)

    def run_use_case(self):
        """
        Step 1:
            - configure controller:
                -
        step 2:
            - load devices:
                - load all devices needed for test\n
                - set default values for each zone \n
        Step 3:
            - setting up programming:
                - set_up_programs to programs \n
                - set default values for each program \n
        step 4:
            - loop through assigning each zone to a program
            - repeat until on zones are assign to every program
            - after each zone is assign verify the memory ussage of the controller
         """

        # TODO: Need to handle opening and closing of file correctly.
        try:
            number_of_retries = 0
            retries = 0

            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    # TODO need to add close serial port
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:

            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - sets up the controller \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - set the default values for each zone
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        # load all devices need into the controller so that they are available for use in the configuration
        try:
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw
                                                         )

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_3(self):
        """
        setup 99 Programs
        set default values for each program created
        """


        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            program_number_1_start_times = [120]  # 2:00 am start time
            program_number_1_watering_days = [1, 1, 1, 1, 1, 1, 1]  # runs everyday
            program_number_1_water_windows = ['111111111111111111111111']

            for programs in self.program_range:
                self.config.programs[programs] = PG3200(_ad=programs,
                                                        _en=opcodes.true,
                                                        _ww=program_number_1_water_windows,
                                                        _pr=1,
                                                        _mc=1,
                                                        _sa=100,
                                                        _ci=opcodes.week_days,
                                                        _di=None,
                                                        _wd=program_number_1_watering_days,
                                                        _sm=[],
                                                        _st=program_number_1_start_times,
                                                        _bp='')
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_4(self):
        """
        for each program add a zone than check to see what the memory usage of the controller is
        after 200 zones are add to the program increment the clock 1 minute
        after the controller reach 99% usage verify that a message is set "low memory detected"

        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        # enable each zone to run  on ET run times
        # set _rt = 1 so that there was just a value
        try:
            for program in self.program_range:
                for zone in self.config.zn_ad_range:
                    self.config.zone_programs[zone] = ZoneProgram(zone_obj=self.config.zones[zone],
                                                                  prog_obj=self.config.programs[program], _rt=60)
                    self.config.controllers[1].get_memory_usage_on_cn()
                    if self.config.controllers[1].mu >= 99:
                        self.config.controllers[1].do_increment_clock(minutes=1)
                        #TODO need to build this message
                        # self.config.controllers[1].verify_message_on_cn(opcodes.memory_usage)
                    else:
                        print "Controller {0} has {1} total programs {2} total zones and is using {3}% of controller " \
                              "memory." .format(
                                self.config.controllers[1].mac,     # {0} Controller mac address
                                program,                            # {1} Total number of programs used
                                str(200*(program-1) + zone),        # {2} Total number of zones used
                                self.config.controllers[1].mu       # {3} how much memory the controller has used
                                )
                self.config.controllers[1].do_increment_clock(minutes=1)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)



