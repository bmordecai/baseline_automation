import sys
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from datetime import time, timedelta, datetime, date
# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# Browser pages used
# import page_factory

__author__ = 'Tige'


class ControllerUseCase14(object):
    """
    Test name:
        - CN UseCase14 low flow variance
    purpose:
        - set up a full configuration on the controller
            - check for the low flow variance to be working correctly
                - verify message with or without shutdown
    Coverage area: \n
        1. Setting up the controller \n
        2. Searching and assigning: \n
            - Zones \n
            - Master Valves \n
            - Flow Meters \n
        3. Assign each zone a design flow \n
        4. Setting up water sources \n
        5. Set up the programs \n
        6. Assign each zone to a program \n
        7. Set up the zone concurrency for the controller and programs \n
        8. Verify the full configuration
        9. Run programs with a design flow restriction on program 1 with Limit Concurrent active \n
        10. Run programs with two design flow one on each program, one with Limit Concurrent active and one without \n
        11. Run programs with two design flow one on each program, both with Limit Concurrent active \n
        12. Verify the full configuration again
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
            - set max concurrency on controller to 1 we only want 1 zone to run at a time
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # only need this for BaseManager
            # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock
            # for the reboot process.
            # self.config.controllers[1].set_sim_mode_to_off()
            # self.config.basemanager_connection[1].verify_ip_address_state()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=2)
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller failed: " +
                                 str(ae.message))

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Load all devices to controller
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                             ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_3(self):
        """
        set_up_programs
        assign zones to programs \n
        set up primary linked zones \n
        give each zone a run time of 1 hour and 30 minutes \n
        give each program a start time of 8:00 A.M. \n
        must make zone 200 a primary zone before you can link zones to it \n
        assign sensor to primary zone 1
        #assign moisture sensors to a primary zone 1 and set to lower limit watering
        """
        # this is set in the PG3200 object

        program_start_time_10am = [600]  # 10:00am start time
        program_start_time_2pm = [840]  # 2:00pm start time
        program_watering_days = [1, 1, 1, 1, 1, 1, 1]  # runs all days
        program_water_windows = ['111111111111111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _ww=program_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.week_days,
                                             _di=None,
                                             _wd=program_watering_days,
                                             _sm=[],
                                             _st=program_start_time_10am,
                                             _ml=1,
                                             _bp='')
            self.config.programs[99] = PG3200(_ad=99,
                                              _en=opcodes.true,
                                              _ww=program_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.week_days,
                                              _di=None,
                                              _wd=program_watering_days,
                                              _sm=[],
                                              _st=program_start_time_2pm,
                                              _ml=8,
                                              _bp='')

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_4(self):
        """
        - set up Zone Programs
            - add zones to program 1
                - zone 1 is a primary
                    - _rt = runtime 960 seconds
                    - _ct = cycle time = 480 seconds
                    - _so = soak time = 300 seconds
                    - zones 2-5 are linked ot zone 1
            - add zones to program 99
                - zones 6 - 10 are all set to timed
                    - _rt = runtime 360 seconds
                    - _ct = cycle time = 180 seconds
                    - _so = soak time = 120 seconds
                    - _pz = 0 means timed

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=960,
                                                       _ct=480,
                                                       _so=300,
                                                       _pz=1)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _pz=1)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _pz=1)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _pz=1)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _pz=1)

            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[6],
                                                       prog_obj=self.config.programs[99],
                                                       _rt=360,
                                                       _ct=180,
                                                       _so=120,
                                                       _pz=0)
            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[7],
                                                       prog_obj=self.config.programs[99],
                                                       _rt=360,
                                                       _ct=180,
                                                       _so=120,
                                                       _pz=0)
            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[8],
                                                       prog_obj=self.config.programs[99],
                                                       _rt=360,
                                                       _ct=180,
                                                       _so=120,
                                                       _pz=0)
            self.config.zone_programs[9] = ZoneProgram(zone_obj=self.config.zones[9],
                                                       prog_obj=self.config.programs[99],
                                                       _rt=360,
                                                       _ct=180,
                                                       _so=120,
                                                       _pz=0)
            self.config.zone_programs[10] = ZoneProgram(zone_obj=self.config.zones[10],
                                                        prog_obj=self.config.programs[99],
                                                        _rt=360,
                                                        _ct=180,
                                                        _so=120,
                                                        _pz=0)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_5(self):
        """
        Give each zone a design flow
            - zones 1 -5 20 gpm
            - zones 6 - 10 50 gpm
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.zones[1].set_design_flow_on_cn(_df=20.0)
            self.config.zones[2].set_design_flow_on_cn(_df=20.0)
            self.config.zones[3].set_design_flow_on_cn(_df=20.0)
            self.config.zones[4].set_design_flow_on_cn(_df=20.0)
            self.config.zones[5].set_design_flow_on_cn(_df=20.0)
            self.config.zones[6].set_design_flow_on_cn(_df=50.0)
            self.config.zones[7].set_design_flow_on_cn(_df=50.0)
            self.config.zones[8].set_design_flow_on_cn(_df=50.0)
            self.config.zones[9].set_design_flow_on_cn(_df=50.0)
            self.config.zones[10].set_design_flow_on_cn(_df=50.0)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_6(self):
        """
        set_mainlines_3200
        - set up main line 1 \n
            - set limit zones by flow to true \n
            - set the pipe fill time to 4 minutes \n
            - set the target flow to 500 \n
            - set the high variance limit to 5% and enable the high variance shut down \n
            - set the low variance limit to 20% and enable the low variance shut down \n
        - set up main line 8 \n
            - set limit zones by flow to true \n
            - set the pipe fill time to 1 minutes \n
            - set the target flow to 500 \n
            - set the high variance limit to 5% and enable the high variance shut down \n
            - set the low variance limit to 20% and enable the low variance shut down \n
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.mainlines[1] = Mainline(_ad=1,              # address number
                                                _ft=4,              # pipe file time in minutes
                                                _fl=20,             # Target flow
                                                _lc=opcodes.true,   # limit zones by flow
                                                _hv=5,              # high flow variance in percentage
                                                _hs=opcodes.true,   # high flow variance shutdown enabled
                                                _lv=20,             # low flow variance in percentage
                                                _ls=opcodes.true)   # low flow variance shutdown

            self.config.mainlines[8] = Mainline(_ad=8,              # address number
                                                _ft=1,              # pipe file time in minutes
                                                _fl=50,             # Target flow
                                                _lc=opcodes.true,   # limit zones by flow
                                                _hv=10,              # high flow variance in percentage
                                                _hs=opcodes.false,   # high flow variance shutdown disabled
                                                _lv=5,               # low flow variance in percentage
                                                _ls=opcodes.false)   # low flow variance shutdown disabled

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_7(self):
        """
        set_poc_3200
            - set up POC 1 \n
                - enable POC 1 \n
                - assign master valve TMV0001 and flow meter TWF0001 to POC 1 \n
                - assign POC 1 a target flow of 500 \n
                - assign POC 1 to main line 1 \n
                - set POC priority to 2-medium \n
                - set high flow limit to 550 and enable high flow shut down \n
                - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                - set water budget to 0 and disable the water budget shut down \n
                - disable water rationing \n
            - set up POC 8 \n
                - enable POC 8 \n
                - assign master valve TMV0002 and flow meter TWF0002 to POC 1 \n
                - assign POC 8 a target flow of 500 \n
                - assign POC 8 to main line 1 \n
                - set POC priority to 2-medium \n
                - set high flow limit to 550 and enable high flow shut down \n
                - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                - set water budget to 0 and disable the water budget shut down \n
                - disable water rationing \n
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1] = POC3200(
                _ad=1,                  # address number
                _en=opcodes.true,       # poc enabled
                _mv=1,                  # poc assigned to master valve 1
                _fm=1,                  # poc assigned to flow meter 1
                _fl=500,                # target flow
                _ml=1,                  # poc assigned to mainline 1
                _pr=2,                  # priority
                _hf=550,                # high flow
                _hs=opcodes.true,       # high flow shutdown enabled
                _uf=15.0,               # unscheduled flow
                _us=opcodes.true,       # unscheduled flow shutdown enabled
                _wb=0,                  # water budget
                _ws=opcodes.false,      # water budget exceeded shutdown enabled false
                _wr=opcodes.false)      # Water Rationing Enable false

            self.config.poc[8] = POC3200(
                _ad=8,                  # address number
                _en=opcodes.true,       # poc enabled
                _mv=2,                  # poc assigned to master valve 2
                _fm=2,                  # poc assigned to flow meter 2
                _fl=500,                # target flow
                _ml=8,                  # poc assigned to mainline 8
                _pr=2,                  # priority
                _hf=550,                # high flow
                _hs=opcodes.true,       # high flow shutdown enabled
                _uf=40.0,               # unscheduled flow
                _us=opcodes.true,       # unscheduled flow shutdown enabled
                _wb=0,                  # water budget
                _ws=opcodes.false,      # water budget exceeded shutdown enabled false
                _wr=opcodes.false)      # Water Rationing Enable false

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_8(self):
        """
        verify full configuration in objects match the controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_9(self):
        """
        trigger a low flow variance on just the mainline 1 with shutdown enabled
            - mainline 1 has a low flow variance of 20% with shutdown
        set the GPM on flow meter 1 to 15 GPM
        set the GPM on flow meter 2 to 0 GPM because this program in not running
        program 1 has a 10:am start time
        zones 1-5 will get a low flow variance alarm and will shutdown so there status will be an error
        verify that the zones shut down in the appropriate amount of time
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=15.0)
            self.config.flow_meters[2].set_flow_rate_on_cn(_flow_rate=0)
            date_mngr.set_current_date_to_match_computer()
            self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='09:59:00')
            self.config.controllers[1].do_increment_clock(minutes=2)
            not_done = True

            while not_done:

                zones_still_running = False
                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone in range(1, 6):
                    self.config.zones[zone].get_data()
                    _zone_status = self.config.zones[zone].data.get_value_string_by_key(opcodes.status_code)
                    # for each zone get the ss per zone
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    # check the status of the zone and record each minute it is water so that we can determing number
                    # of minute before shutdown
                    if _zone_status == opcodes.watering:
                        self.config.zones[zone].seconds_zone_ran += 60  # using 60 so that we are in seconds
                    # set flag not all zones are done
                    if _zone_status != opcodes.done_watering and _zone_status != opcodes.error:
                        zones_still_running = True

                        # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------

                if zones_still_running:
                    not_done = True
                    self.config.controllers[1].do_increment_clock(minutes=1)
                    self.config.controllers[1].verify_date_and_time()
                else:
                    not_done = False
            #TODO there is a problem if a zone doesnt have shutdown enabled what is the message
            for zone in range(1, 6):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(status=opcodes.error)
                self.config.zone_programs[zone].verify_message_on_cn(opcodes.low_flow_variance_shutdown)
                self.config.zone_programs[zone].clear_message_on_cn(opcodes.low_flow_variance_shutdown)
            # check to see if each zone shut down in the correct amount of time wich is pipe fill plus 1 minute
            seconds_before_error = (self.config.mainlines[1].ft * 60) + 60
            for zone in range(1, 6):
                if seconds_before_error != self.config.zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to fail with a low flow variance " + str(seconds_before_error) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to fail with a low flow variance")
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_10(self):
        """
        trigger a low flow variance on just the mainline 8 with shutdown disabled
            - mainline 1 has a low flow variance of 20% with shutdown
            - mainline 8 has a low flow variance of 5%  with no shut down
        set the GPM on flow meter 1 to 0 GPM this is because program 1 is not running
        set the GPM on flow meter 2 to 40 GPM%
        zones 6-10 will get a low flow variance alarm and will shutdown so there status will be an error
        program 99 has a 2:pm start time
        verify that the zones shut down in the appropriate amount of time
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=0)
            self.config.flow_meters[2].set_flow_rate_on_cn(_flow_rate=40.0)
            date_mngr.set_current_date_to_match_computer()
            self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='13:59:00')
            self.config.controllers[1].do_increment_clock(minutes=2)
            not_done = True

            while not_done:

                zones_still_running = False
                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone in range(6, 11):
                    self.config.zones[zone].get_data()
                    _zone_status = self.config.zones[zone].data.get_value_string_by_key(opcodes.status_code)
                    # for each zone get the ss per zone
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    # check the status of the zone and record each minute it is water so that we can determing number
                    # of minute before shutdown
                    if _zone_status == opcodes.watering:
                        self.config.zones[zone].seconds_zone_ran += 60  # using 60 so that we are in seconds
                    # set flag not all zones are done
                    if _zone_status != opcodes.done_watering:
                        zones_still_running = True

                        # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------

                if zones_still_running:
                    not_done = True
                    self.config.controllers[1].do_increment_clock(minutes=1)
                    self.config.controllers[1].verify_date_and_time()
                else:
                    not_done = False
            for zone in range(6, 11):
                # because shutdown is not enabled the zones will not error they will finish watering
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
                self.config.zone_programs[zone].verify_message_on_cn(opcodes.low_flow_variance_detected)
                self.config.zone_programs[zone].clear_message_on_cn(opcodes.low_flow_variance_detected)
            # check to see if each zone shut down in the correct amount of time wich is pipe fill plus 1 minute
            for zone in range(6, 11):
                if self.config.zone_programs[zone].rt != self.config.zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to fail with a low flow variance " + str(self.config.zone_programs[zone].rt * 60) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to fail with a low flow variance")
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_11(self):
        """
        trigger a low flow variance on both mainlines
            - mainline 1 has a low flow variance of 20% with shutdown
            - mainline 8 has a low flow variance of 5%  with no shut down
        set the GPM on flow meter 1 to 15 GPM
        set the GPM on flow meter 2 to 40 GPM%
        zones 1-5 will get a low flow variance alarm and will shut down so there status will be an error
        zones 6-10 will get a low flow variance alarm and will shut down so there status will be an error
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set controller to be able to run two zones at a time
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=2)
            # set program 99 start time to match program 1 so that they both start at the same time
            program_start_time_10am = 600  # 10:00am start time
            self.config.programs[99].set_start_times_on_cn(_st_list=[program_start_time_10am])
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=15.0)
            self.config.flow_meters[2].set_flow_rate_on_cn(_flow_rate=40.0)
            date_mngr.set_current_date_to_match_computer()
            self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='09:59:00')
            self.config.controllers[1].do_increment_clock(minutes=2)

            # this verifies both mainlines turn on the at the same time
            self.config.zones[1].get_data()
            self.config.zones[6].get_data()
            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[6].verify_status_on_cn(opcodes.watering)
            not_done = True

            while not_done:

                zones_still_running = False
                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone in range(1, 11):

                    self.config.zones[zone].get_data()
                    _zone_status = self.config.zones[zone].data.get_value_string_by_key(opcodes.status_code)
                    # for each zone get the ss per zone
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    # when zone 5 finishes and gets an error
                    #  set flow meter back to zero because zones are off
                    # if zone in range(1, 6):
                    #     _zone_status == opcodes.done_watering and _zone_status == opcodes.error
                    #     self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=0)
                    if _zone_status == opcodes.watering:
                        self.config.zones[zone].seconds_zone_ran += 60  # using 60 so that we are in seconds
                    # set flag not all zones are done
                    if _zone_status != opcodes.done_watering and _zone_status != opcodes.error:
                        zones_still_running = True

                        # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------


                # this could say if zones_still_running:
                if zones_still_running:
                    not_done = True
                    self.config.controllers[1].do_increment_clock(minutes=1)
                    self.config.controllers[1].verify_date_and_time()
                else:
                    not_done = False

            for zone in range(1, 6):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(status=opcodes.error)
                self.config.zone_programs[zone].verify_message_on_cn(opcodes.low_flow_variance_shutdown)
                self.config.zone_programs[zone].clear_message_on_cn(opcodes.low_flow_variance_shutdown)
                # check to see if each zone shut down in the correct amount of time which is pipe fill plus 1 minute
            seconds_before_error = (self.config.mainlines[1].ft * 60) + 60
            for zone in range(1, 6):
                if seconds_before_error != self.config.zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to fail with a low flow variance " + str(seconds_before_error) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to fail with a low flow variance")
            for zone in range(6, 11):
                # self.config.zones[zone].get_data()
                # self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
                self.config.zones[zone].get_data_and_verify_status_on_cn(status=opcodes.done_watering)
                self.config.zone_programs[zone].verify_message_on_cn(opcodes.low_flow_variance_detected)
                self.config.zone_programs[zone].clear_message_on_cn(opcodes.low_flow_variance_detected)
            for zone in range(6, 11):
                if self.config.zone_programs[zone].rt != self.config.zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to fail with a low flow variance " + str(
                        self.config.zone_programs[zone].rt * 60) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to fail with a low flow variance")
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_12(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))