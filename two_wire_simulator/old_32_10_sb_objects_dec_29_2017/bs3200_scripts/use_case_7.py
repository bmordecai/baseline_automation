import sys
from time import sleep

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_3200 import StartConditionFor3200, \
                                                                StopConditionFor3200, \
                                                                PauseConditionFor3200

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# this import allows us to directly use the date_mngr
from datetime import time

__author__ = 'Turu'


class ControllerUseCase7(object):
    """
    Test name:
        - Multiple Start Stop Pause on event decoder
    purpose:
        - this test was create for to verify a customer configuration that want to use the 3200 for a fire suppression\n
            system
        - confirming that one event decoder can have a start, stop, condition on multiple programs
        - one SSP condition can:
            - stop multiple programs at once
            - start a program
    Coverage area: \n
        This test covers using an event switch to set and activate SSP conditions on multiple programs at once \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n
        - setting programs: \n
            - start times
            - mainlines

        - attaching zones to program \n

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow\n
                - high variance limit
                - high variance shut down \n
                - low variance limit
                - low variance shut down \n

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n

        - turning off multiple programs at once
        - starting a program with the same event decoder state that stopped another program
        - checking when programs turn on they obey zone concurrency
    """
    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        Step 1:
            - configure controller:
                - initiate controller to a known state so that it doesnt have a configuration or any devices loaded
                - turn on echo so the commands are displayed in the console
                - turn on sim mode so the clock can be stopped
                - stop the clock
                - Set the date and time so that the controller is in a known state
                - turn on faux IO
                - clear all devices
                - clear all programming
            - configure BaseManager: \n
                - verify the controller is connected to basemanager \n
            - set zone concurrency to 5 \n

        step 2:
            - setting up devices:
                - Loading devices into controller from JSON
                - Searching for devices so that they can be addressed
                - Address zones and master valves
                - Set all devices:
                    - descriptions
                    - locations
                    - default parameters
        Step 3:
            - setting up programming:
                - set_up_programs
                - give each program a start time of 8:00 A.M. \n
        step 4:
            - setup start/stop/pause conditions: \n
                - make programs 1-10 stop when any of the 4 event switches is on \n
                - programs 96, 97, 98, and 99 are started by event switches 1, 2, 3, and 4 respectively
        Step 5:
            - assign zones to programs
                - programs 1-10 have two zones each
                - programs 96, 97, 98, and 99 have 4 zones each
        Step 6:
            - setting up mainlines: \n
                - set up main line 1 \n
                    set the target flow to 500 \n
                \n
        Step 7:
            - setting up POCs: \n
                - set up POC 1 \n
                    - enable POC 1 \n
                    - assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
                    - assign POC 1 a target flow of 500 \n
                    - assign POC 1 to main line 1 \n
        Step 8:
            - verify the full configuration \n
        Step 9:
            - set the time to 7:59:00 \n
            - verify all zones have status done watering \n
            - verify all programs have status done watering \n
        Step 10:
            - set the time to 8:10:00 \n
            - verify zones 1-5 watering \n
            - verify programs 1-3 have running \n
            - verify programs 4-10 are waiting to water \n
        Step 11:
            - open event switch 1 \n
            - verify only program 96 is running \n
            - verify only zones 81-84 are watering \n
        Step 12:
            - open event switch 2 \n
            - verify only program 97 is running \n
            - verify only zones 85-88 are watering \n
        Step 13:
            - open event switch 3 \n
            - verify only program 98 is running \n
            - verify only zones 89-92 are watering \n
        Step 14:
            - open event switch 4 \n
            - verify only program 99 is running \n
            - verify only zones 93-96 are watering \n
        Step 15:
            - verify the full configuration again \n
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - sets up the controller \n
        - verify BaseManager connection
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # self.config.basemanager_connection[1].verify_ip_address_state()

            # this is used to give the serial port a long timeout
            self.config.controllers[1].set_serial_port_timeout(timeout=1200)

            # this allows no more than 5 controllers to be watering at the same time
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=5)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        # # load all devices need into the controller so that they are available for use in the configuration
        try:
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                             ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_3(self):
        """
        set up our PG3200 objects
        set_up_programs
            Program 1-10, 96, 97, 98, 99:
                - start time 8:00 am
                - mainline 1
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        program_8am_start_time = [480]
        program_no_start_time = ['']

        try:
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _st=program_8am_start_time,
                                             _ml=1,
                                             _mc=2)
            self.config.programs[2] = PG3200(_ad=2,
                                             _en=opcodes.true,
                                             _st=program_8am_start_time,
                                             _ml=1,
                                             _mc=2)
            self.config.programs[3] = PG3200(_ad=3,
                                             _en=opcodes.true,
                                             _st=program_8am_start_time,
                                             _ml=1,
                                             _mc=2)
            self.config.programs[4] = PG3200(_ad=4,
                                             _en=opcodes.true,
                                             _st=program_8am_start_time,
                                             _ml=1,
                                             _mc=2)
            self.config.programs[5] = PG3200(_ad=5,
                                             _en=opcodes.true,
                                             _st=program_8am_start_time,
                                             _ml=1,
                                             _mc=2)
            self.config.programs[6] = PG3200(_ad=6,
                                             _en=opcodes.true,
                                             _st=program_8am_start_time,
                                             _ml=1,
                                             _mc=2)
            self.config.programs[7] = PG3200(_ad=7,
                                             _en=opcodes.true,
                                             _st=program_8am_start_time,
                                             _ml=1,
                                             _mc=2)
            self.config.programs[8] = PG3200(_ad=8,
                                             _en=opcodes.true,
                                             _st=program_8am_start_time,
                                             _ml=1,
                                             _mc=2)
            self.config.programs[9] = PG3200(_ad=9,
                                             _en=opcodes.true,
                                             _st=program_8am_start_time,
                                             _ml=1,
                                             _mc=2)
            self.config.programs[10] = PG3200(_ad=10,
                                              _en=opcodes.true,
                                              _st=program_8am_start_time,
                                              _ml=1,
                                              _mc=2)
            self.config.programs[96] = PG3200(_ad=96,
                                              _en=opcodes.true,
                                              _st=program_no_start_time,
                                              _ml=1,
                                              _mc=2)
            self.config.programs[97] = PG3200(_ad=97,
                                              _en=opcodes.true,
                                              _st=program_no_start_time,
                                              _ml=1,
                                              _mc=2)
            self.config.programs[98] = PG3200(_ad=98,
                                              _en=opcodes.true,
                                              _st=program_no_start_time,
                                              _ml=1,
                                              _mc=2)
            self.config.programs[99] = PG3200(_ad=99,
                                              _en=opcodes.true,
                                              _st=program_no_start_time,
                                              _ml=1,
                                              _mc=2)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_4(self):
        """
        setup start/stop/pause conditions
            - programs 1-10 should stop when any of the 4 event switches opens
            - program 96 should start when SW1 opens, and close on the other 3
            - program 97 should start when SW2 opens, and close on the other 3
            - program 98 should start when SW3 opens, and close on the other 3
            - program 99 should start when SW4 opens, and close on the other 3
        """
        # this is set in the PG3200 object
        # TODO need to have concurrent zones per program added
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            condition_counter = 1
            # Loops through the first 10 programs and sets them to stop whenever an event switch 1-4 is on
            for i in range(1, 11):
                self.config.program_stop_conditions[condition_counter] = StopConditionFor3200(program_ad=i)
                self.config.program_stop_conditions[condition_counter].set_event_switch_condition_on_pg(
                    serial_number=self.config.event_switches[1].sn, mode=opcodes.open)
                self.config.program_stop_conditions[condition_counter+1] = StopConditionFor3200(program_ad=i)
                self.config.program_stop_conditions[condition_counter+1].set_event_switch_condition_on_pg(
                    serial_number=self.config.event_switches[2].sn, mode=opcodes.open)
                self.config.program_stop_conditions[condition_counter+2] = StopConditionFor3200(program_ad=i)
                self.config.program_stop_conditions[condition_counter+2].set_event_switch_condition_on_pg(
                    serial_number=self.config.event_switches[3].sn, mode=opcodes.open)
                self.config.program_stop_conditions[condition_counter+3] = StopConditionFor3200(program_ad=i)
                self.config.program_stop_conditions[condition_counter+3].set_event_switch_condition_on_pg(
                    serial_number=self.config.event_switches[4].sn, mode=opcodes.open)
                # We increment by 4 because we are adding 4 conditions each time we go through the loop
                condition_counter += 4

            # Start/Stop/Pause conditions for program 96, started by SW[1] and stopped by SW[2-4]
            # We start at 41 because that is where condition counter will end after assigning 4 switch events to 10 programs
            self.config.program_stop_conditions[41] = StopConditionFor3200(program_ad=96)
            self.config.program_stop_conditions[41].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[2].sn, mode=opcodes.open)
            self.config.program_stop_conditions[42] = StopConditionFor3200(program_ad=96)
            self.config.program_stop_conditions[42].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[3].sn, mode=opcodes.open)
            self.config.program_stop_conditions[43] = StopConditionFor3200(program_ad=96)
            self.config.program_stop_conditions[43].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[4].sn, mode=opcodes.open)

            self.config.program_start_conditions[1] = StartConditionFor3200(program_ad=96)
            self.config.program_start_conditions[1].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.open)

            # Start/Stop/Pause conditions for program 97, started by SW[2] and stopped by SW[3,4]
            self.config.program_stop_conditions[45] = StopConditionFor3200(program_ad=97)
            self.config.program_stop_conditions[45].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[3].sn, mode=opcodes.open)
            self.config.program_stop_conditions[46] = StopConditionFor3200(program_ad=97)
            self.config.program_stop_conditions[46].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[4].sn, mode=opcodes.open)

            self.config.program_start_conditions[97] = StartConditionFor3200(program_ad=97)
            self.config.program_start_conditions[97].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[2].sn, mode=opcodes.open)

            # Start/Stop/Pause conditions for program 98, started by SW[3] and stopped by SW[4]
            self.config.program_stop_conditions[49] = StopConditionFor3200(program_ad=98)
            self.config.program_stop_conditions[49].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[4].sn, mode=opcodes.open)

            self.config.program_start_conditions[2] = StartConditionFor3200(program_ad=98)
            self.config.program_start_conditions[2].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[3].sn, mode=opcodes.open)

            # Start/Stop/Pause conditions for program 99, started by SW[4] and not stopped by any switch
            self.config.program_start_conditions[3] = StartConditionFor3200(program_ad=99)
            self.config.program_start_conditions[3].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[4].sn, mode=opcodes.open)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_5(self):
        """
        Zone Programs
            - assign 2 zones to programs 1-10 \n
                - give each zone a run time of 1 hour and 30 minutes \n
            - assign 4 zones to programs 96, 97, 98, 99 \n
                - give each zone a run time of 1 hour and 30 minutes \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            # Programs 1-10 have 2 zones each
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900)
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=900)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=900)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[3],
                                                       _rt=900)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[6],
                                                       prog_obj=self.config.programs[3],
                                                       _rt=900)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[7],
                                                       prog_obj=self.config.programs[4],
                                                       _rt=900)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[8],
                                                       prog_obj=self.config.programs[4],
                                                       _rt=900)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[9],
                                                       prog_obj=self.config.programs[5],
                                                       _rt=900)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[10],
                                                       prog_obj=self.config.programs[5],
                                                       _rt=900)
            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[11],
                                                       prog_obj=self.config.programs[6],
                                                       _rt=900)
            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[12],
                                                       prog_obj=self.config.programs[6],
                                                       _rt=900)
            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[13],
                                                       prog_obj=self.config.programs[7],
                                                       _rt=900)
            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[14],
                                                       prog_obj=self.config.programs[7],
                                                       _rt=900)
            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[15],
                                                       prog_obj=self.config.programs[8],
                                                       _rt=900)
            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[16],
                                                       prog_obj=self.config.programs[8],
                                                       _rt=900)
            self.config.zone_programs[9] = ZoneProgram(zone_obj=self.config.zones[17],
                                                       prog_obj=self.config.programs[9],
                                                       _rt=900)
            self.config.zone_programs[9] = ZoneProgram(zone_obj=self.config.zones[18],
                                                       prog_obj=self.config.programs[9],
                                                       _rt=900)
            self.config.zone_programs[10] = ZoneProgram(zone_obj=self.config.zones[19],
                                                       prog_obj=self.config.programs[10],
                                                       _rt=900)
            self.config.zone_programs[10] = ZoneProgram(zone_obj=self.config.zones[20],
                                                       prog_obj=self.config.programs[10],
                                                       _rt=900)

            # Program 96 has 4 zones (81-84)
            self.config.zone_programs[96] = ZoneProgram(zone_obj=self.config.zones[81],
                                                       prog_obj=self.config.programs[96],
                                                       _rt=900)
            self.config.zone_programs[96] = ZoneProgram(zone_obj=self.config.zones[82],
                                                       prog_obj=self.config.programs[96],
                                                       _rt=900)
            self.config.zone_programs[96] = ZoneProgram(zone_obj=self.config.zones[83],
                                                       prog_obj=self.config.programs[96],
                                                       _rt=900)
            self.config.zone_programs[96] = ZoneProgram(zone_obj=self.config.zones[84],
                                                       prog_obj=self.config.programs[96],
                                                       _rt=900)

            # Program 97 has 4 zones (85-88)
            self.config.zone_programs[97] = ZoneProgram(zone_obj=self.config.zones[85],
                                                        prog_obj=self.config.programs[97],
                                                        _rt=900)
            self.config.zone_programs[97] = ZoneProgram(zone_obj=self.config.zones[86],
                                                        prog_obj=self.config.programs[97],
                                                        _rt=900)
            self.config.zone_programs[97] = ZoneProgram(zone_obj=self.config.zones[87],
                                                        prog_obj=self.config.programs[97],
                                                        _rt=900)
            self.config.zone_programs[97] = ZoneProgram(zone_obj=self.config.zones[88],
                                                        prog_obj=self.config.programs[97],
                                                        _rt=900)

            # Program 98 has 4 zones (89-92)
            self.config.zone_programs[98] = ZoneProgram(zone_obj=self.config.zones[89],
                                                        prog_obj=self.config.programs[98],
                                                        _rt=900)
            self.config.zone_programs[98] = ZoneProgram(zone_obj=self.config.zones[90],
                                                        prog_obj=self.config.programs[98],
                                                        _rt=900)
            self.config.zone_programs[98] = ZoneProgram(zone_obj=self.config.zones[91],
                                                        prog_obj=self.config.programs[98],
                                                        _rt=900)
            self.config.zone_programs[98] = ZoneProgram(zone_obj=self.config.zones[92],
                                                        prog_obj=self.config.programs[98],
                                                        _rt=900)

            # Program 99 has 4 zones (93-96)
            self.config.zone_programs[99] = ZoneProgram(zone_obj=self.config.zones[93],
                                                        prog_obj=self.config.programs[99],
                                                        _rt=900)
            self.config.zone_programs[99] = ZoneProgram(zone_obj=self.config.zones[94],
                                                        prog_obj=self.config.programs[99],
                                                        _rt=900)
            self.config.zone_programs[99] = ZoneProgram(zone_obj=self.config.zones[95],
                                                        prog_obj=self.config.programs[99],
                                                        _rt=900)
            self.config.zone_programs[99] = ZoneProgram(zone_obj=self.config.zones[96],
                                                        prog_obj=self.config.programs[99],
                                                        _rt=900)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_6(self):
        """
        set_mainlines_3200
        set up main line 1 \n
            set the target flow to 500 \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        try:
            self.config.mainlines[1] = Mainline(_ad=1,
                                                _fl=500)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_7(self):
        """
        set_poc_3200
        set up POC 1 \n
            enable POC 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            assign POC 1 a target flow of 500 \n
            assign POC 1 to main line 1 \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1] = POC3200(
                _ad=1,
                _en=opcodes.true,
                _mv=1,
                _fm=1,
                _fl=500,
                _ml=1
            )

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_8(self):
        """
        Verify the entire configuration
        :return:
        :rtype:
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            # time.sleep(20)
            self.config.verify_full_configuration()
            # this turns off fast sim mode and start the clock
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_9(self):
        """
        Set the time to 7:56:00 \n
        Increment the clock by 3 minutes to 7:59:00 \n
        Verify the status for each program and zone on the controller \n
        - Program status: \n
            - program 1-10 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - all zones done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_date_and_time_on_cn(_date='08/28/2014', _time='7:56:00')
            self.config.controllers[1].verify_date_and_time()
            self.config.controllers[1].do_increment_clock(minutes=3)
            self.config.controllers[1].verify_date_and_time()
            # For now, the zones are expected to have the same status
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            for program in range(1,11):
                self.config.programs[program].get_data()
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)

            # These 4 programs will differ in future steps so we don't have them in a loop
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_10(self):
        """
        Increment the clock by 2 minutes to 8:01:00 \n
        Programs 1- 10 have a start time of 8:00am  programs 96-99 don't have a start time
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - programs 1, 2, 3 running \n
            - programs 4-10 waiting \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-5 watering \n
            - zones 6-20 waiting \n
            - zones 81-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=2)
            self.config.controllers[1].verify_date_and_time()

            for zone in range(1, 6):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(6, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in range(81, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            # Programs 1-3 are running since each program holds 2 zones and the concurrency is 5
            for program in range(1, 4):
                self.config.programs[program].get_data()
                self.config.programs[program].verify_status_on_cn(opcodes.running)
            for program in range(4, 11):
                self.config.programs[program].get_data()
                self.config.programs[program].verify_status_on_cn(opcodes.waiting_to_water)

            # These 4 programs will differ in future steps so we don't have them in a loop
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_11(self):
        """
        Increment the clock by 5 minutes to 8:05:00 \n
        Open event switch 1 \n
        The the opening the switch should stop the first 10 programs and start program 96
        you have to increment the clock a minute in order for this to take place this makes the time 8:06am
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1-10 done \n
            - program 96 running \n
            - program 97-99 done \n

        - Zone status: \n
            - zone 1-20 done \n
            - zone 81-82 watering \n
            - zone 83-85 waiting to watering \n
            - zone 85-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.open)
            self.config.event_switches[1].do_self_test()

            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            for zone in range(81, 83):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)

            for zone in range(83, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)

            for zone in range(85, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            for program in range(1, 11):
                self.config.programs[program].get_data()
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.running)

            for program in range(97, 100):
                self.config.programs[program].get_data()
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_12(self):
        """
        Increment the clock by 4 minutes to 8:10:00 \n
        Open event switch 2 \n
        The the opening the switch should stop program 96 programs and start program 97
        you have to increment the clock a minute in order for this to take place this makes the time 8:11am
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1-10 done \n
            - program 96 done \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zone 1-20 done \n
            - zone 81-84 done \n
            - zone 85-86 watering \n
            - zone 87-88  waiting to watering \n
            - zone 89-96 done \n
          """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.event_switches[2].set_contact_state_on_cn(_contact_state=opcodes.open)
            self.config.event_switches[2].do_self_test()

            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            for zone in range(85, 87):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)

            for zone in range(87, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)

            for zone in range(89, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            for program in range(1, 11):
                self.config.programs[program].get_data()
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)

            # These 4 programs will differ in future steps so we don't have them in a loop
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.running)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_13(self):
        """
        Increment the clock by 4 minutes to 8:15:00 \n
        Open event switch 3 \n
        The the opening the switch should stop program 97 programs and start program 98
        you have to increment the clock a minute in order for this to take place this makes the time 8:16am
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1-10 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zone 1-20 done \n
            - zone 81-84 done \n
            - zone 85-88 done \n
            - zone 89-90 watering \n
            - zone 91-92 waiting to watering \n
            - zone 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.event_switches[3].set_contact_state_on_cn(_contact_state=opcodes.open)
            self.config.event_switches[3].do_self_test()

            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1,21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            for zone in range(81, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            for zone in range(89, 91):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)

            for zone in range(91, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)

            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            for program in range(1, 11):
                self.config.programs[program].get_data()
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)

            # These 4 programs will differ in future steps so we don't have them in a loop
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.running)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_14(self):
        """
        Increment the clock by 4 minutes to 8:20:00 \n
        Open event switch 4 \n
        The the opening the switch should stop program 98 programs and start program 99
        you have to increment the clock a minute in order for this to take place this makes the time 8:21am
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1-10 done \n
            - program 96 - 98 done \n
            - program 99 running \n
        - Zone status: \n
            - zone 1-20 done \n
            - zone 81-92 done \n
            - zone 93-94 watering \n
            - zone 95-96 waiting to watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.event_switches[4].set_contact_state_on_cn(_contact_state=opcodes.open)
            self.config.event_switches[4].do_self_test()

            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1,21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            for zone in range(81, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            for zone in range(93, 95):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)

            for zone in range(95, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)

            # These ten programs will always be the same so we have them in a loop together
            for program in range(1, 11):
                self.config.programs[program].get_data()
                self.config.programs[program].verify_status_on_cn(opcodes.done_watering)

            # These 4 programs will differ in future steps so we don't have them in a loop
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.running)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_15(self):
        """
        Verify the entire configuration again
        :return:
        :rtype:
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            # time.sleep(20)
            self.config.verify_full_configuration()
            # this turns off fast sim mode and start the clock
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

