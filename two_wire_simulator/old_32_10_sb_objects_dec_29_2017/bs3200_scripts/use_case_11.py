from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# Browser pages used
import page_factory

__author__ = 'Tige'


class ControllerUseCase11(object):
    """
    Test name:
        - CN UseCase 11 firmware update
    purpose:
        - set up a full configuration on the controller
            - verify that no programming is lost when you:
                - update the firmware of the controller
            - verify that firmware update take effects:
                - verify all programing even with certain devices or attributes are disabled

    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n

        - setting programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign programs to water sources \n
            - assign zones to programs \n
            - set up primary linked zones \n
            - give each zone a run time of 1 hour and 30 minutes \n
            - assign moisture sensors to a primary zone 1 and set to lower limit watering

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow\n
                - high variance limit
                - high variance shut down \n
                - low variance limit
                - low variance shut down \n

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n
                - priority
                    - low
                    - medium
                    - high\n
            - set high flow limit
            - high flow shut down \n
            - set unscheduled flow limit
            - enable unscheduled flow shut down \n
            - set water budget
            - enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions
                - event switch\n
                - set switch empty condition to closed \n
                - set empty wait time\n

            - disable
                - all one of each device type \n
                - program
                - mainline
                - poc
            - firmware update from both basemanager and usb: \n
            - verify message
            - verify configuration all stayed \n
    Date References:
        - configuration for script is located common\configuration_files\back_restore_firmware_update.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        Step 1:
            - configure controller:
                -  initiate controller to a known state so that it doesnt have a configuration or any devices loaded
                - turn on echo so the commands are displayed in the console
                - turn on sim mode so the clock can be stopped
                - stop the clock
                - Set the date and time so  that the controller is in a known state
                - turn on faux IO
                - clear all devices
                - clear all programming

            - configure basemanager: \n
                - verify the controller is connected to basemanager \n

        step 2:
            - setting up devices:
                - Loading devices into controller
                - Searching for devices so that they can be addressed
                - Address zones and master valves
                - Set all devices:
                    - descriptions
                    - locations
                    - default parameters
        Step 3:
            - setting up programming:
                - set_up_programs
                - assign zones to programs \n
                - set up primary linked zones \n
                - give each zone a run time of 1 hour and 30 minutes \n
                - give each program a start time of 8:00 A.M. \n
        step 4:
            - setup zone programs: \n
                - must make zone 200 a primary zone before you can link zones to it \n
                - assign sensor to primary zone 1
                - assign moisture sensors to a primary zone 1 and set to lower limit watering
        Step 5:
            - setting up mainlines: \n
                - set up main line 1 \n
                    set limit zones by flow to true \n
                    set the pipe fill time to 4 minutes \n
                    set the target flow to 500 \n
                    set the high variance limit to 5% and enable the high variance shut down \n
                    set the low variance limit to 20% and enable the low variance shut down \n
                \n
                - set up main line 8 \n
                    set limit zones by flow to true \n
                    set the pipe fill time to 1 minute \n
                    set the target flow to 50 \n
                    set the high variance limit to 20% and disable the high variance shut down \n
                    set the low variance limit to 5% and disable the low variance shut down \n
        Step 6:
            - setting up POCs: \n
                - set up POC 1 \n
                    - enable POC 1 \n
                    - assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
                    - assign POC 1 a target flow of 500 \n
                    - assign POC 1 to main line 1 \n
                    - set POC priority to 2-medium \n
                    - set high flow limit to 550 and enable high flow shut down \n
                    - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                    - set water budget to 100000 and enable the water budget shut down \n
                    - enable water rationing \n
                \n
                - set up POC 8 \n
                    - enable POC 8 \n
                    - assign master valve TMV0004 and flow meter TWF0004 to POC 8 \n
                    - assign POC 8 a target flow of 50 \n
                    - assign POC 8 to main line 8 \n
                    - set POC priority to 3-low \n
                    - set high flow limit to 75 and disable high flow shut down \n
                    - set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
                    - set water budget to 1000 and disable water budget shut down \n
                    - disable water rationing \n
                    - assign event switch TPD0001 to POC 8 \n
                    - set switch empty condition to closed \n
                    - set empty wait time to 540 minutes \n

        Step 7:
            - disable able devices:
        Step 8 :
            - update firmware from basemanager:
            - verify message
        step 9:
            - update all objects
        step 10:
            - verify that all attributes for each device did not change
            - because the clock was increment we can verify status and verify that the zone didn't start \n
        Step 11 :
            - update firmware from usb:
            - verify message
        step 12:
            - update all objects
        step 13:
            - verify that all attributes for each device did not change
            - because the clock was increment we can verify status and verify that the zone didn't start \n
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    # self.step_8()
                    # self.step_9()
                    # self.step_10()
                    # self.step_11()
                    self.step_12()
                    self.step_13()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - sets up the controller \n
        - verify basemanager connection
        """
        self.config.controllers[1].init_cn()

        # only need this for BaseManager
        # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock for
        # self.config.basemanager_connection[1].verify_ip_address_state()

    def step_2(self):
        # """
        # - sets the devices that will be used in the configuration of the controller \n
        # - search and address the devices:
        #     - zones                 {zn}
        #     - Master Valves         {mv}
        #     - Moisture Sensors      {ms}
        #     - Temperature Sensors   {ts}
        #     - Event Switches        {sw}
        #     - Flow Meter            {fm}
        # - once the devices are found they can be addressed so that they can be used in the programming
        #     - zones can use addresses {1-200}
        #     - Master Valves can use address {1-8}
        # - the 3200 auto address certain devices in the order it receives them:
        #     - Master Valves         {mv}
        #     - Moisture Sensors      {ms}
        #     - Temperature Sensors   {ts}
        #     - Event Switches        {sw}
        #     - Flow Meter            {fm}
        # """
        # # load all devices need into the controller so that they are available for use in the configuration
        self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                     mv_d1_list=self.config.mv_d1,
                                                     d2_list=self.config.d2,
                                                     mv_d2_list=self.config.mv_d2,
                                                     d4_list=self.config.d4,
                                                     dd_list=self.config.dd,
                                                     ms_list=self.config.ms,
                                                     fm_list=self.config.fm,
                                                     ts_list=self.config.ts,
                                                     sw_list=self.config.sw)

        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
        # assign zones an address between 1-200
        self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                         zn_ad_range=self.config.zn_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
        self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                         mv_ad_range=self.config.mv_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                         ms_ad_range=self.config.ms_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                         ts_ad_range=self.config.ts_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
        self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                         sw_ad_range=self.config.sw_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
        self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                         fm_ad_range=self.config.fm_ad_range)
        self.config.create_mainline_objects()
        self.config.create_3200_poc_objects()

    def step_3(self):
        """
        set_up_programs
        assign zones to programs \n
        set up primary linked zones \n
        give each zone a run time of 1 hour and 30 minutes \n
        give each program a start time of 8:00 A.M. \n
        must make zone 200 a primary zone before you can link zones to it \n
        assign sensor to primary zone 1
        #assign moisture sensors to a primary zone 1 and set to lower limit watering
        """
        # this is set in the PG3200 object
        # TODO need to have concurrent zones per program added

        program_number_1_start_times = [480, 540, 600, 660]
        program_number_3_start_times = [480, 540, 600, 660]
        program_number_4_start_times = [480, 540, 600, 660]
        program_number_99_start_times = [480, 540, 600, 660]
        program_number_1_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
        program_number_3_watering_days = []
        program_number_4_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
        program_number_99_watering_days = [0, 0, 0, 0, 0, 0, 8, 8, 6, 5, 5, 4, 3, 3, 3, 3, 4, 5, 6, 7, 0, 0, 0, 0]
        program_number_1_water_windows = ['011111100001111111111110']
        program_number_3_water_windows = ['011111100001111111111110']
        program_number_4_water_windows = ['011111100001111111111110']
        program_number_99_water_windows = ['011111100000111111111110',
                                           '011111100001111111111111',
                                           '011111100001111111111110',
                                           '011111100001111111111110',
                                           '011111100001111111111110',
                                           '011111100001111111111110',
                                           '011111100001111111111111']
        self.config.programs[1] = PG3200(_ad=1,
                                         _en=opcodes.true,
                                         _ww=program_number_1_water_windows,
                                         _pr=1,
                                         _mc=1,
                                         _sa=100,
                                         _ci=opcodes.week_days,
                                         _di=None,
                                         _wd=program_number_1_watering_days,
                                         _sm=[],
                                         _st=program_number_1_start_times,
                                         _ml=1,
                                         _bp='')
        self.config.programs[3] = PG3200(_ad=3,
                                         _en=opcodes.true,
                                         _ww=program_number_3_water_windows,
                                         _pr=1,
                                         _mc=1,
                                         _sa=100,
                                         _ci=opcodes.odd_day,
                                         _di=None,
                                         _wd=program_number_3_watering_days,
                                         _sm=[],
                                         _st=program_number_3_start_times,
                                         _ml=2,
                                         _bp='')
        self.config.programs[4] = PG3200(_ad=4,
                                         _en=opcodes.true,
                                         _ww=program_number_4_water_windows,
                                         _pr=3,
                                         _mc=4,
                                         _sa=100,
                                         _ci=opcodes.week_days,
                                         _di=None,
                                         _wd=program_number_4_watering_days,
                                         _sm=[],
                                         _st=program_number_4_start_times,
                                         _ml=3,
                                         _bp='')
        self.config.programs[99] = PG3200(_ad=99,
                                          _en=opcodes.true,
                                          _ww=program_number_99_water_windows,
                                          _pr=1,
                                          _mc=1,
                                          _sa=100,
                                          _ci=opcodes.historical_calendar,
                                          _di=None,
                                          _wd=[],
                                          _sm=program_number_99_watering_days,
                                          _st=program_number_99_start_times,
                                          _ml=8,
                                          _bp='')

    def step_4(self):

        # Zone Programs
        self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                   prog_obj=self.config.programs[1],
                                                   _rt=900,
                                                   _ct=300,
                                                   _so=300,
                                                   _pz=1)
        self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                   prog_obj=self.config.programs[1],
                                                   _rt=self.config.zone_programs[1].rt,
                                                   _ct=self.config.zone_programs[1].ct,
                                                   _so=self.config.zone_programs[1].so,
                                                   _ra=100,
                                                   _pz=1)
        self.config.zone_programs[49] = ZoneProgram(zone_obj=self.config.zones[49],
                                                    prog_obj=self.config.programs[3],
                                                    _rt=1200,
                                                    _ct=600,
                                                    _so=3600,
                                                    _ws=opcodes.timed)
        self.config.zone_programs[50] = ZoneProgram(zone_obj=self.config.zones[50],
                                                    prog_obj=self.config.programs[3],
                                                    _rt=1200,
                                                    _ct=600,
                                                    _so=3600,
                                                    _ws=opcodes.timed)
        self.config.zone_programs[50] = ZoneProgram(zone_obj=self.config.zones[50],
                                                    prog_obj=self.config.programs[4],
                                                    _rt=1200,
                                                    _ct=600,
                                                    _so=3600,
                                                    _ws=opcodes.timed)

        self.config.zone_programs[200] = ZoneProgram(zone_obj=self.config.zones[200],
                                                     prog_obj=self.config.programs[99],
                                                     _rt=1980,
                                                     _pz=200,
                                                     _ct=180,
                                                     _so=780)

        # Zone programs linked to Zone 200
        self.config.zone_programs[197] = ZoneProgram(zone_obj=self.config.zones[197],
                                                     prog_obj=self.config.programs[99],
                                                     _rt=self.config.zone_programs[200].rt,
                                                     _ct=self.config.zone_programs[200].ct,
                                                     _so=self.config.zone_programs[200].so,
                                                     _pz=200,
                                                     _ra=50)
        self.config.zone_programs[198] = ZoneProgram(zone_obj=self.config.zones[198],
                                                     prog_obj=self.config.programs[99],
                                                     _rt=self.config.zone_programs[200].rt,
                                                     _ct=self.config.zone_programs[200].ct,
                                                     _so=self.config.zone_programs[200].so,
                                                     _pz=200,
                                                     _ra=100)
        self.config.zone_programs[199] = ZoneProgram(zone_obj=self.config.zones[199],
                                                     prog_obj=self.config.programs[99],
                                                     _rt=self.config.zone_programs[200].rt,
                                                     _ct=self.config.zone_programs[200].ct,
                                                     _so=self.config.zone_programs[200].so,
                                                     _pz=200,
                                                     _ra=150)

    def step_5(self):
        """
        set_mainlines_3200
        set up main line 1 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 8 \n
            set limit zones by flow to true \n
            set the pipe fill time to 1 minute \n
            set the target flow to 50 \n
            set the high variance limit to 20% and disable the high variance shut down \n
            set the low variance limit to 5% and disable the low variance shut down \n
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        self.config.mainlines[1] = Mainline(_ad=1,
                                            _ft=4,
                                            _fl=500,
                                            _lc=opcodes.true,
                                            _hv=5,
                                            _hs=opcodes.true,
                                            _lv=20,
                                            _ls=opcodes.true)
        self.config.mainlines[8] = Mainline(_ad=8,
                                            _ft=1,
                                            _fl=50,
                                            _lc=opcodes.true,
                                            _hv=20,
                                            _hs=opcodes.false,
                                            _lv=5,
                                            _ls=opcodes.false)

    def step_6(self):
        """
        set_poc_3200
        set up POC 1 \n
            enable POC 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            assign POC 1 a target flow of 500 \n
            assign POC 1 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 8 \n
            enable POC 8 \n
            assign master valve TMV0004 and flow meter TWF0004 to POC 8 \n
            assign POC 8 a target flow of 50 \n
            assign POC 8 to main line 8 \n
            set POC priority to 3-low \n
            set high flow limit to 75 and disable high flow shut down \n
            set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
            set water budget to 1000 and disable water budget shut down \n
            disable water rationing \n
            assign event switch TPD0001 to POC 8 \n
            set switch empty condition to closed \n
            set empty wait time to 540 minutes \n
        """
        self.config.poc[1] = POC3200(
            _ad=1,
            _en=opcodes.true,
            _mv=1,
            _fm=1,
            _fl=500,
            _ml=1,
            _pr=2,
            _hf=550,
            _hs=opcodes.true,
            _uf=10,
            _us=opcodes.true,
            _wb=100000,
            _ws=opcodes.true,
            _wr=opcodes.true
        )

        self.config.poc[8] = POC3200(
            _ad=8,
            _en=opcodes.true,
            _mv=1,
            _fm=1,
            _fl=50,
            _ml=8,
            _pr=3,
            _hf=75,
            _hs=opcodes.false,
            _uf=5,
            _us=opcodes.false,
            _wb=1000,
            _ws=opcodes.false,
            _wr=opcodes.false,
            _sw=1,
            _se=opcodes.closed,
            _ew=540
        )

    def step_7(self):
        """
        Disable devices
        This area covers not losing programing when a device is disabled during a firmware update or reboot
        """
        # disable Zone (198}
        self.config.zones[198].set_enable_state_on_cn("FA")

        # disable flow meter {3}
        self.config.flow_meters[1].set_enable_state_on_cn("FA")

    def step_8(self):
        """
        We do a firmware update from BaseManager.
        """
        # Updates the firmware to 12.31.418, to update to 12.31.417 set the 'bm_id_number to 121
        self.config.controllers[1].do_firmware_update(were_from=opcodes.basemanager, bm_id_number=122)
        # TODO do we need to reconnect to BaseManager after update?
        self.config.controllers[1].stop_clock()
        self.config.controllers[1].verify_message_on_cn(_status_code=opcodes.restore_successful,
                                                        _helper_object=opcodes.basemanager_source)

    def step_9(self):
        """
        After the reboot all of the devices get set to default values in the controller so you have to do a self test
        and than do an update to get each object to match what the controller has.
            - This doesn't update every value in our objects, only certain ones. Usually things like solenoid current,
              two-wire drop, etc.
        Perform a self test on all zones to verify that they are functioning properly \n
        """
        for zone in self.config.zn_ad_range:
            self.config.zones[zone].self_test_and_update_object_attributes()
        for moisture_sensors in self.config.ms_ad_range:
            self.config.moisture_sensors[moisture_sensors].self_test_and_update_object_attributes()
        for master_valves in self.config.mv_ad_range:
            self.config.master_valves[master_valves].self_test_and_update_object_attributes()
        for flow_meters in self.config.fm_ad_range:
            self.config.flow_meters[flow_meters].self_test_and_update_object_attributes()
        for event_switches in self.config.sw_ad_range:
            self.config.event_switches[event_switches].self_test_and_update_object_attributes()
        for temperature_sensors in self.config.ts_ad_range:
            self.config.temperature_sensors[temperature_sensors].self_test_and_update_object_attributes()

    def step_10(self):
        """
        Verify that all the values on our objects are still matching the values in the controller. This includes
        the firmware version.
        """
        # this also verifies firmware version
        self.config.verify_full_configuration()

    def step_11(self):
        """
        Do a firmware update from a USB that will have an older version of firmware.
        :return:
        :rtype:
        """
        self.config.controllers[1].do_firmware_update(were_from=opcodes.usb_flash_storage, file_name='Update')
        self.config.controllers[1].stop_clock()
        self.config.controllers[1].verify_message_on_cn(opcodes.restore_successful, opcodes.usb_flash_storage_source)

    def step_12(self):
        """
        After the reboot all of the devices get set to default values in the controller so you have to do a self test
        and than do an update to get each object to match what the controller has.
            - This doesn't update every value in our objects, only certain ones. Usually things like solenoid current,
              two-wire drop, etc.
        Perform a test on all zones to verify that they are functioning properly \n
        """
        self.config.controllers[1].do_increment_clock(minutes=5)
        self.config.controllers[1].do_reboot_controller()
        self.config.controllers[1].stop_clock()
        for zone in self.config.zn_ad_range:
            self.config.zones[zone].self_test_and_update_object_attributes()
        for moisture_sensors in self.config.ms_ad_range:
            self.config.moisture_sensors[moisture_sensors].self_test_and_update_object_attributes()
        for master_valves in self.config.mv_ad_range:
            self.config.master_valves[master_valves].self_test_and_update_object_attributes()
        for flow_meters in self.config.fm_ad_range:
            self.config.flow_meters[flow_meters].self_test_and_update_object_attributes()
        for event_switches in self.config.sw_ad_range:
            self.config.event_switches[event_switches].self_test_and_update_object_attributes()
        for temperature_sensors in self.config.ts_ad_range:
            self.config.temperature_sensors[temperature_sensors].self_test_and_update_object_attributes()

    def step_13(self):
        """
        Verifies all of our object's variables are matching their controller counterparts
        """
        # this also verifies firmware version
        self.config.verify_full_configuration()




