import sys
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration


# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# Browser pages used
# import page_factory

__author__ = 'Tige'


class ControllerUseCase10(object):
    """
    Test name:
        - CN UseCase10 seasonal adjust
    purpose:
        - set up a full configuration on the controller
            - check tracking ration
            - Check seasonal adjust with tracking ration added
            - adjust the seasonal adjust :
                - verify primary zones run correctly
                - verify linked zones run correctly


    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n

        - setting programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign programs to water sources \n
            - assign zones to programs \n
            - set up primary linked zones \n
            - give each zone a run time of 1 hour and 30 minutes \n
            - assign moisture sensors to a primary zone 1 and set to lower limit watering

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow\n
                - high variance limit
                - high variance shut down \n
                - low variance limit
                - low variance shut down \n

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n
                - priority
                    - low
                    - medium
                    - high\n
            - set high flow limit
            - high flow shut down \n
            - set unscheduled flow limit
            - enable unscheduled flow shut down \n
            - set water budget
            - enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions
                - event switch\n
                - set switch empty condition to closed \n
                - set empty wait time\n


    Date References:
        - configuration for script is located common\configuration_files\EPA_test_configuration.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        Step 1:
            - configure controller:
                -  initiate controller to a known state so that it doesnt have a configuration or any devices loaded
                - turn on echo so the commands are displayed in the console
                - turn on sim mode so the clock can be stopped
                - stop the clock
                - Set the date and time so  that the controller is in a known state
                - turn on faux IO
                - clear all devices
                - clear all programming

            - configure basemanager: \n
                - verify the controller is connected to basemanager \n

        step 2:
            - setting up devices:
                - Loading devices into controller
                - Searching for devices so that they can be addressed
                - Address zones and master valves
                - Set all devices:
                    - descriptions
                    - locations
                    - default parameters
        Step 3:
            - setting up programming:
                - set_up_programs
                - assign zones to programs \n
                - set up primary linked zones \n
                - give each zone a run time of 1 hour and 30 minutes \n
                - give each program a start time of 8:00 A.M. \n
        step 4:
            - setup zone programs: \n
                - must make zone 200 a primary zone before you can link zones to it \n
                - assign sensor to primary zone 1
                - assign moisture sensors to a primary zone 1 and set to lower limit watering
        Step 5:
            - setting up mainlines: \n
                - set up main line 1 \n
                    set limit zones by flow to true \n
                    set the pipe fill time to 4 minutes \n
                    set the target flow to 500 \n
                    set the high variance limit to 5% and enable the high variance shut down \n
                    set the low variance limit to 20% and enable the low variance shut down \n
                \n
                - set up main line 8 \n
                    set limit zones by flow to true \n
                    set the pipe fill time to 1 minute \n
                    set the target flow to 50 \n
                    set the high variance limit to 20% and disable the high variance shut down \n
                    set the low variance limit to 5% and disable the low variance shut down \n
        Step 6:
            - setting up POCs: \n
                - set up POC 1 \n
                    - enable POC 1 \n
                    - assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
                    - assign POC 1 a target flow of 500 \n
                    - assign POC 1 to main line 1 \n
                    - set POC priority to 2-medium \n
                    - set high flow limit to 550 and enable high flow shut down \n
                    - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                    - set water budget to 100000 and enable the water budget shut down \n
                    - enable water rationing \n
                \n
                - set up POC 8 \n
                    - enable POC 8 \n
                    - assign master valve TMV0004 and flow meter TWF0004 to POC 8 \n
                    - assign POC 8 a target flow of 50 \n
                    - assign POC 8 to main line 8 \n
                    - set POC priority to 3-low \n
                    - set high flow limit to 75 and disable high flow shut down \n
                    - set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
                    - set water budget to 1000 and disable water budget shut down \n
                    - disable water rationing \n
                    - assign event switch TPD0001 to POC 8 \n
                    - set switch empty condition to closed \n
                    - set empty wait time to 540 minutes \n

        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    # self.update_controller_firmware_from_bm()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - sets up the controller \n
        """
        self.config.controllers[1].init_cn()

    def step_2(self):
        # """
        # - sets the devices that will be used in the configuration of the controller \n
        # - search and address the devices:
        #     - zones                 {zn}
        #     - Master Valves         {mv}
        #     - Moisture Sensors      {ms}
        #     - Temperature Sensors   {ts}
        #     - Event Switches        {sw}
        #     - Flow Meter            {fm}
        # - once the devices are found they can be addressed so that they can be used in the programming
        #     - zones can use addresses {1-200}
        #     - Master Valves can use address {1-8}
        # - the 3200 auto address certain devices in the order it receives them:
        #     - Master Valves         {mv}
        #     - Moisture Sensors      {ms}
        #     - Temperature Sensors   {ts}
        #     - Event Switches        {sw}
        #     - Flow Meter            {fm}
        # """
        # # load all devices need into the controller so that they are available for use in the configuration
        self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                     mv_d1_list=self.config.mv_d1,
                                                     d2_list=self.config.d2,
                                                     mv_d2_list=self.config.mv_d2,
                                                     d4_list=self.config.d4,
                                                     dd_list=self.config.dd,
                                                     ms_list=self.config.ms,
                                                     fm_list=self.config.fm,
                                                     ts_list=self.config.ts,
                                                     sw_list=self.config.sw)

        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
        # assign zones an address between 1-200
        self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                         zn_ad_range=self.config.zn_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
        self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                         mv_ad_range=self.config.mv_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                         ms_ad_range=self.config.ms_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                         ts_ad_range=self.config.ts_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
        self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                         sw_ad_range=self.config.sw_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
        self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                         fm_ad_range=self.config.fm_ad_range)
        self.config.create_mainline_objects()
        self.config.create_3200_poc_objects()

    def step_3(self):
        """
        set_up_programs
        assign zones to programs \n
        set up primary linked zones \n
        give each zone a run time of 1 hour and 30 minutes \n
        give each program a start time of 8:00 A.M. \n
        must make zone 200 a primary zone before you can link zones to it \n
        assign sensor to primary zone 1
        #assign moisture sensors to a primary zone 1 and set to lower limit watering
        """
        # this is set in the PG3200 object
        # TODO need to have concurrent zones per program added

        program_start_times = [480]
        program_watering_days = [1, 1, 1, 1, 1, 1, 1]  # runs all days
        program_water_windows = ['111111111111111111111111']

        self.config.programs[1] = PG3200(_ad=1,
                                         _en=opcodes.true,
                                         _ww=program_water_windows,
                                         _pr=1,
                                         _mc=3,
                                         _sa=100,
                                         _ci=opcodes.week_days,
                                         _di=None,
                                         _wd=program_watering_days,
                                         _sm=[],
                                         _st=program_start_times,
                                         _ml=1,
                                         _bp='')
        self.config.programs[2] = PG3200(_ad=2,
                                         _en=opcodes.true,
                                         _ww=program_water_windows,
                                         _pr=1,
                                         _mc=2,
                                         _sa=100,
                                         _ci=opcodes.week_days,
                                         _di=None,
                                         _wd=program_watering_days,
                                         _sm=[],
                                         _st=program_start_times,
                                         _ml=1,
                                         _bp='')
        self.config.programs[99] = PG3200(_ad=99,
                                          _en=opcodes.true,
                                          _ww=program_water_windows,
                                          _pr=1,
                                          _mc=4,
                                          _sa=100,
                                          _ci=opcodes.week_days,
                                          _di=None,
                                          _wd=program_watering_days,
                                          _sm=[],
                                          _st=program_start_times,
                                          _ml=1,
                                          _bp='')

    def step_4(self):

        # Zone Programs
        self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                   prog_obj=self.config.programs[1],
                                                   _rt=960,
                                                   _ct=300,
                                                   _so=300,
                                                   _pz=1)
        self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                   prog_obj=self.config.programs[1],
                                                   _rt=self.config.zone_programs[1].rt,
                                                   _ct=self.config.zone_programs[1].ct,
                                                   _so=self.config.zone_programs[1].so,
                                                   _pz=1)
        self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                    prog_obj=self.config.programs[1],
                                                    _rt=960,
                                                    _ct=300,
                                                    _so=300,
                                                    _ws=opcodes.timed)
        self.config.zone_programs[50] = ZoneProgram(zone_obj=self.config.zones[50],
                                                    prog_obj=self.config.programs[2],
                                                    _rt=1200,
                                                    _ct=600,
                                                    _so=600,
                                                    _pz=50)
        self.config.zone_programs[51] = ZoneProgram(zone_obj=self.config.zones[51],
                                                    prog_obj=self.config.programs[2],
                                                    _rt=self.config.zone_programs[50].rt,
                                                    _ct=self.config.zone_programs[50].ct,
                                                    _so=self.config.zone_programs[50].so,
                                                    _pz=50)

        self.config.zone_programs[200] = ZoneProgram(zone_obj=self.config.zones[200],
                                                     prog_obj=self.config.programs[99],
                                                     _rt=1200,
                                                     _ct=600,
                                                     _so=600,
                                                     _pz=200)

        # TODO runtime for a linked zone is returning a bad value
        # TODO this zone is also not show up as a linked zone
        # Zone programs linked to Zone 200
        self.config.zone_programs[197] = ZoneProgram(zone_obj=self.config.zones[197],
                                                     prog_obj=self.config.programs[99],
                                                     _rt=self.config.zone_programs[200].rt,
                                                     _ct=self.config.zone_programs[200].ct,
                                                     _so=self.config.zone_programs[200].so,
                                                     _pz=200)
        self.config.zone_programs[198] = ZoneProgram(zone_obj=self.config.zones[198],
                                                     prog_obj=self.config.programs[99],
                                                     _rt=self.config.zone_programs[200].rt,
                                                     _ct=self.config.zone_programs[200].ct,
                                                     _so=self.config.zone_programs[200].so,
                                                     _pz=200)
        self.config.zone_programs[199] = ZoneProgram(zone_obj=self.config.zones[199],
                                                     prog_obj=self.config.programs[99],
                                                     _rt=self.config.zone_programs[200].rt,
                                                     _ct=self.config.zone_programs[200].ct,
                                                     _so=self.config.zone_programs[200].so,
                                                     _pz=200)

    def step_5(self):
        """
        set_mainlines_3200
        set up main line 1 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        self.config.mainlines[1] = Mainline(_ad=1,
                                            _ft=4,
                                            _fl=500,
                                            _lc=opcodes.true,
                                            _hv=5,
                                            _hs=opcodes.true,
                                            _lv=20,
                                            _ls=opcodes.true)

    def step_6(self):
        """
        set_poc_3200
        set up POC 1 \n
            enable POC 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            assign POC 1 a target flow of 500 \n
            assign POC 1 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
         """
        self.config.poc[1] = POC3200(
            _ad=1,
            _en=opcodes.true,
            _mv=1,
            _fm=1,
            _fl=500,
            _ml=1,
            _pr=2,
            _hf=550,
            _hs=opcodes.true,
            _uf=10,
            _us=opcodes.true,
            _wb=100000,
            _ws=opcodes.true,
            _wr=opcodes.true)

    def step_7(self):
        """
        :return:
        :rtype:
        """
        self.config.verify_full_configuration()

    def step_8(self):
        """
        set max concurrency on controller to 9 this will insure that all zones for all programs start at one time
        Set tracking ration for each zone
        set date time on controller to 11/22/2016 at 7:58am
        increment clock one minute to 7:59am which is one minute before start time
        verify that all zones are in a done watering state

        :return:
        :rtype:
        """
        self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=9)

        self.config.zone_programs[1].set_runtime_ratio_on_cn(_runtime_ratio=100)
        self.config.zone_programs[2].set_runtime_ratio_on_cn(_runtime_ratio=50)
        self.config.zone_programs[3].set_runtime_ratio_on_cn(_runtime_ratio=100)
        self.config.zone_programs[50].set_runtime_ratio_on_cn(_runtime_ratio=100)
        self.config.zone_programs[51].set_runtime_ratio_on_cn(_runtime_ratio=50)
        self.config.zone_programs[197].set_runtime_ratio_on_cn(_runtime_ratio=50)
        self.config.zone_programs[198].set_runtime_ratio_on_cn(_runtime_ratio=100)
        self.config.zone_programs[199].set_runtime_ratio_on_cn(_runtime_ratio=150)
        self.config.zone_programs[200].set_runtime_ratio_on_cn(_runtime_ratio=100)

        for zone_programs in self.config.zn_ad_range:
            self.config.zone_programs[zone_programs].get_data()

        for zone in self.config.zn_ad_range:
            if self.config.zone_programs[zone].pz != 0 and self.config.zone_programs[zone].pz != self.config.zone_programs[zone].zone.ad:
                self.config.zone_programs[zone].verify_runtime_tracking_ratio_on_cn()
            elif self.config.zone_programs[zone].pz == self.config.zone_programs[zone].zone.ad:
                self.config.zone_programs[zone].verify_water_strategy_on_cn()
            self.config.zone_programs[zone].verify_runtime_on_cn()
            # TODO there is a bug in the 3200
            # self.config.zone_programs[zone].verify_cycle_time_on_cn()

    def step_9(self):
        """

        :return:
        :rtype:
        """
        self.config.controllers[1].set_date_and_time_on_cn(_date='11/22/2016', _time='7:58:00')

        self.config.controllers[1].do_increment_clock(minutes=1)

        for zones in self.config.zn_ad_range:
            self.config.zones[zones].verify_status_on_cn(status=opcodes.done_watering)

    def step_10(self):
        """


        :return:
        :rtype:
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        # need to clear out values from varibles for the new test
        #TODO need to add these to the program
        for program in self.config.pg_ad_range:
            self.config.programs[program].seconds_program_ran = 0
            self.config.programs[program].seconds_program_waited = 0
        for zone in self.config.zn_ad_range:
            self.config.zones[zone].seconds_zone_ran = 0
            self.config.zones[zone].seconds_zone_soaked = 0
            self.config.zones[zone].seconds_zone_waited = 0
            self.config.zones[zone].seconds_for_each_cycle_duration = []
            self.config.zones[zone].seconds_for_each_soak_duration = []
            self.config.zones[zone].last_ss = ''
            # ----- END FOR ----------------------------------------------------------------------------------------
        # increment the clock 1 minute to 8:01 this will start all of the programs
        self.config.controllers[1].do_increment_clock(minutes=2)
        not_done = True

        while not_done:

            zones_still_running = False
            # ------------------------------------------------------------------------------------------------------
            # for each zone:
            #   1. get current data from controller (for getting updated status)
            #   2. get current status
            #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
                _zone_status = self.config.zones[zone].data.get_value_string_by_key(opcodes.status_code)
                # for each zone get the ss per zone
                #   update the attribute in the object
                #       (this is so we can store it for what happened during the last run time)
                if _zone_status == opcodes.watering:
                    self.config.zones[zone].seconds_zone_ran += 60  # using 60 so that we are in seconds
                elif _zone_status == opcodes.soak_cycle:
                    self.config.zones[zone].seconds_zone_soaked += 60
                elif _zone_status == opcodes.waiting_to_water:
                    self.config.zones[zone].seconds_zone_waited += 60

                # If the a zone changes from watering to soaking, put the runtime in the cycle-time list
                if self.config.zones[zone].last_ss == opcodes.watering and (_zone_status == opcodes.soaking or _zone_status == opcodes.waiting_to_water or _zone_status == opcodes.done_watering):
                    cycle_duration = self.config.zones[zone].seconds_zone_ran
                    for cycle in self.config.zones[zone].seconds_for_each_cycle_duration:
                        # subtract from the list the time that was just in the list so that we can get the next value in the list
                        cycle_duration = cycle_duration-cycle
                    self.config.zones[zone].seconds_for_each_cycle_duration.append(cycle_duration)

                # If the zone changes from soaking to watering, put the soak-time in the soak-time list
                if self.config.zones[zone].last_ss == opcodes.soaking and _zone_status == opcodes.watering: #and in the past it went from waiting, done or watering
                    soak_duration = self.config.zones[zone].seconds_zone_soaked
                    for soak in self.config.zones[zone].seconds_for_each_soak_duration:
                        soak_duration = soak_duration-soak
                    self.config.zones[zone].seconds_for_each_soak_duration.append(soak_duration)

                # update last zone status
                self.config.zones[zone].last_ss = _zone_status
                # set flag not all zones are done
                if _zone_status != opcodes.done_watering:
                    zones_still_running = True
                # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------

            # if any of the zones are not done than go back through loop :
            if zones_still_running:
                not_done = True
                self.config.controllers[1].do_increment_clock(minutes=1)
                self.config.controllers[1].verify_date_and_time()
            else:
                not_done = False

        for zone in self.config.zn_ad_range:
            print "zone " + str(zone) + " runtime " + str(self.config.zone_programs[zone].rt)
            print "zone " + str(zone) + " Total seconds zone ran " + str(self.config.zones[zone].seconds_zone_ran)
            print "zone " + str(zone) + " cycle time " + str(self.config.zone_programs[zone].ct)
            print "zone " + str(zone) + " duration of each cycle time " + str(self.config.zones[zone].seconds_for_each_cycle_duration)
            print "zone " + str(zone) + " Soak time " + str(self.config.zone_programs[zone].so)
            print "zone " + str(zone) + " duration of each soak time  " + str(self.config.zones[zone].seconds_for_each_soak_duration)
            print "zone " + str(zone) + " Total second zone soaked " + str(self.config.zones[zone].seconds_zone_soaked)
            print "zone " + str(zone) + " wait time " + str(self.config.zones[zone].seconds_zone_waited)
            print "zone " + str(zone) + " " + str(self.config.zones[zone].last_ss)

        # ----- END WHILE ------------------------------------------------------------------------------------------
    def step_11(self):
        """
        verify that all zones water correctly and that the zones that have tracking ratio adjust there runtime and
        cycle time
        tracking ratio adjusts run time and cycle time not soak time
        Program 1
            - zones
                1 primary zone
                    - runtime 960 seconds
                    - cycle time 300 seconds
                    - soak time  300 seconds
                2 has a 50% tracking ratio of primary zone 1
                    - original runtime = 960 after tracking ration runtime = 480
                    - original cycle 300 after tracking ration cycle time = 150 seconds this gets rounded up to 180 seconds
                    - soak time = 300 seconds
                3 is a timed zone
                    - runtime 960 seconds
                    - cycle time = 300 seconds
                    - soak time = 300 seconds
        Program 2
                50 is a timed zone
                    - runtime = 1200 seconds
                    - cycle time = 600 seconds
                    - soak time = 600 seconds
                51 has a 50% tracking ratio of primary zone 50
                    - original runtime = 1200 seconds after tracking ration runtime = 600 seconds
                    - original cycle time = 600 seconds after tracking ration cycle time = 300 seconds
                    - soak time = 600 seconds
        Program 99
                197 has a 50% tracking ratio of primary zone 200
                    - original runtime = 1200 seconds after tracking ration runtime = 600 seconds
                    - original cycle time = 600 seconds after tracking ration runtime = 300 seconds
                    - soak for  600 seconds
                198 has a 100% tracking ratio of primary zone 200
                    - original runtime = 1200 seconds after tracking ration runtime = 1200 seconds
                    - original cycle time = 600 seconds after tracking ration runtime = 600 seconds
                    - soak for  600 seconds
                199 has a 150% tracking ratio of primary zone 200
                    - original runtime = 1200 seconds after tracking ration runtime = 1800 seconds
                    - original cycle time = 600 seconds after tracking ration runtime = 900 seconds
                    - soak for  600 seconds
                200 primary zone
                    - original runtime = 1200 seconds
                    - cycle time = 600 seconds
                    - soak time =  600 seconds
        :return:
        :rtype:
        """
        # verifiers
            # - Total Run time
            # - Cycle time on controller against Each Cycle time duration
            # - Soak time on controller against Each Soak time duration
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        for zone in self.config.zn_ad_range:
            self.config.zones[zone].get_data()
            self.config.zone_programs[zone].get_data()
            # runtime is the expected time calculated for the controller to run
            # the controller doesnt always round so this rounds everything
            runtime = 60*(int(round(float(self.config.zone_programs[zone].rt)/(float(60)))))
            if self.config.zones[zone].seconds_zone_ran == runtime:
                e_msg = "Zone '{0}' ran for '{1}' seconds which match expected runtime of '{2}' seconds, " \
                        "it soaked for '{3}' seconds, and it waited for '{4}' seconds"\
                    .format(
                        str(zone),                                          # {0}
                        str(self.config.zones[zone].seconds_zone_ran),      # {1}
                        str(runtime),                                       # {2}
                        str(self.config.zones[zone].seconds_zone_soaked),   # {3}
                        str(self.config.zones[zone].seconds_zone_waited)    # {4}}

                    )
                print(e_msg)
            else:
                e_msg = "Zone '{0}' ran for '{1}' seconds and was expected run for '{2}' seconds, " \
                        "it soaked for '{3}' seconds, and it waited for '{4}' seconds" \
                    .format(
                        str(zone),                                          # {0}
                        str(self.config.zones[zone].seconds_zone_ran),      # {1}
                        str(runtime),                                       # {2}
                        str(self.config.zones[zone].seconds_zone_soaked),   # {3}
                        str(self.config.zones[zone].seconds_zone_waited)    # {4}
                    )
                raise ValueError(e_msg)


            # this only looks at the first cycle time in the list an verifies it against the set cycle time in the Contorller
            cycle_time = self.config.zone_programs[zone].ct

            first_cycle_time = self.config.zones[zone].seconds_for_each_cycle_duration[0]
            if first_cycle_time == cycle_time:
                e_msg = "Zone '{0}' cycled for '{1}' seconds which match expected cycle time of '{2}' seconds " \
                   .format(
                    str(zone),                  # {0}
                    str(first_cycle_time),  # {1}
                    str(cycle_time),            # {2}
                    )
                print(e_msg)
            else:
                e_msg = "A cycle time from:\n" \
                        "\tZone: {0}\n" \
                        "\tProgram: {1}\n" \
                        "did not match the list. Test Object had {2} while the controller returned {3}\n.".format(
                            self.config.zone_programs[zone].zone,       # {0}
                            self.config.zone_programs[zone].program,    # {1}
                            str(first_cycle_time),                      # {2}
                            str(cycle_time)                             # {3}
                        )
                raise ValueError(e_msg)
            # this adds up all cycle times and compares against total run time to verify that total minutes cycle is not greater than the total minutes in run time
            total_of_all_cycle_times = 0
            for each_cycle_time in self.config.zones[zone].seconds_for_each_cycle_duration:
                total_of_all_cycle_times += each_cycle_time
            if total_of_all_cycle_times == runtime:
                e_msg = "Zone '{0}' cycled for a total of '{1}' seconds which match expected runtime of '{2}' seconds " \
                   .format(
                    str(zone),                       # {0}
                    str(total_of_all_cycle_times),  # {1}
                    str(runtime),                  # {2}
                    )
                print(e_msg)
            else:
                e_msg = "A cycle time from:\n" \
                        "\tZone: {0}\n" \
                        "\tProgram: {1}\n" \
                        "did not match the list. Test Object had {2} while the controller returned {3}\n.".format(
                            self.config.zone_programs[zone].zone,       # {0}
                            self.config.zone_programs[zone].program,    # {1}
                            str(total_of_all_cycle_times),              # {2}
                            str(runtime)                                # {3}
                        )
                raise ValueError(e_msg)

            soak_time = self.config.zone_programs[zone].so

            for soak_time_from_list in self.config.zones[zone].seconds_for_each_soak_duration:
                if soak_time_from_list >= soak_time:
                    e_msg = "Zone '{0}' soaked for '{1}' seconds which match expected soak time of '{2}' seconds " \
                       .format(
                        str(zone),                  # {0}
                        str(soak_time_from_list),   # {1}
                        str(soak_time),             # {2}
                        )
                    print(e_msg)
                else:
                    e_msg = "A soak time from:\n" \
                            "\tZone: {0}\n" \
                            "\tProgram: {1}\n" \
                            "did not match the list. Test Object had {2} while the controller returned {3}\n.".format(
                                self.config.zone_programs[zone].zone,           # {0}
                                self.config.zone_programs[zone].program,        # {1}
                                str(soak_time_from_list),                       # {2}
                                str(soak_time)                                  # {3}
                            )
                    raise ValueError(e_msg)

    def step_12(self):
        """
        Make a second adjustment keeping all of the tracking ratio the same as the previous steps now add a
        Seasonal adjustment
        set date time on controller to 11/22/2016 at 7:58am
        increment clock one minute to 7:59am which is one minute before start time
        verify that all zones are in a done watering state

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        # TODO need a method to set and verify a seasonal adjust pass in a int percent you decide
        self.config.programs[1].set_seasonal_adjust_on_cn(50)
        self.config.programs[2].set_seasonal_adjust_on_cn(100)
        self.config.programs[99].set_seasonal_adjust_on_cn(150)
        for zone in self.config.zn_ad_range:
            print self.config.zone_programs[zone].rt

    def step_13(self):
        """
        set max concurrency on controller to 9 this will insure that all zones start at one time
        set date time on controller to 11/23/2016 at 7:58am
        increment clock one minute to 7:59 am
        verify that all zones are in a done watering state
        increment clock 2 minutes to 8:01am
        verify that all zones get set to watering
        zone run will run for 900 seconds
        cycle for 300
        soak for 300

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=9)

        self.config.controllers[1].set_date_and_time_on_cn(_date='11/23/2016', _time='7:58:00')

        # Increment the clock by 1 minute to 7:59, verify that nothing has started yet
        self.config.controllers[1].do_increment_clock(minutes=1)
        for zone in self.config.zn_ad_range:
            self.config.zones[zone].get_data()
        self.config.programs[1].get_data()

        for zones in self.config.zn_ad_range:
            self.config.zones[zones].verify_status_on_cn(status=opcodes.done_watering)

    def step_14(self):
        """

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        # need to clear out values from varibles for the new test
        #TODO need to add these to the program
        for program in self.config.pg_ad_range:
            self.config.programs[program].seconds_program_ran = 0
            self.config.programs[program].seconds_program_waited = 0
        for zone in self.config.zn_ad_range:
            self.config.zones[zone].seconds_zone_ran = 0
            self.config.zones[zone].seconds_zone_soaked = 0
            self.config.zones[zone].seconds_zone_waited = 0
            self.config.zones[zone].seconds_for_each_cycle_duration = []
            self.config.zones[zone].seconds_for_each_soak_duration = []
            self.config.zones[zone].last_ss = ''
            # ----- END FOR ----------------------------------------------------------------------------------------
        # increment the clock 1 minute to 8:01 this will start all of the programs
        self.config.controllers[1].do_increment_clock(minutes=2)
        not_done = True

        while not_done:

            zones_still_running = False
            # ------------------------------------------------------------------------------------------------------
            # for each zone:
            #   1. get current data from controller (for getting updated status)
            #   2. get current status
            #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
                _zone_status = self.config.zones[zone].data.get_value_string_by_key(opcodes.status_code)
                # for each zone get the ss per zone
                #   update the attribute in the object
                #       (this is so we can store it for what happened during the last run time)
                if _zone_status == opcodes.watering:
                    self.config.zones[zone].seconds_zone_ran += 60  # using 60 so that we are in seconds
                elif _zone_status == opcodes.soak_cycle:
                    self.config.zones[zone].seconds_zone_soaked += 60
                elif _zone_status == opcodes.waiting_to_water:
                    self.config.zones[zone].seconds_zone_waited += 60

                # If the a zone changes from watering to soaking, put the runtime in the cycle-time list
                if self.config.zones[zone].last_ss == opcodes.watering and (
                            _zone_status == opcodes.soaking or _zone_status == opcodes.waiting_to_water or _zone_status == opcodes.done_watering):
                    cycle_duration = self.config.zones[zone].seconds_zone_ran
                    for cycle in self.config.zones[zone].seconds_for_each_cycle_duration:
                        # subtract from the list the time that was just in the list so that we can get the next value in the list
                        cycle_duration = cycle_duration - cycle
                    self.config.zones[zone].seconds_for_each_cycle_duration.append(cycle_duration)

                # If the zone changes from soaking to watering, put the soak-time in the soak-time list
                if self.config.zones[zone].last_ss == opcodes.soaking and _zone_status == opcodes.watering:  # and in the past it went from waiting, done or watering
                    soak_duration = self.config.zones[zone].seconds_zone_soaked
                    for soak in self.config.zones[zone].seconds_for_each_soak_duration:
                        soak_duration = soak_duration - soak
                    self.config.zones[zone].seconds_for_each_soak_duration.append(soak_duration)

                # update last zone status
                self.config.zones[zone].last_ss = _zone_status
                # set flag not all zones are done
                if _zone_status != opcodes.done_watering:
                    zones_still_running = True
                # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------

            # if any of the zones are not done than go back through loop :
            if zones_still_running:
                not_done = True
                self.config.controllers[1].do_increment_clock(minutes=1)
                self.config.controllers[1].verify_date_and_time()
            else:
                not_done = False

        for zone in self.config.zn_ad_range:
            print "zone " + str(zone) + " runtime " + str(self.config.zone_programs[zone].rt)
            print "zone " + str(zone) + " Total seconds zone ran " + str(self.config.zones[zone].seconds_zone_ran)
            print "zone " + str(zone) + " cycle time " + str(self.config.zone_programs[zone].ct)
            print "zone " + str(zone) + " duration of each cycle time " + str(self.config.zones[zone].seconds_for_each_cycle_duration)
            print "zone " + str(zone) + " Soak time " + str(self.config.zone_programs[zone].so)
            print "zone " + str(zone) + " duration of each soak time  " + str(self.config.zones[zone].seconds_for_each_soak_duration)
            print "zone " + str(zone) + " Total second zone soaked " + str(self.config.zones[zone].seconds_zone_soaked)
            print "zone " + str(zone) + " wait time " + str(self.config.zones[zone].seconds_zone_waited)
            print "zone " + str(zone) + " " + str(self.config.zones[zone].last_ss)
        # ----- END WHILE ------------------------------------------------------------------------------------------

    def step_15(self):
        """
        increment clock 2 minutes to 8:01am this will allow all programs to start watering
        verify that all zones water correctly and that the zones that have tracking ration adjust there runtime and
        cycle time the runtime will also be adjusted for the seasonal adjust but the cycle and soak times will not be
        adjusted past the tracking ration
        New program adjustments
        - set program 1 to seasonally adjust 50%
        - Set program 2 to seasonally adjust 100%
        - set program 99 to seasonally adjust 150%
        Program 1 has a seasonal adjustment of 50%
            - zones
                1 primary zone
                    - runtime 960 seconds after seasonal adjustment the runtime = 480
                    - cycle time 300 seconds
                    - soak time  300 seconds
                2 has a 50% tracking ratio of primary zone 1
                    - original runtime = 900 after tracking ration runtime = 480 seconds after seasonal adjustment the runtime = 240 seconds
                    - original cycle 300 after tracking ration cycle time = 150 seconds
                    - soak time = 300 seconds
                3 is a timed zone
                    - runtime 960 seconds after seasonal adjustment the runtime = 480 seconds
                    - cycle time = 300 seconds
                    - soak time = 300 seconds
        Program 2 has a seasonally adjust 100%
                50 is a timed zone
                    - runtime = 1200 seconds after seasonal adjustment the runtime = 1200 seconds
                    - cycle time = 600 seconds
                    - soak time = 600 seconds
                51 has a 50% tracking ratio of primary zone 50
                    - original runtime = 1200 seconds after tracking ration runtime = 600 seconds after seasonal adjustment the runtime = 600 seconds
                    - original cycle time = 600 seconds after tracking ration cycle time = 300 seconds
                    - soak time = 600 seconds
        Program 99 has a seasonally adjust 150%
                197 has a 50% tracking ratio of primary zone 200
                    - original runtime = 1200 seconds after tracking ration runtime = 600 seconds after seasonal adjustment the runtime = 1200 seconds
                    - original cycle time = 600 seconds after tracking ration runtime = 300 seconds
                    - soak for  600 seconds
                198 has a 100% tracking ratio of primary zone 200
                    - original runtime = 1200 seconds after tracking ration runtime = 1200 seconds after seasonal adjustment the runtime = 1800 seconds
                    - original cycle time = 600 seconds after tracking ration runtime = 600 seconds
                    - soak for  600 seconds
                199 has a 150% tracking ratio of primary zone 200
                    - original runtime = 1200 seconds after tracking ration runtime = 1800 seconds after seasonal adjustment the runtime = 2700 seconds
                    - original cycle time = 600 seconds after tracking ration runtime = 900 seconds
                    - soak for  600 seconds
                200 primary zone
                    - original runtime = 1200 seconds after seasonal adjustment the runtime = 1800 seconds
                    - cycle time = 600 seconds
                    - soak time =  600 seconds

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        self.config.controllers[1].do_increment_clock(minutes=2)
        self.config.controllers[1].verify_date_and_time()

        for zone in self.config.zn_ad_range:
            self.config.zones[zone].get_data()
            self.config.zone_programs[zone].get_data()
            # runtime is the expected time calculated for the controller to run
            # the controller doesnt always round so this rounds everything
            runtime = 60*(int(round(float(self.config.zone_programs[zone].rt)/(float(60)))))
            if self.config.zones[zone].seconds_zone_ran == runtime:
                e_msg = "Zone '{0}' ran for '{1}' seconds which match expected runtime of '{2}' seconds, " \
                        "it soaked for '{3}' seconds, and it waited for '{4}' seconds"\
                    .format(
                        str(zone),                                          # {0}
                        str(self.config.zones[zone].seconds_zone_ran),      # {1}
                        str(runtime),                                       # {2}
                        str(self.config.zones[zone].seconds_zone_soaked),   # {3}
                        str(self.config.zones[zone].seconds_zone_waited)    # {4}}

                    )
                print(e_msg)
            else:
                e_msg = "Zone '{0}' ran for '{1}' seconds and was expected run for '{2}' seconds, " \
                        "it soaked for '{3}' seconds, and it waited for '{4}' seconds" \
                    .format(
                        str(zone),                                          # {0}
                        str(self.config.zones[zone].seconds_zone_ran),      # {1}
                        str(runtime),                                       # {2}
                        str(self.config.zones[zone].seconds_zone_soaked),   # {3}
                        str(self.config.zones[zone].seconds_zone_waited)    # {4}
                    )
                raise ValueError(e_msg)


            # this only looks at the first cycle time in the list an verifies it against the set cycle time in the Contorller
            cycle_time = self.config.zone_programs[zone].ct

            first_cycle_time = self.config.zones[zone].seconds_for_each_cycle_duration[0]
            if first_cycle_time == cycle_time:
                e_msg = "Zone '{0}' cycled for '{1}' seconds which match expected cycle time of '{2}' seconds " \
                   .format(
                    str(zone),                  # {0}
                    str(first_cycle_time),      # {1}
                    str(cycle_time),            # {2}
                    )
                print(e_msg)
            else:
                e_msg = "A cycle time from:\n" \
                        "\tZone: {0}\n" \
                        "\tProgram: {1}\n" \
                        "did not match the list. Test Object had {2} while the controller returned {3}\n.".format(
                            self.config.zone_programs[zone].zone,       # {0}
                            self.config.zone_programs[zone].program,    # {1}
                            str(first_cycle_time),                      # {2}
                            str(cycle_time)                             # {3}
                        )
                raise ValueError(e_msg)
            # this adds up all cycle times and compares against total run time to verify that total minutes cycle is not greater than the total minutes in run time
            total_of_all_cycle_times = 0
            for each_cycle_time in self.config.zones[zone].seconds_for_each_cycle_duration:
                total_of_all_cycle_times += each_cycle_time
            if total_of_all_cycle_times == runtime:
                e_msg = "Zone '{0}' cycled for a total of '{1}' seconds which match expected runtime of '{2}' seconds " \
                   .format(
                    str(zone),                       # {0}
                    str(total_of_all_cycle_times),   # {1}
                    str(runtime),                    # {2}
                    )
                print(e_msg)
            else:
                e_msg = "A cycle time from:\n" \
                        "\tZone: {0}\n" \
                        "\tProgram: {1}\n" \
                        "did not match the list. Test Object had {2} while the controller returned {3}\n.".format(
                            self.config.zone_programs[zone].zone,       # {0}
                            self.config.zone_programs[zone].program,    # {1}
                            str(total_of_all_cycle_times),              # {2}
                            str(runtime)                                # {3}
                        )
                raise ValueError(e_msg)

            soak_time = self.config.zone_programs[zone].so

            for soak_time_from_list in self.config.zones[zone].seconds_for_each_soak_duration:
                if soak_time_from_list >= soak_time:
                    e_msg = "Zone '{0}' soaked for '{1}' seconds which match expected soak time of '{2}' seconds " \
                       .format(
                        str(zone),                  # {0}
                        str(soak_time_from_list),   # {1}
                        str(soak_time),             # {2}
                        )
                    print(e_msg)
                else:
                    e_msg = "A soak time from:\n" \
                            "\tZone: {0}\n" \
                            "\tProgram: {1}\n" \
                            "did not match the list. Test Object had {2} while the controller returned {3}\n.".format(
                                self.config.zone_programs[zone].zone,           # {0}
                                self.config.zone_programs[zone].program,        # {1}
                                str(soak_time_from_list),                       # {2}
                                str(soak_time)                                  # {3}
                            )
                    raise ValueError(e_msg)