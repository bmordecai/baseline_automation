import sys
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from datetime import time, timedelta, datetime, date
# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# Browser pages used
# import page_factory

__author__ = 'Eldin'


class ControllerUseCase17(object):
    """
    Test name:
        - CN UseCase17 Soak Cycles
    purpose:
        - set up a zones on the controller and attach them to a program
            - Make two zones timed
            - Make one zone the primary zone
                - Make the other two into linked zones with different ratios
    Coverage Area: \n
        1. Able to disable zones \n
        2. Able to set zone concurrency \n
        3. Able to set up a soak cycle and verify that it runs properly \n
        4. able to set up primary zones and linked zones
        5. verify linked zones follow the primary zone ratios
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'ControllerUseCase16' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id
        date_mngr.set_date_to_default()

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        set max number of concurrent zones running on the controller to 2 \n
        verify BaseManager connection \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(2)

            self.config.basemanager_connection[1].verify_ip_address_state()
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller failed: " +
                                 str(ae.message))

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Load all devices to controller
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                             ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_3(self):
        """
        Make a very basic program that will be used to attach all the zones together. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _st=[1200],
                                             _mc=1)
            self.config.programs[2] = PG3200(_ad=2,
                                             _en=opcodes.true,
                                             _st=[1200],
                                             _mc=1)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_4(self):
        """
        assign zones to programs \n
        set up primary linked zones \n
        Zone 1 is a primary zone \n
            - Zone 2 and 3 linked to the primary zone 1 \n
            - Zone 2 has a runtime tracking ratio of 200% \n
            - Zone 3 has a runtime tracking ratio of 50% \n
        Zone 4 is timed \n
        Zone 5 is timed \n
        Set zone 5 to have enabled state set to false. (disable it)
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600,
                                                       _ct=600,
                                                       _so=1500,
                                                       _pz=1)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=200,
                                                       _pz=1)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=50,
                                                       _pz=1)

            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=3600,
                                                       _ct=900,
                                                       _so=900,
                                                       _pz=0)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=720,
                                                       _ct=120,
                                                       _so=300,
                                                       _pz=0)

            self.config.zones[5].set_enable_state_on_cn(_state=opcodes.false)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_5(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_6(self):
        """
        Set the date and time to some arbitrary date/time \n
        Start Program 1, which has all 5 zones on it \n
        Loop through until every zone is done watering \n
            - In each loop check the status of the zone. \n
            - Since we are incrementing by a minute during each loop, whatever the status is, add 60 (seconds) to the
              variable that represents that certain status in the zone
            - We will later use these values to make sure the zone ran/soaked for the correct amount of time \n
            
        Zone Behavior:
            - Zone 1:
                > Waters for 10 minutes (600s), then soaks for 25 minutes (1500s) then repeats
            - Zone 2:
                > Waits for 10 minutes (600s), then waters for 20 minutes (1200s) then soaks for 25 minutes (1500s) then
                  repeats.
            - Zone 3:
                > Waits for 30 minutes (1800s), then waters for 5 minutes (300s) then soaks for 50 minutes (3000s) then
                  repeats.
            - Zone 4:
                > Waters for 15 minutes (900s), then soaks for 15 minutes (900s) then repeats until 4th time watering,
                  then it shuts off.
            - Zone 5:
                > Never runs, is disabled.
        """

        expected_status_dict = {
            "07:59:00": {
                1: "DN", 2: "DN", 3: "DN", 4: "DN", 5: "DS"
            },
            "08:00:00": {
                1: "WT", 2: "WA", 3: "WA", 4: "WT", 5: "DS"
            },
            "08:05:00": {
                1: "WT", 2: "WA", 3: "WA", 4: "WT", 5: "DS"
            },
            "08:10:00": {
                1: "SO", 2: "WT", 3: "WA", 4: "WT", 5: "DS"
            },
            "08:15:00": {
                1: "SO", 2: "WT", 3: "WA", 4: "SO", 5: "DS"
            },
            "08:20:00": {
                1: "SO", 2: "WT", 3: "WA", 4: "SO", 5: "DS"
            },
            "08:25:00": {
                1: "SO", 2: "WT", 3: "WA", 4: "SO", 5: "DS"
            },
            "08:30:00": {
                1: "SO", 2: "SO", 3: "WT", 4: "WT", 5: "DS"
            },
            "08:35:00": {
                1: "WT", 2: "SO", 3: "SO", 4: "WT", 5: "DS"
            },
            "08:40:00": {
                1: "WT", 2: "SO", 3: "SO", 4: "WT", 5: "DS"
            },
            "08:45:00": {
                1: "SO", 2: "SO", 3: "SO", 4: "SO", 5: "DS"
            },
            "08:50:00": {
                1: "SO", 2: "SO", 3: "SO", 4: "SO", 5: "DS"
            },
            "08:55:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "SO", 5: "DS"
            },
            "09:00:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "WT", 5: "DS"
            },
            "09:05:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "WT", 5: "DS"
            },
            "09:10:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "WT", 5: "DS"
            },
            "09:15:00": {
                1: "WT", 2: "SO", 3: "SO", 4: "SO", 5: "DS"
            },
            "09:20:00": {
                1: "WT", 2: "SO", 3: "SO", 4: "SO", 5: "DS"
            },
            "09:25:00": {
                1: "SO", 2: "SO", 3: "WT", 4: "SO", 5: "DS"
            },
            "09:30:00": {
                1: "SO", 2: "SO", 3: "SO", 4: "WT", 5: "DS"
            },
            "09:35:00": {
                1: "SO", 2: "SO", 3: "SO", 4: "WT", 5: "DS"
            },
            "09:40:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "WT", 5: "DS"
            },
            "09:45:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "DN", 5: "DS"
            },
            "09:50:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "DN", 5: "DS"
            },
            "09:55:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "DN", 5: "DS"
            },
            "10:00:00": {
                1: "WT", 2: "SO", 3: "SO", 4: "DN", 5: "DS"
            },
        }

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_controller_datetime(date="02/12/2017", time="7:59:00")

            self.config.controllers[1].set_date_and_time_on_cn(
                _date=date_mngr.controller_datetime.date_string_for_controller(),
                _time=date_mngr.controller_datetime.time_string_for_controller()
            )

            self.config.controllers[1].verify_date_and_time()

            # -------------------------------------------------------------------------------------------------------- #
            # Verify initial starting status'
            # -------------------------------------------------------------------------------------------------------- #
            for zone_ad in self.config.zones.keys():

                zone = self.config.zones[zone_ad]
                zone.get_data()
                zone_status = zone.data.get_value_string_by_key('SS')
                time_string = date_mngr.controller_datetime.time_string_for_controller()

                expected_status = expected_status_dict[time_string][zone_ad]

                if zone_status != expected_status:
                    e_msg = "{0}: Received unexpected status for Zone {1} before starting program. Received: {2}, " \
                            "Expected: {3}".format(
                                time_string,
                                zone_ad,
                                zone_status,
                                expected_status
                            )
                    raise AssertionError(e_msg)
                else:
                    print "{0}: Verified Zone {1}'s initial status: {2}".format(time_string, zone_ad, zone_status)

            # Start Programming
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.start_program)
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function=opcodes.start_program)

            # Increment clock for watering to start
            self.config.controllers[1].do_increment_clock(minutes=1)

            # -------------------------------------------------------------------------------------------------------- #
            # Verify zone status' after program has started.
            # -------------------------------------------------------------------------------------------------------- #
            for zone_ad in self.config.zones.keys():

                zone = self.config.zones[zone_ad]
                zone.get_data()
                zone_status = zone.data.get_value_string_by_key('SS')
                time_string = date_mngr.controller_datetime.time_string_for_controller()

                expected_status = expected_status_dict[time_string][zone_ad]

                if zone_status != expected_status:
                    e_msg = "{0}: Received unexpected status for Zone {1} after starting program. Received: {2}, " \
                            "Expected: {3}".format(
                                time_string,
                                zone_ad,
                                zone_status,
                                expected_status
                            )
                    raise AssertionError(e_msg)
                else:
                    print "{0}: Verified Zone {1}'s status: {2}".format(time_string, zone_ad, zone_status)

            # -------------------------------------------------------------------------------------------------------- #
            # Run programming for two hours and verify status'
            # -------------------------------------------------------------------------------------------------------- #
            while date_mngr.controller_datetime.time_string_for_controller() != "10:00:00":

                # Run simulation time at 5 minute intervals until 10:00:00 is reached.
                self.config.controllers[1].do_increment_clock(minutes=5)

                # Verify zone status' after program has started.
                for zone_ad in self.config.zones.keys():

                    zone = self.config.zones[zone_ad]
                    zone.get_data()
                    zone_status = zone.data.get_value_string_by_key('SS')
                    time_string = date_mngr.controller_datetime.time_string_for_controller()

                    expected_status = expected_status_dict[time_string][zone_ad]

                    if zone_status != expected_status:
                        e_msg = "{0}: Received unexpected status for Zone {1} after starting program. Received: {2}, " \
                                "Expected: {3}".format(
                                    time_string,
                                    zone_ad,
                                    zone_status,
                                    expected_status
                                )
                        raise AssertionError(e_msg)
                    else:
                        print "{0}: Verified Zone {1}'s status: {2}".format(time_string, zone_ad, zone_status)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))
