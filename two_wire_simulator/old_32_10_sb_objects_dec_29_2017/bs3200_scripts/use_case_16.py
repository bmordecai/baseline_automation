import sys
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from datetime import time, timedelta, datetime, date
# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_3200 import StartConditionFor3200, PauseConditionFor3200, StopConditionFor3200
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# Browser pages used
# import page_factory

__author__ = 'Eldin'


class ControllerUseCase16(object):
    """
    Test name:
        - CN UseCase16 Timed Zones
    purpose:
        - set up a zones on the controller and attach them to a program
            - Make one zone timed
            - Make one zone the primary zone
                - Make the other three into linked zones with different ratios
    Coverage Area: \n
        1. able to change zone modes: timed, primary, and linked \n
        2. able to set up run times and soak cycles and verify that they run properly \n
        3. verify linked zones follow the primary zone ratios \n
        4. able to disable zones \n
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'ControllerUseCase16' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        print '############################## Running ' + test_name + ' ###########################################\n'
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    self.step_18()
                    self.step_19()
                    self.step_20()
                    self.step_21()
                    self.step_22()
                    self.step_23()
                    self.step_24()
                    self.step_25()
                    self.step_26()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        set max number of concurrent zones running on the controller to 15 \n
        verify BaseManager connection \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].init_cn()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(15)

            self.config.basemanager_connection[1].verify_ip_address_state()
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller failed: " +
                                 str(ae.message))

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Load all devices to controller
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                             ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_3(self):
        """
        Make a very basic program that will be used to attach all the zones together. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _st=[1200],
                                             _mc=1)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_4(self):
        """
        assign zones to programs \n
        set up primary linked zones \n
        Zone 1 is timed \n
        Zone 2 is a primary zone \n
            - Zone 3-5 are linked to the primary zone 2 \n
            - Zone 3 has a runtime tracking ratio of 150% \n
            - Zone 4 has a runtime tracking ratio of 50% \n
            - Zone 5 has a runtime tracking ratio of 100% \n
        Set zone 5 to have enabled state set to false. (disable it)
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # TODO The runtime of ZoneProgram 1 was actually 3600, it just takes a long time to run
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=600,
                                                       _ct=120,
                                                       _so=480,
                                                       _pz=0)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=720,
                                                       _ct=120,
                                                       _so=300,
                                                       _pz=2)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=150,
                                                       _pz=2)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=50,
                                                       _pz=2)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=2)

            self.config.zones[5].set_enable_state_on_cn(_state=opcodes.false)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_5(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_6(self):
        """
        turn on sim mode in the controller \n
        stop the clock so that it can be incremented manually \n
        set the dial to off \n
        set the dial to run \n
        stop both programs \n
        advance the clock 1 second \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        advance the clock 1 second \n
        verify that the zones are a functioning correctly by getting their statuses before starting \n
        start program \n
        advance the clock 1 minute \n
        Zone Behavior:
            verify that zone 1 waits for 2 minutes at the start of the test then waters for 2 minutes and soaks for 8
            minutes then repeats \n
            verify that zone 2 starts out watering for 2 minutes then soaks for 5 minutes then repeats \n
            verify that zone 3 waits for 3 minutes at the start of the test then waters for 3 minutes and soaks for 9
            minutes then repeats \n
            verify that zone 4 waits for 9 minutes at the start of the test then waters for 1 minute and soaks for 9
            minutes then repeats \n
            verify that zone 5 is disable for the entire test \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            date_mngr.set_controller_datetime(date="04/08/2014", time="02:38:00")

            self.config.controllers[1].set_date_and_time_on_cn(
                _date=date_mngr.controller_datetime.date_string_for_controller(),
                _time=date_mngr.controller_datetime.time_string_for_controller()
            )
            # self.config.controllers[1].set_date_and_time_on_cn(_date='04/08/2014',_time='02:38:00')
            self.config.controllers[1].verify_date_and_time()
            for zone in range(1, 5):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.start_program)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_7(self):
        """
        zone 2 start first because it is a primary zone and zone is a timed zone
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 waiting to water
                - Zone 2 watering
                - Zone 3 waiting to water
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_8(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 waiting to water
                - Zone 2 watering
                - Zone 3 waiting to water
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_9(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 watering
                - Zone 2 soaking
                - Zone 3 waiting to water
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_10(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 watering
                - Zone 2 soaking
                - Zone 3 waiting to water
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_11(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 soaking
                - Zone 3 watering
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_12(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 soaking
                - Zone 3 watering
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
                self.config.controllers[1].do_increment_clock(minutes=1)
                self.config.controllers[1].verify_date_and_time()
                for zone in self.config.zn_ad_range:
                    self.config.zones[zone].get_data()
                self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
                self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
                self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
                self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
                self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_13(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 soaking
                - Zone 3 watering
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_14(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 watering
                - Zone 3 soaking
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_15(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 watering
                - Zone 3 soaking
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_16(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 soaking
                 - Zone 4 watering
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_17(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 soaking
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_18(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 soaking
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_19(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 watering
                 - Zone 2 soaking
                 - Zone 3 soaking
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_20(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 watering
                 - Zone 2 soaking
                 - Zone 3 soaking
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_21(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 watering
                 - Zone 3 soaking
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_22(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 watering
                 - Zone 3 soaking
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_23(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 watering
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_24(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 watering
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_25(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 watering
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_26(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 soaking
                 - Zone 4 watering
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].verify_date_and_time()
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)
        except AssertionError, ae:
            Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_27(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))