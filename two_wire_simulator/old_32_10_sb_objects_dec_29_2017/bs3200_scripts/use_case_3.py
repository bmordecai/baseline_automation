import sys

# this import allows us to directly use the date_mngr
from datetime import time, timedelta, datetime, date

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline

from csv_handler import FileIOOptions, CSVWriter
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes, csv
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.epa_package import equations, wbw_imports, ir_asa_resources
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.messages import status_plus_priority_code_dict_3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_3200 import StartConditionFor3200, StopConditionFor3200, PauseConditionFor3200

# Browser pages used
import page_factory

__author__ = 'Tige'


class ControllerUseCase3(object):
    """
    Test name:
        - Set Messages
    purpose:
        - set up a full configuration on the controller
            - verify that no programming is lost when you:
                - reboot the controller
                - update the firmware of the controller
                - replace a device
            - verify that after a reboot the controller restarts
                - program return to current watering state
                - zones return to current watering state
                - device return to current conditions (event paused stays paused)
            - verify that firmware update take effects:
                - even with certain devices or attributes are disabled

    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n

        - setting programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign programs to water sources \n
            - assign zones to programs \n
            - set up primary linked zones \n
            - give each zone a run time of 1 hour and 30 minutes \n
            - assign moisture sensors to a primary zone 1 and set to lower limit watering

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow\n
                - high variance limit
                - high variance shut down \n
                - low variance limit
                - low variance shut down \n

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n
                - priority
                    - low
                    - medium
                    - high\n
            - set high flow limit
            - high flow shut down \n
            - set unscheduled flow limit
            - enable unscheduled flow shut down \n
            - set water budget
            - enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions
                - event switch\n
                - set switch empty condition to closed \n
                - set empty wait time\n

        - Reboot the controller:
            - verify all setting \n
                - zone decoder
                    - verify all settings were not lost \n
                - moisture sensor
                    - verify all settings were not lost \n
                - flow decoder
                    - verify all settings were not lost \n
                - event decoder
                    - verify all settings were not lost  \n
                - temperature decoder
                    - verify all settings were not lost \n
        - Replace devices: \n
            - zone decoder
                - verify all settings were not lost \n
            - moisture sensor
                - verify the primary zone setting were not lost \n
            - flow decoder
                - verify all setting were deleted\n
            - event decoder
                - verify all setting were deleted \n
            - temperature decoder
                - verify all setting were delete\n
        - firmware update: \n
            - disable
                - all one of each device type \n
                - program
                - mainline
                - poc
            - verify configuration all stayed \n
    Date References:
        - configuration for script is located common\configuration_files\EPA_test_configuration.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, controller_type, cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance,
                 json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        Step 1:
            - configure controller:
                -  initiate controller to a known state so that it doesnt have a configuration or any devices loaded
                - turn on echo so the commands are displayed in the console
                - turn on sim mode so the clock can be stopped
                - stop the clock
                - Set the date and time so  that the controller is in a known state
                - turn on faux IO
                - clear all devices
                - clear all programming

            - configure basemanager: \n
                - verify the controller is connected to basemanager \n

        step 2:
            - setting up devices:
                - Loading devices into controller
                - Searching for devices so that they can be addressed
                - Address zones and master valves
                - Set all devices:
                    - descriptions
                    - locations
                    - default parameters
        Step 3:
            - setting up programming:
                - set_up_programs
                - assign zones to programs \n
                - set up primary linked zones \n
                - give each zone a run time of 1 hour and 30 minutes \n
                - give each program a start time of 8:00 A.M. \n
        step 4:
            - setup zone programs: \n
                - must make zone 200 a primary zone before you can link zones to it \n
                - assign sensor to primary zone 1
                - assign moisture sensors to a primary zone 1 and set to lower limit watering
        Step 5:
            - setting up mainlines: \n
                - set up main line 1 \n
                    set limit zones by flow to true \n
                    set the pipe fill time to 4 minutes \n
                    set the target flow to 500 \n
                    set the high variance limit to 5% and enable the high variance shut down \n
                    set the low variance limit to 20% and enable the low variance shut down \n
                \n
                - set up main line 8 \n
                    set limit zones by flow to true \n
                    set the pipe fill time to 1 minute \n
                    set the target flow to 50 \n
                    set the high variance limit to 20% and disable the high variance shut down \n
                    set the low variance limit to 5% and disable the low variance shut down \n
        Step 6:
            - setting up POCs: \n
                - set up POC 1 \n
                    - enable POC 1 \n
                    - assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
                    - assign POC 1 a target flow of 500 \n
                    - assign POC 1 to main line 1 \n
                    - set POC priority to 2-medium \n
                    - set high flow limit to 550 and enable high flow shut down \n
                    - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                    - set water budget to 100000 and enable the water budget shut down \n
                    - enable water rationing \n
                \n
                - set up POC 8 \n
                    - enable POC 8 \n
                    - assign master valve TMV0004 and flow meter TWF0004 to POC 8 \n
                    - assign POC 8 a target flow of 50 \n
                    - assign POC 8 to main line 8 \n
                    - set POC priority to 3-low \n
                    - set high flow limit to 75 and disable high flow shut down \n
                    - set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
                    - set water budget to 1000 and disable water budget shut down \n
                    - disable water rationing \n
                    - assign event switch TPD0001 to POC 8 \n
                    - set switch empty condition to closed \n
                    - set empty wait time to 540 minutes \n
        Step 7:
            - reboot the controller:
                - verify that all attributes for each device did not change
                - because the clock was increment we can verify status and verify that the zone didn't start \n
                  watering after the reboot \n
        step 8:
            - replace devices:
                - Loading new devices into controller
                - Searching for devices so that they can be addressed
                - Address zones and master valves to exciting addresses
                - set new moisture sensor to exciting primary zone
                - Set default values for:
                    - moisture sensor
                    - flow meters
                    - event switch
                    - temperature sensor
                - verify that all attributes for each device did not change
                - because the clock was increment we can verify status and verify that the zone didn't start \n
                  watering after the reboot \n
        Step 9:
            - disable able devices:
            - update firmware:
                - verify that all attributes for each device did not change
                - because the clock was increment we can verify status and verify that the zone didn't start \n
                  watering after the reboot \n
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    self.step_18()
                    self.step_19()
                    self.step_20()
                    self.step_21()
                    self.step_22()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                        print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        - sets up the controller \n
        - verify basemanager connection
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # setup controller
            # Stop clock
            # enable faux IO
            self.config.controllers[1].init_cn()

            # only need this for BaseManager
            self.config.basemanager_connection[1].verify_ip_address_state()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        # # load all devices need into the controller so that they are available for use in the configuration
        try:
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                             ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        set_up_programs
        assign zones to programs \n
        set up primary linked zones \n
        give each zone a run time of 1 hour and 30 minutes \n
        give each program a start time of 8:00 A.M. \n
        must make zone 200 a primary zone before you can link zones to it \n
        assign sensor to primary zone 1
        #assign moisture sensors to a primary zone 1 and set to lower limit watering
        """
        # this is set in the PG3200 object
        # TODO need to have concurrent zones per program added

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        program_8am_start_time = [480]
        mon_wed_fri_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
        every_day_watering_days = [1, 1, 1, 1, 1, 1, 1]  # run every day
        program_number_1_water_windows = ['011111100001111111111110']
        program_full_open_water_windows = ['111111111111111111111111']
        program_number_closed_water_windows = ['000000000000000000000000']

        try:
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _ww=program_number_1_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.week_days,
                                             _di=None,
                                             _wd=mon_wed_fri_watering_days,
                                             _sm=[],
                                             _st=program_8am_start_time,
                                             _ml=1,
                                             _bp='')
            self.config.programs[2] = PG3200(_ad=2,
                                             _en=opcodes.true,
                                             _ww=program_full_open_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.odd_day,
                                             _di=None,
                                             _wd=every_day_watering_days,
                                             _sm=[],
                                             _st=program_8am_start_time,
                                             _ml=2,
                                             _bp='')
            self.config.programs[3] = PG3200(_ad=3,
                                             _en=opcodes.true,
                                             _ww=program_full_open_water_windows,
                                             _pr=3,
                                             _mc=4,
                                             _sa=100,
                                             _ci=opcodes.week_days,
                                             _di=None,
                                             _wd=every_day_watering_days,
                                             _sm=[],
                                             _st=program_8am_start_time,
                                             _ml=3,
                                             _bp='')
            self.config.programs[4] = PG3200(_ad=4,
                                             _en=opcodes.true,
                                             _ww=program_full_open_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.historical_calendar,
                                             _di=None,
                                             _wd=every_day_watering_days,
                                             _sm=[],
                                             _st=program_8am_start_time,
                                             _ml=4,
                                             _bp='')
            self.config.programs[5] = PG3200(_ad=5,
                                             _en=opcodes.true,
                                             _ww=program_full_open_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.historical_calendar,
                                             _di=None,
                                             _wd=every_day_watering_days,
                                             _sm=[],
                                             _st=program_8am_start_time,
                                             _ml=5,
                                             _bp='')
            self.config.programs[6] = PG3200(_ad=6,
                                             _en=opcodes.true,
                                             _ww=program_full_open_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.historical_calendar,
                                             _di=None,
                                             _wd=every_day_watering_days,
                                             _sm=[],
                                             _st=program_8am_start_time,
                                             _ml=6,
                                             _bp='')
            self.config.programs[7] = PG3200(_ad=7,
                                             _en=opcodes.true,
                                             _ww=program_full_open_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.historical_calendar,
                                             _di=None,
                                             _wd=every_day_watering_days,
                                             _sm=[],
                                             _st=program_8am_start_time,
                                             _ml=7,
                                             _bp='')
            self.config.programs[8] = PG3200(_ad=8,
                                             _en=opcodes.true,
                                             _ww=program_full_open_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.historical_calendar,
                                             _di=None,
                                             _wd=every_day_watering_days,
                                             _sm=[],
                                             _st=program_8am_start_time,
                                             _ml=7,
                                             _bp='')
            self.config.programs[9] = PG3200(_ad=9,
                                             _en=opcodes.true,
                                             _ww=program_full_open_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.historical_calendar,
                                             _di=None,
                                             _wd=every_day_watering_days,
                                             _sm=[],
                                             _st=program_8am_start_time,
                                             _ml=7,
                                             _bp='')
            self.config.programs[10] = PG3200(_ad=10,
                                              _en=opcodes.true,
                                              _ww=program_number_closed_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.historical_calendar,
                                              _di=None,
                                              _wd=every_day_watering_days,
                                              _sm=[],
                                              _st=program_8am_start_time,
                                              _ml=7,
                                              _bp='')
            self.config.programs[11] = PG3200(_ad=11,
                                              _en=opcodes.true,
                                              _ww=program_number_closed_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.historical_calendar,
                                              _di=None,
                                              _wd=every_day_watering_days,
                                              _sm=[],
                                              _st=program_8am_start_time,
                                              _ml=7,
                                              _bp='')
            self.config.programs[12] = PG3200(_ad=12,
                                              _en=opcodes.true,
                                              _ww=program_number_closed_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.historical_calendar,
                                              _di=None,
                                              _wd=every_day_watering_days,
                                              _sm=[],
                                              _st=program_8am_start_time,
                                              _ml=7,
                                              _bp='')
            self.config.programs[13] = PG3200(_ad=13,
                                              _en=opcodes.true,
                                              _ww=program_number_closed_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.historical_calendar,
                                              _di=None,
                                              _wd=every_day_watering_days,
                                              _sm=[],
                                              _st=program_8am_start_time,
                                              _ml=7,
                                              _bp='')
            self.config.programs[14] = PG3200(_ad=14,
                                              _en=opcodes.true,
                                              _ww=program_number_closed_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.historical_calendar,
                                              _di=None,
                                              _wd=every_day_watering_days,
                                              _sm=[],
                                              _st=program_8am_start_time,
                                              _ml=7,
                                              _bp='')
            self.config.programs[15] = PG3200(_ad=15,
                                              _en=opcodes.true,
                                              _ww=program_number_closed_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.historical_calendar,
                                              _di=None,
                                              _wd=every_day_watering_days,
                                              _sm=[],
                                              _st=program_8am_start_time,
                                              _ml=7,
                                              _bp='')
            self.config.programs[16] = PG3200(_ad=16,
                                              _en=opcodes.true,
                                              _ww=program_number_closed_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.historical_calendar,
                                              _di=None,
                                              _wd=every_day_watering_days,
                                              _sm=[],
                                              _st=program_8am_start_time,
                                              _ml=7,
                                              _bp='')
            self.config.programs[17] = PG3200(_ad=17,
                                              _en=opcodes.true,
                                              _ww=program_number_closed_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.historical_calendar,
                                              _di=None,
                                              _wd=every_day_watering_days,
                                              _sm=[],
                                              _st=program_8am_start_time,
                                              _ml=7,
                                              _bp='')
            self.config.programs[18] = PG3200(_ad=18,
                                              _en=opcodes.true,
                                              _ww=program_number_closed_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.historical_calendar,
                                              _di=None,
                                              _wd=every_day_watering_days,
                                              _sm=[],
                                              _st=program_8am_start_time,
                                              _ml=7,
                                              _bp='')
            self.config.programs[19] = PG3200(_ad=18,
                                              _en=opcodes.true,
                                              _ww=program_number_closed_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.historical_calendar,
                                              _di=None,
                                              _wd=every_day_watering_days,
                                              _sm=[],
                                              _st=program_8am_start_time,
                                              _ml=7,
                                              _bp='')
            self.config.programs[20] = PG3200(_ad=18,
                                              _en=opcodes.true,
                                              _ww=program_number_closed_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.historical_calendar,
                                              _di=None,
                                              _wd=every_day_watering_days,
                                              _sm=[],
                                              _st=program_8am_start_time,
                                              _ml=7,
                                              _bp='')

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        setup start stop pause conditions
        """
        # this is set in the PG3200 object
        # TODO need to have concurrent zones per program added

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        # Start conditions
        try:
            self.config.program_start_conditions[1] = StartConditionFor3200(program_ad=1)
            self.config.program_start_conditions[1].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.lower_limit, threshold=20.0)
            self.config.program_start_conditions[2] = StartConditionFor3200(program_ad=2)
            self.config.program_start_conditions[2].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.closed)
            self.config.program_start_conditions[3] = StartConditionFor3200(program_ad=3)
            self.config.program_start_conditions[3].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.lower_limit, threshold=98.6)
            # Stop conditions
            self.config.program_stop_conditions[1] = StopConditionFor3200(program_ad=4)
            self.config.program_stop_conditions[1].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.upper_limit, threshold=20.0)
            self.config.program_stop_conditions[2] = StopConditionFor3200(program_ad=5)
            self.config.program_stop_conditions[2].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.closed)
            self.config.program_stop_conditions[3] = StopConditionFor3200(program_ad=6)
            self.config.program_stop_conditions[3].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.upper_limit, threshold=98.6)

            # Pause conditions
            self.config.program_pause_conditions[1] = PauseConditionFor3200(program_ad=7)
            self.config.program_pause_conditions[1].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.upper_limit, threshold=20.0)
            self.config.program_pause_conditions[2] = PauseConditionFor3200(program_ad=8)
            self.config.program_pause_conditions[2].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.closed)
            self.config.program_pause_conditions[3] = PauseConditionFor3200(program_ad=9)
            self.config.program_pause_conditions[3].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.upper_limit, threshold=98.6)

            # Two more start conditions
            self.config.program_start_conditions[4] = StartConditionFor3200(program_ad=10)
            self.config.program_start_conditions[4].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.lower_limit, threshold=20.0)
            self.config.program_start_conditions[5] = StartConditionFor3200(program_ad=11)
            self.config.program_start_conditions[5].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.lower_limit, threshold=20.0)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        # Zone Programs
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)

            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=2,
                                                       _ms=1)

            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[3],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=3,
                                                       _ms=2)

            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[4],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=4,
                                                       _ms=3)

            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[5],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=5)

            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[6],
                                                       prog_obj=self.config.programs[6],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=6)

            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[7],
                                                       prog_obj=self.config.programs[7],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=7)

            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[8],
                                                       prog_obj=self.config.programs[8],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=8)
            # Zone Programs
            self.config.zone_programs[9] = ZoneProgram(zone_obj=self.config.zones[9],
                                                       prog_obj=self.config.programs[9],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=9)

            self.config.zone_programs[10] = ZoneProgram(zone_obj=self.config.zones[10],
                                                        prog_obj=self.config.programs[10],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=10)

            self.config.zone_programs[11] = ZoneProgram(zone_obj=self.config.zones[11],
                                                        prog_obj=self.config.programs[11],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=11)

            self.config.zone_programs[12] = ZoneProgram(zone_obj=self.config.zones[12],
                                                        prog_obj=self.config.programs[12],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=12)

            self.config.zone_programs[13] = ZoneProgram(zone_obj=self.config.zones[13],
                                                        prog_obj=self.config.programs[13],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=13)

            self.config.zone_programs[14] = ZoneProgram(zone_obj=self.config.zones[14],
                                                        prog_obj=self.config.programs[14],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=14)

            self.config.zone_programs[15] = ZoneProgram(zone_obj=self.config.zones[15],
                                                        prog_obj=self.config.programs[15],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=15,
                                                        _ms=3)

            self.config.zone_programs[16] = ZoneProgram(zone_obj=self.config.zones[16],
                                                        prog_obj=self.config.programs[16],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=16)

            self.config.zone_programs[17] = ZoneProgram(zone_obj=self.config.zones[17],
                                                        prog_obj=self.config.programs[17],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=17)

            self.config.zone_programs[18] = ZoneProgram(zone_obj=self.config.zones[18],
                                                        prog_obj=self.config.programs[18],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=18)
            # Zone Programs
            self.config.zone_programs[19] = ZoneProgram(zone_obj=self.config.zones[19],
                                                        prog_obj=self.config.programs[19],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=19)

            self.config.zone_programs[20] = ZoneProgram(zone_obj=self.config.zones[20],
                                                        prog_obj=self.config.programs[10],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=20)

            self.config.zone_programs[21] = ZoneProgram(zone_obj=self.config.zones[21],
                                                        prog_obj=self.config.programs[20],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=21)

            self.config.zone_programs[22] = ZoneProgram(zone_obj=self.config.zones[22],
                                                        prog_obj=self.config.programs[20],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=22)

            self.config.zone_programs[23] = ZoneProgram(zone_obj=self.config.zones[23],
                                                        prog_obj=self.config.programs[20],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=23)

            self.config.zone_programs[24] = ZoneProgram(zone_obj=self.config.zones[24],
                                                        prog_obj=self.config.programs[20],
                                                        _rt=900,
                                                        _ct=300,
                                                        _so=300,
                                                        _pz=24)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        set_mainlines_3200
        set up main line 1 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 2 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 2 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 3 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 4 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 5 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 6 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 7\n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        try:
            self.config.mainlines[1] = Mainline(_ad=1,
                                                _ft=4,
                                                _fl=500,
                                                _lc=opcodes.true,
                                                _hv=5,
                                                _hs=opcodes.true,
                                                _lv=20,
                                                _ls=opcodes.true)
            self.config.mainlines[2] = Mainline(_ad=2,
                                                _ft=4,
                                                _fl=500,
                                                _lc=opcodes.true,
                                                _hv=5,
                                                _hs=opcodes.true,
                                                _lv=20,
                                                _ls=opcodes.true)
            self.config.mainlines[3] = Mainline(_ad=3,
                                                _ft=4,
                                                _fl=500,
                                                _lc=opcodes.true,
                                                _hv=5,
                                                _hs=opcodes.true,
                                                _lv=20,
                                                _ls=opcodes.true)
            self.config.mainlines[4] = Mainline(_ad=4,
                                                _ft=4,
                                                _fl=500,
                                                _lc=opcodes.true,
                                                _hv=5,
                                                _hs=opcodes.true,
                                                _lv=20,
                                                _ls=opcodes.true)
            self.config.mainlines[5] = Mainline(_ad=5,
                                                _ft=1,
                                                _fl=50,
                                                _lc=opcodes.true,
                                                _hv=20,
                                                _hs=opcodes.false,
                                                _lv=5,
                                                _ls=opcodes.false)
            self.config.mainlines[6] = Mainline(_ad=6,
                                                _ft=1,
                                                _fl=50,
                                                _lc=opcodes.true,
                                                _hv=20,
                                                _hs=opcodes.false,
                                                _lv=5,
                                                _ls=opcodes.false)
            self.config.mainlines[7] = Mainline(_ad=7,
                                                _ft=1,
                                                _fl=50,
                                                _lc=opcodes.true,
                                                _hv=20,
                                                _hs=opcodes.false,
                                                _lv=5,
                                                _ls=opcodes.false)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        set_poc_3200
        set up POC 1 \n
            enable POC 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            assign POC 1 a target flow of 500 \n
            assign POC 1 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 2 \n
            enable POC 2 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 2 \n
            assign POC 2 a target flow of 500 \n
            assign POC 2 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 3 \n
            enable POC 3 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 3 \n
            assign POC 3 a target flow of 500 \n
            assign POC 3 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 4 \n
            enable POC 4 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 4 \n
            assign POC 4 a target flow of 500 \n
            assign POC 4 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 5 \n
            enable POC 5 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 5 \n
            assign POC 5 a target flow of 500 \n
            assign POC 5 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 6 \n
            enable POC 6 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 6 \n
            assign POC 6 a target flow of 500 \n
            assign POC 6 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 7 \n
            enable POC 7 \n
            assign master valve TMV0004 and flow meter TWF0004 to POC 7 \n
            assign POC 7 a target flow of 50 \n
            assign POC 7 to main line 8 \n
            set POC priority to 3-low \n
            set high flow limit to 75 and disable high flow shut down \n
            set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
            set water budget to 1000 and disable water budget shut down \n
            disable water rationing \n
            assign event switch TPD0001 to POC 8 \n
            set switch empty condition to closed \n
            set empty wait time to 540 minutes \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.poc[1] = POC3200(
                _ad=1,
                _en=opcodes.true,
                _mv=1,
                _fm=1,
                _fl=500,
                _ml=1,
                _pr=2,
                _hf=550,
                _hs=opcodes.true,
                _uf=10,
                _us=opcodes.true,
                _wb=100000,
                _ws=opcodes.true,
                _wr=opcodes.true
            )
            self.config.poc[2] = POC3200(
                _ad=2,
                _en=opcodes.true,
                _mv=2,
                _fm=2,
                _fl=500,
                _ml=2,
                _pr=2,
                _hf=550,
                _hs=opcodes.true,
                _uf=10,
                _us=opcodes.true,
                _wb=100000,
                _ws=opcodes.true,
                _wr=opcodes.true
            )
            self.config.poc[3] = POC3200(
                _ad=3,
                _en=opcodes.true,
                _mv=3,
                _fm=3,
                _fl=500,
                _ml=3,
                _pr=2,
                _hf=550,
                _hs=opcodes.true,
                _uf=10,
                _us=opcodes.true,
                _wb=100000,
                _ws=opcodes.true,
                _wr=opcodes.true
            )
            self.config.poc[4] = POC3200(
                _ad=4,
                _en=opcodes.true,
                _mv=4,
                _fm=4,
                _fl=500,
                _ml=4,
                _pr=2,
                _hf=550,
                _hs=opcodes.true,
                _uf=10,
                _us=opcodes.true,
                _wb=100000,
                _ws=opcodes.true,
                _wr=opcodes.true
            )
            self.config.poc[5] = POC3200(
                _ad=5,
                _en=opcodes.true,
                _mv=5,
                _fm=5,
                _fl=500,
                _ml=5,
                _pr=2,
                _hf=550,
                _hs=opcodes.true,
                _uf=10,
                _us=opcodes.true,
                _wb=100000,
                _ws=opcodes.true,
                _wr=opcodes.true,
                _sw=1
            )

            # moisture_stop = Stat()
            # moisture_stop.set_as_ms
            self.config.poc[6] = POC3200(
                _ad=6,
                _en=opcodes.true,
                _mv=6,
                _fm=6,
                _fl=500,
                _ml=6,
                _pr=2,
                _hf=550,
                _hs=opcodes.true,
                _uf=10,
                _us=opcodes.true,
                _wb=100000,
                _ws=opcodes.true,
                _wr=opcodes.true
            )

            self.config.poc[7] = POC3200(
                _ad=7,
                _en=opcodes.true,
                _mv=7,
                _fm=7,
                _fl=50,
                _ml=7,
                _pr=3,
                _hf=75,
                _hs=opcodes.false,
                _uf=5,
                _us=opcodes.false,
                _wb=1000,
                _ws=opcodes.false,
                _wr=opcodes.false,
                _sw=3,
                _se=opcodes.closed,
                _ew=540
            )

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        :return:
        :rtype:
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            # time.sleep(20)
            self.config.verify_full_configuration()
            # this turns off fast sim mode and start the clock

            self.config.controllers[1].turn_off_echo()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_current_date_to_match_computer()
            # By setting a time on the controller we can know what time each message has been set
            self.config.controllers[1].set_date_and_time_on_cn(
                _date=date_mngr.curr_computer_date.date_string_for_controller(),
                _time=date_mngr.curr_computer_date.time_string_for_controller())

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        this sets the zone messages
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.zones[1].set_message_on_cn(opcodes.bad_serial)
            self.config.zones[1].verify_message_on_cn(opcodes.bad_serial)
            self.config.zones[15].set_message_on_cn(opcodes.no_24_vac)
            self.config.zones[15].verify_message_on_cn(opcodes.no_24_vac)
            self.config.zones[16].set_message_on_cn(opcodes.no_response)
            self.config.zones[16].verify_message_on_cn(opcodes.no_response)
            self.config.zones[17].set_message_on_cn(opcodes.open_circuit)
            self.config.zones[17].verify_message_on_cn(opcodes.open_circuit)
            self.config.zones[18].set_message_on_cn(opcodes.short_circuit)
            self.config.zones[18].verify_message_on_cn(opcodes.short_circuit)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        this sets message for zones that require a program
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.zone_programs[2].set_message_on_cn(opcodes.calibrate_failure_no_change)
            self.config.zone_programs[2].verify_message_on_cn(opcodes.calibrate_failure_no_change)
            self.config.zone_programs[3].set_message_on_cn(opcodes.calibrate_successful)
            self.config.zone_programs[3].verify_message_on_cn(opcodes.calibrate_successful)
            self.config.zone_programs[4].set_message_on_cn(opcodes.calibrate_failure_no_saturation)
            self.config.zone_programs[4].verify_message_on_cn(opcodes.calibrate_failure_no_saturation)
            self.config.zone_programs[15].set_message_on_cn(opcodes.requires_soak_cycle)
            self.config.zone_programs[15].verify_message_on_cn(opcodes.requires_soak_cycle)
            self.config.zone_programs[5].set_message_on_cn(opcodes.exceeds_design_flow)
            self.config.zone_programs[5].verify_message_on_cn(opcodes.exceeds_design_flow)
            self.config.zone_programs[6].set_message_on_cn(opcodes.flow_learn_errors)
            self.config.zone_programs[6].verify_message_on_cn(opcodes.flow_learn_errors)
            self.config.zone_programs[7].set_message_on_cn(opcodes.flow_learn_ok)
            self.config.zone_programs[7].verify_message_on_cn(opcodes.flow_learn_ok)
            self.config.zone_programs[8].set_message_on_cn(opcodes.high_flow_shutdown_by_flow_station)
            self.config.zone_programs[8].verify_message_on_cn(opcodes.high_flow_shutdown_by_flow_station)
            self.config.zone_programs[9].set_message_on_cn(opcodes.high_flow_variance_shutdown)
            self.config.zone_programs[9].verify_message_on_cn(opcodes.high_flow_variance_shutdown)
            self.config.zone_programs[10].set_message_on_cn(opcodes.low_flow_shutdown_by_flow_station)
            self.config.zone_programs[10].verify_message_on_cn(opcodes.low_flow_shutdown_by_flow_station)
            self.config.zone_programs[11].set_message_on_cn(opcodes.low_flow_variance_shutdown)
            self.config.zone_programs[11].verify_message_on_cn(opcodes.low_flow_variance_shutdown)
            self.config.zone_programs[12].set_message_on_cn(opcodes.high_flow_variance_detected)
            self.config.zone_programs[12].verify_message_on_cn(opcodes.high_flow_variance_detected)
            self.config.zone_programs[13].set_message_on_cn(opcodes.low_flow_variance_detected)
            self.config.zone_programs[13].verify_message_on_cn(opcodes.low_flow_variance_detected)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        set messages for the temperature sensor
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.temperature_sensors[1].set_message_on_cn(opcodes.bad_serial)
            self.config.temperature_sensors[1].verify_message_on_cn(opcodes.bad_serial)
            self.config.temperature_sensors[2].set_message_on_cn(opcodes.no_response)
            self.config.temperature_sensors[2].verify_message_on_cn(opcodes.no_response)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        set messages for the flow meter
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.flow_meters[1].set_message_on_cn(opcodes.bad_serial)
            self.config.flow_meters[1].verify_message_on_cn(opcodes.bad_serial)
            self.config.flow_meters[2].set_message_on_cn(opcodes.no_response)
            self.config.flow_meters[2].verify_message_on_cn(opcodes.no_response)
            self.config.flow_meters[3].set_message_on_cn(opcodes.set_upper_limit_failed)
            self.config.flow_meters[3].verify_message_on_cn(opcodes.set_upper_limit_failed)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        set messages for event switch
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.event_switches[1].set_message_on_cn(opcodes.bad_serial)
            self.config.event_switches[1].verify_message_on_cn(opcodes.bad_serial)
            self.config.event_switches[2].set_message_on_cn(opcodes.no_response)
            self.config.event_switches[2].verify_message_on_cn(opcodes.no_response)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        set messages for master valves
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.master_valves[1].set_message_on_cn(opcodes.bad_serial)
            self.config.master_valves[1].verify_message_on_cn(opcodes.bad_serial)
            self.config.master_valves[2].set_message_on_cn(opcodes.no_response)
            self.config.master_valves[2].verify_message_on_cn(opcodes.no_response)
            self.config.master_valves[3].set_message_on_cn(opcodes.open_circuit)
            self.config.master_valves[3].verify_message_on_cn(opcodes.open_circuit)
            self.config.master_valves[4].set_message_on_cn(opcodes.short_circuit)
            self.config.master_valves[4].verify_message_on_cn(opcodes.short_circuit)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        set messages for controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].set_message_on_cn(opcodes.commander_paused)
            self.config.controllers[1].verify_message_on_cn(opcodes.commander_paused)
            self.config.controllers[1].set_message_on_cn(opcodes.boot_up)
            self.config.controllers[1].verify_message_on_cn(opcodes.boot_up)
            self.config.controllers[1].set_message_on_cn(opcodes.event_date_stop)
            self.config.controllers[1].verify_message_on_cn(opcodes.event_date_stop)
            self.config.controllers[1].set_message_on_cn(opcodes.usb_flash_storage_failure)
            self.config.controllers[1].verify_message_on_cn(opcodes.usb_flash_storage_failure)
            self.config.controllers[1].set_message_on_cn(opcodes.flow_jumper_stopped)
            self.config.controllers[1].verify_message_on_cn(opcodes.flow_jumper_stopped)
            self.config.controllers[1].set_message_on_cn(opcodes.two_wire_high_current_shutdown)
            self.config.controllers[1].verify_message_on_cn(opcodes.two_wire_high_current_shutdown)
            self.config.controllers[1].set_message_on_cn(opcodes.pause_event_switch,
                                                         self.config.program_pause_conditions[2])
            self.config.controllers[1].verify_message_on_cn(opcodes.pause_event_switch,
                                                            self.config.program_pause_conditions[2])
            self.config.controllers[1].set_message_on_cn(opcodes.pause_jumper)
            self.config.controllers[1].verify_message_on_cn(opcodes.pause_jumper)
            self.config.controllers[1].set_message_on_cn(opcodes.pause_moisture_sensor,
                                                         self.config.program_pause_conditions[1])
            self.config.controllers[1].verify_message_on_cn(opcodes.pause_moisture_sensor,
                                                            self.config.program_pause_conditions[1])
            self.config.controllers[1].set_message_on_cn(opcodes.pause_temp_sensor,
                                                         self.config.program_pause_conditions[3])
            self.config.controllers[1].verify_message_on_cn(opcodes.pause_temp_sensor,
                                                            self.config.program_pause_conditions[3])
            self.config.controllers[1].set_message_on_cn(opcodes.rain_delay_stopped)
            self.config.controllers[1].verify_message_on_cn(opcodes.rain_delay_stopped)
            self.config.controllers[1].set_message_on_cn(opcodes.rain_jumper_stopped)
            self.config.controllers[1].verify_message_on_cn(opcodes.rain_jumper_stopped)
            self.config.controllers[1].set_message_on_cn(opcodes.stop_event_switch,
                                                         self.config.program_stop_conditions[2])
            self.config.controllers[1].verify_message_on_cn(opcodes.stop_event_switch,
                                                            self.config.program_stop_conditions[2])
            self.config.controllers[1].set_message_on_cn(opcodes.stop_moisture_sensor,
                                                         self.config.program_stop_conditions[1])
            self.config.controllers[1].verify_message_on_cn(opcodes.stop_moisture_sensor,
                                                            self.config.program_stop_conditions[1])
            self.config.controllers[1].set_message_on_cn(opcodes.stop_temp_sensor,
                                                         self.config.program_stop_conditions[3])
            self.config.controllers[1].verify_message_on_cn(opcodes.stop_temp_sensor,
                                                            self.config.program_stop_conditions[3])
            # these are new on version 12.31.413 code
            if self.config.controllers[1].vr > '12.31.413':
                self.config.controllers[1].set_message_on_cn(opcodes.restore_failed,
                                                             _helper_object=opcodes.usb_flash_storage_source)
                self.config.controllers[1].verify_message_on_cn(opcodes.restore_failed,
                                                                _helper_object=opcodes.usb_flash_storage_source)
                self.config.controllers[1].set_message_on_cn(opcodes.restore_successful,
                                                             _helper_object=opcodes.basemanager_source)
                self.config.controllers[1].verify_message_on_cn(opcodes.restore_successful,
                                                                _helper_object=opcodes.basemanager_source)
            else:
                pass

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        set messages for mainlines
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.mainlines[1].set_message_on_cn(opcodes.learn_flow_fail_flow_biCoders_disabled)
            self.config.mainlines[1].verify_message_on_cn(opcodes.learn_flow_fail_flow_biCoders_disabled)
            self.config.mainlines[2].set_message_on_cn(opcodes.learn_flow_fail_flow_biCoders_error)
            self.config.mainlines[2].verify_message_on_cn(opcodes.learn_flow_fail_flow_biCoders_error)
            # these messages are not used anymore we removed them from the 3200 in version 12.31.422
            #if coder is older than 12.31.422 than check for these messages
            if self.config.controllers[1].vr < '12.31.422':
                self.config.mainlines[3].set_message_on_cn(opcodes.high_flow_variance_detected)
                self.config.mainlines[3].verify_message_on_cn(opcodes.high_flow_variance_detected)
                self.config.mainlines[4].set_message_on_cn(opcodes.low_flow_variance_detected)
                self.config.mainlines[4].verify_message_on_cn(opcodes.low_flow_variance_detected)
            else:
                try:
                    self.config.mainlines[3].verify_message_on_cn(opcodes.high_flow_variance_detected)
                except Exception, e:
                    if e.message != 'Exception caught in messages.get_message: BC response received from controller':
                        e_msg = 'Controller should have returned us a BC response received from controller but it ' \
                                'did not'
                        raise Exception, Exception(e_msg), sys.exc_info()[2]
                try:
                    self.config.mainlines[4].verify_message_on_cn(opcodes.low_flow_variance_detected)
                except Exception, e:
                    if e.message != 'Exception caught in messages.get_message: BC response received from controller':
                        e_msg = 'Controller should have returned us a BC response received from controller but it ' \
                                'did not'
                        raise Exception, Exception(e_msg), sys.exc_info()[2]

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        set messages for moisture sensors
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.moisture_sensors[1].set_message_on_cn(opcodes.bad_serial)
            self.config.moisture_sensors[1].verify_message_on_cn(opcodes.bad_serial)
            self.config.moisture_sensors[2].set_message_on_cn(opcodes.sensor_disabled)
            self.config.moisture_sensors[2].verify_message_on_cn(opcodes.sensor_disabled)
            self.config.moisture_sensors[3].set_message_on_cn(opcodes.no_response)
            self.config.moisture_sensors[3].verify_message_on_cn(opcodes.no_response)
            self.config.moisture_sensors[4].set_message_on_cn(opcodes.zero_reading)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        set messages for point of connection
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.poc[1].set_message_on_cn("BD")
            self.config.poc[1].verify_message_on_cn("BD")
            self.config.poc[2].set_message_on_cn(opcodes.budget_exceeded_shutdown)
            self.config.poc[2].verify_message_on_cn(opcodes.budget_exceeded_shutdown)
            self.config.poc[3].set_message_on_cn(opcodes.high_flow_detected)
            self.config.poc[3].verify_message_on_cn(opcodes.high_flow_detected)
            self.config.poc[4].set_message_on_cn(opcodes.high_flow_shutdown)
            self.config.poc[4].verify_message_on_cn(opcodes.high_flow_shutdown)
            self.config.poc[5].set_message_on_cn(opcodes.empty_shutdown)
            self.config.poc[5].verify_message_on_cn(opcodes.empty_shutdown)
            self.config.poc[6].set_message_on_cn(opcodes.unscheduled_flow_detected)
            self.config.poc[6].verify_message_on_cn(opcodes.unscheduled_flow_detected)
            self.config.poc[7].set_message_on_cn(opcodes.unscheduled_flow_shutdown)
            self.config.poc[7].verify_message_on_cn(opcodes.unscheduled_flow_shutdown)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        set messages for programs
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.programs[1].set_message_on_cn(opcodes.skipped_by_moisture_sensor,
                                                      self.config.program_start_conditions[1])
            self.config.programs[1].verify_message_on_cn(opcodes.skipped_by_moisture_sensor,
                                                         self.config.program_start_conditions[1])
            self.config.programs[2].set_message_on_cn(opcodes.started_event_switch,
                                                      self.config.program_start_conditions[2])
            self.config.programs[2].verify_message_on_cn(opcodes.started_event_switch,
                                                         self.config.program_start_conditions[2])
            self.config.programs[3].set_message_on_cn(opcodes.started_temp_sensor,
                                                      self.config.program_start_conditions[3])
            self.config.programs[3].verify_message_on_cn(opcodes.started_temp_sensor,
                                                         self.config.program_start_conditions[3])
            self.config.programs[4].set_message_on_cn(opcodes.stop_moisture_sensor,
                                                      self.config.program_stop_conditions[1])
            self.config.programs[4].verify_message_on_cn(opcodes.stop_moisture_sensor,
                                                         self.config.program_stop_conditions[1])
            self.config.programs[5].set_message_on_cn(opcodes.stop_event_switch,
                                                      self.config.program_stop_conditions[2])
            self.config.programs[5].verify_message_on_cn(opcodes.stop_event_switch,
                                                         self.config.program_stop_conditions[2])
            self.config.programs[6].set_message_on_cn(opcodes.stop_temp_sensor,
                                                      self.config.program_stop_conditions[3])
            self.config.programs[6].verify_message_on_cn(opcodes.stop_temp_sensor,
                                                         self.config.program_stop_conditions[3])
            self.config.programs[7].set_message_on_cn(opcodes.pause_moisture_sensor,
                                                      self.config.program_pause_conditions[1])
            self.config.programs[7].verify_message_on_cn(opcodes.pause_moisture_sensor,
                                                         self.config.program_pause_conditions[1])
            self.config.programs[8].set_message_on_cn(opcodes.pause_event_switch,
                                                      self.config.program_pause_conditions[2])
            self.config.programs[8].verify_message_on_cn(opcodes.pause_event_switch,
                                                         self.config.program_pause_conditions[2])
            self.config.programs[9].set_message_on_cn(opcodes.pause_temp_sensor,
                                                      self.config.program_pause_conditions[3])
            self.config.programs[9].verify_message_on_cn(opcodes.pause_temp_sensor,
                                                         self.config.program_pause_conditions[3])
            self.config.programs[10].set_message_on_cn(opcodes.started_by_bad_moisture_sensor,
                                                      self.config.program_start_conditions[1])
            self.config.programs[10].verify_message_on_cn(opcodes.started_by_bad_moisture_sensor,
                                                         self.config.program_start_conditions[1])
            self.config.programs[11].set_message_on_cn(opcodes.started_moisture_sensor,
                                                       self.config.program_start_conditions[1])
            self.config.programs[11].verify_message_on_cn(opcodes.started_moisture_sensor,
                                                          self.config.program_start_conditions[1])
            self.config.programs[12].set_message_on_cn(opcodes.event_date_stop)
            self.config.programs[12].verify_message_on_cn(opcodes.event_date_stop)
            self.config.programs[13].set_message_on_cn(opcodes.learn_flow_with_errors)
            self.config.programs[13].verify_message_on_cn(opcodes.learn_flow_with_errors)
            self.config.programs[14].set_message_on_cn(opcodes.learn_flow_success)
            self.config.programs[14].verify_message_on_cn(opcodes.learn_flow_success)
            self.config.programs[15].set_message_on_cn(opcodes.over_run_start_event)
            self.config.programs[15].verify_message_on_cn(opcodes.over_run_start_event)
            self.config.programs[16].set_message_on_cn(opcodes.priority_paused)
            self.config.programs[16].verify_message_on_cn(opcodes.priority_paused)
            self.config.programs[17].set_message_on_cn(opcodes.restricted_time_by_water_ration)
            self.config.programs[17].verify_message_on_cn(opcodes.restricted_time_by_water_ration)
            self.config.programs[18].set_message_on_cn(opcodes.water_window_paused)
            self.config.programs[18].verify_message_on_cn(opcodes.water_window_paused)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        messages for flow station, substation, pumpstation
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        # TODO need to add the two Basemanager messages
        #  701_BM_ET	Weather ETo Data Not Available: Updated ETo data not received from BaseManager. Used Previous Date = 01/11/17 Used Previous ETo = 0
        #  701_BM_MG	BaseManager Text Message

        # TODO add messages for sub station
        # TODO add messages for pump station
        # 801_FS_MG
        # This is a temporary way to test since a flow station is basically a controller
        try:
            self.config.controllers[1].dv_type = opcodes.flow_station
            self.config.controllers[1].set_message_on_cn(opcodes.fallback_mode_booster_pump)
            self.config.controllers[1].verify_message_on_cn(opcodes.fallback_mode_booster_pump)

            self.config.controllers[1].set_message_on_cn(opcodes.error_operation_terminated)
            self.config.controllers[1].verify_message_on_cn(opcodes.error_operation_terminated)

            self.config.controllers[1].set_message_on_cn(opcodes.fallback_mode_program)
            self.config.controllers[1].verify_message_on_cn(opcodes.fallback_mode_program)

            self.config.controllers[1].set_message_on_cn(opcodes.fallback_mode_poc)
            self.config.controllers[1].verify_message_on_cn(opcodes.fallback_mode_poc)

            self.config.controllers[1].dv_type = opcodes.controller

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        send messages from the controller to baseManager by turning of sim mode
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.controllers[1].set_sim_mode_to_off()
        # turn off echo because some of the message packets contain characters like 'ER' that break the parser

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]