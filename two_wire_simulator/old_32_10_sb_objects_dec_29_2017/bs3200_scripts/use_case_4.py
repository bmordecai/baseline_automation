# this import allows us to directly use the date_mngr
import sys
from datetime import time

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram


__author__ = 'Tige'


class ControllerUseCase4(object):
    """
    Test name:
        - Lower Limit One Time Calibration
    purpose:
        - Run a program using a moisture Sensor and have it do a lower limit Calibration cycles
        - verify linked zones run during calibration cycle
        - verify a calibration cycle will fail because the saturation level wasn't met
        - verify a calibration cycle will pass and set the correct values


    Coverage area: \n
        This test covers smart watering with sensors. It covers the one time calibration for moisture sensor water for
        both
            upper and lower limits. \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n

            - do a single lower limit calibration \n
            - Normal calibration
            - calibration that moisture doesnt hit saturation

    Date References:
        - configuration for script is located common\configuration_files\One_time_calibration.json
        - the devices and addresses range is read from the .json file

    """
    # TODO Things to add to the test or to a new test \n
    # TODO calibration that moisture level doesnt move
    # TODO upper limit calibration
    # TODO do a single upper limit calibration \n (this not covered)
    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        Step 1:
            - configure controller: \n
            - configure basemanager: \n
        step 2:
            - setting up devices:
        Step 3:
            - setting up programming:
        step 4:
            - setup zone programs: \n
        Step 5:
            - setting up mainlines: \n
        Step 6:
            - setting up POCs: \n
        Step 7:
            -
        step 8:
            -
        Step 9:
            -
        Step 10:
            -
        Step 11:
            -
        Step 12:
            -
        step 13:
            -
        Step 14:
            -
        Step 15:
            -
        Step 16:
            -
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    self.step_18()
                    self.step_19()
                    self.step_20()
                    self.step_21()
                    self.step_22()
                    self.step_23()
                    self.step_24()
                    self.step_25()
                    self.step_26()
                    self.step_27()
                    self.step_28()
                    self.step_29()
                    self.step_30()
                    self.step_31()
                    self.step_32()
                    self.step_33()
                    self.step_34()
                    self.step_35()
                    self.step_36()
                    self.step_37()
                    self.step_38()
                    self.step_39()
                    self.step_40()
                    self.step_41()
                    self.step_42()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise

        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()
            # only need this for BaseManager
            # self.config.basemanager_connection[1].verify_ip_address_state()
            # one of the method takes about 70 minutes to complete so the timeout had to be extended
            self.config.controllers[1].set_serial_port_timeout(timeout=5040)  # set time out on serial port 84 minutes
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_2(self):
        """
        load all devices need into the controller so that they are available for use in the configuration \n
        sets the devices that will be used in the configuration of the controller \n
            - search and address the devices: \n
               - zones                 {zn} \n
               - Master Valves         {mv} \n
            - once the devices are found they can be addressed so that they can be used in the programming \n
               - zones can use addresses {1-200} \n
                - Master Valves can use address {1-8} \n
            - the 3200 auto address certain devices in the order it receives them: \n
                - Master Valves         {mv} \n
                - Moisture Sensors      {ms} \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26)  # set initial value for moisture sensor to 26%
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_3(self):
        """
        set_up_programs \n
            - assign zones to programs \n
            - set up primary linked zones \n
            - give each zone a run time of 1 hour and 30 minutes \n
            - give each program a start time of 8:00 A.M. \n
            - must make zone 200 a primary zone before you can link zones to it \n
            - assign sensor to primary zone 1
            - assign moisture sensors to a primary zone 1 and set to lower limit watering
        """
        # this is set in the PG3200 object
        # TODO need to have concurrent zones per program added

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        program_8am_start_time = [480]
        every_day_watering_days = [1, 1, 1, 1, 1, 1, 1]  # run every day
        program_full_open_water_windows = ['111111111111111111111111']
        try:
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _ww=program_full_open_water_windows,
                                             _pr=1,
                                             _mc=1,        # max concurrent zones per program
                                             _sa=100,
                                             _ci=opcodes.week_days,
                                             _di=None,
                                             _wd=every_day_watering_days,
                                             _sm=[],
                                             _st=program_8am_start_time,
                                             _ml=1,
                                             _bp='')

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_4(self):
        # Zone Programs

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _ct=600,
                                                       _so=1200,
                                                       _pz=1,
                                                       _ms=1,     # moisture sensor address assigned to primary zone
                                                       _ws=opcodes.lower_limit,  # set to lower limit watering
                                                       _cc=opcodes.calibrate_one_time)    # set to calibrate one time

            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _pz=1,
                                                       _ra=100)

            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _pz=1,
                                                       _ra=50)

            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _pz=1,
                                                       _ra=150)

            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[1],
                                                       _pz=1,
                                                       _ra=100)
            self.config.verify_full_configuration()

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_5(self):
        """
        set_mainlines_3200 \n
            - set up main line 1 \n
                - set limit zones by flow to true \n
                - set the pipe fill time to 4 minutes \n
                - set the target flow to 500 \n
                - set the high variance limit to 5% and enable the high variance shut down \n
                - set the low variance limit to 20% and enable the low variance shut down \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.mainlines[1] = Mainline(_ad=1,
                                                _ft=4,
                                                _fl=500,
                                                _lc=opcodes.true,
                                                _hv=5,
                                                _hs=opcodes.true,
                                                _lv=20,
                                                _ls=opcodes.true)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_6(self):
        """
        set_poc_3200 \n
            - set up POC 1 \n
                - enable POC 1 \n
                - assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
                - assign POC 1 a target flow of 500 \n
                - assign POC 1 to main line 1 \n
                - set POC priority to 2-medium \n
                - set high flow limit to 550 and enable high flow shut down \n
                - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                - set water budget to 100000 and enable the water budget shut down \n
                - enable water rationing \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1] = POC3200(
                _ad=1,
                _en=opcodes.true,
                _mv=1,
                _fm=1,
                _fl=500,
                _ml=1,
                _pr=2,
                _hf=550,
                _hs=opcodes.true,
                _uf=10,
                _us=opcodes.true,
                _wb=100000,
                _ws=opcodes.true,
                _wr=opcodes.true
            )

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

        self.config.verify_full_configuration()

    def step_7(self):
        """
        - how it works run cycle time than soak time than take reading
        - set the controller date and time so that there is a known days of the week and days of the month \n
        - verify that all zones are working properly by verifying them before starting the test \n
        - start the moisture level at 20% and raise it to 25.5%
        - verify that the primary zones shut off
        - Verify that all link zone shut off
        - each zone has a 30 run time and a 20 minute cycle and a 30 minute soak
        - zone 3 is 50% watering time set to 15 minutes and 10 minute cycle time and a 30 minute soak
        - zone 4 is 150% watering time set to 45 minutes and 30 minute cycle time and a 30 minute soak
        when you first install a sensor you must wait 24 hours in order to do a calibration cycle \n
        set the moisture sensor to be below the upper limit \n
        the upper limit is set to 25 \n
        program 1 has a start time of 8:00 am \n
        set the clock to 7:45 and then increment time to verify the program starts \n
        do a self test on the moisture sensor so that the 3200 will read the sensors and update there current values \n
        verify that none of the zones have started watering \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(24.0)
            self.config.moisture_sensors[1].do_self_test()
            self.config.controllers[1].set_date_and_time_on_cn(_date='08/27/2014', _time='23:45:00')
            self.config.controllers[1].verify_date_and_time()
            self.config.controllers[1].do_increment_clock(minutes=20)
            self.config.controllers[1].verify_date_and_time()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)
        try:
            self.config.controllers[1].set_date_and_time_on_cn(_date='08/28/2014', _time='7:53:00')
            self.config.controllers[1].do_increment_clock(minutes=6)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_8(self):
        """
        set moisture sensor values so the programs can start watering and not be effected \n
        set moisture sensor value to 24% \n
        Do a self test to update the value in the controller \n
        advance the clock 2 minutes to make the time 8:01 this should start the zones watering \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(24.0)
            self.config.moisture_sensors[1].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=2)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[2].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_9(self):
        """
        advance the clock 10 minutes to make the time 8:11 \n
        set the moisture sensor to 24.3% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:

            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(24.3)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.watering)
            self.config.zones[3].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_10(self):
        """
        advance the clock 10 minutes to make the time 8:21 \n
        set the moisture sensor to 24.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(24.5)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(opcodes.watering)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_11(self):
        """
        advance the clock 10 minutes to make the time 8:31 \n
        set the moisture sensor to 25.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 watering \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(25.0)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(opcodes.watering)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_12(self):
        """
        advance the clock 10 minutes to make the time 8:41 \n
        set the moisture sensor to 25.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(25.5)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[2].verify_status_on_cn(opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_13(self):
        """
        advance the clock 10 minutes to make the time 8:51 \n
        set the moisture sensor to 25.6% \n
        Do a self test to update the value in the controller \n
        beacause saturation was not hit zone 1 is set to soaking \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(25.6)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.watering)
            self.config.zones[3].verify_status_on_cn(opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_14(self):
        """
        advance the clock 10 minutes to make the time 9:01 \n
        set the moisture sensor to 26.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 soaking \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.0)
            self.config.moisture_sensors[1].do_self_test()
            time.sleep(10)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(opcodes.watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_15(self):
        """
        advance the clock 10 minutes to make the time 9:11 \n
        set the moisture sensor to 26.1% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.1)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.watering)
            self.config.zones[5].verify_status_on_cn(opcodes.soaking)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_16(self):
        """
        advance the clock 10 minutes to make the time 9:21 \n
        set the moisture sensor to 26.3% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.3)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.soaking)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_17(self):
        """
        advance the clock 10 minutes to make the time 9:27 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=6)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.soaking)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_18(self):
        """
        Check the message on the controller
        Check the message on the controller
        - expected =    (DT = 08/29/14 22:00:00'),
                        (ID = 001_ZN_1_CT
                        (TX = Zone 1: SB05308\n
                        Moisture biSensor Calibration Failed:\n
                        Did not reach Saturation.\n
                        Primary Zone = 1\n
                        Moisture biSensor = SB05308\n
                        Moisture = 26.3 \n),
                        ('PR', 'NN'), ('V1', '20.125'), ('V2', '25.125'), ('PC', '1'), ('PG', '1'), ('ZN', '1')]
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.zone_programs[1].verify_message_on_cn(opcodes.calibrate_failure_no_saturation)
            # this clears the message and then verifies that it was cleared
            self.config.zone_programs[1].clear_message_on_cn(opcodes.calibrate_failure_no_saturation)
            print ('all is good')
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_19(self):
        """
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        advance the clock 10 minutes to make the time 9:41 \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.4)
            self.config.moisture_sensors[1].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=14)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.5)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_20(self):
        """
        Zone Programs \n
            - the controller failed a moisture sensor calibration  because saturation was not reached so the runtime \n
            - is increased so that the saturation can be reached the runtime went from 900 seconds to 1800 seconds \n
            - but the cycle and soak time remained the same also reset primary zone to calibrate one time \n
            - the zone program objects have to be reinitialized in order get all attributes reset \n
        Verify full configuration to verify everything got set correctly \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=1800,
                                                       _ct=600,
                                                       _so=1200,
                                                       _pz=1,
                                                       _ms=1,  # moisture sensor address assigned to primary zone
                                                       _ws=opcodes.lower_limit,  # set to lower limit watering
                                                       _cc=opcodes.calibrate_one_time)  # set to calibrate one time

            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _pz=1,
                                                       _ra=100)

            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _pz=1,
                                                       _ra=50)

            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _pz=1,
                                                       _ra=150)

            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[1],
                                                       _pz=1,
                                                       _ra=100)
            self.config.verify_full_configuration()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_21(self):
        """
        set the moisture sensor to be below the upper limit \n
        the upper limit is set to 25 \n
        program 1 has a start time of 8:00 am \n
        set the clock to 7:45 and then increment time to verify the program starts \n
        do a self test on the moisture sensor so that the 3200 will read the sensors and update there current values \n
        verify that none of the zones have started watering \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(24.0)
            self.config.moisture_sensors[1].do_self_test()
            self.config.controllers[1].set_date_and_time_on_cn(_date='08/28/2014', _time='23:45:00')
            self.config.controllers[1].do_increment_clock(minutes=20)
            self.config.controllers[1].verify_date_and_time()
            self.config.controllers[1].set_date_and_time_on_cn(_date='08/29/2014', _time='7:53:00')
            self.config.controllers[1].do_increment_clock(minutes=6)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_22(self):
        """
        set moisture sensor values so the programs can start watering and not be effected \n
        set moisture sensor value to 24% \n
        Do a self test to update the value in the controller \n
        advance the clock 2 minutes to make the time 8:01 this should st art the zones watering \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(24.0)
            self.config.moisture_sensors[1].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=2)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[2].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_23(self):
        """
        advance the clock 10 minutes to make the time 8:11 \n
        set the moisture sensor to 24.3% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(24.3)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.watering)
            self.config.zones[3].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_24(self):
        """
        advance the clock 10 minutes to make the time 8:21 \n
        set the moisture sensor to 24.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "Running " + sys._getframe().f_code.co_name
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(24.5)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(opcodes.watering)
            self.config.zones[4].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_25(self):
        """
        advance the clock 10 minutes to make the time 8:31 \n
        set the moisture sensor to 25.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 watering \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(25.0)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(opcodes.watering)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_26(self):
        """
        advance the clock 10 minutes to make the time 8:41 \n
        set the moisture sensor to 25.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(25.5)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[2].verify_status_on_cn(opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_27(self):
        """
        advance the clock 10 minutes to make the time 8:51 \n
        set the moisture sensor to 25.6% \n
        Do a self test to update the value in the controller \n
        beacause saturation was not hit zone 1 is set to soaking \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(25.6)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.watering)
            self.config.zones[3].verify_status_on_cn(opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_28(self):
        """
        advance the clock 10 minutes to make the time 9:01 \n
        set the moisture sensor to 26.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.0)
            self.config.moisture_sensors[1].do_self_test()
            time.sleep(10)
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(opcodes.watering)
            self.config.zones[4].verify_status_on_cn(opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_29(self):
        """
        advance the clock 10 minutes to make the time 9:11 \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 watering \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.1)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(opcodes.watering)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_30(self):
        """
        advance the clock 10 minutes to make the time 9:21 \n
        set the moisture sensor to 26.3% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program waiting to water \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.5)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[2].verify_status_on_cn(opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_31(self):
        """
        advance the clock 10 minutes to make the time 9:31 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.4)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.watering)
            self.config.zones[3].verify_status_on_cn(opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_32(self):
        """
        advance the clock 10 minutes to make the time 9:41 \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 done watering \n
            - zone 3 watering \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=10)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.5)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.watering)
            self.config.zones[4].verify_status_on_cn(opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_33(self):
        """
        advance the clock 7 minutes to make the time 9:48 \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=7)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.5)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.watering)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_34(self):
        """
        advance the clock 13 minutes to make the time 10:01 \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=13)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.5)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.waiting_to_water)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_35(self):
        """
        advance the clock 6 minutes to make the time 10:07 \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(minutes=6)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.5)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_36(self):
        """
        advance the clock 1 hour and 11 minutes to make the time 11:18 \n
        increment to completely finish last zone \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(hours=1, minutes=11)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.5)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_37(self):
        """
        advance the clock 2 hours to make the time 13:18 \n
        set the moisture sensor to 26.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(hours=2)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(26.0)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_38(self):
        """
        advance the clock 2 hours to make the time 15:18 \n
        set the moisture sensor to 25.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.controllers[1].do_increment_clock(hours=2)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(25.5)
            self.config.moisture_sensors[1].do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_39(self):
        """
        set the moisture sensor to 24.0% \n
        Do a self test to update the value in the controller \n
        advance the clock 6 hours 43 minutes to make the time 22:01 \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(24.0)
            self.config.moisture_sensors[1].do_self_test()
            self.config.controllers[1].do_increment_clock(hours=6, minutes=43)
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[1].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_40(self):
        """
        Need to set the object to have the correct upper and lower limits this will allow us to verify the correct
        values that the controller set for both upper and lower limits
        also have to resent the calibration cycle to NV because the calibration completed
        Check the message on the controller
        expected =  (DT = 08/29/14 22:00:00'),
                    (ID = 001_ZN_1_CS
                    (TX = Zone 1: SB05308\n
                    Moisture biSensor Calibration Done:\n
                    New watering limits set.\n
                    Primary Zone = 1\n
                    Moisture biSensor = SB05308\n
                    Limit Lower/Upper = 20.125n / 25.125),
                    ('PR', 'NN'), ('V1', '20.125'), ('V2', '25.125'), ('PC', '1'), ('PG', '1'), ('ZN', '1')]
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zone_programs[1].ll = 20.125
            self.config.zone_programs[1].ul = 25.125
            self.config.zone_programs[1].cc = 'NV'
            self.config.zone_programs[1].verify_message_on_cn(opcodes.calibrate_successful)
            print ('all is good')
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_41(self):
        """
        compare each zones information with the expected information to verify that everything is correct \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
             self.config.verify_full_configuration()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_42(self):
        """
        send messages from the controller to baseManager by turning of sim mode
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        self.config.controllers[1].set_sim_mode_to_off()
