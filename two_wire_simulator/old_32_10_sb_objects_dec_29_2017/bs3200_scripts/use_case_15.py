import sys
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from datetime import time, timedelta, datetime, date
# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_3200 import StartConditionFor3200, PauseConditionFor3200, StopConditionFor3200
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# Browser pages used
# import page_factory

__author__ = 'Eldin'


class ControllerUseCase15(object):
    """
    Test name:
        - CN UseCase15 Basic Programming
    purpose:
        - set up a full configuration on the controller
            - reboot the controller
                - verify programming
    Coverage Area: \n
        1. Load devices \n
        2. Search for devices \n
        3. Set descriptions and locations \n
        4. Can assign devices to addresses \n
        5. Can assign zones to programs \n
        6. Can set up programs with; start times, watering days, water windows, zone concurrency, and assign programs to
           water sources \n
        7. Can set up moisture sensors and give them values \n
        8. Can set up temperature sensors and give them values \n
        9. Can set up event switches and give them values \n
        10. Can set up master valves \n
        11. Can set up flow meters and give them values \n
        12. Can set up water sources \n
        13. Can set up booster pumps \n
        14. Can set up water sources \n
        15. Can test all devices \n
            put in for to verify bug ZZ-435 \n
        16. added a program 4 that has one zone and disabled the program this test for a bug that was introduced in \n
            12.20 id a program was disabled and you rebooted the controller all programming would be lost 4/29/2015 \n

        7/29/2014 reboot command before the get command to verify that programing is saved correctly \n
        7/31/2014 added a test temperature sensor method
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'ControllerUseCase15' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        verify BaseManager connection \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            self.config.basemanager_connection[1].verify_ip_address_state()
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller failed: " +
                                 str(ae.message))

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Load all devices to controller
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                             ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_3(self):
        """
        give each program a start time of 8:00 A.M./9:00 A.M./10:00 A.M./11:00 A.M. \n
        set up program watering days \n
        set up water windows for programs \n
        set programs to have a concurrent zone of 4 \n
        disable program 4 \n
        """
        # this is set in the PG3200 object

        program_start_time_8am_9am_10am_11am = [480, 540, 600, 660]
        program_start_time_2pm = [840]  # 2:00pm start time
        program_watering_days_mwf = [0, 1, 0, 1, 0, 1, 1]  # runs monday/wednesday/friday
        program_water_windows = ['111111111111111111111111']
        monthly_water_windows = ['011111100000111111111110', '011111100001111111111111', '011111100001111111111110',
                                 '011111100001111111111110', '011111100001111111111110', '011111100001111111111110',
                                 '011111100001111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _ww=program_water_windows,
                                             _mc=4,
                                             _ci=opcodes.week_days,
                                             _wd=program_watering_days_mwf,
                                             _sm=[],
                                             _st=program_start_time_8am_9am_10am_11am,
                                             _ml=1)
            self.config.programs[3] = PG3200(_ad=3,
                                             _en=opcodes.true,
                                             _ww=program_water_windows,
                                             _mc=4,
                                             _ci=opcodes.odd_day,
                                             _sm=[],
                                             _st=program_start_time_8am_9am_10am_11am,
                                             _ml=1)
            self.config.programs[4] = PG3200(_ad=4,
                                             _en=opcodes.false,
                                             _ww=program_water_windows,
                                             _mc=4,
                                             _ci=opcodes.week_days,
                                             _wd=program_watering_days_mwf,
                                             _sm=[],
                                             _st=program_start_time_8am_9am_10am_11am,
                                             _ml=1)
            self.config.programs[99] = PG3200(_ad=99,
                                              _en=opcodes.true,
                                              _ww=monthly_water_windows,
                                              _mc=4,
                                              _sm=[],
                                              _st=program_start_time_8am_9am_10am_11am,
                                              _ml=8)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_4(self):
        """
        assign zones to programs \n
        set up primary linked zones \n
        give each zone a run time of 1 hour and 30 minutes \n
        must make zone 200 a primary zone before you can link zones to it \n
        assign sensor to primary zone 1 \n
        assign moisture sensors to a primary zone 1 and set to lower limit watering \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=960,
                                                       _ct=480,
                                                       _so=300,
                                                       _ms=self.config.moisture_sensors[2].ad,
                                                       _ws=opcodes.lower_limit,
                                                       _ll=24.0,
                                                       _cc=opcodes.calibrate_one_time,
                                                       _pz=1)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=1)
            self.config.zone_programs[49] = ZoneProgram(zone_obj=self.config.zones[49],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=1200,
                                                        _ct=600,
                                                        _so=3600,
                                                        _ws=opcodes.timed)
            self.config.zone_programs[50] = ZoneProgram(zone_obj=self.config.zones[50],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=1200,
                                                        _ct=600,
                                                        _so=3600,
                                                        _ws=opcodes.timed)
            self.config.zone_programs[51] = ZoneProgram(zone_obj=self.config.zones[50],
                                                        prog_obj=self.config.programs[4],
                                                        _rt=1200,
                                                        _ct=600,
                                                        _so=3600,
                                                        _ws=opcodes.timed)
            self.config.zone_programs[200] = ZoneProgram(zone_obj=self.config.zones[200],
                                                         prog_obj=self.config.programs[99],
                                                         _rt=1980,
                                                         _ct=180,
                                                         _so=780,
                                                         _pz=200)
            self.config.zone_programs[197] = ZoneProgram(zone_obj=self.config.zones[197],
                                                         prog_obj=self.config.programs[99],
                                                         _ra=50,
                                                         _pz=200)
            self.config.zone_programs[198] = ZoneProgram(zone_obj=self.config.zones[198],
                                                         prog_obj=self.config.programs[99],
                                                         _ra=100,
                                                         _pz=200)
            self.config.zone_programs[199] = ZoneProgram(zone_obj=self.config.zones[199],
                                                         prog_obj=self.config.programs[99],
                                                         _ra=150,
                                                         _pz=200)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_5(self):
        """
        Set up non default values on devices \n
        Set up Moisture Sensors \n
            - MS 1: SB05308 \n
                - Moisture value: 10.0 \n
                - Two wire drop: 1.6 \n
            - MS 2: SB07258 \n
                - Moisture value: 26.0 \n
                - Two wire drop: 1.8 \n

        Set up Temperature Sensors \n
            - TS 1: TAT0001 \n
                - Temperature value: 26.0 \n
                - Two wire drop: 1.8 \n

        Set up Event Switches \n
            - SW 1: TPD0001 \n
                - Value: closed \n
                - Two wire drop: 1.8 \n

        Set up Master Valves \n
            - MV 1: TSD0001 \n
                - Booster Pump: true \n
                - Normally Open: closed \n
            - MV 2: TMV0003 \n
                - Normally Open: closed \n
            - MV 8: TMV0004 \n
                - Normally Open: open \n

        Set up Flow Meters \n
            - FM 1: TWF0003 \n
                - Enabled: True \n
                - K-value: 3.10 \n
                - Flow GPM: 25 \n
            - FM 2: TWF0004 \n
                - Enabled: True \n
                - K-value: 5.01 \n
                - Flow GPM: 50 \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Moisture Sensors
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=10.0)
            self.config.moisture_sensors[1].set_two_wire_drop_value_on_cn(_value=1.6)
            self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=26.0)
            self.config.moisture_sensors[2].set_two_wire_drop_value_on_cn(_value=1.8)

            # Temperature Sensors
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=26.0)
            self.config.temperature_sensors[1].set_two_wire_drop_value_on_cn(_value=1.8)

            # Event Switches
            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.closed)
            self.config.event_switches[1].set_two_wire_drop_value_on_cn(_value=1.8)

            # Master Valves
            self.config.master_valves[1].set_booster_enable_state_on_cn(booster_state=opcodes.true)
            self.config.master_valves[1].set_normally_open_state_on_cn(_normally_open=opcodes.false)
            self.config.master_valves[2].set_normally_open_state_on_cn(_normally_open=opcodes.false)
            self.config.master_valves[8].set_normally_open_state_on_cn(_normally_open=opcodes.true)

            # Flow Meters
            self.config.flow_meters[1].set_enable_state_on_cn(_state=opcodes.true)
            self.config.flow_meters[1].set_k_value_on_cn(_k_value=3.10)
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=25)
            self.config.flow_meters[2].set_enable_state_on_cn(_state=opcodes.true)
            self.config.flow_meters[2].set_k_value_on_cn(_k_value=5.01)
            self.config.flow_meters[2].set_flow_rate_on_cn(_flow_rate=50)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_6(self):
        """
        Create Start/stop/pause conditions \n
        Start PG 99 when MS1 gets above 38 \n
        Pause PG 99 when SW1 gets opened \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.program_start_conditions[1] = StartConditionFor3200(program_ad=99)
            self.config.program_start_conditions[1].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.upper_limit, threshold=38)

            self.config.program_pause_conditions[1] = PauseConditionFor3200(program_ad=99)
            self.config.program_pause_conditions[1].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.open)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_7(self):
        """
        Set up POCs \n
        POC 1 \n
            enable POC 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            assign POC 1 a target flow of 500 \n
            assign POC 1 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        8 \n
            enable POC 8 \n
            assign master valve TMV0004 and flow meter TWF0004 to POC 8 \n
            assign POC 8 a target flow of 50 \n
            assign POC 8 to main line 8 \n
            set POC priority to 3-low \n
            set high flow limit to 75 and disable high flow shut down \n
            set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
            set water budget to 1000 and disable water budget shut down \n
            disable water rationing \n
            assign event switch TPD0001 to POC 8 \n
            set switch empty condition to closed \n
            set empty wait time to 540 minutes \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1] = POC3200(_ad=1,
                                         _mv=self.config.master_valves[2].ad,
                                         _fm=self.config.flow_meters[1].ad,
                                         _en=opcodes.true,
                                         _ml=1,
                                         _fl=500,
                                         _pr=2,
                                         _uf=10,
                                         _us=opcodes.true,
                                         _hf=550,
                                         _hs=opcodes.true,
                                         _wb=100000,
                                         _ws=opcodes.true,
                                         _wr=opcodes.true)
            self.config.poc[8] = POC3200(_ad=8,
                                         _mv=self.config.master_valves[8].ad,
                                         _fm=self.config.flow_meters[2].ad,
                                         _en=opcodes.true,
                                         _ml=1,
                                         _fl=50,
                                         _pr=3,
                                         _uf=5,
                                         _us=opcodes.false,
                                         _hf=75,
                                         _hs=opcodes.false,
                                         _wb=1000,
                                         _ws=opcodes.false,
                                         _wr=opcodes.false,
                                         _sw=self.config.event_switches[1].ad,
                                         _se=opcodes.closed,
                                         _ew=450)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_8(self):
        """
        set up main line 1 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 8 \n
            set limit zones by flow to true \n
            set the pipe fill time to 1 minute \n
            set the target flow to 50 \n
            set the high variance limit to 20% and disable the high variance shut down \n
            set the low variance limit to 5% and disable the low variance shut down \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.mainlines[1] = Mainline(_ad=1,
                                                _lc=opcodes.true,
                                                _ft=4,
                                                _fl=500,
                                                _hv=5,
                                                _hs=opcodes.true,
                                                _lv=20,
                                                _ls=opcodes.false)
            self.config.mainlines[8] = Mainline(_ad=8,
                                                _lc=opcodes.true,
                                                _ft=1,
                                                _fl=50,
                                                _hv=20,
                                                _hs=opcodes.false,
                                                _lv=5,
                                                _ls=opcodes.true)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_9(self):
        """
        increment the clock to save settings \n
        turn sim mode off \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.controllers[1].set_sim_mode_to_off()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_10(self):
        """
        reboot the controller \n
        set sim mode to off, wait 10 seconds, then turn it back on \n
        stop the clock \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_reboot_controller()
            self.config.controllers[1].set_sim_mode_to_off()
            time.sleep(30)
            self.config.controllers[1].set_sim_mode_to_on()
            self.config.controllers[1].stop_clock()
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_11(self):
        """
        do a self test on all devices \n
        increment the clock by 1 minute \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].self_test_and_update_object_attributes()
            for ms in self.config.ms_ad_range:
                self.config.moisture_sensors[ms].self_test_and_update_object_attributes()
            for ts in self.config.ts_ad_range:
                self.config.temperature_sensors[ts].self_test_and_update_object_attributes()
            for sw in self.config.sw_ad_range:
                self.config.event_switches[sw].self_test_and_update_object_attributes()
            for mv in self.config.mv_ad_range:
                self.config.master_valves[mv].self_test_and_update_object_attributes()
            for fm in self.config.fm_ad_range:
                self.config.flow_meters[fm].self_test_and_update_object_attributes()

            self.config.controllers[1].do_increment_clock(minutes=1)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_12(self):
        """
        verify the entire configuration again \n
        verify that BaseManager is still connected \n
            - Give it three tries to see if it connected
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()

            attempts = 0
            while attempts <= 3:
                try:
                    self.config.basemanager_connection[1].verify_ip_address_state()
                    break
                except AttributeError as e:
                    attempts += 1
                    print("Sleeping for 30 seconds to allow BaseManager to attempt to connect.")
                    time.sleep(30)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

