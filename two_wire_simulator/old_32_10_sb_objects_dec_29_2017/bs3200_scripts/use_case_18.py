import sys
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from datetime import time, timedelta, datetime, date
# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# Browser pages used
# import page_factory

__author__ = 'Eldin'


class ControllerUseCase18(object):
    """
    Test name:
        - CN UseCase18 Basic Design Flow
    purpose:
        - Set up a zones on the controller and attach them to a program
            - Make Zones 1 and 11 primary zones, and attach the rest of the zones to these two zones
            - Set up a design flow for all zones
        - Set up a POC and Mainline to manipulate the zones in a certain way described in comments in each step \n
    Coverage Area: \n
        1. Manage flow using design flow instead of a flow meter \n
        2. Able to assign flow to zones \n
        3. Able to assign design flow to main lines \n
        4. Run programs with a design flow restriction on program 1 with Limit Concurrent active \n
        5. Run programs with two design flow one on each program, one with Limit Concurrent active and one without \n
        6. Run programs with two design flow one on each program, both with Limit Concurrent active \n
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'ControllerUseCase18' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
        set max number of concurrent zones running on the controller to 2 \n
        verify BaseManager connection \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(10)

            self.config.basemanager_connection[1].verify_ip_address_state()
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller failed: " +
                                 str(ae.message))

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Load all devices to controller
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                             ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_3(self):
        """
        Set the design flow of all zones
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zones[1].set_design_flow_on_cn(_df=20)
            self.config.zones[2].set_design_flow_on_cn(_df=20)
            self.config.zones[3].set_design_flow_on_cn(_df=20)
            self.config.zones[4].set_design_flow_on_cn(_df=12)
            self.config.zones[5].set_design_flow_on_cn(_df=7.5)
            self.config.zones[11].set_design_flow_on_cn(_df=15)
            self.config.zones[12].set_design_flow_on_cn(_df=15)
            self.config.zones[13].set_design_flow_on_cn(_df=7.5)
            self.config.zones[14].set_design_flow_on_cn(_df=7.5)
            self.config.zones[15].set_design_flow_on_cn(_df=7.5)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_4(self):
        """
        Make a very basic program that will be used to attach all the zones together. \n
        Program 1: \n
            - Start time 8:00 A.M. \n
            - Max concurrent zones to 4 \n
            - Set mainline the default value sets it to 1\n
        Program 2: \n
            - Start time 8:00 A.M. \n
            - Max concurrent zones to 3 \n
            - Set mainline the default value sets it to 1 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _st=[480],
                                             _mc=4)
            self.config.programs[2] = PG3200(_ad=2,
                                             _en=opcodes.true,
                                             _st=[480],
                                             _mc=3)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_5(self):
        """
        assign zones to programs \n
        set up primary linked zones \n
        Zone 1 is a primary zone \n
            - Zone 2-5 are linked zones of zone 1, with ra=100%  \n
        Zone 11 is a primary zone \n
            - Zone 11-15 are linked zones of zone 11, with ra=100%  \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600,
                                                       _ct=120,
                                                       _so=240,
                                                       _pz=1)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=1)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=1)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=1)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=1)
            self.config.zone_programs[11] = ZoneProgram(zone_obj=self.config.zones[11],
                                                        prog_obj=self.config.programs[2],
                                                        _rt=3600,
                                                        _ct=120,
                                                        _so=240,
                                                        _pz=11)
            self.config.zone_programs[12] = ZoneProgram(zone_obj=self.config.zones[12],
                                                        prog_obj=self.config.programs[2],
                                                        _ra=100,
                                                        _pz=11)
            self.config.zone_programs[13] = ZoneProgram(zone_obj=self.config.zones[13],
                                                        prog_obj=self.config.programs[2],
                                                        _ra=100,
                                                        _pz=11)
            self.config.zone_programs[14] = ZoneProgram(zone_obj=self.config.zones[14],
                                                        prog_obj=self.config.programs[2],
                                                        _ra=100,
                                                        _pz=11)
            self.config.zone_programs[15] = ZoneProgram(zone_obj=self.config.zones[15],
                                                        prog_obj=self.config.programs[2],
                                                        _ra=100,
                                                        _pz=11)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_6(self):
        """
        set_mainlines_3200
        set up main line 1 \n
            set limit zones by flow to true \n
            set the target flow to 50 \n
        \n
        set up main line 2 \n
            set limit zones by flow to true \n
            set the target flow to 50 \n
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        self.config.mainlines[1] = Mainline(_ad=1,
                                            _fl=50,
                                            _lc=opcodes.true)
        self.config.mainlines[2] = Mainline(_ad=2,
                                            _fl=50,
                                            _lc=opcodes.true)

    def step_7(self):
        """
        set_poc_3200
        set up POC 1 \n
            enable POC 1 \n
            assign mainline 1 \n
            assign POC 1 a target flow of 50 \n
        \n
        set up POC 2 \n
            enable POC 2 \n
            assign mainline 2 \n
            assign POC 2 a target flow of 50 \n
        """
        self.config.poc[1] = POC3200(
            _ad=1,
            _en=opcodes.true,
            _fl=50,
            _ml=1
        )

        self.config.poc[2] = POC3200(
            _ad=2,
            _en=opcodes.true,
            _fl=50,
            _ml=2
        )

    def step_8(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_9(self):
        """
        Set the time to be 7:59, which is 1 minute before both of our program's start times \n
        Verify that nothing is started: \n
            - Verify that no programs are running \n
            - Verify that no zones are watering \n
        Increment the clock two minutes which should trigger both of our programs to start \n
        Verify that specific zones have started: \n
            Program 1: \n
                - Verify that only zones 1, 2, and 5 are watering because water source 1 is limited to 50 GPM while zones 1
                  and 2 run at 20 GPM each and zone 5 runs at 7.5 bring it up to a grand total of 47.5 GPM leaving
                  nothing for the rest of the water source \n
            Program 2: \n
                - Verify that program 2 does not run because it runs off the flow from water source 1 and there is not
                  enough flow left to run any zones on the controller \n
            Stop the programs after we verify they started correctly \n
                - This test only covers that the correct zones are running when there are flow restrictions, for a test
                  that verifies every minute ran, look at use_case_17 and use_case 16 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_date_and_time_on_cn(_date='02/12/2017', _time='7:59:00')
            self.config.controllers[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
            for program in range(1, 3):
                self.config.programs[program].get_data()
                self.config.programs[program].verify_status_on_cn(_expected_status=opcodes.done_watering)

            # This should trigger the programs to start watering
            self.config.controllers[1].do_increment_clock(minutes=2)
            # Verify that zones 1, 2, and 5 are watering
            for zone in [1, 2, 5]:
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(status=opcodes.watering)
            for zone in [3, 4, 11, 12, 13, 14, 15]:
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(status=opcodes.waiting_to_water)

            # Stop the programs after we verify everything ran as expected
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.stop)
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function=opcodes.stop)
            self.config.controllers[1].do_increment_clock(minutes=1)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_10(self):
        """
        Change flow settings: \n
            - POC 1: Change flow to 75 GPM \n
            - POC 2: Change mainline to 2, and flow to 75 GPM \n
            - Mainline 2: Flow to 25 GPM, LC (limit zones by flow) to false \n
            - Program 2: Set mainline to 2 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1].set_target_flow_on_cn(75)
            self.config.poc[2].set_target_flow_on_cn(75)
            self.config.poc[2].set_mainline_on_cn(2)
            self.config.mainlines[2].set_target_flow_on_cn(_new_target_flow=25)
            self.config.mainlines[2].set_limit_zones_by_flow_state_on_cn(_new_state=opcodes.false)
            self.config.programs[2].set_mainline_on_cn(2)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_11(self):
        """
        Verify that nothing is started: \n
            - Verify that no programs are running \n
            - Verify that no zones are watering \n
        Start both programs using commands, and then increment the clock to update the status of the zones \n
        Verify that specific zones have started: \n
        Program 1: \n
            - Verify that only zones 1, 2, and 5 are watering because water source 1 is limited to 50 GPM while zones 1
              and 2 run at 20 GPM each and zone 5 runs at 7.5 bring it up to a grand total of 47.5 GPM leaving
              nothing for the rest of the water source \n
        Program 2: \n
            - Verify that zones 11, 12, and 13 are the only zones running on program 2 because water source 2 is not
              limited by flow so the program runs off the controller and program zone concurrency \n
        Stop the programs after we verify they started correctly \n
            - This test only covers that the correct zones are running when there are flow restrictions, for a test
              that verifies every minute ran, look at use_case_17 and use_case 16 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
            for program in range(1, 3):
                self.config.programs[program].get_data()
                self.config.programs[program].verify_status_on_cn(_expected_status=opcodes.done_watering)

            # This should trigger the programs to start watering
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.start_program)
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function=opcodes.start_program)
            self.config.controllers[1].verify_date_and_time()
            self.config.controllers[1].do_increment_clock(minutes=1)
            # Verify that zones 1, 2, and 5 are watering on program 1, and zones 11, 12, 13 on program 2
            for zone in [1, 2, 5, 11, 12, 13]:
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(status=opcodes.watering)
            for zone in [3, 4, 14, 15]:
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(status=opcodes.waiting_to_water)

            # Stop the programs after we verify everything ran as expected
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.stop)
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function=opcodes.stop)
            self.config.controllers[1].do_increment_clock(minutes=1)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_12(self):
        """
        Change flow settings: \n
            - Mainline 2: Flow to 25 GPM, LC (limit zones by flow) to true \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.mainlines[2].set_target_flow_on_cn(_new_target_flow=25)
            self.config.mainlines[2].set_limit_zones_by_flow_state_on_cn(_new_state=opcodes.true)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_13(self):
        """
        Verify that nothing is started: \n
            - Verify that no programs are running \n
            - Verify that no zones are watering \n
        Start both programs using commands, and then increment the clock to update the status of the zones \n
        Verify that specific zones have started: \n
        Program 1: \n
            - Verify that only zones 1, 2, and 5 are watering because water source 1 is limited to 50 GPM while zones 1
              and 2 run at 20 GPM each and zone 5 runs at 7.5 bring it up to a grand total of 47.5 GPM leaving
              nothing for the rest of the water source \n
        Program 2: \n
            - Verify that zones 11, 12, and 13 are the only zones running on program 2 because water source 2 is not
              limited by flow so the program runs off the controller and program zone concurrency \n
        Stop the programs after we verify they started correctly \n
            - Verify that zones 11 and 13 are the only zones running on program 2 because water source 2 is limited to
              25 GPM because it is now limited by flow and both zones take up 22.5 GPM \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(status=opcodes.done_watering)
            for program in range(1, 3):
                self.config.programs[program].get_data()
                self.config.programs[program].verify_status_on_cn(_expected_status=opcodes.done_watering)

            # This should trigger the programs to start watering
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.start_program)
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function=opcodes.start_program)
            self.config.controllers[1].verify_date_and_time()
            self.config.controllers[1].do_increment_clock(minutes=1)
            # Verify that zones 1, 2, and 5 are watering on program 1, and zones 11, 12, 13 on program 2
            for zone in [1, 2, 5, 11, 13]:
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(status=opcodes.watering)
            for zone in [3, 4, 12, 14, 15]:
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(status=opcodes.waiting_to_water)

            # Stop the programs after we verify everything ran as expected
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.stop)
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function=opcodes.stop)
            self.config.controllers[1].do_increment_clock(minutes=1)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

