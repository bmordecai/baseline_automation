import sys
from time import sleep

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_3200 import StartConditionFor3200, \
                                                                StopConditionFor3200, \
                                                                PauseConditionFor3200

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# this import allows us to directly use the date_mngr
from datetime import time

__author__ = 'Turu'


class ControllerUseCase8(object):
    """
    Test name:
        - Multiple Decoders Having Start Stop Pause Conditions on Multiple Programs \n
    Purpose:
        - confirming that one event/moisture/temp decoder can have a start/stop/pause condition on multiple programs \n
        - one SSP condition can:
            - stop multiple programs at once
            - start a program
    Coverage area: \n
        This test covers having decoders set multiple start stop pause conditions \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions \n
                    - locations \n
        - setting programs: \n
            - start times \n
            - mainlines \n

        - attaching zones to program \n

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow\n
                - high variance limit
                - high variance shut down \n
                - low variance limit
                - low variance shut down \n

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n

        - turning off multiple programs at once
        - trigger a SSP condition on a device that then starts, stops, and pauses different programs \n
        - verifying linked zones work with every SSP condition \n
    Not Covered: \n
        - Two Pause conditions from the same device type on on program \n
            - I thought this wouldn't be necessary because we already do this case for start and stop and if we were
              to do it for pause it may throw off our test patter \n
    Pattern:
        - This test can seem complicated at first with all the SSP conditions, however we tried to incorporate a pattern
          to make it simpler \n
        - The pattern for the first conditions (PT=Start condition, PP=Stop condition, PS=Pause condition):
            PG: 1-10 | 96 | 97 | 98 | 99
            MS1: PT              PP   PS
            MS2: PS    PT             PP
            MS3: PP    PS   PT
            MS4:       PP   PS   PT
            MS5:            PP   PS   PT
            TS1: PT              PP   PS
            TS2: PS    PT             PP
            TS3: PP    PS   PT
            TS4:       PP   PS   PT
            TS5:            PP   PS   PT
            SW1: PT              PP   PS
            SW2: PS    PT             PP
            SW3: PP    PS   PT
            SW4:       PP   PS   PT
            SW5:            PP   PS   PT
            - This pattern has each device start a program, pause a different program, and stop a different program \n
            - This is done so we know each program has a start, stop, and pause condition from each device type \n
        - The pattern for the second conditions (PT=Start condition, PP=Stop condition, PS=Pause condition):
            PG: 1-10 | 96 | 97 | 98 | 99
            MS4: PT                   PP
            MS5: PP    PT
            MS1:       PP   PT
            MS2:            PP   PT
            MS3:                 PP   PT
            TS4: PT                   PP
            TS5: PP    PT
            TS1:       PP   PT
            TS2:            PP   PT
            TS3:                 PP   PT
            # These have been left out for now
            # SW4: PT                   PP
            # SW5: PP    PT
            # SW1:       PP   PT
            # SW2:            PP   PT
            # SW3:                 PP   PT
            - This pattern sets two more of the same device type to have a start and stop condition on each program \n
            - This is done so we know all programs can have the same condition from the same device type (but not the
              same actual device) \n
    """
    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        Runs each step of our use case
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    self.step_15()
                    self.step_16()
                    self.step_17()
                    self.step_18()
                    self.step_19()
                    self.step_20()
                    self.step_21()
                    self.step_22()
                    self.step_23()
                    self.step_24()
                    self.step_25()
                    self.step_26()
                    self.step_27()
                    self.step_28()
                    self.step_29()
                    self.step_30()
                    self.step_31()
                    self.step_32()
                    self.step_33()
                    self.step_34()
                    # self.step_35()
                    # self.step_36()
                    # self.step_37()
                    # self.step_38()
                    # self.step_39()
                    # self.step_40()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - sets up the controller \n
        - verify BaseManager connection \n
        - set max concurrent zones to 5 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # this is used to give the serial port a long timeout
            self.config.controllers[1].set_serial_port_timeout(timeout=1200)

            # this allows no more than 5 controllers to be watering at the same time
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=5)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensor       {ms}
            - Temperature Sensor    {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        # # load all devices need into the controller so that they are available for use in the configuration
        try:
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                             ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_3(self):
        """
        set up our PG3200 objects \n
        set_up_programs \n
            Program 1, 96, 97, 98, 99:
                - start times set to 2:00 AM so they don't interfere with the test. \n
                - mainline 1 \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _st=[120],
                                             _ml=1,
                                             _mc=5)
            self.config.programs[96] = PG3200(_ad=96,
                                              _en=opcodes.true,
                                              _st=[120],
                                              _ml=1,
                                              _mc=4)
            self.config.programs[97] = PG3200(_ad=97,
                                              _en=opcodes.true,
                                              _st=[120],
                                              _ml=1,
                                              _mc=4)
            self.config.programs[98] = PG3200(_ad=98,
                                              _en=opcodes.true,
                                              _st=[120],
                                              _ml=1,
                                              _mc=4)
            self.config.programs[99] = PG3200(_ad=99,
                                              _en=opcodes.true,
                                              _st=[120],
                                              _ml=1,
                                              _mc=4)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_4(self):
        """
        setup start/stop/pause conditions: \n
            - make programs 1
                - start by (MS[1], TS[1], SW[1]), (MS[4], TS[4], SW[4])
                - pause by (MS[2], TS[2], SW[2]),
                - stop by (MS[3], TS[3], SW[3]), (MS[5], TS[5], SW[5])
            - make program 96
                - start by (MS[2], TS[2], SW[2]), (MS[5], TS[5], SW[5])
                - pause by (MS[3], TS[3], SW[3]),
                - stop by (MS[4], TS[4], SW[4]), (MS[1], TS[1], SW[1])
            - make program 97
                - start by (MS[3], TS[3], SW[3]), (MS[1], TS[1], SW[1])
                - pause by (MS[4], TS[4], SW[4]),
                - stop by (MS[5], TS[5], SW[5]), (MS[2], TS[2], SW[2])
            - make program 98
                - start by (MS[4], TS[4], SW[4]), (MS[2], TS[2], SW[2])
                - pause by (MS[5], TS[5], SW[5]),
                - stop by (MS[1], TS[1], SW[1]), (MS[3], TS[3], SW[3])
            - make program 99
                - start by (MS[5], TS[5], SW[5]), (MS[3], TS[3], SW[3])
                - pause by (MS[1], TS[1], SW[1]),
                - stop by (MS[2], TS[2], SW[2]), (MS[4], TS[4], SW[4])
        """
        # this is set in the PG3200 object
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # First 15 start conditions, 1 for each of the 5 moisture sensors, temperature sensors, and event switches
            self.config.program_start_conditions[1] = StartConditionFor3200(program_ad=1)
            self.config.program_start_conditions[1].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_start_conditions[2] = StartConditionFor3200(program_ad=96)
            self.config.program_start_conditions[2].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[2].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_start_conditions[3] = StartConditionFor3200(program_ad=97)
            self.config.program_start_conditions[3].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[3].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_start_conditions[4] = StartConditionFor3200(program_ad=98)
            self.config.program_start_conditions[4].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[4].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_start_conditions[5] = StartConditionFor3200(program_ad=99)
            self.config.program_start_conditions[5].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[5].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_start_conditions[6] = StartConditionFor3200(program_ad=1)
            self.config.program_start_conditions[6].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_start_conditions[7] = StartConditionFor3200(program_ad=96)
            self.config.program_start_conditions[7].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[2].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_start_conditions[8] = StartConditionFor3200(program_ad=97)
            self.config.program_start_conditions[8].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[3].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_start_conditions[9] = StartConditionFor3200(program_ad=98)
            self.config.program_start_conditions[9].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[4].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_start_conditions[10] = StartConditionFor3200(program_ad=99)
            self.config.program_start_conditions[10].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[5].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_start_conditions[11] = StartConditionFor3200(program_ad=1)
            self.config.program_start_conditions[11].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.open)

            self.config.program_start_conditions[12] = StartConditionFor3200(program_ad=96)
            self.config.program_start_conditions[12].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[2].sn, mode=opcodes.open)

            self.config.program_start_conditions[13] = StartConditionFor3200(program_ad=97)
            self.config.program_start_conditions[13].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[3].sn, mode=opcodes.open)

            self.config.program_start_conditions[14] = StartConditionFor3200(program_ad=98)
            self.config.program_start_conditions[14].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[4].sn, mode=opcodes.open)

            self.config.program_start_conditions[15] = StartConditionFor3200(program_ad=99)
            self.config.program_start_conditions[15].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[5].sn, mode=opcodes.open)

            # First 15 pause conditions, 1 for each of the 5 moisture sensors, temperature sensors, and event switches
            self.config.program_pause_conditions[1] = PauseConditionFor3200(program_ad=1)
            self.config.program_pause_conditions[1].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[2].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_pause_conditions[2] = PauseConditionFor3200(program_ad=96)
            self.config.program_pause_conditions[2].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[3].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_pause_conditions[3] = PauseConditionFor3200(program_ad=97)
            self.config.program_pause_conditions[3].set_moisture_condition_on_pg(

                serial_number=self.config.moisture_sensors[4].sn, mode=opcodes.upper_limit, threshold=26)
            self.config.program_pause_conditions[4] = PauseConditionFor3200(program_ad=98)
            self.config.program_pause_conditions[4].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[5].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_pause_conditions[5] = PauseConditionFor3200(program_ad=99)
            self.config.program_pause_conditions[5].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_pause_conditions[6] = PauseConditionFor3200(program_ad=1)
            self.config.program_pause_conditions[6].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[2].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_pause_conditions[7] = PauseConditionFor3200(program_ad=96)
            self.config.program_pause_conditions[7].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[3].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_pause_conditions[8] = PauseConditionFor3200(program_ad=97)
            self.config.program_pause_conditions[8].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[4].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_pause_conditions[9] = PauseConditionFor3200(program_ad=98)
            self.config.program_pause_conditions[9].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[5].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_pause_conditions[10] = PauseConditionFor3200(program_ad=99)
            self.config.program_pause_conditions[10].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_pause_conditions[11] = PauseConditionFor3200(program_ad=1)
            self.config.program_pause_conditions[11].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[2].sn, mode=opcodes.open)

            self.config.program_pause_conditions[12] = PauseConditionFor3200(program_ad=96)
            self.config.program_pause_conditions[12].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[3].sn, mode=opcodes.open)

            self.config.program_pause_conditions[13] = PauseConditionFor3200(program_ad=97)
            self.config.program_pause_conditions[13].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[4].sn, mode=opcodes.open)

            self.config.program_pause_conditions[14] = PauseConditionFor3200(program_ad=98)
            self.config.program_pause_conditions[14].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[5].sn, mode=opcodes.open)

            self.config.program_pause_conditions[15] = PauseConditionFor3200(program_ad=99)
            self.config.program_pause_conditions[15].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.open)

            # First 15 stop conditions, 1 for each of the 5 moisture sensors, temperature sensors, and event switches
            self.config.program_stop_conditions[1] = StopConditionFor3200(program_ad=1)
            self.config.program_stop_conditions[1].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[3].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_stop_conditions[2] = StopConditionFor3200(program_ad=96)
            self.config.program_stop_conditions[2].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[4].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_stop_conditions[3] = StopConditionFor3200(program_ad=97)
            self.config.program_stop_conditions[3].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[5].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_stop_conditions[4] = StopConditionFor3200(program_ad=98)
            self.config.program_stop_conditions[4].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_stop_conditions[5] = StopConditionFor3200(program_ad=99)
            self.config.program_stop_conditions[5].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[2].sn, mode=opcodes.upper_limit, threshold=26)

            self.config.program_stop_conditions[6] = StopConditionFor3200(program_ad=1)
            self.config.program_stop_conditions[6].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[3].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_stop_conditions[7] = StopConditionFor3200(program_ad=96)
            self.config.program_stop_conditions[7].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[4].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_stop_conditions[8] = StopConditionFor3200(program_ad=97)
            self.config.program_stop_conditions[8].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[5].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_stop_conditions[9] = StopConditionFor3200(program_ad=98)
            self.config.program_stop_conditions[9].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_stop_conditions[10] = StopConditionFor3200(program_ad=99)
            self.config.program_stop_conditions[10].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[2].sn, mode=opcodes.upper_limit, threshold=98)

            self.config.program_stop_conditions[11] = StopConditionFor3200(program_ad=1)
            self.config.program_stop_conditions[11].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[3].sn, mode=opcodes.open)

            self.config.program_stop_conditions[12] = StopConditionFor3200(program_ad=96)
            self.config.program_stop_conditions[12].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[4].sn, mode=opcodes.open)

            self.config.program_stop_conditions[13] = StopConditionFor3200(program_ad=97)
            self.config.program_stop_conditions[13].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[5].sn, mode=opcodes.open)

            self.config.program_stop_conditions[14] = StopConditionFor3200(program_ad=98)
            self.config.program_stop_conditions[14].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.open)

            self.config.program_stop_conditions[15] = StopConditionFor3200(program_ad=99)
            self.config.program_stop_conditions[15].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[2].sn, mode=opcodes.open)

            # Second 15 start conditions, another 1 for each of the 5 moisture sensors, temperature sensors, and event switches
            self.config.program_start_conditions[16] = StartConditionFor3200(program_ad=1)
            self.config.program_start_conditions[16].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[4].sn, mode=opcodes.lower_limit, threshold=12)

            self.config.program_start_conditions[17] = StartConditionFor3200(program_ad=96)
            self.config.program_start_conditions[17].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[5].sn, mode=opcodes.lower_limit, threshold=12)

            self.config.program_start_conditions[18] = StartConditionFor3200(program_ad=97)
            self.config.program_start_conditions[18].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.lower_limit, threshold=12)

            self.config.program_start_conditions[19] = StartConditionFor3200(program_ad=98)
            self.config.program_start_conditions[19].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[2].sn, mode=opcodes.lower_limit, threshold=12)

            self.config.program_start_conditions[20] = StartConditionFor3200(program_ad=99)
            self.config.program_start_conditions[20].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[3].sn, mode=opcodes.lower_limit, threshold=12)

            self.config.program_start_conditions[21] = StartConditionFor3200(program_ad=1)
            self.config.program_start_conditions[21].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[4].sn, mode=opcodes.lower_limit, threshold=36)

            self.config.program_start_conditions[22] = StartConditionFor3200(program_ad=96)
            self.config.program_start_conditions[22].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[5].sn, mode=opcodes.lower_limit, threshold=36)

            self.config.program_start_conditions[23] = StartConditionFor3200(program_ad=97)
            self.config.program_start_conditions[23].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.lower_limit, threshold=36)

            self.config.program_start_conditions[24] = StartConditionFor3200(program_ad=98)
            self.config.program_start_conditions[24].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[2].sn, mode=opcodes.lower_limit, threshold=36)

            self.config.program_start_conditions[25] = StartConditionFor3200(program_ad=99)
            self.config.program_start_conditions[25].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[3].sn, mode=opcodes.lower_limit, threshold=36)

            # self.config.program_start_conditions[26] = StartConditionFor3200(program_ad=1)
            # self.config.program_start_conditions[26].set_event_switch_condition_on_pg(
            #     serial_number=self.config.event_switches[4].sn, mode=opcodes.closed)
            #
            # self.config.program_start_conditions[27] = StartConditionFor3200(program_ad=96)
            # self.config.program_start_conditions[27].set_event_switch_condition_on_pg(
            #     serial_number=self.config.event_switches[5].sn, mode=opcodes.closed)
            #
            # self.config.program_start_conditions[28] = StartConditionFor3200(program_ad=97)
            # self.config.program_start_conditions[28].set_event_switch_condition_on_pg(
            #     serial_number=self.config.event_switches[1].sn, mode=opcodes.closed)
            #
            # self.config.program_start_conditions[29] = StartConditionFor3200(program_ad=98)
            # self.config.program_start_conditions[29].set_event_switch_condition_on_pg(
            #     serial_number=self.config.event_switches[2].sn, mode=opcodes.closed)
            #
            # self.config.program_start_conditions[30] = StartConditionFor3200(program_ad=99)
            # self.config.program_start_conditions[30].set_event_switch_condition_on_pg(
            #     serial_number=self.config.event_switches[3].sn, mode=opcodes.closed)

            # Second 15 stop conditions, 1 for each of the 5 moisture sensors, temperature sensors, and event switches
            self.config.program_stop_conditions[16] = StopConditionFor3200(program_ad=1)
            self.config.program_stop_conditions[16].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[5].sn, mode=opcodes.lower_limit, threshold=12)

            self.config.program_stop_conditions[17] = StopConditionFor3200(program_ad=96)
            self.config.program_stop_conditions[17].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.lower_limit, threshold=12)

            self.config.program_stop_conditions[18] = StopConditionFor3200(program_ad=97)
            self.config.program_stop_conditions[18].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[2].sn, mode=opcodes.lower_limit, threshold=12)

            self.config.program_stop_conditions[19] = StopConditionFor3200(program_ad=98)
            self.config.program_stop_conditions[19].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[3].sn, mode=opcodes.lower_limit, threshold=12)

            self.config.program_stop_conditions[20] = StopConditionFor3200(program_ad=99)
            self.config.program_stop_conditions[20].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[4].sn, mode=opcodes.lower_limit, threshold=12)

            self.config.program_stop_conditions[21] = StopConditionFor3200(program_ad=1)
            self.config.program_stop_conditions[21].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[5].sn, mode=opcodes.lower_limit, threshold=36)

            self.config.program_stop_conditions[22] = StopConditionFor3200(program_ad=96)
            self.config.program_stop_conditions[22].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.lower_limit, threshold=36)

            self.config.program_stop_conditions[23] = StopConditionFor3200(program_ad=97)
            self.config.program_stop_conditions[23].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[2].sn, mode=opcodes.lower_limit, threshold=36)

            self.config.program_stop_conditions[24] = StopConditionFor3200(program_ad=98)
            self.config.program_stop_conditions[24].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[3].sn, mode=opcodes.lower_limit, threshold=36)

            self.config.program_stop_conditions[25] = StopConditionFor3200(program_ad=99)
            self.config.program_stop_conditions[25].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[4].sn, mode=opcodes.lower_limit, threshold=36)

            # self.config.program_stop_conditions[26] = StopConditionFor3200(program_ad=1)
            # self.config.program_stop_conditions[26].set_event_switch_condition_on_pg(
            #     serial_number=self.config.event_switches[5].sn, mode=opcodes.closed)
            #
            # self.config.program_stop_conditions[27] = StopConditionFor3200(program_ad=96)
            # self.config.program_stop_conditions[27].set_event_switch_condition_on_pg(
            #     serial_number=self.config.event_switches[1].sn, mode=opcodes.closed)
            #
            # self.config.program_stop_conditions[28] = StopConditionFor3200(program_ad=97)
            # self.config.program_stop_conditions[28].set_event_switch_condition_on_pg(
            #     serial_number=self.config.event_switches[2].sn, mode=opcodes.closed)
            #
            # self.config.program_stop_conditions[29] = StopConditionFor3200(program_ad=98)
            # self.config.program_stop_conditions[29].set_event_switch_condition_on_pg(
            #     serial_number=self.config.event_switches[3].sn, mode=opcodes.closed)
            #
            # self.config.program_stop_conditions[30] = StopConditionFor3200(program_ad=99)
            # self.config.program_stop_conditions[30].set_event_switch_condition_on_pg(
            #     serial_number=self.config.event_switches[4].sn, mode=opcodes.closed)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_5(self):
        """
        Zone Programs
            - assign 2 zones to programs 1 \n
                - give each zone a run time of 1 hour and 30 minutes \n
            - assign 4 zones to programs 96, 97, 98, 99 \n
                - give each zone a run time of 1 hour and 30 minutes \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            # Programs 1 have 2 zones each
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _pz=1)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _pz=1)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _pz=1)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _pz=1)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _pz=1)
            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[6],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _pz=1)
            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[7],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _pz=1)
            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[8],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _pz=1)
            self.config.zone_programs[9] = ZoneProgram(zone_obj=self.config.zones[9],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _pz=1)
            self.config.zone_programs[10] = ZoneProgram(zone_obj=self.config.zones[10],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=900,
                                                        _pz=1)
            self.config.zone_programs[11] = ZoneProgram(zone_obj=self.config.zones[11],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=900,
                                                        _pz=1)
            self.config.zone_programs[12] = ZoneProgram(zone_obj=self.config.zones[12],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=900,
                                                        _pz=1)
            self.config.zone_programs[13] = ZoneProgram(zone_obj=self.config.zones[13],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=900,
                                                        _pz=1)
            self.config.zone_programs[14] = ZoneProgram(zone_obj=self.config.zones[14],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=900,
                                                        _pz=1)
            self.config.zone_programs[15] = ZoneProgram(zone_obj=self.config.zones[15],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=900,
                                                        _pz=1)
            self.config.zone_programs[16] = ZoneProgram(zone_obj=self.config.zones[16],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=900,
                                                        _pz=1)
            self.config.zone_programs[17] = ZoneProgram(zone_obj=self.config.zones[17],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=900,
                                                        _pz=1)
            self.config.zone_programs[18] = ZoneProgram(zone_obj=self.config.zones[18],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=900,
                                                        _pz=1)
            self.config.zone_programs[19] = ZoneProgram(zone_obj=self.config.zones[19],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=900,
                                                        _pz=1)
            self.config.zone_programs[20] = ZoneProgram(zone_obj=self.config.zones[20],
                                                        prog_obj=self.config.programs[1],
                                                        _rt=900,
                                                        _pz=1)

            # Program 96 has 4 zones (81-84)
            self.config.zone_programs[96] = ZoneProgram(zone_obj=self.config.zones[81],
                                                        prog_obj=self.config.programs[96],
                                                        _rt=900)
            self.config.zone_programs[96] = ZoneProgram(zone_obj=self.config.zones[82],
                                                        prog_obj=self.config.programs[96],
                                                        _rt=900)
            self.config.zone_programs[96] = ZoneProgram(zone_obj=self.config.zones[83],
                                                        prog_obj=self.config.programs[96],
                                                        _rt=900)
            self.config.zone_programs[96] = ZoneProgram(zone_obj=self.config.zones[84],
                                                        prog_obj=self.config.programs[96],
                                                        _rt=900)

            # Program 97 has 4 zones (85-88)
            self.config.zone_programs[97] = ZoneProgram(zone_obj=self.config.zones[85],
                                                        prog_obj=self.config.programs[97],
                                                        _rt=900)
            self.config.zone_programs[97] = ZoneProgram(zone_obj=self.config.zones[86],
                                                        prog_obj=self.config.programs[97],
                                                        _rt=900)
            self.config.zone_programs[97] = ZoneProgram(zone_obj=self.config.zones[87],
                                                        prog_obj=self.config.programs[97],
                                                        _rt=900)
            self.config.zone_programs[97] = ZoneProgram(zone_obj=self.config.zones[88],
                                                        prog_obj=self.config.programs[97],
                                                        _rt=900)

            # Program 98 has 4 zones (89-92)
            self.config.zone_programs[98] = ZoneProgram(zone_obj=self.config.zones[89],
                                                        prog_obj=self.config.programs[98],
                                                        _rt=900)
            self.config.zone_programs[98] = ZoneProgram(zone_obj=self.config.zones[90],
                                                        prog_obj=self.config.programs[98],
                                                        _rt=900)
            self.config.zone_programs[98] = ZoneProgram(zone_obj=self.config.zones[91],
                                                        prog_obj=self.config.programs[98],
                                                        _rt=900)
            self.config.zone_programs[98] = ZoneProgram(zone_obj=self.config.zones[92],
                                                        prog_obj=self.config.programs[98],
                                                        _rt=900)

            # Program 99 has 4 zones (93-96)
            self.config.zone_programs[99] = ZoneProgram(zone_obj=self.config.zones[93],
                                                        prog_obj=self.config.programs[99],
                                                        _rt=900)
            self.config.zone_programs[99] = ZoneProgram(zone_obj=self.config.zones[94],
                                                        prog_obj=self.config.programs[99],
                                                        _rt=900)
            self.config.zone_programs[99] = ZoneProgram(zone_obj=self.config.zones[95],
                                                        prog_obj=self.config.programs[99],
                                                        _rt=900)
            self.config.zone_programs[99] = ZoneProgram(zone_obj=self.config.zones[96],
                                                        prog_obj=self.config.programs[99],
                                                        _rt=900)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_6(self):
        """
        set_mainlines_3200
        set up main line 1 \n
            set the target flow to 500 \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        try:
            self.config.mainlines[1] = Mainline(_ad=1,
                                                _fl=500)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_7(self):
        """
        set_poc_3200
        set up POC 1 \n
            enable POC 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            assign POC 1 a target flow of 500 \n
            assign POC 1 to main line 1 \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1] = POC3200(
                _ad=1,
                _en=opcodes.true,
                _mv=1,
                _fm=1,
                _fl=500,
                _ml=1
            )

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_8(self):
        """
        Verify the entire configuration
        :return:
        :rtype:
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            # time.sleep(20)
            self.config.verify_full_configuration()
            # this turns off fast sim mode and start the clock
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_9(self):
        """
        Set the time to 7:53:00 \n
        Increment the clock by 6 minutes to 7:59:00 \n
        Verify the status for each program and zone on the controller \n
        - Program status: \n
            - program 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - all zones done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_date_and_time_on_cn(_date='08/28/2014', _time='7:53:00')
            self.config.controllers[1].verify_date_and_time()
            self.config.controllers[1].do_increment_clock(minutes=6)
            self.config.controllers[1].verify_date_and_time()

            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_10(self):
        """
        - set ms[1] to 28, which is past its upper limit \n
        - start programs 1 \n
        - pause program 99 \n
        - stop program 98 \n
        - set the time to 8:10:00 \n
        - Program status: \n
            - programs 1, 2, 3 running \n
            - programs 4-10 waiting \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-5 watering \n
            - zones 6-20 waiting \n
            - zones 81-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=11)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(28)
            self.config.moisture_sensors[1].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 6):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(6, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in range(81, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.moisture_sensors[1].set_moisture_percent_on_cn(22)
            self.config.moisture_sensors[1].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_11(self):
        """
        - set ms[2] to 28, which is past its upper limit \n
        - start programs 96 \n
        - pause programs 1 \n
        - stop program 99 \n
        - set the time to 8:15:00 \n
        - Program status: \n
            - programs 1 paused \n
            - program 96 running \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 paused \n
            - zones 81-84 watering \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[2].set_moisture_percent_on_cn(28)
            self.config.moisture_sensors[2].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.paused)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.running)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.moisture_sensors[2].set_moisture_percent_on_cn(22)
            self.config.moisture_sensors[2].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_12(self):
        """
        - set ms[3] to 28, which is past its upper limit \n
        - start programs 97 \n
        - pause programs 96 \n
        - stop program 1 \n
        - set the time to 8:20:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 paused \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 paused \n
            - zones 85-88 watering \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[3].set_moisture_percent_on_cn(28)
            self.config.moisture_sensors[3].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.paused)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.running)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.moisture_sensors[3].set_moisture_percent_on_cn(22)
            self.config.moisture_sensors[3].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_13(self):
        """
        - set ms[4] to 28, which is past its upper limit \n
        - start programs 98 \n
        - pause programs 97 \n
        - stop program 96 \n
        - set the time to 8:25:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 done \n
            - program 97 paused \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 paused \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[4].set_moisture_percent_on_cn(28)
            self.config.moisture_sensors[4].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.paused)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.running)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.moisture_sensors[4].set_moisture_percent_on_cn(22)
            self.config.moisture_sensors[4].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_14(self):
        """
        - set ms[5] to 28, which is past its upper limit \n
        - start programs 99 \n
        - pause programs 98 \n
        - stop program 97 \n
        - set the time to 8:30:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 running \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 paused \n
            - zones 93-96 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[5].set_moisture_percent_on_cn(28)
            self.config.moisture_sensors[5].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.paused)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.running)

            self.config.moisture_sensors[5].set_moisture_percent_on_cn(22)
            self.config.moisture_sensors[5].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_15(self):
        """
        - set ts[1] to 100, which is past its upper limit \n
        - start programs 1 \n
        - pause program 99 \n
        - stop program 98 \n
        - set the time to 8:35:00 \n
        - Program status: \n
            - programs 1, 2, 3 running \n
            - programs 4-10 waiting \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 paused \n
        - Zone status: \n
            - zones 1-5 running \n
            - zones 6-20 waiting \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 paused \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(100)
            self.config.temperature_sensors[1].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 6):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(6, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in range(81, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.paused)

            self.config.temperature_sensors[1].set_temperature_reading_on_cn(70)
            self.config.temperature_sensors[1].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_16(self):
        """
        - set ts[2] to 100, which is past its upper limit \n
        - start programs 96 \n
        - pause programs 1 \n
        - stop program 99 \n
        - set the time to 8:40:00 \n
        - Program status: \n
            - programs 1 paused \n
            - program 96 running \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 paused \n
            - zones 81-84 watering \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.temperature_sensors[2].set_temperature_reading_on_cn(100)
            self.config.temperature_sensors[2].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.paused)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.running)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.temperature_sensors[2].set_temperature_reading_on_cn(70)
            self.config.temperature_sensors[2].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_17(self):
        """
        - set ts[3] to 100, which is past its upper limit \n
        - start programs 97 \n
        - pause programs 96 \n
        - stop program 1 \n
        - set the time to 8:45:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 paused \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 paused \n
            - zones 85-88 watering \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.temperature_sensors[3].set_temperature_reading_on_cn(100)
            self.config.temperature_sensors[3].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.paused)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.running)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.temperature_sensors[3].set_temperature_reading_on_cn(70)
            self.config.temperature_sensors[3].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_18(self):
        """
        - set ts[4] to 100, which is past its upper limit \n
        - start programs 98 \n
        - pause programs 97 \n
        - stop program 96 \n
        - set the time to 8:50:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 done \n
            - program 97 paused \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 paused \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.temperature_sensors[4].set_temperature_reading_on_cn(100)
            self.config.temperature_sensors[4].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.paused)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.running)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.temperature_sensors[4].set_temperature_reading_on_cn(70)
            self.config.temperature_sensors[4].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_19(self):
        """
        - set ts[5] to 100, which is past its upper limit \n
        - start programs 99 \n
        - pause programs 98 \n
        - stop program 97 \n
        - set the time to 8:55:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 running \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 paused \n
            - zones 93-96 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.temperature_sensors[5].set_temperature_reading_on_cn(100)
            self.config.temperature_sensors[5].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.paused)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.running)

            self.config.temperature_sensors[5].set_temperature_reading_on_cn(70)
            self.config.temperature_sensors[5].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_20(self):
        """
        - set sw[1] to open \n
        - start programs 1 \n
        - pause program 99 \n
        - stop program 98 \n
        - set the time to 9:00:00 \n
        - Program status: \n
            - programs 1, 2, 3 running \n
            - programs 4-10 waiting \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 paused \n
        - Zone status: \n
            - zones 1-5 running \n
            - zones 6-20 waiting \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 paused \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.event_switches[1].set_contact_state_on_cn(opcodes.open)
            self.config.event_switches[1].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 6):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(6, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in range(81, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.paused)

            self.config.event_switches[1].set_contact_state_on_cn(opcodes.closed)
            self.config.event_switches[1].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_21(self):
        """
        - set sw[2] to open \n
        - start program 96 \n
        - pause programs 1 \n
        - stop program 99 \n
        - set the time to 9:05:00 \n
        - Program status: \n
            - programs 1 paused \n
            - program 96 running \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 paused \n
            - zones 81-84 watering \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.event_switches[2].set_contact_state_on_cn(opcodes.open)
            self.config.event_switches[2].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.paused)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.running)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.event_switches[2].set_contact_state_on_cn(opcodes.closed)
            self.config.event_switches[2].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_22(self):
        """
        - set sw[3] to open \n
        - start program 97 \n
        - pause program 96 \n
        - stop programs 1 \n
        - set the time to 9:10:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 paused \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 paused \n
            - zones 85-88 watering \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.event_switches[3].set_contact_state_on_cn(opcodes.open)
            self.config.event_switches[3].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.paused)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.running)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.event_switches[3].set_contact_state_on_cn(opcodes.closed)
            self.config.event_switches[3].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_23(self):
        """
        - set sw[4] to 100, which is past its upper limit \n
        - start program 98 \n
        - pause program 97 \n
        - stop program 96 \n
        - set the time to 9:15:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 done \n
            - program 97 paused \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 paused \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.event_switches[4].set_contact_state_on_cn(opcodes.open)
            self.config.event_switches[4].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.paused)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.running)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.event_switches[4].set_contact_state_on_cn(opcodes.closed)
            self.config.event_switches[4].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_24(self):
        """
        - set sw[5] to 100, which is past its upper limit \n
        - start program 99 \n
        - pause program 98 \n
        - stop program 97 \n
        - set the time to 9:20:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 running \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 paused \n
            - zones 93-96 watering \n
        - At the end we briefly change moisture sensor's 1 value to trigger some programs upper threshold. We do this
          to reset some of status' to "beginning conditions" for this test.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.event_switches[5].set_contact_state_on_cn(opcodes.open)
            self.config.event_switches[5].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.paused)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.paused)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.running)

            self.config.event_switches[5].set_contact_state_on_cn(opcodes.closed)
            self.config.event_switches[5].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_25(self):
        """
        - set ms[4] to 10, which is past its lower limit \n
        - We stop every program that has been running up until this point so we can have a fresh start on the second
          round of start/stop conditions \n
        - Increment the time by 1 minute so controller can update \n
        - Set moisture sensor 1's moisture percent to 10 which will trigger a start/stop condition \n
            - start programs 1 \n
            - stop program 99 \n
        - Increment the clock by 1 minute to give the controller time to process \n
        - The time should be 9:26:00 \n
        - Program status: \n
            - programs 1, 2, 3 running \n
            - programs 4-10 waiting \n
            - program 96 done \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-5 watering \n
            - zones 6-20 waiting \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 paused \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            # Stop all programs so we can start our second round of conditions
            for program in self.config.programs:
                self.config.controllers[1].set_program_start_stop(_pg_ad=program, _function=opcodes.stop)
            self.config.controllers[1].do_increment_clock(minutes=1)
            self.config.moisture_sensors[4].set_moisture_percent_on_cn(10)
            self.config.moisture_sensors[4].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            # TODO, this is the start of the second batch of Starts/stops. Make sure nothing is running up until here

            for zone in range(1, 6):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(6, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in range(81, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.moisture_sensors[4].set_moisture_percent_on_cn(22)
            self.config.moisture_sensors[4].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_26(self):
        """
        - set ms[5] to 10, which is past its lower limit \n
        - start programs 96 \n
        - stop program 1 \n
        - set the time to 9:30:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 running \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 watering \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=3)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[5].set_moisture_percent_on_cn(10)
            self.config.moisture_sensors[5].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.running)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.moisture_sensors[5].set_moisture_percent_on_cn(22)
            self.config.moisture_sensors[5].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_27(self):
        """
        - set ms[1] to 10, which is past its lower limit \n
        - start programs 97 \n
        - stop program 96 \n
        - set the time to 9:35:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 done \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 watering \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(10)
            self.config.moisture_sensors[1].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.running)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.moisture_sensors[1].set_moisture_percent_on_cn(22)
            self.config.moisture_sensors[1].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_28(self):
        """
        - set ms[2] to 10, which is past its lower limit \n
        - start programs 98 \n
        - stop program 97 \n
        - set the time to 9:40:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[2].set_moisture_percent_on_cn(10)
            self.config.moisture_sensors[2].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.running)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.moisture_sensors[2].set_moisture_percent_on_cn(20)
            self.config.moisture_sensors[2].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_29(self):
        """
        - set ms[3] to 10, which is past its lower limit \n
        - start programs 99 \n
        - stop program 98 \n
        - set the time to 9:45:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 running \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.moisture_sensors[3].set_moisture_percent_on_cn(10)
            self.config.moisture_sensors[3].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.running)

            self.config.moisture_sensors[3].set_moisture_percent_on_cn(20)
            self.config.moisture_sensors[3].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_30(self):
        """
        - set ts[4] to 30, which is past its lower limit \n
        - start programs 1 \n
        - stop program 99 \n
        - set the time to 9:50:00 \n
        - Program status: \n
            - programs 1, 2, 3 running \n
            - programs 4-10 waiting \n
            - program 96 done \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-5 watering \n
            - zones 6-20 waiting \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 paused \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.temperature_sensors[4].set_temperature_reading_on_cn(30)
            self.config.temperature_sensors[4].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 6):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(6, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in range(81, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.running)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.temperature_sensors[4].set_temperature_reading_on_cn(70)
            self.config.temperature_sensors[4].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_31(self):
        """
        - set ts[5] to 30, which is past its lower limit \n
        - start programs 96 \n
        - stop program 1 \n
        - set the time to 9:55:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 running \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 watering \n
            - zones 85-88 done \n
            - zones 89-92 paused \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.temperature_sensors[5].set_temperature_reading_on_cn(30)
            self.config.temperature_sensors[5].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.running)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.temperature_sensors[5].set_temperature_reading_on_cn(70)
            self.config.temperature_sensors[5].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_32(self):
        """
        - set ts[1] to 30, which is past its lower limit \n
        - start programs 97 \n
        - stop program 96 \n
        - set the time to 10:00:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 done \n
            - program 97 running \n
            - program 98 paused \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 watering \n
            - zones 89-92 paused \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(30)
            self.config.temperature_sensors[1].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.running)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.temperature_sensors[1].set_temperature_reading_on_cn(70)
            self.config.temperature_sensors[1].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_33(self):
        """
        - set ts[2] to 30, which is past its lower limit \n
        - start programs 98 \n
        - stop program 97 \n
        - set the time to 10:05:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.temperature_sensors[2].set_temperature_reading_on_cn(30)
            self.config.temperature_sensors[2].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.running)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.done_watering)

            self.config.temperature_sensors[2].set_temperature_reading_on_cn(70)
            self.config.temperature_sensors[2].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_34(self):
        """
        - set ts[3] to 30, which is past its lower limit \n
        - start programs 99 \n
        - stop program 98 \n
        - set the time to 10:10:00 \n
        - Program status: \n
            - programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 running \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()
            self.config.temperature_sensors[3].set_temperature_reading_on_cn(30)
            self.config.temperature_sensors[3].do_self_test()
            self.config.controllers[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(81, 85):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(85, 89):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(89, 93):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
            for zone in range(93, 97):
                self.config.zones[zone].get_data()
                self.config.zones[zone].verify_status_on_cn(opcodes.watering)

            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[96].get_data()
            self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[97].get_data()
            self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[98].get_data()
            self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.running)

            self.config.temperature_sensors[3].set_temperature_reading_on_cn(70)
            self.config.temperature_sensors[3].do_self_test()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    # def step_35(self):
    #     """
    #     setup start/stop/pause conditions: \n
    #         - make programs 1
    #             - start by (SW[4])
    #             - stop by (SW[5])
    #         - make program 96
    #             - start by (SW[5])
    #             - stop by (SW[1])
    #         - make program 97
    #             - start by (SW[1])
    #             - stop by (SW[2])
    #         - make program 98
    #             - start by (SW[2])
    #             - stop by (SW[3])
    #         - make program 99
    #             - start by (SW[3])
    #             - stop by (SW[4])
    #     """
    #     method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
    #     print method
    #     try:
    #         self.config.program_start_conditions[26] = StartConditionFor3200(program_ad=1)
    #         self.config.program_start_conditions[26].set_event_switch_condition_on_pg(
    #             serial_number=self.config.event_switches[4].sn, mode=opcodes.closed)
    #
    #         self.config.program_start_conditions[27] = StartConditionFor3200(program_ad=96)
    #         self.config.program_start_conditions[27].set_event_switch_condition_on_pg(
    #             serial_number=self.config.event_switches[5].sn, mode=opcodes.closed)
    #
    #         self.config.program_start_conditions[28] = StartConditionFor3200(program_ad=97)
    #         self.config.program_start_conditions[28].set_event_switch_condition_on_pg(
    #             serial_number=self.config.event_switches[1].sn, mode=opcodes.closed)
    #
    #         self.config.program_start_conditions[29] = StartConditionFor3200(program_ad=98)
    #         self.config.program_start_conditions[29].set_event_switch_condition_on_pg(
    #             serial_number=self.config.event_switches[2].sn, mode=opcodes.closed)
    #
    #         self.config.program_start_conditions[30] = StartConditionFor3200(program_ad=99)
    #         self.config.program_start_conditions[30].set_event_switch_condition_on_pg(
    #             serial_number=self.config.event_switches[3].sn, mode=opcodes.closed)
    #
    #         self.config.program_stop_conditions[26] = StopConditionFor3200(program_ad=1)
    #         self.config.program_stop_conditions[26].set_event_switch_condition_on_pg(
    #             serial_number=self.config.event_switches[5].sn, mode=opcodes.closed)
    #
    #         self.config.program_stop_conditions[27] = StopConditionFor3200(program_ad=96)
    #         self.config.program_stop_conditions[27].set_event_switch_condition_on_pg(
    #             serial_number=self.config.event_switches[1].sn, mode=opcodes.closed)
    #
    #         self.config.program_stop_conditions[28] = StopConditionFor3200(program_ad=97)
    #         self.config.program_stop_conditions[28].set_event_switch_condition_on_pg(
    #             serial_number=self.config.event_switches[2].sn, mode=opcodes.closed)
    #
    #         self.config.program_stop_conditions[29] = StopConditionFor3200(program_ad=98)
    #         self.config.program_stop_conditions[29].set_event_switch_condition_on_pg(
    #             serial_number=self.config.event_switches[3].sn, mode=opcodes.closed)
    #
    #         self.config.program_stop_conditions[30] = StopConditionFor3200(program_ad=99)
    #         self.config.program_stop_conditions[30].set_event_switch_condition_on_pg(
    #             serial_number=self.config.event_switches[4].sn, mode=opcodes.closed)
    #
    #     except AssertionError, ae:
    #         raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
    #                         formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)
    #
    # def step_36(self):
    #     """
    #     - set sw[4] to closed \n
    #     - start programs 1 \n
    #     - stop program 99 \n
    #     - set the time to 10:15:00 \n
    #     - Program status: \n
    #         - programs 1, 2, 3 running \n
    #         - programs 4-10 waiting \n
    #         - program 96 done \n
    #         - program 97 done \n
    #         - program 98 paused \n
    #         - program 99 done \n
    #     - Zone status: \n
    #         - zones 1-5 watering \n
    #         - zones 6-20 waiting \n
    #         - zones 81-84 done \n
    #         - zones 85-88 done \n
    #         - zones 89-92 paused \n
    #         - zones 93-96 done \n
    #     """
    #     method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
    #     print method
    #     try:
    #         self.config.controllers[1].do_increment_clock(minutes=5)
    #         self.config.controllers[1].verify_date_and_time()
    #         self.config.event_switches[4].set_contact_state_on_cn(opcodes.closed)
    #         self.config.event_switches[4].do_self_test()
    #
    #         for zone in range(1, 6):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.watering)
    #         for zone in range(6, 21):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)
    #         for zone in range(81, 97):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #
    #         self.config.programs[1].get_data()
    #         self.config.programs[1].verify_status_on_cn(opcodes.running)
    #         self.config.programs[96].get_data()
    #         self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[97].get_data()
    #         self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[98].get_data()
    #         self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[99].get_data()
    #         self.config.programs[99].verify_status_on_cn(opcodes.done_watering)
    #     except AssertionError, ae:
    #         raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
    #                         formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)
    #
    # def step_37(self):
    #     """
    #     - set sw[5] to closed \n
    #     - start programs 96 \n
    #     - stop program 1 \n
    #     - set the time to 10:20:00 \n
    #     - Program status: \n
    #         - programs 1 done \n
    #         - program 96 running \n
    #         - program 97 done \n
    #         - program 98 paused \n
    #         - program 99 done \n
    #     - Zone status: \n
    #         - zones 1-20 done \n
    #         - zones 81-84 watering \n
    #         - zones 85-88 done \n
    #         - zones 89-92 paused \n
    #         - zones 93-96 done \n
    #     """
    #     method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
    #     print method
    #     try:
    #         self.config.controllers[1].do_increment_clock(minutes=5)
    #         self.config.controllers[1].verify_date_and_time()
    #         self.config.event_switches[5].set_contact_state_on_cn(opcodes.closed)
    #         self.config.event_switches[5].do_self_test()
    #
    #         for zone in range(1, 21):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #         for zone in range(81, 85):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.watering)
    #         for zone in range(85, 89):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #         for zone in range(89, 93):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #         for zone in range(93, 97):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #
    #         self.config.programs[1].get_data()
    #         self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[96].get_data()
    #         self.config.programs[96].verify_status_on_cn(opcodes.running)
    #         self.config.programs[97].get_data()
    #         self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[98].get_data()
    #         self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[99].get_data()
    #         self.config.programs[99].verify_status_on_cn(opcodes.done_watering)
    #     except AssertionError, ae:
    #         raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
    #                         formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)
    #
    # def step_38(self):
    #     """
    #     - set sw[1] to closed \n
    #     - start programs 97 \n
    #     - stop program 96 \n
    #     - set the time to 10:25:00 \n
    #     - Program status: \n
    #         - programs 1 done \n
    #         - program 96 done \n
    #         - program 97 running \n
    #         - program 98 paused \n
    #         - program 99 done \n
    #     - Zone status: \n
    #         - zones 1-20 done \n
    #         - zones 81-84 done \n
    #         - zones 85-88 watering \n
    #         - zones 89-92 paused \n
    #         - zones 93-96 done \n
    #     """
    #     method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
    #     print method
    #     try:
    #         self.config.controllers[1].do_increment_clock(minutes=5)
    #         self.config.controllers[1].verify_date_and_time()
    #         self.config.event_switches[1].set_contact_state_on_cn(opcodes.closed)
    #         self.config.event_switches[1].do_self_test()
    #
    #         for zone in range(1, 21):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #         for zone in range(81, 85):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #         for zone in range(85, 89):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.watering)
    #         for zone in range(89, 93):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #         for zone in range(93, 97):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #
    #         self.config.programs[1].get_data()
    #         self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[96].get_data()
    #         self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[97].get_data()
    #         self.config.programs[97].verify_status_on_cn(opcodes.running)
    #         self.config.programs[98].get_data()
    #         self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[99].get_data()
    #         self.config.programs[99].verify_status_on_cn(opcodes.done_watering)
    #     except AssertionError, ae:
    #         raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
    #                         formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)
    #
    # def step_39(self):
    #     """
    #     - set sw[2] to closed \n
    #     - start programs 98 \n
    #     - stop program 97 \n
    #     - set the time to 10:30:00 \n
    #     - Program status: \n
    #         - programs 1 done \n
    #         - program 96 done \n
    #         - program 97 done \n
    #         - program 98 running \n
    #         - program 99 done \n
    #     - Zone status: \n
    #         - zones 1-20 done \n
    #         - zones 81-84 done \n
    #         - zones 85-88 done \n
    #         - zones 89-92 watering \n
    #         - zones 93-96 done \n
    #     """
    #     method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
    #     print method
    #     try:
    #         self.config.controllers[1].do_increment_clock(minutes=5)
    #         self.config.controllers[1].verify_date_and_time()
    #         self.config.event_switches[2].set_contact_state_on_cn(opcodes.closed)
    #         self.config.event_switches[2].do_self_test()
    #
    #         for zone in range(1, 21):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #         for zone in range(81, 85):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #         for zone in range(85, 89):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #         for zone in range(89, 93):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.watering)
    #         for zone in range(93, 97):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #
    #         self.config.programs[1].get_data()
    #         self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[96].get_data()
    #         self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[97].get_data()
    #         self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[98].get_data()
    #         self.config.programs[98].verify_status_on_cn(opcodes.running)
    #         self.config.programs[99].get_data()
    #         self.config.programs[99].verify_status_on_cn(opcodes.done_watering)
    #     except AssertionError, ae:
    #         raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
    #                         formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)
    #
    # def step_40(self):
    #     """
    #     - set sw[3] to closed \n
    #     - start programs 99 \n
    #     - stop program 98 \n
    #     - set the time to 10:35:00 \n
    #     - Program status: \n
    #         - programs 1 done \n
    #         - program 96 done \n
    #         - program 97 done \n
    #         - program 98 done \n
    #         - program 99 running \n
    #     - Zone status: \n
    #         - zones 1-20 done \n
    #         - zones 81-84 done \n
    #         - zones 85-88 done \n
    #         - zones 89-92 watering \n
    #         - zones 93-96 done \n
    #     """
    #     method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
    #     print method
    #     try:
    #         self.config.controllers[1].do_increment_clock(minutes=5)
    #         self.config.controllers[1].verify_date_and_time()
    #         self.config.event_switches[3].set_contact_state_on_cn(opcodes.closed)
    #         self.config.event_switches[3].do_self_test()
    #
    #         for zone in range(1, 21):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #         for zone in range(81, 85):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #         for zone in range(85, 89):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #         for zone in range(89, 93):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.done_watering)
    #         for zone in range(93, 97):
    #             self.config.zones[zone].get_data()
    #             self.config.zones[zone].verify_status_on_cn(opcodes.watering)
    #
    #         self.config.programs[1].get_data()
    #         self.config.programs[1].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[96].get_data()
    #         self.config.programs[96].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[97].get_data()
    #         self.config.programs[97].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[98].get_data()
    #         self.config.programs[98].verify_status_on_cn(opcodes.done_watering)
    #         self.config.programs[99].get_data()
    #         self.config.programs[99].verify_status_on_cn(opcodes.running)
    #     except AssertionError, ae:
    #         raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
    #                         formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)
    #
    # def step_41(self):
    #     """
    #     Verify the entire configuration
    #     :return:
    #     :rtype:
    #     """
    #
    #     method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
    #     print method
    #     try:
    #         self.config.controllers[1].do_increment_clock(minutes=1)
    #         # time.sleep(20)
    #         self.config.verify_full_configuration()
    #         # this turns off fast sim mode and start the clock
    #     except AssertionError, ae:
    #         raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
    #                         formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

