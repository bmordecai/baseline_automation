import sys
from time import sleep

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_3200 import StartConditionFor3200, \
                                                                StopConditionFor3200, \
                                                                PauseConditionFor3200

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# this import allows us to directly use the date_mngr
from datetime import time

# Browser pages used
import page_factory

__author__ = 'Tige'


class ControllerUseCase5(object):
    """
    Test name:
        - Using empty conditions of pocs
    purpose:
        -test and verify that pocs empty condition work on empty open empty closed and moisture sensors
    Coverage area: \n
        This test covers usigning flow to water. It covers the use of both POC and Mainline \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n
        - setting programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign programs to water sources \n
            - assign zones to programs \n
            - set up primary linked zones \n
            - give each zone a run time of 1 hour and 30 minutes \n
            - assign moisture sensors to a primary zone 1 and set to lower limit watering

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow\n
                - high variance limit
                - high variance shut down \n
                - low variance limit
                - low variance shut down \n
        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n
                - priority
                    - low
                    - medium
                    - high\n
            - set high flow limit
            - high flow shut down \n
            - set unscheduled flow limit
            - enable unscheduled flow shut down \n
            - set water budget
            - enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions
                - event switch\n
                - set switch empty condition to closed \n
                - set empty wait time\n
        Verifiers:
            - verify open or closing a switch triggers and empty condition
            - verify the empty condition time to wait
            - verify status on zone
            - verify message on zone
            - verify message on program
            - verify message on mainline
            - verify message on poc
            - set up three different poc with only the empty condition being defferent
            -  assign 3 poc's to 1 mainline
            -  assign 3 zones to one program
    Date References:
        - configuration for script is located common\configuration_files\using_real_time_flow.json
        - the devices and addresses range is read from the .json file

    """
    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        Step 1:
            - configure controller:
                -  initiate controller to a known state so that it doesnt have a configuration or any devices loaded
                - turn on echo so the commands are displayed in the console
                - turn on sim mode so the clock can be stopped
                - stop the clock
                - Set the date and time so  that the controller is in a known state
                - turn on faux IO
                - clear all devices
                - clear all programming

        step 2:
            - setting up devices:
                - Loading devices into controller
                - Searching for devices so that they can be addressed
                - Address zones and master valves
                - Set all devices:
                    - descriptions
                    - locations
                    - default parameters
        Step 3:
            - setting up programming:
                - set_up_programs
                - assign zones to programs \n
                - set up primary linked zones \n
        step 4:
            - setup zone programs: \n
            - use pimary and linked zones \n

        Step 5:
            - setting up mainlines: \n
                set limit zones by flow to true \n
                set the pipe fill time to 4 minutes \n
                set the target flow to 500 \n
                set the high variance limit to 5% and enable the high variance shut down \n
                set the low variance limit to 20% and enable the low variance shut down \n
                \n
        Step 6:
         set_poc_3200
                set up POC 1 \n
                    - enable POC 1 \n
                    - assign master valve TMV0001 and flow meter TWF0001 to POC 1 \n
                    - assign POC 1 a target flow of 500 \n
                    - assign POC 1 to main line 1 \n
                    - set POC priority to 2-medium \n
                    - set high flow limit to 550 and enable high flow shut down \n
                    - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                    - set water budget to 100000 and enable the water budget shut down \n
                    - enable water rationing \n
                    - empty conditions:
                        - switch
                        - closed
                        - wait time 6 minutes
                \n
                set up POC 2 \n
                    - enable POC 2 \n
                    - assign master valve TMV0002 and flow meter TWF0002 to POC 3 \n
                    - assign POC 2 a target flow of 500 \n
                    - assign POC 2 to main line 1 \n
                    - set POC priority to 2-medium \n
                    - set high flow limit to 550 and enable high flow shut down \n
                    - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                    - set water budget to 100000 and enable the water budget shut down \n
                    - enable water rationing \n
                    - empty conditions:
                        - switch
                        - open
                        - wait time 6 minutes
                \n
                set up POC 3 \n
                    - enable POC 3 \n
                    - assign master valve TMV0003 and flow meter TWF0003 to POC 5 \n
                    - assign POC 3 a target flow of 500 \n
                    - assign POC 3 to main line 1 \n
                    - set POC priority to 2-medium \n
                    - set high flow limit to 550 and enable high flow shut down \n
                    - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                    - set water budget to 100000 and enable the water budget shut down \n
                    - enable water rationing \n
                    - empty conditions:
                        - moisture sensor
                        - below 15%
                        - wait time 6 minutes
                \n
             -
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - sets up the controller \n
        - verify BaseManager connection
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        # # load all devices need into the controller so that they are available for use in the configuration
        try:
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                             ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_3(self):
        """
        this is set in the PG3200 object
        set_up_programs
            Program 1:
                - priority 2 (medium)
                - start time 6:00 am
                - water window to be open all of the time
                - watering days are set to weekly
                - Watering days enabled all days
                - Mainline set to 1
                - Max concurrent Zone set to 1
        """

        # TODO need to have concurrent zones per program added

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        program_6am_start_time = [360]
        every_day_watering_days = [1, 1, 1, 1, 1, 1, 1]  # run every day
        full_open_water_windows = ['111111111111111111111111']
        try:
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _ww=full_open_water_windows,
                                             _pr=2,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.week_days,
                                             _di=None,
                                             _wd=every_day_watering_days,
                                             _sm=[],
                                             _st=program_6am_start_time,
                                             _ml=1,
                                             _bp='')
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)


    def step_4(self):
        """
        Zone Programs
            - assign zones to program 1 \n
                - set up primary and linked zones \n
                    - set the primary zone to have a runtime 15 minutes \n
                    - set the primary zone to have a cycle-time 5 minutes \n
                    - set the primary zone to have a soak-time 5 minutes \n
                    - link zones 2 and 3 to primary zone 1
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=1)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=1)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_5(self):
        """
        set_mainlines_3200
        set up main line 1 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        try:
            self.config.mainlines[1] = Mainline(_ad=1,
                                                _ft=4,
                                                _fl=500,
                                                _lc=opcodes.true,
                                                _hv=5,
                                                _hs=opcodes.true,
                                                _lv=20,
                                                _ls=opcodes.true)

        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_6(self):
        """
        set_poc_3200
        set up POC 1 \n
            - enable POC 1 \n
            - assign master valve TMV0001 and flow meter TWF0001 to POC 1 \n
            - assign POC 1 a target flow of 500 \n
            - assign POC 1 to main line 1 \n
            - set POC priority to 2-medium \n
            - set high flow limit to 550 and enable high flow shut down \n
            - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            - set water budget to 100000 and enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions:
                - switch
                - closed
                - wait time 6 minutes
        \n
        set up POC 2 \n
            - enable POC 2 \n
            - assign master valve TMV0002 and flow meter TWF0002 to POC 3 \n
            - assign POC 2 a target flow of 500 \n
            - assign POC 2 to main line 1 \n
            - set POC priority to 2-medium \n
            - set high flow limit to 550 and enable high flow shut down \n
            - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            - set water budget to 100000 and enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions:
                - switch
                - open
                - wait time 6 minutes
        \n
        set up POC 3 \n
            - enable POC 3 \n
            - assign master valve TMV0003 and flow meter TWF0003 to POC 5 \n
            - assign POC 3 a target flow of 500 \n
            - assign POC 3 to main line 1 \n
            - set POC priority to 2-medium \n
            - set high flow limit to 550 and enable high flow shut down \n
            - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            - set water budget to 100000 and enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions:
                - moisture sensor
                - below 15%
                - wait time 6 minutes
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1] = POC3200(
                _ad=1,
                _en=opcodes.true,
                _mv=1,
                _fm=1,
                _fl=500,
                _ml=1,
                _pr=2,
                _hf=550,
                _hs=opcodes.true,
                _uf=10,
                _us=opcodes.true,
                _wb=100000,
                _ws=opcodes.true,
                _wr=opcodes.true,
                _sw=1,
                _se=opcodes.closed,
                _sn=opcodes.true,
                _ew=6
            )
            self.config.poc[2] = POC3200(
                _ad=2,
                _en=opcodes.true,
                _mv=2,
                _fm=2,
                _fl=500,
                _ml=1,
                _pr=2,
                _hf=550,
                _hs=opcodes.true,
                _uf=10,
                _us=opcodes.true,
                _wb=100000,
                _ws=opcodes.true,
                _wr=opcodes.true,
                _sw=2,
                _se=opcodes.open,
                _sn=opcodes.true,
                _ew=6
            )
            self.config.poc[3] = POC3200(
                _ad=3,
                _en=opcodes.true,
                _mv=3,
                _fm=3,
                _fl=500,
                _ml=1,
                _pr=2,
                _hf=550,
                _hs=opcodes.true,
                _uf=10,
                _us=opcodes.true,
                _wb=100000,
                _ws=opcodes.true,
                _wr=opcodes.true,
                _ms=1,
                _me=15,
                _mn=opcodes.true,
                _ew=6
            )
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_7(self):
        """
        Verify full configuration
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            # time.sleep(20)
            self.config.verify_full_configuration()
            # this turns off fast sim mode and start the clock
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)

    def step_8(self):
        """
        set default values for all empty conditions so that all poc can go to a watering state
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.contacts_open)
            self.config.event_switches[1].self_test_and_update_object_attributes()
            self.config.event_switches[2].set_contact_state_on_cn(_contact_state=opcodes.contacts_closed)
            self.config.event_switches[2].self_test_and_update_object_attributes()
            self.config.moisture_sensors[1].self_test_and_update_object_attributes()
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(25.0)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_9(self):
        """
        POC 1
            - has an empty condition of a switch
            - it triggers on a open even
            - once the switch closes it waits 6 minutes to turn back on the point of connection
        process:
            - Start by setting clock to a time away from the start time
            - Start program
            - verify all zone and master valve turn on correctly
            - close switch
            - increment clock 2 minutes
            - the master valve on poc 1 will be set to done because there is no water available do to the empty condition
            - verify a message is posted
            - the other two master valves on poc 2 and 3 will continue to water
            - re-open the switch to clear the empty condition
            - but do to the wait timer the master should not turn back on for 6 minutes
            - increment clock only 4 minutes this is to check to verify the wait timer is working
            - after the 4 minutes the master valve for poc 1 is still off
            - increment the clock other 3 minute and verify that the master on poc 1 gets set back to watering
            - Verify that the message was cleared
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].set_date_and_time_on_cn(_date='08/27/2014', _time='23:45:00')
            self.config.controllers[1].verify_date_and_time()
            self.config.controllers[1].do_increment_clock(minutes=2)
            self.config.controllers[1].verify_date_and_time()

            ##################
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.start_program)
            self.config.controllers[1].do_increment_clock(minutes=2)
            self.config.controllers[1].verify_date_and_time()

            self.config.zones[1].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].get_data_and_verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].get_data_and_verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.master_valves[1].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[2].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[3].get_data_and_verify_status_on_cn(status=opcodes.watering)

            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.contacts_closed)
            self.config.event_switches[1].self_test_and_update_object_attributes()

            self.config.controllers[1].do_increment_clock(minutes=2)
            self.config.controllers[1].verify_date_and_time()
            self.config.poc[1].verify_message_on_cn(opcodes.empty_shutdown)


            self.config.zones[1].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].get_data_and_verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].get_data_and_verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.master_valves[1].get_data_and_verify_status_on_cn(status=opcodes.done_watering)
            self.config.master_valves[2].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[3].get_data_and_verify_status_on_cn(status=opcodes.watering)

            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.contacts_open)
            self.config.event_switches[1].self_test_and_update_object_attributes()

            self.config.controllers[1].do_increment_clock(minutes=4)
            self.config.controllers[1].verify_date_and_time()

            self.config.zones[1].get_data_and_verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].get_data_and_verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.master_valves[1].get_data_and_verify_status_on_cn(status=opcodes.done_watering)
            self.config.master_valves[2].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[3].get_data_and_verify_status_on_cn(status=opcodes.watering)

            self.config.controllers[1].do_increment_clock(minutes=3)
            self.config.controllers[1].verify_date_and_time()
            self.config.poc[1].verify_message_not_present_on_cn(_status_code=opcodes.empty_shutdown)

            self.config.zones[1].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].get_data_and_verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].get_data_and_verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.master_valves[1].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[2].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[3].get_data_and_verify_status_on_cn(status=opcodes.watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_10(self):
        """
        POC 2
            - has an empty condition of a switch
            - it triggers on a closed even
            - once the switch closes it waits 6 minutes to turn back on the point of connection
        process:
            - the program is still running from step 9
            - verify all zone and master valve turn on correctly
            - open switch
            - increment clock 2 minutes
            - the master valve on poc 2 will be set to done because there is no water available do to the empty condition
            - verify a message is posted
            - the other two master valves on poc 1 and 3 will continue to water
            - re-close the switch to clear the empty condition
            - but do to the wait timer the master should not turn back on for 6 minutes
            - increment the clock other 6 minute and verify that the master on poc 2 gets set back to watering
            - Verify that the message was cleared
            - also verify that the zones continue to follow the correct run time, cycle time, and soak time
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.event_switches[2].set_contact_state_on_cn(_contact_state=opcodes.contacts_open)
            self.config.event_switches[2].self_test_and_update_object_attributes()

            self.config.controllers[1].do_increment_clock(minutes=2)
            self.config.controllers[1].verify_date_and_time()

            self.config.poc[2].verify_message_on_cn(opcodes.empty_shutdown)

            self.config.zones[1].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].get_data_and_verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].get_data_and_verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.master_valves[1].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[2].get_data_and_verify_status_on_cn(status=opcodes.done_watering)
            self.config.master_valves[3].get_data_and_verify_status_on_cn(status=opcodes.watering)

            self.config.event_switches[2].set_contact_state_on_cn(_contact_state=opcodes.contacts_closed)
            self.config.event_switches[2].self_test_and_update_object_attributes()

            self.config.controllers[1].do_increment_clock(minutes=7)
            self.config.controllers[1].verify_date_and_time()

            self.config.poc[2].verify_message_not_present_on_cn(_status_code=opcodes.empty_shutdown)

            self.config.zones[1].get_data_and_verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].get_data_and_verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.master_valves[1].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[2].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[3].get_data_and_verify_status_on_cn(status=opcodes.watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_11(self):
        """
        POC 2
            - has an empty condition of a moisture sensor
            - it triggers when the threshold drops below 15
            - once the threshold return to above 15 it waits 6 minutes to turn back on the point of connection
        process:
            - the program is still running from step 9 and 10
            - verify all zone and master valve turn on correctly
            - set moisture sensor to 14% which is below the threshold of 15%
            - increment clock 2 minutes
            - the master valve on poc 3 will be set to done because there is no water available do to the empty condition
            - verify a message is posted
            - the other two master valves on poc 1 and 2 will continue to water
            - re-set the moisture sensor to 16% which is above the threshold
            - but do to the wait timer the master should not turn back on for 6 minutes
            - increment the clock other 6 minute and verify that the master on poc 3 gets set back to watering
            - Verify that the message was cleared
            - also verify that the zones continue to follow the correct run time, cycle time, and soak time
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(14.0)
            self.config.moisture_sensors[1].self_test_and_update_object_attributes()

            self.config.controllers[1].do_increment_clock(minutes=2)
            self.config.controllers[1].verify_date_and_time()

            self.config.poc[3].verify_message_on_cn(_status_code=opcodes.empty_shutdown)

            self.config.zones[1].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].get_data_and_verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].get_data_and_verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.master_valves[1].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[2].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[3].get_data_and_verify_status_on_cn(status=opcodes.done_watering)

            self.config.moisture_sensors[1].set_moisture_percent_on_cn(16.0)
            self.config.moisture_sensors[1].self_test_and_update_object_attributes()

            self.config.controllers[1].do_increment_clock(minutes=7)
            self.config.controllers[1].verify_date_and_time()

            self.config.poc[3].verify_message_not_present_on_cn(_status_code=opcodes.empty_shutdown)

            self.config.zones[1].get_data_and_verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].get_data_and_verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.master_valves[1].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[2].get_data_and_verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[3].get_data_and_verify_status_on_cn(status=opcodes.watering)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_12(self):
        """
        Verify the full configuration so that nothing has changed
        :return:
        :rtype:
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].do_increment_clock(minutes=1)
            # time.sleep(20)
            self.config.verify_full_configuration()
            # this turns off fast sim mode and start the clock
        except AssertionError, ae:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)




