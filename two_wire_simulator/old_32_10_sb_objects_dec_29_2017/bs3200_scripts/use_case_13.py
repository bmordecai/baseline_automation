import sys
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from datetime import time, timedelta, datetime, date
# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# Browser pages used
# import page_factory

__author__ = 'Tige'


class ControllerUseCase13(object):
    """
    Test name:
        - CN UseCase13 learn flow
    purpose:
        - set up a full configuration on the controller
            - check that a controller can learn flow accross on multple main lines at the same time
                - verify success learn flow
                - verify fail learn flow
    Coverage area: \n
        1. Setting up the controller \n
        2. Searching and assigning: \n
            - Zones \n
            - Master Valves \n
            - Flow Meters \n
        3. Assign each zone a design flow \n
        4. Setting up water sources \n
        5. Set up the programs \n
        6. Assign each zone to a program \n
        7. Set up the zone concurrency for the controller and programs \n
        7. learn flow failure with 0 flow
        8. learn flow success and zone designg flows are updated
        8. Verify the full configuration
        """
        #TODO Bug the controller will allow two zones to learn flow if they are on different mainline but total
            # zone concurance is set to 1 there for not both program should be learning flow
            # create another set to verify zone concurrency during a learn flow

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str \n

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str \n

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    self.step_14()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        sets up the controller \n
            - verify BaseManager connection \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set the time out on the serial port \n
            - set max concurrency on controller to 1 we only want 1 zone to run at a time
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()

            # only need this for BaseManager
            # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock
            # for the reboot process.
            # self.config.controllers[1].set_sim_mode_to_off()
            self.config.basemanager_connection[1].verify_ip_address_state()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=1)
        except AssertionError, ae:
            raise AssertionError("Limit the total number of concurrent zones on the controller failed: " +
                                 str(ae.message))

    def step_2(self):
        """
        - sets the devices that will be used in the configuration of the controller \n
        - search and address the devices:
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Load all devices to controller
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                             ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_3(self):
        """
        set_up_programs
        assign zones to programs \n
        set up primary linked zones \n
        give each zone a run time of 1 hour and 30 minutes \n
        give each program a start time of 8:00 A.M. \n
        must make zone 200 a primary zone before you can link zones to it \n
        assign sensor to primary zone 1
        #assign moisture sensors to a primary zone 1 and set to lower limit watering
        """
        # this is set in the PG3200 object

        program_start_times = [600] # 10:00am start time
        program_watering_days = [1, 1, 1, 1, 1, 1, 1]  # runs all days
        program_water_windows = ['111111111111111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _ww=program_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.week_days,
                                             _di=None,
                                             _wd=program_watering_days,
                                             _sm=[],
                                             _st=program_start_times,
                                             _ml=1,
                                             _bp='')
            self.config.programs[99] = PG3200(_ad=99,
                                              _en=opcodes.true,
                                              _ww=program_water_windows,
                                              _pr=1,
                                             _mc=1,
                                              _sa=100,
                                              _ci=opcodes.week_days,
                                              _di=None,
                                              _wd=program_watering_days,
                                              _sm=[],
                                              _st=program_start_times,
                                              _ml=8,
                                              _bp='')

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_4(self):
        """
        - set up Zone Programs
            - add zones to program 1
                - zone 1 is a primary
                    - _rt = runtime 960 seconds
                    - _ct = cycle time = 300 seconds
                    - _so = soak time = 300 seconds
                    - zones 2-5 are linked ot zone 1
            - add zones to program 99
                - zones 6 - 10 are all set to timed
                    - _rt = runtime 300 seconds
                    - _ct = cycle time = 120 seconds
                    - _so = soak time = 120 seconds
                    - _pz = 0 means timed

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=960,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _pz=1)
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _pz=1)
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _pz=1)
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _pz=1)

            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[6],
                                                       prog_obj=self.config.programs[99],
                                                       _rt=300,
                                                       _ct=120,
                                                       _so=120,
                                                       _pz=0)
            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[7],
                                                       prog_obj=self.config.programs[99],
                                                       _rt=300,
                                                       _ct=120,
                                                       _so=120,
                                                       _pz=0)
            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[8],
                                                       prog_obj=self.config.programs[99],
                                                       _rt=300,
                                                       _ct=120,
                                                       _so=120,
                                                       _pz=0)
            self.config.zone_programs[9] = ZoneProgram(zone_obj=self.config.zones[9],
                                                       prog_obj=self.config.programs[99],
                                                       _rt=300,
                                                       _ct=120,
                                                       _so=120,
                                                       _pz=0)
            self.config.zone_programs[10] = ZoneProgram(zone_obj=self.config.zones[10],
                                                        prog_obj=self.config.programs[99],
                                                        _rt=300,
                                                        _ct=120,
                                                        _so=120,
                                                        _pz=0)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_5(self):
        """
        Give each zone a design flow
            - zones 1 -5 20 gpm
            - zones 6 - 10 50 gpm
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.zones[1].set_design_flow_on_cn(_df=2)
            self.config.zones[2].set_design_flow_on_cn(_df=2)
            self.config.zones[3].set_design_flow_on_cn(_df=2)
            self.config.zones[4].set_design_flow_on_cn(_df=2)
            self.config.zones[5].set_design_flow_on_cn(_df=2)
            self.config.zones[6].set_design_flow_on_cn(_df=5)
            self.config.zones[7].set_design_flow_on_cn(_df=5)
            self.config.zones[8].set_design_flow_on_cn(_df=5)
            self.config.zones[9].set_design_flow_on_cn(_df=5)
            self.config.zones[10].set_design_flow_on_cn(_df=5)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_6(self):
        """
        set_mainlines_3200
        - set up main line 1 \n
            - set limit zones by flow to true \n
            - set the pipe fill time to 4 minutes \n
            - set the target flow to 500 \n
            - set the high variance limit to 5% and enable the high variance shut down \n
            - set the low variance limit to 20% and enable the low variance shut down \n
        - set up main line 8 \n
            - set limit zones by flow to true \n
            - set the pipe fill time to 1 minutes \n
            - set the target flow to 500 \n
            - set the high variance limit to 5% and enable the high variance shut down \n
            - set the low variance limit to 20% and enable the low variance shut down \n
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.mainlines[1] = Mainline(_ad=1,              # address number
                                                _ft=4,              # pipe file time in minutes
                                                _fl=20,             # Target flow
                                                _lc=opcodes.true,   # limit zones by flow
                                                _hv=5,              # high flow variance in percentage
                                                _hs=opcodes.true,   # high flow variance shutdown enabled
                                                _lv=20,             # low flow variance in percentage
                                                _ls=opcodes.true)   # low flow variance shutdown

            self.config.mainlines[8] = Mainline(_ad=8,              # address number
                                                _ft=1,              # pipe file time in minutes
                                                _fl=50,             # Target flow
                                                _lc=opcodes.true,   # limit zones by flow
                                                _hv=10,              # high flow variance in percentage
                                                _hs=opcodes.false,   # high flow variance shutdown enabled
                                                _lv=5,             # low flow variance in percentage
                                                _ls=opcodes.false)   # low flow variance shutdown

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_7(self):
        """
        set_poc_3200
        set up POC 1 \n
            enable POC 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            assign POC 1 a target flow of 500 \n
            assign POC 1 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1] = POC3200(
                _ad=1,                  # address number
                _en=opcodes.true,       # poc enabled
                _mv=1,                  # poc assigned to master valve 1
                _fm=1,                  # poc assigned to flow meter 1
                _fl=500,                # target flow
                _ml=1,                  # poc assigned to mainline 1
                _pr=2,                  # priority
                _hf=550,                # high flow
                _hs=opcodes.true,       # high flow shutdown enabled
                _uf=10.0,                 # unscheduled flow
                _us=opcodes.true,       # unscheduled flow shutdown enabled
                _wb=100000,             # water budget
                _ws=opcodes.true,       # water budget exceeded shutdown enabled
                _wr=opcodes.true)       # Water Rationing Enable

            self.config.poc[8] = POC3200(
                _ad=8,                  # address number
                _en=opcodes.true,       # poc enabled
                _mv=2,                  # poc assigned to master valve 2
                _fm=2,                  # poc assigned to flow meter 2
                _fl=500,                # target flow
                _ml=8,                  # poc assigned to mainline 8
                _pr=2,                  # priority
                _hf=550,                # high flow
                _hs=opcodes.true,       # high flow shutdown enabled
                _uf=10.0,                 # unscheduled flow
                _us=opcodes.true,       # unscheduled flow shutdown enabled
                _wb=100000,             # water budget
                _ws=opcodes.true,       # water budget exceeded shutdown enabled
                _wr=opcodes.true)       # Water Rationing Enable

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_8(self):
        """
        verify full configuration in objects match the controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_9(self):
        """
        - set flow meter values
            - set flow meter 1 to 0 gpm
            - set flow meter 2 to 0 GPM
            - do a learn flow verify all 10 zones fail and give the failed learn flow message as well as checking the
            design to verify that after a failed learn flow the design flow value is not changed

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=0.0)
            self.config.flow_meters[2].set_flow_rate_on_cn(_flow_rate=0.0)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_10(self):
        """
        learn flow failure
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_current_date_to_match_computer()
            self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='07:59:00')
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=1)
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=99)

            self.config.controllers[1].do_increment_clock(minutes=2)
            # this is looking at zone concurrency because the controller can only run one zone at a time
            # only 1 zone should be learning flow
            self.config.programs[1].get_data()
            self.config.programs[99].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.learn_flow_active)
            self.config.programs[99].verify_status_on_cn(opcodes.learn_flow_active)

            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(opcodes.learn_flow_active)
            for zone in range(2, 6):
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in range(6, 11):
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.controllers[1].do_increment_clock(minutes=2)
            # stop both programs and reset zone concurrency to 2 on the controller
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.stop)
            self.config.controllers[1].set_program_start_stop(_pg_ad=99, _function=opcodes.stop)
            # these message get generated because we stop the program therefore the message is zones didnt learn flow
            # we need to clear theme to continue with the test

            for zone in range(2, 11):
                self.config.zone_programs[zone].verify_message_on_cn(opcodes.learn_flow_fail_flow_biCoders_error)
                self.config.zone_programs[zone].clear_message_on_cn(opcodes.learn_flow_fail_flow_biCoders_error)
            # change controller to have two have a total of two concurrent zones
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=2)
            # increment clock to clear statuses
            self.config.controllers[1].do_increment_clock(minutes=2)

            date_mngr.set_current_date_to_match_computer()
            self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='07:59:00')
            # restart learn flow
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=1)
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=99)

            self.config.controllers[1].do_increment_clock(minutes=1)

            self.config.programs[1].get_data()
            self.config.programs[99].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.learn_flow_active)
            self.config.programs[99].verify_status_on_cn(opcodes.learn_flow_active)

            for zone in self.config.zn_ad_range:
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(opcodes.learn_flow_active)

            for zone in range(2, 6):
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(opcodes.learn_flow_active)
            for zone in range(7, 11):
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)
            not_done = True

            while not_done:

                zones_still_running = False
                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status

                for zone in self.config.zn_ad_range:
                    self.config.zones[zone].get_data()
                    _zone_status = self.config.zones[zone].data.get_value_string_by_key(opcodes.status_code)
                    # for each zone get the ss per zone
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    if _zone_status == opcodes.watering or _zone_status == opcodes.learn_flow_active:
                        self.config.zones[zone].seconds_zone_ran += 60  # using 60 so that we are in seconds
                    # set flag not all zones are done
                    if _zone_status != opcodes.done_watering and _zone_status != opcodes.error:
                        zones_still_running = True

                        # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------

                # this could say if zones_still_running:
                if zones_still_running:
                    not_done = True
                    self.config.controllers[1].do_increment_clock(minutes=1)
                    self.config.controllers[1].verify_date_and_time()
                else:
                    not_done = False
            # self.config.controllers[1].do_increment_clock(minutes=40)
            # TODO have zones run until all are set to done than read messages
            for zone in self.config.zn_ad_range:
                self.config.zone_programs[zone].verify_message_on_cn(opcodes.learn_flow_fail_flow_biCoders_error)
                self.config.zone_programs[zone].clear_message_on_cn(opcodes.learn_flow_fail_flow_biCoders_error)

            for program in self.config.programs.keys():
                self.config.programs[program].verify_message_on_cn(opcodes.learn_flow_with_errors)
                self.config.programs[program].clear_message_on_cn(opcodes.learn_flow_with_errors)
            # check to see if the time each zone took to learn flow is correct
            seconds_learning_flow = (self.config.mainlines[1].ft * 60) + 60
            for zone in range(1, 6):
                if seconds_learning_flow != self.config.zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow")
            seconds_learning_flow = (self.config.mainlines[8].ft * 60) + 60
            for zone in range(6, 11):
                if seconds_learning_flow != self.config.zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow))
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow")
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_11(self):
        """
        - set flow meter values
            - set flow meter 1 to 50 gpm
            - set flow meter 2 to 20 GPM
            - do a learn flow verify all 10 zones fail and give the failed learn flow message as well as checking the
            design to verify that after a failed learn flow the design flow value is not changed

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=50.0)
            self.config.flow_meters[2].set_flow_rate_on_cn(_flow_rate=20.0)

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_12(self):
        """
        learn flow success
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_current_date_to_match_computer()
            self.config.controllers[1].set_date_and_time_on_cn(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='07:59:00')
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=1)
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=99)
            # TODO read how long each zone should run and than calculate against runtime plus pipe file time
            self.config.controllers[1].do_increment_clock(minutes=1)
            not_done = True

            while not_done:

                zones_still_running = False
                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone in self.config.zn_ad_range:
                    self.config.zones[zone].get_data()
                    _zone_status = self.config.zones[zone].data.get_value_string_by_key(opcodes.status_code)
                    # for each zone get the ss per zone
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    if _zone_status == opcodes.watering or _zone_status == opcodes.learn_flow_active:
                        self.config.zones[zone].seconds_zone_ran += 60  # using 60 so that we are in seconds
                    # set flag not all zones are done
                    if _zone_status != opcodes.done_watering and _zone_status != opcodes.error:
                        zones_still_running = True

                        # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------

                # this could say if zones_still_running:
                if zones_still_running:
                    not_done = True
                    self.config.controllers[1].do_increment_clock(minutes=1)
                    self.config.controllers[1].verify_date_and_time()
                else:
                    not_done = False
            for zone in self.config.zn_ad_range:
                self.config.zone_programs[zone].verify_message_on_cn(opcodes.flow_learn_ok)
                self.config.zone_programs[zone].clear_message_on_cn(opcodes.flow_learn_ok)

            for program in self.config.programs.keys():
                self.config.programs[program].verify_message_on_cn(opcodes.learn_flow_success)
                self.config.programs[program].clear_message_on_cn(opcodes.learn_flow_success)
            # check to see if the time each zone took to learn flow is correct
            seconds_learning_flow = (self.config.mainlines[1].ft * 60) + 60
            for zone in range(1, 6):
                if seconds_learning_flow != self.config.zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow")
            seconds_learning_flow = (self.config.mainlines[8].ft * 60) + 60
            for zone in range(6, 11):
                if seconds_learning_flow != self.config.zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow")
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_13(self):
        """
        verify and clear unscheduled flow  \n
            - because the first set of zones got done before the second set and there is still water flowing to poc 8
            - a unscheduled flow is triggered on POC 8
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[8].verify_message_on_cn(opcodes.unscheduled_flow_shutdown)
            self.config.poc[8].clear_message_on_cn(opcodes.unscheduled_flow_shutdown)
        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))

    def step_14(self):
        """
        - update the design flow attribute of object to match the learn flow value expected
        - Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in range(1,6):
                self.config.zones[zone].df = 50.0
            for zone in range(6, 11):
                self.config.zones[zone].df = 20.0
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.controllers[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(e.message))