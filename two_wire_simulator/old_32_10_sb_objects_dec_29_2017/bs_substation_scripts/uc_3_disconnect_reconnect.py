import sys

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

__author__ = 'bens'


class UseCase3DisconnectReconnect(object):
    """
    Test name:
        - Substation Disconnect/Reconnection with 3200.

    Multi-Controller Setup Guide:
    
    Purpose:
        - Test that the 3200 and Substation will actively attempt to reconnect after losing connection to each other.
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Disable substation on 3200 and verify:
            - Substation is disconnected from 3200 via BM status
            - 3200 disconnected from substation status
        
        2. Re-enable substation on 3200 and verify:
            - 3200 is connected to substation via status
            - Substation is connected via BM status
            
        3. Reboot 3200 and wait up to 5 minutes verifying at 1 minute intervals:
            - 3200 is connected to substation via status
            - Substation is connected via BM status
            
        4. Reboot Substation and wait up to 5 minutes verifying at 1 minute intervals:
            - 3200 is connected to substation via status
            - Substation is connected via BM status
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - Currently on a basic disable/enable substation on the 3200, it takes roughly 2-6 increment clock calls per
          controller to get the expected status.
          
        - Currently on a 3200 reboot, it takes 4-6 increment clock calls per controller to get the expected status.
        
        - Currently on a Substation reboot, it takes 11-12 increment clock calls per controller to get the expected 
          status.
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file, substation_firmware_version,
                 number_of_substations_to_use):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        
        :param substation_firmware_version:     Expected substation firmware version.
        :type substation_firmware_version:      str
        
        :param number_of_substations_to_use:    Number of substations to use for the use case.
        :type number_of_substations_to_use:     int
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    sb_fw_version=substation_firmware_version,
                                    number_of_substations_to_use=number_of_substations_to_use)

        self.controllers = self.config.controllers
        self.substations = self.config.substations

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        """
        try:
            self.config.initialize_for_test()
            self.step_1()
            self.step_2()
            self.step_3()
            self.step_4()
            self.step_5()
            self.step_6()

        # Handle an exception
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        # Handle successful test
        else:
            helper_methods.print_test_passed(test_name=self.config.test_name)

        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_multiple_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        1) Initialize controllers and substations.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + \
                 "    ######################"
        print method
        try:
            # Init controller
            self.controllers[1].init_cn()

            # Init each substation created/configured.
            for substation_address in self.config.substations.keys():
                self.substations[substation_address].init_sb()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substations.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + \
                 "    ######################"
        print method
        try:
            helper_methods.connect_controller_to_substations(controller=self.controllers[1],
                                                             substations=self.substations)

            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5)

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_3(self):
        """
        Disable substation on 3200 and verify:
            - Substation is disconnected from 3200 via BM status
            - 3200 disconnected from substation status
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Connect 3200 to Substation
            self.controllers[1].set_connection_to_substation(_address=1, _enabled=opcodes.false)

            # Wait for status change
            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5,
                                                                                 expected_status=opcodes.disconnected)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_4(self):
        """
        Re-enable substation on 3200 and verify:
            - 3200 is connected to substation via status
            - Substation is connected via BM status
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Connect 3200 to Substation
            self.controllers[1].set_connection_to_substation(_address=1, _enabled=opcodes.true)

            # Wait for status change
            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_5(self):
        """
        Reboot 3200 and wait up to 5 minutes verifying at 1 minute intervals:
            - 3200 is connected to substation via status
            - Substation is connected via BM status
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Reboot 3200
            self.controllers[1].do_reboot_controller()

            # Wait for status change
            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=20)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_6(self):
        """
        Reboot Substation and wait up to 5 minutes verifying at 1 minute intervals:
            - 3200 is connected to substation via status
            - Substation is connected via BM status
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Reboot Substation
            self.substations[1].do_reboot()

            # Wait for status change
            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=20)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
