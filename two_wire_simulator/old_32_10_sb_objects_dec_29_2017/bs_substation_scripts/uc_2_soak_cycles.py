import sys

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

__author__ = 'bens'


class UseCase2SoakCycles(object):
    """
    Test name:
        - Substation Soak Cycles

    Multi-Controller Setup Guide:
    
    Purpose:
        - Test and verify that shared Zones between 3200 and Substation return return the same status whether being 
          retrieved through the 3200 or through the Substation.
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Load devices onto both controllers.
        
        2. Search address and set default values for all devices on 3200.
        
        3. Search and sset default values for all devices on Substations.
        
        4. Setup Programs and Program Zones on 3200.
        
        5. Increment clocks forward 1 minute and verify:
            - Configuration on 3200
            - Configuration on Substation
            
        6. Verify initial Zone status' on 3200 and Substation prior to starting both programs.
        
        7. Start the Programs and verify Zone status' update to expected watering status.
        
        8. Simulate watering for 2 hours at 5 minute intervals and verify:
            - Zone status' match between 3200 and Substation
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - This test assumes use of only 1 Substation and 1 3200
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file, substation_firmware_version,
                 number_of_substations_to_use):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        
        :param substation_firmware_version:     Expected substation firmware version.
        :type substation_firmware_version:      str
        
        :param number_of_substations_to_use:    Number of substations to use for the use case.
        :type number_of_substations_to_use:     int
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    sb_fw_version=substation_firmware_version,
                                    number_of_substations_to_use=number_of_substations_to_use)

        self.controllers = self.config.controllers
        self.substations = self.config.substations

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        """
        try:
            self.config.initialize_for_test()
            self.step_1()
            self.step_2()
            self.step_3()
            self.step_4()
            self.step_5()
            self.step_6()
            self.step_7()
            self.step_8()
            self.step_9()
            self.step_10()
            self.step_11()
            self.step_12()
            self.step_13()
            self.step_14()
            self.step_15()
            self.step_16()
            self.step_17()
            self.step_18()
            self.step_19()
            self.step_20()
            self.step_21()
            self.step_22()
            self.step_23()
            self.step_24()
            self.step_25()
            self.step_26()
            self.step_27()
            self.step_28()
            self.step_29()
            self.step_30()
            self.step_31()
            self.step_32()
            self.step_33()

        # Handle an exception
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        # Handle successful test
        else:
            helper_methods.print_test_passed(test_name=self.config.test_name)

        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_multiple_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        1) Initialize controllers and substations.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + \
                 "    ######################"
        print method
        try:
            # Init controller
            self.controllers[1].init_cn()

            # Init each substation created/configured.
            for substation_address in self.config.substations.keys():
                self.substations[substation_address].init_sb()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substations.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + \
                 "    ######################"
        print method
        try:
            helper_methods.connect_controller_to_substations(controller=self.controllers[1],
                                                             substations=self.substations)

            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5)

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        1) Load devices on both Substation and 3200.
        2) Search, address and set default values for devices on 3200.
        3) Search and set default values for devices on Substation.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + \
                 "    ######################"
        print method
        try:
            self.controllers[1].load_assigned_devices()
            self.substations[1].load_assigned_devices()

            self.controllers[1].do_search_for_all_devices()

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # 3200: Address and set default values for Devices
            helper_methods.set_all_device_default_values_on_controller(config=self.config,
                                                                       controller=self.controllers[1])

            # Substation: Set default values for Devices
            helper_methods.set_bicoder_default_values_on_substation(substations=self.substations)

            # Share devices from substation to controller.
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSD0003", address=3)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSD0004", address=4)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSD0005", address=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_4(self):
        """
        1) Create watersource objects for Program.
        2) Create 2 programs.
            - Program 1 and 2:
                + Enabled
                + Start time of 8pm
                + Max concurrent zones: 1
        3) Set controller max concurrent zones to 2
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.create_mainline_objects()

            self.config.programs[1] = PG3200(_ad=1, _en=opcodes.true, _st=[1200], _mc=1)
            self.config.programs[2] = PG3200(_ad=2, _en=opcodes.true, _st=[1200], _mc=1)

            self.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_5(self):
        """
        For 3200, configure 5 Zone Programs.
        
        - Program 1:
            * Zone Program 1:
                + Runtime: 3600
                + Cycle Time: 600
                + Soak Time: 1500
                + Mode: Primary Zone 1
            * Zone Program 2:
                + Runtime Ratio: 200%
                + Mode: Linked to 1
            * Zone Program 3:
                + Runtime Ratio: 50%
                + Mode: Linked to 1
            
        - Program 2:
            * Zone Program 4:
                + Runtime: 3600
                + Cycle Time: 900
                + Soak Time: 900
                + Mode: Timed
            * Zone Program 5:
                + Runtime: 720
                + Cycle Time: 120
                + Soak Time: 300
                + Disabled
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Program 1 Zone Program 1
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600,
                                                       _ct=600,
                                                       _so=1500,
                                                       _pz=1)

            # Program 1 Zone Program 2
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=200,
                                                       _pz=1)

            # Program 1 Zone Program 3
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=50,
                                                       _pz=1)

            # Program 2 Zone Program 1
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=3600,
                                                       _ct=900,
                                                       _so=900,
                                                       _pz=0)

            # Program 2 Zone Program 2
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=720,
                                                       _ct=120,
                                                       _so=300,
                                                       _pz=0)

            self.config.zones[5].set_enable_state_on_cn(_state=opcodes.false)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_6(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()
            self.substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_7(self):
        """
        1) Set date and time on controller and substations to: 04/08/2017 07:59:00
        2) Verify the following status':
            - Programs:
                + Program 1 done watering 
                + Program 2 done watering
            - Zones:
                + Zone 1 done watering
                + Zone 2 done watering
                + Zone 3 done watering
                + Zone 4 done watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0001 done watering
                + TSD0002 done watering
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
        3) Start Programming.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.set_controller_substation_date_and_time(controller=self.config.controllers[1],
                                                                   substations=self.config.substations,
                                                                   _date="04/08/2017", _time="07:59:00")

            # Verify Program 1 is not currently running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)

            # Verify Program 2 is not currently running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)

            # Verify controller zones initial status
            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            # Verify initial status on Substation
            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)

            # Start Program 1 & 2
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.start_program)
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function=opcodes.start_program)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        sim_minute = 08:00:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 watering
                + Zone 2 waiting
                + Zone 3 waiting
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Verify Program 1 started watering
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 started watering
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)

            # Get current data for zones
            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            # Verify initial status on 3200
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            # Verify initial status on Substation
            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        sim_minute = 08:05:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 watering
                + Zone 2 waiting
                + Zone 3 waiting
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_10(self):
        """
        sim_minute = 08:10:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering 
                + Zone 3 waiting
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        sim_minute = 08:15:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering 
                + Zone 3 waiting
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_12(self):
        """
        sim_minute = 08:20:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering 
                + Zone 3 waiting
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_13(self):
        """
        sim_minute = 08:25:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering 
                + Zone 3 waiting
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_14(self):
        """
        sim_minute = 08:30:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 soaking
                + Zone 2 soaking 
                + Zone 3 watering
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 watering
                + TSD0004 watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_15(self):
        """
        sim_minute = 08:35:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 watering
                + Zone 2 soaking 
                + Zone 3 soaking 
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_16(self):
        """
        sim_minute = 08:40:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 watering
                + Zone 2 soaking 
                + Zone 3 soaking 
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_17(self):
        """
        sim_minute = 08:45:00
        Verify the following status':
            - Programs:
                + Program 1 waiting to water
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 soaking 
                + Zone 3 soaking 
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_18(self):
        """
        sim_minute = 08:50:00
        Verify the following status':
            - Programs:
                + Program 1 waiting to water
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 soaking 
                + Zone 3 soaking 
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_19(self):
        """
        sim_minute = 08:55:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking 
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_20(self):
        """
        sim_minute = 09:00:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking 
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_21(self):
        """
        sim_minute = 09:05:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking 
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_22(self):
        """
        sim_minute = 09:10:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking 
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_23(self):
        """
        sim_minute = 09:15:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 waiting to water
            - Zones:
                + Zone 1 watering
                + Zone 2 soaking 
                + Zone 3 soaking 
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_24(self):
        """
        sim_minute = 09:20:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 waiting to water
            - Zones:
                + Zone 1 watering
                + Zone 2 soaking 
                + Zone 3 soaking 
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_25(self):
        """
        sim_minute = 09:25:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 soaking 
                + Zone 3 watering
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_26(self):
        """
        sim_minute = 09:30:00
        Verify the following status':
            - Programs:
                + Program 1 waiting to water
                + Program 2 running 
            - Zones:
                + Zone 1 soaking
                + Zone 2 soaking 
                + Zone 3 soaking
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_27(self):
        """
        sim_minute = 09:35:00
        Verify the following status':
            - Programs:
                + Program 1 waiting to water
                + Program 2 running 
            - Zones:
                + Zone 1 soaking
                + Zone 2 soaking 
                + Zone 3 soaking
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_28(self):
        """
        sim_minute = 09:40:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is running
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_29(self):
        """
        sim_minute = 09:45:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 done watering
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking
                + Zone 4 done watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is done watering
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_30(self):
        """
        sim_minute = 09:50:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 done watering
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking
                + Zone 4 done watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is done watering
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_31(self):
        """
        sim_minute = 09:55:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 done watering
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking
                + Zone 4 done watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is done watering
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_32(self):
        """
        sim_minute = 10:00:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 done watering
            - Zones:
                + Zone 1 watering
                + Zone 2 soaking
                + Zone 3 soaking
                + Zone 4 done watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=5)

            # Verify Program 1 is running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify Program 2 is done watering
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0003'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_33(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()
            self.substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
