import sys
from datetime import timedelta, datetime, date
import time

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.valve_bicoder import ValveBicoder

__author__ = 'bens'


class UseCase5ReplaceDevices(object):
    """
    Test name:
        - Substation Replace Devices

    Multi-Controller Setup Guide:
    
    Purpose:
        - Test and verify controller and substation configurations after rebooting as well as replacing devices.
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Loads initial devices onto both controller and substations.
        2. Sets up initial programming.
        3. Reboots 3200.
        4. Self test's devices and verifies 3200 and substations configuration.
        5. Replace Devices on 3200:
        
            Device:                 Addresses:
            - Zones                 1, 2, 197, 198
            - Master Valve          3 
            - Moisture Sensor       1 
            - Temperature Sensor    1 
            - Event Switch          1
        
        6. Verifies 3200 and substations configuration.
        7. Reboots Substation
        8. Self test's controller devices and verifies controller and substation configurations.
        9. Replace BiCoders on Substation:
        
            BiCoders:           Controller Address:       Serial Numbers:
            - Valve (ZN's)      3, 4                      "TSE0011" -> "TSE0021", "TSE0012" -> "TSE0022"                
            - Valve (MV's)      8                         "TMV0004" -> "TMV0007"              
            - Flow              4                         "TWF0004" -> "TWF0006"
            - Switch            3                         "TPD0003" -> "TPD0005"
            - Temp              2                         "TAT0002" -> "TAT0004"
            - Moisture          3                         "SB07258" -> "SB07184"
            - Pump              N/A
            
        10. Verify controller and substations configurations.
        
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - Test only uses 1 substation.
        
    Controller Programming:
        - Zones:                10  (7-local, 3-shared) 
        - Master Valves:        4   (3-local, 1-shared)
        - Event Switches:       3   (2-local, 1-shared)
        - Temp Sensors:         2   (1-local, 1-shared)
        - Moisture Sensors:     3   (2-local, 1-shared)
        - Flow Meters:          4   (3-local, 1-shared)
        - Programs:             4
        - Zone Programs:        10
        - Mainlines:            2
        - Pocs:                 2
        
    Substation Programming:
        - Valve BiCoders:       3
        - Moisture BiCoders:    1   
        - Temperature BiCoders: 1
        - Switch BiCoders:      1 
        - Flow BiCoders:        1
        - Pump BiCoders:        0
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file, substation_firmware_version,
                 number_of_substations_to_use):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        
        :param substation_firmware_version:     Expected substation firmware version.
        :type substation_firmware_version:      str
        
        :param number_of_substations_to_use:    Number of substations to use for the use case.
        :type number_of_substations_to_use:     int
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    sb_fw_version=substation_firmware_version,
                                    number_of_substations_to_use=number_of_substations_to_use)

        self.controllers = self.config.controllers
        self.substations = self.config.substations

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        """
        try:
            self.config.initialize_for_test()
            self.step_1()
            self.step_2()
            self.step_3()
            self.step_4()
            self.step_5()
            self.step_6()
            self.step_7()
            self.step_8()
            self.step_9()
            self.step_10()
            self.step_11()
            self.step_12()
            self.step_13()
            self.step_14()
            self.step_15()
            self.step_16()
            self.step_17()
            self.step_18()
            self.step_19()
            self.step_20()
            # self.step_21()

        # Handle an exception
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        # Handle successful test
        else:
            helper_methods.print_test_passed(test_name=self.config.test_name)

        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_multiple_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        helper_methods.print_method_name()
        
        try:
            # Init controller
            self.controllers[1].init_cn()

            # Init each substation created/configured.
            for substation_address in self.substations.keys():
                self.substations[substation_address].init_sb()

            helper_methods.set_controller_substation_date_and_time(controller=self.config.controllers[1],
                                                                   substations=self.config.substations,
                                                                   _date="04/08/2017", _time="07:59:00")
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substations:
        - uses the Ip address in the user config files to set the ip address
            Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                  incremented 1 minute at a time for the max minutes or until max number of minutes is met.
        - after connection is established increment clock for each controller 1 minute
            Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                  between controllers.

        """
        helper_methods.print_method_name()
        
        try:
            helper_methods.connect_controller_to_substations(controller=self.controllers[1], 
                                                             substations=self.substations)

            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5)

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.

        Substation Devices Shared:
            - Valves
                + D1: "TMV0004" - Address: 8
                + D2: "TSE0011" - Address: 3, 4
            - Flow BiCoders:
                + "TWF0004" - Address: 4
            - Temp BiCoders:
                + "TAT0002" - Address: 2
            - Moisture BiCoders:
                + "SB07258" - Address: 3
            - Switch BiCoders:
                + "TPD0003" - Address: 3
            - Pump BiCoders:
                + N/A
        """
        helper_methods.print_method_name()
        
        try:
            self.controllers[1].load_assigned_devices()
            self.substations[1].load_assigned_devices()

            self.controllers[1].do_search_for_all_devices()

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # 3200: Address and set default values for Devices
            helper_methods.set_all_device_default_values_on_controller(config=self.config,
                                                                       controller=self.controllers[1])

            # Substation: Set default values for Devices
            helper_methods.set_bicoder_default_values_on_substation(substations=self.substations)

            # Share substation devices with controller
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSE0011", address=3)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSE0012", address=4)
            self.config.share_sb_device_with_cn(device_type=opcodes.master_valve, device_serial="TMV0004", address=8)
            self.config.share_sb_device_with_cn(device_type=opcodes.flow_meter, device_serial="TWF0004", address=4)
            self.config.share_sb_device_with_cn(device_type=opcodes.temperature_sensor, device_serial="TAT0002",
                                                address=2)
            self.config.share_sb_device_with_cn(device_type=opcodes.moisture_sensor, device_serial="SB07258", address=3)
            self.config.share_sb_device_with_cn(device_type=opcodes.event_switch, device_serial="TPD0003", address=3)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        helper_methods.print_method_name()
        
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()

            for sb_address in self.substations.keys():
                self.substations[sb_address].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        Create water sources on 3200.
        """
        helper_methods.print_method_name()
        
        try:
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        Setup programming on 3200.

        Program 1:
            - Start times:              480 (8am), 540 (9am), 600 (10am), 660 (11am)
            - Enabled:                  True
            - Water Windows:            1am-6am, 11am-10pm
            - Priority:                 1
            - Max Concurrent Zones:     1 zone
            - Seasonal Adjust:          100%
            - Calendar Interval:        Week Days
            - Day Interval:             None
            - Watering Week Days:       Monday, Wednesday, Friday
            - Semi-Month Interval:      None
            - Mainline Number:          1
            - Booster Pump:             None

        Program 3:
            - Start times:              480 (8am), 540 (9am), 600 (10am), 660 (11am)
            - Enabled:                  True
            - Water Windows:            1am-6am, 11am-10pm
            - Priority:                 1
            - Max Concurrent Zones:     1 zone
            - Seasonal Adjust:          100%
            - Calendar Interval:        Odd Days
            - Day Interval:             None
            - Watering Week Days:       None
            - Semi-Month Interval:      None
            - Mainline Number:          2
            - Booster Pump:             None

        Program 4: 
            - Start times:              480 (8am), 540 (9am), 600 (10am), 660 (11am)
            - Enabled:                  True
            - Water Windows:            1am-6am, 11am-10pm
            - Priority:                 3
            - Max Concurrent Zones:     4 zones
            - Seasonal Adjust:          100%
            - Calendar Interval:        Week Days
            - Day Interval:             None
            - Watering Week Days:       Monday, Wednesday, Friday
            - Semi-Month Interval:      None
            - Mainline Number:          3
            - Booster Pump:             None

        Program 99:
            - Start times:              480 (8am), 540 (9am), 600 (10am), 660 (11am)
            - Enabled:                  True
            - Water Windows:            Sunday: 1am-6am, 12pm-10pm
                                        Monday: 1am-6am, 11am-11pm
                                        Tuesday: 1am-6am, 11am-10pm
                                        Wednesday: 1am-6am, 11am-10pm
                                        Thursday: 1am-6am, 11am-10pm
                                        Friday: 1am-6am, 11am-10pm
                                        Saturday: 1am-6am, 11am-11pm
            - Priority:                 1
            - Max Concurrent Zones:     1 zone
            - Seasonal Adjust:          100%
            - Calendar Interval:        Historical Calendar
            - Day Interval:             None
            - Watering Week Days:       None
            - Semi-Month Interval:      TODO
            - Mainline Number:          8
            - Booster Pump:             None
        """
        helper_methods.print_method_name()
        
        try:
            program_number_1_start_times = [480, 540, 600, 660]
            program_number_3_start_times = [480, 540, 600, 660]
            program_number_4_start_times = [480, 540, 600, 660]
            program_number_99_start_times = [480, 540, 600, 660]
            program_number_1_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
            program_number_3_watering_days = []
            program_number_4_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
            program_number_99_watering_days = [0, 0, 0, 0, 0, 0, 8, 8, 6, 5, 5, 4, 3, 3, 3, 3, 4, 5, 6, 7, 0, 0, 0, 0]
            program_number_1_water_windows =  ['011111100001111111111110']
            program_number_3_water_windows =  ['011111100001111111111110']
            program_number_4_water_windows =  ['011111100001111111111110']
            program_number_99_water_windows = ['011111100000111111111110',  # Sun
                                               '011111100001111111111111',  # Mon
                                               '011111100001111111111110',  # Tue
                                               '011111100001111111111110',  # Wed
                                               '011111100001111111111110',  # Thu
                                               '011111100001111111111110',  # Fri
                                               '011111100001111111111111']  # Sat
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _ww=program_number_1_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.week_days,
                                             _di=None,
                                             _wd=program_number_1_watering_days,
                                             _sm=[],
                                             _st=program_number_1_start_times,
                                             _ml=1,
                                             _bp='')
            self.config.programs[3] = PG3200(_ad=3,
                                             _en=opcodes.true,
                                             _ww=program_number_3_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.odd_day,
                                             _di=None,
                                             _wd=program_number_3_watering_days,
                                             _sm=[],
                                             _st=program_number_3_start_times,
                                             _ml=2,
                                             _bp='')
            self.config.programs[4] = PG3200(_ad=4,
                                             _en=opcodes.true,
                                             _ww=program_number_4_water_windows,
                                             _pr=3,
                                             _mc=4,
                                             _sa=100,
                                             _ci=opcodes.week_days,
                                             _di=None,
                                             _wd=program_number_4_watering_days,
                                             _sm=[],
                                             _st=program_number_4_start_times,
                                             _ml=3,
                                             _bp='')
            self.config.programs[99] = PG3200(_ad=99,
                                              _en=opcodes.true,
                                              _ww=program_number_99_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.historical_calendar,
                                              _di=None,
                                              _wd=[],
                                              _sm=program_number_99_watering_days,
                                              _st=program_number_99_start_times,
                                              _ml=8,
                                              _bp='')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #################################
    def step_7(self):
        """
        Setup Zone Programs on 3200.

        Zone Program 1:
            - Zone:                 1
            - Program:              1
            - Runtime:              900 (15 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Primary Zone)

        Zone Program 2:
            - Zone:                 2
            - Program:              1
            - Runtime:              900 (15 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Runtime Ratio:        100%
            - Primary Zone Number:  1 (Linked)

        Zone Program 49:
            - Zone:                 49
            - Program:              3
            - Runtime:              1200 (20 minutes)
            - Cycle Time:           600 (10 minutes)
            - Soak Time:            3600 (60 minutes)
            - Runtime Ratio:        100%
            - Water Strategy:       Timed

        Zone Program 50:
            - Zone:                 50
            - Program:              3
            - Runtime:              1200 (20 minutes)
            - Cycle Time:           600 (10 minutes)
            - Soak Time:            3600 (60 minutes)
            - Water Strategy:       Timed

        Zone Program 50:
            - Zone:                 50
            - Program:              4
            - Runtime:              1200 (20 minutes)
            - Cycle Time:           600 (10 minutes)
            - Soak Time:            3600 (60 minutes)
            - Water Strategy:       Timed

        Zone Program 197:
            - Zone:                 197
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Runtime Ratio:        50%
            - Primary Zone Number:  200 (Linked)

        Zone Program 198:
            - Zone:                 198
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Runtime Ratio:        100%
            - Primary Zone Number:  200 (Linked)

        Zone Program 199:
            - Zone:                 199
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Runtime Ratio:        150%
            - Primary Zone Number:  200 (Primary Zone)

        Zone Program 200:
            - Zone:                 200
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Primary Zone Number:  200 (Primary Zone)
        """
        helper_methods.print_method_name()
        
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _ra=100,
                                                       _pz=1)
            self.config.zone_programs[49] = ZoneProgram(zone_obj=self.config.zones[49],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=1200,
                                                        _ct=600,
                                                        _so=3600,
                                                        _ws=opcodes.timed)
            self.config.zone_programs[50] = ZoneProgram(zone_obj=self.config.zones[50],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=1200,
                                                        _ct=600,
                                                        _so=3600,
                                                        _ws=opcodes.timed)
            self.config.zone_programs[50] = ZoneProgram(zone_obj=self.config.zones[50],
                                                        prog_obj=self.config.programs[4],
                                                        _rt=1200,
                                                        _ct=600,
                                                        _so=3600,
                                                        _ws=opcodes.timed)

            self.config.zone_programs[200] = ZoneProgram(zone_obj=self.config.zones[200],
                                                         prog_obj=self.config.programs[99],
                                                         _rt=1980,
                                                         _pz=200,
                                                         _ct=180,
                                                         _so=780)

            # TODO runtime for a linked zone is returning a bad value
            # TODO this zone is also not show up as a linked zone
            # Zone programs linked to Zone 200
            self.config.zone_programs[197] = ZoneProgram(zone_obj=self.config.zones[197],
                                                         prog_obj=self.config.programs[99],
                                                         _rt=self.config.zone_programs[200].rt,
                                                         _ct=self.config.zone_programs[200].ct,
                                                         _so=self.config.zone_programs[200].so,
                                                         _pz=200,
                                                         _ra=50)
            self.config.zone_programs[198] = ZoneProgram(zone_obj=self.config.zones[198],
                                                         prog_obj=self.config.programs[99],
                                                         _rt=self.config.zone_programs[200].rt,
                                                         _ct=self.config.zone_programs[200].ct,
                                                         _so=self.config.zone_programs[200].so,
                                                         _pz=200,
                                                         _ra=100)
            self.config.zone_programs[199] = ZoneProgram(zone_obj=self.config.zones[199],
                                                         prog_obj=self.config.programs[99],
                                                         _rt=self.config.zone_programs[200].rt,
                                                         _ct=self.config.zone_programs[200].ct,
                                                         _so=self.config.zone_programs[200].so,
                                                         _pz=200,
                                                         _ra=150)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #################################
    def step_8(self):
        """
        set up mainline 1:
            - set limit zones by flow to true
            - set the pipe fill time to 4 minutes
            - set the target flow to 500
            - set the high variance limit to 5% and enable the high variance shut down
            - set the low variance limit to 20% and enable the low variance shut down
        
        set up mainline 8:
            - set limit zones by flow to true
            - set the pipe fill time to 1 minute
            - set the target flow to 50
            - set the high variance limit to 20% and disable the high variance shut down
            - set the low variance limit to 5% and disable the low variance shut down
        """
        helper_methods.print_method_name()
        
        try:
            self.config.mainlines[1] = Mainline(_ad=1,
                                                _ft=4,
                                                _fl=500,
                                                _lc=opcodes.true,
                                                _hv=5,
                                                _hs=opcodes.true,
                                                _lv=20,
                                                _ls=opcodes.true)

            self.config.mainlines[8] = Mainline(_ad=8,
                                                _ft=1,
                                                _fl=50,
                                                _lc=opcodes.true,
                                                _hv=20,
                                                _hs=opcodes.false,
                                                _lv=5,
                                                _ls=opcodes.false)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #################################
    def step_9(self):
        """
        set up POC 1:
            - enable POC 1
            - assign master valve TMV0003 and flow meter TWF0003 to POC 1
            - assign POC 1 a target flow of 500
            - assign POC 1 to mainline 1
            - set POC priority to 2-medium
            - set high flow limit to 550 and enable high flow shut down
            - set unscheduled flow limit to 10 and enable unscheduled flow shut down
            - set water budget to 100000 and enable the water budget shut down
            - enable water rationing
        
        set up POC 8:
            - enable POC 8
            - assign master valve TMV0004 and flow meter TWF0004 to POC 8
            - assign POC 8 a target flow of 50
            - assign POC 8 to main line 8
            - set POC priority to 3-low
            - set high flow limit to 75 and disable high flow shut down
            - set unscheduled flow limit to 5 and disable unscheduled flow shut down
            - set water budget to 1000 and disable water budget shut down
            - disable water rationing
            - assign event switch TPD0001 to POC 8
            - set switch empty condition to closed
            - set empty wait time to 540 minutes
        """
        helper_methods.print_method_name()
        
        try:
            self.config.poc[1] = POC3200(
                _ad=1,
                _en=opcodes.true,
                _mv=3,
                _fm=3,
                _fl=500,
                _ml=1,
                _pr=2,
                _hf=550,
                _hs=opcodes.true,
                _uf=10,
                _us=opcodes.true,
                _wb=100000,
                _ws=opcodes.true,
                _wr=opcodes.true
            )

            self.config.poc[8] = POC3200(
                _ad=8,
                _en=opcodes.true,
                _mv=8,
                _fm=4,
                _fl=50,
                _ml=8,
                _pr=3,
                _hf=75,
                _hs=opcodes.false,
                _uf=5,
                _us=opcodes.false,
                _wb=1000,
                _ws=opcodes.false,
                _wr=opcodes.false,
                _sw=1,
                _se=opcodes.closed,
                _ew=540
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Increment the clock to save settings in both the controller and substation \n
        Turn sim mode off on both controller and substation \n
        """
        helper_methods.print_method_name()
        
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Verify controller configuration
            self.config.verify_full_configuration()

            # Verify substation's configurations
            for sb_address in self.config.substations.keys():
                self.config.substations[sb_address].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        1) Reboot controller.
        2) Stop controller's clock.
        3) Wait for substation and controller to reconnect.
        4) Reset controller and substation's date and time to last recorded time in date manager.
        """
        helper_methods.print_method_name()
        
        try:
            self.controllers[1].do_reboot_controller()
            self.controllers[1].stop_clock()

            # Wait for status change
            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=20)

            # Update controller clocks to last known value
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.controllers[1],
                substations=self.config.substations,
                _date=date_mngr.controller_datetime.date_string_for_controller(),
                _time=date_mngr.controller_datetime.time_string_for_controller())

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        1) Self test and update attributes for all controller devices.
        2) Verify controller configuration.
        3) Verify substations configurations.
        """
        helper_methods.print_method_name()
        
        try:
            for zone in self.config.zones.keys():
                self.config.zones[zone].self_test_and_update_object_attributes()
            for mv in self.config.master_valves.keys():
                self.config.master_valves[mv].self_test_and_update_object_attributes()
            for ms in self.config.moisture_sensors.keys():
                self.config.moisture_sensors[ms].self_test_and_update_object_attributes()
            for ts in self.config.temperature_sensors.keys():
                self.config.temperature_sensors[ts].self_test_and_update_object_attributes()
            for fm in self.config.flow_meters.keys():
                self.config.flow_meters[fm].self_test_and_update_object_attributes()
            for sw in self.config.event_switches.keys():
                self.config.event_switches[sw].self_test_and_update_object_attributes()

            # Verify controller configuration
            self.config.verify_full_configuration()

            # Verify substation's configurations
            for sb_address in self.config.substations.keys():
                self.config.substations[sb_address].verify_full_configuration()

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        - load replacement devices on 3200
        

        """
        helper_methods.print_method_name()
        
        try:
            self.config.controllers[1].load_dv_to_cn(dv_type=opcodes.single_valve_decoder,
                                                     list_of_decoder_serial_nums=['TMV0008'])
            self.config.controllers[1].load_dv_to_cn(dv_type=opcodes.four_valve_decoder,
                                                     list_of_decoder_serial_nums=['TSQ0091'])
            self.config.controllers[1].load_dv_to_cn(dv_type=opcodes.moisture_sensor,
                                                     list_of_decoder_serial_nums=['SB01250'])
            self.config.controllers[1].load_dv_to_cn(dv_type=opcodes.temperature_sensor,
                                                     list_of_decoder_serial_nums=['TAT0003'])
            self.config.controllers[1].load_dv_to_cn(dv_type=opcodes.event_switch,
                                                     list_of_decoder_serial_nums=['TPD0004'])
            self.config.controllers[1].load_dv_to_cn(dv_type=opcodes.flow_meter,
                                                     list_of_decoder_serial_nums=['TWF0005'])
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        Replace controller devices. Devices being replaced:
        - search address and replace new devices
            - Zones:
                + Zone 1:   "TSD0001" -> "TSQ0091"
                + Zone 2:   "TSQ0071" -> "TSQ0092"
                + Zone 197: "TSQ0074" -> "TSQ0093"
                + Zone 198: "TSQ0081" -> "TSQ0094"
            - Flow Meters:
                + FM 1: "TWF0001" -> "TWF0005"
            - Master Valves:
                + MV 3: "TMV0003" -> "TMV0008"
            - Temperature Sensors:
                + TS 1: "TAT0001" -> "TAT0003"
            - Moisture Sensors:
                + MS 1: "SB05308" -> "SB01250"
            - Event Switches:
                + SW 1: "TPD0001" -> "TPD0004"
        """
        helper_methods.print_method_name()
        
        try:
            # Need to sleep to wait for the devices to finish loading onto the controller
            time.sleep(15)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            self.config.zones[1].sn = 'TSQ0091'
            self.config.zones[1].set_address_on_cn()
            self.config.zones[2].sn = 'TSQ0092'
            self.config.zones[2].set_address_on_cn()
            self.config.zones[197].sn = 'TSQ0093'
            self.config.zones[197].set_address_on_cn()
            self.config.zones[198].sn = 'TSQ0094'
            self.config.zones[198].set_address_on_cn()

            # TODO: READ BELOW!
            # Below shows the process when we are attempting to change the serial number of a device (essentially
            # replacing) In order for the test objects to remain in a known state, we need to tell the controller what
            # values to associate with the new serial number. Thus we send the default values for the object we
            # replaced, thus passing the previous state to the new device/object.

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.master_valves[3].sn = 'TMV0008'
            self.config.master_valves[3].set_address_on_cn()
            self.config.master_valves[3].set_default_values()

            # had to assign the new master valve to the poc object
            self.config.poc[1].set_master_valve_on_cn(3)

            # Overwrite Moisture Sensor 1
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.moisture_sensors[1].sn = 'SB01250'
            self.config.moisture_sensors[1].set_default_values()

            # Need to update the zone program's moisture sensor serial to match the updated moisture sensor
            self.config.zone_programs[1].set_primary_zone_moisture_sensor_on_cn(_moisture_sensor_ad=1)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.temperature_sensors[1].sn = 'TAT0003'
            self.config.temperature_sensors[1].set_default_values()

            # Overwrite Event Switch 1
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.event_switches[1].sn = 'TPD0004'
            self.config.event_switches[1].set_default_values()

            # Need to update the poc that was using event_switches[1] with the new event switch serial number
            self.config.poc[8].set_event_switch_empty_on_cn(_event_switch_address=1)

            # Overwrite Flow Meter 1 serial number
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.flow_meters[1].sn = 'TWF0005'
            self.config.flow_meters[1].set_default_values()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        Verify that the replacement device on the controllers configuration have maintained all of there settings
        after replacing devices.
        """
        helper_methods.print_method_name()
        
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()

            for sb_address in self.config.substations.keys():
                self.config.substations[sb_address].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        1) Reboot Substation.
        2) Stop Clock.
        3) Reset date and time to match last recorded.
        """
        helper_methods.print_method_name()
        
        try:
            self.substations[1].do_reboot()
            self.substations[1].stop_clock()

            # Wait for status change
            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=20)

            # Update controller clocks to last known value
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.controllers[1],
                substations=self.config.substations,
                _date=date_mngr.controller_datetime.date_string_for_controller(),
                _time=date_mngr.controller_datetime.time_string_for_controller())
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        Verify controller and substation configuration.
        """
        helper_methods.print_method_name()
        
        try:
            for zone in self.config.zones.keys():
                self.config.zones[zone].self_test_and_update_object_attributes()
            for mv in self.config.master_valves.keys():
                self.config.master_valves[mv].self_test_and_update_object_attributes()
            for ms in self.config.moisture_sensors.keys():
                self.config.moisture_sensors[ms].self_test_and_update_object_attributes()
            for ts in self.config.temperature_sensors.keys():
                self.config.temperature_sensors[ts].self_test_and_update_object_attributes()
            for fm in self.config.flow_meters.keys():
                self.config.flow_meters[fm].self_test_and_update_object_attributes()
            for sw in self.config.event_switches.keys():
                self.config.event_switches[sw].self_test_and_update_object_attributes()

            # Verify controller configuration
            self.config.verify_full_configuration()

            # Verify substation's configurations
            for sb_address in self.config.substations.keys():
                self.config.substations[sb_address].verify_full_configuration()

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        1) Clear substation devices.
        2) Load devices back onto substation.
        3) Do a search for devices from the controller.
        
        Substation Devices Shared:
            - Valves
                + D1: "TMV0004" - Address: 8        Replaced with: "TMV0007"
                + D2: "TSE0011" - Address: 3, 4     Replaced with: "TSE0021"
            - Flow BiCoders:
                + "TWF0004" - Address: 4            Replaced with: "TWF0006"
            - Temp BiCoders:
                + "TAT0002" - Address: 2            Replaced with: "TAT0004"
            - Moisture BiCoders:
                + "SB07258" - Address: 3            Replaced with: "SB07184"
            - Switch BiCoders:
                + "TPD0003" - Address: 3            Replaced with: "TPD0005"
            - Pump BiCoders:
                + N/A
        """
        helper_methods.print_method_name()
        
        try:
            self.substations[1].clear_all_programming()

            # Increment both clocks for programming to hold
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Load devices onto Substation
            self.config.substations[1].load_dv_to_cn(dv_type=opcodes.single_valve_decoder,
                                                     list_of_decoder_serial_nums=['TMV0007'])
            self.config.substations[1].load_dv_to_cn(dv_type=opcodes.two_valve_decoder,
                                                     list_of_decoder_serial_nums=['TSE0021'])
            self.config.substations[1].load_dv_to_cn(dv_type=opcodes.moisture_sensor,
                                                     list_of_decoder_serial_nums=['SB07184'])
            self.config.substations[1].load_dv_to_cn(dv_type=opcodes.temperature_sensor,
                                                     list_of_decoder_serial_nums=['TAT0004'])
            self.config.substations[1].load_dv_to_cn(dv_type=opcodes.event_switch,
                                                     list_of_decoder_serial_nums=['TPD0005'])
            self.config.substations[1].load_dv_to_cn(dv_type=opcodes.flow_meter,
                                                     list_of_decoder_serial_nums=['TWF0006'])

            # Increment both clocks for programming to hold
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Search for devices on controller
            self.controllers[1].do_search_for_all_devices()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        1) Create/Assign new devices.
        
        Substation Devices Shared:
            - Valves
                + D1: "TMV0004" - Address: 8        Replaced with: "TMV0007"
                + D2: "TSE0011" - Address: 3, 4     Replaced with: "TSE0021"
            - Flow BiCoders:
                + "TWF0004" - Address: 4            Replaced with: "TWF0006"
            - Temp BiCoders:
                + "TAT0002" - Address: 2            Replaced with: "TAT0004"
            - Moisture BiCoders:
                + "SB07258" - Address: 3            Replaced with: "SB07184"
            - Switch BiCoders:
                + "TPD0003" - Address: 3            Replaced with: "TPD0005"
            - Pump BiCoders:
                + N/A
        """
        helper_methods.print_method_name()
        
        try:

            # Replace Master Valve (8) "TMV0004" -> "TMV0007"
            mv_to_replace = self.substations[1].valve_bicoders['TMV0004']
            # Delete from dictionary entry for previous serial number since we are replacing with a new device.
            del self.substations[1].valve_bicoders['TMV0004']

            # Add previous instance MV back to dictionary with new serial number as key
            self.substations[1].valve_bicoders['TMV0007'] = mv_to_replace
            self.substations[1].valve_bicoders['TMV0007'].sn = 'TMV0007'
            self.substations[1].valve_bicoders['TMV0007'].set_default_values()

            self.config.master_valves[8].sn = 'TMV0007'
            self.config.master_valves[8].set_address_on_cn()
            self.config.master_valves[8].set_default_values()

            self.config.poc[8].set_master_valve_on_cn(8)

            # Replace Valve (3) "TSE0011" -> "TSE0021"
            valve_1_to_replace = self.substations[1].valve_bicoders['TSE0011']
            # Delete from dictionary entry for previous serial number since we are replacing with a new device.
            del self.substations[1].valve_bicoders['TSE0011']

            # Add previous instance ZN back to dictionary with new serial number as key
            self.substations[1].valve_bicoders['TSE0021'] = valve_1_to_replace
            self.substations[1].valve_bicoders['TSE0021'].sn = 'TSE0021'
            self.substations[1].valve_bicoders['TSE0021'].set_default_values()

            self.config.zones[3].sn = 'TSE0021'
            self.config.zones[3].set_address_on_cn()
            self.config.zones[3].set_default_values()

            # Replace Valve (4) "TSE0012" -> "TSE0022"
            valve_2_to_replace = self.substations[1].valve_bicoders['TSE0012']
            # Delete from dictionary entry for previous serial number since we are replacing with a new device.
            del self.substations[1].valve_bicoders['TSE0012']

            # Add previous instance ZN back to dictionary with new serial number as key
            self.substations[1].valve_bicoders['TSE0022'] = valve_2_to_replace
            self.substations[1].valve_bicoders['TSE0022'].sn = 'TSE0022'
            self.substations[1].valve_bicoders['TSE0022'].set_default_values()

            self.config.zones[4].sn = 'TSE0022'
            self.config.zones[4].set_address_on_cn()
            self.config.zones[4].set_default_values()

            # Replace Flow BiCoder (4) "TWF0004" -> "TWF0006"
            fm_to_replace = self.substations[1].flow_bicoders['TWF0004']
            # Delete from dictionary entry for previous serial number since we are replacing with a new device.
            del self.substations[1].flow_bicoders['TWF0004']

            # Add previous instance FM back to dictionary with new serial number as key
            self.substations[1].flow_bicoders['TWF0006'] = fm_to_replace
            self.substations[1].flow_bicoders['TWF0006'].sn = 'TWF0006'
            self.substations[1].flow_bicoders['TWF0006'].set_default_values()

            self.config.flow_meters[4].sn = 'TWF0006'
            self.config.flow_meters[4].set_default_values()

            self.config.poc[8].set_flow_meter_on_cn(4)

            # Replace Temp BiCoder (2) "TAT0002" -> "TAT0004"
            ts_to_replace = self.substations[1].temp_bicoders['TAT0002']
            # Delete from dictionary entry for previous serial number since we are replacing with a new device.
            del self.substations[1].temp_bicoders['TAT0002']

            # Add previous instance TS back to dictionary with new serial number as key
            self.substations[1].temp_bicoders['TAT0004'] = ts_to_replace
            self.substations[1].temp_bicoders['TAT0004'].sn = 'TAT0004'
            self.substations[1].temp_bicoders['TAT0004'].set_default_values()

            self.config.temperature_sensors[2].sn = 'TAT0004'
            self.config.temperature_sensors[2].set_default_values()

            # Replace Moisture BiCoder (3) "SB07258" -> "SB07184"
            ms_to_replace = self.substations[1].moisture_bicoders['SB07258']
            # Delete from dictionary entry for previous serial number since we are replacing with a new device.
            del self.substations[1].moisture_bicoders['SB07258']

            # Add previous instance MS back to dictionary with new serial number as key
            self.substations[1].moisture_bicoders['SB07184'] = ms_to_replace
            self.substations[1].moisture_bicoders['SB07184'].sn = 'SB07184'
            self.substations[1].moisture_bicoders['SB07184'].set_default_values()

            self.config.moisture_sensors[3].sn = 'SB07184'
            self.config.moisture_sensors[3].set_default_values()

            # Replace Switch BiCoder (3) "TPD0003" -> "TPD0005"
            sw_to_replace = self.substations[1].switch_bicoders['TPD0003']
            # Delete from dictionary entry for previous serial number since we are replacing with a new device.
            del self.substations[1].switch_bicoders['TPD0003']

            # Add previous instance SW back to dictionary with new serial number as key
            self.substations[1].switch_bicoders['TPD0005'] = sw_to_replace
            self.substations[1].switch_bicoders['TPD0005'].sn = 'TPD0005'
            self.substations[1].switch_bicoders['TPD0005'].set_default_values()

            self.config.event_switches[3].sn = 'TPD0005'
            self.config.event_switches[3].set_default_values()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        Verify controller and substation configuration.
        """
        helper_methods.print_method_name()

        try:
            # Verify controller configuration
            self.config.verify_full_configuration()

            # Verify substation's configurations
            for sb_address in self.config.substations.keys():
                self.config.substations[sb_address].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
