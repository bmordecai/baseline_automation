import sys
from datetime import timedelta, datetime, date
import time

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes, csv
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

__author__ = 'eldin'


class UseCase9Messages(object):
    """
    Test name: Substation Messages Use Case 9 \n
        -

    Multi-Controller Setup Guide:

    Purpose:
        - The purpose of this Use Case is to verify that every message on a substation is correct when we pass in the
          commands to set and get the messages. \n

    Coverage:
        - Set up a configuration where we have a biCoder on the substation for each message we want to verify.
        - Also set up one of each device type on the 3200 Controller to verify that we can set an arbitrary 3200 message
          on a device that "belongs to" a Controller. \n
        - Do a verify configuration just to make sure all the values still stayed the same.

        Step-by-Step Summary:
        1. Initialize the controller and any substations (in this case 1) to known states.
            - Turn on echo so the commands we send are echoed. \n
            - Turn on sim mode so that we can control the time as we see fit. \n
            - Stop the clock so that it only increments when we tell it to. \n
            - Set the date and time to 1/1/2014 1:00 A.M. so we know what our date time starts at. \n
            - Turn on faux I/O so we aren't using real devices. \n
            - Clear all devices and all programming so we are in known states on the controller and substations. \n
        2. Connect the controller to it's substations. We wait for the connection to be established and then we
           increment the clock to make sure it is also displayed on the controller and substations. \n
        3. Load devices to the controller and substation and set up addresses and default values. \n
            -We load devices using the serial numbers specified in the JSON. \n
            -Do a search on the controller after both all loads are done. \n
            -Increment the clock to make sure all devices have been found and are displayed. \n
            -Share substation devices to controller, setting their addresses and default values: \n

                BiCoders:           Controller Address:         Serial Numbers:
                - Valve (ZN's)      2, 3,                       "TSE0011", "TSE0012",
                                    197, 198, 199, 200          "TSQ0081", "TSQ0082", "TSQ0083", "TSQ0084"

                - Valve (MV's)      2, 3, 4,                    "TMV0002", "TMV0003", "TMV0004",
                                    5, 6, 7                     "TMV0005", "TMV0006", "TMV0007"

                - Flow              2, 3, 4                     "TWF0002", "TWF0003", "TWF0004"
                - Switch            2, 3, 4                     "TPD0002", "TPD0003", "TPD0004"
                - Temp              2, 3, 4                     "TAT0002", "TAT0003", "TAT0004"
                - Moisture          2, 3, 4                     "SB07258", "SB08258", "SB09258"
                - Pump              N/A

        4. Verify that our objects are set up correctly, verify they match the values on the controller/substation. \n
        5. Verify all messages on a valve biCoder (zone) are correct. \n
        6. Verify all messages on a valve biCoder (master valve) are correct. \n
        7. Verify all messages on a moisture biSensor are correct. \n
        8. Verify all messages on a temperature biSensor are correct. \n
        9. Verify all messages on a switch biCoder are correct. \n
        10. Verify all messages on a flow biCoder are correct. \n
        11. Verify the entire configuration on the controller as well as the substation again. \n

    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        -Only 1 Substation is attached to the controller

    Controller Programming:
        - Zones:                7   (1-local, 6-shared)
        - Master Valves:        7   (1-local, 6-shared)
        - Event Switches:       4   (1-local, 3-shared)
        - Temp Sensors:         4   (1-local, 3-shared)
        - Moisture Sensors:     4   (1-local, 3-shared)
        - Flow Meters:          4   (1-local, 3-shared)
        - Programs:             0
        - Zone Programs:        0
        - Mainlines:            0
        - Pocs:                 0

    Substation Programming:
        - Valve BiCoders:       12 (6 for zones, 6 for master valves)
        - Moisture BiCoders:    3
        - Temperature BiCoders: 3
        - Switch BiCoders:      3
        - Flow BiCoders:        3
        - Pump BiCoders:        0
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file, substation_firmware_version,
                 number_of_substations_to_use):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str

        :param substation_firmware_version:     Expected substation firmware version.
        :type substation_firmware_version:      str

        :param number_of_substations_to_use:    Number of substations to use for the use case.
        :type number_of_substations_to_use:     int
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    sb_fw_version=substation_firmware_version,
                                    number_of_substations_to_use=number_of_substations_to_use)

        self.controllers = self.config.controllers
        self.substations = self.config.substations

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        """
        try:
            self.config.initialize_for_test()
            self.step_1()
            self.step_2()
            self.step_3()
            self.step_4()
            self.step_5()
            self.step_6()
            self.step_7()
            self.step_8()
            self.step_9()
            self.step_10()
            self.step_11()
            self.step_12()

        # Handle an exception
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        # Handle successful test
        else:
            helper_methods.print_test_passed(test_name=self.config.test_name)

        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_multiple_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Init controller
            self.controllers[1].init_cn()

            # Init each substation created/configured.
            for substation_address in self.config.substations.keys():
                self.substations[substation_address].init_sb()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substations:
        - uses the Ip address in the user config files to set the ip address
            Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                  incremented 1 minute at a time for the max minutes or until max number of minutes is met.
        - after connection is established increment clock for each controller 1 minute
            Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                  between controllers.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.connect_controller_to_substations(controller=self.controllers[1],
                                                             substations=self.substations)

            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5)

            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.

        Controller devices loaded:

            Device Type             Address         Serial Number
            -----------------------------------------------------
            Zones                   1               "TSD0001"
            -----------------------------------------------------
            Master Valves           1               "TMV0001"
            -----------------------------------------------------
            Flow Meters             1               "TWF0001"
            -----------------------------------------------------
            Moisture Sensors        1               "SB05308"
            -----------------------------------------------------
            Temperature Sensors     1               "TAT0001"
            -----------------------------------------------------
            Event Switches          1               "TSD0001"
            -----------------------------------------------------

        Substation BiCoders Shared:

            BiCoder Type            Address on 3200         Serial Number
            -------------------------------------------------------------
            Valve
            (Zones)                 2                       "TSE0011"
                                    3                       "TSE0012"
                                    197                     "TSQ0081"
                                    198                     "TSQ0082"
                                    199                     "TSQ0083"
                                    200                     "TSQ0084"

            (Master Valves)         2                       "TMV0002"
                                    3                       "TMV0003"
                                    4                       "TMV0004"
                                    5                       "TMV0005"
                                    6                       "TMV0006"
                                    7                       "TMV0007"

            -------------------------------------------------------------
            Flow                    2                       "TWF0002"
                                    3                       "TWF0003"
                                    4                       "TWF0004"
            -------------------------------------------------------------
            Moisture                2                       "SB07258"
                                    3                       "SB08258"
                                    4                       "SB09258"
            -------------------------------------------------------------
            Temperature             2                       "TAT0002"
                                    3                       "TAT0003"
                                    4                       "TAT0004"
            -------------------------------------------------------------
            Switch                  2                       "TPD0002"
                                    3                       "TPD0003"
                                    4                       "TPD0004"
            -------------------------------------------------------------

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].load_assigned_devices()
            self.substations[1].load_assigned_devices()

            self.controllers[1].do_search_for_all_devices()

            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            # 3200: Address and set default values for Devices
            helper_methods.set_all_device_default_values_on_controller(config=self.config,
                                                                       controller=self.controllers[1])

            # Substation: Set default values for Devices
            helper_methods.set_bicoder_default_values_on_substation(substations=self.substations)

            ############################################################################################################
            # Share devices from substation to controller. This makes the controller address the devices that it shares
            # with a substation, and then it sets the default values.
            # Zones
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSE0011", address=2)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSE0012", address=3)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSQ0081", address=197)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSQ0082", address=198)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSQ0083", address=199)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSQ0084", address=200)

            # Master Valves
            self.config.share_sb_device_with_cn(device_type=opcodes.master_valve, device_serial="TMV0002", address=2)
            self.config.share_sb_device_with_cn(device_type=opcodes.master_valve, device_serial="TMV0003", address=3)
            self.config.share_sb_device_with_cn(device_type=opcodes.master_valve, device_serial="TMV0004", address=4)
            self.config.share_sb_device_with_cn(device_type=opcodes.master_valve, device_serial="TMV0005", address=5)
            self.config.share_sb_device_with_cn(device_type=opcodes.master_valve, device_serial="TMV0006", address=6)
            self.config.share_sb_device_with_cn(device_type=opcodes.master_valve, device_serial="TMV0007", address=7)

            # Moisture Sensors
            self.config.share_sb_device_with_cn(device_type=opcodes.moisture_sensor, device_serial="SB07258", address=2)
            self.config.share_sb_device_with_cn(device_type=opcodes.moisture_sensor, device_serial="SB08258", address=3)
            self.config.share_sb_device_with_cn(device_type=opcodes.moisture_sensor, device_serial="SB09258", address=4)

            # Temperature Sensors
            self.config.share_sb_device_with_cn(device_type=opcodes.temperature_sensor, device_serial="TAT0002", address=2)
            self.config.share_sb_device_with_cn(device_type=opcodes.temperature_sensor, device_serial="TAT0003", address=3)
            self.config.share_sb_device_with_cn(device_type=opcodes.temperature_sensor, device_serial="TAT0004", address=4)

            # Temperature Sensors
            self.config.share_sb_device_with_cn(device_type=opcodes.event_switch, device_serial="TPD0002", address=2)
            self.config.share_sb_device_with_cn(device_type=opcodes.event_switch, device_serial="TPD0003", address=3)
            self.config.share_sb_device_with_cn(device_type=opcodes.event_switch, device_serial="TPD0004", address=4)

            # Flow Meters
            self.config.share_sb_device_with_cn(device_type=opcodes.flow_meter, device_serial="TWF0002", address=2)
            self.config.share_sb_device_with_cn(device_type=opcodes.flow_meter, device_serial="TWF0003", address=3)
            self.config.share_sb_device_with_cn(device_type=opcodes.flow_meter, device_serial="TWF0004", address=4)
            ############################################################################################################

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        Increment the clock to save settings. \n
        Verify the entire configuration. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()

            for sb_address in self.substations.keys():
                self.substations[sb_address].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_5(self):
        """
        Set and verify each zone message on the substation using different valve biCoders. \n
        Set and verify one arbitrary message on the controller on a zone that is 'owned' by the 3200. \n
            - We do this to make sure that our controller can properly have messages dealing with it's own devices.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Verify that every message can be set and that it returns us the variables we are expecting (ID, DT, TX)
            self.substations[1].valve_bicoders["TSE0011"].set_message(opcodes.checksum)
            self.substations[1].valve_bicoders["TSE0011"].verify_message(opcodes.checksum)
            self.substations[1].valve_bicoders["TSE0012"].set_message(opcodes.no_response)
            self.substations[1].valve_bicoders["TSE0012"].verify_message(opcodes.no_response)
            self.substations[1].valve_bicoders["TSQ0081"].set_message(opcodes.bad_serial)
            self.substations[1].valve_bicoders["TSQ0081"].verify_message(opcodes.bad_serial)
            self.substations[1].valve_bicoders["TSQ0082"].set_message(opcodes.low_voltage)
            self.substations[1].valve_bicoders["TSQ0082"].verify_message(opcodes.low_voltage)
            self.substations[1].valve_bicoders["TSQ0083"].set_message(opcodes.open_circuit)
            self.substations[1].valve_bicoders["TSQ0083"].verify_message(opcodes.open_circuit)
            self.substations[1].valve_bicoders["TSQ0084"].set_message('OC')
            self.substations[1].valve_bicoders["TSQ0084"].verify_message('OC')

            # Verify one arbitrary message from the 3200
            self.config.zones[1].set_message_on_cn(_status_code=opcodes.bad_serial)
            self.config.zones[1].verify_message_on_cn(_status_code=opcodes.bad_serial)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_6(self):
        """
        Set and verify each master valve message on the substation using different valve biCoders. \n
        Set and verify one arbitrary message on the controller on a master valve that is 'owned' by the 3200. \n
            - We do this to make sure that our controller can properly have messages dealing with it's own devices.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Verify that every message can be set and that it returns us the variables we are expecting (ID, DT, TX)
            self.substations[1].valve_bicoders["TMV0002"].set_message(opcodes.checksum)
            self.substations[1].valve_bicoders["TMV0002"].verify_message(opcodes.checksum)
            self.substations[1].valve_bicoders["TMV0003"].set_message(opcodes.no_response)
            self.substations[1].valve_bicoders["TMV0003"].verify_message(opcodes.no_response)
            self.substations[1].valve_bicoders["TMV0004"].set_message(opcodes.bad_serial)
            self.substations[1].valve_bicoders["TMV0004"].verify_message(opcodes.bad_serial)
            self.substations[1].valve_bicoders["TMV0005"].set_message(opcodes.low_voltage)
            self.substations[1].valve_bicoders["TMV0005"].verify_message(opcodes.low_voltage)
            self.substations[1].valve_bicoders["TMV0006"].set_message(opcodes.open_circuit)
            self.substations[1].valve_bicoders["TMV0006"].verify_message(opcodes.open_circuit)
            self.substations[1].valve_bicoders["TMV0007"].set_message('OC')
            self.substations[1].valve_bicoders["TMV0007"].verify_message('OC')

            # Verify one arbitrary message from the 3200
            self.config.master_valves[1].set_message_on_cn(_status_code=opcodes.bad_serial)
            self.config.master_valves[1].verify_message_on_cn(_status_code=opcodes.bad_serial)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_7(self):
        """
        Set and verify each moisture sensor message on the substation using different moisture biCoders. \n
        Set and verify one arbitrary message on the controller on a moisture sensor that is 'owned' by the 3200. \n
            - We do this to make sure that our controller can properly have messages dealing with it's own devices.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Verify that every message can be set and that it returns us the variables we are expecting (ID, DT, TX)
            self.substations[1].moisture_bicoders["SB07258"].set_message(opcodes.checksum)
            self.substations[1].moisture_bicoders["SB07258"].verify_message(opcodes.checksum)
            self.substations[1].moisture_bicoders["SB08258"].set_message(opcodes.no_response)
            self.substations[1].moisture_bicoders["SB08258"].verify_message(opcodes.no_response)
            self.substations[1].moisture_bicoders["SB09258"].set_message(opcodes.bad_serial)
            self.substations[1].moisture_bicoders["SB09258"].verify_message(opcodes.bad_serial)

            # Verify one arbitrary message from the 3200
            self.config.moisture_sensors[1].set_message_on_cn(_status_code=opcodes.bad_serial)
            self.config.moisture_sensors[1].verify_message_on_cn(_status_code=opcodes.bad_serial)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        Set and verify each temperature sensor message on the substation using different temperature biCoders. \n
        Set and verify one arbitrary message on the controller on a temperature sensor that is 'owned' by the 3200. \n
            - We do this to make sure that our controller can properly have messages dealing with it's own devices.
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.substations[1].temp_bicoders["TAT0002"].set_message(opcodes.checksum)
            self.substations[1].temp_bicoders["TAT0002"].verify_message(opcodes.checksum)
            self.substations[1].temp_bicoders["TAT0003"].set_message(opcodes.no_response)
            self.substations[1].temp_bicoders["TAT0003"].verify_message(opcodes.no_response)
            self.substations[1].temp_bicoders["TAT0004"].set_message(opcodes.bad_serial)
            self.substations[1].temp_bicoders["TAT0004"].verify_message(opcodes.bad_serial)

            # Verify one arbitrary message from the 3200
            self.config.temperature_sensors[1].set_message_on_cn(_status_code=opcodes.bad_serial)
            self.config.temperature_sensors[1].verify_message_on_cn(_status_code=opcodes.bad_serial)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        Set and verify each event switch message on the substation using different switch biCoders. \n
        Set and verify one arbitrary message on the controller on a event switch that is 'owned' by the 3200. \n
            - We do this to make sure that our controller can properly have messages dealing with it's own devices.
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.substations[1].switch_bicoders["TPD0002"].set_message(opcodes.checksum)
            self.substations[1].switch_bicoders["TPD0002"].verify_message(opcodes.checksum)
            self.substations[1].switch_bicoders["TPD0003"].set_message(opcodes.no_response)
            self.substations[1].switch_bicoders["TPD0003"].verify_message(opcodes.no_response)
            self.substations[1].switch_bicoders["TPD0004"].set_message(opcodes.bad_serial)
            self.substations[1].switch_bicoders["TPD0004"].verify_message(opcodes.bad_serial)

            # Verify one arbitrary message from the 3200
            self.config.event_switches[1].set_message_on_cn(_status_code=opcodes.bad_serial)
            self.config.event_switches[1].verify_message_on_cn(_status_code=opcodes.bad_serial)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_10(self):
        """
        Set and verify each flow meter message on the substation using different flow biCoders. \n
        Set and verify one arbitrary message on the controller on a flow meter that is 'owned' by the 3200. \n
            - We do this to make sure that our controller can properly have messages dealing with it's own devices.
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.substations[1].flow_bicoders["TWF0002"].set_message(opcodes.checksum)
            self.substations[1].flow_bicoders["TWF0002"].verify_message(opcodes.checksum)
            self.substations[1].flow_bicoders["TWF0003"].set_message(opcodes.no_response)
            self.substations[1].flow_bicoders["TWF0003"].verify_message(opcodes.no_response)
            self.substations[1].flow_bicoders["TWF0004"].set_message(opcodes.bad_serial)
            self.substations[1].flow_bicoders["TWF0004"].verify_message(opcodes.bad_serial)

            # Verify one arbitrary message from the 3200
            self.config.flow_meters[1].set_message_on_cn(_status_code=opcodes.bad_serial)
            self.config.flow_meters[1].verify_message_on_cn(_status_code=opcodes.bad_serial)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        Set messages for the substation. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.substations[1].set_message(opcodes.two_wire_over_current)
            self.substations[1].verify_message(opcodes.two_wire_over_current)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        Increment the clock and then verify the full configuration again. \n
        Verify the entire configuration again. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()

            for sb_address in self.config.substations.keys():
                self.config.substations[sb_address].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
