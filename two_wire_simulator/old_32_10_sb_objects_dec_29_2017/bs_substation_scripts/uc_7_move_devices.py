import sys
from datetime import timedelta, datetime, date
import time

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes, csv
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.valve_bicoder import ValveBicoder
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.flow_bicoder import FlowBicoder
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.pump_bicoder import PumpBicoder
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.switch_bicoder import SwitchBicoder
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.moisture_bicoder import MoistureBicoder
from old_32_10_sb_objects_dec_29_2017.common.objects.substation.temp_bicoder import TempBicoder

__author__ = 'bens'


class UseCase7MoveDevices(object):
    """
    Test name:
        - Substation Move Devices use case.

    Multi-Controller Setup Guide:
    
    Purpose:
        - To verify working state when moving two wire devices from 3200 to the substation and vice-versa. 
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Load devices onto the 3200.
        2. Address and set default values on 3200.
        3. Verify both controller and substation configurations.
        4. Load same devices onto the Substation & search on 3200.
        5. Verify both controller and substation configurations.
        6. Load devices back onto the 3200 and verify controller and substation configurations.
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - Only tests using 1 substation.
        
    Controller Programming:
        - Zones:                1   (1-local, 0-shared) 
        - Master Valves:        1   (1-local, 0-shared)
        - Event Switches:       1   (1-local, 0-shared)
        - Temp Sensors:         1   (1-local, 0-shared)
        - Moisture Sensors:     1   (1-local, 0-shared)
        - Flow Meters:          1   (1-local, 0-shared)
        - Programs:             0
        - Zone Programs:        0
        - Mainlines:            0
        - Pocs:                 0
        
    Substation Programming:
        - Valve BiCoders:       0
        - Moisture BiCoders:    0   
        - Temperature BiCoders: 0
        - Switch BiCoders:      0 
        - Flow BiCoders:        0
        - Pump BiCoders:        0
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file, substation_firmware_version,
                 number_of_substations_to_use):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        
        :param substation_firmware_version:     Expected substation firmware version.
        :type substation_firmware_version:      str
        
        :param number_of_substations_to_use:    Number of substations to use for the use case.
        :type number_of_substations_to_use:     int
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    sb_fw_version=substation_firmware_version,
                                    number_of_substations_to_use=number_of_substations_to_use)

        self.controllers = self.config.controllers
        self.substations = self.config.substations

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        """
        try:
            self.config.initialize_for_test()
            self.step_1()
            self.step_2()
            self.step_3()
            self.step_4()
            self.step_5()
            self.step_6()
            self.step_7()
            self.step_8()
            self.step_9()
            self.step_10()

        # Handle an exception
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        # Handle successful test
        else:
            helper_methods.print_test_passed(test_name=self.config.test_name)

        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_multiple_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Init controller
            self.controllers[1].init_cn()

            # Init each substation created/configured.
            for substation_address in self.config.substations.keys():
                self.substations[substation_address].init_sb()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substations:
        - uses the Ip address in the user config files to set the ip address
            Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                  incremented 1 minute at a time for the max minutes or until max number of minutes is met.
        - after connection is established increment clock for each controller 1 minute
            Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                  between controllers.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.connect_controller_to_substations(controller=self.controllers[1],
                                                             substations=self.substations)

            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5)

            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.

        Controller devices loaded:
                        
            Device Type             Address         Serial Number
            -----------------------------------------------------
            Zones                   1               "TSD0001"
            -----------------------------------------------------
            Flow Meters             1               "TWF0001"
            -----------------------------------------------------
            Master Valves           1               "TMV0001"
            -----------------------------------------------------
            Moisture Sensors        1               "SB05308"
            -----------------------------------------------------
            Temperature Sensors     1               "TAT0001"
            -----------------------------------------------------
            Event Switches          1               "TPD0001"
            -----------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].load_assigned_devices()
            self.substations[1].load_assigned_devices()

            self.controllers[1].do_search_for_all_devices()

            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            # 3200: Address and set default values for Devices
            helper_methods.set_all_device_default_values_on_controller(config=self.config,
                                                                       controller=self.controllers[1])

            # Substation: Set default values for Devices
            helper_methods.set_bicoder_default_values_on_substation(substations=self.substations)

            # Substation: Share devices with controller.
            # i.e., to share valve bicoders:
            # self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSE0011", address=3)
            # self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSE0012", address=4)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()

            for sb_address in self.substations.keys():
                self.substations[sb_address].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        1) Load devices onto substation.
        2) Search for devices on 3200.
        
        Substation bicoders:
        
            BiCoder Type            Address on 3200         Serial Number
            -------------------------------------------------------------
            Valve                   1 (ZN)                  "TSD0001"
                                    1 (MV)                  "TMV0001"
            -------------------------------------------------------------
            Flow                    1                       "TWF0001"
            -------------------------------------------------------------
            Moisture                1                       "SB05308"
            -------------------------------------------------------------
            Temperature             1                       "TAT0001"
            -------------------------------------------------------------
            Switch                  1                       "TPD0001"
            -------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Load devices onto Substation
            self.config.substations[1].load_dv_to_cn(dv_type=opcodes.single_valve_decoder,
                                                     list_of_decoder_serial_nums=['TMV0001'])
            self.config.substations[1].load_dv_to_cn(dv_type=opcodes.single_valve_decoder,
                                                     list_of_decoder_serial_nums=['TSD0001'])
            self.config.substations[1].load_dv_to_cn(dv_type=opcodes.moisture_sensor,
                                                     list_of_decoder_serial_nums=['SB05308'])
            self.config.substations[1].load_dv_to_cn(dv_type=opcodes.temperature_sensor,
                                                     list_of_decoder_serial_nums=['TAT0001'])
            self.config.substations[1].load_dv_to_cn(dv_type=opcodes.event_switch,
                                                     list_of_decoder_serial_nums=['TPD0001'])
            self.config.substations[1].load_dv_to_cn(dv_type=opcodes.flow_meter,
                                                     list_of_decoder_serial_nums=['TWF0001'])

            # Increment both clocks for programming to hold
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Search for devices on controller
            self.controllers[1].do_search_for_all_devices()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        1) Create Substation bicoder instances for each device moved.
        2) Update Controller devices to be shared from substation.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Create bicoder instances for each device loaded.

            # Valve BiCoder (Zone 1)
            self.substations[1].valve_bicoders["TSD0001"] = ValveBicoder(_sn="TSD0001", _ser=self.substations[1].ser)
            self.substations[1].valve_bicoders["TSD0001"].set_default_values()

            # Valve BiCoder (MV 1)
            self.substations[1].valve_bicoders["TMV0001"] = ValveBicoder(_sn="TMV0001", _ser=self.substations[1].ser)
            self.substations[1].valve_bicoders["TMV0001"].set_default_values()

            # Moisture BiCoder (MS 1)
            self.substations[1].moisture_bicoders["SB05308"] = MoistureBicoder(_sn="SB05308",
                                                                               _ser=self.substations[1].ser)
            self.substations[1].moisture_bicoders["SB05308"].set_default_values()

            # Temperature BiCoder (TS 1)
            self.substations[1].temp_bicoders["TAT0001"] = TempBicoder(_sn="TAT0001", _ser=self.substations[1].ser)
            self.substations[1].temp_bicoders["TAT0001"].set_default_values()

            # Flow BiCoder (FM 1)
            self.substations[1].flow_bicoders["TWF0001"] = FlowBicoder(_sn="TWF0001", _ser=self.substations[1].ser)
            self.substations[1].flow_bicoders["TWF0001"].set_default_values()
            self.substations[1].flow_bicoders["TWF0001"].fm_on_cn = self.config.flow_meters[1]

            # Switch BiCoder (SW 1)
            self.substations[1].switch_bicoders["TPD0001"] = SwitchBicoder(_sn="TPD0001", _ser=self.substations[1].ser)
            self.substations[1].switch_bicoders["TPD0001"].set_default_values()

            # Update Controller Devices
            self.config.zones[1].is_shared_with_substation = True
            self.config.moisture_sensors[1].is_shared_with_substation = True
            self.config.temperature_sensors[1].is_shared_with_substation = True
            self.config.flow_meters[1].is_shared_with_substation = True
            self.config.master_valves[1].is_shared_with_substation = True
            self.config.event_switches[1].is_shared_with_substation = True
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        1) Self test and update device attributes after being moved from 3200 to Substation.
        2) Verify controller and substation configurations after moving devices on the two wire.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in self.config.zones.keys():
                self.config.zones[zone].self_test_and_update_object_attributes()
            for mv in self.config.master_valves.keys():
                self.config.master_valves[mv].self_test_and_update_object_attributes()
            for ms in self.config.moisture_sensors.keys():
                self.config.moisture_sensors[ms].self_test_and_update_object_attributes()
            for ts in self.config.temperature_sensors.keys():
                self.config.temperature_sensors[ts].self_test_and_update_object_attributes()
            for fm in self.config.flow_meters.keys():
                self.config.flow_meters[fm].self_test_and_update_object_attributes()
            for sw in self.config.event_switches.keys():
                self.config.event_switches[sw].self_test_and_update_object_attributes()

            # Verify controller configuration
            self.config.verify_full_configuration()

            # Verify substation's configurations
            for sb_address in self.config.substations.keys():
                self.config.substations[sb_address].verify_full_configuration()

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        1) Load devices back onto the controller.
        2) Search for devices on the 3200.
        
        Controller devices loaded:
                        
            Device Type             Address         Serial Number
            -----------------------------------------------------
            Zones                   1               "TSD0001"
            -----------------------------------------------------
            Flow Meters             1               "TWF0001"
            -----------------------------------------------------
            Master Valves           1               "TMV0001"
            -----------------------------------------------------
            Moisture Sensors        1               "SB05308"
            -----------------------------------------------------
            Temperature Sensors     1               "TAT0001"
            -----------------------------------------------------
            Event Switches          1               "TPD0001"
            -----------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Load devices onto Substation
            self.controllers[1].load_dv_to_cn(dv_type=opcodes.single_valve_decoder,
                                              list_of_decoder_serial_nums=['TMV0001'])
            self.controllers[1].load_dv_to_cn(dv_type=opcodes.single_valve_decoder,
                                              list_of_decoder_serial_nums=['TSD0001'])
            self.controllers[1].load_dv_to_cn(dv_type=opcodes.moisture_sensor,
                                              list_of_decoder_serial_nums=['SB05308'])
            self.controllers[1].load_dv_to_cn(dv_type=opcodes.temperature_sensor,
                                              list_of_decoder_serial_nums=['TAT0001'])
            self.controllers[1].load_dv_to_cn(dv_type=opcodes.event_switch,
                                              list_of_decoder_serial_nums=['TPD0001'])
            self.controllers[1].load_dv_to_cn(dv_type=opcodes.flow_meter,
                                              list_of_decoder_serial_nums=['TWF0001'])

            # Increment both clocks for programming to hold
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Search for devices on controller
            self.controllers[1].do_search_for_all_devices()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        Move two-wire bicoders from Substation to Controller.
        
        1) Delete bicoder objects from Substation.
        2) Update controller devices to not be shared with the substation.
        
        Substation BiCoders Moved:
        
            BiCoder Type            Address on 3200         Serial Number
            -------------------------------------------------------------
            Valve                   1 (ZN)                  "TSD0001"
                                    1 (MV)                  "TMV0001"
            -------------------------------------------------------------
            Flow                    1                       "TWF0001"
            -------------------------------------------------------------
            Moisture                1                       "SB05308"
            -------------------------------------------------------------
            Temperature             1                       "TAT0001"
            -------------------------------------------------------------
            Switch                  1                       "TPD0001"
            -------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Remove devices from substation.
            self.substations[1].clear_all_programming()

            del self.substations[1].valve_bicoders['TSD0001']
            del self.substations[1].valve_bicoders['TMV0001']
            del self.substations[1].moisture_bicoders['SB05308']
            del self.substations[1].temp_bicoders['TAT0001']
            del self.substations[1].switch_bicoders['TPD0001']
            del self.substations[1].flow_bicoders['TWF0001']

            # Update Controller Devices to not be shared.
            self.config.zones[1].is_shared_with_substation = False
            self.config.moisture_sensors[1].is_shared_with_substation = False
            self.config.temperature_sensors[1].is_shared_with_substation = False
            self.config.flow_meters[1].is_shared_with_substation = False
            self.config.master_valves[1].is_shared_with_substation = False
            self.config.event_switches[1].is_shared_with_substation = False
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        1) Self test and update device attributes after being moved from Substation to 3200.
        2) Verify controller and substation configurations after moving devices on the two wire.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in self.config.zones.keys():
                self.config.zones[zone].self_test_and_update_object_attributes()
            for mv in self.config.master_valves.keys():
                self.config.master_valves[mv].self_test_and_update_object_attributes()
            for ms in self.config.moisture_sensors.keys():
                self.config.moisture_sensors[ms].self_test_and_update_object_attributes()
            for ts in self.config.temperature_sensors.keys():
                self.config.temperature_sensors[ts].self_test_and_update_object_attributes()
            for fm in self.config.flow_meters.keys():
                self.config.flow_meters[fm].self_test_and_update_object_attributes()
            for sw in self.config.event_switches.keys():
                self.config.event_switches[sw].self_test_and_update_object_attributes()

            # Verify controller configuration
            self.config.verify_full_configuration()

            # Verify substation's configurations
            for sb_address in self.config.substations.keys():
                self.config.substations[sb_address].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
