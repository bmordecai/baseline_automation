import sys

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

__author__ = 'bens'


class UseCase1TimedZones(object):
    """
    Test name:
        - CN UseCase1 Timed Zones
    purpose:
        - set up a zones on the controller and attach them to a program
            - Make one zone timed
            - Make one zone the primary zone
                - Make the other three into linked zones with different ratios
    Coverage Area: \n
        1. able to change zone modes: timed, primary, and linked \n
        2. able to set up run times and soak cycles and verify that they run properly \n
        3. verify linked zones follow the primary zone ratios \n
        4. able to disable zones \n
        
    Controller Setup:
        - Local Devices:
            + Zone 1 (TSD0001)
            + Zone 2 (TSD0002)
            + Zone 3 (TSD0003)
        - Shared Devices:
            + Zone 4 (TSD0002)
            + Zone 5 (TSD0003)
            + Zone 6 (TSD0004)
            
    Substation Setup:
        - Local Devices:
            + Valve BiCoder TSD0002
            + Valve BiCoder TSD0003
            + Valve BiCoder TSD0004
    """

    def __init__(self, controller_type, cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file, substation_firmware_version,
                 number_of_substations_to_use):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str

        :param substation_firmware_version:     Expected substation firmware version.
        :type substation_firmware_version:      str

        :param number_of_substations_to_use:    Number of substations to use for the use case.
        :type number_of_substations_to_use:     int
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    sb_fw_version=substation_firmware_version,
                                    number_of_substations_to_use=number_of_substations_to_use)

        self.controllers = self.config.controllers
        self.substations = self.config.substations

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            # Resets objects to a known state, creates serial connections, creates all objects.
            self.config.initialize_for_test()
            self.step_1()
            self.step_2()
            self.step_3()
            self.step_4()
            self.step_5()
            self.step_6()
            self.step_7()
            self.step_8()
            self.step_9()
            self.step_10()
            self.step_11()
            self.step_12()
            self.step_13()
            self.step_14()
            self.step_15()
            self.step_16()
            self.step_17()
            self.step_18()
            self.step_19()
            self.step_20()
            self.step_21()
            self.step_22()
            self.step_23()
            self.step_24()
            self.step_25()
            self.step_26()
            self.step_27()
            self.step_28()
        # Handle an exception
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        # Handle successful test
        else:
            helper_methods.print_test_passed(test_name=self.config.test_name)

        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_multiple_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Init controller
            self.controllers[1].init_cn()

            # Init each substation created/configured.
            for substation_address in self.config.substations.keys():
                self.substations[substation_address].init_sb()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substations:
        - uses the Ip address in the user config files to set the ip address
            Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                  incremented 1 minute at a time for the max minutes or until max number of minutes is met.
        - after connection is established increment clock for each controller 1 minute
            Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                  between controllers.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.connect_controller_to_substations(
                controller=self.controllers[1],
                substations=self.substations
            )

            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5)

            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].load_assigned_devices()
            self.substations[1].load_assigned_devices()

            self.controllers[1].do_search_for_all_devices()

            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            # 3200: Address and set default values for Devices
            helper_methods.set_all_device_default_values_on_controller(config=self.config,
                                                                       controller=self.controllers[1])

            # Substation: Set default values for Devices
            helper_methods.set_bicoder_default_values_on_substation(substations=self.substations)

            # Share devices from substation to controller.
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSD0004", address=4)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSD0005", address=5)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSD0006", address=6)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        1) Create mainline objects for Program.
        2) Make a very basic program that will be used to attach all the zones together.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.create_mainline_objects()

            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _st=[1200],
                                             _mc=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_5(self):
        """
        assign zones to programs \n
        set up primary linked zones \n
        Zone 1 is timed \n
        Zone 2 is a primary zone \n
            - Zone 3-5 are linked to the primary zone 2 \n
            - Zone 3 has a runtime tracking ratio of 150% \n
            - Zone 4 has a runtime tracking ratio of 50% \n
            - Zone 5 has a runtime tracking ratio of 100% \n
        Set zone 5 to have enabled state set to false. (disable it)
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # TODO The runtime of ZoneProgram 1 was actually 3600, it just takes a long time to run
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600,
                                                       _ct=120,
                                                       _so=480,
                                                       _pz=0)
            
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=720,
                                                       _ct=120,
                                                       _so=300,
                                                       _pz=2)
            
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=2)
            
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=150,
                                                       _pz=2)
            
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=50,
                                                       _pz=2)
            
            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[6],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=2)

            self.config.zones[3].set_enable_state_on_cn(_state=opcodes.false)
            self.config.zones[6].set_enable_state_on_cn(_state=opcodes.false)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_6(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()
            self.substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_7(self):
        """
        turn on sim mode in the controller \n
        stop the clock so that it can be incremented manually \n
        set the dial to off \n
        set the dial to run \n
        stop both programs \n
        advance the clock 1 second \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        advance the clock 1 second \n
        verify that the zones are a functioning correctly by getting their statuses before starting \n
        start program \n
        advance the clock 1 minute \n
        Zone Behavior:
            verify that zone 1 waits for 2 minutes at the start of the test then waters for 2 minutes and soaks for 8
            minutes then repeats \n
            verify that zone 2 starts out watering for 2 minutes then soaks for 5 minutes then repeats \n
            verify that zone 3 waits for 3 minutes at the start of the test then waters for 3 minutes and soaks for 9
            minutes then repeats \n
            verify that zone 4 waits for 9 minutes at the start of the test then waters for 1 minute and soaks for 9
            minutes then repeats \n
            verify that zone 5 is disable for the entire test \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.set_controller_substation_date_and_time(controller=self.config.controllers[1],
                                                                   substations=self.config.substations,
                                                                   _date="04/08/2014", _time="02:38:00")

            # Verify Program 1 is not watering
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            # Verify Zones 1-6 are not watering
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            # Verify initial status on Substation
            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.start_program)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        zone 2 start first because it is a primary zone and zone is a timed zone
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 waiting to water
                - Zone 2 watering
                - Zone 3 disabled
                - Zone 4 waiting to water
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 waiting to water
                - Zone 2 watering
                - Zone 3 disabled
                - Zone 4 waiting to water
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_10(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 watering
                - Zone 2 soaking
                - Zone 3 disabled
                - Zone 4 waiting to water
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 watering
                - Zone 2 soaking
                - Zone 3 disabled
                - Zone 4 waiting to water
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_12(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 soaking
                - Zone 3 disabled
                - Zone 4 watering
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_13(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 soaking
                - Zone 3 disabled 
                - Zone 4 watering
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_14(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 soaking
                - Zone 3 disabled
                - Zone 4 watering
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_15(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 watering
                - Zone 3 disabled
                - Zone 4 soaking
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_16(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 watering
                - Zone 3 disabled
                - Zone 4 soaking
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_17(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled 
                 - Zone 4 soaking
                 - Zone 5 watering
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_18(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_19(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_20(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 watering
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_21(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 watering
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_22(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 watering
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_23(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 watering
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_24(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 watering
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_25(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 watering
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_26(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 watering
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_27(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 watering
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[2].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[3].verify_status_on_cn(status=opcodes.disabled)
            self.config.zones[4].verify_status_on_cn(status=opcodes.soaking)
            self.config.zones[5].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.disabled)

            self.substations[1].valve_bicoders['TSD0004'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSD0005'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSD0006'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_28(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()
            self.substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
