import sys
from datetime import timedelta, datetime, date
import time

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes, csv
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline

__author__ = 'bens'


class UseCase8LearnFlow(object):
    """
    Test name:
        - Substation Learn Flow Use Case

    Multi-Controller Setup Guide:
    
    Purpose:
        - Demonstrate ability to learn flow across multiple mainlines at the same time.
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Initializes controller and substation.
        2. Share substation devices to controller:
        
            BiCoders:           Controller Address:       Serial Numbers:
            - Valve (ZN's)      6, 7, 8, 9, 10            "TSD0006", "TSD0007", "TSD0008", "TSD0009", "TSD0010"
            - Valve (MV's)      N/A
            - Flow              2                         "FM00002"
            - Switch            N/A
            - Temp              N/A
            - Moisture          N/A
            - Pump              N/A
            
        3. Configures POCs, Mainlines, Programs and Program Zones.
        4. Sets the flow rate of flow meters to 0.
        5. Verifies a failed learn flow.
        6. Sets the flow rate of flow meters back to a value > 0.
        7. Verifies a successful learn flow.
        8. Verifies the final configuration for the controller and substation.
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - Only tests using 1 substation.
        
    Controller Programming:
        - Zones:                10  (5-local, 5-shared) 
        - Master Valves:        2   (2-local, 0-shared)
        - Event Switches:       1   (1-local, 0-shared)
        - Temp Sensors:         1   (1-local, 0-shared)
        - Moisture Sensors:     1   (1-local, 0-shared)
        - Flow Meters:          2   (1-local, 1-shared)
        - Programs:             2
        - Zone Programs:        10
        - Mainlines:            2
        - Pocs:                 2
        
    Substation Programming:
        - Valve BiCoders:       5
        - Moisture BiCoders:    0   
        - Temperature BiCoders: 0
        - Switch BiCoders:      0 
        - Flow BiCoders:        1
        - Pump BiCoders:        0
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file, substation_firmware_version,
                 number_of_substations_to_use):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        
        :param substation_firmware_version:     Expected substation firmware version.
        :type substation_firmware_version:      str
        
        :param number_of_substations_to_use:    Number of substations to use for the use case.
        :type number_of_substations_to_use:     int
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    sb_fw_version=substation_firmware_version,
                                    number_of_substations_to_use=number_of_substations_to_use)

        self.controllers = self.config.controllers
        self.substations = self.config.substations

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        """
        try:
            self.config.initialize_for_test()
            self.step_1()
            self.step_2()
            self.step_3()
            self.step_4()
            self.step_5()
            self.step_6()
            self.step_7()
            self.step_8()
            self.step_9()
            self.step_10()
            self.step_11()
            self.step_12()
            self.step_13()
            self.step_14()
            self.step_15()

        # Handle an exception
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        # Handle successful test
        else:
            helper_methods.print_test_passed(test_name=self.config.test_name)

        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_multiple_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Init controller
            self.controllers[1].init_cn()

            # Init each substation created/configured.
            for substation_address in self.config.substations.keys():
                self.substations[substation_address].init_sb()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substations:
        - uses the Ip address in the user config files to set the ip address
            Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                  incremented 1 minute at a time for the max minutes or until max number of minutes is met.
        - after connection is established increment clock for each controller 1 minute
            Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                  between controllers.

        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.connect_controller_to_substations(controller=self.controllers[1],
                                                             substations=self.substations)

            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5)

            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.
        
        Controller devices loaded:
                        
            Device Type             Address         Serial Number
            -----------------------------------------------------
            Zones                   1               "TSD0001"
                                    2               "TSD0002"
                                    3               "TSD0003"
                                    4               "TSD0004"
                                    5               "TSD0005"
            -----------------------------------------------------
            Master Valves           1               "TMV0001"
                                    2               "TMV0002"
            -----------------------------------------------------
            Flow Meters             1               "FM00001"
            -----------------------------------------------------
            Moisture Sensors        1               "SB00001"
            -----------------------------------------------------
            Temperature Sensors     1               "TS00001"
            -----------------------------------------------------
            Event Switches          1               "SW00001"
            -----------------------------------------------------

        Substation BiCoders Shared:
        
            BiCoder Type            Address on 3200         Serial Number
            -------------------------------------------------------------
            Valve                   6                       "TSD0006"
                                    7                       "TSD0007"
                                    8                       "TSD0008"
                                    9                       "TSD0009"
                                    10                      "TSD0010"
            -------------------------------------------------------------
            Flow                    2                       "FM00002"
            -------------------------------------------------------------
            Moisture
            -------------------------------------------------------------
            Temperature
            -------------------------------------------------------------
            Switch
            -------------------------------------------------------------
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].load_assigned_devices()
            self.substations[1].load_assigned_devices(flow_bicoder_version="5.5")

            self.controllers[1].do_search_for_all_devices()

            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            # 3200: Address and set default values for Devices
            helper_methods.set_all_device_default_values_on_controller(config=self.config,
                                                                       controller=self.controllers[1])

            # Substation: Set default values for Devices
            helper_methods.set_bicoder_default_values_on_substation(substations=self.substations)

            # Substation: Share devices with controller.
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSD0006", address=6)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSD0007", address=7)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSD0008", address=8)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSD0009", address=9)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSD0010", address=10)
            self.config.share_sb_device_with_cn(device_type=opcodes.flow_meter, device_serial="FM00002", address=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()

            for sb_address in self.substations.keys():
                self.substations[sb_address].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        1) Create water sources for use case.
        2) Set up mainlines:
            Mainline 1:
                - set limit zones by flow to true
                - set the pipe fill time to 4 minutes
                - set the target flow to 500
                - set the high variance limit to 5% and enable the high variance shut down
                - set the low variance limit to 20% and enable the low variance shut down
            Mainline 8:
                - set limit zones by flow to true
                - set the pipe fill time to 1 minutes
                - set the target flow to 500
                - set the high variance limit to 5% and enable the high variance shut down
                - set the low variance limit to 20% and enable the low variance shut down
        3) Set up pocs:
            Poc 1:
                - enable
                - assign master valve TMV0001
                - assign flow meter FM00001
                - assign POC 1 a target flow of 500
                - assign POC 1 to mainline 1
                - set POC priority to 2-medium
                - set high flow limit to 550 and enable high flow shut down
                - set unscheduled flow limit to 10 and enable unscheduled flow shut down
                - set water budget to 100000 and enable the water budget shut down
                - enable water rationing
            Poc 8:
                - enable
                - assign master valve TMV0002
                - assign flow meter FM00002
                - assign POC 8 a target flow of 500
                - assign POC 8 to mainline 8
                - set POC priority to 2-medium
                - set high flow limit to 550 and enable high flow shut down
                - set unscheduled flow limit to 10 and enable unscheduled flow shut down
                - set water budget to 100000 and enable the water budget shut down
                - enable water rationing
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()

            # Configure Mainline 1
            self.config.mainlines[1] = Mainline(_ad=1,              # address number
                                                _ft=4,              # pipe file time in minutes
                                                _fl=20,             # Target flow
                                                _lc=opcodes.true,   # limit zones by flow
                                                _hv=5,              # high flow variance in percentage
                                                _hs=opcodes.true,   # high flow variance shutdown enabled
                                                _lv=20,             # low flow variance in percentage
                                                _ls=opcodes.true)   # low flow variance shutdown

            # Configure Mainline 8
            self.config.mainlines[8] = Mainline(_ad=8,              # address number
                                                _ft=1,              # pipe file time in minutes
                                                _fl=50,             # Target flow
                                                _lc=opcodes.true,   # limit zones by flow
                                                _hv=10,              # high flow variance in percentage
                                                _hs=opcodes.false,   # high flow variance shutdown enabled
                                                _lv=5,             # low flow variance in percentage
                                                _ls=opcodes.false)   # low flow variance shutdown

            # Configure Poc 1
            self.config.poc[1] = POC3200(_ad=1,                  # address number
                                         _en=opcodes.true,       # poc enabled
                                         _mv=1,                  # poc assigned to master valve 1
                                         _fm=1,                  # poc assigned to flow meter 1
                                         _fl=500,                # target flow
                                         _ml=1,                  # poc assigned to mainline 1
                                         _pr=2,                  # priority
                                         _hf=550,                # high flow
                                         _hs=opcodes.true,       # high flow shutdown enabled
                                         _uf=10.0,               # unscheduled flow
                                         _us=opcodes.true,       # unscheduled flow shutdown enabled
                                         _wb=100000,             # water budget
                                         _ws=opcodes.true,       # water budget exceeded shutdown enabled
                                         _wr=opcodes.true)       # Water Rationing Enable

            # Configure Poc 8
            self.config.poc[8] = POC3200(_ad=8,                  # address number
                                         _en=opcodes.true,       # poc enabled
                                         _mv=2,                  # poc assigned to master valve 2
                                         _fm=2,                  # poc assigned to flow meter 2
                                         _fl=500,                # target flow
                                         _ml=8,                  # poc assigned to mainline 8
                                         _pr=2,                  # priority
                                         _hf=550,                # high flow
                                         _hs=opcodes.true,       # high flow shutdown enabled
                                         _uf=10.0,               # unscheduled flow
                                         _us=opcodes.true,       # unscheduled flow shutdown enabled
                                         _wb=100000,             # water budget
                                         _ws=opcodes.true,       # water budget exceeded shutdown enabled
                                         _wr=opcodes.true)       # Water Rationing Enable
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        1) Set up programming on the controller.
        
        Program 1:
            - Start times:              600 (10am)
            - Enabled:                  True
            - Water Windows:            12am-11:59pm
            - Priority:                 1
            - Max Concurrent Zones:     1 zone
            - Seasonal Adjust:          100%
            - Calendar Interval:        Week Days
            - Day Interval:             None
            - Watering Week Days:       MO, TU, WE, TH, FR, SA, SU
            - Semi-Month Interval:      None
            - Mainline Number:          1
            - Booster Pump:             None
            
        Program 99:
            - Start times:              600 (10am)
            - Enabled:                  True
            - Water Windows:            12am-11:59pm
            - Priority:                 1
            - Max Concurrent Zones:     1 zone
            - Seasonal Adjust:          100%
            - Calendar Interval:        Week Days
            - Day Interval:             None
            - Watering Week Days:       MO, TU, WE, TH, FR, SA, SU
            - Semi-Month Interval:      None
            - Mainline Number:          8
            - Booster Pump:             None
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            program_start_times = [600]                     # 10:00am start time
            program_watering_days = [1, 1, 1, 1, 1, 1, 1]   # runs all days
            program_water_windows = ['111111111111111111111111']

            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _ww=program_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.week_days,
                                             _di=None,
                                             _wd=program_watering_days,
                                             _sm=[],
                                             _st=program_start_times,
                                             _ml=1,
                                             _bp='')

            self.config.programs[99] = PG3200(_ad=99,
                                              _en=opcodes.true,
                                              _ww=program_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.week_days,
                                              _di=None,
                                              _wd=program_watering_days,
                                              _sm=[],
                                              _st=program_start_times,
                                              _ml=8,
                                              _bp='')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        1) Set up Zone Programs.
        
        Zone Program 1:
            - Zone:                 1
            - Program:              1
            - Runtime:              960 (16 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Primary Zone)

        Zone Program 2:
            - Zone:                 2
            - Program:              1
            - Runtime:              960 (16 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Linked)

        Zone Program 3:
            - Zone:                 3
            - Program:              1
            - Runtime:              960 (16 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Linked)

        Zone Program 4:
            - Zone:                 4
            - Program:              1
            - Runtime:              960 (16 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Linked)

        Zone Program 5:
            - Zone:                 5
            - Program:              1
            - Runtime:              960 (16 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Linked)

        Zone Program 6:
            - Zone:                 6
            - Program:              99
            - Runtime:              300 (5 minutes)
            - Cycle Time:           120 (2 minutes)
            - Soak Time:            120 (2 minutes)
            - Primary Zone Number:  0 (Timed)

        Zone Program 7:
            - Zone:                 7
            - Program:              99
            - Runtime:              300 (5 minutes)
            - Cycle Time:           120 (2 minutes)
            - Soak Time:            120 (2 minutes)
            - Primary Zone Number:  0 (Timed)

        Zone Program 8:
            - Zone:                 8
            - Program:              99
            - Runtime:              300 (5 minutes)
            - Cycle Time:           120 (2 minutes)
            - Soak Time:            120 (2 minutes)
            - Primary Zone Number:  0 (Timed)

        Zone Program 9:
            - Zone:                 9
            - Program:              99
            - Runtime:              300 (5 minutes)
            - Cycle Time:           120 (2 minutes)
            - Soak Time:            120 (2 minutes)
            - Primary Zone Number:  0 (Timed)
            
        Zone Program 10:
            - Zone:                 10
            - Program:              99
            - Runtime:              300 (5 minutes)
            - Cycle Time:           120 (2 minutes)
            - Soak Time:            120 (2 minutes)
            - Primary Zone Number:  0 (Timed)
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=960,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)

            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _pz=1)

            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _pz=1)

            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _pz=1)

            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _pz=1)

            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[6],
                                                       prog_obj=self.config.programs[99],
                                                       _rt=300,
                                                       _ct=120,
                                                       _so=120,
                                                       _pz=0)

            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[7],
                                                       prog_obj=self.config.programs[99],
                                                       _rt=300,
                                                       _ct=120,
                                                       _so=120,
                                                       _pz=0)

            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[8],
                                                       prog_obj=self.config.programs[99],
                                                       _rt=300,
                                                       _ct=120,
                                                       _so=120,
                                                       _pz=0)

            self.config.zone_programs[9] = ZoneProgram(zone_obj=self.config.zones[9],
                                                       prog_obj=self.config.programs[99],
                                                       _rt=300,
                                                       _ct=120,
                                                       _so=120,
                                                       _pz=0)

            self.config.zone_programs[10] = ZoneProgram(zone_obj=self.config.zones[10],
                                                        prog_obj=self.config.programs[99],
                                                        _rt=300,
                                                        _ct=120,
                                                        _so=120,
                                                        _pz=0)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        1) Give each zone a design flow.
        
        Zones 1-5: 20 GPM
        Zones 6-10: 50 GPM
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.zones[1].set_design_flow_on_cn(_df=2)
            self.config.zones[2].set_design_flow_on_cn(_df=2)
            self.config.zones[3].set_design_flow_on_cn(_df=2)
            self.config.zones[4].set_design_flow_on_cn(_df=2)
            self.config.zones[5].set_design_flow_on_cn(_df=2)
            self.config.zones[6].set_design_flow_on_cn(_df=5)
            self.config.zones[7].set_design_flow_on_cn(_df=5)
            self.config.zones[8].set_design_flow_on_cn(_df=5)
            self.config.zones[9].set_design_flow_on_cn(_df=5)
            self.config.zones[10].set_design_flow_on_cn(_df=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        1) Verify both controller and substation's configuraitons after all programming is complete.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)
            self.config.verify_full_configuration()
            self.substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        1) Configure each flow meter to have a flow rate of: 0
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=0)
            self.substations[1].flow_bicoders["FM00002"].set_flow_rate(_flow_rate=0)

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Update shared flow meter's values.
            self.config.flow_meters[2].self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        1) Learn Flow Failure.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            date_mngr.set_current_date_to_match_computer()

            helper_methods.set_controller_substation_date_and_time(controller=self.config.controllers[1],
                                                                   substations=self.config.substations,
                                                                   _date=date_mngr.curr_day.date_string_for_controller(),
                                                                   _time="07:59:00")

            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=1)
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=99)

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=2)

            # this is looking at zone concurrency because the controller can only run one zone at a time
            # only 1 zone should be learning flow
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(opcodes.learn_flow_active)

            self.config.programs[99].get_data()
            self.config.programs[99].verify_status_on_cn(opcodes.learn_flow_active)

            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            # -------------------
            # VERIFY ZONE STATUS'
            # -------------------
            self.config.zones[1].verify_status_on_cn(opcodes.learn_flow_active)

            for zone in range(2, 11):
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)

            self.substations[1].valve_bicoders["TSD0006"].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders["TSD0007"].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders["TSD0008"].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders["TSD0009"].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders["TSD0010"].verify_status(_status=opcodes.done_watering)

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=2)

            # stop both programs and reset zone concurrency to 2 on the controller
            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.stop)
            self.config.controllers[1].set_program_start_stop(_pg_ad=99, _function=opcodes.stop)

            # these message get generated because we stop the program therefore the message is zones didnt learn flow
            # we need to clear theme to continue with the test
            for zone in range(2, 11):
                self.config.zone_programs[zone].verify_message_on_cn(opcodes.learn_flow_fail_flow_biCoders_error)
                self.config.zone_programs[zone].clear_message_on_cn(opcodes.learn_flow_fail_flow_biCoders_error)

            # change controller to have two have a total of two concurrent zones
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=2)

            # increment clock to clear statuses
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=2)

            date_mngr.set_current_date_to_match_computer()
            helper_methods.set_controller_substation_date_and_time(controller=self.config.controllers[1],
                                                                   substations=self.config.substations,
                                                                   _date=date_mngr.curr_day.date_string_for_controller(),
                                                                   _time="07:59:00")

            # restart learn flow
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=1)
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=99)

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            self.config.programs[1].get_data()
            self.config.programs[99].get_data()

            self.config.programs[1].verify_status_on_cn(opcodes.learn_flow_active)
            self.config.programs[99].verify_status_on_cn(opcodes.learn_flow_active)

            # -------------------
            # VERIFY ZONE STATUS'
            # -------------------
            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            self.config.zones[1].verify_status_on_cn(opcodes.learn_flow_active)
            self.config.zones[6].verify_status_on_cn(opcodes.learn_flow_active)
            for zone in range(2, 6):
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)
            for zone in range(7, 11):
                self.config.zones[zone].verify_status_on_cn(opcodes.waiting_to_water)

            self.substations[1].valve_bicoders["TSD0006"].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders["TSD0007"].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders["TSD0008"].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders["TSD0009"].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders["TSD0010"].verify_status(_status=opcodes.done_watering)

            not_done = True
            while not_done:

                zones_still_running = False

                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone in self.config.zones.keys():
                    self.config.zones[zone].get_data()
                    _zone_status = self.config.zones[zone].data.get_value_string_by_key(opcodes.status_code)

                    # for each zone get the ss per zone
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    if _zone_status == opcodes.watering or _zone_status == opcodes.learn_flow_active:
                        self.config.zones[zone].seconds_zone_ran += 60  # using 60 so that we are in seconds

                    # set flag not all zones are done
                    if _zone_status != opcodes.done_watering and _zone_status != opcodes.error:
                        zones_still_running = True

                        # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------

                # this could say if zones_still_running:
                if zones_still_running:
                    not_done = True
                    helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                          substations=self.substations,
                                                                          minutes=1)
                    self.config.controllers[1].verify_date_and_time()
                    self.substations[1].verify_date_and_time()
                else:
                    not_done = False

            # self.config.controllers[1].do_increment_clock(minutes=40)
            # TODO have zones run until all are set to done than read messages
            for zone in self.config.zones.keys():
                self.config.zone_programs[zone].verify_message_on_cn(opcodes.learn_flow_fail_flow_biCoders_error)
                self.config.zone_programs[zone].clear_message_on_cn(opcodes.learn_flow_fail_flow_biCoders_error)

            for program in self.config.programs.keys():
                self.config.programs[program].verify_message_on_cn(opcodes.learn_flow_with_errors)
                self.config.programs[program].clear_message_on_cn(opcodes.learn_flow_with_errors)

            # check to see if the time each zone took to learn flow is correct
            seconds_learning_flow = (self.config.mainlines[1].ft * 60) + 60
            for zone in range(1, 6):
                if seconds_learning_flow != self.config.zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow")

            seconds_learning_flow = (self.config.mainlines[8].ft * 60) + 60
            for zone in range(6, 11):
                if seconds_learning_flow != self.config.zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow))
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow")

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        1) Update flow meter flow rates.
        
        FM 1 (FM00001) - 50 GPM
        FM 2 (FM00002) - 20 GPM
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=50.0)
            self.substations[1].flow_bicoders["FM00002"].set_flow_rate(_flow_rate=20.0)

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Update shared flow meter's values.
            self.config.flow_meters[2].self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        1) Learn Flow Success.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            date_mngr.set_current_date_to_match_computer()
            helper_methods.set_controller_substation_date_and_time(controller=self.config.controllers[1],
                                                                   substations=self.config.substations,
                                                                   _date=date_mngr.curr_day.date_string_for_controller(),
                                                                   _time="07:59:00")

            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=1)
            self.config.controllers[1].set_learn_flow_enabled(_pg_ad=99)

            # TODO read how long each zone should run and than calculate against runtime plus pipe file time
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            not_done = True
            while not_done:

                zones_still_running = False
                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone in self.config.zones.keys():
                    self.config.zones[zone].get_data()
                    _zone_status = self.config.zones[zone].data.get_value_string_by_key(opcodes.status_code)

                    # for each zone get the ss per zone
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    if _zone_status == opcodes.watering or _zone_status == opcodes.learn_flow_active:
                        self.config.zones[zone].seconds_zone_ran += 60  # using 60 so that we are in seconds

                    # set flag not all zones are done
                    if _zone_status != opcodes.done_watering and _zone_status != opcodes.error:
                        zones_still_running = True

                        # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------

                # this could say if zones_still_running:
                if zones_still_running:
                    not_done = True
                    helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                          substations=self.substations,
                                                                          minutes=1)
                    self.config.controllers[1].verify_date_and_time()
                    self.substations[1].verify_date_and_time()
                else:
                    not_done = False

            for zone in self.config.zone_programs.keys():
                self.config.zone_programs[zone].verify_message_on_cn(opcodes.flow_learn_ok)
                self.config.zone_programs[zone].clear_message_on_cn(opcodes.flow_learn_ok)

            for program in self.config.programs.keys():
                self.config.programs[program].verify_message_on_cn(opcodes.learn_flow_success)
                self.config.programs[program].clear_message_on_cn(opcodes.learn_flow_success)

            # check to see if the time each zone took to learn flow is correct
            seconds_learning_flow = (self.config.mainlines[1].ft * 60) + 60
            for zone in range(1, 6):
                if seconds_learning_flow != self.config.zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow")

            seconds_learning_flow = (self.config.mainlines[8].ft * 60) + 60
            for zone in range(6, 11):
                if seconds_learning_flow != self.config.zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.zones[zone].seconds_zone_ran) +
                           " seconds to learn flow")

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        1) Verify POC 8 received an unscheduled flow shutdown message. This is because the first set of zones got done
           before the second set and there is still water flowing to POC 8. 
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            self.config.poc[8].verify_message_on_cn(opcodes.unscheduled_flow_shutdown)
            self.config.poc[8].clear_message_on_cn(opcodes.unscheduled_flow_shutdown)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        1) Update design flows for zones to match expected values:
        
            Zones 1-5: 50.0 GPM
            Zones 6-10: 20.0 GPM
            
        2) Verify controller and substation configurations after learn flow success/failure.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            for zone in range(1, 6):
                self.config.zones[zone].df = 50.0
            for zone in range(6, 11):
                self.config.zones[zone].df = 20.0

            # Verify Controller configuration
            self.config.verify_full_configuration()

            # Verify substation configuration.
            self.substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
