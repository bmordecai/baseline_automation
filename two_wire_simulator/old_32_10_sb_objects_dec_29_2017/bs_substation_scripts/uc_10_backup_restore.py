import sys
from datetime import timedelta, datetime, date
import time

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes, csv
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline


__author__ = 'bens'


class UseCase10BackupRestore(object):
    """
    Test name:
        - Substation Use Case: Backup / Restore 

    Multi-Controller Setup Guide:
    
    Purpose:
        - Verify a 3200 will retain devices shared from a substation after backing up the 3200 and restoring. 
          Demonstrates and verifies this functionality using both BaseManager and USB for backup sources.
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Initializes controller and substation.
        2. Share substation devices to controller:
        
            BiCoders:           Controller Address:       Serial Numbers:
            - Valve (ZN's)      2, 49, 50                 "TSD0002", "TSE0011", "TSE0012"
            - Valve (MV's)      N/A
            - Flow              2                         "FM00002"
            - Switch            N/A
            - Temp              N/A
            - Moisture          N/A
            - Pump              N/A
            
        3. Configures POCs, Mainlines, Programs and Program Zones.
        4. Disable the following devices on the 3200:
        
            - Zones:         2, 198
            - Flow Meters:   1, 2
        
        5. Verify 3200 and substation configuration before the backup.
        6. Backup 3200 programming to BaseManager.
        7. Clear all devices and programming on 3200.
        8. Restore 3200 programming from BaseManager.
        9. Verify 3200 and substation configuration after the restore from BaseManager.
        10. Backup 3200 programming to USB.
        11. Clear all devices and programming on 3200.
        12. Restore 3200 programming from USB.
        13. Verify 3200 and substation configuration after the restore from USB.
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - This test only uses 1 substation.
        
    Controller Programming:
        - Zones:                8   (5-local, 3-shared) 
        - Master Valves:        1   (1-local, 0-shared)
        - Event Switches:       1   (1-local, 0-shared)
        - Temp Sensors:         1   (1-local, 0-shared)
        - Moisture Sensors:     1   (1-local, 0-shared)
        - Flow Meters:          2   (1-local, 1-shared)
        - Programs:             4
        - Zone Programs:        9
        - Mainlines:            2
        - Pocs:                 2
        
    Substation Programming:
        - Valve BiCoders:       3
        - Moisture BiCoders:    0   
        - Temperature BiCoders: 0
        - Switch BiCoders:      0 
        - Flow BiCoders:        1
        - Pump BiCoders:        0
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file, substation_firmware_version,
                 number_of_substations_to_use):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        
        :param substation_firmware_version:     Expected substation firmware version.
        :type substation_firmware_version:      str
        
        :param number_of_substations_to_use:    Number of substations to use for the use case.
        :type number_of_substations_to_use:     int
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    sb_fw_version=substation_firmware_version,
                                    number_of_substations_to_use=number_of_substations_to_use)

        self.controllers = self.config.controllers
        self.substations = self.config.substations

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        """
        try:
            self.config.initialize_for_test()
            self.step_1()
            self.step_2()
            self.step_3()
            self.step_4()
            self.step_5()
            self.step_6()
            self.step_7()
            self.step_8()
            self.step_9()

            # Comment out steps 10-12 to skip Backup/Restore from BaseManager.
            self.step_10()
            self.step_11()
            self.step_12()

            # Comment out steps 13-15 to skip Backup/Restore from USB device.
            self.step_13()
            self.step_14()
            self.step_15()

        # Handle an exception
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        # Handle successful test
        else:
            helper_methods.print_test_passed(test_name=self.config.test_name)

        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_multiple_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Init controller
            self.controllers[1].init_cn()

            # only need this for BaseManager
            self.config.basemanager_connection[1].verify_ip_address_state()

            # Init each substation created/configured.
            for substation_address in self.config.substations.keys():
                self.substations[substation_address].init_sb()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substations:
        - uses the Ip address in the user config files to set the ip address
            Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                  incremented 1 minute at a time for the max minutes or until max number of minutes is met.
        - after connection is established increment clock for each controller 1 minute
            Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                  between controllers.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.connect_controller_to_substations(controller=self.controllers[1],
                                                             substations=self.substations)

            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5)

            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.
        
        Controller devices loaded:
                        
            Device Type             Address         Serial Number
            -----------------------------------------------------
            Zones                   1               "TSD0001"
                                    197             "TSQ0071"
                                    198             "TSQ0072"
                                    199             "TSQ0073"
                                    200             "TSQ0074"
            -----------------------------------------------------
            Master Valves           1               "TMV0001"
            -----------------------------------------------------
            Flow Meters             1               "FM00001"
            -----------------------------------------------------
            Moisture Sensors        1               "SB00001"
            -----------------------------------------------------
            Temperature Sensors     1               "TS00001"
            -----------------------------------------------------
            Event Switches          1               "SW00001"
            -----------------------------------------------------

        Substation BiCoders Shared:
        
            BiCoder Type            Address on 3200         Serial Number
            -------------------------------------------------------------
            Valve                   2                       "TSD0002"
                                    49                      "TSE0011"
                                    50                      "TSE0012"
            -------------------------------------------------------------
            Flow                    2                       "FM00002"
            -------------------------------------------------------------
            Moisture
            -------------------------------------------------------------
            Temperature
            -------------------------------------------------------------
            Switch
            -------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].load_assigned_devices()
            self.substations[1].load_assigned_devices()

            self.controllers[1].do_search_for_all_devices()

            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            # 3200: Address and set default values for Devices
            helper_methods.set_all_device_default_values_on_controller(config=self.config,
                                                                       controller=self.controllers[1])

            # Substation: Set default values for Devices
            helper_methods.set_bicoder_default_values_on_substation(substations=self.substations)

            # Substation: Share devices with controller.
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSD0002", address=2)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSE0011", address=49)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSE0012", address=50)
            self.config.share_sb_device_with_cn(device_type=opcodes.flow_meter, device_serial="FM00002", address=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()

            for sb_address in self.substations.keys():
                self.substations[sb_address].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        Configure Water Sources:
        
        Mainline 1:
            - Pipe Fill Time:               4 minutes
            - Target Flow:                  500 GPM
            - Limit Zones by Flow:          Enabled
            - High Variance Limit:          5%
            - High Variance Shutdown:       Enabled
            - Low Variance limit:           20%
            - Low Variance Shutdown:        Enabled
        
        Mainline 8:
            - Pipe Fill Time:               1 minute
            - Target Flow:                  50 GPM
            - Limit Zones by Flow:          Enabled
            - High Variance Limit:          20%
            - High Variance Shutdown:       Disabled
            - Low Variance limit:           5%
            - Low Variance Shutdown:        Disabled
        
        POC 1:
            - Enable:                       Enabled
            - Master Valve:                 1 (TMV0001)
            - Flow Meter:                   1 (FM00001)
            - Target Flow:                  500 
            - Mainline:                     1
            - Priority:                     2 (medium)
            - High Flow Limit:              550
            - High Flow Shutdown:           Enabled
            - Unschedule Flow Limit:        10
            - Unscheduled Flow Shutdown:    Enabled
            - Water Budget:                 100000
            - Shutdown on over Budget:      Enabled
            - Water Rationing:              Enabled
            
        POC 8:
            - Enable:                       Enabled
            - Master Valve:                 1 (TMV0001)
            - Flow Meter:                   1 (FM00001)
            - Target Flow:                  50
            - Mainline:                     8
            - Priority:                     3 (low)
            - High Flow Limit:              75
            - High Flow Shutdown:           Disabled 
            - Unschedule Flow Limit:        5
            - Unscheduled Flow Shutdown:    Disabled
            - Water Budget:                 1000
            - Shutdown on over Budget:      Disabled 
            - Water Rationing:              Disabled
            - Event Switch:                 1 (SW00001)
            - Switch Empty Condition:       Closed
            - Empty Wait Time:              540
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()

            self.config.mainlines[1] = Mainline(_ad=1,
                                                _ft=4,
                                                _fl=500,
                                                _lc=opcodes.true,
                                                _hv=5,
                                                _hs=opcodes.true,
                                                _lv=20,
                                                _ls=opcodes.true)
            self.config.mainlines[8] = Mainline(_ad=8,
                                                _ft=1,
                                                _fl=50,
                                                _lc=opcodes.true,
                                                _hv=20,
                                                _hs=opcodes.false,
                                                _lv=5,
                                                _ls=opcodes.false)

            self.config.poc[1] = POC3200(
                _ad=1,
                _en=opcodes.true,
                _mv=1,
                _fm=1,
                _fl=500,
                _ml=1,
                _pr=2,
                _hf=550,
                _hs=opcodes.true,
                _uf=10,
                _us=opcodes.true,
                _wb=100000,
                _ws=opcodes.true,
                _wr=opcodes.true
            )
            self.config.poc[8] = POC3200(
                _ad=8,
                _en=opcodes.true,
                _mv=1,
                _fm=1,
                _fl=50,
                _ml=8,
                _pr=3,
                _hf=75,
                _hs=opcodes.false,
                _uf=5,
                _us=opcodes.false,
                _wb=1000,
                _ws=opcodes.false,
                _wr=opcodes.false,
                _sw=1,
                _se=opcodes.closed,
                _ew=540
            )

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        Configure Programming.
        
        Program 1:
            - Start times:              480 (8am), 540 (9am), 600 (10am), 660 (11am)
            - Enabled:                  True
            - Water Windows:            1am-6am, 11am-10pm
            - Priority:                 1
            - Max Concurrent Zones:     1 zone
            - Seasonal Adjust:          100%
            - Calendar Interval:        Week Days
            - Day Interval:             None
            - Watering Week Days:       Monday, Wednesday, Friday
            - Semi-Month Interval:      None
            - Mainline Number:          1
            - Booster Pump:             None

        Program 3:
            - Start times:              480 (8am), 540 (9am), 600 (10am), 660 (11am)
            - Enabled:                  True
            - Water Windows:            1am-6am, 11am-10pm
            - Priority:                 1
            - Max Concurrent Zones:     1 zone
            - Seasonal Adjust:          100%
            - Calendar Interval:        Odd Days
            - Day Interval:             None
            - Watering Week Days:       None
            - Semi-Month Interval:      None
            - Mainline Number:          2
            - Booster Pump:             None

        Program 4: 
            - Start times:              480 (8am), 540 (9am), 600 (10am), 660 (11am)
            - Enabled:                  True
            - Water Windows:            1am-6am, 11am-10pm
            - Priority:                 3
            - Max Concurrent Zones:     4 zones
            - Seasonal Adjust:          100%
            - Calendar Interval:        Week Days
            - Day Interval:             None
            - Watering Week Days:       Monday, Wednesday, Friday
            - Semi-Month Interval:      None
            - Mainline Number:          3
            - Booster Pump:             None

        Program 99:
            - Start times:              480 (8am), 540 (9am), 600 (10am), 660 (11am)
            - Enabled:                  True
            - Water Windows:            Sunday: 1am-6am, 12pm-10pm
                                        Monday: 1am-6am, 11am-11pm
                                        Tuesday: 1am-6am, 11am-10pm
                                        Wednesday: 1am-6am, 11am-10pm
                                        Thursday: 1am-6am, 11am-10pm
                                        Friday: 1am-6am, 11am-10pm
                                        Saturday: 1am-6am, 11am-11pm
            - Priority:                 1
            - Max Concurrent Zones:     1 zone
            - Seasonal Adjust:          100%
            - Calendar Interval:        Historical Calendar
            - Day Interval:             None
            - Watering Week Days:       None
            - Semi-Month Interval:      TODO
            - Mainline Number:          8
            - Booster Pump:             None
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            program_number_1_start_times = [480, 540, 600, 660]
            program_number_3_start_times = [480, 540, 600, 660]
            program_number_4_start_times = [480, 540, 600, 660]
            program_number_99_start_times = [480, 540, 600, 660]
            program_number_1_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
            program_number_3_watering_days = []
            program_number_4_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
            program_number_99_watering_days = [0, 0, 0, 0, 0, 0, 8, 8, 6, 5, 5, 4, 3, 3, 3, 3, 4, 5, 6, 7, 0, 0, 0, 0]
            program_number_1_water_windows = ['011111100001111111111110']
            program_number_3_water_windows = ['011111100001111111111110']
            program_number_4_water_windows = ['011111100001111111111110']
            program_number_99_water_windows = ['011111100000111111111110',
                                               '011111100001111111111111',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111111']

            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _ww=program_number_1_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.week_days,
                                             _di=None,
                                             _wd=program_number_1_watering_days,
                                             _sm=[],
                                             _st=program_number_1_start_times,
                                             _ml=1,
                                             _bp='')
            self.config.programs[3] = PG3200(_ad=3,
                                             _en=opcodes.true,
                                             _ww=program_number_3_water_windows,
                                             _pr=1,
                                             _mc=1,
                                             _sa=100,
                                             _ci=opcodes.odd_day,
                                             _di=None,
                                             _wd=program_number_3_watering_days,
                                             _sm=[],
                                             _st=program_number_3_start_times,
                                             _ml=2,
                                             _bp='')
            self.config.programs[4] = PG3200(_ad=4,
                                             _en=opcodes.true,
                                             _ww=program_number_4_water_windows,
                                             _pr=3,
                                             _mc=4,
                                             _sa=100,
                                             _ci=opcodes.week_days,
                                             _di=None,
                                             _wd=program_number_4_watering_days,
                                             _sm=[],
                                             _st=program_number_4_start_times,
                                             _ml=3,
                                             _bp='')
            self.config.programs[99] = PG3200(_ad=99,
                                              _en=opcodes.true,
                                              _ww=program_number_99_water_windows,
                                              _pr=1,
                                              _mc=1,
                                              _sa=100,
                                              _ci=opcodes.historical_calendar,
                                              _di=None,
                                              _wd=[],
                                              _sm=program_number_99_watering_days,
                                              _st=program_number_99_start_times,
                                              _ml=8,
                                              _bp='')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        Configure Zone Programs:
        
        Zone Program 1:
            - Zone:                 1
            - Program:              1
            - Runtime:              900 (15 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Primary Zone)

        Zone Program 2:
            - Zone:                 2
            - Program:              1
            - Runtime:              900 (15 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Runtime Ratio:        100%
            - Primary Zone Number:  1 (Linked)

        Zone Program 49:
            - Zone:                 49
            - Program:              3
            - Runtime:              1200 (20 minutes)
            - Cycle Time:           600 (10 minutes)
            - Soak Time:            3600 (60 minutes)
            - Runtime Ratio:        100%
            - Water Strategy:       Timed

        Zone Program 50:
            - Zone:                 50
            - Program:              3
            - Runtime:              1200 (20 minutes)
            - Cycle Time:           600 (10 minutes)
            - Soak Time:            3600 (60 minutes)
            - Water Strategy:       Timed

        Zone Program 50:
            - Zone:                 50
            - Program:              4
            - Runtime:              1200 (20 minutes)
            - Cycle Time:           600 (10 minutes)
            - Soak Time:            3600 (60 minutes)
            - Water Strategy:       Timed

        Zone Program 197:
            - Zone:                 197
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Runtime Ratio:        50%
            - Primary Zone Number:  200 (Linked)

        Zone Program 198:
            - Zone:                 198
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Runtime Ratio:        100%
            - Primary Zone Number:  200 (Linked)

        Zone Program 199:
            - Zone:                 199
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Runtime Ratio:        150%
            - Primary Zone Number:  200 (Primary Zone)

        Zone Program 200:
            - Zone:                 200
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Primary Zone Number:  200 (Primary Zone)
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=self.config.zone_programs[1].rt,
                                                       _ct=self.config.zone_programs[1].ct,
                                                       _so=self.config.zone_programs[1].so,
                                                       _ra=100,
                                                       _pz=1)
            self.config.zone_programs[49] = ZoneProgram(zone_obj=self.config.zones[49],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=1200,
                                                        _ct=600,
                                                        _so=3600,
                                                        _ws=opcodes.timed)
            self.config.zone_programs[50] = ZoneProgram(zone_obj=self.config.zones[50],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=1200,
                                                        _ct=600,
                                                        _so=3600,
                                                        _ws=opcodes.timed)
            self.config.zone_programs[50] = ZoneProgram(zone_obj=self.config.zones[50],
                                                        prog_obj=self.config.programs[4],
                                                        _rt=1200,
                                                        _ct=600,
                                                        _so=3600,
                                                        _ws=opcodes.timed)

            self.config.zone_programs[200] = ZoneProgram(zone_obj=self.config.zones[200],
                                                         prog_obj=self.config.programs[99],
                                                         _rt=1980,
                                                         _pz=200,
                                                         _ct=180,
                                                         _so=780)

            # Zone programs linked to Zone 200
            self.config.zone_programs[197] = ZoneProgram(zone_obj=self.config.zones[197],
                                                         prog_obj=self.config.programs[99],
                                                         _rt=self.config.zone_programs[200].rt,
                                                         _ct=self.config.zone_programs[200].ct,
                                                         _so=self.config.zone_programs[200].so,
                                                         _pz=200,
                                                         _ra=50)
            self.config.zone_programs[198] = ZoneProgram(zone_obj=self.config.zones[198],
                                                         prog_obj=self.config.programs[99],
                                                         _rt=self.config.zone_programs[200].rt,
                                                         _ct=self.config.zone_programs[200].ct,
                                                         _so=self.config.zone_programs[200].so,
                                                         _pz=200,
                                                         _ra=100)
            self.config.zone_programs[199] = ZoneProgram(zone_obj=self.config.zones[199],
                                                         prog_obj=self.config.programs[99],
                                                         _rt=self.config.zone_programs[200].rt,
                                                         _ct=self.config.zone_programs[200].ct,
                                                         _so=self.config.zone_programs[200].so,
                                                         _pz=200,
                                                         _ra=150)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        Disable Devices.
        
        Coverage:
        This area covers not losing programming when a device is disabled during a backup/restore.
        
        Disable the following devices on the 3200:
            - Zones:         2, 198
            - Flow Meters:   1, 2
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.zones[2].set_enable_state_on_cn(_state=opcodes.false)
            self.config.zones[198].set_enable_state_on_cn(_state=opcodes.false)
            self.config.flow_meters[1].set_enable_state_on_cn(_state=opcodes.false)
            self.config.flow_meters[2].set_enable_state_on_cn(_state=opcodes.false)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        Verify 3200 and substation configuration before backup/restore process to ensure our objects are in correct
        state.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.verify_full_configuration()
            self.substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        1) Start 3200 clock for configuration to save.
        2) Sleep 5 seconds to give controller some time.
        3) Back up 3200 programming to BaseManager.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].start_clock()
            time.sleep(5)
            self.controllers[1].do_backup_programming(were_to=opcodes.basemanager)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Clear all programming from the 3200 so we can restore from a clean state.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].init_cn()

            # This is needed to reset the substation clock to match the controller
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.controllers[1],
                substations=self.config.substations,
                _date=date_mngr.controller_datetime.date_string_for_controller(),
                _time=date_mngr.controller_datetime.time_string_for_controller())

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        1) Restore 3200 programming from BaseManager.
        2) Run self tests on all devices to update two-wire values for verification.
        3) Verify both 3200 and substation configurations to validate successful backup/restore from BaseManager.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.basemanager_connection[1].verify_ip_address_state()
            self.controllers[1].do_restore_programming(opcodes.basemanager)

            self.controllers[1].do_search_for_all_devices()

            for zone in self.config.zones.keys():
                self.config.zones[zone].self_test_and_update_object_attributes()
            for mv in self.config.master_valves.keys():
                self.config.master_valves[mv].self_test_and_update_object_attributes()
            for ms in self.config.moisture_sensors.keys():
                self.config.moisture_sensors[ms].self_test_and_update_object_attributes()
            for ts in self.config.temperature_sensors.keys():
                self.config.temperature_sensors[ts].self_test_and_update_object_attributes()
            for fm in self.config.flow_meters.keys():
                self.config.flow_meters[fm].self_test_and_update_object_attributes()
            for sw in self.config.event_switches.keys():
                self.config.event_switches[sw].self_test_and_update_object_attributes()

            self.config.verify_full_configuration()
            self.substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        1) Start 3200 clock for configuration to save.
        2) Sleep 5 seconds to give controller some time.
        3) Back up 3200 programming to USB Device.
        
        ** NOTE: Have USB inserted into the 3200!!
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].start_clock()
            time.sleep(5)
            self.controllers[1].do_backup_programming(were_to=opcodes.usb_flash_storage)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        Clear all programming from the 3200 so we can restore from a clean state.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].init_cn()

            # This is needed to reset the substation clock to match the controller
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.controllers[1],
                substations=self.config.substations,
                _date=date_mngr.controller_datetime.date_string_for_controller(),
                _time=date_mngr.controller_datetime.time_string_for_controller())
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        1) Restore 3200 programming from USB.
        2) Run self tests on all devices to update two-wire values for verification.
        3) Verify both 3200 and substation configurations to validate successful backup/restore from USB.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].do_restore_programming(opcodes.usb_flash_storage, file_number=1)

            self.controllers[1].do_search_for_all_devices()

            for zone in self.config.zones.keys():
                self.config.zones[zone].self_test_and_update_object_attributes()
            for mv in self.config.master_valves.keys():
                self.config.master_valves[mv].self_test_and_update_object_attributes()
            for ms in self.config.moisture_sensors.keys():
                self.config.moisture_sensors[ms].self_test_and_update_object_attributes()
            for ts in self.config.temperature_sensors.keys():
                self.config.temperature_sensors[ts].self_test_and_update_object_attributes()
            for fm in self.config.flow_meters.keys():
                self.config.flow_meters[fm].self_test_and_update_object_attributes()
            for sw in self.config.event_switches.keys():
                self.config.event_switches[sw].self_test_and_update_object_attributes()

            self.config.verify_full_configuration()
            self.substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
