import sys
from datetime import timedelta, datetime, date
import time

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_start_stop_pause_3200 import StartConditionFor3200, PauseConditionFor3200, StopConditionFor3200

import threading

__author__ = 'bens'


class UseCase4BasicProgramming(object):
    """
    Test name:
        - Substation Basic Programming Use Case 4 \n

    Multi-Controller Setup Guide:
    
    Purpose:
        - Set up a full configuration on the controller and substation \n
        - Reboot the controller \n
        - Verify the programming is still present \n
    
    Coverage:
        Step-by-Step Summary:
        1. Initialize the controller and any substations (in this case 1) to known states.
            - Turn on echo so the commands we send are echoed. \n
            - Turn on sim mode so that we can control the time as we see fit. \n
            - Stop the clock so that it only increments when we tell it to. \n
            - Set the date and time to 1/1/2014 1:00 A.M. so we know what our date time starts at. \n
            - Turn on faux I/O so we aren't using real devices. \n
            - Clear all devices and all programming so we are in known states on the controller and substations. \n
        2. Connect the controller to it's substations. We wait for the connection to be established and then we
           increment the clock to make sure it is also displayed on the controller and substations. \n
        3. Load devices to the controller and substation and set up addresses and default values. \n
            -We load devices using the serial numbers specified in the JSON. \n
            -Do a search on the controller after both all loads are done. \n
            -Increment the clock to make sure all devices have been found and are displayed. \n
            -Share substation devices to controller, setting their addresses and default values: \n

                BiCoders:           Controller Address:       Serial Numbers:
                - Valve (ZN's)      197, 198, 199, 200        "TSQ0081", "TSQ0082", "TSQ0083", "TSQ0084"
                - Valve (MV's)      1, 2, 8                   "TMV0001", "TMV0003", "TMV0004"
                - Flow              2                         "TWF0004"
                - Switch            2                         "TPD0002"
                - Temp              2                         "TAT0002"
                - Moisture          2                         "SB07258"
                - Pump              N/A

        4. Set up Mainlines \n
        5. Set up Point of Connections \n
        6. Set up Programs \n
        7. Group zones onto programs, setting some primary, linked, and timed zones \n
        8. Set values on various devices so that we can verify them after the reboot. \n
        9. Create start/stop/pause conditions so that we can verify they are kept after a reboot. \n
        10. Increment the clock so that the controller and substations can update their values. \n
        11. Reboot the controller and substation, and then verify that the controller still has it's connection to the
            substation. Set date/time to a known state. \n
        12. Do a self test on our devices on a 3200 and update attributes on our objects \n
        13. Verify that all of our objects match their respective values on the controller and substation \n
            - Verify BaseManager connection. \n
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - Only tests using 1 substation.

    Controller Programming:
        - Zones:                8   (4-local, 4-shared)
        - Master Valves:        3   (0-local, 3-shared)
        - Event Switches:       2   (1-local, 1-shared)
        - Temp Sensors:         2   (1-local, 1-shared)
        - Moisture Sensors:     2   (1-local, 1-shared)
        - Flow Meters:          2   (1-local, 1-shared)
        - Programs:             4
        - Zone Programs:        9
        - Mainlines:            2
        - Pocs:                 2

    Substation Programming:
        - Valve BiCoders:       4
        - Moisture BiCoders:    1
        - Temperature BiCoders: 1
        - Switch BiCoders:      1
        - Flow BiCoders:        1
        - Pump BiCoders:        0
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file, substation_firmware_version,
                 number_of_substations_to_use):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        
        :param substation_firmware_version:     Expected substation firmware version.
        :type substation_firmware_version:      str
        
        :param number_of_substations_to_use:    Number of substations to use for the use case.
        :type number_of_substations_to_use:     int
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    sb_fw_version=substation_firmware_version,
                                    number_of_substations_to_use=number_of_substations_to_use)

        self.controllers = self.config.controllers
        self.substations = self.config.substations

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        """
        try:
            self.config.initialize_for_test()
            self.step_1()
            self.step_2()
            self.step_3()
            self.step_4()
            self.step_5()
            self.step_6()
            self.step_7()
            self.step_8()
            self.step_9()
            self.step_10()
            self.step_11()
            self.step_12()
            self.step_13()

        # Handle an exception
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        # Handle successful test
        else:
            helper_methods.print_test_passed(test_name=self.config.test_name)

        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_multiple_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        1) Initialize controllers and substations.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Init controller
            self.controllers[1].init_cn()

            self.config.basemanager_connection[1].verify_ip_address_state()

            # Init each substation created/configured.
            for substation_address in self.config.substations.keys():
                self.substations[substation_address].init_sb()
            # set both the 3200 and the substation to be the same time as your computer
            date_mngr.set_current_date_to_match_computer()
            helper_methods.set_controller_substation_date_and_time\
                (controller=self.config.controllers[1],
                 substations=self.config.substations,
                 _date=date_mngr.curr_day.date_string_for_controller(),
                 _time=date_mngr.curr_day.time_string_for_controller())
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substations.
        """
        method = "\n###################     Running " + sys._getframe().f_code.co_name + "    ######################\n"
        print method
        try:
            helper_methods.connect_controller_to_substations(
                controller=self.controllers[1],
                substations=self.substations
            )

            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5)
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.

        Controller devices loaded:

            Device Type             Address         Serial Number
            -----------------------------------------------------
            Zones                   1               "TSQ0071"
                                    2               "TSQ0072"
                                    49              "TSQ0073"
                                    50              "TSQ0074"
            -----------------------------------------------------
            Master Valves
            -----------------------------------------------------
            Flow Meters             1               "TWF0003"
            -----------------------------------------------------
            Moisture Sensors        1               "SB05308"
            -----------------------------------------------------
            Temperature Sensors     1               "TAT0001"
            -----------------------------------------------------
            Event Switches          1               "TPD0001"
            -----------------------------------------------------

        Substation BiCoders Shared:

            BiCoder Type            Address on 3200         Serial Number
            -------------------------------------------------------------
            Valve
            (Zones)                 197                     "TSQ0081"
                                    198                     "TSQ0082"
                                    199                     "TSQ0083"
                                    200                     "TSQ0084"

            (Master Valves)         1                       "TMV0001"
                                    2                       "TMV0003"
                                    8                       "TMV0004"

            -------------------------------------------------------------
            Flow                    2                       "TWF0004"
            -------------------------------------------------------------
            Moisture                2                       "SB07258"
            -------------------------------------------------------------
            Temperature             2                       "TAT0002"
            -------------------------------------------------------------
            Switch                  2                       "TPD0002"
            -------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].load_assigned_devices()
            self.substations[1].load_assigned_devices()

            self.controllers[1].do_search_for_all_devices()

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # 3200: Address and set default values for Devices
            helper_methods.set_all_device_default_values_on_controller(config=self.config,
                                                                       controller=self.controllers[1])

            # Substation: Set default values for Devices
            helper_methods.set_bicoder_default_values_on_substation(substations=self.substations)

            ############################################################################################################
            # Share devices from substation to controller. This makes the controller address the devices that it shares
            # with a substation, and then it sets the default values. We share these devices so that we can test and
            # verify that devices shared with a substation will retain their values after a reboot.
            # Zones
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSQ0081", address=197)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSQ0082", address=198)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSQ0083", address=199)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSQ0084", address=200)

            # Master Valves
            self.config.share_sb_device_with_cn(device_type=opcodes.master_valve, device_serial="TMV0001", address=1)
            self.config.share_sb_device_with_cn(device_type=opcodes.master_valve, device_serial="TMV0003", address=2)
            self.config.share_sb_device_with_cn(device_type=opcodes.master_valve, device_serial="TMV0004", address=8)

            # Flow Meters
            self.config.share_sb_device_with_cn(device_type=opcodes.flow_meter, device_serial="TWF0004", address=2)

            # Moisture Sensors
            self.config.share_sb_device_with_cn(device_type=opcodes.moisture_sensor, device_serial="SB07258", address=2)

            # Temperature Sensors
            self.config.share_sb_device_with_cn(device_type=opcodes.temperature_sensor,
                                                device_serial="TAT0002",
                                                address=2)

            # Event Switch
            self.config.share_sb_device_with_cn(device_type=opcodes.event_switch, device_serial="TPD0002", address=2)

            ############################################################################################################

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_4(self):
        """
        Main line 1: \n
            Set limit zones by flow to true \n
            Set the pipe fill time to 4 minutes \n
            Set the target flow to 500 \n
            Set the high variance limit to 5% and enable the high variance shut down \n
            Set the low variance limit to 20% and enable the low variance shut down \n
        \n
        Main line 8: \n
            Set limit zones by flow to true \n
            Set the pipe fill time to 1 minute \n
            Set the target flow to 50 \n
            Set the high variance limit to 20% and disable the high variance shut down \n
            Set the low variance limit to 5% and disable the low variance shut down \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.mainlines[1] = Mainline(_ad=1,
                                                _lc=opcodes.true,
                                                _ft=4,
                                                _fl=500,
                                                _hv=5,
                                                _hs=opcodes.true,
                                                _lv=20,
                                                _ls=opcodes.false)
            self.config.mainlines[8] = Mainline(_ad=8,
                                                _lc=opcodes.true,
                                                _ft=1,
                                                _fl=50,
                                                _hv=20,
                                                _hs=opcodes.false,
                                                _lv=5,
                                                _ls=opcodes.true)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_5(self):
        """
        Set up POCs \n
        POC 1: \n
            Enable POC 1 \n
            Assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            Assign POC 1 a target flow of 500 \n
            Assign POC 1 to main line 1 \n
            Set POC priority to 2-medium \n
            Set high flow limit to 550 and enable high flow shut down \n
            Set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            Set water budget to 100000 and enable the water budget shut down \n
            Enable water rationing \n
        \n
        POC 8: \n
            Enable POC 8 \n
            Assign master valve TMV0004 and flow meter TWF0004 to POC 8 \n
            Assign POC 8 a target flow of 50 \n
            Assign POC 8 to main line 8 \n
            Set POC priority to 3-low \n
            Set high flow limit to 75 and disable high flow shut down \n
            Set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
            Set water budget to 1000 and disable water budget shut down \n
            Disable water rationing \n
            Assign event switch TPD0001 to POC 8 \n
            Set switch empty condition to closed \n
            Set empty wait time to 540 minutes \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.poc[1] = POC3200(_ad=1,
                                         _mv=self.config.master_valves[2].ad,
                                         _fm=self.config.flow_meters[1].ad,
                                         _en=opcodes.true,
                                         _ml=1,
                                         _fl=500,
                                         _pr=2,
                                         _uf=10,
                                         _us=opcodes.true,
                                         _hf=550,
                                         _hs=opcodes.true,
                                         _wb=100000,
                                         _ws=opcodes.true,
                                         _wr=opcodes.true)
            self.config.poc[8] = POC3200(_ad=8,
                                         _mv=self.config.master_valves[8].ad,
                                         _fm=self.config.flow_meters[2].ad,
                                         _en=opcodes.true,
                                         _ml=1,
                                         _fl=50,
                                         _pr=3,
                                         _uf=5,
                                         _us=opcodes.false,
                                         _hf=75,
                                         _hs=opcodes.false,
                                         _wb=1000,
                                         _ws=opcodes.false,
                                         _wr=opcodes.false,
                                         _sw=self.config.event_switches[1].ad,
                                         _se=opcodes.closed,
                                         _ew=450)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_6(self):
        """
        Set up Each Program: \n
        Program 1: \n
            - Enabled \n
            - Water Windows to water every day, weekly. \n
            - Max Concurrent Zones to 4 \n
            - Calendar Interval to 'week days' \n
            - Set the watering week days to MON/WED/FRI \n
            - Set the start times to be 8 AM, 9 AM, 10 AM, and 11 AM \n
            - Assign it mainline 1 \n
        Program 3: \n
            - Enabled \n
            - Water Windows to water every day, weekly. \n
            - Max Concurrent Zones to 4 \n
            - Calendar Interval to 'odd days' \n
            - Set the watering week days to MON/WED/FRI \n
            - Set the start times to be 8 AM, 9 AM, 10 AM, and 11 AM \n
            - Assign it mainline 1 \n
        Program 4: \n
            - Disabled so we can verify that the program will stay disabled after the reboot. \n
            - Water Windows to water every day, weekly. \n
            - Max Concurrent Zones to 4 \n
            - Calendar Interval to 'week days' \n
            - Set the watering week days to MON/WED/FRI \n
            - Set the start times to be 8 AM, 9 AM, 10 AM, and 11 AM \n
            - Assign it mainline 1 \n
        Program 99: \n
            - Enabled \n
            - Water Windows to water every day, weekly. \n
            - Max Concurrent Zones to 4 \n
            - Calendar Interval to 'week days' \n
            - Set the watering week days to MON/WED/FRI \n
            - Set the start times to be 8 AM, 9 AM, 10 AM, and 11 AM \n
            - Assign it mainline 1 \n
        """
        # Variables to help with setting up the programs
        program_start_time_8am_9am_10am_11am = [480, 540, 600, 660]
        program_watering_days_mwf = [0, 1, 0, 1, 0, 1, 1]  # runs monday/wednesday/friday
        program_water_windows = ['111111111111111111111111']
        monthly_water_windows = ['011111100000111111111110', '011111100001111111111111', '011111100001111111111110',
                                 '011111100001111111111110', '011111100001111111111110', '011111100001111111111110',
                                 '011111100001111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _ww=program_water_windows,
                                             _mc=4,
                                             _ci=opcodes.week_days,
                                             _wd=program_watering_days_mwf,
                                             _sm=[],
                                             _st=program_start_time_8am_9am_10am_11am,
                                             _ml=1)
            self.config.programs[3] = PG3200(_ad=3,
                                             _en=opcodes.true,
                                             _ww=program_water_windows,
                                             _mc=4,
                                             _ci=opcodes.odd_day,
                                             _sm=[],
                                             _st=program_start_time_8am_9am_10am_11am,
                                             _ml=1)
            self.config.programs[4] = PG3200(_ad=4,
                                             _en=opcodes.false,
                                             _ww=program_water_windows,
                                             _mc=4,
                                             _ci=opcodes.week_days,
                                             _wd=program_watering_days_mwf,
                                             _sm=[],
                                             _st=program_start_time_8am_9am_10am_11am,
                                             _ml=1)
            self.config.programs[99] = PG3200(_ad=99,
                                              _en=opcodes.true,
                                              _ww=monthly_water_windows,
                                              _mc=4,
                                              _sm=[],
                                              _st=program_start_time_8am_9am_10am_11am,
                                              _ml=8)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_7(self):
        """
        Assign zones to programs \n
        Set up primary linked zones \n
        We assign zone 50 to two different programs (3 & 4) because PG 4 is disabled, so it is a testable edge case \n

        Zone 1 Program 1 (Primary Zone): \n
            - Run Time: 960 \n
            - Cycle Time: 480 \n
            - Soak Time: 300 \n
            - Moisture Sensor: 2 \n
            - Watering Strategy: Lower limit \n
            - Lower Limit: 24 \n
            - Calibrate Cycle: One time \n

        Zone 2 Program 1 (Linked Zone: Its primary zone is 1): \n
            - Tracking Ratio: 100% \n

        Zone 49 Program 3 (Timed Zone): \n
            - Run Time: 1200 \n
            - Cycle Time: 600 \n
            - Soak Time: 3600 \n
            - Watering Strategy: Timed \n

        Zone 50 Program 3 (Timed Zone): \n
            - Run Time: 1200 \n
            - Cycle Time: 600 \n
            - Soak Time: 3600 \n
            - Watering Strategy: Timed \n

        Zone 50 Program 4 (Timed Zone: But it's program is disabled): \n
            - Run Time: 1200 \n
            - Cycle Time: 600 \n
            - Soak Time: 3600 \n
            - Watering Strategy: Timed \n

        Zone 200 Program 99 (Primary Zone): \n
            - Run Time: 1980 \n
            - Cycle Time: 180 \n
            - Soak Time: 780 \n

        Zone 197 Program 99 (Linked Zone: Its primary zone is 200): \n
            - Tracking Ratio: 50% \n

        Zone 198 Program 99 (Linked Zone: Its primary zone is 200): \n
            - Tracking Ratio: 100% \n

        Zone 199 Program 99 (Linked Zone: Its primary zone is 200): \n
            - Tracking Ratio: 150% \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=960,
                                                       _ct=480,
                                                       _so=300,
                                                       _ms=self.config.moisture_sensors[2].ad,
                                                       _ws=opcodes.lower_limit,
                                                       _ll=24.0,
                                                       _cc=opcodes.calibrate_one_time,
                                                       _pz=1)
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=1)
            self.config.zone_programs[49] = ZoneProgram(zone_obj=self.config.zones[49],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=1200,
                                                        _ct=600,
                                                        _so=3600,
                                                        _ws=opcodes.timed)
            self.config.zone_programs[50] = ZoneProgram(zone_obj=self.config.zones[50],
                                                        prog_obj=self.config.programs[3],
                                                        _rt=1200,
                                                        _ct=600,
                                                        _so=3600,
                                                        _ws=opcodes.timed)
            self.config.zone_programs[51] = ZoneProgram(zone_obj=self.config.zones[50],
                                                        prog_obj=self.config.programs[4],
                                                        _rt=1200,
                                                        _ct=600,
                                                        _so=3600,
                                                        _ws=opcodes.timed)
            self.config.zone_programs[200] = ZoneProgram(zone_obj=self.config.zones[200],
                                                         prog_obj=self.config.programs[99],
                                                         _rt=1980,
                                                         _ct=180,
                                                         _so=780,
                                                         _pz=200)
            self.config.zone_programs[197] = ZoneProgram(zone_obj=self.config.zones[197],
                                                         prog_obj=self.config.programs[99],
                                                         _ra=50,
                                                         _pz=200)
            self.config.zone_programs[198] = ZoneProgram(zone_obj=self.config.zones[198],
                                                         prog_obj=self.config.programs[99],
                                                         _ra=100,
                                                         _pz=200)
            self.config.zone_programs[199] = ZoneProgram(zone_obj=self.config.zones[199],
                                                         prog_obj=self.config.programs[99],
                                                         _ra=150,
                                                         _pz=200)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        Set up non default values on devices \n
        Set up Zones \n
            - ZN 1: TSQ0071 \n
                - Design Flow: 10 \n
            - ZN 2: TSQ0072 \n
                - Design Flow: 10 \n
            - ZN 49: TSQ0073 \n
                - Design Flow: 30 \n
            - ZN 50: TSQ0074 \n
                - Design Flow: 30 \n
            - ZN 197: TSQ0081 \n
                - Design Flow: 80 \n
            - ZN 198: TSQ0082 \n
                - Design Flow: 80 \n
            - ZN 199: TSQ0083 \n
                - Design Flow: 80 \n
            - ZN 200: TSQ0084 \n
                - Design Flow: 100 \n

        Set up Moisture Sensors \n
            - MS 1: SB05308 \n
                - Moisture value: 10.0 \n
                - Two wire drop: 1.6 \n
            - MS 2: SB07258 \n
                - Moisture value: 26.0 \n
                - Two wire drop: 1.8 \n

        Set up Temperature Sensors \n
            - TS 1: TAT0001 \n
                - Temperature value: 26.0 \n
                - Two wire drop: 1.8 \n

        Set up Event Switches \n
            - SW 1: TPD0001 \n
                - Value: closed \n
                - Two wire drop: 1.8 \n

        Set up Master Valves \n
            - MV 1: TSD0001 \n
                - Booster Pump: true \n
                - Normally Open: closed \n
            - MV 2: TMV0003 \n
                - Normally Open: closed \n
            - MV 8: TMV0004 \n
                - Normally Open: open \n

        Set up Flow Meters \n
            - FM 1: TWF0003 \n
                - Enabled: True \n
                - K-value: 3.10 \n
                - Flow GPM: 25 \n
            - FM 2: TWF0004 \n
                - Enabled: True \n
                - K-value: 5.01 \n
                - Flow GPM: 50 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Zones
            self.config.zones[1].set_design_flow_on_cn(_df=10)
            self.config.zones[2].set_design_flow_on_cn(_df=10)
            self.config.zones[49].set_design_flow_on_cn(_df=30)
            self.config.zones[50].set_design_flow_on_cn(_df=30)
            self.config.zones[197].set_design_flow_on_cn(_df=80)
            self.config.zones[198].set_design_flow_on_cn(_df=80)
            self.config.zones[199].set_design_flow_on_cn(_df=80)
            self.config.zones[200].set_design_flow_on_cn(_df=100)

            # Moisture Sensors
            self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=10.0)
            self.config.moisture_sensors[1].set_two_wire_drop_value_on_cn(_value=1.60)
            self.substations[1].moisture_bicoders["SB07258"].set_moisture_percent(_percent=26.0)
            self.substations[1].moisture_bicoders["SB07258"].set_two_wire_drop_value(_value=1.8)

            # Temperature Sensors
            self.config.temperature_sensors[1].set_temperature_reading_on_cn(_temp=12.0)
            self.config.temperature_sensors[1].set_two_wire_drop_value_on_cn(_value=1.9)
            self.substations[1].temp_bicoders["TAT0002"].set_temperature_reading(_temp=26.0)
            self.substations[1].temp_bicoders["TAT0002"].set_two_wire_drop_value(_value=1.8)

            # Event Switches
            self.config.event_switches[1].set_contact_state_on_cn(_contact_state=opcodes.open)
            self.config.event_switches[1].set_two_wire_drop_value_on_cn(_value=1.1)
            self.substations[1].switch_bicoders["TPD0002"].set_contact_state(_contact_state=opcodes.open)
            self.substations[1].switch_bicoders["TPD0002"].set_two_wire_drop_value(_value=1.8)

            # Master Valves
            self.config.master_valves[1].set_booster_enable_state_on_cn(booster_state=opcodes.true)
            self.config.master_valves[1].set_normally_open_state_on_cn(_normally_open=opcodes.false)
            self.config.master_valves[2].set_normally_open_state_on_cn(_normally_open=opcodes.false)
            self.config.master_valves[8].set_normally_open_state_on_cn(_normally_open=opcodes.true)

            # Flow Meters
            self.config.flow_meters[1].set_enable_state_on_cn(_state=opcodes.true)
            self.config.flow_meters[1].set_k_value_on_cn(_k_value=3.10)
            self.config.flow_meters[1].set_flow_rate_on_cn(_flow_rate=25)
            self.config.flow_meters[2].set_enable_state_on_cn(_state=opcodes.true)
            self.config.flow_meters[2].set_k_value_on_cn(_k_value=5.01)
            self.substations[1].flow_bicoders["TWF0004"].set_flow_rate(_flow_rate=50)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        Create Start/stop/pause conditions \n
            - Verify that a program can have one of each start stop pause condition on it and it can be the same device
              triggering two different events \n
            - Verify that each program can have a device that can be located on either the 3200 or the Sub Station \n

            - Start PG 3 when Moisture Sensor 1 gets above 38 This sensor is on the 3200 \n
            - Pause PG 3 when Moisture Sensor 2 gets below 15 This sensor is on the Sub Station \n
            - Pause PG 3 when Temperature Sensor 1 gets above 48 This sensor is on the 3200 pause 3 minutes\n
            - Pause PG 3 when Temperature Sensor 2 gets below This sensor is on the Sub Station \n
            - Stop PG 3 when Event Switch 1 state changes to open This sensor is on the 3200 \n
            - Stop PG 3 when Event Switch 2  state changes to Closed  This sensor is on the Sub Station \n

            - Verify that all the devices can be on multiple programs

            - Start PG 99 when Moisture Sensor 1 gets above 38 This sensor is on the 3200 \n
            - Pause PG 99 when Moisture Sensor 2 gets below 15 This sensor is on the Sub Station \n
            - Pause PG 99 when Temperature Sensor 1 gets above 48 This sensor is on the 3200 \n
            - Pause PG 99 when Temperature Sensor 2 gets below This sensor is on the Sub Station \n
            - Stop PG 99 when Event Switch 1 state changes to open This sensor is on the 3200 \n
            - Stop PG 99 when Event Switch 2  state changes to Closed  This sensor is on the Sub Station \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # setup device on program 3
            self.config.program_start_conditions[1] = StartConditionFor3200(program_ad=3)
            self.config.program_start_conditions[1].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.upper_limit, threshold=38)

            self.config.program_start_conditions[2] = StartConditionFor3200(program_ad=3)
            self.config.program_start_conditions[2].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[2].sn, mode=opcodes.lower_limit, threshold=15)

            self.config.program_pause_conditions[1] = PauseConditionFor3200(program_ad=3)
            self.config.program_pause_conditions[1].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.upper_limit, threshold=48, pt=3)

            self.config.program_pause_conditions[2] = PauseConditionFor3200(program_ad=3)
            self.config.program_pause_conditions[2].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[2].sn, mode=opcodes.lower_limit, threshold=33)

            self.config.program_stop_conditions[1] = StopConditionFor3200(program_ad=3)
            self.config.program_stop_conditions[1].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.open)

            self.config.program_stop_conditions[2] = StopConditionFor3200(program_ad=3)
            self.config.program_stop_conditions[2].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[2].sn, mode=opcodes.closed)

            # setup devices on program 99
            self.config.program_start_conditions[1] = StartConditionFor3200(program_ad=99)
            self.config.program_start_conditions[1].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[1].sn, mode=opcodes.upper_limit, threshold=38)

            self.config.program_start_conditions[2] = StartConditionFor3200(program_ad=99)
            self.config.program_start_conditions[2].set_moisture_condition_on_pg(
                serial_number=self.config.moisture_sensors[2].sn, mode=opcodes.lower_limit, threshold=15)

            self.config.program_pause_conditions[1] = PauseConditionFor3200(program_ad=99)
            self.config.program_pause_conditions[1].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[1].sn, mode=opcodes.upper_limit, threshold=48)

            self.config.program_pause_conditions[2] = PauseConditionFor3200(program_ad=99)
            self.config.program_pause_conditions[2].set_temperature_condition_on_pg(
                serial_number=self.config.temperature_sensors[2].sn, mode=opcodes.lower_limit, threshold=33)

            self.config.program_stop_conditions[1] = StopConditionFor3200(program_ad=99)
            self.config.program_stop_conditions[1].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[1].sn, mode=opcodes.open)

            self.config.program_stop_conditions[2] = StopConditionFor3200(program_ad=99)
            self.config.program_stop_conditions[2].set_event_switch_condition_on_pg(
                serial_number=self.config.event_switches[2].sn, mode=opcodes.closed)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Verify that every object's attribute is the same as its counterpart on the actual controller and substation. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()

            for sb_address in self.config.substations.keys():
                self.config.substations[sb_address].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        Reboot the controller \n
        Stop the clock \n
        Wait for the controller and substation to confirm their reconnected status. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].do_reboot_controller()
            self.substations[1].do_reboot()

            self.controllers[1].stop_clock()
            self.substations[1].stop_clock()

            helper_methods.set_controller_substation_date_and_time\
                (controller=self.config.controllers[1],
                 substations=self.config.substations,
                 _date=date_mngr.curr_day.date_string_for_controller(),
                 _time=date_mngr.curr_day.time_string_for_controller())

            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_12(self):
        """
        Do a self test on all devices. \n
        Increment the clock by 1 minute to allow the controller time to update values. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            for zone in self.config.zones.keys():
                self.config.zones[zone].self_test_and_update_object_attributes()

            for ms in self.config.moisture_sensors.keys():
                self.config.moisture_sensors[ms].self_test_and_update_object_attributes()

            for ts in self.config.temperature_sensors.keys():
                self.config.temperature_sensors[ts].self_test_and_update_object_attributes()

            for sw in self.config.event_switches.keys():
                self.config.event_switches[sw].self_test_and_update_object_attributes()

            for fm in self.config.flow_meters.keys():
                self.config.flow_meters[fm].self_test_and_update_object_attributes()

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        Increment the clock and then verify the full configuration again on both the controller and substation. \n
        Verify the entire controller configuration again. \n
        Verify the entire substation configuration again. \n
        Verify that we are still connected to BaseManager. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()

            for sb_address in self.config.substations.keys():
                self.config.substations[sb_address].verify_full_configuration()

            self.config.basemanager_connection[1].verify_ip_address_state()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
