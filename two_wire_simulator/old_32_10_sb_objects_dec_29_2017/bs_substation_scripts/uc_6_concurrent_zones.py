import sys

# import old_32_10_sb_objects_dec_29_2017.common.product as helper_methods
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline

# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

__author__ = 'Tige'


class UseCase6ConcurrentZones(object):
    """
    Test name:
        - Substation Concurrent Zones

    Multi-Controller Setup Guide:
    
    Purpose:
        - Test and verify that shared Zones between 3200 and Substation return return the same status whether being 
          retrieved through the 3200 or through the Substation.
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Load valve bicoders on the 3200 and the substation
           the valve bicoder get addressed from the json file for the 3200 but not for the substation

        2. Assign valve bicoders to zones:
            - assign 2 local valve bicdoer from the 3200 to zones 4 and 5
                - set default valves for the zones
            - Assign 6 shared valve bicoder from the substaion to zone 1 - 3 and 6 - 8 on the 3200
                - manually address the shared valve bicoders from the substation to the 3200
                - set default valves for the valve bicoder and then the zones
        3. Set up zone programs
            - Set up 3200 to have local zones
            - Set up Substation to have shared valve bicoders with the 3200
            - setup two programs put 4 zones on each program
            - second program will have the primary zone list and linked zones to come first in the list
        4. set up mainlines:
            - setup mainline 1 to have have 1 master valve
            - setup mainline 2 to have have 2 master valves shared from substation
                  - we are not testing zones concurrency by flow only by numbers turned on by program and bicoder \n
                    limitation so set concurrency by flow to false
        5. set up programs:
            - setup program 1 to have have a start time of 8:00 am
            - set up zone concurrency on program 1 to be 3
            - setup program 2 to have have a start time of 8:00 am
            - set up zone concurrency on program 2 to be 3
        6. set up POC'S:
            - setup POC1 to have have 1 master valve
            - setup POC 2 to have have 2 master valves shared from substation assign to mainline 2
            - setup POC 3 to have have 2 master valves shared from substation assign to mainline 2
        7. Set up a design for each zone
            - do this so we know we are using concurrent zone and not flow by design flow
        8. Increment clocks forward 1 minute and verify full configuration:
            - Configuration on 3200
            - Configuration on Substation
        7. we are not testing zones concurrancy by flow only by numbers turned on by program and bicoder limitation \n
        so set concurrancy by flow to false

        Verify initial Zone status' on 3200 and Substation prior to starting both programs.
        
        7. Verify
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - This test assumes use of only 1 Substation and 1 3200
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file, substation_firmware_version,
                 number_of_substations_to_use):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        
        :param substation_firmware_version:     Expected substation firmware version.
        :type substation_firmware_version:      str
        
        :param number_of_substations_to_use:    Number of substations to use for the use case.
        :type number_of_substations_to_use:     int
        """
        self.config = Configuration(cn_type=controller_type,
                                    cn_fw_version=controller_firmware_version,
                                    cn_serial_number=cn_serial_number,
                                    test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    sb_fw_version=substation_firmware_version,
                                    number_of_substations_to_use=number_of_substations_to_use)

        self.controllers = self.config.controllers
        self.substations = self.config.substations

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        """
        try:
            self.config.initialize_for_test()
            self.step_1()
            self.step_2()
            self.step_3()
            self.step_4()
            self.step_5()
            self.step_6()
            self.step_7()
            self.step_8()
            self.step_9()
            self.step_10()
            self.step_11()
            self.step_12()
            self.step_13()

        # Handle an exception
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        # Handle successful test
        else:
            helper_methods.print_test_passed(test_name=self.config.test_name)

        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_multiple_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Init controller
            self.controllers[1].init_cn()

            self.config.controllers[1].set_max_concurrent_zones_on_cn(1)

            self.config.basemanager_connection[1].verify_ip_address_state()

            # Init each substation created/configured.
            for substation_address in self.config.substations.keys():
                self.substations[substation_address].init_sb()
            # set both the 3200 and the substation to be the same time as your computer
            date_mngr.set_current_date_to_match_computer()
            helper_methods.set_controller_substation_date_and_time \
                (controller=self.config.controllers[1],
                 substations=self.config.substations,
                 _date=date_mngr.curr_day.date_string_for_controller(),
                 _time=date_mngr.curr_day.time_string_for_controller())
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substations:
            - uses the Ip address in the user config files to set the ip address
                Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                      incremented 1 minute at a time for the max minutes or until max number of minutes is met.
            - after connection is established increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                      between controllers.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.connect_controller_to_substations(controller=self.controllers[1],
                                                             substations=self.substations)

            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5)

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller through the first controller.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].load_assigned_devices()
            self.substations[1].load_assigned_devices()

            self.controllers[1].do_search_for_all_devices()

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # 3200: Address and set default values for Devices
            helper_methods.set_all_device_default_values_on_controller(config=self.config,
                                                                       controller=self.controllers[1])

            # Substation: Set default values for Devices
            helper_methods.set_bicoder_default_values_on_substation(substations=self.substations)

            # Share valve bicoders from substation to controller.
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TVA3301", address=1)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TVA3302", address=2)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TVA3303", address=3)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TVA3304", address=6)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSE0021", address=7)
            self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSE0022", address=8)

            # Share valve bicoders as master valves from substation to controller.
            self.config.share_sb_device_with_cn(device_type=opcodes.master_valve, device_serial="TSE0031", address=2)
            self.config.share_sb_device_with_cn(device_type=opcodes.master_valve, device_serial="TSE0032", address=3)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        Create mainline objects and set values on mainlines
        - set up main line 1 \n
            - set limit zones by flow to false \n
            - set the pipe fill time to 4 minutes \n
            - set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 2 \n
            set limit zones by flow to true \n
            set the pipe fill time to 1 minute \n
            set the target flow to 50 \n
            set the high variance limit to 20% and disable the high variance shut down \n
            set the low variance limit to 5% and disable the low variance shut down \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.mainlines[1] = Mainline(_ad=1,
                                                _lc=opcodes.false,
                                                _ft=4,
                                                _fl=500,
                                                _hv=5,
                                                _hs=opcodes.true,
                                                _lv=20,
                                                _ls=opcodes.false)
            self.config.mainlines[2] = Mainline(_ad=2,
                                                _lc=opcodes.false,
                                                _ft=1,
                                                _fl=50,
                                                _hv=20,
                                                _hs=opcodes.false,
                                                _lv=5,
                                                _ls=opcodes.true)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        - Set up PG3200 objects and program controller
            - give each program a start time of 8:00 A.M. \n
            - set up program watering days \n
            - set up water windows for programs \n
            - set programs to have a concurrent zone of 3 \n
        """
        # this is set in the PG3200 object

        program_start_time_8am = [480]
        program_water_windows = ['111111111111111111111111']
        program_watering_days = [1, 1, 1, 1, 1, 1, 1]  # runs monday/wednesday/friday

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _ww=program_water_windows,
                                             _mc=3,
                                             _ci=opcodes.week_days,
                                             _wd=program_watering_days,
                                             _sm=[],
                                             _st=program_start_time_8am,
                                             _ml=1)
            self.config.programs[2] = PG3200(_ad=2,
                                             _en=opcodes.true,
                                             _ww=program_water_windows,
                                             _mc=3,
                                             _ci=opcodes.week_days,
                                             _wd=program_watering_days,
                                             _sm=[],
                                             _st=program_start_time_8am,
                                             _ml=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        For 3200:
            - configure 4 Zone Programs assigned to program 1.
            - configure 4 Zone Programs assigned to program 2.

        - Program 1:
            - Zone 1 assigned to Program 1:
                - Runtime: 3600
                - Cycle Time: 100
                - Soak Time: 100
                - Mode: Primary Zone
            - Zone 2 linked Primary Zone 1:
                - Tracking Ratio: 100%
                - Mode: Linked
            - Zone 3 linked Primary Zone  1:
                - Tracking Ratio: 100%
                - Mode: Linked
            - Zone 4 linked Primary Zone  1:
                - Tracking Ratio: 100%
                - Mode: Linked
            
        - Program 2:
            - Zone 5 linked to Primary Zone 8:
                - Tracking Ratio: 100%
                - Mode: Linked
            - Zone 6 linked to Primary Zone 8:
                - Tracking Ratio: 100%
                - Mode: Linked
            - Zone 7 linked Primary Zone 8:
                - Tracking Ratio: 100%
                - Mode: Linked
            - Zone 8 assigned to Program 2:
                - Runtime: 3600
                - Cycle Time: 100
                - Soak Time: 100
                - Mode: Primary zone
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Create Zone Program object 1 which assigns zone 1 to program 1
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=3600,
                                                       _ct=120,
                                                       _so=100,
                                                       _pz=1)

            # Create Zone Program object 2 which links zone 2 to zone 1 on program 1
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=1)

            # Create Zone Program object 3 which links zone 3 to zone 1 on program 1
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=1)
            # Create Zone Program object 4 which links zone 4 to zone 1 on program 1
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _ra=100,
                                                       _pz=1)

            # Create Zone Program object 5 which assigns zone 8 to program 2
            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[8],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=3600,
                                                       _ct=120,
                                                       _so=120,
                                                       _pz=8)

            # Create Zone Program object 6 which links zone 6 to zone 8 on program 2
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[2],
                                                       _ra=100,
                                                       _pz=8)

            # Create Zone Program object 7 which links zone 7 to zone 8 on program 2
            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[6],
                                                       prog_obj=self.config.programs[2],
                                                       _ra=100,
                                                       _pz=8)
            # Create Zone Program object 8 which links zone 5 to zone 8 on program 2
            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[7],
                                                       prog_obj=self.config.programs[2],
                                                       _ra=100,
                                                       _pz=8)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        Set up POCs \n
        POC 1 \n
            enable POC 1 \n
            assign master valve 3 TMV0001 \n
            assign POC 1 a target flow of 500 \n
            assign POC 1 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            disable water rationing \n
        \n
        POC 2 \n
            enable POC 2 \n
            assign master valve 1 TSE0031  \n
            assign POC 8 a target flow of 50 \n
            assign POC 8 to main line 2 \n
            set POC priority to 3-low \n
            set high flow limit to 75 and disable high flow shut down \n
            set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
            set water budget to 1000 and disable water budget shut down \n
            disable water rationing \n
            \n
        POC 3 \n
            enable POC 2 \n
            assign master valve 2 TSE0032  \n
            assign POC 8 a target flow of 50 \n
            assign POC 8 to main line 2 \n
            set POC priority to 3-low \n
            set high flow limit to 75 and disable high flow shut down \n
            set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
            set water budget to 1000 and disable water budget shut down \n
            disable water rationing \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.poc[1] = POC3200(_ad=1,
                                         _mv=self.config.master_valves[1].ad,
                                         _en=opcodes.true,
                                         _ml=1,
                                         _fl=500,
                                         _pr=2,
                                         _uf=10,
                                         _us=opcodes.true,
                                         _hf=550,
                                         _hs=opcodes.true,
                                         _wb=100000,
                                         _ws=opcodes.true,
                                         _wr=opcodes.true)
            self.config.poc[2] = POC3200(_ad=2,
                                         _mv=self.config.master_valves[2].ad,
                                         _en=opcodes.true,
                                         _ml=2,
                                         _fl=50,
                                         _pr=3,
                                         _uf=5,
                                         _us=opcodes.false,
                                         _hf=75,
                                         _hs=opcodes.false,
                                         _wb=1000,
                                         _ws=opcodes.false,
                                         _wr=opcodes.false)
            self.config.poc[3] = POC3200(_ad=3,
                                         _mv=self.config.master_valves[3].ad,
                                         _en=opcodes.true,
                                         _ml=2,
                                         _fl=50,
                                         _pr=3,
                                         _uf=5,
                                         _us=opcodes.false,
                                         _hf=75,
                                         _hs=opcodes.false,
                                         _wb=1000,
                                         _ws=opcodes.false,
                                         _wr=opcodes.false)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        Set up a design for each zone
        do this so we know we are using concurrent zone and not flow by design flow
         """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            for zone in self.config.zones.keys():
                self.config.zones[zone].set_design_flow_on_cn(_df=100.0)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()
            self.substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        1) Set date and time on controller and substations to match your computer
        2) Increment clock on both 3200 and substation to update statuses
        3) Because no programs are running all programs master valve and zone should not be in any state but done
        4) Set date and time on both substation and on the 3200 to the same time as your computer
        5) Verify the following status:
            - Programs:
                - Program 1 done watering
                - Program 2 done watering
            - Master Valves
                - MV1 = done watering
                - MV2 = done watering
                - MV3 = done watering
                - Valve BiCoder assigned to master valve address numbers located on substation
                    - TSE0031 = done watering
                    - TSE0032 = done watering
            - Zones:
                - Zone 1 done watering
                - Zone 2 done watering
                - Zone 3 done watering
                - Zone 4 done watering
                - Zone 5 done watering
                - Zone 6 done watering
                - Zone 7 done watering
                - Zone 8 done watering
                - Valve BiCoder assigned to zones address numbers located on substation
                    - TVA3301 done watering
                    - TVA3302 done watering
                    - TVA3303 done watering
                    - TVA3306 done watering
                    - TVA3307 done watering
                    - TVA3308 done watering
        3) Start Programming.
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # set both the 3200 and the substation to be the same time as your computer
            date_mngr.set_current_date_to_match_computer()
            helper_methods.set_controller_substation_date_and_time \
                (controller=self.config.controllers[1],
                 substations=self.config.substations,
                 _date=date_mngr.curr_day.date_string_for_controller(),
                 _time=date_mngr.curr_day.time_string_for_controller())

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)
            # Verify Programs is not currently running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)

            # Verify controller master valves initial status
            for master_valve in self.config.master_valves.keys():
                self.config.master_valves[master_valve].get_data()
            self.config.master_valves[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.master_valves[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.master_valves[3].verify_status_on_cn(status=opcodes.done_watering)

            # Verify initial status on Substation valve bicoders assigned to master valves
            self.substations[1].valve_bicoders['TSE0031'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSE0032'].verify_status(_status=opcodes.done_watering)


            # Get data from controller using the keys
            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            # Verify controller zones initial status
            self.config.zones[1].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[4].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[7].verify_status_on_cn(status=opcodes.done_watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.done_watering)

            # Verify initial status on Substation valve bicoders assigned to zones
            self.substations[1].valve_bicoders['TVA3301'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TVA3302'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TVA3303'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TVA3306'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TVA3307'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TVA3308'].verify_status(_status=opcodes.done_watering)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        1) Start program 1 and 2
        2) Increment clock on both 3200 and substation to update statuses
            - because the controller is set to one concurrent zone only zone 1 and master valve should be set to \n
            watering all other zones should be set to waiting
            - program one will got to water but since no zones are running on program 2 it should be set to waiting
        3) Because no programs are running all programs master valve and zone should not be in any state but done
        4) Verify the following status:
            - Programs:
                - Program 1 done watering
                - Program 2 Waiting to water
            - Master Valves
                - MV1 = watering
                - MV2 = waiting to water
                - MV3 = waiting to water
                - Valve BiCoder assigned to master valve address numbers located on substation
                    - TSE0031 = done watering
                    - TSE0032 = done watering
            - Zones:
                - Zone 1 watering
                - Zone 2 waiting to water
                - Zone 3 waiting to water
                - Zone 4 waiting to water
                - Zone 5 waiting to water
                - Zone 6 waiting to water
                - Zone 7 waiting to water
                - Zone 8 waiting to water
                - Valve BiCoder assigned to zones address numbers located on substation
                    - TVA3301 watering
                    - TVA3302 done watering
                    - TVA3303 done watering
                    - TVA3306 done watering
                    - TSE0021 done watering
                    - TSE0022 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:

            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.start_program)
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function=opcodes.start_program)

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Verify Programs is not currently running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)

            # Verify controller master valves initial status
            for master_valve in self.config.master_valves.keys():
                self.config.master_valves[master_valve].get_data()
            self.config.master_valves[1].verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[2].verify_status_on_cn(status=opcodes.done_watering)
            self.config.master_valves[3].verify_status_on_cn(status=opcodes.done_watering)

            # Verify initial status on Substation valve bicoders assigned to master valves
            self.substations[1].valve_bicoders['TSE0031'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSE0032'].verify_status(_status=opcodes.done_watering)

            # Get data from controller using the keys
            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            # Verify controller zones initial status
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[5].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[6].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[7].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[8].verify_status_on_cn(status=opcodes.waiting_to_water)

            # Verify initial status on Substation valve bicoders assigned to zones
            self.substations[1].valve_bicoders['TVA3301'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TVA3302'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TVA3303'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TVA3306'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSE0021'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSE0022'].verify_status(_status=opcodes.done_watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """

        1) Stop program 1 and 2
        2) Increment clock on both 3200 and substation to update statuses
        2) Reset the max zone the controller can run to 7
        2) Increment clock on both 3200 and substation to update statuses
            -  Both programs should start watering
            - Because master valves are not included in master concurrent zones all 3 master valve should start \n
            watering
                - Program
                    - zones 1-3 come from a 12 valve -
                    - zone 4 is on a dual
                - program 2
                    - zones 5 is the other half of the dual assign to zone 4
                    - zone 6 is from the same 12 valve as 1-3
                    - zone 7-8 are a dual
            - 12 valve decoders can only operate 2 valves at a time

        4) Verify the following status:
            - Programs:
                - Program 1 watering
                - Program 2 watering
            - Master Valves
                - MV1 = watering
                - MV2 = watering
                - MV3 = watering
                - Valve BiCoder assigned to master valve address numbers located on substation
                    - TSE0031 = watering
                    - TSE0032 = watering
            - Zones:
                - Zone 1 watering
                - Zone 2 watering
                - Zone 3 waiting to water
                - Zone 4 watering
                - Zone 5 watering
                - Zone 6 waiting to water
                - Zone 7 waiting to water
                - Zone 8 waiting to water
                - Valve BiCoder assigned to zones address numbers located on substation
                    - TVA3301 watering
                    - TVA3302 watering
                    - TVA3303 done watering
                    - TVA3306 done watering
                    - TSE0021 watering
                    - TSE0022 watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:

            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.stop_program)
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function=opcodes.stop_program)

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)
            self.config.controllers[1].set_max_concurrent_zones_on_cn(_max_zones=7)

            self.config.controllers[1].set_program_start_stop(_pg_ad=1, _function=opcodes.start_program)
            self.config.controllers[1].set_program_start_stop(_pg_ad=2, _function=opcodes.start_program)

            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            # Verify Programs is not currently running
            self.config.programs[1].get_data()
            self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
            self.config.programs[2].get_data()
            self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)

            # Verify controller master valves initial status
            for master_valve in self.config.master_valves.keys():
                self.config.master_valves[master_valve].get_data()
            self.config.master_valves[1].verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[2].verify_status_on_cn(status=opcodes.watering)
            self.config.master_valves[3].verify_status_on_cn(status=opcodes.watering)

            # Verify initial status on Substation valve bicoders assigned to master valves
            self.substations[1].valve_bicoders['TSE0031'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSE0032'].verify_status(_status=opcodes.watering)

            # Get data from controller using the keys
            for zone in self.config.zones.keys():
                self.config.zones[zone].get_data()

            # Verify controller zones initial status
            self.config.zones[1].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[2].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[3].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[4].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[5].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[6].verify_status_on_cn(status=opcodes.waiting_to_water)
            self.config.zones[7].verify_status_on_cn(status=opcodes.watering)
            self.config.zones[8].verify_status_on_cn(status=opcodes.watering)

            # Verify initial status on Substation valve bicoders assigned to zones
            self.substations[1].valve_bicoders['TVA3301'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TVA3302'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TVA3303'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TVA3306'].verify_status(_status=opcodes.done_watering)
            self.substations[1].valve_bicoders['TSE0021'].verify_status(_status=opcodes.watering)
            self.substations[1].valve_bicoders['TSE0022'].verify_status(_status=opcodes.watering)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.controllers[1],
                                                                  substations=self.substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()
            self.substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
