import csv
import os

__author__ = 'baseline'


class FileIOOptions (object):
    """
    Sample File IO enum class. The following options are available to implement, with only a select few implemented.

    USAGE:
        FileIOOptions.APPENDING returns character "a"
        FileIOOptions.WRITE_OVERWRITE returns characters "w+"

    FILE_IO_OPTIONS:
    r ......Opens a file for reading only. The file pointer is placed at the beginning of the file. This is the 
            default mode.
    rb .....Opens a file for reading only in binary format. The file pointer is placed at the beginning of the 
            file. This is the default mode.
    r+ .....Opens a file for both reading and writing. The file pointer placed at the beginning of the file.
    rb+ ....Opens a file for both reading and writing in binary format. The file pointer placed at the beginning 
            of the file.
    w ......Opens a file for writing only. Overwrites the file if the file exists. If the file does not exist, 
            creates a new file for writing.
    wb .....Opens a file for writing only in binary format. Overwrites the file if the file exists. If the file 
            does not exist, creates a new file for writing.
    w+ .....Opens a file for both writing and reading. Overwrites the existing file if the file exists. If the 
            file does not exist, creates a new file for reading and writing.
    wb+ ....Opens a file for both writing and reading in binary format. Overwrites the existing file if the file 
            exists. If the file does not exist, creates a new file for reading and writing.
    a ......Opens a file for appending. The file pointer is at the end of the file if the file exists. That is, 
            the file is in the append mode. If the file does not exist, it creates a new file for writing.
    ab .....Opens a file for appending in binary format. The file pointer is at the end of the file if the file 
            exists. That is, the file is in the append mode. If the file does not exist, it creates a new file 
            for writing.
    a+ .....Opens a file for both appending and reading. The file pointer is at the end of the file if the file 
            exists. The file opens in the append mode. If the file does not exist, it creates a new file for 
            reading and writing.
    ab+ ....Opens a file for both appending and reading in binary format. The file pointer is at the end of the 
            file if the file exists. The file opens in the append mode. If the file does not exist, it creates a 
            new file for reading and writing.
    """
    APPENDING = "a"
    APPEND_READ = "a+"
    READ_ONLY = "r"
    READ_WRITE_NO_OVERWRITE = "r+"
    READ_WRITE_OVERWRITE = "w+"
    WRITE_ONLY = "w"


class CSVWriter(object):
    """
    ToDo's:
        1)  Output Attributes of any object to CSV
            1.1)    A method takes in an object, creates a dictionary containing it's attribute:value pairs    
            1.2)    A method which takes a dictionary object and writes it to the CSV file
            1.3)    A wrapper method around {1.1} which takes a list of objects to output to CSV

        2)  add object attribute: obj_type to every object containing their equivalent opcode value:
            i.e., obj_type = opcodes.zone, obj_type = opcodes.program + "32" (for "PG32")

        3)  add default template for each object type
        4)  add current specific templates as needed (EPA Zone Config Headers)
    """

    _base_path = os.path.dirname(os.path.abspath(__file__))

    def __init__(self, file_name, relative_path, delimiter, line_terminator, file_io_option=FileIOOptions.APPENDING):
        """
        CSV Writer Initialization Class

        example init:
            foo = CSVWriter(
                    file_name="some_name.csv",
                    relative_path="bmw1000_scripts",
                    file_io_options=FileIOOptions.APPENDING,
                    delimeter=",",
                    line_terminator="\n"
            );

            - this creates a csv file named 'some_name' into the directory bmw1000_scripts.


        :param file_name:       name for file
        :type file_name:        str
        :param relative_path:   path/dir for file
        :type relative_path:    str
        :param file_io_option:  python file io option specified on: "http://www.tutorialspoint.com/python/python_files_io.htm"
        :type file_io_option:   str
        :param delimiter:       character to separate each column (i.e., delimeter=',' - a comma)
        :type delimiter:        str
        :param line_terminator: line ending character to use
        :type line_terminator:  str
        """
        self.fname = file_name + ".csv"
        self.rel_path = relative_path
        self.file_io_option = file_io_option
        self.delim_char = delimiter
        self.line_term_char = line_terminator
        self.file_path = self._base_path + '/' + self.rel_path + '/' + self.fname

        if os.path.exists(self.file_path):
            os.remove(self.file_path)

        self.headers_printed = False  # Flag to make sure headers are only printed once for dictionary

        # empty file
        self.csv_file = None
        # null csv writer (hopefully)
        self.csv_writer = None


    def write_dict_of_obj(self, dict_of_objs, template=None):
        """
        Takes in a dictionary of objects and outputs it to the csv file. (WRAPPER METHOD)
        :param dict_of_objs:    The dictionary that holds all the objects
        :param template:        Header for this section of the csv. Prints all object attributes if left 'None'
        :return:
        """
        for each_obj_index in sorted(dict_of_objs.keys()):
            self.write_obj(obj=dict_of_objs[each_obj_index], template=template)
            self.headers_printed = True  # Flag to make sure headers are only printed once

        self.headers_printed = False  # Reset the flag so header's can be printed again

    def write_obj(self, obj, template=None):
        """
        Takes in a single object and outputs it to the csv.
        :param obj:             The object who's attributes will be output to the csv
        :param template:        Header for this object. Prints all object attributes if left 'None'
        :return:
        """
        # Gets all the object's attributes and stores them in a dictionary
        temp_dict = obj.__dict__

        # If template was specified then output the template to the csv
        if template is not None:
            self.dict_to_csv(dict=temp_dict, headers=template)
        else:
            self.dict_to_csv(dict=temp_dict, headers=sorted(temp_dict.keys()))

    def dict_to_csv(self, dict, headers):
        """
        Takes in a dictionary and outputs it to the csv (HELPER METHOD)
        :param dict:        The dictionary to be output to the csv.
        :param headers:     The header's for this section of the csv.
        :return:
        """
        with open(self.file_path, self.file_io_option, buffering=0) as output_file:
            dict_writer = csv.DictWriter(output_file, fieldnames=headers, extrasaction='ignore',
                                         lineterminator=self.line_term_char)
            if not self.headers_printed:
                dict_writer.writeheader()
            dict_writer.writerow(dict)

    def open(self):
        self.csv_file = open(self.file_path, self.file_io_option, buffering=0)
        self.csv_writer = csv.writer(self.csv_file, delimiter=self.delim_char, lineterminator=self.line_term_char)

    def close(self):
        self.csv_file.close()

    def writerow(self, data_to_write):
        with open(self.file_path, self.file_io_option, buffering=0) as output_file:
            csv_writer = csv.writer(output_file, delimiter=self.delim_char, lineterminator=self.line_term_char)
            csv_writer.writerow(data_to_write)

    def writerows(self, datarows_to_write):
        with open(self.file_path, self.file_io_option, buffering=0) as output_file:
            csv_writer = csv.writer(output_file, delimiter=self.delim_char, lineterminator=self.line_term_char)
            csv_writer.writerows(datarows_to_write)

if __name__ == "__main__":

    # write to home directory of project
    writer = CSVWriter('foo.csv', '', FileIOOptions.READ_WRITE_NO_OVERWRITE, ',', '\n')
    writer.open()

    output = [
        ["Zone", "SN", "DS", "EN", "TY", "LA", "LG", "DF", "SS", "KC", "PR", "RZ", "EF", "MB", "VA", "VV", "VT", "RT", "CT", "SO", "PZ", "EE", "WS", "SS"],
        ["Zone", "SN", "DS", "EN", "TY", "LA", "LG", "DF", "SS", "KC", "PR", "RZ", "EF", "MB", "VA", "VV", "VT", "RT", "CT", "SO", "PZ", "EE", "WS", "SS"]
    ]
    # outputs array on same row
    writer.writerow(output)
    writer.close()

    writer = CSVWriter('foo2.csv', 'bs1000_scripts', FileIOOptions.READ_WRITE_NO_OVERWRITE, ',', '\n')
    output = [
        ["Zone", "SN", "DS", "EN", "TY", "LA", "LG", "DF", "SS", "KC", "PR", "RZ", "EF", "MB", "VA", "VV", "VT", "RT", "CT", "SO", "PZ", "EE", "WS", "SS"],
        ["Zone", "SN", "DS", "EN", "TY", "LA", "LG", "DF", "SS", "KC", "PR", "RZ", "EF", "MB", "VA", "VV", "VT", "RT", "CT", "SO", "PZ", "EE", "WS", "SS"]
    ]
    writer.open()
    # outputs array on different rows
    writer.writerows(output)
    writer.close()

    print "DONE!!"
