__author__ = 'baseline'
"""
page_factory.py provides a way to get page objects from a single location as well
as avoiding circular imports.

This module imports each python script as a module,
    i.e., import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page as main_page

instead of,
    from old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page import MainPage
    -> these types of imports create circular import dependencies

This allows us to unit test all constructors for page objects.

Pages implemented so far:

Desktop:
    01. Login Page
    02. Main Page
    03. Controller Settings Dialog
    04. Maps
    05. Programs
    06. QuickView
    07. LiveView
    08. Zones
    09. Moisture Sensors
    10. Master Valves
    11. Flow Meters
    12. Temperature Sensors
    13. Event Switches
    14. Water Sources (1000 Controller)

Mobile:
    01. Login Page
    02. Main Page

Usage:
1. import page_factory
2. main_page = factory.get_main_page_object(_webdriver=some_webdriver_instance)
"""
from selenium import webdriver
import old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver as driver
import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page as main_page
import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.login_page as login_page
import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.maps_tab as maps_tab
import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.pg_tab as pg_tab
import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.qv_tab as qv_tab
import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.dv_tab as dv_tab
import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.live_view_tab as live_view_tab
import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.water_sources_tab_1000 as water_sources_tab_1000
import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.water_sources_tab_3200 as water_sources_tab_3200
import old_32_10_sb_objects_dec_29_2017.common.objects.mobile_access.m_login_page as m_login_page
import old_32_10_sb_objects_dec_29_2017.common.objects.mobile_access.m_main_page as m_main_page
import old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.subscriptions_page as subscription_page


def get_login_page_object(_webdriver):
    """
    Returns a login page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Login Page instance
    :rtype: login_page.LoginPage
    """
    return login_page.LoginPage(web_driver=_webdriver)


def get_mobile_login_page_object(_webdriver):
    """
    Returns a mobile login page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Mobile Login Page instance
    :rtype: m_login_page.MobileLoginPage
    """
    return m_login_page.MobileLoginPage(web_driver=_webdriver)


def get_main_menu_object(_webdriver):
    """
    Returns a main menu page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Main Menu instance
    :rtype: main_page.MainPageMenu
    """
    return main_page.MainPageMenu(web_driver=_webdriver)


def get_controller_settings_dialog_object(_webdriver):
    """
    Returns a controller settings dialog page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Controller Settings Dialog instance
    :rtype: main_page.ControllerSettingsDialogBox
    """
    return main_page.ControllerSettingsDialogBox(web_driver=_webdriver)


def get_main_page_object(_webdriver):
    """
    Returns a main page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Main Page Instance
    :rtype: main_page.MainPage
    """
    return main_page.MainPage(web_driver=_webdriver)


def get_mobile_main_page_object(_webdriver):
    """
    Returns a mobile main page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Mobile Main Page Instance
    :rtype: m_main_page.MobileMainPage
    """
    return m_main_page.MobileMainPage(web_driver=_webdriver)


def get_maps_page_object(_webdriver):
    """
    Returns a maps page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Maps Page Instance
    :rtype: maps_tab.MapsTab
    """
    return maps_tab.MapsTab(web_driver=_webdriver)


def get_programs_page_object(_webdriver):
    """
    Returns a programs page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Programs Page Instance
    :rtype: pg_tab.ProgramsTab
    """
    return pg_tab.ProgramsTab(web_driver=_webdriver)


def get_quick_view_page_object(_webdriver):
    """
    Returns a quick view page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: QuickView Page Instance
    :rtype: qv_tab.QuickViewTab
    """
    return qv_tab.QuickViewTab(web_driver=_webdriver)


def get_zones_page_object(_webdriver):
    """
    Returns a zones page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Zones Page Instance
    :rtype: dv_tab.ZonesTab
    """
    return dv_tab.ZonesTab(driver=_webdriver)


def get_moisture_sensors_page_object(_webdriver):
    """
    Returns a moisture sensors page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Moisture Sensors Page Instance
    :rtype: dv_tab.MoistureSensorsTab
    """
    return dv_tab.MoistureSensorsTab(driver=_webdriver)


def get_master_valves_page_object(_webdriver):
    """
    Returns a master valve page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Master Valve Page Instance
    :rtype: dv_tab.MasterValvesTab
    """
    return dv_tab.MasterValvesTab(driver=_webdriver)


def get_flow_meter_page_object(_webdriver):
    """
    Returns a flow meter page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Flow Meter page Instance
    :rtype: dv_tab.FlowMetersTab
    """
    return dv_tab.FlowMetersTab(driver=_webdriver)


def get_temperature_sensors_page_object(_webdriver):
    """
    Returns a temperature sensors page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Temperature Sensor Page Instance
    :rtype: dv_tab.TemperatureSensorsTab
    """
    return dv_tab.TemperatureSensorsTab(driver=_webdriver)


def get_event_switch_page_object(_webdriver):
    """
    Returns a event switch page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Event Switch Page Instance
    :rtype: dv_tab.EventSwitchTab
    """
    return dv_tab.EventSwitchTab(driver=_webdriver)


def get_1000_water_sources_tab(_webdriver):
    """
    Returns a 1000 water sources page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: 1000 Water Sources Page Instance
    :rtype: water_sources_tab_1000.WaterSourcesTab1000
    """
    return water_sources_tab_1000.WaterSourcesTab1000(web_driver=_webdriver)


def get_3200_poc_tab(_webdriver):
    """
    Returns a 3200 Point Of Connection page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: 3200 Point Of Connection Page Instance
    :rtype: water_sources_tab_3200.PointOfConnectionPage3200
    """
    return water_sources_tab_3200.PointOfConnectionPage3200(web_driver=_webdriver)


def get_3200_mainlines_tab(_webdriver):
    """
    Returns a 3200 Mainlines page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: 3200 Mainlines Page Instance
    :rtype: water_sources_tab_3200.MainlinesPage3200
    """
    return water_sources_tab_3200.MainlinesPage3200(web_driver=_webdriver)


def get_live_view_page_object(_webdriver):
    """
    Returns a live view page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: LiveView Page Instance
    :rtype: live_view_tab.LiveViewTab
    """
    return live_view_tab.LiveViewTab(web_driver=_webdriver)

def get_subscription_page_object(_webdriver):
    """
    Returns a subscription page object
    :param _webdriver: Web Driver instance
    :type _webdriver: driver.WebDriver
    :return: Subscription Page Instance
    :rtype: subscription_page.SubscpriptionPage
    :param _webdriver:
    :return:
    """
    return subscription_page.SubscriptionsPage(web_driver=_webdriver)