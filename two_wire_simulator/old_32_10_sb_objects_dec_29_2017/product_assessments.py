
import sys
import os

from old_32_10_sb_objects_dec_29_2017.common.logging_handler import LoggingHandler
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler
import old_32_10_sb_objects_dec_29_2017.common.user_configuration as user_conf_module
# 1000 use cases
# 3200 use cases
# import old_32_10_sb_objects_dec_29_2017.bs3200_scripts.use_case_15 as cuc_3200_15_module

# bmw use cases

# jade use cases

# bacnet use cases

# Substation use cases
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_1_timed_zones as cuc_substation_1_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_2_soak_cycles as cuc_substation_2_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_3_disconnect_reconnect as cuc_substation_3_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_4_basic_programming as cuc_substation_4_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_5_replace_devices as cuc_substation_5_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_6_concurrent_zones as cuc_substation_6_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_7_move_devices as cuc_substation_7_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_8_learn_flow as cuc_substation_8_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_9_messages as cuc_substation_9_module
import old_32_10_sb_objects_dec_29_2017.bs_substation_scripts.uc_10_backup_restore as cuc_substation_10_module


_author__ = 'Tige'


def main(argv):
    """
    ControllerUseCase1:
        - Coverage Area:
            -Reboot controller
            -Update Firmware
            #TODO - verify configuration
            #TODO -Replace Device

    BaseManager Use Cases:
        1. UseCase1
            - Coverage Area:
                -Login to url
        2. UseCase2
            - Selects and verifies all BaseManager main tabs open for a 1000:
                1. Maps
                2. QuickView
                3. Programs
                4. All Device tabs
                5. Water Sources (1000)
                6. LiveView
    """

    # Load in user configured items from text file (necessary for serial connection)
    user_conf = user_conf_module.UserConfiguration(os.path.join('common/user_credentials', 'user_credentials_eldin.json'))

    # Logging information. Set 'enabled' to True if you want to run through each use case and not bomb out on a failure.
    # It will also log any passes and fails (with error messages) into a log file.
    # Set 'enabled' to false if you want the program to bomb out if an individual test fails, and there will be no log.
    LoggingHandler.enable_continuous_run_and_logging_for_use_cases(enabled=False)
    # Set 'overwrite' to true if you want to log file to contain only the contents of your current run (it will
    # overwrite any previous runs of product assessment).
    LoggingHandler.overwrite_log_file(overwrite=True)
    # This will not log anything if logging is disabled, if it is enabled, it will log the location of the log file
    log_handler.debug("Logging started, output file is at: automated_testing_2015/{0}".format(log_handler.LOG_FILENAME))

    # 3200 Serial number and firmware version
    cn_serial_num = '3K10001'
    firm_version = '12.34'

    # Substation firmware version (latest firmware version tested against that "passed" all substation use cases)
    substation_fw_version = "1.0"

    # ---------------------------------------------------------------------------------------------------------------- #
    #     1000 Tests                                                                                                   #
    # ---------------------------------------------------------------------------------------------------------------- #
    # uc2 = cuc_1000_2_module.ControllerUseCase2(controller_type="10",
    #                                            controller_firmware_version="1.20.1",
    #                                            fw_database_id="88",
    #                                            test_name="EPATestConfiguration",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='EPA_test_configuration.json')
    #
    # uc2.run_use_case()
    #
    # uc3 = cuc_1000_3_module.ControllerUseCase3(controller_type="10",
    #                                            controller_firmware_version="0.170",
    #                                            fw_database_id="88",
    #                                            test_name="SetMessages",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='set_messages.json')
    #
    # uc3.run_use_case()
    #
    # uc4 = cuc_1000_4_module.ControllerUseCase4(controller_type="10",
    #                                            controller_firmware_version="0.171",
    #                                            fw_database_id="88",
    #                                            test_name="NelsonFeatureProgramCycles",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='nelson_features.json')
    #
    # uc4.run_use_case()
    #
    # uc5 = cuc_1000_5_module.ControllerUseCase5(controller_type="10",
    #                                            controller_firmware_version="0.171",
    #                                            fw_database_id="88",
    #                                            test_name="NelsonFeatureOver15Concurrent",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='nelson_features.json')
    #
    # uc5.run_use_case()
    #
    # uc6 = cuc_1000_6_module.ControllerUseCase6(controller_type="10",
    #                                            controller_firmware_version="0.171",
    #                                            fw_database_id="88",
    #                                            test_name="NelsonMasterValesWithZones",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='nelson_features.json')
    #
    # uc6.run_use_case()
    #
    # uc7 = cuc_1000_7_module.ControllerUseCase7(controller_type="10",
    #                                            controller_firmware_version="0.171",
    #                                            fw_database_id="88",
    #                                            test_name="NelsonFeatureMirroredZones",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='nelson_features.json')
    #
    # uc7.run_use_case()
    #
    # ---------------------------------------------------------------------------------------------------------------- #
    #    Jade Tests                                                                                                    #
    # ---------------------------------------------------------------------------------------------------------------- #
    #
    # uc1 = cuc_jade_1_module.ControllerUseCase1(controller_type="10",
    #                                            controller_firmware_version="0.171",
    #                                            fw_database_id="88",
    #                                            test_name="BasicLearnFlowTest",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='basic_flow_test.json')
    #
    # uc1.run_use_case()
    #
    # uc2 = cuc_jade_2_module.ControllerUseCase2(controller_type="10",
    #                                            controller_firmware_version="0.171",
    #                                            fw_database_id="88",
    #                                            test_name="BasicDesignFlowTest",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='basic_flow_test.json')
    #
    # uc2.run_use_case()
    #
    # uc3 = cuc_jade_3_module.ControllerUseCase3(controller_type="10",
    #                                            controller_firmware_version="0.171",
    #                                            fw_database_id="88",
    #                                            test_name="BasicProgrammingTest",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='basic_programming.json')
    #
    # uc3.run_use_case()
    #
    # uc4 = cuc_jade_4_module.ControllerUseCase4(controller_type="10",
    #                                            controller_firmware_version="0.171",
    #                                            fw_database_id="88",
    #                                            test_name="ConcurrentZonesTest",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='concurrent_zones.json')
    #
    # uc4.run_use_case()
    #
    # uc5 = cuc_jade_5_module.ControllerUseCase5(controller_type="10",
    #                                            controller_firmware_version="0.171",
    #                                            fw_database_id="88",
    #                                            test_name="EventSwitchTest",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='event_decoder_test.json')
    #
    # uc5.run_use_case()
    # TODO fails step 9 line 408 program is watering when it should be paused
    # uc6 = cuc_jade_6_module.ControllerUseCase6(controller_type="10",
    #                                            controller_firmware_version="1.17",
    #                                            fw_database_id="88",
    #                                            test_name="MoistureDecoderTest",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='moisture_decoder_test.json')
    #
    # uc6.run_use_case()
    #
    # uc7 = cuc_jade_7_module.ControllerUseCase7(controller_type="10",
    #                                            controller_firmware_version="0.171",
    #                                            fw_database_id="88",
    #                                            test_name="TemperatureDecoderTest",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='temperature_decoder_test.json')
    #
    # uc7.run_use_case()
    #
    # uc8 = cuc_jade_8_module.ControllerUseCase8(controller_type="10",
    #                                            controller_firmware_version="0.171",
    #                                            fw_database_id="88",
    #                                            test_name="StartTimesTest",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='start_times_test.json')
    #
    # uc8.run_use_case()
    #
    # uc9 = cuc_jade_9_module.ControllerUseCase9(controller_type="10",
    #                                            controller_firmware_version="0.171",
    #                                            fw_database_id="88",
    #                                            test_name="MasterValvePumpTest",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='mv_pump_test.json')
    #
    # uc9.run_use_case()
    # TODO This test needs a complete rewrite
    # uc10 = cuc_jade_10_module.ControllerUseCase10(controller_type="10",
    #                                               controller_firmware_version="0.171",
    #                                               fw_database_id="88",
    #                                               test_name="OneTimeCalibrationTest",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='calibration_tests.json')
    #
    # uc10.run_use_case()
    #
    # uc11 = cuc_jade_11_module.ControllerUseCase11(controller_type="10",
    #                                               controller_firmware_version="0.171",
    #                                               fw_database_id="88",
    #                                               test_name="ReplacingDevicesTest",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='replacing_devices_test.json')
    #
    # uc11.run_use_case()
    #
    # uc12 = cuc_jade_12_module.ControllerUseCase12(controller_type="10",
    #                                               controller_firmware_version="0.171",
    #                                               fw_database_id="88",
    #                                               test_name="SoakCycles",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='soak_cycles_test.json')
    #
    # uc12.run_use_case()
    #
    # uc13 = cuc_jade_13_module.ControllerUseCase13(controller_type="10",
    #                                               controller_firmware_version="0.171",
    #                                               fw_database_id="88",
    #                                               test_name="WateringDayScheduleTest",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='watering_day_schedule_test.json')
    #
    # uc13.run_use_case()
    #
    # uc14 = cuc_jade_14_module.ControllerUseCase14(controller_type="10",
    #                                               controller_firmware_version="0.171",
    #                                               fw_database_id="88",
    #                                               test_name="UpperLimitTest",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='upper_limit_watering.json')
    #
    # uc14.run_use_case()
    #
    # uc15 = cuc_jade_15_module.ControllerUseCase15(controller_type="10",
    #                                               controller_firmware_version="0.171",
    #                                               fw_database_id="88",
    #                                               test_name="AlertRelayTest",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='alert_relay.json')
    #
    # uc15.run_use_case()
    #
    # uc16 = cuc_jade_16_module.ControllerUseCase16(controller_type="10",
    #                                               controller_firmware_version="0.171",
    #                                               fw_database_id="88",
    #                                               test_name="TimedZonesWithSoakCycles",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='timed_zones_with_soak_cycles.json')
    #
    # uc16.run_use_case()
    #
    # uc17 = cuc_jade_17_module.ControllerUseCase17(controller_type="10",
    #                                               controller_firmware_version="0.193",
    #                                               fw_database_id="88",
    #                                               test_name="CNUseCase17firmwareupdate",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='back_restore_firmware_updates.json')
    #
    # uc17.run_use_case()
    #
    # uc18 = cuc_jade_18_module.ControllerUseCase18(controller_type="10",
    #                                               controller_firmware_version="0.193",
    #                                               fw_database_id="88",
    #                                               test_name="CNUseCase18backuprestoreprogramming",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='back_restore_firmware_updates.json')
    #
    # uc18.run_use_case()
    #
    # ---------------------------------------------------------------------------------------------------------------- #
    #     3200 Tests                                                                                                   #
    # ---------------------------------------------------------------------------------------------------------------- #
    # test Passes
    # uc1 = cuc_3200_1_module.ControllerUseCase1(controller_type="32",
    #                                            controller_firmware_version=firm_version,
    #                                            cn_serial_number=cn_serial_num,
    #                                            fw_database_id="88",
    #                                            test_name="CN-UseCase1-RebootConfigVerify",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='update_cn.json')
    # uc1.run_use_case()
    # test Passes
    # uc2 = cuc_3200_2_module.ControllerUseCase2(controller_type="32",
    #                                            controller_firmware_version=firm_version,
    #                                            cn_serial_number=cn_serial_num,
    #                                            fw_database_id="88",
    #                                            test_name="EPATestConfiguration",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='EPA_test_configuration.json')
    #
    # uc2.run_use_case()
    # test Passes
    # uc3 = cuc_3200_3_module.ControllerUseCase3(controller_type="32",
    #                                            controller_firmware_version=firm_version,
    #                                            cn_serial_number=cn_serial_num,
    #                                            fw_database_id="88",
    #                                            test_name="SetMessages",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='set_messages.json')
    #
    # uc3.run_use_case()
    # test Passes
    # uc4 = cuc_3200_4_module.ControllerUseCase4(controller_type="32",
    #                                            controller_firmware_version=firm_version,
    #                                            cn_serial_number=cn_serial_num,
    #                                            fw_database_id="88",
    #                                            test_name="LowerLimitOneTimeCalibration",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='calibration_tests.json')
    #
    # uc4.run_use_case()
    # test Passes
    # uc5 = cuc_3200_5_module.ControllerUseCase5(controller_type="32",
    #                                            controller_firmware_version=firm_version,
    #                                            cn_serial_number=cn_serial_num,
    #                                            fw_database_id="88",
    #                                            test_name="POC_empty_conditions",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='empty_conditions.json')
    #
    # uc5.run_use_case()
    # test Passes
    # uc6 = cuc_3200_6_module.ControllerUseCase6(controller_type="32",
    #                                            controller_firmware_version=firm_version,
    #                                            cn_serial_number=cn_serial_num,
    #                                            fw_database_id="88",
    #                                            test_name="check cn serial number",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='check_cn_serial_number.json')
    #
    # uc6.run_use_case()
    # test Passes
    # uc7 = cuc_3200_7_module.ControllerUseCase7(controller_type="32",
    #                                            controller_firmware_version=firm_version,
    #                                            cn_serial_number=cn_serial_num,
    #                                            fw_database_id="88",
    #                                            test_name="multiple ssp with event decoders",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='multiple_ssp_with_event_decoders.json')
    #
    # uc7.run_use_case()
    # test Passes
    # uc8 = cuc_3200_8_module.ControllerUseCase8(controller_type="32",
    #                                            controller_firmware_version=firm_version,
    #                                            cn_serial_number=cn_serial_num,
    #                                            fw_database_id="88",
    #                                            test_name="multiple ssp with all devices",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='multiple_ssp_with_all_devices.json')
    #
    # uc8.run_use_case()
    # # TODO the controller slows down and this test now fails
    # uc9 = cuc_3200_9_module.ControllerUseCase9(controller_type="32",
    #                                            controller_firmware_version=firm_version,
    #                                            cn_serial_number=cn_serial_num,
    #                                            fw_database_id="88",
    #                                            test_name="memory_usage",
    #                                            user_configuration_instance=user_conf,
    #                                            json_configuration_file='ram_test_3200.json')
    #
    # uc9.run_use_case()
    # test Passes
    # uc10 = cuc_3200_10_module.ControllerUseCase10(controller_type="32",
    #                                               controller_firmware_version=firm_version,
    #                                               cn_serial_number=cn_serial_num,
    #                                               fw_database_id="88",
    #                                               test_name="CN-UseCase10-SeasonalAdjust",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='seasonal_adjust.json')
    # uc10.run_use_case()
    # # TODO this test doesnt pass
    # uc11 = cuc_3200_11_module.ControllerUseCase11(controller_type="32",
    #                                               controller_firmware_version=firm_version,
    #                                               cn_serial_number=cn_serial_num,
    #                                               fw_database_id="14",
    #                                               test_name="CNUseCase11firmwareupdate",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='back_restore_firmware_updates.json')
    # uc11.run_use_case()
    # # TODO this test doesnt pass
    # uc12 = cuc_3200_12_module.ControllerUseCase12(controller_type="32",
    #                                               controller_firmware_version=firm_version,
    #                                               cn_serial_number=cn_serial_num,
    #                                               fw_database_id="88",
    #                                               test_name="CNbackuprestoreprograming",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='back_restore_firmware_updates.json')
    #
    # uc12.run_use_case()
    # test Passes
    # uc13 = cuc_3200_13_module.ControllerUseCase13(controller_type="32",
    #                                               controller_firmware_version=firm_version,
    #                                               cn_serial_number=cn_serial_num,
    #                                               fw_database_id="88",
    #                                               test_name="CN-UseCase13-learnflowmultiplemainlines",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='flow_variance.json')
    # uc13.run_use_case()
    # test Passes
    # uc14 = cuc_3200_14_module.ControllerUseCase14(controller_type="32",
    #                                               controller_firmware_version=firm_version,
    #                                               cn_serial_number=cn_serial_num,
    #                                               fw_database_id="88",
    #                                               test_name="CN-UseCase14-lowflowvariance",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='flow_variance.json')
    # uc14.run_use_case()
    # test Passes
    # uc15 = cuc_3200_15_module.ControllerUseCase15(controller_type="32",
    #                                               controller_firmware_version=firm_version,
    #                                               cn_serial_number=cn_serial_num,
    #                                               fw_database_id="88",
    #                                               test_name="CN-UseCase15-basicProgramming",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='basic_programming.json')
    # uc15.run_use_case()
    # test Passes
    # uc16 = cuc_3200_16_module.ControllerUseCase16(controller_type="32",
    #                                               controller_firmware_version=firm_version,
    #                                               cn_serial_number=cn_serial_num,
    #                                               fw_database_id="88",
    #                                               test_name="CN-UseCase16-timedZones",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='timed_zones_with_soak_cycles.json')
    # uc16.run_use_case()
    # test Passes
    # uc17 = cuc_3200_17_module.ControllerUseCase17(controller_type="32",
    #                                               controller_firmware_version=firm_version,
    #                                               cn_serial_number=cn_serial_num,
    #                                               fw_database_id="88",
    #                                               test_name="CN-UseCase17-soak_cycles",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='timed_zones_with_soak_cycles.json')
    # uc17.run_use_case()
    # test Passes
    # uc18 = cuc_3200_18_module.ControllerUseCase18(controller_type="32",
    #                                               controller_firmware_version=firm_version,
    #                                               cn_serial_number=cn_serial_num,
    #                                               fw_database_id="88",
    #                                               test_name="CN-UseCase18-basic_design_flow",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='basic_flow_test.json')
    # uc18.run_use_case()
    # test Passes
    # uc19 = cuc_3200_19_module.ControllerUseCase19(controller_type="32",
    #                                               controller_firmware_version=firm_version,
    #                                               cn_serial_number=cn_serial_num,
    #                                               fw_database_id="88",
    #                                               test_name="CN-UseCase19-highflowvariance",
    #                                               user_configuration_instance=user_conf,
    #                                               json_configuration_file='flow_variance.json')
    # uc19.run_use_case()

    ###############################TESTS NEEDED########################################################################

    # TODO write a test that test manually running zones with POC disabled
    # TODO write a test that makes flow meters jump by 5X
    # TODO write a test that assigns zones to both substation and the 3200
    # TODO write a test that forces a 3200 to lose connection from a substation and than reconnect
    # uut = LoadLargeProgram(ser)
    # uut.load_large_program_3200()
    #
    # uut = MoistureWatering3200(ser)
    # uut.all_moisture_watering_tests_3200()
    #
    # ---------------------------------------------------------------------------------------------------------------- #
    #     BaseManager Tests                                                                                            #
    # ---------------------------------------------------------------------------------------------------------------- #
    # uc1 = uc1_module.BaseManagerUseCase1(controller_type="32",
    #                                      test_name="BM-UseCase1-LoginTest-3200",
    #                                      user_configuration_instance=user_conf,
    #                                      json_configuration_file='update_cn.json')
    # uc1.run_use_case()
    #
    # uc2 = uc2_module.BaseManagerUseCase2(controller_type="10",
    #                                      test_name="BM-UseCase2-MenuTabTest-1000",
    #                                      user_configuration_instance=user_conf,
    #                                      json_configuration_file='update_cn.json')
    # uc2.run_use_case()
    #
    # uc3 = uc3_module.BaseManagerUseCase3(controller_type="32",
    #                                      test_name="BM-UseCase3-MenuTabTest-3200",
    #                                      user_configuration_instance=user_conf,
    #                                      json_configuration_file='update_cn.json')
    # uc3.run_use_case()
    #
    # uc4 = uc4_module.BaseManagerUseCase4(controller_type="32",
    #                                      test_name="BM-UseCase4-AddressDevTest-3200",
    #                                      user_configuration_instance=user_conf,
    #                                      json_configuration_file='update_cn.json')
    # uc4.run_use_case()
    #
    # uc5 = uc5_module.BaseManagerUseCase5(controller_type="10",
    #                                      test_name="BM-UseCase5-AddressDevTest-1000",
    #                                      user_configuration_instance=user_conf,
    #                                      json_configuration_file='update_cn.json')
    # uc5.run_use_case()
    #
    # uc6 = uc6_module.BaseManagerUseCase6(controller_type="10",
    #                                      test_name="BM-UseCase6-AddressDevTest-1000",
    #                                      user_configuration_instance=user_conf,
    #                                      json_configuration_file='update_cn.json')
    # uc6.run_use_case()
    #
    # ---------------------------------------------------------------------------------------------------------------- #
    #      BacNet Tests                                                                                                #
    # ---------------------------------------------------------------------------------------------------------------- #
    #
    # uc1 = cuc_bacnet_1_module.ControllerUseCase1(controller_type="32",
    #                                              controller_firmware_version=firm_version,
    #                                              fw_database_id="88",
    #                                              test_name="TestAlarmCodes",
    #                                              user_configuration_instance=user_conf,
    #                                              json_configuration_file='bacnet_alarms.json')
    #
    # uc1.run_use_case()
    #
    # uc2 = cuc_bacnet_2_module.ControllerUseCase2(controller_type="32",
    #                                              controller_firmware_version=firm_version,
    #                                              fw_database_id="88",
    #                                              test_name="TestStatusCodes",
    #                                              user_configuration_instance=user_conf,
    #                                              json_configuration_file='status_codes.json')
    #
    #
    # uc2.run_use_case()
    
    # ---------------------------------------------------------------------------------------------------------------- #
    #      SubStation Tests                                                                                            #
    # ---------------------------------------------------------------------------------------------------------------- #
    
    # Test Passed
    # sbuc1 = cuc_substation_1_module.UseCase1TimedZones(
    #     controller_type='32',
    #     controller_firmware_version=firm_version,
    #     cn_serial_number=cn_serial_num,
    #     fw_database_id="88",
    #     test_name="SB UC 1 - Timed Zones",
    #     user_configuration_instance=user_conf,
    #     json_configuration_file='SB_timed_zones.json',
    #     substation_firmware_version=substation_fw_version,
    #     number_of_substations_to_use=1
    # )
    # sbuc1.run_use_case()
    
    # Test Passed
    # sbuc2 = cuc_substation_2_module.UseCase2SoakCycles(
    #     controller_type="32",
    #     controller_firmware_version=firm_version,
    #     cn_serial_number=cn_serial_num,
    #     fw_database_id="88",
    #     test_name="SB UC 2 - Soak Cycles",
    #     user_configuration_instance=user_conf,
    #     json_configuration_file='SB_soak_cycles.json',
    #     substation_firmware_version=substation_fw_version,
    #     number_of_substations_to_use=1
    # )
    # sbuc2.run_use_case()
    
    # Test Passed
    # sbuc3 = cuc_substation_3_module.UseCase3DisconnectReconnect(
    #     controller_type="32",
    #     controller_firmware_version=firm_version,
    #     cn_serial_number=cn_serial_num,
    #     fw_database_id="88",
    #     test_name="SB UC 3 - Disconn/Reconn",
    #     user_configuration_instance=user_conf,
    #     json_configuration_file='SB_timed_zones.json',
    #     substation_firmware_version=substation_fw_version,
    #     number_of_substations_to_use=1
    # )
    # sbuc3.run_use_case()
    
    # Test Passed
    # sbuc4 = cuc_substation_4_module.UseCase4BasicProgramming(
    #     controller_type="32",
    #     controller_firmware_version=firm_version,
    #     cn_serial_number=cn_serial_num,
    #     fw_database_id="88",
    #     test_name="SB UC 4-Basic Programming",
    #     user_configuration_instance=user_conf,
    #     json_configuration_file="SB_basic_programming.json",
    #     substation_firmware_version=substation_fw_version,
    #     number_of_substations_to_use=1
    # )
    # sbuc4.run_use_case()
    
    # Test Passed
    # sbuc5 = cuc_substation_5_module.UseCase5ReplaceDevices(
    #     controller_type="32",
    #     controller_firmware_version=firm_version,
    #     cn_serial_number=cn_serial_num,
    #     fw_database_id="88",
    #     test_name="SB UC 5 - Replace Devices",
    #     user_configuration_instance=user_conf,
    #     json_configuration_file="SB_replace_devices.json",
    #     substation_firmware_version=substation_fw_version,
    #     number_of_substations_to_use=1
    # )
    # sbuc5.run_use_case()
    
    # Test Passed
    # sbuc6 = cuc_substation_6_module.UseCase6ConcurrentZones(
    #     controller_type="32",
    #     controller_firmware_version=firm_version,
    #     cn_serial_number=cn_serial_num,
    #     fw_database_id="88",
    #     test_name="SB UC 6 - Concurrent Zones",
    #     user_configuration_instance=user_conf,
    #     json_configuration_file="SB_concurrent_zones.json",
    #     substation_firmware_version=substation_fw_version,
    #     number_of_substations_to_use=1
    # )
    # sbuc6.run_use_case()
    
    # Test Passed
    # sbuc7 = cuc_substation_7_module.UseCase7MoveDevices(
    #     controller_type="32",
    #     controller_firmware_version=firm_version,
    #     cn_serial_number=cn_serial_num,
    #     fw_database_id="88",
    #     test_name="SB UC 7 - Move Devices",
    #     user_configuration_instance=user_conf,
    #     json_configuration_file="SB_move_devices.json",
    #     substation_firmware_version=substation_fw_version,
    #     number_of_substations_to_use=1
    # )
    # sbuc7.run_use_case()
    
    # Test Passed
    # sbuc8 = cuc_substation_8_module.UseCase8LearnFlow(
    #     controller_type="32",
    #     controller_firmware_version=firm_version,
    #     cn_serial_number=cn_serial_num,
    #     fw_database_id="88",
    #     test_name="SB UC 8 - Learn Flow",
    #     user_configuration_instance=user_conf,
    #     json_configuration_file="SB_learn_flow.json",
    #     substation_firmware_version=substation_fw_version,
    #     number_of_substations_to_use=1
    # )
    # sbuc8.run_use_case()
    
    # Test Passed
    # sbuc9 = cuc_substation_9_module.UseCase9Messages(
    #     controller_type="32",
    #     controller_firmware_version=firm_version,
    #     cn_serial_number=cn_serial_num,
    #     fw_database_id="88",
    #     test_name="SB UC 9 - Messages",
    #     user_configuration_instance=user_conf,
    #     json_configuration_file="SB_messages.json",
    #     substation_firmware_version=substation_fw_version,
    #     number_of_substations_to_use=1
    # )
    # sbuc9.run_use_case()
    
    # Test Passed
    # TODO: THIS USE CASE 10 REQUIRES A BASEMANAGER CONNECTION & USB PLUGGED INTO 3200.
    # TODO: TO NOT TEST USING BASEMANAGER, COMMENT OUT LINES 152, 153, 154
    # TODO: TO NOT TEST USING USB, COMMENT OUT LINES 157, 158, 159
    # sbuc10 = cuc_substation_10_module.UseCase10BackupRestore(
    #     controller_type="32",
    #     controller_firmware_version=firm_version,
    #     cn_serial_number=cn_serial_num,
    #     fw_database_id="88",
    #     test_name="SB UC 10 - Backup Restore",
    #     user_configuration_instance=user_conf,
    #     json_configuration_file="SB_backup_restore.json",
    #     substation_firmware_version=substation_fw_version,
    #     number_of_substations_to_use=1
    # )
    # sbuc10.run_use_case()

    print("######################### ----YOU ARE A WINNER WINNER CHICKEN DINNER---- ##################################")

    exit()

if __name__ == "__main__":
    main(sys.argv[1:])
