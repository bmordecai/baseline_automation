import re

_VALUE_DELIMITER = r';'
_KEYVALUE_DELIMITER = r'\,'
_KEYVALUE_ASSIGNMENT = r'='
_ESCAPE = r'\\'


#############################################
def split_with_escape(str_to_split, delimiter, maxsplit=0):
#############################################
    """
    This method does the same as a normal split, but uses a "negative lookbehind" regular expression
    to only split if the delimiter is *not* preceded by the _ESCAPE character
    (for example, passing in the "^" would split on all "^" characters, but would not split on a "\^" (assuming
    "\" is the _ESCAPE character)
    Errors or missing arguments result in the return of an empty list
    """
    if (str_to_split is None) or (delimiter is None):
        return []
    foo = r'(?<!' + _ESCAPE + r')' + delimiter
    regex = re.compile(foo)
    to_return = re.split(regex, str_to_split, maxsplit)
    return to_return


##############################
class Value:
##############################
    """
    A Value is zero or more strings
    """
    _value_list = []

    @property
    def value_list(self):
        return self._value_list

    ##################
    def __init__(self, string_in=None):
    ##################
        self._value_list = []
        self.parse(string_in)

    ##################
    def __eq__(self, other):
    ##################
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    ##################
    def __ne__(self, other):
    ##################
        return not self.__eq__(other)

    def showme(self):
        for v in self.value_list:
            print(" \t\t\t\tvalue is " + str(v))

    def parse(self, string_in):
        """
        Parse something of the form:
            string;string;string...
        there are zero or more semicolon-seperated strings
        """
        value_list = split_with_escape(string_in, _VALUE_DELIMITER)
        for value in value_list:
            # Don't allow a null value
            if not(value is None or value == ""):
                # Unescape any escaped characters in a value string
                value = re.sub(r'[\\]', '', value)
                self._value_list.append(value)

    #####################
    def get_value_string(self):
    #####################
        """
        Return the value list re-serialized with the value delimiter in between.
        None if there aren't any values in the list
        """
        toreturn = None
        for value in self.value_list:
            if toreturn is None:
                toreturn = value
            else:
                toreturn = toreturn + _VALUE_DELIMITER + value
        return toreturn


##############################
class KeyValue:
##############################
    """
    A KeyValue has a Key string and a Value
    """

    _key = None
    _value = None

    @property
    def key(self):
        return self._key

    @property
    def value(self):
        return self._value

    ##################
    def __init__(self, string_in=None):
    ##################
        self._key = None
        self._value = None
        self.parse(string_in)

    ##################
    def __eq__(self, other):
    ##################
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    ##################
    def __ne__(self, other):
    ##################
        return not self.__eq__(other)

    ##################
    def showme(self, fromstring=None):
    ##################
        if fromstring is None:
            fromstr = ""
        else:
            fromstr = "From: \t" + fromstring
        print("KeyValue " + fromstr)
        if self.key is not None:
            print("\t\t--------- KeyValue key:" + self.key)
            self._value.showme()

    ##################
    def parse(self, string_in):
    ##################
        # Only split on the first _KEYVALUE_ASSIGNMENT.   This allows (ack!) a legal construct like
        # ST=100=200=400=500, which have a key of ST and a value of 100=200=300=400=500
        fixedkv = split_with_escape(string_in, _KEYVALUE_ASSIGNMENT, 1)
        if len(fixedkv) != 2:
            self._key = None
            self._value = None
        else:
            self._key = fixedkv[0]
            self._value = Value(fixedkv[1])

    ##################
    def get_value_string(self):
    ##################
        return self.value.get_value_string()


##############################
class KeyValues:
##############################
    """"
    Zero or more KeyValues, separated by the KeyValue delimiter ( , )
    Key=Value , Key=Value
    """
    _keyvalue_list = []

    @property
    def keyvalue_list(self):
        return self._keyvalue_list

    ##################
    def __init__(self, string_in=None):
    ##################
        self._keyvalue_list = []
        self.parse(string_in)

    ##################
    def __eq__(self, other):
    ##################
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    ##################
    def __ne__(self, other):
    ##################
        return not self.__eq__(other)

    ##################
    def showme(self, from_string=None):
    ##################
        for singlekv in self.keyvalue_list:
            singlekv.showme(from_string)

    ##################
    def parse(self, string_in):
    ##################
        """
        Take a comma-delimited string of key-value pairs and parse it into a list of KeyValue objects
        """
        self._keyvalue_list = []
        a_keyvalue_str_list = split_with_escape(string_in, _KEYVALUE_DELIMITER)
        for a_keyvalue_str in a_keyvalue_str_list:
            self._keyvalue_list.append(KeyValue(a_keyvalue_str))

    ################
    def get_key_values(self):
    ################
        """
        Just return a list, maybe empty, of all the KeyValues
        """
        return self.keyvalue_list

    ################
    def get_key_value_by_key(self, key):
    ################
        """
        Find the KeyValue matching the key.   If found, return it, otherwise return None
        # :rtype: KeyValue
        """
        for keyvalue in self.keyvalue_list:
            if keyvalue.key == key:
                return keyvalue
        return None

    ################
    def replace_value_string_by_key(self, key, new_value):
    ################
        """
        Find the KeyValue matching the Key.   If found, change it's Value to the new Value, and return the
        new KeyValue.
        Otherwise, return None
        """
        newvalue = Value(str(new_value))
        keyvalue = self.get_key_value_by_key(key)
        if keyvalue is not None:
            keyvalue.value = newvalue
            return keyvalue
        else:
            return None

    ################
    def get_value_string_by_key(self, key):
    ################
        """
        Find the KeyValue matching the key.  If found, return the character string equivalent of the value.
        Otherwise, return None
        """
        keyvalue = self.get_key_value_by_key(key)
        if keyvalue is not None:
            return keyvalue.get_value_string()
        else:
            return None
