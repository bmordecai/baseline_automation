import sys

import common.configuration as test_conf
import common.user_configuration as user_conf
import common.page_factory as factory
from common import helper_methods
from common.imports import opcodes

# import log_handler for logging functionality
from common.logging_handler import log_handler

__author__ = 'baseline'


class BaseManagerUseCase4(object):
    """
    Purpose:
        This test verifies the following BaseManager functionality for the v16 3200 and it's devices: \n
        - Search for each device
        - Assign each device to an address/serial number and give descriptions
        - Verify that the search and address holds after save

    Coverage Area: \n
        - Devices Tabs
            - Zones
                - Search and Assign
                - Give each zone a description
                - Save changes and verify
            - Moisture Sensors
                - Search and Assign
                - Give each moisture sensor a description
                - Save changes and verify
            - Flow Meters
                - Search and Assign
                - Give each flow meter a description
                - Save changes and verify
            - Temperature Sensors
                - Search and Assign
                - Give each temperature sensor a description
                - Save changes and verify
            - Master Valves
                - Search and Assign
                - Give each master valve a description
                - Save changes and verify
            - Event Switches
                - Search and Assign
                - Give each event switch a description
                - Save changes and verify
            - Pumps
                - Search and Assign
                - Give each event switch a description
                - Save changes and verify
            - Pressure Sensors
                - Search and Assign
                - Give each event switch a description
                - Save changes and verify

    Areas not Covered: \n
        - Editing devices
        - Testing devices
        - Manually Start/Stop devices

    Bugs covered by test:
        (1) BM-1893 (unable to address & change descriptions in device assignment views)
    """
    # ~~~~ Local Variables ~~~~ #

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = test_conf.Configuration(test_name=test_name,
                                              user_conf_file=user_configuration_instance,
                                              data_json_file=json_configuration_file)
        
        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        The following steps are done on all browser specified in the user configuration: \n

        Step 1:
            - Set controller to real-time
            - Initialize browser web page objects.

        Step 2:
            - initialize & open browser

        step 3:
            - login & verify successful logged in

        Step 4:
            - select site
            - select controller
            - verify QuickView tab opens

        Step 5:
            - Select Zones Tab
            - Search, address and give a description to all loaded Zones
            - Save and verify all changes held

        Step 6:
            - Select Temperature Sensors Tab
            - Search, address and give a description to all loaded Moisture Sensors
            - Save and verify all changes held

        Step 7:
            - Select Flow Meter Tab
            - Search, address and give a description to all loaded Flow Meter
            - Save and verify all changes held

        step 8:
            - Select Master Valve Tab
            - Search, address and give a description to all loaded Master Valve
            - Save and verify all changes held

        step 9:
            - Select Temperature Sensors Tab
            - Search, address and give a description to all loaded Temperature Sensors
            - Save and verify all changes held

        step 10:
            - Select Event Switches Tab
            - Search, address and give a description to all loaded Event Switches
            - Save and verify all changes held

        step 11:
            - Select Pumps Tab
            - Search, address and give a description to all loaded Pumps
            - Save and verify all changes held

        step 12:
            - Select Pressure Sensors Tab
            - Search, address and give a description to all loaded Pressure Sensors
            - Save and verify all changes held
        """
        try:
            # browsers_for_test = ["chrome", "firefox"]
            browsers_for_test = ['chrome']
            for run_number, browser in enumerate(browsers_for_test):
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test(connect_to_basemanager=True, address_devices=False, web_tests=True)
                self.step_1()
                self.step_2(_browser=browser)
                self.step_3()
                self.step_4()
                self.step_5()
                self.step_6()
                self.step_7()
                self.step_8()
                self.step_9()
                self.step_10()
                self.step_11()
                self.step_12()

                # Close the first webdriver to start the second web driver
                if run_number == 0 and len(browsers_for_test) > 1:
                    self.config.resource_handler.web_driver.web_driver.quit()

        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        else:
            helper_methods.print_test_passed(self.config.test_name)

        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)

    #################################
    def step_1(self):
        """
        Initialize the controller:
        1. Clear programming
        2. Load devices
        3. Do Search for all devices
        """
        helper_methods.print_method_name()
        try:
            # Set sim mode to off so that BM packets get sent up to the client.
            self.config.BaseStation3200[1].set_sim_mode_to_off()
            
            # # Get web driver instance from config
            # self.web_driver = self.config.resource_handler.web_driver
            #
            # # Create page instances to use for Client use
            # self.config.BaseManager.main_page_main_menu = factory.get_main_menu_object(base_page=self.config.BaseManager)
            #
            # # Main Tab Page Objects
            # self.config.BaseManager.login_page = factory.get_basemanager_login_page_object(base_page=self.config.BaseManager)
            # self.config.BaseManager.main_page = factory.get_main_page_object(base_page=self.config.BaseManager)
            # self.config.BaseManager.quick_view_page = factory.get_quick_view_page_object(base_page=self.config.BaseManager)
            #
            # # Device page Objects
            # self.config.BaseManager.zones_tab_page = factory.get_zones_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.moisture_sensor_tab_page = factory.get_moisture_sensors_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.master_valve_tab_page = factory.get_master_valves_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.flow_sensor_tab_page = factory.get_flow_meter_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.temperature_sensor_tab_page = factory.get_temperature_sensors_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.event_switch_tab_page = factory.get_event_switch_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.pump_tab_page = factory.get_pumps_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.pressure_sensor_tab_page = factory.get_pressure_sensors_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg=""
            )

    #################################
    def step_2(self, _browser):
        """
        Initializes the web driver to the correct browser executor
        """
        helper_methods.print_method_name()
        try:
            pass
            # Open browser
            # self.config.resource_handler.web_driver.open(_browser)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Opened Web Browser"
            )

    #################################
    def step_3(self):
        """
        """
        helper_methods.print_method_name()
        try:
            # enter the user name and password selects login button
            self.config.BaseManager.login_page.enter_login_info()
            self.config.BaseManager.login_page.click_login_button()
            self.config.BaseManager.main_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Logged In"
            )

    #################################
    def step_4(self):
        """
        - Selects a site and controller
        - Verifies QuickView Tab is opened
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page_main_menu.navigate_to_my_controller(
                mac_address=self.config.BaseStation3200[1].mac)

            # selecting a controller returns you to the quick view tab
            self.config.BaseManager.quick_view_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected Site and Controller"
            )

    #################################
    def step_5(self):
        """
        - Selects Zones Tab
        - Do search for zones
        - Assign each zone to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.zone)
            self.config.BaseManager.zones_tab_page.verify_is_open()
            self.config.BaseManager.zones_tab_page.select_assign_bicoders()

            for each_zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseManager.zones_tab_page.set_assigned_serial_number(address=each_zone_address)
                self.config.BaseManager.zones_tab_page.set_description(address=each_zone_address)

            self.config.BaseManager.zones_tab_page.save_device_assignment()

            for each_zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseManager.zones_tab_page.verify_address(address=each_zone_address)
                self.config.BaseManager.zones_tab_page.verify_description(address=each_zone_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Searched, Addressed and Described All Zones and Verified changes"
            )

    #################################
    def step_6(self):
        """
        - Selects Moisture Sensor Tab
        - Do search for Moisture Sensor
        - Assign each Moisture Sensor to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.moisture_sensor)
            self.config.BaseManager.moisture_sensor_tab_page.verify_is_open()
            self.config.BaseManager.moisture_sensor_tab_page.select_assign_bicoders()

            for each_ms_address in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseManager.moisture_sensor_tab_page.set_assigned_serial_number(address=each_ms_address)
                self.config.BaseManager.moisture_sensor_tab_page.set_description(address=each_ms_address)

            self.config.BaseManager.moisture_sensor_tab_page.save_device_assignment()

            for each_ms_address in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseManager.moisture_sensor_tab_page.verify_address(address=each_ms_address)
                self.config.BaseManager.moisture_sensor_tab_page.verify_description(address=each_ms_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Searched, Addressed and Described All Moisture Sensors and Verified changes"
            )

    #################################
    def step_7(self):
        """
        - Selects Flow Meter Tab
        - Do search for Flow Meter
        - Assign each Flow Meter to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.flow_meter)
            self.config.BaseManager.flow_sensor_tab_page.verify_is_open()
            self.config.BaseManager.flow_sensor_tab_page.select_assign_bicoders()

            for each_fm_address in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseManager.flow_sensor_tab_page.set_assigned_serial_number(address=each_fm_address)
                self.config.BaseManager.flow_sensor_tab_page.set_description(address=each_fm_address)

            self.config.BaseManager.flow_sensor_tab_page.save_device_assignment()

            for each_fm_address in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseManager.flow_sensor_tab_page.verify_address(address=each_fm_address)
                self.config.BaseManager.flow_sensor_tab_page.verify_description(address=each_fm_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Searched, Addressed and Described All Flow Meters and Verified changes"
            )

    #################################
    def step_8(self):
        """
        - Selects Master Valve Tab
        - Do search for Master Valve
        - Assign each Master Valve to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.master_valve)
            self.config.BaseManager.master_valve_tab_page.verify_is_open()
            self.config.BaseManager.master_valve_tab_page.select_assign_bicoders()

            for each_mv_address in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseManager.master_valve_tab_page.set_assigned_serial_number(address=each_mv_address)
                self.config.BaseManager.master_valve_tab_page.set_description(address=each_mv_address)

            self.config.BaseManager.master_valve_tab_page.save_device_assignment()

            for each_mv_address in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseManager.master_valve_tab_page.verify_address(address=each_mv_address)
                self.config.BaseManager.master_valve_tab_page.verify_description(address=each_mv_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Searched, Addressed and Described All Master Valves and Verified changes"
            )

    #################################
    def step_9(self):
        """
        - Selects Temperature Sensor Tab
        - Do search for Temperature Sensor
        - Assign each Temperature Sensor to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.temperature_sensor)
            self.config.BaseManager.temperature_sensor_tab_page.verify_is_open()
            self.config.BaseManager.temperature_sensor_tab_page.select_assign_bicoders()

            for each_ts_address in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseManager.temperature_sensor_tab_page.set_assigned_serial_number(address=each_ts_address)
                self.config.BaseManager.temperature_sensor_tab_page.set_description(address=each_ts_address)

            self.config.BaseManager.temperature_sensor_tab_page.save_device_assignment()

            for each_ts_address in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseManager.temperature_sensor_tab_page.verify_address(address=each_ts_address)
                self.config.BaseManager.temperature_sensor_tab_page.verify_description(address=each_ts_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Searched, Addressed and Described All Temperature Sensors and Verified changes"
            )

    #################################
    def step_10(self):
        """
        - Selects Event Switch Tab
        - Do search for Event Switch
        - Assign each Event Switch to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.event_switch)
            self.config.BaseManager.event_switch_tab_page.verify_is_open()
            self.config.BaseManager.event_switch_tab_page.select_assign_bicoders()

            for each_sw_address in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseManager.event_switch_tab_page.set_assigned_serial_number(address=each_sw_address)
                self.config.BaseManager.event_switch_tab_page.set_description(address=each_sw_address)

            self.config.BaseManager.event_switch_tab_page.save_device_assignment()

            for each_sw_address in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseManager.event_switch_tab_page.verify_address(address=each_sw_address)
                self.config.BaseManager.event_switch_tab_page.verify_description(address=each_sw_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Searched, Addressed and Described All Event Switches and Verified changes"
            )

    #################################
    def step_11(self):
        """
        - Selects Pumps Tab
        - Do search for Pumps
        - Assign each Pump to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.pump)
            self.config.BaseManager.pump_tab_page.verify_is_open()
            self.config.BaseManager.pump_tab_page.select_assign_bicoders()
        
            for each_pm_address in sorted(self.config.BaseStation3200[1].pumps.keys()):
                self.config.BaseManager.pump_tab_page.set_assigned_serial_number(address=each_pm_address)
                self.config.BaseManager.pump_tab_page.set_description(address=each_pm_address)
        
            self.config.BaseManager.pump_tab_page.save_device_assignment()
        
            for each_pm_address in sorted(self.config.BaseStation3200[1].pumps.keys()):
                self.config.BaseManager.pump_tab_page.verify_address(address=each_pm_address)
                self.config.BaseManager.pump_tab_page.verify_description(address=each_pm_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Searched, Addressed and Described All Pumps and Verified changes"
            )

    #################################
    def step_12(self):
        """
        - Selects Pressure Sensors Tab
        - Do search for Pressure Sensors
        - Assign each Pressure Sensor to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.pressure_sensor)
            self.config.BaseManager.pressure_sensor_tab_page.verify_is_open()
            self.config.BaseManager.pressure_sensor_tab_page.select_assign_bicoders()

            for each_ps_address in sorted(self.config.BaseStation3200[1].pressure_sensors.keys()):
                self.config.BaseManager.pressure_sensor_tab_page.set_assigned_serial_number(address=each_ps_address)
                self.config.BaseManager.pressure_sensor_tab_page.set_description(address=each_ps_address)

            self.config.BaseManager.pressure_sensor_tab_page.save_device_assignment()

            for each_ps_address in sorted(self.config.BaseStation3200[1].pressure_sensors.keys()):
                self.config.BaseManager.pressure_sensor_tab_page.verify_address(address=each_ps_address)
                self.config.BaseManager.pressure_sensor_tab_page.verify_description(address=each_ps_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Searched, Addressed and Described All Pressure Sensors and Verified changes"
            )
