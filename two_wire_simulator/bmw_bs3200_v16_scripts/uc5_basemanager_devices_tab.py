import sys

import common.configuration as test_conf
import common.user_configuration as user_conf
import common.page_factory as factory
from common import helper_methods
from common.imports import opcodes

# import log_handler for logging functionality
from common.logging_handler import log_handler
from time import sleep

__author__ = 'baseline'


class BaseManagerUseCase5(object):
    """

    Test name:
        - Basemanager Test Devices Tab

    # TODO this is currently failing due to a bug on the 3200. See Jira Bug ZZ-1949. Once the bug is fixed, this test should pass.

   User Story: \n
    1)  As a user I want to be able to to be able to make an edit to a device and have the changes reflected
    in the Devices tab and in Quick View

      Coverage and Objectives:
        This test verifies the following BaseManager functionality for the v16 3200 and it's devices: \n
        - Verify that when a device is edited, the attributes get saved in Basemanager.
        - Verify that for Moisture Sensors, Flow Meters, Temperature Sensors, Event Switches, Weather Station
          the changes to the description show up in Quick View
        - These devices include:
            - Zones
            - Moisture Sensors
            - Flow Meters
            - Temperature Sensors
            - Master Valves
            - Event Switches
            - Pumps
            - Pressure Sensors

    Areas not Covered: \n
        - Testing devices
        - Manually Start/Stop devices
        - Actual packets flying up and down. We verify this indirectly by verifying the values but we don't capture
            any packets.


    Test Overview:
        - Setup all devices we want to verify in the 'test device' tab in Mobile Access:
            - Zones
            - Master Valves
            - Moisture Sensors
            - Temperature Sensors
            - Pressure Sensors
            - Event Switches
            - Flow Meters
        - Log into Base Manager
        - Browse to the controller I am using
        - Go to the test devices menu
        - Make changes to attributes on each device.
        - Wait 10 seconds for GetXML to be called
        - Verify the changes in the devices list view
        - Verify the changes in QuickView for the following devices:
             - Moisture Sensors
             - Flow Meters
             - Temperature Sensors
             - Event Switches
             - Pumps
             - Pressure Sensors

    Test Configuration setup: \n
        - Zones
            - 1 (TSD0001)
            - 200 (TSD0002)
        - Master Valves
            - 1 (TMV0001)
            - 8 (TMV0002)
        - Pumps
            - 1 (TPR0001)
            - 8 (TPR0002)
        - Moisture Sensors
            - 1 (SB00001)
            - 8 (SB00002)
        - Temperature Sensors
            - 1 (TAT0001)
            - 8 (TAT0002)
        - Pressure Sensors
            - 1 (PSF0001)
            - 8 (PSF0002)
        - Event Switches
            - 1 (TPD0001)
            - 8 (TPD0002)
        - Flow Meters
            - 1 (TWF0001)
            - 8 (TWF0002)

    Bugs covered by test:
        (1) BM-2143 (Recursive Get XML calls causing QuickView to display "Unkown" / "Disabled" Status
    """
    # ~~~~ Local Variables ~~~~ #

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = test_conf.Configuration(test_name=test_name,
                                              user_conf_file=user_configuration_instance,
                                              data_json_file=json_configuration_file)
        
        self.run_use_case()

    def run_use_case(self):
        """
        This method initializes the test and runs the steps. \n
        """
        try:
            for run_number, browser in enumerate(["chrome"]):  # Firefox taken out for now
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test(connect_to_basemanager=True, web_tests=True)

                # get list of all the steps by function name in the use case
                method_list = [func for func in dir(self) if
                               callable(getattr(self, func)) and func.startswith('step')]

                # sort list in numerical order of numbers in steps step names must be 'step_X'
                sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                # run each step_1,2,3 esc.
                for method in sorted_new_list:
                    getattr(self, method)()

                # Close the first webdriver to start the second web driver
                if run_number == 0:
                    self.config.resource_handler.web_driver.web_driver.quit()

        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise
        else:
            helper_methods.print_test_passed(self.config.test_name)

        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)

    #################################
    def step_1(self):
        """
        Login into BaseManager
        """
        helper_methods.print_method_name()
        try:
            # enter the user name and password selects login button
            self.config.BaseManager.login_page.enter_login_info()
            self.config.BaseManager.login_page.click_login_button()
            self.config.BaseManager.main_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Logged In"
            )

    #################################
    def step_2(self):
        """
        - Selects a site and controller based on the user configuration
        - Verifies QuickView Tab is opened
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            # Navigate to the controller. This will take into account whether or not the user is a super user.
            self.config.BaseManager.main_page_main_menu.navigate_to_my_controller(
                mac_address=self.config.BaseStation3200[1].mac
            )

            # selecting a controller returns you to the quick view tab
            self.config.BaseManager.quick_view_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected Site and Controller"
            )

    #################################
    def step_3(self):
        """
        - Selects Master Valve Tab
        - Select edit button for a specific Master Valve
        - Change description on the Master Valve
        - Save and verify changes are saved by checking the description in list view
        - Verifies the Master Valve in Quick View
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            # make edit on the devices tab
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.master_valve)
            self.config.BaseManager.master_valve_tab_page.verify_is_open()
            self.config.BaseManager.master_valve_tab_page.select_device_view_button()
            self.config.BaseManager.master_valve_tab_page.select_basemanager_master_valve_edit_button(_address=1)
            self.config.BaseManager.master_valve_tab_page.set_device_description(description="Test Master Valve 1", address=1)
            self.config.BaseManager.master_valve_tab_page.save_device_description_edit()

            # Verify the edits on the device tab
            for each_mv_address in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseManager.master_valve_tab_page.verify_address(address=each_mv_address)
                self.config.BaseManager.master_valve_tab_page.verify_description(address=each_mv_address)
                self.config.BaseStation3200[1].master_valves[each_mv_address].get_data()
                self.config.BaseStation3200[1].master_valves[each_mv_address].verify_description()

            # verify the edits in quick view
            self.config.BaseManager.master_valve_tab_page.select_quick_view_tab()
            self.config.BaseManager.quick_view_page.verify_open()

            for each_mv_address in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseManager.quick_view_page.verify_qv_master_valve_description(_master_valve_address=each_mv_address)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Changed Description on a Master Valve and verified all changes"
            )

    #################################
    def step_4(self):
        """
        - Selects Flow Meter Tab
        - Select a Flow Meter
        - Change the description on the Flow Meter
        - Save and verify changes are saved in the List View
        - Verify the changes on the Quick View tab
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            # make edit on the devices tab
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.flow_meter)
            self.config.BaseManager.flow_sensor_tab_page.verify_is_open()
            self.config.BaseManager.flow_sensor_tab_page.select_device_view_button()
            self.config.BaseManager.flow_sensor_tab_page.select_basemanager_flow_meter_edit_button(_address=1)
            self.config.BaseManager.flow_sensor_tab_page.set_device_description(description="Test Flow Meter 1", address=1)
            self.config.BaseManager.flow_sensor_tab_page.save_device_description_edit()

            # Verify the edits on the device tab
            for each_fm_address in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseManager.flow_sensor_tab_page.verify_address(address=each_fm_address)
                self.config.BaseManager.flow_sensor_tab_page.verify_description(address=each_fm_address)
                self.config.BaseStation3200[1].master_valves[each_fm_address].get_data()
                self.config.BaseStation3200[1].master_valves[each_fm_address].verify_description()

            # verify the edits in quick view
            self.config.BaseManager.flow_sensor_tab_page.select_quick_view_tab()
            self.config.BaseManager.quick_view_page.verify_open()

            for each_fm_address in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseManager.quick_view_page.verify_qv_flow_meter_description(_flow_meter_address=each_fm_address)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Searched, Addressed and Described All Flow Meters and Verified changes"
            )

    #################################
    def step_5(self):
        """
        - Selects Moisture Sensor Tab
        - Select edit button for a specific Moisture Sensor
        - Change description on the Moisture Sensor
        - Save and verify changes are saved by checking the description in list view.
        - Verify the changes on the Quick View tab
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            # make edit on the devices tab
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.moisture_sensor)
            self.config.BaseManager.moisture_sensor_tab_page.verify_is_open()
            self.config.BaseManager.moisture_sensor_tab_page.select_device_view_button()
            self.config.BaseManager.moisture_sensor_tab_page.select_basemanager_moisture_sensor_edit_button(_address=1)
            self.config.BaseManager.moisture_sensor_tab_page.set_device_description(description="Test Moisture Sensor 1", address=1)
            self.config.BaseManager.moisture_sensor_tab_page.save_device_description_edit()

            # Verify the edits on the device tab
            for each_ms_address in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseManager.moisture_sensor_tab_page.verify_address(address=each_ms_address)
                self.config.BaseManager.moisture_sensor_tab_page.verify_description(address=each_ms_address)
                self.config.BaseStation3200[1].moisture_sensors[each_ms_address].get_data()
                self.config.BaseStation3200[1].moisture_sensors[each_ms_address].verify_description()

            # verify the edit in quick view
            self.config.BaseManager.moisture_sensor_tab_page.select_quick_view_tab()
            self.config.BaseManager.quick_view_page.verify_open()
            for each_ms_address in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseManager.quick_view_page.verify_qv_moisture_sensor_description(_moisture_sensor_address=each_ms_address)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Changed Description on a Moisutre Sensor and verified all changes"
            )

    #################################
    def step_6(self):
        """
        - Selects Temperature Sensor Tab
        - Select edit button for the Temperature Sensor
        - Change description on the Temperature Sensor
        - Save and verify changes are saved by checking the description in list view.
        - Verify the changes on the Quick View tab
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            # make edit on the devices tab
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.temperature_sensor)
            self.config.BaseManager.temperature_sensor_tab_page.verify_is_open()
            self.config.BaseManager.temperature_sensor_tab_page.select_device_view_button()
            self.config.BaseManager.temperature_sensor_tab_page.select_basemanager_temperature_sensor_edit_button(_address=1)
            self.config.BaseManager.temperature_sensor_tab_page.set_device_description(description="Test Temperature Sensor 1", address=1)
            self.config.BaseManager.temperature_sensor_tab_page.save_device_description_edit()

            # Verify the edits on the device tab
            for each_ts_address in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseManager.temperature_sensor_tab_page.verify_address(address=each_ts_address)
                self.config.BaseManager.temperature_sensor_tab_page.verify_description(address=each_ts_address)
                self.config.BaseStation3200[1].temperature_sensors[each_ts_address].get_data()
                self.config.BaseStation3200[1].temperature_sensors[each_ts_address].verify_description()

            # verify the edit in quick view
            self.config.BaseManager.temperature_sensor_tab_page.select_quick_view_tab()
            self.config.BaseManager.quick_view_page.verify_open()
            for each_ts_address in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseManager.quick_view_page.verify_qv_temperature_sensor_description(_temperature_sensor_address=each_ts_address)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Changed Description on a Moisture Sensor and verified all changes"
            )

    #################################
    def step_7(self):
        """
        - Selects Event Switch Tab
        - Select edit button for the Event Switch
        - Change description on the Event Switch
        - Save and verify changes are saved by checking the description in list view.
        - Verify the changes on the Quick View tab
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            # make edit on the devices tab
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.event_switch)
            self.config.BaseManager.event_switch_tab_page.verify_is_open()
            self.config.BaseManager.event_switch_tab_page.select_device_view_button()
            self.config.BaseManager.event_switch_tab_page.select_basemanager_event_switch_edit_button(_address=1)
            self.config.BaseManager.event_switch_tab_page.set_device_description(description="Test Event Switch 1", address=1)
            self.config.BaseManager.event_switch_tab_page.save_device_description_edit()

            # Verify the edits on the device tab
            for each_sw_address in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseManager.event_switch_tab_page.verify_address(address=each_sw_address)
                self.config.BaseManager.event_switch_tab_page.verify_description(address=each_sw_address)
                self.config.BaseStation3200[1].event_switches[each_sw_address].get_data()
                self.config.BaseStation3200[1].event_switches[each_sw_address].verify_description()

            # verify the edit in quick view
            self.config.BaseManager.event_switch_tab_page.select_quick_view_tab()
            self.config.BaseManager.quick_view_page.verify_open()
            for each_sw_address in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseManager.quick_view_page.verify_qv_event_switch_description(_event_switch_address=each_sw_address)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Changed Description on an Event Switch and verified all changes"
            )

    #################################
    def step_8(self):
        """
        - Selects Pumps Tab
        - Select edit button for the Pump
        - Change description on the Pump
        - Save and verify changes are saved by checking the description in list view.
        - Verifies the pum description in the Zones tab
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            # make edit on the devices tab
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.pump)
            self.config.BaseManager.pump_tab_page.verify_is_open()
            self.config.BaseManager.pump_tab_page.select_device_view_button()
            self.config.BaseManager.pump_tab_page.select_basemanager_pump_edit_button(_address=1)
            self.config.BaseManager.pump_tab_page.set_device_description(description="Test Pump 1", address=1)
            self.config.BaseManager.pump_tab_page.save_device_description_edit()

            # Verify the edits on the device tab
            for each_pm_address in sorted(self.config.BaseStation3200[1].pumps.keys()):
                self.config.BaseManager.pump_tab_page.verify_address(address=each_pm_address)
                self.config.BaseManager.pump_tab_page.verify_description(address=each_pm_address)
                self.config.BaseStation3200[1].pumps[each_pm_address].get_data()
                self.config.BaseStation3200[1].pumps[each_pm_address].verify_description()

            # verify the edit in quick view
            self.config.BaseManager.pump_tab_page.select_quick_view_tab()
            self.config.BaseManager.quick_view_page.verify_open()
            for each_pm_address in sorted(self.config.BaseStation3200[1].pumps.keys()):
                self.config.BaseManager.quick_view_page.verify_qv_pump_description(_pump_address=each_pm_address)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                     "\tThe Exception thrown was {2}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Changed Description on Pump and verified all changes"
            )


    ###############################
    def step_9(self):
        """
        - Selects Pressure Sensor Tab
        - Select edit button for the Pressure Sensor
        - Change description on the Pressure Sensor
        - Save and verify changes are saved by checking the description in list view.
        - Verify the changes on the Quick View tab
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            # make edit on the devices tab
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.pressure_sensor)
            self.config.BaseManager.pressure_sensor_tab_page.verify_is_open()
            self.config.BaseManager.pressure_sensor_tab_page.select_device_view_button()
            self.config.BaseManager.pressure_sensor_tab_page.select_basemanager_pressure_sensor_edit_button(_address=1)
            self.config.BaseManager.pressure_sensor_tab_page.set_device_description(description="Pressure Sensor 1 Test", address=1)
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseManager.pressure_sensor_tab_page.save_device_description_edit()

            # Verify the edits on the device tab
            for each_ps_address in sorted(self.config.BaseStation3200[1].pressure_sensors.keys()):
                self.config.BaseManager.pressure_sensor_tab_page.verify_address(address=each_ps_address)
                self.config.BaseManager.pressure_sensor_tab_page.verify_description(address=each_ps_address)
                self.config.BaseStation3200[1].pressure_sensors[each_ps_address].get_data()
                self.config.BaseStation3200[1].pressure_sensors[each_ps_address].verify_description()

            # verify the edit in quick view
            self.config.BaseManager.pressure_sensor_tab_page.select_quick_view_tab()
            self.config.BaseManager.quick_view_page.verify_open()
            for each_ps_address in sorted(self.config.BaseStation3200[1].pressure_sensors.keys()):
                self.config.BaseManager.quick_view_page.verify_qv_pressure_sensor_description(_pressure_sensor_address=each_ps_address)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Changed Description on the Pressure Sensor and verified all changes"
            )

    #################################
    def step_10(self):
        """
        - Selects Zones Tab
        - Select edit button for the Zone
        - Change description on the Zone
        - Save and verify changes are saved by checking the description in list view.
        - Verify the changes on the Quick View tab
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            # make edit on the devices tab
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.zone)
            self.config.BaseManager.zones_tab_page.verify_is_open()
            self.config.BaseManager.zones_tab_page.select_device_view_button()
            self.config.BaseManager.zones_tab_page.select_basemanager_zone(_address=1)
            self.config.BaseManager.zones_tab_page.select_basemanager_zone_edit_button(_address=1)
            self.config.BaseManager.zones_tab_page.set_device_description(description="Test BM Zone 1", address=1)
            self.config.BaseManager.zones_tab_page.save_device_description_edit()

            # wait for the packets to be processed
            # sleep(opcodes.seconds_for_server_to_process_packets)

            # Verify the edits on the device tab
            for each_zn_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseManager.zones_tab_page.verify_address(address=each_zn_address)
                self.config.BaseManager.zones_tab_page.verify_description(address=each_zn_address)
                self.config.BaseStation3200[1].zones[each_zn_address].get_data()
                self.config.BaseStation3200[1].zones[each_zn_address].verify_description()

            # verify the edit in quick view
            self.config.BaseManager.pressure_sensor_tab_page.select_quick_view_tab()
            self.config.BaseManager.quick_view_page.verify_open()
            for each_zn_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseManager.quick_view_page.verify_qv_zone_description(_zone_address=each_zn_address)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Changed Description on a Zone and verified changes in list view and in Quick View"
            )