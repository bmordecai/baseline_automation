import sys
from time import sleep

import common.configuration as test_conf
from common import helper_methods
from common.imports import opcodes

# import log_handler for logging functionality
from common.logging_handler import log_handler

__author__ = 'baseline'


class BaseManagerUseCase7(object):
    """
    Purpose:
        - This test verifies the POC attributes displayed in the UI on BaseManager for v16 BaseStation 3200's.
        - Attribute text labels are verified.

    Coverage:
        - The following POC attributes are verified to:
            - Be visible for v16 3200
            - Have correct text

        POC Attributes verified:
        - Low Pressure Shutdown Label Text

    TODOs / Not Covered:
        - Verify the rest of the POC attributes.

    Bugs covered by test:
        (1) BM-1922
        (2) BM-1926
    """
    # ~~~~ Local Variables ~~~~ #

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = test_conf.Configuration(test_name=test_name,
                                              user_conf_file=user_configuration_instance,
                                              data_json_file=json_configuration_file)
        
        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        The following steps are done on all browser specified in the user configuration: \n

        Step 1:
            - Set up POC 1 on controller

        Step 2:
            - Initialize browser web page objects.

        Step 3:
            - initialize & open browser

        step 4:
            - login & verify successful logged in

        Step 5:
            - select site
            - select controller
            - verify QuickView tab opens

        step 6:
            - Select POC tab
            - Verify POC view opens

        step 7:
            - Verify all POC list UI (incomplete)

        step 8:
            - Select POC 1
            - Verify POC detail view displayed

        step 9:
            - Verify JIRA Bugs
                - BM-1922


        Not Covered:
            - Selecting the different map views and verifying each view opens
            - Selecting dialog boxes in each tab to verify they work/open to the correct destination
        """
        try:
            # browsers_for_test = ["chrome", "firefox"]
            browsers_for_test = ["chrome"]
            for run_number, browser in enumerate(browsers_for_test):
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test(connect_to_basemanager=True, web_tests=True)

                # get list of all the steps by function name in the use case
                method_list = [func for func in dir(self) if
                               callable(getattr(self, func)) and func.startswith('step')]

                # sort list in numerical order of numbers in steps step names must be 'step_X'
                sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                # run each step_1,2,3 esc.
                for method in sorted_new_list:
                    getattr(self, method)()

                # Close the first webdriver to start the second web driver
                if run_number == 0 and len(browsers_for_test) > 1:
                    self.config.resource_handler.web_driver.web_driver.quit()

        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        else:
            helper_methods.print_test_passed(self.config.test_name)

        finally:
            # We close the first web-driver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)
            
    #################################
    def step_1(self):
        """
        Initialize POC 1 for controller
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(_master_valve_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Initialized web pages"
            )

    #################################
    def step_2(self):
        """
        Initializes all pages needed for test
        """
        helper_methods.print_method_name()
        try:
            pass
            # # Get web driver instance from config
            # self.web_driver = self.config.resource_handler.web_driver
            #
            # # Create page instances to use for Client use
            # self.config.BaseManager.main_page_main_menu = factory.get_main_menu_object(base_page=self.config.BaseManager)
            #
            # # Main Tab Page Objects
            # self.config.BaseManager.login_page = factory.get_basemanager_login_page_object(base_page=self.config.BaseManager)
            # self.config.BaseManager.main_page = factory.get_main_page_object(base_page=self.config.BaseManager)
            # self.config.BaseManager.quick_view_page = factory.get_quick_view_page_object(base_page=self.config.BaseManager)
            #
            # # 3200 specific tabs
            # self.config.BaseManager.point_of_control_3200_page = factory.get_3200_poc_tab(base_page=self.config.BaseManager)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Initialized web pages"
            )

    #################################
    def step_3(self):
        """
        Initializes the web driver to the correct browser executor
        """
        helper_methods.print_method_name()
        try:
            pass
            # Open browser
            # self.config.resource_handler.web_driver.open(_browser)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Opened Web Browser"
            )

    #################################
    def step_4(self):
        """
        """
        helper_methods.print_method_name()
        try:
            # enter the user name and password selects login button
            self.config.BaseManager.login_page.enter_login_info()
            self.config.BaseManager.login_page.click_login_button()
            self.config.BaseManager.main_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Logged In"
            )

    #################################
    def step_5(self):
        """
        - Selects a site and controller
        - Verifies QuickView Tab is opened
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page_main_menu.navigate_to_my_controller(
                mac_address=self.config.BaseStation3200[1].mac)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected Site and Controller"
            )

    #################################
    def step_6(self):
        """
        Selects the POC tab and verifies it is open.
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_water_sources_tab(water_source_type=opcodes.point_of_connection)
            self.config.BaseManager.point_of_control_3200_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected POC Tab and Verified it opened"
            )

    #################################
    def step_7(self):
        """
        Verify POC view header and table headers
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.point_of_control_3200_page.verify_header_text()
            self.config.BaseManager.point_of_control_3200_page.verify_list_view_table_id_column_displayed()
            self.config.BaseManager.point_of_control_3200_page.verify_list_view_table_description_column_displayed()
            self.config.BaseManager.point_of_control_3200_page.verify_list_view_table_mainline_column_displayed()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified POC List View UI"
            )

    #################################
    def step_8(self):
        """
        Select POC 1
        - Verify that the detail view displays and that the description is correct
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.point_of_control_3200_page.select_poc(poc_number=1)
            self.config.BaseManager.point_of_control_3200_page.verify_detail_view_displayed(poc_number=1)
            self.config.BaseManager.point_of_control_3200_page.verify_description(_control_point_address=1)
            # TODO we can add other detail view verifiers here
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified POC Detail View Displayed for POC 1."
            )

    #################################
    def step_9(self):
        """
        Verify JIRA Bugs
        """
        helper_methods.print_method_name()
        try:
            # Verify Low Pressure Shutdown Label text is "Shutdown" and not "Pause"
            self.config.BaseManager.point_of_control_3200_page.verify_low_pressure_shutdown_label_text_jira_bm_1922()
            # Verify v12 bug fix didn't affect v16
            self.config.BaseManager.point_of_control_3200_page.verify_master_valve_enable_is_visible_jira_bm_1926()
            # Verify in edit view the same thing
            self.config.BaseManager.point_of_control_3200_page.click_edit_button()
            # Verify Low Pressure Shutdown Label text is "Shutdown" and not "Pause" in edit mode
            self.config.BaseManager.point_of_control_3200_page.verify_low_pressure_shutdown_label_text_jira_bm_1922()
            # Verify v12 bug fix didn't affect v16 in edit mode
            self.config.BaseManager.point_of_control_3200_page.verify_master_valve_enable_is_visible_jira_bm_1926()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified JIRA Bugs (BM-1922, BM-1926) for POC Detail View"
            )

    #################################
    def step_10(self):
        """
        Select the Flow Setup/Control Point tab and verify that your description is correct.
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_flow_setup_point_of_control()
            self.config.BaseManager.point_of_control_3200_page.verify_description_in_control_point_list(_address=1)

            # Put us back into the water source list view
            self.config.BaseManager.main_page.select_flow_setup_point_of_control()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Attributes for Water Source 1 were Displayed on BaseManager"
            )

    #################################
    def step_11(self):
        """
        Verify Water Source 1 values can be changed in BaseManager and we can see those changes on the 3200
        """
        helper_methods.print_method_name()
        try:
            control_point_address = 1
            self.config.BaseManager.point_of_control_3200_page.select_poc(poc_number=control_point_address)
            self.config.BaseManager.point_of_control_3200_page.click_edit_button()
            self.config.BaseManager.point_of_control_3200_page.set_description(
                _control_point_address=control_point_address,
                _description="DESCRIPTION SET IN BASEMANAGER")
            self.config.BaseManager.point_of_control_3200_page.save_edits()
            # Increment the clock to give the controller time to update
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)
            sleep(opcodes.seconds_for_server_to_process_packets)
            self.config.BaseManager.point_of_control_3200_page.select_poc(poc_number=control_point_address)
            self.config.BaseManager.point_of_control_3200_page.verify_description(
                _control_point_address=control_point_address)
            # Get the most recent values from the controller and verify they mirror the client
            self.config.BaseStation3200[1].points_of_control[1].get_data()
            self.config.BaseStation3200[1].points_of_control[control_point_address].verify_description()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(

                test_name=self.config.test_name,
                msg="Verified we can change an attribute of Control Point in BaseManager and have it reflect in the "
                    "3200"
            )

    #################################
    def step_12(self):
        """
        Verify Control Point 1 in QuickView
        - Verify the tooltips when clicking on a program
        - Change the control point description and verify the change is populated in QuickView
        """
        helper_methods.print_method_name()
        try:
            # Verifying control point 1 in quick view
            control_point_address = 1
            self.config.BaseManager.point_of_control_3200_page.select_quick_view_tab()
            self.config.BaseManager.quick_view_page.select_control_point(_address=control_point_address)
            self.config.BaseManager.quick_view_page.verify_tooltip_control_point_description(
                _address=control_point_address)

            # Change the value on the controller and verify that it gets populated up into QuickView
            self.config.BaseStation3200[1].points_of_control[control_point_address].set_description(
                _ds='CP{0} DESCRIPTION SET FOR QUICKVIEW'.format(control_point_address))
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)
            sleep(opcodes.seconds_for_server_to_process_packets)
            self.config.BaseManager.quick_view_page.select_control_point(_address=control_point_address)
            self.config.BaseManager.quick_view_page.verify_tooltip_control_point_description(
                _address=control_point_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Control Point in Quick View"
            )
