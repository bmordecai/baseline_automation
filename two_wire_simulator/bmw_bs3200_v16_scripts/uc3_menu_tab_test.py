import sys

__author__ = 'baseline'
from common.imports import *
import common.configuration as test_conf
import common.user_configuration as user_conf
import common.page_factory as factory
from common import helper_methods
from common.imports import opcodes

# import log_handler for logging functionality
from common.logging_handler import log_handler


class BaseManagerUseCase3(object):
    """
    Purpose:
        This test verifies all Base Manager tabs for a 3200 controller. \n
            - verify each tab opens successfully
                - Maps Tab
                - QuickView Tab
                - Programs Tab
                - Devices Tab
                - Water Sources
                    - TODO: Water Sources
                    - Point of Connection
                    - Mainlines
                - LiveView

    TODOs / Not Covered:
        - Selecting the different map views and verifying each view opens
        - Selecting dialog boxes in each tab to verify they work/open to the correct destination
    """
    # ~~~~ Local Variables ~~~~ #

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = test_conf.Configuration(test_name=test_name,
                                              user_conf_file=user_configuration_instance,
                                              data_json_file=json_configuration_file)
        
        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        The following steps are done on all browser specified in the user configuration: \n

        Step 1:
            - Initialize all web pages for browser

        Step 2:
            - initialize & open browser

        step 3:
            - login & verify successful logged in

        Step 4:
            - select site
            - select controller
            - verify QuickView tab opens

        Step 5:
            - select Maps
            - verify it opened

        Step 6:
            - Select Quick View tab
            - Verify it opened

        Step 7:
            - Select Programs Tab
            - Verify it opened

        step 8:
            - Verify all device tabs:
                - select Zones & verify it opens
                - select Moisture Sensors & verify it opens
                - select Flow Meters & verify it opens
                - select Temperature Sensors & verify it opens
                - select Master Valves & verify it opens
                - select Event Switch & verify it opens
                - select Pumps & verify it opens
                - select Pressure Sensors & verify it opens

        step 9:
            - Verify all Water Sources sub tabs:
                - select Point of Connection tab
                    - verify it opened
                - select Mainlines
                    - verify it opened

        step 10:
            - select LiveView
            - verify it opens

        Not Covered:
            - Selecting the different map views and verifying each view opens
            - Selecting dialog boxes in each tab to verify they work/open to the correct destination
        """
        try:
            # browsers_for_test = ["chrome", "firefox"]
            browsers_for_test = ["chrome"]
            for run_number, browser in enumerate(browsers_for_test):
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test(connect_to_basemanager=True, web_tests=True)
                self.step_1()
                self.step_2(_browser=browser)
                self.step_3()
                self.step_4()
                self.step_5()
                self.step_6()
                self.step_7()
                self.step_8()
                self.step_9()
                self.step_10()

                # Close the first webdriver to start the second web driver
                if run_number == 0 and len(browsers_for_test) > 1:
                    self.config.resource_handler.web_driver.web_driver.quit()

        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        else:
            helper_methods.print_test_passed(self.config.test_name)

        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)

    #################################
    def step_1(self):
        """
        Initializes all pages needed for test
        """
        helper_methods.print_method_name()
        try:
            pass
            # Get web driver instance from config
            # self.web_driver = self.config.resource_handler.web_driver
            #
            # # Create page instances to use for Client use
            # self.config.BaseManager.main_page_main_menu = factory.get_main_menu_object(base_page=self.config.BaseManager)
            #
            # # Main Tab Page Objects
            # self.config.BaseManager.login_page = factory.get_basemanager_login_page_object(base_page=self.config.BaseManager)
            # self.config.BaseManager.main_page = factory.get_main_page_object(base_page=self.config.BaseManager)
            # self.config.BaseManager.maps_page = factory.get_maps_page_object(base_page=self.config.BaseManager)
            # self.config.BaseManager.quick_view_page = factory.get_quick_view_page_object(base_page=self.config.BaseManager)
            # self.config.BaseManager.live_view_page = factory.get_live_view_page_object(base_page=self.config.BaseManager)
            # self.config.BaseManager.programs_page = factory.get_programs_page_object(base_page=self.config.BaseManager)
            #
            # # Device page Objects
            # self.config.BaseManager.zones_tab_page = factory.get_zones_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.moisture_sensor_tab_page = factory.get_moisture_sensors_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.master_valve_tab_page = factory.get_master_valves_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.flow_sensor_tab_page = factory.get_flow_meter_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.temperature_sensor_tab_page = factory.get_temperature_sensors_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.event_switch_tab_page = factory.get_event_switch_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.pump_tab_page = factory.get_pumps_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            # self.config.BaseManager.pressure_sensor_tab_page = factory.get_pressure_sensors_page_object(base_page=self.config.BaseManager, _controller=self.config.BaseStation3200[1])
            #
            # # 3200 specific tabs
            # self.config.BaseManager.point_of_control_3200_page = factory.get_3200_poc_tab(base_page=self.config.BaseManager)
            # self.config.BaseManager.mainline_3200_page = factory.get_3200_mainlines_tab(base_page=self.config.BaseManager)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Initialized web pages"
            )

    #################################
    def step_2(self, _browser):
        """
        Initializes the web driver to the correct browser executor
        """
        helper_methods.print_method_name()
        try:
            # Open browser
            # self.config.resource_handler.web_driver.open(_browser)
            pass
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Opened Web Browser"
            )

    #################################
    def step_3(self):
        """
        """
        helper_methods.print_method_name()
        try:
            # enter the user name and password selects login button
            self.config.BaseManager.login_page.enter_login_info()
            self.config.BaseManager.login_page.click_login_button()
            self.config.BaseManager.main_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Logged In"
            )

    #################################
    def step_4(self):
        """
        - Selects a site and controller
        - Verifies QuickView Tab is opened
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page_main_menu.navigate_to_my_controller(
                mac_address=self.config.BaseStation3200[1].mac)

            # selecting a controller returns you to the quick view tab
            self.config.BaseManager.quick_view_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected Site and Controller"
            )

    #################################
    def step_5(self):
        """
        Selects the map tab and verifies it is open.
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_maps_tab(map_view="company")
            self.config.BaseManager.maps_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected Maps Tab and Verified it opened"
            )

    #################################
    def step_6(self):
        """
        Selects the Quick View tab and verifies it opens.
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_quick_view_tab()
            self.config.BaseManager.quick_view_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected QuickView Tab and Verified it opened"
            )

    #################################
    def step_7(self):
        """
        Selects the program's tab and verifies it opens
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_programs_tab()
            self.config.BaseManager.programs_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected Programs Tab and Verified it opened"
            )

    #################################
    def step_8(self):
        """
        - Verify all device tabs:
            - select Zones & verify it opens
            - select Moisture Sensors & verify it opens
            - select Flow Meters & verify it opens
            - select Temperature Sensors & verify it opens
            - select Master Valves & verify it opens
            - select Event Switch & verify it opens
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.zone)
            self.config.BaseManager.zones_tab_page.verify_is_open()
            
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.master_valve)
            self.config.BaseManager.master_valve_tab_page.verify_is_open()

            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.pump)
            self.config.BaseManager.pump_tab_page.verify_is_open()

            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.flow_meter)
            self.config.BaseManager.flow_sensor_tab_page.verify_is_open()

            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.moisture_sensor)
            self.config.BaseManager.moisture_sensor_tab_page.verify_is_open()
            
            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.pressure_sensor)
            self.config.BaseManager.pressure_sensor_tab_page.verify_is_open()

            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.event_switch)
            self.config.BaseManager.event_switch_tab_page.verify_is_open()

            self.config.BaseManager.main_page.select_devices_tab(device_type=opcodes.temperature_sensor)
            self.config.BaseManager.temperature_sensor_tab_page.verify_is_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected and verified all Device Tabs and verified it opened"
            )

    #################################
    def step_9(self):
        """
        Select and verify the Water Sources tab opens
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_water_sources_tab(water_source_type=opcodes.point_of_connection)
            self.config.BaseManager.point_of_control_3200_page.verify_open()

            self.config.BaseManager.main_page.select_water_sources_tab(water_source_type=opcodes.mainline)
            self.config.BaseManager.mainline_3200_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected Water Sources > Point of Connection & Mainlines and Verified they opened"
            )

    #################################
    def step_10(self):
        """
        Selects and verifies the LiveView tab opens
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_live_view_tab()
            self.config.BaseManager.live_view_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected LiveView Tab and Verified it opened"
            )
