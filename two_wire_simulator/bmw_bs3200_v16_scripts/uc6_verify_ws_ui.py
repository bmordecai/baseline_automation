import sys

from time import sleep

# import common.product as helper_methods
from common import helper_methods
from common.imports import opcodes
from common.configuration import Configuration

# import log_handler for logging functionality
from common.logging_handler import log_handler


__author__ = 'Eldin'


class BaseManagerUseCase6(object):
    """
    Test name:
        - Flow Setup Water Sources Tabs Test

    User Story: \n
        1)  As a user I want to be able to see how much water I have used from a particular water source, and I want to
            be able to view this data when I am not near my controller. I want my controller and whatever client I have
            to match up and display the same data.

    Coverage and Objectives:
        1.  When a user goes to BaseManager and goes to 'Flow Setup'->'Water Sources' we want to display all of the
            information relevant to the water source, such as budgets, shutdowns, etc.
        2.  When a water source value is changed on the controller, we want to verify it populates up to the client
            after giving time for the server to process.
        3.  When a value gets changed in the client, we expect the controller to reflect that as well.

    Not Covered:
        1.	Actual packets flying up and down. We verify this indirectly by verifying the values but we don't capture
            any packets.
        2.  No database storage is verified.

    Test Overview:
        - Setup the devices that can be on a water source empty condition
            - Event Switches (Empty Condition)
            - Moisture Sensors (Empty Condition)
            - Pressure Sensors (Empty Condition)
        - Setup a WS->CP->ML connection
        - Log into BaseManager
        - Go to your Water Sources tab and verify the data is correct.
        - Change a value for a water source in BaseManager and verify it also gets changed in a 3200.
        - Verify changing the description in a 3200 also changes it in BaseManager without having to reload the page.

    Test Configuration setup: \n
        - Zones
            - 1 (TSD0001)
            - 2 (TSD0002)
            - 3 (TSD0003)
            - 4 (TSD0004)
        - Moisture Sensors
            - 1 (SB00001)
            - 8 (SB00002)
        - Pressure Sensors
            - 1 (PSF0001)
            - 8 (PSF0002)
        - Event Switches
            - 1 (TPD0001)
            - 8 (TPD0002)
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize Use Case instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir="common/configuration_files/bmw_json_config_files")

        self.run_use_case()

    def run_use_case(self):
        """
        This method initializes the test and runs the steps. \n
        """
        try:
            for run_number, browser in enumerate(["chrome"]):  # Firefox taken out for now
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test(connect_to_basemanager=True, web_tests=True)

                # get list of all the steps by function name in the use case
                method_list = [func for func in dir(self) if
                               callable(getattr(self, func)) and func.startswith('step')]

                # sort list in numerical order of numbers in steps step names must be 'step_X'
                sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                # run each step_1,2,3 esc.
                for method in sorted_new_list:
                    getattr(self, method)()

                # Close the first webdriver to start the second web driver
                if run_number == 0:
                    self.config.resource_handler.web_driver.web_driver.quit()

        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise
        else:
            helper_methods.print_test_passed(self.config.test_name)

        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup water sources
        ############################
        """
        helper_methods.print_method_name()
        try:
            # Setup Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_description(_ds="Initial Description WS 1")
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=10000)
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)

            # Leave Water Source 8 with default values
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Set up the Water Sources"
            )

    #################################
    def step_2(self):
        """
        ############################
        setup control point
        ############################
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Setup the Control Point to Water Source Assignment"
            )

    #################################
    def step_3(self):
        """
        ############################
        setup mainline
        ############################
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Setup the Mainline to Control Point Assignment"
            )

    #################################
    def step_4(self):
        """
        ############################
        setup empty conditions
        ############################
        Setup an empty condition
        """
        helper_methods.print_method_name()
        try:
            # Add the Moisture Sensor Empty Condition
            self.config.BaseStation3200[1].water_sources[1].add_moisture_empty_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].water_sources[1].moisture_empty_conditions[1].set_empty_wait_time(_minutes=5)
            self.config.BaseStation3200[1].water_sources[1].moisture_empty_conditions[1].set_moisture_empty_limit(
                _percent=28)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Created Empty Conditions Conditions"
            )

    #################################
    def step_5(self):
        """
        - Verify the full configuration
        - Increment the clock 1 minute so all programming gets sent up to BaseManager
        """
        helper_methods.print_method_name()
        try:
            self.config.verify_full_configuration()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified the Full Configuration"
            )

    #################################
    def step_6(self):
        """
        - Login to BaseManager
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.login_page.enter_login_info()
            self.config.BaseManager.login_page.click_login_button()
            self.config.BaseManager.main_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Logged into BaseManager"
            )

    #################################
    def step_7(self):
        """
        - Select your controller
        - Verify QuickView Tab is open
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page_main_menu.navigate_to_my_controller(
                mac_address=self.config.BaseStation3200[1].mac)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected Site and Controller"
            )

    #################################
    def step_8(self):
        """
        Select the Flow Setup/Water Sources tab and verify that your description is correct.
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_flow_setup_water_sources()
            self.config.BaseManager.water_source_3200_page.select_water_source(_address=1)
            self.config.BaseManager.water_source_3200_page.verify_description(_water_source_address=1)
            # TODO add verifiers for the other water source attributes here
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Attributes for Water Source 1 were Displayed on BaseManager"
            )

    #################################
    def step_9(self):
        """
        Select the Flow Setup/Water Sources tab and verify that your description is correct.
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_flow_setup_water_sources()
            self.config.BaseManager.water_source_3200_page.verify_description_in_water_source_list(_address=1)

            # Put us back into the water source list view
            self.config.BaseManager.main_page.select_flow_setup_water_sources()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Attributes for Water Source 1 were Displayed on BaseManager"
            )

    #################################
    def step_10(self):
        """
        Verify Water Source 1 values can be changed in BaseManager and we can see those changes on the 3200
        """
        helper_methods.print_method_name()
        try:
            water_source_address = 1
            self.config.BaseManager.water_source_3200_page.select_water_source(_address=water_source_address)
            self.config.BaseManager.water_source_3200_page.click_edit_button()
            self.config.BaseManager.water_source_3200_page.set_description(_water_source_address=water_source_address,
                                                                           _description="DESCRIPTION SET IN BASEMANAGER")
            self.config.BaseManager.water_source_3200_page.save_edits()
            # Increment the clock to give the controller time to update
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)
            sleep(opcodes.seconds_for_server_to_process_packets)
            self.config.BaseManager.water_source_3200_page.select_water_source(_address=water_source_address)
            self.config.BaseManager.water_source_3200_page.verify_description(_water_source_address=water_source_address)
            # Get the most recent values from the controller and verify they mirror the client
            self.config.BaseStation3200[1].water_sources[1].get_data()
            self.config.BaseStation3200[1].water_sources[water_source_address].verify_description()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(

                test_name=self.config.test_name,
                msg="Verified we can change an attribute of Water Source in BaseManager and have it reflect in the 3200"
            )

    #################################
    def step_11(self):
        """
        Verify Water Source 1 in QuickView
        - Verify the tooltips when clicking on a water source
        - Change the water source description and verify the change is populated in QuickView
        """
        helper_methods.print_method_name()
        try:
            # Verifying water source 1 in quick view
            water_source_address = 1
            self.config.BaseManager.water_source_3200_page.select_quick_view_tab()
            self.config.BaseManager.quick_view_page.select_water_source(_address=water_source_address)
            self.config.BaseManager.quick_view_page.verify_tooltip_water_source_description(
                _address=water_source_address)

            # Change the value on the controller and verify that it gets populated up into QuickView
            self.config.BaseStation3200[1].water_sources[water_source_address].set_description(
                _ds='WS{0} DESCRIPTION SET FOR QUICKVIEW'.format(water_source_address))
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)
            sleep(opcodes.seconds_for_server_to_process_packets)
            self.config.BaseManager.quick_view_page.select_water_source(_address=water_source_address)
            self.config.BaseManager.quick_view_page.verify_tooltip_water_source_description(
                _address=water_source_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Water Source in Quick View"
            )
