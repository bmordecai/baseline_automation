import sys

__author__ = 'bens'
# import common.product as helper_methods
from common.imports import *
from common import helper_methods
from common.configuration import Configuration
from common import resource_handler

# import log_handler for logging functionality
from common.logging_handler import log_handler

# Browser pages used
# from common.objects.basemanager.main_page import *
# from common.objects.basemanager.basemanager_login_page import LoginPage
# from common.objects.basemanager.main_page import MainPage

# Mobile pages used
# from common.objects.mobile_access.m_login_page import MobileLoginPage

from common.objects.basemanager.locators import *
import common.page_factory as factory


class BaseManagerUseCase1(object):
    """
    Purpose:
        - verify browser opens:
            - Firefox
            - Chrome
        - verify login:
            - desktop
            - mobile
        - verify that links work on the main login page

    Coverage area: \n
         - login:
             - desktop
             - mobile. \n
         - input correct:
             - username
             - password
         - input incorrect:
             - username
             - password
         - select old version
         - retrieve forgotten password
         - set up new account
    3. Able to login to both Firefox and Google Chrome browsers. \n
    """

    # ~~~~ Local Variables ~~~~ #

    # TODO: Still need to further investigate Internet Explorer implementation which is not
    # TODO: currently supported.
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.run_use_case()

    def run_use_case(self):
        """
        The following steps are done on all browser specified in the user configuration: \n

        Step 1:
            - open browser
        step 2:
            - retrieve forgotten password:
                -  verify popup window appears
        Step 3:
            - setup new account:
                - verify form loads
        Step 4:
            - enter incorrect user name and password
                - verify error message
        step 5:
            - login into desktop view as a company admin
                - select a site
                - select a controller
                - verify quick view loads
        step 6:
            - login into desktop view as a supper user
                - select a company
                - select a site
                - select a controller
                - verify quick view loads
        step 7:
            - login into old desktop view as a company admin
                - select a site
                - select a controller
                - verify quick view loads
        step 8:
            - login into mobile view as a company admin
                - select a site
                - select a controller
                - verify quick view load

    Not Covered:
            - login into old desktop view as a supper user
            - login into mobile access view as a supper user is not supported at this time
        """
        try:
            for run_number, browser in enumerate(["chrome"]): # Firefox taken out for now
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test(connect_to_basemanager=True, web_tests=True)
                self.step_1(_browser=browser)
                self.step_2()
                self.step_3()
                self.step_4()
                self.step_5()
                self.step_6()
                # TODO think about removing this step there is no button to go into old versions
                # self.step_7()
                # TODO this is not done need to add mobile
                self.step_8()

                # Close the first webdriver to start the second web driver
                if run_number == 0:
                    self.config.resource_handler.web_driver.web_driver.quit()

        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise
        else:
            helper_methods.print_test_passed(self.config.test_name)

        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)

    #################################
    def step_1(self, _browser):
        """
        Initializes the web driver to the correct browser executor
        :return:
        """
        helper_methods.print_method_name()
        try:
            pass
            # # Create page instances to use for Client use
            # self.config.BaseManager.login_page = factory.get_basemanager_login_page_object(base_page=self.config.BaseManager)
            # self.config.BaseManager.main_page_main_menu = factory.get_basemanager_main_menu_object(base_page=self.config.BaseManager)
            # self.m_login_page = factory.get_mobile_login_page_object(base_page=self.config.MobileAccess)
            # # Open browser
            # self.config.resource_handler.web_driver.open(_browser)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Initialized BaseManager page objects and opened browser"
            )

    #################################
    def step_2(self):
        """
        select the retrieve forgotten password: \n
            -  verify popup window appears
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.login_page.select_retrieve_forgotten_password_link()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Forgotten Password Link"
            )

    #################################
    def step_3(self):
        """
        select the setup new account: \n
            - verify form loads
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.login_page.select_set_up_a_new_account_link()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Set Up a New Account Link"
            )

    #################################
    def step_4(self):
        """
        Sends in login credentials, username and password that are incorrect and then clicks the login button \n
            - verify error message
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.login_page.enter_login_info(_user_name='user test', _password='password test')
            self.config.BaseManager.login_page.click_login_button()
            # verify text returned
            self.config.BaseManager.login_page.verify_login_error()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Login Error"
            )

    #################################
    def step_5(self):
        """
        Sends in login credentials, username and password for a company admin and then clicks the login button \n
        Method runs as follows: \n
            - Finds both the username and password text boxes located on the login page and clears \n
                their contents, ensuring there is no garbage left over, and then enters the username \n
                and password credentials provided by the user configuration \n
            - select the login button  \n
            - Selects the site passed in from the user configuration \n
            - select the controller passed in from the user configuration \n
            - Finds and selects the quickview tab verifying that the login, site, and controller selections worked \n
            - select the main menu  \n
            - select the sign out button adn verify the login page loads  \n
        :return
        """
        helper_methods.print_method_name()
        try:
            # enter the user name and password selects login button
            self.config.BaseManager.login_page.enter_login_info()
            self.config.BaseManager.login_page.click_login_button()
            self.config.BaseManager.main_page.verify_open()
            self.config.BaseManager.login_page.check_login_response()
            self.config.BaseManager.main_page.select_main_menu()
            self.config.BaseManager.main_page_main_menu.select_site()
            self.config.BaseManager.main_page_main_menu.select_a_controller(mac_address=self.config.BaseStation3200[1].mac)
            self.config.BaseManager.main_page_main_menu.wait_for_main_menu_close()
            self.config.BaseManager.main_page.select_quick_view_tab()
            # self.config.BaseManager.main_page.select_a_menu_tab(_menu_name_locators=MainPageLocators.QUICK_VIEW_TAB)
            self.config.BaseManager.main_page.select_main_menu()
            self.config.BaseManager.main_page_main_menu.click_sign_out_button()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Company Admin Login"
            )

    #################################
    def step_6(self):
        """
        Sends in login credentials, username and password for a super user and then clicks the login button \n
        Method runs as follows: \n
            - Finds both the username and password text boxes located on the login page and clears \n
                their contents, ensuring there is no garbage left over, and then enters the username \n
                and password credentials these are being over writen for this test only\n
            - select the login button  \n
            - select the company which is being passed in \n
            - Selects the site which is being passed in\n
            - select the controller passed in from the user configuration \n
            - Finds and selects the quickview tab verifying that the login, site, and controller selections worked \n
            - select the main menu  \n
            - select the sign out button adn verify the login page loads  \n
        :return
        """
        helper_methods.print_method_name()
        try:
            # enter the user name and password selects login button
            self.config.BaseManager.login_page.enter_login_info(_user_name="SuperAutoTests",
                                                                _password="SuperTest@10259")
            self.config.BaseManager.login_page.click_login_button()
            self.config.BaseManager.main_page.verify_open()
            self.config.BaseManager.login_page.check_login_response()
            self.config.BaseManager.main_page.select_main_menu()
            self.config.BaseManager.main_page_main_menu.select_a_company(company=self.config.user_conf.company)
            self.config.BaseManager.main_page_main_menu.wait_for_main_menu_close()
            self.config.BaseManager.main_page.select_main_menu()
            self.config.BaseManager.main_page_main_menu.select_site(site_name=self.config.user_conf.site_name)
            self.config.BaseManager.main_page_main_menu.select_a_controller(mac_address=self.config.BaseStation3200[1].mac)
            self.config.BaseManager.main_page_main_menu.wait_for_main_menu_close()
            self.config.BaseManager.main_page.select_quick_view_tab()
            # self.config.BaseManager.main_page.select_a_menu_tab(_menu_name_locators=MainPageLocators.QUICK_VIEW_TAB)
            self.config.BaseManager.main_page.select_main_menu()
            self.config.BaseManager.main_page_main_menu.click_sign_out_button()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Super Admin Login"
            )

    #################################
    def step_7(self):
        """
        Sends in login credentials, username and password for a company admin and then clicks the login button \n
        Method runs as follows: \n
            - select the use previous version so that the old web site will load up \n
            - Finds both the username and password text boxes located on the login page and clears \n
                their contents, ensuring there is no garbage left over, and then enters the username \n
                and password credentials provided by the user configuration \n
            - select the login button  \n
            - Selects the site passed in from the user configuration \n
            - select the controller passed in from the user configuration \n
            - Finds and selects the quickview tab verifying that the login, site, and controller selections worked \n
            - select the logout button and verify the login page loads  \n
        :return
        """
        helper_methods.print_method_name()
        try:
            # enter the user name and password selects login button
            self.config.BaseManager.login_page.click_use_previous_version_button()
            self.config.BaseManager.login_page.enter_login_info()
            self.config.BaseManager.login_page.click_login_button()
            # Wait for the page to load and the main menu to be visible.
            self.config.resource_handler.web_driver.wait_for_element_clickable(MainPageLocators.COLOR_BUTTON)
            self.config.resource_handler.web_driver.is_visible(MainPageLocators.COLOR_BUTTON)
            self.config.BaseManager.login_page.check_login_response()
            self.config.BaseManager.main_page.select_quick_view_tab()
            # self.config.BaseManager.main_page.select_a_menu_tab(_menu_name_locators=MainPageLocators.QUICK_VIEW_TAB)
            self.config.BaseManager.main_page.click_logout_button()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Old Desktop Login"
            )

    #################################
    def step_8(self):
        """
        Sends in login credentials, username and password for a company admin and then clicks the login button \n
        Method runs as follows: \n
            - select use the mobile version
            - Finds both the username and password text boxes located on the login page and clears \n
                their contents, ensuring there is no garbage left over, and then enters the username \n
                and password credentials provided by the user configuration \n
            - select the login button  \n
            - Selects the site passed in from the user configuration \n
            - select the controller passed in from the user configuration \n
            - Finds and selects the quickview tab verifying that the login, site, and controller selections worked \n
            - select the main menu  \n
            - select the sign out button adn verify the login page loads  \n
        :return
         """
        helper_methods.print_method_name()
        try:
            # self.basemanager_login_page.verify_open()
            self.config.BaseManager.login_page.click_go_to_mobile_version()
            self.config.MobileAccess.login_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Mobile Login Page Link From Desktop"
            )
        # #TODO need mobile
        # self.basemanager_login_page.enter_login_info()
        # self.basemanager_login_page.click_login_button()
        # select a controller
        # select header from Controller operations
        # <h3 id="menuTitle">Controller Operations</h3>

