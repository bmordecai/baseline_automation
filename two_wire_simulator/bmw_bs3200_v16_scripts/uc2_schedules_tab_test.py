import sys
from time import sleep

# import common.product as helper_methods
from common import helper_methods
from common.imports import opcodes
from common.configuration import Configuration

# import log_handler for logging functionality
from common.logging_handler import log_handler


__author__ = 'Eldin'


class BaseManagerUseCase2(object):
    """
    Test name:
        - Mobile Access Schedule Tab Test

    User Story: \n
        1)  As a user I want to be able to view my watering schedules at a client when I am not near my physical
            controller. I want the values I see on that client to mirror what I would be seeing on my controller.

    Coverage and Objectives:
        1.  When a user goes to BaseManager and clicks on the 'Schedules' tab, we want to display all of their programs
            and the attributes on the programs. These include:
            - Program Zones Attributes
            - Start Conditions Attributes
            - Pause Conditions Attributes
            - Stop Conditions Attributes
            - Program Attributes
        2. When a program value is changed on the controller, the client gets an update without refreshing.
        3. When a value gets changed in the client, the controller will get the updated value

    Not Covered:
        1.	Actual packets flying up and down. We verify this indirectly by verifying the values but we don't capture
            any packets.
        2.  No database storage is verified.

    Test Overview:
        - Setup all devices that could possible be on a program:
            - Zones
            - Master Valves
            - Pumps
            - Moisture Sensors
        - Setup devices that could be in start/stop/pause conditions:
            - Moisture Sensors
            - Temperature Sensors
            - Pressure Sensors
            - Event Switches
        - Setup the Zone Programs
        - Setup the Start/Stop/Pause Conditions
        - Log into BaseManager
        - Browse to the controller I am using
        - Go to the schedules tab
        - Verify every value, and then change the values and verify them again after waiting for the server to send
          the packet notifying the client to update.
        - Change the value in BaseManager and verify it takes effect in the controller.

    Test Configuration setup: \n
        - Zones
            - 1 (TSD0001)
            - 2 (TSD0002)
            - 3 (TSQ0011)
            - 198 (TSQ0012)
            - 199 (TSQ0013)
            - 200 (TSQ0014)
        - Master Valves
            - 1 (TMV0001)
        - Pumps
            - 1 (TPR0001)
        - Moisture Sensors
            - 1 (SB00001)
            - 2 (SB00002)
            - 3 (SB00003)
            - 4 (SB00004)
        - Temperature Sensors
            - 1 (TAT0001)
            - 2 (TAT0002)
            - 3 (TAT0003)
            - 4 (TAT0004)
        - Pressure Sensors
            - 1 (PSF0001)
            - 2 (PSF0002)
            - 3 (PSF0003)
            - 4 (PSF0004)
        - Event Switches
            - 1 (TPD0001)
            - 2 (TPD0002)
            - 3 (TPD0003)
            - 4 (TPD0004)
        - Flow Meters
            - NONE
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize Use Case instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.run_use_case()

    def run_use_case(self):
        """
        The following steps are done on all browser specified in the user configuration: \n

        Not Covered:
            - login into old desktop view as a supper user
            - login into mobile access view as a supper user is not supported at this time
        """
        try:
            for run_number, browser in enumerate(["chrome"]):  # Firefox taken out for now
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test(connect_to_basemanager=True, web_tests=True)

                # get list of all the steps by function name in the use case
                method_list = [func for func in dir(self) if
                               callable(getattr(self, func)) and func.startswith('step')]

                # sort list in numerical order of numbers in steps step names must be 'step_X'
                sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                # run each step_1,2,3 esc.
                for method in sorted_new_list:
                    getattr(self, method)()

                # Close the first webdriver to start the second web driver
                if run_number == 0:
                    self.config.resource_handler.web_driver.web_driver.quit()

        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise
        else:
            helper_methods.print_test_passed(self.config.test_name)

        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        - Leave one program with default values and change the other programs values
        """
        helper_methods.print_method_name()
        try:
            # Leave program 1 as default values
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)

            # Edit values for program 2
            program_water_windows = [
                '111111111111111111111111', '000000001111111111111111', '000011111111111111110000',
                '111111111111111111111111', '111111111111111111111111', '111111111111111111111111',
                '111111111111111111111111'
            ]
            program_semi_month_interval = [1, 0, 0, 0 , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
            program_start_times = [480, 560]

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_description(_ds="TEST PROGRAM 99")
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[99].set_weekly_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[99].set_seasonal_adjust(_percent=50)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_start_times)
            self.config.BaseStation3200[1].programs[99].set_priority_level(_pr_level=2)
            self.config.BaseStation3200[1].programs[99].add_booster_pump(_mv_address=1)
            self.config.BaseStation3200[1].programs[99].set_watering_intervals_to_semi_monthly(
                _sm=program_semi_month_interval)
            self.config.BaseStation3200[1].programs[99].set_watering_intervals_to_all_days_of_the_week()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Set up the Programs"
            )

    #################################
    def step_2(self):
        """
        ############################
        setup zone programs
        ############################
        - Put a primary, linked, and timed zone on each program
        """
        helper_methods.print_method_name()
        try:
            # Add the zones to program 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_water_strategy_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=4)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_tracking_ratio(_runtime_ratio=50)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_run_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_soak_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_cycle_time(_minutes=5)

            # Add the zones to program 99
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=198)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_soak_time(_minutes=4)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_cycle_time(_minutes=4)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[99].zone_programs[198].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=4)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=199)
            self.config.BaseStation3200[1].programs[99].zone_programs[199].set_as_linked_zone(_primary_zone=198)
            self.config.BaseStation3200[1].programs[99].zone_programs[199].set_tracking_ratio(_runtime_ratio=25)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=200)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_run_time(_minutes=10)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_soak_time(_minutes=5)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_cycle_time(_minutes=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Setup 3 Zones on each Program"
            )

    #################################
    def step_3(self):
        """
        ############################
        setup start/stop/pause conditions
        ############################
        Only program 1 will ahve conditions
        """
        helper_methods.print_method_name()
        try:
            # Add program 1's start/stop/pause conditions
            # MOISTURE SENSOR START/STOP/PAUSE
            self.config.BaseStation3200[1].programs[1].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].moisture_start_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].moisture_start_conditions[1].set_moisture_threshold(_percent=27)

            self.config.BaseStation3200[1].programs[1].add_moisture_pause_condition(_moisture_sensor_address=2)
            self.config.BaseStation3200[1].programs[1].moisture_pause_conditions[2].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].moisture_pause_conditions[2].set_moisture_threshold(_percent=25)

            self.config.BaseStation3200[1].programs[1].add_moisture_stop_condition(_moisture_sensor_address=3)
            self.config.BaseStation3200[1].programs[1].moisture_stop_conditions[3].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].moisture_stop_conditions[3].set_moisture_threshold(_percent=23)
            
            # TEMPERATURE SENSOR START/STOP/PAUSE
            self.config.BaseStation3200[1].programs[1].add_temperature_start_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].temperature_start_conditions[1].set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].temperature_start_conditions[1].set_temperature_threshold(
                _degrees=90)

            self.config.BaseStation3200[1].programs[1].add_temperature_pause_condition(_temperature_sensor_address=2)
            self.config.BaseStation3200[1].programs[1].temperature_pause_conditions[2].set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].temperature_pause_conditions[2].set_temperature_threshold(
                _degrees=70)

            self.config.BaseStation3200[1].programs[1].add_temperature_stop_condition(_temperature_sensor_address=3)
            self.config.BaseStation3200[1].programs[1].temperature_stop_conditions[3].set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].temperature_stop_conditions[3].set_temperature_threshold(
                _degrees=50)
            
            # PRESSURE SENSOR START/STOP/PAUSE
            self.config.BaseStation3200[1].programs[1].add_pressure_start_condition(_pressure_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].pressure_start_conditions[1].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].pressure_start_conditions[1].set_pressure_limit(
                _limit=30)

            self.config.BaseStation3200[1].programs[1].add_pressure_pause_condition(_pressure_sensor_address=2)
            self.config.BaseStation3200[1].programs[1].pressure_pause_conditions[2].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].pressure_pause_conditions[2].set_pressure_limit(
                _limit=25)

            self.config.BaseStation3200[1].programs[1].add_pressure_stop_condition(_pressure_sensor_address=3)
            self.config.BaseStation3200[1].programs[1].pressure_stop_conditions[3].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].pressure_stop_conditions[3].set_pressure_limit(
                _limit=20)

            # EVENT SWITCH START/STOP/PAUSE
            self.config.BaseStation3200[1].programs[1].add_switch_start_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[1].event_switch_start_conditions[1].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[1].add_switch_pause_condition(_event_switch_address=2)
            self.config.BaseStation3200[1].programs[1].event_switch_pause_conditions[2].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[1].add_switch_stop_condition(_event_switch_address=3)
            self.config.BaseStation3200[1].programs[1].event_switch_stop_conditions[3].set_switch_mode_to_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Created Start/Stop/Pause Conditions"
            )

    #################################
    def step_4(self):
        """
        - Verify the full configuration
        - Increment the clock 1 minute so all programming gets sent up to BaseManager
        """
        helper_methods.print_method_name()
        try:
            self.config.verify_full_configuration()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified the Full Configuration"
            )

    #################################
    def step_5(self):
        """
        - Login to BaseManager
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.login_page.enter_login_info()
            self.config.BaseManager.login_page.click_login_button()
            self.config.BaseManager.main_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Logged into BaseManager"
            )

    #################################
    def step_6(self):
        """
        - Select your controller
        - Verify QuickView Tab is open
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page_main_menu.navigate_to_my_controller(
                mac_address=self.config.BaseStation3200[1].mac)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected Site and Controller"
            )

    #################################
    def step_7(self):
        """
        Select the program/schedules tab
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseManager.main_page.select_programs_tab()
            self.config.BaseManager.programs_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected Schedules Tab"
            )

    #################################
    def step_8(self):
        """
        Verify Program 1
        - Verify values we have set are shown in BaseManager
        - Make a change in the controller while the page is still up and verify that the page updates with that value
        """
        helper_methods.print_method_name()
        try:
            program_address = 1
            # Change some values so we aren't just verifying defaults
            self.config.BaseManager.programs_page.select_program(_address=program_address)
            self.config.BaseManager.programs_page.verify_description(_program_address=program_address)
            # TODO VERIFY ALL THESE WHEN WE DECIDE WE WANT MORE COVERAGE (METHODS WILL NEED TO BE WRITTEN)
            # self.config.BaseManager.programs_page.verify_address(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_enabled(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_zone_program_mode(_program_address=program_address,
            #                                                                _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_runtime(_program_address=program_address,
            #                                                                   _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_cycle_time(_program_address=program_address,
            #                                                                      _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_soak_time(_program_address=program_address,
            #                                                                     _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_gpm(_program_address=program_address,
            #                                                               _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_moisture_sensor(_program_address=program_address,
            #                                                                           _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_watering_strategy(_program_address=program_address,
            #                                                                             _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_limit(_program_address=program_address,
            #                                                                 _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_current_moisture(_program_address=program_address,
            #                                                                            _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_mode(_program_address=program_address,
            #                                                                _zone_address=2)
            # self.config.BaseManager.programs_page.verify_zone_program_tracking_ratio(_program_address=program_address,
            #                                                                          _zone_address=2)
            # self.config.BaseManager.programs_page.verify_zone_program_soak_time(_program_address=program_address,
            #                                                                     _zone_address=2)
            # self.config.BaseManager.programs_page.verify_zone_program_gpm(_program_address=program_address,
            #                                                               _zone_address=2)
            # self.config.BaseManager.programs_page.verify_zone_program_mode(_program_address=program_address,
            #                                                                _zone_address=3)
            # self.config.BaseManager.programs_page.verify_zone_program_runtime(_program_address=program_address,
            #                                                                   _zone_address=3)
            # self.config.BaseManager.programs_page.verify_zone_program_cycle_time(_program_address=program_address,
            #                                                                      _zone_address=3)
            # self.config.BaseManager.programs_page.verify_zone_program_soak_time(_program_address=program_address,
            #                                                                     _zone_address=3)
            # self.config.BaseManager.programs_page.verify_zone_program_gpm(_program_address=program_address,
            #                                                               _zone_address=3)
            # self.config.BaseManager.programs_page.verify_start_times(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_event_switch_start(_program_address=program_address,
            #                                                                 _event_switch_address=1)
            # self.config.BaseManager.programs_page.verify_moisture_sensor_start(_program_address=program_address,
            #                                                                    _moisture_sensor_address=1)
            # self.config.BaseManager.programs_page.verify_temperature_sensor_start(_program_address=program_address,
            #                                                                       _temperature_sensor_address=1)
            # self.config.BaseManager.programs_page.verify_event_switch_pause(_program_address=program_address,
            #                                                                 _event_switch_address=2)
            # self.config.BaseManager.programs_page.verify_moisture_sensor_pause(_program_address=program_address,
            #                                                                    _moisture_sensor_address=2)
            # self.config.BaseManager.programs_page.verify_temperature_sensor_pause(_program_address=program_address,
            #                                                                       _temperature_sensor_address=2)
            # self.config.BaseManager.programs_page.verify_event_switch_stop(_program_address=program_address,
            #                                                                _event_switch_address=3)
            # self.config.BaseManager.programs_page.verify_moisture_sensor_stop(_program_address=program_address,
            #                                                                   _moisture_sensor_address=3)
            # self.config.BaseManager.programs_page.verify_temperature_sensor_stop(_program_address=program_address,
            #                                                                      _temperature_sensor_address=3)
            # self.config.BaseManager.programs_page.verify_water_window(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_priority(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_seasonal_adjust(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_concurrent_zone(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_booster_pump(_program_address=program_address)

            # Put us back into the program list view
            self.config.BaseManager.programs_page.select_programs_tab()

            # Change a value and verify the page loads after waiting for the time it takes for the server to process
            self.config.BaseStation3200[1].programs[program_address].set_description(
                _ds="PG{0} DESCRIPTION SET ON CONTROLLER".format(program_address))
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)
            sleep(opcodes.seconds_for_server_to_process_packets)
            self.config.BaseManager.programs_page.verify_description_in_program_list(_address=program_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Program 1 Test Attributes and That Changes on the Controller Reflect in BaseManager After"
                    "Some Time to Process"
            )

    #################################
    def step_9(self):
        """
        Verify Program 99
        - Verify values we have set are shown in BaseManager
        - Make a change in the controller while the program list page is still up and verify that the page updates with
          that value
        """
        helper_methods.print_method_name()
        try:
            program_address = 99
            # Change some values so we aren't just verifying defaults
            self.config.BaseManager.programs_page.select_program(_address=program_address)
            self.config.BaseManager.programs_page.verify_description(_program_address=program_address)
            # TODO VERIFY ALL THESE WHEN WE DECIDE WE WANT MORE COVERAGE (METHODS WILL NEED TO BE WRITTEN)
            # self.config.BaseManager.programs_page.verify_address(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_enabled(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_zone_program_mode(_program_address=program_address,
            #                                                                _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_runtime(_program_address=program_address,
            #                                                                   _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_cycle_time(_program_address=program_address,
            #                                                                      _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_soak_time(_program_address=program_address,
            #                                                                     _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_gpm(_program_address=program_address,
            #                                                               _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_moisture_sensor(_program_address=program_address,
            #                                                                           _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_watering_strategy(_program_address=program_address,
            #                                                                             _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_limit(_program_address=program_address,
            #                                                                 _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_current_moisture(_program_address=program_address,
            #                                                                            _zone_address=1)
            # self.config.BaseManager.programs_page.verify_zone_program_mode(_program_address=program_address,
            #                                                                _zone_address=2)
            # self.config.BaseManager.programs_page.verify_zone_program_tracking_ratio(_program_address=program_address,
            #                                                                          _zone_address=2)
            # self.config.BaseManager.programs_page.verify_zone_program_soak_time(_program_address=program_address,
            #                                                                     _zone_address=2)
            # self.config.BaseManager.programs_page.verify_zone_program_gpm(_program_address=program_address,
            #                                                               _zone_address=2)
            # self.config.BaseManager.programs_page.verify_zone_program_mode(_program_address=program_address,
            #                                                                _zone_address=3)
            # self.config.BaseManager.programs_page.verify_zone_program_runtime(_program_address=program_address,
            #                                                                   _zone_address=3)
            # self.config.BaseManager.programs_page.verify_zone_program_cycle_time(_program_address=program_address,
            #                                                                      _zone_address=3)
            # self.config.BaseManager.programs_page.verify_zone_program_soak_time(_program_address=program_address,
            #                                                                     _zone_address=3)
            # self.config.BaseManager.programs_page.verify_zone_program_gpm(_program_address=program_address,
            #                                                               _zone_address=3)
            # self.config.BaseManager.programs_page.verify_start_times(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_event_switch_start(_program_address=program_address,
            #                                                                 _event_switch_address=1)
            # self.config.BaseManager.programs_page.verify_moisture_sensor_start(_program_address=program_address,
            #                                                                    _moisture_sensor_address=1)
            # self.config.BaseManager.programs_page.verify_temperature_sensor_start(_program_address=program_address,
            #                                                                       _temperature_sensor_address=1)
            # self.config.BaseManager.programs_page.verify_event_switch_pause(_program_address=program_address,
            #                                                                 _event_switch_address=2)
            # self.config.BaseManager.programs_page.verify_moisture_sensor_pause(_program_address=program_address,
            #                                                                    _moisture_sensor_address=2)
            # self.config.BaseManager.programs_page.verify_temperature_sensor_pause(_program_address=program_address,
            #                                                                       _temperature_sensor_address=2)
            # self.config.BaseManager.programs_page.verify_event_switch_stop(_program_address=program_address,
            #                                                                _event_switch_address=3)
            # self.config.BaseManager.programs_page.verify_moisture_sensor_stop(_program_address=program_address,
            #                                                                   _moisture_sensor_address=3)
            # self.config.BaseManager.programs_page.verify_temperature_sensor_stop(_program_address=program_address,
            #                                                                      _temperature_sensor_address=3)
            # self.config.BaseManager.programs_page.verify_water_window(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_priority(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_seasonal_adjust(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_concurrent_zone(_program_address=program_address)
            # self.config.BaseManager.programs_page.verify_booster_pump(_program_address=program_address)

            # Put us back into the program list view
            self.config.BaseManager.programs_page.select_programs_tab()

            # Change a value and verify the page loads after waiting for the time it takes for the server to process
            self.config.BaseStation3200[1].programs[program_address].set_description(
                _ds="PG{0} DESCRIPTION SET ON CONTROLLER".format(program_address))
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)
            sleep(opcodes.seconds_for_server_to_process_packets)
            self.config.BaseManager.programs_page.verify_description_in_program_list(_address=program_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Program 99 Test Attributes and That Changes on the Controller Reflect in BaseManager "
                    "After Some Time to Process"
            )

    #################################
    def step_10(self):
        """
        Verify Program 1 values can be changed in BaseManager and we can see those changes on the 3200
        """
        helper_methods.print_method_name()
        try:
            program_address = 1
            self.config.BaseManager.programs_page.select_program(_address=program_address)
            self.config.BaseManager.programs_page.select_edit_button()
            self.config.BaseManager.programs_page.set_description(_program_address=program_address,
                                                                  _description="DESCRIPTION SET IN BASEMANAGER")
            self.config.BaseManager.programs_page.save_new_program()
            sleep(opcodes.seconds_for_server_to_process_packets)
            # Increment the clock to give the controller time to update
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)
            self.config.BaseManager.programs_page.select_program(_address=program_address)
            self.config.BaseManager.programs_page.verify_description(_program_address=program_address)
            # Get the most recent values from the controller and verify they mirror the client
            self.config.BaseStation3200[1].programs[1].get_data()
            self.config.BaseStation3200[1].programs[program_address].verify_description()

            # Put us back into the program list view
            self.config.BaseManager.programs_page.select_programs_tab()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Program 1 Test Attributes and That Changes on the Controller Reflect in BaseManager After"
                    "Some Time to Process"
            )

    #################################
    def step_11(self):
        """
        Verify Program 1 in QuickView
        - Verify the tooltips when clicking on a program
        - Change the program description and verify the change is populated in QuickView
        """
        helper_methods.print_method_name()
        try:
            # Verifying program 1 in quick view
            program_address = 1
            self.config.BaseManager.programs_page.select_quick_view_tab()
            self.config.BaseManager.quick_view_page.select_program(_address=program_address)
            self.config.BaseManager.quick_view_page.verify_tooltip_program_description(_program_address=program_address)

            # Change the value on the controller and verify that it gets populated up into QuickView
            self.config.BaseStation3200[1].programs[program_address].set_description(
                _ds='PG{0} DESCRIPTION SET FOR QUICKVIEW'.format(program_address))
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)
            sleep(opcodes.seconds_for_server_to_process_packets)
            self.config.BaseManager.quick_view_page.select_program(_address=program_address)
            self.config.BaseManager.quick_view_page.verify_tooltip_program_description(_program_address=program_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Program 1 in Quick View"
            )
