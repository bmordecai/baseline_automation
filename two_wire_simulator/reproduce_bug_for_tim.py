import sys
import os

import common.user_configuration as user_conf_module

# Substation use cases
import bs_substation_scripts.uc_4_basic_programming_for_tim as cuc_substation_4_module

_author__ = 'Ben'


def main(argv):
    """
    """
    
    # Load in user configured items from text file (necessary for serial connection)
    user_conf = user_conf_module.UserConfiguration(os.path.join('common/user_credentials', 'user_credentials_tim_substn.json'))
    
    # 3200 Serial number and firmware version
    cn_serial_num = '3K10001'
    firm_version = '16.0.588'
    
    # Substation firmware version
    substation_fw_version = "1.0"
    
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #      SubStation Tests                                                                                            #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #

    cuc_substation_4_module.UseCase4BasicProgramming_BugReproduce(
        test_name="SB UC 4-Basic Programming",
        user_configuration_instance=user_conf,
        json_configuration_file="SB_basic_programming_simple.json")


if __name__ == "__main__":
    main(sys.argv[1:])
