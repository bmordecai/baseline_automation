import os

import common.user_configuration as user_conf_module

# 3200 use cases
import Hardware_and_Manufacturing_scripts.use_case_2 as ms_use_case_2

_author__ = 'Tige'
user_credentials = "user_credentials_eldin.json"
# auto_update_fw = False


def run_use_cases_hardware(user_configuration_file_name, passing_tests, failing_tests, manual_tests,
                       specific_tests, auto_update_fw=False):
    """
    This is where we will run all of our 3200 only use cases. \n

    :param user_configuration_file_name:    The name of the configuration file we will be using for the tests. \n
    :type user_configuration_file_name:     str
    
    :param auto_update_fw:    If you want to automatically update firmware versions of controllers to "latest" version.
    :type auto_update_fw:     bool

    :param passing_tests:   If you want to run 3200 passing tests. True for yes, False for no. \n
    :type passing_tests:    bool

    :param failing_tests:   If you want to run 3200 failing tests. True for yes, False for no. \n
    :type failing_tests:    bool

    :param manual_tests:    If you want to run 3200 manual tests. True for yes, False for no. \n
    :type manual_tests:     bool

    :param specific_tests:  Pick which tests you want to run individually. Will run all if the list is empty. \n
    :type specific_tests:   list[int | float]
    """

    # Load in user configured items from text file (necessary for serial connection)
    user_conf = user_conf_module.UserConfiguration(os.path.join(
        'common/user_credentials',
        user_configuration_file_name)
    )
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #                                                   3200 TESTS                                                     #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Passing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if passing_tests:
        if 1 in specific_tests or len(specific_tests) == 0:
            ms_use_case_2.ControllerUseCase2(test_name="CN-UseCase2-MoistureSensorTest",
                                             user_configuration_instance=user_conf,
                                             json_configuration_file='real_devices.json')

        print("########################### ----FINISHED RUNNING PASSING 3200 TESTS---- ###############################")

    print("######################### ----YOU ARE A WINNER WINNER CHICKEN DINNER---- ##################################")


if __name__ == "__main__":
    run_use_cases_hardware(user_configuration_file_name=user_credentials,
                           passing_tests=True,
                           failing_tests=False,
                           manual_tests=False,
                           specific_tests=[1],
                           auto_update_fw=False)
    exit()

