import os

import common.user_configuration as user_conf_module

# 3200 use cases
import bs3200_scripts.use_case_1 as cuc_3200_1_module
import bs3200_scripts.use_case_2 as cuc_3200_2_module
import bs3200_scripts.use_case_3 as cuc_3200_3_module
import bs3200_scripts.use_case_4 as cuc_3200_4_module
import bs3200_scripts.use_case_5 as cuc_3200_5_module
import bs3200_scripts.use_case_5_1 as cuc_3200_5_1_module
import bs3200_scripts.use_case_5_2 as cuc_3200_5_2_module
import bs3200_scripts.use_case_5_3 as cuc_3200_5_3_module
import bs3200_scripts.use_case_5_4 as cuc_3200_5_4_module
import bs3200_scripts.use_case_6 as cuc_3200_6_module
import bs3200_scripts.use_case_7 as cuc_3200_7_module
import bs3200_scripts.use_case_8 as cuc_3200_8_module
import bs3200_scripts.use_case_9 as cuc_3200_9_module
import bs3200_scripts.use_case_10 as cuc_3200_10_module
import bs3200_scripts.use_case_10_1 as cuc_3200_10_1_module
import bs3200_scripts.use_case_11 as cuc_3200_11_module
import bs3200_scripts.use_case_12 as cuc_3200_12_module
import bs3200_scripts.use_case_12_1 as cuc_3200_12_1_module
import bs3200_scripts.use_case_12_2 as cuc_3200_12_2_module
import bs3200_scripts.use_case_12_3 as cuc_3200_12_3_module
import bs3200_scripts.use_case_13 as cuc_3200_13_module
import bs3200_scripts.use_case_14 as cuc_3200_14_module
import bs3200_scripts.use_case_15 as cuc_3200_15_module
import bs3200_scripts.use_case_16 as cuc_3200_16_module
import bs3200_scripts.use_case_17 as cuc_3200_17_module
import bs3200_scripts.use_case_18 as cuc_3200_18_module
import bs3200_scripts.use_case_19 as cuc_3200_19_module
import bs3200_scripts.use_case_20 as cuc_3200_20_module
import bs3200_scripts.use_case_20_1 as cuc_3200_20_1_module
import bs3200_scripts.use_case_20_2 as cuc_3200_20_2_module
import bs3200_scripts.use_case_20_3 as cuc_3200_20_3_module
import bs3200_scripts.use_case_20_4 as cuc_3200_20_4_module
import bs3200_scripts.use_case_20_5 as cuc_3200_20_5_module
import bs3200_scripts.use_case_21 as cuc_3200_21_module
import bs3200_scripts.use_case_22 as cuc_3200_22_module
import bs3200_scripts.use_case_23 as cuc_3200_23_module
import bs3200_scripts.use_case_24 as cuc_3200_24_module
import bs3200_scripts.use_case_25 as cuc_3200_25_module
import bs3200_scripts.use_case_26 as cuc_3200_26_module
import bs3200_scripts.use_case_27 as cuc_3200_27_module
import bs3200_scripts.use_case_28 as cuc_3200_28_module
import bs3200_scripts.use_case_29 as cuc_3200_29_module
import bs3200_scripts.use_case_30 as cuc_3200_30_module
import bs3200_scripts.use_case_31 as cuc_3200_31_module
import bs3200_scripts.use_case_32 as cuc_3200_32_module
import bs3200_scripts.use_case_33 as cuc_3200_33_module
import bs3200_scripts.use_case_34 as cuc_3200_34_module
import bs3200_scripts.use_case_34_1 as cuc_3200_34_1_module
import bs3200_scripts.use_case_34_2 as cuc_3200_34_2_module
import bs3200_scripts.use_case_35 as cuc_3200_35_module
import bs3200_scripts.use_case_36 as cuc_3200_36_module
import bs3200_scripts.use_case_37 as cuc_3200_37_module
import bs3200_scripts.use_case_38 as cuc_3200_38_module
import bs3200_scripts.use_case_39 as cuc_3200_39_module
import bs3200_scripts.use_case_40 as cuc_3200_40_module
import bs3200_scripts.use_case_41 as cuc_3200_41_module
import bs3200_scripts.use_case_43 as cuc_3200_43_module
import bs3200_scripts.use_case_43_1 as cuc_3200_43_1_module
import bs3200_scripts.use_case_44 as cuc_3200_44_module
import bs3200_scripts.use_case_44_1 as cuc_3200_44_1_module
import bs3200_scripts.use_case_44_2 as cuc_3200_44_2_module
import bs3200_scripts.use_case_44_3 as cuc_3200_44_3_module
import bs3200_scripts.use_case_44_4 as cuc_3200_44_4_module
import bs3200_scripts.use_case_44_5 as cuc_3200_44_5_module
import bs3200_scripts.use_case_44_6 as cuc_3200_44_6_module
import bs3200_scripts.use_case_44_7 as cuc_3200_44_7_module
import bs3200_scripts.use_case_45 as cuc_3200_45_module
import bs3200_scripts.use_case_46 as cuc_3200_46_module
import bs3200_scripts.use_case_47 as cuc_3200_47_module
import bs3200_scripts.use_case_47_1 as cuc_3200_47_1_module
import bs3200_scripts.use_case_47_2 as cuc_3200_47_2_module
import bs3200_scripts.use_case_48 as cuc_3200_48_module
import bs3200_scripts.use_case_48_1 as cuc_3200_48_1_module
import bs3200_scripts.use_case_49 as cuc_3200_49_module
import bs3200_scripts.use_case_50 as cuc_3200_50_module
import bs3200_scripts.use_case_52 as cuc_3200_52_module
import bs3200_scripts.use_case_53 as cuc_3200_53_module
import bs3200_scripts.use_case_54_1 as cuc_3200_54_1_module
import bs3200_scripts.use_case_54_2 as cuc_3200_54_2_module
import bs3200_scripts.use_case_54_3 as cuc_3200_54_3_module
import bs3200_scripts.use_case_54_4 as cuc_3200_54_4_module
import bs3200_scripts.use_case_55 as cuc_3200_55_module
import bs3200_scripts.use_case_56 as cuc_3200_56_module
import bs3200_scripts.use_case_57_1 as cuc_3200_57_1_module
import bs3200_scripts.use_case_57_2 as cuc_3200_57_2_module
import bs3200_scripts.use_case_57_3 as cuc_3200_57_3_module
import bs3200_scripts.use_case_57_4 as cuc_3200_57_4_module
import bs3200_scripts.use_case_58 as cuc_3200_58_module
import bs3200_scripts.use_case_59 as cuc_3200_59_module
import bs3200_scripts.use_case_60 as cuc_3200_60_module
import bs3200_scripts.use_case_61 as cuc_3200_61_module
import bs3200_scripts.use_case_62 as cuc_3200_62_module
import bs3200_scripts.use_case_63 as cuc_3200_63_module
import bs3200_scripts.use_case_65 as cuc_3200_65_module
import bs3200_scripts.use_case_66 as cuc_3200_66_module
import bs3200_scripts.use_case_67 as cuc_3200_67_module
# import bs3200_scripts.use_case_68 as cuc_3200_68_module
import bs3200_scripts.use_case_69 as cuc_3200_69_module

_author__ = 'Tige'
user_credentials = "user_credentials_tim_local.json"
# auto_update_fw = False


def run_use_cases_3200(user_configuration_file_name, passing_tests, failing_tests, manual_tests,
                       specific_tests, auto_update_fw=False):
    """
    This is where we will run all of our 3200 only use cases. \n

    :param user_configuration_file_name:    The name of the configuration file we will be using for the tests. \n
    :type user_configuration_file_name:     str
    
    :param auto_update_fw:    If you want to automatically update firmware versions of controllers to "latest" version.
    :type auto_update_fw:     bool

    :param passing_tests:   If you want to run 3200 passing tests. True for yes, False for no. \n
    :type passing_tests:    bool

    :param failing_tests:   If you want to run 3200 failing tests. True for yes, False for no. \n
    :type failing_tests:    bool

    :param manual_tests:    If you want to run 3200 manual tests. True for yes, False for no. \n
    :type manual_tests:     bool

    :param specific_tests:  Pick which tests you want to run individually. Will run all if the list is empty. \n
    :type specific_tests:   list[int | float]
    """

    # Load in user configured items from text file (necessary for serial connection)
    user_conf = user_conf_module.UserConfiguration(os.path.join(
        'common/user_credentials',
        user_configuration_file_name)
    )
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #                                                   3200 TESTS                                                     #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Passing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if passing_tests:
        if 1 in specific_tests or len(specific_tests) == 0:
            cuc_3200_1_module.ControllerUseCase1(test_name="CN-UseCase1-ReplaceDevices",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='update_cn_use_case_1.json')
        if 2 in specific_tests or len(specific_tests) == 0:
            cuc_3200_2_module.ControllerUseCase2(test_name="CN-UseCase2-EPATestConfiguration",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='EPA_test_configuration.json')
        if 3 in specific_tests or len(specific_tests) == 0:
            cuc_3200_3_module.ControllerUseCase3(test_name="CN-UseCase3-SetMessages",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='set_messages.json')
        if 4 in specific_tests or len(specific_tests) == 0:
            cuc_3200_4_module.ControllerUseCase4(test_name="CN-UseCase4-LowerLimitOneTimeCalibration",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='calibration_tests.json')
        if 5 in specific_tests or len(specific_tests) == 0:
            cuc_3200_5_module.ControllerUseCase5(test_name="CN-UseCase5-WS_empty_conditions",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='empty_conditions.json')
        if 5.1 in specific_tests or len(specific_tests) == 0:
            cuc_3200_5_1_module.ControllerUseCase5_1(test_name="CN-UseCase5_1-WS_Budget_conditions",
                                                     user_configuration_instance=user_conf,
                                                     json_configuration_file='water_budget.json')
        if 5.2 in specific_tests or len(specific_tests) == 0:
            cuc_3200_5_2_module.ControllerUseCase5_2(test_name="CN-UseCase5_2-WS_Budget_conditions",
                                                     user_configuration_instance=user_conf,
                                                     json_configuration_file='water_budget.json')
        if 5.3 in specific_tests or len(specific_tests) == 0:
            cuc_3200_5_3_module.ControllerUseCase5_3(test_name="CN-UseCase5_3-WS_Budget_conditions",
                                                     user_configuration_instance=user_conf,
                                                     json_configuration_file='water_budget.json')
        if 6 in specific_tests or len(specific_tests) == 0:
            cuc_3200_6_module.ControllerUseCase6(test_name="CN-UseCase6-check cn serial number",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='check_cn_serial_number.json')
        if 7 in specific_tests or len(specific_tests) == 0:
            cuc_3200_7_module.ControllerUseCase7(test_name="CN-UseCase7-multi ssp with event decoder",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='multiple_ssp_with_event_decoders.json')
        if 8 in specific_tests or len(specific_tests) == 0:
            cuc_3200_8_module.ControllerUseCase8(test_name="CN-UseCase8-multi ssp with all devices",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='multiple_ssp_with_all_devices.json')
        if 9 in specific_tests or len(specific_tests) == 0:
            cuc_3200_9_module.ControllerUseCase9(test_name="CN-UseCase9-memory_usage",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='ram_test_3200.json')
        if 10 in specific_tests or len(specific_tests) == 0:
            cuc_3200_10_module.ControllerUseCase10(test_name="CN-UseCase10-SeasonalAdjust",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='seasonal_adjust.json')
        if 10.1 in specific_tests or len(specific_tests) == 0:
            cuc_3200_10_1_module.ControllerUseCase10_1(test_name="CN-UseCase10_1-SeasonalAdjust_NoML",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='seasonal_adjust.json')
        if 12 in specific_tests or len(specific_tests) == 0:
            cuc_3200_12_module.ControllerUseCase12(test_name="CN-UseCase12-backup_restore_from_bmw",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file=
                                                   'back_restore_firmware_updates_no_et.json')
        if 12.3 in specific_tests or len(specific_tests) == 0:
            cuc_3200_12_3_module.ControllerUseCase12_3(test_name="CN-UseCase12_3-Two Backups with Reboot",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file=
                                                       'back_restore_firmware_updates_no_et.json')
        if 13 in specific_tests or len(specific_tests) == 0:
            cuc_3200_13_module.ControllerUseCase13(test_name="CN-UseCase13-learnflowmultiplemainlines",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='learn_flow_multiple_mainlines.json')
        if 14 in specific_tests or len(specific_tests) == 0:
            cuc_3200_14_module.ControllerUseCase14(test_name="CN-UseCase14-lowflowvariance",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='flow_variance.json')
        if 15 in specific_tests or len(specific_tests) == 0:
            cuc_3200_15_module.ControllerUseCase15(test_name="CN-UseCase15-basicProgramming",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_programming.json')
        if 16 in specific_tests or len(specific_tests) == 0:
            cuc_3200_16_module.ControllerUseCase16(test_name="CN-UseCase16-timedZones",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='timed_zones_with_soak_cycles.json')
        if 17 in specific_tests or len(specific_tests) == 0:
            cuc_3200_17_module.ControllerUseCase17(test_name="CN-UseCase17-soak_cycles",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='timed_zones_with_soak_cycles.json')
        if 18 in specific_tests or len(specific_tests) == 0:
            cuc_3200_18_module.ControllerUseCase18(test_name="CN-UseCase18-basic_design_flow",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_flow_test.json')
        if 19 in specific_tests or len(specific_tests) == 0:
            cuc_3200_19_module.ControllerUseCase19(test_name="CN-UseCase19-highflowvariance",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='flow_variance.json')
        if 20 in specific_tests or len(specific_tests) == 0:
            cuc_3200_20_module.ControllerUseCase20(test_name="CN-UseCase20-AdvancedFlowVariance",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='advanced_flow_variance.json')
        if 20.1 in specific_tests or len(specific_tests) == 0:
            cuc_3200_20_1_module.ControllerUseCase20_1(test_name="CN-UseCase20_1-AdvancedHiFlowVarianceTier1",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='advanced_flow_variance.json')
        if 20.2 in specific_tests or len(specific_tests) == 0:
            cuc_3200_20_2_module.ControllerUseCase20_2(test_name="CN-UseCase20_2-AdvancedLoFlowVarianceTier1",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='advanced_flow_variance.json')
        if 20.3 in specific_tests or len(specific_tests) == 0:
            cuc_3200_20_3_module.ControllerUseCase20_3(test_name="CN-UseCase20_3-AdvancedHiFlowVarianceTier2",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='advanced_flow_variance.json')
        if 20.4 in specific_tests or len(specific_tests) == 0:
            cuc_3200_20_4_module.ControllerUseCase20_4(test_name="CN-UseCase20_4-AdvancedLoFlowVarianceTier2",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='advanced_flow_variance.json')
        if 20.5 in specific_tests or len(specific_tests) == 0:
            cuc_3200_20_5_module.ControllerUseCase20_5(test_name="CN-UseCase20_5-MainlineStrikeRuleVariance",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='advanced_flow_variance_20_5.json')
        if 21 in specific_tests or len(specific_tests) == 0:
            cuc_3200_21_module.ControllerUseCase21(test_name="CN-UseCase21-StartTimes",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='start_times_test.json')
        if 23 in specific_tests or len(specific_tests) == 0:
            cuc_3200_23_module.ControllerUseCase23(test_name="CN-UseCase23-Connect/Disconnect BM",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='updating_to_v16.json')
        if 24 in specific_tests or len(specific_tests) == 0:
            cuc_3200_24_module.ControllerUseCase24(test_name="CN-UseCase24-PrimaryZoneTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='primary_zone_test.json')
        if 25 in specific_tests or len(specific_tests) == 0:
            cuc_3200_25_module.ControllerUseCase25(test_name="CN-UseCase25-PrimaryZoneZeroRuntime",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='updating_to_v16.json')
        if 27 in specific_tests or len(specific_tests) == 0:
            cuc_3200_27_module.ControllerUseCase27(test_name="CN-UseCase27-WateringDays",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='watering_day_schedule_test.json')
        if 28 in specific_tests or len(specific_tests) == 0:
            cuc_3200_28_module.ControllerUseCase28(test_name="CN-UseCase28-WaterWindows",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='water_windows.json')
        if 29 in specific_tests or len(specific_tests) == 0:
            cuc_3200_29_module.ControllerUseCase29(test_name="CN-UseCase29-EventDecoderTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='event_decoder_test.json')
        if 30 in specific_tests or len(specific_tests) == 0:
            cuc_3200_30_module.ControllerUseCase30(test_name="CN-UseCase30-TemperatureDecoderTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='temperature_decoder_test.json')
        if 31 in specific_tests or len(specific_tests) == 0:
            cuc_3200_31_module.ControllerUseCase31(test_name="CN-UseCase31-ConcurrentZonesTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='concurrent_zones.json')
        if 32 in specific_tests or len(specific_tests) == 0:
            cuc_3200_32_module.ControllerUseCase32(test_name="CN-UseCase32-MoistureDecoderTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='moisture_decoder_test.json')
        if 33 in specific_tests or len(specific_tests) == 0:
            cuc_3200_33_module.ControllerUseCase33(test_name="CN-UseCase33-MasterValvePump",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='mv_pump_test.json')
        if 34 in specific_tests or len(specific_tests) == 0:
            cuc_3200_34_module.ControllerUseCase34(test_name ="CN-UseCase34-BasicLearnFlow",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_learn_flow_test.json')
        if 34.1 in specific_tests or len(specific_tests) == 0:
            cuc_3200_34_1_module.ControllerUseCase34_1(test_name ="CN-UseCase34_1-QuickLearnFlow",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='basic_learn_flow_test.json')
        if 34.2 in specific_tests or len(specific_tests) == 0:
            cuc_3200_34_2_module.ControllerUseCase34_2(test_name ="CN-UseCase34_2-LearnFlowPump",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='mv_pump_test.json')
        if 35 in specific_tests or len(specific_tests) == 0:
            cuc_3200_35_module.ControllerUseCase35(test_name="CN-UseCase35-LargeConfigTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='large_configuration_programming.json')
        if 36 in specific_tests or len(specific_tests) == 0:
            cuc_3200_36_module.ControllerUseCase36(test_name="CN-UseCase36-UpperLimitWateringTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='upper_limit_watering.json')
        if 37 in specific_tests or len(specific_tests) == 0:
            cuc_3200_37_module.ControllerUseCase37(test_name="CN-UseCase37-LowerLimitWateringTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='lower_limit_watering.json')
        if 38 in specific_tests or len(specific_tests) == 0:
            cuc_3200_38_module.ControllerUseCase38(test_name="CN-UseCase38-RebootBugTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='reboot_bug_test.json')
        if 41 in specific_tests or len(specific_tests) == 0:
            cuc_3200_41_module.ControllerUseCase41(test_name="CN-UseCase41-RebootConfigVerify",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='back_restore_firmware_updates.json')
        if 44 in specific_tests or len(specific_tests) == 0:
            cuc_3200_44_module.ControllerUseCase44(test_name="CN-UseCase44-BasicMainlineZoneDelays",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_mainline_zone_delays.json')
        if 44.1 in specific_tests or len(specific_tests) == 0:
            cuc_3200_44_1_module.ControllerUseCase44_1(test_name="CN-UseCase44_1-BasicMainlineZoneDelays",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='basic_mainline_zone_delays.json')
        if 44.2 in specific_tests or len(specific_tests) == 0:
            cuc_3200_44_2_module.ControllerUseCase44_2(test_name="CN-UseCase44_2-BasicMainlineZoneDelays",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='basic_mainline_zone_delays.json')
        if 44.3 in specific_tests or len(specific_tests) == 0:
            cuc_3200_44_3_module.ControllerUseCase44_3(test_name="CN-UseCase44_3-BasicMainlineZoneDelays",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='basic_mainline_zone_delays.json')
        if 44.4 in specific_tests or len(specific_tests) == 0:
            cuc_3200_44_4_module.ControllerUseCase44_4(test_name="CN-UseCase44_4-BasicMainlineZoneDelays",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='basic_mainline_zone_delays.json')
        if 44.5 in specific_tests or len(specific_tests) == 0:
            cuc_3200_44_5_module.ControllerUseCase44_5(test_name="CN-UseCase44_5-BasicMainlineZoneDelays",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='basic_mainline_zone_delays.json')
        if 44.6 in specific_tests or len(specific_tests) == 0:
            cuc_3200_44_6_module.ControllerUseCase44_6(test_name="CN-UseCase44_6-BasicMainlineZoneDelays",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='basic_mainline_zone_delays.json')
        if 44.7 in specific_tests or len(specific_tests) == 0:
            cuc_3200_44_7_module.ControllerUseCase44_7(test_name="CN-UseCase44_7-BasicMainlineZoneDelays",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='basic_mainline_zone_delays.json')
        if 45 in specific_tests or len(specific_tests) == 0:
            cuc_3200_45_module.ControllerUseCase45(test_name="CN-UseCase45-DCLatchSerialNumbers",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='dc_latching_decoder_serial_numbers.json')
        if 46 in specific_tests or len(specific_tests) == 0:
            cuc_3200_46_module.ControllerUseCase46(test_name="CN-UseCase46-AssignDevices",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='assign_devices.json')
        if 47.2 in specific_tests or len(specific_tests) == 0:
            cuc_3200_47_2_module.ControllerUseCase47_2(test_name="CN-UseCase47_2-ZoneInfiniteWateringBug",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='fw_update_no_ML.json')
        if 49 in specific_tests or len(specific_tests) == 0:
            cuc_3200_49_module.ControllerUseCase49(test_name="CN-UseCase49-VerifyProgramOverrunLogic",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='program_overrun_logic.json')
        if 50 in specific_tests or len(specific_tests) == 0:
            cuc_3200_50_module.ControllerUseCase50(test_name="CN-UseCase50-ProgramOverRuns",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='water_windows.json')
        if 53 in specific_tests or len(specific_tests) == 0:
            cuc_3200_53_module.ControllerUseCase53(test_name="CN-UseCase53-Rain_Jumper",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='empty_conditions.json')
        if 54.1 in specific_tests or len(specific_tests) == 0:
            cuc_3200_54_1_module.ControllerUseCase54_1(test_name="CN-UseCase54_1-POCPressureShutdown",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='empty_conditions.json')
        if 54.2 in specific_tests or len(specific_tests) == 0:
            cuc_3200_54_2_module.ControllerUseCase54_2(test_name="CN-UseCase54_2-POCHighFlowShutdown",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='empty_conditions.json')
        if 54.3 in specific_tests or len(specific_tests) == 0:
            cuc_3200_54_3_module.ControllerUseCase54_3(test_name="CN-UseCase54_3-POCUnscheduledFlowShutdown",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='empty_conditions.json')
        if 55 in specific_tests or len(specific_tests) == 0:
            cuc_3200_55_module.ControllerUseCase55(test_name="CN-UseCase55-Flow_Jumper",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='empty_conditions.json')
        if 56 in specific_tests or len(specific_tests) == 0:
            cuc_3200_56_module.ControllerUseCase56(test_name="CN-UseCase56-Pause_Jumper",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='empty_conditions.json')
        if 57.1 in specific_tests or len(specific_tests) == 0:
            cuc_3200_57_1_module.ControllerUseCase57_1(test_name="CN-UseCase57_1-IgnoreStopPauseConditions",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='ignore_global.json')
        if 57.2 in specific_tests or len(specific_tests) == 0:
            cuc_3200_57_2_module.ControllerUseCase57_2(test_name="CN-UseCase57_2-IgnoreEventDates",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='ignore_global.json')
        if 57.3 in specific_tests or len(specific_tests) == 0:
            cuc_3200_57_3_module.ControllerUseCase57_3(test_name="CN-UseCase57_3-IgnoreRainDays",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='ignore_global.json')
        if 57.4 in specific_tests or len(specific_tests) == 0:
            cuc_3200_57_4_module.ControllerUseCase57_4(test_name="CN-UseCase57_4-IgnoreJumpers",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='ignore_global.json')
        if 58 in specific_tests or len(specific_tests) == 0:
            cuc_3200_58_module.ControllerUseCase58(test_name="CN-UseCase58-multissp with pressure sensor",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='multiple_ssp_with_pressure_sensors.json')
        if 59 in specific_tests or len(specific_tests) == 0:
            cuc_3200_59_module.ControllerUseCase59(test_name="CN-UseCase59-IgnoreConcurrentZones",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='ignore_global.json')
        if 60 in specific_tests or len(specific_tests) == 0:
            cuc_3200_60_module.ControllerUseCase60(test_name="CN-UseCase60-dynamic_flow",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='across_mainlines.json')
        if 65 in specific_tests or len(specific_tests) == 0:
            cuc_3200_65_module.ControllerUseCase65(test_name="CN-UseCase65-MasterValveOperation",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='use_case_65.json')

        print("########################### ----FINISHED RUNNING PASSING 3200 TESTS---- ###############################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Manual Tests------############################################## #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #     3200 Tests (Requires Manual Device/Use-Case Modification)                                                    #
    #                                                                                                                  #
    #    - THESE USE CASES ARE NOT APART OF NIGHTLY BUILD                                                              #
    #    - These use cases require setting breakpoints at specific locations in the test in order to configure         #
    #      a real-device to be in an expected state for the test.                                                      #
    #    - Above each use case is a list of break points to set inside the use case. The breakpoints should point      #
    #      to a "To Do" item which explains what the user needs to do.                                                 #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    if manual_tests:
        if 11 in specific_tests or len(specific_tests) == 0:
            cuc_3200_11_module.ControllerUseCase11(test_name="CNUseCase11-firmwareupdate",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='back_restore_firmware_updates.json')
        # TODO: Breakpoint at line 546 in step 9, Perform the backup manually at the front panel of the controller
        # TODO: Breakpoint at line 588 in step 11, Perform the restore at the front panel of the controller
        # TODO: Line 546 & 588 both need to be COMMENTED OUT!!
        if 12.1 in specific_tests or len(specific_tests) == 0:
            cuc_3200_12_1_module.ControllerUseCase12_1(test_name="CN-UseCase12_1-backup_restore_from_usb",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file=
                                                       'back_restore_firmware_updates_no_et.json')
        # TODO: This is a stress test for backups/restores with random values. Change amount of tries on line 471
        if 12.2 in specific_tests or len(specific_tests) == 0:
            cuc_3200_12_2_module.ControllerUseCase12_2(test_name="CN-UseCase12_2-Backup/Restore BM Stres Tst",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file=
                                                       'back_restore_firmware_updates_no_et.json')
        # TODO: Breakpoint at line 912, on the get_data() method
        # TODO: Now update your controller from BaseManager from the face plate. (You can also do it from USB), and
        # TODO:     once the controller's display comes back online, then you can continue the test.
        if 22 in specific_tests or len(specific_tests) == 0:
            cuc_3200_22_module.ControllerUseCase22(test_name="CN-UseCase22-MainlinesUpdateV16",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='use_case_22.json')
        if 26 in specific_tests or len(specific_tests) == 0:
            # TODO this test requires some manual inputs for BaseManager
            cuc_3200_26_module.ControllerUseCase26(test_name="CN-UseCase26-UpdatePrimaryZoneZeroRuntime",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='updating_to_v16.json')

        # TODO: Breakpoint at line 1192, on the get_data() method
        # TODO: Now update your controller from BaseManager from the face plate. (You can also do it from USB), and
        # TODO:     once the controller's display comes back online, then you can continue the test.
        if 47 in specific_tests or len(specific_tests) == 0:
            cuc_3200_47_module.ControllerUseCase47(test_name="CN-UseCase47-V12ToV16FirmwareUpdate",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='back_restore_firmware_updates.json')
        if 47.1 in specific_tests or len(specific_tests) == 0:
            cuc_3200_47_1_module.ControllerUseCase47_1(test_name="CN-UseCase47_1-12.34_BMW_Firmware_update",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='empty_configuration.json')
        if 48 in specific_tests or len(specific_tests) == 0:
            cuc_3200_48_module.ControllerUseCase48(test_name="CN-UseCase48-16.0_BMW_Firmware_update",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='empty_configuration.json')
        if 48.1 in specific_tests or len(specific_tests) == 0:
            cuc_3200_48_1_module.ControllerUseCase48_1(test_name="CN-UseCase48_1-BMW_downgrade_upgrade",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='empty_configuration.json')
        if 52 in specific_tests or len(specific_tests) == 0:
            # TODO: Go to the top of each step and modify test based on "To Do" statement at the top of each step.
            cuc_3200_52_module.ControllerUseCase52(test_name="CN-UseCase52-BasicSearchAssignTestRealDevices",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='real_devices.json')
        if 61 in specific_tests or len(specific_tests) == 0:
            # TODO: Needs to consistently pass. Currently it will stop the manual run at the top of the minute 50% of
            # TODO:     the time
            cuc_3200_61_module.ControllerUseCase61(test_name="CN-UseCase61-ManualRunZoneType5",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='use_case_61_basic_flow_test.json')
        if 62 in specific_tests or len(specific_tests) == 0:
            # TODO: GO to step_2 where zones are being configured and change the 4 serial numbers to be the serial
            # TODO:     of your real devices.
            # TODO: Go to step_3 and step_4 and make sure the serial numbers are correct for your real devices.
            cuc_3200_62_module.ControllerUseCase62(test_name="CN-UseCase62-IgnoreGlobalStop",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='real_devices.json')
        if 63 in specific_tests or len(specific_tests) == 0:
            cuc_3200_63_module.ControllerUseCase63(test_name="CN-UseCase63-IgnoreGlobalPause",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='real_devices.json')
        # TODO check the TODO lines in step 10, change your database ID's and firmware updates accordingly
        if 66 in specific_tests or len(specific_tests) == 0:
            cuc_3200_66_module.ControllerUseCase66(test_name="CN-UseCase66-InfiniteFirmwareUpdates",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='back_restore_firmware_updates.json')
        # TODO while the test is running in its loop, hook up to the 3200 and verify that the files are getting deleted
        # TODO once flash memory gets to
        if 67 in specific_tests or len(specific_tests) == 0:
            cuc_3200_67_module.ControllerUseCase67(test_name="CN-UseCase67-InfiniteMinuteLogging",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='minute_log_devices.json')
        # if 68 in specific_tests or len(specific_tests) == 0:
        #     cuc_3200_68_module.ControllerUseCase68(test_name="CN-UseCase68-SecurityFwUpgrades",
        #                                            user_configuration_instance=user_conf,
        #                                            json_configuration_file='use_case_65.json')
        # TODO: After running the test get the flow meter log file and verify the first entry
        # TODO: is not not the total water used
        if 69 in specific_tests or len(specific_tests) == 0:
            cuc_3200_69_module.ControllerUseCase69(test_name="CN-UseCase69-FlowMeterLogTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='basic_mainline_zone_delays.json')

        print("########################### ----FINISHED RUNNING MANUAL 3200 TESTS---- ################################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Failing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if failing_tests:
        if 5.4 in specific_tests or len(specific_tests) == 0:
            # TODO: Failing, use JIRA ZZ-1691 to verify
            cuc_3200_5_4_module.ControllerUseCase5_4(test_name="CN-UseCase5_4-WS_empty_conditions",
                                                     user_configuration_instance=user_conf,
                                                     json_configuration_file='water_budget.json')
        if 39 in specific_tests or len(specific_tests) == 0:
            # TODO still needs a complete rewrite
            cuc_3200_39_module.ControllerUseCase39(test_name="CN-UseCase39-MoistureSensorWatering",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='reboot_bug_test.json')
        #TODO the 43 test need somekind of a second serial port in order to run
        if 43 in specific_tests or len(specific_tests) == 0:
            cuc_3200_43_module.ControllerUseCase43(test_name="CN-UseCase43-verify_data_group_packets",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='update_cn.json')
        if 43.1 in specific_tests or len(specific_tests) == 0:
            cuc_3200_43_1_module.ControllerUseCase43_1(test_name="CN-UseCase43_1-verify_data_group_packets",
                                                       user_configuration_instance=user_conf,
                                                       json_configuration_file='update_cn.json')
        # if 54.4 in specific_tests or len(specific_tests) == 0:
            # TODO this test needs writen to cover Program Zone Pressure Shutdown (THIS TEST DOESN'T EXIST YET?)
            # cuc_3200_54_4_module.ControllerUseCase54_4(test_name="CN-UseCase54-4-UnscheduledFlow",
            #                                        user_configuration_instance=user_conf,
            #                                        json_configuration_file='empty_conditions.json')
        # TODO We need to right an actual test for unscheduled flow on the POC. This is empty until we decide to
        # TODO write one.
        # if 54.4 in specific_tests or len(specific_tests) == 0:
        #     cuc_3200_54_4_module.ControllerUseCase54_4(test_name="CN-UseCase54_4-POCUnscheduledFlow",
        #                                                user_configuration_instance=user_conf,
        #                                                json_configuration_file='empty_conditions.json')

        print("########################### ----FINISHED RUNNING FAILING 3200 TESTS---- ###############################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # #######################################------Future Needed Tests------########################################## #
    # ---------------------------------------------------------------------------------------------------------------- #
    # TODO Unscheduled Flow Started at 54.4
    # TODO Delay stops using pressure
    # TODO Delay between zones using pressure also add design flow
    # TODO Delay between zones using timed also add design flow
    # TODO allocated on demand flow

    # TODO write a test that test manually running zones with POC disabled
    # TODO write a test that makes flow meters jump by 5X
    # TODO write a test that assigns zones to both substation and the 3200
    # TODO write a test that forces a 3200 to lose connection from a substation and than reconnect

    print("######################### ----YOU ARE A WINNER WINNER CHICKEN DINNER---- ##################################")


if __name__ == "__main__":
    run_use_cases_3200(user_configuration_file_name=user_credentials,
                       passing_tests=True,
                       failing_tests=True,
                       manual_tests=False,
                       specific_tests=[34],
                       auto_update_fw=False)
    exit()

