import sys
from datetime import timedelta, datetime, date
import time

# import common.product as helper_methods
from common.configuration import Configuration

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes, csv
from common import helper_methods

__author__ = 'bens'


class UseCaseExample(object):
    """
    Test name:
        - 

    Multi-Controller Setup Guide:
    
    Purpose:
        - 
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. 
        2.
        ...
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - 
        
    Controller Programming:
        - Zones:                0   (0-local, 0-shared) 
        - Master Valves:        0   (0-local, 0-shared)
        - Event Switches:       0   (0-local, 0-shared)
        - Temp Sensors:         0   (0-local, 0-shared)
        - Moisture Sensors:     0   (0-local, 0-shared)
        - Flow Meters:          0   (0-local, 0-shared)
        - Programs:             0
        - Zone Programs:        0
        - Mainlines:            0
        - Pocs:                 0
        
    Substation Programming:
        - Valve BiCoders:       0
        - Moisture BiCoders:    0   
        - Temperature BiCoders: 0
        - Switch BiCoders:      0 
        - Flow BiCoders:        0
        - Pump BiCoders:        0
    """

    def __init__(self, controller_type,  cn_serial_number, controller_firmware_version, fw_database_id, test_name,
                 user_configuration_instance, json_configuration_file, substation_firmware_version,
                 number_of_substations_to_use):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str

        :param cn_serial_number:                 controller serial number \n
        :type cn_serial_number:                  str

        :param controller_firmware_version:     Expected controller firmware version \n
        :type controller_firmware_version:      str

        :param fw_database_id:                  Database ID for controller's firmware version. \n
        :type fw_database_id:                   str

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        
        :param substation_firmware_version:     Expected substation firmware version.
        :type substation_firmware_version:      str
        
        :param number_of_substations_to_use:    Number of substations to use for the use case.
        :type number_of_substations_to_use:     int
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )

        self.controllers = self.config.controllers
        self.substations = self.config.substations

        self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        """
        """
        try:
            self.config.initialize_for_test()
            self.step_1()
            self.step_2()
            self.step_3()
            self.step_4()

        # Handle an exception
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        # Handle successful test
        else:
            helper_methods.print_test_passed(test_name=self.config.test_name)

        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_multiple_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Init controller
            self.controllers[1].initialize_for_test()

            # Init each substation created/configured.
            for substation_address in self.config.substations.keys():
                self.substations[substation_address].init_sb()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substations:
        - uses the Ip address in the user config files to set the ip address
            Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                  incremented 1 minute at a time for the max minutes or until max number of minutes is met.
        - after connection is established increment clock for each controller 1 minute
            Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                  between controllers.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.connect_controller_to_substations(controller=self.controllers[1],
                                                             substations=self.substations)

            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.controllers[1],
                                                                                 substations=self.substations,
                                                                                 max_wait=5)

            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.controllers[1].load_assigned_devices()
            self.substations[1].load_assigned_devices()

            self.controllers[1].do_search_for_all_devices()

            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            # 3200: Address and set default values for Devices
            helper_methods.set_all_device_default_values_on_controller(config=self.config,
                                                                       controller=self.controllers[1])

            # Substation: Set default values for Devices
            helper_methods.set_bicoder_default_values_on_substation(substations=self.substations)

            # Substation: Share devices with controller.
            # i.e., to share valve bicoders:
            # self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSE0011", address=3)
            # self.config.share_sb_device_with_cn(device_type=opcodes.zone, device_serial="TSE0012", address=4)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.controllers[1],
                                                                  substations=self.config.substations,
                                                                  minutes=1)

            for sb_address in self.substations.keys():
                self.substations[sb_address].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]


    #################################
    def step_13(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller agaist information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]