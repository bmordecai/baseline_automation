import sys
from time import sleep

# import common.product as helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes
from common import helper_methods

# import log_handler for logging functionality
from common.logging_handler import log_handler

__author__ = 'bens'


class UseCase4BasicProgramming(object):
    """
    Test name:
        - Substation Basic Programming Use Case 4 \n

    Multi-Controller Setup Guide:
    
    Purpose:
        - Set up a full configuration on the controller and substation \n
        - Reboot the controller \n
        - Verify the programming is still present \n
    
    Coverage:
        Step-by-Step Summary:
        1. Initialize the controller and any substations (in this case 1) to known states.
            - Turn on echo so the commands we send are echoed. \n
            - Turn on sim mode so that we can control the time as we see fit. \n
            - Stop the clock so that it only increments when we tell it to. \n
            - Set the date and time to 1/1/2014 1:00 A.M. so we know what our date time starts at. \n
            - Turn on faux I/O so we aren't using real devices. \n
            - Clear all devices and all programming so we are in known states on the controller and substations. \n
        2. Connect the controller to it's substations. We wait for the connection to be established and then we
           increment the clock to make sure it is also displayed on the controller and substations. \n
        3. Load devices to the controller and substation and set up addresses and default values. \n
            -We load devices using the serial numbers specified in the JSON. \n
            -Do a search on the controller after both all loads are done. \n
            -Increment the clock to make sure all devices have been found and are displayed. \n
            -Share substation devices to controller, setting their addresses and default values: \n

                BiCoders:           Controller Address:       Serial Numbers:
                - Valve (ZN's)      197, 198, 199, 200        "TSQ0081", "TSQ0082", "TSQ0083", "TSQ0084"
                - Valve (MV's)      1, 2, 8                   "TMV0001", "TMV0003", "TMV0004"
                - Flow              2                         "TWF0004"
                - Switch            2                         "TPD0002"
                - Temp              2                         "TAT0002"
                - Moisture          2                         "SB07258"
                - Pump              N/A

        4. Set up Mainlines \n
        5. Set up Point of Connections \n
        6. Set up Programs \n
        7. Group zones onto Programs, setting some primary, linked, and timed zones \n
        8. Set values on various devices so that we can verify them after the reboot. \n
        9. Create start/stop/pause conditions so that we can verify they are kept after a reboot. \n
        10. Increment the clock so that the controller and substations can update their values. \n
        11. Reboot the controller and substation, and then verify that the controller still has it's connection to the
            substation. Set date/time to a known state. \n
        12. Do a self test on our devices on a 3200 and update attributes on our objects \n
        13. Verify that all of our objects match their respective values on the controller and substation \n
            - Verify BaseManager connection. \n
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - Only tests using 1 substation.

    Controller Programming:
        - Zones:                8   (4-local, 4-shared)
        - Master Valves:        3   (0-local, 3-shared)
        - Event Switches:       2   (1-local, 1-shared)
        - Temp Sensors:         2   (1-local, 1-shared)
        - Moisture Sensors:     2   (1-local, 1-shared)
        - Flow Meters:          2   (1-local, 1-shared)
        - Programs:             4
        - Zone Programs:        9
        - Mainlines:            2
        - Pocs:                 2

    Substation Programming:
        - Valve BiCoders:       4
        - Moisture BiCoders:    1
        - Temperature BiCoders: 1
        - Switch BiCoders:      1
        - Flow BiCoders:        1
        - Pump BiCoders:        0
    """

    #################################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str

        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )

        self.run_use_case()

    #################################
    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_controller_test(config_object=self.config)
        
    #################################
    def step_1(self):
        """
        Sync the controller and substation(s) clock
        """
        method = "\n###################     Running " + sys._getframe().f_code.co_name + "    ######################\n"
        print method
        try:
            # set computer time
            date_mngr.set_current_date_to_match_computer()

            # set both the 3200 and the substation to be the same time as your computer
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.SubStations,
                _date=date_mngr.curr_day.date_string_for_controller(),
                _time=date_mngr.curr_day.time_string_for_controller())
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substation(s)
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].add_substation_to_controller(_substation_address=1,
                                                                        _substation=self.config.SubStations[1])
        
            # Increment clocks after connection established
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.

        Controller devices loaded:

            Device Type             Address         Serial Number
            -----------------------------------------------------
            Zones                   1               "TSQ0071"
                                    2               "TSQ0072"
                                    49              "TSQ0073"
                                    50              "TSQ0074"
            -----------------------------------------------------
            Master Valves
            -----------------------------------------------------
            Flow Meters             1               "TWF0003"
            -----------------------------------------------------
            Moisture Sensors        1               "SB05308"
            -----------------------------------------------------
            Temperature Sensors     1               "TAT0001"
            -----------------------------------------------------
            Event Switches          1               "TPD0001"
            -----------------------------------------------------

        Substation BiCoders Shared:

            BiCoder Type            Address on 3200         Serial Number
            -------------------------------------------------------------
            Valve
            (Zones)                 197                     "TSQ0081"
                                    198                     "TSQ0082"
                                    199                     "TSQ0083"
                                    200                     "TSQ0084"

            (Master Valves)         1                       "TMV0001"
                                    2                       "TMV0003"
                                    8                       "TMV0004"

            -------------------------------------------------------------
            Flow                    2                       "TWF0004"
            -------------------------------------------------------------
            Moisture                2                       "SB07258"
            -------------------------------------------------------------
            Temperature             2                       "TAT0002"
            -------------------------------------------------------------
            Switch                  2                       "TPD0002"
            -------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Prior to this step devices were loaded onto both the 3200 and substation. This search is required in order
            # for the 3200 to load devices from sub station.
            self.config.BaseStation3200[1].do_search_for_zones()

            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            # Substation: Set default values for Devices
            self.config.SubStations[1].set_bicoder_default_values()

            ############################################################################################################
            # Share devices from substation to controller. This makes the controller address the devices that it shares
            # with a substation, and then it sets the default values. We share these devices so that we can test and
            # verify that devices shared with a substation will retain their values after a reboot.

            # Zones
            self.config.SubStations[1].valve_bicoders["TSQ0081"].add_to_zone(_address=197)
            self.config.SubStations[1].valve_bicoders["TSQ0082"].add_to_zone(_address=198)
            self.config.SubStations[1].valve_bicoders["TSQ0083"].add_to_zone(_address=199)
            self.config.SubStations[1].valve_bicoders["TSQ0084"].add_to_zone(_address=200)

            # Master Valves
            self.config.SubStations[1].valve_bicoders["TMV0001"].add_to_master_valve(_address=1)
            self.config.SubStations[1].valve_bicoders["TMV0003"].add_to_master_valve(_address=2)
            self.config.SubStations[1].valve_bicoders["TMV0004"].add_to_master_valve(_address=8)

            # Flow Meters
            self.config.SubStations[1].flow_bicoders["TWF0004"].add_to_flow_meter(_address=2)

            # Moisture Sensors
            self.config.SubStations[1].moisture_bicoders["SB07258"].add_to_moisture_sensor(
                _address=2
            )

            # Temperature Sensors
            self.config.SubStations[1].temperature_bicoders["TAT0002"].add_to_temperature_sensor(
                _address=2
            )

            # Event Switch
            self.config.SubStations[1].switch_bicoders["TPD0002"].add_to_event_switch(_address=2)
            #
            ############################################################################################################
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_4(self):
        """
        ############################
        Setup Programs
        ############################
        
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Variables to help with setting up the Programs
            program_start_time_8am_9am_10am_11am = [480, 540, 600, 660]
            program_watering_days_mwf = [0, 1, 0, 1, 0, 1, 1]  # runs monday/wednesday/friday
            program_water_windows = ['111111111111111111111111']
            monthly_water_windows = ['011111100000111111111110', '011111100001111111111111', '011111100001111111111110',
                                     '011111100001111111111110', '011111100001111111111110', '011111100001111111111110',
                                     '011111100001111111111111']

            # Add & Configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_8am_9am_10am_11am)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=False, _mon=True, _tues=False, _wed=True, _thurs=False, _fri=True, _sat=False
            )

            # Add & Configure Program 3
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_start_times(_st_list=program_start_time_8am_9am_10am_11am)
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[3].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[3].set_watering_intervals_to_odd_days()

            # Add & Configure Program 4
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation3200[1].programs[4].set_disabled()
            self.config.BaseStation3200[1].programs[4].set_start_times(_st_list=program_start_time_8am_9am_10am_11am)
            self.config.BaseStation3200[1].programs[4].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[4].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=False, _mon=True, _tues=False, _wed=True, _thurs=False, _fri=True, _sat=False
            )

            # Add & Configure Program 99
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_start_time_8am_9am_10am_11am)
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[99].set_water_window(_ww=monthly_water_windows, _is_weekly=False)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_5(self):
        """
        ############################
        Setup Zones on Programs
        ############################
        
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # ------------------- #
            # Add Program 1 Zones #
            # ------------------- #

            # Zone Program 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=16)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=8)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=2
            )
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_lower_limit_threshold(_percent=24.0)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_one_time_calibration_cycle()

            # Zone Program 2
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)
            
            # ------------------- #
            # Add Program 3 Zones #
            # ------------------- #

            # Zone Program 49
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=49)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_soak_time(_minutes=60)

            # Zone Program 50
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_soak_time(_minutes=60)
            
            # ------------------- #
            # Add Program 4 Zones #
            # ------------------- #
            
            # Zone Program 50
            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_soak_time(_minutes=60)
            
            # -------------------- #
            # Add Program 99 Zones #
            # -------------------- #

            # Zone Program 200
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=200)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_run_time(_minutes=33)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_cycle_time(_minutes=3)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_soak_time(_minutes=13)

            # Zone Program 197
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=197)
            self.config.BaseStation3200[1].programs[99].zone_programs[197].set_as_linked_zone(_primary_zone=200,
                                                                                              _tracking_ratio=50)

            # Zone Program 198
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=198)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_as_linked_zone(_primary_zone=200)

            # Zone Program 199
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=199)
            self.config.BaseStation3200[1].programs[99].zone_programs[199].set_as_linked_zone(_primary_zone=200,
                                                                                              _tracking_ratio=150)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_6(self):
        """
        ############################
        Setup WaterSources
        ############################
        
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add & Configure Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

            # Add & Configure Water Source 8
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[1].water_sources[8].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].set_priority(_priority_for_water_source=3)
            self.config.BaseStation3200[1].water_sources[8].set_monthly_watering_budget(_budget=1000)
            self.config.BaseStation3200[1].water_sources[8].set_water_rationing_to_disabled()
            self.config.BaseStation3200[1].water_sources[8].add_switch_empty_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_switch_empty_condition_to_closed()
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_empty_wait_time(_minutes=450)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #############################
    def step_7(self):
        """
        ############################
        Setup Point of Controls
        ############################
        
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(
                _limit=550, 
                with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(
                _gallons=10, 
                with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1
            )
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=2
            )
            # Add POC 1 to WS 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1
            )
            
            # Add & Configure POC 8
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[8].set_high_flow_limit(
                _limit=75,
                with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].points_of_control[8].set_unscheduled_flow_limit(
                _gallons=5,
                with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].points_of_control[8].add_flow_meter_to_point_of_control(
                _flow_meter_address=2
            )
            self.config.BaseStation3200[1].points_of_control[8].add_master_valve_to_point_of_control(
                _master_valve_address=8
            )
            # Add POC 8 to WS 8
            self.config.BaseStation3200[1].water_sources[8].add_point_of_control_to_water_source(
                _point_of_control_address=8
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_8(self):
        """
        ##################
        setup mainlines
        ##################

        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add & Configure Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(
                _percent=5,
                _with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(
                _percent=20,
                _with_shutdown_enabled=False
            )
            # Add Mainline 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            # Add & Configure Mainline 8
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[1].mainlines[8].set_enabled()
            self.config.BaseStation3200[1].mainlines[8].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[8].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_high_flow_variance_tier_one(
                _percent=20,
                _with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].mainlines[8].set_low_flow_variance_tier_one(
                _percent=5,
                _with_shutdown_enabled=True
            )
            # Add Mainline 8 to POC 8
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(_mainline_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_9(self):
        """
        ######################
        Setup zones on mainlines
        ######################

        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add Zones to Mainline 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=10)
            
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=10)
            
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=49)
            self.config.BaseStation3200[1].zones[49].set_design_flow(_gallons_per_minute=30)
            
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=50)
            self.config.BaseStation3200[1].zones[50].set_design_flow(_gallons_per_minute=30)
            
            # Add Zones to Mainline 8
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=197)
            self.config.BaseStation3200[1].zones[197].set_design_flow(_gallons_per_minute=80)
            
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=198)
            self.config.BaseStation3200[1].zones[198].set_design_flow(_gallons_per_minute=80)
            
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=199)
            self.config.BaseStation3200[1].zones[199].set_design_flow(_gallons_per_minute=80)
            
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=200)
            self.config.BaseStation3200[1].zones[200].set_design_flow(_gallons_per_minute=100)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_10(self):
        """
        ######################
        Setup devices
        ######################
        
        Set up Moisture Sensors \n
            - MS 1: SB05308 \n
                - Moisture value: 10.0 \n
                - Two wire drop: 1.6 \n
            - MS 2: SB07258 \n
                - Moisture value: 26.0 \n
                - Two wire drop: 1.8 \n

        Set up Temperature Sensors \n
            - TS 1: TAT0001 \n
                - Temperature value: 26.0 \n
                - Two wire drop: 1.8 \n

        Set up Event Switches \n
            - SW 1: TPD0001 \n
                - Value: closed \n
                - Two wire drop: 1.8 \n

        Set up Master Valves \n
            - MV 1: TSD0001 \n
                - Booster Pump: true \n
                - Normally Open: closed \n
            - MV 2: TMV0003 \n
                - Normally Open: closed \n
            - MV 8: TMV0004 \n
                - Normally Open: open \n

        Set up Flow Meters \n
            - FM 1: TWF0003 \n
                - Enabled: True \n
                - K-value: 3.10 \n
                - Flow GPM: 25 \n
            - FM 2: TWF0004 \n
                - Enabled: True \n
                - K-value: 5.01 \n
                - Flow GPM: 50 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Moisture Sensors
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=10.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_two_wire_drop_value(_value=1.60)
            self.config.SubStations[1].moisture_bicoders["SB07258"].set_moisture_percent(_percent=26.0)
            self.config.SubStations[1].moisture_bicoders["SB07258"].set_two_wire_drop_value(_value=1.8)

            # Temperature Sensors
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=12.0)
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_two_wire_drop_value(_value=1.9)
            self.config.SubStations[1].temperature_bicoders["TAT0002"].set_temperature_reading(_degrees=26.0)
            self.config.SubStations[1].temperature_bicoders["TAT0002"].set_two_wire_drop_value(_value=1.8)

            # Event Switches
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_two_wire_drop_value(_value=1.1)
            self.config.SubStations[1].switch_bicoders["TPD0002"].set_contact_open()
            self.config.SubStations[1].switch_bicoders["TPD0002"].set_two_wire_drop_value(_value=1.8)

            # Master Valves
            self.config.BaseStation3200[1].master_valves[1].set_booster_enabled()
            self.config.BaseStation3200[1].master_valves[1].set_normally_open_state(_normally_open=opcodes.false)
            self.config.BaseStation3200[1].master_valves[2].set_normally_open_state(_normally_open=opcodes.false)
            self.config.BaseStation3200[1].master_valves[8].set_normally_open_state(_normally_open=opcodes.true)

            # Flow Meters
            self.config.BaseStation3200[1].flow_meters[1].set_enabled()
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_k_value(_value=3.10)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=25)

            self.config.BaseStation3200[1].flow_meters[2].set_enabled()
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_k_value(_value=5.01)
            self.config.SubStations[1].flow_bicoders["TWF0004"].set_flow_rate(_gallons_per_minute=50)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_11(self):
        """
        Create Start/stop/pause conditions \n
            - Verify that a program can have one of each start stop pause condition on it and it can be the same device
              triggering two different events \n
            - Verify that each program can have a device that can be located on either the 3200 or the Sub Station \n

            - Start PG 3 when Moisture Sensor 1 gets above 38 This sensor is on the 3200 \n
            - Pause PG 3 when Moisture Sensor 2 gets below 15 This sensor is on the Sub Station \n
            - Pause PG 3 when Temperature Sensor 1 gets above 48 This sensor is on the 3200 pause 3 minutes\n
            - Pause PG 3 when Temperature Sensor 2 gets below This sensor is on the Sub Station \n
            - Stop PG 3 when Event Switch 1 state changes to open This sensor is on the 3200 \n
            - Stop PG 3 when Event Switch 2  state changes to Closed  This sensor is on the Sub Station \n

            - Verify that all the devices can be on multiple Programs

            - Start PG 99 when Moisture Sensor 1 gets above 38 This sensor is on the 3200 \n
            - Pause PG 99 when Moisture Sensor 2 gets below 15 This sensor is on the Sub Station \n
            - Pause PG 99 when Temperature Sensor 1 gets above 48 This sensor is on the 3200 \n
            - Pause PG 99 when Temperature Sensor 2 gets below This sensor is on the Sub Station \n
            - Stop PG 99 when Event Switch 1 state changes to open This sensor is on the 3200 \n
            - Stop PG 99 when Event Switch 2  state changes to Closed  This sensor is on the Sub Station \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # ----------------------------------- #
            # Program 3 Moisture Start Conditions #
            # ----------------------------------- #

            self.config.BaseStation3200[1].programs[3].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[3].moisture_start_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[3].moisture_start_conditions[1].set_moisture_threshold(_percent=38)

            self.config.BaseStation3200[1].programs[3].add_moisture_start_condition(_moisture_sensor_address=2)
            self.config.BaseStation3200[1].programs[3].moisture_start_conditions[2].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[3].moisture_start_conditions[2].set_moisture_threshold(_percent=15)

            # -------------------------------------- #
            # Program 3 Temperature Pause Conditions #
            # -------------------------------------- #

            self.config.BaseStation3200[1].programs[3].add_temperature_pause_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[3].temperature_pause_conditions[1].set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[3].temperature_pause_conditions[1].set_temperature_threshold(_degrees=48)
            self.config.BaseStation3200[1].programs[3].temperature_pause_conditions[1].set_temperature_pause_time(_minutes=3)

            self.config.BaseStation3200[1].programs[3].add_temperature_pause_condition(_temperature_sensor_address=2)
            self.config.BaseStation3200[1].programs[3].temperature_pause_conditions[2].set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[3].temperature_pause_conditions[2].set_temperature_threshold(_degrees=33)

            # -------------------------------- #
            # Program 3 Switch Stop Conditions #
            # -------------------------------- #

            self.config.BaseStation3200[1].programs[3].add_switch_stop_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[3].event_switch_stop_conditions[1].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[3].add_switch_stop_condition(_event_switch_address=2)
            self.config.BaseStation3200[1].programs[3].event_switch_stop_conditions[2].set_switch_mode_to_closed()

            # ------------------------------------ #
            # Program 99 Moisture Start Conditions #
            # ------------------------------------ #

            self.config.BaseStation3200[1].programs[99].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[99].moisture_start_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[99].moisture_start_conditions[1].set_moisture_threshold(_percent=38)

            self.config.BaseStation3200[1].programs[99].add_moisture_start_condition(_moisture_sensor_address=2)
            self.config.BaseStation3200[1].programs[99].moisture_start_conditions[2].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[99].moisture_start_conditions[2].set_moisture_threshold(_percent=15)

            # --------------------------------------- #
            # Program 99 Temperature Pause Conditions #
            # --------------------------------------- #

            self.config.BaseStation3200[1].programs[99].add_temperature_pause_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[99].temperature_pause_conditions[1].set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[99].temperature_pause_conditions[1].set_temperature_threshold(_degrees=48)

            self.config.BaseStation3200[1].programs[99].add_temperature_pause_condition(_temperature_sensor_address=2)
            self.config.BaseStation3200[1].programs[99].temperature_pause_conditions[2].set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[99].temperature_pause_conditions[2].set_temperature_threshold(_degrees=33)

            # --------------------------------- #
            # Program 99 Switch Stop Conditions #
            # --------------------------------- #

            self.config.BaseStation3200[1].programs[99].add_switch_stop_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[99].event_switch_stop_conditions[1].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[99].add_switch_stop_condition(_event_switch_address=2)
            self.config.BaseStation3200[1].programs[99].event_switch_stop_conditions[2].set_switch_mode_to_closed()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        ###############################
        Verify the entire configuration
        ###############################

        - Get information for each object from controller
        - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Increment clocks to save configuration
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Verify configuration
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.SubStations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_13(self):
        """
        Reboot the controller \n
        Stop the clock \n
        Wait for the controller and substation to confirm their reconnected status. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Reboot Controller & Substation
            self.config.BaseStation3200[1].do_reboot_controller()

            # Wait for 3200 and substation to reconnect
            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.config.BaseStation3200[1],
                                                                                 substations=self.config.BaseStation3200[1].substations,
                                                                                 max_wait=60)
            # Reboot Substation
            self.config.SubStations[1].do_reboot_controller()

            # Wait for 3200 and substation to reconnect
            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.config.BaseStation3200[1],
                                                                                 substations=self.config.BaseStation3200[1].substations,
                                                                                 max_wait=60)

            # Stop clocks after reconnecting
            self.config.BaseStation3200[1].stop_clock()
            self.config.SubStations[1].stop_clock()

            # Reset date/time to match test engine
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                _date=date_mngr.curr_day.date_string_for_controller(),
                _time=date_mngr.curr_day.time_string_for_controller()
            )

            # Wait for controller and substation to reconnect
            self.config.BaseStation3200[1].wait_for_controller_and_substations_connection_status(
                _substation_address=1,
                _max_wait=5
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_14(self):
        """
        Do a self test on all devices. \n
        Increment the clock by 1 minute to allow the controller time to update values. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()

            for moisture_sensors in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[moisture_sensors].bicoder.self_test_and_update_object_attributes()

            for master_valves in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[master_valves].bicoder.self_test_and_update_object_attributes()

            for flow_meters in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseStation3200[1].flow_meters[flow_meters].bicoder.self_test_and_update_object_attributes()

            for event_switches in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseStation3200[1].event_switches[event_switches].bicoder.self_test_and_update_object_attributes()

            for temperature_sensors in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[temperature_sensors].bicoder.self_test_and_update_object_attributes()

            for pump in sorted(self.config.BaseStation3200[1].pumps.keys()):
                self.config.BaseStation3200[1].pumps[pump].bicoder.self_test_and_update_object_attributes()

            for pressure_sensor in sorted(self.config.BaseStation3200[1].pressure_sensors.keys()):
                self.config.BaseStation3200[1].pressure_sensors[pressure_sensor].bicoder.self_test_and_update_object_attributes()

            # Increment clocks to save configuration
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        Increment the clock and then verify the full configuration again on both the controller and substation. \n
        Verify the entire controller configuration again. \n
        Verify the entire substation configuration again. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Increment clocks to save configuration
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Do final verification of configuration after self-testing devices to update two wire attrs.
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.SubStations[1].verify_full_configuration()

            # Verify still connected to BaseManager.
            self.config.BaseStation3200[1].basemanager_connection[1].wait_for_bm_connection()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
