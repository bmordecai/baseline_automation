import sys
from time import sleep

# import common.product as helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes
from common import helper_methods

# import log_handler for logging functionality
from common.logging_handler import log_handler

__author__ = 'bens'


class UseCase3DisconnectReconnect(object):
    """
    Test name:
        - Substation Disconnect/Reconnection with 3200.

    Multi-Controller Setup Guide:
    
    Purpose:
        - Test that the 3200 and Substation will actively attempt to reconnect after losing connection to each other.
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Disable substation on 3200 and verify:
            - Substation is disconnected from 3200 via BM status
            - 3200 disconnected from substation status
        
        2. Re-enable substation on 3200 and verify:
            - 3200 is connected to substation via status
            - Substation is connected via BM status
            
        3. Reboot 3200 and wait up to 5 minutes verifying at 1 minute intervals:
            - 3200 is connected to substation via status
            - Substation is connected via BM status
            
        4. Reboot Substation and wait up to 5 minutes verifying at 1 minute intervals:
            - 3200 is connected to substation via status
            - Substation is connected via BM status
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - Currently on a basic disable/enable substation on the 3200, it takes roughly 2-6 increment clock calls per
          controller to get the expected status.
          
        - Currently on a 3200 reboot, it takes 4-6 increment clock calls per controller to get the expected status.
        
        - Currently on a Substation reboot, it takes 11-12 increment clock calls per controller to get the expected 
          status.
    """

    #################################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str

        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )

        self.run_use_case()

    #################################
    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Connect controller to substations.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + \
                 "    ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_substation_to_controller(_substation_address=1,
                                                                        _substation=self.config.SubStations[1])

            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Disable substation on 3200 and verify:
            - Substation is disconnected from 3200 via BM status
            - 3200 disconnected from substation status
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + \
                 "    ######################"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            # Disconnect 3200 from Substation
            self.config.BaseStation3200[1].set_connection_to_substation_as_disconnected(_substation_address=1)
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Re-enable substation on 3200 and verify:
            - 3200 is connected to substation via status
            - Substation is connected via BM status
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            # Connect 3200 to Substation
            self.config.BaseStation3200[1].set_connection_to_substation_as_connected(_substation_address=1)
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        Reboot 3200 and wait up to 20 minutes verifying at 1 minute intervals:
            - 3200 is connected to substation via status
            - Substation is connected via BM status
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            # Reboot 3200
            self.config.BaseStation3200[1].do_reboot_controller()

            # Wait for 3200 and substation to reconnect
            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.config.BaseStation3200[1],
                                                                                 substations=self.config.BaseStation3200[1].substations,
                                                                                 max_wait=20)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        Reboot Substation and wait up to 20 minutes verifying at 1 minute intervals:
            - 3200 is connected to substation via status
            - Substation is connected via BM status
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            # Reboot Substation
            self.config.SubStations[1].do_reboot_controller()

            # Wait for status change
            helper_methods.wait_for_controller_and_substations_connection_status(controller=self.config.BaseStation3200[1],
                                                                                 substations=self.config.BaseStation3200[1].substations,
                                                                                 max_wait=20)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
