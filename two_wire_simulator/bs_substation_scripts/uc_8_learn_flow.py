import sys
from datetime import timedelta, datetime, date
from time import sleep

# import common.product as helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes, csv
from common import helper_methods

# import log_handler for logging functionality
from common.logging_handler import log_handler


__author__ = 'bens'


class UseCase8LearnFlow(object):
    """
    Test name:
        - Substation Learn Flow Use Case

    Multi-Controller Setup Guide:
    
    Purpose:
        - Demonstrate ability to learn flow across multiple mainlines at the same time.
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Initializes controller and substation.
        2. Share substation devices to controller:
        
            BiCoders:           Controller Address:       Serial Numbers:
            - Valve (ZN's)      6, 7, 8, 9, 10            "TSD0006", "TSD0007", "TSD0008", "TSD0009", "TSD0010"
            - Valve (MV's)      N/A
            - Flow              2                         "FM00002"
            - Switch            N/A
            - Temp              N/A
            - Moisture          N/A
            - Pump              N/A
            
        3. Configures POCs, Mainlines, Programs and Program Zones.
        4. Sets the flow rate of flow meters to 0.
        5. Verifies a failed learn flow.
        6. Sets the flow rate of flow meters back to a value > 0.
        7. Verifies a successful learn flow.
        8. Verifies the final configuration for the controller and substation.
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - Only tests using 1 substation.
        
    Controller Programming:
        - Zones:                10  (5-local, 5-shared) 
        - Master Valves:        2   (2-local, 0-shared)
        - Event Switches:       1   (1-local, 0-shared)
        - Temp Sensors:         1   (1-local, 0-shared)
        - Moisture Sensors:     1   (1-local, 0-shared)
        - Flow Meters:          2   (1-local, 1-shared)
        - Programs:             2
        - Zone Programs:        10
        - Mainlines:            2
        - Pocs:                 2
        
    Substation Programming:
        - Valve BiCoders:       5
        - Moisture BiCoders:    0   
        - Temperature BiCoders: 0
        - Switch BiCoders:      0 
        - Flow BiCoders:        1
        - Pump BiCoders:        0
    """

    #################################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )

        self.run_use_case()

    #################################
    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        helper_methods.print_method_name()
        try:
            # set computer time
            date_mngr.set_current_date_to_match_computer()

            # set both the 3200 and the substation to be the same time as your computer
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.SubStations,
                _date=date_mngr.curr_day.date_string_for_controller(),
                _time=date_mngr.curr_day.time_string_for_controller())
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substations:
        - uses the Ip address in the user config files to set the ip address
            Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                  incremented 1 minute at a time for the max minutes or until max number of minutes is met.
        - after connection is established increment clock for each controller 1 minute
            Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                  between controllers.

        """
        helper_methods.print_method_name()
        try:
            # Connect 3200 to substation 1
            self.config.BaseStation3200[1].add_substation_to_controller(
                _substation_address=1,
                _substation=self.config.SubStations[1])
        
            # Increment clocks after connection established
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.
        
        Controller devices loaded:
                        
            Device Type             Address         Serial Number
            -----------------------------------------------------
            Zones                   1               "TSD0001"
                                    2               "TSD0002"
                                    3               "TSD0003"
                                    4               "TSD0004"
                                    5               "TSD0005"
            -----------------------------------------------------
            Master Valves           1               "TMV0001"
                                    2               "TMV0002"
            -----------------------------------------------------
            Flow Meters             1               "FM00001"
            -----------------------------------------------------
            Moisture Sensors        1               "SB00001"
            -----------------------------------------------------
            Temperature Sensors     1               "TS00001"
            -----------------------------------------------------
            Event Switches          1               "SW00001"
            -----------------------------------------------------

        Substation BiCoders Shared:
        
            BiCoder Type            Address on 3200         Serial Number
            -------------------------------------------------------------
            Valve                   6                       "TSD0006"
                                    7                       "TSD0007"
                                    8                       "TSD0008"
                                    9                       "TSD0009"
                                    10                      "TSD0010"
            -------------------------------------------------------------
            Flow                    2                       "FM00002"
            -------------------------------------------------------------
            Moisture
            -------------------------------------------------------------
            Temperature
            -------------------------------------------------------------
            Switch
            -------------------------------------------------------------
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Load devices from substation
            self.config.BaseStation3200[1].do_search_for_zones()

            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            # Substation: Share devices with controller.
            
            ############################################################################################################
            # Share devices from substation to controller. This makes the controller address the devices that it shares
            # with a substation, and then it sets the default values. We share these devices so that we can test and
            # verify that devices shared with a substation will retain their values after a reboot.

            # Zones
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0006"].add_to_zone(_address=6)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0007"].add_to_zone(_address=7)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0008"].add_to_zone(_address=8)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0009"].add_to_zone(_address=9)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0010"].add_to_zone(_address=10)

            # Flow Meters
            self.config.BaseStation3200[1].substations[1].flow_bicoders['FM00002'].add_to_flow_meter(_address=2)
            #
            ############################################################################################################
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:

            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()

            for moisture_sensors in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[moisture_sensors].bicoder.self_test_and_update_object_attributes()

            for master_valves in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[master_valves].bicoder.self_test_and_update_object_attributes()

            for flow_meters in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseStation3200[1].flow_meters[flow_meters].bicoder.self_test_and_update_object_attributes()

            for event_switches in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseStation3200[1].event_switches[event_switches].bicoder.self_test_and_update_object_attributes()

            for temperature_sensors in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[temperature_sensors].bicoder.self_test_and_update_object_attributes()

            for pump in sorted(self.config.BaseStation3200[1].pumps.keys()):
                self.config.BaseStation3200[1].pumps[pump].bicoder.self_test_and_update_object_attributes()

            for pressure_sensor in sorted(self.config.BaseStation3200[1].pressure_sensors.keys()):
                self.config.BaseStation3200[1].pressure_sensors[pressure_sensor].bicoder.self_test_and_update_object_attributes()
            
            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            # Verify full config
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        Setup Programs
        ############################

        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        helper_methods.print_method_name()
        try:
            program_start_times = [600]                     # 10:00am start time
            program_water_windows = ['111111111111111111111111']

            # Add & Configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_times)

            # Add & Configure Program 99
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[99].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[99].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[99].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_start_times)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        1) Set up Zone Programs.

        Zone Program 1:
            - Zone:                 1
            - Program:              1
            - Runtime:              960 (16 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Primary Zone)

        Zone Program 2:
            - Zone:                 2
            - Program:              1
            - Runtime:              960 (16 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Linked)

        Zone Program 3:
            - Zone:                 3
            - Program:              1
            - Runtime:              960 (16 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Linked)

        Zone Program 4:
            - Zone:                 4
            - Program:              1
            - Runtime:              960 (16 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Linked)

        Zone Program 5:
            - Zone:                 5
            - Program:              1
            - Runtime:              960 (16 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Linked)

        Zone Program 6:
            - Zone:                 6
            - Program:              99
            - Runtime:              300 (5 minutes)
            - Cycle Time:           120 (2 minutes)
            - Soak Time:            120 (2 minutes)
            - Primary Zone Number:  0 (Timed)

        Zone Program 7:
            - Zone:                 7
            - Program:              99
            - Runtime:              300 (5 minutes)
            - Cycle Time:           120 (2 minutes)
            - Soak Time:            120 (2 minutes)
            - Primary Zone Number:  0 (Timed)

        Zone Program 8:
            - Zone:                 8
            - Program:              99
            - Runtime:              300 (5 minutes)
            - Cycle Time:           120 (2 minutes)
            - Soak Time:            120 (2 minutes)
            - Primary Zone Number:  0 (Timed)

        Zone Program 9:
            - Zone:                 9
            - Program:              99
            - Runtime:              300 (5 minutes)
            - Cycle Time:           120 (2 minutes)
            - Soak Time:            120 (2 minutes)
            - Primary Zone Number:  0 (Timed)

        Zone Program 10:
            - Zone:                 10
            - Program:              99
            - Runtime:              300 (5 minutes)
            - Cycle Time:           120 (2 minutes)
            - Soak Time:            120 (2 minutes)
            - Primary Zone Number:  0 (Timed)
        """
        helper_methods.print_method_name()
        try:
            # ------------------- #
            # Add Program 1 Zones #
            # ------------------- #

            # Zone Program 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=16)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            # Zone Program 2
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)
            
            # Zone Program 3
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            # Zone Program 4
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1)
              
            # Zone Program 5
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_as_linked_zone(_primary_zone=1)

            # -------------------- #
            # Add Program 99 Zones #
            # -------------------- #

            # Zone Program 6
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[99].zone_programs[6].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[6].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[99].zone_programs[6].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[99].zone_programs[6].set_soak_time(_minutes=2)

            # Zone Program 7
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[99].zone_programs[7].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[7].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[99].zone_programs[7].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[99].zone_programs[7].set_soak_time(_minutes=2)

            # Zone Program 8
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[99].zone_programs[8].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[8].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[99].zone_programs[8].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[99].zone_programs[8].set_soak_time(_minutes=2)

            # Zone Program 9
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=9)
            self.config.BaseStation3200[1].programs[99].zone_programs[9].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[9].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[99].zone_programs[9].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[99].zone_programs[9].set_soak_time(_minutes=2)

            # Zone Program 10
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=10)
            self.config.BaseStation3200[1].programs[99].zone_programs[10].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[10].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[99].zone_programs[10].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[99].zone_programs[10].set_soak_time(_minutes=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ############################
        Setup WaterSources
        ############################

        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

            # Add & Configure Water Source 8
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[1].water_sources[8].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[8].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[8].set_water_rationing_to_enabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ############################
        Setup Points of Control
        ############################

        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(
                _limit=550,
                with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(
                _gallons=10,
                with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1
            )
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1
            )
            # Add POC 1 to WS 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1
            )

            # Add & Configure POC 8
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[8].set_high_flow_limit(
                _limit=550,
                with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].points_of_control[8].set_unscheduled_flow_limit(
                _gallons=10,
                with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].points_of_control[8].add_flow_meter_to_point_of_control(
                _flow_meter_address=2
            )
            self.config.BaseStation3200[1].points_of_control[8].add_master_valve_to_point_of_control(
                _master_valve_address=2
            )
            # Add POC 8 to WS 8
            self.config.BaseStation3200[1].water_sources[8].add_point_of_control_to_water_source(
                _point_of_control_address=8
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ############################
        Setup Mainlines
        ############################

        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=20)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(
                _percent=5,
                _with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(
                _percent=20,
                _with_shutdown_enabled=True
            )
            # Add Mainline 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            # Add & Configure Mainline 8
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[1].mainlines[8].set_enabled()
            self.config.BaseStation3200[1].mainlines[8].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[8].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_high_flow_variance_tier_one(
                _percent=10,
                _with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].mainlines[8].set_low_flow_variance_tier_one(
                _percent=5,
                _with_shutdown_enabled=False
            )
            # Add Mainline 8 to POC 8
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(_mainline_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_10(self):
        """
        ######################
        Setup zones on mainlines
        ######################

        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
                
        PG 1 -> ML 1
        PG 99 -> ML 8
        
        Zones 1-5: 2 GPM
        Zones 6-10: 5 GPM
        """
        helper_methods.print_method_name()
        try:
            # Add Zones to Mainline 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=2)
            
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=2)
            
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=2)
            
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=4)
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=2)
            
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=5)
            self.config.BaseStation3200[1].zones[5].set_design_flow(_gallons_per_minute=2)

            # Add Zones to Mainline 8
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=6)
            self.config.BaseStation3200[1].zones[6].set_design_flow(_gallons_per_minute=5)
            
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=7)
            self.config.BaseStation3200[1].zones[7].set_design_flow(_gallons_per_minute=5)
             
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=8)
            self.config.BaseStation3200[1].zones[8].set_design_flow(_gallons_per_minute=5)
            
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=9)
            self.config.BaseStation3200[1].zones[9].set_design_flow(_gallons_per_minute=5)
            
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=10)
            self.config.BaseStation3200[1].zones[10].set_design_flow(_gallons_per_minute=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        1) Verify both controller and substation's configuraitons after all programming is complete.
        """
        helper_methods.print_method_name()
        try:
            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            # Verify full config
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        1) Configure each flow meter to have a flow rate of: 0
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0)
            self.config.BaseStation3200[1].substations[1].flow_bicoders["FM00002"].set_flow_rate(_gallons_per_minute=0)

            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            # Update shared flow meter's values.
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        1) Learn Flow Failure.
        """
        helper_methods.print_method_name()
        try:
            date_mngr.set_current_date_to_match_computer()

            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                _date=date_mngr.curr_day.date_string_for_controller(),
                _time="07:59:00")

            self.config.BaseStation3200[1].programs[1].set_learn_flow_to_start()
            self.config.BaseStation3200[1].programs[99].set_learn_flow_to_start()

            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=2)

            # this is looking at zone concurrency because the controller can only run one zone at a time
            # only 1 zone should be learning flow
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_learning_flow()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_learning_flow()

            # -------------------
            # VERIFY ZONE STATUS'
            # -------------------
            
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_learning_flow()

            for zone in range(2, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0006"].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0007"].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0008"].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0009"].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0010"].statuses.verify_status_is_done()

            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=2)

            # stop both Programs and reset zone concurrency to 2 on the controller
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[99].set_program_to_stop()

            # these message get generated because we stop the program therefore the message is zones didnt learn flow
            # we need to clear theme to continue with the test
            
            # Program 1 zone progs
            for zone in range(2, 6):
                self.config.BaseStation3200[1].zones[zone].messages.verify_learn_flow_complete_error_message()
                self.config.BaseStation3200[1].zones[zone].messages.clear_learn_flow_complete_error_message()
                
            # Program 99 zone progs
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].messages.verify_learn_flow_complete_error_message()
                self.config.BaseStation3200[1].zones[zone].messages.clear_learn_flow_complete_error_message()

            # change controller to have two have a total of two concurrent zones
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=2)

            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=2)

            date_mngr.set_current_date_to_match_computer()
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                _date=date_mngr.curr_day.date_string_for_controller(),
                _time="07:59:00")

            # restart learn flow
            self.config.BaseStation3200[1].programs[1].set_learn_flow_to_start()
            self.config.BaseStation3200[1].programs[99].set_learn_flow_to_start()

            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_learning_flow()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_learning_flow()

            # -------------------
            # VERIFY ZONE STATUS'
            # -------------------

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_learning_flow()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_learning_flow()
            
            for zone in range(2, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()
                
            for zone in range(7, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0006"].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0007"].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0008"].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0009"].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0010"].statuses.verify_status_is_done()

            not_done = True
            while not_done:

                zones_still_running = False

                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone_ad in self.config.BaseStation3200[1].zones.keys():
                    
                    # zone
                    zone = self.config.BaseStation3200[1].zones[zone_ad]
                    
                    # get last zone status
                    zone.get_data()

                    # for each zone get the ss per zone
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    if zone.statuses.status_is_watering() or zone.statuses.status_is_learning_flow():
                        zone.seconds_zone_ran += 60  # using 60 so that we are in seconds

                    # set flag not all zones are done
                    if zone.statuses.status_is_not_done_watering() and zone.statuses.status_is_not_error():
                        zones_still_running = True

                # ----- END FOR ----------------------------------------------------------------------------------------

                # this could say if zones_still_running:
                if zones_still_running:
                    not_done = True
                    
                    # Increment clocks
                    helper_methods.increment_controller_substation_clocks(
                        controller=self.config.BaseStation3200[1],
                        substations=self.config.BaseStation3200[1].substations,
                        minutes=1)
                    
                    self.config.BaseStation3200[1].verify_date_and_time()
                    self.config.BaseStation3200[1].substations[1].verify_date_and_time()
                else:
                    not_done = False
                    
            # ---------------------------------
            # VERIFY ZONE PROGRAM ERROR MESSAGE
            # ---------------------------------

            # self.config.BaseStation3200[1].do_increment_clock(minutes=40)
            # Program 1 zone progs
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].messages.verify_learn_flow_complete_error_message()
                self.config.BaseStation3200[1].zones[zone].messages.clear_learn_flow_complete_error_message()

            # Program 99 zone progs
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].messages.verify_learn_flow_complete_error_message()
                self.config.BaseStation3200[1].zones[zone].messages.clear_learn_flow_complete_error_message()

            # ----------------------------
            # VERIFY PROGRAM ERROR MESSAGE
            # ----------------------------

            self.config.BaseStation3200[1].programs[1].messages.verify_learn_flow_complete_errors_message()
            self.config.BaseStation3200[1].programs[1].messages.clear_learn_flow_complete_errors_message()
            
            self.config.BaseStation3200[1].programs[99].messages.verify_learn_flow_complete_errors_message()
            self.config.BaseStation3200[1].programs[99].messages.clear_learn_flow_complete_errors_message()

            # check to see if the time each zone took to learn flow is correct
            seconds_learning_flow = (self.config.BaseStation3200[1].mainlines[1].ft * 60) + 60
            for zone_ad in range(1, 6):
                zone = self.config.BaseStation3200[1].zones[zone_ad]
                
                if seconds_learning_flow != zone.seconds_zone_ran:
                    print ("Zone " + str(zone_ad) + " took " + str(zone.seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow) + " seconds")
                else:
                    print ("Zone " + str(zone_ad) + " took " + str(zone.seconds_zone_ran) +
                           " seconds to learn flow")

            seconds_learning_flow = (self.config.BaseStation3200[1].mainlines[8].ft * 60) + 60
            for zone_ad in range(6, 11):
                zone = self.config.BaseStation3200[1].zones[zone_ad]
                
                if seconds_learning_flow != zone.seconds_zone_ran:
                    print ("Zone " + str(zone_ad) + " took " + str(zone.seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow))
                else:
                    print ("Zone " + str(zone_ad) + " took " + str(zone.seconds_zone_ran) +
                           " seconds to learn flow")
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        1) Update flow meter flow rates.
        
        FM 1 (FM00001) - 50 GPM
        FM 2 (FM00002) - 20 GPM
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50.0)
            self.config.BaseStation3200[1].substations[1].flow_bicoders["FM00002"].set_flow_rate(_gallons_per_minute=20.0)

            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            # Update shared flow meter's values.
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        1) Learn Flow Success.
        """
        helper_methods.print_method_name()
        try:
            date_mngr.set_current_date_to_match_computer()

            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                _date=date_mngr.curr_day.date_string_for_controller(),
                _time="07:59:00")

            self.config.BaseStation3200[1].programs[1].set_learn_flow_to_start()
            self.config.BaseStation3200[1].programs[99].set_learn_flow_to_start()

            # Increment clocks
            # TODO read how long each zone should run and than calculate against runtime plus pipe file time
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            # this is looking at zone concurrency because the controller can only run one zone at a time
            # only 1 zone should be learning flow
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_learning_flow()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_learning_flow()

            not_done = True
            while not_done:

                zones_still_running = False

                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone_ad in self.config.BaseStation3200[1].zones.keys():

                    # zone
                    zone = self.config.BaseStation3200[1].zones[zone_ad]

                    # get last zone status
                    zone.get_data()

                    # for each zone get the ss per zone
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    if zone.statuses.status_is_watering() or zone.statuses.status_is_learning_flow():
                        zone.seconds_zone_ran += 60  # using 60 so that we are in seconds

                    # set flag not all zones are done
                    if zone.statuses.status_is_not_done_watering() and zone.statuses.status_is_not_error():
                        zones_still_running = True

                # ----- END FOR ----------------------------------------------------------------------------------------

                # this could say if zones_still_running:
                if zones_still_running:
                    not_done = True

                    # Increment clocks
                    helper_methods.increment_controller_substation_clocks(
                        controller=self.config.BaseStation3200[1],
                        substations=self.config.BaseStation3200[1].substations,
                        minutes=1)

                    self.config.BaseStation3200[1].verify_date_and_time()
                    self.config.BaseStation3200[1].substations[1].verify_date_and_time()
                else:
                    not_done = False

            # ----------------------------------------------
            # VERIFY ZONE PROGRAM LEARN FLOW SUCCESS MESSAGE
            # ----------------------------------------------
            
            # Update design flows to expected learn-flow outcome values so that the message verifiers work.
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].df = 50.0
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].df = 20.0

            # self.config.BaseStation3200[1].do_increment_clock(minutes=40)
            
            # Program 1 zone progs
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].messages.verify_learn_flow_complete_success_message()
                self.config.BaseStation3200[1].zones[zone].messages.clear_learn_flow_complete_success_message()

            # Program 99 zone progs
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].messages.verify_learn_flow_complete_success_message()
                self.config.BaseStation3200[1].zones[zone].messages.clear_learn_flow_complete_success_message()

            # -----------------------------------------
            # VERIFY PROGRAM LEARN FLOW SUCCESS MESSAGE
            # -----------------------------------------

            self.config.BaseStation3200[1].programs[1].messages.verify_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].programs[1].messages.clear_learn_flow_complete_success_message()

            self.config.BaseStation3200[1].programs[99].messages.verify_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].programs[99].messages.clear_learn_flow_complete_success_message()

            # check to see if the time each zone took to learn flow is correct
            seconds_learning_flow = (self.config.BaseStation3200[1].mainlines[1].ft * 60) + 60
            for zone_ad in range(1, 6):
                zone = self.config.BaseStation3200[1].zones[zone_ad]

                if seconds_learning_flow != zone.seconds_zone_ran:
                    print ("Zone " + str(zone_ad) + " took " + str(zone.seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow) + " seconds")
                else:
                    print ("Zone " + str(zone_ad) + " took " + str(zone.seconds_zone_ran) +
                           " seconds to learn flow")

            seconds_learning_flow = (self.config.BaseStation3200[1].mainlines[8].ft * 60) + 60
            for zone_ad in range(6, 11):
                zone = self.config.BaseStation3200[1].zones[zone_ad]

                if seconds_learning_flow != zone.seconds_zone_ran:
                    print ("Zone " + str(zone_ad) + " took " + str(zone.seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow))
                else:
                    print ("Zone " + str(zone_ad) + " took " + str(zone.seconds_zone_ran) +
                           " seconds to learn flow")
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        1) Verify POC 8 received an unscheduled flow shutdown message. This is because the first set of zones got done
           before the second set and there is still water flowing to POC 8. 
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            self.config.BaseStation3200[1].points_of_control[8].messages.verify_unscheduled_flow_shutdown_message()
            self.config.BaseStation3200[1].points_of_control[8].messages.clear_unscheduled_flow_shutdown_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        1) Update design flows for zones to match expected values:
        
            Zones 1-5: 50.0 GPM
            Zones 6-10: 20.0 GPM
            
        2) Verify controller and substation configurations after learn flow success/failure.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + " ######################\n"
        print method
        try:
            # Update design flows to expected learn-flow outcome values so that the message verifiers work.
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].df = 50.0
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].df = 20.0
                
            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            # Verify Controller configuration
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
