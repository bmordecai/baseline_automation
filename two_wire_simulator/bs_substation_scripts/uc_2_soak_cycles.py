import sys
from time import sleep

# import common.product as helper_methods
from common.configuration import Configuration
from common.objects.programming.pg_3200 import PG3200
from common.objects.programming.zp import ZoneProgram

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes
from common import helper_methods

# import log_handler for logging functionality
from common.logging_handler import log_handler

__author__ = 'bens'


class UseCase2SoakCycles(object):
    """
    Test name:
        - Substation Soak Cycles

    Multi-Controller Setup Guide:
    
    Purpose:
        - Test and verify that shared Zones between 3200 and Substation return return the same status whether being 
          retrieved through the 3200 or through the Substation.
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Load devices onto both controllers.
        
        2. Search address and set default values for all devices on 3200.
        
        3. Search and sset default values for all devices on Substations.
        
        4. Setup Programs and Program Zones on 3200.
        
        5. Increment clocks forward 1 minute and verify:
            - Configuration on 3200
            - Configuration on Substation
            
        6. Verify initial Zone status' on 3200 and Substation prior to starting both Programs.
        
        7. Start the Programs and verify Zone status' update to expected watering status.
        
        8. Simulate watering for 2 hours at 5 minute intervals and verify:
            - Zone status' match between 3200 and Substation
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - This test assumes use of only 1 Substation and 1 3200
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        

        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )

        self.run_use_case()

    def run_use_case(self):
        """
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_2(self):
        """
        Connect controller to substations.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + \
                 "    ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_substation_to_controller(_substation_address=1,
                                                                        _substation=self.config.SubStations[1])

            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        1) Load devices on both Substation and 3200.
        2) Search, address and set default values for devices on 3200.
        3) Search and set default values for devices on Substation.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + \
                 "    ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_search_for_zones()

            # Increment the clocks so the search updates available bicoders in the controller
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            # Set the default values for the substation bicoders and assign them on the 3200
            self.config.BaseStation3200[1].substations[1].set_bicoder_default_values()
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0003"].add_to_zone(_address=3)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0004"].add_to_zone(_address=4)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0005"].add_to_zone(_address=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_4(self):
        """
        1) Create mainline objects for Program.
        2) Create 2 Programs.
            - Program 1 and 2:
                + Enabled
                + Start time of 8pm
                + Max concurrent zones: 1
        3) Set controller max concurrent zones to 2
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # For loop to create 8 Mainline objects, there are always 8.
            for mainline_address in range(1, 9):
                # Create each Mainline and store into the dictionary using the address as a lookup key
                self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=mainline_address)

            # Create the programs that the zones will be placed on
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=[1200])
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=[1200])
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=1)

            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_5(self):
        """
        For 3200, configure 5 Zone Programs.
        
        - Program 1:
            * Zone Program 1:
                + Runtime: 3600
                + Cycle Time: 600
                + Soak Time: 1500
                + Mode: Primary Zone 1
            * Zone Program 2:
                + Runtime Ratio: 200%
                + Mode: Linked to 1
            * Zone Program 3:
                + Runtime Ratio: 50%
                + Mode: Linked to 1
            
        - Program 2:
            * Zone Program 4:
                + Runtime: 3600
                + Cycle Time: 900
                + Soak Time: 900
                + Mode: Timed
            * Zone Program 5:
                + Runtime: 720
                + Cycle Time: 120
                + Soak Time: 300
                + Disabled
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Program 1 Zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=25)

            # Program 1 Zone 2
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=200)

            # Program 1 Zone 3
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=50)

            # Program 1 Zone 4
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_cycle_time(_minutes=15)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_soak_time(_minutes=15)

            # Program 1 Zone 5
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_run_time(_minutes=12)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].zones[5].set_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_6(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()
            self.config.SubStations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_7(self):
        """
        1) Set date and time on controller and substations to: 04/08/2017 07:59:00
        2) Verify the following status':
            - Programs:
                + Program 1 done watering 
                + Program 2 done watering
            - Zones:
                + Zone 1 done watering
                + Zone 2 done watering
                + Zone 3 done watering
                + Zone 4 done watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0001 done watering
                + TSD0002 done watering
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
        3) Start Programming.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.set_controller_substation_date_and_time(controller=self.config.BaseStation3200[1],
                                                                   substations=self.config.BaseStation3200[1].substations,
                                                                   _date="04/08/2014", _time="07:59:00")

            # Verify Program 1 is not currently running
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # Verify Program 2 is not currently running
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            # Verify Zones 1-5 are not watering
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            # Verify initial status on Substation
            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()

            # Start Program 1 & 2
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        sim_minute = 08:00:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 watering
                + Zone 2 waiting
                + Zone 3 waiting
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Verify Zones 1-5
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            # Verify initial status on Substation
            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        sim_minute = 08:05:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 watering
                + Zone 2 waiting
                + Zone 3 waiting
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Verify Zones 1-5
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_10(self):
        """
        sim_minute = 08:10:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering 
                + Zone 3 waiting
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Verify Zones 1-5 are not watering
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        sim_minute = 08:15:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering 
                + Zone 3 waiting
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()

            # Verify Zones 1-5
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_12(self):
        """
        sim_minute = 08:20:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering 
                + Zone 3 waiting
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_13(self):
        """
        sim_minute = 08:25:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering 
                + Zone 3 waiting
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_14(self):
        """
        sim_minute = 08:30:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 soaking
                + Zone 2 soaking 
                + Zone 3 watering
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 watering
                + TSD0004 watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_15(self):
        """
        sim_minute = 08:35:00
        Verify the following status':
            - Programs:
                + Program 1 running 
                + Program 2 running 
            - Zones:
                + Zone 1 watering
                + Zone 2 soaking 
                + Zone 3 soaking 
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_16(self):
        """
        sim_minute = 08:40:00
        Verify the following status':
            - Programs:
                + Program 1 running
                + Program 2 running
            - Zones:
                + Zone 1 watering
                + Zone 2 soaking
                + Zone 3 soaking
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_17(self):
        """
        sim_minute = 08:45:00
        Verify the following status':
            - Programs:
                + Program 1 waiting to water
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 soaking
                + Zone 3 soaking
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_18(self):
        """
        sim_minute = 08:50:00
        Verify the following status':
            - Programs:
                + Program 1 waiting to water
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 soaking
                + Zone 3 soaking
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_19(self):
        """
        sim_minute = 08:55:00
        Verify the following status':
            - Programs:
                + Program 1 running
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_20(self):
        """
        sim_minute = 09:00:00
        Verify the following status':
            - Programs:
                + Program 1 running
                + Program 2 running
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_21(self):
        """
        sim_minute = 09:05:00
        Verify the following status':
            - Programs:
                + Program 1 running
                + Program 2 running
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_22(self):
        """
        sim_minute = 09:10:00
        Verify the following status':
            - Programs:
                + Program 1 running
                + Program 2 running
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_23(self):
        """
        sim_minute = 09:15:00
        Verify the following status':
            - Programs:
                + Program 1 running
                + Program 2 waiting to water
            - Zones:
                + Zone 1 watering
                + Zone 2 soaking
                + Zone 3 soaking
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_24(self):
        """
        sim_minute = 09:20:00
        Verify the following status':
            - Programs:
                + Program 1 running
                + Program 2 waiting to water
            - Zones:
                + Zone 1 watering
                + Zone 2 soaking
                + Zone 3 soaking
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_25(self):
        """
        sim_minute = 09:25:00
        Verify the following status':
            - Programs:
                + Program 1 running
                + Program 2 waiting to water
            - Zones:
                + Zone 1 soaking
                + Zone 2 soaking
                + Zone 3 watering
                + Zone 4 soaking
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_26(self):
        """
        sim_minute = 09:30:00
        Verify the following status':
            - Programs:
                + Program 1 waiting to water
                + Program 2 running
            - Zones:
                + Zone 1 soaking
                + Zone 2 soaking
                + Zone 3 soaking
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_27(self):
        """
        sim_minute = 09:35:00
        Verify the following status':
            - Programs:
                + Program 1 waiting to water
                + Program 2 running
            - Zones:
                + Zone 1 soaking
                + Zone 2 soaking
                + Zone 3 soaking
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_28(self):
        """
        sim_minute = 09:40:00
        Verify the following status':
            - Programs:
                + Program 1 running
                + Program 2 running
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking
                + Zone 4 watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_29(self):
        """
        sim_minute = 09:45:00
        Verify the following status':
            - Programs:
                + Program 1 running
                + Program 2 done watering
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking
                + Zone 4 done watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_30(self):
        """
        sim_minute = 09:50:00
        Verify the following status':
            - Programs:
                + Program 1 running
                + Program 2 done watering
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking
                + Zone 4 done watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_31(self):
        """
        sim_minute = 09:55:00
        Verify the following status':
            - Programs:
                + Program 1 running
                + Program 2 done watering
            - Zones:
                + Zone 1 soaking
                + Zone 2 watering
                + Zone 3 soaking
                + Zone 4 done watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_32(self):
        """
        sim_minute = 10:00:00
        Verify the following status':
            - Programs:
                + Program 1 running
                + Program 2 done watering
            - Zones:
                + Zone 1 watering
                + Zone 2 soaking
                + Zone 3 soaking
                + Zone 4 done watering
                + Zone 5 disabled
            - Valve BiCoders:
                + TSD0003 done watering
                + TSD0004 done watering
                + TSD0005 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=5)

            # Verify Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify Program 2
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0003'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_33(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
