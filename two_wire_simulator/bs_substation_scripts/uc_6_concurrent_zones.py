import sys
from time import sleep

# import common.product as helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes
from common import helper_methods

# import log_handler for logging functionality
from common.logging_handler import log_handler

__author__ = 'Tige'


class UseCase6ConcurrentZones(object):
    """
    Test name:
        - Substation Concurrent Zones

    Multi-Controller Setup Guide:
    
    Purpose:
        - Test and verify that shared Zones between 3200 and Substation return return the same status whether being 
          retrieved through the 3200 or through the Substation.
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Load valve bicoders on the 3200 and the substation
           the valve bicoder get addressed from the json file for the 3200 but not for the substation

        2. Assign valve bicoders to zones:
            - assign 2 local valve bicdoer from the 3200 to zones 4 and 5
                - set default valves for the zones
            - Assign 6 shared valve bicoder from the substaion to zone 1 - 3 and 6 - 8 on the 3200
                - manually address the shared valve bicoders from the substation to the 3200
                - set default valves for the valve bicoder and then the zones
        3. Set up zone Programs
            - Set up 3200 to have local zones
            - Set up Substation to have shared valve bicoders with the 3200
            - setup two Programs put 4 zones on each program
            - second program will have the primary zone list and linked zones to come first in the list
        4. set up mainlines:
            - setup mainline 1 to have have 1 master valve
            - setup mainline 2 to have have 2 master valves shared from substation
                  - we are not testing zones concurrency by flow only by numbers turned on by program and bicoder \n
                    limitation so set concurrency by flow to false
        5. set up Programs:
            - setup program 1 to have have a start time of 8:00 am
            - set up zone concurrency on program 1 to be 3
            - setup program 2 to have have a start time of 8:00 am
            - set up zone concurrency on program 2 to be 3
        6. set up POC'S:
            - setup POC1 to have have 1 master valve
            - setup POC 2 to have have 2 master valves shared from substation assign to mainline 2
            - setup POC 3 to have have 2 master valves shared from substation assign to mainline 2
        7. Set up a design for each zone
            - do this so we know we are using concurrent zone and not flow by design flow
        8. Increment clocks forward 1 minute and verify full configuration:
            - Configuration on 3200
            - Configuration on Substation
        7. we are not testing zones concurrancy by flow only by numbers turned on by program and bicoder limitation \n
        so set concurrancy by flow to false

        Verify initial Zone status' on 3200 and Substation prior to starting both Programs.
        
        7. Verify
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - This test assumes use of only 1 Substation and 1 3200
    """

    #################################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )

        self.run_use_case()

    #################################
    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Connect controller to substations:
        - uses the Ip address in the user config files to set the ip address
            Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                  incremented 1 minute at a time for the max minutes or until max number of minutes is met.
        - after connection is established increment clock for each controller 1 minute
            Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                  between controllers.

        """
        helper_methods.print_method_name()
        try:
            # Connect 3200 to substation 1
            self.config.BaseStation3200[1].add_substation_to_controller(
                _substation_address=1,
                _substation=self.config.SubStations[1])

            # Increment clocks after connection established
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        helper_methods.print_method_name()
        try:
            # set computer time
            date_mngr.set_current_date_to_match_computer()
        
            # set both the 3200 and the substation to be the same time as your computer
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                _date=date_mngr.curr_day.date_string_for_controller(),
                _time=date_mngr.curr_day.time_string_for_controller())
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller through the first controller.

        Substation Devices Shared:
            - Valves
                + D2: "TSE0021" - ZN Address: 7,8
                + D2: "TSE0031" - MV Address: 2,3
                + DD: "TVA3301" - ZN Address: 1,2,3,6
            - Flow BiCoders:
                + N/A
            - Temp BiCoders:
                + N/A
            - Moisture BiCoders:
                + N/A
            - Switch BiCoders:
                + N/A
            - Pump BiCoders:
                + N/A
        """
        helper_methods.print_method_name()
        try:
            # Prior to this step devices were loaded onto both the 3200 and substation. This search is required in order
            # for the 3200 to load devices from sub station.
            self.config.BaseStation3200[1].do_search_for_zones()

            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Substation: Set default values for Devices
            self.config.BaseStation3200[1].substations[1].set_bicoder_default_values()

            ############################################################################################################
            # Share devices from substation to controller. This makes the controller address the devices that it shares
            # with a substation, and then it sets the default values. We share these devices so that we can test and
            # verify that devices shared with a substation will retain their values after a reboot.

            # Zones
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TVA3301"].add_to_zone(_address=1)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TVA3302"].add_to_zone(_address=2)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TVA3303"].add_to_zone(_address=3)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TVA3304"].add_to_zone(_address=6)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0021"].add_to_zone(_address=7)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0022"].add_to_zone(_address=8)

            # Master Valves
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0031"].add_to_master_valve(_address=2)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0032"].add_to_master_valve(_address=3)
            #
            ############################################################################################################
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        Setup Programs
        ############################

        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        helper_methods.print_method_name()
        try:
            program_start_time_8am = [480]
            program_water_windows = ['111111111111111111111111']

            # Add & Configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_8am)

            # Add & Configure Program 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_start_time_8am)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        helper_methods.print_method_name()
        try:
            # ------------------- #
            # Add Program 1 Zones #
            # ------------------- #

            # Zone Program 1 (Primary Zone)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=2)

            # Zone Program 2
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            # Zone Program 3
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            # Zone Program 4
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1)

            # ------------------- #
            # Add Program 2 Zones #
            # ------------------- #

            # Zone Program 8 (Primary Zone)
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[2].zone_programs[8].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[8].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[2].zone_programs[8].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[8].set_soak_time(_minutes=2)
            
            # Zone Program 5
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_as_linked_zone(_primary_zone=8)
            
            # Zone Program 6
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_as_linked_zone(_primary_zone=8)
            
            # Zone Program 7
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[2].zone_programs[7].set_as_linked_zone(_primary_zone=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ############################
        Setup WaterSources
        ############################

        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()
            
            # Add & Configure Water Source 2
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].water_sources[2].set_enabled()
            self.config.BaseStation3200[1].water_sources[2].set_priority(_priority_for_water_source=3)
            self.config.BaseStation3200[1].water_sources[2].set_monthly_watering_budget(_budget=1000,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].water_sources[2].set_water_rationing_to_disabled()
            
            # Add & Configure Water Source 3
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=3)
            self.config.BaseStation3200[1].water_sources[3].set_enabled()
            self.config.BaseStation3200[1].water_sources[3].set_priority(_priority_for_water_source=3)
            self.config.BaseStation3200[1].water_sources[3].set_monthly_watering_budget(_budget=1000,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].water_sources[3].set_water_rationing_to_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ############################
        Setup Points of Control
        ############################

        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
            - set up points of control Attributes \n
                - set enable state \n
                - set target flow \n
                - set high flow limit with shut down state \n
                - set unscheduled flow limit with shut down state \n
                - set high pressure limit with shut down state \n
                - set low pressure limit with shut down state \n
            - Add flow meters ---> to point of control \n
            - Add pump ---> to point of control \n
            - Add master valve  ---> to point of control \n
            - Add pressure sensor  ---> to point of control \n
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(
                _limit=550,
                with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(
                _gallons=10,
                with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1
            )
            # Add POC 1 to WS 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1
            )

            # Add & Configure POC 2
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_enabled()
            self.config.BaseStation3200[1].points_of_control[2].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[2].set_high_flow_limit(
                _limit=75,
                with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].points_of_control[2].set_unscheduled_flow_limit(
                _gallons=5,
                with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].points_of_control[2].add_master_valve_to_point_of_control(
                _master_valve_address=2
            )
            # Add POC 2 to WS 2
            self.config.BaseStation3200[1].water_sources[2].add_point_of_control_to_water_source(
                _point_of_control_address=2
            )

            # Add & Configure POC 3
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=3)
            self.config.BaseStation3200[1].points_of_control[3].set_enabled()
            self.config.BaseStation3200[1].points_of_control[3].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[3].set_high_flow_limit(
                _limit=75,
                with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].points_of_control[3].set_unscheduled_flow_limit(
                _gallons=5,
                with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].points_of_control[3].add_master_valve_to_point_of_control(
                _master_valve_address=3
            )
            # Add POC 3 to WS 3
            self.config.BaseStation3200[1].water_sources[3].add_point_of_control_to_water_source(
                _point_of_control_address=3
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ############################
        Setup Mainlines
        ############################

        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_false()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(
                _percent=5,
                _with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(
                _percent=20,
                _with_shutdown_enabled=False
            )
            # Add Mainline 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            
            # Add & Configure Mainline 2
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].mainlines[2].set_enabled()
            self.config.BaseStation3200[1].mainlines[2].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[2].set_limit_zones_by_flow_to_false()
            self.config.BaseStation3200[1].mainlines[2].set_high_flow_variance_tier_one(
                _percent=20,
                _with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].mainlines[2].set_low_flow_variance_tier_one(
                _percent=5,
                _with_shutdown_enabled=True
            )
            # Add Mainline 2 to POC 2 & 3
            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=2)
            self.config.BaseStation3200[1].points_of_control[3].add_mainline_to_point_of_control(_mainline_address=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ######################
        Setup zones on mainlines
        ######################

        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        helper_methods.print_method_name()
        try:
            # Add Zones to Mainline 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=100)
            
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=100)

            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=100)

            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=4)
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=100)

            # Add Zones to Mainline 2
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=5)
            self.config.BaseStation3200[1].zones[5].set_design_flow(_gallons_per_minute=100)
            
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=6)
            self.config.BaseStation3200[1].zones[6].set_design_flow(_gallons_per_minute=100)

            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=7)
            self.config.BaseStation3200[1].zones[7].set_design_flow(_gallons_per_minute=100)

            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=8)
            self.config.BaseStation3200[1].zones[8].set_design_flow(_gallons_per_minute=100)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        helper_methods.print_method_name()
        try:
            # Increment clock to save config
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Verify controller configuration
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        1) Set date and time on controller and substations to match your computer
        2) Increment clock on both 3200 and substation to update statuses
        3) Because no Programs are running all Programs master valve and zone should not be in any state but done
        4) Set date and time on both substation and on the 3200 to the same time as your computer
        5) Verify the following status:
            - Programs:
                - Program 1 done watering
                - Program 2 done watering
            - Master Valves
                - MV1 = done watering
                - MV2 = done watering
                - MV3 = done watering
                - Valve BiCoder assigned to master valve address numbers located on substation
                    - TSE0031 = done watering
                    - TSE0032 = done watering
            - Zones:
                - Zone 1 done watering
                - Zone 2 done watering
                - Zone 3 done watering
                - Zone 4 done watering
                - Zone 5 done watering
                - Zone 6 done watering
                - Zone 7 done watering
                - Zone 8 done watering
                - Valve BiCoder assigned to zones address numbers located on substation
                    - TVA3301 done watering
                    - TVA3302 done watering
                    - TVA3303 done watering
                    - TVA3304 done watering
                    - TSE0021 done watering
                    - TSE0022 done watering
        3) Start Programming.
        """
        helper_methods.print_method_name()
        try:
            # set both the 3200 and the substation to be the same time as your computer
            date_mngr.set_current_date_to_match_computer()
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                _date=date_mngr.curr_day.date_string_for_controller(),
                _time=date_mngr.curr_day.time_string_for_controller()
            )

            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1], 
                substations=self.config.BaseStation3200[1].substations, 
                minutes=1
            )
            
            # Verify Programs is not currently running
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            # Verify controller master valves initial status
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()

            # Verify initial status on Substation valve bicoders assigned to master valves
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0031'].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0032'].statuses.verify_status_is_done()

            # Verify controller zones initial status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()

            # Verify initial status on Substation valve bicoders assigned to zones
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA3301'].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA3302'].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA3303'].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA3304'].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0021'].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0022'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        1) Start program 1 and 2
        2) Increment clock on both 3200 and substation to update statuses
            - because the controller is set to one concurrent zone only zone 1 and master valve should be set to \n
            watering all other zones should be set to waiting
            - program one will got to water but since no zones are running on program 2 it should be set to waiting
        3) Because no Programs are running all Programs master valve and zone should not be in any state but done
        4) Verify the following status:
            - Programs:
                - Program 1 done watering
                - Program 2 Waiting to water
            - Master Valves
                - MV1 = watering
                - MV2 = waiting to water
                - MV3 = waiting to water
                - Valve BiCoder assigned to master valve address numbers located on substation
                    - TSE0031 = done watering
                    - TSE0032 = done watering
            - Zones:
                - Zone 1 watering
                - Zone 2 waiting to water
                - Zone 3 waiting to water
                - Zone 4 waiting to water
                - Zone 5 waiting to water
                - Zone 6 waiting to water
                - Zone 7 waiting to water
                - Zone 8 waiting to water
                - Valve BiCoder assigned to zones address numbers located on substation
                    - TVA3301 watering
                    - TVA3302 done watering
                    - TVA3303 done watering
                    - TVA3304 done watering
                    - TSE0021 done watering
                    - TSE0022 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Verify Programs is not currently running
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()

            # Verify controller master valves initial status
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()

            # Verify initial status on Substation valve bicoders assigned to master valves
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0031'].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0032'].statuses.verify_status_is_done()

            # Verify controller zones initial status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_waiting_to_water()

            # Verify initial status on Substation valve bicoders assigned to zones
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA3301'].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA3302'].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA3303'].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA3304'].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0021'].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0022'].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        1) Stop program 1 and 2
        2) Increment clock on both 3200 and substation to update statuses
        2) Reset the max zone the controller can run to 7
        2) Increment clock on both 3200 and substation to update statuses
            -  Both Programs should start watering
            - Because master valves are not included in master concurrent zones all 3 master valve should start \n
            watering
                - Program
                    - zones 1-3 come from a 12 valve -
                    - zone 4 is on a dual
                - program 2
                    - zones 5 is the other half of the dual assign to zone 4
                    - zone 6 is from the same 12 valve as 1-3
                    - zone 7-8 are a dual
            - 12 valve decoders can only operate 2 valves at a time

        4) Verify the following status:
            - Programs:
                - Program 1 watering
                - Program 2 watering
            - Master Valves
                - MV1 = watering
                - MV2 = watering
                - MV3 = watering
                - Valve BiCoder assigned to master valve address numbers located on substation
                    - TSE0031 = watering
                    - TSE0032 = watering
            - Zones:
                - Zone 1 watering
                - Zone 2 watering
                - Zone 3 waiting to water
                - Zone 4 watering
                - Zone 5 watering
                - Zone 6 waiting to water
                - Zone 7 waiting to water
                - Zone 8 waiting to water
                - Valve BiCoder assigned to zones address numbers located on substation
                    - TVA3301 watering
                    - TVA3302 watering
                    - TVA3303 done watering
                    - TVA3306 done watering
                    - TSE0021 watering
                    - TSE0022 watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Stop programs
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Update controller's max concurrent zones
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=7)

            # Restart programs
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Verify Programs is not currently running
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Verify controller master valves initial status
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()

            # Verify initial status on Substation valve bicoders assigned to master valves
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0031'].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0032'].statuses.verify_status_is_watering()

            # Verify controller zones initial status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()

            # Verify initial status on Substation valve bicoders assigned to zones
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA3301'].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA3302'].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA3303'].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA3304'].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0021'].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0022'].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        helper_methods.print_method_name()
        try:
            # Increment clock to save config
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Verify controller configuration
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
