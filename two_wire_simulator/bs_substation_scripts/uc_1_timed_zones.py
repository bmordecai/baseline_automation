import sys

# import common.product as helper_methods
from common.configuration import Configuration
from common.objects.programming.pg_3200 import PG3200
from common.objects.programming.zp import ZoneProgram

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes
from common import helper_methods

# import log_handler for logging functionality
from common.logging_handler import log_handler

from time import sleep

__author__ = 'bens'


class UseCase1TimedZones(object):
    """
    Test name:
        - CN UseCase1 Timed Zones
    purpose:
        - set up a zones on the controller and attach them to a program
            - Make one zone timed
            - Make one zone the primary zone
                - Make the other three into linked zones with different ratios
    Coverage Area: \n
        1. able to change zone modes: timed, primary, and linked \n
        2. able to set up run times and soak cycles and verify that they run properly \n
        3. verify linked zones follow the primary zone ratios \n
        4. able to disable zones \n
        
    Controller Setup:
        - Local Devices:
            + Zone 1 (TSD0001)
            + Zone 2 (TSD0002)
            + Zone 3 (TSD0003)
        - Shared Devices:
            + Zone 4 (TSD0002)
            + Zone 5 (TSD0003)
            + Zone 6 (TSD0004)
            
    Substation Setup:
        - Local Devices:
            + Valve BiCoder TSD0002
            + Valve BiCoder TSD0003
            + Valve BiCoder TSD0004
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str

        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )
        self.run_use_case()

        # self.database_id_for_fw_update = fw_database_id

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Connect controller to substations.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].add_substation_to_controller(_substation_address=1,
                                                                        _substation=self.config.SubStations[1])

            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:

            self.config.BaseStation3200[1].do_search_for_zones()

            # Increment the clocks so the search updates available bicoders in the controller
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            # Set the default values for the substation bicoders and assign them on the 3200
            self.config.BaseStation3200[1].substations[1].set_bicoder_default_values()
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0004"].add_to_zone(_address=4)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0005"].add_to_zone(_address=5)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0006"].add_to_zone(_address=6)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        1) Create mainline objects for Program.
        2) Make a very basic program that will be used to attach all the zones together.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # For loop to create 8 Mainline objects, there are always 8.
            for mainline_address in range(1, 9):
                # Create each Mainline and store into the dictionary using the address as a lookup key
                self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=mainline_address)

            # Create the program that the zones will be placed on
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=[1200])
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        assign zones to programs \n
        set up primary linked zones \n
        Zone 1 is timed \n
        Zone 2 is a primary zone \n
            - Zone 3-5 are linked to the primary zone 2 \n
            - Zone 3 has a runtime tracking ratio of 100% \n
            - Zone 4 has a runtime tracking ratio of 150% \n
            - Zone 5 has a runtime tracking ratio of 50% \n
        Set zone 6 to have enabled state set to false. (disable it)
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Program 1 Zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=8)

            # Program 1 Zone 2
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_run_time(_minutes=12)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_soak_time(_minutes=5)

            # Program 1 Zone 3
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=2, _tracking_ratio=100)

            # Program 1 Zone 4
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=2, _tracking_ratio=150)

            # Program 1 Zone 5
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_as_linked_zone(_primary_zone=2, _tracking_ratio=50)

            # Program 1 Zone 6
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[1].zone_programs[6].set_as_linked_zone(_primary_zone=2, _tracking_ratio=100)

            # Disable zones 3 and 6
            self.config.BaseStation3200[1].zones[3].set_disabled()
            self.config.BaseStation3200[1].zones[6].set_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_6(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)
            # TODO try a self test and update object attributes here
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_7(self):
        """
        turn on sim mode in the controller \n
        stop the clock so that it can be incremented manually \n
        set the dial to off \n
        set the dial to run \n
        stop both Programs \n
        advance the clock 1 second \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        advance the clock 1 second \n
        verify that the zones are a functioning correctly by getting their statuses before starting \n
        start program \n
        advance the clock 1 minute \n
        Zone Behavior:
            verify that zone 1 waits for 2 minutes at the start of the test then waters for 2 minutes and soaks for 8
            minutes then repeats \n
            verify that zone 2 starts out watering for 2 minutes then soaks for 5 minutes then repeats \n
            verify that zone 3 waits for 3 minutes at the start of the test then waters for 3 minutes and soaks for 9
            minutes then repeats \n
            verify that zone 4 waits for 9 minutes at the start of the test then waters for 1 minute and soaks for 9
            minutes then repeats \n
            verify that zone 5 is disable for the entire test \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.set_controller_substation_date_and_time(controller=self.config.BaseStation3200[1],
                                                                   substations=self.config.BaseStation3200[1].substations,
                                                                   _date="04/08/2014", _time="02:38:00")

            # Verify Program 1 is not watering

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # Verify Zones 1-6 are not watering
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            # Verify initial status on Substation

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].set_program_to_start()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        zone 2 start first because it is a primary zone and zone is a timed zone
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 waiting to water
                - Zone 2 watering
                - Zone 3 disabled
                - Zone 4 waiting to water
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 waiting to water
                - Zone 2 watering
                - Zone 3 disabled
                - Zone 4 waiting to water
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_10(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 watering
                - Zone 2 soaking
                - Zone 3 disabled
                - Zone 4 waiting to water
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 watering
                - Zone 2 soaking
                - Zone 3 disabled
                - Zone 4 waiting to water
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_12(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 soaking
                - Zone 3 disabled
                - Zone 4 watering
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_13(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 soaking
                - Zone 3 disabled 
                - Zone 4 watering
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_14(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 soaking
                - Zone 3 disabled
                - Zone 4 watering
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_15(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 watering
                - Zone 3 disabled
                - Zone 4 soaking
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_16(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 watering
                - Zone 3 disabled
                - Zone 4 soaking
                - Zone 5 waiting to water
                - Zone 6 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_17(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled 
                 - Zone 4 soaking
                 - Zone 5 watering
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_18(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_19(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_20(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 watering
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_21(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 watering
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_22(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 watering
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_23(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 watering
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_24(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 watering
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_25(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 watering
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_26(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 watering
                 - Zone 5 soaking
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_27(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 disabled
                 - Zone 4 soaking
                 - Zone 5 watering
                 - Zone 6 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_disabled()

            self.config.SubStations[1].valve_bicoders['TSD0004'].statuses.verify_status_is_done()
            self.config.SubStations[1].valve_bicoders['TSD0005'].statuses.verify_status_is_watering()
            self.config.SubStations[1].valve_bicoders['TSD0006'].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_28(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            helper_methods.increment_controller_substation_clocks(controller=self.config.BaseStation3200[1],
                                                                  substations=self.config.BaseStation3200[1].substations,
                                                                  minutes=1)

            # self.config.verify_full_configuration()
            # self.substations[1].verify_full_configuration()
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
