import sys
from time import sleep

# import common.product as helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes
from common import helper_methods

# import log_handler for logging functionality
from common.logging_handler import log_handler

__author__ = 'bens'


class UseCase5ReplaceDevices(object):
    """
    Test name:
        - Substation Replace Devices

    Multi-Controller Setup Guide:
    
    Purpose:
        - Test and verify controller and substation configurations after rebooting as well as replacing devices.
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Loads initial devices onto both controller and substations.
        2. Sets up initial programming.
        3. Reboots 3200.
        4. Self test's devices and verifies 3200 and substations configuration.
        5. Replace Devices on 3200:
        
            Device:                 Addresses:
            - Zones                 1, 2, 197, 198
            - Master Valve          3 
            - Moisture Sensor       1 
            - Temperature Sensor    1 
            - Event Switch          1
        
        6. Verifies 3200 and substations configuration.
        7. Reboots Substation
        8. Self test's controller devices and verifies controller and substation configurations.
        9. Replace BiCoders on Substation:
        
            BiCoders:           Controller Address:       Serial Numbers:
            - Valve (ZN's)      3, 4                      "TSE0011" -> "TSE0021", "TSE0012" -> "TSE0022"                
            - Valve (MV's)      8                         "TMV0004" -> "TMV0007"              
            - Flow              4                         "TWF0004" -> "TWF0006"
            - Switch            3                         "TPD0003" -> "TPD0005"
            - Temp              2                         "TAT0002" -> "TAT0004"
            - Moisture          3                         "SB07258" -> "SB07184"
            - Pump              N/A
            
        10. Verify controller and substations configurations.
        
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - Test only uses 1 substation.
        
    Controller Programming:
        - Zones:                10  (7-local, 3-shared) 
        - Master Valves:        4   (3-local, 1-shared)
        - Event Switches:       3   (2-local, 1-shared)
        - Temp Sensors:         2   (1-local, 1-shared)
        - Moisture Sensors:     3   (2-local, 1-shared)
        - Flow Meters:          4   (3-local, 1-shared)
        - Programs:             4
        - Zone Programs:        10
        - Mainlines:            2
        - Pocs:                 2
        
    Substation Programming:
        - Valve BiCoders:       3
        - Moisture BiCoders:    1   
        - Temperature BiCoders: 1
        - Switch BiCoders:      1 
        - Flow BiCoders:        1
        - Pump BiCoders:        0
    """

    #################################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str

        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )

        self.run_use_case()

    #################################
    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        helper_methods.print_method_name()
        try:
            # set computer time
            date_mngr.set_current_date_to_match_computer()

            # set both the 3200 and the substation to be the same time as your computer
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.SubStations,
                _date=date_mngr.curr_day.date_string_for_controller(),
                _time=date_mngr.curr_day.time_string_for_controller())
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substations:
        - uses the Ip address in the user config files to set the ip address
            Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                  incremented 1 minute at a time for the max minutes or until max number of minutes is met.
        - after connection is established increment clock for each controller 1 minute
            Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                  between controllers.

        """
        helper_methods.print_method_name()
        try:
            # Connect 3200 to substation 1
            self.config.BaseStation3200[1].add_substation_to_controller(
                _substation_address=1,
                _substation=self.config.SubStations[1]
            )

            # Increment clocks after connection established
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.

        Substation Devices Shared:
            - Valves
                + D1: "TMV0004" - Address: 8
                + D2: "TSE0011" - Address: 3, 4
            - Flow BiCoders:
                + "TWF0004" - Address: 4
            - Temp BiCoders:
                + "TAT0002" - Address: 2
            - Moisture BiCoders:
                + "SB07258" - Address: 3
            - Switch BiCoders:
                + "TPD0003" - Address: 3
            - Pump BiCoders:
                + N/A
        """
        helper_methods.print_method_name() 
        try:
            # Prior to this step devices were loaded onto both the 3200 and substation. This search is required in order
            # for the 3200 to load devices from sub station.
            self.config.BaseStation3200[1].do_search_for_zones()

            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Substation: Set default values for Devices
            self.config.BaseStation3200[1].substations[1].set_bicoder_default_values()

            ############################################################################################################
            # Share devices from substation to controller. This makes the controller address the devices that it shares
            # with a substation, and then it sets the default values. We share these devices so that we can test and
            # verify that devices shared with a substation will retain their values after a reboot.

            # Zones
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0011"].add_to_zone(_address=3)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0012"].add_to_zone(_address=4)

            # Master Valves
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0004"].add_to_master_valve(_address=8)

            # Flow Meters
            self.config.BaseStation3200[1].substations[1].flow_bicoders["TWF0004"].add_to_flow_meter(_address=4)

            # Temperature Sensors
            self.config.BaseStation3200[1].substations[1].temperature_bicoders["TAT0002"].add_to_temperature_sensor(
                _address=2
            )
            
            # Moisture Sensors
            self.config.BaseStation3200[1].substations[1].moisture_bicoders["SB07258"].add_to_moisture_sensor(
                _address=3
            )

            # Event Switch
            self.config.BaseStation3200[1].substations[1].switch_bicoders["TPD0003"].add_to_event_switch(_address=3)
            #
            ############################################################################################################
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        helper_methods.print_method_name()
        try:
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1], 
                substations=self.config.BaseStation3200[1].substations, 
                minutes=1
            )

            # self.config.verify_full_configuration()
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        Setup Programs
        ############################

        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        helper_methods.print_method_name()
        try:
            program_number_1_start_times = [480, 540, 600, 660]
            program_number_3_start_times = [480, 540, 600, 660]
            program_number_4_start_times = [480, 540, 600, 660]
            program_number_99_start_times = [480, 540, 600, 660]
            program_number_99_watering_days = [0, 0, 0, 0, 0, 0, 8, 8, 6, 5, 5, 4, 3, 3, 3, 3, 4, 5, 6, 7, 0, 0, 0, 0]
            program_number_1_water_windows =  ['011111100001111111111110']
            program_number_3_water_windows =  ['011111100001111111111110']
            program_number_4_water_windows =  ['011111100001111111111110']
            program_number_99_water_windows = ['011111100000111111111110',  # Sun
                                               '011111100001111111111111',  # Mon
                                               '011111100001111111111110',  # Tue
                                               '011111100001111111111110',  # Wed
                                               '011111100001111111111110',  # Thu
                                               '011111100001111111111110',  # Fri
                                               '011111100001111111111111']  # Sat

            # Add & Configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_number_1_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=False, _mon=True, _tues=False, _wed=True, _thurs=False, _fri=True, _sat=False
            )
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_number_1_start_times)

            # Add & Configure Program 3
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_water_window(_ww=program_number_3_water_windows)
            self.config.BaseStation3200[1].programs[3].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[3].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[3].set_watering_intervals_to_odd_days()
            self.config.BaseStation3200[1].programs[3].set_start_times(_st_list=program_number_3_start_times)

            # Add & Configure Program 4
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation3200[1].programs[4].set_enabled()
            self.config.BaseStation3200[1].programs[4].set_water_window(_ww=program_number_4_water_windows)
            self.config.BaseStation3200[1].programs[4].set_priority_level(_pr_level=3)
            self.config.BaseStation3200[1].programs[4].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[4].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=False, _mon=True, _tues=False, _wed=True, _thurs=False, _fri=True, _sat=False
            )
            self.config.BaseStation3200[1].programs[4].set_start_times(_st_list=program_number_4_start_times)

            # Add & Configure Program 99
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_water_window(_ww=program_number_99_water_windows, _is_weekly=False)
            self.config.BaseStation3200[1].programs[99].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[99].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[99].set_watering_intervals_to_semi_monthly(_sm=program_number_99_watering_days)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_number_99_start_times)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """

        Zone Program 1:
            - Zone:                 1
            - Program:              1
            - Runtime:              900 (15 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Primary Zone)

        Zone Program 2:
            - Zone:                 2
            - Program:              1
            - Runtime:              900 (15 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Runtime Ratio:        100%
            - Primary Zone Number:  1 (Linked)

        Zone Program 49:
            - Zone:                 49
            - Program:              3
            - Runtime:              1200 (20 minutes)
            - Cycle Time:           600 (10 minutes)
            - Soak Time:            3600 (60 minutes)
            - Runtime Ratio:        100%
            - Water Strategy:       Timed

        Zone Program 50:
            - Zone:                 50
            - Program:              3
            - Runtime:              1200 (20 minutes)
            - Cycle Time:           600 (10 minutes)
            - Soak Time:            3600 (60 minutes)
            - Water Strategy:       Timed

        Zone Program 50:
            - Zone:                 50
            - Program:              4
            - Runtime:              1200 (20 minutes)
            - Cycle Time:           600 (10 minutes)
            - Soak Time:            3600 (60 minutes)
            - Water Strategy:       Timed

        Zone Program 197:
            - Zone:                 197
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Runtime Ratio:        50%
            - Primary Zone Number:  200 (Linked)

        Zone Program 198:
            - Zone:                 198
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Runtime Ratio:        100%
            - Primary Zone Number:  200 (Linked)

        Zone Program 199:
            - Zone:                 199
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Runtime Ratio:        150%
            - Primary Zone Number:  200 (Primary Zone)

        Zone Program 200:
            - Zone:                 200
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Primary Zone Number:  200 (Primary Zone)
        """
        helper_methods.print_method_name()
        try:
            # ------------------- #
            # Add Program 1 Zones #
            # ------------------- #

            # Zone Program 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            # Zone Program 2
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            # ------------------- #
            # Add Program 3 Zones #
            # ------------------- #

            # Zone Program 49
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=49)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_soak_time(_minutes=60)

            # Zone Program 50
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_soak_time(_minutes=60)

            # ------------------- #
            # Add Program 4 Zones #
            # ------------------- #

            # Zone Program 50
            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_soak_time(_minutes=60)

            # -------------------- #
            # Add Program 99 Zones #
            # -------------------- #

            # Zone Program 200
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=200)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_run_time(_minutes=33)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_cycle_time(_minutes=3)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_soak_time(_minutes=13)

            # Zone Program 197
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=197)
            self.config.BaseStation3200[1].programs[99].zone_programs[197].set_as_linked_zone(_primary_zone=200,
                                                                                              _tracking_ratio=50)

            # Zone Program 198
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=198)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_as_linked_zone(_primary_zone=200)

            # Zone Program 199
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=199)
            self.config.BaseStation3200[1].programs[99].zone_programs[199].set_as_linked_zone(_primary_zone=200,
                                                                                              _tracking_ratio=150)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #################################
    def step_7(self):
        """
        ############################
        Setup WaterSources
        ############################

        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

            # Add & Configure Water Source 8
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[1].water_sources[8].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].set_priority(_priority_for_water_source=3)
            self.config.BaseStation3200[1].water_sources[8].set_monthly_watering_budget(_budget=1000)
            self.config.BaseStation3200[1].water_sources[8].set_water_rationing_to_disabled()
            self.config.BaseStation3200[1].water_sources[8].add_switch_empty_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_switch_empty_condition_to_closed()
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_empty_wait_time(_minutes=540)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #################################
    def step_8(self):
        """
        ############################
        Setup Points of Control
        ############################

        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(
                _limit=550,
                with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(
                _gallons=10,
                with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=3
            )
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=3
            )
            # Add POC 1 to WS 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1
            )

            # Add & Configure POC 8
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[8].set_high_flow_limit(
                _limit=75,
                with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].points_of_control[8].set_unscheduled_flow_limit(
                _gallons=5,
                with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].points_of_control[8].add_flow_meter_to_point_of_control(
                _flow_meter_address=4
            )
            self.config.BaseStation3200[1].points_of_control[8].add_master_valve_to_point_of_control(
                _master_valve_address=8
            )
            # Add POC 8 to WS 8
            self.config.BaseStation3200[1].water_sources[8].add_point_of_control_to_water_source(
                _point_of_control_address=8
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #################################
    def step_9(self):
        """
        ############################
        Setup Mainlines
        ############################

        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(
                _percent=5,
                _with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(
                _percent=20,
                _with_shutdown_enabled=True
            )
            # Add Mainline 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            # Add & Configure Mainline 8
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[1].mainlines[8].set_enabled()
            self.config.BaseStation3200[1].mainlines[8].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[8].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_high_flow_variance_tier_one(
                _percent=20,
                _with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].mainlines[8].set_low_flow_variance_tier_one(
                _percent=5,
                _with_shutdown_enabled=False
            )
            # Add Mainline 8 to POC 8
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(_mainline_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_10(self):
        """
        ######################
        Setup zones on mainlines
        ######################

        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        helper_methods.print_method_name()
        try:
            # Add Zones to Mainline 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=49)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=50)

            # Add Zones to Mainline 8
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=197)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=198)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=199)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=200)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Increment the clock to save settings in both the controller and substation \n
        Turn sim mode off on both controller and substation \n
        """
        helper_methods.print_method_name()
        try:
            # Increment clock to save config
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Verify controller configuration
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        1) Reboot controller.
        2) Stop controller's clock.
        3) Wait for substation and controller to reconnect.
        4) Reset controller and substation's date and time to last recorded time in date manager.
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].do_reboot_controller()
            self.config.BaseStation3200[1].stop_clock()

            # Wait for status change
            self.config.BaseStation3200[1].wait_for_controller_and_substations_connection_status(
                _substation_address=1,
                _max_wait=60
            )

            # Update controller clocks to last known value
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                _date=date_mngr.controller_datetime.date_string_for_controller(),
                _time=date_mngr.controller_datetime.time_string_for_controller()
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        1) Self test and update attributes for all controller devices.
        2) Verify controller configuration.
        """
        helper_methods.print_method_name()
        try:
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()

            for moisture_sensors in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[moisture_sensors].bicoder.self_test_and_update_object_attributes()

            for master_valves in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[master_valves].bicoder.self_test_and_update_object_attributes()

            for flow_meters in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseStation3200[1].flow_meters[flow_meters].bicoder.self_test_and_update_object_attributes()

            for event_switches in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseStation3200[1].event_switches[event_switches].bicoder.self_test_and_update_object_attributes()

            for temperature_sensors in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[temperature_sensors].bicoder.self_test_and_update_object_attributes()

            # Verify controller configuration
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()

            # Increment clock to save configuration
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        Replace controller devices. Devices being replaced:
        - search address and replace new devices
            - Zones:
                + Zone 1:   "TSD0001" -> "TSQ0091"
                + Zone 2:   "TSQ0071" -> "TSQ0092"
                + Zone 197: "TSQ0074" -> "TSQ0093"
                + Zone 198: "TSQ0081" -> "TSQ0094"
            - Flow Meters:
                + FM 1: "TWF0001" -> "TWF0005"
            - Master Valves:
                + MV 3: "TMV0003" -> "TMV0008"
            - Temperature Sensors:
                + TS 1: "TAT0001" -> "TAT0003"
            - Moisture Sensors:
                + MS 1: "SB05308" -> "SB01250"
            - Event Switches:
                + SW 1: "TPD0001" -> "TPD0004"
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].master_valves[3].replace_single_valve_bicoder(_new_serial_number='TMV0008')

            self.config.BaseStation3200[1].zones[1].replace_quad_valve_bicoder(_new_serial_number='TSQ0091')
            self.config.BaseStation3200[1].zones[2].replace_quad_valve_bicoder(_new_serial_number='TSQ0092')
            self.config.BaseStation3200[1].zones[197].replace_quad_valve_bicoder(_new_serial_number='TSQ0093')
            self.config.BaseStation3200[1].zones[198].replace_quad_valve_bicoder(_new_serial_number='TSQ0094')

            self.config.BaseStation3200[1].moisture_sensors[1].replace_moisture_bicoder(_new_serial_number='SB01250')

            self.config.BaseStation3200[1].temperature_sensors[1].replace_temperature_bicoder(_new_serial_number='TAT0003')

            self.config.BaseStation3200[1].event_switches[1].replace_switch_bicoder(_new_serial_number='TPD0004')

            self.config.BaseStation3200[1].flow_meters[1].replace_flow_bicoder(_new_serial_number='TWF0005')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        Verify that the replacement device on the controllers configuration have maintained all of there settings
        after replacing devices.
        """
        helper_methods.print_method_name()
        try:
            # Increment clock to save config
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Verify config with replaced 3200 devices
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        1) Reboot Substation.
        2) Stop Clock.
        3) Reset date and time to match last recorded.
        """
        helper_methods.print_method_name()
        try:
            # Reboot Substation
            self.config.SubStations[1].do_reboot_controller()
            self.config.BaseStation3200[1].substations[1].stop_clock()

            # Wait for status change
            self.config.BaseStation3200[1].wait_for_controller_and_substations_connection_status(
                _substation_address=1,
                _max_wait=60
            )

            # Update controller clocks to last known value
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                _date=date_mngr.controller_datetime.date_string_for_controller(),
                _time=date_mngr.controller_datetime.time_string_for_controller()
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        Verify controller and substation configuration.
        """
        helper_methods.print_method_name()
        try:
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()

            for moisture_sensors in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[moisture_sensors].bicoder.self_test_and_update_object_attributes()

            for master_valves in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[master_valves].bicoder.self_test_and_update_object_attributes()

            for flow_meters in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseStation3200[1].flow_meters[flow_meters].bicoder.self_test_and_update_object_attributes()

            for event_switches in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseStation3200[1].event_switches[event_switches].bicoder.self_test_and_update_object_attributes()

            for temperature_sensors in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[temperature_sensors].bicoder.self_test_and_update_object_attributes()

            # Verify controller configuration
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()

            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        1) Clear substation devices.
        2) Load devices back onto substation.
        3) Do a search for devices from the controller.
        
        Substation Devices Shared:
            - Valves
                + D1: "TMV0004" - Address: 8        Replaced with: "TMV0007"
                + D2: "TSE0011" - Address: 3, 4     Replaced with: "TSE0021"
            - Flow BiCoders:
                + "TWF0004" - Address: 4            Replaced with: "TWF0006"
            - Temp BiCoders:
                + "TAT0002" - Address: 2            Replaced with: "TAT0004"
            - Moisture BiCoders:
                + "SB07258" - Address: 3            Replaced with: "SB07184"
            - Switch BiCoders:
                + "TPD0003" - Address: 3            Replaced with: "TPD0005"
            - Pump BiCoders:
                + N/A
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].substations[1].clear_all_programming()

            # Delete substation objects that were replaced (new ones will be created)
            del self.config.BaseStation3200[1].substations[1].valve_bicoders['TMV0004']     # MV 8
            del self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0011']     # ZN 3
            del self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0012']     # ZN 4
            del self.config.BaseStation3200[1].substations[1].flow_bicoders['TWF0004']      # FM 4
            del self.config.BaseStation3200[1].substations[1].temperature_bicoders['TAT0002']      # TS 2
            del self.config.BaseStation3200[1].substations[1].moisture_bicoders['SB07258']  # MS 3
            del self.config.BaseStation3200[1].substations[1].switch_bicoders['TPD0003']    # SW 3

            # Increment both clocks for programming to hold
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Load devices onto Substation
            self.config.BaseStation3200[1].substations[1].load_dv(dv_type=opcodes.single_valve_decoder,     # MV 8
                                                                  list_of_decoder_serial_nums=['TMV0007'],
                                                                  device_type=opcodes.master_valve)
            self.config.BaseStation3200[1].substations[1].load_dv(dv_type=opcodes.two_valve_decoder,        # ZN 3,4
                                                                  list_of_decoder_serial_nums=['TSE0021'],
                                                                  device_type=opcodes.zone)
            self.config.BaseStation3200[1].substations[1].load_dv(dv_type=opcodes.moisture_sensor,
                                                                  list_of_decoder_serial_nums=['SB07184'])  # MS 3
            self.config.BaseStation3200[1].substations[1].load_dv(dv_type=opcodes.temperature_sensor,
                                                                  list_of_decoder_serial_nums=['TAT0004'])  # TS 2
            self.config.BaseStation3200[1].substations[1].load_dv(dv_type=opcodes.event_switch,
                                                                  list_of_decoder_serial_nums=['TPD0005'])  # SW 3
            self.config.BaseStation3200[1].substations[1].load_dv(dv_type=opcodes.flow_meter,
                                                                  list_of_decoder_serial_nums=['TWF0006'])  # FM 4

            # Increment both clocks for programming to hold
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Search for devices on controller to load them onto the 3200 from substation
            self.config.BaseStation3200[1].do_search_for_all_devices(_list_of_device_types=[opcodes.zone])
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        1) Create/Assign new devices.

        Substation Devices Shared:
            - Valves
                + D1: "TMV0004" - Address: 8        Replaced with: "TMV0007"
                + D2: "TSE0011" - Address: 3, 4     Replaced with: "TSE0021"
            - Flow BiCoders:
                + "TWF0004" - Address: 4            Replaced with: "TWF0006"
            - Temp BiCoders:
                + "TAT0002" - Address: 2            Replaced with: "TAT0004"
            - Moisture BiCoders:
                + "SB07258" - Address: 3            Replaced with: "SB07184"
            - Switch BiCoders:
                + "TPD0003" - Address: 3            Replaced with: "TPD0005"
            - Pump BiCoders:
                + N/A
        """
        helper_methods.print_method_name()
        try:
            # Replace bicoders
            self.config.BaseStation3200[1].master_valves[8].replace_single_valve_bicoder(_new_serial_number='TMV0007')
            self.config.BaseStation3200[1].zones[3].replace_dual_valve_bicoder(_new_serial_number='TSE0021')
            self.config.BaseStation3200[1].zones[4].replace_dual_valve_bicoder(_new_serial_number='TSE0022')
            self.config.BaseStation3200[1].flow_meters[4].replace_flow_bicoder(_new_serial_number='TWF0006')
            self.config.BaseStation3200[1].temperature_sensors[2].replace_temperature_bicoder(_new_serial_number='TAT0004')
            self.config.BaseStation3200[1].moisture_sensors[3].replace_moisture_bicoder(_new_serial_number='SB07184')
            self.config.BaseStation3200[1].event_switches[3].replace_switch_bicoder(_new_serial_number='TPD0005')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        Verify controller and substation configuration.
        """
        helper_methods.print_method_name()
        try:
            # Increment both clocks for programming to hold
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )

            # Verify controller configuration
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
