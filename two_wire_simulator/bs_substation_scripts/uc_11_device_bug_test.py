import sys
from time import sleep

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

# Browser pages used
# import page_factory

__author__ = 'Eldin'


class SubStationUseCase11(object):
    """
    Test name:
        - CN UseCase11 replace devices on SubStation
    User Story:
        - As a user I want to be able to clear my old bicoders from memory on the SubStation and have them be forgotten
          on the 3200 as well.
    purpose:
        - This test replaces devices on the SubStation and verify the 3200 forgets them and new devices can be found
        - This verifies that we can clear all of the bicoders on a substation but we will still have the 3200 bicoders
          that were loaded on the 3200 seperately.
    Coverage Area: \n
        - load a first set of all biCoder types on the 3200 and the SubStation
            - Do a search from the 3200
            - verify all biCoders are not assigned to an address
            - verify all SubStation biCoders are not assigned to an address on the 3200
        - load a second set of all biCoder types but don't include the first set on the 3200 and the SubStation
            - Do a search from the 3200
            - verify all old biCoders have been removed
            - verify all new biCoders are not assigned to an address
            - verify all new SubStation biCoders are not assigned to an address on the 3200
        - add or Address all the new biCoders to devices
            - verify all biCoders attributes through the 3200
            - verify all biCoders are not assigned to an address
            - verify all biCoders attributes through the SubStations
            - verify all biCoders are addressed on the 3200
        - add all devices to programs
            - verify all programing
        - remove or un-address all the new devices
            - verify all biCoder are un-assigned or set to zero address
            - verify all programing has been lost
        -
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str or None \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )
        self.run_use_case()
        print("Test Skipped. This bug is impossible to reproduce on v16 3200.")

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        1) add SubStation to the Controller
        2) synchronize both clocks
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            self.config.BaseStation3200[1].add_substation_to_controller(
                _substation=self.config.SubStations[1],
                _substation_address=1)
            
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        load devices on the 3200 will not be using json for this test
        Controller devices loaded:

            BiCoder Type        Serial Number on 3200   Serial Number on Substation
            -----------------------------------------------------------------------
            - Single Valve
                                "TSD0001"               "TSD0002"
            -----------------------------------------------------------------------
            - Dual Valve
                                "TSE0011"               "TSE0021"
            -----------------------------------------------------------------------
            - Quad Valve
                                "TSQ0011"               "TSQ0021"
            -----------------------------------------------------------------------
            - Twelve Valve
                                "TVA1001"               "TVA20001"
            -----------------------------------------------------------------------
            - Master Valves
                                "TMV0001"               "TMV0002"
                                "TMV0011"               "TMV0021"
            -----------------------------------------------------------------------
            - Pump
                                "PMV0001"               "PMV0002"
                                "PMV0011"               "PMV0021"
            -----------------------------------------------------------------------
            - Flow
                                "TWF0001"               "TWF0002"
            -----------------------------------------------------------------------
            - Moisture
                                "SB00001"               "SB00002"
            -----------------------------------------------------------------------
            - Temperature
                                "TAT0001"               "TAT0002"
            -----------------------------------------------------------------------
            - Switch
                                "TPD0001"               "TPD0002"
            -----------------------------------------------------------------------
            - Analog
                                "PSD0001"               "PSD0002"
            -----------------------------------------------------------------------
        :return:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # load devices on 3200
            self.config.BaseStation3200[1].load_all_dv(d1_list=["TSD0001"],
                                                       mv_d1_list=["TMV0001"],
                                                       pm_d1_list=["PMV0001"],
                                                       d2_list=["TSE0011"],
                                                       mv_d2_list=["TMV0011"],
                                                       pm_d2_list=["PMV0011"],
                                                       d4_list=["TSQ0011"],
                                                       dd_list=["TVA1001"],
                                                       ms_list=["SB00001"],
                                                       fm_list=["TWF0001"],
                                                       ts_list=["TAT0001"],
                                                       sw_list=["TPD0001"])
                                                       # an_list=["PSD0001"])

            self.config.SubStations[1].load_all_dv(d1_list=["TSD0002"],
                                                   mv_d1_list=["TMV0002"],
                                                   pm_d1_list=["PMV0002"],
                                                   d2_list=["TSE0021"],
                                                   mv_d2_list=["TMV0021"],
                                                   pm_d2_list=["PMV0021"],
                                                   d4_list=["TSQ0021"],
                                                   dd_list=["TVA2001"],
                                                   ms_list=["SB00002"],
                                                   fm_list=["TWF0002"],
                                                   ts_list=["TAT0002"],
                                                   sw_list=["TPD0002"])
                                                   # an_list=["PSD0002"])

            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Do a search for all biCoders
        verify that all devices show up in the 3200 but they all have an address of zero
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # one search will populate all devices
            self.config.BaseStation3200[1].do_search_for_zones()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        address Devices on the controller:

        Device Type     biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Zones
                        Single          "TSD0001"       0

                        Single          "TSD0002"       0

                        Dual            "TSE0011"       0
                                        "TSE0012"       0

                        Dual            "TSE0021"       0
                                        "TSE0022"       0

                        Quad            "TSQ0011"       0
                                        "TSQ0012"       0
                                        "TSQ0013"       0
                                        "TSQ0014"       0

                        Quad            "TSQ0021"       0
                                        "TSQ0022"       0
                                        "TSQ0023"       0
                                        "TSQ0024"       0

                        Twelve          "TVA1001"       0
                                        "TVA1002"       0
                                        "TVA1003"       0

                        Twelve          "TVA20001"      0
                                        "TVA20002"      0
                                        "TVA20003"      0
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify single biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSD0001"].verify_device_address()

            # verify single valve biCoder values on SubStation and address 0 on 3200
            self.config.BaseStation3200[1].valve_bicoders["TSD0002"].verify_device_address()

            # verify dual biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSE0011"].verify_device_address()

            # verify single biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSE0012"].verify_device_address()

            # verify dual biCoder values on the SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSE0021"].verify_device_address()

            # verify dual valve biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSE0022"].verify_device_address()

            # verify quad valve biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0011"].verify_device_address()

            # verify quad biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0012"].verify_device_address()

            # verify quad biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0013"].verify_device_address()

            # verify quad biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0014"].verify_device_address()

            # verify quad biCoder values on the SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0021"].verify_device_address()

            # verify quad valve biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0022"].verify_device_address()

            # verify quad valve biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0023"].verify_device_address()

            # verify quad valve biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0024"].verify_device_address()

            # verify twelve valve biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA1001"].verify_device_address()

            # verify twelve biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA1002"].verify_device_address()

            # verify twelve biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA1003"].verify_device_address()

            # verify twelve biCoder values on the SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA2001"].verify_device_address()

            # verify twelve valve biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA2002"].verify_device_address()

            # verify twelve valve biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA2003"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Pumps
                        Single          "PMV0001"       0

                        Single          "PMV0002"       0

                        Dual            "PMV0011"       0
                                        "PMV0011"       0

                        Dual            "PMV0021"       0
                                        "PMV0021"       0
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify pump biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0001"].verify_device_address()

            # verify pump biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0002"].verify_device_address()

            # verify pump biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0011"].verify_device_address()

            # verify pump biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0012"].verify_device_address()

            # verify pump biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0021"].verify_device_address()

            # verify pump biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0022"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Master Valves     Single          "TMV0001"       0

                            Single          "TMV0002"       0

                            Dual            "TMV0011"       0
                                            "TMV0012"       0

                            Dual            "TMV0021"       0
                                            "TMV0022"       0
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify master_valve biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0001"].verify_device_address()

            # verify master_valve biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0002"].verify_device_address()

            # verify master_valve biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0011"].verify_device_address()

            # verify master_valve biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0012"].verify_device_address()

            # verify master_valve biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0021"].verify_device_address()

            # verify master_valve biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0022"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        address Devices on the controller:

        Device Type     biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Flow Meters   Flow            "TWF0001"       0
                        Flow            "TWF0002"       0
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify flow biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].flow_bicoders["TWF0001"].verify_device_address()

            # verify flow biCoder values on the SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].flow_bicoders["TWF0002"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Moisture Sensors  Moisture        "SB00001"       0
                            Moisture        "SB00002"       0
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify moisture biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].moisture_bicoders["SB00001"].verify_device_address()

            # verify moisture biCoder values on the SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].moisture_bicoders["SB00002"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        address Devices on the controller:

        Device Type             biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Temperature Sensor    Temperature    "TAT0001"       0
                                Temperature    "TAT0002"       0
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify temperature biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].temperature_bicoders["TAT0001"].verify_device_address()

            # verify temperature biCoder values on the SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].temperature_bicoders["TAT0002"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Event Switches    Switch          "TPD0001"       0
                            Switch          "TPD0002"       0
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify switch biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].switch_bicoders["TPD0001"].verify_device_address()

            # verify switch biCoder values on the SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].switch_bicoders["TPD0002"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Pressure Sensors  Analog          "PSD0001"       0
                            Analog          "PSD0002"       0
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            pass
            # verify analog biCoder values and address 0 on the 3200
            # self.config.BaseStation3200[1].analog_bicoders["PSD0001"].verify_device_address()

            # verify analog biCoder values on the SubStation and address 0 on the 3200
            # self.config.BaseStation3200[1].analog_bicoders["PSD0002"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        load devices on the 3200 will not be using json for this test
        Controller devices loaded:

            BiCoder Type        Serial Number on 3200   Serial Number on Substation
            -----------------------------------------------------------------------
            - Single Valve
                                "TSD0003"               "TSD0004"
            -----------------------------------------------------------------------
            - Dual Valve
                                "TSE0031"               "TSE0031"
            -----------------------------------------------------------------------
            - Quad Valve
                                "TSQ0031"               "TSQ0041"
            -----------------------------------------------------------------------
            - Twelve Valve
                                "TVA3001"               "TVA40001"
            -----------------------------------------------------------------------
            - Master Valves
                                "TMV0003"               "TMV0003"
                                "TMV0031"               "TMV0041"
            -----------------------------------------------------------------------
            - Pump
                                "PMV0003"               "PMV0003"
                                "PMV0031"               "PMV0041"
            -----------------------------------------------------------------------
            - Flow
                                "TWF0003"               "TWF0004"
            -----------------------------------------------------------------------
            - Moisture
                                "SB00003"               "SB00004"
            -----------------------------------------------------------------------
            - Temperature
                                "TAT0003"               "TAT0004"
            -----------------------------------------------------------------------
            - Switch
                                "TPD0003"               "TPD0004"
            -----------------------------------------------------------------------
            - Analog
                                "PSD0003"               "PSD0004"
            -----------------------------------------------------------------------
        :return:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # Clear all devices on 3200
            # self.config.BaseStation3200[1].clear_all_devices()
            self.config.BaseStation3200[1].substations[1].clear_all_devices()
            self.config.BaseStation3200[1].substations[1].clear_all_programming()
            # This was added so we don't have any leftover biCoder test engine objects in our substation object
            self.config.BaseStation3200[1].substations[1].clear_bicoder_objects()

            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            self.config.BaseStation3200[1].do_search_for_zones()

            # load devices on 3200
            self.config.BaseStation3200[1].load_all_dv(d1_list=["TSD0003"],
                                                       mv_d1_list=["TMV0003"],
                                                       pm_d1_list=["PMV0003"],
                                                       d2_list=["TSE0031"],
                                                       mv_d2_list=["TMV0031"],
                                                       pm_d2_list=["PMV0031"],
                                                       d4_list=["TSQ0031"],
                                                       dd_list=["TVA3001"],
                                                       ms_list=["SB00003"],
                                                       fm_list=["TWF0003"],
                                                       ts_list=["TAT0003"],
                                                       sw_list=["TPD0003"])
                                                       # an_list=["PSD0003"])

            self.config.SubStations[1].load_all_dv(d1_list=["TSD0004"],
                                                   mv_d1_list=["TMV0004"],
                                                   pm_d1_list=["PMV0004"],
                                                   d2_list=["TSE0041"],
                                                   mv_d2_list=["TMV0041"],
                                                   pm_d2_list=["PMV0041"],
                                                   d4_list=["TSQ0041"],
                                                   dd_list=["TVA4001"],
                                                   ms_list=["SB00004"],
                                                   fm_list=["TWF0004"],
                                                   ts_list=["TAT0004"],
                                                   sw_list=["TPD0004"],
                                                   # an_list=["PSD0004"]
                                                   )
            
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        Do a search for all biCoders
        verify that all devices show up in the 3200 but they all have an address of zero
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # one search will populate all devices
            self.config.BaseStation3200[1].do_search_for_zones()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        address Devices on the controller:

        Device Type     biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Zones
                        Single          "TSD0003"       1

                        Single          "TSD0004"       2

                        Dual            "TSE0031"       3
                                        "TSE0032"       4

                        Dual            "TSE0041"       5
                                        "TSE0042"       6

                        Quad            "TSQ0031"       7
                                        "TSQ0032"       8
                                        "TSQ0033"       9
                                        "TSQ0034"       10

                        Quad            "TSQ0041"       11
                                        "TSQ0042"       12
                                        "TSQ0043"       13
                                        "TSQ0044"       14

                        Twelve          "TVA3001"       15
                                        "TVA3002"       16
                                        "TVA3003"       17

                        Twelve          "TVA40001"      18
                                        "TVA40002"      19
                                        "TVA40003"      20
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # - Zones
            # add single valve biCoder to zone address 1 on the 3200
            self.config.BaseStation3200[1].add_zone_to_controller(_address=1,_serial_number='TSD0003')
            self.config.BaseStation3200[1].valve_bicoders["TSD0003"].verify_device_address()

            # add single valve biCoder from SubStation to zone address 2 on the 3200
            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=1, _device_serial='TSD0002')
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSD0004'].add_to_zone(_address=2)
            # self.config.BaseStation3200[1].add_zone_to_controller(_address=2, _serial_number='TSD0002')
            self.config.BaseStation3200[1].valve_bicoders["TSD0004"].verify_device_address()

            # add dual valve biCoder to zone addresses 3, 4 on the 3200
            self.config.BaseStation3200[1].add_zone_to_controller(_address=3, _serial_number='TSE0031')
            self.config.BaseStation3200[1].valve_bicoders["TSE0031"].verify_device_address()

            self.config.BaseStation3200[1].add_zone_to_controller(_address=4, _serial_number='TSE0032')
            self.config.BaseStation3200[1].valve_bicoders["TSE0032"].verify_device_address()

            # add dual valve biCoder from SubStation to zone addresses 5, 6 on the 3200
            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=1, _device_serial='')
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0041'].add_to_zone(_address=5)
            # self.config.BaseStation3200[1].add_zone_to_controller(_address=5, _serial_number='TSE0041')
            self.config.BaseStation3200[1].valve_bicoders["TSE0041"].verify_device_address()

            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=1, _device_serial='TSE0042')
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSE0042'].add_to_zone(_address=6)
            # self.config.BaseStation3200[1].add_zone_to_controller(_address=6, _serial_number='TSE0042')
            self.config.BaseStation3200[1].valve_bicoders["TSE0042"].verify_device_address()

            # add quad valve biCoder to zone addresses 7,8,9,10 on the 3200
            self.config.BaseStation3200[1].add_zone_to_controller(_address=7, _serial_number='TSQ0031')
            self.config.BaseStation3200[1].valve_bicoders["TSQ0031"].verify_device_address()

            self.config.BaseStation3200[1].add_zone_to_controller(_address=8, _serial_number='TSQ0032')
            self.config.BaseStation3200[1].valve_bicoders["TSQ0032"].verify_device_address()

            self.config.BaseStation3200[1].add_zone_to_controller(_address=9, _serial_number='TSQ0033')
            self.config.BaseStation3200[1].valve_bicoders["TSQ0033"].verify_device_address()

            self.config.BaseStation3200[1].add_zone_to_controller(_address=10, _serial_number='TSQ0034')
            self.config.BaseStation3200[1].valve_bicoders["TSQ0034"].verify_device_address()

            # add quad valve biCoder from SubStation to zone addresses 11,12,13,14 on the 3200
            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=1, _device_serial='TSQ0041')
            # self.config.BaseStation3200[1].add_zone_to_controller(_address=11, _serial_number='TSQ0041')
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSQ0041'].add_to_zone(_address=11)
            self.config.BaseStation3200[1].valve_bicoders["TSQ0041"].verify_device_address()

            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=1, _device_serial='TSQ0042')
            # self.config.BaseStation3200[1].add_zone_to_controller(_address=12, _serial_number='TSQ0042')
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSQ0042'].add_to_zone(_address=12)
            self.config.BaseStation3200[1].valve_bicoders["TSQ0042"].verify_device_address()

            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=1, _device_serial='TSQ0043')
            # self.config.BaseStation3200[1].add_zone_to_controller(_address=13, _serial_number='TSQ0043')
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSQ0043'].add_to_zone(_address=13)
            self.config.BaseStation3200[1].valve_bicoders["TSQ0043"].verify_device_address()

            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=1, _device_serial='TSQ0044')
            # self.config.BaseStation3200[1].add_zone_to_controller(_address=14, _serial_number='TSQ0044')
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSQ0044'].add_to_zone(_address=14)
            self.config.BaseStation3200[1].valve_bicoders["TSQ0044"].verify_device_address()

            # add Twelve valve biCoder to zone addresses 15,16,17 on the 3200
            self.config.BaseStation3200[1].add_zone_to_controller(_address=15, _serial_number='TVA3001')
            self.config.BaseStation3200[1].valve_bicoders["TVA3001"].verify_device_address()

            self.config.BaseStation3200[1].add_zone_to_controller(_address=16, _serial_number='TVA3002')
            self.config.BaseStation3200[1].valve_bicoders["TVA3002"].verify_device_address()

            self.config.BaseStation3200[1].add_zone_to_controller(_address=17, _serial_number='TVA3003')
            self.config.BaseStation3200[1].valve_bicoders["TVA3003"].verify_device_address()

            # add Twelve valve biCoder from SubStation to zone addresses 18,19,20 on the 3200
            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=1, _device_serial='TVA4001')
            # self.config.BaseStation3200[1].add_zone_to_controller(_address=18, _serial_number='TVA4001')
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA4001'].add_to_zone(_address=18)
            self.config.BaseStation3200[1].valve_bicoders["TVA4001"].verify_device_address()

            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=1, _device_serial='TVA4002')
            # self.config.BaseStation3200[1].add_zone_to_controller(_address=19, _serial_number='TVA4002')
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA4002'].add_to_zone(_address=19)
            self.config.BaseStation3200[1].valve_bicoders["TVA4002"].verify_device_address()

            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=1, _device_serial='TVA4003')
            # self.config.BaseStation3200[1].add_zone_to_controller(_address=20, _serial_number='TVA4003')
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TVA4003'].add_to_zone(_address=20)
            self.config.BaseStation3200[1].valve_bicoders["TVA4003"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Pumps
                        Single          "PMV0003"       1

                        Single          "PMV0004"       2

                        Dual            "PMV0031"       3
                                        "PMV0041"       4

                        Dual            "PMV0031"       5
                                        "PMV0041"       6
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # add single valve biCoder to pump addresses 1 on the 3200
            self.config.BaseStation3200[1].add_pump_to_controller(_address=1, _serial_number='PMV0003')
            self.config.BaseStation3200[1].valve_bicoders["PMV0003"].verify_device_address()

            # add single valve biCoder from SubStation to pump addresses 2 on the 3200
            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=1, _device_serial='PMV0003')
            # self.config.BaseStation3200[1].add_pump_to_controller(_address=2, _serial_number='PMV0003')
            self.config.BaseStation3200[1].substations[1].valve_bicoders["PMV0004"].add_to_pump(_address=2)
            self.config.BaseStation3200[1].valve_bicoders["PMV0004"].verify_device_address()

            # add dual valve biCoder to pump addresses 3,4 on the 3200
            self.config.BaseStation3200[1].add_pump_to_controller(_address=3, _serial_number='PMV0031')
            self.config.BaseStation3200[1].valve_bicoders["PMV0031"].verify_device_address()

            self.config.BaseStation3200[1].add_pump_to_controller(_address=4, _serial_number='PMV0032')
            self.config.BaseStation3200[1].valve_bicoders["PMV0032"].verify_device_address()

            # add dual valve biCoder from SubStation to zone addresses 5,6 on the 3200
            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=5, _device_serial='PMV0041')
            # self.config.BaseStation3200[1].add_pump_to_controller(_address=5, _serial_number='PMV0041')
            self.config.BaseStation3200[1].substations[1].valve_bicoders["PMV0041"].add_to_pump(_address=5)
            self.config.BaseStation3200[1].valve_bicoders["PMV0041"].verify_device_address()

            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=6, _device_serial='PMV0042')
            # self.config.BaseStation3200[1].add_pump_to_controller(_address=6, _serial_number='PMV0042')
            self.config.BaseStation3200[1].substations[1].valve_bicoders["PMV0042"].add_to_pump(_address=6)
            self.config.BaseStation3200[1].valve_bicoders["PMV0042"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Master Valves     Single          "TMV0003"       1

                            Single          "TMV0004"       2

                            Dual            "TMV0031"       3
                                            "TMV0042"       4

                            Dual            "TMV0031"       5
                                            "TMV0041"       6
        -----------------------------------------------------------------------

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # add single valve biCoder to master valve addresses 1 on the 3200
            self.config.BaseStation3200[1].add_master_valve_to_controller(_address=1, _serial_number='TMV0003')
            self.config.BaseStation3200[1].valve_bicoders["TMV0003"].verify_device_address()

            # add single valve biCoder from SubStation to zone addresses 2 on the 3200
            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=1, _device_serial='TMV0004')
            # self.config.BaseStation3200[1].add_master_valve_to_controller(_address=2, _serial_number='TMV0004')
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0004"].add_to_master_valve(_address=2)
            self.config.BaseStation3200[1].valve_bicoders["TMV0004"].verify_device_address()

            # add dual valve biCoder to master valve addresses 3,4 on the 3200
            self.config.BaseStation3200[1].add_master_valve_to_controller(_address=3, _serial_number='TMV0031')
            self.config.BaseStation3200[1].valve_bicoders["TMV0031"].verify_device_address()

            self.config.BaseStation3200[1].add_master_valve_to_controller(_address=4, _serial_number='TMV0032')
            self.config.BaseStation3200[1].valve_bicoders["TMV0032"].verify_device_address()

            # add dual valve biCoder from SubStation to zone addresses 5,6 on the 3200
            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=5, _device_serial='TMV0041')
            # self.config.BaseStation3200[1].add_master_valve_to_controller(_address=5, _serial_number='TMV0041')
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0041"].add_to_master_valve(_address=5)
            self.config.BaseStation3200[1].valve_bicoders["TMV0041"].verify_device_address()

            # self.config.BaseStation3200[1].add_substation_valve_bicoder(_substation_address=6, _device_serial='TMV0042')
            # self.config.BaseStation3200[1].add_master_valve_to_controller(_address=6, _serial_number='TMV0042')
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0042"].add_to_master_valve(_address=6)
            self.config.BaseStation3200[1].valve_bicoders["TMV0042"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        address Devices on the controller:

        Device Type     biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Flow Meters   Flow            "TWF0003"       1
                        Flow            "TWF0004"       2
        -----------------------------------------------------------------------


        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # add flow biCoder to flow meter addresses a on the 3200
            self.config.BaseStation3200[1].add_flow_meter_to_controller(_address=1, _serial_number='TWF0003')
            self.config.BaseStation3200[1].flow_bicoders["TWF0003"].verify_device_address()

            # add flow biCoder from SubStation to flow meter addresses 2 on the 3200
            # self.config.BaseStation3200[1].add_substation_flow_bicoder(_substation_address=1,
            #                                                            _device_serial='TWF0004')
            # self.config.BaseStation3200[1].add_flow_meter_to_controller(_address=2, _serial_number='TWF0004')

            self.config.BaseStation3200[1].substations[1].flow_bicoders["TWF0004"].add_to_flow_meter(_address=2)
            self.config.BaseStation3200[1].flow_bicoders["TWF0004"].verify_device_address()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Moisture Sensors  Moisture        "SB00003"       1
                            Moisture        "SB00004"       2
        -----------------------------------------------------------------------


        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # add moisture biCoder to temperature sensor addresses a on the 3200
            self.config.BaseStation3200[1].add_moisture_sensor_to_controller(_address=1, _serial_number='SB00003')
            self.config.BaseStation3200[1].moisture_bicoders["SB00003"].verify_device_address()

            # add flow biCoder from SubStation to temperature sensor addresses 2 on the 3200
            # self.config.BaseStation3200[1].add_substation_moisture_bicoder(_substation_address=1,
            #                                                                _device_serial='SB00004')
            # self.config.BaseStation3200[1].add_moisture_sensor_to_controller(_address=2, _serial_number='SB00004')
            self.config.BaseStation3200[1].substations[1].moisture_bicoders["SB00004"].add_to_moisture_sensor(_address=2)
            self.config.BaseStation3200[1].moisture_bicoders["SB00004"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        address Devices on the controller:

        Device Type             biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Temperature Sensor    Temperature    "TAT0003"       1
                                Temperature    "TAT0004"       2
        -----------------------------------------------------------------------

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # add temperature biCoder to temperature sensor addresses a on the 3200
            self.config.BaseStation3200[1].add_temperature_sensor_to_controller(_address=1, _serial_number='TAT0003')
            self.config.BaseStation3200[1].temperature_bicoders["TAT0003"].verify_device_address()

            # add temperature biCoder from SubStation to temperature sensor addresses 2 on the 3200
            # self.config.BaseStation3200[1].add_substation_temperature_bicoder(_substation_address=1,
            #                                                                   _device_serial='TAT0004')
            # self.config.BaseStation3200[1].add_temperature_sensor_to_controller(_address=2, _serial_number='TAT0004')
            self.config.BaseStation3200[1].substations[1].temperature_bicoders["TAT0004"].add_to_temperature_sensor(_address=2)
            self.config.BaseStation3200[1].temperature_bicoders["TAT0004"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Event Switches    Switch          "TPD0003"       1
                            Switch          "TPD0004"       2
        -----------------------------------------------------------------------

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # add event biCoder to event switch addresses a on the 3200
            self.config.BaseStation3200[1].add_event_switch_to_controller(_address=1, _serial_number='TPD0003')
            self.config.BaseStation3200[1].switch_bicoders["TPD0003"].verify_device_address()

            # add event biCoder from SubStation to event switch addresses 2 on the 3200
            # self.config.BaseStation3200[1].add_substation_switch_bicoder(_substation_address=1,
            #                                                              _device_serial='TPD0004')
            # self.config.BaseStation3200[1].add_event_switch_to_controller(_address=2, _serial_number='TPD0004')
            self.config.BaseStation3200[1].substations[1].switch_bicoders["TPD0004"].add_to_event_switch(_address=2)
            self.config.BaseStation3200[1].switch_bicoders["TPD0004"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Pressure Sensors  Analog          "PSD0003"       1
                            Analog          "PSD0004"       2
        -----------------------------------------------------------------------

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            pass
            # add analog biCoder to pressure sensor addresses a on the 3200
            # self.config.BaseStation3200[1].add_pressure_sensor_to_controller(_address=1, _serial_number='PSD0003')
            # self.config.BaseStation3200[1].analog_bicoders["PSD0003"].verify_device_address()

            # # add analog biCoder from SubStation to pressure sensor addresses 2 on the 3200
            # self.config.BaseStation3200[1].add_substation_analog_bicoder(_substation_address=1,
            #                                                              _device_serial='PSD0004')
            # self.config.BaseStation3200[1].add_pressure_sensor_to_controller(_address=2, _serial_number='PSD0004')
            # self.config.BaseStation3200[1].substations[1].analog_bicoders["PSD0004"].add_to_pressure_sensor(_address=2)
            # self.config.BaseStation3200[1].analog_bicoders["PSD0004"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        Verify fist valve biCoders are not present:

        Device Type     biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Zones
                        Single          "TSD0001"

                        Single          "TSD0002"

                        Dual            "TSE0011"
                                        "TSE0012"

                        Dual            "TSE0021"
                                        "TSE0022"

                        Quad            "TSQ0011"
                                        "TSQ0012"
                                        "TSQ0013"
                                        "TSQ0014"

                        Quad            "TSQ0021"
                                        "TSQ0022"
                                        "TSQ0023"
                                        "TSQ0024"

                        Twelve          "TVA1001"
                                        "TVA1002"
                                        "TVA1003"

                        Twelve          "TVA20001"
                                        "TVA20002"
                                        "TVA20003"
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify single biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSD0001"].verify_bicoder_present()

            # verify single valve biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSD0002"].verify_bicoder_not_present()

            # verify dual valve biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSE0011"].verify_bicoder_present()

            # verify dual valve biCoder not present on  the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSE0012"].verify_bicoder_present()

            # verify dual valve biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSE0021"].verify_bicoder_not_present()

            # verify dual valve biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSE0022"].verify_bicoder_not_present()

            # verify quad valve biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0011"].verify_bicoder_present()

            # verify quad valve biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0012"].verify_bicoder_present()

            # verify quad biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0013"].verify_bicoder_present()

            # verify quad biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0014"].verify_bicoder_present()

            # verify quad valve biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0021"].verify_bicoder_not_present()

            # verify quad valve biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0022"].verify_bicoder_not_present()

            # verify quad valve biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0023"].verify_bicoder_not_present()

            # verify quad valve biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0024"].verify_bicoder_not_present()

            # verify twelve valve biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA1001"].verify_bicoder_present()

            # verify twelve biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA1002"].verify_bicoder_present()

            # verify twelve biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA1003"].verify_bicoder_present()

            # verify twelve biCoder biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA2001"].verify_bicoder_not_present()

            # verify twelve valve biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA2002"].verify_bicoder_not_present()

            # verify twelve valve biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA2003"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_23(self):
        """
        Verify fist pump biCoders are not present:

        Device Type         biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Pumps
                        Single          "PMV0001"

                        Single          "PMV0002"

                        Dual            "PMV0011"
                                        "PMV0012"

                        Dual            "PMV0021"
                                        "PMV0022"
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify pump biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0001"].verify_bicoder_present()

            #  verify pump biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0002"].verify_bicoder_not_present()

            # verify pump biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0011"].verify_bicoder_present()

            # verify pump biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0012"].verify_bicoder_present()

            #  verify pump biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0021"].verify_bicoder_not_present()

            # v verify pump biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0022"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_24(self):
        """
        Verfiy fist master valve biCoders are not present:

        Device Type         biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Master Valves     Single          "TMV0001"

                            Single          "TMV0002"

                            Dual            "TMV0011"
                                            "TMV0012"

                            Dual            "TMV0021"
                                            "TMV0022"
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify master_valve biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0001"].verify_bicoder_present()

            # v verify master valve biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0002"].verify_bicoder_not_present()

            # verify master_valve biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0011"].verify_bicoder_present()

            # verify master_valve biCoder present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0012"].verify_bicoder_present()

            #  verify master valve biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0021"].verify_bicoder_not_present()

            #  verify master valve biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0022"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_25(self):
        """
        Verify fist flow biCoders are not present:

        Device Type     biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Flow Meters   Flow            "TWF0001"
                        Flow            "TWF0002"
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify flow biCoder values present on the 3200
            self.config.BaseStation3200[1].flow_bicoders["TWF0001"].verify_bicoder_present()

            # v verify flow biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].flow_bicoders["TWF0002"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_26(self):
        """
        aVerify fist moisture biCoders are not present:

        Device Type         biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Moisture Sensors  Moisture        "SB00001"
                            Moisture        "SB00002"
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify moisture biCoder present on the 3200
            self.config.BaseStation3200[1].moisture_bicoders["SB00001"].verify_bicoder_present()

            #  verify moisture biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].moisture_bicoders["SB00002"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_27(self):
        """
        Verify fist temperature biCoders are not present:

        Device Type             biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Temperature Sensor    Temperature    "TAT0001"
                                Temperature    "TAT0002"
        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify temperature biCoder present on the 3200
            self.config.BaseStation3200[1].temperature_bicoders["TAT0001"].verify_bicoder_present()

            # verify temperature biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].temperature_bicoders["TAT0002"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_28(self):
        """
        Verify fist switch biCoders are not present:

        Device Type         biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Event Switches    Switch          "TPD0001"
                            Switch          "TPD0002"
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify switch biCoder present on the 3200
            self.config.BaseStation3200[1].switch_bicoders["TPD0001"].verify_bicoder_present()

            # verify switch biCoder not present on the SubStation or the 3200
            self.config.BaseStation3200[1].switch_bicoders["TPD0002"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_29(self):
        """
        Verify fist analog biCoders are not present:

        Device Type         biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Pressure Sensors  Analog          "PSD0001"
                            Analog          "PSD0002"
        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            pass
            # verify analog biCoder present on the 3200
            # self.config.BaseStation3200[1].analog_bicoders["PSD0001"].verify_bicoder_not_present()

            # verify analog biCoder not present on the SubStation or the 3200
            # self.config.BaseStation3200[1].analog_bicoders["PSD0002"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_31(self):
        """
        do a self test on all devices \n
        increment the clock by 1 minute \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()
            for moisture_sensors in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[moisture_sensors].\
                    bicoder.self_test_and_update_object_attributes()
            for master_valves in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[master_valves].\
                    bicoder.self_test_and_update_object_attributes()
            for flow_meters in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseStation3200[1].flow_meters[flow_meters].bicoder.self_test_and_update_object_attributes()
            for event_switches in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseStation3200[1].event_switches[event_switches].\
                    bicoder.self_test_and_update_object_attributes()
            for temperature_sensors in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[temperature_sensors].\
                    bicoder.self_test_and_update_object_attributes()
            for pump in sorted(self.config.BaseStation3200[1].pumps.keys()):
                self.config.BaseStation3200[1].pumps[pump].bicoder.self_test_and_update_object_attributes()
            for pressure_sensor in sorted(self.config.BaseStation3200[1].pressure_sensors.keys()):
                self.config.BaseStation3200[1].pressure_sensors[pressure_sensor].\
                    bicoder.self_test_and_update_object_attributes()

            # Self test all SubStation bicoders and udpate theri attributes
            for valve_sn in self.config.BaseStation3200[1].substations[1].valve_bicoders.keys():
                self.config.BaseStation3200[1].substations[1].valve_bicoders[valve_sn].self_test_and_update_object_attributes()
            for flow_sn in self.config.BaseStation3200[1].substations[1].flow_bicoders.keys():
                self.config.BaseStation3200[1].substations[1].flow_bicoders[flow_sn].self_test_and_update_object_attributes()
            for switch_sn in self.config.BaseStation3200[1].substations[1].switch_bicoders.keys():
                self.config.BaseStation3200[1].substations[1].switch_bicoders[switch_sn].self_test_and_update_object_attributes()
            for moisture_sn in self.config.BaseStation3200[1].substations[1].moisture_bicoders.keys():
                self.config.BaseStation3200[1].substations[1].moisture_bicoders[moisture_sn].self_test_and_update_object_attributes()
            for temperature_sn in self.config.BaseStation3200[1].substations[1].temperature_bicoders.keys():
                self.config.BaseStation3200[1].substations[1].temperature_bicoders[temperature_sn].self_test_and_update_object_attributes()
            for analog_sn in self.config.BaseStation3200[1].substations[1].analog_bicoders.keys():
                self.config.BaseStation3200[1].substations[1].analog_bicoders[analog_sn].self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_32(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller agaist information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)
            
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
