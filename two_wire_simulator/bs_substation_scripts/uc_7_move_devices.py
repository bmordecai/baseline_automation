import sys
from time import sleep

# import common.product as helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes, csv
from common import helper_methods

# import log_handler for logging functionality
from common.logging_handler import log_handler

__author__ = 'bens'


class UseCase7MoveDevices(object):
    """
    Test name:
        - Substation Move Devices use case.

    Multi-Controller Setup Guide:
    
    Purpose:
        - To verify working state when moving two wire devices from 3200 to the substation and vice-versa. 
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Load devices onto the 3200.
        2. Address and set default values on 3200.
        3. Verify both controller and substation configurations.
        4. Load same devices onto the Substation & search on 3200.
        5. Verify both controller and substation configurations.
        6. Load devices back onto the 3200 and verify controller and substation configurations.
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - Only tests using 1 substation.
        
    Controller Programming:
        - Zones:                1   (1-local, 0-shared) 
        - Master Valves:        1   (1-local, 0-shared)
        - Event Switches:       1   (1-local, 0-shared)
        - Temp Sensors:         1   (1-local, 0-shared)
        - Moisture Sensors:     1   (1-local, 0-shared)
        - Flow Meters:          1   (1-local, 0-shared)
        - Programs:             0
        - Zone Programs:        0
        - Mainlines:            0
        - Pocs:                 0
        
    Substation Programming:
        - Valve BiCoders:       0
        - Moisture BiCoders:    0   
        - Temperature BiCoders: 0
        - Switch BiCoders:      0 
        - Flow BiCoders:        0
        - Pump BiCoders:        0
    """

    #################################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )

        self.run_use_case()

    #################################
    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        Initialize starting date/time
        ############################
        """
        helper_methods.print_method_name()
        try:
            # set computer time
            date_mngr.set_current_date_to_match_computer()

            # set both the 3200 and the substation to be the same time as your computer
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.SubStations,
                _date=date_mngr.curr_day.date_string_for_controller(),
                _time=date_mngr.curr_day.time_string_for_controller()
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        Connect 3200 to Substations
        ############################
        
        Connect controller to substations:
        - uses the Ip address in the user config files to set the ip address
            Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                  incremented 1 minute at a time for the max minutes or until max number of minutes is met.
        - after connection is established increment clock for each controller 1 minute
            Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                  between controllers.
        """
        helper_methods.print_method_name()
        try:
            # Connect 3200 to substation 1
            self.config.BaseStation3200[1].add_substation_to_controller(
                _substation_address=1,
                _substation=self.config.SubStations[1]
            )

            # Increment clocks after connection established
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        Finish initial device setup on 3200
        ############################
        
        Setting up devices on each controller:
            - Search on 3200 for devices to load any substation devices.

        Controller devices loaded:
                        
            Device Type             Address         Serial Number
            -----------------------------------------------------
            Zones                   1               "TSD0001"
            -----------------------------------------------------
            Flow Meters             1               "TWF0001"
            -----------------------------------------------------
            Master Valves           1               "TMV0001"
            -----------------------------------------------------
            Moisture Sensors        1               "SB05308"
            -----------------------------------------------------
            Temperature Sensors     1               "TAT0001"
            -----------------------------------------------------
            Event Switches          1               "TPD0001"
            -----------------------------------------------------
        """
        helper_methods.print_method_name()
        try:
            # Do search to detect any substation devices (in this case, substation has no devices yet).
            self.config.BaseStation3200[1].do_search_for_zones()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        Verify initial device setup
        ############################
        
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        helper_methods.print_method_name()
        try:
            # Increment clock to save configuration
            helper_methods.increment_controller_substation_clocks(
                    controller=self.config.BaseStation3200[1],
                    substations=self.config.BaseStation3200[1].substations,
                    minutes=1)

            # Verify full config
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        Load (moved) devices onto Substation
        ############################
        
        1) Move bicoders onto substation two-wire.
        2) Search for devices on 3200.
        
        Substation bicoders:
        
            BiCoder Type            Address on 3200         Serial Number
            -------------------------------------------------------------
            Valve                   1 (ZN)                  "TSD0001"
                                    1 (MV)                  "TMV0001"
            -------------------------------------------------------------
            Flow                    1                       "TWF0001"
            -------------------------------------------------------------
            Moisture                1                       "SB05308"
            -------------------------------------------------------------
            Temperature             1                       "TAT0001"
            -------------------------------------------------------------
            Switch                  1                       "TPD0001"
            -------------------------------------------------------------
        """
        helper_methods.print_method_name()
        try:
            # Move devices onto Substation
            
            # Move Zone 1's bicoder from 3200 two-wire to substation two-wire
            self.config.BaseStation3200[1].zones[1].bicoder.move_from_3200_to_substation(
                _substation=self.config.BaseStation3200[1].substations[1])

            # Move Master Valve 1's bicoder from 3200 two-wire to substation two-wire
            self.config.BaseStation3200[1].master_valves[1].bicoder.move_from_3200_to_substation(
                _substation=self.config.BaseStation3200[1].substations[1])

            # Move Flow Meter 1's bicoder from 3200 two-wire to substation two-wire
            self.config.BaseStation3200[1].flow_meters[1].bicoder.move_from_3200_to_substation(
                _substation=self.config.BaseStation3200[1].substations[1])

            # Move Moisture Sensor 1's bicoder from 3200 two-wire to substation two-wire
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.move_from_3200_to_substation(
                _substation=self.config.BaseStation3200[1].substations[1])

            # Move Temperature Sensor 1's bicoder from 3200 two-wire to substation two-wire
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.move_from_3200_to_substation(
                _substation=self.config.BaseStation3200[1].substations[1])

            # Move Event Switch 1's bicoder from 3200 two-wire to substation two-wire
            self.config.BaseStation3200[1].event_switches[1].bicoder.move_from_3200_to_substation(
                _substation=self.config.BaseStation3200[1].substations[1])
            
            # Increment both clocks for programming to hold
            helper_methods.increment_controller_substation_clocks(
                    controller=self.config.BaseStation3200[1],
                    substations=self.config.BaseStation3200[1].substations,
                    minutes=1)

            # Search for devices on controller to load them from substation to controller
            self.config.BaseStation3200[1].do_search_for_zones()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ############################
        Update 3200 devices to be "shared" with Substation
        ############################
        
        Substation bicoders:
        
            BiCoder Type            Address on 3200         Serial Number
            -------------------------------------------------------------
            Valve                   1 (ZN)                  "TSD0001"
                                    1 (MV)                  "TMV0001"
            -------------------------------------------------------------
            Flow                    1                       "TWF0001"
            -------------------------------------------------------------
            Moisture                1                       "SB05308"
            -------------------------------------------------------------
            Temperature             1                       "TAT0001"
            -------------------------------------------------------------
            Switch                  1                       "TPD0001"
            -------------------------------------------------------------
        """
        helper_methods.print_method_name()
        try:
            # TODO: (01/6/2018) is this still necessary??
            
            # Update Controller Devices to be "shared"
            self.config.BaseStation3200[1].zones[1].is_shared_with_substation = True
            self.config.BaseStation3200[1].moisture_sensors[1].is_shared_with_substation = True
            self.config.BaseStation3200[1].temperature_sensors[1].is_shared_with_substation = True
            self.config.BaseStation3200[1].flow_meters[1].is_shared_with_substation = True
            self.config.BaseStation3200[1].master_valves[1].is_shared_with_substation = True
            self.config.BaseStation3200[1].event_switches[1].is_shared_with_substation = True
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        1) Self test and update device attributes after being moved from 3200 to Substation.
        2) Verify controller and substation configurations after moving devices on the two wire.
        """
        helper_methods.print_method_name()
        try:
            for zone in self.config.BaseStation3200[1].zones.keys():
                self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()
                
            for mv in self.config.BaseStation3200[1].master_valves.keys():
                self.config.BaseStation3200[1].master_valves[mv].bicoder.self_test_and_update_object_attributes()
                
            for ms in self.config.BaseStation3200[1].moisture_sensors.keys():
                self.config.BaseStation3200[1].moisture_sensors[ms].bicoder.self_test_and_update_object_attributes()
                
            for ts in self.config.BaseStation3200[1].temperature_sensors.keys():
                self.config.BaseStation3200[1].temperature_sensors[ts].bicoder.self_test_and_update_object_attributes()
                
            for fm in self.config.BaseStation3200[1].flow_meters.keys():
                self.config.BaseStation3200[1].flow_meters[fm].bicoder.self_test_and_update_object_attributes()
                
            for sw in self.config.BaseStation3200[1].event_switches.keys():
                self.config.BaseStation3200[1].event_switches[sw].bicoder.self_test_and_update_object_attributes()
                
            # Increment both clocks for programming to hold
            helper_methods.increment_controller_substation_clocks(
                    controller=self.config.BaseStation3200[1],
                    substations=self.config.BaseStation3200[1].substations,
                    minutes=1)

            # Verify controller configuration
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        1) Clear substation programming
        2) Load devices back onto the controller.
        3) Search for devices on the 3200.
        
        Controller devices loaded:
                        
            Device Type             Address         Serial Number
            -----------------------------------------------------
            Zones                   1               "TSD0001"
            -----------------------------------------------------
            Flow Meters             1               "TWF0001"
            -----------------------------------------------------
            Master Valves           1               "TMV0001"
            -----------------------------------------------------
            Moisture Sensors        1               "SB05308"
            -----------------------------------------------------
            Temperature Sensors     1               "TAT0001"
            -----------------------------------------------------
            Event Switches          1               "TPD0001"
            -----------------------------------------------------
        """
        helper_methods.print_method_name()
        try:
            # Remove devices from substation.
            self.config.BaseStation3200[1].substations[1].clear_all_programming()
    
            # Move devices from substation back to 3200
            
            # Move MV 1 bicoder from Substation back to 3200
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TMV0001'].move_from_substation_to_3200(
                _basestation3200=self.config.BaseStation3200[1])

            # Move ZN 1 bicoder from Substation back to 3200
            self.config.BaseStation3200[1].substations[1].valve_bicoders['TSD0001'].move_from_substation_to_3200(
                    _basestation3200=self.config.BaseStation3200[1])

            # Move FM 1 bicoder from Substation back to 3200
            self.config.BaseStation3200[1].substations[1].flow_bicoders['TWF0001'].move_from_substation_to_3200(
                    _basestation3200=self.config.BaseStation3200[1])

            # Move MS 1 bicoder from Substation back to 3200
            self.config.BaseStation3200[1].substations[1].moisture_bicoders['SB05308'].move_from_substation_to_3200(
                    _basestation3200=self.config.BaseStation3200[1])

            # Move TS 1 bicoder from Substation back to 3200
            self.config.BaseStation3200[1].substations[1].temperature_bicoders['TAT0001'].move_from_substation_to_3200(
                    _basestation3200=self.config.BaseStation3200[1])

            # Move SW 1 bicoder from Substation back to 3200
            self.config.BaseStation3200[1].substations[1].switch_bicoders['TPD0001'].move_from_substation_to_3200(
                    _basestation3200=self.config.BaseStation3200[1])
            
            # Increment both clocks for programming to hold
            helper_methods.increment_controller_substation_clocks(
                    controller=self.config.BaseStation3200[1],
                    substations=self.config.BaseStation3200[1].substations,
                    minutes=1)

            # Search for devices on controller
            self.config.BaseStation3200[1].do_search_for_zones()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        Update shared attribute for devices
        """
        helper_methods.print_method_name()
        try:
            # Update Controller Devices to not be shared.
            self.config.BaseStation3200[1].zones[1].is_shared_with_substation = False
            self.config.BaseStation3200[1].moisture_sensors[1].is_shared_with_substation = False
            self.config.BaseStation3200[1].temperature_sensors[1].is_shared_with_substation = False
            self.config.BaseStation3200[1].flow_meters[1].is_shared_with_substation = False
            self.config.BaseStation3200[1].master_valves[1].is_shared_with_substation = False
            self.config.BaseStation3200[1].event_switches[1].is_shared_with_substation = False
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        1) Self test and update device attributes after being moved from Substation to 3200.
        2) Verify controller and substation configurations after moving devices on the two wire.
        """
        helper_methods.print_method_name()
        try:
            for zone in self.config.BaseStation3200[1].zones.keys():
                self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()
    
            for mv in self.config.BaseStation3200[1].master_valves.keys():
                self.config.BaseStation3200[1].master_valves[mv].bicoder.self_test_and_update_object_attributes()
    
            for ms in self.config.BaseStation3200[1].moisture_sensors.keys():
                self.config.BaseStation3200[1].moisture_sensors[ms].bicoder.self_test_and_update_object_attributes()
    
            for ts in self.config.BaseStation3200[1].temperature_sensors.keys():
                self.config.BaseStation3200[1].temperature_sensors[ts].bicoder.self_test_and_update_object_attributes()
    
            for fm in self.config.BaseStation3200[1].flow_meters.keys():
                self.config.BaseStation3200[1].flow_meters[fm].bicoder.self_test_and_update_object_attributes()
    
            for sw in self.config.BaseStation3200[1].event_switches.keys():
                self.config.BaseStation3200[1].event_switches[sw].bicoder.self_test_and_update_object_attributes()
    
            # Increment both clocks for programming to hold
            helper_methods.increment_controller_substation_clocks(
                    controller=self.config.BaseStation3200[1],
                    substations=self.config.BaseStation3200[1].substations,
                    minutes=1)
    
            # Verify controller configuration
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
