import sys
from datetime import timedelta, datetime, date
from time import sleep

# import common.product as helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes, csv
from common import helper_methods

# import log_handler for logging functionality
from common.logging_handler import log_handler

__author__ = 'bens'


class UseCase10BackupRestore(object):
    """
    Test name:
        - Substation Use Case: Backup / Restore 

    Multi-Controller Setup Guide:
    
    Purpose:
        - Verify a 3200 will retain devices shared from a substation after backing up the 3200 and restoring. 
          Demonstrates and verifies this functionality using both BaseManager and USB for backup sources.
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. Initializes controller and substation.
        2. Share substation devices to controller:
        
            BiCoders:           Controller Address:       Serial Numbers:
            - Valve (ZN's)      2, 49, 50                 "TSD0002", "TSE0011", "TSE0012"
            - Valve (MV's)      N/A
            - Flow              2                         "FM00002"
            - Switch            N/A
            - Temp              N/A
            - Moisture          N/A
            - Pump              N/A
            
        3. Configures POCs, Mainlines, Programs and Program Zones.
        4. Disable the following devices on the 3200:
        
            - Zones:         2, 198
            - Flow Meters:   1, 2
        
        5. Verify 3200 and substation configuration before the backup.
        6. Backup 3200 programming to BaseManager.
        7. Clear all devices and programming on 3200.
        8. Restore 3200 programming from BaseManager.
        9. Verify 3200 and substation configuration after the restore from BaseManager.
        10. Backup 3200 programming to USB.
        11. Clear all devices and programming on 3200.
        12. Restore 3200 programming from USB.
        13. Verify 3200 and substation configuration after the restore from USB.
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - This test only uses 1 substation.
        
    Controller Programming:
        - Zones:                8   (5-local, 3-shared) 
        - Master Valves:        1   (1-local, 0-shared)
        - Event Switches:       1   (1-local, 0-shared)
        - Temp Sensors:         1   (1-local, 0-shared)
        - Moisture Sensors:     1   (1-local, 0-shared)
        - Flow Meters:          2   (1-local, 1-shared)
        - Programs:             4
        - Zone Programs:        9
        - Mainlines:            2
        - Pocs:                 2
        
    Substation Programming:
        - Valve BiCoders:       3
        - Moisture BiCoders:    0   
        - Temperature BiCoders: 0
        - Switch BiCoders:      0 
        - Flow BiCoders:        1
        - Pump BiCoders:        0
    """

    #################################
    def __init__(self, 
                 test_name, 
                 user_configuration_instance, 
                 json_configuration_file, 
                 use_basemanager_for_backup=False,
                 use_usb_for_backup=False):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
       
        :param use_basemanager_for_backup:      Use BaseManager for backup/restore process. Defaults to BaseManager. \n
        :type use_basemanager_for_backup:       bool
        
        :param use_usb_for_backup:              Use a USB for backup/restore process. Defaults to BaseManager. \n
        :type use_usb_for_backup:               bool
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )
        
        # Get flags from test runner
        self.use_basemanager_for_backup = use_basemanager_for_backup
        self.use_usb_for_backup = use_usb_for_backup

        self.run_use_case()

    #################################
    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Connect controller to substations:
        - uses the Ip address in the user config files to set the ip address
            Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                  incremented 1 minute at a time for the max minutes or until max number of minutes is met.
        - after connection is established increment clock for each controller 1 minute
            Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                  between controllers.

        """
        helper_methods.print_method_name()
        try:
            # Connect 3200 to substation 1
            self.config.BaseStation3200[1].add_substation_to_controller(
                _substation_address=1,
                _substation=self.config.SubStations[1])
        
            # Increment clocks after connection established
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        helper_methods.print_method_name()
        try:
            # set computer time
            date_mngr.set_current_date_to_match_computer()

            # set both the 3200 and the substation to be the same time as your computer
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                _date=date_mngr.curr_day.date_string_for_controller(),
                _time=date_mngr.curr_day.time_string_for_controller())
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.
        
        Controller devices loaded:
                        
            Device Type             Address         Serial Number
            -----------------------------------------------------
            Zones                   1               "TSD0001"
                                    197             "TSQ0071"
                                    198             "TSQ0072"
                                    199             "TSQ0073"
                                    200             "TSQ0074"
            -----------------------------------------------------
            Master Valves           1               "TMV0001"
            -----------------------------------------------------
            Flow Meters             1               "FM00001"
            -----------------------------------------------------
            Moisture Sensors        1               "SB00001"
            -----------------------------------------------------
            Temperature Sensors     1               "TS00001"
            -----------------------------------------------------
            Event Switches          1               "SW00001"
            -----------------------------------------------------

        Substation BiCoders Shared:
        
            BiCoder Type            Address on 3200         Serial Number
            -------------------------------------------------------------
            Valve                   2                       "TSD0002"
                                    49                      "TSE0011"
                                    50                      "TSE0012"
            -------------------------------------------------------------
            Flow                    2                       "FM00002"
            -------------------------------------------------------------
            Moisture
            -------------------------------------------------------------
            Temperature
            -------------------------------------------------------------
            Switch
            -------------------------------------------------------------
        """
        helper_methods.print_method_name()
        try:
            # Load devices from substation
            self.config.BaseStation3200[1].do_search_for_zones()

            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            # Substation: Share devices with controller.

            ############################################################################################################
            # Share devices from substation to controller. This makes the controller address the devices that it shares
            # with a substation, and then it sets the default values. We share these devices so that we can test and
            # verify that devices shared with a substation will retain their values after a reboot.

            # Zones
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSD0002"].add_to_zone(_address=2)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0011"].add_to_zone(_address=49)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0012"].add_to_zone(_address=50)

            # Flow Meters
            self.config.BaseStation3200[1].substations[1].flow_bicoders['FM00002'].add_to_flow_meter(_address=2)
            ############################################################################################################
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        increment the clock to save settings \n
        verify the entire configuration \n
        """
        helper_methods.print_method_name()
        try:
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()

            for moisture_sensors in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[moisture_sensors].bicoder.self_test_and_update_object_attributes()

            for master_valves in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[master_valves].bicoder.self_test_and_update_object_attributes()

            for flow_meters in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseStation3200[1].flow_meters[flow_meters].bicoder.self_test_and_update_object_attributes()

            for event_switches in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseStation3200[1].event_switches[event_switches].bicoder.self_test_and_update_object_attributes()

            for temperature_sensors in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[temperature_sensors].bicoder.self_test_and_update_object_attributes()

            for pump in sorted(self.config.BaseStation3200[1].pumps.keys()):
                self.config.BaseStation3200[1].pumps[pump].bicoder.self_test_and_update_object_attributes()

            for pressure_sensor in sorted(self.config.BaseStation3200[1].pressure_sensors.keys()):
                self.config.BaseStation3200[1].pressure_sensors[pressure_sensor].bicoder.self_test_and_update_object_attributes()
                
            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        Setup WaterSources
        ############################

        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

            # Add & Configure Water Source 8
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[1].water_sources[8].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].set_priority(_priority_for_water_source=3)
            self.config.BaseStation3200[1].water_sources[8].set_monthly_watering_budget(_budget=1000,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].water_sources[8].set_water_rationing_to_disabled()
            self.config.BaseStation3200[1].water_sources[8].add_switch_empty_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_switch_empty_condition_to_closed()
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_empty_wait_time(_minutes=9)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ############################
        Setup Points of Control
        ############################

        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        POC 1:
            - Enable:                       Enabled
            - Master Valve:                 1 (TMV0001)
            - Flow Meter:                   1 (FM00001)
            - Target Flow:                  500
            - Mainline:                     1
            - Priority:                     2 (medium)
            - High Flow Limit:              550
            - High Flow Shutdown:           Enabled
            - Unschedule Flow Limit:        10
            - Unscheduled Flow Shutdown:    Enabled
            - Water Budget:                 100000
            - Shutdown on over Budget:      Enabled
            - Water Rationing:              Enabled

        POC 8:
            - Enable:                       Enabled
            - Master Valve:                 1 (TMV0001)
            - Flow Meter:                   1 (FM00001)
            - Target Flow:                  50
            - Mainline:                     8
            - Priority:                     3 (low)
            - High Flow Limit:              75
            - High Flow Shutdown:           Disabled
            - Unschedule Flow Limit:        5
            - Unscheduled Flow Shutdown:    Disabled
            - Water Budget:                 1000
            - Shutdown on over Budget:      Disabled
            - Water Rationing:              Disabled
            - Event Switch:                 1 (SW00001)
            - Switch Empty Condition:       Closed
            - Empty Wait Time:              540
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(
                _limit=550,
                with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(
                _gallons=10,
                with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1
            )
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1
            )
            # Add POC 1 to WS 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1
            )

            # Add & Configure POC 8
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[8].set_high_flow_limit(
                _limit=75,
                with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].points_of_control[8].set_unscheduled_flow_limit(
                _gallons=5,
                with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].points_of_control[8].add_flow_meter_to_point_of_control(
                _flow_meter_address=1
            )
            self.config.BaseStation3200[1].points_of_control[8].add_master_valve_to_point_of_control(
                _master_valve_address=1
            )
            # Add POC 8 to WS 8
            self.config.BaseStation3200[1].water_sources[8].add_point_of_control_to_water_source(
                _point_of_control_address=8
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ############################
        Setup Mainlines
        ############################

        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
            - Add Zones ---> to Mainline

        Mainline 1:
            - Pipe Fill Time:               4 minutes
            - Target Flow:                  500 GPM
            - Limit Zones by Flow:          Enabled
            - High Variance Limit:          5%
            - High Variance Shutdown:       Enabled
            - Low Variance limit:           20%
            - Low Variance Shutdown:        Enabled

        Mainline 8:
            - Pipe Fill Time:               1 minute
            - Target Flow:                  50 GPM
            - Limit Zones by Flow:          Enabled
            - High Variance Limit:          20%
            - High Variance Shutdown:       Disabled
            - Low Variance limit:           5%
            - Low Variance Shutdown:        Disabled
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=20)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(
                _percent=5,
                _with_shutdown_enabled=True
            )
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(
                _percent=20,
                _with_shutdown_enabled=True
            )
            # Add Mainline 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            # Add & Configure Mainline 8
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[1].mainlines[8].set_enabled()
            self.config.BaseStation3200[1].mainlines[8].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[8].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_high_flow_variance_tier_one(
                _percent=10,
                _with_shutdown_enabled=False
            )
            self.config.BaseStation3200[1].mainlines[8].set_low_flow_variance_tier_one(
                _percent=5,
                _with_shutdown_enabled=False
            )
            # Add Mainline 8 to POC 8
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(_mainline_address=8)

            # Add Zones to Mainline 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=49)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=50)

            # Add Zones to Mainline 8
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=197)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=198)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=199)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=200)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        Configure Programming.
        
        Program 1:
            - Start times:              480 (8am), 540 (9am), 600 (10am), 660 (11am)
            - Enabled:                  True
            - Water Windows:            1am-6am, 11am-10pm
            - Priority:                 1
            - Max Concurrent Zones:     1 zone
            - Seasonal Adjust:          100%
            - Calendar Interval:        Week Days
            - Day Interval:             None
            - Watering Week Days:       Monday, Wednesday, Friday
            - Semi-Month Interval:      None
            - Mainline Number:          1
            - Booster Pump:             None

        Program 3:
            - Start times:              480 (8am), 540 (9am), 600 (10am), 660 (11am)
            - Enabled:                  True
            - Water Windows:            1am-6am, 11am-10pm
            - Priority:                 1
            - Max Concurrent Zones:     1 zone
            - Seasonal Adjust:          100%
            - Calendar Interval:        Odd Days
            - Day Interval:             None
            - Watering Week Days:       None
            - Semi-Month Interval:      None
            - Mainline Number:          2
            - Booster Pump:             None

        Program 4: 
            - Start times:              480 (8am), 540 (9am), 600 (10am), 660 (11am)
            - Enabled:                  True
            - Water Windows:            1am-6am, 11am-10pm
            - Priority:                 3
            - Max Concurrent Zones:     4 zones
            - Seasonal Adjust:          100%
            - Calendar Interval:        Week Days
            - Day Interval:             None
            - Watering Week Days:       Monday, Wednesday, Friday
            - Semi-Month Interval:      None
            - Mainline Number:          3
            - Booster Pump:             None

        Program 99:
            - Start times:              480 (8am), 540 (9am), 600 (10am), 660 (11am)
            - Enabled:                  True
            - Water Windows:            Sunday: 1am-6am, 12pm-10pm
                                        Monday: 1am-6am, 11am-11pm
                                        Tuesday: 1am-6am, 11am-10pm
                                        Wednesday: 1am-6am, 11am-10pm
                                        Thursday: 1am-6am, 11am-10pm
                                        Friday: 1am-6am, 11am-10pm
                                        Saturday: 1am-6am, 11am-11pm
            - Priority:                 1
            - Max Concurrent Zones:     1 zone
            - Seasonal Adjust:          100%
            - Calendar Interval:        Historical Calendar
            - Day Interval:             None
            - Watering Week Days:       None
            - Semi-Month Interval:      TODO
            - Mainline Number:          8
            - Booster Pump:             None
        """
        helper_methods.print_method_name()
        try:
            program_number_1_start_times = [480, 540, 600, 660]
            program_number_3_start_times = [480, 540, 600, 660]
            program_number_4_start_times = [480, 540, 600, 660]
            program_number_99_start_times = [480, 540, 600, 660]
            program_number_99_watering_days = [0, 0, 0, 0, 0, 0, 8, 8, 6, 5, 5, 4, 3, 3, 3, 3, 4, 5, 6, 7, 0, 0, 0, 0]
            program_number_1_water_windows = ['011111100001111111111110']
            program_number_3_water_windows = ['011111100001111111111110']
            program_number_4_water_windows = ['011111100001111111111110']
            program_number_99_water_windows = ['011111100000111111111110',
                                               '011111100001111111111111',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111111']

            # Add & Configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_number_1_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=False, _mon=True, _tues=False, _wed=True, _thurs=False, _fri=True, _sat=False
            )
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_number_1_start_times)

            # Add & Configure Program 3
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_water_window(_ww=program_number_3_water_windows)
            self.config.BaseStation3200[1].programs[3].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[3].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[3].set_watering_intervals_to_odd_days()
            self.config.BaseStation3200[1].programs[3].set_start_times(_st_list=program_number_3_start_times)

            # Add & Configure Program 4
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation3200[1].programs[4].set_enabled()
            self.config.BaseStation3200[1].programs[4].set_water_window(_ww=program_number_4_water_windows)
            self.config.BaseStation3200[1].programs[4].set_priority_level(_pr_level=3)
            self.config.BaseStation3200[1].programs[4].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[4].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=False, _mon=True, _tues=False, _wed=True, _thurs=False, _fri=True, _sat=False
            )
            self.config.BaseStation3200[1].programs[4].set_start_times(_st_list=program_number_4_start_times)

            # Add & Configure Program 99
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_water_window(_ww=program_number_99_water_windows, _is_weekly=False)
            self.config.BaseStation3200[1].programs[99].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[99].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[99].set_watering_intervals_to_semi_monthly(_sm=program_number_99_watering_days)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_number_99_start_times)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        Configure Zone Programs:
        
        Zone Program 1:
            - Zone:                 1
            - Program:              1
            - Runtime:              900 (15 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Primary Zone Number:  1 (Primary Zone)

        Zone Program 2:
            - Zone:                 2
            - Program:              1
            - Runtime:              900 (15 minutes)
            - Cycle Time:           300 (5 minutes)
            - Soak Time:            300 (5 minutes)
            - Runtime Ratio:        100%
            - Primary Zone Number:  1 (Linked)

        Zone Program 49:
            - Zone:                 49
            - Program:              3
            - Runtime:              1200 (20 minutes)
            - Cycle Time:           600 (10 minutes)
            - Soak Time:            3600 (60 minutes)
            - Runtime Ratio:        100%
            - Water Strategy:       Timed

        Zone Program 50:
            - Zone:                 50
            - Program:              3
            - Runtime:              1200 (20 minutes)
            - Cycle Time:           600 (10 minutes)
            - Soak Time:            3600 (60 minutes)
            - Water Strategy:       Timed

        Zone Program 50:
            - Zone:                 50
            - Program:              4
            - Runtime:              1200 (20 minutes)
            - Cycle Time:           600 (10 minutes)
            - Soak Time:            3600 (60 minutes)
            - Water Strategy:       Timed

        Zone Program 197:
            - Zone:                 197
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Runtime Ratio:        50%
            - Primary Zone Number:  200 (Linked)

        Zone Program 198:
            - Zone:                 198
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Runtime Ratio:        100%
            - Primary Zone Number:  200 (Linked)

        Zone Program 199:
            - Zone:                 199
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Runtime Ratio:        150%
            - Primary Zone Number:  200 (Primary Zone)

        Zone Program 200:
            - Zone:                 200
            - Program:              99
            - Runtime:              1980 (33 minutes)
            - Cycle Time:           180 (3 minutes)
            - Soak Time:            780 (13 minutes)
            - Primary Zone Number:  200 (Primary Zone)
        """
        helper_methods.print_method_name()
        try:
            # ------------------- #
            # Add Program 1 Zones #
            # ------------------- #

            # Zone Program 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            # Zone Program 2
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            # ------------------- #
            # Add Program 3 Zones #
            # ------------------- #

            # Zone Program 49
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=49)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_soak_time(_minutes=60)

            # Zone Program 50
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_soak_time(_minutes=60)

            # ------------------- #
            # Add Program 4 Zones #
            # ------------------- #

            # Zone Program 50
            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_soak_time(_minutes=60)

            # -------------------- #
            # Add Program 99 Zones #
            # -------------------- #

            # Zone Program 200
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=200)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_run_time(_minutes=33)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_cycle_time(_minutes=3)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_soak_time(_minutes=13)

            # Zone Program 197
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=197)
            self.config.BaseStation3200[1].programs[99].zone_programs[197].set_as_linked_zone(_primary_zone=200,
                                                                                              _tracking_ratio=50)

            # Zone Program 198
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=198)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_as_linked_zone(_primary_zone=200)

            # Zone Program 199
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=199)
            self.config.BaseStation3200[1].programs[99].zone_programs[199].set_as_linked_zone(_primary_zone=200,
                                                                                              _tracking_ratio=150)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Disable Devices.
        
        Coverage:
        This area covers not losing programming when a device is disabled during a backup/restore.
        
        Disable the following devices on the 3200:
            - Zones:         2, 198
            - Flow Meters:   1, 2
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].zones[2].set_disabled()
            self.config.BaseStation3200[1].zones[198].set_disabled()
            self.config.BaseStation3200[1].flow_meters[1].set_disabled()
            self.config.BaseStation3200[1].flow_meters[2].set_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Verify 3200 and substation configuration before backup/restore process to ensure our objects are in correct
        state.
        """
        helper_methods.print_method_name()
        try:
            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        1) Start 3200 clock for configuration to save.
        2) Sleep 5 seconds to give controller some time.
        3) Back up 3200 programming to BaseManager.
        """
        # Only run step if User specified backup from BaseManager
        if self.use_basemanager_for_backup:
            
            helper_methods.print_method_name()
            try:
                self.config.BaseStation3200[1].start_clock()
                sleep(5)
                self.config.BaseStation3200[1].do_backup_programming(where_to=opcodes.basemanager)
            except Exception as e:
                e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                        "\tThe Controller Date and time was currently set to {2}\n" \
                        "\tThe Exception thrown was {3}".format(
                            self.config.test_name,
                            sys._getframe().f_code.co_name,
                            date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                            str(e.message))
                raise Exception, Exception(e_msg), sys.exc_info()[2]
            
        else:
            # Print skipping method
            helper_methods.print_skipping_method(
                # use case name
                who=self.config.test_name,
                # what the step was doing
                what='Backing BaseStation3200 up to BaseManager',
                # why it was skipped
                why='BaseManager backup was disabled in product_assessments.py')

    #################################
    def step_13(self):
        """
        Clear all programming from the 3200 so we can restore from a clean state.
        """
        # Only run step if User specified backup from BaseManager
        if self.use_basemanager_for_backup:

            helper_methods.print_method_name()
            try:
                self.config.BaseStation3200[1].initialize_for_test()

                # This is needed to reset the substation clock to match the controller
                helper_methods.set_controller_substation_date_and_time(
                    controller=self.config.BaseStation3200[1],
                    substations=self.config.BaseStation3200[1].substations,
                    _date=date_mngr.controller_datetime.date_string_for_controller(),
                    _time=date_mngr.controller_datetime.time_string_for_controller())
            except Exception as e:
                e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                        "\tThe Controller Date and time was currently set to {2}\n" \
                        "\tThe Exception thrown was {3}".format(
                            self.config.test_name,
                            sys._getframe().f_code.co_name,
                            date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                            str(e.message))
                raise Exception, Exception(e_msg), sys.exc_info()[2]

        else:
            # Print skipping method
            helper_methods.print_skipping_method(
                # use case name
                who=self.config.test_name,
                # what the step was doing
                what='Clearing devices and programming from BaseStation3200 after backup',
                # why it was skipped
                why='BaseManager backup was disabled in product_assessments.py')

    #################################
    def step_14(self):
        """
        1) Restore 3200 programming from BaseManager.
        2) Run self tests on all devices to update two-wire values for verification.
        3) Verify both 3200 and substation configurations to validate successful backup/restore from BaseManager.
        """
        # Only run step if User specified backup from BaseManager
        if self.use_basemanager_for_backup:
            
            helper_methods.print_method_name()
            try:
                self.config.BaseStation3200[1].basemanager_connection[1].verify_ip_address_state()
                self.config.BaseStation3200[1].do_restore_programming(opcodes.basemanager)

                self.config.BaseStation3200[1].do_search_for_zones()

                for zone in self.config.BaseStation3200[1].zones.keys():
                    self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()

                for mv in self.config.BaseStation3200[1].master_valves.keys():
                    self.config.BaseStation3200[1].master_valves[mv].bicoder.self_test_and_update_object_attributes()

                for ms in self.config.BaseStation3200[1].moisture_sensors.keys():
                    self.config.BaseStation3200[1].moisture_sensors[ms].bicoder.self_test_and_update_object_attributes()

                for ts in self.config.BaseStation3200[1].temperature_sensors.keys():
                    self.config.BaseStation3200[1].temperature_sensors[ts].bicoder.self_test_and_update_object_attributes()

                for fm in self.config.BaseStation3200[1].flow_meters.keys():
                    self.config.BaseStation3200[1].flow_meters[fm].bicoder.self_test_and_update_object_attributes()

                for sw in self.config.BaseStation3200[1].event_switches.keys():
                    self.config.BaseStation3200[1].event_switches[sw].bicoder.self_test_and_update_object_attributes()

                # Increment both clocks for programming to hold
                helper_methods.increment_controller_substation_clocks(
                    controller=self.config.BaseStation3200[1],
                    substations=self.config.BaseStation3200[1].substations,
                    minutes=1)

                # Verify controller configuration
                self.config.BaseStation3200[1].verify_full_configuration()
                self.config.BaseStation3200[1].substations[1].verify_full_configuration()
            except Exception as e:
                e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                        "\tThe Controller Date and time was currently set to {2}\n" \
                        "\tThe Exception thrown was {3}".format(
                            self.config.test_name,
                            sys._getframe().f_code.co_name,
                            date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                            str(e.message))
                raise Exception, Exception(e_msg), sys.exc_info()[2]
            
        else:
            # Print skipping method
            helper_methods.print_skipping_method(
                # use case name
                who=self.config.test_name,
                # what the step was doing
                what='Restoring BaseStation3200 from BaseManager backup and verifying configuration',
                # why it was skipped
                why='BaseManager backup was disabled in product_assessments.py')

    #################################
    def step_15(self):
        """
        1) Start 3200 clock for configuration to save.
        2) Sleep 5 seconds to give controller some time.
        3) Back up 3200 programming to USB Device.
        
        ** NOTE: Have USB inserted into the 3200!!
        """
        # Only run step if User specified backup from USB
        if self.use_usb_for_backup:
            
            helper_methods.print_method_name()
            try:
                self.config.BaseStation3200[1].start_clock()
                sleep(5)
                self.config.BaseStation3200[1].do_backup_programming(where_to=opcodes.usb_flash_storage)
            except Exception as e:
                e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                        "\tThe Controller Date and time was currently set to {2}\n" \
                        "\tThe Exception thrown was {3}".format(
                            self.config.test_name,
                            sys._getframe().f_code.co_name,
                            date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                            str(e.message))
                raise Exception, Exception(e_msg), sys.exc_info()[2]

        else:
            # Print skipping method
            helper_methods.print_skipping_method(
                # use case name
                who=self.config.test_name,
                # what the step was doing
                what='Backing BaseStation3200 up to USB',
                # why it was skipped
                why='USB backup was disabled in product_assessments.py')

    #################################
    def step_16(self):
        """
        Clear all programming from the 3200 so we can restore from a clean state.
        """
        # Only run step if User specified backup from USB
        if self.use_usb_for_backup:
            
            helper_methods.print_method_name()
            try:
                self.config.BaseStation3200[1].initialize_for_test()

                # This is needed to reset the substation clock to match the controller
                helper_methods.set_controller_substation_date_and_time(
                    controller=self.config.BaseStation3200[1],
                    substations=self.config.BaseStation3200[1].substations,
                    _date=date_mngr.controller_datetime.date_string_for_controller(),
                    _time=date_mngr.controller_datetime.time_string_for_controller())
            except Exception as e:
                e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                        "\tThe Controller Date and time was currently set to {2}\n" \
                        "\tThe Exception thrown was {3}".format(
                            self.config.test_name,
                            sys._getframe().f_code.co_name,
                            date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                            str(e.message))
                raise Exception, Exception(e_msg), sys.exc_info()[2]

        else:
            # Print skipping method
            helper_methods.print_skipping_method(
                # use case name
                who=self.config.test_name,
                # what the step was doing
                what='Clearing devices and programming from BaseStation3200 after backup',
                # why it was skipped
                why='USB backup was disabled in product_assessments.py')

    #################################
    def step_17(self):
        """
        1) Restore 3200 programming from USB.
        2) Run self tests on all devices to update two-wire values for verification.
        3) Verify both 3200 and substation configurations to validate successful backup/restore from USB.
        """
        # Only run step if User specified backup from USB
        if self.use_usb_for_backup:
            
            helper_methods.print_method_name()
            try:
                self.config.BaseStation3200[1].do_restore_programming(opcodes.usb_flash_storage, file_number=1)

                self.config.BaseStation3200[1].do_search_for_zones()

                for zone in self.config.BaseStation3200[1].zones.keys():
                    self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()

                for mv in self.config.BaseStation3200[1].master_valves.keys():
                    self.config.BaseStation3200[1].master_valves[mv].bicoder.self_test_and_update_object_attributes()

                for ms in self.config.BaseStation3200[1].moisture_sensors.keys():
                    self.config.BaseStation3200[1].moisture_sensors[ms].bicoder.self_test_and_update_object_attributes()

                for ts in self.config.BaseStation3200[1].temperature_sensors.keys():
                    self.config.BaseStation3200[1].temperature_sensors[ts].bicoder.self_test_and_update_object_attributes()

                for fm in self.config.BaseStation3200[1].flow_meters.keys():
                    self.config.BaseStation3200[1].flow_meters[fm].bicoder.self_test_and_update_object_attributes()

                for sw in self.config.BaseStation3200[1].event_switches.keys():
                    self.config.BaseStation3200[1].event_switches[sw].bicoder.self_test_and_update_object_attributes()

                # Increment both clocks for programming to hold
                helper_methods.increment_controller_substation_clocks(
                    controller=self.config.BaseStation3200[1],
                    substations=self.config.BaseStation3200[1].substations,
                    minutes=1)

                # Verify controller configuration
                self.config.BaseStation3200[1].verify_full_configuration()
                self.config.BaseStation3200[1].substations[1].verify_full_configuration()
            except Exception as e:
                e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                        "\tThe Controller Date and time was currently set to {2}\n" \
                        "\tThe Exception thrown was {3}".format(
                            self.config.test_name,
                            sys._getframe().f_code.co_name,
                            date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                            str(e.message))
                raise Exception, Exception(e_msg), sys.exc_info()[2]

        else:
            # Print skipping method
            helper_methods.print_skipping_method(
                # use case name
                who=self.config.test_name,
                # what the step was doing
                what='Restoring BaseStation3200 from USB backup and verifying configuration',
                # why it was skipped
                why='USB backup was disabled in product_assessments.py')
