import sys
from datetime import timedelta, datetime, date
from time import sleep

# import common.product as helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes, csv
from common import helper_methods

# import log_handler for logging functionality
from common.logging_handler import log_handler

__author__ = 'eldin'


class UseCase9Messages(object):
    """
    Test name: Substation Messages Use Case 9 \n
        -

    Multi-Controller Setup Guide:

    Purpose:
        - The purpose of this Use Case is to verify that every message on a substation is correct when we pass in the
          commands to set and get the messages. \n

    Coverage:
        - Set up a configuration where we have a biCoder on the substation for each message we want to verify.
        - Also set up one of each device type on the 3200 Controller to verify that we can set an arbitrary 3200 message
          on a device that "belongs to" a Controller. \n
        - Do a verify configuration just to make sure all the values still stayed the same.

        Step-by-Step Summary:
        1. Initialize the controller and any substations (in this case 1) to known states.
            - Turn on echo so the commands we send are echoed. \n
            - Turn on sim mode so that we can control the time as we see fit. \n
            - Stop the clock so that it only increments when we tell it to. \n
            - Set the date and time to 1/1/2014 1:00 A.M. so we know what our date time starts at. \n
            - Turn on faux I/O so we aren't using real devices. \n
            - Clear all devices and all programming so we are in known states on the controller and substations. \n
        2. Connect the controller to it's substations. We wait for the connection to be established and then we
           increment the clock to make sure it is also displayed on the controller and substations. \n
        3. Load devices to the controller and substation and set up addresses and default values. \n
            -We load devices using the serial numbers specified in the JSON. \n
            -Do a search on the controller after both all loads are done. \n
            -Increment the clock to make sure all devices have been found and are displayed. \n
            -Share substation devices to controller, setting their addresses and default values: \n

                BiCoders:           Controller Address:         Serial Numbers:
                - Valve (ZN's)      2, 3,                       "TSE0011", "TSE0012",
                                    197, 198, 199, 200          "TSQ0081", "TSQ0082", "TSQ0083", "TSQ0084"

                - Valve (MV's)      2, 3, 4,                    "TMV0002", "TMV0003", "TMV0004",
                                    5, 6, 7                     "TMV0005", "TMV0006", "TMV0007"

                - Flow              2, 3, 4                     "TWF0002", "TWF0003", "TWF0004"
                - Switch            2, 3, 4                     "TPD0002", "TPD0003", "TPD0004"
                - Temp              2, 3, 4                     "TAT0002", "TAT0003", "TAT0004"
                - Moisture          2, 3, 4                     "SB07258", "SB08258", "SB09258"
                - Pump              N/A

        4. Verify that our objects are set up correctly, verify they match the values on the controller/substation. \n
        5. Verify all messages on a valve biCoder (zone) are correct. \n
        6. Verify all messages on a valve biCoder (master valve) are correct. \n
        7. Verify all messages on a moisture biSensor are correct. \n
        8. Verify all messages on a temperature biSensor are correct. \n
        9. Verify all messages on a switch biCoder are correct. \n
        10. Verify all messages on a flow biCoder are correct. \n
        11. Verify the entire configuration on the controller as well as the substation again. \n

    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        -Only 1 Substation is attached to the controller

    Controller Programming:
        - Zones:                7   (1-local, 6-shared)
        - Master Valves:        7   (1-local, 6-shared)
        - Event Switches:       4   (1-local, 3-shared)
        - Temp Sensors:         4   (1-local, 3-shared)
        - Moisture Sensors:     4   (1-local, 3-shared)
        - Flow Meters:          4   (1-local, 3-shared)
        - Programs:             0
        - Zone Programs:        0
        - Mainlines:            0
        - Pocs:                 0

    Substation Programming:
        - Valve BiCoders:       12 (6 for zones, 6 for master valves)
        - Moisture BiCoders:    3
        - Temperature BiCoders: 3
        - Switch BiCoders:      3
        - Flow BiCoders:        3
        - Pump BiCoders:        0
    """

    #################################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )

        self.run_use_case()

    #################################
    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Connect controller to substations:
        - uses the Ip address in the user config files to set the ip address
            Note: the max wait time  is the number of minutes to wait for reconnection to succeed. the clocks are \n
                  incremented 1 minute at a time for the max minutes or until max number of minutes is met.
        - after connection is established increment clock for each controller 1 minute
            Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                  between controllers.

        """
        helper_methods.print_method_name()
        try:
            # Connect 3200 to substation 1
            self.config.BaseStation3200[1].add_substation_to_controller(
                _substation_address=1,
                _substation=self.config.SubStations[1])
        
            # Increment clocks after connection established
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Initialize all controllers and substations known state so that it doesnt have a configuration or any devices \n
        loaded.
        """
        helper_methods.print_method_name()
        try:
            # set computer time
            date_mngr.set_current_date_to_match_computer()

            # set both the 3200 and the substation to be the same time as your computer
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                _date=date_mngr.curr_day.date_string_for_controller(),
                _time=date_mngr.curr_day.time_string_for_controller())
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.

        Controller devices loaded:

            Device Type             Address         Serial Number
            -----------------------------------------------------
            Zones                   1               "TSD0001"
            -----------------------------------------------------
            Master Valves           1               "TMV0001"
            -----------------------------------------------------
            Flow Meters             1               "TWF0001"
            -----------------------------------------------------
            Moisture Sensors        1               "SB05308"
            -----------------------------------------------------
            Temperature Sensors     1               "TAT0001"
            -----------------------------------------------------
            Event Switches          1               "TSD0001"
            -----------------------------------------------------

        Substation BiCoders Shared:

            BiCoder Type            Address on 3200         Serial Number
            -------------------------------------------------------------
            Valve
            (Zones)                 2                       "TSE0011"
                                    3                       "TSE0012"
                                    197                     "TSQ0081"
                                    198                     "TSQ0082"
                                    199                     "TSQ0083"
                                    200                     "TSQ0084"

            (Master Valves)         2                       "TMV0002"
                                    3                       "TMV0003"
                                    4                       "TMV0004"
                                    5                       "TMV0005"
                                    6                       "TMV0006"
                                    7                       "TMV0007"

            -------------------------------------------------------------
            Flow                    2                       "TWF0002"
                                    3                       "TWF0003"
                                    4                       "TWF0004"
            -------------------------------------------------------------
            Moisture                2                       "SB07258"
                                    3                       "SB08258"
                                    4                       "SB09258"
            -------------------------------------------------------------
            Temperature             2                       "TAT0002"
                                    3                       "TAT0003"
                                    4                       "TAT0004"
            -------------------------------------------------------------
            Switch                  2                       "TPD0002"
                                    3                       "TPD0003"
                                    4                       "TPD0004"
            -------------------------------------------------------------

        """
        helper_methods.print_method_name()
        try:
            # Load devices from substation
            self.config.BaseStation3200[1].do_search_for_zones()

            # Increment clocks
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            # Substation: Share devices with controller.
            self.config.SubStations[1].set_bicoder_default_values()

            ############################################################################################################
            # Share devices from substation to controller. This makes the controller address the devices that it shares
            # with a substation, and then it sets the default values. We share these devices so that we can test and
            # verify that devices shared with a substation will retain their values after a reboot.

            # Zones
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0011"].add_to_zone(_address=2)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0012"].add_to_zone(_address=3)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSQ0081"].add_to_zone(_address=197)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSQ0082"].add_to_zone(_address=198)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSQ0083"].add_to_zone(_address=199)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSQ0084"].add_to_zone(_address=200)

            # Master Valves
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0002"].add_to_master_valve(_address=2)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0003"].add_to_master_valve(_address=3)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0004"].add_to_master_valve(_address=4)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0005"].add_to_master_valve(_address=5)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0006"].add_to_master_valve(_address=6)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0007"].add_to_master_valve(_address=7)

            # Moisture Sensors
            self.config.BaseStation3200[1].substations[1].moisture_bicoders["SB07258"].add_to_moisture_sensor(_address=2)
            self.config.BaseStation3200[1].substations[1].moisture_bicoders["SB08258"].add_to_moisture_sensor(_address=3)
            self.config.BaseStation3200[1].substations[1].moisture_bicoders["SB09258"].add_to_moisture_sensor(_address=4)

            # Temperature Sensors
            self.config.BaseStation3200[1].substations[1].temperature_bicoders["TAT0002"].add_to_temperature_sensor(_address=2)
            self.config.BaseStation3200[1].substations[1].temperature_bicoders["TAT0003"].add_to_temperature_sensor(_address=3)
            self.config.BaseStation3200[1].substations[1].temperature_bicoders["TAT0004"].add_to_temperature_sensor(_address=4)

            # Event Switches 
            self.config.BaseStation3200[1].substations[1].switch_bicoders["TPD0002"].add_to_event_switch(_address=2)
            self.config.BaseStation3200[1].substations[1].switch_bicoders["TPD0003"].add_to_event_switch(_address=3)
            self.config.BaseStation3200[1].substations[1].switch_bicoders["TPD0004"].add_to_event_switch(_address=4)

            # Flow Meters
            self.config.BaseStation3200[1].substations[1].flow_bicoders['TWF0002'].add_to_flow_meter(_address=2)
            self.config.BaseStation3200[1].substations[1].flow_bicoders['TWF0003'].add_to_flow_meter(_address=3)
            self.config.BaseStation3200[1].substations[1].flow_bicoders['TWF0004'].add_to_flow_meter(_address=4)
            ############################################################################################################
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        Increment the clock to save settings. \n
        Verify the entire configuration. \n
        """
        helper_methods.print_method_name()
        try:
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        Set and verify each zone message on the substation using different valve biCoders. \n
        Set and verify one arbitrary message on the controller on a zone that is 'owned' by the 3200. \n
            - We do this to make sure that our controller can properly have messages dealing with it's own devices.
        """
        helper_methods.print_method_name()
        try:
            # Verify that every message can be set and that it returns us the variables we are expecting (ID, DT, TX)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0011"].set_message(opcodes.checksum)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0011"].verify_message(opcodes.checksum)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0012"].set_message(opcodes.no_response)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSE0012"].verify_message(opcodes.no_response)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSQ0081"].set_message(opcodes.bad_serial)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSQ0081"].verify_message(opcodes.bad_serial)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSQ0082"].set_message(opcodes.low_voltage)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSQ0082"].verify_message(opcodes.low_voltage)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSQ0083"].set_message(opcodes.open_circuit)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSQ0083"].verify_message(opcodes.open_circuit)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSQ0084"].set_message('OC')
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TSQ0084"].verify_message('OC')

            # Verify one arbitrary message from the 3200
            self.config.BaseStation3200[1].zones[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].zones[1].messages.verify_bad_serial_number_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        Set and verify each master valve message on the substation using different valve biCoders. \n
        Set and verify one arbitrary message on the controller on a master valve that is 'owned' by the 3200. \n
            - We do this to make sure that our controller can properly have messages dealing with it's own devices.
        """
        helper_methods.print_method_name()
        try:
            # Verify that every message can be set and that it returns us the variables we are expecting (ID, DT, TX)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0002"].set_message(opcodes.checksum)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0002"].verify_message(opcodes.checksum)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0003"].set_message(opcodes.no_response)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0003"].verify_message(opcodes.no_response)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0004"].set_message(opcodes.bad_serial)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0004"].verify_message(opcodes.bad_serial)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0005"].set_message(opcodes.low_voltage)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0005"].verify_message(opcodes.low_voltage)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0006"].set_message(opcodes.open_circuit)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0006"].verify_message(opcodes.open_circuit)
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0007"].set_message('OC')
            self.config.BaseStation3200[1].substations[1].valve_bicoders["TMV0007"].verify_message('OC')

            # Verify one arbitrary message from the 3200
            self.config.BaseStation3200[1].master_valves[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].master_valves[1].messages.verify_bad_serial_number_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        Set and verify each moisture sensor message on the substation using different moisture biCoders. \n
        Set and verify one arbitrary message on the controller on a moisture sensor that is 'owned' by the 3200. \n
            - We do this to make sure that our controller can properly have messages dealing with it's own devices.
        """
        helper_methods.print_method_name()
        try:
            # Verify that every message can be set and that it returns us the variables we are expecting (ID, DT, TX)
            self.config.BaseStation3200[1].substations[1].moisture_bicoders["SB07258"].set_message(opcodes.checksum)
            self.config.BaseStation3200[1].substations[1].moisture_bicoders["SB07258"].verify_message(opcodes.checksum)
            self.config.BaseStation3200[1].substations[1].moisture_bicoders["SB08258"].set_message(opcodes.no_response)
            self.config.BaseStation3200[1].substations[1].moisture_bicoders["SB08258"].verify_message(opcodes.no_response)
            self.config.BaseStation3200[1].substations[1].moisture_bicoders["SB09258"].set_message(opcodes.bad_serial)
            self.config.BaseStation3200[1].substations[1].moisture_bicoders["SB09258"].verify_message(opcodes.bad_serial)

            # Verify one arbitrary message from the 3200
            self.config.BaseStation3200[1].moisture_sensors[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].moisture_sensors[1].messages.verify_bad_serial_number_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        Set and verify each temperature sensor message on the substation using different temperature biCoders. \n
        Set and verify one arbitrary message on the controller on a temperature sensor that is 'owned' by the 3200. \n
            - We do this to make sure that our controller can properly have messages dealing with it's own devices.
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].substations[1].temperature_bicoders["TAT0002"].set_message(opcodes.checksum)
            self.config.BaseStation3200[1].substations[1].temperature_bicoders["TAT0002"].verify_message(opcodes.checksum)
            self.config.BaseStation3200[1].substations[1].temperature_bicoders["TAT0003"].set_message(opcodes.no_response)
            self.config.BaseStation3200[1].substations[1].temperature_bicoders["TAT0003"].verify_message(opcodes.no_response)
            self.config.BaseStation3200[1].substations[1].temperature_bicoders["TAT0004"].set_message(opcodes.bad_serial)
            self.config.BaseStation3200[1].substations[1].temperature_bicoders["TAT0004"].verify_message(opcodes.bad_serial)

            # Verify one arbitrary message from the 3200
            self.config.BaseStation3200[1].temperature_sensors[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].temperature_sensors[1].messages.verify_bad_serial_number_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        Set and verify each event switch message on the substation using different switch biCoders. \n
        Set and verify one arbitrary message on the controller on a event switch that is 'owned' by the 3200. \n
            - We do this to make sure that our controller can properly have messages dealing with it's own devices.
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].substations[1].switch_bicoders["TPD0002"].set_message(opcodes.checksum)
            self.config.BaseStation3200[1].substations[1].switch_bicoders["TPD0002"].verify_message(opcodes.checksum)
            self.config.BaseStation3200[1].substations[1].switch_bicoders["TPD0003"].set_message(opcodes.no_response)
            self.config.BaseStation3200[1].substations[1].switch_bicoders["TPD0003"].verify_message(opcodes.no_response)
            self.config.BaseStation3200[1].substations[1].switch_bicoders["TPD0004"].set_message(opcodes.bad_serial)
            self.config.BaseStation3200[1].substations[1].switch_bicoders["TPD0004"].verify_message(opcodes.bad_serial)

            # Verify one arbitrary message from the 3200
            self.config.BaseStation3200[1].event_switches[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].event_switches[1].messages.verify_bad_serial_number_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Set and verify each flow meter message on the substation using different flow biCoders. \n
        Set and verify one arbitrary message on the controller on a flow meter that is 'owned' by the 3200. \n
            - We do this to make sure that our controller can properly have messages dealing with it's own devices.
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].substations[1].flow_bicoders["TWF0002"].set_message(opcodes.checksum)
            self.config.BaseStation3200[1].substations[1].flow_bicoders["TWF0002"].verify_message(opcodes.checksum)
            self.config.BaseStation3200[1].substations[1].flow_bicoders["TWF0003"].set_message(opcodes.no_response)
            self.config.BaseStation3200[1].substations[1].flow_bicoders["TWF0003"].verify_message(opcodes.no_response)
            self.config.BaseStation3200[1].substations[1].flow_bicoders["TWF0004"].set_message(opcodes.bad_serial)
            self.config.BaseStation3200[1].substations[1].flow_bicoders["TWF0004"].verify_message(opcodes.bad_serial)

            # Verify one arbitrary message from the 3200
            self.config.BaseStation3200[1].flow_meters[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].flow_meters[1].messages.verify_bad_serial_number_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Set messages for the substation. \n
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].substations[1].set_message(opcodes.two_wire_over_current)
            self.config.BaseStation3200[1].substations[1].verify_message(opcodes.two_wire_over_current)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        Increment the clock and then verify the full configuration again. \n
        Verify the entire configuration again. \n
        """
        helper_methods.print_method_name()
        try:
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[1].substations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
