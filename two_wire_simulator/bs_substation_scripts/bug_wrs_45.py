import sys
from time import sleep

# import common.product as helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes
from common import helper_methods

# import log_handler for logging functionality
from common.logging_handler import log_handler

__author__ = 'bens'


class UseCase12Wrs45(object):
    """
    Test name:
        - bug WRS_45

    Multi-Controller Setup Guide:
    
    Purpose:
        - The purpose of this test was to reproduce a bug in the SubStation \n
        - The Substation was sending split packet data that would be in the same TCP packet \n
        - The BL-3200 couldn't understand these packets
        - The Substation has a 1436 byte packet so we needed to send the correct data so that is would over flow the \n
        buffer. \n
        - all of this would cause the 3200 to lockup during the search
    
    Coverage:
        Step-by-Step Summary:
        1. Initialize the controller and any substations (in this case 1) to known states.
            - Turn on echo so the commands we send are echoed. \n
            - Turn on sim mode so that we can control the time as we see fit. \n
            - Stop the clock so that it only increments when we tell it to. \n
            - Set the date and time to 1/1/2014 1:00 A.M. so we know what our date time starts at. \n
            - Turn on faux I/O so we aren't using real devices. \n
            - Clear all devices and all programming so we are in known states on the controller and substations. \n
        2. Connect the controller to it's substations. We wait for the connection to be established and then we
           increment the clock to make sure it is also displayed on the controller and substations. \n
        3. Load devices to the substation \n
            - We load devices using strings in the use case not from the JSON. \n
            - We used the firmware version and the and the device type to make the packets large enough to overflow \n
            the buffer
            - Do a search on the controller after both all loads are done. \n
            - Verify the search return all serial numbers to the Substation and the 3200
            - Verify the devices can be addressed in the BL-3200


    """

    #################################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str

        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )

        self.run_use_case()

    #################################
    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break

                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_controller_test(config_object=self.config)
        
    #################################
    def step_1(self):
        """
        Sync the controller and substation(s) clock
        """
        method = "\n###################     Running " + sys._getframe().f_code.co_name + "    ######################\n"
        print method
        try:
            # set computer time
            date_mngr.set_current_date_to_match_computer()

            # set both the 3200 and the substation to be the same time as your computer
            helper_methods.set_controller_substation_date_and_time(
                controller=self.config.BaseStation3200[1],
                substations=self.config.SubStations,
                _date=date_mngr.curr_day.date_string_for_controller(),
                _time=date_mngr.curr_day.time_string_for_controller())

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        Connect controller to substation(s)
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].add_substation_to_controller(_substation_address=1,
                                                                        _substation=self.config.SubStations[1])
        
            # Increment clocks after connection established
            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Setting up devices on each controller:
            - Load devices on first controller that are part of its configuration
            - Load devices on second controller that are part of its configuration.
                Note: The same device cant be loaded on both controllers
            - increment clock for each controller 1 minute
                Note: all controllers clocks need to be incremented the same in order for packets to go back and forth \n
                between controllers.
            - Search, address and set default values for devices on the first controller.
                Note: bicoder specific attributes are only set on the controller that has them in its configuration
            - Search, address and set default values for devices on the Second controller.

        Controller devices loaded:

            Device Type             Serial Number
            -----------------------------------------------------
            Valve biCoder           "VA10001"
                                    "VE10001"
                                    "D222222"
                                    "VB10001"
                                    "VF10001"
                                    "D444444"
                                    "VC10001"
                                    "D333333"
                                    "D111111"


        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Load the devices only to the substation

            self.config.SubStations[1].add_twelve_valve_bicoder(_serial_number="VA10001",
                                                                _device_type=opcodes.zone,
                                                                _valve_bicoder_version=5.12)
            self.config.SubStations[1].add_dual_valve_bicoder(_serial_number="VE10001",
                                                              _device_type=opcodes.zone)
            self.config.SubStations[1].add_single_valve_bicoder(_serial_number="D222222",
                                                                _device_type=opcodes.zone,
                                                                _valve_bicoder_version=6.1)
            self.config.SubStations[1].add_twelve_valve_bicoder(_serial_number="VB10001",
                                                                _device_type=opcodes.zone,
                                                                _valve_bicoder_version=5.12)
            self.config.SubStations[1].add_dual_valve_bicoder(_serial_number="VF10001",
                                                              _device_type=opcodes.zone)
            self.config.SubStations[1].add_single_valve_bicoder(_serial_number="D444444",
                                                                _device_type=opcodes.zone,
                                                                _valve_bicoder_version=6.1)
            self.config.SubStations[1].add_twelve_valve_bicoder(_serial_number="VC10001",
                                                                _device_type=opcodes.zone,
                                                                _valve_bicoder_version=5.12)
            self.config.SubStations[1].add_single_valve_bicoder(_serial_number="D333333",
                                                                _device_type=opcodes.zone,
                                                                _valve_bicoder_version=6.1)
            self.config.SubStations[1].add_single_valve_bicoder(_serial_number="D111111",
                                                                _device_type=opcodes.zone,
                                                                _valve_bicoder_version=6.1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        - Do a search for the zones from the BL-3200
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:

            self.config.BaseStation3200[1].do_search_for_zones()

            helper_methods.increment_controller_substation_clocks(
                controller=self.config.BaseStation3200[1],
                substations=self.config.BaseStation3200[1].substations,
                minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        - Verify serial numbers are on the SubStation
            Device Type             Serial Number
            -----------------------------------------------------
            Valve biCoder           "VA10001"
                                    "VE10001"
                                    "D222222"
                                    "VB10001"
                                    "VF10001"
                                    "D444444"
                                    "VC10001"
                                    "D333333"
                                    "D111111"

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:

            self.config.SubStations[1].valve_bicoders["VA10001"].verify_serial_number()
            self.config.SubStations[1].valve_bicoders["VE10001"].verify_serial_number()
            self.config.SubStations[1].valve_bicoders["D222222"].verify_serial_number()
            self.config.SubStations[1].valve_bicoders["VB10001"].verify_serial_number()
            self.config.SubStations[1].valve_bicoders["VF10001"].verify_serial_number()
            self.config.SubStations[1].valve_bicoders["D444444"].verify_serial_number()
            self.config.SubStations[1].valve_bicoders["VC10001"].verify_serial_number()
            self.config.SubStations[1].valve_bicoders["D333333"].verify_serial_number()
            self.config.SubStations[1].valve_bicoders["D111111"].verify_serial_number()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]



    #################################
    def step_6(self):
        """
        - Do a search for the zones from the BL-3200
        - After the search is complete verify we can address a few zones on the BL-3200
            Device Type             Address         Serial Number
            -----------------------------------------------------
            Valve biCoder           1               "VA10001"
                                    2               "VE10001"
                                    49              "D222222"
                                    50              "VB10001"
                                    60              "VF10001"
                                    100             "D444444"
                                    150             "VC10001"
                                    175             "D333333"
                                    200             "D111111"
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:

            self.config.BaseStation3200[1].valve_bicoders["VA10001"].add_to_zone(_address=1)
            self.config.BaseStation3200[1].valve_bicoders["VE10001"].add_to_zone(_address=2)
            self.config.BaseStation3200[1].valve_bicoders["D222222"].add_to_zone(_address=49)
            self.config.BaseStation3200[1].valve_bicoders["VB10001"].add_to_zone(_address=50)
            self.config.BaseStation3200[1].valve_bicoders["VF10001"].add_to_zone(_address=60)
            self.config.BaseStation3200[1].valve_bicoders["D444444"].add_to_zone(_address=100)
            self.config.BaseStation3200[1].valve_bicoders["VC10001"].add_to_zone(_address=150)
            self.config.BaseStation3200[1].valve_bicoders["D333333"].add_to_zone(_address=175)
            self.config.BaseStation3200[1].valve_bicoders["D111111"].add_to_zone(_address=200)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        - Verify addresses on 3200
            Device Type             Address         Serial Number
            -----------------------------------------------------
            Valve biCoder           1               "VA10001"
                                    2               "VE10001"
                                    49              "D222222"
                                    50              "VB10001"
                                    60              "VF10001"
                                    100             "D444444"
                                    150             "VC10001"
                                    175             "D333333"
                                    200             "D111111"
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:

            self.config.BaseStation3200[1].zones[1].verify_device_address()
            self.config.BaseStation3200[1].zones[2].verify_device_address()
            self.config.BaseStation3200[1].zones[49].verify_device_address()
            self.config.BaseStation3200[1].zones[50].verify_device_address()
            self.config.BaseStation3200[1].zones[60].verify_device_address()
            self.config.BaseStation3200[1].zones[100].verify_device_address()
            self.config.BaseStation3200[1].zones[150].verify_device_address()
            self.config.BaseStation3200[1].zones[175].verify_device_address()
            self.config.BaseStation3200[1].zones[200].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]