import sys
from time import sleep

# import common.product as helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes
from common import helper_methods

# import log_handler for logging functionality
from common.logging_handler import log_handler

__author__ = 'bens'


class UseCase4BasicProgramming_BugReproduce(object):
    """
    Purpose of this test is to help reproduce JIRA Bug ZZ-1674

    Controller Programming:
        - Flow Meter:     2   (1-local, 1-shared)

    Substation Programming:
        - Flow BiCoders:    1
    """

    #################################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str

        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/sb_32_json_config_files'
                                    )

        self.config.initialize_for_test(connect_to_basemanager=False)
        
        # --------------------------
        # Connect 3200 to Substation
        # --------------------------
        
        self.config.BaseStation3200[1].add_substation_to_controller(_substation_address=1, _substation=self.config.SubStations[1])
        
        helper_methods.increment_controller_substation_clocks(
            controller=self.config.BaseStation3200[1],
            substations=self.config.BaseStation3200[1].substations,
            minutes=1)
        
        # ------------------------------------------
        # Search two-wire to find Substation devices
        # ------------------------------------------
        
        self.config.BaseStation3200[1].do_search_for_flow_meters()
        
        helper_methods.increment_controller_substation_clocks(
            controller=self.config.BaseStation3200[1],
            substations=self.config.BaseStation3200[1].substations,
            minutes=1)
        
        # -------------------------------------
        # Set Substation BiCoder default values
        # -------------------------------------

        self.config.BaseStation3200[1].substations[1].set_bicoder_default_values()
        
        # --------------------------------------------------------------------------
        # Create a FlowMeter object on 3200 for "found" Flow BiCoder from the Search
        # --------------------------------------------------------------------------
        
        self.config.BaseStation3200[1].substations[1].flow_bicoders["FM00005"].add_to_flow_meter(
            _address=1
        )

        # -------------------------------------------
        # Verify setting flow rate on local FlowMeter
        # -------------------------------------------
        
        self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=30)
        self.config.BaseStation3200[1].flow_meters[2].bicoder.do_self_test()
        self.config.BaseStation3200[1].flow_meters[2].bicoder.verify_flow_rate()
        
        # ---------------------------------------------------
        # Verify setting flow rate on Substation Flow BiCoder
        # ---------------------------------------------------

        # Set flow rate through substation serial port
        self.config.BaseStation3200[1].flow_bicoders["FM00005"].set_flow_rate(_gallons_per_minute=50)

        # change flow bicoder serial port to 3200
        self.config.BaseStation3200[1].flow_bicoders["FM00005"].ser = self.config.BaseStation3200[1].ser

        # change flow bicoder "controller" to 3200 so that the self-test and update object attributes treat results
        # as coming from 3200
        self.config.BaseStation3200[1].flow_bicoders["FM00005"].controller = self.config.BaseStation3200[1]

        # Do self test and update attributes
        self.config.BaseStation3200[1].flow_bicoders["FM00005"].do_self_test()

        # Should verify flow rate of 50 GPM
        self.config.BaseStation3200[1].flow_bicoders["FM00005"].verify_flow_rate()
