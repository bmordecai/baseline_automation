import sys
import os

import common.user_configuration as user_conf_module
#
# # Substation use cases
import bs_substation_scripts.uc_1_timed_zones as cuc_substation_1_module
import bs_substation_scripts.uc_2_soak_cycles as cuc_substation_2_module
import bs_substation_scripts.uc_3_disconnect_reconnect as cuc_substation_3_module
import bs_substation_scripts.uc_4_basic_programming as cuc_substation_4_module
import bs_substation_scripts.uc_5_replace_devices as cuc_substation_5_module
import bs_substation_scripts.uc_6_concurrent_zones as cuc_substation_6_module
import bs_substation_scripts.uc_7_move_devices as cuc_substation_7_module
import bs_substation_scripts.uc_8_learn_flow as cuc_substation_8_module
import bs_substation_scripts.uc_9_messages as cuc_substation_9_module
import bs_substation_scripts.uc_10_backup_restore as cuc_substation_10_module
import bs_substation_scripts.uc_11_device_bug_test as cuc_substation_11_module
import bs_substation_scripts.bug_wrs_45 as cuc_substation_12_module

_author__ = 'Tige'


def run_use_cases_3200_with_substations(user_configuration_file_name, passing_tests, failing_tests,
                                        manual_tests, specific_tests, auto_update_fw=False):
    """
    This is where we will run all of our SubStation + 3200 use cases. \n

    :param user_configuration_file_name:    The name of the configuration file we will be using for the tests. \n
    :type user_configuration_file_name:     str
    
    :param auto_update_fw:    If you want to automatically update firmware versions of controllers to "latest" version.
    :type auto_update_fw:     bool

    :param passing_tests:   If you want to run passing tests. True for yes, False for no. \n
    :type passing_tests:    bool

    :param failing_tests:   If you want to run failing tests. True for yes, False for no. \n
    :type failing_tests:    bool

    :param manual_tests:    If you want to run manual tests. True for yes, False for no. \n
    :type manual_tests:     bool

    :param specific_tests:  Pick which tests you want to run individually. Will run all if the list is empty. \n
    :type specific_tests:   list[int | float]
    """

    # Load in user configured items from text file (necessary for serial connection)
    user_conf = user_conf_module.UserConfiguration(os.path.join('common/user_credentials', user_configuration_file_name))

    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #                                             SUBSTATION + 3200 TESTS                                              #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Passing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if passing_tests:
        if 1 in specific_tests or len(specific_tests) == 0:
            cuc_substation_1_module.UseCase1TimedZones(
                test_name="SB UC 1 - Timed Zones",
                user_configuration_instance=user_conf,
                json_configuration_file='SB_timed_zones.json')
        if 2 in specific_tests or len(specific_tests) == 0:
            cuc_substation_2_module.UseCase2SoakCycles(
                test_name="SB UC 2 - Soak Cycles",
                user_configuration_instance=user_conf,
                json_configuration_file='SB_soak_cycles.json')
        if 3 in specific_tests or len(specific_tests) == 0:
            cuc_substation_3_module.UseCase3DisconnectReconnect(
                test_name="SB UC 3 - Disconnect/Reconnect",
                user_configuration_instance=user_conf,
                json_configuration_file='SB_timed_zones.json')
        if 4 in specific_tests or len(specific_tests) == 0:
            cuc_substation_4_module.UseCase4BasicProgramming(
                test_name="SB UC 4-Basic Programming",
                user_configuration_instance=user_conf,
                json_configuration_file="SB_basic_programming.json")
        if 5 in specific_tests or len(specific_tests) == 0:
            cuc_substation_5_module.UseCase5ReplaceDevices(
                test_name="SB UC 5 - Replace Devices",
                user_configuration_instance=user_conf,
                json_configuration_file="SB_replace_devices.json")
        if 6 in specific_tests or len(specific_tests) == 0:
            cuc_substation_6_module.UseCase6ConcurrentZones(
                test_name="SB UC 6 - Concurrent Zones",
                user_configuration_instance=user_conf,
                json_configuration_file="SB_concurrent_zones.json")
        if 7 in specific_tests or len(specific_tests) == 0:
            cuc_substation_7_module.UseCase7MoveDevices(
                test_name="SB UC 7 - Move Devices",
                user_configuration_instance=user_conf,
                json_configuration_file="SB_move_devices.json")
        if 8 in specific_tests or len(specific_tests) == 0:
            cuc_substation_8_module.UseCase8LearnFlow(
                test_name="SB UC 8 - Learn Flow",
                user_configuration_instance=user_conf,
                json_configuration_file="SB_learn_flow.json")
        if 9 in specific_tests or len(specific_tests) == 0:
            cuc_substation_9_module.UseCase9Messages(
                test_name="SB UC 9 - Messages",
                user_configuration_instance=user_conf,
                json_configuration_file="SB_messages.json")
        if 11 in specific_tests or len(specific_tests) == 0:
            cuc_substation_11_module.SubStationUseCase11(
                test_name="SB UC 11 - Device Bug",
                user_configuration_instance=user_conf,
                json_configuration_file=None)
        if 12 in specific_tests or len(specific_tests) == 0:
            cuc_substation_12_module.UseCase12Wrs45(
                test_name="SB UC 12 - bug_wrs_45",
                user_configuration_instance=user_conf,
                json_configuration_file="bug_wrs_45.json")

        print("########################### ----FINISHED RUNNING PASSING SUBSTATION TESTS---- #########################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Manual Tests------############################################## #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #      (Requires Manual Device/Use-Case Modification)                                                              #
    #                                                                                                                  #
    #    - THESE USE CASES ARE NOT APART OF NIGHTLY BUILD                                                              #
    #    - These use cases require setting breakpoints at specific locations in the test in order to configure         #
    #      a real-device to be in an expected state for the test.                                                      #
    #    - Above each use case is a list of break points to set inside the use case. The breakpoints should point      #
    #      to a "To Do" item which explains what the user needs to do.                                                 #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    # TODO: THIS USE CASE 10 REQUIRES A BASEMANAGER CONNECTION & USB PLUGGED INTO 3200.
    # TODO: -> TO NOT TEST USING BASEMANAGER, SET `use_basemanager_for_backup=False`
    # TODO: -> TO NOT TEST USING USB, SET `use_usb_for_backup=False`
    if manual_tests:
        if 10 in specific_tests or len(specific_tests) == 0:
            cuc_substation_10_module.UseCase10BackupRestore(
                test_name="SB UC 10 - Backup Restore",
                user_configuration_instance=user_conf,
                json_configuration_file="SB_backup_restore.json",
                use_basemanager_for_backup=True,
                use_usb_for_backup=True)

        print("########################### ----FINISHED RUNNING MANUAL SUBSTATION TESTS---- ##########################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Failing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if failing_tests:

        print("########################### ----FINISHED RUNNING FAILING SUBSTATION TESTS---- #########################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # #######################################------Future Needed Tests------########################################## #
    # ---------------------------------------------------------------------------------------------------------------- #

    print("######################### ----YOU ARE A WINNER WINNER CHICKEN DINNER---- ##################################")


if __name__ == "__main__":
    run_use_cases_3200_with_substations(user_configuration_file_name="user_credentials_tige_32_SB.json",
                                        passing_tests=True,
                                        failing_tests=False,
                                        manual_tests=False,
                                        specific_tests=[])
    exit()
