import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common import helper_methods
from common.imports import opcodes

__author__ = 'Tige'


class ControllerUseCase4(object):
    """
    Test Name:
        - Nelson Feature Program Cycles
    purpose:
        - set up the nelson feature
            - verify that program cycles work

    Coverage area: \n
        - Programs: \n
            - program continues cycles \n
                - if water window closes program pause and than continues \n
                - if day interval doesnt allow continues cycles \n
                - stop program using an event switch
    Things that are not covered: \n
        - Programs: \n
            - program continues cycles \n
                - if day interval doesnt allow continues cycles \n
    Date References:
        - configuration for script is located common\configuration_files\nelson_features.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        Setup Points of Connection
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Create POC 1
            self.config.BaseStation1000[1].add_poc(_poc_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation1000[1].points_of_control[1].set_target_flow(_gpm=40)
            self.config.BaseStation1000[1].points_of_control[1].set_high_flow_limit(_limit=50,
                                                                                    with_shutdown_enabled=False)
            self.config.BaseStation1000[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=False)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        Setup Programs
        ############################
        """
        program_8_am_start_times = [480]  # 8:00 am start time
        program_waters_every_day = [1, 1, 1, 1, 1, 1, 1]  # runs everyday
        program_fully_open_water_windows = ['111111111111111111111111']
        program_water_windows_closed_11am_to_12_pm = ['111111111101111111111111']

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # create program 1
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_description(_ds='Continuous Program Cycles')
            self.config.BaseStation1000[1].programs[1].set_water_window(_ww=program_fully_open_water_windows)
            self.config.BaseStation1000[1].programs[1].add_point_of_connection(_poc_address=1)
            self.config.BaseStation1000[1].programs[1].set_soak_cycle_mode_disabled()

            # Create your start/stop condition
            self.config.BaseStation1000[1].programs[1].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[1].date_time_start_conditions[1].set_day_time_start(
                _dt=opcodes.true,
                _st_list=program_8_am_start_times,
                _interval_type=opcodes.week_days,
                _interval_args=program_waters_every_day)
            self.config.BaseStation1000[1].programs[1].add_switch_stop_condition(_event_switch_address=1)
            self.config.BaseStation1000[1].programs[1].event_switch_stop_conditions[1].set_switch_mode_to_open()
            self.config.BaseStation1000[1].programs[1].event_switch_stop_conditions[1].set_stop_immediately_true()

            # create program 5
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=5)
            self.config.BaseStation1000[1].programs[5].set_description(_ds='Master Valves Assigned to Zones')
            self.config.BaseStation1000[1].programs[5].set_water_window(_ww=program_water_windows_closed_11am_to_12_pm)
            self.config.BaseStation1000[1].programs[5].add_point_of_connection(_poc_address=1)
            self.config.BaseStation1000[1].programs[5].set_soak_cycle_mode_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=10)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[1].zone_programs[2].set_run_time(_minutes=10)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation1000[1].programs[1].zone_programs[3].set_run_time(_minutes=10)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation1000[1].programs[1].zone_programs[4].set_run_time(_minutes=10)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation1000[1].programs[1].zone_programs[5].set_run_time(_minutes=10)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
         Verify all attribute values for all objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """
        Testing Continuous cycles for a program with a fully open water window
        - set program 1 cycles to continuous
        - set max concurrent zones on the program to 5
        - set max concurrent zones on the controller to 5
        - this will have all the zones run at one time
        - set clock on controller to 7:59am
        - increment clock to 8:00
        - program 1 should start
        - increment time for 48 hours in 9 minute interval's program should continuously run 9 minutes is off all runtime boundaries so I can verify still running
        - also this will verify that we can cross midnight and still keep running
        - verify every 9 minutes that all zones are running
        - after 48 hours stop program
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # date_mngr.set_dates_from_date_range(start_date='12-01-2015', end_date='12-28-2015')
            date_mngr.set_current_date_to_match_computer()
            # set program to be continuous program cycles 16 = continous
            self.config.BaseStation1000[1].programs[1].set_program_cycles(_number_of_cycles=26)

            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=5)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=5)

            self.config.BaseStation1000[1].set_date_and_time(_date='12/12/2018',
                                                             _time='07:59:00')
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            date_mngr.end_date = date_mngr.curr_day.obj + timedelta(days=2)
            # loops through the program zones looks at the program and finds the zones in that program
            while not date_mngr.reached_last_date():
                for zone_program in self.config.BaseStation1000[1].programs[1].zone_programs.values():
                    if zone_program.program.ad == 1:
                        self.config.BaseStation1000[1].zones[zone_program.zone.ad].get_data()
                        self.config.BaseStation1000[1].zones[zone_program.zone.ad].statuses.verify_status_is_watering()
                self.config.BaseStation1000[1].do_increment_clock(minutes=9)

            self.config.BaseStation1000[1].programs[1].set_program_to_stop()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_6(self):
        """
        Testing Continuous cycles for a program with a water window that opens and closes
        - disable Programs 5,10,40 so that they don't effect the test
        - set program 1 cycles to continuous
        - set max concurrent zones on the program to 5
        - set max concurrent zones on the controller to 5
        - this will have all the zones run at one time
        - set clock on controller to 7:59am
        - increment clock to 8:00
        - program 1 should start
        using date manager set end date of test 2 days later
        - increment time for 2 days  in 9 minute interval's program should continuously run 9 minutes is off all
        runtime boundaries so I can verify still running
        - also this will verify that we can cross midnight and still keep running
        - verify every 9 minutes that all zones are running
        - verify that when water window close the zone pause and when it open back up they start watering again
        - after 48 hours stop program with event switch
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        program_water_windows_closed_11am_to_12_pm = ['111111111110111111111111']
        try:
            self.config.BaseStation1000[1].programs[1].set_water_window(_ww=program_water_windows_closed_11am_to_12_pm)
            water_window_closed = datetime.strptime("11:00:00", '%H:%M:%S')
            water_window_open = datetime.strptime("11:59:00", '%H:%M:%S')

            # set program to be continuous program cycles 16 = continous
            self.config.BaseStation1000[1].programs[1].set_program_cycles(_number_of_cycles=26)

            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=5)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=5)
            self.config.BaseStation1000[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='07:59:00')
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            date_mngr.end_date = date_mngr.curr_day.obj + timedelta(days=2)
            # loops through the program zones looks at the program and finds the zones in that program
            while not date_mngr.reached_last_date():
                for zone_program in self.config.BaseStation1000[1].programs[1].zone_programs.values():
                    if zone_program.program.ad == 1:
                        self.config.BaseStation1000[1].zones[zone_program.zone.ad].get_data()
                        if water_window_closed.time() <= date_mngr.curr_day.time_obj.obj <= water_window_open.time():
                            self.config.BaseStation1000[1].zones[zone_program.zone.ad].statuses.verify_status_is_paused()
                        else:
                            self.config.BaseStation1000[1].zones[zone_program.zone.ad].statuses.verify_status_is_watering()
                self.config.BaseStation1000[1].do_increment_clock(minutes=9)

            self.config.BaseStation1000[1].event_switches[1].bicoder.set_contact_open()
            # stop program verify all zones are set to done
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            for zone_program in self.config.BaseStation1000[1].programs[1].zone_programs.values():
                if zone_program.program.ad == 1:
                    self.config.BaseStation1000[1].zones[zone_program.zone.ad].get_data()
                    self.config.BaseStation1000[1].zones[zone_program.zone.ad].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
