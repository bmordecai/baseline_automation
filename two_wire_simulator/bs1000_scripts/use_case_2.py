import csv
import os
import logging
import sys
from csv_handler import CSVWriter
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods
from common.imports import opcodes, csv

# this import allows us to keep track of time
from time import sleep
from datetime import time, timedelta, datetime, date

__author__ = 'Tige'


class ControllerUseCase2(object):
    """
    Test Name:
        - EPA Test Configuration
    purpose:
        - set up a the EPA test
            - setup 6 different zones using Weather based watering
            - verify that teh ETc runtime values change when the eto input value changes
            - verify that correct cycle and soak are performed
            - verify that controller starts watering when deficit is below 50%
            - verify that the controller meets a 80% efficacy rating

    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
        - setting programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign zones to programs \n
            - set each zone to run with ETc run times \n
            - set each zone to have a cycle and soak based on zone properties\n
    Date References:
        - configuration for script is located common\configuration_files\update_cn.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase2' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        # initializes the csv writer object so it can be used in the remainder of the use_case
        controller_type = opcodes.basestation_1000
        self.csv_writer = CSVWriter(file_name=test_name + controller_type,
                                    relative_path='bs1000_scripts',
                                    delimiter=',',
                                    line_terminator='\n')

        # if the file isn't already in use, the writer is opened. Otherwise an exception is thrown
        try:
            self.csv_writer.open()
        except IOError:
            e_msg = self.csv_writer.fname + ' file is in use'
            raise IOError(e_msg)

        # self.hydrozone_information = csv.EPA.hydro_zone_headers
        self.logger = logging.getLogger()
        self.hdlr = logging.FileHandler(str(os.path.dirname(os.path.abspath(__file__))) + '/' + test_name +
                                        controller_type + ".txt", mode="w")
        self.formatter = logging.Formatter('%(levelname)s: %(message)s')
        self.hdlr.setFormatter(self.formatter)
        self.logger.addHandler(self.hdlr)
        self.logger.setLevel(logging.INFO)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        - Add a POC
        - Load ETO and rain fall values
        - Log the successful completion of controller initiation
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        self.config.BaseStation1000[1].add_poc(_poc_address=1)
        self.config.load_eto_and_rain_fall_values()
        self.logger.info("Successfully initiated controller")
        self.logger.info("Step 1 complete!")

    def step_2(self):
        """
        - Set all attribute for a zone the a client could specify
        - Set the new descriptions to match EPA test for each zone
            - set the zone attribute for soil type for each zone
            - set the zone attribute for slope for each zone
            - set the zone attribute for sun exposure for each zone
            - set the zone attribute for allowable depletion for each zone
            - set the zone attribute for root depth for each zone
            - set the zone precipitation rates for each zone for each zone
            - set the zone distribution uniformity for each zone for each zone
            - set the zone attribute for Gross are a for each zone
        - set crop coefficient for each zone
            - Set zone 1 is attribute for sun exposure shade tall Fescue
            - Set zone 2 is attribute for sun exposure Bermuda
            - Set zone 6 is attribute for sun exposure Bermuda
        - set the root water holding capacity for each zone
            - this uses an equation to get the root zone water holding capacity and then sends the value \n
              to the controller \n
        """
        #######################################
        # user input fields simulating coming from the client
        #######################################
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        # set descriptions to match EPA test for each zone
        self.config.BaseStation1000[1].zones[1].set_description(_ds='Loam Soil')
        self.config.BaseStation1000[1].zones[2].set_description(_ds='Silty Clay Soil')
        self.config.BaseStation1000[1].zones[3].set_description(_ds='Loamy Sand Soil')
        self.config.BaseStation1000[1].zones[4].set_description(_ds='Sandy Loam Soil')
        self.config.BaseStation1000[1].zones[5].set_description(_ds='Clay Loam soil')
        self.config.BaseStation1000[1].zones[6].set_description(_ds='Clay soil')

        # set the zone attribute for soil type
        self.config.BaseStation1000[1].zones[1].soil_type = "Loam"
        self.config.BaseStation1000[1].zones[2].soil_type = "Silty Clay"
        self.config.BaseStation1000[1].zones[3].soil_type = "Loamy Sand"
        self.config.BaseStation1000[1].zones[4].soil_type = "Sandy Loam"
        self.config.BaseStation1000[1].zones[5].soil_type = "Clay Loam"
        self.config.BaseStation1000[1].zones[6].soil_type = "Clay"

        # set the zone attribute for slope
        self.config.BaseStation1000[1].zones[1].slope = .06
        self.config.BaseStation1000[1].zones[2].slope = .10
        self.config.BaseStation1000[1].zones[3].slope = .08
        self.config.BaseStation1000[1].zones[4].slope = .12
        self.config.BaseStation1000[1].zones[5].slope = .02
        self.config.BaseStation1000[1].zones[6].slope = .20

        # set the zone attribute for sun exposure
        self.config.BaseStation1000[1].zones[1].sun_exposure = .75
        self.config.BaseStation1000[1].zones[2].sun_exposure = 1.00
        self.config.BaseStation1000[1].zones[3].sun_exposure = 1.00
        self.config.BaseStation1000[1].zones[4].sun_exposure = .50
        self.config.BaseStation1000[1].zones[5].sun_exposure = 1.00
        self.config.BaseStation1000[1].zones[6].sun_exposure = 1.00

        # set the zone attribute for allowable depletion
        self.config.BaseStation1000[1].zones[1].allowable_depletion = .50
        self.config.BaseStation1000[1].zones[2].allowable_depletion = .40
        self.config.BaseStation1000[1].zones[3].allowable_depletion = .50
        self.config.BaseStation1000[1].zones[4].allowable_depletion = .55
        self.config.BaseStation1000[1].zones[5].allowable_depletion = .50
        self.config.BaseStation1000[1].zones[6].allowable_depletion = .35

        # set the zone attribute for root depth
        self.config.BaseStation1000[1].zones[1].root_depth = 10.0
        self.config.BaseStation1000[1].zones[2].root_depth = 8.1
        self.config.BaseStation1000[1].zones[3].root_depth = 20.0
        self.config.BaseStation1000[1].zones[4].root_depth = 28.0
        self.config.BaseStation1000[1].zones[5].root_depth = 25.0
        self.config.BaseStation1000[1].zones[6].root_depth = 9.2

        # set zone precipitation rates for each zone
        self.config.BaseStation1000[1].zones[1].set_precipitation_rate_value(_pr_value=1.60)
        self.config.BaseStation1000[1].zones[2].set_precipitation_rate_value(_pr_value=1.60)
        self.config.BaseStation1000[1].zones[3].set_precipitation_rate_value(_pr_value=1.40)
        self.config.BaseStation1000[1].zones[4].set_precipitation_rate_value(_pr_value=1.40)
        self.config.BaseStation1000[1].zones[5].set_precipitation_rate_value(_pr_value=0.20)
        self.config.BaseStation1000[1].zones[6].set_precipitation_rate_value(_pr_value=0.35)

        # set the zone attribute for distribution uniformity
        self.config.BaseStation1000[1].zones[1].set_distribution_uniformity_value(_du_value=55)
        self.config.BaseStation1000[1].zones[2].set_distribution_uniformity_value(_du_value=60)
        self.config.BaseStation1000[1].zones[3].set_distribution_uniformity_value(_du_value=70)
        self.config.BaseStation1000[1].zones[4].set_distribution_uniformity_value(_du_value=75)
        self.config.BaseStation1000[1].zones[5].set_distribution_uniformity_value(_du_value=80)
        self.config.BaseStation1000[1].zones[6].set_distribution_uniformity_value(_du_value=65)

        # set the zone attribute for Gross area
        self.config.BaseStation1000[1].zones[1].gross_area = 1000
        self.config.BaseStation1000[1].zones[2].gross_area = 1200
        self.config.BaseStation1000[1].zones[3].gross_area = 800
        self.config.BaseStation1000[1].zones[4].gross_area = 500
        self.config.BaseStation1000[1].zones[5].gross_area = 650
        self.config.BaseStation1000[1].zones[6].gross_area = 1600

        # this has to come from some table associated back to a date
        self.config.BaseStation1000[1].zones[1].set_crop_coefficient_value(_kc_value=0.50)

        # Set zone 2 is full sun Bermuda
        self.config.BaseStation1000[1].zones[2].set_crop_coefficient_value(_kc_value=0.60)
        self.config.BaseStation1000[1].zones[3].set_crop_coefficient_value(_ks_value=0.50, _kd_value=1.00, _kmc_value=1.10)
        self.config.BaseStation1000[1].zones[4].set_crop_coefficient_value(_ks_value=0.50, _kd_value=1.00, _kmc_value=.80)
        self.config.BaseStation1000[1].zones[5].set_crop_coefficient_value(_ks_value=0.50, _kd_value=1.10, _kmc_value=1.10)
        # Set zone 6 is full sun Bermuda
        self.config.BaseStation1000[1].zones[6].set_crop_coefficient_value(_kc_value=0.73)

        # set root zone working water holding storage this uses an equation to get the root zone water holding capacity
        # and then sends the value to the controller
        for zone in self.config.zn_ad_range:
            self.config.BaseStation1000[1].zones[zone].set_root_zone_working_storage_capacity()

        self.logger.info("Set all of the zone's attributes.")
        self.logger.info(sys._getframe().f_code.co_name + " complete!")

    def step_3(self):
        """
        ############################
        Setup Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        program_number_1_start_times = 120  # 2:00 am start time
        program_number_1_watering_days = 1111111  # runs everyday
        program_number_1_water_windows = ['111111111111111111111111']

        # self.config.BaseStation1000[1].programs[1] = PG1000(_address=1,
        #                                  _description='EPA Test Program',
        #                                  _enabled_state=opcodes.true,
        #                                  _water_window=program_number_1_water_windows,
        #                                  _max_concurrent_zones=1,
        #                                  _point_of_connection_address=[1],
        #                                  _soak_cycle_mode=opcodes.zone_soak_cycles,
        #                                  _ee=opcodes.true)
        self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
        self.config.BaseStation1000[1].programs[1].set_description(_ds="EPA Test Program")
        self.config.BaseStation1000[1].programs[1].set_enabled()
        self.config.BaseStation1000[1].programs[1].set_water_window(_ww=program_number_1_water_windows)
        self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(1)
        self.config.BaseStation1000[1].programs[1].add_point_of_connection(_poc_address=1)
        self.config.BaseStation1000[1].programs[1].set_soak_cycle_mode(_mode=opcodes.zone_soak_cycles)
        self.config.BaseStation1000[1].programs[1].set_enable_et_state(_state=opcodes.true)
        self.config.BaseStation1000[1].ser.send_and_wait_for_reply('SET,'
                                                                   'PT=1,'
                                                                   'TY=DT,'
                                                                   'DT=TR,'
                                                                   'WD='+str(program_number_1_watering_days)+',' +
                                                                   'ST='+str(program_number_1_start_times)
                                                                   )

        self.logger.info("Program has been setup.")
        self.logger.info(sys._getframe().f_code.co_name + " complete!")

    def step_4(self):
        """
        - Set a cycle time for each zone set the new cycle time values
            - this uses an equation to get the cycle time and then sends the value \n
              to the controller \n
        - Set a soak time for each zone set the new cycle time values
            - this uses an equation to get the soak time and then sends the value \n
              to the controller \n
        :return:
        :rtype:
        """
        # Set the new cycle time values
        # Need this to come for the answer to the wrapper

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method


        for zone in self.config.zn_ad_range:
            self.config.BaseStation1000[1].zones[zone].set_cycle_time(_use_calculated_cycle_time=True)

        # Set the new soak time values
        # Need this to come for the answer to the wrapper
        for zone in self.config.zn_ad_range:
            self.config.BaseStation1000[1].zones[zone].set_soak_time(_use_calculated_soak_time=True)

        self.csv_writer.write_dict_of_obj(dict_of_objs=self.config.BaseStation1000[1].zones,
                                          template=csv.EPA.hydro_zone_headers_zn_var_names)

        self.csv_writer.write_dict_of_obj(dict_of_objs=self.config.BaseStation1000[1].programs[1].zone_programs,
                                          template=csv.EPA.hydro_zone_headers_zp_var_names)

        self.logger.info(sys._getframe().f_code.co_name + " complete!")

    def step_5(self):
        """
        enable the ET runtime for each zone by assigning it to a program and then enabling it
        :return:
        :rtype:
        """
        # enable each zone to run  on ET run times
        # set _rt = 1 so that there was just a value
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        for zone in self.config.zn_ad_range:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=zone)
            self.config.BaseStation1000[1].programs[1].zone_programs[zone].set_run_time(_minutes=1)
            # self.config.BaseStation1000[1].programs[1].zone_programs[zone] = ZoneProgram(zone_obj=self.config.BaseStation1000[1].zones[zone],
            #                                               prog_obj=self.config.BaseStation1000[1].programs[1], _rt=60)

        self.logger.info(sys._getframe().f_code.co_name + " complete!")

    def step_6(self):
        """
         Verify all attribute values for all objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        self.config.verify_full_configuration()

        self.logger.info("Successfully verified the values for all objects in the configuration.")
        self.logger.info(sys._getframe().f_code.co_name + " complete!")

    def step_7(self):

        """
        input 30 days of eto
        input 30 days of rain fall

        Run controller for x number of days adjusting the Eto and rain fall value for each day


        Build a list of dates by passing in a date range
        build a list of ETO and rain fall data points by accessing the list of eto and rain fall values
        using date manager select the first day
        for each date assign an eto and rain fall value
        start by setting controller time to 11:55pm of the first date of the date range
        increment clock to 12:03  this will allow the controller to do all necessary operations
        Set clock to 12:45 am
        Send Eto and Rain fail data to the controller
        increment clock to 1:01 am this will allow controller to make all necessary calculations
        Compare the calculated moisture balance from the test to the moisture balance in the controller
        Compare the calculated Run time from the test to the Run time in the controller
        Compare the calculated Soak and Cycle Time balance from the test to the Soak and Cycle Time in the controller
        run program until all zones are done
        Compare minute my minute what is happening

        :return:
        :rtype:
        """
        # while date_mngr.reached_last_date() == False:
        #   1. Start by setting controller time to 11:55pm of the first date of the date range
        #   2. While there are days left to complete: (for each date)
        #   3. .... Assign an eto and rain fall value
        #   4. .... Increment clock to 12:03am (allow controller to do calculations)
        #   5. .... Set clock to 12:45am
        #   6. .... Send Eto and Rain fail data to the controller
        #   7. .... Increment clock to 1:01 am this will allow controller to make all necessary calculations
        #   8. .... Compare the calculated moisture balance from the test to the moisture balance in the controller
        #   9. .... Compare the calculated Run time from the test to the Run time in the controller
        #   10..... Compare the calculated Soak and Cycle Time balance from the test to the Soak and Cycle Time in
        #           the controller
        #   11..... Run program until all zones are done
        #   12..... Compare minute my minute what is happening

        # START
        # This will be at the beginning of the script before step one
        # when it wants to run seven minutes it drops the last minute

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        date_mngr.set_dates_from_date_range(start_date='12-01-2015', end_date='12-28-2015')
        self.csv_writer.writerow(["Date range of test: ", "", str(date_mngr.begin_date) + " to " + str(date_mngr.end_date)])
        # With current eto/rainfall list:
        print "Day Before Test Starts", date_mngr.prev_day.date_string_for_controller()
        print "First Day of Test", date_mngr.curr_day.date_string_for_controller()

        self.logger.info("Day Before Test Starts: " + date_mngr.prev_day.date_string_for_controller())
        self.logger.info("First Day of Test: " + date_mngr.curr_day.date_string_for_controller())

        # set date and time on the controller to be 2 minutes before midnight than increment the clock past
        # midnight so the controller can make of of its decisions for the new day

        self.config.BaseStation1000[1].set_date_and_time(_date=date_mngr.prev_day.date_string_for_controller(),
                                                         _time='23:59:00')
        # increment clock past midnight this will allow the controller to run all of its own calculations on things like
        # day intervals
        self.config.BaseStation1000[1].do_increment_clock(minutes=2)
        self.config.BaseStation1000[1].verify_date_and_time()

        # First time through test set default values on et to zero
        first_time = True

        # -------------------------------------------------------------------------------------------------------------

        # Start of MAIN LOOP: This loop runs through each day of the test
        while not date_mngr.reached_last_date():

            # set controller to current date or first day of test
            # set the controller to 12:45am
            # the controller takes in all of its eto data at 12:45 and than does all of its eto calculations for the day
            self.config.BaseStation1000[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='00:45:00')
            index = date_mngr.get_current_day_index()
            eto = self.config.daily_eto_value[index]
            rain_fall = self.config.daily_rainfall_value[index]

            # if first day, set initial eto value on all zones to eto (zero for first time through)
            if first_time:
                self.config.BaseStation1000[1].set_initial_eto_value_cn(_initial_et_value=eto)
                first_time = False

            # else, set controller et and rain value for current day
            else:
                self.config.BaseStation1000[1].set_eto_and_date_stamp(_controller_et_value=eto,
                                                                      _controller_rain_value=rain_fall,
                                                                      _controller_date=date_mngr.curr_day.
                                                                      formatted_date_string('%Y%m%d'))

            self.config.BaseStation1000[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='00:59:00')
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)
            self.config.BaseStation1000[1].verify_date_and_time()

            # outputs the date to the log file
            self.logger.info(date_mngr.curr_day.date_string_for_controller())

            # outputting the date to a csv file
            self.csv_writer.writerow("\n")
            self.csv_writer.writerow(["Date: " + str(date_mngr.curr_day.date_string_for_controller())])
            self.csv_writer.writerow(["ETO Value: " + str(eto)])
            self.csv_writer.writerow(["Rain Fall Value: " + str(rain_fall)])

            # ----------------------------------------------------------------------------------------------------------
            # for each zone:
            #   1. get current data from controller
            #   2. verify zone mb
            #   3. get current data for zone program from controller
            #   4. verify zone program runtime
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
                self.config.BaseStation1000[1].zones[zone].verify_moisture_balance()

                self.config.BaseStation1000[1].programs[1].zone_programs[zone].get_data()
                self.config.BaseStation1000[1].programs[1].zone_programs[zone].verify_runtime()
                # output to log file
                logging.info(opcodes.zone + ": " + str(zone) +
                             ", Calculated MB: " + str(self.config.BaseStation1000[1].zones[zone].mb) +
                             ", Controller MB: " + self.config.BaseStation1000[1].zones[zone].data.get_value_string_by_key(
                    opcodes.daily_moisture_balance) +
                             ", Calculated RT: " + str(self.config.BaseStation1000[1].programs[1].zone_programs[zone].rt) +
                             ", Controller RT: " + self.config.BaseStation1000[1].programs[1].zone_programs[zone].data.get_value_string_by_key(
                    opcodes.runtime_total))

                # takes in each value of the key_value_list from each zone program and stores it in a list to be output

            # ----- END FOR --------------------------------------------------------------------------------------------

            self.config.BaseStation1000[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='01:59:00')
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.csv_writer.write_dict_of_obj(self.config.BaseStation1000[1].zones)
            self.csv_writer.write_dict_of_obj(self.config.BaseStation1000[1].programs[1].zone_programs, template=csv.EPA.hydro_zone_headers_zp_var_names)
            self.csv_writer.writerow(["\n"])

            clock = time(hour=2, minute=0, second=15)

            # this should go minute by minute until all zones are done

            #  need to clear out values for accumulating the new day
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].seconds_zone_ran = 0
                self.config.BaseStation1000[1].zones[zone].seconds_zone_soaked = 0
                self.config.BaseStation1000[1].zones[zone].seconds_zone_waited = 0
            # ----- END FOR ----------------------------------------------------------------------------------------

            not_done = True

            while not_done:

                zones_still_running = False
                log_string = "Time: " + str(clock.isoformat())

                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone in self.config.zn_ad_range:
                    self.config.BaseStation1000[1].zones[zone].get_data()
                    _zone_status = self.config.BaseStation1000[1].zones[zone].data.get_value_string_by_key(opcodes.status_code)
                    if _zone_status == opcodes.watering:
                        self.config.BaseStation1000[1].zones[zone].seconds_zone_ran += 60  # using 60 so that we are in seconds
                    elif _zone_status == opcodes.soak_cycle:
                        self.config.BaseStation1000[1].zones[zone].seconds_zone_soaked += 60
                    elif _zone_status == opcodes.waiting_to_water:
                        self.config.BaseStation1000[1].zones[zone].seconds_zone_waited += 60
                    # set flag not all zones are done
                    if _zone_status != opcodes.done_watering:
                        zones_still_running = True

                    # Prepare a string for logging
                    log_string += ", " + opcodes.status_code + "=" + str(_zone_status)
                    # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------

                self.logger.info(log_string)
                log_string = ""
                # this could say if zones_still_running:
                if zones_still_running:
                    not_done = True
                    self.config.BaseStation1000[1].do_increment_clock(minutes=1)
                    self.config.BaseStation1000[1].verify_date_and_time()
                    try:
                        dummy_date = date(1, 1, 1)
                        full_datetime = datetime.combine(dummy_date, clock)
                        added_datetime = full_datetime + timedelta(minutes=1)
                        clock = added_datetime.time()
                    except Exception, ae:
                        raise Exception("catch clock error: " + ae.message)
                else:
                    not_done = False

            # ----- END WHILE ------------------------------------------------------------------------------------------
            # TODO need to log the values for each runtime soaktime and cycle time below
            # increment controller clock to the next day 2 minutes before midnight than increment the clock past
            # midnight so the controller can make of of its decisions for the new day
            self.config.BaseStation1000[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='23:59:00')
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)
            self.config.BaseStation1000[1].verify_date_and_time()

            # move date manager to the next day to remain in sync with controller's current date
            date_mngr.skip_to_next_day()

            output = [["Seconds ran:"], ["Expected Seconds ran:"], ["Seconds soaked:"], ["Seconds Waited:"],["e_msg"]]
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].yesterdays_mb = self.config.BaseStation1000[1].zones[zone].mb
                # runtime is the expected time calculated for the controller to run
                runtime = 60*(int(self.config.BaseStation1000[1].programs[1].zone_programs[zone].rt/60))
                if runtime <= 180:
                    runtime = 0
                if 239 >= runtime > 180:
                    runtime = 240
                # Outputs the seconds ran, soaked, and cycled to the CSV
                # TODO tune all of these into the log file
                # log_string += ", " + opcodes.runtime + "=" + str(.rt)
                # log_string += (str(self.config.BaseStation1000[1].zones[zone].seconds_zone_ran))
                # output[1].append(str(runtime))
                # output[2].append(str(self.config.BaseStation1000[1].zones[zone].seconds_zone_soaked))
                # output[3].append(str(self.config.BaseStation1000[1].zones[zone].seconds_zone_waited))

                if self.config.BaseStation1000[1].zones[zone].seconds_zone_ran == runtime:
                    e_msg = "Zone '{0}' ran for '{1}' seconds which match expected runtime of '{2}' seconds, " \
                            "it soaked for '{3}' seconds, and it waited for '{4}' seconds" \
                        .format(
                        str(zone),                                          # {0}
                        str(self.config.BaseStation1000[1].zones[zone].seconds_zone_ran),      # {1}
                        str(runtime),                                       # {2}
                        str(self.config.BaseStation1000[1].zones[zone].seconds_zone_soaked),   # {3}
                        str(self.config.BaseStation1000[1].zones[zone].seconds_zone_waited)    # {4}}

                    )
                    print(e_msg)
                    self.logger.info(e_msg)
                elif self.config.BaseStation1000[1].zones[zone].seconds_zone_ran > runtime and \
                        abs(self.config.BaseStation1000[1].zones[zone].seconds_zone_ran - runtime) <= 120:
                    e_msg = "Zone '{0}' ran for a longer period of time than was expected: Zone '{0}' " \
                            "It was expected to run for '{1}' but it ran for " \
                            "'{2}' seconds, it soaked for '{3}' seconds, and it waited for '{4}' seconds" \
                        .format(
                        str(zone),                                          # {0}
                        str(runtime),                                       # {1}
                        str(self.config.BaseStation1000[1].zones[zone].seconds_zone_ran),      # {2}
                        str(self.config.BaseStation1000[1].zones[zone].seconds_zone_soaked),   # {3}
                        str(self.config.BaseStation1000[1].zones[zone].seconds_zone_waited)    # {4}
                    )
                    print(e_msg)
                    self.logger.info(e_msg)
                elif self.config.BaseStation1000[1].zones[zone].seconds_zone_ran < runtime and \
                        abs(self.config.BaseStation1000[1].zones[zone].seconds_zone_ran - runtime) <= 120:
                    e_msg = "Zone '{0}' ran for a shorter period of time than was expected: Zone '{0}'" \
                            " It was expected to run for '{1}' but it " \
                            "ran for '{2}' seconds, it soaked for '{3}' seconds, and it waited for '{4}' seconds" \
                        .format(
                        str(zone),                                          # {0}
                        str(runtime),                                       # {1}
                        str(self.config.BaseStation1000[1].zones[zone].seconds_zone_ran),      # {2}
                        str(self.config.BaseStation1000[1].zones[zone].seconds_zone_soaked),   # {3}
                        str(self.config.BaseStation1000[1].zones[zone].seconds_zone_waited)    # {4}
                    )
                    print(e_msg)
                    self.logger.info(e_msg)
                else:
                    e_msg = "Zone '{0}' ran for '{1}' seconds and was expected run for '{2}' seconds, " \
                            "it soaked for '{3}' seconds, and it waited for '{4}' seconds" \
                        .format(
                        str(zone),                                          # {0}
                        str(self.config.BaseStation1000[1].zones[zone].seconds_zone_ran),      # {1}
                        str(runtime),                                       # {2}
                        str(self.config.BaseStation1000[1].zones[zone].seconds_zone_soaked),   # {3}
                        str(self.config.BaseStation1000[1].zones[zone].seconds_zone_waited)    # {4}
                    )
                    self.logger.info("Value Error: " + e_msg)
                    raise ValueError(e_msg)
                self.config.BaseStation1000[1].zones[zone].yesterdays_rt = self.config.BaseStation1000[1].zones[zone].seconds_zone_ran

            self.csv_writer.writerows(output)
