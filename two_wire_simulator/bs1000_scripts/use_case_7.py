import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common import helper_methods
from common.imports import opcodes

__author__ = 'Tige'


class ControllerUseCase7(object):
    """
    Test Name:
        - Nelson Feature Mirrored Zones
    purpose:
        - set up the Nelson feature
            - verify that mirrored zones work

    Coverage area: \n
        - zones
            - Zones mirroring other zones \n
            - verify max concurrency is still honored
            - verify that mirrored zone are not taken into account in zone concurrency

    Things that are not covered: \n
        - verify manual run still makes mirrored zone run \n
        - verifying if master valve is involved does it turn on \n

    Date References:
        - configuration for script is located common\configuration_files\nelson_features.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    ###################################################################################################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        Setup Points of Connection
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].add_poc(_poc_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        Setup Programs
        ############################
        """
        program_8_am_start_times = [480]  # 8:00 am start time
        program_waters_every_day = [1, 1, 1, 1, 1, 1, 1]  # runs everyday
        program_fully_open_water_windows = ['111111111111111111111111']

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the start time for each program to 8:00 am
            # Create program 1
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_description(_ds='PG 1 mirrored zones')
            self.config.BaseStation1000[1].programs[1].set_water_window(_ww=program_fully_open_water_windows)
            self.config.BaseStation1000[1].programs[1].add_point_of_connection(_poc_address=1)
            self.config.BaseStation1000[1].programs[1].set_soak_cycle_mode_disabled()

            self.config.BaseStation1000[1].programs[1].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[1].date_time_start_conditions[1].set_day_time_start(
                _dt=opcodes.true,
                _st_list=program_8_am_start_times,
                _interval_type=opcodes.week_days,
                _interval_args=program_waters_every_day)

            # Create program 10
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=10)
            self.config.BaseStation1000[1].programs[10].set_description(_ds='PG 10 mirrored zones')
            self.config.BaseStation1000[1].programs[10].set_water_window(_ww=program_fully_open_water_windows)
            self.config.BaseStation1000[1].programs[10].add_point_of_connection(_poc_address=1)
            self.config.BaseStation1000[1].programs[10].set_soak_cycle_mode_disabled()

            self.config.BaseStation1000[1].programs[10].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[10].date_time_start_conditions[1].set_day_time_start(
                _dt=opcodes.true,
                _st_list=program_8_am_start_times,
                _interval_type=opcodes.week_days,
                _interval_args=program_waters_every_day)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in range(1, 6):
                self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=zone)
                self.config.BaseStation1000[1].programs[1].zone_programs[zone].set_run_time(_minutes=10)

            for zone in range(6, 11):
                self.config.BaseStation1000[1].programs[10].add_zone_to_program(_zone_address=zone)
                self.config.BaseStation1000[1].programs[10].zone_programs[zone].set_run_time(_minutes=10)

            for zone in range(11, 16):
                self.config.BaseStation1000[1].zones[zone].set_mirror_zone_number(10)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
         Verify all attribute values for all objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """
        Testing zones assign to master valves
        - set controller to allow 10 zones to run concurrent
        - set program 1 to allow 10 zones to run concurrent
        - set clock on controller to 7:59am
        - increment clock to 8:00
        - program 1 should start
        - because program 1 can run 10 zones and the controller can run 10 all zones should run
        - only zones 1-5 should turn on with there associated master valves
        - verify that zones 1 -5 are watering and 6 - 10 are waiting to water
        - because the runtime is set to 11 minutes increment clock 11 minutes
        - only zones 6-10 should turn on with there associated master valves
        - verify that zones 6 -15 are watering and 1 - 5 are done
        - mirrored zones are not accounted for in zone concurrency
        - because the runtime is set to 11 minutes increment clock 11 minutes
        - verify all zones are set to done
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=5)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=5)
            self.config.BaseStation1000[1].programs[10].set_max_concurrent_zones(_number_of_zones=5)

            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation1000[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='07:59:00')
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            for zone in range(1, 6):
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_waiting_to_water()
            for zone in range(11, 16):
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation1000[1].do_increment_clock(minutes=11)

            for zone in range(1, 6):
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()

            for zone in range(6, 11):
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_watering()
            # these are the mirrored zones
            for zone in range(11, 16):
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_watering()

            self.config.BaseStation1000[1].do_increment_clock(minutes=11)

            for zone in range(1, 16):
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                         self.config.test_name,
                         sys._getframe().f_code.co_name,
                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                         str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

