import csv
import os
import logging
import sys
from csv_handler import CSVWriter
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods
from common.imports import opcodes, csv

# this import allows us to keep track of time
from time import sleep
from datetime import time, timedelta, datetime, date

__author__ = 'Tige'


class ControllerUseCase3():
    """
    Test Name:
        - Set Messages
    purpose:
        - set up a full configuration on the controller
            - verify that no programming is lost when you:
                - reboot the controller
                - update the firmware of the controller
                - replace a device
            - verify that after a reboot the controller restarts
                - program return to current watering state
                - zones return to current watering state
                - device return to current conditions (event paused stays paused)
            - verify that firmware update take effects:
                - even with certain devices or attributes are disabled

    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n

        - setting Programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign Programs to water sources \n
            - assign zones to Programs \n
            - set up primary linked zones \n
            - give each zone a run time of 1 hour and 30 minutes \n
            - assign moisture sensors to a primary zone 1 and set to lower limit watering

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow\n
                - high variance limit
                - high variance shut down \n
                - low variance limit
                - low variance shut down \n

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n
                - priority
                    - low
                    - medium
                    - high\n
            - set high flow limit
            - high flow shut down \n
            - set unscheduled flow limit
            - enable unscheduled flow shut down \n
            - set water budget
            - enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions
                - event switch\n
                - set switch empty condition to closed \n
                - set empty wait time\n

        - Reboot the controller:
            - verify all setting \n
                - zone decoder
                    - verify all settings were not lost \n
                - moisture sensor
                    - verify all settings were not lost \n
                - flow decoder
                    - verify all settings were not lost \n
                - event decoder
                    - verify all settings were not lost  \n
                - temperature decoder
                    - verify all settings were not lost \n
        - Replace devices: \n
            - zone decoder
                - verify all settings were not lost \n
            - moisture sensor
                - verify the primary zone setting were not lost \n
            - flow decoder
                - verify all setting were deleted\n
            - event decoder
                - verify all setting were deleted \n
            - temperature decoder
                - verify all setting were delete\n
        - firmware update: \n
            - disable
                - all one of each device type \n
                - program
                - mainline
                - poc
            - verify configuration all stayed \n
    Date References:
        - configuration for script is located common\configuration_files\EPA_test_configuration.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    ###############################################################################################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        Setup Points of Connection
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].add_poc(_poc_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation1000[1].points_of_control[1].set_enabled()
            self.config.BaseStation1000[1].points_of_control[1].set_target_flow(_gpm=1000)
            self.config.BaseStation1000[1].points_of_control[1].set_high_flow_limit(_limit=0.0)
            self.config.BaseStation1000[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=0.0)
            self.config.BaseStation1000[1].points_of_control[1].set_limit_concurrent_to_target_state_disabled()
            self.config.BaseStation1000[1].points_of_control[1].set_fill_time(_minutes=1)
            self.config.BaseStation1000[1].points_of_control[1].set_flow_variance(_percent=10)
            self.config.BaseStation1000[1].points_of_control[1].set_flow_variance_disabled()
            self.config.BaseStation1000[1].points_of_control[1].set_flow_fault_state_disabled()

            self.config.BaseStation1000[1].add_poc(_poc_address=2)
            self.config.BaseStation1000[1].points_of_control[2].add_flow_meter_to_point_of_control(_flow_meter_address=2)
            self.config.BaseStation1000[1].points_of_control[2].add_master_valve_to_point_of_control(_master_valve_address=2)
            self.config.BaseStation1000[1].points_of_control[2].set_enabled()
            self.config.BaseStation1000[1].points_of_control[2].set_target_flow(_gpm=1000)
            self.config.BaseStation1000[1].points_of_control[2].set_high_flow_limit(_limit=0.0)
            self.config.BaseStation1000[1].points_of_control[2].set_unscheduled_flow_limit(_gallons=0.0)
            self.config.BaseStation1000[1].points_of_control[2].set_limit_concurrent_to_target_state_disabled()
            self.config.BaseStation1000[1].points_of_control[2].set_fill_time(_minutes=1)
            self.config.BaseStation1000[1].points_of_control[2].set_flow_variance(_percent=10)
            self.config.BaseStation1000[1].points_of_control[2].set_flow_variance_disabled()
            self.config.BaseStation1000[1].points_of_control[2].set_flow_fault_state_disabled()

            self.config.BaseStation1000[1].add_poc(_poc_address=3)
            self.config.BaseStation1000[1].points_of_control[3].set_enabled()
            self.config.BaseStation1000[1].points_of_control[3].set_target_flow(_gpm=1000)
            self.config.BaseStation1000[1].points_of_control[3].set_high_flow_limit(_limit=0.0)
            self.config.BaseStation1000[1].points_of_control[3].set_unscheduled_flow_limit(_gallons=0.0)
            self.config.BaseStation1000[1].points_of_control[3].set_limit_concurrent_to_target_state_disabled()
            self.config.BaseStation1000[1].points_of_control[3].set_fill_time(_minutes=1)
            self.config.BaseStation1000[1].points_of_control[3].set_flow_variance(_percent=10)
            self.config.BaseStation1000[1].points_of_control[3].set_flow_variance_disabled()
            self.config.BaseStation1000[1].points_of_control[3].set_flow_fault_state_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        Setup Programs
        ############################
        """
        program_number_1_water_windows = ['011111100001111111111110']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for program in range(1, 31):
                self.config.BaseStation1000[1].add_program_to_controller(_program_address=program)
                self.config.BaseStation1000[1].programs[program].set_enabled()
                self.config.BaseStation1000[1].programs[program].set_water_window(_ww=program_number_1_water_windows)
                self.config.BaseStation1000[1].programs[program].set_max_concurrent_zones(1)
                self.config.BaseStation1000[1].programs[program].set_seasonal_adjust(_percent=100)
                self.config.BaseStation1000[1].programs[program].add_point_of_connection(_poc_address=1)
                self.config.BaseStation1000[1].programs[program].add_master_valve(_mv_address=1)
                self.config.BaseStation1000[1].programs[program].set_soak_cycle_mode(_mode=opcodes.disabled)
                self.config.BaseStation1000[1].programs[program].set_cycle_count(_new_count=2)
                self.config.BaseStation1000[1].programs[program].set_soak_time_in_seconds(_seconds=600)
                self.config.BaseStation1000[1].programs[program].set_program_cycles(_number_of_cycles=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for address in range(1, 25):
                self.config.BaseStation1000[1].programs[address].add_zone_to_program(_zone_address=address)
                self.config.BaseStation1000[1].programs[address].zone_programs[address].set_run_time(_minutes=15)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            # time.sleep(20)
            self.config.BaseStation1000[1].verify_full_configuration()
            # this turns off fast sim mode and start the clock
            self.config.BaseStation1000[1].set_sim_mode_to_off()
            self.config.BaseStation1000[1].start_clock()
            # this press the run key so that the controller will come out of the paused state
            self.config.BaseStation1000[1].set_controller_to_run()
    
            self.config.BaseStation1000[1].turn_off_echo()
    
            date_mngr.set_current_date_to_match_computer()
            # By setting a time on the controller we can know what time each message has been set
            self.config.BaseStation1000[1].set_date_and_time(
                _date=date_mngr.curr_computer_date.date_string_for_controller(),
                _time=date_mngr.curr_computer_date.time_string_for_controller())
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """
        This sets the zone messages
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].zones[1].messages.set_checksum_message()
            self.config.BaseStation1000[1].zones[1].messages.verify_checksum_message()
            self.config.BaseStation1000[1].zones[2].messages.set_no_response_message()
            self.config.BaseStation1000[1].zones[2].messages.verify_no_response_message()
            self.config.BaseStation1000[1].zones[3].messages.set_bad_serial_number_message()
            self.config.BaseStation1000[1].zones[3].messages.verify_bad_serial_number_message()
            self.config.BaseStation1000[1].zones[4].messages.set_low_voltage_message()
            self.config.BaseStation1000[1].zones[4].messages.verify_low_voltage_message()
            self.config.BaseStation1000[1].zones[5].messages.set_open_circuit_message()
            self.config.BaseStation1000[1].zones[5].messages.verify_open_circuit_message()
            self.config.BaseStation1000[1].zones[6].messages.set_short_circuit_message()
            self.config.BaseStation1000[1].zones[6].messages.verify_short_circuit_message()
            self.config.BaseStation1000[1].zones[7].messages.set_exceeds_design_flow_message()
            self.config.BaseStation1000[1].zones[7].messages.verify_exceeds_design_flow_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_6(self):
        """
        set messages for the temperature sensor
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].temperature_sensors[1].messages.set_checksum_message()
            self.config.BaseStation1000[1].temperature_sensors[1].messages.verify_checksum_message()
            self.config.BaseStation1000[1].temperature_sensors[2].messages.set_no_response_message()
            self.config.BaseStation1000[1].temperature_sensors[2].messages.verify_no_response_message()
            self.config.BaseStation1000[1].temperature_sensors[3].messages.set_bad_serial_number_message()
            self.config.BaseStation1000[1].temperature_sensors[3].messages.verify_bad_serial_number_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_7(self):
        """
        set messages for the flow meter
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].flow_meters[1].messages.set_checksum_message()
            self.config.BaseStation1000[1].flow_meters[1].messages.verify_checksum_message()
            self.config.BaseStation1000[1].flow_meters[2].messages.set_no_response_message()
            self.config.BaseStation1000[1].flow_meters[2].messages.verify_no_response_message()
            self.config.BaseStation1000[1].flow_meters[3].messages.set_bad_serial_number_message()
            self.config.BaseStation1000[1].flow_meters[3].messages.verify_bad_serial_number_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_8(self):
        """
        set messages for event switch
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].event_switches[1].messages.set_checksum_message()
            self.config.BaseStation1000[1].event_switches[1].messages.verify_checksum_message()
            self.config.BaseStation1000[1].event_switches[2].messages.set_no_response_message()
            self.config.BaseStation1000[1].event_switches[2].messages.verify_no_response_message()
            self.config.BaseStation1000[1].event_switches[3].messages.set_bad_serial_number_message()
            self.config.BaseStation1000[1].event_switches[3].messages.verify_bad_serial_number_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_9(self):
        """
        set messages for master valves
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].master_valves[1].messages.set_checksum_message()
            self.config.BaseStation1000[1].master_valves[1].messages.verify_checksum_message()
            self.config.BaseStation1000[1].master_valves[2].messages.set_no_response_message()
            self.config.BaseStation1000[1].master_valves[2].messages.verify_no_response_message()
            self.config.BaseStation1000[1].master_valves[3].messages.set_bad_serial_number_message()
            self.config.BaseStation1000[1].master_valves[3].messages.verify_bad_serial_number_message()
            self.config.BaseStation1000[1].master_valves[4].messages.set_low_voltage_message()
            self.config.BaseStation1000[1].master_valves[4].messages.verify_low_voltage_message()
            self.config.BaseStation1000[1].master_valves[5].messages.set_open_circuit_message()
            self.config.BaseStation1000[1].master_valves[5].messages.verify_open_circuit_message()
            self.config.BaseStation1000[1].master_valves[6].messages.set_short_circuit_message()
            self.config.BaseStation1000[1].master_valves[6].messages.verify_short_circuit_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_10(self):
        """
        set messages for controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].messages.set_two_wire_over_current_message()
            self.config.BaseStation1000[1].messages.verify_two_wire_over_current_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_11(self):
        """
        set messages for moisture sensors
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].messages.set_checksum_message()
            self.config.BaseStation1000[1].moisture_sensors[1].messages.verify_checksum_message()
            self.config.BaseStation1000[1].moisture_sensors[2].messages.set_no_response_message()
            self.config.BaseStation1000[1].moisture_sensors[2].messages.verify_no_response_message()
            self.config.BaseStation1000[1].moisture_sensors[3].messages.set_bad_serial_number_message()
            self.config.BaseStation1000[1].moisture_sensors[3].messages.verify_bad_serial_number_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        set messages for point of connection
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].points_of_control[1].messages.set_device_error_message()
            self.config.BaseStation1000[1].points_of_control[1].messages.verify_device_error_message()
            self.config.BaseStation1000[1].points_of_control[2].messages.set_high_flow_shutdown_message()
            self.config.BaseStation1000[1].points_of_control[2].messages.verify_high_flow_shutdown_message()
            self.config.BaseStation1000[1].points_of_control[3].messages.set_unscheduled_flow_shutdown_message()
            self.config.BaseStation1000[1].points_of_control[3].messages.verify_unscheduled_flow_shutdown_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_13(self):
        """
        set messages for Programs
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].messages.set_overrun_message()
            self.config.BaseStation1000[1].programs[1].messages.verify_overrun_message()
            self.config.BaseStation1000[1].programs[2].messages.set_learn_flow_complete_errors_message()
            self.config.BaseStation1000[1].programs[2].messages.verify_learn_flow_complete_errors_message()
            self.config.BaseStation1000[1].programs[3].messages.set_calibration_failure_message()
            self.config.BaseStation1000[1].programs[3].messages.verify_calibration_failure_message()
            self.config.BaseStation1000[1].programs[4].messages.set_start_message(opcodes.basemanager)
            self.config.BaseStation1000[1].programs[4].messages.verify_start_message(opcodes.basemanager)
            self.config.BaseStation1000[1].programs[5].messages.set_start_message(opcodes.user)
            self.config.BaseStation1000[1].programs[5].messages.verify_start_message(opcodes.user)
            self.config.BaseStation1000[1].programs[6].messages.set_start_message(opcodes.operator)
            self.config.BaseStation1000[1].programs[6].messages.verify_start_message(opcodes.operator)
            self.config.BaseStation1000[1].programs[7].messages.set_start_message(opcodes.programmer)
            self.config.BaseStation1000[1].programs[7].messages.verify_start_message(opcodes.programmer)
            self.config.BaseStation1000[1].programs[8].messages.set_start_message(opcodes.admin)
            self.config.BaseStation1000[1].programs[8].messages.verify_start_message(opcodes.admin)
            self.config.BaseStation1000[1].programs[9].messages.set_start_message(opcodes.moisture)
            self.config.BaseStation1000[1].programs[9].messages.verify_start_message(opcodes.moisture)
            self.config.BaseStation1000[1].programs[10].messages.set_start_message(opcodes.event_switch)
            self.config.BaseStation1000[1].programs[10].messages.verify_start_message(opcodes.event_switch)
            self.config.BaseStation1000[1].programs[11].messages.set_start_message(opcodes.temperature_sensor)
            self.config.BaseStation1000[1].programs[11].messages.verify_start_message(opcodes.temperature_sensor)
            self.config.BaseStation1000[1].programs[12].messages.set_start_message(opcodes.date_time)
            self.config.BaseStation1000[1].programs[12].messages.verify_start_message(opcodes.date_time)
            self.config.BaseStation1000[1].programs[13].messages.set_pause_message(opcodes.system)
            self.config.BaseStation1000[1].programs[13].messages.verify_pause_message(opcodes.system)
            self.config.BaseStation1000[1].programs[14].messages.set_pause_message(opcodes.moisture)
            self.config.BaseStation1000[1].programs[14].messages.verify_pause_message(opcodes.moisture)
            self.config.BaseStation1000[1].programs[15].messages.set_pause_message(opcodes.event_switch)
            self.config.BaseStation1000[1].programs[15].messages.verify_pause_message(opcodes.event_switch)
            self.config.BaseStation1000[1].programs[16].messages.set_pause_message(opcodes.temperature_sensor)
            self.config.BaseStation1000[1].programs[16].messages.verify_pause_message(opcodes.temperature_sensor)
            # TODO we do not have the logic to calculate when the water window will open yet
            # self.config.BaseStation1000[1].programs[17].messages.set_pause_message(opcodes.water_window_paused)
            # self.config.BaseStation1000[1].programs[17].messages.verify_pause_message(opcodes.water_window_paused)
            self.config.BaseStation1000[1].programs[18].messages.set_pause_message(opcodes.event_day)
            self.config.BaseStation1000[1].programs[18].messages.verify_pause_message(opcodes.event_day)
            self.config.BaseStation1000[1].programs[19].messages.set_stop_message(opcodes.basemanager)
            self.config.BaseStation1000[1].programs[19].messages.verify_stop_message(opcodes.basemanager)
            self.config.BaseStation1000[1].programs[20].messages.set_stop_message(opcodes.user)
            self.config.BaseStation1000[1].programs[20].messages.verify_stop_message(opcodes.user)
            self.config.BaseStation1000[1].programs[21].messages.set_stop_message(opcodes.operator)
            self.config.BaseStation1000[1].programs[21].messages.verify_stop_message(opcodes.operator)
            self.config.BaseStation1000[1].programs[22].messages.set_stop_message(opcodes.programmer)
            self.config.BaseStation1000[1].programs[22].messages.verify_stop_message(opcodes.programmer)
            self.config.BaseStation1000[1].programs[23].messages.set_stop_message(opcodes.admin)
            self.config.BaseStation1000[1].programs[23].messages.verify_stop_message(opcodes.admin)
            self.config.BaseStation1000[1].programs[24].messages.set_stop_message(opcodes.moisture)
            self.config.BaseStation1000[1].programs[24].messages.verify_stop_message(opcodes.moisture)
            self.config.BaseStation1000[1].programs[25].messages.set_stop_message(opcodes.event_switch)
            self.config.BaseStation1000[1].programs[25].messages.verify_stop_message(opcodes.event_switch)
            self.config.BaseStation1000[1].programs[26].messages.set_stop_message(opcodes.temperature_sensor)
            self.config.BaseStation1000[1].programs[26].messages.verify_stop_message(opcodes.temperature_sensor)
            self.config.BaseStation1000[1].programs[27].messages.set_stop_message(opcodes.date_time)
            self.config.BaseStation1000[1].programs[27].messages.verify_stop_message(opcodes.date_time)
            self.config.BaseStation1000[1].programs[28].messages.set_stop_message(opcodes.system_off)
            self.config.BaseStation1000[1].programs[28].messages.verify_stop_message(opcodes.system_off)
            self.config.BaseStation1000[1].programs[29].messages.set_stop_message(opcodes.rain_switch)
            self.config.BaseStation1000[1].programs[29].messages.verify_stop_message(opcodes.rain_switch)
            self.config.BaseStation1000[1].programs[30].messages.set_stop_message(opcodes.rain_delay)
            self.config.BaseStation1000[1].programs[30].messages.verify_stop_message(opcodes.rain_delay)

            date_mngr.set_date_to_default()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

