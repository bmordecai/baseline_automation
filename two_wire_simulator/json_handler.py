__author__ = 'baseline'
"""
Using the with statement as seen below helps ensure the file is closed once we are at the end of the code
underneath the with statement.

With json files, writing to the file or updating an attribute of the file is a three step process:

1. First: Reading the data from the file
    -   Open the file
    -   Load the data to a variable (for storage)
    -   Close the file

2. Second: Modify the read in data as needed

3. Third: Updating the file with new data
    -   Open the file
    -   Write the new data to the file from the variable it was stored in
    -   Close the file

Key File IO:
1. 'mode' - when we open a file, the mode tells it how to treat the file:
    a)  mode='a'    - Says open for writing, appending to the end of the current file
    b)  mode='w+'   - Open file, truncate the file (minimizes the file size temp) for updating (reading and writing)
    c)  mode='r'    - Open file for reading (default)

"""

import os
import json

# TODO: Need to add package 'PyYAML' to IntelliJ/PyCharm
# Here I decided to use the python yaml decoder to decode (read) the JSON data because it returns the dictionary with
# all key and value pairs in 'str' form, not in unicode form (i.e., unicode: u'hello' -> string: 'hello')
# This was necessary when trying to convert serial numbers read from json file to integers -> not encoded properly.
# But now they are. Line 51 is where this is used.

import yaml
# import common.objects.baseunit


def get_data(file_name):
    """
    Get the data from the json file with the file name passed in. \n
    :param file_name: Filename of json object to open \n
    :type file_name: str \n
    :return: The data contained in the json file \n
    :rtype: dict \n
    """
    with open(file_name, 'r') as opened_file:
        # data = json.load(fp=opened_file, encoding="UTF-8")
        data = yaml.safe_load(opened_file)
    return data


def update_data(file_name, data):
    """
    Updates the json object with the specified file name with the new data passed in. The file name can also be a
    path to the location to store the file with the intended filename appended onto the end. \n

    Usage:
        let,
            file_name = "common/object_json_files/test.json"
            data = {'lname': 'baseline'}

        the result,
            the file 'test.json' located in 'common/object_json_files' is opened, modified and then closed.

    :param file_name: Filename of json object to open \n
    :type file_name: str \n

    :param data: Data to update the json file with \n
    :type data: dict \n
    """
    with open(file_name, 'w+') as json_file:
        json_file.write(json.dumps(data, sort_keys=True, indent=4, ensure_ascii=False))


def create_json_object(file_name):
    """
    Creates a json file named as the file name passed in. The file name can also be a path to the location to store
    the file with the intended filename appended onto the end. \n

    Usage:
        let,
            file_name = "common/object_json_files/test.json"

        the result,
            a file named 'test.json' is created and stored in the directory: 'common/object_json_files'

    :param file_name: Name of the json file to create \n
    :type file_name: str \n
    """
    with open(file_name, 'w+') as outfile:
        # here, sort_keys + indent helps 'prettify' the json file
        json.dump({}, outfile, sort_keys=True, indent=4, ensure_ascii=False)


def is_file_present(_file_name):
    """
    Returns if the file is created or not
    :param _file_name: The file to check if it exists
    :type _file_name: str
    :return: True if the file exists, otherwise False
    :rtype: bool
    """
    if os.path.isfile(_file_name) and os.access(_file_name, os.R_OK):
        return True
    else:
        return False


def convert_string_to_json(_json_string):
    """
    :param _json_string: A json string to convert into a json object
    :type _json_string: str
    :return: A json object
    :rtype: dict
    """
    return json.loads(_json_string)


def convert_json_to_string(_json_object):
    """
    :param _json_object: A json object to convert into a string
    :type _json_object: dict
    :return: A json object in string representation
    :rtype: str
    """
    return json.dumps(_json_object, sort_keys=True, indent=4, ensure_ascii=False, encoding="UTF-8")


if __name__ == "__main__":
    fileName = "device_info.json"
    if not is_file_present(fileName):
        create_json_object(fileName)

    # Get the data from the json file
    baseline_versions_json = get_data(fileName)
    print json.dumps(baseline_versions_json, indent=2, sort_keys=True)

    # Modify single-valve major version to 10
    baseline_versions_json['Baseline']['Versions']['1-valve']['major'] = 10

    # Modify quad-valve minor version to 7
    baseline_versions_json['Baseline']['Versions']['4-valve']['minor'] = 7

    # update the file
    update_data(fileName, baseline_versions_json)
    print json.dumps(baseline_versions_json, indent=2, sort_keys=True)
