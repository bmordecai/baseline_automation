import sys
import logging

# this import allows us to directly use the date_mngr
from datetime import time, timedelta, datetime, date

# import common.product as helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from common.logging_handler import log_handler

# Objects
from common.objects.base_classes.web_driver import *

from csv_handler import FileIOOptions, CSVWriter
from common.imports import opcodes, csv
from common import helper_methods

# Objects
from common.epa_package import equations, wbw_imports, ir_asa_resources
from common.objects.base_classes.messages import status_plus_priority_code_dict_3200
# from common.objects.programming.pg_start_stop_pause_3200 import StartConditionFor3200, StopConditionFor3200, PauseConditionFor3200

from common.bacnetpypes.bacnet_request_properties import BacnetClient
# Browser pages used
import page_factory

__author__ = 'Tige'


class ControllerUseCase1(object):
    """
    Test name:
        - Bacnet alarm test
        - This is a copy of the set messages test
    purpose:
        - set up a full configuration on the controller
        - Program up the controller
        - Setup watersource, POC, Mainlines, zones
        - Set all messages and verify them
        - Start up Bacnet client verify messages are set in bacnet.

    Setup
        - Target Controller need to be connect to p2
        - Basemanager login JoeAutoTests must have accsss
        - Banet gold uses this login to get controllers
        - Test controller should show up in a bacnet browser
        - This completes configuration

    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=5)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            program_8am_start_time = [480]
            program_number_1_water_windows = ['011111100001111111111110']
            program_full_open_water_windows = ['111111111111111111111111']
            program_full_closed_water_windows = ['000000000000000000000000']

            # PROGRAM 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_number_1_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=False,
                                                                                                           _mon=True,
                                                                                                           _tues=False,
                                                                                                           _wed=True,
                                                                                                           _thurs=False,
                                                                                                           _fri=True,
                                                                                                           _sat=False)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_water_window(_ww=program_full_open_water_windows)
            self.config.BaseStation3200[1].programs[2].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[2].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_odd_days()
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 3
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_water_window(_ww=program_full_open_water_windows)
            self.config.BaseStation3200[1].programs[3].set_priority_level(_pr_level=3)
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[3].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[3].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[3].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 4
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation3200[1].programs[4].set_enabled()
            self.config.BaseStation3200[1].programs[4].set_water_window(_ww=program_full_open_water_windows)
            self.config.BaseStation3200[1].programs[4].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[4].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[4].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[4].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[4].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 5
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=5)
            self.config.BaseStation3200[1].programs[5].set_enabled()
            self.config.BaseStation3200[1].programs[5].set_water_window(_ww=program_full_open_water_windows)
            self.config.BaseStation3200[1].programs[5].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[5].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[5].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[5].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[5].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 6
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=6)
            self.config.BaseStation3200[1].programs[6].set_enabled()
            self.config.BaseStation3200[1].programs[6].set_water_window(_ww=program_full_open_water_windows)
            self.config.BaseStation3200[1].programs[6].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[6].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[6].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[6].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[6].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 7
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=7)
            self.config.BaseStation3200[1].programs[7].set_enabled()
            self.config.BaseStation3200[1].programs[7].set_water_window(_ww=program_full_open_water_windows)
            self.config.BaseStation3200[1].programs[7].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[7].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[7].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[7].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[7].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 8
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=8)
            self.config.BaseStation3200[1].programs[8].set_enabled()
            self.config.BaseStation3200[1].programs[8].set_water_window(_ww=program_full_open_water_windows)
            self.config.BaseStation3200[1].programs[8].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[8].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[8].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[8].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[8].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 9
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=9)
            self.config.BaseStation3200[1].programs[9].set_enabled()
            self.config.BaseStation3200[1].programs[9].set_water_window(_ww=program_full_open_water_windows)
            self.config.BaseStation3200[1].programs[9].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[9].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[9].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[9].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[9].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 10
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=10)
            self.config.BaseStation3200[1].programs[10].set_enabled()
            self.config.BaseStation3200[1].programs[10].set_water_window(_ww=program_full_closed_water_windows)
            self.config.BaseStation3200[1].programs[10].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[10].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[10].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[10].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[10].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 11
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=11)
            self.config.BaseStation3200[1].programs[11].set_enabled()
            self.config.BaseStation3200[1].programs[11].set_water_window(_ww=program_full_closed_water_windows)
            self.config.BaseStation3200[1].programs[11].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[11].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[11].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[11].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[11].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 12
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=12)
            self.config.BaseStation3200[1].programs[12].set_enabled()
            self.config.BaseStation3200[1].programs[12].set_water_window(_ww=program_full_closed_water_windows)
            self.config.BaseStation3200[1].programs[12].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[12].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[12].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[12].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[12].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 13
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=13)
            self.config.BaseStation3200[1].programs[13].set_enabled()
            self.config.BaseStation3200[1].programs[13].set_water_window(_ww=program_full_closed_water_windows)
            self.config.BaseStation3200[1].programs[13].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[13].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[13].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[13].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[13].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 14
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=14)
            self.config.BaseStation3200[1].programs[14].set_enabled()
            self.config.BaseStation3200[1].programs[14].set_water_window(_ww=program_full_closed_water_windows)
            self.config.BaseStation3200[1].programs[14].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[14].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[14].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[14].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[14].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 15
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=15)
            self.config.BaseStation3200[1].programs[15].set_enabled()
            self.config.BaseStation3200[1].programs[15].set_water_window(_ww=program_full_closed_water_windows)
            self.config.BaseStation3200[1].programs[15].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[15].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[15].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[15].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[15].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 16
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=16)
            self.config.BaseStation3200[1].programs[16].set_enabled()
            self.config.BaseStation3200[1].programs[16].set_water_window(_ww=program_full_closed_water_windows)
            self.config.BaseStation3200[1].programs[16].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[16].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[16].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[16].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[16].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 17
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=17)
            self.config.BaseStation3200[1].programs[17].set_enabled()
            self.config.BaseStation3200[1].programs[17].set_water_window(_ww=program_full_closed_water_windows)
            self.config.BaseStation3200[1].programs[17].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[17].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[17].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[17].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[17].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 18
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=18)
            self.config.BaseStation3200[1].programs[18].set_enabled()
            self.config.BaseStation3200[1].programs[18].set_water_window(_ww=program_full_closed_water_windows)
            self.config.BaseStation3200[1].programs[18].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[18].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[18].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[18].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[18].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 19
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=19)
            self.config.BaseStation3200[1].programs[19].set_enabled()
            self.config.BaseStation3200[1].programs[19].set_water_window(_ww=program_full_closed_water_windows)
            self.config.BaseStation3200[1].programs[19].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[19].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[19].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[19].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[19].set_start_times(_st_list=program_8am_start_time)

            # PROGRAM 20
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=20)
            self.config.BaseStation3200[1].programs[20].set_enabled()
            self.config.BaseStation3200[1].programs[20].set_water_window(_ww=program_full_closed_water_windows)
            self.config.BaseStation3200[1].programs[20].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[20].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[20].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[20].set_watering_intervals_to_semi_monthly()
            self.config.BaseStation3200[1].programs[20].set_start_times(_st_list=program_8am_start_time)

            # self.logger.info("Programs have been setup.")
            # self.logger.info(sys._getframe().f_code.co_name + " complete!")
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Start conditions
            self.config.BaseStation3200[1].programs[1].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].moisture_start_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].moisture_start_conditions[1].set_moisture_threshold(_percent=20.0)

            self.config.BaseStation3200[1].programs[2].add_switch_start_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[2].event_switch_start_conditions[1].set_switch_mode_to_closed()

            self.config.BaseStation3200[1].programs[3].add_temperature_start_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[3].temperature_start_conditions[1].set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[3].temperature_start_conditions[1].set_temperature_threshold(_degrees=98.6)

            self.config.BaseStation3200[1].programs[4].add_pressure_start_condition(_pressure_sensor_address=1)
            self.config.BaseStation3200[1].programs[4].pressure_start_conditions[1].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[4].pressure_start_conditions[1].set_pressure_limit(_limit=45.0)

            # Stop conditions
            self.config.BaseStation3200[1].programs[5].add_moisture_stop_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[5].moisture_stop_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[5].moisture_stop_conditions[1].set_moisture_threshold(_percent=20.0)

            self.config.BaseStation3200[1].programs[6].add_switch_stop_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[6].event_switch_stop_conditions[1].set_switch_mode_to_closed()

            self.config.BaseStation3200[1].programs[7].add_temperature_stop_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[7].temperature_stop_conditions[1].set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[7].temperature_stop_conditions[1].set_temperature_threshold(_degrees=98.6)

            self.config.BaseStation3200[1].programs[8].add_pressure_stop_condition(_pressure_sensor_address=1)
            self.config.BaseStation3200[1].programs[8].pressure_stop_conditions[1].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[8].pressure_stop_conditions[1].set_pressure_limit(_limit=45.0)

            # Pause conditions
            self.config.BaseStation3200[1].programs[9].add_moisture_pause_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[9].moisture_pause_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[9].moisture_pause_conditions[1].set_moisture_threshold(_percent=20.0)

            self.config.BaseStation3200[1].programs[10].add_switch_pause_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[10].event_switch_pause_conditions[1].set_switch_mode_to_closed()

            self.config.BaseStation3200[1].programs[11].add_temperature_pause_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[11].temperature_pause_conditions[1].set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[11].temperature_pause_conditions[1].set_temperature_threshold(_degrees=98.6)

            self.config.BaseStation3200[1].programs[12].add_pressure_pause_condition(_pressure_sensor_address=1)
            self.config.BaseStation3200[1].programs[12].pressure_pause_conditions[1].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[12].pressure_pause_conditions[1].set_pressure_limit(_limit=45.0)

            # Two more start conditions
            self.config.BaseStation3200[1].programs[13].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[13].moisture_start_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[13].moisture_start_conditions[1].set_moisture_threshold(_percent=20.0)

            self.config.BaseStation3200[1].programs[14].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[14].moisture_start_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[14].moisture_start_conditions[1].set_moisture_threshold(_percent=20.0)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        # Zone Programs
        try:

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_soak_time(_minutes=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=1)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[3].zone_programs[3].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[3].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[3].zone_programs[3].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[3].zone_programs[3].set_soak_time(_minutes=5)
            self.config.BaseStation3200[1].programs[3].zone_programs[3].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=2)

            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[4].zone_programs[4].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[4].zone_programs[4].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[4].zone_programs[4].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[4].zone_programs[4].set_soak_time(_minutes=5)
            self.config.BaseStation3200[1].programs[4].zone_programs[4].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=3)

            self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[5].zone_programs[5].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[5].zone_programs[5].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[5].zone_programs[5].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[5].zone_programs[5].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[6].zone_programs[6].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[6].zone_programs[6].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[6].zone_programs[6].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[6].zone_programs[6].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[7].zone_programs[7].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[7].zone_programs[7].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[7].zone_programs[7].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[7].zone_programs[7].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[8].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[8].zone_programs[8].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[8].zone_programs[8].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[8].zone_programs[8].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[8].zone_programs[8].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[9].add_zone_to_program(_zone_address=9)
            self.config.BaseStation3200[1].programs[9].zone_programs[9].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[9].zone_programs[9].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[9].zone_programs[9].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[9].zone_programs[9].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[10].add_zone_to_program(_zone_address=10)
            self.config.BaseStation3200[1].programs[10].zone_programs[10].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[10].zone_programs[10].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[10].zone_programs[10].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[10].zone_programs[10].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[11].add_zone_to_program(_zone_address=11)
            self.config.BaseStation3200[1].programs[11].zone_programs[11].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[11].zone_programs[11].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[11].zone_programs[11].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[11].zone_programs[11].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=12)
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[13].add_zone_to_program(_zone_address=13)
            self.config.BaseStation3200[1].programs[13].zone_programs[13].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[13].zone_programs[13].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[13].zone_programs[13].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[13].zone_programs[13].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[14].add_zone_to_program(_zone_address=14)
            self.config.BaseStation3200[1].programs[14].zone_programs[14].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[14].zone_programs[14].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[14].zone_programs[14].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[14].zone_programs[14].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[15].add_zone_to_program(_zone_address=15)
            self.config.BaseStation3200[1].programs[15].zone_programs[15].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[15].zone_programs[15].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[15].zone_programs[15].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[15].zone_programs[15].set_soak_time(_minutes=5)
            self.config.BaseStation3200[1].programs[15].zone_programs[15].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=3)

            self.config.BaseStation3200[1].programs[16].add_zone_to_program(_zone_address=16)
            self.config.BaseStation3200[1].programs[16].zone_programs[16].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[16].zone_programs[16].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[16].zone_programs[16].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[16].zone_programs[16].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[17].add_zone_to_program(_zone_address=17)
            self.config.BaseStation3200[1].programs[17].zone_programs[17].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[17].zone_programs[17].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[17].zone_programs[17].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[17].zone_programs[17].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[18].add_zone_to_program(_zone_address=18)
            self.config.BaseStation3200[1].programs[18].zone_programs[18].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[18].zone_programs[18].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[18].zone_programs[18].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[18].zone_programs[18].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[19].add_zone_to_program(_zone_address=19)
            self.config.BaseStation3200[1].programs[19].zone_programs[19].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[19].zone_programs[19].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[19].zone_programs[19].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[19].zone_programs[19].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[10].add_zone_to_program(_zone_address=20)
            self.config.BaseStation3200[1].programs[10].zone_programs[20].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[10].zone_programs[20].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[10].zone_programs[20].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[10].zone_programs[20].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[20].add_zone_to_program(_zone_address=21)
            self.config.BaseStation3200[1].programs[20].zone_programs[21].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[20].zone_programs[21].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[20].zone_programs[21].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[20].zone_programs[21].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[20].add_zone_to_program(_zone_address=22)
            self.config.BaseStation3200[1].programs[20].zone_programs[22].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[20].zone_programs[22].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[20].zone_programs[22].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[20].zone_programs[22].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[20].add_zone_to_program(_zone_address=23)
            self.config.BaseStation3200[1].programs[20].zone_programs[23].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[20].zone_programs[23].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[20].zone_programs[23].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[20].zone_programs[23].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[20].add_zone_to_program(_zone_address=24)
            self.config.BaseStation3200[1].programs[20].zone_programs[24].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[20].zone_programs[24].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[20].zone_programs[24].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[20].zone_programs[24].set_soak_time(_minutes=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n

        set up WS 1 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - water budget: 100000
            - water budget shutdown: enabled \n
            - water rationing: enabled \n
            - empty conditions:
                - none
        \n
        set up WS 2 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - water budget: 100000
            - water budget shutdown: enabled \n
            - water rationing: enabled \n
            - empty conditions:
                - none
        \n
        set up WS 3 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - water budget: 100000
            - water budget shutdown: enabled \n
            - water rationing: enabled \n
            - empty conditions:
                - none
        \n
        set up WS 4 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - water budget: 100000
            - water budget shutdown: enabled \n
            - water rationing: enabled \n
            - empty conditions:
                - none
        \n
        set up WS 5 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - water budget: 100000
            - water budget shutdown: enabled \n
            - water rationing: enabled \n
            - empty conditions:
                - none
        \n
        set up WS 6 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - water budget: 100000
            - water budget shutdown: enabled \n
            - water rationing: enabled \n
            - empty conditions:
                - none
        \n
        set up WS 7 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - water budget: 100000
            - water budget shutdown: enabled \n
            - water rationing: enabled \n
            - empty conditions:
                - switch (#3), closed state, 8 min wait time
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

            # Water Source 2
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].water_sources[2].set_enabled()
            self.config.BaseStation3200[1].water_sources[2].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[2].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[2].set_water_rationing_to_enabled()

            # Water Source 3
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=3)
            self.config.BaseStation3200[1].water_sources[3].set_enabled()
            self.config.BaseStation3200[1].water_sources[3].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[3].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[3].set_water_rationing_to_enabled()

            # Water Source 4
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=4)
            self.config.BaseStation3200[1].water_sources[4].set_enabled()
            self.config.BaseStation3200[1].water_sources[4].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[4].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[4].set_water_rationing_to_enabled()

            # Water Source 5
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=5)
            self.config.BaseStation3200[1].water_sources[5].set_enabled()
            self.config.BaseStation3200[1].water_sources[5].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[5].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[5].set_water_rationing_to_enabled()

            # Water Source 6
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=6)
            self.config.BaseStation3200[1].water_sources[6].set_enabled()
            self.config.BaseStation3200[1].water_sources[6].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[6].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[6].set_water_rationing_to_enabled()

            # Water Source 7
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=7)
            self.config.BaseStation3200[1].water_sources[7].set_enabled()
            self.config.BaseStation3200[1].water_sources[7].set_priority(_priority_for_water_source=3)
            self.config.BaseStation3200[1].water_sources[7].set_monthly_watering_budget(_budget=1000,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].water_sources[7].set_water_rationing_to_disabled()

            self.config.BaseStation3200[1].water_sources[5].add_pressure_empty_condition(_pressure_sensor_address=1)
            self.config.BaseStation3200[1].water_sources[5].pressure_empty_conditions[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[5].pressure_empty_conditions[1].set_pressure_empty_limit(_psi=20)
            self.config.BaseStation3200[1].water_sources[5].pressure_empty_conditions[1].set_empty_wait_time(_minutes=8)

            self.config.BaseStation3200[1].water_sources[6].add_switch_empty_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].water_sources[6].switch_empty_conditions[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[6].switch_empty_conditions[1].set_switch_empty_condition_to_closed()
            self.config.BaseStation3200[1].water_sources[6].switch_empty_conditions[1].set_empty_wait_time(_minutes=8)

            self.config.BaseStation3200[1].water_sources[7].add_moisture_empty_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].water_sources[7].moisture_empty_conditions[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[7].moisture_empty_conditions[1].set_moisture_empty_limit(_percent=20)
            self.config.BaseStation3200[1].water_sources[7].moisture_empty_conditions[1].set_empty_wait_time(_minutes=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        set up POC 1 \n
            enable POC 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            assign POC 1 a target flow of 500 \n
            assign POC 1 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 2 \n
            enable POC 2 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 2 \n
            assign POC 2 a target flow of 500 \n
            assign POC 2 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 3 \n
            enable POC 3 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 3 \n
            assign POC 3 a target flow of 500 \n
            assign POC 3 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 4 \n
            enable POC 4 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 4 \n
            assign POC 4 a target flow of 500 \n
            assign POC 4 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 5 \n
            enable POC 5 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 5 \n
            assign POC 5 a target flow of 500 \n
            assign POC 5 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 6 \n
            enable POC 6 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 6 \n
            assign POC 6 a target flow of 500 \n
            assign POC 6 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up POC 7 \n
            enable POC 7 \n
            assign master valve TMV0004 and flow meter TWF0004 to POC 7 \n
            assign POC 7 a target flow of 50 \n
            assign POC 7 to main line 8 \n
            set POC priority to 3-low \n
            set high flow limit to 75 and disable high flow shut down \n
            set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
            set water budget to 1000 and disable water budget shut down \n
            disable water rationing \n
            assign event switch TPD0001 to POC 8 \n
            set switch empty condition to closed \n
            set empty wait time to 540 minutes \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_pressure_sensor_to_point_of_control(_pressure_sensor_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(_point_of_control_address=1)

            # Add & Configure POC 2
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_enabled()
            self.config.BaseStation3200[1].points_of_control[2].add_master_valve_to_point_of_control(_master_valve_address=2)
            self.config.BaseStation3200[1].points_of_control[2].add_flow_meter_to_point_of_control(_flow_meter_address=2)
            self.config.BaseStation3200[1].points_of_control[2].add_pressure_sensor_to_point_of_control(_pressure_sensor_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[2].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[2].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add POC 2 to Water Source 2
            self.config.BaseStation3200[1].water_sources[2].add_point_of_control_to_water_source(_point_of_control_address=2)

            # Add & Configure POC 3
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=3)
            self.config.BaseStation3200[1].points_of_control[3].set_enabled()
            self.config.BaseStation3200[1].points_of_control[3].add_master_valve_to_point_of_control(_master_valve_address=3)
            self.config.BaseStation3200[1].points_of_control[3].add_flow_meter_to_point_of_control(_flow_meter_address=3)
            self.config.BaseStation3200[1].points_of_control[3].add_pressure_sensor_to_point_of_control(_pressure_sensor_address=3)
            self.config.BaseStation3200[1].points_of_control[3].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[3].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[3].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add POC 3 to Water Source 3
            self.config.BaseStation3200[1].water_sources[3].add_point_of_control_to_water_source(_point_of_control_address=3)

            # Add & Configure POC 4
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=4)
            self.config.BaseStation3200[1].points_of_control[4].set_enabled()
            self.config.BaseStation3200[1].points_of_control[4].add_master_valve_to_point_of_control(_master_valve_address=4)
            self.config.BaseStation3200[1].points_of_control[4].add_flow_meter_to_point_of_control(_flow_meter_address=4)
            self.config.BaseStation3200[1].points_of_control[4].add_pressure_sensor_to_point_of_control(_pressure_sensor_address=4)
            self.config.BaseStation3200[1].points_of_control[4].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[4].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[4].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add POC 4 to Water Source 4
            self.config.BaseStation3200[1].water_sources[4].add_point_of_control_to_water_source(_point_of_control_address=4)

            # Add & Configure POC 5
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=5)
            self.config.BaseStation3200[1].points_of_control[5].set_enabled()
            self.config.BaseStation3200[1].points_of_control[5].add_master_valve_to_point_of_control(_master_valve_address=5)
            self.config.BaseStation3200[1].points_of_control[5].add_flow_meter_to_point_of_control(_flow_meter_address=5)
            self.config.BaseStation3200[1].points_of_control[5].add_pressure_sensor_to_point_of_control(_pressure_sensor_address=5)
            self.config.BaseStation3200[1].points_of_control[5].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[5].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[5].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add POC 5 to Water Source 5
            self.config.BaseStation3200[1].water_sources[5].add_point_of_control_to_water_source(_point_of_control_address=5)

            # Add & Configure POC 6
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=6)
            self.config.BaseStation3200[1].points_of_control[6].set_enabled()
            self.config.BaseStation3200[1].points_of_control[6].add_master_valve_to_point_of_control(_master_valve_address=6)
            self.config.BaseStation3200[1].points_of_control[6].add_flow_meter_to_point_of_control(_flow_meter_address=6)
            self.config.BaseStation3200[1].points_of_control[6].add_pressure_sensor_to_point_of_control(_pressure_sensor_address=6)
            self.config.BaseStation3200[1].points_of_control[6].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[6].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[6].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add POC 6 to Water Source 6
            self.config.BaseStation3200[1].water_sources[6].add_point_of_control_to_water_source(_point_of_control_address=6)

            # Add & Configure POC 7
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=7)
            self.config.BaseStation3200[1].points_of_control[7].set_enabled()
            self.config.BaseStation3200[1].points_of_control[7].add_master_valve_to_point_of_control(_master_valve_address=7)
            self.config.BaseStation3200[1].points_of_control[7].add_flow_meter_to_point_of_control(_flow_meter_address=7)
            self.config.BaseStation3200[1].points_of_control[7].add_pressure_sensor_to_point_of_control(_pressure_sensor_address=7)
            self.config.BaseStation3200[1].points_of_control[7].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[7].set_high_flow_limit(_limit=75,
                                                                                    with_shutdown_enabled=False)
            self.config.BaseStation3200[1].points_of_control[7].set_unscheduled_flow_limit(_gallons=5,
                                                                                           with_shutdown_enabled=False)
            # Add POC 7 to Water Source 7
            self.config.BaseStation3200[1].water_sources[7].add_point_of_control_to_water_source(_point_of_control_address=7)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control

        set up main line 1 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 2 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 2 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 3 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 4 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 5 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 6 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 7\n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=True)
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            # Add & Configure ML 2
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].mainlines[2].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[2].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[2].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[2].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=True)
            # Add ML 2 to POC 2
            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=2)

            # Add & Configure ML 3
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=3)
            self.config.BaseStation3200[1].mainlines[3].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[3].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[3].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[3].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[3].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=True)
            # Add ML 3 to POC 3
            self.config.BaseStation3200[1].points_of_control[3].add_mainline_to_point_of_control(_mainline_address=3)

            # Add & Configure ML 4
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=4)
            self.config.BaseStation3200[1].mainlines[4].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[4].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[4].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[4].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[4].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=True)
            # Add ML 4 to POC 4
            self.config.BaseStation3200[1].points_of_control[4].add_mainline_to_point_of_control(_mainline_address=4)

            # Add & Configure ML 5
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=5)
            self.config.BaseStation3200[1].mainlines[5].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[5].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[5].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[5].set_high_flow_variance_tier_one(_percent=20,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[5].set_low_flow_variance_tier_one(_percent=5,
                                                                                       _with_shutdown_enabled=False)
            # Add ML 5 to POC 5
            self.config.BaseStation3200[1].points_of_control[5].add_mainline_to_point_of_control(_mainline_address=5)

            # Add & Configure ML 6
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=6)
            self.config.BaseStation3200[1].mainlines[6].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[6].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[6].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[6].set_high_flow_variance_tier_one(_percent=20,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[6].set_low_flow_variance_tier_one(_percent=5,
                                                                                       _with_shutdown_enabled=False)
            # Add ML 6 to POC 6
            self.config.BaseStation3200[1].points_of_control[6].add_mainline_to_point_of_control(_mainline_address=6)

            # Add & Configure ML 7
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=7)
            self.config.BaseStation3200[1].mainlines[7].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[7].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[7].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[7].set_high_flow_variance_tier_one(_percent=20,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[7].set_low_flow_variance_tier_one(_percent=5,
                                                                                       _with_shutdown_enabled=False)
            # Add ML 7 to POC 7
            self.config.BaseStation3200[1].points_of_control[7].add_mainline_to_point_of_control(_mainline_address=7)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone

        Program | Mainline  | Zones
        ---------------------------------------------
        1       | 1         | 1
        2       | 2         | 2
        3       | 3         | 3
        4       | 4         | 4
        5       | 5         | 5
        6       | 6         | 6
        7       | 7         | 7
        8       | 7         | 8
        9       | 7         | 9
        10      | 7         | 10,20
        11      | 7         | 11
        12      | 7         | 12
        13      | 7         | 13
        14      | 7         | 14
        15      | 7         | 15
        16      | 7         | 16
        17      | 7         | 17
        18      | 7         | 18
        19      | 7         | 19
        20      | 7         | 20,21,22,23,24
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add ZN 1 to ML 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            # Add ZN 2 to ML 2
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=2)
            # Add ZN 3 to ML 3
            self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=3)
            # Add ZN 4 to ML 4
            self.config.BaseStation3200[1].mainlines[4].add_zone_to_mainline(_zone_address=4)
            # Add ZN 5 to ML 5
            self.config.BaseStation3200[1].mainlines[5].add_zone_to_mainline(_zone_address=5)
            # Add ZN 6 to ML 6
            self.config.BaseStation3200[1].mainlines[6].add_zone_to_mainline(_zone_address=6)
            # Add ZN 7 - 24 to ML 7
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=7)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=8)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=9)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=10)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=11)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=12)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=13)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=14)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=15)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=16)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=17)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=18)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=19)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=20)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=21)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=22)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=23)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=24)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Re-set date and time on controller.
        :return:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_current_date_to_match_computer()
            # By setting a time on the controller we can know what time each message has been set
            self.config.BaseStation3200[1].set_date_and_time(
                _date=date_mngr.curr_computer_date.date_string_for_controller(),
                _time=date_mngr.curr_computer_date.time_string_for_controller())

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        this sets the zone messages
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].zones[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].zones[1].messages.verify_bad_serial_number_message()

            self.config.BaseStation3200[1].zones[2].messages.set_missing_24_vac_message()
            self.config.BaseStation3200[1].zones[2].messages.verify_missing_24_vac_message()

            self.config.BaseStation3200[1].zones[3].messages.set_no_response_message()
            self.config.BaseStation3200[1].zones[3].messages.verify_no_response_message()

            self.config.BaseStation3200[1].zones[4].messages.set_open_circuit_message()
            self.config.BaseStation3200[1].zones[4].messages.verify_open_circuit_message()

            self.config.BaseStation3200[1].zones[5].messages.set_short_circuit_message()
            self.config.BaseStation3200[1].zones[5].messages.verify_short_circuit_message()

            self.config.BaseStation3200[1].zones[6].messages.set_learn_flow_complete_error_message()
            self.config.BaseStation3200[1].zones[6].messages.verify_learn_flow_complete_error_message()

            self.config.BaseStation3200[1].zones[7].messages.set_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].zones[7].messages.verify_learn_flow_complete_success_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        set messages for the temperature sensor
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].temperature_sensors[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].temperature_sensors[1].messages.verify_bad_serial_number_message()

            self.config.BaseStation3200[1].temperature_sensors[2].messages.set_device_disabled_message()
            self.config.BaseStation3200[1].temperature_sensors[2].messages.verify_device_disabled_message()

            self.config.BaseStation3200[1].temperature_sensors[3].messages.set_no_response_message()
            self.config.BaseStation3200[1].temperature_sensors[3].messages.verify_no_response_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        set messages for the flow meter
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].flow_meters[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].flow_meters[1].messages.verify_bad_serial_number_message()

            self.config.BaseStation3200[1].flow_meters[2].messages.set_device_disabled_message()
            self.config.BaseStation3200[1].flow_meters[2].messages.verify_device_disabled_message()

            self.config.BaseStation3200[1].flow_meters[3].messages.set_no_response_message()
            self.config.BaseStation3200[1].flow_meters[3].messages.verify_no_response_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        set messages for event switch
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].event_switches[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].event_switches[1].messages.verify_bad_serial_number_message()

            self.config.BaseStation3200[1].event_switches[2].messages.set_disabled_message()
            self.config.BaseStation3200[1].event_switches[2].messages.verify_disabled_message()

            self.config.BaseStation3200[1].event_switches[3].messages.set_no_response_message()
            self.config.BaseStation3200[1].event_switches[3].messages.verify_no_response_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        set messages for pressure sensors
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].pressure_sensors[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].pressure_sensors[1].messages.verify_bad_serial_number_message()

            self.config.BaseStation3200[1].pressure_sensors[2].messages.set_disabled_message()
            self.config.BaseStation3200[1].pressure_sensors[2].messages.verify_disabled_message()

            self.config.BaseStation3200[1].pressure_sensors[3].messages.set_no_response_message()
            self.config.BaseStation3200[1].pressure_sensors[3].messages.verify_no_response_message()

            self.config.BaseStation3200[1].pressure_sensors[4].messages.set_invalid_pressure_reading_message()
            self.config.BaseStation3200[1].pressure_sensors[4].messages.verify_invalid_pressure_reading_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        set messages for moisture sensors
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].moisture_sensors[1].messages.verify_bad_serial_number_message()

            self.config.BaseStation3200[1].moisture_sensors[2].messages.set_device_disabled_message()
            self.config.BaseStation3200[1].moisture_sensors[2].messages.verify_device_disabled_message()

            self.config.BaseStation3200[1].moisture_sensors[3].messages.set_no_response_message()
            self.config.BaseStation3200[1].moisture_sensors[3].messages.verify_no_response_message()

            self.config.BaseStation3200[1].moisture_sensors[4].messages.set_zero_reading_message()
            self.config.BaseStation3200[1].moisture_sensors[4].messages.verify_zero_reading_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        set messages for pumps
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:

            self.config.BaseStation3200[1].pumps[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].pumps[1].messages.verify_bad_serial_number_message()

            self.config.BaseStation3200[1].pumps[2].messages.set_device_disabled_message()
            self.config.BaseStation3200[1].pumps[2].messages.verify_device_disabled_message()

            self.config.BaseStation3200[1].pumps[3].messages.set_no_response_message()
            self.config.BaseStation3200[1].pumps[3].messages.verify_no_response_message()

            self.config.BaseStation3200[1].pumps[3].messages.set_open_circuit_message()
            self.config.BaseStation3200[1].pumps[3].messages.verify_open_circuit_message()

            self.config.BaseStation3200[1].pumps[3].messages.set_short_circuit_message()
            self.config.BaseStation3200[1].pumps[3].messages.verify_short_circuit_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        set messages for master valves
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].master_valves[1].messages.set_bad_serial_number_message()
            self.config.BaseStation3200[1].master_valves[1].messages.verify_bad_serial_number_message()

            self.config.BaseStation3200[1].master_valves[2].messages.set_device_disabled_message()
            self.config.BaseStation3200[1].master_valves[2].messages.verify_device_disabled_message()

            self.config.BaseStation3200[1].master_valves[3].messages.set_no_response_message()
            self.config.BaseStation3200[1].master_valves[3].messages.verify_no_response_message()

            self.config.BaseStation3200[1].master_valves[4].messages.set_open_circuit_message()
            self.config.BaseStation3200[1].master_valves[4].messages.verify_open_circuit_message()

            self.config.BaseStation3200[1].master_valves[5].messages.set_short_circuit_message()
            self.config.BaseStation3200[1].master_valves[5].messages.verify_short_circuit_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

     #################################
    def step_19(self):
        """
        set messages water source
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            pass
            self.config.BaseStation3200[1].water_sources[1].messages.set_exceed_monthly_budget_message()
            self.config.BaseStation3200[1].water_sources[1].messages.verify_exceed_monthly_budget_message()

            self.config.BaseStation3200[1].water_sources[2].messages.set_exceed_monthly_budget_with_shutdown_message()
            self.config.BaseStation3200[1].water_sources[2].messages.verify_exceed_monthly_budget_with_shutdown_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        set messages water source Empty Conditions
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].water_sources[5].pressure_empty_conditions[1].messages.set_empty_condition_with_pressure_sensor_message()
            self.config.BaseStation3200[1].water_sources[5].pressure_empty_conditions[1].messages.verify_empty_condition_with_pressure_sensor_message()

            self.config.BaseStation3200[1].water_sources[6].switch_empty_conditions[1].messages.set_empty_condition_with_event_switch_message()
            self.config.BaseStation3200[1].water_sources[6].switch_empty_conditions[1].messages.verify_empty_condition_with_event_switch_message()

            self.config.BaseStation3200[1].water_sources[7].moisture_empty_conditions[1].messages.set_empty_condition_with_moisture_sensor_message()
            self.config.BaseStation3200[1].water_sources[7].moisture_empty_conditions[1].messages.verify_empty_condition_with_moisture_sensor_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        set messages for point of control
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].points_of_control[1].messages.set_configuration_error_message()
            self.config.BaseStation3200[1].points_of_control[1].messages.verify_configuration_error_message()

            self.config.BaseStation3200[1].points_of_control[2].messages.set_disabled_message()
            self.config.BaseStation3200[1].points_of_control[2].messages.verify_disabled_message()

            self.config.BaseStation3200[1].points_of_control[3].messages.set_high_flow_detected_message()
            self.config.BaseStation3200[1].points_of_control[3].messages.verify_high_flow_detected_message()

            self.config.BaseStation3200[1].points_of_control[4].messages.set_high_flow_shutdown_message()
            self.config.BaseStation3200[1].points_of_control[4].messages.verify_high_flow_shutdown_message()

            self.config.BaseStation3200[1].points_of_control[5].messages.set_high_pressure_detected_message()
            self.config.BaseStation3200[1].points_of_control[5].messages.verify_high_pressure_detected_message()

            self.config.BaseStation3200[1].points_of_control[6].messages.set_high_pressure_shutdown_message()
            self.config.BaseStation3200[1].points_of_control[6].messages.verify_high_pressure_shutdown_message()

            self.config.BaseStation3200[1].points_of_control[7].messages.set_low_pressure_detected_message()
            self.config.BaseStation3200[1].points_of_control[7].messages.verify_low_pressure_detected_message()

            self.config.BaseStation3200[1].points_of_control[7].messages.set_low_pressure_shutdown_message()
            self.config.BaseStation3200[1].points_of_control[7].messages.verify_low_pressure_shutdown_message()

            self.config.BaseStation3200[1].points_of_control[7].messages.set_unscheduled_flow_detected_message()
            self.config.BaseStation3200[1].points_of_control[7].messages.verify_unscheduled_flow_detected_message()

            self.config.BaseStation3200[1].points_of_control[7].messages.set_unscheduled_flow_shutdown_message()
            self.config.BaseStation3200[1].points_of_control[7].messages.verify_unscheduled_flow_shutdown_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        set messages for mainlines
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[1].messages.set_configuration_error_number_message()
            self.config.BaseStation3200[1].mainlines[1].messages.verify_configuration_error_number_message()

            self.config.BaseStation3200[1].mainlines[2].messages.set_disabled_message()
            self.config.BaseStation3200[1].mainlines[2].messages.verify_disabled_message()

            self.config.BaseStation3200[1].mainlines[3].messages.set_learn_flow_fail_flow_bicoders_disabled_message()
            self.config.BaseStation3200[1].mainlines[3].messages.verify_learn_flow_fail_flow_bicoders_disabled_message()

            self.config.BaseStation3200[1].mainlines[4].messages.set_learn_flow_fail_flow_bicoders_error_message()
            self.config.BaseStation3200[1].mainlines[4].messages.verify_learn_flow_fail_flow_bicoders_error_message()

            self.config.BaseStation3200[1].mainlines[5].messages.set_high_flow_variance_shutdown_message()
            self.config.BaseStation3200[1].mainlines[5].messages.verify_high_flow_variance_shutdown_message()

            self.config.BaseStation3200[1].mainlines[6].messages.set_high_flow_variance_detected_message()
            self.config.BaseStation3200[1].mainlines[6].messages.verify_high_flow_variance_detected_message()

            self.config.BaseStation3200[1].mainlines[7].messages.set_low_flow_variance_shutdown_message()
            self.config.BaseStation3200[1].mainlines[7].messages.verify_low_flow_variance_shutdown_message()

            self.config.BaseStation3200[1].mainlines[7].messages.set_low_flow_variance_detected_message()
            self.config.BaseStation3200[1].mainlines[7].messages.verify_low_flow_variance_detected_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_23(self):
        """
        messages for programs
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].messages.set_stop_event_day_message()
            self.config.BaseStation3200[1].programs[1].messages.verify_stop_event_day_message()

            self.config.BaseStation3200[1].programs[2].messages.set_learn_flow_complete_errors_message()
            self.config.BaseStation3200[1].programs[2].messages.verify_learn_flow_complete_errors_message()

            self.config.BaseStation3200[1].programs[3].messages.set_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].programs[3].messages.verify_learn_flow_complete_success_message()

            self.config.BaseStation3200[1].programs[4].messages.set_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].programs[4].messages.verify_learn_flow_complete_success_message()

            self.config.BaseStation3200[1].programs[5].messages.set_priority_preempted_message()
            self.config.BaseStation3200[1].programs[5].messages.verify_priority_preempted_message()

            self.config.BaseStation3200[1].programs[6].messages.set_water_rationed_message()
            self.config.BaseStation3200[1].programs[6].messages.verify_water_rationed_message()

            self.config.BaseStation3200[1].programs[7].messages.set_paused_water_window_message()
            self.config.BaseStation3200[1].programs[7].messages.verify_paused_water_window_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
            #################################

    def step_24(self):
        """
        this sets message for zones that require a program
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].programs[2].zone_programs[2].messages.set_moisture_sensor_calibration_failure_no_change_message()
            self.config.BaseStation3200[1].programs[2].zone_programs[2].messages.verify_moisture_sensor_calibration_failure_no_change_message()

            self.config.BaseStation3200[1].programs[3].zone_programs[3].messages.set_moisture_sensor_calibration_failure_no_saturation_message()
            self.config.BaseStation3200[1].programs[3].zone_programs[3].messages.verify_moisture_sensor_calibration_failure_no_saturation_message()

            self.config.BaseStation3200[1].programs[4].zone_programs[4].messages.set_moisture_sensor_calibration_success_message()
            self.config.BaseStation3200[1].programs[4].zone_programs[4].messages.verify_moisture_sensor_calibration_success_message()

            self.config.BaseStation3200[1].programs[5].zone_programs[5].messages.set_exceeds_available_flow_message()
            self.config.BaseStation3200[1].programs[5].zone_programs[5].messages.verify_exceeds_available_flow_message()

            self.config.BaseStation3200[1].programs[8].zone_programs[8].messages.set_shutdown_on_high_flow_by_flowstation_message()
            self.config.BaseStation3200[1].programs[8].zone_programs[8].messages.verify_shutdown_on_high_flow_by_flowstation_message()

            self.config.BaseStation3200[1].programs[9].zone_programs[9].messages.set_shutdown_on_high_flow_variance_message()
            self.config.BaseStation3200[1].programs[9].zone_programs[9].messages.verify_shutdown_on_high_flow_variance_message()

            self.config.BaseStation3200[1].programs[10].zone_programs[10].messages.set_shutdown_on_low_flow_by_flowstation_message()
            self.config.BaseStation3200[1].programs[10].zone_programs[10].messages.verify_shutdown_on_low_flow_by_flowstation_message()

            self.config.BaseStation3200[1].programs[11].zone_programs[11].messages.set_shutdown_on_low_flow_variance_message()
            self.config.BaseStation3200[1].programs[11].zone_programs[11].messages.verify_shutdown_on_low_flow_variance_message()

            self.config.BaseStation3200[1].programs[2].zone_programs[2].messages.set_moisture_sensor_requires_soak_cycle_message()
            self.config.BaseStation3200[1].programs[2].zone_programs[2].messages.verify_moisture_sensor_requires_soak_cycle_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_25(self):
        """
        set messages program Start Conditions
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].programs[4].pressure_start_conditions[1].messages.set_start_condition_with_pressure_sensor_message()
            self.config.BaseStation3200[1].programs[4].pressure_start_conditions[1].messages.verify_start_condition_with_pressure_sensor_message()

            self.config.BaseStation3200[1].programs[1].moisture_start_conditions[1].messages.set_start_condition_with_moisture_sensor_message()
            self.config.BaseStation3200[1].programs[1].moisture_start_conditions[1].messages.verify_start_condition_with_moisture_sensor_message()

            self.config.BaseStation3200[1].programs[3].temperature_start_conditions[1].messages.set_start_condition_with_temperature_sensor_message()
            self.config.BaseStation3200[1].programs[3].temperature_start_conditions[1].messages.verify_start_condition_with_temperature_sensor_message()

            self.config.BaseStation3200[1].programs[2].event_switch_start_conditions[1].messages.set_start_condition_with_event_switch_message()
            self.config.BaseStation3200[1].programs[2].event_switch_start_conditions[1].messages.verify_start_condition_with_event_switch_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_26(self):
        """
        set messages program Stop Conditions
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].programs[8].pressure_stop_conditions[1].messages.set_stop_condition_with_pressure_sensor_message()
            self.config.BaseStation3200[1].programs[8].pressure_stop_conditions[1].messages.verify_stop_condition_with_pressure_sensor_message()

            self.config.BaseStation3200[1].programs[5].moisture_stop_conditions[1].messages.set_stop_condition_with_moisture_sensor_message()
            self.config.BaseStation3200[1].programs[5].moisture_stop_conditions[1].messages.verify_stop_condition_with_moisture_sensor_message()

            self.config.BaseStation3200[1].programs[7].temperature_stop_conditions[1].messages.set_stop_condition_with_temperature_sensor_message()
            self.config.BaseStation3200[1].programs[7].temperature_stop_conditions[1].messages.verify_stop_condition_with_temperature_sensor_message()

            self.config.BaseStation3200[1].programs[6].event_switch_stop_conditions[1].messages.set_stop_condition_with_event_switch_message()
            self.config.BaseStation3200[1].programs[6].event_switch_stop_conditions[1].messages.verify_stop_condition_with_event_switch_message()

            self.config.BaseStation3200[1].programs[8].pressure_stop_conditions[1].messages.set_stop_condition_with_pressure_sensor_on_controller_message()
            self.config.BaseStation3200[1].programs[8].pressure_stop_conditions[1].messages.verify_stop_condition_with_pressure_sensor_on_controller_message()

            self.config.BaseStation3200[1].programs[5].moisture_stop_conditions[1].messages.set_stop_condition_with_moisture_sensor_on_controller_message()
            self.config.BaseStation3200[1].programs[5].moisture_stop_conditions[1].messages.verify_stop_condition_with_moisture_sensor_on_controller_message()

            self.config.BaseStation3200[1].programs[7].temperature_stop_conditions[1].messages.set_stop_condition_with_temperature_sensor_on_controller_message()
            self.config.BaseStation3200[1].programs[7].temperature_stop_conditions[1].messages.verify_stop_condition_with_temperature_sensor_on_controller_message()

            self.config.BaseStation3200[1].programs[6].event_switch_stop_conditions[1].messages.set_stop_condition_with_event_switch_on_controller_message()
            self.config.BaseStation3200[1].programs[6].event_switch_stop_conditions[1].messages.verify_stop_condition_with_event_switch_on_controller_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_27(self):
        """
        set messages program Pause Conditions
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].programs[12].pressure_pause_conditions[1].messages.set_pause_condition_with_pressure_sensor_message()
            self.config.BaseStation3200[1].programs[12].pressure_pause_conditions[1].messages.verify_pause_condition_with_pressure_sensor_message()

            self.config.BaseStation3200[1].programs[9].moisture_pause_conditions[1].messages.set_pause_condition_with_moisture_sensor_message()
            self.config.BaseStation3200[1].programs[9].moisture_pause_conditions[1].messages.verify_pause_condition_with_moisture_sensor_message()

            self.config.BaseStation3200[1].programs[11].temperature_pause_conditions[1].messages.set_pause_condition_with_temperature_sensor_message()
            self.config.BaseStation3200[1].programs[11].temperature_pause_conditions[1].messages.verify_pause_condition_with_temperature_sensor_message()

            self.config.BaseStation3200[1].programs[10].event_switch_pause_conditions[1].messages.set_pause_condition_with_event_switch_message()
            self.config.BaseStation3200[1].programs[10].event_switch_pause_conditions[1].messages.verify_pause_condition_with_event_switch_message()

            self.config.BaseStation3200[1].programs[12].pressure_pause_conditions[1].messages.set_pause_condition_with_pressure_sensor_on_controller_message()
            self.config.BaseStation3200[1].programs[12].pressure_pause_conditions[1].messages.verify_pause_condition_with_pressure_sensor_on_controller_message()

            self.config.BaseStation3200[1].programs[9].moisture_pause_conditions[1].messages.set_pause_condition_with_moisture_sensor_on_controller_message()
            self.config.BaseStation3200[1].programs[9].moisture_pause_conditions[1].messages.verify_pause_condition_with_moisture_sensor_on_controller_message()

            self.config.BaseStation3200[1].programs[11].temperature_pause_conditions[1].messages.set_pause_condition_with_temperature_sensor_on_controller_message()
            self.config.BaseStation3200[1].programs[11].temperature_pause_conditions[1].messages.verify_pause_condition_with_temperature_sensor_on_controller_message()

            self.config.BaseStation3200[1].programs[10].event_switch_pause_conditions[1].messages.set_pause_condition_with_event_switch_on_controller_message()
            self.config.BaseStation3200[1].programs[10].event_switch_pause_conditions[1].messages.verify_pause_condition_with_event_switch_on_controller_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_28(self):
        """
        set messages for controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].messages.set_restore_failed_from_basemanager_message()
            self.config.BaseStation3200[1].messages.verify_restore_failed_from_basemanager_message()

            self.config.BaseStation3200[1].messages.set_restore_failed_from_usb_drive_message()
            self.config.BaseStation3200[1].messages.verify_restore_failed_from_usb_drive_message()

            self.config.BaseStation3200[1].messages.set_restore_failed_from_internal_flash_message()
            self.config.BaseStation3200[1].messages.verify_restore_failed_from_internal_flash_message()

            self.config.BaseStation3200[1].messages.set_restore_successful_from_basemanager_message()
            self.config.BaseStation3200[1].messages.verify_restore_successful_from_basemanager_message()

            self.config.BaseStation3200[1].messages.set_restore_successful_from_usb_drive_message()
            self.config.BaseStation3200[1].messages.verify_restore_successful_from_usb_drive_message()

            self.config.BaseStation3200[1].messages.set_restore_successful_from_internal_flash_message()
            self.config.BaseStation3200[1].messages.verify_restore_successful_from_internal_flash_message()

            # This message was removed in code version 16.0.565
            # self.config.BaseStation3200[1].messages.set_bl_commander_pause_message()
            # self.config.BaseStation3200[1].messages.verify_bl_commander_pause_message()

            self.config.BaseStation3200[1].messages.set_boot_up_message()
            self.config.BaseStation3200[1].messages.verify_boot_up_message()

            self.config.BaseStation3200[1].messages.set_usb_storage_failure_message()
            self.config.BaseStation3200[1].messages.verify_usb_storage_failure_message()

            self.config.BaseStation3200[1].messages.set_event_date_stop_message()
            self.config.BaseStation3200[1].messages.verify_event_date_stop_message()

            self.config.BaseStation3200[1].messages.set_flow_jumper_stopped_message()
            self.config.BaseStation3200[1].messages.verify_flow_jumper_stopped_message()
            self.config.BaseStation3200[1].messages.set_rain_stop_jumper_message()
            self.config.BaseStation3200[1].messages.set_two_wire_short_circuit_message()
            self.config.BaseStation3200[1].messages.verify_two_wire_short_circuit_message()

            self.config.BaseStation3200[1].messages.set_memory_usage_message()
            self.config.BaseStation3200[1].messages.verify_memory_usage_message()

            self.config.BaseStation3200[1].messages.set_off_message()
            self.config.BaseStation3200[1].messages.verify_off_message()

            self.config.BaseStation3200[1].messages.set_off_message()
            self.config.BaseStation3200[1].messages.verify_off_message()

            self.config.BaseStation3200[1].messages.set_pause_jumper_message()
            self.config.BaseStation3200[1].messages.verify_pause_jumper_message()

            self.config.BaseStation3200[1].messages.set_rain_days_stop_message()
            self.config.BaseStation3200[1].messages.verify_rain_days_stop_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_29(self):
        """
        messages for BaseManager
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            self.config.BaseStation3200[1].basemanager_connection[1].messages.set_connection_error_number_message()
            self.config.BaseStation3200[1].basemanager_connection[1].messages.verify_connection_error_number_message()

            self.config.BaseStation3200[1].basemanager_connection[1].messages.set_no_eto_data_message()
            self.config.BaseStation3200[1].basemanager_connection[1].messages.verify_no_eto_data_message()

            self.config.BaseStation3200[1].basemanager_connection[1].messages.set_message_from_basemanager_message(
                _message_from_basemanager="The IRS is looking for you. Hide!"
            )
            self.config.BaseStation3200[1].basemanager_connection[1].messages.verify_message_from_basemanager_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]



    #################################
    def step_31(self):
        """
        Bacnet client verify the expected messages are set
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            print "Waiting for updates to Bacnet objects"
            time.sleep(60)

            # create the bacnet client start listening for requests
            self.bacnet_client = BacnetClient()

            # what is the expected results
            expectedValuesDict = {1270: 1L, 1570: 1L, 870: 0L, 1670: 1L, 970: 1L, 1070: 1L, 370: 1L, 270: 1L, 1170: 1L,
                                  1870: 1L, 470: 1L,
                                  1470: 1L, 170: 1L, 670: 0L, 770: 1L}

            # set expected results
            self.bacnet_client.setExpectedValueDict(expectedValuesDict)

            # are the attributes of the target device
            # all of this comes from the test configuration
            self.bacnet_client.setTargetDevice(self.config.test_name,
                                               self.config.user_conf.mac_address_for_3200,
                                               self.config.user_conf.bacnet_server_ip)

            print "Looking for Controller ", self.bacnet_client.targetdevicename

            # set the what test to run
            self.bacnet_client.setTestToBinaryInput()

            # run the application
            self.bacnet_client.runloop()

            # verify the results
            self.bacnet_client.verifyExpectedValues()

            # did we pass
            if self.bacnet_client.results:
                print "BACnet Properties Passed Verification "
            else:
                msg = "BACnet Properties Failed Verification "
                print msg
                exception_message = msg, self.bacnet_client.targetdevicebacnetip, self.bacnet_client.targetdevicebacnetport
                raise AssertionError(exception_message)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_32(self):
        """
        Template for next step default pass
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            pass

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]


