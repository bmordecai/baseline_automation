import sys
import logging

# this import allows us to directly use the date_mngr
from datetime import time, timedelta, datetime, date

# import common.product as helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from common.logging_handler import log_handler

# Objects
from common.objects.base_classes.web_driver import *

from csv_handler import FileIOOptions, CSVWriter
from common.imports import opcodes, csv
from common import helper_methods

# Objects
from common.epa_package import equations, wbw_imports, ir_asa_resources
from common.objects.base_classes.messages import status_plus_priority_code_dict_3200
# from common.objects.programming.pg_start_stop_pause_3200 import StartConditionFor3200, StopConditionFor3200, PauseConditionFor3200

from common.bacnetpypes.bacnet_request_properties import BacnetClient
# Browser pages used
import page_factory

__author__ = 'Joe'


class ControllerUseCase2(object):
    """
    Test name:
        - Bacnet Find controller


    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=5)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        Bacnet client verify the expected messages are set
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            print "Waiting for updates to Bacnet objects"
            time.sleep(60)

            # create the bacnet client start listening for requests
            self.bacnet_client = BacnetClient()


            # are the attributes of the target device
            # all of this comes from the test configuration
            self.bacnet_client.setTargetDevice(self.config.test_name,
                                               self.config.user_conf.mac_address_for_3200,
                                               self.config.user_conf.bacnet_server_ip)

            print "Looking for Controller ", self.bacnet_client.targetdevicename

            # set the what test to run
            self.bacnet_client.setTestToFindDevice()

            # run the application
            self.bacnet_client.runloop()

            # verify the results
            self.bacnet_client.verifyDeviceFound()

            # did we pass
            if self.bacnet_client.results:
                print "BACnet Find Device Passed Verification "
            else:
                msg = "BACnet Find Device Failed Verification "
                print msg
                exception_message = msg, self.bacnet_client.targetdevicebacnetip, self.bacnet_client.targetdevicebacnetport
                raise AssertionError(exception_message)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]



