import os

import common.user_configuration as user_conf_module

# jade use cases
import bs_foo_1000_aka_jade.use_case_1 as cuc_jade_1_module
import bs_foo_1000_aka_jade.use_case_2 as cuc_jade_2_module
import bs_foo_1000_aka_jade.use_case_3 as cuc_jade_3_module
import bs_foo_1000_aka_jade.use_case_4 as cuc_jade_4_module
import bs_foo_1000_aka_jade.use_case_5 as cuc_jade_5_module
import bs_foo_1000_aka_jade.use_case_6 as cuc_jade_6_module
import bs_foo_1000_aka_jade.use_case_7 as cuc_jade_7_module
import bs_foo_1000_aka_jade.use_case_8 as cuc_jade_8_module
import bs_foo_1000_aka_jade.use_case_9 as cuc_jade_9_module
import bs_foo_1000_aka_jade.use_case_10 as cuc_jade_10_module
import bs_foo_1000_aka_jade.use_case_11 as cuc_jade_11_module
import bs_foo_1000_aka_jade.use_case_12 as cuc_jade_12_module
import bs_foo_1000_aka_jade.use_case_13 as cuc_jade_13_module
import bs_foo_1000_aka_jade.use_case_14 as cuc_jade_14_module
import bs_foo_1000_aka_jade.use_case_15 as cuc_jade_15_module
import bs_foo_1000_aka_jade.use_case_16 as cuc_jade_16_module
import bs_foo_1000_aka_jade.use_case_17 as cuc_jade_17_module
import bs_foo_1000_aka_jade.use_case_18 as cuc_jade_18_module

_author__ = 'Tige'


def run_use_cases_jade(user_configuration_file_name, passing_tests, failing_tests, manual_tests,
                       specific_tests, auto_update_fw=False):
    """
    This is where we will run all of our 3200 only use cases. \n

    :param user_configuration_file_name:    The name of the configuration file we will be using for the tests. \n
    :type user_configuration_file_name:     str

    :param auto_update_fw:    If you want to automatically update firmware versions of controllers to "latest" version.
    :type auto_update_fw:     bool

    :param passing_tests:   If you want to run 3200 passing tests. True for yes, False for no. \n
    :type passing_tests:    bool

    :param failing_tests:   If you want to run 3200 failing tests. True for yes, False for no. \n
    :type failing_tests:    bool

    :param manual_tests:    If you want to run 3200 manual tests. True for yes, False for no. \n
    :type manual_tests:     bool

    :param specific_tests:  Pick which tests you want to run individually. Will run all if the list is empty. \n
    :type specific_tests:   list[int | float]
    """

    # Load in user configured items from text file (necessary for serial connection)
    user_conf = user_conf_module.UserConfiguration(os.path.join(
        'common/user_credentials',
        user_configuration_file_name)
    )
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #                                                  JADE TESTS                                                      #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Passing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if passing_tests:
        if 1 in specific_tests or len(specific_tests) == 0:
            cuc_jade_1_module.ControllerUseCase1(test_name="1000-UseCase1-BasicLearnFlow",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='basic_flow_test.json')
        if 2 in specific_tests or len(specific_tests) == 0:
            cuc_jade_2_module.ControllerUseCase2(test_name="1000-BasicDesignFlowTest",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='basic_flow_test.json')
        # TODO This test requires a hardware mod on the 1000 for a larger capacitor on the board.
        if 3 in specific_tests or len(specific_tests) == 0:
            cuc_jade_3_module.ControllerUseCase3(test_name="BasicProgrammingTest",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='basic_programming.json')

        if 4 in specific_tests or len(specific_tests) == 0:
            cuc_jade_4_module.ControllerUseCase4(test_name="1000-ConcurrentZonesTest",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='concurrent_zones.json')

        if 5 in specific_tests or len(specific_tests) == 0:
            cuc_jade_5_module.ControllerUseCase5(test_name="1000-EventSwitchTest",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='event_decoder_test.json')

        if 6 in specific_tests or len(specific_tests) == 0:
            cuc_jade_6_module.ControllerUseCase6(test_name="1000-MoistureDecoderTest",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='moisture_decoder_test.json')
        if 7 in specific_tests or len(specific_tests) == 0:
            cuc_jade_7_module.ControllerUseCase7(test_name="1000-TemperatureDecoderTest",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='temperature_decoder_test.json')
        if 8 in specific_tests or len(specific_tests) == 0:
            cuc_jade_8_module.ControllerUseCase8(test_name="1000-StartTimesTest",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='start_times_test.json')
        if 9 in specific_tests or len(specific_tests) == 0:
            cuc_jade_9_module.ControllerUseCase9(test_name="1000-MasterValvePumpTest",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='mv_pump_test.json')
        # TODO This test requires a hardware mod on the 1000 for a larger capacitor on the board.
        if 11 in specific_tests or len(specific_tests) == 0:
            cuc_jade_11_module.ControllerUseCase11(test_name="ReplacingDevicesTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='replacing_devices_test.json')
        if 12 in specific_tests or len(specific_tests) == 0:
            cuc_jade_12_module.ControllerUseCase12(test_name="1000-SoakCycles",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='soak_cycles_test.json')
        if 13 in specific_tests or len(specific_tests) == 0:
            cuc_jade_13_module.ControllerUseCase13(test_name="1000-WateringDayScheduleTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='watering_day_schedule_test.json')
        if 14 in specific_tests or len(specific_tests) == 0:
            cuc_jade_14_module.ControllerUseCase14(test_name="1000-UpperLimitTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='upper_limit_watering.json')

        if 16 in specific_tests or len(specific_tests) == 0:
            cuc_jade_16_module.ControllerUseCase16(test_name="1000-TimedZonesWithSoakCycles",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='timed_zones_with_soak_cycles.json')


    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Manual Tests------############################################## #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #      (Requires Manual Device/Use-Case Modification)                                                              #
    #                                                                                                                  #
    #    - THESE USE CASES ARE NOT APART OF NIGHTLY BUILD                                                              #
    #    - These use cases require setting breakpoints at specific locations in the test in order to configure         #
    #      a real-device to be in an expected state for the test.                                                      #
    #    - Above each use case is a list of break points to set inside the use case. The breakpoints should point      #
    #      to a "To Do" item which explains what the user needs to do.                                                 #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    # Requires USB to be plugged in
    if manual_tests:
        if 18 in specific_tests or len(specific_tests) == 0:
            cuc_jade_18_module.ControllerUseCase18(test_name="CNUseCase18backuprestoreprogramming",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='back_restore_firmware_updates.json')

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Failing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if failing_tests:
        if 10 in specific_tests or len(specific_tests) == 0:
            # TODO This test needs a complete rewrite
            cuc_jade_10_module.ControllerUseCase10(test_name="OneTimeCalibrationTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='calibration_tests.json')
        # TODO this test is failing because the contact state is not changing. This seems to work differently for
        # TODO different 1000s. Eldin's passes 100% of the time, nightly build fails 100% of the time with same version.
        # TODO Maybe look into any hardware differences since the software is the same.
        if 15 in specific_tests or len(specific_tests) == 0:
            cuc_jade_15_module.ControllerUseCase15(test_name="1000-AlertRelayTest",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='alert_relay.json')
        # TODO Breakpoint on line 280 and line 281, follow the on screen prompts
        if 17 in specific_tests or len(specific_tests) == 0:
            cuc_jade_17_module.ControllerUseCase17(test_name="CNUseCase17firmwareupdate",
                                                   user_configuration_instance=user_conf,
                                                   json_configuration_file='back_restore_firmware_updates.json')

        print("########################### ----FINISHED RUNNING Failing 1000 TESTS---- ###############################")
    # ---------------------------------------------------------------------------------------------------------------- #
    # #######################################------Future Needed Tests------########################################## #
    # ---------------------------------------------------------------------------------------------------------------- #

    print("######################### ----YOU ARE A WINNER WINNER CHICKEN DINNER---- ##################################")


if __name__ == "__main__":
    run_use_cases_jade(user_configuration_file_name="user_credentials_eldin.json",
                       passing_tests=True,
                       failing_tests=False,
                       manual_tests=False,
                       specific_tests=[])
    exit()
