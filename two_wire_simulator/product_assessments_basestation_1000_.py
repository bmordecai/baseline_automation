import os

import common.user_configuration as user_conf_module
# 1000 use cases
import bs1000_scripts.use_case_2 as cuc_1000_2_module
import bs1000_scripts.use_case_3 as cuc_1000_3_module
import bs1000_scripts.use_case_4 as cuc_1000_4_module
import bs1000_scripts.use_case_5 as cuc_1000_5_module
import bs1000_scripts.use_case_6 as cuc_1000_6_module
import bs1000_scripts.use_case_7 as cuc_1000_7_module

_author__ = 'Tige'


def run_use_cases_1000(user_configuration_file_name, passing_tests, failing_tests, manual_tests, specific_tests):
    """
    This is where we will run all of our 1000 use cases. \n

    :param user_configuration_file_name:    The name of the configuration file we will be using for the tests. \n
    :type user_configuration_file_name:     str

    :param passing_tests:   If you want to run passing tests. True for yes, False for no. \n
    :type passing_tests:    bool

    :param failing_tests:   If you want to run failing tests. True for yes, False for no. \n
    :type failing_tests:    bool

    :param manual_tests:    If you want to run manual tests. True for yes, False for no. \n
    :type manual_tests:     bool

    :param specific_tests:  Pick which tests you want to run individually. Will run all if the list is empty. \n
    :type specific_tests:   list[int]
    """

    # Load in user configured items from text file (necessary for serial connection)
    user_conf = user_conf_module.UserConfiguration(os.path.join(
        'common/user_credentials',
        user_configuration_file_name)
    )

    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #                                                  1000 TESTS                                                      #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Passing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if passing_tests:
        if 2 in specific_tests or len(specific_tests) == 0:
            cuc_1000_2_module.ControllerUseCase2(test_name="EPATestConfiguration",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='EPA_test_configuration.json')
        if 3 in specific_tests or len(specific_tests) == 0:
            cuc_1000_3_module.ControllerUseCase3(test_name="SetMessages",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='set_messages.json')
        if 4 in specific_tests or len(specific_tests) == 0:
            cuc_1000_4_module.ControllerUseCase4(test_name="NelsonFeatureProgramCycles",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='nelson_features.json')
        if 5 in specific_tests or len(specific_tests) == 0:
            cuc_1000_5_module.ControllerUseCase5(test_name="NelsonFeatureOver15Concurrent",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='nelson_features.json')
        if 6 in specific_tests or len(specific_tests) == 0:
            cuc_1000_6_module.ControllerUseCase6(test_name="NelsonMasterValesWithZones",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='nelson_features.json')
        if 7 in specific_tests or len(specific_tests) == 0:
            cuc_1000_7_module.ControllerUseCase7(test_name="NelsonFeatureMirroredZones",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='nelson_features.json')

        print("########################### ----FINISHED RUNNING PASSING 1000 TESTS---- ###############################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Manual Tests------############################################## #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #      (Requires Manual Device/Use-Case Modification)                                                              #
    #                                                                                                                  #
    #    - THESE USE CASES ARE NOT APART OF NIGHTLY BUILD                                                              #
    #    - These use cases require setting breakpoints at specific locations in the test in order to configure         #
    #      a real-device to be in an expected state for the test.                                                      #
    #    - Above each use case is a list of break points to set inside the use case. The breakpoints should point      #
    #      to a "To Do" item which explains what the user needs to do.                                                 #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    if manual_tests:

        print("########################### ----FINISHED RUNNING MANUAL 1000 TESTS---- ################################")
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Failing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if failing_tests:

        print("########################### ----FINISHED RUNNING Failing 1000 TESTS---- ###############################")
    # ---------------------------------------------------------------------------------------------------------------- #
    # #######################################------Future Needed Tests------########################################## #
    # ---------------------------------------------------------------------------------------------------------------- #

    print("######################### ----YOU ARE A WINNER WINNER CHICKEN DINNER---- ##################################")


if __name__ == "__main__":
    run_use_cases_1000(user_configuration_file_name="user_credentials_eldin.json",
                       passing_tests=True,
                       failing_tests=False,
                       manual_tests=False,
                       specific_tests=[])
    exit()
