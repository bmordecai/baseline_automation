import json_handler
import socket
from common.objects.base_classes.ser import Ser
from common.objects.baseunit import bu, bu_imports, bu_helpers
max_buffer = 1024


class TcpHandler(object):

    def __init__(self, eth_port):
        self._host = socket.gethostname()
        self._port = eth_port
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.bind((self._host, self._port))

        # initialize connection to dummy socket place holder for IDE typing auto complete
        self._connection = socket.socket()

        self._connected_address = ''

    def start_connection(self):
        """
        Start listening for incoming connection request from UI
        :return: True if connection successful, or raise Exception if unsuccessful
        :rtype: bool
        """
        # here, 5 represents the max number of connections to listen for to be made to this socket
        self._socket.listen(5)

        try:
            # accept connection object which returns a connection instance and the connected address
            self._connection, self._connected_address = self._socket.accept()
        except Exception as e:
            e_msg = "Exception occurred trying to accept incoming connection in TCP 'start_connection'. " \
                    "MSG: {msg}".format(msg=e.message)
            raise Exception(e_msg)
        else:
            print "Connection request received from address: {address}".format(address=self._connected_address)
            return True

    def send_msg_and_listen_for_reply(self, tcp_packet):
        """
        Send the message to the UI (user prompt) and then wait for replay from UI (this is a blocking call to the
        current thread)
        :param tcp_packet: TCP packet to send
        :type tcp_packet:
        :return: Reply from UI
        :rtype:
        """
        # Valid packet structure?
        # if not isinstance(tcp_packet, dict):
        #     e_msg = "Invalid packet type for sending over TCP. Expects type: 'dict', received: {received}".format(
        #         received=type(tcp_packet)
        #     )
        #     raise TypeError(e_msg)

        # yes, send message
        #else:
        self.send_msg(_msg=tcp_packet)

        # flag to break out of while loop once we receive the correct response from UI
        received_response = False
        _rmsg = {}

        # infinite receive call (this blocks current thread)
        while not received_response:
            _rmsg = self.receive_msg()

            # check if we received the correct response
            if _rmsg["HEADER"]["TYPE"] == bu_imports.PacketTypes.UI_RESPONSE:
                received_response = True

        return _rmsg

    def send_msg(self, _msg):
        """
        Sends the _msg to the UI
        :param _msg: Message packet to send to UI from Python (this is assumed to be in dictionary format conforming
        to defined manufacturing tcp packet protocol
        :type _msg:
        """
        message_as_tcp_packet = json_handler.convert_json_to_string(_json_object=_msg)
        self._connection.send(message_as_tcp_packet)

    def receive_msg(self):
        """
        Listens for message from UI
        :return: Received message from UI as dictionary
        :rtype: dict
        """
        received_request = self._connection.recv(max_buffer)
        return json_handler.convert_string_to_json(_json_string=received_request)

    def build_tcp_prompt_packet(self, _step, _msg, _p_type, _test_name):
        """
        Build tcp packet from defined Manufacturing structure as a json object in string form
        :param _step: Step number
        :type _step: int
        :param _msg: Message to prompt user
        :type _msg: str
        :param _p_type: Type of message prompt to user (i.e., bu_imports.TCP.PROMPT_TYPES.YES_NO)
        :type _p_type: str
        :param _test_name: Title of prompt to be displayed
        :type _test_name: str

        :return: A tcp packet as json object
        :rtype: dict

        """
        prompt_packet = {}

        # if _p_type in bu_imports.TCP.PROMPT_TYPES:
        prompt_packet = bu_imports.TCP.PACKET_TEMPLATES.PROMPT_UI
        prompt_packet["HEADER"]["STEP_NUMBER"] = _step
        prompt_packet["MESSAGE"] = _msg
        prompt_packet["PROMPT_TYPE"] = _p_type
        # else:
        #     e_msg = "Invalid MFG UI prompt type: {received}. Refer to bu_imports.TCP.PROMPT_TYPES for available types."\
        #             .format(received=_p_type)
        #     raise ValueError(e_msg)

        return prompt_packet

    def build_step_results_packet(self, step, msg, result):
        """
        Build tcp packet from defined Manufacturing structure as a json object in string form
        :param step: Step number
        :type step: int
        :param msg: Step output message
        :type msg: str
        :param result: Success, Failure
        :type result: str
        :return: A tcp packet as json object
        :rtype: dict
        """
        step_result_packet = bu_imports.TCP.PACKET_TEMPLATES.STEP_OUTPUT
        step_result_packet["HEADER"]["STEP_NUMBER"] = step
        step_result_packet["HEADER"]["RESULT"] = result
        step_result_packet["MESSAGE"] = msg
        return step_result_packet

    def build_test_results_packet(self, msg, result):
        """
        Build tcp packet from bu_imports.TCP.PACKET_TEMPLATES.TEST_RESULTS for sending test results to MFG UI.
        :param msg: Output from test to MFG UI
        :type msg: str
        :param result: Result of test - Success, Failure
        :type result: str
        :return: A TCP test results packet
        :rtype: dict
        """
        results_packet = bu_imports.TCP.PACKET_TEMPLATES.TEST_RESULTS
        results_packet["HEADER"]["STEP_NUMBER"] = 0
        results_packet["HEADER"]["RESULT"] = result
        results_packet["MESSAGE"] = msg
        return results_packet


#
# def handle_request(_request):
#     from common.objects.baseunit import bu
#     from common.objects.baseunit import bu_imports
#     from common.objects.baseunit import bu_helpers
#     pass

#
# if __name__ == "__main__":
#     s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#     host = socket.gethostname()
#     port = 10259
#     s.bind((host, port))
#
#     # Wait for someone to attempt to connect
#     s.listen(5)
#
#     # Someone has attempted to connect -> Successfully
#     client, addr = s.accept()
#     client.send("connected")
#     print "Received connection from address: ", addr
#
#     # Sit in infinite loop to process all requests.
#     while True:
#         request = client.recv(max_buffer)
#         received_json = json_handler.convert_string_to_json(request)
#
#         # ###############################################
#         # For Testing Communication with base unit
#         if received_json["header"] == "get_voltage":
#
#             rec_com_num = received_json["comNum"]
#             ser_obj = Ser(comport=rec_com_num)
#             bu_obj = BaseUnit(ser_obj)
#
#             returned_string = bu_obj.read_power(1)
#
#             print "****************** Returned from controller" + returned_string
#
#
#         # ###############################################
#
#         received_json["comNum"] = 10
#         received_json["manuf"] = "Foo"
#         request = json_handler.convert_json_to_string(received_json)
#
#         if request == "close":
#             client.close()
#
#         # elif received_json["test"] == "run":
#         #     MFG_test_suites.test()
#
#         else:
#             # handle_request(request)
#             print "Echoing: {req} back to {add}".format(req=request, add=addr)
#             client.send(request)
