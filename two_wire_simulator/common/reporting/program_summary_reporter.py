from csv_handler import CSVWriter, FileIOOptions
from common.imports.types import *
from common.reporting.graph_view import GraphView
from common.helper_methods import get_data_for_each_object_in_dict

__authors__ = 'Dillon, Ben, Kent, Andy (aka, the "A-Team")'


class ReportingUtil(object):
    """
    Utility used to record irrigation events and programming into a CSV file (similar to the program summary file).
    """

    PROGRAM_START_TIME_EVENT = 'Program Start Time Event'
    PROGRAM_LEARN_FLOW_EVENT = 'Start Program Learn Flow'
    PROGRAM_MANUAL_RUN_EVENT = 'Start Program Manual Run'
    PROGRAM_START_CONDITION_EVENT = 'Program Start Condition'
    ZONE_MANUAL_RUN_EVENT = 'Zone Manual Run'
    ZONE_LEARN_FLOW_EVENT = 'Zone Learn Flow'
    PROGRAMS = 'Programs'
    PROGRAM_ZONES = 'Program Zones'
    PROGRAM_START_CONDITIONS = 'Program Start Conditions'
    PROGRAM_STOP_CONDITIONS = 'Program Stop Conditions'
    PROGRAM_PAUSE_CONDITIONS = 'Program Pause Conditions'
    ZONES = 'Zones'
    MASTER_VALVES = 'Master Valves'
    FLOW_METERS = 'Flow Meters'
    TEMPERATURE_SENSORS = 'Temperature Sensors'
    MOISTURE_SENSORS = 'Moisture Sensors'
    PRESSURE_SENSORS = 'Pressure Sensors'
    EVENT_SWITCHES = 'Event Switches'
    WATER_SOURCES = 'Water Sources'
    POCS = 'POCs'
    MAINLINES = 'Mainlines'

    #################################
    def __init__(self, use_case_config_file, file_name):
        """
        :param use_case_config_file:
        type: use_case_config_file: common.configuration.Configuration
        :param file_name:
        :type file_name: str
        """

        self.local_config = use_case_config_file
        self.file_name = file_name
        if ".csv" in file_name or ".txt" in file_name:
            self.writer = CSVWriter(file_name, '', FileIOOptions.READ_WRITE_OVERWRITE, ',', '\n')
        else:
            e_msg = "Wrong file extension: {0}. Use csv or txt.".format(file_name)
            raise TypeError(e_msg)
        self.water_path_data_store = dict()
        self.water_path_pipe_connections = []
        self.pipe_number = 0

    #################################
    def record_all_programming(self):
        pass

    #################################
    def record_program_start_time_event(self, time, program_ids):
        """
        Captures the start time of the supplied programs and writes to a file.
        Use this action method to populate the next set of status tables.
        
        :param time: Time the start event occurred.
        :type time: str
        :param program_ids: List of all programs that need to start.
        :type program_ids: [int]
        """
        self.writer.writerow([self.PROGRAM_START_TIME_EVENT])
        self.writer.writerow(['Time:', time])
        self.writer.writerow(['Program(s):', program_ids])
        self.writer.writerow([''])

    #################################
    def record_program_learn_flow_event(self, time, program_ids):
        """
        Capture the start time of the Learned Flow event.
        To describe what happened for the specified program summary.
        Use this action method to populate the next set of status tables.

        :param time: Time the Learn Flow event occurs.
        :type time: str
        :param program_ids: List of all programs that need to start.
        :type program_ids: [int]
        """
        self.writer.writerow([self.PROGRAM_LEARN_FLOW_EVENT])
        self.writer.writerow(['Time:', time])
        self.writer.writerow(['Program(s):', program_ids])
        self.writer.writerow([''])

    #################################
    def record_program_manual_run_event(self, time, program_ids):
        """
        Capture the start time of the manual run event.
        To describe what happened for the specified program summary.
        Use this action method to populate the next set of status tables.

        :param time: Time of the manual run event.
        :type time: str
        :param program_ids: List of programs that need to start.
        :type program_ids: [int]
        """
        self.writer.writerow([self.PROGRAM_MANUAL_RUN_EVENT])
        self.writer.writerow(['Time:', time])
        self.writer.writerow(['Program(s):', program_ids])
        self.writer.writerow([''])

    #################################
    def record_program_start_condition_event(self, time, program_ids):
        """
        Capture the start time of the start condition run event.
        To describe what happened for the specified program summary.
        Use this action method to populate the next set of status tables.

        :param time: Time of the start condition run event.
        :type time: str
        :param program_ids: List of programs that need to start.
        :type program_ids: [int]
        """
        self.writer.writerow([self.PROGRAM_START_CONDITION_EVENT])
        self.writer.writerow(['Time:', time])
        self.writer.writerow(['Program(s):', program_ids])
        self.writer.writerow([''])

    #################################
    def record_zone_manual_run_event(self, time, zone_ids):
        """
        Capture the start time of the zone manual run event.
        To describe what happened for the specified program summary.
        Use this action method to populate the next set of status tables.
        :param time: Time of the zone manual run event.
        :type time: str
        :param zone_ids: List of zones that need to start
        :type zone_ids: [int]
        """
        self.writer.writerow([self.ZONE_MANUAL_RUN_EVENT])
        self.writer.writerow(['Time:', time])
        self.writer.writerow(['Zone(s):', zone_ids])
        self.writer.writerow([''])

    #################################
    def record_zone_learn_flow_event(self, time, zone_ids):
        """
        Capture the start time of the zone learn flow event.
        To describe what happened for the specified program summary.
        Use this action method to populate the next set of status tables.
        :param time: Time of the zone learn flow event.
        :type time: str
        :param zone_ids: List of zones that need to start
        :type zone_ids: [int]
        """
        self.writer.writerow([self.ZONE_LEARN_FLOW_EVENT])
        self.writer.writerow(['Time:', time])
        self.writer.writerow(['Zone(s):', zone_ids])
        self.writer.writerow([''])

    ################################
    def record_all_programs_data(self, controller_number):
        """
        Captures all the known programs on specified controller.
        This will output all of the data in the get_data string returned from the controller for all of its programs.

        :param controller_number: Number of controller in the configuration file that you want output for.
        :type controller_number: int
        """
        pass

    #################################
    def get_all_possible_water_path_for_zone(self, zone_address, controller_number):
        """
        Follow the zones references all the way up to a water source to construct the zones water path (where it could
        potentially get its water).
        """
        zone = self.local_config.BaseStation3200[controller_number].zones[zone_address]
        water_path = {}
        mainline_level = {}
        poc_level = {}
        water_source_level = {}
        for mainline in self.local_config.BaseStation3200[controller_number].mainlines.values():
            if mainline.ad == zone.ml:
                # TODO CHECK IF SHARED (If it is shared it should look like FSML1)
                mainline_key = "CN{0}ML{1}".format(controller_number, mainline.ad)
                zone_key = "CN{0}ZN{1}".format(controller_number, zone_address)
                zone_level = {
                    "DF": zone.df,
                    "SS": zone.ss
                }
                mainline_level["FL"] = mainline.fl
                mainline_level["SS"] = mainline.ss
                mainline_level[zone_key] = zone_level
                for poc in self.local_config.BaseStation3200[controller_number].points_of_control.values():
                    # TODO CHECK IF SHARED (If it is shared it should look like FSPC1)
                    if poc.ml == mainline.ad:
                        poc_key = "CN{0}PC{1}".format(controller_number, poc.ad)
                        poc_level["SS"] = poc.ss
                        poc_level["FL"] = self.local_config.BaseStation3200[controller_number].flow_meters[poc.fm].bicoder.vr
                        poc_level[mainline_key] = mainline_level

                        for water_source in self.local_config.BaseStation3200[controller_number].water_sources.values():
                            if water_source.pc == poc.ad:
                                # TODO CHECK IF SHARED (If it is shared it should look like FSWS1)
                                water_source_key = "CN{0}WS{1}".format(controller_number, water_source.ad)
                                water_source_level[poc_key] = poc_level
                                water_source_level["SS"] = water_source.ss
                                water_path[water_source_key] = water_source_level
        return water_path

    ###############################
    def get_configuration(self):
        """
        Retreives all possible water paths starting from the water source.
        For each controller dict we will
            for each controller in controller_dict
                we will be grabbing the water source, poc_list, mainline_list for the controller we are accessing
                for each water source
                    get the water source key
                    get all poc's connected to WS
                    add water_source_level to water_path
                    for each poc number in the water_source_poc_list
                        get poc based off poc number
                        get flow of poc from flow meter
                        get mainline
                        add mainline level to poc level
                        add poc level to water_source level
                        for the poc's mainline
                            get mainline object
                            get mainline flow
                            for each zone assigned to mainline
                                add zone to mainline_level dict
                            for each down stream poc, process it's children
                                repeat starting at line 216
                            for each down stream mainline, process it's children
                                repeat line 223
        """
        #
        # water_path = {}
        # mainline_level = {}
        # poc_level = {}
        # water_source_dict = {}
        # water_source_level = {}
        # for controller_dict in controllers:
        #     for controller in controller_dict.values():
        #         water_source_level = controller.water_sources.

    ################################
    def process_water_paths(self):
        """
        This is a recursive function that will process all down stream pocs, mainlines, and zones.
        for each water source connect it to the poc
        """
        # Assign a number to each flow object
        water_sources = self.get_list_of_water_sources()
        for water_source in water_sources:
            foo = self.get_next_pipe_number()
            self.add_pipe_obj_data_store(water_source, foo)
            self.process_down_stream_pipe_path(water_source, foo)

    ###############################
    def process_down_stream_pipe_path(self, pipe_obj, parent_number):
        """
        This recursive function will number the down stream pipes into a dict.

        :param pipe_obj:        Flow object that we want to map in the data store.
        :type pipe_obj:         common.objects.base_classes.flow.BaseFlow
        :param parent_number:   Number that the passed in object was mapped to.
        :type parent_number:    int
        """
        down_stream_list = pipe_obj.get_down_stream_pipes()
        if len(down_stream_list) == 0:
            return
        else:
            for pipe in down_stream_list:
                if pipe not in self.water_path_data_store.values():
                    foo = self.get_next_pipe_number()
                else:
                    for key, value in self.water_path_data_store.items():
                        if value == pipe:
                            foo = key
                self.add_pipe_obj_data_store(pipe, foo)
                self.add_pipe_connection(parent_number, foo)
                self.process_down_stream_pipe_path(pipe, foo)

    ###############################
    def add_pipe_obj_data_store(self, pipe_obj, pipe_obj_num):
        """
        Give the object an address in our data store.

        :param pipe_obj:        Flow object that we want to map in the data store.
        :type pipe_obj:         common.objects.base_classes.flow.BaseFlow
        :param pipe_obj_num:    Number the object will be mapped to
        :type pipe_obj_num:     int
        :return:
        """
        if pipe_obj in self.water_path_data_store.values():
            return
        self.water_path_data_store[pipe_obj_num] = pipe_obj

    ###############################
    def add_pipe_connection(self, upstream_pipe_object_num, pipe_obj_num):
        """
        Store a connection between two pipe numbers

        :param upstream_pipe_object_num:    Number of the upstream pipe for the connection.
        :type upstream_pipe_object_num:     int
        :param pipe_obj_num:                Number of the downstream pipe for the connection.
        :type pipe_obj_num:                 int
        :return:
        """
        connection_tuple = (upstream_pipe_object_num, pipe_obj_num)
        # If the connection was not already stored, store it
        if connection_tuple not in self.water_path_pipe_connections:
            self.water_path_pipe_connections.append(connection_tuple)

    ###############################
    def get_next_pipe_number(self):
        self.pipe_number += 1
        return self.pipe_number

    # def get_mls_connect_to_poc(self, water_source_obj):
    #         get_list_of_downstream_ml_obj = []
    #     return address_list
    #
    # def get_mls_connect_to_ml(self, water_source_obj):
    #     get_list_of_downstream_ml_obj = []
    #     return address_list
    #
    # def get_pocs_connect_to_ml(self, water_source_obj):
    #     get_list_of_downstream_ml_obj
    #     return address_list
    #
    # def get_zns_connect_to_ml(self, water_source_obj):
    #     get_list_of_downstream_ml_obj = []
    #     return address_list

    ################################
    def get_water_source_key(self, water_source_obj):
        water_source_key = "{0}{1}:{2}{3}".format(
            ControllerCommands.Type.CONTROLLER,     # {0}
            water_source_obj.controller.ad,         # {1}
            WaterSourceCommands.Water_source,       # {2}
            water_source_obj.ad                     # {3}
        )
        return water_source_key

    ################################
    def get_poc_key(self, poc_obj):
        """
        This is a recursive function that will process all down stream pocs, mainlines, and zones.
        :param poc_obj: The water source object we are finding configuration for.
        :type poc_obj: common.objects.programming.ws.WaterSource
        """
        poc_key = "{0}{1}:{2}{3}".format(
            ControllerCommands.Type.CONTROLLER,     # {0}
            poc_obj.controller.ad,                  # {1}
            PointofControlCommands.PointOfControl,  # {2}
            poc_obj.ad                              # {3}
        )
        return poc_key

    ################################
    def get_mainlines_key(self, mainlines_obj):
        """
        This is a recursive function that will process all down stream pocs, mainlines, and zones.
        :param mainlines_obj: The water source object we are finding configuration for.
        :type mainlines_obj: common.objects.programming.ws.WaterSource
        """
        mainline_key = "{0}{1}:{2}{3}".format(
            ControllerCommands.Type.CONTROLLER,     # {0}
            mainlines_obj.controller.ad,            # {1}
            MainlineCommands.Mainline,              # {2}
            mainlines_obj.ad                        # {3}
        )
        return mainline_key

    ################################
    def get_list_of_water_sources(self):
        """
        Get the list of all water sources from all controller
        :return water_source_list: list of water source objects
        :rtype [common.objects.programming.ws.WaterSource]
        """
        water_source_list = []
        list_of_controllers = self.get_list_of_controllers()
        for controller in list_of_controllers:
            water_source_list.extend(controller.water_sources.values())
        return water_source_list

    ################################
    def get_list_of_controllers(self):
        """
        :return: list of all controllers in the configuration
        :rtype: list
        """
        return self.local_config.all_controllers

    ################################
    def get_list_of_pocs(self):
        """
        Get the list of all pocs from all controller
        :return poc_list: list of poc objects
        :rtype [common.objects.programming.point_of_control.PointOfControl]
        """
        poc_list = []
        list_of_controllers = self.get_list_of_controllers()
        for controller in list_of_controllers:
            poc_list.extend(controller.points_of_control.values())
        return poc_list

    ################################
    def get_list_of_mainlines(self):
        """
        Get the list of all mainlines from all controller
        :return mainlines_list: list of mainline objects
        :rtype [common.objects.programming.ml.Mainline]
        """
        mainline_list = []
        list_of_controllers = self.get_list_of_controllers()
        for controller in list_of_controllers:
            mainline_list.extend(controller.mainlines.values())
        return mainline_list

    ################################
    def get_output_as_graph(self, file_name):
        """
        Returns program summary report in a graph format.
        """
        graph_view = GraphView(self.water_path_data_store, self.water_path_pipe_connections)
        graph_view.draw_graph(file_name)

    ################################
    def update_all_statuses(self):
        """
        Get the upstream path for a zone, including certain related attributes
            - Controller Time/Status (connection to FS, BM,
            - WS
        :return:
        """
        for controller_number in self.local_config.BaseStation3200.keys():
            list_of_object_pointers = [
                self.local_config.BaseStation3200[controller_number].zones,
                self.local_config.BaseStation3200[controller_number].moisture_sensors,
                self.local_config.BaseStation3200[controller_number].master_valves,
                self.local_config.BaseStation3200[controller_number].temperature_sensors,
                self.local_config.BaseStation3200[controller_number].flow_meters,
                self.local_config.BaseStation3200[controller_number].event_switches,
                self.local_config.BaseStation3200[controller_number].programs,
                self.local_config.BaseStation3200[controller_number].mainlines,
                self.local_config.BaseStation3200[controller_number].points_of_control,
                self.local_config.BaseStation3200[controller_number].pressure_sensors,
                self.local_config.BaseStation3200[controller_number].pumps,
                self.local_config.BaseStation3200[controller_number].alert_relays,
                self.local_config.BaseStation3200[controller_number].water_sources
            ]

            for each_object in list_of_object_pointers:
                for _ad in sorted(each_object.keys()):
                    if each_object in [self.local_config.BaseStation3200[controller_number].water_sources]:
                        self.local_config.BaseStation3200[controller_number].get_water_source(_ad).get_data()
                    elif each_object in [self.local_config.BaseStation3200[controller_number].points_of_control]:
                        self.local_config.BaseStation3200[controller_number].get_point_of_control(_ad).get_data()
                    elif each_object in [self.local_config.BaseStation3200[controller_number].mainlines]:
                        self.local_config.BaseStation3200[controller_number].get_mainline(_ad).get_data()
                    else:
                        each_object[_ad].get_data()

        for controller_number in self.local_config.FlowStations.keys():
            list_of_object_pointers = [
                self.local_config.FlowStations[controller_number].mainlines,
                self.local_config.FlowStations[controller_number].points_of_control,
                self.local_config.FlowStations[controller_number].water_sources
            ]

            for each_object in list_of_object_pointers:
                for _ad in sorted(each_object.keys()):
                    if each_object in [self.local_config.FlowStations[controller_number].water_sources]:
                        self.local_config.FlowStations[controller_number].get_water_source(_ad).get_data()
                    elif each_object in [self.local_config.FlowStations[controller_number].points_of_control]:
                        self.local_config.FlowStations[controller_number].get_point_of_control(_ad).get_data()
                    elif each_object in [self.local_config.FlowStations[controller_number].mainlines]:
                        self.local_config.FlowStations[controller_number].get_mainline(_ad).get_data()
                    else:
                        each_object[_ad].get_data()