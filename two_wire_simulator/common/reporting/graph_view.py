import networkx as nx
import matplotlib.pyplot as plt


__authors__ = 'Dillon, Ben, Kent, Andy (aka, the "A-Team")'


class GraphView(object):


    #################################
    def __init__(self, pipe_nodes, pipe_connections) :
        """
        This method initializes the GraphView with a reference to Pipe nodes and Pipe connections
        :param pipe_nodes: dictionary of pipes
        :type pipe_nodes: dict[int, common.objects.base_classes.flow.BaseFlow]
        :param pipe_connections: list of pipe connections
        :type pipe_connections: list[(int,int)]
        """

        self.pipe_nodes = pipe_nodes
        self.pipe_connections = pipe_connections
        self.graph = nx.Graph()
        self.positions = {}
        self.running_status_edges = []

    def convert_pipe_nodes_to_graph_nodes(self):
        """
        Convert pipe nodes to graph nodes in networkx library
        """
        foo = [
            (10, 30),
            (0, 20),
            (10, 10),
            (20, 20)
        ]

        # seen_first = False
        for i, node in enumerate(self.pipe_nodes.values()):
            self.graph.add_node(node.get_pipe_identifier())

            # if not seen_first:
            # self.positions[node.get_pipe_identifier()] = foo[i]
                # seen_first = True
            # else:
            #     self.positions[node.get_pipe_identifier()] = ()

    def convert_pipe_connections_to_graph_edges(self):
        """
        Convert pipe connections to edges in networkx library
        """
        for pipe_tuple in self.pipe_connections:
            if len(pipe_tuple) == 2:
                first_item = pipe_tuple[0]
                second_item = pipe_tuple[1]

                first_object = self.pipe_nodes[first_item]
                second_object = self.pipe_nodes[second_item]
                if first_object.statuses.status_is_running() and second_object.statuses.status_is_running():
                    self.running_status_edges.append((first_object.get_pipe_identifier(), second_object.get_pipe_identifier()))
                self.graph.add_edge(first_object.get_pipe_identifier(), second_object.get_pipe_identifier())

    def draw_graph(self, saved_file_name):
        """
        Draw the graph. Allow for the creation of a PNG file in a specified location.

        :return:
        """
        self.convert_pipe_nodes_to_graph_nodes()
        self.convert_pipe_connections_to_graph_edges()
        # nx.draw(self.graph, self.positions, with_labels=True)
        #nx.draw(self.graph, with_labels=True)
        nx.draw_networkx_edges(self.graph, self.positions,
                               width=8, alpha=0.5)
        nx.draw_networkx_edges(self.graph, self.positions,
                               edgelist=self.running_status_edges,
                               width=8, alpha=0.5, edge_color='b')

        # Creates a pop-up window with the diagram
        plt.show(self.graph)
        # Creates a file
        # plt.savefig(saved_file_name)

