__author__ = 'Tige'

from decimal import Decimal


# class BaseVariable():
#     """
# 
#     """
#     def __init__(self):
# 
#         # ~~~ VARIABLES ~~~
#         # ~~~ SETTINGS ~~~

DEFAULT_TIMEOUT = 120

# Even though this dictionary is initialized either in the common 1000 or the common 3200 variables,
# we need an empty place holder here in order to pass inheritance from the common variables to
# base_browser_common
dictionary_for_address_ranges = {}

days_per_month = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30,
                       7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12: 31}

dictionary_for_basemanager_address_pointers = {'p1.basemanagerrr.net': '104.130.159.113',
                                                    'p2.basemanagerrr.net': '104.130.246.18',
                                                    'p3.basemanagerrr.net': '104.130.242.33',
                                                    '104.130.159.113': '104.130.159.113',
                                                    '104.130.246.18': '104.130.246.18',
                                                    '104.130.242.33': '104.130.242.33'
                                                    }
list_of_controller_types = ['1000', '3200']

# These colors are located in css property of the element, specifically the 'background-color' property
# the header is based on the word that is displayed in the device drop menu
list_of_device_types = ['Controller', 'Zones', 'Moisture', 'Flow',
                             'Master Valves', 'Temperature',
                             'Event Switches']
# Non-zone device specific web element headers
# the header is based on the word that is displayed in the header location on the web site

list_of_zone_headers = ['Zone', 'Description', 'Status', 'Programs']
list_of_moisture_headers = ['Sensor', 'Description', 'Status', 'Moisture', 'Temp']
list_of_flow_meter_headers = ['Sensor', 'Description', 'Status', 'Rate', 'Usage', 'Stable']
list_of_master_valve_headers = ['Sensor', 'Description', 'Status']
list_of_temperature_headers = ['Sensor', 'Description', 'Status', 'Temperature']
list_of_event_switches_headers = ['Sensor', 'Description', 'Status', 'State']

dictionary_for_status_colors = {'Okay': ['Green', '#70bf41'],
                                     'Watering': ['Blue', '#0155a4'],
                                     'Done': ['Green', '#70bf41'],
                                     'Waiting': ['yellow', '#f6d328'],
                                     'Soaking': ['Light Blue', '#57a7f9'],
                                     'Paused': ['magenta', '#ff2f92'],
                                     'Rain Delay': ['purple' '#a349a4'],
                                     'Error': ['red', '#c82605'],
                                     'No Connection': ['grey', '#a6aaa9', '#c8c8c8'],
                                     'Disabled': ['grey', '#a6aaa9'],
                                     'Off': ['orange',  '#ffff60'],
                                     'Unassigned': ['transparent', ''],
                                     'Online': ['black', '#484848']}

dictionary_for_device_header_value_types = {'Programs': int,        # Zone device header
                                                 'Moisture': float,      # Moisture Sensors header
                                                 'Temp': float,          # Moisture Sensors header
                                                 'Rate': float,          # Flow Meters header
                                                 'Usage': '',            # TODO Flow Meters header
                                                 'Stable': '',           # TODO Flow Meters header
                                                 'Temperature': float    # Temperature Sensors header
                                                 }

dictionary_reverse_status_color_dictionary = {'#0155a4': 'Watering',
                                                   '#70bf41': 'Done',
                                                   '#f6d328': 'Waiting',
                                                   '#57a7f9': 'Soaking',
                                                   '#ff2f92': 'Paused',
                                                   '#a349a4': 'Rain Delay',
                                                   '#c82605': 'Error',
                                                   '#a6aaa9': 'Disabled',
                                                   '#c8c8c8': 'No Connection',
                                                   '#ffff60': 'Off',
                                                   '#484848': 'Online',
                                                   '': 'Unassigned'}

dictionary_web_id_for_device_status = {'Zones': 'zoneStatusColor'}

status_word = {'Okay': 'Okay',
                    'Watering': 'Watering',
                    'Done': 'Done',
                    'Waiting': 'Waiting',
                    'Soaking': 'Soaking',
                    'Paused': 'Paused',
                    'Rain Delay': 'Rain Delay',
                    'Error': 'Error',
                    'No connection': 'No connection',
                    'Off': 'Off',
                    'Unassigned': 'Unassigned'}

dictionary_for_assign_device_buttons = {'zoneAssign': 'zone',
                                             'moistureAssign': 'moisture',
                                             'flowAssign': 'flowmeter',
                                             'masterAssign': 'mastervalve',
                                             'temperatureAssign': 'temp',
                                             'switchAssign': 'eventswitch'}

dictionary_web_ids_for_main_menu_tabs = {'Maps': 'maptype',
                                              'Quick View': 'manage_view_l',
                                              'Programs': 'programs_view_l',
                                              'LiveView': 'remote_view_l',
                                              'Devices': 'zonesensor_view_l',
                                              'Water Sources': 'water_view_l'}

# All current sub menu items supported
dictionary_web_ids_for_sub_menus = {'Zones': 'zones_view_l',
                                         'Moisture': 'moisture_view_l',
                                         'Flow': 'flow_view_l',
                                         'Master Valves': 'mv_view_l',
                                         'Temperature': 'temperature_view_l',
                                         'Event Switches': 'switch_view_l',
                                         'Point of Connection': 'poc_view_l',
                                         'Mainlines': 'mainline_view_l',
                                         'Company': 'global_view_l',
                                         'Current Site': 'company_view_l',
                                         'Current Controller': 'controller_view_l'}

# Web id's for the edit marker form input fields
dictionary_for_edit_marker_form_ids = {'Label': 'map_marker_label',
                                            'Name': 'map_marker_name',
                                            'Url': 'map_marker_url',
                                            'Description': 'map_marker_description',
                                            'Notes': 'map_marker_notes'}

# the 'range(a, b)' function returns a range from a to b-1, i.e., range(1, 10) returns from 1 to 9
# a range starting with a 1 means that it requires at least 1 character.
dictionary_for_edit_marker_form_max_input_ranges = {'label': range(1, 3),
                                                         'name': range(1, 25),
                                                         'url': range(0, 2000),
                                                         'description': range(0, 70),
                                                         'notes': range(0, 125)}

dictionary_for_map_edit_sub_menu_ids = {'Company': 'map-menu-allsites',
                                             'Current Site': 'map-menu-currentsite',
                                             'Current Controller': 'map-menu-controller',
                                             'Markers': 'map-menu-markers',
                                             'Sites': 'map-menu-markers-type-sites',
                                             'Controllers': 'map-menu-markers-type-controllers',
                                             'Zones': 'map-menu-markers-type-devices-ZN',
                                             'Moisture Sensors': 'map-menu-markers-type-devices-MS',
                                             'Temperature Sensors': 'map-menu-markers-type-devices-TS',
                                             'Master Valves': 'map-menu-markers-type-devices-MV',
                                             'Event Switches': 'map-menu-markers-type-devices-SW',
                                             'Flow Meters': 'map-menu-markers-type-devices-FM',
                                             'Custom Markers': 'map-menu-markers-type-custom'}

dictionary_for_map_edit_sub_menu_back_ids = {'Company Markers': 'menu-marker-list',
                                                  'Site Markers': 'menu-marker-list',
                                                  'Controller Markers': 'menu-marker-list',
                                                  'Sites': 'menu-marker-list-sites',
                                                  'Controllers': 'menu-marker-list-controllers',
                                                  'Custom Markers': 'menu-marker-list-custom',
                                                  'Zones': 'menu-marker-list-devices',
                                                  'Moisture Sensors': 'menu-marker-list-devices',
                                                  'Temperature Sensors': 'menu-marker-list-devices',
                                                  'Master Valves': 'menu-marker-list-devices',
                                                  'Event Switches': 'menu-marker-list-devices',
                                                  'Flow Meters': 'menu-marker-list-devices'}

dictionary_for_edit_marker_color_ids = {'black': 'mapMarkerColor9',
                                             'white': 'mapMarkerColor142',
                                             'blue': 'mapMarkerColor130',
                                             'green': 'mapMarkerColor100',
                                             'orange': 'mapMarkerColor143',
                                             'purple': 'mapMarkerColor144'}

# Old colors from old custom marker method.
# dictionary_for_edit_marker_color_ids =  {'black': 'mapMarkerColor9', 'ivory': 2, 'blue': 3,
#                                         'olive': 4, 'orange': 5, 'violet': 6}

# Device Type Dictionary
dictionary_for_device_assignment_options = {'Zones': 'Assign biCoder',
                                                 'Moisture': 'Assign Sensors',
                                                 'Flow': 'Assign Flow Meters',
                                                 'Master Valves': 'Assign Master Valves',
                                                 'Temperature': 'Assign Temp Sensors',
                                                 'Event Switches': 'Assign Event Switches'}

dictionary_for_convert_device_search_button_to_device_element_id = {'Assign biCoder': 'zoneAssign',
                                                                         'Assign Sensors': 'moistureAssign',
                                                                         'Assign Flow Meters': 'flowAssign',
                                                                         'Assign Master Valves': 'masterAssign',
                                                                         'Assign Temp Sensors': 'temperatureAssign',
                                                                         'Assign Event Switches': 'switchAssign'}

dictionary_for_edit_map_marker_sub_menu_web_ids = {'Sites': 'map-menu-markers-type-sites',
                                                        'Controllers': 'map-menu-markers-type-controllers',
                                                        'Zones': 'map-menu-markers-type-devices-ZN',
                                                        'Moisture Sensors': 'map-menu-markers-type-devices-MS',
                                                        'Temperature Sensors': 'map-menu-markers-type-devices-TS',
                                                        'Master Valves': 'map-menu-markers-type-devices-MV',
                                                        'Event Switches': 'map-menu-markers-type-devices-ES',
                                                        'Flow Meters': 'map-menu-markers-type-devices-FM',
                                                        'Custom Markers': 'map-menu-markers-type-custom'}

dictionary_for_device_type_headers = {'Controller': '',
                                           'Zones': list_of_zone_headers,
                                           'Moisture': list_of_moisture_headers,
                                           'Flow':  list_of_flow_meter_headers,
                                           'Master Valves': list_of_master_valve_headers,
                                           'Temperature': list_of_temperature_headers,
                                           'Event Switches': list_of_event_switches_headers}

dictionary_of_device_type_with_key_value = {'Zones': 'ZN', 'Moisture': 'MS', 'Flow': 'FM',
                                                 'Master Valves': 'MV', 'Temperature': 'TS',
                                                 'Event Switches': 'SW'}

dictionary_for_map_marker_pin_key_values = {"Sites": "site",
                                                 "Controllers": "CN",
                                                 "Zones": "ZN",
                                                 "Moisture Sensors": "MS",
                                                 "Temperature Sensors": "TS",
                                                 "Master Valves": "MV",
                                                 "Event Switches": "SW",
                                                 "Flow Meters": "FM",
                                                 "Custom Markers": "custom"}

# TODO Why is this commented out
# Todo: This is commented out and can be deleted because these values are controller dependent in some cases,
# Todo: so at the controller level, ie common_1000 and common_3200, is where this is handled.
# dictionary_of_zone_headers_max_length = {'Zone': 3, 'Description': 50, 'Status': 8, 'Programs': ''}
# dictionary_of_moisture_headers_max_length = {'Sensor': 7, 'Description': 50, 'Status': 8, 'Moisture': 4,
#                                              'Temp': 4}
# dictionary_of_flow_meter_headers_max_length = {'Sensor': 7, 'Description': 50, 'Status': 8, 'Rate': 4,
#                                                'Usage': '', 'Stable': 4}
# dictionary_of_master_valve_headers_max_length = {'Sensor': 7, 'Description': 50, 'Status': 8}
# dictionary_of_temperature_headers_max_length = {'Sensor': 7, 'Description': 50, 'Status': 8, 'Temperature': 4}
# dictionary_of_event_switches_headers_max_length = {'Sensor': 7, 'Description': 50, 'Status': 8, 'State': 6}

dictionary_for_device_type_headers_max_lengths = {}

map_marker_form_max_character_counts = {'Name': 25, 'Url': 2000, 'Description': 70, 'Notes': 125}

menu_rain_delay_set_footer_ids = {'Current Controller': 'rainDelayControllerfooter',
                                       'Current Site': 'rainDelaySitefooter',
                                       'Current Company': 'rainDelayGlobalfooter'}

menu_rain_delay_end_date_footer_ids = {'Current Controller': 'rainDelayControllerfooterEndDate',
                                            'Current Site': 'rainDelaySitefooterEndDate',
                                            'Current Company': 'rainDelayGlobalfooterEndDate'}

menu_rain_delay_input_ids = {'Current Controller': 'rainDelayControllerInput',
                                  'Current Site': 'rainDelaySiteInput',
                                  'Current Company': 'rainDelayGlobalInput'}

menu_rain_delay_submit_button_ids = {'Current Controller': 'rainDelayControllerSubmit',
                                          'Current Site': 'rainDelaySiteSubmit',
                                          'Current Company': 'rainDelayGlobalSubmit'}

menu_rain_delay_cancel_button_ids = {'Current Controller': 'rainDelayControllerCancel',
                                          'Current Site': 'rainDelaySiteCancel',
                                          'Current Company': 'rainDelayGlobalCancel'}

list_of_valid_marker_types = ("Sites", "Controllers", "Zones", "Moisture Sensors", "Temperature Sensors",
                                   "Master Valves", "Event Switches", "Flow Meters", "Custom Markers")

dictionary_for_device_status_colors = {'Moisture': 'moisture',
                                            'Flow': 'flowmeter',
                                            'Master Valves': 'mastervalve',
                                            'Temperature': 'temp',
                                            'Event Switches': 'eventswitch'}
cn_serial_number = '3K10001'
cn_description = ''
cn_latitude = Decimal('43.609537')
cn_longitude = Decimal('-116.311258')
# Device Serial Number Assignments
zone_number_1 = 'TVA3301'
zone_number_2 = 'TVA3302'
zone_number_3 = 'TVA3303'
zone_number_4 = 'TVA3304'
zone_number_5 = 'TVA3305'
zone_number_6 = 'TVA3306'
zone_number_7 = 'TVA3307'
zone_number_8 = 'TVA3308'
zone_number_9 = 'TVA3309'
zone_number_10 = 'TVA3310'
zone_number_11 = 'TVA3311'
zone_number_12 = 'TVA3312'
zone_number_13 = 'TVB3301'
zone_number_14 = 'TVB3302'
zone_number_15 = 'TVB3303'
zone_number_16 = 'TVB3304'
zone_number_17 = 'TVB3305'
zone_number_18 = 'TVB3306'
zone_number_19 = 'TVB3307'
zone_number_20 = 'TVB3308'
zone_number_21 = 'TVB3309'
zone_number_22 = 'TVB3310'
zone_number_23 = 'TVB3311'
zone_number_24 = 'TVB3312'
zone_number_25 = 'TVE3301'
zone_number_26 = 'TVE3302'
#addresses 47-100
zone_number_47 = 'TSD0002'
zone_number_48 = 'TSD0003'
zone_number_49 = 'TSD0004'
zone_number_50 = 'TSD0005'
zone_number_51 = 'TSD0006'
zone_number_52 = 'TSD0007'
zone_number_53 = 'TSD0008'
zone_number_54 = 'TSE0011'
zone_number_55 = 'TSE0012'
zone_number_56 = 'TSE0021'
zone_number_57 = 'TSE0022'
zone_number_58 = 'TSE0041'
zone_number_59 = 'TSE0042'
zone_number_60 = 'TSE0051'
zone_number_61 = 'TSE0052'
zone_number_62 = 'TSE0061'
zone_number_63 = 'TSE0062'
zone_number_64 = 'TSE0071'
zone_number_65 = 'TSE0072'
zone_number_66 = 'TSE0081'
zone_number_67 = 'TSE0082'
zone_number_69 = 'TSQ0011'
zone_number_70 = 'TSQ0012'
zone_number_71 = 'TSQ0013'
zone_number_72 = 'TSQ0014'
zone_number_73 = 'TSQ0021'
zone_number_74 = 'TSQ0022'
zone_number_75 = 'TSQ0023'
zone_number_76 = 'TSQ0024'
zone_number_77 = 'TSQ0031'
zone_number_78 = 'TSQ0032'
zone_number_79 = 'TSQ0033'
zone_number_80 = 'TSQ0034'
zone_number_81 = 'TSQ0041'
zone_number_82 = 'TSQ0042'
zone_number_83 = 'TSQ0043'
zone_number_84 = 'TSQ0044'
zone_number_85 = 'TSQ0051'
zone_number_86 = 'TSQ0052'
zone_number_87 = 'TSQ0053'
zone_number_88 = 'TSQ0054'
zone_number_89 = 'TSQ0061'
zone_number_90 = 'TSQ0062'
zone_number_91 = 'TSQ0063'
zone_number_92 = 'TSQ0064'
zone_number_93 = 'TSQ0071'
zone_number_94 = 'TSQ0072'
zone_number_95 = 'TSQ0073'
zone_number_96 = 'TSQ0074'
zone_number_97 = 'TSQ0081'
zone_number_98 = 'TSQ0082'
zone_number_99 = 'TSQ0083'
zone_number_100 = 'TSQ0084'
zone_number_101 = 'TVA4301'
zone_number_102 = 'TVA4302'
zone_number_103 = 'TVA4303'
zone_number_104 = 'TVA4304'
zone_number_105 = 'TVA4305'
zone_number_106 = 'TVA4306'
zone_number_107 = 'TVA4307'
zone_number_108 = 'TVA4308'
zone_number_109 = 'TVA4309'
zone_number_110 = 'TVA4310'
zone_number_111 = 'TVA4311'
zone_number_112 = 'TVA4312'
zone_number_113 = 'TVB4301'
zone_number_114 = 'TVB4302'
zone_number_115 = 'TVB4303'
zone_number_116 = 'TVB4304'
zone_number_117 = 'TVB4305'
zone_number_118 = 'TVB4306'
zone_number_119 = 'TVB4307'
zone_number_120 = 'TVB4308'
zone_number_121 = 'TVB4309'
zone_number_122 = 'TVB4310'
zone_number_123 = 'TVB4311'
zone_number_124 = 'TVB4312'
zone_number_125 = 'TVE4301'
zone_number_126 = 'TVE4302'
zone_number_127 = 'TVA5301'
zone_number_128 = 'TVA5302'
zone_number_128 = 'TVA5303'
zone_number_129 = 'TVA5304'
zone_number_130 = 'TVA5305'
zone_number_131 = 'TVA5306'
zone_number_132 = 'TVA5307'
zone_number_133 = 'TVA5308'
zone_number_134 = 'TVA5309'
zone_number_136 = 'TVA5310'
zone_number_137 = 'TVA5311'
zone_number_138 = 'TVA5312'
zone_number_139 = 'TVB5301'
zone_number_140 = 'TVB5302'
zone_number_141 = 'TVB5303'
zone_number_142 = 'TVB5304'
zone_number_143 = 'TVB5305'
zone_number_144 = 'TVB5306'
zone_number_145 = 'TVB5307'
zone_number_146 = 'TVB5308'
zone_number_147 = 'TVB5309'
zone_number_148 = 'TVB5310'
zone_number_149 = 'TVB5311'
zone_number_150 = 'TVB5312'
zone_number_151 = 'TVE5301'
zone_number_152 = 'TVE5302'
zone_number_153 = 'TVA6301'
zone_number_154 = 'TVA6302'
zone_number_155 = 'TVA6303'
zone_number_156 = 'TVA6304'
zone_number_157 = 'TVA6305'
zone_number_158 = 'TVA6306'
zone_number_159 = 'TVA6307'
zone_number_160 = 'TVA6308'
zone_number_161 = 'TVA6309'
zone_number_162 = 'TVA6310'
zone_number_163 = 'TVA6311'
zone_number_164 = 'TVA6312'
zone_number_165 = 'TVB6301'
zone_number_166 = 'TVB6302'
zone_number_167 = 'TVB6303'
#addresses 190-200
zone_number_190 = 'TVB6304'
zone_number_191 = 'TVB6305'
zone_number_192 = 'TVB6306'
zone_number_193 = 'TVB6307'
zone_number_194 = 'TVB6308'
zone_number_195 = 'TVB6309'
zone_number_196 = 'TVB6310'
zone_number_197 = 'TVB6311'
zone_number_198 = 'TVB6312'
zone_number_199 = 'TVE6301'
zone_number_200 = 'TVE6302'
master_valve_1 = 'TSD0001'
master_valve_2 = 'TMV0003'
master_valve_3 = 'TMV0004'
master_valve_6 = 'TWF0002'
master_valve_7 = 'TPR0011'
master_valve_8 = 'TPR0012'

#moisture sensor serial number list
moisture_sensor_1 = 'SB05308'
moisture_sensor_2 = 'SB07258'
moisture_sensor_3 = 'SB01250'
moisture_sensor_4 = 'SB06300'
moisture_sensor_5 = 'SB07184'
moisture_sensor_6 = 'SB02723'
#list of moisture
ms_small_serial_number_list = [moisture_sensor_1,
                                    moisture_sensor_2]

ms_large_serial_number_list = [moisture_sensor_1,
                                    moisture_sensor_2,
                                    moisture_sensor_3,
                                    moisture_sensor_4,
                                    moisture_sensor_5,
                                    moisture_sensor_6]

event_switch_1 = 'TPD0001'
event_switch_2 = 'TPD0002'
event_switch_3 = 'TPD0003'
event_switch_4 = 'TPD0004'
temperature_sensor_1 = 'TAT0001'
temperature_sensor_2 = 'TAT0002'
temperature_sensor_3 = 'SB07258'
temperature_sensor_4 = 'SB07184'
temperature_sensor_5 = 'SB01250'
temperature_sensor_6 = 'SB06300'
temperature_sensor_7 = 'SB05308'
temperature_sensor_8 = 'SB02723'
flow_meter_1 = 'TWF0001'
flow_meter_2 = 'TWF0002'
flow_meter_3 = 'TWF0003'
flow_meter_4 = 'TWF0004'