# import basic methods for helpers
from common.imports import opcodes

# Import warnings so we can mark stuff deprecated
import warnings

# Import main objects for setting Classes
from common.objects.bicoders.valve_bicoder import ValveBicoder
from common.objects.bicoders.flow_bicoder import FlowBicoder
from common.objects.bicoders.pump_bicoder import PumpBicoder
from common.objects.bicoders.switch_bicoder import SwitchBicoder
from common.objects.bicoders.moisture_bicoder import MoistureBicoder
from common.objects.bicoders.temp_bicoder import TempBicoder
from common.objects.bicoders.analog_bicoder import AnalogBicoder
from common.objects.bicoders.alert_relay_bicoder import AlertRelayBicoder

# import simulated bicoders
from common.objects.simulated_bicoders.valve_bicoder import SimulatedValveBicoder
from common.objects.simulated_bicoders.flow_bicoder import SimulatedFlowBicoder
from common.objects.simulated_bicoders.pump_bicoder import SimulatedPumpBicoder
from common.objects.simulated_bicoders.switch_bicoder import SimulatedSwitchBicoder
from common.objects.simulated_bicoders.moisture_bicoder import SimulatedMoistureBicoder
from common.objects.simulated_bicoders.temp_bicoder import SimulatedTempBicoder
from common.objects.simulated_bicoders.analog_bicoder import SimulatedAnalogBicoder
from common.objects.simulated_bicoders.alert_relay_bicoder import SimulatedAlertRelayBicoder


# Pass controller lat/lng to all Device objects
from common.objects.base_classes.devices import BaseDevices
from common.objects.base_classes.basemanager_connections import BaseManagerConnection
BaseDevices.controller_lat = 43.609768
BaseDevices.controller_long = -116.310569

# Imports main objects for setting Devices
from common.objects.devices.zn import Zone
from common.objects.devices.mv import MasterValve
from common.objects.devices.pm import Pump
from common.objects.devices.ms import MoistureSensor
from common.objects.devices.fm import FlowMeter
from common.objects.devices.sw import EventSwitch
from common.objects.devices.ts import TemperatureSensor
from common.objects.devices.ps import PressureSensor
from common.objects.devices.ar import AlertRelay

# flow items
from common.objects.programming.ml import Mainline
from common.objects.programming.ws import WaterSource
from common.objects.programming.conditions.empty_condition import SwitchEmptyCondition, PressureEmptyCondition, MoistureEmptyCondition
from common.objects.programming.point_of_control import PointOfControl
from common.objects.programming.poc_1000 import POC1000

from common.objects.programming.pg_3200 import PG3200
from common.objects.programming.pg_1000 import PG1000

from common.objects.programming.conditions.start_condition_1000 import DateStartCondition1000
from common.objects.programming.conditions.start_condition import SwitchStartCondition, TemperatureStartCondition, MoistureStartCondition, PressureStartCondition
from common.objects.programming.conditions.stop_condition import SwitchStopCondition, TemperatureStopCondition, MoistureStopCondition, PressureStopCondition
from common.objects.programming.conditions.pause_condition import SwitchPauseCondition, TemperaturePauseCondition, MoisturePauseCondition, PressurePauseCondition

from common.objects.programming.zp import ZoneProgram

from common.imports import opcodes

__author__ = 'baseline'


#################################
def create_bicoder_object(_controller, _type, _serial_number, _device_type=None):
    """
    Create all moisture sensor objects for a given range and serial numbers. \n

    :param _controller:     The controller that will be passed in to each moisture sensor and moisture bicoder made
                            by this method. \n
    :type _controller:      common.objects.base_classes.controller.BaseController |
                            common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 |
                            common.objects.controllers.bl_sb.Substation \n

    :param _type:           Type of bicoder. (ex: 'd1', 'd2', 'ms', 'fm') \n
    :type _type:            str \n

    :param _serial_number:  The serial number of this bicoder. \n
    :type _serial_number:   str \n

    :param _device_type:    The type of device the bicoder will be attached to. (Only required for valve bicoders) \n
    :type _device_type:     str \n

    :return:
    """
    if _type is opcodes.single_valve_decoder:
        create_single_valve_bicoder_object(_controller=_controller, _serial_number=_serial_number, _device_type=_device_type)
    elif _type is opcodes.two_valve_decoder:
        create_dual_valve_bicoder_object(_controller=_controller, _serial_number=_serial_number, _device_type=_device_type)
    elif _type is opcodes.four_valve_decoder:
        create_quad_valve_bicoder_object(_controller=_controller, _serial_number=_serial_number, _device_type=_device_type)
    elif _type is opcodes.twelve_valve_decoder:
        create_twelve_valve_bicoder_object(_controller=_controller, _serial_number=_serial_number, _device_type=_device_type)
    elif _type is opcodes.flow_meter:
        create_flow_bicoder_object(_controller=_controller, _serial_number=_serial_number)
    elif _type is opcodes.moisture_sensor:
        create_moisture_bicoder_object(_controller=_controller, _serial_number=_serial_number)
    elif _type is opcodes.temperature_sensor:
        create_temperature_bicoder_object(_controller=_controller, _serial_number=_serial_number)
    elif _type is opcodes.event_switch:
        create_switch_bicoder_object(_controller=_controller, _serial_number=_serial_number)
    elif _type is opcodes.analog_decoder:
        create_analog_bicoder_object(_controller=_controller, _serial_number=_serial_number)
    elif _type is opcodes.alert_relay:
        create_alert_relay_bicoder_object(_controller=_controller, _serial_number=_serial_number)


#################################
def create_single_valve_bicoder_object(_controller, _serial_number, _device_type):
    """
    Create a single valve bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 |
                            common.objects.controllers.bl_sb.Substation \n

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :param _device_type:    The device type that this bicoder will later be attached to. \n
    :type _device_type:     str \n

    :return:
    """
    # TODO do we need to check for proper starts to serial numbers
    # if _serial_number.startswith(("TSD", "TPR", "TMV", "D")):
    new_bicoder = ValveBicoder(_sn=_serial_number, _controller=_controller, _id=_device_type, _decoder_type=opcodes.single_valve_decoder)
    _controller.valve_bicoders[_serial_number] = new_bicoder

    # If creating bicoder for substation, need to update controller's bicoder dictionary with new bicoder also so that
    # we can replace a device's bicoder without the 3200 trying to load the device.
    if _controller.is_substation() and _controller.base_station_3200:
        _controller.base_station_3200.valve_bicoders[_serial_number] = new_bicoder


#################################
def create_dual_valve_bicoder_object(_controller, _serial_number, _device_type):
    """
    Create a dual valve bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 |
                            common.objects.controllers.bl_sb.Substation \n

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :param _device_type:    The device type that this bicoder will later be attached to. \n
    :type _device_type:     str \n

    :return:
    """
    # TODO do we need to check for proper starts to serial numbers
    # _serial_number.startswith(("TVE", "TSE", "E")):
    if _serial_number[6] == "0":
        raise ValueError("Serial Number {0} is not valid for a dual valve bicoder. Cannot end with a '0'".format(
            _serial_number)
        )
    # Gets the first 5 characters of the serial number, then the last 2 characters
    first_half = _serial_number[:5]     # First 5
    int_half = _serial_number[5:]       # Last 2

    for serial_range in range(1, 3):
        # Skip the first iteration of the loop to use initial dual valve decoder serial number before incrementing it
        if serial_range > 1:
            int_half = int(int_half) + 1

            # If last two digits for incrementing start with '0', ie: "01", after incrementing "01", int_half has a
            # value as an integer of 2, length 1, but we need a length of 2, thus add the '0' back to the front for
            # a valid serial. See https://docs.python.org/2/library/string.html#formatspec
            int_half = '{0:0>2}'.format(int_half)

        # New constructed serial number
        new_serial = first_half + str(int_half)

        # New valve bicoder instance
        new_valve_bicoder = ValveBicoder(_sn=new_serial, _controller=_controller, _id=_device_type, _decoder_type=opcodes.two_valve_decoder)

        # Add to controller's dict
        _controller.valve_bicoders[new_serial] = new_valve_bicoder

        # If creating bicoder for substation, need to update controller's bicoder dictionary with new bicoder also so
        # that we can replace a device's bicoder without the 3200 trying to load the device.
        if _controller.is_substation() and _controller.base_station_3200:
            _controller.base_station_3200.valve_bicoders[new_serial] = new_valve_bicoder


#################################
def create_quad_valve_bicoder_object(_controller, _serial_number, _device_type):
    """
    Create a quad valve bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 |
                            common.objects.controllers.bl_sb.Substation \n

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :param _device_type:    The device type that this bicoder will later be attached to. \n
    :type _device_type:     str \n

    :return:
    """
    # TODO do we need to check for proper starts to serial numbers
    # _serial_number.startswith(("TSQ", "Q")):
    if _serial_number[6] == "0":
        raise ValueError("Serial Number {0} is not valid for a quad valve bicoder. Cannot end with a '0'".format(
            _serial_number)
        )
    # Gets the first 5 characters of the serial number, then the last 2 characters
    first_half = _serial_number[:5]     # First 5
    int_half = _serial_number[5:]       # Last 2

    for serial_range in range(1, 5):
        # Skip the first iteration of the loop to use initial quad valve decoder serial number before incrementing it
        if serial_range > 1:
            int_half = int(int_half) + 1

            # If last two digits for incrementing start with '0', ie: "01", after incrementing "01", int_half has a
            # value as an integer of 2, length 1, but we need a length of 2, thus add the '0' back to the front for
            # a valid serial.  See https://docs.python.org/2/library/string.html#formatspec
            int_half = '{0:0>2}'.format(int_half)

        # New constructed serial number
        new_serial = first_half + str(int_half)

        # New valve bicoder instance
        new_valve_bicoder = ValveBicoder(_sn=new_serial, _controller=_controller, _id=_device_type, _decoder_type=opcodes.four_valve_decoder)

        # Add to controller's dict
        _controller.valve_bicoders[new_serial] = new_valve_bicoder

        # If creating bicoder for substation, need to update controller's bicoder dictionary with new bicoder also so
        # that we can replace a device's bicoder without the 3200 trying to load the device.
        if _controller.is_substation() and _controller.base_station_3200:
            _controller.base_station_3200.valve_bicoders[new_serial] = new_valve_bicoder


#################################
def create_twelve_valve_bicoder_object(_controller, _serial_number, _device_type):
    """
    Create a twelve valve bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 |
                            common.objects.controllers.bl_sb.Substation \n

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :param _device_type:    The device type that this bicoder will later be attached to. \n
    :type _device_type:     str \n

    :return:
    """
    # TODO do we need to check for proper starts to serial numbers
    # _serial_number.startswith(("TVA", "TVB", "V")):
    # Gets the first 5 characters of the serial number, then the last 2 characters
    first_half = _serial_number[:5]     # First 5
    int_half = _serial_number[5:]       # Last 2

    for serial_range in range(1, 13):
        # Skip the first iteration of the loop to use initial twelve valve decoder serial number before incrementing it
        if serial_range > 1:
            int_half = int(int_half) + 1

            # If last two digits for incrementing start with '0', ie: "01", after incrementing "01", int_half has a
            # value as an integer of 2, length 1, but we need a length of 2, thus add the '0' back to the front for
            # a valid serial.  See https://docs.python.org/2/library/string.html#formatspec
            int_half = '{0:0>2}'.format(int_half)

        # New constructed serial number
        new_serial = first_half + str(int_half)

        # New valve bicoder instance
        new_valve_bicoder = ValveBicoder(_sn=new_serial, _controller=_controller, _id=_device_type, _decoder_type=opcodes.twelve_valve_decoder)

        # Add to controller's dict
        _controller.valve_bicoders[new_serial] = new_valve_bicoder

        # If creating bicoder for substation, need to update controller's bicoder dictionary with new bicoder also so
        # that we can replace a device's bicoder without the 3200 trying to load the device.
        if _controller.is_substation() and _controller.base_station_3200:
            _controller.base_station_3200.valve_bicoders[new_serial] = new_valve_bicoder


#################################
def create_flow_bicoder_object(_controller, _serial_number):
    """
    Create a flow bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 |
                            common.objects.controllers.bl_sb.Substation \n

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :return:
    """
    new_flow_bicoder = FlowBicoder(_sn=_serial_number, _controller=_controller)
    _controller.flow_bicoders[_serial_number] = new_flow_bicoder

    # If creating bicoder for substation, need to update controller's bicoder dictionary with new bicoder also so that
    # we can replace a device's bicoder without the 3200 trying to load the device.
    if _controller.is_substation() and _controller.base_station_3200:
        _controller.base_station_3200.flow_bicoders[_serial_number] = new_flow_bicoder


#################################
def create_moisture_bicoder_object(_controller, _serial_number):
    """
    Create a moisture bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 |
                            common.objects.controllers.bl_sb.Substation \n

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :return:
    """
    new_moisture_bicoder = MoistureBicoder(_sn=_serial_number, _controller=_controller)
    _controller.moisture_bicoders[_serial_number] = new_moisture_bicoder

    # If creating bicoder for substation, need to update controller's bicoder dictionary with new bicoder also so that
    # we can replace a device's bicoder without the 3200 trying to load the device.
    if _controller.is_substation() and _controller.base_station_3200:
        _controller.base_station_3200.moisture_bicoders[_serial_number] = new_moisture_bicoder


#################################
def create_temperature_bicoder_object(_controller, _serial_number):
    """
    Create a temperature bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 |
                            common.objects.controllers.bl_sb.Substation \n

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :return:
    """
    new_temp_bicoder = TempBicoder(_sn=_serial_number, _controller=_controller)
    _controller.temperature_bicoders[_serial_number] = new_temp_bicoder

    # If creating bicoder for substation, need to update controller's bicoder dictionary with new bicoder also so that
    # we can replace a device's bicoder without the 3200 trying to load the device.
    if _controller.is_substation() and _controller.base_station_3200:
        _controller.base_station_3200.temperature_bicoders[_serial_number] = new_temp_bicoder


#################################
def create_switch_bicoder_object(_controller, _serial_number):
    """
    Create a switch bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 |
                            common.objects.controllers.bl_sb.Substation \n

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :return:
    """
    new_switch_bicoder = SwitchBicoder(_sn=_serial_number, _controller=_controller)
    _controller.switch_bicoders[_serial_number] = new_switch_bicoder

    # If creating bicoder for substation, need to update controller's bicoder dictionary with new bicoder also so that
    # we can replace a device's bicoder without the 3200 trying to load the device.
    if _controller.is_substation() and _controller.base_station_3200:
        _controller.base_station_3200.switch_bicoders[_serial_number] = new_switch_bicoder


#################################
def create_analog_bicoder_object(_controller, _serial_number):
    """
    Create a analog bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 |
                            common.objects.controllers.bl_sb.Substation \n

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :return:
    """
    new_analog_bicoder = AnalogBicoder(_sn=_serial_number, _controller=_controller)
    _controller.analog_bicoders[_serial_number] = new_analog_bicoder

    # If creating bicoder for substation, need to update controller's bicoder dictionary with new bicoder also so that
    # we can replace a device's bicoder without the 3200 trying to load the device.
    if _controller.is_substation() and _controller.base_station_3200:
        _controller.base_station_3200.analog_bicoders[_serial_number] = new_analog_bicoder


#################################
def create_alert_relay_bicoder_object(_controller, _serial_number):
    """
    Create a alert relay bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.bl_10.BaseStation1000 \n

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :return:
    """
    new_alert_bicoder = AlertRelayBicoder(_sn=_serial_number, _controller=_controller)
    _controller.alert_relay_bicoders[_serial_number] = new_alert_bicoder


#################################
def create_zone_objects(controller, zn_ad_range, d1, d2, d4, dd):
    """
    Creates a Zone object for each value in the zone address range. Will only create as many zones as specified
    in the .json file for zones. \n

    :param controller:      The controller that will be passed in to each zone and valve bicoder made by this method. \n
    :type controller:       common.objects.controllers.cn_watering_engine.BaseWateringEngine

    :param zn_ad_range:     The addresses of all zones on this controller \n
    :type zn_ad_range:      list[int] \n

    :param d1:              The serial numbers of all single-valve bicoder. \n
    :type d1:               list[str] \n

    :param d2:              The serial numbers of all dual-valve bicoder. \n
    :type d2:               list[str] \n

    :param d4:              The serial numbers of all quad-valve bicoder. \n
    :type d4:               list[str] \n

    :param dd:              The serial numbers of all twelve-valve bicoder. \n
    :type dd:               list[str] \n
    """

    # These ranges tell us how many of each decoder to create and address
    decoder_range_dict = {
        "D1": range(1, 2),      # 1
        "D2": range(1, 3),      # 2
        "D4": range(1, 5),      # 4
        "DD": range(1, 13)      # 12
    }

    # Get available zone ranges
    zn_ad_range = determine_zone_range_available(zn_ad_range=zn_ad_range)

    # Generate a list of all decoder's combined to for loop through each of them.
    list_of_zn_sn = d1 + d2 + d4 + dd
    # list_of_zn_sn += self.shared_d1 + self.shared_d2 + self.shared_d4 + self.shared_dd

    # is_shared_zone = False

    zn_list_index = 0
    for zn_sn in list_of_zn_sn:
        # Parse everything besides the last two integers, this parsed string remains constant the entire method
        # this return the first 5 characters
        first_half = zn_sn[:5]

        # Gets the int portion of the serial number to increment (the last two digits)
        # this return the last 2 characters
        int_half = zn_sn[5:]

        # ------------------------------------------------------------------------------------------------------
        # Case 1: Single Valve Decoders
        # The "" is a test hack to bypass S/N checking, so we can use any
        # type of decoder as a single-valve.  DO NOT CHECK INTO TRUNK!!!
        if zn_sn.startswith(("TSD", "TPR", "TMV", "D", "K", "H1")) and zn_sn in d1:

            # Make sure that we have zone addresses left in the list to address too
            if zn_list_index < len(zn_ad_range):

                # Check shared single valve decoder serial numbers to see if zone is shared with a Substation.
                # is_shared_zone = zn_sn in self.shared_d1

                # Get the next available zone address from range for addressing
                zn_ad = zn_ad_range[zn_list_index]

                # Create a valve bicoder
                valve_bicoder = controller.valve_bicoders[zn_sn]
                valve_bicoder.ad = zn_ad

                # Create the zone using the valve bicoder
                controller.zones[zn_ad] = Zone(_controller=controller, _address=zn_ad, _valve_bicoder=valve_bicoder)
                zn_list_index += 1

        # ------------------------------------------------------------------------------------------------------
        # Case 2: Dual Valve Decoders
        elif zn_sn.startswith(("TVE", "TSE", "E", "K", "H2")) and zn_sn in d2:

            # Check shared dual valve decoder serial numbers to see if zone is shared with a Substation.
            # is_shared_zone = zn_sn in self.shared_d2
            # For loop to create two serial numbers for every dual valve decoder serial number
            for i in decoder_range_dict["D2"]:

                # Make sure that we have zones addresses left to address to
                if zn_list_index < len(zn_ad_range):
                    # Get the next available zone address from range for addressing
                    zn_ad = zn_ad_range[zn_list_index]

                    # Skip the first iteration of the loop to send the initial dual valve decoder serial number
                    # before incrementing it
                    if i > 1:
                        int_half = int(int_half) + 1

                        # If last two digits for incrementing start with '0', ie: "01", after incrementing "01",
                        # int_half has a value as an integer of 2, length 1, but we need a length of 2, thus add
                        # the '0' back to the front for a valid serial.
                        # See https://docs.python.org/2/library/string.html#formatspec
                        int_half = '{0:0>2}'.format(int_half)

                    zn_sn = first_half + str(int_half)

                    # Create a valve bicoder
                    valve_bicoder = controller.valve_bicoders[zn_sn]
                    valve_bicoder.ad = zn_ad

                    # Create the zone using the valve bicoder
                    controller.zones[zn_ad] = Zone(_controller=controller, _address=zn_ad, _valve_bicoder=valve_bicoder)
                    zn_list_index += 1

        # ------------------------------------------------------------------------------------------------------
        # Case 3: Quad Valve Decoders
        elif zn_sn.startswith(("TSQ", "Q", "H4")) and zn_sn in d4:

            # Check shared quad valve decoder serial numbers to see if zone is shared with a Substation.
            # is_shared_zone = zn_sn in self.shared_d4

            # For loop to create four serial numbers for every quad valve decoder serial number
            for i in decoder_range_dict["D4"]:

                # Make sure that we have zones addresses left to address to
                if zn_list_index < len(zn_ad_range):
                    # Get the next available zone address from range for addressing
                    zn_ad = zn_ad_range[zn_list_index]

                    # Skip the first iteration of the loop to send the initial dual valve decoder serial number
                    # before incrementing it
                    if i > 1:
                        int_half = int(int_half) + 1

                        # If last two digits for incrementing start with '0', ie: "01", after incrementing "01",
                        # int_half has a value as an integer of 2, length 1, but we need a length of 2, thus add
                        # the '0' back to the front for a valid serial.
                        # See https://docs.python.org/2/library/string.html#formatspec
                        int_half = '{0:0>2}'.format(int_half)

                    zn_sn = first_half + str(int_half)

                    # Create a valve bicoder
                    valve_bicoder = controller.valve_bicoders[zn_sn]
                    valve_bicoder.ad = zn_ad

                    # Create the zone using the valve bicoder
                    controller.zones[zn_ad] = Zone(_controller=controller, _address=zn_ad, _valve_bicoder=valve_bicoder)
                    zn_list_index += 1

        # ------------------------------------------------------------------------------------------------------
        # Case 4: Twelve Valve Decoders
        elif zn_sn.startswith(("TVA", "TVB", "V", "TVE", "HA")) and zn_sn in dd:

            # Check shared twelve valve decoder serial numbers to see if zone is shared with a Substation.
            # is_shared_zone = zn_sn in self.shared_dd

            # For loop to create twelve serial numbers for every twelve valve decoder serial number
            for i in decoder_range_dict["DD"]:

                # Make sure that we have zones addresses left to address to
                if zn_list_index < len(zn_ad_range):
                    # Get the next available zone address from range for addressing
                    zn_ad = zn_ad_range[zn_list_index]

                    # Skip the first iteration of the loop to send the initial dual valve decoder serial number
                    # before incrementing it
                    if i > 1:
                        int_half = int(int_half) + 1

                        # If last two digits for incrementing start with '0', ie: "01", after incrementing "01",
                        # int_half has a value as an integer of 2, length 1, but we need a length of 2, thus add
                        # the '0' back to the front for a valid serial
                        # See https://docs.python.org/2/library/string.html#formatspec
                        int_half = '{0:0>2}'.format(int_half)

                    zn_sn = first_half + str(int_half)

                    # Create a valve bicoder
                    valve_bicoder = controller.valve_bicoders[zn_sn]
                    valve_bicoder.ad = zn_ad

                    # Create the zone using the valve bicoder
                    controller.zones[zn_ad] = Zone(_controller=controller, _address=zn_ad, _valve_bicoder=valve_bicoder)
                    zn_list_index += 1
        else:
            e_msg = "Serial number {0} not allowed.".format(zn_sn)
            raise ValueError(e_msg)


#################################
def determine_zone_range_available(zn_ad_range):
    """
    Takes the list of zone range start/end values from the json file and creates a range of zone addresses based
    on the values from the json file. Expects the zone range list in the json file either be empty or an even
    length \n

    :param zn_ad_range:     The addresses of all zones on this controller \n
    :type zn_ad_range:      list[int] \n

    :return:
    """

    # Makes sure that we have an even number of start/stop values in the .json file
    if len(zn_ad_range) % 2 != 0:
        e_msg = "Invalid list of zone ranges provided in the data .json file for the current test. Valid Zone " \
                "ranges must have 2 numbers for each range. Correct format in .json file should look like: \n" + \
                "'range': [1, 200] -> says to create a range from 1 to 200. \n" + \
                "'range': [1, 5, 10, 20] -> says to create a range from 1 to 5 and 10 to 20 with a gap between 5 " \
                "and 10."
        raise ValueError(e_msg)

    # Convert zn_ad_range into a list of 2-tuples, e.g. [1,4,8,10,14,18] -> [(1,4),(8,10),(14,18)],
    # use those tuples to construct a list of lists where each list is all the numbers in each range, and then
    # concatenate the lists together into a single list
    it = iter(zn_ad_range)        # Get an iterator for the zn_ad_range list, e.g. [1,4,8,10,14,18]
    range_tuples = zip(it, it)    # Convert to list of 2-tuples  [(1,4),(8,10),(14,18)]
    # Use list comprehension to create a list of lists, where each individual list is a list of the numbers bounded
    # by the numbers in the 2-tuples (inclusive) [[1,2,3,4],[8,9,10],[14,15,16,17,18]]
    list_of_ranges = [range(x[0], x[1]+1) for x in range_tuples]
    # Smoosh all the individual lists together into a single list [1,2,3,4,8,9,10,14,15,16,17,18]
    ad_range = sum(list_of_ranges, [])
    return ad_range


#################################
def create_moisture_sensor_objects(controller, address_range, serial_numbers):
    """
    Create all moisture sensor objects for a given range and serial numbers. \n

    :param controller:          The controller that will be passed in to each moisture sensor and moisture bicoder made
                                by this method. \n
    :type controller:           common.objects.controllers.cn_watering_engine.BaseWateringEngine

    :param address_range:       The addresses of all moisture sensors on this controller \n
    :type address_range:        list[int] \n

    :param serial_numbers:      The serial numbers of all moisture sensor bicoders on this controller \n
    :type serial_numbers:       list[str] \n

    :return:
    """
    for index, ms_ad in enumerate(address_range):
        # 'enumerate' makes ms_address start at 0 (off by 1 problem) so we add 1 to compensate because we can't
        # have a starting address of 0.
        serial = serial_numbers[index]

        # Create a moisture bicoder
        moisture_bicoder = controller.moisture_bicoders[serial]

        # Here we are creating an integer key value to lookup each moisture sensor
        controller.moisture_sensors[ms_ad] = MoistureSensor(_controller=controller,
                                                            _moisture_bicoder=moisture_bicoder,
                                                            _address=ms_ad)


#################################
def create_master_valve_objects(controller, address_range, serial_numbers):
    """
    Create all master valve objects for a given range and serial numbers. \n

    :param controller:          The controller that will be passed in to each master valve and valve bicoder made by
                                this method. \n
    :type controller:           common.objects.controllers.cn_watering_engine.BaseWateringEngine

    :param address_range:       The addresses of all master valves on this controller \n
    :type address_range:        list[int] \n

    :param serial_numbers:      The addresses of all master valves on this controller \n
    :type serial_numbers:       list[str] \n

    :return:
    """
    for index, mv_ad in enumerate(address_range):
        # 'enumerate' makes mv_address start at 0 (off by 1 problem) so we add 1 to compensate because we can't
        # have a starting address of 0.
        serial = serial_numbers[index]

        # Create a valve bicoder
        valve_bicoder = controller.valve_bicoders[serial]

        # Here we are creating an integer key value to lookup each master valve
        controller.master_valves[mv_ad] = MasterValve(_controller=controller,
                                                      _valve_bicoder=valve_bicoder,
                                                      _address=mv_ad)


#################################
def create_pump_objects(controller, address_range, serial_numbers):
    """
    Create all pump objects for a given range and serial numbers. \n

    :param controller:          The controller that will be passed in to each pump and pump bicoder made by this
                                method. \n
    :type controller:           common.objects.controllers.cn_watering_engine.BaseWateringEngine

    :param address_range:       The addresses of all pumps on this controller \n
    :type address_range:        list[int] \n

    :param serial_numbers:      The addresses of all pumps on this controller \n
    :type serial_numbers:       list[str] \n

    :return:
    """
    for index, pm_ad in enumerate(address_range):
        # 'enumerate' makes pm_address start at 0 (off by 1 problem) so we add 1 to compensate because we can't
        # have a starting address of 0.
        serial = serial_numbers[index]

        # Create a pump bicoder
        # pump_bicoder = PumpBicoder(_controller=controller, _sn=serial, _address=pm_ad)
        pump_bicoder = controller.valve_bicoders[serial]

        # Here we are creating an integer key value to lookup each pump
        controller.pumps[pm_ad] = Pump(_controller=controller, _pump_bicoder=pump_bicoder, _address=pm_ad)


#################################
def create_flow_meter_objects(controller, address_range, serial_numbers):
    """
    Create all flow meter objects for a given range and serial numbers. \n

    :param controller:          The controller that will be passed in to each flow meter and flow bicoder made by this
                                method. \n
    :type controller:           common.objects.controllers.cn_watering_engine.BaseWateringEngine

    :param address_range:       The addresses of all flow meters on this controller \n
    :type address_range:        list[int] \n

    :param serial_numbers:      The addresses of all flow meters on this controller \n
    :type serial_numbers:       list[str] \n

    :return:
    """
    for index, fm_ad in enumerate(address_range):
        # 'enumerate' makes fm_address start at 0 (off by 1 problem) so we add 1 to compensate because we can't
        # have a starting address of 0.
        serial = serial_numbers[index]

        # Create a flow bicoder
        flow_bicoder = controller.flow_bicoders[serial]

        # Here we are creating an integer key value to lookup each flow meter
        controller.flow_meters[fm_ad] = FlowMeter(_controller=controller, _flow_bicoder=flow_bicoder, _address=fm_ad)


#################################
def create_event_switch_objects(controller, address_range, serial_numbers):
    """
    Create all event switches objects for a given range and serial numbers. \n

    :param controller:          The controller that will be passed in to each event switch and switch bicoder made by
                                this method. \n
    :type controller:           common.objects.controllers.cn_watering_engine.BaseWateringEngine

    :param address_range:       The addresses of all event switches on this controller \n
    :type address_range:        list[int] \n

    :param serial_numbers:      The addresses of all event switches on this controller \n
    :type serial_numbers:       list[str] \n

    :return:
    """
    for index, sw_ad in enumerate(address_range):
        # 'enumerate' makes sw_address start at 0 (off by 1 problem) so we add 1 to compensate because we can't
        # have a starting address of 0.
        serial = serial_numbers[index]

        # Create a switch bicoder
        event_switch = controller.switch_bicoders[serial]

        # Here we are creating an integer key value to lookup each event switch
        controller.event_switches[sw_ad] = EventSwitch(_controller=controller,
                                                       _switch_bicoder=event_switch,
                                                       _address=sw_ad)


#################################
def create_temperature_sensor_objects(controller, address_range, serial_numbers):
    """
    Create all temperature sensor objects for a given range and serial numbers. \n

    :param controller:          The controller that will be passed in to each temperature sensor and temperature bicoder
                                made by this method. \n
    :type controller:           common.objects.controllers.cn_watering_engine.BaseWateringEngine

    :param address_range:       The addresses of all temperature sensors on this controller \n
    :type address_range:        list[int] \n

    :param serial_numbers:      The addresses of all temperature sensors on this controller \n
    :type serial_numbers:       list[str] \n

    :return:
    """
    for index, ts_ad in enumerate(address_range):
        # 'enumerate' makes ts_address start at 0 (off by 1 problem) so we add 1 to compensate because we can't
        # have a starting address of 0.
        serial = serial_numbers[index]

        # Create a temperature bicoder
        temp_bicoder = controller.temperature_bicoders[serial]

        # Here we are creating an integer key value to lookup each temperature sensor
        controller.temperature_sensors[ts_ad] = TemperatureSensor(_controller=controller,
                                                                  _temp_bicoder=temp_bicoder,
                                                                  _address=ts_ad)


#################################
def create_pressure_sensor_objects(controller, address_range, serial_numbers):
    """
    Create all pressure sensor objects for a given range and serial numbers. \n

    :param controller:          The controller that will be passed in to each pressure sensor and analog bicoder made by
                                this method. \n
    :type controller:           common.objects.controllers.cn_watering_engine.BaseWateringEngine

    :param address_range:       The addresses of all pressure sensors on this controller \n
    :type address_range:        list[int] \n

    :param serial_numbers:      The addresses of all pressure sensors on this controller \n
    :type serial_numbers:       list[str] \n

    :return:
    """
    for index, ab_ad in enumerate(address_range):
        # 'enumerate' makes ps_address start at 0 (off by 1 problem) so we add 1 to compensate because we can't
        # have a starting address of 0.
        serial = serial_numbers[index]

        # Create analog bicoder
        analog_bicoder = controller.analog_bicoders[serial]

        # Here we are creating an integer key value to lookup each pressure sensor
        controller.pressure_sensors[ab_ad] = PressureSensor(_controller=controller,
                                                            _analog_bicoder=analog_bicoder,
                                                            _address=ab_ad)


#################################
def create_alert_relay_objects(controller, address_range, serial_numbers):
    """
    Create all flow meter objects for a given range and serial numbers. \n

    :param controller:          The controller that will be passed in to each alert relay made by this method. \n
    :type controller:           common.objects.controllers.bl_10.BaseStation1000 \n

    :param address_range:       The addresses of all alert relay on this controller \n
    :type address_range:        list[int] \n

    :param serial_numbers:      The addresses of all alert relay on this controller \n
    :type serial_numbers:       list[str] \n

    :return:
    """
    for index, ar_ad in enumerate(address_range):
        # 'enumerate' makes ar_address start at 0 (off by 1 problem) so we add 1 to compensate because we can't
        # have a starting address of 0.
        serial = serial_numbers[index]

        # Create a alert relay bicoder
        alert_relay_bicoder = controller.alert_relay_bicoders[serial]

        # Here we are creating an integer key value to lookup each alert relay
        controller.alert_relays[ar_ad] = AlertRelay(_controller=controller,
                                                    _alert_relay_bicoder=alert_relay_bicoder,
                                                    _address=ar_ad)


#################################
def create_water_source_object(controller, _water_source_address):
    """
    Create a water source object with the specified address. \n

    :param controller:              The controller that will be passed in to each water source made by this method. \n
    :type controller:               common.objects.controllers.bl_32.BaseStation3200 |
                                    common.objects.controllers.bl_10.BaseStation1000 \n

    :param _water_source_address:   The address of the pressure sensor on this controller \n
    :type _water_source_address:    int \n
    """
    # Create a water and store into the dictionary using the address as a lookup key
    controller.water_sources[_water_source_address] = WaterSource(_controller=controller, _ad=_water_source_address)


#################################
def create_point_of_control_object(controller, _point_of_control_address):
    """
    Create a point of control object with the specified address. \n

    :param controller:                  The controller that will be passed in to each point of control made by this
                                        method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param _point_of_control_address:   The address of the point of control on this controller \n
    :type _point_of_control_address:    int \n
    """
    # Create a Point Of Control object and store into the dictionary using the address as a lookup key
    controller.points_of_control[_point_of_control_address] = PointOfControl(_controller=controller,
                                                                             _ad=_point_of_control_address)


#################################
def create_1000_point_of_control_object(controller, _point_of_control_address):
    """
    Create a point of control object with the specified address. \n

    :param controller:                  The controller that will be passed in to each point of control made by this
                                        method. \n
    :type controller:                   common.objects.controllers.bl_10.BaseStation1000 \n

    :param _point_of_control_address:   The address of the point of control on this controller \n
    :type _point_of_control_address:    int \n
    """
    # Create a Point Of Control object and store into the dictionary using the address as a lookup key
    controller.points_of_control[_point_of_control_address] = POC1000(_controller=controller,
                                                                      _ad=_point_of_control_address)


#################################
def create_mainline_object(controller, _mainline_address):
    """
    Create a mainline object with the specified address. \n

    :param controller:          The controller that will be passed in to each mainline made by this method. \n
    :type controller:           common.objects.controllers.cn_watering_engine.BaseWateringEngine

    :param _mainline_address:   The addresses of the mainline on this controller \n
    :type _mainline_address:    int \n
    """
    # Create a Mainline object and store into the dictionary using the address as a lookup key
    controller.mainlines[_mainline_address] = Mainline(_controller=controller, _ad=_mainline_address)


#################################
def create_3200_program_object(controller, program_address):
    """
    Create a 3200 program object with the specified address. \n

    :param controller:      The controller that will be passed in to each program made by this method. \n
    :type controller:       common.objects.controllers.cn_watering_engine.BaseWateringEngine

    :param program_address: The addresses of the program on this controller \n
    :type program_address:  int \n
    """
    controller.programs[program_address] = PG3200(_controller=controller, _ad=program_address)


#################################
def create_1000_program_object(controller, program_address):
    """
    Create a 1000 program object with the specified address. \n

    :param controller:      The controller that will be passed in to each program made by this method. \n
    :type controller:       common.objects.controllers.cn_watering_engine.BaseWateringEngine

    :param program_address: The addresses of the program on this controller \n
    :type program_address:  int \n
    """
    controller.programs[program_address] = PG1000(_controller=controller, _ad=program_address)


#################################
def create_zone_program_object(controller, program_address, zone_address):
    """
    Create a 1000 program object with the specified address. \n

    :param controller:      The controller that will be passed in to each zone program made by this method. \n
    :type controller:       common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address: The addresses of the program on this controller \n
    :type program_address:  int \n

    :param zone_address:    The addresses of the program on this controller \n
    :type zone_address:     int \n
    """
    controller.programs[program_address].zone_programs[zone_address] = ZoneProgram(_controller=controller,
                                                                                   zone_ad=zone_address,
                                                                                   prog_ad=program_address)


#################################
def create_date_time_start_condition_object(controller, program_address, condition_address):
    """
    Create a 3200 program start condition object with the specified address. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address:             The address of the program on this controller \n
    :type program_address:              int \n

    :param condition_address:           The address of the program on this controller \n
    :type condition_address:            int \n
    """
    controller.programs[program_address].date_time_start_conditions[condition_address] = DateStartCondition1000(
        _controller=controller,
        _program_ad=program_address,
        _condition_address=condition_address
    )


#################################
def create_moisture_start_condition_object(controller, program_address, moisture_sensor_address):
    """
    Create a 3200 program start condition object with the specified address. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address:             The address of the program on this controller \n
    :type program_address:              int \n

    :param moisture_sensor_address:     The address of the moisture sensor on this controller \n
    :type moisture_sensor_address:      int \n
    """
    controller.programs[program_address].moisture_start_conditions[moisture_sensor_address] = MoistureStartCondition(
        _controller=controller,
        _program_ad=program_address,
        _device_serial=controller.moisture_sensors[moisture_sensor_address].sn
    )


#################################
def create_moisture_stop_condition_object(controller, program_address, moisture_sensor_address):
    """
    Create a 3200 program stop condition object with the specified address. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address:             The addresses of the program on this controller \n
    :type program_address:              int \n

    :param moisture_sensor_address:     The addresses of the program on this controller \n
    :type moisture_sensor_address:      int \n
    """
    controller.programs[program_address].moisture_stop_conditions[moisture_sensor_address] = MoistureStopCondition(
        _controller=controller,
        _program_ad=program_address,
        _device_serial=controller.moisture_sensors[moisture_sensor_address].sn
    )


#################################
def create_moisture_stop_all_condition_object(controller, moisture_sensor_address):
    """
    Create a 3200 program stop all condition object. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param moisture_sensor_address:     The addresses of the program on this controller \n
    :type moisture_sensor_address:      int \n
    """
    controller.moisture_stop_conditions[moisture_sensor_address] = MoistureStopCondition(
        _controller=controller,
        _program_ad=opcodes.all,
        _device_serial=controller.moisture_sensors[moisture_sensor_address].sn
    )


#################################
def create_moisture_pause_condition_object(controller, program_address, moisture_sensor_address):
    """
    Create a 3200 program pause condition object with the specified address. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address:             The addresses of the program on this controller \n
    :type program_address:              int \n

    :param moisture_sensor_address:     The addresses of the program on this controller \n
    :type moisture_sensor_address:      int \n
    """
    controller.programs[program_address].moisture_pause_conditions[moisture_sensor_address] = MoisturePauseCondition(
        _controller=controller,
        _program_ad=program_address,
        _device_serial=controller.moisture_sensors[moisture_sensor_address].sn
    )


#################################
def create_moisture_pause_all_condition_object(controller, moisture_sensor_address):
    """
    Create a 3200 pause condition object with the specified address. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param moisture_sensor_address:     The addresses of the program on this controller \n
    :type moisture_sensor_address:      int \n
    """
    controller.moisture_pause_conditions[moisture_sensor_address] = MoisturePauseCondition(
        _controller=controller,
        _program_ad=opcodes.all,
        _device_serial=controller.moisture_sensors[moisture_sensor_address].sn
    )


#################################
def create_temperature_start_condition_object(controller, program_address, temperature_sensor_address):
    """
    Create a 3200 program start condition object with the specified temperature sensor. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address:             The addresses of the program on this controller \n
    :type program_address:              int \n

    :param temperature_sensor_address:     The addresses of the program on this controller \n
    :type temperature_sensor_address:      int \n
    """
    controller.programs[program_address].temperature_start_conditions[temperature_sensor_address] = TemperatureStartCondition(
        _controller=controller,
        _program_ad=program_address,
        _device_serial=controller.temperature_sensors[temperature_sensor_address].sn
    )


#################################
def create_temperature_stop_condition_object(controller, program_address, temperature_sensor_address):
    """
    Create a 3200 program stop condition object with the specified temperature sensor. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address:             The addresses of the program on this controller \n
    :type program_address:              int \n

    :param temperature_sensor_address:     The addresses of the program on this controller \n
    :type temperature_sensor_address:      int \n
    """
    controller.programs[program_address].temperature_stop_conditions[temperature_sensor_address] = TemperatureStopCondition(
        _controller=controller,
        _program_ad=program_address,
        _device_serial=controller.temperature_sensors[temperature_sensor_address].sn
    )


#################################
def create_temperature_stop_all_condition_object(controller, temperature_sensor_address):
    """
    Create a 3200 stop condition object with the specified temperature sensor. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param temperature_sensor_address:     The addresses of the program on this controller \n
    :type temperature_sensor_address:      int \n
    """
    controller.temperature_stop_conditions[temperature_sensor_address] = TemperatureStopCondition(
        _controller=controller,
        _program_ad=opcodes.all,
        _device_serial=controller.temperature_sensors[temperature_sensor_address].sn
    )


#################################
def create_temperature_pause_condition_object(controller, program_address, temperature_sensor_address):
    """
    Create a 3200 program pause condition object with the specified temperature sensor. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address:             The addresses of the program on this controller \n
    :type program_address:              int \n

    :param temperature_sensor_address:     The addresses of the program on this controller \n
    :type temperature_sensor_address:      int \n
    """
    controller.programs[program_address].temperature_pause_conditions[temperature_sensor_address] = TemperaturePauseCondition(
        _controller=controller,
        _program_ad=program_address,
        _device_serial=controller.temperature_sensors[temperature_sensor_address].sn
    )


#################################
def create_temperature_pause_all_condition_object(controller, temperature_sensor_address):
    """
    Create a 3200 program pause condition object with the specified temperature sensor. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param temperature_sensor_address:     The addresses of the program on this controller \n
    :type temperature_sensor_address:      int \n
    """
    controller.temperature_pause_conditions[temperature_sensor_address] = TemperaturePauseCondition(
        _controller=controller,
        _program_ad=opcodes.all,
        _device_serial=controller.temperature_sensors[temperature_sensor_address].sn
    )


#################################
def create_pressure_start_condition_object(controller, program_address, pressure_sensor_address):
    """
    Create a 3200 program start condition object with the specified address. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address:             The addresses of the program on this controller \n
    :type program_address:              int \n

    :param pressure_sensor_address:     The addresses of the program on this controller \n
    :type pressure_sensor_address:      int \n
    """
    controller.programs[program_address].pressure_start_conditions[pressure_sensor_address] = PressureStartCondition(
        _controller=controller,
        _program_ad=program_address,
        _device_serial=controller.pressure_sensors[pressure_sensor_address].sn
    )


#################################
def create_pressure_stop_condition_object(controller, program_address, pressure_sensor_address):
    """
    Create a 3200 program stop condition object with the specified address. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address:             The addresses of the program on this controller \n
    :type program_address:              int \n

    :param pressure_sensor_address:     The addresses of the program on this controller \n
    :type pressure_sensor_address:      int \n
    """
    controller.programs[program_address].pressure_stop_conditions[pressure_sensor_address] = PressureStopCondition(
        _controller=controller,
        _program_ad=program_address,
        _device_serial=controller.pressure_sensors[pressure_sensor_address].sn
    )


#################################
def create_pressure_stop_all_condition_object(controller, pressure_sensor_address):
    """
    Create a 3200 stop condition object with the specified pressure sensor. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param pressure_sensor_address:     The addresses of the program on this controller \n
    :type pressure_sensor_address:      int \n
    """
    controller.pressure_stop_conditions[pressure_sensor_address] = PressureStopCondition(
        _controller=controller,
        _program_ad=opcodes.all,
        _device_serial=controller.pressure_sensors[pressure_sensor_address].sn
    )


#################################
def create_pressure_pause_condition_object(controller, program_address, pressure_sensor_address):
    """
    Create a 3200 program pause condition object with the specified address. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address:             The addresses of the program on this controller \n
    :type program_address:              int \n

    :param pressure_sensor_address:     The addresses of the program on this controller \n
    :type pressure_sensor_address:      int \n
    """
    controller.programs[program_address].pressure_pause_conditions[pressure_sensor_address] = PressurePauseCondition(
        _controller=controller,
        _program_ad=program_address,
        _device_serial=controller.pressure_sensors[pressure_sensor_address].sn
    )


#################################
def create_pressure_pause_all_condition_object(controller, pressure_sensor_address):
    """
    Create a 3200 program pause condition object with the specified address. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param pressure_sensor_address:     The addresses of the program on this controller \n
    :type pressure_sensor_address:      int \n
    """
    controller.pressure_pause_conditions[pressure_sensor_address] = PressurePauseCondition(
        _controller=controller,
        _program_ad=opcodes.all,
        _device_serial=controller.pressure_sensors[pressure_sensor_address].sn
    )


#################################
def create_switch_start_condition_object(controller, program_address, event_switch_address):
    """
    Create a 3200 program start condition object with the specified address. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address:             The addresses of the program on this controller \n
    :type program_address:              int \n

    :param event_switch_address:     The addresses of the program on this controller \n
    :type event_switch_address:      int \n
    """
    controller.programs[program_address].event_switch_start_conditions[event_switch_address] = SwitchStartCondition(
        _controller=controller,
        _program_ad=program_address,
        _device_serial=controller.event_switches[event_switch_address].sn
    )


#################################
def create_switch_stop_condition_object(controller, program_address, event_switch_address):
    """
    Create a 3200 program stop condition object with the specified address. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address:             The addresses of the program on this controller \n
    :type program_address:              int \n

    :param event_switch_address:     The addresses of the program on this controller \n
    :type event_switch_address:      int \n
    """
    controller.programs[program_address].event_switch_stop_conditions[event_switch_address] = SwitchStopCondition(
        _controller=controller,
        _program_ad=program_address,
        _device_serial=controller.event_switches[event_switch_address].sn
    )


#################################
def create_switch_stop_all_condition_object(controller, event_switch_address):
    """
    Create a 3200 stop condition object with the specified event switch. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param event_switch_address:     The addresses of the program on this controller \n
    :type event_switch_address:      int \n
    """
    controller.event_switch_stop_conditions[event_switch_address] = SwitchStopCondition(
        _controller=controller,
        _program_ad=opcodes.all,
        _device_serial=controller.event_switches[event_switch_address].sn
    )


#################################
def create_switch_pause_condition_object(controller, program_address, event_switch_address):
    """
    Create a 3200 program pause condition object with the specified event switch. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param program_address:             The addresses of the program on this controller \n
    :type program_address:              int \n

    :param event_switch_address:     The addresses of the program on this controller \n
    :type event_switch_address:      int \n
    """
    controller.programs[program_address].event_switch_pause_conditions[event_switch_address] = SwitchPauseCondition(
        _controller=controller,
        _program_ad=program_address,
        _device_serial=controller.event_switches[event_switch_address].sn
    )


#################################
def create_switch_pause_all_condition_object(controller, event_switch_address):
    """
    Create a 3200 pause condition object with the specified event switch. \n

    :param controller:                  The controller that will be passed in to each condition made by this method. \n
    :type controller:                   common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

    :param event_switch_address:     The addresses of the program on this controller \n
    :type event_switch_address:      int \n
    """
    controller.event_switch_pause_conditions[event_switch_address] = SwitchPauseCondition(
        _controller=controller,
        _program_ad=opcodes.all,
        _device_serial=controller.event_switches[event_switch_address].sn
    )


#################################
def create_switch_empty_condition_object(controller, water_source_address, event_switch_address, empty_address):
    """

    3200 Water Source Empty Condition Constructor. \n
    :param controller:          The controller that will be passed in to empty condition made by this method. \n
    :type controller:           common.objects.controllers.bl_32.BaseStation3200

    :param water_source_address:             The addresses of the water source on this controller \n
    :type water_source_address:              int \n

    :param event_switch_address:     The addresses of the program on this controller \n
    :type event_switch_address:      int \n

    :param empty_address:               The addresses of the empty condition on the water source \n
    :type empty_address:                int \n
    """
    controller.water_sources[water_source_address].switch_empty_conditions[event_switch_address] = SwitchEmptyCondition(
        _controller=controller,
        _empty_condition_address=empty_address,
        _water_source_address=water_source_address,
        _device_serial_number=controller.event_switches[event_switch_address].sn
    )


#################################
def create_pressure_empty_condition_object(controller, water_source_address, pressure_sensor_address, empty_address):
    """

    3200 Water Source Empty Condition Constructor. \n
    :param controller:          The controller that will be passed in to empty condition made by this method. \n
    :type controller:           common.objects.controllers.bl_32.BaseStation3200

    :param water_source_address:             The addresses of the water source on this controller \n
    :type water_source_address:              int \n

    :param pressure_sensor_address:     The addresses of the program on this controller \n
    :type pressure_sensor_address:      int \n

    :param empty_address:               The addresses of the empty condition on the water source \n
    :type empty_address:                int \n
    """
    controller.water_sources[water_source_address].pressure_empty_conditions[pressure_sensor_address] = PressureEmptyCondition(
        _controller=controller,
        _empty_condition_address=empty_address,
        _water_source_address=water_source_address,
        _device_serial_number=controller.pressure_sensors[pressure_sensor_address].sn
    )


#################################
def create_moisture_empty_condition_object(controller, water_source_address, moisture_sensor_address, empty_address):
    """

    3200 Water Source Empty Condition Constructor. \n
    :param controller:          The controller that will be passed in to empty condition made by this method. \n
    :type controller:           common.objects.controllers.bl_32.BaseStation3200

    :param water_source_address:        The addresses of the water source on this controller \n
    :type water_source_address:         int \n

    :param moisture_sensor_address:     The addresses of the program on this controller \n
    :type moisture_sensor_address:      int \n

    :param empty_address:               The addresses of the empty condition on the water source \n
    :type empty_address:                int \n
    """
    controller.water_sources[water_source_address].moisture_empty_conditions[moisture_sensor_address] = MoistureEmptyCondition(
        _controller=controller,
        _empty_condition_address=empty_address,
        _water_source_address=water_source_address,
        _device_serial_number=controller.moisture_sensors[moisture_sensor_address].sn
    )


#################################
def create_basemanager_connection_object(controller, url, fixed_ip):
    """

    3200 Water Source Empty Condition Constructor. \n
    :param controller:  The controller that will be passed in to empty condition made by this method. \n
    :type controller:   common.objects.base_classes.controller.BaseController

    :param url:        The DNS url of the basemanager server you want to connect to. \n
    :type url:          str \n

    :param fixed_ip:    The fixed ip of the basemanager server you want to connect to. \n
    :type fixed_ip:     str \n
    """
    controller.basemanager_connection[1] = BaseManagerConnection(
        _controller=controller,
        _json_url=url,
        _json_fixed_ip=fixed_ip
    )


#################################
def create_valve_decoder_serial_numbers(serial_list, serial_type):
    """
    :param serial_list: Serial number to expand.
    :type serial_list: list[str]

    :param serial_type: 'DUAL' | 'QUAD' | 'TWELVE' | 'TWENTYFOUR' | 'FORTYEIGHT'
    :type serial_type: str

    :return: List of serial numbers generated from the Serial and Serial Type
    :rtype: list[str]
    """
    number_of_sn_to_create = {
        'DUAL': 2,
        'QUAD': 4,
        'TWELVE': 12,
        'TWENTYFOUR': 24,
        'FORTYEIGHT': 48
    }
    list_of_serial_numbers_generated = []

    # For each serial in the list
    for serial in serial_list:

        # Parse everything besides the last two integers, this parsed string remains constant the entire method
        # this return the first 5 characters
        first_half = serial[:5]

        # Gets the int portion of the serial number to increment (the last two digits)
        # this return the last 2 characters
        int_half = int(serial[5:])

        number_of_serial_numbers_to_create = number_of_sn_to_create[serial_type]
        for number_to_create in range(0, number_of_serial_numbers_to_create):

            if number_to_create > 0:
                int_half += 1

            int_half_as_str = str(int_half)

            # Check if `int_half` is a single digit, if so, add a "0" before to end as "07"
            # See https://docs.python.org/2/library/string.html#formatspec
            int_half_as_str = '{0:0>2}'.format(int_half_as_str)

            # Combine serial number back to a 7 character string value
            serial_to_add = first_half + int_half_as_str
            list_of_serial_numbers_generated.append(serial_to_add)

    return list_of_serial_numbers_generated

########################################################################################################################
#                                                                                                                      #
#                        Create Simulated biCoder Objects                                                              #
#                                                                                                                      #
########################################################################################################################


#################################
def create_simulated_bicoder_object(_controller, _type, _serial_number, _device_type=None):
    """
    Create all moisture sensor objects for a given range and serial numbers. \n

    :param _controller:     The controller that will be passed in to each moisture sensor and moisture bicoder made
                            by this method. \n
    :type _controller:      common.objects.controllers.tw_sim.TWSimulator
    :param _type:           Type of bicoder. (ex: 'd1', 'd2', 'ms', 'fm') \n
    :type _type:            str \n

    :param _serial_number:  The serial number of this bicoder. \n
    :type _serial_number:   str \n

    :param _device_type:    The type of device the bicoder will be attached to. (Only required for valve bicoders) \n
    :type _device_type:     str \n

    :return:
    """
    if _type is opcodes.single_valve_decoder:
        create_simulated_single_valve_bicoder_object(_controller=_controller, _serial_number=_serial_number,
                                                     _device_type=_device_type)
    elif _type is opcodes.two_valve_decoder:
        create_simulated_dual_valve_bicoder_object(_controller=_controller, _serial_number=_serial_number,
                                                   _device_type=_device_type)
    elif _type is opcodes.four_valve_decoder:
        create_simulated_quad_valve_bicoder_object(_controller=_controller, _serial_number=_serial_number,
                                                   _device_type=_device_type)
    elif _type is opcodes.twelve_valve_decoder:
        create_simulated_twelve_valve_bicoder_object(_controller=_controller, _serial_number=_serial_number,
                                                     _device_type=_device_type)
    elif _type is opcodes.flow_meter:
        create_simulated_flow_bicoder_object(_controller=_controller, _serial_number=_serial_number)
    elif _type is opcodes.moisture_sensor:
        create_simulated_moisture_bicoder_object(_controller=_controller, _serial_number=_serial_number)
    elif _type is opcodes.temperature_sensor:
        create_simulated_temperature_bicoder_object(_controller=_controller, _serial_number=_serial_number)
    elif _type is opcodes.event_switch:
        create_simulated_switch_bicoder_object(_controller=_controller, _serial_number=_serial_number)
    elif _type is opcodes.analog_decoder:
        create_simulated_analog_bicoder_object(_controller=_controller, _serial_number=_serial_number)


#################################
def create_simulated_single_valve_bicoder_object(_controller, _serial_number, _device_type):
    """
    Create a single valve bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.tw_sim.TWSimulator

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :param _device_type:    The device type that this bicoder will later be attached to. \n
    :type _device_type:     str \n

    :return:
    """
    # TODO do we need to check for proper starts to serial numbers
    # if _serial_number.startswith(("TSD", "TPR", "TMV", "D")):
    new_bicoder = SimulatedValveBicoder(_sn=_serial_number, _controller=_controller, _id=_device_type,
                                        _decoder_type=opcodes.single_valve_decoder)
    _controller.simulated_valve_bicoders[_serial_number] = new_bicoder


#################################
def create_simulated_dual_valve_bicoder_object(_controller, _serial_number, _device_type):
    """
    Create a dual valve bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.tw_sim.TWSimulator

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :param _device_type:    The device type that this bicoder will later be attached to. \n
    :type _device_type:     str \n

    :return:
    """
    # TODO do we need to check for proper starts to serial numbers
    # _serial_number.startswith(("TVE", "TSE", "E")):
    if _serial_number[6] == "0":
        raise ValueError("Serial Number {0} is not valid for a dual valve bicoder. Cannot end with a '0'".format(
            _serial_number)
        )
    # Gets the first 5 characters of the serial number, then the last 2 characters
    first_half = _serial_number[:5]     # First 5
    int_half = _serial_number[5:]       # Last 2

    for serial_range in range(1, 3):
        # Skip the first iteration of the loop to use initial dual valve decoder serial number before incrementing it
        if serial_range > 1:
            int_half = int(int_half) + 1

            # If last two digits for incrementing start with '0', ie: "01", after incrementing "01", int_half has a
            # value as an integer of 2, length 1, but we need a length of 2, thus add the '0' back to the front for
            # a valid serial. See https://docs.python.org/2/library/string.html#formatspec
            int_half = '{0:0>2}'.format(int_half)

        # New constructed serial number
        new_serial = first_half + str(int_half)

        # New valve bicoder instance
        new_valve_bicoder = SimulatedValveBicoder(_sn=new_serial, _controller=_controller, _id=_device_type,
                                                  _decoder_type=opcodes.two_valve_decoder)

        # Add to controller's dict
        _controller.simulated_valve_bicoders[new_serial] = new_valve_bicoder


#################################
def create_simulated_quad_valve_bicoder_object(_controller, _serial_number, _device_type):
    """
    Create a quad valve bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.tw_sim.TWSimulator

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :param _device_type:    The device type that this bicoder will later be attached to. \n
    :type _device_type:     str \n

    :return:
    """
    # TODO do we need to check for proper starts to serial numbers
    # _serial_number.startswith(("TSQ", "Q")):
    if _serial_number[6] == "0":
        raise ValueError("Serial Number {0} is not valid for a quad valve bicoder. Cannot end with a '0'".format(
            _serial_number)
        )
    # Gets the first 5 characters of the serial number, then the last 2 characters
    first_half = _serial_number[:5]     # First 5
    int_half = _serial_number[5:]       # Last 2

    for serial_range in range(1, 5):
        # Skip the first iteration of the loop to use initial quad valve decoder serial number before incrementing it
        if serial_range > 1:
            int_half = int(int_half) + 1

            # If last two digits for incrementing start with '0', ie: "01", after incrementing "01", int_half has a
            # value as an integer of 2, length 1, but we need a length of 2, thus add the '0' back to the front for
            # a valid serial.  See https://docs.python.org/2/library/string.html#formatspec
            int_half = '{0:0>2}'.format(int_half)

        # New constructed serial number
        new_serial = first_half + str(int_half)

        # New valve bicoder instance
        new_valve_bicoder = SimulatedValveBicoder(_sn=new_serial, _controller=_controller, _id=_device_type,
                                                  _decoder_type=opcodes.four_valve_decoder)

        # Add to controller's dict
        _controller.simulated_valve_bicoders[new_serial] = new_valve_bicoder


#################################
def create_simulated_twelve_valve_bicoder_object(_controller, _serial_number, _device_type):
    """
    Create a twelve valve bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.tw_sim.TWSimulator

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :param _device_type:    The device type that this bicoder will later be attached to. \n
    :type _device_type:     str \n

    :return:
    """
    # TODO do we need to check for proper starts to serial numbers
    # _serial_number.startswith(("TVA", "TVB", "V")):
    # Gets the first 5 characters of the serial number, then the last 2 characters
    first_half = _serial_number[:5]     # First 5
    int_half = _serial_number[5:]       # Last 2

    for serial_range in range(1, 13):
        # Skip the first iteration of the loop to use initial twelve valve decoder serial number before incrementing it
        if serial_range > 1:
            int_half = int(int_half) + 1

            # If last two digits for incrementing start with '0', ie: "01", after incrementing "01", int_half has a
            # value as an integer of 2, length 1, but we need a length of 2, thus add the '0' back to the front for
            # a valid serial.  See https://docs.python.org/2/library/string.html#formatspec
            int_half = '{0:0>2}'.format(int_half)

        # New constructed serial number
        new_serial = first_half + str(int_half)

        # New valve bicoder instance
        new_valve_bicoder = SimulatedValveBicoder(_sn=new_serial, _controller=_controller, _id=_device_type,
                                                  _decoder_type=opcodes.twelve_valve_decoder)

        # Add to controller's dict
        _controller.simulated_valve_bicoders[new_serial] = new_valve_bicoder


#################################
def create_simulated_flow_bicoder_object(_controller, _serial_number):
    """
    Create a flow bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.tw_sim.TWSimulator

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :return:
    """
    new_flow_bicoder = SimulatedFlowBicoder(_sn=_serial_number, _controller=_controller)
    _controller.simulated_flow_bicoders[_serial_number] = new_flow_bicoder


#################################
def create_simulated_moisture_bicoder_object(_controller, _serial_number):
    """
    Create a moisture bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.tw_sim.TWSimulator

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :return:
    """
    new_moisture_bicoder = SimulatedMoistureBicoder(_sn=_serial_number, _controller=_controller)
    _controller.simulated_moisture_bicoders[_serial_number] = new_moisture_bicoder


#################################
def create_simulated_temperature_bicoder_object(_controller, _serial_number):
    """
    Create a temperature bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.tw_sim.TWSimulator

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :return:
    """
    new_temp_bicoder = SimulatedTempBicoder(_sn=_serial_number, _controller=_controller)
    _controller.simulated_temperature_bicoders[_serial_number] = new_temp_bicoder


#################################
def create_simulated_switch_bicoder_object(_controller, _serial_number):
    """
    Create a switch bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.tw_sim.TWSimulator

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :return:
    """
    new_switch_bicoder = SimulatedSwitchBicoder(_sn=_serial_number, _controller=_controller)
    _controller.simulated_switch_bicoders[_serial_number] = new_switch_bicoder


#################################
def create_simulated_analog_bicoder_object(_controller, _serial_number):
    """
    Create a analog bicoder with the specified serial. \n

    :param _controller:     The controller that will be passed into the bicoder made by this method. \n
    :type _controller:      common.objects.controllers.tw_sim.TWSimulator

    :param _serial_number:  The serial number of the bicoder. \n
    :type _serial_number:   str \n

    :return:
    """
    new_analog_bicoder = SimulatedAnalogBicoder(_sn=_serial_number, _controller=_controller)
    _controller.simulated_analog_bicoders[_serial_number] = new_analog_bicoder


########################################################################################################################
#                                                                                                                      #
#                        Old Methods for Creating biCoder Objects                                                      #
#                                                                                                                      #
########################################################################################################################

#################################
# def create_valve_bicoders_for_sb(controller, serial_list, all_valve_bicoders):
#     """
#     :param serial_list: List of valve bicoder serial numbers to create. \n
#     :type serial_list: list[str]
#
#     :return: Dictionary of Valve Bicoders indexed by Serial Number.
#     :rtype: dict[str, common.objects.substation.valve_bicoder.ValveBicoder]
#     """
#     for serial_number in serial_list:
#         if serial_number in all_valve_bicoders:
#             controller.d1_bicoders
#
#     for serial_number in serial_list:
#         valve_bicoder = ValveBicoder(_sn=serial_number, _controller=controller)
#         valve_bicoders[serial_number] = valve_bicoder
#
#     return valve_bicoders

#################################
def create_flow_bicoders_for_sb(serial_list, controller):
    """
    :param serial_list: List of flow bicoder serial numbers to create.
    :type serial_list: list[str]

    :param sb_ser: Substation's serial connection instance.
    :type sb_ser: common.objects.base_classes.ser.Ser

    :return: Dictionary of Flow Bicoders indexed by Serial Number.
    :rtype: dict[str, common.objects.substation.flow_bicoder.FlowBicoder]
    """
    warnings.warn('create_flow_bicoders_for_sb is obsolete', DeprecationWarning)
    pass

    # flow_bicoders = dict()
    #
    # for serial_number in serial_list:
    #     flow_bicoder = FlowBicoder(_sn=serial_number, _controller=controller)
    #     flow_bicoders[serial_number] = flow_bicoder
    #
    # return flow_bicoders


#################################
def create_pump_bicoders_for_sb(serial_list, controller):
    """
    :param serial_list: List of pump bicoder serial numbers to create.
    :type serial_list: list[str]

    :param sb_ser: Substation's serial connection instance.
    :type sb_ser: common.objects.base_classes.ser.Ser

    :return: Dictionary of Pump Bicoders indexed by Serial Number.
    :rtype: dict[str, common.objects.substation.pump_bicoder.PumpBicoder]
    """
    warnings.warn('create_pump_bicoders_for_sb is obsolete', DeprecationWarning)
    pass

    # pump_bicoders = dict()
    #
    # for serial_number in serial_list:
    #     pump_bicoder = PumpBicoder(_sn=serial_number, _controller=controller)
    #     pump_bicoders[serial_number] = pump_bicoder
    #
    # return pump_bicoders


#################################
def create_switch_bicoders_for_sb(serial_list, controller):
    """
    :param serial_list: List of switch bicoder serial numbers to create.
    :type serial_list: list[str]

    :param sb_ser: Substation's serial connection instance.
    :type sb_ser: common.objects.base_classes.ser.Ser

    :return: Dictionary of Switch Bicoders indexed by Serial Number.
    :rtype: dict[str, common.objects.substation.switch_bicoder.SwitchBicoder]
    """
    warnings.warn('create_switch_bicoders_for_sb is obsolete', DeprecationWarning)
    pass

    # switch_bicoders = dict()
    #
    # for serial_number in serial_list:
    #     switch_bicoder = SwitchBicoder(_sn=serial_number, _controller=controller)
    #     switch_bicoders[serial_number] = switch_bicoder
    #
    # return switch_bicoders


#################################
def create_moisture_bicoders_for_sb(serial_list, controller):
    """
    :param serial_list: List of moisture bicoder serial numbers to create.
    :type serial_list: list[str]

    :param sb_ser: Substation's serial connection instance.
    :type sb_ser: common.objects.base_classes.ser.Ser

    :return: Dictionary of Moisture Bicoders indexed by Serial Number.
    :rtype: dict[str, common.objects.substation.moisture_bicoder.MoistureBicoder]
    """
    warnings.warn('create_moisture_bicoders_for_sb is obsolete', DeprecationWarning)
    pass

    # moisture_bicoders = dict()
    #
    # for serial_number in serial_list:
    #     moisture_bicoder = MoistureBicoder(_sn=serial_number, _controller=controller)
    #     moisture_bicoders[serial_number] = moisture_bicoder
    #
    # return moisture_bicoders

#################################
def create_temperature_bicoders_for_sb(serial_list, controller):
    """
    :param serial_list: List of temperature bicoder serial numbers to create.
    :type serial_list: list[str]

    :param sb_ser: Substation's serial connection instance.
    :type sb_ser: common.objects.base_classes.ser.Ser

    :return: Dictionary of Temperature Bicoders indexed by Serial Number.
    :rtype: dict[str, common.objects.substation.temp_bicoder.TempBicoder]
    """
    warnings.warn('create_temperature_bicoders_for_sb is obsolete', DeprecationWarning)
    pass

    # temperature_bicoders = dict()
    #
    # for serial_number in serial_list:
    #     temperature_bicoder = TempBicoder(_sn=serial_number, _controller=controller)
    #     temperature_bicoders[serial_number] = temperature_bicoder
    #
    # return temperature_bicoders
#################################
def share_sb_device_with_cn(self, device_type, device_serial, address, substation_number=1):
    """
    Share a Substation device with the Controller being tested.

    Operations:
    1) Creates a new instance of the device and adds it to the respecitve object dictionary.
    2) Addresses and sets default values on the controller.

    :param device_type: Type of device to share. (i.e., "FM" | "ZN" | "MV" | "SW" | "TS" | "MS" )
    :type device_type: str

    :param device_serial: Serial number of device to share.
    :type device_serial: str

    :param address: Address to set for device.
    :type address: int

    :param substation_number: Address of Substation to share device for.
    :type substation_number: int
    """
    warnings.warn('This share_sb_device_with_cn is obsolete - use the one in configuration.py', DeprecationWarning)
    pass

    # valid_device_types = [opcodes.zone, opcodes.flow_meter, opcodes.master_valve, opcodes.event_switch,
    #                       opcodes.temperature_sensor, opcodes.moisture_sensor]
    #
    # # Validate device type
    # if device_type not in valid_device_types:
    #     e_msg = "Attempted to share an invalid device type ('{0}') with a controller. Valid device types " \
    #             "are: {1}".format(device_type, valid_device_types)
    #     raise ValueError(e_msg)
    #
    # # device_type == "ZN"
    # if device_type == opcodes.zone:
    #
    #     # Validate zone address
    #     if address in self.zones.keys():
    #         e_msg = "Attempted to share a Substation valve serial number: {0} at address: {1} which is already " \
    #                 "addressed on the Controller. Current valve addresses are: {2}".format(
    #                     device_serial,
    #                     address,
    #                     self.zones.keys()
    #                 )
    #         raise ValueError(e_msg)
    #
    #     else:
    #         self.zones[address] = Zone(_serial=device_serial, _address=address, _shared_with_sb=True)
    #         self.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.zones,
    #                                                                   zn_ad_range=[address])
    #
    # # device_type == "MV"
    # elif device_type == opcodes.master_valve:
    #
    #     # Validate master valve address
    #     if address in self.master_valves.keys():
    #         e_msg = "Attempted to share a Substation valve serial number: {0} at address: {1} which is already " \
    #                 "addressed on the Controller. Current valve addresses are: {2}".format(
    #                     device_serial,
    #                     address,
    #                     self.master_valves.keys()
    #                 )
    #         raise ValueError(e_msg)
    #
    #     else:
    #         self.master_valves[address] = MasterValve(_serial=device_serial, _address=address, _shared_with_sb=True)
    #         self.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.master_valves,
    #                                                                   mv_ad_range=[address])
    #
    # # device_type == "FM"
    # elif device_type == opcodes.flow_meter:
    #
    #     # Validate flow meter address
    #     if address in self.flow_meters.keys():
    #         e_msg = "Attempted to share a Substation flow bicoder serial number: {0} at address: {1} which is " \
    #                 "already addressed on the Controller. Current flow device addresses are: {2}".format(
    #                     device_serial,
    #                     address,
    #                     self.flow_meters.keys()
    #                 )
    #         raise ValueError(e_msg)
    #
    #     else:
    #         self.flow_meters[address] = FlowMeter(_serial=device_serial, _address=address, _shared_with_sb=True)
    #         self.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.flow_meters,
    #                                                                   fm_ad_range=[address])
    #
    #         # Added when the flow rate count/flow usage count equations were implemented. The kvalue from the
    #         # Controller's Flow Meter is required for calculations. It works for only 1 substation for right now.
    #         self.substations[substation_number].flow_bicoders[device_serial].fm_on_cn = self.flow_meters[address]
    #
    # # device_type == "MS"
    # elif device_type == opcodes.moisture_sensor:
    #
    #     # Validate moisture sensor address
    #     if address in self.moisture_sensors.keys():
    #         e_msg = "Attempted to share a Substation moisture bicoder serial number: {0} at address: {1} which " \
    #                 "is already addressed on the Controller. Current moisture device addresses are: {2}".format(
    #                     device_serial,
    #                     address,
    #                     self.moisture_sensors.keys()
    #                 )
    #         raise ValueError(e_msg)
    #
    #     else:
    #         self.moisture_sensors[address] = MoistureSensor(_serial=device_serial, _address=address,
    #                                                         _shared_with_sb=True)
    #         self.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.moisture_sensors,
    #                                                                   ms_ad_range=[address])
    #
    # # device_type == "TS"
    # elif device_type == opcodes.temperature_sensor:
    #
    #     # Validate temperature sensor address
    #     if address in self.temperature_sensors.keys():
    #         e_msg = "Attempted to share a Substation temp bicoder serial number: {0} at address: {1} which is " \
    #                 "already addressed on the Controller. Current temp device addresses are: {2}".format(
    #                     device_serial,
    #                     address,
    #                     self.temperature_sensors.keys()
    #                 )
    #         raise ValueError(e_msg)
    #
    #     else:
    #         self.temperature_sensors[address] = TemperatureSensor(_serial=device_serial, _address=address,
    #                                                               _shared_with_sb=True)
    #         self.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.temperature_sensors,
    #                                                                   ts_ad_range=[address])
    #
    # # device_type == "SW"
    # # NOTE - 'else' can be used here because "SW" is the last 'valid' device type, and since we already validated
    # #        device types above, we can assume we have a "SW" at this point.
    # else:
    #
    #     # Validate event switch address
    #     if address in self.event_switches.keys():
    #         e_msg = "Attempted to share a Substation switch bicoder serial number: {0} at address: {1} which is " \
    #                 "already addressed on the Controller. Current switch device addresses are: {2}".format(
    #                     device_serial,
    #                     address,
    #                     self.event_switches.keys()
    #                 )
    #         raise ValueError(e_msg)
    #
    #     else:
    #         self.event_switches[address] = EventSwitch(_serial=device_serial, _address=address,
    #                                                    _shared_with_sb=True)
    #         self.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.event_switches,
    #                                                                   sw_ad_range=[address])
