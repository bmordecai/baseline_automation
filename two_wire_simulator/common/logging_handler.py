import logging


class LoggingHandler(object):
    """
    This class handles the logging functionality that we use in product assessments and our use cases

    There is a tightly coupled relationship with our logging and the continuous running of our use cases

    To use logging functionality in any class (like a use case) simply put this line at the top:
        from common.logging_handler import log_handler

    You can then access the methods of this class by using the 'log_handler' variable.
    To actually log a message, it will look something like this in the use case: log_handler.debug('example message')
    """
    # The file name that our logger will output to
    LOG_FILENAME = "_run_automated_test_suit_log.txt"
    logger = None
    serial_logger = None
    get_data_logger = None

    # Boolean value stating if we should be logging, will also allow our use cases to continue running the next use case
    # even if a use cases bombs out with an exception.
    enabled = False

    # Boolean value stating if we should clear the contents of the log file before we run
    overwrite = True
    
    # Boolean value stating if we should include extra serial port logging
    serial_port_logging = False
    
    # Boolean value stating if we should include extra incoming data from controller(s) from "get_data()" logging
    get_data_logging = False

    ########################################
    def __init__(self, filename=LOG_FILENAME, level=logging.DEBUG):
        self.had_exception = None

    ########################################
    def start_logging(self, logger_name, filename=LOG_FILENAME, level=logging.DEBUG):
        l = logging.getLogger(logger_name)
        
        formatter = logging.Formatter('%(asctime)s : %(message)s')
        formatter_without_timestap = logging.Formatter('%(message)s')
        
        file_handler = logging.FileHandler(filename=filename, mode='w')

        if logger_name == 'AutomatedTestSuiteGetDataLogger':
            file_handler.setFormatter(formatter_without_timestap)
        else:
            file_handler.setFormatter(formatter)
            
        l.setLevel(level)
        l.addHandler(file_handler)
        
        if logger_name == 'AutomatedTestSuiteSerialPortIOLogger':
            LoggingHandler.serial_logger = l
        elif logger_name == 'AutomatedTestSuiteGetDataLogger':
            LoggingHandler.get_data_logger = l
        else:
            stream_handler = logging.StreamHandler()
            stream_handler.setFormatter(formatter)
            l.addHandler(stream_handler)
            LoggingHandler.logger = l
            
        # # Clears the contents of the log file
        # if LoggingHandler.overwrite:
        #     open(filename, 'w').close()
        #
        # # Creates the logging
        # logging.basicConfig(filename=filename, level=level)

    ########################################
    @staticmethod
    def overwrite_log_file(overwrite):
        LoggingHandler.overwrite = overwrite

    ########################################
    @staticmethod
    def enable_continuous_run_and_logging_for_use_cases(enabled):
        """
        This method controls if we will be logging in our use cases. If we do enable logging, then instead of crashing
        on errors, we instead log the error and continue running. \n

        :param enabled: True or False value that determines if we will log in our use cases \n
        :type enabled: bool
        """
        LoggingHandler.enabled = enabled
        
    ########################################
    @staticmethod
    def enable_serial_port_logging(enabled):
        """
        This method controls whether or not we include extra logging from the serial port into the log.

        :param enabled: True or False value that determines if we will log in our use cases \n
        :type enabled: bool
        """
        LoggingHandler.serial_port_logging = enabled
        
    ########################################
    @staticmethod
    def enable_incoming_get_data_logging(enabled):
        """
        This method controls whether or not we include extra logging from the incoming data received by get_data()
        into the log.

        :param enabled: True or False value that determines if we will log in our use cases \n
        :type enabled: bool
        """
        LoggingHandler.get_data_logging = enabled

    ########################################
    @staticmethod
    def is_enabled():
        return LoggingHandler.enabled
    
    ########################################
    @staticmethod
    def is_log_serial_port_io_enabled():
        return LoggingHandler.serial_port_logging
    
    ########################################
    @staticmethod
    def is_log_get_data_to_csv_enabled():
        return LoggingHandler.get_data_logging

    ########################################
    def log_serial_io(self, message):
        """
        Logging method called by serial port for extra serial output
        
        Logs a message at the 'DEBUG' level if logging is enabled
        """
        if LoggingHandler.is_log_serial_port_io_enabled() and self.serial_logger:
            self.serial_logger.debug(message)
            print(message)
            
    ########################################
    def log_incoming_get_data_response_as_csv(self, message):
        """
        Logging method called by serial port for minimal log file (csv) containing only data from
        get_data() calls.
        
        Logs a message at the 'DEBUG' level if logging is enabled
        """
        if LoggingHandler.is_log_get_data_to_csv_enabled() and self.get_data_logger:
            self.get_data_logger.debug(message)
            print(message)

    ########################################
    def debug(self, message):
        """
        Logs a message at the 'DEBUG' level if logging is enabled
        """
        if LoggingHandler.is_enabled():
            self.logger.debug(message)

    ########################################
    def info(self, message):
        """
        Logs a message at the 'INFO' level if logging is enabled
        """
        if LoggingHandler.is_enabled():
            self.logger.info(message)

    ########################################
    def warning(self, message):
        """
        Logs a message at the 'WARNING' level if logging is enabled
        """
        if LoggingHandler.is_enabled():
            self.logger.warning(message)

    ########################################
    def error(self, message):
        """
        Logs a message at the 'ERROR' level if logging is enabled
        """
        if LoggingHandler.is_enabled():
            self.logger.error(message)

    ########################################
    def critical(self, message):
        """
        Logs a message at the 'CRITICAL' level if logging is enabled
        """
        if LoggingHandler.is_enabled():
            self.logger.critical(message)

    ########################################
    def exception(self, message):
        """
        Logs a message at the 'CRITICAL' level if logging is enabled
        """
        if LoggingHandler.is_enabled():
            self.logger.exception(message)
        self.had_exception = True

    ########################################
    def return_email_subject_and_body(self, user):
        """
        Returns a string depending on if our log_handler caught an exception in one of the use cases
        """
        if self.had_exception:
            subject = "Build Failed Tests: {0} Gets the Clown Shoes".format(user)
            body = "See the attached log file for more details about which tests passed/failed."
        else:
            subject = "Build Successfully Passed Tests: {0} is a Winner Winner Chicken Dinner".format(user)
            body = "The attached log file will show which tests passed."

        return subject, body

# GLOBAL Declaration: This must be initialized in product assessments, and then it can be used anywhere with a simple
# import statement that will look like this: 'import common.logging_handler
log_handler = LoggingHandler()
