__author__ = 'baseline'
"""
page_factory.py provides a way to get page objects from a single location as well
as avoiding circular imports.

This module imports each python script as a module,
    i.e., import common.objects.basemanager.main_page as main_page

instead of,
    from common.objects.basemanager.main_page import MainPage
    -> these types of imports create circular import dependencies

This allows us to unit test all constructors for page objects.

Pages implemented so far:

Desktop:
    01. Login Page
    02. Main Page
    03. Controller Settings Dialog
    04. Maps
    05. Programs
    06. QuickView
    07. LiveView
    08. Zones
    09. Moisture Sensors
    10. Master Valves
    11. Flow Meters
    12. Temperature Sensors
    13. Event Switches
    14. Water Sources (1000 Controller)
    15. Admin Tools 

Mobile:
    01. Login Page
    02. Main Page

Usage:
1. import page_factory
2. main_page = factory.get_main_page_object(_webdriver=some_webdriver_instance)
"""
from selenium import webdriver
import common.objects.base_classes.web_driver as driver

# BaseManager Pages
import common.objects.basemanager.login_page as basemanager_login_page
import common.objects.basemanager.main_page as basemanager_main_page
import common.objects.basemanager.admin_tools_page as basemanager_admin_tools_page
import common.objects.basemanager.maps_tab as basemanager_maps_tab
import common.objects.basemanager.pg_tab as basemanager_pg_tab
import common.objects.basemanager.qv_tab as basemanager_qv_tab
import common.objects.basemanager.dv_tab as basemanager_dv_tab
import common.objects.basemanager.live_view_tab as basemanager_live_view_tab
import common.objects.basemanager.water_sources_tab_1000 as basemanager_water_sources_tab_1000
import common.objects.basemanager.water_sources_tab_3200 as basemanager_water_sources_tab_3200
import common.objects.basemanager.subscriptions_page as basemanager_subscription_page


# APPManager Pages
import common.objects.appmanager.login_page as appmanager_login_page
import common.objects.appmanager.main_page as appmanager_main_page

# mobile Access Pages
import common.objects.mobile_access.login_page as mobile_access_login_page
import common.objects.mobile_access.main_page as mobile_access_main_page
import common.objects.mobile_access.test_device_page as mobile_access_test_device_page


def get_basemanager_login_page_object(base_page):
    """
    Returns a login page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    """
    base_page.login_page = basemanager_login_page.LoginPage(web_driver=base_page.driver)


def get_basemanager_main_page_object(base_page):
    """
    Returns a main page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    """
    base_page.main_page = basemanager_main_page.MainPage(web_driver=base_page.driver)


def get_basemanager_main_menu_object(base_page):
    """
    Returns a main page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    """
    base_page.main_page_main_menu = basemanager_main_page.MainPageMenu(web_driver=base_page.driver)


def get_basemanager_controller_settings_dialog_object(base_page):
    """
    Returns a controller settings dialog page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    """
    base_page.controller_dialog_box = basemanager_main_page.ControllerSettingsDialogBox(web_driver=base_page.driver)


def get_basemanager_admin_tools_page_object(base_page):
    """
    Returns a main page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    """
    base_page.admin_tools_page = basemanager_admin_tools_page.AdminToolsPage(web_driver=base_page.driver)


def get_basemanager_maps_page_object(base_page):
    """
    Returns a maps page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    """
    base_page.maps_page = basemanager_maps_tab.MapsTabPage(web_driver=base_page.driver)


def get_basemanager_programs_page_object(base_page, _controller):
    """
    Returns a Programs page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    """
    base_page.programs_page = basemanager_pg_tab.ProgramsTabPage(web_driver=base_page.driver, controller=_controller)


def get_basemanager_quick_view_page_object(base_page, _controller):
    """
    Returns a quick view page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    :param _controller: Controller instance \n
    :type _controller: common.objects.controllers.bl_32.BaseStation3200
    """
    base_page.quick_view_page = basemanager_qv_tab.QuickViewTabPage(web_driver=base_page.driver,
                                                                    controller=_controller)


def get_basemanager_zones_page_object(base_page, _controller):
    """
    Returns a zones page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    :param _controller: Controller instance \n
    :type _controller: common.objects.controllers.bl_32.BaseStation3200
    """
    base_page.zones_tab_page = basemanager_dv_tab.ZonesTabPage(driver=base_page.driver, controller=_controller)


def get_basemanager_moisture_sensors_page_object(base_page, _controller):
    """
    Returns a moisture sensors page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    :param _controller: Controller instance \n
    :type _controller: common.objects.controllers.bl_32.BaseStation3200
    """

    base_page.moisture_sensor_tab_page = basemanager_dv_tab.MoistureSensorsTabPage(driver=base_page.driver,
                                                                                   controller=_controller)


def get_basemanager_master_valves_page_object(base_page, _controller):
    """
    Returns a master valve page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    :param _controller: Controller instance \n
    :type _controller: common.objects.controllers.bl_32.BaseStation3200
    """
    base_page.master_valve_tab_page = basemanager_dv_tab.MasterValvesTabPage(driver=base_page.driver,
                                                                             controller=_controller)


def get_basemanager_flow_meter_page_object(base_page, _controller):
    """
    Returns a flow meter page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    :param _controller: Controller instance \n
    :type _controller: common.objects.controllers.bl_32.BaseStation3200
    """
    base_page.flow_sensor_tab_page = basemanager_dv_tab.FlowMetersTabPage(driver=base_page.driver,
                                                                          controller=_controller)


def get_basemanager_temperature_sensors_page_object(base_page, _controller):
    """
    Returns a temperature sensors page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    :param _controller: Controller instance \n
    :type _controller: common.objects.controllers.bl_32.BaseStation3200
    """
    base_page.temperature_sensor_tab_page = basemanager_dv_tab.TemperatureSensorsTabPage(driver=base_page.driver,
                                                                                         controller=_controller)


def get_basemanager_event_switch_page_object(base_page, _controller):
    """
    Returns a event switch page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    :param _controller: Controller instance \n
    :type _controller: common.objects.controllers.bl_32.BaseStation3200
    """
    base_page.event_switch_tab_page = basemanager_dv_tab.EventSwitchTabPage(driver=base_page.driver,
                                                                            controller=_controller)


def get_basemanager_pressure_sensors_page_object(base_page, _controller):
    """
    Returns a pressure sensors page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    :param _controller: Controller instance \n
    :type _controller: common.objects.controllers.bl_32.BaseStation3200
    """
    base_page.pressure_sensor_tab_page = basemanager_dv_tab.PressureSensorsTabPage(driver=base_page.driver,
                                                                                   controller=_controller)


def get_basemanager_pumps_page_object(base_page, _controller):
    """
    Returns a pumps page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    :param _controller: Controller instance \n
    :type _controller: common.objects.controllers.bl_32.BaseStation3200
    :return: Pumps Page Instance
    :rtype: dv_tab.PumpsTabPage
    """
    base_page.pump_tab_page = basemanager_dv_tab.PumpsTabPage(driver=base_page.driver, controller=_controller)


def get_basemanager_1000_water_sources_tab(base_page):
    """
    Returns a 1000 water sources page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    """
    base_page.water_source_1000_page = basemanager_water_sources_tab_1000.WaterSourcesTab1000(web_driver=base_page.driver)


def get_basemanager_3200_water_sources_tab(base_page, _controller):
    """
    Returns a v16 3200 water sources page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    :param _controller: Controller instance \n
    :type _controller: common.objects.controllers.bl_32.BaseStation3200
    """
    base_page.water_source_3200_page = basemanager_water_sources_tab_3200.WaterSourcesPage3200(
        web_driver=base_page.driver, controller=_controller)


def get_basemanager_3200_poc_tab(base_page, _controller):
    """
    Returns a 3200 Point Of Connection page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    :param _controller: Controller instance \n
    :type _controller: common.objects.controllers.bl_32.BaseStation3200
    """
    base_page.point_of_control_3200_page = basemanager_water_sources_tab_3200.PointOfConnectionPage3200(
        web_driver=base_page.driver, controller=_controller)


def get_basemanager_3200_mainlines_tab(base_page, _controller):
    """
    Returns a 3200 Mainlines page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    :param _controller: Controller instance \n
    :type _controller: common.objects.controllers.bl_32.BaseStation3200
    """
    base_page.mainline_3200_page = basemanager_water_sources_tab_3200.MainlinesPage3200(
        web_driver=base_page.driver, controller=_controller)


def get_basemanager_live_view_page_object(base_page):
    """
    Returns a live view page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    """
    base_page.live_view_page = basemanager_live_view_tab.LiveViewTabPage(web_driver=base_page.driver)


def get_basemanager_subscription_page_object(base_page):
    """
    Returns a subscription page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.basemanager.base_page.BaseManager
    """
    base_page.subscriptions_page = basemanager_subscription_page.SubscriptionsPage(web_driver=base_page.driver)


def get_appmanager_login_page_object(base_page):

    """
    Returns a login page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.appmanager.base_page.AppManager
    """
    base_page.login_page = appmanager_login_page.LoginPage(web_driver=base_page.driver)


def get_appmanager_main_page_object(base_page):
    """
    Returns a main page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.appmanager.base_page.AppManager
    """
    base_page.main_page = appmanager_main_page.MainPage(web_driver=base_page.driver)


def get_mobile_login_page_object(base_page):
    """
    Returns a mobile login page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.mobile_access.base_page.MobileAccess
    """
    base_page.login_page = mobile_access_login_page.LoginPage(web_driver=base_page.driver)


def get_mobile_main_page_object(base_page):
    """
    Returns a mobile main page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.mobile_access.base_page.MobileAccess
    """
    base_page.main_page = mobile_access_main_page.MainPage(web_driver=base_page.driver)


def get_mobile_test_device_page_object(base_page):
    """
    Returns a mobile test device page object
    :param base_page: Web Driver instance
    :type base_page: common.objects.mobile_access.base_page.MobileAccess
    """
    base_page.test_device_page = mobile_access_test_device_page.TestDevicePage(web_driver=base_page.driver)
