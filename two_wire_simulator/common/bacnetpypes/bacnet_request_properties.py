import sys

from collections import defaultdict, deque

from bacpypes.debugging import bacpypes_debugging, ModuleLogger
from bacpypes.consolelogging import ConfigArgumentParser

from bacpypes.core import run, deferred, stop, run_once

from bacpypes.app import BIPSimpleApplication
# from bacpypes.service.device import LocalDeviceObject

import sys

from bacpypes.debugging import bacpypes_debugging, ModuleLogger
from bacpypes.consolelogging import ConfigArgumentParser
from bacpypes.consolecmd import ConsoleCmd

from bacpypes.core import run, enable_sleeping
from bacpypes.iocb import IOCB

from bacpypes.pdu import Address, GlobalBroadcast
from bacpypes.apdu import WhoIsRequest, IAmRequest, ReadPropertyRequest, ReadPropertyACK
from bacpypes.primitivedata import CharacterString, Tag
from bacpypes.basetypes import ServicesSupported
from bacpypes.errors import DecodingError

from bacpypes.app import BIPSimpleApplication

# TODO: UPDATE - BEN - 5/11/18
# TODO:     Bacpypes project moved the location of `LocalDeviceObject` from
# TODO:     `bacpypes.service.device` to `bacpypes.local.device`.
# TODO:     See for more info:
# TODO:         https://github.com/JoelBender/bacpypes/commit/c25b0862117f67cf3ac59b8be7a46fe44e9616b2#diff-0b2e4e45aa9e92dd4210ed811216cd8eR18
from bacpypes.service.device import LocalDeviceObject

from bacpypes.object import get_object_class, get_datatype

# some debugging
# this import allows us to directly use the date_mngr
import time
# this import allows us to directly use the date_mngr
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.date_package.date_resource import date_mngr
from threading import Thread


testFindDevice = 1
testBinaryInput = 2

#
#   BACmet Simple application
#   This application uses the bacpypes library version 0.16.6
#   Location:  https://pypi.python.org/pypi/BACpypes/0.16.6
#   bacpypes is a bacnet client application.
#   Application meaning we need to call the run() method
#


class BacnetClient(BIPSimpleApplication):
    def __init__(self):
        # default values for this class
        self.resetdefault()

        # Create the client for getting devices and properties
        # this client will used to listen Iam and ask for property values
        # device values needed for the client
        self.objectname = "bacclienttest"
        self.address = helper_methods.get_ip_currentsystem()
        self.objectIdentifier = "599000"
        self.maxApduLengthAccepted = "1024"
        self.segmentationSupported = ""
        self.vendorIdentifier = "15"

        # make a device object
        self.this_device = LocalDeviceObject(
            objectName=self.objectname,
            objectIdentifier=int(self.objectIdentifier),
            maxApduLengthAccepted=int(self.maxApduLengthAccepted),
            segmentationSupported=0,
            vendorIdentifier=int(self.vendorIdentifier)
        )

        # lets startup the client
        BIPSimpleApplication.__init__(self, self.this_device, self.address)

        # get the services supported
        services_supported = self.get_services_supported()
        # let the device object know
        self.this_device.protocolServicesSupported = services_supported.value


    def resetdefault(self):
        # Set default vlues for the target device
        self.resetTargetDevice()

        # found device values
        self.resetFoundDevice()

        # we want to count how many devices we have found
        # after we have found 20 we will not find the target device
        self.founddevicescount = 0
        self.founddeviceslimit = 40

        # Points to find the expected results for those points and the responses for those points
        self.pointsList = []
        self.expectedValuesDict = {}
        self.responseDict = {}

        # Set the default test to run
        self.testToRun = self.setTestToFindDevice()

        # set the default test results
        self.results = False

    def setTestToFindDevice(self):
        # The test to be run is the Find Device object test
        self.testToRun = testFindDevice

    def setTestToBinaryInput(self):
        # test to be run is testing binary inputs
        # the list of binary_inputs to test is in the expectreulst list
        self.testToRun = testBinaryInput

    def isTestFindDevice(self):
        # return true if its the FindDevice test
        if testFindDevice == self.testToRun:
            return True
        else:
            return False

    def isTestBinaryInput(self):
        # return true if its the Binaryinput test
        if testBinaryInput == self.testToRun:
            return True
        else:
            return False

    def setExpectedValueDict(self, dict):
        # the use case needs to send a list of points to test and
        # the expected values at those points.
        # This method set the points list so we have alist of
        # bacnet points to get and sets the expexcted dict with
        # bacnet poitns and values
        self.expectedValuesDict = dict
        for repKey in self.expectedValuesDict:
            self.pointsList.append(repKey)

    def isPointListAndExpexctedResultsSet(self):
        # This checks to make sure the points list and
        # expected values dict are set and same size
        rtn = True
        listlen = len(self.pointsList)
        if listlen < 1:
            rtn = False

        dictlen = len(self.expectedValuesDict)
        if dictlen < 1:
            rtn = False

        return rtn

    def setFoundDevice(self, name, port, ip, id):
        # When a bacnet device is found its attributes are
        # Stored here.  These are used when verifing against the
        # target device.
        self.founddevicename = name
        strsplit = name.split("-")
        self.founddevicemac = strsplit[len(strsplit)-1]
        self.founddeviceport = port
        self.founddeviceip = ip
        self.founddevicebacnetid = id

    def resetFoundDevice(self):
        # This is to reset the found device attributes
        # So the last one found can be cleared out.
        self.founddevicename = ''
        self.founddevicemac = ''
        self.founddeviceport = 1
        self.founddeviceip = '1.1.1.1.1'
        self.founddevicebacnetid = 0

    def setTargetDevice(self, name, mac, ip):
        # Target device must be set so we know what device
        # to look for.
        self.targetdevicename = name
        self.targetdevicemac = mac
        self.targetdevicebacnetport = 47808
        self.targetdevicebacnetip = ip
        self.targetdevicebacnetid = 0
        self.targetdevicefound = False

    def resetTargetDevice(self):
        # This reset the target device to defaults
        self.targetdevicename = 'NotSet'
        self.targetdevicemac = '000000000000'
        self.targetdevicebacnetport = 47808
        self.targetdevicebacnetip = '10.11.12.27'
        self.targetdevicebacnetid = 0
        self.targetdevicefound = False

    def isFoundDeviceTargetDevice(self):
        # this verifies that the device found matchtes the
        # Target device.
        # Does found device match target device
        # I use the verify count so I could debug each
        # comparison
        rtn = False
        verifycount = 0
        if self.targetdevicename in self.founddevicename:
            verifycount = verifycount + 1

        if self.targetdevicemac in self.founddevicemac:
            verifycount = verifycount + 1

        if self.targetdevicebacnetip in self.founddeviceip:
            verifycount = verifycount + 1

        if verifycount == 3:
            rtn = True

        self.targetdevicefound = rtn
        return rtn

    def areDone(self):
        # This is a hack
        # We are running an applicaiton so inside the application we
        # have to determine when to stop running.
        # We stop running if
        # the list of point to test is same length as the
        # reponse dictionary. We have recived a responses that can
        # be compared expected results.
        listlen = len(self.pointsList)
        repDictLen = 0
        result = False
        for repKey in self.responseDict:
            repDict = self.responseDict[repKey]
            repDictLen = len(repDict)
            if listlen == repDictLen:
                result = True
                break

        return result

    ###############################################
    # This section of code is for handeling the bacnet traffic
    # Sending requests and handeling the reponses
    #
    #
    ###############################################

    def request(self, apdu):
        # save a copy of just the Who-Is request
        if isinstance(apdu, WhoIsRequest):
            self.who_is_request = apdu

        # forward it along
        BIPSimpleApplication.request(self, apdu)

    def do_IAmRequest(self, apdu):
        # This is to process the I am  requests
        # HACK
        # each time we get an I am request we check to see if
        # we have already recieved the repsonse to all our requests
        # if so lets stop the application.
        if self.areDone():
            stop(0)

        # check for required parameters
        if apdu.iAmDeviceIdentifier is None:
            raise Exception, Exception("iAmDeviceIdentifier required")
        if apdu.maxAPDULengthAccepted is None:
            raise Exception, Exception("maxAPDULengthAccepted required")
        if apdu.segmentationSupported is None:
            raise Exception, Exception("segmentationSupported required")
        if apdu.vendorID is None:
            raise Exception, Exception("vendorID required")

        # extract the device instance number
        device_instance = apdu.iAmDeviceIdentifier[1]
        # extract the source address
        device_address = apdu.pduSource

        print "do_IAmRequest: ", device_instance, " :",device_address

        # We have a valid device so lets send request to get device
        # information.
        # Build request for the device found
        request = ReadPropertyRequest(
            destination=apdu.pduSource,
            objectIdentifier=apdu.iAmDeviceIdentifier,
            propertyIdentifier='objectName',
            )

        # make an IOCB
        iocb = IOCB(request)

        # let us know when its complete
        iocb.add_callback(self.device_discovered)

        # give it to the application
        self.request_io(iocb)

    def device_discovered(self, iocb):
        # we got response from our device request so let
        # processs it and see what we need to do with it

        # do something for error/reject/abort
        if iocb.ioError:
            sys.stdout.write(str(iocb.ioError) + '\n')

        # do something for success
        elif iocb.ioResponse:

            # we have a valid response.
            # lets reset the found device so we can
            # use this response to populate a clean founddevice
            self.resetFoundDevice()

            # get the response for the IO
            apdu = iocb.ioResponse

            # should be an ack
            if not isinstance(apdu, ReadPropertyACK):
                print "device_discovered    - not an ack"
                return

            # pull out the name
            device_name = apdu.propertyValue.cast_out(CharacterString)
            #pull out ip
            device_ip = apdu.pduSource.addrBroadcastTuple[0]
            #pull out port
            device_port = apdu.pduSource.addrBroadcastTuple[1]
            #pull out id
            device_id = 0

            # we found a valid device and parsed the information
            # set attributes of found device so later we can check and verifyr
            # device as target.
            print "device_discovered ", device_name
            self.setFoundDevice(device_name, device_port, device_ip, device_id)

        # do something with nothing?
        else:
            # we got and error tell the console and continue processing
            print "device_discovered - ioError or ioResponse expected"
            return



        # We have a valid device, we have stored the attributes in the founddevice
        # lets check to see if it is the device we are looking for
        if self.isFoundDeviceTargetDevice():

            # we found the device we are looking for lets see what test
            # we are running

            if self.isTestFindDevice():
                # we only wanted to see if we could find a target device
                # we have do that lets stop the application.
                # the calling use case will need to call verify to
                # see if we passed the test
                stop()

            if self.isTestBinaryInput():
                # we are going to test the binary inputs that is listed in
                # the expected results set.
                # lets see if we have points list and expected results
                if self.isPointListAndExpexctedResultsSet():
                    # we have the expected results lets go ask for all the
                    # binary points we want to test
                    # fire off request for each point
                    for alarm_Id in self.pointsList:
                        self.binaryInput_sendRquest(alarm_Id, device_name, apdu.pduSource)
                else:
                    # Expect results was not set correctly fail test
                    self.results = False
                    print "No BinaryInput Points or Expected Results"
                    stop()
        else:
            # each time we find a device and its not the target device we
            # count it
            self.founddevicescount += 1
            if self.founddevicescount > self.founddeviceslimit:
                # we have found the limit of devices and we have not found our
                # target device.  Clearly the device is not out there
                # fail the test
                self.targetdevicefound = False
                self.results = False
                print "Did not find target device. retry limt exceeded"
                stop()


    def binaryInput_sendRquest(self, alramId, deviceName, pduSource):
        # we are going send a proptery requst to the
        # binary point passed in

        # build a request
        # we are asking for the present value attribute
        request = ReadPropertyRequest(
            destination= pduSource,
            objectIdentifier=('binaryInput', alramId),
            propertyIdentifier='presentValue',
        )

        # make an IOCB
        iocb = IOCB(request)
        print "Build request:", deviceName, " ", request.objectIdentifier[0], ": ", request.objectIdentifier[
            1], " ", request.propertyIdentifier

        # SEt the call back funtion called when a response comes back
        # this function is below
        iocb.add_callback(self.binaryInput_response)

        # give the request to the application to send
        self.request_io(iocb)



    def binaryInput_response(self, iocb):
        # we have recieve a resposne to our binary property
        # request let process it

        # do something for success
        if iocb.ioResponse:
            # code was stolen from the sample application
            # am not sure what i does.  The end result is the
            # value for the property we asked for.

            # here is the response
            apdu = iocb.ioResponse

            # get the ip address and port number
            addr = iocb.ioController.address.addrBroadcastTuple


            # peek at the value tag
            value_tag = apdu.propertyValue.tagList.Peek()

            # make sure that it is application tagged
            if value_tag.tagClass != Tag.applicationTagClass:
                sys.stdout.write("value is not application encoded\n")

            else:
                # we have the response to our application
                # find the datatype
                datatype = Tag._app_tag_class[value_tag.tagNumber]
                if not datatype:
                    raise TypeError("unknown datatype")

                # cast out the value
                # Here is the property value yay
                value = apdu.propertyValue.cast_out(datatype)

                # lets store the value so let create a ket for the particular
                # device (controller that responsed
                # we do this because we could have two responsing
                controllerkey = addr[0], addr[1]

                # Store the bianry input point value in the response dictionary
                self.storeValues(controllerkey, apdu.objectIdentifier[1], value )

            sys.stdout.flush()

        # do something for error/reject/abort
        if iocb.ioError:
            sys.stdout.write(str(iocb.ioError) + '\n')



    def storeValues(self, controllerKey, objectId, value):
        # This method is for storing in dictionary
        # contrller (key to dict) and the value pair of
        # object id and value

        controllerDict = {}
        if self.responseDict.has_key(str(controllerKey)):
            controllerDict = self.responseDict[str(controllerKey)]

        controllerDict[objectId] = value

        self.responseDict[(str(controllerKey))] = controllerDict

        print str(controllerKey), " Response :", objectId, "  value :", value

    ###############################################
    # This section of code verifing the results of the test
    # that was set.  The use case needs to set the type of test
    # and the call these to verify the tests
    ###############################################
    def verifyExpectedValues(self):
        rtn = False
        verifycount = 0
        if self.isPointListAndExpexctedResultsSet():
            for repKey in self.responseDict:
                repDict = self.responseDict[repKey]
                break

            for key in self.expectedValuesDict:
                if repDict[key] == self.expectedValuesDict[key]:
                    verifycount = verifycount + 1

        if verifycount == len(self.pointsList):
            rtn = True

        self.results = rtn
        return self.results

    def verifyDeviceFound(self):
        self.results = self.isFoundDeviceTargetDevice()
        return self.results

    ###############################################
    # This is the run loop of the application
    # since we have an applicaiton need to call the
    # run method.  When we are done processing we call
    # the stop method and that ends the run method.
    ###############################################

    def runloop(self):
        #   bacpypes is a bacnet client application.
        #   Application meaning we need to call the run() method
        print "Start runloop - Waiting for Iam Requests"
        run()

        # run method is done lets close the socket.
        # need to close the socket so we can call the run
        # method again
        self.close_socket()






