"""
Object Type 'opcodes' module. Defines all opcodes that are used in test scripts when
communicating with the 1000 and 3200 controllers.

These can be accessed once this 'opcodes.py' module has been imported, for example:
    - First, import module as:

        >>> from common.imports import statuscodes import

    - Now,

        >>> print statuscodes.error
        "ER"

        >>> print statuscodes.watering
        "WT"
"""

# Controllers statuses #
disabled = "DS"
unassigned = "UA"
done = "DN"
okay = "OK"
manually_running = "MR"
running = "RN"
watering = "WT"
learning_flow = "LA"
soaking = "SO"
waiting = "WA"
water_empty = "EM"
paused = "PA"
rain_delay = "RD"
rain_jumper = "RS"
off = "OF"
error = "ER"
flow_fault = "FF"
over_budget = "OB"
pressure_fault = "PF"

# BaseManager statuses #
disconnect_from_basemanager = "DC"
disconnected = "DC"
disconnect_basemanager_ethernet = "DS"
distribution_uniformity = "DU"