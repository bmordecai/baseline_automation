"""
Object Type 'enum' class. Defines all object's that are used in test scripts.

These can be accessed once this 'types.py' module has been imported, for example:
    - First, import module as:

        >>> from common.imports import types

    - Now,

        >>> print types.program
        "PG"

        >>> print DeviceCommands.Type.FLOW_METER
        "FM"
"""
from . import opcodes


# general types go below.
program = opcodes.program


class ActionCommands(object):
    SET = opcodes.set_action
    GET = opcodes.get_action
    DO = opcodes.do_action
    ADD = opcodes.add_device_action
    ASSIGN_CONTROLLER = opcodes.assign_controller
    DEVICE = opcodes.device_action
    ASSIGN = opcodes.address_number
    DEVICETEST = opcodes.device_test
    SEARCH = opcodes.search_for_device


class BiCoderCommands(object):
    """
    Enumeration Class for string representations of the type of BiCoder.
    """

    class Type (object):
        VALVE = opcodes.VALVE
        FLOW = opcodes.FLOW
        TEMPERATURE = opcodes.TEMPERATURE
        MOISTURE = opcodes.MOISTURE
        SWITCH = opcodes.SWITCH
        PUMP = opcodes.PUMP
        ANALOG = opcodes.ANALOG
        ALERT = opcodes.ALERT

    class Attributes(object):
        TWO_WIRE_DROP = opcodes.two_wire_drop
        SERIAL_NUMBER = opcodes.serial_number

    class Dictionaries(object):
        """

        """
        TYPE_LOOK_UP = {
            opcodes.zone: opcodes.assign_zone_decoder,
            opcodes.moisture_sensor: opcodes.assign_moisture_sensor_decoder,
            opcodes.master_valve: opcodes.assign_master_valve_decoder,
            opcodes.pump: opcodes.assign_pump_decoder,
            opcodes.flow_meter: opcodes.assign_flow_meter_decoder,
            opcodes.temperature_sensor: opcodes.assign_temperature_Sensor_decoder,
            opcodes.event_switch: opcodes.assign_event_switch_decoder,
            opcodes.alert_relay: opcodes.assign_alert_relay_decoder,
            opcodes.pressure_sensor: opcodes.assign_4to20_milliamp_sensor_decoder
        }
        BICODER_TYPE = {
            'D1': opcodes.single_valve_decoder,
            'D2': opcodes.two_valve_decoder,
            'D4': opcodes.four_valve_decoder,
            'DD': opcodes.twelve_valve_decoder,
            # 'FB': opcodes.flow_decoder,
            # 'MB': opcodes.moisture_decoder,
            # 'TB': opcodes.temperature_decoder,
            # 'AB': opcodes.analog_decoder,
            # 'AR': opcodes.alert_relay_decoder,
        }
        TYPE_LONG = {
            opcodes.ANALOG: "Analog BiCoder",
            opcodes.VALVE: "Valve BiCoder",
            opcodes.FLOW: "Flow BiCoder",
            opcodes.PUMP: "Pump BiCoder",
            opcodes.SWITCH: "Switch BiCoder",
            opcodes.TEMPERATURE: "Temperature BiCoder",
            opcodes.MOISTURE: "Moisture BiCoder",
            opcodes.ALERT: "Alert BiCoder",
        }


class ValveBiCoderCommands(BiCoderCommands):

    class Attributes(BiCoderCommands.Attributes):
        SOLENOID_CURRENT = opcodes.solenoid_current
        SOLENOID_VOLTAGE = opcodes.solenoid_voltage


class FlowBiCoderCommands(BiCoderCommands):
    """

    """

    class Attributes(BiCoderCommands.Attributes):
        K_VALUE = opcodes.k_value
        FLOW_RATE = opcodes.flow_rate
        TOTAL_FLOW_USAGE = opcodes.total_usage


class ProgramCommands(object):
    """
    Common opcodes for all device Types
    Contains different device types (or objects we consider to be "Device").
    """
    Program = opcodes.program

    class Type(object):
        """
        Enumeration Class for string representations of the identifiers of a BiCoder.
        """

    class Attributes(BiCoderCommands.Attributes):
        DESCRIPTION = opcodes.description
        ENABLED = opcodes.enabled
        WATER_WINDOW = opcodes.weekly_water_window
        MAX_CONCURRENT_ZONES = opcodes.max_concurrent_zones
        DAY_INTERVALS = opcodes.day_interval
        INTERVAL_DAYS = opcodes.interval_days
        CALENDAR_INTERVALS = opcodes.calendar_interval
        EVEN_DAYS = opcodes.even_day
        ODD_DAYS = opcodes.odd_day
        ODD_DAYS_SKIP_31 = opcodes.odd_days_skip_31
        WEEKDAYS = opcodes.week_days
        WEEKlY_WATERING = opcodes.weekly_water_window
        SEASONAL_ADJUST = opcodes.seasonal_adjust
        START_TIMES = opcodes.start_times
        SEMI_MOTHLY_WATERING = opcodes.program_semi_month_interval
        PRIORITY = opcodes.priority
        BOOSTER_PUMP = opcodes.booster_pump
        HISTORICAL_ET_CALENDER = opcodes.historical_calendar
        IGNORE_GLOBAL_CONDITIONS = opcodes.ignore_global_conditions
        IGNORE_ZONE_CONCURRENCY = opcodes.ignore_concurrent_zones

class ProgramConditionCommands(object):
    """
    Enums for Program Start/Stop/Pause Conditions
    """
    Program = opcodes.program

    class Type(object):
        """
        Enumeration Class for string representations of the identifiers of a BiCoder.
        """
        START = opcodes.program_start_condition
        STOP = opcodes.program_stop_condition
        PAUSE = opcodes.program_pause_condition

    class Attributes(BiCoderCommands.Attributes):
        ENABLED = opcodes.enabled
        STOP_IMMEDIATELY = opcodes.stop_immediately
        MOISTURE_MODE = opcodes.moisture_mode
        MOISTURE_THRESHOLD = opcodes.moisture_trigger_threshold
        MOISTURE_SENSOR = opcodes.moisture_sensor
        MOISTURE_PAUSE_TIME = opcodes.moisture_pause_time
        TEMPERATURE_MODE = opcodes.temperature_sensor_mode
        TEMPERATURE_THRESHOLD = opcodes.temperature_trigger_threshold
        TEMPERATURE_SENSOR = opcodes.temperature_sensor
        TEMPERATURE_PAUSE_TIME = opcodes.temperature_pause_time
        SWITCH_MODE = opcodes.switch_mode
        EVENT_SWITCH = opcodes.event_switch
        SWITCH_PAUSE_TIME = opcodes.switch_pause_time
        OFF_MODE = opcodes.off
        UPPER_LIMIT_MODE = opcodes.upper_limit
        LOWER_LIMIT_MODE = opcodes.lower_limit
        OPEN_MODE = opcodes.open
        CLOSED_MODE = opcodes.closed


class EmptyConditionCommands(object):
    """
    Enums for WaterSource Empty Conditions
    """
    Watersource = opcodes.water_source

    class Type(object):
        """
        Enumeration Class for string representations of the identifiers of a BiCoder.
        """
        EMPTY = opcodes.empty_condition

    class Attributes(BiCoderCommands.Attributes):
        ENABLED = opcodes.enabled


class FullConditionCommands(object):
    """
    Enums for WaterSource Empty Conditions
    """
    Watersource = opcodes.water_source

    class Type(object):
        """
        Enumeration Class for string representations of the identifiers of a BiCoder.
        """
        FULL = opcodes.full_condition

    class Attributes(BiCoderCommands.Attributes):
        ENABLED = opcodes.enabled


class DeviceCommands(object):
    """
    Common opcodes for all device Types
    Contains different device types (or objects we consider to be "Device").
    """

    class Type(object):
        """
        Enumeration Class for string representations of the identifiers of a BiCoder.
        """
        CONTROLLER = opcodes.controller
        FLOWSTATION = opcodes.flow_station
        SUBSTATION = opcodes.substation
        EVENT_SWITCH = opcodes.event_switch
        FLOW_METER = opcodes.flow_meter
        MASTER_VALVE= opcodes.master_valve
        PUMP = opcodes.pump
        PRESSURE_SENSOR = opcodes.pressure_sensor
        MOISTURE_SENSOR = opcodes.moisture_sensor
        TEMPERATURE_SENSOR = opcodes.temperature_sensor
        ZONE = opcodes.zone
        ALERT_RELAY = opcodes.alert_relay
        ANALOG_BICODER = opcodes.analog_bicoder

    class Attributes(BiCoderCommands.Attributes):
        DESCRIPTION = opcodes.description
        DEVICE_ADDRESS = opcodes.address_number
        ENABLED = opcodes.enabled
        LATITUDE = opcodes.latitude
        LONGITUDE = opcodes.longitude
        FIRMWARE_VERSION = opcodes.firmware_version

    class Dictionaries(object):
        DEVICE_ASSIGNMENTS = {
            opcodes.zone: opcodes.assign_zone_decoder,
            opcodes.moisture_sensor: opcodes.assign_moisture_sensor_decoder,
            opcodes.master_valve: opcodes.assign_master_valve_decoder,
            opcodes.pump: opcodes.assign_pump_decoder,
            opcodes.flow_meter: opcodes.assign_flow_meter_decoder,
            opcodes.temperature_sensor: opcodes.assign_temperature_Sensor_decoder,
            opcodes.event_switch: opcodes.assign_event_switch_decoder,
            opcodes.alert_relay: opcodes.assign_alert_relay_decoder,
            opcodes.pressure_sensor: opcodes.pressure_sensor
            }
        DEFAULT_DESCRIPTIONS = {
            opcodes.zone: ["Test Zone"],
            opcodes.moisture_sensor: ["Test Moisture Sensor"],
            opcodes.master_valve:["Test Master Valve"],
            opcodes.pump: ["Test Pump"],
            opcodes.flow_meter:["Test Flow Meter"],
            opcodes.temperature_sensor: ["Test Temp Sensor"],
            opcodes.event_switch: ["Test Event Switch"],
            opcodes.alert_relay: ["Test Alert Relay"],
            opcodes.pressure_sensor: ["Test Pressure Sensor"],
            }


class ControllerCommands(DeviceCommands):
    """

    """
    Controller = opcodes.controller
    REBOOT = opcodes.reboot

    class Type(DeviceCommands.Type):
        """

        """
        BASESTATION_1000 = opcodes.basestation_1000
        BASESTATION_3200 = opcodes.basestation_3200
        FLOWSTATION = opcodes.flow_station
        SUBSTATION = opcodes.substation
        TWSIMULATOR = opcodes.tw_simulator

    class Attributes(DeviceCommands.Attributes):
        """

        """
        DATETIME = opcodes.date_time
        MAC_ADDRESS = opcodes.mac_address
        MAX_CONCURRENT_ZONES = opcodes.max_concurrent_zones
        RAIN_PAUSE_DAYS = opcodes.rain_pause_days
        RAIN_JUMPER = opcodes.rain_jumper
        FLOW_JUMPER = opcodes.flow_jumper
        PAUSE_JUMPER = opcodes.pause_jumper
        MEMORY_USAGE = opcodes.memory_usage
        CURRENT = opcodes.solenoid_current
        VOLTAGE = opcodes.solenoid_voltage

    class Dictionaries(DeviceCommands.Dictionaries):
        """

        """


class FlowStationCommands(DeviceCommands):
    """

    """
    FlowStation = opcodes.flow_station

    class Type(DeviceCommands.Type):
        pass

    class Attributes(DeviceCommands.Attributes):
        """

        """
        MAX_COUNTS = opcodes.max_counts
        MAX_DEVICES = opcodes.controller
        MAX_CONTROLLERS = opcodes.controller
        MAX_WATER_SOURCES = opcodes.water_source
        MAX_POINTS_OF_CONTROL = opcodes.point_of_control
        MAX_MAINLINES = opcodes.mainline


class ZoneCommands(DeviceCommands):
    """

    """
    Zones = opcodes.zone

    class Attributes(DeviceCommands.Attributes):
        """

        """
        SERIAL_NUMBER = BiCoderCommands.Attributes.SERIAL_NUMBER
        DESIGN_FLOW = opcodes.design_flow
        CROP_COEFFICIENT = opcodes.crop_coefficient
        PRECIPITATION_RATE = opcodes.precipitation_rate
        ROOT_ZONE_WATER_CAPACITY = opcodes.root_zone_working_water_storage
        EFFICIENCY_PERCENT = opcodes.efficiency_percentage
        MOISTURE_BALANCE = opcodes.daily_moisture_balance
        MAINLINE = opcodes.mainline
        MIRRORED_ZONE = opcodes.mirror_zone
        MASTER_VALVE = opcodes.master_valve
        HIGH_FLOW_VARIANCE = opcodes.high_flow_variance
        CYCLE_TIME = opcodes.cycle_time
        SOAK_TIME = opcodes.soak_time

    class Dictionaries(DeviceCommands.Dictionaries):
        """

        """


class ZoneProgramCommands(object):
    """

    """
    Zones = opcodes.zone
    Programs = opcodes.program
    PointsOfControl = opcodes.point_of_control

    class Attributes(object):
        """

        """

    class Dictionaries(object):
        """

        """


class FlowMeterCommands(DeviceCommands):
    """

    """
    Flow_Meter = opcodes.flow_meter

    class Attributes(DeviceCommands.Attributes):
        """

        """
        TYPE = opcodes.type
        K_VALUE = opcodes.k_value

    class Dictionaries(DeviceCommands.Dictionaries):
        """

        """


class PumpCommands(DeviceCommands):
    """

    """
    Pump = opcodes.pump

    class Attributes(DeviceCommands.Attributes):
        """

        """
        TYPE = opcodes.type
        BOOSTER = opcodes.booster_pump

    class Dictionaries(DeviceCommands.Dictionaries):
        """

        """


class PressureSensorCommands(DeviceCommands):
    """

    """
    Pressure_Sensor = opcodes.pressure_sensor

    class Attributes(DeviceCommands.Attributes):
        """

        """
        TYPE = opcodes.type

    class Dictionaries(DeviceCommands.Dictionaries):
        """

        """

class MasterValvesCommands(DeviceCommands):
    """

    """
    MasterValves = opcodes.master_valve

    class Attributes(DeviceCommands.Attributes):
        """

        """
        TYPE = opcodes.type
        BOOSTER = opcodes.booster_pump
        NORMALLY_OPEN = opcodes.normally_open

    class Dictionaries(DeviceCommands.Dictionaries):
        """

        """


class TemperatureSensorCommands(DeviceCommands):
    """

    """
    Temperature_Sensor = opcodes.temperature_sensor

    class Attributes(DeviceCommands.Attributes):
        """

        """
        TYPE = opcodes.type
        DESCRIPTION = DeviceCommands.Attributes.DESCRIPTION

    class Dictionaries(DeviceCommands.Dictionaries):
        """

        """


class MoistureSensorCommands(DeviceCommands):
    """

    """
    Moisture_Sensor = opcodes.moisture_sensor

    class Attributes(DeviceCommands.Attributes):
        """

        """
        TYPE = opcodes.type


class WaterSourceCommands(object):
    """

    """
    Water_source = opcodes.water_source

    class Attributes(object):
        """

        """
        TYPE = opcodes.type
        DESCRIPTION = opcodes.description
        ENABLED = opcodes.enabled
        PRIORITY = opcodes.priority
        MONTHLY_BUDGET = opcodes.monthly_water_budget
        WATER_RATIONING = opcodes.water_rationing_enable
        POINT_OF_CONTROL = opcodes.point_of_control

    class Dictionaries(DeviceCommands.Dictionaries):
        """

        """


class EventSwitchCommands(DeviceCommands):
    """

    """
    Event_Switch = opcodes.event_switch

    class Attributes(DeviceCommands.Attributes):
        """

        """
        TYPE = opcodes.type


class MainlineCommands(object):
    """

    """
    Mainline = opcodes.mainline

    class Attributes(object):
        """

        """
        TYPE = opcodes.type
        DESCRIPTION = opcodes.description
        PRIORITY = opcodes.priority
        MAINLINE = opcodes.mainline
        POINT_OF_CONTROL = opcodes.point_of_control


class PointofControlCommands(object):
    """

    """

    PointOfControl = opcodes.point_of_control

    class Attributes(object):
        """

        """
        PUMP = opcodes.pump_on_poc
        GROUP = opcodes.group

    class Dictionaries(DeviceCommands.Dictionaries):
        """

        """


class PauseConditionCommands(object):
    """

    """
    Program = opcodes.program

    class Attributes(object):
        """

        """


    class Dictionaries(DeviceCommands.Dictionaries):
        """

        """


class StopConditionCommands(object):
    """

    """
    Program = opcodes.program

    class Attributes(object):
        """

        """


    class Dictionaries(DeviceCommands.Dictionaries):
        """

        """


class StartConditionCommands(object):
    """

    """
    Program = opcodes.program

    class Attributes(object):
        """

        """


    class Dictionaries(DeviceCommands.Dictionaries):
        """

        """


class BasemanagerCommands(object):
    """

    """
    BaseManager = opcodes.basemanager
    class Attributes(object):
        """

        """

    class Dictionaries(DeviceCommands.Dictionaries):
        """

        """


class ObjectTypes(object):
    obj_dict = {
        opcodes.zone: "Zone",
        opcodes.alert_relay: "Alert Relay",
        opcodes.flow_meter: "Flow Meter",
        opcodes.moisture_sensor: "Moisture Sensor",
        opcodes.master_valve: "Master Valve",
        opcodes.pump: "Pump",
        opcodes.pressure_sensor: "Pressure Sensor",
        opcodes.event_switch: "Event Switch",
        opcodes.temperature_sensor: "Temperature Sensor",
        opcodes.program: "Program",
        opcodes.mainline: "Mainline",
        opcodes.point_of_control: "Point of Control",
        opcodes.water_source: "Water Source",
        opcodes.zone_program: "Zone Program",
        opcodes.water_source_empty_condition: "Empty Condition",
        opcodes.empty_condition: "Empty Condition",
        opcodes.full_condition: "Full Condition",
        opcodes.program_start_condition: "Start Condition",
        opcodes.program_stop_condition: "Stop Condition",
        opcodes.program_pause_condition: "Pause Condition",
        opcodes.controller: "Controller",
        opcodes.flow_station: "FlowStation",
        opcodes.basemanager: "BaseManager",
        opcodes.type: "Type",
    }


class FileTypeCategory(object):
    """
    Contains different File category types
    """
    live_view = opcodes.live_view
    firm_update = opcodes.firmware_update
    firm_update_with_handshack = opcodes.firmware_update_with_handshake
    config_store_restore = opcodes.config_store_restore
    event_daily_log = opcodes.event_daily_log
    event_full_log = opcodes.event_full_log
    program_summary = opcodes.program_summary
    water_rate_log = opcodes.water_rate_log


class MessageCategory(object):
    """
    Contains different message category types
    """
    basemanager = opcodes.basemanager
    controller = opcodes.controller
    event_switch = opcodes.event_switch
    flow_meter = opcodes.flow_meter
    flow_station = opcodes.flow_station
    mainline = opcodes.mainline
    moisture_sensor = opcodes.moisture_sensor
    master_valve = opcodes.master_valve
    point_of_connection = opcodes.point_of_connection
    program = opcodes.program
    pump_station = opcodes.pump_station
    temperature_sensor = opcodes.temperature_sensor
    zone = opcodes.zone
    zone_program =opcodes.zone_program


class MessagePriority(object):
    low = 'LW'
    medium = 'MD'
    high = 'HH'
    none = 'NN'


class Message(object):
    """
    Contains different message types and status codes used.
    """
    admin = opcodes.admin
    bad_reading = opcodes.bad_reading
    bad_serial = opcodes.bad_serial
    base_manager = opcodes.basemanager
    base_manager_source = opcodes.basemanager_source
    boot_up = opcodes.boot_up
    budget_exceeded = "BD"
    budget_exceeded_shutdown = opcodes.budget_exceeded_shutdown
    calibrate_failure_no_change = opcodes.calibrate_failure_no_change
    calibrate_failure_no_saturation = opcodes.calibrate_failure_no_saturation
    calibrate_successful = opcodes.calibrate_successful
    checksum = opcodes.checksum
    commander_paused = opcodes.commander_paused
    configuration_error = opcodes.configuration_error
    connection_error = opcodes.connection_error
    day_time = opcodes.date_time
    day_and_time_done = opcodes.date_time
    device_error = "DE"
    device_disabled = opcodes.disabled
    disabled = opcodes.disabled
    empty_shutdown = opcodes.empty_shutdown
    empty_condition_with_pressure_sensor = opcodes.empty_condition_with_pressure_sensor
    empty_condition_with_moisture_sensor = opcodes.empty_condition_with_moisture_sensor
    empty_condition_with_event_switch = opcodes.empty_condition_with_event_Switch
    error_operation_terminated = opcodes.error_operation_terminated
    event_date_stop = opcodes.event_date_stop
    event_day = opcodes.event_day
    event_switch = opcodes.event_switch
    exceeds_design_flow = opcodes.exceeds_design_flow
    exceeds_design_flow_1000 = opcodes.exceeds_design_flow_1000
    fallback_mode_booster_pump = opcodes.fallback_mode_booster_pump
    fallback_mode_program = opcodes.fallback_mode_program
    fallback_mode_poc = opcodes.fallback_mode_poc
    flow_jumper_stopped = opcodes.flow_jumper_stopped
    flow_learn_errors = opcodes.flow_learn_errors
    flow_learn_ok = opcodes.flow_learn_ok
    high_flow_detected = opcodes.high_flow_detected
    high_flow_shutdown = opcodes.high_flow_shutdown
    high_flow_shutdown_by_flow_station = opcodes.high_flow_shutdown_by_flow_station
    high_flow_variance_detected = opcodes.high_flow_variance_detected
    high_flow_variance_shutdown = opcodes.high_flow_variance_shutdown
    high_pressure_detected = "PH"
    high_pressure_shutdown = "PS"
    internal_failure = opcodes.internal_failure
    learn_flow_complete_errors = opcodes.learn_flow_with_errors
    learn_flow_complete_success = opcodes.learn_flow_success
    learn_flow_fail_flow_biCoders_disabled = opcodes.learn_flow_fail_flow_biCoders_disabled
    learn_flow_fail_flow_biCoders_error = opcodes.learn_flow_fail_flow_biCoders_error
    learn_flow_with_errors = opcodes.learn_flow_with_errors
    learn_flow_success = opcodes.learn_flow_success
    low_flow_shutdown_by_flow_station = opcodes.low_flow_shutdown_by_flow_station
    low_flow_variance_detected = opcodes.low_flow_variance_detected
    low_flow_variance_shutdown = opcodes.low_flow_variance_shutdown
    low_pressure_detected = "PL"
    low_pressure_shutdown = "PX"
    low_voltage = "LV"
    memory_usage = opcodes.memory_usage
    message = opcodes.message
    missing_24_vac = opcodes.no_24_vac
    moisture = opcodes.moisture
    moisture_sensor_calibration_failure_no_change = opcodes.calibrate_failure_no_change
    moisture_sensor_calibration_failure_no_saturation = opcodes.calibrate_failure_no_saturation
    moisture_sensor_calibration_success = opcodes.calibrate_successful
    no_eto_data = opcodes.no_et_data
    no_message = opcodes.no_message
    no_24_vac = opcodes.no_24_vac
    no_response = opcodes.no_response
    off = opcodes.off
    open_circuit = opcodes.open_circuit
    operator = opcodes.operator
    open_circuit_1000 = "OP"
    over_run_start_event = opcodes.over_run_start_event
    pause = "PU"
    pause_event_switch = opcodes.pause_event_switch
    pause_jumper = "PJ"
    pause_moisture_sensor = opcodes.pause_moisture_sensor
    pause_pressure_sensor = opcodes.pause_pressure_sensor
    pause_temp_sensor = opcodes.pause_temp_sensor
    priority_paused = opcodes.priority_paused
    program_overrun = opcodes.program_overrun
    programmer = opcodes.programmer
    rain_delay = opcodes.rain_delay
    rain_delay_stopped = opcodes.rain_delay_stopped
    rain_jumper_stopped = opcodes.rain_jumper_stopped
    rain_switch = opcodes.rain_switch
    restore_failed = opcodes.restore_failed
    restore_successful = opcodes.restore_successful
    restricted_time_by_water_ration = opcodes.restricted_time_by_water_ration
    requires_soak_cycle = opcodes.requires_soak_cycle
    sensor_disabled = opcodes.sensor_disabled
    serial_number = "DV"
    set_upper_limit_failed = opcodes.set_upper_limit_failed
    short_circuit = opcodes.short_circuit
    short_circuit_1000 = "OC"
    skipped_by_moisture_sensor = opcodes.skipped_by_moisture_sensor
    start = "ST"
    started_event_switch = opcodes.started_event_switch
    started_moisture_sensor = opcodes.started_moisture_sensor
    started_temp_sensor = opcodes.started_temp_sensor
    started_pressure_sensor = opcodes.started_pressure_sensor
    started_by_bad_moisture_sensor = opcodes.started_by_bad_moisture_sensor
    stop = "SP"
    stop_event_day = opcodes.event_date_stop
    stop_event_switch = opcodes.stop_event_switch
    stop_moisture_sensor = opcodes.stop_moisture_sensor
    stop_temp_sensor = opcodes.stop_temp_sensor
    stop_pressure_sensor = opcodes.stop_pressure_sensor
    system = opcodes.system
    system_off = opcodes.system_off
    temperature = opcodes.temperature_sensor
    two_wire_high_current_shutdown = opcodes.two_wire_high_current_shutdown
    two_wire_over_current = "OC"
    unscheduled_flow_detected = opcodes.unscheduled_flow_detected
    unscheduled_flow_shutdown = opcodes.unscheduled_flow_shutdown
    variable_one = "V1"
    variable_two = "V2"
    variance_shutdown = opcodes.variance_shutdown
    usb_flash_storage_failure = opcodes.usb_flash_storage_failure
    usb_flash_storage_source = opcodes.usb_flash_storage_source
    user = opcodes.user
    water_window_paused = opcodes.water_window_paused
    water_window = opcodes.water_window_paused
    weather_eto_data_not_available = "ET"
    zero_reading = opcodes.zero_reading
    zone_learn_flow_complete_errors = opcodes.flow_learn_errors
    zone_learn_flow_complete_success = opcodes.flow_learn_ok
    invalid_reading = opcodes.invalid_reading


class IrrigationSystem(object):
    """
    Contains the different types of sprinklers and other irrigation systems used for EPA programming.
    """
    bubblers = "Bubblers"
    impact = "Impacts"
    popup_rotory_heads = "Popup Rotors Heads"
    popup_spray_heads = "Popup Spray Heads"
    rotors = "Rotors"
    surface_drip = "Surface Drip"


class SoilTexture(object):
    """
    Contains different soil textures used for EPA programming.
    """
    clay = "Clay"
    clay_loam = "Clay Loam"
    loam = "Loam"
    loamy_sand = "Loamy Sand"
    sand = "Sand"
    sandy_loam = "Sandy Loam"
    silty_clay = "Silty Clay"


# class StartStopPauseCondition(object):
#     """
#     Contains the different program start/stop/pause condition types.
#     """
#     pause = opcodes.program_pause_condition     # "PS"
#     start = opcodes.program_start_condition     # "PT"
#     stop = opcodes.program_stop_condition       # "PP"
#
#
# class StartStopPauseEvent(object):
#     """
#     Contains the different program start/stop/pause event types.
#     """
#     date_time = opcodes.date_time               # "DT"
#     event_switch = opcodes.event_switch         # "SW"
#     event_switch = opcodes.event_switch   # "MS"
#     temp_sensor = opcodes.event_switch    # "TS"
#     weather_based = opcodes.program_event_weather_based     # "ET"


class Vegetation(object):
    """
    Contains the different types of vegetation used for EPA programming.
    """
    bermuda = "Bermuda"
    fescue = "Fescue"
    ground_cover = "Ground Cover"
    trees_and_ground_cover = "Trees and Ground Cover"
    woody_shrubs = "Woody Shrubs"


status_code_dict_1000 = {
    MessageCategory.controller: [
        Message.two_wire_over_current
    ],
    MessageCategory.event_switch: [
        Message.checksum,
        Message.no_response,
        Message.bad_serial
    ],
    MessageCategory.flow_meter: [
        Message.checksum,
        Message.no_response,
        Message.bad_serial
    ],
    MessageCategory.moisture_sensor: [
        Message.checksum,
        Message.no_response,
        Message.bad_serial
    ],
    MessageCategory.master_valve: [
        Message.checksum,
        Message.no_response,
        Message.bad_serial,
        Message.low_voltage,
        "OP",
        "OC"
    ],
    MessageCategory.point_of_connection: [
        Message.device_error,
        Message.high_flow_shutdown,
        Message.unscheduled_flow_shutdown
    ],
    MessageCategory.program: [
        Message.over_run_start_event,
        Message.learn_flow_with_errors,
        Message.calibrate_failure_no_change,
        Message.start,
        Message.pause,
        Message.stop
    ],
    MessageCategory.temperature_sensor: [
        Message.checksum,
        Message.no_response,
        Message.bad_serial
    ],
    MessageCategory.zone: [
        Message.checksum,
        Message.no_response,
        Message.bad_serial,
        Message.low_voltage,
        Message.open_circuit_1000,
        Message.short_circuit_1000,
        Message.high_flow_variance_shutdown
    ]
}

# HOW TO USE
# pg=_status_code_dict_3200[cy_type][_status_code]
# This dictionary is used to iterate through the status codes of each object
status_plus_priority_code_dict_3200 = {
    MessageCategory.controller: {
        Message.commander_paused: MessagePriority.low,
        Message.boot_up: MessagePriority.medium,
        Message.event_date_stop: MessagePriority.low,
        Message.usb_flash_storage_failure: MessagePriority.medium,
        Message.flow_jumper_stopped: MessagePriority.high,
        Message.two_wire_high_current_shutdown: MessagePriority.high,
        Message.pause_event_switch: MessagePriority.low,
        Message.pause_jumper: MessagePriority.low,
        Message.pause_moisture_sensor: MessagePriority.low,
        Message.pause_temp_sensor: MessagePriority.low,
        Message.rain_delay_stopped: MessagePriority.low,
        Message.rain_jumper_stopped: MessagePriority.low,
        Message.stop_event_switch: MessagePriority.low,
        Message.stop_moisture_sensor: MessagePriority.low,
        Message.stop_temp_sensor: MessagePriority.low,
        Message.internal_failure: MessagePriority.high,
        Message.restore_failed: MessagePriority.high,
        Message.restore_successful: MessagePriority.low,
    },
    MessageCategory.event_switch: {
        Message.bad_serial: MessagePriority.low,
        Message.no_response: MessagePriority.high,
    },
    MessageCategory.flow_meter: {
        Message.bad_serial: MessagePriority.low,
        Message.no_response: MessagePriority.high,
        Message.set_upper_limit_failed: MessagePriority.medium,
    },
    MessageCategory.flow_station: {
        Message.bad_serial: MessagePriority.low,
        Message.no_response: MessagePriority.high,
        Message.set_upper_limit_failed: MessagePriority.medium,
        Message.fallback_mode_booster_pump: MessagePriority.medium,
        Message.fallback_mode_program: MessagePriority.medium,
        Message.fallback_mode_poc: MessagePriority.medium,
        Message.error_operation_terminated: MessagePriority.medium,
    },
    MessageCategory.mainline: {
        Message.learn_flow_fail_flow_biCoders_disabled: MessagePriority.medium,
        Message.learn_flow_fail_flow_biCoders_error: MessagePriority.medium,
        Message.high_flow_variance_detected: MessagePriority.medium,
        Message.low_flow_variance_detected: MessagePriority.low,
    },
    MessageCategory.moisture_sensor: {
        Message.bad_serial: MessagePriority.low,
        Message.sensor_disabled: MessagePriority.medium,
        Message.no_response: MessagePriority.high,
        Message.zero_reading: MessagePriority.medium,
    },
    MessageCategory.master_valve: {
        Message.bad_serial: MessagePriority.low,
        Message.no_response: MessagePriority.medium,
        Message.open_circuit: MessagePriority.medium,
        Message.short_circuit: MessagePriority.medium,
    },
    MessageCategory.point_of_connection: {
        Message.budget_exceeded: MessagePriority.low,
        Message.budget_exceeded_shutdown: MessagePriority.medium,
        Message.high_flow_detected: MessagePriority.high,
        Message.high_flow_shutdown: MessagePriority.high,
        Message.empty_shutdown: MessagePriority.medium,
        Message.unscheduled_flow_detected: MessagePriority.low,
        Message.unscheduled_flow_shutdown: MessagePriority.high,
    },
    MessageCategory.program: {
        Message.event_date_stop: MessagePriority.none,
        Message.skipped_by_moisture_sensor: MessagePriority.low,
        Message.learn_flow_with_errors: MessagePriority.medium,
        Message.learn_flow_success: MessagePriority.low,
        Message.started_by_bad_moisture_sensor: MessagePriority.high,
        Message.over_run_start_event: MessagePriority.medium,
        Message.pause_event_switch: MessagePriority.none,
        Message.pause_moisture_sensor: MessagePriority.none,
        Message.priority_paused: MessagePriority.none,
        Message.pause_temp_sensor: MessagePriority.none,
        Message.restricted_time_by_water_ration: MessagePriority.none,
        Message.stop_event_switch: MessagePriority.none,
        Message.stop_moisture_sensor: MessagePriority.none,
        Message.stop_temp_sensor: MessagePriority.none,
        Message.started_event_switch: MessagePriority.none,
        Message.started_moisture_sensor: MessagePriority.none,
        Message.started_temp_sensor: MessagePriority.none,
        Message.water_window_paused: MessagePriority.none,
    },
    MessageCategory.temperature_sensor: {
        Message.bad_serial: MessagePriority.low,
        Message.no_response: MessagePriority.high,
    },
    MessageCategory.zone: {
        Message.bad_serial: MessagePriority.medium,
        Message.no_24_vac: MessagePriority.high,
        Message.no_response: MessagePriority.high,
        Message.open_circuit: MessagePriority.medium,
        Message.short_circuit: MessagePriority.medium,
    },
    MessageCategory.zone_program: {
        Message.calibrate_failure_no_change: MessagePriority.medium,
        Message.calibrate_successful: MessagePriority.none,
        Message.calibrate_failure_no_saturation: MessagePriority.medium,
        Message.exceeds_design_flow: MessagePriority.medium,
        Message.flow_learn_errors: MessagePriority.medium,
        Message.flow_learn_ok: MessagePriority.low,
        Message.high_flow_shutdown_by_flow_station: MessagePriority.high,
        Message.high_flow_variance_shutdown: MessagePriority.high,
        Message.high_flow_variance_detected: MessagePriority.high,
        Message.low_flow_shutdown_by_flow_station: MessagePriority.medium,
        Message.low_flow_variance_shutdown: MessagePriority.medium,
        Message.low_flow_variance_detected: MessagePriority.medium,
        Message.requires_soak_cycle: MessagePriority.medium,
    }
}


program_1000_who_dict = {
    Message.start: {
        Message.base_manager: "BaseManager",
        Message.user: "User",
        Message.operator: "Operator",
        Message.programmer: "Programmer",
        Message.admin: "Admin",
        Message.moisture: "Moisture",
        Message.event_switch: "E. Switch",
        Message.temperature: "Temperature",
        Message.day_time: "Day & Time"
    },
    Message.pause: {
        Message.system: "System Wait",
        Message.moisture: "Moisture",
        Message.event_switch: "E. Switch",
        Message.temperature: "Temperature",
        Message.water_window: "Water Win.",
        Message.event_day: "Event Day",
    },
    Message.stop: {
        Message.base_manager: "BaseManager",
        Message.user: "User",
        Message.operator: "Operator",
        Message.programmer: "Programmer",
        Message.admin: "Admin",
        Message.moisture: "Moisture",
        Message.event_switch: "E. Switch",
        Message.temperature: "Temperature",
        Message.day_and_time_done: "Day & Time",
        Message.system_off: "System Off",
        Message.rain_switch: "Rain Switch",
        Message.rain_delay: "Rain Delay"
    }
}