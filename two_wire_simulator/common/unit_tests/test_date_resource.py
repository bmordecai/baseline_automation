__author__ = 'Baseline'

import unittest
import mock
import datetime
from dateutil.parser import parse
from common.date_package import date_resource
from common.date_package.date_resource import date_mngr


class TestDateResourceObject(unittest.TestCase):
    """
    Unit test class for date_package.date_resource.py
    """

    ########################################
    def setUp(self):
        """ Setting up for the test. """
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + self.shortDescription())

    ########################################
    def tearDown(self):
        """ Cleaning up after the test. """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_date_object_set_from_datetime_obj_pass1(self):
        """ Verifies valid attribute values set """
        datetime_obj = parse("2015-10-06").date()
        date_obj = date_resource.Date()
        date_obj.set_from_datetime_obj(datetime_obj=datetime_obj)
        self.assertEqual(date_obj.weekDayIndex, datetime_obj.weekday())
        self.assertEqual(date_obj.obj, datetime_obj)
        self.assertEqual(date_obj.day, 6)
        self.assertEqual(date_obj.year, 2015)
        self.assertEqual(date_obj.weekDayKey, date_resource.tuesday_key)
        self.assertEqual(date_obj.weekDayName, "Tuesday")
        self.assertEqual(date_obj.monthKey, "OCT")
        self.assertEqual(date_obj.monthName, "October")

    def test_date_mngr_init_pass(self):
        """ Verifies valid init of date_mngr object instance """
        date_mngr = date_resource.DateManager()
        self.assertIsInstance(date_mngr.curr_day, date_resource.Date)
        self.assertIsInstance(date_mngr.next_day, date_resource.Date)
        self.assertIsInstance(date_mngr.prev_day, date_resource.Date)

    def test_date_mngr_set_dates_from_date_range_pass1(self):
        """ Verifies a valid date range contained within date_mngr for dates passed in """
        sdate = "10-06-2015"
        edate = "10-13-2015"
        sdate_datetime = parse(sdate).date()
        edate_datetime = parse(edate).date()
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(sdate_datetime, date_mngr.begin_date)
        self.assertEqual(edate_datetime, date_mngr.end_date)

    def test_date_mngr_set_dates_from_date_range_pass2(self):
        """ Verifies a valid date range contained within date_mngr for dates passed in """
        sdate = "02-01-2015"
        edate = "02-28-2015"
        sdate_datetime = parse(sdate).date()
        edate_datetime = parse(edate).date()
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(sdate_datetime, date_mngr.begin_date)
        self.assertEqual(edate_datetime, date_mngr.end_date)

    def test_date_mngr_set_dates_from_date_range_pass3(self):
        """ Verifies a valid date range within date_mngr when the start/end dates passed in are in the same week """
        sdate = "02-02-2015"  # A Monday (considered first day of the week)
        edate = "02-07-2015"  # A Sunday (considered last day of the week)
        sdate_datetime = parse(sdate).date()
        edate_datetime = parse(edate).date()
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(sdate_datetime, date_mngr.begin_date)
        self.assertEqual(edate_datetime, date_mngr.end_date)

    def test_date_mngr_set_dates_from_date_range_pass4(self):
        """ Verifies a valid date range within date_mngr when the start/end dates are farther than a month apart """
        sdate = "05-29-2016"  # A Monday (considered first day of the week)
        edate = "07-09-2016"  # A Sunday (considered last day of the week)
        sdate_datetime = parse(sdate).date()
        edate_datetime = parse(edate).date()
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(sdate_datetime, date_mngr.begin_date)
        self.assertEqual(edate_datetime, date_mngr.end_date)

    def test_add_new_week_to_front_for_intial_prev_day_pass1(self):
        """ Verifies the parser adds a week at the beginning of the date range if the start date falls on a monday """
        sdate = "06-01-2015"
        edate = "06-30-2015"
        sdate_datetime = parse(sdate).date()
        edate_datetime = parse(edate).date()
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(first=date_mngr.prev_day.day, second=31)
        self.assertEqual(first=date_mngr.prev_day.weekDayKey, second=date_resource.sunday_key)

    def test_add_new_week_to_front_for_intial_prev_day_pass2(self):
        """ Verifies that a week is added before the initial start day """
        sdate = "01-01-2016"
        edate = "01-30-2016"
        sdate_datetime = parse(sdate).date()
        edate_datetime = parse(edate).date()
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        date_mngr.add_new_week_to_front_for_intial_prev_day()
        self.assertEqual(first=date_mngr.prev_day.day, second=31)
        self.assertEqual(first=date_mngr.prev_day.weekDayKey, second=date_resource.thursday_key)

    def test_add_addtnl_week_for_final_next_day_pass1(self):
        """ Verifies the parser adds a final week onto the range of days if the month ends on a sunday """
        sdate = "05-01-2015"
        edate = "05-31-2015"
        sdate_datetime = parse(sdate).date()
        edate_datetime = parse(edate).date()
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)

        while date_mngr.reached_last_date() != True:
            date_mngr.skip_to_next_day()

        # a passing test must mean that as our "current day", the "next day" should be the 1st of the next month
        self.assertEqual(first=date_mngr.next_day.day, second=1)
        self.assertEqual(first=date_mngr.next_day.weekDayKey, second=date_resource.monday_key)


    def test_date_mngr_skip_to_next_day_pass1(self):
        """ Verifies a valid iteration to the next day from the current day """
        sdate = "10-06-2015"
        cdate = "10-07-2015"
        ndate = "10-08-2015"
        edate = "10-13-2015"
        sdate_datetime = parse(sdate).date()
        cdate_datetime = parse(cdate).date()
        ndate_datetime = parse(ndate).date()
        edate_datetime = parse(edate).date()

        prev_date = date_resource.Date()
        prev_date.set_from_datetime_obj(sdate_datetime)

        expected_curr_date = date_resource.Date()
        expected_curr_date.set_from_datetime_obj(cdate_datetime)

        expected_next_date = date_resource.Date()
        expected_next_date.set_from_datetime_obj(ndate_datetime)

        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(sdate_datetime, date_mngr.curr_day.obj)

        date_mngr.skip_to_next_day()
        self.assertEqual(prev_date.obj, date_mngr.prev_day.obj)
        self.assertEqual(expected_curr_date.obj, date_mngr.curr_day.obj)
        self.assertEqual(expected_next_date.obj, date_mngr.next_day.obj)

    def test_date_mngr_skip_to_next_day_pass2(self):
        """ Verifies a valid next day is set if trying to move current day from saturday to sunday which changes
        current week index """
        sdate = "10-10-2015"
        cdate = "10-11-2015"
        ndate = "10-12-2015"
        edate = "10-14-2015"
        sdate_datetime = parse(sdate).date()
        cdate_datetime = parse(cdate).date()
        ndate_datetime = parse(ndate).date()
        edate_datetime = parse(edate).date()

        prev_date = date_resource.Date()
        prev_date.set_from_datetime_obj(sdate_datetime)

        curr_date = date_resource.Date()
        curr_date.set_from_datetime_obj(cdate_datetime)

        next_date = date_resource.Date()
        next_date.set_from_datetime_obj(ndate_datetime)

        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(sdate_datetime, date_mngr.curr_day.obj)

        date_mngr.skip_to_next_day()
        self.assertEqual(prev_date.obj, date_mngr.prev_day.obj)
        self.assertEqual(curr_date.obj, date_mngr.curr_day.obj)
        self.assertEqual(next_date.obj, date_mngr.next_day.obj)
        self.assertEqual(edate_datetime, date_mngr.end_date)
        self.assertEqual(1, date_mngr.get_current_day_index())
        date_mngr.skip_to_next_day()
        date_mngr.skip_to_next_day()
        date_mngr.skip_to_next_day()
        self.assertEqual(4, date_mngr.get_current_day_index())
        self.assertTrue(date_mngr.reached_last_date())

    def test_date_mngr_skip_to_prev_day_pass1(self):
        """ Verifies a valid iteration to the previous day from the current day """
        sdate = "10-06-2015"
        cdate = "10-07-2015"
        ndate = "10-08-2015"
        edate = "10-15-2015"

        sdate_datetime = parse(sdate).date()
        cdate_datetime = parse(cdate).date()
        ndate_datetime = parse(ndate).date()
        edate_datetime = parse(edate).date()

        prev_date = date_resource.Date()
        prev_date.set_from_datetime_obj(sdate_datetime)

        curr_date = date_resource.Date()
        curr_date.set_from_datetime_obj(cdate_datetime)

        next_date = date_resource.Date()
        next_date.set_from_datetime_obj(ndate_datetime)

        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        self.assertEqual(sdate_datetime, date_mngr.curr_day.obj)

        date_mngr.skip_to_next_day()
        date_mngr.skip_to_next_day()

        date_mngr.skip_to_prev_day()
        self.assertEqual(prev_date.obj, date_mngr.prev_day.obj)
        self.assertEqual(curr_date.obj, date_mngr.curr_day.obj)
        self.assertEqual(next_date.obj, date_mngr.next_day.obj)
        self.assertEqual(edate_datetime, date_mngr.end_date)

    def test_date_mngr_contains_date_pass(self):
        """ Verifies a true is returned when looking for a date in the current range """
        sdate = "10-06-2015"
        edate = "10-15-2015"

        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)

        self.assertTrue(date_mngr.contains_date(date="10-10-2015"))
    #
    # def test_increment_time_pass1(self):
    #     """ Does nothing for now as that method is not complete"""
    #     Time = date_resource.Time()
    #     Time.increment_time(hours=1)

    def test_set_from_datetime_obj_fail1(self):
        """ A ValueError is raised and caught after trying ot update the time object from a time string """
        datetime_obj = parse("2016-08-08").date()
        dt = date_resource.Date()
        dt.time_obj.update_from_time_string = mock.MagicMock(side_effect=ValueError)

        expected_msg = "Incorrect time format passed into the date object, should be HH:MM:SS"

        with self.assertRaises(ValueError) as context:
            dt.set_from_datetime_obj(datetime_obj=datetime_obj, time_string='invalid')

        self.assertEqual(expected_msg, context.exception.message)

    def test_date_string_for_controller(self):
        """ Verify that the correct string is returned when the method is called """
        datetime_obj = parse("2016-08-08").date()
        dt = date_resource.Date()
        dt.set_from_datetime_obj(datetime_obj=datetime_obj)

        expected_string = '08/08/2016'

        actual_string = dt.date_string_for_controller()

        self.assertEqual(expected_string, actual_string)

    def test_msg_date_string_for_controller_3200_pass1(self):
        """ Verify that the correct string is returned when the method is called """
        datetime_obj = parse("2016-08-08").date()
        dt = date_resource.Date()
        dt.set_from_datetime_obj(datetime_obj=datetime_obj)

        expected_string = '08/08/16'

        actual_string = dt.msg_date_string_for_controller_3200()

        self.assertEqual(expected_string, actual_string)

    def test_time_string_for_controller_pass1(self):
        """ Verify that the correct string is returned when the method is called """
        datetime_obj = parse("2016-08-08").date()
        dt = date_resource.Date()
        dt.set_from_datetime_obj(datetime_obj=datetime_obj, time_string="11:11:11")

        expected_string = '11:11:11'

        actual_string = dt.time_string_for_controller()

        self.assertEqual(expected_string, actual_string)

    def test_formatted_date_time_string_pass1(self):
        """ Verify that the correct string is returned when the method is called """
        datetime_obj = parse("2016-08-08")
        
        date_mngr.controller_datetime.set_from_datetime_obj(datetime_obj, time_string='18:37:00')
    
        expected_string = '08/08/16 06:37 PM'
    
        actual_string = date_mngr.controller_datetime.formatted_date_time_string(date_format_string='%m/%d/%y',
                                                                                 time_format_string='%I:%M %p')
    
        self.assertEqual(expected_string, actual_string)

    def test_msg_time_string_for_controller_3200_pass1(self):
        """ Verify that the correct string is returned when the method is called """
        datetime_obj = parse("2016-08-08").date()
        dt = date_resource.Date()
        dt.set_from_datetime_obj(datetime_obj=datetime_obj, time_string="11:11:11")

        expected_string = '11:11:11'

        actual_string = dt.msg_time_string_for_controller_3200()

        self.assertEqual(expected_string, actual_string)

    def test_increment_date_time_pass1(self):
        """ Verify that the date object has been incremented by the expected amount """
        datetime_obj = parse("2016-08-08").date()
        dt = date_resource.Date()
        dt.set_from_datetime_obj(datetime_obj=datetime_obj, time_string="10:00:00")

        dt.increment_date_time(hours='1', minutes='37', seconds='21')

        self.assertEqual(dt.time_obj.hour, 11)
        self.assertEqual(dt.time_obj.minute, 37)
        self.assertEqual(dt.time_obj.second, 21)

    def test_set_date_to_defaul_pass1(self):
        """ Verifies that the method resets all the values in a Date object """
        dt = date_resource.DateManager()
        dt.curr_day = 404
        dt.prev_day = 404
        dt.next_day = 404

        dt.set_date_to_default()

        self.assertIsInstance(dt.curr_day, date_resource.Date)
        self.assertIsInstance(dt.prev_day, date_resource.Date)
        self.assertIsInstance(dt.next_day, date_resource.Date)

    def test_set_current_date_to_match_computer_pass1(self):
        """ Test to make sure the current date and time is stored in the object """
        dt = date_resource.DateManager()

        dt.set_current_date_to_match_computer()

        self.assertEqual(dt.curr_computer_date.day, datetime.datetime.now().day)

    # TODO possible error in code where it only sets next_day to None but the comments say curr_day should also be None
    def test_skip_to_next_day_pass1(self):
        """ The current day is the last day, confirm the variables are changed appropriately """
        sdate = "01-30-2016"
        edate = "01-30-2016"
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        date_mngr.skip_to_next_day()
        self.assertEqual(first=date_mngr.next_day, second=None)

    def test_skip_to_prev_day_pass1(self):
        """ The current day is the first day, confirm the variables are changed appropriately """
        sdate = "01-30-2016"
        edate = "01-30-2016"
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        date_mngr.skip_to_prev_day()
        self.assertEqual(first=date_mngr.prev_day, second=None)

    def test_skip_to_prev_day_pass2(self):
        """ The current day is the first day, confirm the variables are changed appropriately """
        sdate = "08-01-2016"
        edate = "08-07-2016"
        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        date_mngr.skip_to_next_day()
        date_mngr.skip_to_prev_day()
        self.assertEqual(first=date_mngr.curr_day.weekDayIndex, second=0)
        self.assertEqual(first=date_mngr.next_day.weekDayIndex, second=1)

    def test_skip_to_prev_day_fail1(self):
        """ We try to move back a day even though we are currently on the first day """
        sdate = "08-01-2016"
        edate = "08-07-2016"

        date_mngr = date_resource.DateManager()
        date_mngr.set_dates_from_date_range(start_date=sdate, end_date=edate)
        date_mngr.prev_day.weekDayIndex = 0
        date_mngr.curr_week_index = 0
        date_mngr.skip_to_next_day()

        expected_msg = "DateMngr unable to move back one day. At the start of the range: {date}".format(
            date=date_mngr.curr_day.formatted_date_string(format_string="%m-%d-%Y"))

        with self.assertRaises(IndexError) as context:
            date_mngr.skip_to_prev_day()

        self.assertEqual(expected_msg, context.exception.message)

