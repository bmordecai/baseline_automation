
import unittest
from csv_handler import CSVWriter
from common.reporting.program_summary_reporter import ReportingUtil
from common.user_configuration import UserConfiguration
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.programming.ws import WaterSource
from common.objects.programming.point_of_control import PointOfControl
from common.objects.programming.ml import Mainline
import mock
import datetime
from dateutil.parser import parse
from common.date_package import date_resource
from common.date_package.date_resource import date_mngr

__authors__ = 'Dillon, Ben, Andy, Kent'


class TestProgramSummaryReporter(unittest.TestCase):

    ########################################
    def setUp(self):
        """ Setting up for the test. """
        self.reporter = ReportingUtil(use_case_config_file=mock.MagicMock(spec=UserConfiguration), file_name='test.csv')
        self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + self.shortDescription())

    ########################################
    def tearDown(self):
        """ Cleaning up after the test. """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #######################################
    def test_program_summary_reporter_constructor_happy_path(self):
        """ Verifies the happy path for constructing a ReportingUtil object """
        file_name = 'test_config_file.csv'
        config_file = mock.Mock()
        foo = ReportingUtil(config_file, file_name)
        self.assertIsNotNone(foo)
        self.assertIsNotNone(foo.writer)
        self.assertEqual(foo.file_name, file_name)
        self.assertEqual(foo.local_config, config_file)

    #######################################
    def test_program_summary_reporter_constructor_fail_path(self):
        """ Verifies the fail path for constructing a ReportingUtil object """
        file_name = 'test_config_file'
        config_file = mock.Mock()

        expected_msg = "Wrong file extension: {0}. Use csv or txt.".format(file_name)

        with self.assertRaises(TypeError) as context:
            foo = ReportingUtil(config_file, file_name)

        self.assertEqual(expected_msg, context.exception.message)

    @mock.patch.object(CSVWriter, "writerow")
    ########################################
    def test_program_summary_reporter_program_start_time(self, mock_writerow):
        """ Verifies the correct time format is output to the file. """
        program_ids = [1, 2, 3, 4]
        time = '11:00:00'
        self.reporter.record_program_start_time_event(time, program_ids)
        first_call = mock.call([ReportingUtil.PROGRAM_START_TIME_EVENT])
        second_call = mock.call(['Time:', time])
        third_call = mock.call(['Program(s):', program_ids])
        fourth_call = mock.call([''])
        expected_calls = [first_call, second_call, third_call, fourth_call]
        mock_writerow.assert_has_calls(expected_calls, any_order=False)

    @mock.patch.object(CSVWriter, "writerow")
    ########################################
    def test_program_summary_reporter_program_learn_flow(self, mock_writerow):
        """ Verifies the correct learn flow time format is output to the file. """
        program_ids = [1, 2, 3, 4]
        time = '11:00:00'
        self.reporter.record_program_learn_flow_event(time, program_ids)
        first_call = mock.call([ReportingUtil.PROGRAM_LEARN_FLOW_EVENT])
        second_call = mock.call(['Time:', time])
        third_call = mock.call(['Program(s):', program_ids])
        fourth_call = mock.call([''])
        expected_calls = [first_call, second_call, third_call, fourth_call]
        mock_writerow.assert_has_calls(expected_calls, any_order=False)

    @mock.patch.object(CSVWriter, "writerow")
    ########################################
    def test_program_summary_reporter_program_manual_run(self, mock_writerow):
        """ Verifies the correct manual run time format is output to the file. """
        program_ids = [1, 2, 3, 4]
        time = '11:00:00'
        self.reporter.record_program_manual_run_event(time, program_ids)
        first_call = mock.call([ReportingUtil.PROGRAM_MANUAL_RUN_EVENT])
        second_call = mock.call(['Time:', time])
        third_call = mock.call(['Program(s):', program_ids])
        fourth_call = mock.call([''])
        expected_calls = [first_call, second_call, third_call, fourth_call]
        mock_writerow.assert_has_calls(expected_calls, any_order=False)

    @mock.patch.object(CSVWriter, "writerow")
    ########################################
    def test_program_summary_reporter_program_start_condition(self, mock_writerow):
        """ Verifies the correct start condition time format is output to the file. """
        program_ids = [1, 2, 3, 4]
        time = '11:00:00'
        self.reporter.record_program_start_condition_event(time, program_ids)
        first_call = mock.call([ReportingUtil.PROGRAM_START_CONDITION_EVENT])
        second_call = mock.call(['Time:', time])
        third_call = mock.call(['Program(s):', program_ids])
        fourth_call = mock.call([''])
        expected_calls = [first_call, second_call, third_call, fourth_call]
        mock_writerow.assert_has_calls(expected_calls, any_order=False)

    @mock.patch.object(CSVWriter, "writerow")
    ########################################
    def test_program_summary_reporter_zone_manual_run(self, mock_writerow):
        """ Verifies the correct zone manual run time format is output to the file. """
        zone_ids = [1, 2, 3, 4]
        time = '11:00:00'
        self.reporter.record_zone_manual_run_event(time, zone_ids)
        first_call = mock.call([ReportingUtil.ZONE_MANUAL_RUN_EVENT])
        second_call = mock.call(['Time:', time])
        third_call = mock.call(['Zone(s):', zone_ids])
        fourth_call = mock.call([''])
        expected_calls = [first_call, second_call, third_call, fourth_call]
        mock_writerow.assert_has_calls(expected_calls, any_order=False)

    @mock.patch.object(CSVWriter, "writerow")
    ########################################
    def test_program_summary_reporter_zone_learn_flow(self, mock_writerow):
        """ Verifies the correct zone learn flow event time format is output to the file. """
        zone_ids = [1, 2, 3, 4]
        time = '11:00:00'
        self.reporter.record_zone_learn_flow_event(time, zone_ids)
        first_call = mock.call([ReportingUtil.ZONE_LEARN_FLOW_EVENT])
        second_call = mock.call(['Time:', time])
        third_call = mock.call(['Zone(s):', zone_ids])
        fourth_call = mock.call([''])
        expected_calls = [first_call, second_call, third_call, fourth_call]
        mock_writerow.assert_has_calls(expected_calls, any_order=False)

    ########################################
    def test_get_all_possible_water_path_for_zone(self):
        """ Verifies the correct zones get written """
        # Create all controller objects needed for a water path
        controller_3200 = mock.MagicMock()
        zone = mock.MagicMock()
        zone.ss = "WT"
        zone.df = 25
        zone.ml = 1
        zone.ad = 1
        mainline = mock.MagicMock()
        mainline.fl = 25
        mainline.ss = "RN"
        mainline.ad = 1
        poc = mock.MagicMock()
        poc.fm = 1
        poc.ss = "RN"
        poc.ml = 1
        poc.ad = 1
        flow_meter = mock.MagicMock()
        flow_meter.bicoder.vr = 25
        flow_meter.ad = 1
        water_source = mock.MagicMock()
        water_source.ss = "RN"
        water_source.pc = 1
        water_source.ad = 1
        self.reporter.local_config.BaseStation3200 = {1: controller_3200}
        self.reporter.local_config.BaseStation3200[1].zones = {1: zone}
        self.reporter.local_config.BaseStation3200[1].mainlines = {1: mainline}
        self.reporter.local_config.BaseStation3200[1].points_of_control = {1: poc}
        self.reporter.local_config.BaseStation3200[1].flow_meters = {1: flow_meter}
        self.reporter.local_config.BaseStation3200[1].water_sources = {1: water_source}

        actual_water_path = self.reporter.get_all_possible_water_path_for_zone(zone_address=1, controller_number=1)
        import json
        print "self.config.BaseStation3200[index].zones[zn_ad].status.verify_watering()"
        print actual_water_path
        print json.dumps(actual_water_path, indent=4)

    ########################################
    def test_process_water_paths(self):
        """ Tests the happy path for constructing an entire water path """
        # Create the mock objects we want to use
        controller_3200 = mock.MagicMock(spec=BaseStation3200)
        controller_3200.ser = mock.MagicMock()
        controller_3200.controller_type = "32"
        controller_3200.mac = "foo"
        controller_3200.vr = "16.0.500"
        water_source = WaterSource(_controller=controller_3200, _ad=1)
        poc = PointOfControl(_controller=controller_3200, _ad=1)
        poc2 = PointOfControl(_controller=controller_3200, _ad=1)
        mainline = Mainline(_controller=controller_3200, _ad=1)
        zone = mock.MagicMock()

        # Initialize the attributes of the mocked objects we need for the water path
        water_source.ad = 1
        water_source.ss = 'RN'
        water_source.statuses.status = 'RN'
        water_source.downstream_pipes = [poc, poc2]
        poc.ad = 1
        poc.ss = 'RN'
        poc.statuses.status = 'RN'
        poc.downstream_pipes = [mainline]
        poc2.ad = 2
        poc2.ss = 'OF'
        poc2.statuses.status = 'OF'
        poc2.downstream_pipes = [mainline]
        mainline.downstream_pipes = []
        mainline.ss = 'RN'
        mainline.statuses.status = 'RN'
        zone.ml = 1

        # Store the objects in their respective dictionaries
        self.reporter.local_config.all_controllers = [controller_3200]
        self.reporter.local_config.BaseStation3200 = {1: controller_3200}
        self.reporter.local_config.BaseStation3200[1].water_sources = {1: water_source}
        self.reporter.local_config.BaseStation3200[1].points_of_control = {1: poc, 2: poc2}
        self.reporter.local_config.BaseStation3200[1].mainlines = {1: mainline}

        self.reporter.process_water_paths()
        print self.reporter.water_path_data_store
        print self.reporter.water_path_pipe_connections

        self.reporter.get_output_as_graph("test.png")
