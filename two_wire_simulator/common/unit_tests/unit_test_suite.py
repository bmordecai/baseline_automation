__author__ = 'baseline'
"""
READ BEFORE MODIFICATION

In order for a unit test to be added to the unit test suite, proper 'setUp' and 'tearDown' methods must be present in
the unit test file. Refer to other unit test files for further examples.

Usage:

    -   To Add Additional Tests:
        1.  Add import statement below in related group, or create new group if there are no similar groups.
        2.  Add the imported 'Class' Name (the token following the 'import' statement below) to the 'test_list' list
            in similar fashion.

    -   To Run:
        1.  For Specific Tests:
            - Comment/Uncomment the 'Class' names that you would like to run tests for.
            - Right click this file and click the 'Debug 'test_objects'' option
        2.  For All Tests:
            - Make sure all 'Class' names in the 'test_list' list are un-commented.
            - Right click this file and click the 'Debug 'test_objects'' option
"""

import unittest

from common.objects.base_classes.devices import BaseDevices
from common.unit_tests.test_product import TestProductObject

BaseDevices.controller_lat = float(43.609768)
BaseDevices.controller_long = float(-116.310569)

# OBJECTS

# Base Classes

from common.unit_tests.objects.base_classes.test_bicoder import TestBiCoderObject
from common.unit_tests.objects.base_classes.test_programs import TestBaseProgramsObject
from common.unit_tests.objects.base_classes.test_ser import TestSerialObject
# from common.unit_tests.objects.base_classes.test_poc import TestPOCObject
from common.unit_tests.objects.base_classes.test_web_driver import TestWebDriverObject
from common.unit_tests.objects.base_classes.test_base_methods import TestBaseMethodsObject
from common.unit_tests.objects.base_classes.test_devices import TestBaseDeviceObject
from common.unit_tests.objects.controller.test_base_controller import TestBaseControllerObject
# from common.unit_tests.objects.base_classes.test_pg_start_stop_pause_cond import TestBaseStartStopPause
from common.unit_tests.objects.base_classes.test_messages import TestMessages
from common.unit_tests.objects.base_classes.test_base_conditions import TestBaseConditions
from common.unit_tests.objects.base_classes.test_flow import TestBaseFlow
from common.unit_tests.objects.base_classes.test_basemanager_connections import TestBaseManagerConnections

# Base Manager

# Base Unit

# Controller
from common.unit_tests.objects.controller.test_zn import TestZoneObject

from common.unit_tests.objects.controller.test_fm import TestFlowMeterObject
from common.unit_tests.objects.controller.test_ms import TestMoistureSensorObject
from common.unit_tests.objects.controller.test_mv import TestMasterValveObject
from common.unit_tests.objects.controller.test_ml import TestMainLineObject
from common.unit_tests.objects.controller.test_point_of_control import TestPointOfControl
# from common.unit_tests.objects.controller.test_poc_1000 import TestPOC1000Object
# from common.unit_tests.objects.controller.test_pg_1000 import TestPG1000Object
from common.unit_tests.objects.controller.test_pg_3200 import TestPG3200Object
# from common.unit_tests.objects.controller.test_pg_start_stop_pause_1000 import TestStartStopPause1000
# from common.unit_tests.objects.controller.test_pg_start_stop_pause_3200 import TestStartStopPause3200
from common.unit_tests.objects.controller.test_sw import TestEventSwitchObject
from common.unit_tests.objects.controller.test_ts import TestTemperatureSensorObject
from common.unit_tests.objects.controller.test_zp import TestZoneProgramObject
from common.unit_tests.objects.flowstation.test_flow_station import TestFlowStationObject
from common.unit_tests.objects.substation.test_sb import TestSubstationObject
from common.unit_tests.objects.controller.test_ps import TestPressureSensorObject
from common.unit_tests.objects.controller.test_pm import TestPumpObject
from common.unit_tests.objects.controller.test_ws import TestWaterSourceObject
from common.unit_tests.objects.controller.test_controller_3200 import TestBaseStation3200Object
from common.unit_tests.objects.controller.test_controller_1000 import TestBaseStation1000Object
from common.unit_tests.objects.programming.conditions.test_pause_condition import TestPauseCondition,\
    TestSwitchPauseCondition, TestTemperaturePauseCondition, TestMoisturePauseCondition, TestPressurePauseCondition
from common.unit_tests.objects.programming.conditions.test_start_condition import TestStartCondition,\
    TestSwitchStartCondition, TestTemperatureStartCondition, TestMoistureStartCondition, TestPressureStartCondition
from common.unit_tests.objects.programming.conditions.test_stop_condition import TestStopCondition,\
    TestSwitchStopCondition, TestTemperatureStopCondition, TestMoistureStopCondition, TestPressureStopCondition
from common.unit_tests.objects.programming.conditions.test_empty_condition import TestEmptyCondition,\
    TestSwitchEmptyCondition, TestMoistureEmptyCondition, TestPressureEmptyCondition
from common.unit_tests.objects.programming.conditions.test_full_condition import TestFullCondition, \
    TestSwitchFullCondition, TestMoistureFullCondition, TestPressureFullCondition

# Substation
from common.unit_tests.objects.substation.test_flow_bicoder import TestFlowBiCoderObject
from common.unit_tests.objects.substation.test_moisture_bicoder import TestMoistureBiCoderObject
# from common.unit_tests.objects.substation.test_pump_bicoder import TestPumpBiCoderObject
from common.unit_tests.objects.substation.test_switch_bicoder import TestSwitchBiCoderObject
from common.unit_tests.objects.substation.test_temp_bicoder import TestTempBiCoderObject
from common.unit_tests.objects.substation.test_valve_bicoder import TestValveBiCoderObject
from common.unit_tests.objects.substation.test_analog_bicoder import TestAnalogBiCoderObject

# Simulated BiCoders
from common.unit_tests.objects.simulated_bicoders.test_flow_bicoder import TestFlowBiCoderObject as SimFlow
from common.unit_tests.objects.simulated_bicoders.test_moisture_bicoder import TestMoistureBiCoderObject as SimMoisture
from common.unit_tests.objects.simulated_bicoders.test_switch_bicoder import TestSwitchBiCoderObject as SimSwitch
from common.unit_tests.objects.simulated_bicoders.test_temp_bicoder import TestTempBiCoderObject as SimTemperature
from common.unit_tests.objects.simulated_bicoders.test_valve_bicoder import TestValveBiCoderObject as SimValve
from common.unit_tests.objects.simulated_bicoders.test_analog_bicoder import TestAnalogBiCoderObject as SimAnalog

# Weather Based Watering
from common.unit_tests.objects.weather_based_watering.test_equations import TestEquationsObject
from common.unit_tests.objects.weather_based_watering.test_ir_asa_resources import TestIrAsaResourcesObject

# USER CREDENTIALS
from common.unit_tests.user_credentials.test_user_configuration import TestUserConfiguration

# Configuration object
from common.unit_tests.configuration_object.test_configuration import TestConfiguration

# Object factory
from common.unit_tests.object_factory.test_object_factory import TestObjectFactory

# Handlers
from common.unit_tests.test_json_handler import TestJsonHandler
from common.unit_tests.test_date_resource import TestDateResourceObject
from common.unit_tests.test_file_transfer_handler import TestFileTransferHandler

# Helper Methods
from common.unit_tests.test_helper_methods import TestHelperMethods

# Status Parser
from common.unit_tests.test_status_parser import TestKeyValue, TestKeyValues, TestValue

# List of object test case class names for execution
test_list = [
    TestProductObject,

    TestObjectFactory,

    ####################################
    # Base Classes
    ####################################
    TestSerialObject,
    TestBaseMethodsObject,
    TestBiCoderObject,
    TestBaseDeviceObject,
    # TestMessages,
    TestBaseManagerConnections,
    TestBaseConditions,
    TestBaseControllerObject,
    # TestBaseControllerPackets,
    TestBaseFlow,
    TestBaseProgramsObject,
    TestWebDriverObject,
    # TestPacketBuilder

    ####################################
    # BiCoder Objects
    ####################################
    TestFlowBiCoderObject,
    TestMoistureBiCoderObject,
    TestSwitchBiCoderObject,
    TestTempBiCoderObject,
    TestValveBiCoderObject,
    TestAnalogBiCoderObject,

    ####################################
    # Simulated BiCoder Objects
    ####################################
    SimFlow,
    SimMoisture,
    SimSwitch,
    SimTemperature,
    SimValve,
    SimAnalog,

    ####################################
    # Device Objects
    ####################################
    TestZoneObject,
    TestEventSwitchObject,
    TestTemperatureSensorObject,
    TestZoneProgramObject,
    TestFlowMeterObject,
    TestMoistureSensorObject,
    TestMasterValveObject,
    TestPumpObject,
    TestPressureSensorObject,

    ####################################
    # Programming Objects
    ####################################
    # TestPOCObject,
    TestMainLineObject,
    TestPointOfControl,
    TestWaterSourceObject,
    TestPauseCondition,
    TestSwitchPauseCondition,
    TestTemperaturePauseCondition,
    TestMoisturePauseCondition,
    TestPressurePauseCondition,
    TestStartCondition,
    TestSwitchStartCondition,
    TestTemperatureStartCondition,
    TestMoistureStartCondition,
    TestPressureStartCondition,
    TestStopCondition,
    TestSwitchStopCondition,
    TestTemperatureStopCondition,
    TestMoistureStopCondition,
    TestPressureStopCondition,
    TestEmptyCondition,
    TestSwitchEmptyCondition,
    TestMoistureEmptyCondition,
    TestPressureEmptyCondition,
    TestFullCondition,
    TestSwitchFullCondition,
    TestMoistureFullCondition,
    TestPressureFullCondition,
    # TestPOC1000Object,
    # TestPG1000Object,
    TestPG3200Object,
    # TestStartStopPause1000,
    # TestStartStopPause3200,
    # TestBaseStartStopPause,

    ####################################
    # Controller Objects
    ####################################
    TestBaseStation1000Object,
    TestBaseStation3200Object,
    TestFlowStationObject,
    TestSubstationObject,

    # TestBUSerialObject,
    ####################################
    # WeatherBasedWatering
    ####################################
    TestEquationsObject,
    TestIrAsaResourcesObject,

    ####################################
    # Configuration objects
    ####################################
    # USER CREDENTIALS
    TestUserConfiguration,
    # Configuration object
    TestConfiguration,

    ####################################
    # Handler Objects
    ####################################
    TestJsonHandler,
    TestDateResourceObject,
    TestFileTransferHandler,

    ####################################
    # Helper Methods
    ####################################
    TestHelperMethods,

    ####################################
    # Parsing Objects
    ####################################
    # TestOpcodeParser,
    TestValue,
    TestKeyValue,
    TestKeyValues,

    # BASE UNIT
    # TestBUHelpers, Can't be ran because of the File I/O involved, must individually run this test
    ]


# ---------------------------------------------------------------------------------------------------------------------
# For each object in the test_list, create a test suite for the object, and add the suite to the suite_list
suite_list = []
for test_case in test_list:

    # Create a test suite for each object
    test_suite = unittest.TestLoader().loadTestsFromTestCase(testCaseClass=test_case)

    # Append each object's test suite to a list
    suite_list.append(test_suite)

# ---------------------------------------------------------------------------------------------------------------------
# Create a suite wrapper containing all object's test suite
main_test_suite = unittest.TestSuite(tests=suite_list)

# ---------------------------------------------------------------------------------------------------------------------
# Create an executor to run each suite and all test cases contained in each suite.
test_runner = unittest.TextTestRunner(verbosity=0)
test_runner.run(test=main_test_suite)
