from unittest import TestCase

__author__ = 'kent'

import mock

import common.imports.opcodes as opcodes

from common.objects.packets.devices.flow_meter_packets import FlowMeterPackets

class TestFlowMeterPackets(TestCase):
    """
    This tests the data group packets object for Flow Meters.
    """


    #################################
    def setUp(self):
        """ Setting up for the test. """
        self.flow_meter = mock.MagicMock()
        self.flow_meter.sn = 'test12345'
        self.flow_meter.ds = 'testFlowMeter'
        self.flow_meter.la  = 45.6555
        self.flow_meter.lg = -115.6555
        self.flow_meter.en = opcodes.false
        self.flow_meter.bicoder.kv = 45.5
        self.flow_meter.ad = 5

        self.uut = FlowMeterPackets(_flow_meter_object=self.flow_meter)
        self.uut.send_ts_packet = mock.MagicMock()
        self.uut.send_tq_packet = mock.MagicMock()

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_set_flow_meter_dg131(self):

        expected_packet = "{0}:{1}/{2}={3}/{4}={5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}".format(
            opcodes.flow_meter,                                 # {0}
            str(self.flow_meter.sn),                            # {1}
            opcodes.description,                                # {2}
            str(self.flow_meter.ds),                            # {3}
            opcodes.latitude,                                   # {4}
            str(self.flow_meter.la),                            # {5}
            opcodes.longitude,                                  # {6}
            str(self.flow_meter.lg),                            # {7}
            opcodes.enabled,                                    # {8}
            self.flow_meter.en,                                 # {9}
            opcodes.k_value,                                    # {10}
            self.flow_meter.bicoder.kv,                         # {11}
            opcodes.address_number,                             # {12}
            self.flow_meter.ad,                                 # {13}
        )

        self.uut.set_flow_meter_dg131()
        self.assertEquals(self.uut.send_ts_packet.call_count, 1)
        self.uut.send_ts_packet.assert_called_with(_data_group_number='131', _variable_string=expected_packet)

