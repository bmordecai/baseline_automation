from unittest import TestCase

__author__ = 'kent'

import mock

import common.imports.opcodes as opcodes
from common.objects.packets.devices.master_valve_packets import MasterValvePackets

class TestMasterValvePackets(TestCase):
    """
    This tests MasterValvePackets and the building of the TS packet.
    """


    #################################
    def setUp(self):
        """ Setting up for the test. """
        self.master_valve = mock.MagicMock()
        self.master_valve.ds = 'test'
        self.master_valve.la = 115.52
        self.master_valve.lg = -115.5
        self.master_valve.en = opcodes.false
        self.master_valve.no = opcodes.true
        self.master_valve.ad = 3

        self.uut = MasterValvePackets(_master_valve_object=self.master_valve)
        self.uut.send_ts_packet = mock.MagicMock()

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")


    def test_set_master_valve_dg111(self):

        expected_packet = "{0}:{1}/{2}={3}/{4}={5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}".format(
            opcodes.master_valve,                               # {0}
            str(self.master_valve.sn),                          # {1}
            opcodes.description,                                # {2}
            str(self.master_valve.ds),                          # {3}
            opcodes.latitude,                                   # {4}
            str(self.master_valve.la),                          # {5}
            opcodes.longitude,                                  # {6}
            str(self.master_valve.lg),                          # {7}
            opcodes.enabled,                                    # {8}
            self.master_valve.en,                               # {9}
            opcodes.normally_open,                              # {10}
            self.master_valve.no,                               # {11}
            opcodes.address_number,                             # {12}
            self.master_valve.ad,                               # {13}
        )

        self.uut.set_master_valve_dg111()
        self.assertEquals(self.uut.send_ts_packet.call_count, 1)
        self.uut.send_ts_packet.assert_called_with(_data_group_number='111', _variable_string=expected_packet)

