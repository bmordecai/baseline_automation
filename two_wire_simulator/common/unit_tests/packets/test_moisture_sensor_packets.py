from unittest import TestCase

__author__ = 'kent'

import mock

import common.imports.opcodes as opcodes

from common.objects.packets.devices.moisture_sensor_packets import MoistureSensorPackets

class TestMoistureSensorPackets(TestCase):
    """
    This tests the data group packets object for Moisture Sensors
    """


    #################################
    def setUp(self):
        """ Setting up for the test. """
        self.moisture_sensor = mock.MagicMock()
        self.moisture_sensor.ds = 'test'
        self.moisture_sensor.la = 115.52
        self.moisture_sensor.lg = -115.5
        self.moisture_sensor.en = opcodes.false
        self.moisture_sensor.sn = 3

        self.uut = MoistureSensorPackets(_moisture_sensor_object=self.moisture_sensor)
        self.uut.send_ts_packet = mock.MagicMock()

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")


    def test_set_moisture_sensor_dg302(self):

        expected_packet = "{0}:{1}/{2}={3}/{4}={5}/{6}={7}/{8}={9}".format(
            opcodes.moisture_sensor,        # {0}
            str(self.moisture_sensor.sn),   # {1}
            opcodes.description,            # {2}
            str(self.moisture_sensor.ds),   # {3}
            opcodes.latitude,               # {4}
            str(self.moisture_sensor.la),   # {5}
            opcodes.longitude,              # {6}
            str(self.moisture_sensor.lg),   # {7}
            opcodes.enabled,                # {8}
            self.moisture_sensor.en,        # {9}
        )

        self.uut.set_moisture_sensor_dg302()
        self.assertEquals(self.uut.send_ts_packet.call_count, 1)
        self.uut.send_ts_packet.assert_called_with(_data_group_number='302', _variable_string=expected_packet)

