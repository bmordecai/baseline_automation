from unittest import TestCase

__author__ = 'kent'

import os
import mock

import common.imports.opcodes as opcodes

from common.objects.packets.devices.zone_packets import ZonePackets
from common.objects.base_classes.data_group_packets import DataGroupPackets

class TestZonePackets(TestCase):
    """
    This tests the data group packets object for Zones
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        self.controller = mock.MagicMock()
        self.controller.mac = '123ABC'
        self.controller.data_group_packets = DataGroupPackets(controller=self.controller, test_user_name='', test_password='')
        self.zone = mock.MagicMock()
        self.zone.controller = self.controller

        self.zone = mock.MagicMock()
        self.zone.ds = 'test'
        self.zone.la = 115.52
        self.zone.lg = -115.5
        self.zone.en = opcodes.false
        self.zone.df = 1.5
        self.zone.ml = 1
        self.zone.kc = 0.85
        self.zone.pr = 0.45
        self.zone.ef = 65
        self.zone.rz = 0.55
        self.zone.ad = 1

        self.uut = ZonePackets(_zone_object=self.zone)
        self.uut.send_ts_packet = mock.MagicMock()

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")


    def test_set_zone_dg101(self):

        expected_packet = "{0}:{1}/{2}={3}/{4}={5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}/{14}={15}/{16}={17}/{18}={19}/{20}={21}".format(
            opcodes.zone,                               # {0}
            str(self.zone.ad),                          # {1}
            opcodes.description,                        # {2}
            str(self.zone.ds),                          # {3}
            opcodes.latitude,                           # {4}
            str(self.zone.la),                          # {5}
            opcodes.longitude,                          # {6}
            str(self.zone.lg),                          # {7}
            opcodes.enabled,                            # {8}
            self.zone.en,                               # {9}
            opcodes.design_flow,                        # {10}
            self.zone.df,                               # {11}
            opcodes.mainline,                           # {12}
            self.zone.ml,                               # {13}
            opcodes.crop_coefficient,                   # {14}
            self.zone.kc,                               # {15}
            opcodes.precipitation_rate,                 # {16}
            self.zone.pr,                               # {17}
            opcodes.efficiency_percentage,              # {18}
            self.zone.ef,                               # {19}
            opcodes.root_zone_working_water_storage,    # {20}
            self.zone.rz,                               # {21}
        )

        self.uut.set_zone_dg101()
        self.assertEquals(self.uut.send_ts_packet.call_count, 1)
        self.uut.send_ts_packet.assert_called_with(_data_group_number='101', _variable_string=expected_packet)

