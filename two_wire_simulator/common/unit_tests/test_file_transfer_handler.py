__author__ = 'Baseline'

import unittest
import os
import file_transfer_handler
# from common.variables import common as pvar


class TestFileTransferHandler(unittest.TestCase):
    """
    Unit test class for helper_methods.py
    """

    ########################################

    # def setUp(self):
    #     self.file_name = 'update_file.zip'
    #     self.data =
    #
    #     # Makes sure the Json starts out the same for each test
    #     file_transfer_handler.update_data(file_name=self.file_name, data=self.data)
    #
    #     test_name = self._testMethodName
    #     print("------------------------------------------------------------------------------------------------")
    #     print("Starting test: '" + test_name + "'")
    #     print("Covers: " + str(self.shortDescription()))
    #
    # def tearDown(self):
    #     """ Cleaning up after the test. """
    #     test_name = self._testMethodName
    #     print("Ending test: '" + test_name + "'")
    #     print("------------------------------------------------------------------------------------------------\n")

    def test_get_data_pass_1(self):
        """ look in directory and open file """
        # TODO need to look at how to do this without an absolute directory
        # real directory  'C:\\Users\\Tige-PC\\PycharmProjects\\automated_testing_2015\\common\\unit_tests\\'
        directory = 'common' + os.sep + 'unit_tests'
        file_name = 'test_update_file'
        file_type = ".zip"
        dir_path = os.path.dirname(os.path.realpath(__file__))
        expected_path = os.path.join(dir_path, file_name+file_type)
        actual_path = file_transfer_handler.get_file_from_directory(_directory=directory, _file_name=file_name, _file_type=file_type)
        self.assertEqual(expected_path, actual_path)

    def test_get_data_fail_1(self):
        """ look in directory and try to open file with incorrect name.  This should throw an exception  """
        # TODO need to look at how to do this without an absolute directory
        # real directory  'C:\\Users\\Tige-PC\\PycharmProjects\\automated_testing_2015\\common\\unit_tests\\'
        directory = 'common' + os.sep + 'unit_tests'
        bad_file_name = 'test_updat_file'
        file_type = ".zip"
        dir_path = os.path.dirname(os.path.realpath(__file__))
        expected_path = os.path.join(dir_path, bad_file_name+file_type)
        e_msg = "File {0} is not present" .format(expected_path)
        with self.assertRaises(Exception) as context:
            file_transfer_handler.get_file_from_directory(_directory=directory, _file_name=bad_file_name, _file_type=file_type)
        self.assertEqual(first=e_msg, second=context.exception.message)

    def test_get_data_pass_2(self):
        """ Test that we can suck in a convert a full-size firmware update zip file without throwing an exception """
        # TODO need to look at how to do this without an absolute directory
        # real directory  'C:\\Users\\Tige-PC\\PycharmProjects\\automated_testing_2015\\common\\unit_tests\\'
        # directory = '\\common\\unit_tests\\'
        directory = 'common' + os.sep + 'unit_tests'
        file_name = 'test_update_file'
        file_type = ".zip"
        maxlinelen = 1000
        complete_path = file_transfer_handler.get_file_from_directory(_directory=directory, _file_name=file_name,
                                                                      _file_type=file_type)
        actual_data = file_transfer_handler.open_file(_complete_path=complete_path, _mode_type='reading',
                                                      _file_type='binary')
        base_64_data = file_transfer_handler.convert_data(_file_contents=actual_data, _data_type='base64',
                                                          _maxlinelength=maxlinelen)
        string_replace = file_transfer_handler.convert_to_baseline_base64(_firmware_list_of_packets=base_64_data)
        # need to compare string_replace to base64 data

    def test_get_data_pass_4(self):
        """ look in directory, open zip file with correct name, suck in the data and convert it to
          'funky Baseline Base64' format """
        # TODO need to look at how to do this without an absolute directory
        # real directory  'C:\\Users\\Tige-PC\\PycharmProjects\\automated_testing_2015\\common\\unit_tests\\'
        # directory = '/common/unit_tests/'
        directory = 'common' + os.sep + 'unit_tests'
        file_name = 'test_update_file_tiny'
        file_type = ".zip"
        maxlinelen = 50
        expected_data =  ['UEsDBBQAAAAIAAFuZ0vDzQokvgAAAPQAAAAZABwAdGVzdF91cA__',
                          'ZGF0ZV9maWxlX3RpbnkuYjY0VVQJAAMBHAJaARwCWnV4CwABBA__',
                          '9AEAAAT0AQAAVY-LDoIwFEQ-yA0RF7hwcSvFItBQfIDueEgb8A__',
                          'RUsR-XqtceMsZpJzJ5ncHVYuQgw+8iHIo86ShXjV6cYQyo2vjA__',
                          'Ja2lKyKsquxGXAJfQsbsRtlOwWNEsbC0nFyeLN3DZaiH-lR6fA__',
                          'q4L4NQZ6bMNMyGWO06GwHbedu7omM6cZ2oyuSGTHDaHn5HEe5Q__',
                          '+kB2WCEQDDwzChzut6fc9JrQ+dE3ZPrlB-iTD+ib+EjDazKrPA__',
                          'FCL267DPzTX-IfVrL95QSwECHgMUAAAACAABbmdLw80KJL4AAA__',
                          'APQAAAAZABgAAAAAAAEAAAC0gQAAAAB0ZXN0X3VwZGF0ZV9maQ__',
                          'bGVfdGlueS5iNjRVVAUAAwEcAlp1eAsAAQT0AQAABPQBAABQSw__',
                          'BQYAAAAAAQABAF8AAAARAQAAAAA_']

        complete_path = file_transfer_handler.get_file_from_directory(_directory=directory, _file_name=file_name,
                                                                      _file_type=file_type)
        actual_data = file_transfer_handler.open_file(_complete_path=complete_path, _mode_type='reading',
                                                      _file_type='binary')
        base_64_data = file_transfer_handler.convert_data(_file_contents=actual_data, _data_type='base64',
                                                          _maxlinelength=maxlinelen)
        base_64_list = file_transfer_handler.convert_to_baseline_base64(_firmware_list_of_packets=base_64_data)

        self.assertEquals(expected_data, base_64_list)

    def test_get_data_pass_3(self):
        """ look in directory and try to open file with incorrect name  """
        # TODO need to look at how to do this without an absolute directory
        # real directory  'C:\\Users\\Tige-PC\\PycharmProjects\\automated_testing_2015\\common\\unit_tests\\'
        cn_type = '32'
        directory = 'common' + os.sep + 'unit_tests'
        file_name = 'test_update_file'
        file_type = ".zip"
        maxlinelen = 1000
        complete_path = file_transfer_handler.packet_to_send_to_cn(
            _cn_type=cn_type, _directory=directory,
            _file_name=file_name,
            _file_type=file_type,
            _maxlinelen=maxlinelen)

    def test_get_data_pass_5(self):
        """ look in directory and try to open file with incorrect name  """
        # TODO need to look at how to do this without an absolute directory
        # real directory  'C:\\Users\\Tige-PC\\PycharmProjects\\automated_testing_2015\\common\\unit_tests\\'
        cn_type = '32'
        directory = 'common' + os.sep + 'firmware_update_files'
        file_name = 'BL_32_vlatest'
        file_type = ".zip"
        complete_path = file_transfer_handler.get_file_from_directory(_directory=directory,
                                                                      _file_name=file_name,
                                                                      _file_type=file_type)
        file_name = 'Update' + '/' + 'Version.txt'
        file_type = "t"
        actual_data = file_transfer_handler.open_file(_complete_path=complete_path,
                                                      _mode_type='reading',
                                                      _file_type=file_type,
                                                      _file_in_zip=file_name)
        firmware_version = str(actual_data).strip('\r\n')
        print firmware_version
            # need to compare string_replace to base64 data
        # self.assertEqual(expected_data, actual_data)
    # def test_update_data_pass_1(self):
    #     """ Test Update Data Pass Case 1: update the data in a specified Json file """
    #     new_data = {"NewData": ["1", "2", "3"]}
    #     file_transfer_handler.update_data(file_name=self.file_name, data=new_data)
    #     actual_data = file_transfer_handler.get_data(self.file_name)
    #     self.assertEqual(new_data, actual_data)
    #
    # def test_create_udate_object_pass_1(self):
    #     """ Test Create Json Object Pass Case 1: create a udate object and verify it is there """
    #     new_file = 'new_file.udate'
    #     file_transfer_handler.create_udate_object(new_file)
    #     self.assertTrue(os.path.exists(new_file))
    #     os.remove(new_file)
    #
    # def test_is_file_present_pass_1(self):
    #     """ Test Is File Present Pass Case 1: test to see if our udate file exists, pass in a valid path """
    #     self.assertTrue(file_transfer_handler.is_file_present(self.file_name))
    #
    # def test_is_file_present_fail_1(self):
    #     """ Test Is File Present Fail Case 1: test to see if our udate file exists, pass in an invalid path """
    #     self.assertFalse(file_transfer_handler.is_file_present('invalid/file/path'))
    #
    # def test_convert_string_to_udate(self):
    #     """ Test Convert String To Json Pass Case 1: pass in a string and check to make sure it converts into a udate """
    #     udate_string = '{"TestFirstLevel": ["TestSecondLevel0", "TestSecondLevel1", "TestSecondLevel2"]}'
    #     file_transfer_handler.convert_string_to_udate(udate_string)
    #
    # def test_convert_string_to_udate_pass_1(self):
    #     """ Test Convert String To Json Pass Case 1: pass in a string and check to make sure it converts into a udate """
    #     udate_string = '{"TestFirstLevel": ["TestSecondLevel0", "TestSecondLevel1", "TestSecondLevel2"]}'
    #     expected_udate = {"TestFirstLevel": ["TestSecondLevel0", "TestSecondLevel1", "TestSecondLevel2"]}
    #     actual_udate = file_transfer_handler.convert_string_to_udate(udate_string)
    #     self.assertEqual(expected_udate, actual_udate)
    #
    # def test_convert_udate_to_string_pass_1(self):
    #     """ Test Convert Json To String Pass Case 1: pass in a udate and check to make sure it converts into a string """
    #     udate = {"TestFirstLevel": ["TestSecondLevel0", "TestSecondLevel1", "TestSecondLevel2"]}
    #     expected_string = u'{\n    "TestFirstLevel": [\n        "TestSecondLevel0", \n        "TestSecondLevel1", \n        "TestSecondLevel2"\n    ]\n}'
    #     actual_string = file_transfer_handler.convert_udate_to_string(udate)
    #     self.assertEqual(expected_string, actual_string)
    #
    # def test_main_pass_1(self):
    #     """ Test Main Pass Case 1: When the file_transfer_handler is executed, verify it's 'main' works """
    #     # Create a starting and expected dictionary that will turn into a udate
    #     starting_data = {'Baseline': {'Versions': {'1-valve': {'major': 0}, '4-valve': {'minor': 0}}}}
    #     expected_data = {'Baseline': {'Versions': {'1-valve': {'major': 10}, '4-valve': {'minor': 7}}}}
    #
    #     # Create a udate file and populate it with the starting data
    #     file_transfer_handler.create_udate_object('device_info.udate')
    #     file_transfer_handler.update_data(file_name='device_info.udate', data=starting_data)
    #
    #     # This runs the file_transfer_handler as if it were the main module
    #     runpy.run_path(path_name='../../file_transfer_handler.py', run_name='__main__')
    #
    #     # Retrieve the actual data from the udate file after the statements have been executed
    #     actual_data = file_transfer_handler.get_data('device_info.udate')
    #     self.assertEqual(expected_data, actual_data)
    #
    #     # Remove the udate file
    #     os.remove('device_info.udate')
    #
    #
    #
    #     #
    #     # with mock.patch('file_transfer_handler.get_data', return_value=starting_data):
    #     #     runpy.run_path(path_name='../../file_transfer_handler.py', run_name='__main__')
    #     #
    #     # actual_data = file_transfer_handler.get_data(file_name='device_info.udate')
    #     #
    #     # # file_transfer_handler.get_data = method_reference
    #     #
    #     # self.assertEqual(expected_data, actual_data)
    #     #
    #     # os.remove('device_info.udate')

    if __name__ == '__main__':
        unittest.main()
