from unittest import TestCase
import mock
from serial import Serial
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.controllers.bl_10 import BaseStation1000
from common.imports import opcodes

import common.object_factory as uut


class TestObjectFactory(TestCase):
    ################################
    def setUp(self):
        mock_controller = mock.MagicMock(spec=BaseStation3200)
        mock_controller.ser = "HG00001"
        mock_ser = mock.MagicMock(spec=Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        ip_address = "192.168.100.100"
        port_address = "192.168.100.101"
        socket_port = 1067

        self.controller = BaseStation3200(_mac="12345",
                                          _serial_number="3K0001",
                                          _firmware_version="",
                                          _serial_port=mock_ser,
                                          _port_address=port_address,
                                          _socket_port=socket_port,
                                          _ip_address=ip_address)

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    ###############################################
    @mock.patch('common.object_factory.create_single_valve_bicoder_object')
    @mock.patch('common.object_factory.create_dual_valve_bicoder_object')
    @mock.patch('common.object_factory.create_quad_valve_bicoder_object')
    @mock.patch('common.object_factory.create_twelve_valve_bicoder_object')
    @mock.patch('common.object_factory.create_flow_bicoder_object')
    @mock.patch('common.object_factory.create_moisture_bicoder_object')
    @mock.patch('common.object_factory.create_temperature_bicoder_object')
    @mock.patch('common.object_factory.create_switch_bicoder_object')
    @mock.patch('common.object_factory.create_analog_bicoder_object')
    def test_create_bicoder_object_pass1(self,
                                         mock_create_analog_bicoder_object,
                                         mock_create_switch_bicoder_object,
                                         mock_create_temperature_bicoder_object,
                                         mock_create_moisture_bicoder_object,
                                         mock_create_flow_bicoder_object,
                                         mock_create_twelve_valve_bicoder_object,
                                         mock_create_quad_valve_bicoder_object,
                                         mock_create_dual_valve_bicoder_object,
                                         mock_create_single_valve_bicoder_object):
        """ Verify that correct creation method is called for a single valve bicoder """
        serial_number = 'TVB0001'
        type = opcodes.single_valve_decoder
        device_type = 'ZN'

        uut.create_bicoder_object(_controller=self.controller, _type=type, _serial_number=serial_number,
                                  _device_type=device_type)

        self.assertEqual(mock_create_dual_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_quad_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_twelve_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_flow_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_moisture_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_temperature_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_switch_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_analog_bicoder_object.call_count, 0)

        mock_create_single_valve_bicoder_object.assert_called_once_with(_controller=self.controller,
                                                                        _serial_number=serial_number,
                                                                        _device_type=device_type)

    ###############################################
    @mock.patch('common.object_factory.create_single_valve_bicoder_object')
    @mock.patch('common.object_factory.create_dual_valve_bicoder_object')
    @mock.patch('common.object_factory.create_quad_valve_bicoder_object')
    @mock.patch('common.object_factory.create_twelve_valve_bicoder_object')
    @mock.patch('common.object_factory.create_flow_bicoder_object')
    @mock.patch('common.object_factory.create_moisture_bicoder_object')
    @mock.patch('common.object_factory.create_temperature_bicoder_object')
    @mock.patch('common.object_factory.create_switch_bicoder_object')
    @mock.patch('common.object_factory.create_analog_bicoder_object')
    def test_create_bicoder_object_pass2(self,
                                         mock_create_analog_bicoder_object,
                                         mock_create_switch_bicoder_object,
                                         mock_create_temperature_bicoder_object,
                                         mock_create_moisture_bicoder_object,
                                         mock_create_flow_bicoder_object,
                                         mock_create_twelve_valve_bicoder_object,
                                         mock_create_quad_valve_bicoder_object,
                                         mock_create_dual_valve_bicoder_object,
                                         mock_create_single_valve_bicoder_object):
        """ Verify that correct creation method is called for a dual valve bicoder """
        serial_number = 'TVB0001'
        type = opcodes.two_valve_decoder
        device_type = 'ZN'

        uut.create_bicoder_object(_controller=self.controller, _type=type, _serial_number=serial_number,
                                  _device_type=device_type)

        self.assertEqual(mock_create_single_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_quad_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_twelve_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_flow_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_moisture_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_temperature_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_switch_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_analog_bicoder_object.call_count, 0)

        mock_create_dual_valve_bicoder_object.assert_called_once_with(_controller=self.controller,
                                                                      _serial_number=serial_number,
                                                                      _device_type=device_type)

    ###############################################
    @mock.patch('common.object_factory.create_single_valve_bicoder_object')
    @mock.patch('common.object_factory.create_dual_valve_bicoder_object')
    @mock.patch('common.object_factory.create_quad_valve_bicoder_object')
    @mock.patch('common.object_factory.create_twelve_valve_bicoder_object')
    @mock.patch('common.object_factory.create_flow_bicoder_object')
    @mock.patch('common.object_factory.create_moisture_bicoder_object')
    @mock.patch('common.object_factory.create_temperature_bicoder_object')
    @mock.patch('common.object_factory.create_switch_bicoder_object')
    @mock.patch('common.object_factory.create_analog_bicoder_object')
    def test_create_bicoder_object_pass3(self,
                                         mock_create_analog_bicoder_object,
                                         mock_create_switch_bicoder_object,
                                         mock_create_temperature_bicoder_object,
                                         mock_create_moisture_bicoder_object,
                                         mock_create_flow_bicoder_object,
                                         mock_create_twelve_valve_bicoder_object,
                                         mock_create_quad_valve_bicoder_object,
                                         mock_create_dual_valve_bicoder_object,
                                         mock_create_single_valve_bicoder_object):
        """ Verify that correct creation method is called for a quad valve bicoder """
        serial_number = 'TVB0001'
        type = opcodes.four_valve_decoder
        device_type = 'ZN'

        uut.create_bicoder_object(_controller=self.controller, _type=type, _serial_number=serial_number,
                                  _device_type=device_type)

        self.assertEqual(mock_create_single_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_dual_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_twelve_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_flow_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_moisture_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_temperature_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_switch_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_analog_bicoder_object.call_count, 0)

        mock_create_quad_valve_bicoder_object.assert_called_once_with(_controller=self.controller,
                                                                      _serial_number=serial_number,
                                                                      _device_type=device_type)

    ###############################################
    @mock.patch('common.object_factory.create_single_valve_bicoder_object')
    @mock.patch('common.object_factory.create_dual_valve_bicoder_object')
    @mock.patch('common.object_factory.create_quad_valve_bicoder_object')
    @mock.patch('common.object_factory.create_twelve_valve_bicoder_object')
    @mock.patch('common.object_factory.create_flow_bicoder_object')
    @mock.patch('common.object_factory.create_moisture_bicoder_object')
    @mock.patch('common.object_factory.create_temperature_bicoder_object')
    @mock.patch('common.object_factory.create_switch_bicoder_object')
    @mock.patch('common.object_factory.create_analog_bicoder_object')
    def test_create_bicoder_object_pass4(self,
                                         mock_create_analog_bicoder_object,
                                         mock_create_switch_bicoder_object,
                                         mock_create_temperature_bicoder_object,
                                         mock_create_moisture_bicoder_object,
                                         mock_create_flow_bicoder_object,
                                         mock_create_twelve_valve_bicoder_object,
                                         mock_create_quad_valve_bicoder_object,
                                         mock_create_dual_valve_bicoder_object,
                                         mock_create_single_valve_bicoder_object):
        """ Verify that correct creation method is called for a twelve valve bicoder """
        serial_number = 'TVB0001'
        type = opcodes.twelve_valve_decoder
        device_type = 'ZN'

        uut.create_bicoder_object(_controller=self.controller, _type=type, _serial_number=serial_number,
                                  _device_type=device_type)

        self.assertEqual(mock_create_single_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_dual_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_quad_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_flow_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_moisture_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_temperature_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_switch_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_analog_bicoder_object.call_count, 0)

        mock_create_twelve_valve_bicoder_object.assert_called_once_with(_controller=self.controller,
                                                                        _serial_number=serial_number,
                                                                        _device_type=device_type)

    ###############################################
    @mock.patch('common.object_factory.create_single_valve_bicoder_object')
    @mock.patch('common.object_factory.create_dual_valve_bicoder_object')
    @mock.patch('common.object_factory.create_quad_valve_bicoder_object')
    @mock.patch('common.object_factory.create_twelve_valve_bicoder_object')
    @mock.patch('common.object_factory.create_flow_bicoder_object')
    @mock.patch('common.object_factory.create_moisture_bicoder_object')
    @mock.patch('common.object_factory.create_temperature_bicoder_object')
    @mock.patch('common.object_factory.create_switch_bicoder_object')
    @mock.patch('common.object_factory.create_analog_bicoder_object')
    def test_create_bicoder_object_pass5(self,
                                         mock_create_analog_bicoder_object,
                                         mock_create_switch_bicoder_object,
                                         mock_create_temperature_bicoder_object,
                                         mock_create_moisture_bicoder_object,
                                         mock_create_flow_bicoder_object,
                                         mock_create_twelve_valve_bicoder_object,
                                         mock_create_quad_valve_bicoder_object,
                                         mock_create_dual_valve_bicoder_object,
                                         mock_create_single_valve_bicoder_object):
        """ Verify that correct creation method is called for a flow bicoder """
        serial_number = 'TVB0001'
        type = opcodes.flow_meter

        uut.create_bicoder_object(_controller=self.controller, _type=type, _serial_number=serial_number)

        self.assertEqual(mock_create_single_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_dual_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_quad_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_twelve_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_moisture_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_temperature_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_switch_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_analog_bicoder_object.call_count, 0)

        mock_create_flow_bicoder_object.assert_called_once_with(_controller=self.controller,
                                                                _serial_number=serial_number)

    ###############################################
    @mock.patch('common.object_factory.create_single_valve_bicoder_object')
    @mock.patch('common.object_factory.create_dual_valve_bicoder_object')
    @mock.patch('common.object_factory.create_quad_valve_bicoder_object')
    @mock.patch('common.object_factory.create_twelve_valve_bicoder_object')
    @mock.patch('common.object_factory.create_flow_bicoder_object')
    @mock.patch('common.object_factory.create_moisture_bicoder_object')
    @mock.patch('common.object_factory.create_temperature_bicoder_object')
    @mock.patch('common.object_factory.create_switch_bicoder_object')
    @mock.patch('common.object_factory.create_analog_bicoder_object')
    def test_create_bicoder_object_pass6(self,
                                         mock_create_analog_bicoder_object,
                                         mock_create_switch_bicoder_object,
                                         mock_create_temperature_bicoder_object,
                                         mock_create_moisture_bicoder_object,
                                         mock_create_flow_bicoder_object,
                                         mock_create_twelve_valve_bicoder_object,
                                         mock_create_quad_valve_bicoder_object,
                                         mock_create_dual_valve_bicoder_object,
                                         mock_create_single_valve_bicoder_object):
        """ Verify that correct creation method is called for a moisture bicoder """
        serial_number = 'TVB0001'
        type = opcodes.moisture_sensor

        uut.create_bicoder_object(_controller=self.controller, _type=type, _serial_number=serial_number)

        self.assertEqual(mock_create_single_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_dual_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_quad_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_twelve_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_flow_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_temperature_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_switch_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_analog_bicoder_object.call_count, 0)

        mock_create_moisture_bicoder_object.assert_called_once_with(_controller=self.controller,
                                                                    _serial_number=serial_number)

    ###############################################
    @mock.patch('common.object_factory.create_single_valve_bicoder_object')
    @mock.patch('common.object_factory.create_dual_valve_bicoder_object')
    @mock.patch('common.object_factory.create_quad_valve_bicoder_object')
    @mock.patch('common.object_factory.create_twelve_valve_bicoder_object')
    @mock.patch('common.object_factory.create_flow_bicoder_object')
    @mock.patch('common.object_factory.create_moisture_bicoder_object')
    @mock.patch('common.object_factory.create_temperature_bicoder_object')
    @mock.patch('common.object_factory.create_switch_bicoder_object')
    @mock.patch('common.object_factory.create_analog_bicoder_object')
    def test_create_bicoder_object_pass7(self,
                                         mock_create_analog_bicoder_object,
                                         mock_create_switch_bicoder_object,
                                         mock_create_temperature_bicoder_object,
                                         mock_create_moisture_bicoder_object,
                                         mock_create_flow_bicoder_object,
                                         mock_create_twelve_valve_bicoder_object,
                                         mock_create_quad_valve_bicoder_object,
                                         mock_create_dual_valve_bicoder_object,
                                         mock_create_single_valve_bicoder_object):
        """ Verify that correct creation method is called for a temperature bicoder """
        serial_number = 'TVB0001'
        type = opcodes.temperature_sensor

        uut.create_bicoder_object(_controller=self.controller, _type=type, _serial_number=serial_number)

        self.assertEqual(mock_create_single_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_dual_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_quad_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_twelve_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_flow_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_moisture_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_switch_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_analog_bicoder_object.call_count, 0)

        mock_create_temperature_bicoder_object.assert_called_once_with(_controller=self.controller,
                                                                       _serial_number=serial_number)

    ###############################################
    @mock.patch('common.object_factory.create_single_valve_bicoder_object')
    @mock.patch('common.object_factory.create_dual_valve_bicoder_object')
    @mock.patch('common.object_factory.create_quad_valve_bicoder_object')
    @mock.patch('common.object_factory.create_twelve_valve_bicoder_object')
    @mock.patch('common.object_factory.create_flow_bicoder_object')
    @mock.patch('common.object_factory.create_moisture_bicoder_object')
    @mock.patch('common.object_factory.create_temperature_bicoder_object')
    @mock.patch('common.object_factory.create_switch_bicoder_object')
    @mock.patch('common.object_factory.create_analog_bicoder_object')
    def test_create_bicoder_object_pass8(self,
                                         mock_create_analog_bicoder_object,
                                         mock_create_switch_bicoder_object,
                                         mock_create_temperature_bicoder_object,
                                         mock_create_moisture_bicoder_object,
                                         mock_create_flow_bicoder_object,
                                         mock_create_twelve_valve_bicoder_object,
                                         mock_create_quad_valve_bicoder_object,
                                         mock_create_dual_valve_bicoder_object,
                                         mock_create_single_valve_bicoder_object):
        """ Verify that correct creation method is called for a switch bicoder """
        serial_number = 'TVB0001'
        type = opcodes.event_switch

        uut.create_bicoder_object(_controller=self.controller, _type=type, _serial_number=serial_number)

        self.assertEqual(mock_create_single_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_dual_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_quad_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_twelve_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_flow_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_moisture_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_temperature_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_analog_bicoder_object.call_count, 0)

        mock_create_switch_bicoder_object.assert_called_once_with(_controller=self.controller,
                                                                  _serial_number=serial_number)

    ###############################################
    @mock.patch('common.object_factory.create_single_valve_bicoder_object')
    @mock.patch('common.object_factory.create_dual_valve_bicoder_object')
    @mock.patch('common.object_factory.create_quad_valve_bicoder_object')
    @mock.patch('common.object_factory.create_twelve_valve_bicoder_object')
    @mock.patch('common.object_factory.create_flow_bicoder_object')
    @mock.patch('common.object_factory.create_moisture_bicoder_object')
    @mock.patch('common.object_factory.create_temperature_bicoder_object')
    @mock.patch('common.object_factory.create_switch_bicoder_object')
    @mock.patch('common.object_factory.create_analog_bicoder_object')
    def test_create_bicoder_object_pass9(self,
                                         mock_create_analog_bicoder_object,
                                         mock_create_switch_bicoder_object,
                                         mock_create_temperature_bicoder_object,
                                         mock_create_moisture_bicoder_object,
                                         mock_create_flow_bicoder_object,
                                         mock_create_twelve_valve_bicoder_object,
                                         mock_create_quad_valve_bicoder_object,
                                         mock_create_dual_valve_bicoder_object,
                                         mock_create_single_valve_bicoder_object):
        """ Verify that correct creation method is called for a analog bicoder """
        serial_number = 'TVB0001'
        type = opcodes.analog_decoder

        uut.create_bicoder_object(_controller=self.controller, _type=type, _serial_number=serial_number)

        self.assertEqual(mock_create_single_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_dual_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_quad_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_twelve_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_flow_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_moisture_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_temperature_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_switch_bicoder_object.call_count, 0)

        mock_create_analog_bicoder_object.assert_called_once_with(_controller=self.controller,
                                                                  _serial_number=serial_number)

    ###############################################
    @mock.patch('common.object_factory.create_single_valve_bicoder_object')
    @mock.patch('common.object_factory.create_dual_valve_bicoder_object')
    @mock.patch('common.object_factory.create_quad_valve_bicoder_object')
    @mock.patch('common.object_factory.create_twelve_valve_bicoder_object')
    @mock.patch('common.object_factory.create_flow_bicoder_object')
    @mock.patch('common.object_factory.create_moisture_bicoder_object')
    @mock.patch('common.object_factory.create_temperature_bicoder_object')
    @mock.patch('common.object_factory.create_switch_bicoder_object')
    @mock.patch('common.object_factory.create_analog_bicoder_object')
    def test_create_bicoder_object_pass10(self,
                                          mock_create_analog_bicoder_object,
                                          mock_create_switch_bicoder_object,
                                          mock_create_temperature_bicoder_object,
                                          mock_create_moisture_bicoder_object,
                                          mock_create_flow_bicoder_object,
                                          mock_create_twelve_valve_bicoder_object,
                                          mock_create_quad_valve_bicoder_object,
                                          mock_create_dual_valve_bicoder_object,
                                          mock_create_single_valve_bicoder_object):
        """ Verify that no creation methods are called for a bogus bicoder type """
        serial_number = 'TVB0001'
        bogus_decoder_type = 'ZZZ'
        device_type = 'ZN'

        uut.create_bicoder_object(_controller=self.controller, _type=bogus_decoder_type, _serial_number=serial_number,
                                  _device_type=device_type)

        self.assertEqual(mock_create_single_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_dual_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_quad_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_twelve_valve_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_flow_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_moisture_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_temperature_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_switch_bicoder_object.call_count, 0)
        self.assertEqual(mock_create_analog_bicoder_object.call_count, 0)

    ###############################################
    @mock.patch('common.object_factory.ValveBicoder', autospec=True)
    def test_create_single_valve_bicoder_object(self, mock_valve_bicoder_constructor):
        """ Verify that create_single_value_bicoder calls the expected constructor with the expected arguments, and
        that the controller valve_bicoders dictionary has the result of the contructor added in the expected location
        """
        serial_number = 'TMV0001'
        device_type = 'ZN'

        uut.create_single_valve_bicoder_object(_controller=self.controller, _serial_number=serial_number,
                                               _device_type=device_type)

        mock_valve_bicoder_constructor.assert_called_with(_controller=self.controller, _id=device_type,
                                                          _sn=serial_number, _decoder_type='D1')
        self.assertEquals(self.controller.valve_bicoders[serial_number], mock_valve_bicoder_constructor.return_value)

    ###############################################
    @mock.patch('common.object_factory.ValveBicoder', autospec=True)
    def test_create_dual_valve_bicoder_object(self, mock_valve_bicoder_constructor):
        """ Verify that create_dual_value_bicoder calls the expected constructor twice with the expected arguments, and
        that the controller valve_bicoders dictionary has the two valve bicoders added in the expected locations """
        starting_serial_number = 'TMV0001'
        device_type = 'ZN'

        created_valve_bicoders = [mock.MagicMock(), mock.MagicMock()]
        mock_valve_bicoder_constructor.side_effect = created_valve_bicoders

        uut.create_dual_valve_bicoder_object(_controller=self.controller, _serial_number=starting_serial_number,
                                             _device_type=device_type)

        self.assertEquals(mock_valve_bicoder_constructor.call_count, 2)
        expected_calls = [mock.call(_controller=self.controller, _id=device_type, _sn='TMV0001', _decoder_type='D2'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0002', _decoder_type='D2')]
        mock_valve_bicoder_constructor.assert_has_calls(expected_calls)
        self.assertEquals(self.controller.valve_bicoders['TMV0001'], created_valve_bicoders[0])
        self.assertEquals(self.controller.valve_bicoders['TMV0002'], created_valve_bicoders[1])

    ###############################################
    @mock.patch('common.object_factory.ValveBicoder', autospec=True)
    def test_create_quad_valve_bicoder_object(self, mock_valve_bicoder_constructor):
        """ Verify that create_quad_value_bicoder calls the expected constructor four times with the expected arguments,
         and that the controller valve_bicoders dictionary has the four valve bicoders added in the expected
         locations """
        starting_serial_number = 'TMV0001'
        device_type = 'ZN'

        created_valve_bicoders = [mock.MagicMock(), mock.MagicMock(), mock.MagicMock(), mock.MagicMock()]
        mock_valve_bicoder_constructor.side_effect = created_valve_bicoders

        uut.create_quad_valve_bicoder_object(_controller=self.controller, _serial_number=starting_serial_number,
                                             _device_type=device_type)

        self.assertEquals(mock_valve_bicoder_constructor.call_count, 4)
        expected_calls = [mock.call(_controller=self.controller, _id=device_type, _sn='TMV0001', _decoder_type='D4'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0002', _decoder_type='D4'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0003', _decoder_type='D4'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0004', _decoder_type='D4')]
        mock_valve_bicoder_constructor.assert_has_calls(expected_calls)
        self.assertEquals(self.controller.valve_bicoders['TMV0001'], created_valve_bicoders[0])
        self.assertEquals(self.controller.valve_bicoders['TMV0002'], created_valve_bicoders[1])
        self.assertEquals(self.controller.valve_bicoders['TMV0003'], created_valve_bicoders[2])
        self.assertEquals(self.controller.valve_bicoders['TMV0004'], created_valve_bicoders[3])

    ###############################################
    @mock.patch('common.object_factory.ValveBicoder', autospec=True)
    def test_create_twelve_valve_bicoder_object(self, mock_valve_bicoder_constructor):
        """ Verify that create_twelve_value_bicoder calls the expected constructor twelve times with the expected
        arguments, and that the controller valve_bicoders dictionary has the twelve valve bicoders added in the
        expected locations """
        starting_serial_number = 'TMV0001'
        device_type = 'ZN'

        created_valve_bicoders = [mock.MagicMock(), mock.MagicMock(), mock.MagicMock(), mock.MagicMock(),
                                  mock.MagicMock(), mock.MagicMock(), mock.MagicMock(), mock.MagicMock(),
                                  mock.MagicMock(), mock.MagicMock(), mock.MagicMock(), mock.MagicMock()]
        mock_valve_bicoder_constructor.side_effect = created_valve_bicoders

        uut.create_twelve_valve_bicoder_object(_controller=self.controller, _serial_number=starting_serial_number,
                                               _device_type=device_type)

        self.assertEquals(mock_valve_bicoder_constructor.call_count, 12)
        expected_calls = [mock.call(_controller=self.controller, _id=device_type, _sn='TMV0001', _decoder_type='DD'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0002', _decoder_type='DD'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0003', _decoder_type='DD'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0004', _decoder_type='DD'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0005', _decoder_type='DD'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0006', _decoder_type='DD'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0007', _decoder_type='DD'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0008', _decoder_type='DD'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0009', _decoder_type='DD'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0010', _decoder_type='DD'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0011', _decoder_type='DD'),
                          mock.call(_controller=self.controller, _id=device_type, _sn='TMV0012', _decoder_type='DD')]
        mock_valve_bicoder_constructor.assert_has_calls(expected_calls)
        self.assertEquals(self.controller.valve_bicoders['TMV0001'], created_valve_bicoders[0])
        self.assertEquals(self.controller.valve_bicoders['TMV0002'], created_valve_bicoders[1])
        self.assertEquals(self.controller.valve_bicoders['TMV0003'], created_valve_bicoders[2])
        self.assertEquals(self.controller.valve_bicoders['TMV0004'], created_valve_bicoders[3])
        self.assertEquals(self.controller.valve_bicoders['TMV0005'], created_valve_bicoders[4])
        self.assertEquals(self.controller.valve_bicoders['TMV0006'], created_valve_bicoders[5])
        self.assertEquals(self.controller.valve_bicoders['TMV0007'], created_valve_bicoders[6])
        self.assertEquals(self.controller.valve_bicoders['TMV0008'], created_valve_bicoders[7])
        self.assertEquals(self.controller.valve_bicoders['TMV0009'], created_valve_bicoders[8])
        self.assertEquals(self.controller.valve_bicoders['TMV0010'], created_valve_bicoders[9])
        self.assertEquals(self.controller.valve_bicoders['TMV0011'], created_valve_bicoders[10])
        self.assertEquals(self.controller.valve_bicoders['TMV0012'], created_valve_bicoders[11])

    ###############################################
    @mock.patch('common.object_factory.FlowBicoder', autospec=True)
    def test_create_flow_bicoder_object(self, mock_flow_bicoder_constructor):
        """ Verify that create_flow_bicoder_object calls the expected constructor with the expected arguments, and
        that the controller flow_bicoders dictionary has the result of the contructor added in the expected location """
        serial_number = 'TMV0001'

        uut.create_flow_bicoder_object(_controller=self.controller, _serial_number=serial_number)

        mock_flow_bicoder_constructor.assert_called_with(_sn=serial_number, _controller=self.controller)

        self.assertEquals(self.controller.flow_bicoders[serial_number], mock_flow_bicoder_constructor.return_value)

    ###############################################
    @mock.patch('common.object_factory.MoistureBicoder', autospec=True)
    def test_create_moisture_bicoder_object(self, mock_moisture_bicoder_constructor):
        """ Verify that create_moisture_bicoder_object calls the expected constructor with the expected arguments, and
        that the controller moisture_bicoders dictionary has the result of the contructor added in the expected location
        """
        serial_number = 'TMV0001'

        uut.create_moisture_bicoder_object(_controller=self.controller, _serial_number=serial_number)

        mock_moisture_bicoder_constructor.assert_called_with(_sn=serial_number, _controller=self.controller)

        self.assertEquals(self.controller.moisture_bicoders[serial_number],
                          mock_moisture_bicoder_constructor.return_value)

    ###############################################
    @mock.patch('common.object_factory.TempBicoder', autospec=True)
    def test_create_temperature_bicoder_object(self, mock_temperature_bicoder_constructor):
        """ Verify that create_temperature_bicoder_object calls the expected constructor with the expected arguments,
        and that the controller temperature_bicoders dictionary has the result of the contructor added in the expected
        location """
        serial_number = 'TMV0001'

        uut.create_temperature_bicoder_object(_controller=self.controller, _serial_number=serial_number)

        mock_temperature_bicoder_constructor.assert_called_with(_sn=serial_number, _controller=self.controller)

        self.assertEquals(self.controller.temperature_bicoders[serial_number],
                          mock_temperature_bicoder_constructor.return_value)

    ###############################################
    @mock.patch('common.object_factory.SwitchBicoder', autospec=True)
    def test_create_switch_bicoder_object(self, mock_switch_bicoder_constructor):
        """ Verify that create_switch_bicoder_object calls the expected constructor with the expected arguments,
        and that the controller switch_bicoders dictionary has the result of the contructor added in the expected
        location """
        serial_number = 'TMV0001'

        uut.create_switch_bicoder_object(_controller=self.controller, _serial_number=serial_number)

        mock_switch_bicoder_constructor.assert_called_with(_sn=serial_number, _controller=self.controller)

        self.assertEquals(self.controller.switch_bicoders[serial_number],
                          mock_switch_bicoder_constructor.return_value)

    ###############################################
    @mock.patch('common.object_factory.AnalogBicoder', autospec=True)
    def test_create_analog_bicoder_object(self, mock_analog_bicoder_constructor):
        """ Verify that create_analog_bicoder_object calls the expected constructor with the expected arguments,
        and that the controller analog_bicoders dictionary has the result of the contructor added in the expected
        location """
        serial_number = 'TMV0001'

        uut.create_analog_bicoder_object(_controller=self.controller, _serial_number=serial_number)

        mock_analog_bicoder_constructor.assert_called_with(_sn=serial_number, _controller=self.controller)

        self.assertEquals(self.controller.analog_bicoders[serial_number],
                          mock_analog_bicoder_constructor.return_value)

    ##############################################
    @mock.patch('common.object_factory.Zone', autospec=True)
    def test_create_zone_objects_single_valve_decoders(self, mock_zone_constructor):
        """ Verify that create_zone_objects properly detects single-valve decoder serial numbers, and properly assigns
        zones to decoders """
        expected_zones = 4
        zone_addresses = [1, expected_zones]
        single_serials = ['TSD0001', 'TPR0001', 'TMV0001', 'D00001']
        dual_serials = []
        quad_serials = []
        twelve_serials = []

        # Mock that bicoders have already been loaded onto the controller
        mock_valve_bicoders = {'TSD0001': mock.MagicMock(), 'TPR0001': mock.MagicMock(), 'TMV0001': mock.MagicMock(),
                               'D00001': mock.MagicMock()}
        self.controller.valve_bicoders = mock_valve_bicoders

        # Empty dictionary so zones can be assigned
        self.controller.zones = dict()

        # The mock Zone objects that will be generated whenever Zone() is called
        mock_zones = [mock.MagicMock() for _ in range(1, expected_zones + 1)]
        mock_zone_constructor.side_effect = mock_zones

        uut.create_zone_objects(controller=self.controller,
                                zn_ad_range=zone_addresses,
                                d1=single_serials,
                                d2=dual_serials,
                                d4=quad_serials,
                                dd=twelve_serials)

        # Verify the expected number of Zones were created, and they are assigned to the expected addresses
        self.assertEquals(mock_zone_constructor.call_count, expected_zones)
        for address in self.controller.zones.keys():
            self.assertEqual(self.controller.zones[address], mock_zones[address - 1])

    ##############################################
    @mock.patch('common.object_factory.Zone', autospec=True)
    def test_create_zone_objects_single_valve_decoders_insufficent_addresses(self, mock_zone_constructor):
        """ In the case that we supply fewer desired zone addresses than available decoder serial numbers, verify that
        create_zone_objects only assigns the number zones that are requested """
        expected_zones = 3  # Only want 3 zones
        zone_addresses = [1, expected_zones]
        single_serials = ['TSD0001', 'TPR0001', 'TMV0001', 'D00001']  # Four bicoders, only three will be assigned
        dual_serials = []
        quad_serials = []
        twelve_serials = []

        # Mock that bicoders have already been loaded onto the controller
        mock_valve_bicoders = {'TSD0001': mock.MagicMock(), 'TPR0001': mock.MagicMock(), 'TMV0001': mock.MagicMock(),
                               'D00001': mock.MagicMock()}
        self.controller.valve_bicoders = mock_valve_bicoders

        # Empty dictionary so zones can be assigned
        self.controller.zones = dict()

        # The mock Zone objects that will be generated whenever Zone() is called
        mock_zones = [mock.MagicMock() for _ in range(1, expected_zones + 1)]
        mock_zone_constructor.side_effect = mock_zones

        uut.create_zone_objects(controller=self.controller,
                                zn_ad_range=zone_addresses,
                                d1=single_serials,
                                d2=dual_serials,
                                d4=quad_serials,
                                dd=twelve_serials)

        # Verify the expected number of Zones were created, and they are assigned to the expected addresses
        self.assertEquals(mock_zone_constructor.call_count, expected_zones)
        for address in self.controller.zones.keys():
            self.assertEqual(self.controller.zones[address], mock_zones[address - 1])

    ##############################################
    @mock.patch('common.object_factory.Zone', autospec=True)
    def test_create_zone_objects_dual_valve_decoders(self, mock_zone_constructor):
        """ Verify that create_zone_objects properly detects dual-valve decoder serial numbers, and properly assigns
        zones to decoders """
        expected_zones = 6
        zone_addresses = [1, expected_zones]
        single_serials = []
        dual_serials = ['TVE0001', 'TSE0001', 'E000001']
        quad_serials = []
        twelve_serials = []

        # Mock that bicoders have already been loaded onto the controller
        mock_valve_bicoders = {'TVE0001': mock.MagicMock(), 'TVE0002': mock.MagicMock(),
                               'TSE0001': mock.MagicMock(), 'TSE0002': mock.MagicMock(),
                               'E000001': mock.MagicMock(), 'E000002': mock.MagicMock()}
        self.controller.valve_bicoders = mock_valve_bicoders

        # Empty dictionary so zones can be assigned
        self.controller.zones = dict()

        # The mock Zone objects that will be generated whenever Zone() is called
        mock_zones = [mock.MagicMock() for _ in range(1, expected_zones + 1)]
        mock_zone_constructor.side_effect = mock_zones

        uut.create_zone_objects(controller=self.controller,
                                zn_ad_range=zone_addresses,
                                d1=single_serials,
                                d2=dual_serials,
                                d4=quad_serials,
                                dd=twelve_serials)

        # Verify the expected number of Zones were created, and they are assigned to the expected addresses
        self.assertEquals(mock_zone_constructor.call_count, expected_zones)
        for address in self.controller.zones.keys():
            self.assertEqual(self.controller.zones[address], mock_zones[address - 1])

    ##############################################
    @mock.patch('common.object_factory.Zone', autospec=True)
    def test_create_zone_objects_dual_valve_decoders_insufficient_addresses(self, mock_zone_constructor):
        """ In the case that we supply fewer desired zone addresses than available decoder serial numbers, verify that
        create_zone_objects only assigns the number zones that are requested """
        expected_zones = 4  # Only want 4 zones
        zone_addresses = [1, expected_zones]
        single_serials = []
        dual_serials = ['TVE0001', 'TSE0001', 'E000001']
        quad_serials = []
        twelve_serials = []

        # Mock that bicoders have already been loaded onto the controller
        mock_valve_bicoders = {'TVE0001': mock.MagicMock(), 'TVE0002': mock.MagicMock(),
                               'TSE0001': mock.MagicMock(), 'TSE0002': mock.MagicMock(),
                               'E000001': mock.MagicMock(), 'E000002': mock.MagicMock()}
        self.controller.valve_bicoders = mock_valve_bicoders

        # Empty dictionary so zones can be assigned
        self.controller.zones = dict()

        # The mock Zone objects that will be generated whenever Zone() is called
        mock_zones = [mock.MagicMock() for _ in range(1, expected_zones + 1)]
        mock_zone_constructor.side_effect = mock_zones

        uut.create_zone_objects(controller=self.controller,
                                zn_ad_range=zone_addresses,
                                d1=single_serials,
                                d2=dual_serials,
                                d4=quad_serials,
                                dd=twelve_serials)

        # Verify the expected number of Zones were created, and they are assigned to the expected addresses
        self.assertEquals(mock_zone_constructor.call_count, expected_zones)
        for address in self.controller.zones.keys():
            self.assertEqual(self.controller.zones[address], mock_zones[address - 1])

    ##############################################
    @mock.patch('common.object_factory.Zone', autospec=True)
    def test_create_zone_objects_quad_valve_decoders(self, mock_zone_constructor):
        """ Verify that create_zone_objects properly detects quad-valve decoder serial numbers, and properly assigns
        zones to decoders """
        expected_zones = 8
        zone_addresses = [1, expected_zones]
        single_serials = []
        dual_serials = []
        quad_serials = ['TSQ0001', 'Q000001']
        twelve_serials = []

        # Mock that bicoders have already been loaded onto the controller
        mock_valve_bicoders = {'TSQ0001': mock.MagicMock(), 'TSQ0002': mock.MagicMock(), 'TSQ0003': mock.MagicMock(),
                               'TSQ0004': mock.MagicMock(), 'Q000001': mock.MagicMock(), 'Q000002': mock.MagicMock(),
                               'Q000003': mock.MagicMock(), 'Q000004': mock.MagicMock()}
        self.controller.valve_bicoders = mock_valve_bicoders

        # Empty dictionary so zones can be assigned
        self.controller.zones = dict()

        # The mock Zone objects that will be generated whenever Zone() is called
        mock_zones = [mock.MagicMock() for _ in range(1, expected_zones + 1)]
        mock_zone_constructor.side_effect = mock_zones

        uut.create_zone_objects(controller=self.controller,
                                zn_ad_range=zone_addresses,
                                d1=single_serials,
                                d2=dual_serials,
                                d4=quad_serials,
                                dd=twelve_serials)

        # Verify the expected number of Zones were created, and they are assigned to the expected addresses
        self.assertEquals(mock_zone_constructor.call_count, expected_zones)
        for address in self.controller.zones.keys():
            self.assertEqual(self.controller.zones[address], mock_zones[address - 1])

    ##############################################
    @mock.patch('common.object_factory.Zone', autospec=True)
    def test_create_zone_objects_quad_valve_decoders_insufficent_addresses(self, mock_zone_constructor):
        """ In the case that we supply fewer desired zone addresses than available decoder serial numbers, verify that
        create_zone_objects only assigns the number zones that are requested """
        expected_zones = 6
        zone_addresses = [1, expected_zones]
        single_serials = []
        dual_serials = []
        quad_serials = ['TSQ0001', 'Q000001']
        twelve_serials = []

        # Mock that bicoders have already been loaded onto the controller
        mock_valve_bicoders = {'TSQ0001': mock.MagicMock(), 'TSQ0002': mock.MagicMock(), 'TSQ0003': mock.MagicMock(),
                               'TSQ0004': mock.MagicMock(), 'Q000001': mock.MagicMock(), 'Q000002': mock.MagicMock(),
                               'Q000003': mock.MagicMock(), 'Q000004': mock.MagicMock()}
        self.controller.valve_bicoders = mock_valve_bicoders

        # Empty dictionary so zones can be assigned
        self.controller.zones = dict()

        # The mock Zone objects that will be generated whenever Zone() is called
        mock_zones = [mock.MagicMock() for _ in range(1, expected_zones + 1)]
        mock_zone_constructor.side_effect = mock_zones

        uut.create_zone_objects(controller=self.controller,
                                zn_ad_range=zone_addresses,
                                d1=single_serials,
                                d2=dual_serials,
                                d4=quad_serials,
                                dd=twelve_serials)

        # Verify the expected number of Zones were created, and they are assigned to the expected addresses
        self.assertEquals(mock_zone_constructor.call_count, expected_zones)
        for address in self.controller.zones.keys():
            self.assertEqual(self.controller.zones[address], mock_zones[address - 1])

    ##############################################
    @mock.patch('common.object_factory.Zone', autospec=True)
    def test_create_zone_objects_twelve_valve_decoders(self, mock_zone_constructor):
        """ Verify that create_zone_objects properly detects twelve-valve decoder serial numbers, and properly assigns
        zones to decoders """
        expected_zones = 36
        zone_addresses = range(1, expected_zones + 1)
        single_serials = []
        dual_serials = []
        quad_serials = []
        twelve_serials = ['TVA0001', 'TVB0001', 'V000001']

        # Mock that bicoders have already been loaded onto the controller
        mock_valve_bicoders = {'TVA0001': mock.MagicMock(), 'TVA0002': mock.MagicMock(), 'TVA0003': mock.MagicMock(),
                               'TVA0004': mock.MagicMock(), 'TVA0005': mock.MagicMock(), 'TVA0006': mock.MagicMock(),
                               'TVA0007': mock.MagicMock(), 'TVA0008': mock.MagicMock(), 'TVA0009': mock.MagicMock(),
                               'TVA0010': mock.MagicMock(), 'TVA0011': mock.MagicMock(), 'TVA0012': mock.MagicMock(),
                               'TVB0001': mock.MagicMock(), 'TVB0002': mock.MagicMock(), 'TVB0003': mock.MagicMock(),
                               'TVB0004': mock.MagicMock(), 'TVB0005': mock.MagicMock(), 'TVB0006': mock.MagicMock(),
                               'TVB0007': mock.MagicMock(), 'TVB0008': mock.MagicMock(), 'TVB0009': mock.MagicMock(),
                               'TVB0010': mock.MagicMock(), 'TVB0011': mock.MagicMock(), 'TVB0012': mock.MagicMock(),
                               'V000001': mock.MagicMock(), 'V000002': mock.MagicMock(), 'V000003': mock.MagicMock(),
                               'V000004': mock.MagicMock(), 'V000005': mock.MagicMock(), 'V000006': mock.MagicMock(),
                               'V000007': mock.MagicMock(), 'V000008': mock.MagicMock(), 'V000009': mock.MagicMock(),
                               'V000010': mock.MagicMock(), 'V000011': mock.MagicMock(), 'V000012': mock.MagicMock()}
        self.controller.valve_bicoders = mock_valve_bicoders

        # Empty dictionary so zones can be assigned
        self.controller.zones = dict()

        # The mock Zone objects that will be generated whenever Zone() is called
        mock_zones = [mock.MagicMock() for _ in range(1, expected_zones + 1)]
        mock_zone_constructor.side_effect = mock_zones

        uut.create_zone_objects(controller=self.controller,
                                zn_ad_range=zone_addresses,
                                d1=single_serials,
                                d2=dual_serials,
                                d4=quad_serials,
                                dd=twelve_serials)

        # Verify the expected number of Zones were created, and they are assigned to the expected addresses
        self.assertEquals(mock_zone_constructor.call_count, expected_zones)
        for address in self.controller.zones.keys():
            self.assertEqual(self.controller.zones[address], mock_zones[address - 1])

    ##############################################
    @mock.patch('common.object_factory.Zone', autospec=True)
    def test_create_zone_objects_twelve_valve_decoders_insufficient_addresses(self, mock_zone_constructor):
        """ In the case that we supply fewer desired zone addresses than available decoder serial numbers, verify that
        create_zone_objects only assigns the number of zones that are requested """
        expected_zones = 10  # Only want 10 zones
        zone_addresses = range(1, expected_zones + 1)
        single_serials = []
        dual_serials = []
        quad_serials = []
        twelve_serials = ['TVA0001', 'TVB0001', 'V000001']

        # Mock that bicoders have already been loaded onto the controller
        mock_valve_bicoders = {'TVA0001': mock.MagicMock(), 'TVA0002': mock.MagicMock(), 'TVA0003': mock.MagicMock(),
                               'TVA0004': mock.MagicMock(), 'TVA0005': mock.MagicMock(), 'TVA0006': mock.MagicMock(),
                               'TVA0007': mock.MagicMock(), 'TVA0008': mock.MagicMock(), 'TVA0009': mock.MagicMock(),
                               'TVA0010': mock.MagicMock(), 'TVA0011': mock.MagicMock(), 'TVA0012': mock.MagicMock(),
                               'TVB0001': mock.MagicMock(), 'TVB0002': mock.MagicMock(), 'TVB0003': mock.MagicMock(),
                               'TVB0004': mock.MagicMock(), 'TVB0005': mock.MagicMock(), 'TVB0006': mock.MagicMock(),
                               'TVB0007': mock.MagicMock(), 'TVB0008': mock.MagicMock(), 'TVB0009': mock.MagicMock(),
                               'TVB0010': mock.MagicMock(), 'TVB0011': mock.MagicMock(), 'TVB0012': mock.MagicMock(),
                               'V000001': mock.MagicMock(), 'V000002': mock.MagicMock(), 'V000003': mock.MagicMock(),
                               'V000004': mock.MagicMock(), 'V000005': mock.MagicMock(), 'V000006': mock.MagicMock(),
                               'V000007': mock.MagicMock(), 'V000008': mock.MagicMock(), 'V000009': mock.MagicMock(),
                               'V000010': mock.MagicMock(), 'V000011': mock.MagicMock(), 'V000012': mock.MagicMock()}
        self.controller.valve_bicoders = mock_valve_bicoders

        # Empty dictionary so zones can be assigned
        self.controller.zones = dict()

        # The mock Zone objects that will be generated whenever Zone() is called
        mock_zones = [mock.MagicMock() for _ in range(1, expected_zones + 1)]
        mock_zone_constructor.side_effect = mock_zones

        uut.create_zone_objects(controller=self.controller,
                                zn_ad_range=zone_addresses,
                                d1=single_serials,
                                d2=dual_serials,
                                d4=quad_serials,
                                dd=twelve_serials)

        # Verify the expected number of Zones were created, and they are assigned to the expected addresses
        self.assertEquals(mock_zone_constructor.call_count, expected_zones)
        for address in self.controller.zones.keys():
            self.assertEqual(self.controller.zones[address], mock_zones[address - 1])

    ###############################################
    def test_determine_zone_range_available_happy_path(self):
        """ Verify that determine_zone_range_available takes a list of numbers, treats each even-odd pair as the
        upper and lower limits of a range (inclusive), and creates a list of numbers equivalent to appending the
        resulting ranges """
        zone_limits = [0, 12, 14, 23, 28, 29]
        expected = range(0, 13) + range(14, 24) + range(28, 30)
        addresses = uut.determine_zone_range_available(zone_limits)
        self.assertListEqual(expected, addresses)

    ###############################################
    def test_determine_zone_range_available_odd_length(self):
        """ Verify that determine_zone_range_available throws the expected exception if the list of zone limits
        doesn't contain an even number of elements
        """
        zone_limits_odd_length = [0, 12, 14, 23, 28]

        with self.assertRaises(ValueError) as context:
            uut.determine_zone_range_available(zone_limits_odd_length)

        e_msg = ("Invalid list of zone ranges provided in the data .json file for the current test. Valid Zone ranges "
                 "must have 2 numbers for each range. Correct format in .json file should look like: \n'range': "
                 "[1, 200] -> says to create a range from 1 to 200. \n'range': [1, 5, 10, 20] -> says to create a "
                 "range from 1 to 5 and 10 to 20 with a gap between 5 and 10.")

        self.assertEqual(first=e_msg, second=context.exception.message)

    ###############################################
    @mock.patch('common.object_factory.MoistureSensor', autospec=True)
    def test_create_moisture_sensor_objects(self, mock_moisture_sensor_constructor):
        """ Verify that create_moisture_sensor_objects creates MoistureSensor objects using the expected parameters,
        and the controller moisture_sensors dictionary gets populated with the created moisture sensors """
        address_range = [7, 9]
        serial_numbers = ['TVM0007', 'TVM0009']

        existing_moisture_bicoders = {serial_numbers[0]: mock.MagicMock, serial_numbers[1]: mock.MagicMock}
        self.controller.moisture_bicoders = existing_moisture_bicoders
        created_moisture_sensors = [mock.MagicMock(), mock.MagicMock()]
        mock_moisture_sensor_constructor.side_effect = created_moisture_sensors

        uut.create_moisture_sensor_objects(controller=self.controller, address_range=address_range,
                                           serial_numbers=serial_numbers)

        self.assertEquals(mock_moisture_sensor_constructor.call_count, 2)
        expected_constructor_calls = [
            mock.call(_controller=self.controller, _moisture_bicoder=existing_moisture_bicoders[serial_numbers[0]],
                      _address=address_range[0]),
            mock.call(_controller=self.controller, _moisture_bicoder=existing_moisture_bicoders[serial_numbers[1]],
                      _address=address_range[1])]
        mock_moisture_sensor_constructor.assert_has_calls(expected_constructor_calls)
        self.assertEquals(self.controller.moisture_sensors[address_range[0]], created_moisture_sensors[0])
        self.assertEquals(self.controller.moisture_sensors[address_range[1]], created_moisture_sensors[1])

    ###############################################
    @mock.patch('common.object_factory.MasterValve', autospec=True)
    def test_create_master_valve_objects(self, mock_master_valve_constructor):
        """ Verify that create_master_valve_objects creates MasterValve objects using the expected parameters,
        and the controller master_valves dictionary gets populated with the created master valves """
        address_range = [7, 9]
        serial_numbers = ['TVM0007', 'TVM0009']

        existing_valve_bicoders = {serial_numbers[0]: mock.MagicMock, serial_numbers[1]: mock.MagicMock}
        self.controller.valve_bicoders = existing_valve_bicoders
        created_master_valves = [mock.MagicMock(), mock.MagicMock()]
        mock_master_valve_constructor.side_effect = created_master_valves

        uut.create_master_valve_objects(controller=self.controller, address_range=address_range,
                                        serial_numbers=serial_numbers)

        self.assertEquals(mock_master_valve_constructor.call_count, 2)
        expected_constructor_calls = [
            mock.call(_controller=self.controller, _valve_bicoder=existing_valve_bicoders[serial_numbers[0]],
                      _address=address_range[0]),
            mock.call(_controller=self.controller, _valve_bicoder=existing_valve_bicoders[serial_numbers[1]],
                      _address=address_range[1])]
        mock_master_valve_constructor.assert_has_calls(expected_constructor_calls, any_order=True)
        self.assertEquals(self.controller.master_valves[address_range[0]], created_master_valves[0])
        self.assertEquals(self.controller.master_valves[address_range[1]], created_master_valves[1])

    ###############################################
    @mock.patch('common.object_factory.Pump', autospec=True)
    def test_create_pump_objects(self, mock_pump_constructor):
        """ Verify that create_pump_objects creates Pump objects using the expected parameters,
        and the controller pumps dictionary gets populated with the created pump sensors """
        address_range = [7, 9]
        serial_numbers = ['TVM0007', 'TVM0009']

        existing_pump_bicoders = {serial_numbers[0]: mock.MagicMock, serial_numbers[1]: mock.MagicMock}
        self.controller.valve_bicoders = existing_pump_bicoders
        created_pumps = [mock.MagicMock(), mock.MagicMock()]
        mock_pump_constructor.side_effect = created_pumps

        uut.create_pump_objects(controller=self.controller, address_range=address_range,
                                serial_numbers=serial_numbers)

        self.assertEquals(mock_pump_constructor.call_count, 2)
        expected_constructor_calls = [
            mock.call(_controller=self.controller, _pump_bicoder=existing_pump_bicoders[serial_numbers[0]],
                      _address=address_range[0]),
            mock.call(_controller=self.controller, _pump_bicoder=existing_pump_bicoders[serial_numbers[1]],
                      _address=address_range[1])]
        mock_pump_constructor.assert_has_calls(expected_constructor_calls)
        self.assertEquals(self.controller.pumps[address_range[0]], created_pumps[0])
        self.assertEquals(self.controller.pumps[address_range[1]], created_pumps[1])

    ###############################################
    @mock.patch('common.object_factory.FlowMeter', autospec=True)
    def test_create_flow_meter_objects(self, mock_flow_meter_constructor):
        """ Verify that create_flow_meter_objects creates FlowMeter objects using the expected parameters,
        and the controller flow_meters dictionary gets populated with the created flowmeters """
        address_range = [7, 9]
        serial_numbers = ['TVM0007', 'TVM0009']

        existing_flow_bicoders = {serial_numbers[0]: mock.MagicMock, serial_numbers[1]: mock.MagicMock}
        self.controller.flow_bicoders = existing_flow_bicoders
        created_flow_meters = [mock.MagicMock(), mock.MagicMock()]
        mock_flow_meter_constructor.side_effect = created_flow_meters

        uut.create_flow_meter_objects(controller=self.controller, address_range=address_range,
                                      serial_numbers=serial_numbers)

        self.assertEquals(mock_flow_meter_constructor.call_count, 2)
        expected_constructor_calls = [
            mock.call(_controller=self.controller, _flow_bicoder=existing_flow_bicoders[serial_numbers[0]],
                      _address=address_range[0]),
            mock.call(_controller=self.controller, _flow_bicoder=existing_flow_bicoders[serial_numbers[1]],
                      _address=address_range[1])]
        mock_flow_meter_constructor.assert_has_calls(expected_constructor_calls, any_order=True)
        self.assertEquals(self.controller.flow_meters[address_range[0]], created_flow_meters[0])
        self.assertEquals(self.controller.flow_meters[address_range[1]], created_flow_meters[1])

    ###############################################
    @mock.patch('common.object_factory.EventSwitch', autospec=True)
    def test_create_event_switch_objects(self, mock_event_switch_constructor):
        """ Verify that create_event_switch_objects creates EventSwitch objects using the expected parameters,
        and the controller event_switches dictionary gets populated with the created event switches """
        address_range = [7, 9]
        serial_numbers = ['TVM0007', 'TVM0009']

        existing_switch_bicoders = {serial_numbers[0]: mock.MagicMock, serial_numbers[1]: mock.MagicMock}
        self.controller.switch_bicoders = existing_switch_bicoders
        created_event_switches = [mock.MagicMock(), mock.MagicMock()]
        mock_event_switch_constructor.side_effect = created_event_switches

        uut.create_event_switch_objects(controller=self.controller, address_range=address_range,
                                        serial_numbers=serial_numbers)

        self.assertEquals(mock_event_switch_constructor.call_count, 2)
        expected_constructor_calls = [
            mock.call(_controller=self.controller, _switch_bicoder=existing_switch_bicoders[serial_numbers[0]],
                      _address=address_range[0]),
            mock.call(_controller=self.controller, _switch_bicoder=existing_switch_bicoders[serial_numbers[1]],
                      _address=address_range[1])]
        mock_event_switch_constructor.assert_has_calls(expected_constructor_calls, any_order=True)
        self.assertEquals(self.controller.event_switches[address_range[0]], created_event_switches[0])
        self.assertEquals(self.controller.event_switches[address_range[1]], created_event_switches[1])

    ###############################################
    @mock.patch('common.object_factory.TemperatureSensor', autospec=True)
    def test_create_temperature_sensor_objects(self, mock_temperature_sensor_constructor):
        """ Verify that create_temperature_sensor_objects creates TemperatureSensor objects using the
        expected parameters, and the controller temperature_sensors dictionary gets populated with the
        created temperature sensors """
        address_range = [7, 9]
        serial_numbers = ['TVM0007', 'TVM0009']

        existing_temperature_bicoders = {serial_numbers[0]: mock.MagicMock, serial_numbers[1]: mock.MagicMock}
        self.controller.temperature_bicoders = existing_temperature_bicoders
        created_temperature_sensors = [mock.MagicMock(), mock.MagicMock()]
        mock_temperature_sensor_constructor.side_effect = created_temperature_sensors

        uut.create_temperature_sensor_objects(controller=self.controller, address_range=address_range,
                                              serial_numbers=serial_numbers)

        self.assertEquals(mock_temperature_sensor_constructor.call_count, 2)
        expected_constructor_calls = [
            mock.call(_controller=self.controller, _temp_bicoder=existing_temperature_bicoders[serial_numbers[0]],
                      _address=address_range[0]),
            mock.call(_controller=self.controller, _temp_bicoder=existing_temperature_bicoders[serial_numbers[1]],
                      _address=address_range[1])]
        mock_temperature_sensor_constructor.assert_has_calls(expected_constructor_calls, any_order=True)
        self.assertEquals(self.controller.temperature_sensors[address_range[0]], created_temperature_sensors[0])
        self.assertEquals(self.controller.temperature_sensors[address_range[1]], created_temperature_sensors[1])

    ###############################################
    @mock.patch('common.object_factory.PressureSensor', autospec=True)
    def test_create_pressure_sensor_objects(self, mock_pressure_sensor_constructor):
        """ Verify that create_pressure_sensor_objects creates PressureSensor objects using the expected parameters,
        and the controller pressure_sensors dictionary gets populated with the created pressure sensors """
        address_range = [7, 9]
        serial_numbers = ['TVM0007', 'TVM0009']

        existing_analog_bicoders = {serial_numbers[0]: mock.MagicMock, serial_numbers[1]: mock.MagicMock}
        self.controller.analog_bicoders = existing_analog_bicoders
        created_pressure_sensors = [mock.MagicMock(), mock.MagicMock()]
        mock_pressure_sensor_constructor.side_effect = created_pressure_sensors

        uut.create_pressure_sensor_objects(controller=self.controller, address_range=address_range,
                                           serial_numbers=serial_numbers)

        self.assertEquals(mock_pressure_sensor_constructor.call_count, 2)
        expected_constructor_calls = [
            mock.call(_controller=self.controller, _analog_bicoder=existing_analog_bicoders[serial_numbers[0]],
                      _address=address_range[0]),
            mock.call(_controller=self.controller, _analog_bicoder=existing_analog_bicoders[serial_numbers[1]],
                      _address=address_range[1])]
        mock_pressure_sensor_constructor.assert_has_calls(expected_constructor_calls, any_order=True)
        self.assertEquals(self.controller.pressure_sensors[address_range[0]], created_pressure_sensors[0])
        self.assertEquals(self.controller.pressure_sensors[address_range[1]], created_pressure_sensors[1])

    ###############################################
    @mock.patch('common.object_factory.WaterSource', autospec=True)
    def test_create_water_source_object(self, mock_water_source_constructor):
        """ Verify that create_water_source_object calls WaterSource constructor with the expected
        arguments, and that the created WaterSource is stored in the expected location in the controller
        object """
        water_source_address = 5
        mock_water_source = mock.MagicMock()
        mock_water_source_constructor.return_value = mock_water_source

        uut.create_water_source_object(controller=self.controller, _water_source_address=water_source_address)

        mock_water_source_constructor.assert_called_with(_controller=self.controller, _ad=water_source_address)
        self.assertEquals(self.controller.water_sources[water_source_address],
                          mock_water_source)

    ###############################################
    @mock.patch('common.object_factory.PointOfControl', autospec=True)
    def test_create_point_of_control_object(self, mock_point_of_control_constructor):
        """ Verify that create_point_of_control_object calls PointOfControl constructor with the expected
        arguments, and that the created PointOfControl is stored in the expected location in the controller
        object """
        point_of_control_address = 5
        mock_point_of_control = mock.MagicMock()
        mock_point_of_control_constructor.return_value = mock_point_of_control

        uut.create_point_of_control_object(controller=self.controller,
                                           _point_of_control_address=point_of_control_address)

        mock_point_of_control_constructor.assert_called_with(_controller=self.controller, _ad=point_of_control_address)
        self.assertEquals(self.controller.points_of_control[point_of_control_address],
                          mock_point_of_control)

    ###############################################
    @mock.patch('common.object_factory.Mainline', autospec=True)
    def test_create_mainline_object(self, mock_mainline_constructor):
        """ Verify that create_mainline_object calls Mainline constructor with the expected
        arguments, and that the created Mainline is stored in the expected location in the controller
        object """
        mainline_address = 5
        mock_mainline = mock.MagicMock()
        mock_mainline_constructor.return_value = mock_mainline

        uut.create_mainline_object(controller=self.controller,
                                   _mainline_address=mainline_address)

        mock_mainline_constructor.assert_called_with(_controller=self.controller, _ad=mainline_address)
        self.assertEquals(self.controller.mainlines[mainline_address],
                          mock_mainline)

    ###############################################
    @mock.patch('common.object_factory.PG3200', autospec=True)
    def test_create_3200_program_object(self, mock_PG3200_constructor):
        """ Verify that create_PG3200_object calls PG3200 constructor with the expected
        arguments, and that the created PG3200 is stored in the expected location in the controller
        object """
        program_address = 12
        mock_PG3200 = mock.MagicMock()
        mock_PG3200_constructor.return_value = mock_PG3200

        uut.create_3200_program_object(controller=self.controller,
                                       program_address=program_address)

        mock_PG3200_constructor.assert_called_with(_controller=self.controller, _ad=program_address)
        self.assertEquals(self.controller.programs[program_address], mock_PG3200)

    ###############################################
    @mock.patch('common.object_factory.PG1000', autospec=True)
    def test_create_1000_program_object(self, mock_PG1000_constructor):
        """ Verify that create_PG1000_object calls PG1000 constructor with the expected
        arguments, and that the created PG1000 is stored in the expected location in the controller
        object """
        program_address = 12
        mock_controller = mock.MagicMock(spec=BaseStation1000, autospec=True)
        mock_controller.programs = dict()
        mock_PG1000 = mock.MagicMock()
        mock_PG1000_constructor.return_value = mock_PG1000

        uut.create_1000_program_object(controller=mock_controller,
                                       program_address=program_address)

        mock_PG1000_constructor.assert_called_with(_controller=mock_controller, _ad=program_address)
        self.assertEquals(mock_controller.programs[program_address], mock_PG1000)

    ###############################################
    @mock.patch('common.object_factory.ZoneProgram', autospec=True)
    def test_create_zone_program_object(self, mock_zoneprogram_constructor):
        """ Verify that create_ZoneProgram_object calls ZoneProgram constructor with the expected
        arguments, and that the created ZoneProgram is stored in the expected location in the controller
        object """
        program_address = 12
        zone_address = 24
        mock_zoneprogram = mock.MagicMock()
        mock_zoneprogram_constructor.return_value = mock_zoneprogram
        self.controller.programs = {program_address: mock.MagicMock()}
        self.controller.programs[program_address].zone_programs = {zone_address: mock.MagicMock()}
        uut.create_zone_program_object(controller=self.controller,
                                       program_address=program_address,
                                       zone_address=zone_address)

        mock_zoneprogram_constructor.assert_called_with(_controller=self.controller,
                                                        zone_ad=zone_address,
                                                        prog_ad=program_address)
        self.assertEquals(self.controller.programs[program_address].zone_programs[zone_address], mock_zoneprogram)

    ###############################################
    @mock.patch('common.object_factory.MoistureStartCondition', autospec=True)
    def test_create_moisture_start_condition_object(self, mock_moisture_start_condition_constructor):
        """ Verify that create_moisture_start_condition_object calls MoistureStartCondition constructor with
        the expected arguments, and that the created MoistureStartCondition is stored in the expected location in
        the controller object """
        moisture_sensor_address = 5
        program_address = 12
        mock_moisture_sensor = mock.MagicMock()
        mock_moisture_sensor.sn = 'SB01234'
        self.controller.moisture_sensors = {moisture_sensor_address: mock_moisture_sensor}
        mock_program = mock.MagicMock()
        mock_program.moisture_start_conditions = {moisture_sensor_address: None}
        self.controller.programs = {program_address: mock_program}
        mock_moisture_start_condition = mock.MagicMock()
        mock_moisture_start_condition_constructor.return_value = mock_moisture_start_condition

        uut.create_moisture_start_condition_object(controller=self.controller,
                                                   program_address=program_address,
                                                   moisture_sensor_address=moisture_sensor_address)

        mock_moisture_start_condition_constructor.assert_called_with(_controller=self.controller,
                                                                     _program_ad=program_address,
                                                                     _device_serial=mock_moisture_sensor.sn)
        self.assertEquals(self.controller.programs[program_address].moisture_start_conditions[moisture_sensor_address],
                          mock_moisture_start_condition)

    ###############################################
    @mock.patch('common.object_factory.MoistureStopCondition', autospec=True)
    def test_create_moisture_stop_condition_object(self, mock_moisture_stop_condition_constructor):
        """ Verify that create_moisture_stop_condition_object calls MoistureStopCondition constructor with
        the expected arguments, and that the created MoistureStopCondition is stored in the expected location in
        the controller object """
        moisture_sensor_address = 5
        program_address = 12
        mock_moisture_sensor = mock.MagicMock()
        mock_moisture_sensor.sn = 'SB01234'
        self.controller.moisture_sensors = {moisture_sensor_address: mock_moisture_sensor}
        mock_program = mock.MagicMock()
        mock_program.moisture_stop_conditions = {moisture_sensor_address: None}
        self.controller.programs = {program_address: mock_program}
        mock_moisture_stop_condition = mock.MagicMock()
        mock_moisture_stop_condition_constructor.return_value = mock_moisture_stop_condition

        uut.create_moisture_stop_condition_object(controller=self.controller,
                                                  program_address=program_address,
                                                  moisture_sensor_address=moisture_sensor_address)

        mock_moisture_stop_condition_constructor.assert_called_with(_controller=self.controller,
                                                                    _program_ad=program_address,
                                                                    _device_serial=mock_moisture_sensor.sn)
        self.assertEquals(self.controller.programs[program_address].moisture_stop_conditions[moisture_sensor_address],
                          mock_moisture_stop_condition)

    ###############################################
    @mock.patch('common.object_factory.MoisturePauseCondition', autospec=True)
    def test_create_moisture_pause_condition_object(self, mock_moisture_pause_condition_constructor):
        """ Verify that create_moisture_pause_condition_object calls MoisturePauseCondition constructor with
        the expected arguments, and that the created MoisturePauseCondition is stored in the expected location in
        the controller object """
        moisture_sensor_address = 5
        program_address = 12
        mock_moisture_sensor = mock.MagicMock()
        mock_moisture_sensor.sn = 'SB01234'
        self.controller.moisture_sensors = {moisture_sensor_address: mock_moisture_sensor}
        mock_program = mock.MagicMock()
        mock_program.moisture_pause_conditions = {moisture_sensor_address: None}
        self.controller.programs = {program_address: mock_program}
        mock_moisture_pause_condition = mock.MagicMock()
        mock_moisture_pause_condition_constructor.return_value = mock_moisture_pause_condition

        uut.create_moisture_pause_condition_object(controller=self.controller,
                                                   program_address=program_address,
                                                   moisture_sensor_address=moisture_sensor_address)

        mock_moisture_pause_condition_constructor.assert_called_with(_controller=self.controller,
                                                                     _program_ad=program_address,
                                                                     _device_serial=mock_moisture_sensor.sn)
        self.assertEquals(self.controller.programs[program_address].moisture_pause_conditions[moisture_sensor_address],
                          mock_moisture_pause_condition)

    ###############################################
    @mock.patch('common.object_factory.TemperatureStartCondition', autospec=True)
    def test_create_temperature_start_condition_object(self, mock_temperature_start_condition_constructor):
        """ Verify that create_temperature_start_condition_object calls TemperatureStartCondition constructor with
        the expected arguments, and that the created TemperatureStartCondition is stored in the expected location in
        the controller object """
        temperature_sensor_address = 5
        program_address = 12
        mock_temperature_sensor = mock.MagicMock()
        mock_temperature_sensor.sn = 'TAT1001'
        self.controller.temperature_sensors = {temperature_sensor_address: mock_temperature_sensor}
        mock_program = mock.MagicMock()
        mock_program.temperature_start_conditions = {temperature_sensor_address: None}
        self.controller.programs = {program_address: mock_program}
        mock_temperature_start_condition = mock.MagicMock()
        mock_temperature_start_condition_constructor.return_value = mock_temperature_start_condition

        uut.create_temperature_start_condition_object(controller=self.controller,
                                                      program_address=program_address,
                                                      temperature_sensor_address=temperature_sensor_address)

        mock_temperature_start_condition_constructor.assert_called_with(_controller=self.controller,
                                                                        _program_ad=program_address,
                                                                        _device_serial=mock_temperature_sensor.sn)
        self.assertEquals(
            self.controller.programs[program_address].temperature_start_conditions[temperature_sensor_address],
            mock_temperature_start_condition)

    ###############################################
    @mock.patch('common.object_factory.TemperatureStopCondition', autospec=True)
    def test_create_temperature_stop_condition_object(self, mock_temperature_stop_condition_constructor):
        """ Verify that create_temperature_stop_condition_object calls TemperatureStopCondition constructor with
        the expected arguments, and that the created TemperatureStopCondition is stored in the expected location in
        the controller object """
        temperature_sensor_address = 5
        program_address = 12
        mock_temperature_sensor = mock.MagicMock()
        mock_temperature_sensor.sn = 'TAT1001'
        self.controller.temperature_sensors = {temperature_sensor_address: mock_temperature_sensor}
        mock_program = mock.MagicMock()
        mock_program.temperature_stop_conditions = {temperature_sensor_address: None}
        self.controller.programs = {program_address: mock_program}
        mock_temperature_stop_condition = mock.MagicMock()
        mock_temperature_stop_condition_constructor.return_value = mock_temperature_stop_condition

        uut.create_temperature_stop_condition_object(controller=self.controller,
                                                     program_address=program_address,
                                                     temperature_sensor_address=temperature_sensor_address)

        mock_temperature_stop_condition_constructor.assert_called_with(_controller=self.controller,
                                                                       _program_ad=program_address,
                                                                       _device_serial=mock_temperature_sensor.sn)
        self.assertEquals(
            self.controller.programs[program_address].temperature_stop_conditions[temperature_sensor_address],
            mock_temperature_stop_condition)

    ###############################################
    @mock.patch('common.object_factory.TemperaturePauseCondition', autospec=True)
    def test_create_temperature_pause_condition_object(self, mock_temperature_pause_condition_constructor):
        """ Verify that create_temperature_pause_condition_object calls TemperaturePauseCondition constructor with
        the expected arguments, and that the created TemperaturePauseCondition is stored in the expected location in
        the controller object """
        temperature_sensor_address = 5
        program_address = 12
        mock_temperature_sensor = mock.MagicMock()
        mock_temperature_sensor.sn = 'TAT1001'
        self.controller.temperature_sensors = {temperature_sensor_address: mock_temperature_sensor}
        mock_program = mock.MagicMock()
        mock_program.temperature_pause_conditions = {temperature_sensor_address: None}
        self.controller.programs = {program_address: mock_program}
        mock_temperature_pause_condition = mock.MagicMock()
        mock_temperature_pause_condition_constructor.return_value = mock_temperature_pause_condition

        uut.create_temperature_pause_condition_object(controller=self.controller,
                                                      program_address=program_address,
                                                      temperature_sensor_address=temperature_sensor_address)

        mock_temperature_pause_condition_constructor.assert_called_with(_controller=self.controller,
                                                                        _program_ad=program_address,
                                                                        _device_serial=mock_temperature_sensor.sn)
        self.assertEquals(
            self.controller.programs[program_address].temperature_pause_conditions[temperature_sensor_address],
            mock_temperature_pause_condition)

    ###############################################
    @mock.patch('common.object_factory.PressureStartCondition', autospec=True)
    def test_create_pressure_start_condition_object(self, mock_pressure_start_condition_constructor):
        """ Verify that create_pressure_start_condition_object calls PressureStartCondition constructor with
        the expected arguments, and that the created PressureStartCondition is stored in the expected location in
        the controller object """
        pressure_sensor_address = 5
        program_address = 12
        mock_pressure_sensor = mock.MagicMock()
        mock_pressure_sensor.sn = 'PSF1001'
        self.controller.pressure_sensors = {pressure_sensor_address: mock_pressure_sensor}
        mock_program = mock.MagicMock()
        mock_program.pressure_start_conditions = {pressure_sensor_address: None}
        self.controller.programs = {program_address: mock_program}
        mock_pressure_start_condition = mock.MagicMock()
        mock_pressure_start_condition_constructor.return_value = mock_pressure_start_condition

        uut.create_pressure_start_condition_object(controller=self.controller,
                                                   program_address=program_address,
                                                   pressure_sensor_address=pressure_sensor_address)

        mock_pressure_start_condition_constructor.assert_called_with(_controller=self.controller,
                                                                     _program_ad=program_address,
                                                                     _device_serial=mock_pressure_sensor.sn)
        self.assertEquals(self.controller.programs[program_address].pressure_start_conditions[pressure_sensor_address],
                          mock_pressure_start_condition)

    ###############################################
    @mock.patch('common.object_factory.PressureStopCondition', autospec=True)
    def test_create_pressure_stop_condition_object(self, mock_pressure_stop_condition_constructor):
        """ Verify that create_pressure_stop_condition_object calls PressureStopCondition constructor with
        the expected arguments, and that the created PressureStopCondition is stored in the expected location in
        the controller object """
        pressure_sensor_address = 5
        program_address = 12
        mock_pressure_sensor = mock.MagicMock()
        mock_pressure_sensor.sn = 'PSF1001'
        self.controller.pressure_sensors = {pressure_sensor_address: mock_pressure_sensor}
        mock_program = mock.MagicMock()
        mock_program.pressure_stop_conditions = {pressure_sensor_address: None}
        self.controller.programs = {program_address: mock_program}
        mock_pressure_stop_condition = mock.MagicMock()
        mock_pressure_stop_condition_constructor.return_value = mock_pressure_stop_condition

        uut.create_pressure_stop_condition_object(controller=self.controller,
                                                  program_address=program_address,
                                                  pressure_sensor_address=pressure_sensor_address)

        mock_pressure_stop_condition_constructor.assert_called_with(_controller=self.controller,
                                                                    _program_ad=program_address,
                                                                    _device_serial=mock_pressure_sensor.sn)
        self.assertEquals(self.controller.programs[program_address].pressure_stop_conditions[pressure_sensor_address],
                          mock_pressure_stop_condition)

    ###############################################
    @mock.patch('common.object_factory.PressurePauseCondition', autospec=True)
    def test_create_pressure_pause_condition_object(self, mock_pressure_pause_condition_constructor):
        """ Verify that create_pressure_pause_condition_object calls PressurePauseCondition constructor with
        the expected arguments, and that the created PressurePauseCondition is stored in the expected location in
        the controller object """
        pressure_sensor_address = 5
        program_address = 12
        mock_pressure_sensor = mock.MagicMock()
        mock_pressure_sensor.sn = 'PSF1001'
        self.controller.pressure_sensors = {pressure_sensor_address: mock_pressure_sensor}
        mock_program = mock.MagicMock()
        mock_program.pressure_pause_conditions = {pressure_sensor_address: None}
        self.controller.programs = {program_address: mock_program}
        mock_pressure_pause_condition = mock.MagicMock()
        mock_pressure_pause_condition_constructor.return_value = mock_pressure_pause_condition

        uut.create_pressure_pause_condition_object(controller=self.controller,
                                                   program_address=program_address,
                                                   pressure_sensor_address=pressure_sensor_address)

        mock_pressure_pause_condition_constructor.assert_called_with(_controller=self.controller,
                                                                     _program_ad=program_address,
                                                                     _device_serial=mock_pressure_sensor.sn)
        self.assertEquals(self.controller.programs[program_address].pressure_pause_conditions[pressure_sensor_address],
                          mock_pressure_pause_condition)

    ###############################################
    @mock.patch('common.object_factory.SwitchStartCondition', autospec=True)
    def test_create_switch_start_condition_object(self, mock_event_switch_start_condition_constructor):
        """ Verify that create_switch_start_condition_object calls SwitchStartCondition constructor with
        the expected arguments, and that the created SwitchStartCondition is stored in the expected location in
        the controller object """
        event_switch_address = 5
        program_address = 12
        mock_event_switch_sensor = mock.MagicMock()
        mock_event_switch_sensor.sn = 'TPD1001'
        self.controller.event_switches = {event_switch_address: mock_event_switch_sensor}
        mock_program = mock.MagicMock()
        mock_program.event_switch_start_conditions = {event_switch_address: None}
        self.controller.programs = {program_address: mock_program}
        mock_event_switch_start_condition = mock.MagicMock()
        mock_event_switch_start_condition_constructor.return_value = mock_event_switch_start_condition

        uut.create_switch_start_condition_object(controller=self.controller,
                                                 program_address=program_address,
                                                 event_switch_address=event_switch_address)

        mock_event_switch_start_condition_constructor.assert_called_with(_controller=self.controller,
                                                                         _program_ad=program_address,
                                                                         _device_serial=mock_event_switch_sensor.sn)
        self.assertEquals(self.controller.programs[program_address].event_switch_start_conditions[event_switch_address],
                          mock_event_switch_start_condition)

    ###############################################
    @mock.patch('common.object_factory.SwitchStopCondition', autospec=True)
    def test_create_switch_stop_condition_object(self, mock_event_switch_stop_condition_constructor):
        """ Verify that create_switch_stop_condition_object calls SwitchStopCondition constructor with
        the expected arguments, and that the created SwitchStopCondition is stored in the expected location in
        the controller object """
        event_switch_address = 5
        program_address = 12
        mock_event_switch_sensor = mock.MagicMock()
        mock_event_switch_sensor.sn = 'TPD1001'
        self.controller.event_switches = {event_switch_address: mock_event_switch_sensor}
        mock_program = mock.MagicMock()
        mock_program.event_switch_stop_conditions = {event_switch_address: None}
        self.controller.programs = {program_address: mock_program}
        mock_event_switch_stop_condition = mock.MagicMock()
        mock_event_switch_stop_condition_constructor.return_value = mock_event_switch_stop_condition

        uut.create_switch_stop_condition_object(controller=self.controller,
                                                program_address=program_address,
                                                event_switch_address=event_switch_address)

        mock_event_switch_stop_condition_constructor.assert_called_with(_controller=self.controller,
                                                                        _program_ad=program_address,
                                                                        _device_serial=mock_event_switch_sensor.sn)
        self.assertEquals(self.controller.programs[program_address].event_switch_stop_conditions[event_switch_address],
                          mock_event_switch_stop_condition)

    ###############################################
    @mock.patch('common.object_factory.SwitchPauseCondition', autospec=True)
    def test_create_switch_pause_condition_object(self, mock_event_switch_pause_condition_constructor):
        """ Verify that create_switch_pause_condition_object calls SwitchPauseCondition constructor with
        the expected arguments, and that the created SwitchPauseCondition is stored in the expected location in
        the controller object """
        event_switch_address = 5
        program_address = 12
        mock_event_switch_sensor = mock.MagicMock()
        mock_event_switch_sensor.sn = 'TPD1001'
        self.controller.event_switches = {event_switch_address: mock_event_switch_sensor}
        mock_program = mock.MagicMock()
        mock_program.event_switch_pause_conditions = {event_switch_address: None}
        self.controller.programs = {program_address: mock_program}
        mock_event_switch_pause_condition = mock.MagicMock()
        mock_event_switch_pause_condition_constructor.return_value = mock_event_switch_pause_condition

        uut.create_switch_pause_condition_object(controller=self.controller,
                                                 program_address=program_address,
                                                 event_switch_address=event_switch_address)

        mock_event_switch_pause_condition_constructor.assert_called_with(_controller=self.controller,
                                                                         _program_ad=program_address,
                                                                         _device_serial=mock_event_switch_sensor.sn)
        self.assertEquals(self.controller.programs[program_address].event_switch_pause_conditions[event_switch_address],
                          mock_event_switch_pause_condition)

    ###############################################
    @mock.patch('common.object_factory.SwitchEmptyCondition', autospec=True)
    def test_create_switch_empty_condition_object(self, mock_event_switch_empty_condition_constructor):
        """ Verify that create_switch_empty_condition_object calls SwitchEmptyCondition constructor with
        the expected arguments, and that the created SwitchEmptyCondition is stored in the expected location in
        the controller object """
        empty_address = 1
        event_switch_address = 5
        water_source_address = 6
        mock_event_switch_sensor = mock.MagicMock()
        mock_event_switch_sensor.sn = 'TPD1001'
        self.controller.event_switches = {event_switch_address: mock_event_switch_sensor}
        mock_water_source = mock.MagicMock()
        mock_water_source.switch_empty_conditions = {event_switch_address: None}
        self.controller.water_sources = {water_source_address: mock_water_source}
        mock_event_switch_empty_condition = mock.MagicMock()
        mock_event_switch_empty_condition_constructor.return_value = mock_event_switch_empty_condition

        uut.create_switch_empty_condition_object(controller=self.controller,
                                                 empty_address=empty_address,
                                                 water_source_address=water_source_address,
                                                 event_switch_address=event_switch_address)

        mock_event_switch_empty_condition_constructor.assert_called_with(_controller=self.controller,
                                                                         _empty_condition_address=empty_address,
                                                                         _water_source_address=water_source_address,
                                                                         _device_serial_number=mock_event_switch_sensor.sn)
        self.assertEquals(
            self.controller.water_sources[water_source_address].switch_empty_conditions[event_switch_address],
            mock_event_switch_empty_condition)

    ###############################################
    @mock.patch('common.object_factory.PressureEmptyCondition', autospec=True)
    def test_create_pressure_empty_condition_object(self, mock_pressure_empty_condition_constructor):
        """ Verify that create_pressure_empty_condition_object calls PressureEmptyCondition constructor with
        the expected arguments, and that the created PressureEmptyCondition is stored in the expected location in
        the controller object """
        empty_address = 1
        pressure_sensor_address = 5
        water_source_address = 6
        mock_pressure_sensor = mock.MagicMock()
        mock_pressure_sensor.sn = 'TPD1001'
        self.controller.pressure_sensors = {pressure_sensor_address: mock_pressure_sensor}
        mock_water_source = mock.MagicMock()
        mock_water_source.pressure_empty_conditions = {pressure_sensor_address: None}
        self.controller.water_sources = {water_source_address: mock_water_source}
        mock_pressure_empty_condition = mock.MagicMock()
        mock_pressure_empty_condition_constructor.return_value = mock_pressure_empty_condition

        uut.create_pressure_empty_condition_object(controller=self.controller,
                                                   empty_address=empty_address,
                                                   water_source_address=water_source_address,
                                                   pressure_sensor_address=pressure_sensor_address)

        mock_pressure_empty_condition_constructor.assert_called_with(_controller=self.controller,
                                                                     _empty_condition_address=empty_address,
                                                                     _water_source_address=water_source_address,
                                                                     _device_serial_number=mock_pressure_sensor.sn)
        self.assertEquals(
            self.controller.water_sources[water_source_address].pressure_empty_conditions[pressure_sensor_address],
            mock_pressure_empty_condition)

    ###############################################
    @mock.patch('common.object_factory.MoistureEmptyCondition', autospec=True)
    def test_create_moisture_empty_condition_object(self, mock_moisture_empty_condition_constructor):
        """ Verify that create_moisture_empty_condition_object calls MoistureEmptyCondition constructor with
        the expected arguments, and that the created MoistureEmptyCondition is stored in the expected location in
        the controller object """
        empty_address = 1
        moisture_sensor_address = 5
        water_source_address = 6
        mock_moisture_sensor = mock.MagicMock()
        mock_moisture_sensor.sn = 'SB12345'
        self.controller.moisture_sensors = {moisture_sensor_address: mock_moisture_sensor}
        mock_water_source = mock.MagicMock()
        mock_water_source.moisture_empty_conditions = {moisture_sensor_address: None}
        self.controller.water_sources = {water_source_address: mock_water_source}
        mock_moisture_empty_condition = mock.MagicMock()
        mock_moisture_empty_condition_constructor.return_value = mock_moisture_empty_condition

        uut.create_moisture_empty_condition_object(controller=self.controller,
                                                   empty_address=empty_address,
                                                   water_source_address=water_source_address,
                                                   moisture_sensor_address=moisture_sensor_address)

        mock_moisture_empty_condition_constructor.assert_called_with(_controller=self.controller,
                                                                     _empty_condition_address=empty_address,
                                                                     _water_source_address=water_source_address,
                                                                     _device_serial_number=mock_moisture_sensor.sn)
        self.assertEquals(
            self.controller.water_sources[water_source_address].moisture_empty_conditions[moisture_sensor_address],
            mock_moisture_empty_condition)

    ###############################################
    @mock.patch('common.object_factory.BaseManagerConnection', autospec=True)
    def test_create_basemanager_connection_object(self, mock_basemanagerconnection_constructor):
        """ Verify that create_basemanager_connection_object calls BaseManagerConnection constructor with the expected
        argument, and that the created BaseManagerConnection is stored in the expected location in the controller
        object """
        mock_basemanagerconnection = mock.MagicMock()
        mock_basemanagerconnection_constructor.return_value = mock_basemanagerconnection
        
        fake_url = ''
        fake_fixed_ip = ''

        uut.create_basemanager_connection_object(controller=self.controller, url=fake_url, fixed_ip=fake_fixed_ip)

        mock_basemanagerconnection_constructor.assert_called_with(_controller=self.controller, _json_url=fake_url, _json_fixed_ip=fake_fixed_ip)
        self.assertEquals(self.controller.basemanager_connection[1],
                          mock_basemanagerconnection)

    ###############################################
    def test_create_valve_decoder_serial_numbers_dual(self):
        """ Verify that create_valve_decoder_serial_numbers generates the expected number of serial numbers """
        expected_serial_numbers = 2
        initial_serial_list = ['Q000045']
        serial_type = 'DUAL'

        result = uut.create_valve_decoder_serial_numbers(initial_serial_list, serial_type)

        self.assertEquals(len(result), expected_serial_numbers)
        self.assertEquals(result[0], initial_serial_list[0])
        self.assertEquals(result[expected_serial_numbers - 1], 'Q000046')

    ###############################################
    def test_create_valve_decoder_serial_numbers_quad(self):
        """ Verify that create_valve_decoder_serial_numbers generates the expected number of serial numbers """
        expected_serial_numbers = 4
        initial_serial_list = ['Q000045']
        serial_type = 'QUAD'

        result = uut.create_valve_decoder_serial_numbers(initial_serial_list, serial_type)

        self.assertEquals(len(result), expected_serial_numbers)
        self.assertEquals(result[0], initial_serial_list[0])
        self.assertEquals(result[expected_serial_numbers - 1], 'Q000048')

    ###############################################
    def test_create_valve_decoder_serial_numbers_twelve(self):
        """ Verify that create_valve_decoder_serial_numbers generates the expected number of serial numbers """
        expected_serial_numbers = 12
        initial_serial_list = ['Q000045']
        serial_type = 'TWELVE'

        result = uut.create_valve_decoder_serial_numbers(initial_serial_list, serial_type)

        self.assertEquals(len(result), expected_serial_numbers)
        self.assertEquals(result[0], initial_serial_list[0])
        self.assertEquals(result[expected_serial_numbers - 1], 'Q000056')

    ###############################################
    def test_create_valve_decoder_serial_numbers_twentyfour(self):
        """ Verify that create_valve_decoder_serial_numbers generates the expected number of serial numbers """
        expected_serial_numbers = 24
        initial_serial_list = ['Q000045']
        serial_type = 'TWENTYFOUR'

        result = uut.create_valve_decoder_serial_numbers(initial_serial_list, serial_type)

        self.assertEquals(len(result), expected_serial_numbers)
        self.assertEquals(result[0], initial_serial_list[0])
        self.assertEquals(result[expected_serial_numbers - 1], 'Q000068')

    ###############################################
    def test_create_valve_decoder_serial_numbers_fortyeight(self):
        """ Verify that create_valve_decoder_serial_numbers generates the expected number of serial numbers """
        expected_serial_numbers = 48
        initial_serial_list = ['Q000045']
        serial_type = 'FORTYEIGHT'

        result = uut.create_valve_decoder_serial_numbers(initial_serial_list, serial_type)

        self.assertEquals(len(result), expected_serial_numbers)
        self.assertEquals(result[0], initial_serial_list[0])
        self.assertEquals(result[expected_serial_numbers - 1], 'Q000092')

        ###############################################
        # @mock.patch('common.object_factory.FlowBicoder', autospec=True)
        # def test_create_flow_bicoders_for_sb(self, mock_flowbicoder_constructor):
        #     serial_list = ['SB12345', 'SB67789', 'BB123456']
        #     flowbicoders = [mock.MagicMock(), mock.MagicMock(), mock.MagicMock()]
        #     mock_flowbicoder_constructor.side_effect = flowbicoders
        #
        #     flow_bicoders = uut.create_flow_bicoders_for_sb(serial_list=serial_list,
        #                                                     controller=self.controller)
        #     self.assertEquals(len(flow_bicoders), 3)
        #     self.assertEquals(flow_bicoders[serial_list[0]], flowbicoders[0])
        #     self.assertEquals(flow_bicoders[serial_list[1]], flowbicoders[1])
        #     self.assertEquals(flow_bicoders[serial_list[2]], flowbicoders[2])

        ###############################################
        # @mock.patch('common.object_factory.PumpBicoder', autospec=True)
        # def test_create_pump_bicoders_for_sb(self, mock_pumpbicoder_constructor):
        #     serial_list = ['SB12345', 'SB67789', 'BB123456']
        #     pumpbicoders = [mock.MagicMock(), mock.MagicMock(), mock.MagicMock()]
        #     mock_pumpbicoder_constructor.side_effect = pumpbicoders
        #
        #     pump_bicoders = uut.create_pump_bicoders_for_sb(serial_list=serial_list,
        #                                                     controller=self.controller)
        #     self.assertEquals(len(pump_bicoders), 3)
        #     self.assertEquals(pump_bicoders[serial_list[0]], pumpbicoders[0])
        #     self.assertEquals(pump_bicoders[serial_list[1]], pumpbicoders[1])
        #     self.assertEquals(pump_bicoders[serial_list[2]], pumpbicoders[2])

        ###############################################
        # @mock.patch('common.object_factory.SwitchBicoder', autospec=True)
        # def test_create_switch_bicoders_for_sb(self, mock_switchbicoder_constructor):
        #     serial_list = ['SB12345', 'SB67789', 'BB123456']
        #     switchbicoders = [mock.MagicMock(), mock.MagicMock(), mock.MagicMock()]
        #     mock_switchbicoder_constructor.side_effect = switchbicoders
        #
        #     switch_bicoders = uut.create_switch_bicoders_for_sb(serial_list=serial_list,
        #                                                         controller=self.controller)
        #     self.assertEquals(len(switch_bicoders), 3)
        #     self.assertEquals(switch_bicoders[serial_list[0]], switchbicoders[0])
        #     self.assertEquals(switch_bicoders[serial_list[1]], switchbicoders[1])
        #     self.assertEquals(switch_bicoders[serial_list[2]], switchbicoders[2])

        ###############################################
        # @mock.patch('common.object_factory.MoistureBicoder', autospec=True)
        # def test_create_moisture_bicoders_for_sb(self, mock_moisturebicoder_constructor):
        #     serial_list = ['SB12345', 'SB67789', 'BB123456']
        #     moisturebicoders = [mock.MagicMock(), mock.MagicMock(), mock.MagicMock()]
        #     mock_moisturebicoder_constructor.side_effect = moisturebicoders
        #
        #     moisture_bicoders = uut.create_moisture_bicoders_for_sb(serial_list=serial_list,
        #                                                             controller=self.controller)
        #     self.assertEquals(len(moisture_bicoders), 3)
        #     self.assertEquals(moisture_bicoders[serial_list[0]], moisturebicoders[0])
        #     self.assertEquals(moisture_bicoders[serial_list[1]], moisturebicoders[1])
        #     self.assertEquals(moisture_bicoders[serial_list[2]], moisturebicoders[2])

        ###############################################
        # @mock.patch('common.object_factory.TempBicoder', autospec=True)
        # def test_create_temperature_bicoders_for_sb(self, mock_temperaturebicoder_constructor):
        #     serial_list = ['SB12345', 'SB67789', 'BB123456']
        #     temperaturebicoders = [mock.MagicMock(), mock.MagicMock(), mock.MagicMock()]
        #     mock_temperaturebicoder_constructor.side_effect = temperaturebicoders
        #
        #     temperature_bicoders = uut.create_temperature_bicoders_for_sb(serial_list=serial_list,
        #                                                                   controller=self.controller)
        #     self.assertEquals(len(temperature_bicoders), 3)
        #     self.assertEquals(temperature_bicoders[serial_list[0]], temperaturebicoders[0])
        #     self.assertEquals(temperature_bicoders[serial_list[1]], temperaturebicoders[1])
        #     self.assertEquals(temperature_bicoders[serial_list[2]], temperaturebicoders[2])

        ###############################################
        # def test_share_sb_device_with_cn(self):
        #     self.fail()
