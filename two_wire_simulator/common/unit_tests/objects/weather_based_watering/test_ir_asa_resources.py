__author__ = 'baseline'

import unittest
from common.epa_package import ir_asa_resources


class TestIrAsaResourcesObject(unittest.TestCase):

    def setUp(self):
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_frange_percent_pass1(self):
        """ Verifies valid list of percent values are returned with valid input: 0 to 3 percent """
        start = 0
        stop = 3
        step = 0.01
        expected = [0.0, 0.01, 0.02, 0.03]
        percent_list = ir_asa_resources.frange_percent(start, stop, step)
        self.assertEqual(first=expected, second=percent_list)

    def test_frange_percent_pass2(self):
        """ Verifies valid list of percent values are returned with valid input: 4 to 6 percent """
        start = 4
        stop = 6
        step = 0.01
        expected = [0.04, 0.05, 0.06]
        percent_list = ir_asa_resources.frange_percent(start, stop, step)
        self.assertEqual(first=expected, second=percent_list)

    def test_frange_percent_pass3(self):
        """ Verifies valid list of percent values are returned with valid input: 7 to 12 percent """
        start = 7
        stop = 12
        step = 0.01
        expected = [0.07, 0.08, 0.09, 0.10, 0.11, 0.12]
        percent_list = ir_asa_resources.frange_percent(start, stop, step)
        self.assertEqual(first=expected, second=percent_list)


if __name__ == '__main__':
    unittest.main()
