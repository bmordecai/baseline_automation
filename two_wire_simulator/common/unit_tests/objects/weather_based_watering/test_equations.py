import csv
import os
import unittest
import mock
import serial
import status_parser

# from common.epa_package.wbw_imports import *
from common.imports.types import SoilTexture
from common.epa_package.equations import *
from common.objects.base_classes.devices import BaseDevices
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.devices.zn import Zone

__author__ = 'baseline'
BaseDevices.controller_lat = float(43.609768)
BaseDevices.controller_long = float(-116.310569)


class TestEquationsObject(unittest.TestCase):
    # All soil textures (sl)
    soil_texture_zn1 = SoilTexture.loam
    soil_texture_zn2 = SoilTexture.silty_clay
    soil_texture_zn3 = SoilTexture.loamy_sand
    soil_texture_zn4 = SoilTexture.sandy_loam
    soil_texture_zn5 = SoilTexture.clay_loam
    soil_texture_zn6 = SoilTexture.clay

    # All slope percentages (sp)
    slope_zn1 = .06
    slope_zn2 = .10
    slope_zn3 = .08
    slope_zn4 = .12
    slope_zn5 = .02
    slope_zn6 = .20

    # All root zone working water storage (rzwws)
    rzwws_zn1 = .85
    rzwws_zn2 = .55
    rzwws_zn3 = .90
    rzwws_zn4 = 2.00
    rzwws_zn5 = 2.25
    rzwws_zn6 = .55

    # Crop(turf)/landscape coefficient (kc/kl)
    crop_landscape_coefficient_zn1 = .41  # January, 75% shade, fescue
    crop_landscape_coefficient_zn2 = .52  # January, full sun, bermuda
    crop_landscape_coefficient_zn3 = .55
    crop_landscape_coefficient_zn4 = .40
    crop_landscape_coefficient_zn5 = .61
    crop_landscape_coefficient_zn6 = .52  # January, full sun, bermuda

    # Precipitation rate (pr)
    precipitation_rate_zn1 = 1.60
    precipitation_rate_zn2 = 1.60
    precipitation_rate_zn3 = 1.40
    precipitation_rate_zn4 = 1.40
    precipitation_rate_zn5 = 0.20
    precipitation_rate_zn6 = .35

    # Estimated Application efficiency (du)
    application_efficiency_zn1 = .55
    application_efficiency_zn2 = .60
    application_efficiency_zn3 = .70
    application_efficiency_zn4 = .75
    application_efficiency_zn5 = .80
    application_efficiency_zn6 = .65

    # Basic soil intake rate (ir)
    soil_intake_rate_zn1 = .35
    soil_intake_rate_zn2 = .15
    soil_intake_rate_zn3 = .5
    soil_intake_rate_zn4 = .4
    soil_intake_rate_zn5 = .2
    soil_intake_rate_zn6 = .1

    # Allowable Depletion (ad)
    allowable_depletion_zn1 = .50
    allowable_depletion_zn2 = .40
    allowable_depletion_zn3 = .50
    allowable_depletion_zn4 = .55
    allowable_depletion_zn5 = .50
    allowable_depletion_zn6 = .35

    # Root zone depth (rd)
    root_depth_zn1 = 10
    root_depth_zn2 = 8.1
    root_depth_zn3 = 20.0
    root_depth_zn4 = 28.0
    root_depth_zn5 = 25.0
    root_depth_zn6 = 9.2

    # Available water (aw)
    available_water_zn1 = .17
    available_water_zn2 = .17
    available_water_zn3 = .09
    available_water_zn4 = .13
    available_water_zn5 = .18
    available_water_zn6 = .17

    # Reference crop evapotranspiration(retrieved from "user_case_2.py", day 7) (eto)
    evapotrans = 0.00575393

    # Gross amount of daily rainfall(in) (retrieved from "user_case_2.py", day 7) (ra)
    gross_rainfall = 0.338582

    # Yesterday's daily moisture balance(days 6 value from Tige's chart) (yesterdays_mb)
    yest_daily_moisture_balance = 0

    # Yesterday's runtime
    yest_runtime = 0

    # Yesterday's rainfall
    yest_rainfall = 0

    # Yesterday's evapotranspiration(retrieved from "user_case_2.py", day 6) (
    yest_evapotrans = 0.0787714

    # csvfile used to store test outputs
    filepath = os.path.dirname(os.path.abspath(__file__)) + "/test_equations.csv"
    csvfile = open(filepath, 'w')
    my_writer = csv.writer(csvfile, delimiter=" ", lineterminator='\n')

    # Creates the headers then empties the list
    output = ["zone,", "1,", "2,", "3,", "4,", "5,", "6,"]
    my_writer.writerow(output)
    del output[:]

    def setUp(self):
        """ Setting up for the test. """
        self.zone_list = []

        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the zone.
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)

        # Attach the mocked serial to the controller ser
        self.bl_3200.ser = self.mock_ser
        # Create the mainline attribute on the 3200
        self.bl_3200.mainlines = dict()
        main_line = mock.MagicMock()
        main_line.ad = 1
        self.bl_3200.mainlines[1] = main_line

        # Mock a valve bicoder to pass into the zone
        self.valve_bicoder = mock.MagicMock()
        # Set serial instance to mock serial
        self.zone = mock.MagicMock()
        BaseDevices.ser = self.mock_ser
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")

        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_zone_object(self, _diffserial="TSD0001", _diffaddress=1):
        """
        Creates a new zone object for use in a unit test
        """
        self.valve_bicoder.sn = _diffserial
        zone = Zone(_controller=self.bl_3200, _address=_diffaddress, _valve_bicoder=self.valve_bicoder)

        return zone

    def test_mocking(self):
        mock_key_value_returns = ["DN", "WT", "LF", "SO", "RD", "DN"]
        mock_get_value_string_by_key = mock.MagicMock(side_effect=mock_key_value_returns)

        # zones = []
        for zone in range(1, 7):
            newzone = self.create_test_zone_object()

            # this tells when 'get_value_string_by_key' is called, execute the mock_get.. instead
            newzone.data.get_value_string_by_key = mock_get_value_string_by_key

            self.zone_list.append(newzone)

        for zone in self.zone_list:
            status = zone.data.get_value_string_by_key("SS")
            print status

        print mock_key_value_returns
        self.zone.set_design_flow(_gallons_per_minute=5.0)
        # zone.df = 5.0
        test_pass = False


        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,DF=5")
        self.zone.data = mock_data


    """
    ****************************************************
    Tests for each zone of return_calculated_soak_time()
    ****************************************************
    """
    # CODE THAT WAS AN ALTERNATE IDEA OF HOW TO TEST AND OUTPUT TO CSV FILE
    # def test_return_calculated_soak_time(self):
    #     """ Verifies the soak time of all zones and outputs them to an excel file """
    #     self.output.append(self._testMethodName + ",")
    #     # soak time
    #     val_returned1 = return_calculated_soak_time(_sl=self.soil_texture_zn1, _pr=self.precipitation_rate_zn1,
    #                                                 _sp=self.slope_zn1, _du=self.application_efficiency_zn1)
    #     self.output.append(str(val_returned1) + ",")
    #
    #     val_returned2 = return_calculated_soak_time(_sl=self.soil_texture_zn2, _pr=self.precipitation_rate_zn2,
    #                                                 _sp=self.slope_zn2, _du=self.application_efficiency_zn2)
    #     self.output.append(str(val_returned2) + ",")
    #
    #     val_returned3 = return_calculated_soak_time(_sl=self.soil_texture_zn3, _pr=self.precipitation_rate_zn3,
    #                                                 _sp=self.slope_zn3, _du=self.application_efficiency_zn3)
    #     self.output.append(str(val_returned3) + ",")
    #
    #     val_returned4 = return_calculated_soak_time(_sl=self.soil_texture_zn4, _pr=self.precipitation_rate_zn4,
    #                                                _sp=self.slope_zn4, _du=self.application_efficiency_zn4)
    #     self.output.append(str(val_returned4) + ",")
    #
    #     val_returned5 = return_calculated_soak_time(_sl=self.soil_texture_zn5, _pr=self.precipitation_rate_zn5,
    #                                                _sp=self.slope_zn5, _du=self.application_efficiency_zn5)
    #     self.output.append(str(val_returned5) + ",")
    #
    #     val_returned6 = return_calculated_soak_time(_sl=self.soil_texture_zn6, _pr=self.precipitation_rate_zn6,
    #                                                _sp=self.slope_zn6, _du=self.application_efficiency_zn6)
    #     self.output.append(str(val_returned6) + ",")
    #
    #     self.my_writer.writerow(self.output)
    #     del self.output[:]


    def test_return_calculated_soak_time_zone1(self):
        """ Verifies the soak time of zone 1 """
        # soak time
        val_returned = return_calculated_soak_time(_sl=self.soil_texture_zn1, _pr=self.precipitation_rate_zn1,
                                                   _sp=self.slope_zn1, _du=self.application_efficiency_zn1)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("return_calculated_soak_time" + ',')
        self.output.append(str(val_returned) + ',')

    def test_return_calculated_soak_time_zone2(self):
        """ Verifies the soak time of zone 2 """
        # soak time
        val_returned = return_calculated_soak_time(_sl=self.soil_texture_zn2, _pr=self.precipitation_rate_zn2,
                                                   _sp=self.slope_zn2, _du=self.application_efficiency_zn2)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_calculated_soak_time_zone3(self):
        """ Verifies the soak time of zone 3 """
        # soak time
        val_returned = return_calculated_soak_time(_sl=self.soil_texture_zn3, _pr=self.precipitation_rate_zn3,
                                                   _sp=self.slope_zn3, _du=self.application_efficiency_zn3)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_calculated_soak_time_zone4(self):
        """ Verifies the soak time of zone 4 """
        # soak time
        val_returned = return_calculated_soak_time(_sl=self.soil_texture_zn4, _pr=self.precipitation_rate_zn4,
                                                   _sp=self.slope_zn4, _du=self.application_efficiency_zn4)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_calculated_soak_time_zone5(self):
        """ Verifies the soak time of zone 5 """
        # soak time
        val_returned = return_calculated_soak_time(_sl=self.soil_texture_zn5, _pr=self.precipitation_rate_zn5,
                                                   _sp=self.slope_zn5, _du=self.application_efficiency_zn5)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_calculated_soak_time_zone6(self):
        """ Verifies the soak time of zone 6 """
        # soak time
        val_returned = return_calculated_soak_time(_sl=self.soil_texture_zn6, _pr=self.precipitation_rate_zn6,
                                                   _sp=self.slope_zn6, _du=self.application_efficiency_zn6)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]


    """
    *********************************************
    Tests for each zone of calculated_soak_time()
    *********************************************
    """
    def test_calculated_soak_time_zone1(self):
        """ Verifies the soak time of zone 1 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn1, _pr=self.precipitation_rate_zn1,
                                          _sp=self.slope_zn1)
        # gross irrigation
        i = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn1)
        # net irrigation
        da = calculate_net_irrigation(_i=i, _du=self.application_efficiency_zn1)
        # soak time
        val_returned = calculate_soak_time(_sl=self.soil_texture_zn1, _da=da)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("calculated_soak_time" + ',')
        self.output.append(str(val_returned) + ',')

    def test_calculated_soak_time_zone2(self):
        """ Verifies the soak time of zone 2 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn2, _pr=self.precipitation_rate_zn2,
                                          _sp=self.slope_zn2)
        # gross irrigation
        i = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn2)
        # net irrigation
        da = calculate_net_irrigation(_i=i, _du=self.application_efficiency_zn2)
        # soak time
        val_returned = calculate_soak_time(_sl=self.soil_texture_zn2, _da=da)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculated_soak_time_zone3(self):
        """ Verifies the soak time of zone 3 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn3, _pr=self.precipitation_rate_zn3,
                                          _sp=self.slope_zn3)
        # gross irrigation
        i = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn3)
        # net irrigation
        da = calculate_net_irrigation(_i=i, _du=self.application_efficiency_zn3)
        # soak time
        val_returned = calculate_soak_time(_sl=self.soil_texture_zn3, _da=da)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculated_soak_time_zone4(self):
        """ Verifies the soak time of zone 4 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn4, _pr=self.precipitation_rate_zn4,
                                          _sp=self.slope_zn4)
        # gross irrigation
        i = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn4)
        # net irrigation
        da = calculate_net_irrigation(_i=i, _du=self.application_efficiency_zn4)
        # soak time
        val_returned = calculate_soak_time(_sl=self.soil_texture_zn4, _da=da)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculated_soak_time_zone5(self):
        """ Verifies the soak time of zone 5 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn5, _pr=self.precipitation_rate_zn5,
                                          _sp=self.slope_zn5)
        # gross irrigation
        i = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn5)
        # net irrigation
        da = calculate_net_irrigation(_i=i, _du=self.application_efficiency_zn5)
        # soak time
        val_returned = calculate_soak_time(_sl=self.soil_texture_zn5, _da=da)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculated_soak_time_zone6(self):
        """ Verifies the soak time of zone 6 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn6, _pr=self.precipitation_rate_zn6,
                                          _sp=self.slope_zn6)
        # gross irrigation
        i = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn6)
        # net irrigation
        da = calculate_net_irrigation(_i=i, _du=self.application_efficiency_zn6)
        # soak time
        val_returned = calculate_soak_time(_sl=self.soil_texture_zn6, _da=da)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]

    """
    **************************************************************************
    Tests for each zone of calculate_landscape_coefficient() for zones 3, 4, 5
    **************************************************************************
    """
    def test_calculate_landscape_coefficient_zone3(self):
        """ Verifies the landscape coefficient of zone 3 """
        # species factor
        ks = .5
        # density factor
        kd = 1.0
        # shade index
        kmc = 1.1
        # landscape coefficient
        val_returned = calculate_landscape_coefficient(_ks=ks, _kd=kd, _kmc=kmc)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("calculate_landscape_coefficient" + ',')
        self.output.append(',')
        self.output.append(',')
        self.output.append(str(val_returned) + ',')

    def test_calculate_landscape_coefficient_zone4(self):
        """ Verifies the landscape coefficient of zone 4 """
        # species factor
        ks = .5
        # density factor
        kd = 1.0
        # shade index
        kmc = 0.8
        # landscape coefficient
        val_returned = calculate_landscape_coefficient(_ks=ks, _kd=kd, _kmc=kmc)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_landscape_coefficient_zone5(self):
        """ Verifies the landscape coefficient of zone 5 """
        # species factor
        ks = .5
        # density factor
        kd = 1.1
        # shade index
        kmc = 1.1
        # landscape coefficient
        val_returned = calculate_landscape_coefficient(_ks=ks, _kd=kd, _kmc=kmc)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]

    """
    *****************************************************
    Tests for each zone of return_calculated_cycle_time()
    *****************************************************
    """
    def test_return_calculate_cycle_time_zone1(self):
        """ Verifies the cycle time of zone 1 """
        # cycle time
        val_returned = return_calculated_cycle_time(_sl=self.soil_texture_zn1, _pr=self.precipitation_rate_zn1,
                                                    _sp=self.slope_zn1)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("return_calculated_cycle_time" + ',')
        self.output.append(str(val_returned) + ',')

    def test_return_calculate_cycle_time_zone2(self):
        """ Verifies the cycle time of zone 2 """
        # cycle time
        val_returned = return_calculated_cycle_time(_sl=self.soil_texture_zn2, _pr=self.precipitation_rate_zn2,
                                                    _sp=self.slope_zn2)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_calculate_cycle_time_zone3(self):
        """ Verifies the cycle time of zone 3 """
        # cycle time
        val_returned = return_calculated_cycle_time(_sl=self.soil_texture_zn3, _pr=self.precipitation_rate_zn3,
                                                    _sp=self.slope_zn3)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_calculate_cycle_time_zone4(self):
        """ Verifies the cycle time of zone 4 """
        # cycle time
        val_returned = return_calculated_cycle_time(_sl=self.soil_texture_zn4, _pr=self.precipitation_rate_zn4,
                                                    _sp=self.slope_zn4)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_calculate_cycle_time_zone5(self):
        """ Verifies the cycle time of zone 5 """
        # cycle time
        val_returned = return_calculated_cycle_time(_sl=self.soil_texture_zn5, _pr=self.precipitation_rate_zn5,
                                                    _sp=self.slope_zn5)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_calculate_cycle_time_zone6(self):
        """ Verifies the cycle time of zone 6 """
        # cycle time
        val_returned = return_calculated_cycle_time(_sl=self.soil_texture_zn6, _pr=self.precipitation_rate_zn6,
                                                    _sp=self.slope_zn6)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]

    """
    **********************************************
    Tests for each zone of calculated_cycle_time()
    **********************************************
    """

    def test_calculate_cycle_time_zone1(self):
        """ Verifies the cycle time of zone 1 """
        # cycle time
        val_returned = calculate_cycle_time(_sl=self.soil_texture_zn1, _pr=self.precipitation_rate_zn1,
                                            _sp=self.slope_zn1)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("calculate_cycle_time" + ',')
        self.output.append(str(val_returned) + ',')

    def test_calculate_cycle_time_zone2(self):
        """ Verifies the cycle time of zone 2 """
        # cycle time
        val_returned = calculate_cycle_time(_sl=self.soil_texture_zn2, _pr=self.precipitation_rate_zn2,
                                            _sp=self.slope_zn2)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_cycle_time_zone3(self):
        """ Verifies the cycle time of zone 3 """
        # cycle time
        val_returned = calculate_cycle_time(_sl=self.soil_texture_zn3, _pr=self.precipitation_rate_zn3,
                                            _sp=self.slope_zn3)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_cycle_time_zone4(self):
        """ Verifies the cycle time of zone 4 """
        # cycle time
        val_returned = calculate_cycle_time(_sl=self.soil_texture_zn4, _pr=self.precipitation_rate_zn4,
                                            _sp=self.slope_zn4)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_cycle_time_zone5(self):
        """ Verifies the cycle time of zone 5 """
        # cycle time
        val_returned = calculate_cycle_time(_sl=self.soil_texture_zn5, _pr=self.precipitation_rate_zn5,
                                            _sp=self.slope_zn5)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_cycle_time_zone6(self):
        """ Verifies the cycle time of zone 6 """
        # cycle time
        val_returned = calculate_cycle_time(_sl=self.soil_texture_zn6, _pr=self.precipitation_rate_zn6,
                                            _sp=self.slope_zn6)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]

    """
    **********************************************************
    Tests for each zone of calculate_gross_irrigation_amount()
    **********************************************************
    """

    def test_calculate_gross_irrigation_amount_zone1(self):
        """ Verifies the gross irrigation of zone 1 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn1, _pr=self.precipitation_rate_zn1,
                                          _sp=self.slope_zn1)
        # gross irrigation
        val_returned = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn1)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("calculate_gross_irrigation_amount" + ',')
        self.output.append(str(val_returned) + ',')

    def test_calculate_gross_irrigation_amount_zone2(self):
        """ Verifies the gross irrigation of zone 2 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn2, _pr=self.precipitation_rate_zn2,
                                          _sp=self.slope_zn2)
        # gross irrigation
        val_returned = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn2)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_gross_irrigation_amount_zone3(self):
        """ Verifies the gross irrigation of zone 3 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn3, _pr=self.precipitation_rate_zn3,
                                          _sp=self.slope_zn3)
        # gross irrigation
        val_returned = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn3)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_gross_irrigation_amount_zone4(self):
        """ Verifies the gross irrigation of zone 4 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn4, _pr=self.precipitation_rate_zn4,
                                          _sp=self.slope_zn4)
        # gross irrigation
        val_returned = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn4)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_gross_irrigation_amount_zone5(self):
        """ Verifies the gross irrigation of zone 5 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn5, _pr=self.precipitation_rate_zn5,
                                          _sp=self.slope_zn5)
        # gross irrigation
        val_returned = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn5)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_gross_irrigation_amount_zone6(self):
        """ Verifies the gross irrigation of zone 6 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn6, _pr=self.precipitation_rate_zn6,
                                          _sp=self.slope_zn6)
        # gross irrigation
        val_returned = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn6)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]

    """
    *************************************************
    Tests for each zone of calculate_net_irrigation()
    *************************************************
    """
    def test_calculate_net_irrigation_zone1(self):
        """ Verifies the net irrigation of zone 1 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn1, _pr=self.precipitation_rate_zn1,
                                          _sp=self.slope_zn1)
        # gross irrigation
        i = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn1)
        # net irrigation
        val_returned = calculate_net_irrigation(_i=i, _du=self.application_efficiency_zn1)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("calculate_net_irrigation" + ',')
        self.output.append(str(val_returned) + ',')

    def test_calculate_net_irrigation_zone2(self):
        """ Verifies the net irrigation of zone 2 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn2, _pr=self.precipitation_rate_zn2,
                                          _sp=self.slope_zn2)
        # gross irrigation
        i = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn2)
        # net irrigation
        val_returned = calculate_net_irrigation(_i=i, _du=self.application_efficiency_zn2)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_net_irrigation_zone3(self):
        """ Verifies the net irrigation of zone 3 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn3, _pr=self.precipitation_rate_zn3,
                                          _sp=self.slope_zn3)
        # gross irrigation
        i = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn3)
        # net irrigation
        val_returned = calculate_net_irrigation(_i=i, _du=self.application_efficiency_zn3)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_net_irrigation_zone4(self):
        """ Verifies the net irrigation of zone 4 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn4, _pr=self.precipitation_rate_zn4,
                                          _sp=self.slope_zn4)
        # gross irrigation
        i = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn4)
        # net irrigation
        val_returned = calculate_net_irrigation(_i=i, _du=self.application_efficiency_zn4)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_net_irrigation_zone5(self):
        """ Verifies the net irrigation of zone 5 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn5, _pr=self.precipitation_rate_zn5,
                                          _sp=self.slope_zn5)
        # gross irrigation
        i = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn5)
        # net irrigation
        val_returned = calculate_net_irrigation(_i=i, _du=self.application_efficiency_zn5)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_net_irrigation_zone6(self):
        """ Verifies the net irrigation of zone 6 """
        # cycle time
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn6, _pr=self.precipitation_rate_zn6,
                                          _sp=self.slope_zn6)
        # gross irrigation
        i = calculate_gross_irrigation_amount(_ct=ct, _pr=self.precipitation_rate_zn6)
        # net irrigation
        val_returned = calculate_net_irrigation(_i=i, _du=self.application_efficiency_zn6)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]


    """
    **************************************
    Tests for each zone of calculate_etc()
    **************************************
    """

    def test_calculate_etc_zone1(self):
        """ Verifies the etc of zone 1 """
        # moisture requirements
        val_returned = calculate_etc(_kc=self.crop_landscape_coefficient_zn1, _eto=self.evapotrans)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("calculate_etc" + ',')
        self.output.append(str(val_returned) + ',')

    def test_calculate_etc_zone2(self):
        """ Verifies the etc of zone 2 """
        # moisture requirements
        val_returned = calculate_etc(_kc=self.crop_landscape_coefficient_zn2, _eto=self.evapotrans)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_etc_zone3(self):
        """ Verifies the etc of zone 3 """
        # moisture requirements
        val_returned = calculate_etc(_kc=self.crop_landscape_coefficient_zn3, _eto=self.evapotrans)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_etc_zone4(self):
        """ Verifies the etc of zone 4 """
        # moisture requirements
        val_returned = calculate_etc(_kc=self.crop_landscape_coefficient_zn4, _eto=self.evapotrans)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_etc_zone5(self):
        """ Verifies the etc of zone 5 """
        # moisture requirements
        val_returned = calculate_etc(_kc=self.crop_landscape_coefficient_zn5, _eto=self.evapotrans)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_etc_zone6(self):
        """ Verifies the etc of zone 6 """
        # moisture requirements
        val_returned = calculate_etc(_kc=self.crop_landscape_coefficient_zn6, _eto=self.evapotrans)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]


    """
    *********************************************
    Tests for each zone of calculate_free_water()
    *********************************************
    """

    def test_calculate_free_water__zone1(self):
        """ Verifies the free water of zone 1 """
        # cycle time in minutes
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn1, _pr=self.precipitation_rate_zn1,
                                          _sp=self.slope_zn1)
        # free water
        val_returned = calculate_free_water_(_ct=ct, _pr=self.precipitation_rate_zn1, _ir=self.soil_intake_rate_zn1)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("calculate_free_water" + ',')
        self.output.append(str(val_returned) + ',')

    def test_calculate_free_water__zone2(self):
        """ Verifies the free water of zone 2 """
        # cycle time in minutes
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn2, _pr=self.precipitation_rate_zn2,
                                          _sp=self.slope_zn2)
        # free water
        val_returned = calculate_free_water_(_ct=ct, _pr=self.precipitation_rate_zn2, _ir=self.soil_intake_rate_zn2)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_free_water__zone3(self):
        """ Verifies the free water of zone 3 """
        # cycle time in minutes
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn3, _pr=self.precipitation_rate_zn3,
                                          _sp=self.slope_zn3)
        # free water
        val_returned = calculate_free_water_(_ct=ct, _pr=self.precipitation_rate_zn3, _ir=self.soil_intake_rate_zn3)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_free_water__zone4(self):
        """ Verifies the free water of zone 4 """
        # cycle time in minutes
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn4, _pr=self.precipitation_rate_zn4,
                                          _sp=self.slope_zn4)
        # free water
        val_returned = calculate_free_water_(_ct=ct, _pr=self.precipitation_rate_zn4, _ir=self.soil_intake_rate_zn4)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_free_water__zone5(self):
        """ Verifies the free water of zone 5 """
        # cycle time in minutes
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn5, _pr=self.precipitation_rate_zn5,
                                          _sp=self.slope_zn5)
        # free water
        val_returned = calculate_free_water_(_ct=ct, _pr=self.precipitation_rate_zn5, _ir=self.soil_intake_rate_zn5)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_free_water__zone6(self):
        """ Verifies the free water of zone 6 """
        # cycle time in minutes
        ct = return_calculated_cycle_time(_sl=self.soil_texture_zn6, _pr=self.precipitation_rate_zn6,
                                          _sp=self.slope_zn6)
        # free water
        val_returned = calculate_free_water_(_ct=ct, _pr=self.precipitation_rate_zn6, _ir=self.soil_intake_rate_zn6)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]


    """
    ********************************************
    Tests for each zone of calculate_rain_fall()
    ********************************************
    """

    def test_calculate_rain_fall_zone1(self):
        """ Verifies the net rainfall in inches for zone 1 """
        # net rainfall in inches
        val_returned = calculate_rain_fall(_ra=self.gross_rainfall)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("calculate_rain_fall" + ',')
        self.output.append(str(val_returned) + ',')

    def test_calculate_rain_fall_zone2(self):
        """ Verifies the net rainfall in inches for zone 2 """
        # net rainfall in inches
        val_returned = calculate_rain_fall(_ra=self.gross_rainfall)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_rain_fall_zone3(self):
        """ Verifies the net rainfall in inches for zone 3 """
        # net rainfall in inches
        val_returned = calculate_rain_fall(_ra=self.gross_rainfall)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_rain_fall_zone4(self):
        """ Verifies the net rainfall in inches for zone 4 """
        # net rainfall in inches
        val_returned = calculate_rain_fall(_ra=self.gross_rainfall)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_rain_fall_zone5(self):
        """ Verifies the net rainfall in inches for zone 5 """
        # net rainfall in inches
        val_returned = calculate_rain_fall(_ra=self.gross_rainfall)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_rain_fall_zone6(self):
        """ Verifies the net rainfall in inches for zone 6 """
        # net rainfall in inches
        val_returned = calculate_rain_fall(_ra=self.gross_rainfall)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]

    """
    ********************************************************
    Tests for each zone of calculate_definition_of_deficit()
    ********************************************************
    """

    def test_calculate_definition_of_deficit_zone1(self):
        """ Verifies the net rainfall in inches for zone 1 """
        # moisture balance calculation TODO: equation not complete

    """
    *************************************************************************
    Tests for each zone of return_calculate_root_zone_working_water_storage()
    *************************************************************************
    """

    def test_return_calculate_root_zone_working_water_storage_zone1(self):
        """ Verifies the amount of water that can be stored in the roots of zone 1 """
        # root zone working water storage
        val_returned = return_calculate_root_zone_working_water_storage(_sl=self.soil_texture_zn1,
                                                                        _ad=self.allowable_depletion_zn1,
                                                                        _rd=self.root_depth_zn1)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("return_calculate_root_zone_working_water_storage" + ',')
        self.output.append(str(val_returned) + ',')

    def test_return_calculate_root_zone_working_water_storage_zone2(self):
        """ Verifies the amount of water that can be stored in the roots of zone 2 """
        # root zone working water storage
        val_returned = return_calculate_root_zone_working_water_storage(_sl=self.soil_texture_zn2,
                                                                        _ad=self.allowable_depletion_zn2,
                                                                        _rd=self.root_depth_zn2)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_calculate_root_zone_working_water_storage_zone3(self):
        """ Verifies the amount of water that can be stored in the roots of zone 3 """
        # root zone working water storage
        val_returned = return_calculate_root_zone_working_water_storage(_sl=self.soil_texture_zn3,
                                                                        _ad=self.allowable_depletion_zn3,
                                                                        _rd=self.root_depth_zn3)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_calculate_root_zone_working_water_storage_zone4(self):
        """ Verifies the amount of water that can be stored in the roots of zone 4 """
        # root zone working water storage
        val_returned = return_calculate_root_zone_working_water_storage(_sl=self.soil_texture_zn4,
                                                                        _ad=self.allowable_depletion_zn4,
                                                                        _rd=self.root_depth_zn4)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_calculate_root_zone_working_water_storage_zone5(self):
        """ Verifies the amount of water that can be stored in the roots of zone 5 """
        # root zone working water storage
        val_returned = return_calculate_root_zone_working_water_storage(_sl=self.soil_texture_zn5,
                                                                        _ad=self.allowable_depletion_zn5,
                                                                        _rd=self.root_depth_zn5)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_calculate_root_zone_working_water_storage_zone6(self):
        """ Verifies the amount of water that can be stored in the roots of zone 6 """
        # root zone working water storage
        val_returned = return_calculate_root_zone_working_water_storage(_sl=self.soil_texture_zn6,
                                                                        _ad=self.allowable_depletion_zn6,
                                                                        _rd=self.root_depth_zn6)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]

    """
    ******************************************************************
    Tests for each zone of calculate_root_zone_working_water_storage()
    ******************************************************************
    """

    def test_calculate_root_zone_working_water_storage_zone1(self):
        """ Verifies the amount of water that can be stored in the roots of zone 1 """
        # root zone working water storage
        val_returned = calculate_root_zone_working_water_storage(_ad=self.allowable_depletion_zn1,
                                                                 _aw=self.available_water_zn1,
                                                                 _rd=self.root_depth_zn1)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("calculate_root_zone_working_water_storage" + ',')
        self.output.append(str(val_returned) + ',')

    def test_calculate_root_zone_working_water_storage_zone2(self):
        """ Verifies the amount of water that can be stored in the roots of zone 2 """
        # root zone working water storage
        val_returned = calculate_root_zone_working_water_storage(_ad=self.allowable_depletion_zn2,
                                                                 _aw=self.available_water_zn2,
                                                                 _rd=self.root_depth_zn2)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_root_zone_working_water_storage_zone3(self):
        """ Verifies the amount of water that can be stored in the roots of zone 3 """
        # root zone working water storage
        val_returned = calculate_root_zone_working_water_storage(_ad=self.allowable_depletion_zn3,
                                                                 _aw=self.available_water_zn3,
                                                                 _rd=self.root_depth_zn3)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_root_zone_working_water_storage_zone4(self):
        """ Verifies the amount of water that can be stored in the roots of zone 4 """
        # root zone working water storage
        val_returned = calculate_root_zone_working_water_storage(_ad=self.allowable_depletion_zn4,
                                                                 _aw=self.available_water_zn4,
                                                                 _rd=self.root_depth_zn4)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_root_zone_working_water_storage_zone5(self):
        """ Verifies the amount of water that can be stored in the roots of zone 5 """
        # root zone working water storage
        val_returned = calculate_root_zone_working_water_storage(_ad=self.allowable_depletion_zn5,
                                                                 _aw=self.available_water_zn5,
                                                                 _rd=self.root_depth_zn5)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_calculate_root_zone_working_water_storage_zone6(self):
        """ Verifies the amount of water that can be stored in the roots of zone 6 """
        # root zone working water storage
        val_returned = calculate_root_zone_working_water_storage(_ad=self.allowable_depletion_zn6,
                                                                 _aw=self.available_water_zn6,
                                                                 _rd=self.root_depth_zn6)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]

    """
    ********************************************************
    Tests for each zone of calculate_definition_of_surplus()
    ********************************************************
    """

    def test_calculate_definition_of_surplus_zone1(self):
        """ Verifies the definition of surplus of zone 1 """
        # moisture balance calculation(used day 7)
        mb = .11
        # root zone working water storage(from chart)
        rzwws = None  # TODO waiting on equation completion

    """
    ******************************************************************
    Tests for each zone of return_todays_calculated_moisture_balance()
    ******************************************************************
    """

    def test_return_todays_calculated_moisture_balance_zone1(self):
        """ Verifies the daily moisture balance of zone 1 """
        # etc value to be passed into a method
        etc = calculate_etc(_kc=self.crop_landscape_coefficient_zn1, _eto=self.evapotrans)
        # daily moisture balance
        val_returned = return_todays_calculated_moisture_balance(_yesterdays_mb=self.yest_daily_moisture_balance,
                                                                 _yesterdays_rt=self.yest_runtime,
                                                                 _pr=self.precipitation_rate_zn1,
                                                                 _du=self.application_efficiency_zn1,
                                                                 _ra=self.gross_rainfall,
                                                                 _etc=etc)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("return_todays_calculated_daily_moisture_balance" + ',')
        self.output.append(str(val_returned) + ',')

    def test_return_todays_calculated_moisture_balance_zone2(self):
        """ Verifies the daily moisture balance of zone 2 """
        # etc value to be passed into a method
        etc = calculate_etc(_kc=self.crop_landscape_coefficient_zn2, _eto=self.evapotrans)
        # daily moisture balance
        val_returned = return_todays_calculated_moisture_balance(_yesterdays_mb=self.yest_daily_moisture_balance,
                                                                 _yesterdays_rt=self.yest_runtime,
                                                                 _pr=self.precipitation_rate_zn2,
                                                                 _du=self.application_efficiency_zn2,
                                                                 _ra=self.gross_rainfall,
                                                                 _etc=etc)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_todays_calculated_moisture_balance_zone3(self):
        """ Verifies the daily moisture balance of zone 3 """
        # etc value to be passed into a method
        etc = calculate_etc(_kc=self.crop_landscape_coefficient_zn3, _eto=self.evapotrans)
        # daily moisture balance
        val_returned = return_todays_calculated_moisture_balance(_yesterdays_mb=self.yest_daily_moisture_balance,
                                                                 _yesterdays_rt=self.yest_runtime,
                                                                 _pr=self.precipitation_rate_zn3,
                                                                 _du=self.application_efficiency_zn3,
                                                                 _ra=self.gross_rainfall,
                                                                 _etc=etc)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_todays_calculated_moisture_balance_zone4(self):
        """ Verifies the daily moisture balance of zone 4 """
        # etc value to be passed into a method
        etc = calculate_etc(_kc=self.crop_landscape_coefficient_zn4, _eto=self.evapotrans)
        # daily moisture balance
        val_returned = return_todays_calculated_moisture_balance(_yesterdays_mb=self.yest_daily_moisture_balance,
                                                                 _yesterdays_rt=self.yest_runtime,
                                                                 _pr=self.precipitation_rate_zn4,
                                                                 _du=self.application_efficiency_zn4,
                                                                 _ra=self.gross_rainfall,
                                                                 _etc=etc)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_todays_calculated_moisture_balance_zone5(self):
        """ Verifies the daily moisture balance of zone 5 """
        # etc value to be passed into a method
        etc = calculate_etc(_kc=self.crop_landscape_coefficient_zn5, _eto=self.evapotrans)
        # daily moisture balance
        val_returned = return_todays_calculated_moisture_balance(_yesterdays_mb=self.yest_daily_moisture_balance,
                                                                 _yesterdays_rt=self.yest_runtime,
                                                                 _pr=self.precipitation_rate_zn5,
                                                                 _du=self.application_efficiency_zn5,
                                                                 _ra=self.gross_rainfall,
                                                                 _etc=etc)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_todays_calculated_moisture_balance_zone6(self):
        """ Verifies the daily moisture balance of zone 6 """
        # etc value to be passed into a method
        etc = calculate_etc(_kc=self.crop_landscape_coefficient_zn6, _eto=self.evapotrans)
        # daily moisture balance
        val_returned = return_todays_calculated_moisture_balance(_yesterdays_mb=self.yest_daily_moisture_balance,
                                                                 _yesterdays_rt=self.yest_runtime,
                                                                 _pr=self.precipitation_rate_zn6,
                                                                 _du=self.application_efficiency_zn6,
                                                                 _ra=self.gross_rainfall,
                                                                 _etc=etc)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]

    """
    *******************************************************
    Tests for each zone of return_todays_calculated_runtime()
    *******************************************************
    """

    def test_return_todays_calculated_runtime_zone1(self):
        """ Verifies the total runtime of zone 1 """
        # stores the calculated runtime
        val_returned = return_todays_calculated_runtime(_du=self.application_efficiency_zn1,
                                                        _pr=self.precipitation_rate_zn1,
                                                        _mb=self.yest_daily_moisture_balance)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("return_calculate_total_runtime" + ',')
        self.output.append(str(val_returned) + ',')

    def test_return_todays_calculated_runtime_zone2(self):
        """ Verifies the total runtime of zone 2 """
        # stores the calculated runtime
        val_returned = return_todays_calculated_runtime(_du=self.application_efficiency_zn2,
                                                        _pr=self.precipitation_rate_zn2,
                                                        _mb=self.yest_daily_moisture_balance)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_todays_calculated_runtime_zone3(self):
        """ Verifies the total runtime of zone 3 """
        # stores the calculated runtime
        val_returned = return_todays_calculated_runtime(_du=self.application_efficiency_zn3,
                                                        _pr=self.precipitation_rate_zn3,
                                                        _mb=self.yest_daily_moisture_balance)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_todays_calculated_runtime_zone4(self):
        """ Verifies the total runtime of zone 4 """
        # stores the calculated runtime
        val_returned = return_todays_calculated_runtime(_du=self.application_efficiency_zn4,
                                                        _pr=self.precipitation_rate_zn4,
                                                        _mb=self.yest_daily_moisture_balance)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_todays_calculated_runtime_zone5(self):
        """ Verifies the total runtime of zone 5 """
        # stores the calculated runtime
        val_returned = return_todays_calculated_runtime(_du=self.application_efficiency_zn5,
                                                        _pr=self.precipitation_rate_zn5,
                                                        _mb=self.yest_daily_moisture_balance)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_return_todays_calculated_runtime_zone6(self):
        """ Verifies the total runtime of zone 6 """
        # stores the calculated runtime
        val_returned = return_todays_calculated_runtime(_du=self.application_efficiency_zn6,
                                                        _pr=self.precipitation_rate_zn6,
                                                        _mb=self.yest_daily_moisture_balance)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]


    """
    ***************************************************
    Tests for each zone of verify_irrigation_adequacy()
    ***************************************************
    """

    def test_verify_irrigation_adequacy_zone1(self):
        """ Verifies the irrigation adequacy of zone 1 """
        # turf/landscape moisture requirements
        etc = calculate_etc(_kc=self.crop_landscape_coefficient_zn1, _eto=self.evapotrans)
        # daily moisture balance
        mb = return_todays_calculated_moisture_balance(_yesterdays_mb=self.yest_daily_moisture_balance,
                                                       _yesterdays_rt=self.yest_runtime,
                                                       _pr=self.precipitation_rate_zn1,
                                                       _du=self.application_efficiency_zn1,
                                                       _ra=self.gross_rainfall,
                                                       _etc=etc)
        # true/false if equal to 100%
        val_returned = verify_irrigation_adequacy(_etc=etc, _mb=mb)
        print val_returned
        # store method name and value in list to be output to csv
        self.output.append("verify_irrigation_adequacy" + ',')
        self.output.append(str(val_returned) + ',')

    def test_verify_irrigation_adequacy_zone2(self):
        """ Verifies the irrigation adequacy of zone 2 """
        # turf/landscape moisture requirements
        etc = calculate_etc(_kc=self.crop_landscape_coefficient_zn2, _eto=self.evapotrans)
        # daily moisture balance
        mb = return_todays_calculated_moisture_balance(_yesterdays_mb=self.yest_daily_moisture_balance,
                                                       _yesterdays_rt=self.yest_runtime,
                                                       _pr=self.precipitation_rate_zn2,
                                                       _du=self.application_efficiency_zn2,
                                                       _ra=self.gross_rainfall,
                                                       _etc=etc)
        # true/false if equal to 100%
        val_returned = verify_irrigation_adequacy(_etc=etc, _mb=mb)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_verify_irrigation_adequacy_zone3(self):
        """ Verifies the irrigation adequacy of zone 3 """
        # turf/landscape moisture requirements
        etc = calculate_etc(_kc=self.crop_landscape_coefficient_zn3, _eto=self.evapotrans)
        # daily moisture balance
        mb = return_todays_calculated_moisture_balance(_yesterdays_mb=self.yest_daily_moisture_balance,
                                                       _yesterdays_rt=self.yest_runtime,
                                                       _pr=self.precipitation_rate_zn3,
                                                       _du=self.application_efficiency_zn3,
                                                       _ra=self.gross_rainfall,
                                                       _etc=etc)
        # true/false if equal to 100%
        val_returned = verify_irrigation_adequacy(_etc=etc, _mb=mb)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_verify_irrigation_adequacy_zone4(self):
        """ Verifies the irrigation adequacy of zone 4 """
        # turf/landscape moisture requirements
        etc = calculate_etc(_kc=self.crop_landscape_coefficient_zn4, _eto=self.evapotrans)
        # daily moisture balance
        mb = return_todays_calculated_moisture_balance(_yesterdays_mb=self.yest_daily_moisture_balance,
                                                       _yesterdays_rt=self.yest_runtime,
                                                       _pr=self.precipitation_rate_zn4,
                                                       _du=self.application_efficiency_zn4,
                                                       _ra=self.gross_rainfall,
                                                       _etc=etc)
        # true/false if equal to 100%
        val_returned = verify_irrigation_adequacy(_etc=etc, _mb=mb)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_verify_irrigation_adequacy_zone5(self):
        """ Verifies the irrigation adequacy of zone 5 """
        # turf/landscape moisture requirements
        etc = calculate_etc(_kc=self.crop_landscape_coefficient_zn5, _eto=self.evapotrans)
        # daily moisture balance
        mb = return_todays_calculated_moisture_balance(_yesterdays_mb=self.yest_daily_moisture_balance,
                                                       _yesterdays_rt=self.yest_runtime,
                                                       _pr=self.precipitation_rate_zn5,
                                                       _du=self.application_efficiency_zn5,
                                                       _ra=self.gross_rainfall,
                                                       _etc=etc)
        # true/false if equal to 100%
        val_returned = verify_irrigation_adequacy(_etc=etc, _mb=mb)
        print val_returned
        # store value in list to be output to csv
        self.output.append(str(val_returned) + ',')

    def test_verify_irrigation_adequacy_zone6(self):
        """ Verifies the irrigation adequacy of zone 6 """
        # turf/landscape moisture requirements
        etc = calculate_etc(_kc=self.crop_landscape_coefficient_zn6, _eto=self.evapotrans)
        # daily moisture balance
        mb = return_todays_calculated_moisture_balance(_yesterdays_mb=self.yest_daily_moisture_balance,
                                                       _yesterdays_rt=self.yest_runtime,
                                                       _pr=self.precipitation_rate_zn6,
                                                       _du=self.application_efficiency_zn6,
                                                       _ra=self.gross_rainfall,
                                                       _etc=etc)
        # true/false if equal to 100%
        val_returned = verify_irrigation_adequacy(_etc=etc, _mb=mb)
        print val_returned
        # store value in list to be output to csv and write the list to csv file, then empty it
        self.output.append(str(val_returned) + ',')
        self.my_writer.writerow(self.output)
        del self.output[:]


if __name__ == '__main__':
    unittest.main()