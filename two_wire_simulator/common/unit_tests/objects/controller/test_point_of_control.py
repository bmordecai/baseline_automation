__author__ = 'Brice "Ajo Grande" Garlick'

import unittest
import mock
import serial
import status_parser

from common.objects.controllers.bl_32 import BaseStation3200
from common.imports.types import ActionCommands,PointofControlCommands
from common.objects.programming.point_of_control import PointOfControl
from common.imports import opcodes


class TestPointOfControl(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.309759

    Master Valve Starting lat & lng:
    latitude = 43.609768
    longitude = -116.310359
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # # Set serial instance to mock serial
        # BaseDevices.ser = self.mock_ser
        # poc.POC.ser = self.mock_ser
        # Mainline.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)
        self.bl_3200.ser = self.mock_ser
        self.bl_3200.zones = dict()
        self.bl_3200.programs = dict()
        self.bl_3200.points_of_control = dict()
        self.bl_3200.moisture_sensors = dict()
        self.bl_3200.master_valves = dict()
        self.bl_3200.flow_meters = dict()
        self.bl_3200.mainlines = dict()
        self.bl_3200.event_switches = dict()
        self.bl_3200.pumps = dict()
        self.bl_3200.pressure_sensors = dict()
        self.bl_3200.controller_type = "32"
        self.bl_3200.vr = "16.0.500"

        # master_valves[3] = MasterValve("MVD0001", 3)
        master_valve = mock.MagicMock()
        master_valve.sn = "MVD0001"
        master_valve.ad = 3
        self.bl_3200.master_valves[3] = master_valve

        # flow_meters[5] = FlowMeter("FMD0001", 5)
        flow_meter = mock.MagicMock()
        flow_meter.sn = "FMD0001"
        flow_meter.ad = 5
        self.bl_3200.flow_meters[5] = flow_meter

        # mainlines32[1] = Mainline(1)
        main_line = mock.MagicMock()
        main_line.ad = 1
        self.bl_3200.mainlines[1] = main_line

        # moisture_sensors[4] = MoistureSensor("MSD0001", 4)
        moisture_sensor = mock.MagicMock()
        moisture_sensor.ad = 4
        moisture_sensor.sn = "MSD0001"
        self.bl_3200.moisture_sensors[4] = moisture_sensor

        # event_switches[2] = EventSwitch("ESD0001", 2)
        event_switch = mock.MagicMock()
        event_switch.ad = 2
        event_switch.sn = "ESD0001"
        self.bl_3200.event_switches[2] = event_switch

        # pump
        pump = mock.MagicMock()
        pump.ad = 3
        pump.sn = "PMD1005"
        self.bl_3200.pumps[3] = pump

        # pressure
        pressure_sensor = mock.MagicMock()
        pressure_sensor.ad = 4
        pressure_sensor.sn = "PS1005"
        self.bl_3200.pressure_sensors[4] = pressure_sensor
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # when these objects are created in this test, they carry over to any test after this one because they are
        # just pointers and not test variables specific to just THIS module.
        del self.bl_3200.master_valves[3]
        del self.bl_3200.flow_meters[5]
        del self.bl_3200.mainlines[1]
        del self.bl_3200.moisture_sensors[4]
        del self.bl_3200.event_switches[2]
        del self.bl_3200.pumps[3]
        del self.bl_3200.pressure_sensors[4]

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_poc_3200_object(self):
        """ Creates a new Point of Control object for testing purposes """
        poc = PointOfControl(_controller=self.bl_3200, _ad=1)

        return poc
        # TODO add a test_programming case for sending a full configuration command to a controller running V16

    #################################
    def test_send_programming_pass1(self):
        """ Send Programming To Controller Pass Case 1: Correct Command Sent """
        self.bl_3200.vr = "12.0.12"
        poc = self.create_test_poc_3200_object()
        expected_command = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14},{15}={16},{17}={18}" \
                           ",{19}={20},{21}={22},{23}={24},{25}={26},{27}={28},{29}={30}".format(
                                ActionCommands.SET,                     # {0}
                                opcodes.point_of_control,               # {1}
                                str(poc.ad),                            # {2}
                                opcodes.description,                    # {3}
                                poc.ds,                                 # {4}
                                opcodes.enabled,                        # {5}
                                poc.en,                                 # {6}
                                opcodes.target_flow,                    # {7}
                                str(poc.fl),                            # {8}
                                opcodes.high_flow_limit,                # {9}
                                str(poc.hf),                            # {10}
                                opcodes.shutdown_on_high_flow,          # {11}
                                poc.hs,                                 # {12}
                                opcodes.unscheduled_flow_limit,         # {13}
                                str(poc.uf),                            # {14}
                                opcodes.shutdown_on_unscheduled,        # {15}
                                poc.us,                                 # {16}
                                opcodes.high_pressure_shutdown,         # {17}
                                poc.he,                                 # {18}
                                opcodes.low_pressure_shutdown,          # {19}
                                poc.le,                                 # {20}
                                opcodes.shutdown_on_over_budget,        # {21}
                                poc.ws,                                 # {22}
                                opcodes.water_rationing_enable,         # {23}
                                poc.wr,                                 # {24}
                                opcodes.moisture_sensor_empty_enable,   # {25}
                                poc.mn,                                 # {26}
                                "SE",                                   # {27}
                                poc.se,                                 # {28}
                                opcodes.switch_empty_enable,            # {29}
                                poc.sn,                                 # {30}
                            )

        poc.send_programming()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_fail1(self):
        """ Send Programming To Controller Fail Case 1: Failed communication with controller """
        self.bl_3200.vr = "12.0.12"
        poc = self.create_test_poc_3200_object()
        expected_command = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14},{15}={16},{17}={18}" \
                           ",{19}={20},{21}={22},{23}={24},{25}={26},{27}={28},{29}={30}".format(
                                ActionCommands.SET,                     # {0}
                                opcodes.point_of_control,               # {1}
                                str(poc.ad),                            # {2}
                                opcodes.description,                    # {3}
                                poc.ds,                                 # {4}
                                opcodes.enabled,                        # {5}
                                poc.en,                                 # {6}
                                opcodes.target_flow,                    # {7}
                                str(poc.fl),                            # {8}
                                opcodes.high_flow_limit,                # {9}
                                str(poc.hf),                            # {10}
                                opcodes.shutdown_on_high_flow,          # {11}
                                poc.hs,                                 # {12}
                                opcodes.unscheduled_flow_limit,         # {13}
                                str(poc.uf),                            # {14}
                                opcodes.shutdown_on_unscheduled,        # {15}
                                poc.us,                                 # {16}
                                opcodes.high_pressure_shutdown,         # {17}
                                poc.he,                                 # {18}
                                opcodes.low_pressure_shutdown,          # {19}
                                poc.le,                                 # {20}
                                opcodes.shutdown_on_over_budget,        # {21}
                                poc.ws,                                 # {22}
                                opcodes.water_rationing_enable,         # {23}
                                poc.wr,                                 # {24}
                                opcodes.moisture_sensor_empty_enable,   # {25}
                                poc.mn,                                 # {26}
                                "SE",                                   # {27}
                                poc.se,                                 # {28}
                                opcodes.switch_empty_enable,            # {29}
                                poc.sn,                                 # {30}
                            )

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set Point of Control {0}'s 'Default Values' to: '{1}' -> {2}".format(
            str(poc.ad),        # {0}
            expected_command,   # {1}
            ""                  # {2}
        )
        with self.assertRaises(Exception) as context:
            poc.send_programming()

        self.assertEqual(first=e_msg, second=context.exception.message)
    # TODO move to base methods unit test for BaseFlow
    # #################################
    # def test_set_priority_pass1(self):
    #     """ Set Priority On POC Pass Case 1: Setting new _pr value = 6.0, check that object
    #     variable _pr is set """
    #     poc = self.create_test_poc_3200_object()
    #
    #     new_value = 6
    #     poc.set_priority(new_value)
    #
    #     # value is set during this method and should equal the value passed into the method
    #     actual_value = poc.pr
    #     self.assertEqual(new_value, actual_value)

    # TODO move to base methods unit test for BaseFlow
    # #################################
    # def test_set_priority_pass2(self):
    #     """ Set Priority On POC Pass Case 2: Command with correct values sent to controller """
    #     poc = self.create_test_poc_3200_object()
    #
    #     new_value = 6
    #     expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.priority, str(new_value))
    #     poc.set_priority(new_value)
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    # TODO move to base methods unit test for BaseFlow
    # #################################
    # def test_set_priority_fail1(self):
    #     """ Set Priority On POC Fail Case 1: Failed communication with controller """
    #     poc = self.create_test_poc_3200_object()
    #     new_value = 6
    #
    #     # A contrived Exception is thrown when communicating with the mock serial port
    #     self.mock_ser.send_and_wait_for_reply.side_effect = Exception
    #
    #     with self.assertRaises(Exception) as context:
    #         poc.set_priority(new_value)
    #     e_msg = "Exception occurred trying to set POC {0}'s 'Priority' to: '{1}' -> ".format(
    #             str(poc.ad), str(new_value))
    #     self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_high_flow_limit_pass1(self):
        """ Set High Flow Limit On POC Pass Case 1: Setting new _hf value = 7.0, new _hs value = False.Check that object
        variable _hf and _hs are set """
        poc = self.create_test_poc_3200_object()

        new_hf_value = 7
        poc.set_high_flow_limit(new_hf_value)

        # value is set during this method and should equal the value passed into the method

        self.assertEqual(new_hf_value, poc.hf)
        self.assertEqual(opcodes.false, poc.hs)

    #################################
    def test_set_high_flow_limit_pass2(self):
        """ Set High Flow Limit On POC Pass Case 2: Setting new _hf value = 7.0, new _hs value = True. Check that object
        variable _hf and _hs are set """
        poc = self.create_test_poc_3200_object()

        new_hf_value = 7
        poc.set_high_flow_limit(new_hf_value, with_shutdown_enabled=True)

        # value is set during this method and should equal the value passed into the method

        self.assertEqual(new_hf_value, poc.hf)
        self.assertEqual(opcodes.true, poc.hs)

    #################################
    def test_set_high_flow_limit_pass3(self):
        """ Set High Flow Limit On POC Pass Case 2: Pass in a value and with_shutdown_enabled= false  """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2},{3}={4}".format(
            opcodes.point_of_connection,
            opcodes.high_flow_limit,
            str(new_value),
            opcodes.shutdown_on_high_flow,
            opcodes.false
        )
        poc.set_high_flow_limit(_limit=new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

        #################################
    def test_set_high_flow_limit_pass4(self):
        """ Set High Flow Limit On POC Pass Case 3: Pass in a value and with_shutdown_enabled= true """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2},{3}={4}".format(
            opcodes.point_of_connection,
            opcodes.high_flow_limit,
            str(new_value),
            opcodes.shutdown_on_high_flow,
            opcodes.true
        )
        poc.set_high_flow_limit(_limit=new_value, with_shutdown_enabled=True)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

        #################################
    def test_set_high_flow_limit_pass5(self):
        """ Set High Flow Limit On POC Pass Case 4: Set default values """
        poc = self.create_test_poc_3200_object()
        expected_command = "SET,{0}=1,{1}={2},{3}={4}".format(
            opcodes.point_of_connection,
            opcodes.high_flow_limit,
            str(0),
            opcodes.shutdown_on_high_flow,
            opcodes.false
        )
        poc.set_high_flow_limit()

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_flow_limit_fail1(self):
        """ Set High Flow Limit On POC Fail Case 1: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set POC {0}'s 'High Flow Limit' to: '{1}' and 'High Flow Shutdown'" \
                "to {2} -> ".format(
                    str(poc.ad),        # {0}
                    str(new_value),     # {1}
                    str(poc.hs)         # {2}
                )
        with self.assertRaises(Exception) as context:
            poc.set_high_flow_limit(_limit=new_value)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_high_flow_limit_fail2(self):
        """ Set High Flow Limit On POC Fail Case 2: Invalid value passed in for with_shutdown_enabled """
        poc = self.create_test_poc_3200_object()
        shutdown_enabled = "invalid value"
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "High Flow Limit' {0} entered for Point of Control {1}. Must be True or False." \
                "You passed in an incorrect type of: {2}.".format(
                    str(shutdown_enabled),  # {0} this is if shutdown will be enabled
                    str(poc.ad),            # {1} point of control address
                    type(shutdown_enabled)  # {2} type of passed in value
                )
        with self.assertRaises(Exception) as context:
            poc.set_high_flow_limit(_limit=new_value, with_shutdown_enabled=shutdown_enabled)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_unscheduled_flow_limit_pass1(self):
        """ Set Unscheduled Flow Limit On POC Pass Case 1: Setting new _hf value = 7.0, check that object
        variable _uf and _hs are set """
        poc = self.create_test_poc_3200_object()

        new_value = 7
        poc.set_unscheduled_flow_limit(_gallons=new_value)

        # value is set during this method and should equal the value passed into the method
        self.assertEqual(new_value, poc.uf)
        self.assertEqual(opcodes.false, poc.us)

        #################################
    def test_set_unscheduled_flow_limit_pass2(self):
        """ Set Unscheduled Flow Limit On POC Pass Case 2: Setting new _hf value = 7.0, check that object
        variable _uf and _hs are set """
        poc = self.create_test_poc_3200_object()

        new_value = 7
        poc.set_unscheduled_flow_limit(new_value, with_shutdown_enabled=True)

        # value is set during this method and should equal the value passed into the method

        self.assertEqual(new_value, poc.uf)
        self.assertEqual(opcodes.true, poc.us)

    #################################
    def test_set_unscheduled_flow_limit_pass3(self):
        """ Set Unscheduled Flow Limit On POC Pass Case 3: Pass in a value and with_shutdown_enabled= false  """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2},{3}={4}".format(
            opcodes.point_of_connection,
            opcodes.unscheduled_flow_limit,
            float(new_value),
            opcodes.shutdown_on_unscheduled,
            opcodes.false
        )
        poc.set_unscheduled_flow_limit(_gallons=new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

        #################################
    def test_set_unscheduled_flow_limit_pass4(self):
        """ Set High Flow Limit On POC Pass Case 4: Pass in a value and with_shutdown_enabled= true """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2},{3}={4}".format(
            opcodes.point_of_connection,
            opcodes.unscheduled_flow_limit,
            float(new_value),
            opcodes.shutdown_on_unscheduled,
            opcodes.true
        )
        poc.set_unscheduled_flow_limit(_gallons=new_value, with_shutdown_enabled=True)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

        #################################
    def test_set_unscheduled_flow_limit_pass5(self):
        """ Set High Flow Limit On POC Pass Case 5: Set default values """
        poc = self.create_test_poc_3200_object()
        expected_command = "SET,{0}=1,{1}={2},{3}={4}".format(
            opcodes.point_of_connection,
            opcodes.unscheduled_flow_limit,
            float(0),
            opcodes.shutdown_on_unscheduled,
            opcodes.false
        )
        poc.set_unscheduled_flow_limit()

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_unscheduled_flow_limit_fail1(self):
        """ Set Unscheduled Flow Limit On POC Fail Case 1: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set POC {0}'s 'Unscheduled Flow Limit' to: '{1}' and " \
                "'With Shutdown Enabled' to {2} -> ".format(
                    str(poc.ad),        # {0}
                    float(new_value),     # {1}
                    str(poc.us)         # {2}
                )
        with self.assertRaises(Exception) as context:
            poc.set_unscheduled_flow_limit(_gallons=new_value)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_unscheduled_flow_limit_fail2(self):
        """ Set Unscheduled Flow Limit On POC Fail Case 2: Invalid value passed in for with_shutdown_enabled """
        poc = self.create_test_poc_3200_object()
        shutdown_enabled = "invalid value"
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Invalid 'shutdown enabled' {0} entered for Point of Control {1}. Must be True or False." \
                "You passed in an incorrect type of: {2}.".format(
                    str(shutdown_enabled),  # {0} this is if shutdown will be enabled
                    str(poc.ad),            # {1} point of control address
                    type(shutdown_enabled)  # {2} type of passed in value
                )
        with self.assertRaises(Exception) as context:
            poc.set_unscheduled_flow_limit(_gallons=new_value, with_shutdown_enabled=shutdown_enabled)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_high_pressure_limit_pass1(self):
        """ Set High Pressure Limit On POC Pass Case 1: Setting new _hf value = 7.0, check that object
        variable _hp and _he are set """
        poc = self.create_test_poc_3200_object()

        new_value = 7
        poc.set_high_pressure_limit(_limit=new_value)

        # value is set during this method and should equal the value passed into the method

        self.assertEqual(new_value, poc.hp)
        self.assertEqual(opcodes.false, poc.he)

    #################################
    def test_set_high_pressure_limit_pass2(self):
        """ Set High Pressure Limit On POC Pass Case 2: Setting new _hf value = 7.0, check that object
        variable _hp and _he are set """
        poc = self.create_test_poc_3200_object()

        new_value = 7
        poc.set_high_pressure_limit(_limit=new_value, with_shutdown_enabled=True)

        # value is set during this method and should equal the value passed into the method

        self.assertEqual(new_value, poc.hp)
        self.assertEqual(opcodes.true, poc.he)

    #################################
    def test_set_high_pressure_limit_pass3(self):
        """ Set High Pressure Limit On POC Pass Case 3: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        new_value = 6.006   # This value should be rounded up to 6.01 in the method
        expected_command = "SET,{0}=1,{1}={2},{3}={4}".format(
            opcodes.point_of_connection,
            opcodes.high_pressure_limit,
            "6.01",  # The method will convert the number passed in into a float with two digits past the decimal
            opcodes.high_pressure_shutdown,
            opcodes.false
        )
        poc.set_high_pressure_limit(_limit=new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

        #################################
    def test_set_high_pressure_limit_pass4(self):
        """ Set High Flow Limit On POC Pass Case 4: Pass in a new limit and with_shutdown_enabled= true """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2},{3}={4}".format(
            opcodes.point_of_connection,
            opcodes.high_pressure_limit,
            "6.00",     # The method will convert the number passed in into a float with two digits past the decimal
            opcodes.high_pressure_shutdown,
            opcodes.true
        )
        poc.set_high_pressure_limit(_limit=new_value, with_shutdown_enabled=True)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

        #################################
    def test_set_high_pressure_limit_pass5(self):
        """ Set High Flow Limit On POC Pass Case 5: Set default values """
        poc = self.create_test_poc_3200_object()
        expected_command = "SET,{0}=1,{1}={2},{3}={4}".format(
            opcodes.point_of_connection,
            opcodes.high_pressure_limit,
            "0.00",  # The method will convert the number passed in into a float with two digits past the decimal
            opcodes.high_pressure_shutdown,
            opcodes.false
        )
        poc.set_high_pressure_limit()

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_pressure_limit_fail1(self):
        """ Set High Pressure Limit On POC Fail Case 1: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set POC {0}'s 'High Pressure Limit' to: '{1}' and " \
                "'With Shutdown Enabled' to {2} -> ".format(
                    str(poc.ad),    # {0}
                    str("6.00"),    # {1}
                    str(poc.he)     # {2}
                )
        with self.assertRaises(Exception) as context:
            poc.set_high_pressure_limit(_limit=new_value)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_high_pressure_limit_fail2(self):
        """ Set High Pressure Limit On POC Fail Case 2: Invalid value passed in for with_shutdown_enabled """
        poc = self.create_test_poc_3200_object()
        shutdown_enabled = "invalid value"
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Invalid 'shutdown enabled' {0} entered for Point of Control {1}. Must be True or False." \
                "You passed in an incorrect type of: {2}.".format(
                    str(shutdown_enabled),  # {0} this is if shutdown will be enabled
                    str(poc.ad),            # {1} point of control address
                    type(shutdown_enabled)  # {2} type of passed in value
                )
        with self.assertRaises(Exception) as context:
            poc.set_high_pressure_limit(_limit=new_value, with_shutdown_enabled=shutdown_enabled)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_low_pressure_limit_pass1(self):
        """ Set Low Pressure Limit On POC Pass Case 1: Setting new _hf value = 7.0, check that object
        variable _lp and _le are set """
        poc = self.create_test_poc_3200_object()

        new_value = 7
        poc.set_low_pressure_limit(_limit=new_value)

        # value is set during this method and should equal the value passed into the method

        self.assertEqual(new_value, poc.lp)
        self.assertEqual(opcodes.false, poc.le)

    #################################
    def test_set_low_pressure_limit_pass2(self):
        """ Set Low Pressure Limit On POC Pass Case 2: Setting new _hf value = 7.0, check that object
        variable _lp and _le are set """
        poc = self.create_test_poc_3200_object()

        new_value = 7
        poc.set_low_pressure_limit(_limit=new_value, with_shutdown_enabled=True)

        # value is set during this method and should equal the value passed into the method

        self.assertEqual(new_value, poc.lp)
        self.assertEqual(opcodes.true, poc.le)

    #################################
    def test_set_low_pressure_limit_pass3(self):
        """ Set Low Pressure Limit On POC Pass Case 3: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        new_value = 6.003
        expected_command = "SET,{0}=1,{1}={2},{3}={4}".format(
            opcodes.point_of_connection,
            opcodes.low_pressure_limit,
            "6.00",         # {1} method converts the number passed in to a float with two digits past decimal
            opcodes.low_pressure_shutdown,
            opcodes.false
        )
        poc.set_low_pressure_limit(_limit=new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

        #################################
    def test_set_low_pressure_limit_pass4(self):
        """ Set Low Flow Limit On POC Pass Case 4: Pass in a new limit and with_shutdown_enabled= true """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2},{3}={4}".format(
            opcodes.point_of_connection,
            opcodes.low_pressure_limit,
            "6.00",                     # method converts the number passed in to a float with two digits past decimal
            opcodes.low_pressure_shutdown,
            opcodes.true
        )
        poc.set_low_pressure_limit(_limit=new_value, with_shutdown_enabled=True)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

        #################################
    def test_set_low_pressure_limit_pass5(self):
        """ Set Low Flow Limit On POC Pass Case 5: Set default values """
        poc = self.create_test_poc_3200_object()
        expected_command = "SET,{0}=1,{1}={2},{3}={4}".format(
            opcodes.point_of_connection,
            opcodes.low_pressure_limit,
            "0.00",  # The method will convert the number passed in into a float with two digits past the decimal
            opcodes.low_pressure_shutdown,
            opcodes.false
        )
        poc.set_low_pressure_limit()

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_low_pressure_limit_fail1(self):
        """ Set Low Pressure Limit On POC Fail Case 1: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set POC {0}'s 'Low Pressure Limit' to: '{1}' and " \
                "'With Shutdown Enabled' to {2} -> ".format(
                    str(poc.ad),    # {0}
                    "6.00",         # {1} method converts the number passed in to a float with two digits past decimal
                    str(poc.le)     # {2}
                )
        with self.assertRaises(Exception) as context:
            poc.set_low_pressure_limit(_limit=new_value)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_low_pressure_limit_fail2(self):
        """ Set Low Pressure Limit On POC Fail Case 2: Invalid value passed in for with_shutdown_enabled """
        poc = self.create_test_poc_3200_object()
        shutdown_enabled = "invalid value"
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Invalid 'shutdown enabled' {0} entered for Point of Control {1}. Must be True or False." \
                "You passed in an incorrect type of: {2}.".format(
                    str(shutdown_enabled),  # {0} this is if shutdown will be enabled
                    str(poc.ad),            # {1} point of control address
                    type(shutdown_enabled)  # {2} type of passed in value
                )
        with self.assertRaises(Exception) as context:
            poc.set_low_pressure_limit(_limit=new_value, with_shutdown_enabled=shutdown_enabled)

        self.assertEqual(first=e_msg, second=context.exception.message)

    def test_set_monthly_water_budget_pass1(self):
        """ Set Monthly Water Budget On POC Pass Case 1: Setting new _wb value = 6.0, check that object
        variable _wb is set """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        poc.set_monthly_water_budget(_gallons=new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = poc.wb
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_monthly_water_budget_cn_pass2(self):
        """ Set Monthly Water Budget On POC Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.monthly_water_budget,
                                                      str(new_value))
        poc.set_monthly_water_budget(_gallons=new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_monthly_water_budget_fail1(self):
        """ Set Monthly Water Budget On POC Fail Case 1: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_monthly_water_budget(_gallons=new_value)
        e_msg = "Exception occurred trying to set POC {0}'s 'Monthly Water Budget' to: '{1}' -> ".format(
                str(poc.ad), str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_shutdown_on_over_budget_state_pass1(self):
        """ Set Shutdown on Over Budget State On POC Pass Case 1: set attribute to true """
        poc = self.create_test_poc_3200_object()
        poc.set_shutdown_on_over_budget_to_true()
        expected_command = "{0},{1}={2},{3}={4}".format(
                                               ActionCommands.SET,                  # {0}
                                               opcodes.point_of_connection,         # {1}
                                               str(poc.ad),                         # {2}
                                               opcodes.shutdown_on_over_budget,     # {3}
                                               str(opcodes.true))                   # {4}

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_shutdown_on_over_budget_state_pass2(self):
        """ Set Shutdown on Over Budget State On POC Pass Case 2: set attribute to false """
        poc = self.create_test_poc_3200_object()
        poc.set_shutdown_on_over_budget_to_false()
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                  # {0}
            opcodes.point_of_connection,         # {1}
            str(poc.ad),                         # {2}
            opcodes.shutdown_on_over_budget,     # {3}
            str(opcodes.false))                  # {4}

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_shutdown_on_over_budget_state_fail1(self):
        """ Set Shutdown on Over Budget State On POC Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_shutdown_on_over_budget_to_true()
        e_msg = "Exception occurred trying to set POC {0}'s 'Shutdown on Over Budget' to: '{1}' -> ".format(
            str(poc.ad),
            str(poc.ws)
        )
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_shutdown_on_over_budget_state_fail2(self):
        """ Set Shutdown on Over Budget State On POC Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_shutdown_on_over_budget_to_false()
        e_msg = "Exception occurred trying to set POC {0}'s 'Shutdown on Over Budget' to: '{1}' -> ".format(
            str(poc.ad),
            str(poc.ws)
        )
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_water_rationing_enable_state_pass1(self):
        """ Set Water Rationing Enable State On POC Pass Case 1: set to true"""
        poc = self.create_test_poc_3200_object()
        poc.set_water_rationing_enable_to_true()
        expected_command = "{0},{1}={2},{3}={4}".format(
                        ActionCommands.SET,              # {0}
                        opcodes.point_of_connection,     # {1}
                        str(poc.ad),                     # {2}
                        opcodes.water_rationing_enable,  # {3}
                        str(opcodes.true))               # {4}

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_rationing_enable_state_pass2(self):
        """ Set Water Rationing Enable State On POC Pass Case 1: set to true"""
        poc = self.create_test_poc_3200_object()
        poc.set_water_rationing_enable_to_false()
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,              # {0}
            opcodes.point_of_connection,     # {1}
            str(poc.ad),                     # {2}
            opcodes.water_rationing_enable,  # {3}
            str(opcodes.false))              # {4}

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_rationing_enable_state_pass3(self):
        """ Set Monthly Water Budget On POC Pass Case 1: Setting new _wr value = false, check that object
        variable _wr is set """
        poc = self.create_test_poc_3200_object()
        poc.set_water_rationing_enable_to_false()

        new_value = opcodes.false

        # value is set during this method and should equal the value passed into the method
        actual_value = poc.wr
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_water_rationing_enable_state_pass4(self):
        """ Set Monthly Water Budget On POC Pass Case 1: Setting new _wr value = true, check that object
        variable _wr is set """
        poc = self.create_test_poc_3200_object()
        poc.set_water_rationing_enable_to_true()

        new_value = opcodes.true

        # value is set during this method and should equal the value passed into the method
        actual_value = poc.wr
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_water_rationing_enable_state_fail1(self):
        """ Set Water Rationing Enable State On POC Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set POC {0}'s 'Water Rationing Enable' to: '{1}' -> {2}".format(
            str(poc.ad),            # {0}
            str(opcodes.false),     # {1}
            ""                      # {2}
        )
        with self.assertRaises(Exception) as context:
            poc.set_water_rationing_enable_to_false()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_water_rationing_enable_state_fail2(self):
        """ Set Water Rationing Enable State On POC Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set POC {0}'s 'Water Rationing Enable' to: '{1}' -> {2}".format(
            str(poc.ad),            # {0}
            str(opcodes.true),      # {1}
            ""                      # {2}
        )
        with self.assertRaises(Exception) as context:
            poc.set_water_rationing_enable_to_true()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_add_mainline_to_point_of_control_pass1(self):
        """ Set Mainline On POC Pass Case 1: Passed in address of a present Mainline """
        poc = self.create_test_poc_3200_object()

        expected_value = 1
        poc.add_mainline_to_point_of_control(_mainline_address=1)

        this_obj = poc.controller.mainlines[1]
        actual_value = this_obj.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_add_mainline_to_point_of_control_pass2(self):
        """ Set Mainline On POC Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        this_obj = poc.controller.mainlines[1]
        ad_value = this_obj.ad
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                 # {0}
            opcodes.point_of_control,           # {1}
            str(poc.ad),                        # {2}
            opcodes.mainline,                   # {3}
            str(this_obj.ad)                    # {4}
        )
        poc.add_mainline_to_point_of_control(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_add_mainline_to_point_of_control_fail1(self):
        """ Set Mainline On POC Fail Case 1: Pass an address of Mainline that is below available addresses """
        poc = self.create_test_poc_3200_object()

        new_ad_value = 0
        with self.assertRaises(Exception) as context:
            poc.add_mainline_to_point_of_control(new_ad_value)
        expected_message = "Invalid 'Mainline' address entered for POC {0}. Available mainlines are 1 - 8, " \
                           "user entered: {1}".format(str(poc.ad), str(new_ad_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_add_mainline_to_point_of_control_fail2(self):
        """ Set Mainline On POC Fail Case 2: Pass an address of Mainline that is above available addresses """
        poc = self.create_test_poc_3200_object()

        new_ad_value = 9
        with self.assertRaises(Exception) as context:
            poc.add_mainline_to_point_of_control(new_ad_value)
        expected_message = "Invalid 'Mainline' address entered for POC {0}. Available mainlines are 1 - 8, " \
                           "user entered: {1}".format(str(poc.ad), str(new_ad_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_add_mainline_to_point_of_control_fail3(self):
        """ Set Mainline On POC Fail Case 3: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        this_obj = poc.controller.mainlines[1]
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.add_mainline_to_point_of_control(ad_value)
        e_msg = "Exception occurred trying to set POC {0}'s 'Mainline' to: '{1}' -> "\
            .format(str(poc.ad),
                    str(ad_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_add_flow_meter_to_point_of_control_pass1(self):
        """ Set Flow Meter On POC Pass Case 1: Passed in address of a present Flow Meter """
        poc = self.create_test_poc_3200_object()

        expected_value = 5
        poc.add_flow_meter_to_point_of_control(_flow_meter_address=expected_value)

        actual_value = poc.fm
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_add_flow_meter_to_point_of_control_pass2(self):
        """ Set Flow Meter On POC Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()
        flow_meter_address = 5
        this_obj = poc.controller.flow_meters[flow_meter_address]
        sn_value = this_obj.sn
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                 # {0}
            opcodes.point_of_control,           # {1}
            str(poc.ad),                        # {2}
            opcodes.flow_meter,                 # {3}
            str(sn_value)                       # {4}
        )
        poc.add_flow_meter_to_point_of_control(_flow_meter_address=flow_meter_address)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_add_flow_meter_to_point_of_control_fail1(self):
        """ Set Flow Meter On POC Fail Case 1: Pass an address of Flow Meter that is not in available addresses """
        poc = self.create_test_poc_3200_object()

        ad_value = 0
        expected_message = "Invalid 'flow meter address' {0} entered for Point of Control {1}" \
            .format(
                str(ad_value),   # {0} this should be the flow meter address
                str(poc.ad)       # {1} point of control address
            )
        with self.assertRaises(Exception) as context:
            poc.add_flow_meter_to_point_of_control(ad_value)

        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_add_flow_meter_to_point_of_control_fail2(self):
        """ Set Flow Meter On POC Fail Case 2: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        fm_ad = 5
        this_obj = poc.controller.flow_meters[fm_ad]
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set flow meter {0} on point of control {1}. Exception: ".format(
            str(fm_ad),   # {0} flow meter address
            str(poc.ad),   # {1}
        )
        with self.assertRaises(Exception) as context:
            poc.add_flow_meter_to_point_of_control(ad_value)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_add_pressure_sensor_to_point_of_control_pass1(self):
        """ Set Pressure Sensor On POC Pass Case 1: Passed in address of a present Pressure Sensor """
        poc = self.create_test_poc_3200_object()

        expected_value = 4
        poc.add_pressure_sensor_to_point_of_control(_pressure_sensor_address=expected_value)

        actual_value = poc.ps
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_add_pressure_sensor_to_point_of_control_pass2(self):
        """ Set Pressure Sensor On POC Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()
        pressure_sensor_address = 4
        this_obj = poc.controller.pressure_sensors[pressure_sensor_address]
        sn_value = this_obj.sn
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                 # {0}
            opcodes.point_of_control,           # {1}
            str(poc.ad),                        # {2}
            "PS",                               # {3}
            str(sn_value)                       # {4}
        )
        poc.add_pressure_sensor_to_point_of_control(_pressure_sensor_address=pressure_sensor_address)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_add_pressure_sensor_to_point_of_control_fail1(self):
        """ Set Pressure Sensor On POC Fail Case 1: Pass an address of Flow Meter that is not in available addresses """
        poc = self.create_test_poc_3200_object()

        ad_value = 0
        expected_message = "Invalid 'pressure sensor address' {0} entered for Point of Control {1}" \
            .format(
                    str(ad_value),   # {0} this should be the flow meter address
                    str(poc.ad)       # {1} point of control address
                )
        with self.assertRaises(Exception) as context:
            poc.add_pressure_sensor_to_point_of_control(_pressure_sensor_address=ad_value)

        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_add_pressure_sensor_to_point_of_control_fail2(self):
        """ Set Pressure Sensor On POC Fail Case 2: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        ps_ad = 4
        this_obj = poc.controller.pressure_sensors[ps_ad]
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set pressure sensor {0} on point of control {1}. Exception: ".format(
            str(ps_ad),   # {0} pressure sensor address
            str(poc.ad),   # {1}
        )
        with self.assertRaises(Exception) as context:
            poc.add_pressure_sensor_to_point_of_control(ad_value)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_add_pump_to_point_of_control_pass1(self):
        """ Set Pump On POC Pass Case 1: Passed in address of a present Pump """
        poc = self.create_test_poc_3200_object()

        expected_value = 3
        poc.add_pump_to_point_of_control(_pump_address=expected_value)

        actual_value = poc.pp
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_add_pump_to_point_of_control_pass2(self):
        """ Set Pump On POC Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()
        pump_address = 3
        this_obj = poc.controller.pumps[pump_address]
        sn_value = this_obj.sn
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                             # {0}
            opcodes.point_of_control,                       # {1}
            str(poc.ad),                                    # {2}
            PointofControlCommands.Attributes.PUMP,        # {3}
            str(sn_value)                       # {4}
        )
        poc.add_pump_to_point_of_control(_pump_address=pump_address)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_add_pump_to_point_of_control_fail1(self):
        """ Set Pump On POC Fail Case 1: Pass an address of Flow Meter that is not in available addresses """
        poc = self.create_test_poc_3200_object()

        ad_value = 0
        expected_message = "Invalid 'pump address' {0} entered for Point of Control {1}" \
            .format(
                    str(ad_value),      # {0} this should be the flow meter address
                    str(poc.ad)         # {1} point of control address
                )
        with self.assertRaises(Exception) as context:
            poc.add_pump_to_point_of_control(_pump_address=ad_value)

        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_add_pump_to_point_of_control_fail2(self):
        """ Set Pump On POC Fail Case 2: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        pp_ad = 3
        this_obj = poc.controller.pumps[pp_ad]
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set pump {0} on point of control {1}. Exception: ".format(
            str(pp_ad),   # {0} pump address
            str(poc.ad),   # {1}
        )
        with self.assertRaises(Exception) as context:
            poc.add_pump_to_point_of_control(ad_value)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_add_master_valve_to_point_of_control_pass1(self):
        """ Set Master Valve On POC Pass Case 1: Passed in address of a present Pump """
        poc = self.create_test_poc_3200_object()

        expected_value = 3
        poc.add_master_valve_to_point_of_control(_master_valve_address=expected_value)

        actual_value = poc.mv
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_add_master_valve_to_point_of_control_pass2(self):
        """ Set Master Valve On POC Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()
        master_valve_address = 3
        this_obj = poc.controller.master_valves[master_valve_address]
        sn_value = this_obj.sn
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                 # {0}
            opcodes.point_of_control,           # {1}
            str(poc.ad),                        # {2}
            opcodes.master_valve,               # {3}
            str(sn_value)                       # {4}
        )
        poc.add_master_valve_to_point_of_control(_master_valve_address=master_valve_address)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_add_master_valve_to_point_of_control_fail1(self):
        """ Set Master Valve On POC Fail Case 1: Pass an address of Master Valve that is not in available addresses """
        poc = self.create_test_poc_3200_object()

        ad_value = 0
        expected_message = "Invalid 'master valve address' {0} entered for Point of Control {1}" \
            .format(
                str(ad_value),      # {0} this should be the flow meter address
                str(poc.ad)         # {1} point of control address
            )
        with self.assertRaises(Exception) as context:
            poc.add_master_valve_to_point_of_control(_master_valve_address=ad_value)

        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_add_master_valve_to_point_of_control_fail2(self):
        """ Set Master Valve On POC Fail Case 2: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        ad_value = 3
        this_obj = poc.controller.master_valves[ad_value]
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set 'Master Valve' {0} on point of control {1}. Exception: ".format(
            str(ad_value),   # {0} master valve address
            str(poc.ad),     # {1}
        )
        with self.assertRaises(Exception) as context:
            poc.add_master_valve_to_point_of_control(_master_valve_address=ad_value)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_add_moisture_sensor_to_point_of_control_pass1(self):
        """ Set Moisture Sensor On POC Pass Case 1: Passed in address of a present Moisture Sensor """
        poc = self.create_test_poc_3200_object()

        expected_value = 4
        poc.add_moisture_sensor_to_point_of_control(expected_value)

        actual_value = poc.ms
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_add_moisture_sensor_to_point_of_control_pass2(self):
        """ Set Moisture Sensor On POC Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        this_obj = poc.controller.moisture_sensors[4]
        sn_value = this_obj.sn
        ad_value = this_obj.ad
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,             # {0}
            opcodes.point_of_control,       # {1}
            str(poc.ad),                    # {2}
            opcodes.moisture_sensor,        # {3}
            str(sn_value)                   # {4}
        )

        poc.add_moisture_sensor_to_point_of_control(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_moisture_sensor_fail1(self):
        """ Set Moisture Sensor On POC Fail Case 1: Pass an address of Moisture Sensor that is not present """
        poc = self.create_test_poc_3200_object()
        new_ad_value = 2
        expected_message = "Invalid 'moisture sensor address' {0} entered for Point of Control {1}".format(
            str(new_ad_value),  # {0} moisture_sensor address
            str(poc.ad)         # {1} point of control address
        )
        with self.assertRaises(Exception) as context:
            poc.add_moisture_sensor_to_point_of_control(new_ad_value)

        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_moisture_sensor_fail2(self):
        """ Set Moisture Sensor On POC Fail Case 2: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        this_obj = poc.controller.moisture_sensors[4]
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set 'Moisture Sensor' {0} on point of control {1}. Exception: " \
            .format(str(ad_value),  # {0}
                    str(poc.ad),    # {1}
                    )
        with self.assertRaises(Exception) as context:
            poc.add_moisture_sensor_to_point_of_control(ad_value)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_moisture_empty_limit_pass1(self):
        """ Set Moisture Empty Limit On POC Pass Case 1: Setting new _me value = 6.0, check that object
        variable _me is set """
        poc = self.create_test_poc_3200_object()

        new_value = 6.0
        poc.set_moisture_empty_limit(_new_percent=new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = poc.me
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_moisture_empty_limit_pass2(self):
        """ Set Moisture Empty Limit On POC Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        new_value = 6.0
        expected_command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                 # {0}
                opcodes.point_of_connection,        # {1}
                str(poc.ad),                        # {2}
                opcodes.moisture_empty_limit,       # {3}
                str(new_value))                     # {4}
        poc.set_moisture_empty_limit(_new_percent=new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_moisture_empty_limit_fail1(self):
        """ Set Moisture Empty Limit On POC Fail Case 1: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set POC {0}'s 'Moisture Empty Limit' to: '{1}' -> ".format(
            str(poc.ad),
            str(new_value))

        with self.assertRaises(Exception) as context:
            poc.set_moisture_empty_limit(_new_percent=new_value)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_moisture_sensor_empty_enable_to_true_pass1(self):
        """ Set Moisture Sensor Empty Enable State On POC Pass Case 1: Set to True """
        poc = self.create_test_poc_3200_object()
        expected_command = "{0},{1}={2},{3}={4}".format(
                            ActionCommands.SET,                      # {0}
                            opcodes.point_of_connection,             # {1}
                            str(poc.ad),                             # {2}
                            opcodes.moisture_sensor_empty_enable,    # {3}
                            str(opcodes.true))                       # {4}

        poc.set_moisture_sensor_empty_enable_to_true()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_moisture_sensor_empty_enable_to_false_pass1(self):
        """ Set Moisture Sensor Empty Enable State On POC Pass Case 2: Set to False """
        poc = self.create_test_poc_3200_object()
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                      # {0}
            opcodes.point_of_connection,             # {1}
            str(poc.ad),                             # {2}
            opcodes.moisture_sensor_empty_enable,    # {3}
            str(opcodes.false))                      # {4}

        poc.set_moisture_sensor_empty_enable_to_false()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_moisture_sensor_empty_enable_to_false_pass2(self):
        """ Set Moisture Sensor Empty Enable State On POC Pass Case 1: Set to False """
        poc = self.create_test_poc_3200_object()

        new_value = opcodes.false
        poc.set_moisture_sensor_empty_enable_to_false()

        # value is set during this method and should equal the value passed into the method
        actual_value = poc.mn
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_moisture_sensor_empty_enable_to_true_pass2(self):
        """ Set Moisture Sensor Empty Enable State On POC Pass Case 1: Set to True """
        poc = self.create_test_poc_3200_object()

        new_value = opcodes.true
        poc.set_moisture_sensor_empty_enable_to_true()

        # value is set during this method and should equal the value passed into the method
        actual_value = poc.mn
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_moisture_sensor_empty_enable_to_false_fail1(self):
        """ Set Moisture Sensor Empty Enable State On POC Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        e_msg = "Exception occurred trying to set POC {0}'s 'Moisture Sensor Empty Enable' to: '{1}' -> ".format(
            str(poc.ad),            # {0}
            str(opcodes.false),     # {1}
        )

        with self.assertRaises(Exception) as context:
            poc.set_moisture_sensor_empty_enable_to_false()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_moisture_sensor_empty_enable_to_true_fail1(self):
        """ Set Moisture Sensor Empty Enable State On POC Fail Case 2: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        e_msg = "Exception occurred trying to set POC {0}'s 'Moisture Sensor Empty Enable' to: '{1}' -> ".format(
            str(poc.ad),        # {0}
            str(opcodes.true),  # {1}
        )

        with self.assertRaises(Exception) as context:
            poc.set_moisture_sensor_empty_enable_to_true()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_empty_wait_time_pass1(self):
        """ Set Empty Wait Time On POC Pass Case 1: Setting new _ew value = 6, check that object
        variable _ew is set """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        poc.set_empty_wait_time(_new_empty_wait_time=new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = poc.ew
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_empty_wait_time_pass2(self):
        """ Set Empty Wait Time On POC Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        new_value = 6
        expected_command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,              # {0}
                                                        opcodes.point_of_connection,     # {1}
                                                        str(poc.ad),                     # {2}
                                                        opcodes.empty_wait_time,         # {3}
                                                        str(new_value))                  # {4}
        poc.set_empty_wait_time(_new_empty_wait_time=new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_empty_wait_time_fail1(self):
        """ Set Empty Wait Time On POC Fail Case 1: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.set_empty_wait_time(new_value)
        e_msg = "Exception occurred trying to set POC {0}'s 'Empty Wait Time' to: '{1}' -> ".format(
                str(poc.ad), str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_add_event_switch_to_point_of_control_pass1(self):
        """ Add Event Switch On POC Pass Case 1: Passed in address of a present Event Switch """
        poc = self.create_test_poc_3200_object()

        expected_value = 2
        poc.add_event_switch_to_point_of_control(expected_value)

        actual_value = poc.sw
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_add_event_switch_to_point_of_control_pass2(self):
        """ Add Event Switch On POC Pass Case 2: Command with correct values sent to controller """
        poc = self.create_test_poc_3200_object()

        this_obj = poc.controller.event_switches[2]
        sn_value = this_obj.sn
        ad_value = this_obj.ad
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            opcodes.point_of_control,   # {1}
            str(poc.ad),                # {2}
            opcodes.event_switch,       # {3}
            str(sn_value)               # {4} this needs to be the serial number
        )

        poc.add_event_switch_to_point_of_control(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_add_event_switch_to_point_of_control_fail1(self):
        """ Add Event Switch Empty On POC Fail Case 1: Pass an address of Event Switch that is not present """
        poc = self.create_test_poc_3200_object()

        new_ad_value = 3
        expected_message = "Invalid 'Event Switch address' {0} entered for Point of Control {1}" \
            .format(
                    str(new_ad_value),     # {0} event switch address
                    str(poc.ad)            # {1} point of control address
            )
        with self.assertRaises(Exception) as context:
            poc.add_event_switch_to_point_of_control(new_ad_value)

        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_add_event_switch_to_point_of_control_fail2(self):
        """ Add Event Switch Empty On POC Fail Case 2: Failed communication with controller """
        poc = self.create_test_poc_3200_object()
        this_obj = poc.controller.event_switches[2]
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            poc.add_event_switch_to_point_of_control(ad_value)
        e_msg = "Exception occurred trying to set 'Event Switch' {0} on point of control {1}. Exception: "\
            .format(str(ad_value),
                    str(poc.ad))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_switch_empty_condition_to_op_pass1(self):
        """ Set Switch Empty Condition On POC Pass Case 1: Set to 'OP' """
        poc = self.create_test_poc_3200_object()
        expected_command = "{0},{1}={2},{3}={4}".format(
                            ActionCommands.SET,                      # {0}
                            opcodes.point_of_connection,             # {1}
                            str(poc.ad),                             # {2}
                            opcodes.switch_empty_condition,          # {3}
                            str(opcodes.open))                       # {4}

        poc.set_switch_empty_condition_to_op()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_switch_empty_condition_to_cl_pass1(self):
        """ Set Switch Empty Condition On POC Pass Case 2: Set to 'CL' """
        poc = self.create_test_poc_3200_object()
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                      # {0}
            opcodes.point_of_connection,             # {1}
            str(poc.ad),                             # {2}
            opcodes.switch_empty_condition,          # {3}
            str(opcodes.closed))                     # {4}

        poc.set_switch_empty_condition_to_cl()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_switch_empty_condition_to_op_pass2(self):
        """ Set Switch Empty Condition On POC to open Pass Case 2: Set to 'OP' """
        poc = self.create_test_poc_3200_object()

        expected_value = opcodes.open
        poc.set_switch_empty_condition_to_op()

        actual_value = poc.se
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_switch_empty_condition_to_cl_pass2(self):
        """ Set Switch Empty Condition On POC to close Pass Case 2: Set to 'CL' """
        poc = self.create_test_poc_3200_object()

        expected_value = opcodes.closed
        poc.set_switch_empty_condition_to_cl()

        actual_value = poc.se
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_switch_empty_condition_to_cl_fail1(self):
        """ Set Switch Empty Condition On POC Fail to close Case 1: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set POC {0}'s 'Switch Empty Condition' to: '{1}' -> ".format(
            str(poc.ad),          # {0}
            str(opcodes.closed),  # {1}
        )
        with self.assertRaises(Exception) as context:
            poc.set_switch_empty_condition_to_cl()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_switch_empty_condition_to_op_fail1(self):
        """ Set Switch Empty Condition On POC to open Fail Case 1: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        e_msg = "Exception occurred trying to set POC {0}'s 'Switch Empty Condition' to: '{1}' -> ".format(
            str(poc.ad),        # {0}
            str(opcodes.open),  # {1}
        )
        with self.assertRaises(Exception) as context:
            poc.set_switch_empty_condition_to_op()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_switch_empty_enable_to_true_pass1(self):
        """ Set Switch Empty Enable State On POC to true Pass Case 1: Set to True """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.switch_empty_enable,
                                                      opcodes.true)
        poc = self.create_test_poc_3200_object()
        poc.set_switch_empty_enable_to_true()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_switch_empty_enable_to_false_pass1(self):
        """ Set Switch Empty Enable State On POC to false Pass Case 1: Set to False """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.switch_empty_enable,
                                                      opcodes.false)
        poc = self.create_test_poc_3200_object()
        poc.set_switch_empty_enable_to_false()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_switch_empty_enable_to_false_fail1(self):
        """ Set Switch Empty Enable State On POC to false Fail Case 1: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        e_msg = "Exception occurred trying to set POC {0}'s 'Switch Empty Enable' to: '{1}' -> ".format(
            str(poc.ad),            # {0}
            str(opcodes.false),     # {1}
        )

        with self.assertRaises(Exception) as context:
            poc.set_switch_empty_enable_to_false()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_switch_empty_enable_to_true_fail1(self):
        """ Set Switch Empty Enable State On POC to true Fail Case 1: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        e_msg = "Exception occurred trying to set POC {0}'s 'Switch Empty Enable' to: '{1}' -> ".format(
            str(poc.ad),            # {0}
            str(opcodes.true),      # {1}
        )

        with self.assertRaises(Exception) as context:
            poc.set_switch_empty_enable_to_true()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_monthly_water_budget_pass1(self):
        """ Verify Monthly Water Budget On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        expected_value = 5
        poc.wb = expected_value
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
                        opcodes.monthly_water_budget,
                        expected_value))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_monthly_water_budget()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_set_share_with_flow_station_to_true_pass1(self):
        """ Set Share With FlowStation State On POC to true Pass Case 1: Set to True """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.share_with_flowstation,
                                                      opcodes.true)
        poc = self.create_test_poc_3200_object()
        poc.set_share_with_flow_station_to_true()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_share_with_flow_station_to_true_fail1(self):
        """ Set Share With FlowStation State On POC to true Fail Case 1: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        e_msg = "Exception occurred trying to set POC {0}'s 'Share With FlowStation' to: '{1}' -> ".format(
            str(poc.ad),            # {0}
            str(opcodes.true),      # {1}
        )

        with self.assertRaises(Exception) as context:
            poc.set_share_with_flow_station_to_true()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_share_with_flow_station_to_false_pass1(self):
        """ Set Share With FlowStation State On POC to false Pass Case 1: Set to False """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.point_of_connection, opcodes.share_with_flowstation,
                                                      opcodes.false)
        poc = self.create_test_poc_3200_object()
        poc.set_share_with_flow_station_to_false()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_share_with_flow_station_to_false_fail1(self):
        """ Set Share With FlowStation State On POC to false Fail Case 1: Failed Communication With Controller """
        poc = self.create_test_poc_3200_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        e_msg = "Exception occurred trying to set POC {0}'s 'Share With FlowStation' to: '{1}' -> ".format(
            str(poc.ad),            # {0}
            str(opcodes.false),     # {1}
        )

        with self.assertRaises(Exception) as context:
            poc.set_share_with_flow_station_to_false()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_monthly_water_budget_fail1(self):
        """ Verify Monthly Water Budget On POC Fail Case 1: Value on controller does not match what is
        stored in poc.wb """
        poc = self.create_test_poc_3200_object()
        controller_value = 5
        poc.wb = 6
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.monthly_water_budget,
            str(controller_value)))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Monthly Water Budget' value. Received: {1}, Expected: {2}"\
            .format(
                str(poc.ad),
                str(controller_value),
                str(poc.wb)
            )
        try:
            poc.verify_monthly_water_budget()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_shutdown_on_over_budget_pass1(self):
        """ Verify Shutdown on Over Budget On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.ws = "FA"
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.shutdown_on_over_budget,
            opcodes.false))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_shutdown_on_over_budget()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_shutdown_on_over_budget_fail1(self):
        """ Verify Shutdown on Over Budget On POC Fail Case 1: Value on controller does not match what is
        stored in poc.ws """
        poc = self.create_test_poc_3200_object()
        poc.ws = opcodes.true
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(opcodes.shutdown_on_over_budget, opcodes.false))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Shutdown On Over Budget State' value. Received: {1}, " \
                           "Expected: {2}"\
            .format(
                str(poc.ad),
                opcodes.false,
                str(poc.ws)
            )
        try:
            poc.verify_shutdown_on_over_budget()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_high_flow_limit_pass1(self):
        """ Verify High Flow Limit On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        controller_value = 5
        poc.hf = controller_value
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.high_flow_limit,
            controller_value))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_high_flow_limit()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_high_flow_limit_fail1(self):
        """ Verify High Flow Limit On POC Fail Case 1: Value on controller does not match what is
        stored in poc.hf """
        poc = self.create_test_poc_3200_object()
        controller_value = 5
        poc.hf = 6
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.high_flow_limit,
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'High Flow Limit' value. Received: {1}, Expected: {2}" \
            .format(
                    str(poc.ad),
                    str(controller_value),
                    str(poc.hf)
                )
        try:
            poc.verify_high_flow_limit()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_shutdown_on_high_flow_pass1(self):
        """ Verify Shutdown On High Flow On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        controller_value = opcodes.true
        poc.hs = controller_value
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.shutdown_on_high_flow,
            controller_value))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_shutdown_on_high_flow()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_shutdown_on_high_flow_fail1(self):
        """ Verify Shutdown On High Flow On POC Fail Case 1: Value on controller does not match what is
        stored in poc.hs """
        poc = self.create_test_poc_3200_object()
        controller_value = opcodes.true
        poc.hs = opcodes.false
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.shutdown_on_high_flow,
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Shutdown On High Flow' value. Received: {1}, Expected: {2}" \
            .format(
                    str(poc.ad),
                    str(controller_value),
                    str(poc.hs)
                )
        try:
            poc.verify_shutdown_on_high_flow()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_unscheduled_flow_limit_pass1(self):
        """ Verify Unscheduled Flow Limit On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        controller_value = 5
        poc.uf = controller_value
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.unscheduled_flow_limit,
            controller_value))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_unscheduled_flow_limit()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_unscheduled_flow_limit_fail1(self):
        """ Verify Unscheduled Flow Limit On POC Fail Case 1: Value on controller does not match what is
        stored in poc.hs """
        poc = self.create_test_poc_3200_object()
        controller_value = 5
        poc.uf = 6
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.unscheduled_flow_limit,
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Unscheduled Flow Limit' value. Received: {1}, Expected: {2}" \
            .format(
                    str(poc.ad),
                    str(controller_value),
                    str(poc.uf)
                )
        try:
            poc.verify_unscheduled_flow_limit()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_shutdown_on_unscheduled_pass1(self):
        """ verify_shutdown_on_unscheduled On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        controller_value = opcodes.true
        poc.us = controller_value
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.unscheduled_flow_shutdown,
            controller_value))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_shutdown_on_unscheduled()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_shutdown_on_unscheduled_fail1(self):
        """ verify_shutdown_on_unscheduled On POC Fail Case 1: Value on controller does not match what is
        stored in poc.hs """
        poc = self.create_test_poc_3200_object()
        controller_value = opcodes.true
        poc.us = opcodes.false
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.unscheduled_flow_shutdown,
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Shutdown on Unscheduled' value. Received: {1}, Expected: {2}" \
            .format(
                    str(poc.ad),
                    str(controller_value),
                    str(poc.us)
                )
        try:
            poc.verify_shutdown_on_unscheduled()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_flow_meter_serial_number_pass1(self):
        """ verify_flow_meter_serial_number On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        controller_value = 5
        flow_meter_sn = poc.controller.flow_meters[controller_value].sn
        poc.fm = controller_value
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.flow_meter,
            flow_meter_sn))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_flow_meter_serial_number()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_flow_meter_serial_number_pass2(self):
        """ verify_flow_meter_serial_number On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.flow_meter,
            ""))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_flow_meter_serial_number()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_flow_meter_serial_number_fail1(self):
        """ verify_flow_meter_serial_number On POC Fail Case 1: Serial number for Flow Meter in controller
         does not match what is currently associated with the flow meter test engine object"""
        poc = self.create_test_poc_3200_object()
        fm_ad = 5
        poc.fm = fm_ad
        this_obj = poc.controller.flow_meters[fm_ad]
        controller_value = "Invalid_SN"
        poc.us = opcodes.false
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.flow_meter,
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Flow Meter Serial Number' value. Received: {1}, Expected: {2}" \
            .format(
                str(poc.ad),
                str(controller_value),
                str(this_obj.sn)
            )
        try:
            poc.verify_flow_meter_serial_number()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_flow_meter_serial_number_fail2(self):
        """ verify_flow_meter_serial_number On POC Fail Case 1: There is no Flow meter in the controller"""
        poc = self.create_test_poc_3200_object()
        fm_ad = 5
        poc.fm = fm_ad
        this_obj = poc.controller.flow_meters[fm_ad]
        controller_value = ""
        poc.us = opcodes.false
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.flow_meter,
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Flow Meter Serial Number' value. Received: {1}, Expected: {2}" \
            .format(
                str(poc.ad),
                None,
                str(this_obj.sn)
            )
        try:
            poc.verify_flow_meter_serial_number()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_master_valve_serial_number_pass1(self):
        """ verify_master_valve_serial_number On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        controller_value = 3
        master_valve_sn = poc.controller.master_valves[controller_value].sn
        poc.mv = controller_value
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.master_valve,
            master_valve_sn))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_master_valve_serial_number()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_master_valve_serial_number_pass2(self):
        """ verify_master_valve_serial_number On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.master_valve,
            ""))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_master_valve_serial_number()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_master_valve_serial_number_fail1(self):
        """ verify_master_valve_serial_number On POC Fail Case 1: Serial number for Master Valve in controller
         does not match what is currently associated with the Master Valve test engine object"""
        poc = self.create_test_poc_3200_object()
        ad_value = 3
        poc.mv = ad_value
        this_obj = poc.controller.master_valves[ad_value]
        controller_value = "Invalid_SN"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.master_valve,
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Master Valve Serial Number' value. Received: {1}, " \
                           "Expected: {2}".format(
                                str(poc.ad),
                                str(controller_value),
                                str(this_obj.sn)
                            )
        try:
            poc.verify_master_valve_serial_number()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_master_valve_serial_number_fail2(self):
        """ verify_master_valve_serial_number On POC Fail Case 1: There is no Master Valve in the controller"""
        poc = self.create_test_poc_3200_object()
        ad_value = 3
        poc.mv = ad_value
        this_obj = poc.controller.master_valves[ad_value]
        controller_value = ""
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.master_valve,
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Master Valve Serial Number' value. Received: {1}, Expected: {2}" \
            .format(
                    str(poc.ad),
                    None,
                    str(this_obj.sn)
                )
        try:
            poc.verify_master_valve_serial_number()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_pump_serial_number_pass1(self):
        """ verify_pump_serial_number On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        controller_value = 3
        pump_sn = poc.controller.pumps[controller_value].sn
        poc.pp = controller_value
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            "PP",
            pump_sn))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_pump_serial_number()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_pump_serial_number_pass2(self):
        """ verify_pump_serial_number On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            "PP",
            ""))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_pump_serial_number()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_pump_serial_number_fail1(self):
        """ verify_pump_serial_number On POC Fail Case 1: Serial number for Pump in controller
         does not match what is currently associated with the Master Valve test engine object"""
        poc = self.create_test_poc_3200_object()
        ad_value = 3
        poc.pp = ad_value
        this_obj = poc.controller.pumps[ad_value]
        controller_value = "Invalid_SN"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            "PP",
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Pump Serial Number' value. Received: {1}, " \
                           "Expected: {2}".format(
                                str(poc.ad),
                                str(controller_value),
                                str(this_obj.sn)
                            )
        try:
            poc.verify_pump_serial_number()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_pump_serial_number_fail2(self):
        """ verify_pump_serial_number On POC Fail Case 1: There is no Master Valve in the controller"""
        poc = self.create_test_poc_3200_object()
        ad_value = 3
        poc.pp = ad_value
        this_obj = poc.controller.pumps[ad_value]
        controller_value = ""
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            "PP",
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Pump Serial Number' value. Received: {1}, Expected: {2}" \
            .format(
                    str(poc.ad),
                    None,
                    str(this_obj.sn)
                )
        try:
            poc.verify_pump_serial_number()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_share_with_flow_station_pass1(self):
        """ Verify Share With Flow Station on Over Budget On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.sh = opcodes.false
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.share_with_flowstation,
            opcodes.false))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_share_with_flow_station()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_share_with_flow_station_fail1(self):
        """ Verify Share With Flow Station On POC Fail Case 1: Value on controller does not match what is
        stored in poc.sh """
        poc = self.create_test_poc_3200_object()
        poc.sh = opcodes.true
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.share_with_flowstation,
            opcodes.false))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Share With Flow Station' value. Received: {1}, Expected: {2}" \
            .format(
                    poc.ad,
                    opcodes.false,
                    str(poc.sh)
                )
        try:
            poc.verify_share_with_flow_station()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_pressure_sensor_serial_number_pass1(self):
        """ verify_pressure_sensor_serial_number On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        controller_value = 4
        pressure_sensor_sn = poc.controller.pressure_sensors[controller_value].sn
        poc.ps = controller_value
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            "PS",
            pressure_sensor_sn))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_pressure_sensor_serial_number()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_pressure_sensor_serial_number_pass2(self):
        """ verify_pump_serial_number On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            "PS",
            ""))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_pressure_sensor_serial_number()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_pressure_sensor_serial_number_fail1(self):
        """ verify_pump_serial_number On POC Fail Case 1: Serial number for Pump in controller
         does not match what is currently associated with the Master Valve test engine object"""
        poc = self.create_test_poc_3200_object()
        ad_value = 4
        poc.ps = ad_value
        this_obj = poc.controller.pressure_sensors[ad_value]
        controller_value = "Invalid_SN"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            "PS",
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Pressure Sensor Serial Number' value. Received: {1}, " \
                           "Expected: {2}".format(
                                str(poc.ad),
                                str(controller_value),
                                str(this_obj.sn)
                            )
        try:
            poc.verify_pressure_sensor_serial_number()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_pressure_sensor_serial_number_fail2(self):
        """ verify_pump_serial_number On POC Fail Case 1: There is no Pressure Sensor in the controller"""
        poc = self.create_test_poc_3200_object()
        ad_value = 4
        poc.ps = ad_value
        this_obj = poc.controller.pressure_sensors[ad_value]
        controller_value = ""
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            "PS",
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Pressure Sensor Serial Number' value. Received: {1}, " \
                           "Expected: {2}".format(
                                str(poc.ad),
                                None,
                                str(this_obj.sn)
                            )
        try:
            poc.verify_pressure_sensor_serial_number()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_water_rationing_enable_state_pass1(self):
        """ Verify Water Rationing Enable on Over Budget On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.wr = opcodes.false
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.water_rationing_enable,
            opcodes.false))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_water_rationing_enable()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_water_rationing_enable_state_fail1(self):
        """ Verify Water Rationing Enable On POC Fail Case 1: Value on controller does not match what is
        stored in poc.wr """
        poc = self.create_test_poc_3200_object()
        poc.wr = opcodes.true
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.water_rationing_enable,
            opcodes.false))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Water Rationing Enable' value. Received: {1}, Expected: {2}"\
            .format(
                poc.ad,
                opcodes.false,
                str(poc.wr)
             )
        try:
            poc.verify_water_rationing_enable()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_high_pressure_shutdown_pass1(self):
        """ Verify High Pressure Shutdown On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.he = opcodes.false
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.high_pressure_shutdown,
            opcodes.false))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_high_pressure_shutdown()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_high_pressure_shutdown_fail1(self):
        """ Verify Water Rationing Enable On POC Fail Case 1: Value on controller does not match what is
        stored in poc.he """
        poc = self.create_test_poc_3200_object()
        poc.he = opcodes.true
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.high_pressure_shutdown,
            opcodes.false))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'High Pressure Shutdown' value. Received: {1}, Expected: {2}" \
            .format(
                    poc.ad,
                    opcodes.false,
                    str(poc.he)
                )
        try:
            poc.verify_high_pressure_shutdown()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_low_pressure_limit_pass1(self):
        """ Verify Low Pressure Limit On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.lp = 5
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.low_pressure_limit,
            str(poc.lp)))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_low_pressure_limit()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_low_pressure_limit_fail1(self):
        """ Verify Low Pressure Limit On POC Fail Case 1: Value on controller does not match what is
        stored in poc.lp """
        poc = self.create_test_poc_3200_object()
        poc.lp = 5.0
        controller_value = 6.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.low_pressure_limit,
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Low Pressure Limit' value. Received: {1}, Expected: {2}" \
            .format(
                    poc.ad,
                    str(controller_value),
                    str(poc.lp)
                )
        try:
            poc.verify_low_pressure_limit()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_high_pressure_limit_pass1(self):
        """ Verify High Pressure Limit On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.hp = 5
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.high_pressure_limit,
            str(poc.hp)))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_high_pressure_limit()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_high_pressure_limit_fail1(self):
        """ Verify High Pressure Limit On POC Fail Case 1: Value on controller does not match what is
        stored in poc.hp """
        poc = self.create_test_poc_3200_object()
        poc.hp = 5.0
        controller_value = 6.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.high_pressure_limit,
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'High Pressure Limit' value. Received: {1}, Expected: {2}" \
            .format(
                    poc.ad,
                    str(controller_value),
                    str(poc.hp)
                )
        try:
            poc.verify_high_pressure_limit()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_low_pressure_shutdown_pass1(self):
        """ Verify Low Pressure Shutdown On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.le = opcodes.false
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.low_pressure_shutdown,
            str(poc.le)))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_low_pressure_shutdown()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_low_pressure_shutdown_fail1(self):
        """ Verify Low Pressure Shutdown On POC Fail Case 1: Value on controller does not match what is
        stored in poc.lp """
        poc = self.create_test_poc_3200_object()
        poc.le = opcodes.true
        controller_value = opcodes.false
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.low_pressure_shutdown,
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Low Pressure Shutdown' value. Received: {1}, Expected: {2}" \
            .format(
                    poc.ad,
                    str(controller_value),
                    str(poc.le)
                )
        try:
            poc.verify_low_pressure_shutdown()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_mainline_on_point_of_control_pass1(self):
        """ Verify Mainline On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        test_pass = False
        poc.ml = 1

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.mainline,
            str(poc.ml)))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_mainline_on_point_of_control()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_mainline_fail1(self):
        """ Verify Mainline On POC Fail Case 1: Value on controller does not match what is
        stored in serial number for the mainline """
        poc = self.create_test_poc_3200_object()
        controller_ml = 2
        poc.ml = 1
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.mainline,
            str(controller_ml)))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Mainline'. Received: {1}, Expected: {2}".format(
            poc.ad,
            str(controller_ml),
            str(poc.ml)
        )
        try:
            poc.verify_mainline_on_point_of_control()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_moisture_sensor_pass1(self):
        """ Verify Moisture Sensor On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.ms = 4
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_sensor,
            poc.controller.moisture_sensors[poc.ms].sn))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_moisture_sensor_serial_number()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_moisture_sensor_fail1(self):
        """ Verify Moisture Sensor On POC Fail Case 1: Value on controller does not match what is
        stored in serial number for the Moisture Sensor """
        poc = self.create_test_poc_3200_object()
        poc.ms = 4
        this_obj = poc.controller.moisture_sensors[poc.ms]
        controller_sn = this_obj.sn
        this_obj.sn = "MSD0005"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_sensor,
            controller_sn))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Moisture Sensor Serial Number' value. Received: {1}," \
                           " Expected: {2}".format(
                                poc.ad,
                                controller_sn,
                                this_obj.sn
                            )
        try:
            poc.verify_moisture_sensor_serial_number()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_moisture_empty_limit_pass1(self):
        """ Verify Moisture Empty Limit On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.me = 5
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_empty_limit,
            str(poc.me)))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_moisture_empty_limit()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_moisture_empty_limit_fail1(self):
        """ Verify Moisture Empty Limit On POC Fail Case 1: Value on controller does not match what is
        stored in poc.me """
        poc = self.create_test_poc_3200_object()
        controller_value = 5.0
        poc.me = 6.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_empty_limit,
            str(controller_value)))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Moisture Empty Limit' value. Received: {1}, Expected: {2}"\
            .format(
                str(poc.ad),
                str(controller_value),
                str(poc.me)
            )
        try:
            poc.verify_moisture_empty_limit()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_moisture_empty_enable_state_pass1(self):
        """ Verify Moisture Sensor Empty Enable On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.mn = opcodes.false
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_sensor_empty_enable,
            opcodes.false))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_moisture_sensor_empty_enable()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_moisture_sensor_empty_enable_fail1(self):
        """ Verify Moisture Empty Enable On POC Fail Case 1: Value on controller does not match what is
        stored in poc.mn """
        poc = self.create_test_poc_3200_object()
        poc.mn = opcodes.true
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_sensor_empty_enable,
            opcodes.false))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Moisture Sensor Empty Enable' value. Received: {1}, Expected:" \
                           " {2}".format(
                            poc.ad,
                            opcodes.false,
                            opcodes.true
                            )
        try:
            poc.verify_moisture_sensor_empty_enable()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_empty_wait_time_pass1(self):
        """ Verify Empty Wait Time On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.ew = 5
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.empty_wait_time,
            str(poc.ew)))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_empty_wait_time()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_empty_wait_time_fail1(self):
        """ Verify Empty Wait Time On POC Fail Case 1: Value on controller does not match what is
        stored in poc.ew """
        poc = self.create_test_poc_3200_object()
        controller_value = 5
        poc.ew = 6
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.empty_wait_time,
            controller_value
            ))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Empty Wait Time' value. Received: {1}, Expected: {2}".format(
            poc.ad,
            str(controller_value),
            str(poc.ew)
        )
        try:
            poc.verify_empty_wait_time()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_event_switch_serial_number_pass1(self):
        """ Verify Event Switch On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.sw = 2
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.event_switch,
            poc.controller.event_switches[poc.sw].sn))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_event_switch_serial_number()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg == expected_message:
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_event_switch_fail1(self):
        """ Verify Event Switch On POC Fail Case 1: Value on controller does not match what is
        stored in serial number for the Event Switch """
        poc = self.create_test_poc_3200_object()
        poc.sw = 2
        this_obj = poc.controller.event_switches[2]
        controller_value = this_obj.sn
        this_obj.sn = "ESD0005"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.event_switch,
            controller_value))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Event Switch Serial Number' value. Received: {1}, " \
                           "Expected: {2}".format(
                                poc.ad,
                                controller_value,
                                this_obj.sn
                            )
        try:
            poc.verify_event_switch_serial_number()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg == expected_message:
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_switch_empty_condition_pass1(self):
        """ Verify Switch Empty Condition on Over Budget On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.se = opcodes.open
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            "SE",
            poc.se))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_switch_empty_condition()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_switch_empty_condition_fail1(self):
        """ Verify Switch Empty Condition On POC Fail Case 1: Value on controller does not match what is
        stored in poc.se """
        poc = self.create_test_poc_3200_object()
        poc.se = opcodes.closed
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            "SE",
            opcodes.open))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Switch Empty Condition' value. Received: {1}, " \
                           "Expected: {2}".format(
                                poc.ad,
                                opcodes.open,
                                poc.se
                            )
        try:
            poc.verify_switch_empty_condition()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_switch_empty_enable_pass1(self):
        """ Verify Switch Empty Enable on Over Budget On POC Pass Case 1: Exception is not raised """
        poc = self.create_test_poc_3200_object()
        poc.sn = opcodes.false
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.switch_empty_enable,
            poc.sn))
        poc.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                poc.verify_switch_empty_enable()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_switch_empty_enable_fail1(self):
        """ Verify Switch Empty Enable On POC Fail Case 1: Value on controller does not match what is
        stored in poc.sn """
        poc = self.create_test_poc_3200_object()
        poc.sn = opcodes.true
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.switch_empty_enable,
            opcodes.false
        ))
        poc.data = mock_data

        expected_message = "Unable to verify POC {0}'s 'Switch Empty Enable' value. Received: {1}, Expected: {2}"\
            .format(
                poc.ad,
                opcodes.false,
                poc.sn
            )
        try:
            poc.verify_switch_empty_enable()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_who_i_am_pass1(self):
        """Tests who i am method for a 3200 controller running V16"""
        poc = self.create_test_poc_3200_object()
        poc.ad = 1
        poc.ds = "Test POC 1"
        poc.en = opcodes.true
        poc.fl = 2
        poc.hf = 5
        poc.hs = opcodes.false
        poc.uf = 7
        poc.us = opcodes.true
        poc.fm = 5
        poc.mv = 3
        poc.pp = 3
        poc.ml = 0
        poc.sh = opcodes.false
        poc.ps = 4
        poc.hp = 150
        poc.he = opcodes.true
        poc.lp = 160
        poc.le = opcodes.false

        mock_data = status_parser.KeyValues("{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13},{14}={15},"
                                            "{16}={17},{18}={19},{20}={21},{22}={23},{24}={25},{26}={27},{28}={29},"
                                            "{30}={31},{32}={33},{34}={35}".format(
                                                opcodes.point_of_control,
                                                str(poc.ad),
                                                opcodes.description,
                                                str(poc.ds),
                                                opcodes.enabled,
                                                str(poc.en),
                                                opcodes.target_flow,
                                                str(poc.fl),
                                                opcodes.high_flow_limit,
                                                str(poc.hf),
                                                opcodes.shutdown_on_high_flow,
                                                str(poc.hs),
                                                opcodes.unscheduled_flow_limit,
                                                str(poc.uf),
                                                opcodes.shutdown_on_unscheduled,
                                                str(poc.us),
                                                opcodes.flow_meter,
                                                "FMD0001",
                                                opcodes.master_valve,
                                                "MVD0001",
                                                "PP",
                                                "PMD1005",
                                                opcodes.mainline,
                                                str(poc.ml),
                                                opcodes.share_with_flowstation,
                                                str(poc.sh),
                                                "PS",
                                                "PS1005",
                                                opcodes.high_pressure_limit,
                                                str(poc.hp),
                                                opcodes.high_pressure_shutdown,
                                                str(poc.he),
                                                opcodes.low_pressure_limit,
                                                str(poc.lp),
                                                opcodes.low_pressure_shutdown,
                                                str(poc.le)
                                            )

                                            )

        poc.data = mock_data
        poc.get_data = mock.MagicMock(sideeffect=None)
        poc.verify_who_i_am()

    #################################
    def test_who_i_am_pass2(self):
        """Tests who i am method for a 3200 controller running V12"""

        poc = self.create_test_poc_3200_object()
        poc.controller.vr = "12.0.023"
        poc.ad = 1
        poc.ds = "Test POC 1"
        poc.en = opcodes.true
        poc.fl = 2
        poc.hf = 5
        poc.hs = opcodes.false
        poc.uf = 7
        poc.us = opcodes.true
        poc.fm = 5
        poc.mv = 3
        poc.pp = 3
        poc.ml = 0
        poc.sh = opcodes.false
        poc.ps = 4
        poc.hp = 150
        poc.he = opcodes.true
        poc.lp = 160
        poc.le = opcodes.false
        poc.pr = 2
        poc.wb = 50
        poc.ws = opcodes.true
        poc.wr = opcodes.true
        poc.ms = 4
        poc.me = 60
        poc.mn = opcodes.false
        poc.ew = 40
        poc.sw = 2
        poc.se = opcodes.closed
        poc.sn = opcodes.true

        mock_data = status_parser.KeyValues("{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13},{14}={15},"
                                            "{16}={17},{18}={19},{20}={21},{22}={23},{24}={25},{26}={27},{28}={29},"
                                            "{30}={31},{32}={33},{34}={35},{36}={37},{38}={39},{40}={41},{42}={43},"
                                            "{44}={45},{46}={47},{48}={49},{50}={51},{52}={53},{54}={55},{56}={57}"
                                            .format(
                                                opcodes.point_of_control,
                                                str(poc.ad),
                                                opcodes.description,
                                                str(poc.ds),
                                                opcodes.enabled,
                                                str(poc.en),
                                                opcodes.target_flow,
                                                str(poc.fl),
                                                opcodes.high_flow_limit,
                                                str(poc.hf),
                                                opcodes.shutdown_on_high_flow,
                                                str(poc.hs),
                                                opcodes.unscheduled_flow_limit,
                                                str(poc.uf),
                                                opcodes.shutdown_on_unscheduled,
                                                str(poc.us),
                                                opcodes.flow_meter,
                                                "FMD0001",
                                                opcodes.master_valve,
                                                "MVD0001",
                                                "PP",
                                                "PMD1005",
                                                opcodes.mainline,
                                                str(poc.ml),
                                                opcodes.share_with_flowstation,
                                                str(poc.sh),
                                                "PS",
                                                "PS1005",
                                                opcodes.high_pressure_limit,
                                                str(poc.hp),
                                                opcodes.high_pressure_shutdown,
                                                str(poc.he),
                                                opcodes.low_pressure_limit,
                                                str(poc.lp),
                                                opcodes.low_pressure_shutdown,
                                                str(poc.le),
                                                opcodes.priority,
                                                str(poc.pr),
                                                opcodes.monthly_water_budget,
                                                str(poc.wb),
                                                opcodes.shutdown_on_over_budget,
                                                str(poc.ws),
                                                opcodes.water_rationing_enable,
                                                str(poc.wr),
                                                opcodes.moisture_sensor,
                                                "MSD0001",
                                                opcodes.moisture_empty_limit,
                                                str(poc.me),
                                                opcodes.moisture_sensor_empty_enable,
                                                str(poc.mn),
                                                opcodes.empty_wait_time,
                                                str(poc.ew),
                                                opcodes.event_switch,
                                                "ESD0001",
                                                "SE",
                                                str(poc.se),
                                                opcodes.switch_empty_enable,
                                                str(poc.sn)
                                            ))
        poc.data = mock_data
        poc.get_data = mock.MagicMock(sideeffect=None)
        poc.verify_who_i_am()

    if __name__ == "__main__":
        unittest.main()