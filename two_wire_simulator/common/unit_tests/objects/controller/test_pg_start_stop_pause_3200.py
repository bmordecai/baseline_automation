__author__ = 'Eldinner Plate'

import unittest
import mock
import serial

from common.imports import opcodes, types

from common.objects.base_classes.pg_start_stop_pause_cond import BaseStartStopPause

from common.objects.programming.pg_start_stop_pause_3200 import StartConditionFor3200, StopConditionFor3200, PauseConditionFor3200


class TestStartStopPause3200(unittest.TestCase):
    """
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        BaseStartStopPause.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_start_condition_for_3200_object(self):
        """ Creates a new Start condition object for testing purposes """
        ssp = StartConditionFor3200(program_ad=1)
        return ssp

    #################################
    def create_test_stop_condition_for_3200_object(self):
        """ Creates a new Pause condition object for testing purposes """
        ssp = StopConditionFor3200(program_ad=1)
        return ssp

    #################################
    def create_test_pause_condition_for_3200_object(self):
        """ Creates a new Pause condition object for testing purposes """
        ssp = PauseConditionFor3200(program_ad=1)
        return ssp

    def test_initializing_objects(self):
        """ Test Initializing Objects: Create instances of the three different objects """
        start = self.create_test_start_condition_for_3200_object()
        stop = self.create_test_stop_condition_for_3200_object()
        pause = self.create_test_pause_condition_for_3200_object()

    def test_set_as_et_event_pass_1(self):
        """ Test Set As Evapotranspiration Event Pass Case 1: Set the appropriate variables and verify they are set
        Create test object
        Assign values to the appropriate variables
        Call the method
        Assert that the two variables have been changed
        """
        start = self.create_test_start_condition_for_3200_object()
        threshold = .08
        mode = opcodes.true

        start.set_as_et_event(mode=mode, threshold=threshold)

        self.assertEqual(start._em, mode)
        self.assertEqual(start._et, threshold)

    def test_set_as_et_event_fail_1(self):
        """ Test Set As Evapotranspiration Event Fail Case 1: Pass in an invalid 'mode' and expect a ValueError
        Create test object
        Create a variable that will cause an error when passed into the method
        Create an expected message that will be compared to the error that will be thrown
        Call the method while asserting that it will throw a ValueError
        Compare our expected message with the actual ValueError message
        """
        start = self.create_test_start_condition_for_3200_object()
        mode = 'invalid'

        expected_msg = "Invalid Deficit trigger threshold mode for 3200 start condition #{cond_num}: {mode}. Valid " \
                       "options are: 'TR' | 'FA'".format(
                           cond_num=start._program_ad,
                           mode=mode
                       )

        with self.assertRaises(ValueError) as context:
            start.set_as_et_event(mode=mode)

        self.assertEqual(expected_msg, context.exception.message)


