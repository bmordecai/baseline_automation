import unittest
import mock
import serial
import status_parser

# from common.objects.base_classes.ser import Ser
from common.imports import opcodes
from common.objects.base_classes.devices import BaseDevices
# you have to set the lat and long after the devices class is called so that they don't get skipped over
BaseDevices.controller_lat = float(43.609768)
BaseDevices.controller_long = float(-116.310569)

from common.objects.devices.fm import FlowMeter
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.bicoders.flow_bicoder import FlowBicoder

__author__ = 'Brice "Ajo Grande" Garlick'


class TestFlowMeterObject(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.309759

    Flow Meter Starting lat & lng:
    latitude = 43.609768
    longitude = -116.309354
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the moisture sensor
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)

        # Mock a flow meter to pass into the temp sensor
        self.flow_bicoder = mock.MagicMock(spec=FlowBicoder)


        # Set serial instance to mock serial
        self.bl_3200.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_flow_meter_object(self, _diffserial="TSD0001", _diffaddress=1):
        """ Creates a new Flow Meter object for testing purposes """
        self.flow_bicoder.sn = _diffserial
        self.flow_bicoder.ad = _diffaddress
        self.flow_bicoder.kv = 2.0
        fm = FlowMeter(_controller=self.bl_3200, _flow_bicoder=self.flow_bicoder, _address=_diffaddress)

        return fm

    #################################
    def test_set_default_values_pass1(self):
        """ Set Default Values On Flow Meter Pass Case 1: Correct Command Sent """
        expected_command = "SET," \
                           "FM=TSD0001," \
                           "EN=TR," \
                           "DS=Test Flow Meter TSD0001," \
                           "LA=43.609773," \
                           "LG=-116.310164," \
                           "KV=2.0"
        fm = self.create_test_flow_meter_object()

        fm.set_default_values()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values On Flow Meter Fail Case 1: Failed communication with controller """
        expected_command = "SET," \
                           "FM=TSD0001," \
                           "EN=TR," \
                           "DS=Test Flow Meter TSD0001," \
                           "LA=43.609773," \
                           "LG=-116.310164," \
                           "KV=2.0"
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            fm = self.create_test_flow_meter_object()
            fm.set_default_values()
        e_msg = "Exception occurred trying to set Flow Meter TSD0001 (1)'s 'Default values' to: '{0}'".format(
                expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)
    # TODO move to base methods unit tests
    # #################################
    # def test_set_enable_state_pass1(self):
    #     """ Set Enable State On Controller Pass Case 1: Using Default Value """
    #     expected_command = "SET,FM=TSD0001,EN=TR"
    #     fm = self.create_test_flow_meter_object()
    #     fm.set_enable_state()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO move to base methods unit tests
    # #################################
    # def test_set_enable_state_pass2(self):
    #     """ Set Enable State On Controller Pass Case 2: Using Passed In Argument """
    #     expected_command = "SET,FM=TSD0001,EN=FA"
    #     fm = self.create_test_flow_meter_object()
    #     fm.set_enable_state(_state="FA")
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO move to base methods unit tests
    # #################################
    # def test_set_enable_state_pass3(self):
    #     """ Set Enable State On Controller Pass Case 3: Command with correct values sent to controller """
    #     fm = self.create_test_flow_meter_object()
    #
    #     en_value = str(fm.en)
    #     expected_command = "SET,FM=TSD0001,EN=" + en_value
    #     fm.set_enable_state()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO move to base methods unit tests
    #################################
    # def test_set_enable_state_fail1(self):
    #     """ Set Enable State On Controller Fail Case 1: Invalid State Argument """
    #     fm = self.create_test_flow_meter_object()
    #     with self.assertRaises(ValueError) as context:
    #         fm.set_enable_state(_state="Foo")
    #     e_msg = "Invalid enabled state entered for Flow Meter TSD0001 (1). Received: Foo, Expected: 'TR' or 'FA'"
    #     self.assertEqual(first=e_msg, second=context.exception.message)
    # TODO move to base methods unit tests
    #################################
    # def test_set_enable_state_fail2(self):
    #     """ Set Enable State On Controller Fail Case 2: Failed Communication With Controller """
    #     fm = self.create_test_flow_meter_object()
    #
    #     # Set the send_and_wait_for_reply method to raise an 'Exception' after zone instance is created to avoid
    #     # raising an exception trying to set default values.
    #     self.mock_ser.send_and_wait_for_reply.side_effect = Exception
    #
    #     with self.assertRaises(Exception) as context:
    #         fm.set_enable_state(_state="FA")
    #     e_msg = "Exception occurred trying to enable flow meter TSD0001"
    #     self.assertEqual(first=e_msg, second=context.exception.message)


    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_flow_rate())
    # #################################
    # def test_set_flow_rate_pass1(self):
    #     """ Set Flow Rate On Controller Pass Case 1: Using Default _vr value """
    #     fm = self.create_test_flow_meter_object()
    #
    #     #Expected value is the _df value set at object Zone object creation
    #     expected_value = fm.vr
    #     fm.set_flow_rate()
    #
    #     #_df value is set during this method and should equal the original value
    #     actual_value = fm.vr
    #     self.assertEqual(first=expected_value, second=actual_value)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_flow_rate())
    #################################
    # def test_set_flow_rate_pass2(self):
    #     """ Set Flow Rate On Controller Pass Case 2: Setting new _vr value = 6.0 """
    #     fm = self.create_test_flow_meter_object()
    #
    #     #Expected _df value is 6
    #     expected_value = 6.0
    #     fm.set_flow_rate(expected_value)
    #
    #     #_df value is set during this method and should equal the value passed into the method
    #     actual_value = fm.vr
    #     self.assertEqual(expected_value, actual_value)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_flow_rate())
    # #################################
    # def test_set_flow_rate_pass3(self):
    #     """ Set Flow Rate On Controller Pass Case 3: Command with correct values sent to controller """
    #     fm = self.create_test_flow_meter_object()
    #
    #     vr_value = "0.0"
    #     expected_command = "SET,FM=TSD0001,VR=" + vr_value
    #     fm.set_flow_rate()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_flow_rate())
    # #################################
    # def test_set_flow_rate_fail1(self):
    #     """ Set Flow Rate On Controller Fail Case 1: Pass String value as argument """
    #     fm = self.create_test_flow_meter_object()
    #
    #     new_vr_value = "b"
    #     with self.assertRaises(Exception) as context:
    #         fm.set_flow_rate(new_vr_value)
    #     expected_message = "Failed trying to set FM {0} ({1})'s Flow Rate, invalid argument type, expected a int " \
    #                        "or float, received: {2}".format(fm.sn, str(fm.ad), type(new_vr_value))
    #     self.assertEqual(expected_message, context.exception.message)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_flow_rate())
    # #################################
    # def test_set_flow_rate_fail2(self):
    #     """ Set Flow Rate On Controller Fail Case 2: Failed communication with controller """
    #     fm = self.create_test_flow_meter_object()
    #
    #     #A contrived Exception is thrown when communicating with the mock serial port
    #     self.mock_ser.send_and_wait_for_reply.side_effect = Exception
    #
    #     with self.assertRaises(Exception) as context:
    #         fm.set_flow_rate()
    #     e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'Flow Rate' to: '({2})'".format(
    #             fm.sn, str(fm.ad), str(fm.vr))
    #     self.assertEqual(first=e_msg, second=context.exception.message)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_water_usage()
    # #################################
    # def test_set_water_usage_pass1(self):
    #     """ Set Water Usage On Controller Pass Case 1: Using Default _vg value """
    #     fm = self.create_test_flow_meter_object()
    #
    #     #Expected value is the _df value set at object Zone object creation
    #     expected_value = fm.vg
    #     fm.set_water_usage()
    #
    #     #_df value is set during this method and should equal the original value
    #     actual_value = fm.vg
    #     self.assertEqual(first=expected_value, second=actual_value)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_water_usage()
    # #################################
    # def test_set_water_usage_pass2(self):
    #     """ Set Water Usage On Controller Pass Case 2: Setting new _vg value = 6.0 """
    #     fm = self.create_test_flow_meter_object()
    #
    #     #Expected _df value is 6
    #     expected_value = 6.0
    #     fm.set_water_usage(expected_value)
    #
    #     #_df value is set during this method and should equal the value passed into the method
    #     actual_value = fm.vg
    #     self.assertEqual(expected_value, actual_value)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_water_usage()
    # #################################
    # def test_set_water_usage_pass3(self):
    #     """ Set Water Usage On Controller Pass Case 3: Command with correct values sent to controller """
    #     fm = self.create_test_flow_meter_object()
    #
    #     vg_value = str(fm.vg)
    #     expected_command = "SET,FM=TSD0001,VG=" + vg_value
    #     fm.set_water_usage()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_water_usage()
    #################################
    # def test_set_water_usage_fail1(self):
    #     """ Set Water Usage On Controller Fail Case 1: Pass String value as argument """
    #     fm = self.create_test_flow_meter_object()
    #
    #     new_vg_value = "b"
    #     with self.assertRaises(Exception) as context:
    #         fm.set_water_usage(new_vg_value)
    #     expected_message = "Failed trying to set FM {0} ({1})'s Water Usage, invalid argument type, expected a int " \
    #                        "or float, received: {2}".format(fm.sn, str(fm.ad), type(new_vg_value))
    #     self.assertEqual(expected_message, context.exception.message)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_water_usage()
    # #################################
    # def test_set_water_usage_fail2(self):
    #     """ Set Water Usage On Controller Fail Case 2: Failed communication with controller """
    #     fm = self.create_test_flow_meter_object()
    #     new_vg_value = 6.0
    #
    #     #A contrived Exception is thrown when communicating with the mock serial port
    #     self.mock_ser.send_and_wait_for_reply.side_effect = Exception
    #
    #     with self.assertRaises(Exception) as context:
    #         fm.set_water_usage(new_vg_value)
    #     e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'water usage' to: '{2}'".format(
    #             fm.sn, str(fm.ad), str(new_vg_value))
    #     self.assertEqual(first=e_msg, second=context.exception.message)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_two_wire_drop_value()
    #################################
    # def test_set_two_wire_drop_value_pass1(self):
    #     """ Set Two Wire Drop Value On Controller Pass Case 1: Using Default _vt value """
    #     fm = self.create_test_flow_meter_object()
    #
    #     #Expected value is the _va value set at object Zone object creation
    #     expected_value = fm.vt
    #     fm.set_two_wire_drop_value()
    #
    #     #_va value is set during this method and should equal the original value
    #     actual_value = fm.vt
    #     self.assertEqual(first=expected_value, second=actual_value)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_two_wire_drop_value()
    #################################
    # def test_set_two_wire_drop_value_pass2(self):
    #     """ Set Two Wire Drop Value On Controller Pass Case 2: Using 6 as passed in value for _vt """
    #     fm = self.create_test_flow_meter_object()
    #
    #     #Expected value is the _va value set at object Zone object creation
    #     expected_value = 6
    #     fm.set_two_wire_drop_value(expected_value)
    #
    #     #_va value is set during this method and should equal the original value
    #     actual_value = fm.vt
    #     self.assertEqual(first=expected_value, second=actual_value)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_pass3(self):
    #     """ Set Two Wire Drop Value Controller Pass Case 3: Command with correct values sent to controller """
    #     fm = self.create_test_flow_meter_object()
    #
    #     vt_value = str(fm.vt)
    #     expected_command = "SET,FM=TSD0001,VT=" + vt_value
    #     fm.set_two_wire_drop_value()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_two_wire_drop_value()
    #################################
    # def test_set_two_wire_drop_value_fail1(self):
    #     """ Set Two Wire Drop Value On Controller Fail Case 1: Pass String value as argument """
    #     fm = self.create_test_flow_meter_object()
    #
    #     new_vt_value = "b"
    #     with self.assertRaises(Exception) as context:
    #         fm.set_two_wire_drop_value(new_vt_value)
    #     expected_message = "Failed trying to set FM {0} ({1})'s Two Wire Drop, invalid argument type, expected a " \
    #                        "int or float, received: {2}".format(fm.sn, str(fm.ad), type(new_vt_value))
    #     self.assertEqual(expected_message, context.exception.message)
    # TODO need to add a wrapper method in fm.py to wrap bicoder.set_two_wire_drop_value()
    #################################
    # def test_set_two_wire_drop_value_flow_fail2(self):
    #     """ Set Two Wire Drop Value On Controller Fail Case 2: Failed communication with controller """
    #     fm = self.create_test_flow_meter_object()
    #
    #     #A contrived Exception is thrown when communicating with the mock serial port
    #     self.mock_ser.send_and_wait_for_reply.side_effect = Exception
    #
    #     with self.assertRaises(Exception) as context:
    #         fm.set_two_wire_drop_value()
    #     e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'Two Wire Drop Value' to: '{2}'".format(
    #             fm.sn, str(fm.ad), str(fm.vt))
    #     self.assertEqual(first=e_msg, second=context.exception.message)

    # #################################
    # def test_verify_k_value_pass1(self):
    #     """ Verify K Value On Controller Pass Case 1: Exception is not raised """
    #     fm = self.create_test_flow_meter_object()
    #     fm.kv = 5.0
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.k_value))
    #     fm.data = mock_data
    #
    #     try:
    #         #.assertRaises raises an Assertion Error if an Exception is not raised in the method
    #         with self.assertRaises(Exception):
    #             fm.verify_k_value()
    #
    #     # Catches an Assertion Error from above, meaning the method did NOT raise an exception
    #     # meaning verification passed
    #     except AssertionError as ae:
    #         e_msg = ae.message
    #         expected_message = "Exception not raised"
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
    #
    # #################################
    # def test_verify_k_value_fail1(self):
    #     """ Verify K Value On Controller Fail Case 1: Value on controller does not match what is
    #     stored in fm.kv """
    #     fm = self.create_test_flow_meter_object()
    #     fm.kv = 5.0
    #     test_pass = False
    #     e_msg = ""
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.k_value))
    #     fm.data = mock_data
    #
    #     expected_message = "Unable verify Flow Meter TSD0001 (1)'s k_value. Received: 6.0, Expected: 5.0"
    #     try:
    #         fm.verify_k_value()
    #
    #     # Catches an Exception from above, meaning the method did raise an exception
    #     # meaning verification failed
    #     except Exception as e:
    #         e_msg = e.message
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
    #                      .format(expected_message, e_msg))
    # TODO need to add a wrapper method in fm.py to wrap bicoder.verify_flow_rate()
    # #################################
    # def test_verify_flow_rate_pass1(self):
    #     """ Verify Flow Rate On Controller Pass Case 1: Exception is not raised """
    #     fm = self.create_test_flow_meter_object()
    #     fm.vr = 5
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.flow_rate))
    #     fm.data = mock_data
    #
    #     try:
    #         #.assertRaises raises an Assertion Error if an Exception is not raised in the method
    #         with self.assertRaises(Exception):
    #             fm.verify_flow_rate()
    #
    #     # Catches an Assertion Error from above, meaning the method did NOT raise an exception
    #     # meaning verification passed
    #     except AssertionError as ae:
    #         e_msg = ae.message
    #         expected_message = "Exception not raised"
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
    # TODO need to add a wrapper method in fm.py to wrap bicoder.verify_flow_rate()
    #################################
    # def test_verify_flow_rate_fail1(self):
    #     """ Verify Flow Rate On Controller Fail Case 1: Value on controller does not match what is stored in fm.vr """
    #     fm = self.create_test_flow_meter_object()
    #     fm.vr = 5
    #     test_pass = False
    #     e_msg = ""
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.flow_rate))
    #     fm.data = mock_data
    #
    #     expected_message = "Unable verify Flow Meter TSD0001 (1)'s flow rate. Received: 6.0, Expected: 5"
    #     try:
    #         fm.verify_flow_rate()
    #
    #     # Catches an Exception from above, meaning the method did raise an exception
    #     # meaning verification failed
    #     except Exception as e:
    #         e_msg = e.message
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
    #                      .format(expected_message, e_msg))
    # TODO need to add a wrapper method in fm.py to wrap bicoder.verify_water_usage()
    #################################
    # def test_verify_water_usage_pass1(self):
    #     """ Verify Water Usage On Controller Pass Case 1: Exception is not raised """
    #     fm = self.create_test_flow_meter_object()
    #     fm.vg = 5
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.total_usage))
    #     fm.data = mock_data
    #
    #     try:
    #         #.assertRaises raises an Assertion Error if an Exception is not raised in the method
    #         with self.assertRaises(Exception):
    #             fm.verify_water_usage()
    #
    #     # Catches an Assertion Error from above, meaning the method did NOT raise an exception
    #     # meaning verification passed
    #     except AssertionError as ae:
    #         e_msg = ae.message
    #         expected_message = "Exception not raised"
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
    # TODO need to add a wrapper method in fm.py to wrap bicoder.verify_water_usage()
    # #################################
    # def test_verify_water_usage_fail1(self):
    #     """ Verify Water Usage On Controller Fail Case 1: Value on controller does not match what is
    #     stored in fm.vg """
    #     fm = self.create_test_flow_meter_object()
    #     fm.vg = 5.0
    #     test_pass = False
    #     e_msg = ""
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.total_usage))
    #     fm.data = mock_data
    #
    #     expected_message = "Unable verify Flow Meter TSD0001 (1)'s water usage. Received: 6.0, Expected: 5.0"
    #     try:
    #         fm.verify_water_usage()
    #
    #     # Catches an Exception from above, meaning the method did raise an exception
    #     # meaning verification failed
    #     except Exception as e:
    #         e_msg = e.message
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
    #                      .format(expected_message, e_msg))
    # TODO need to add a wrapper method in fm.py to wrap bicoder.verify_two_wire_drop_value()
    # #################################
    # def test_verify_two_wire_drop_value_pass1(self):
    #     """ Verify Two Wire Drop Value On Controller Pass Case 1: Exception is not raised """
    #     fm = self.create_test_flow_meter_object()
    #     fm.vt = 5
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.two_wire_drop))
    #     fm.data = mock_data
    #
    #     try:
    #         #.assertRaises raises an Assertion Error if an Exception is not raised in the method
    #         with self.assertRaises(Exception):
    #             fm.verify_two_wire_drop_value()
    #
    #     # Catches an Assertion Error from above, meaning the method did NOT raise an exception
    #     # meaning verification passed
    #     except AssertionError as ae:
    #         e_msg = ae.message
    #         expected_message = "Exception not raised"
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
    # TODO need to add a wrapper method in fm.py to wrap bicoder.verify_two_wire_drop_value()
    #################################
    # def test_verify_two_wire_drop_value_fail1(self):
    #     """ Verify Two Wire Drop Value On Controller Fail Case 1: Value on controller does not match what is
    #     stored in fm.vt """
    #     fm = self.create_test_flow_meter_object()
    #     fm.vt = 5.0
    #     test_pass = False
    #     e_msg = ""
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.two_wire_drop))
    #     fm.data = mock_data
    #
    #     expected_message = "Unable verify Flow Meter TSD0001 (1)'s two wire drop value. Received: 6.0, Expected: 5.0"
    #     try:
    #         fm.verify_two_wire_drop_value()
    #
    #     # Catches an Exception from above, meaning the method did raise an exception
    #     # meaning verification failed
    #     except Exception as e:
    #         e_msg = e.message
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
    #                      .format(expected_message, e_msg))
    # TODO move test to base methods
    #################################
    # def test_verify_enable_state_pass1(self):
    #     """ Verify Enable State On Controller Pass Case 1: Exception is not raised """
    #     fm = self.create_test_flow_meter_object()
    #     fm.en = 'TR'
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
    #     fm.data = mock_data
    #
    #     try:
    #         #.assertRaises raises an Assertion Error if an Exception is not raised in the method
    #         with self.assertRaises(Exception):
    #             fm.verify_enable_state()
    #
    #     # Catches an Assertion Error from above, meaning the method did NOT raise an exception
    #     # meaning verification passed
    #     except AssertionError as ae:
    #         e_msg = ae.message
    #         expected_message = "Exception not raised"
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
    # TODO move test to base methods
    #################################
    # def test_verify_enable_state_fail1(self):
    #     """ Verify Enable State On Controller Fail Case 1: Value on controller does not match what is
    #     stored in fm.en """
    #     fm = self.create_test_flow_meter_object()
    #     fm.en = 'Tr'
    #     test_pass = False
    #     e_msg = ""
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
    #     fm.data = mock_data
    #
    #     expected_message = "Unable verify Flow Meter TSD0001 (1)'s enabled state. Received: TR, " \
    #                        "Expected: {0}".format(str(fm.en))
    #     try:
    #         fm.verify_enable_state()
    #
    #     # Catches an Exception from above, meaning the method did raise an exception
    #     # meaning verification failed
    #     except Exception as e:
    #         e_msg = e.message
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
    #                      .format(expected_message, e_msg))

    #################################
    def test_verify_who_i_am_pass1(self):
        """
        verify_who_i_am pass case 1:
        Create: test flow meter object
        Mock: get_data because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_description because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_serial_number because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_latitude because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_longitude because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_status because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_enable_state because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_k_value because it goes outside the method
        Return: None, so it does not fail
        Mock: verify_flow_rate because it goes outside the method
        Return: None, so it does not fail
        Run: Test, passing _expected_status in as something besides None
        Passes: If all the code runs through without any exceptions. This merely tests the flow, not any of the verified
        methods inside of the code.
        """
        # Create test flow meter object
        fm = self.create_test_flow_meter_object()

        # Mock get_data
        mock_get_data = mock.MagicMock(side_effect=None)
        fm.get_data = mock_get_data

        fm.data = mock.MagicMock()

        # Mock verify_description
        mock_verify_description = mock.MagicMock(side_effect=None)
        fm.verify_description = mock_verify_description

        # Mock verify_serial_number
        mock_verify_serial_number = mock.MagicMock(side_effect=None)
        fm.verify_serial_number = mock_verify_serial_number

        # Mock verify_latitude
        mock_verify_latitude = mock.MagicMock(side_effect=None)
        fm.verify_latitude = mock_verify_latitude

        # Mock verify_longitude
        mock_verify_longitude = mock.MagicMock(side_effect=None)
        fm.verify_longitude = mock_verify_longitude

        # Mock verify_status
        mock_verify_status = mock.MagicMock(side_effect=None)
        fm.verify_status = mock_verify_status

        # Mock verify_enable_state
        mock_verify_enable_state = mock.MagicMock(side_effect=None)
        fm.verify_enabled_state = mock_verify_enable_state

        # Mock verify_k_value
        mock_verify_k_value = mock.MagicMock(side_effect=None)
        fm.verify_k_value = mock_verify_k_value

        # Mock verify_flow_rate
        mock_verify_flow_rate = mock.MagicMock(side_effect=None)
        fm.verify_flow_rate = mock_verify_flow_rate

        # Run test
        fm.verify_who_i_am(_expected_status='OK')

    def test_replace_flow_bicoder_pass1(self):
        """ Tests replacing a flow bicoder with an EXISTING bicoder. """
        new_sn_value = "MSD1005"

        fm = self.create_test_flow_meter_object()
        flow_bicoder_2 = mock.MagicMock(spec=FlowBicoder)
        flow_bicoder_2.sn = new_sn_value

        # Setup controller flow bicoder dictionary
        fm.controller.flow_bicoders = dict()
        fm.controller.flow_bicoders[new_sn_value] = flow_bicoder_2

        set_address_mock = fm.set_address = mock.MagicMock()
        flow_bicoder_2.self_test_and_update_object_attributes = mock.MagicMock()

        # run test
        fm.replace_flow_bicoder(_new_serial_number=new_sn_value)

        # verify test
        self.assertEquals(fm.sn, flow_bicoder_2.sn)
        self.assertEquals(fm.identifiers[0][1], flow_bicoder_2.sn)
        self.assertEquals(set_address_mock.call_count, 1)
        self.assertEquals(flow_bicoder_2.self_test_and_update_object_attributes.call_count, 1)

    def test_replace_flow_bicoder_pass2(self):
        """ Tests replacing a flow bicoder with a NEW bicoder. Verifies bicoder is loaded onto controller. """
        new_sn_value = "TSD1005"

        # Create and mock new flow bicoder
        ts = self.create_test_flow_meter_object()
        flow_bicoder_2 = mock.MagicMock(spec=FlowBicoder)
        flow_bicoder_2.sn = new_sn_value
        flow_bicoder_2.self_test_and_update_object_attributes = mock.MagicMock()

        # Setup controller temp bicoder dictionary
        ts.controller.flow_bicoders = dict()

        # mock both controller methods being called
        load_dv_mock = ts.controller.load_dv = mock.MagicMock()
        do_search_for_flow_meters_mock = ts.controller.do_search_for_flow_meters = mock.MagicMock()

        # Verifies that a key error is raised when self.bicoder is set to the new serial number. We are trying to
        # test when the serial number is not in the dictionary, and it won't be in the dictionary when
        # self.bicoder is set to the flow_bicoder of the new serial number.
        with self.assertRaises(KeyError):
            ts.replace_flow_bicoder(_new_serial_number=new_sn_value)

        self.assertEquals(load_dv_mock.call_count, 1)
        self.assertEquals(do_search_for_flow_meters_mock.call_count, 1)

    if __name__ == "__main__":
        unittest.main()
