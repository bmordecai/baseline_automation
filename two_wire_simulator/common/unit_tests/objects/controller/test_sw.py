import unittest
import mock
import serial
import status_parser

from common.imports import opcodes
from common.objects.base_classes.devices import BaseDevices
# you have to set the lat and long after the devices class is called so that they don't get skipped over
BaseDevices.controller_lat = float(43.609768)
BaseDevices.controller_long = float(-116.310569)
from common.objects.devices.sw import EventSwitch
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.bicoders.switch_bicoder import SwitchBicoder
# you have to set the lat and long after the devices class is called so that they don't get skipped over


from common.objects.devices.sw import EventSwitch
__author__ = 'Brice "Ajo Grande" Garlick'


class TestEventSwitchObject(unittest.TestCase):

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the zone.
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)

        # Attach the mocked serial to the controller ser
        self.bl_3200.ser = self.mock_ser

        # Mock a switch bicoder to pass into the zone
        self.switch_bicoder = mock.MagicMock()
        # Set serial instance to mock serial
        BaseDevices.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_event_switch_object(self, _diffserial="TPD0001", _diffaddress=1):
        """ Creates a new Event Switch object for testing purposes """
        self.switch_bicoder.sn = _diffserial
        switch = EventSwitch(_controller=self.bl_3200, _address=_diffaddress, _switch_bicoder=self.switch_bicoder)

        return switch

    #################################
    def test_set_default_values_pass1(self):
        """ Set Default Values On Controller Pass Case 1: Correct Command Sent """
        switch = self.create_test_event_switch_object()
        expected_command = "SET," \
                           "SW=TPD0001," \
                           "EN=TR," \
                           "DS=Test Event Switch TPD0001," \
                           "LA=43.609773," \
                           "LG=-116.309864" \

        switch.set_default_values()
        self.mock_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values On Controller Fail Case 1: Handle Exception Raised From Controller """
        switch = self.create_test_event_switch_object()
        expected_command = "SET," \
                           "SW=TPD0001," \
                           "EN=TR," \
                           "DS=Test Event Switch TPD0001," \
                           "LA=43.609773," \
                           "LG=-116.309864" \

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            switch.set_default_values()
        e_msg = "Exception occurred trying to set Event Switch TPD0001 (1)'s 'Default values' to: '{0}'"\
            .format(expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    def test_replace_event_switch_bicoder_pass1(self):
        """ Tests replacing a switch bicoder with an EXISTING bicoder. """
        new_sn_value = "MSD1005"

        sw = self.create_test_event_switch_object()
        switch_bicoder_2 = mock.MagicMock(spec=SwitchBicoder)
        switch_bicoder_2.sn = new_sn_value

        # Setup controller switch bicoder dictionary
        sw.controller.switch_bicoders = dict()
        sw.controller.switch_bicoders[new_sn_value] = switch_bicoder_2

        set_address_mock = sw.set_address = mock.MagicMock()
        switch_bicoder_2.self_test_and_update_object_attributes = mock.MagicMock()

        # run test
        sw.replace_switch_bicoder(_new_serial_number=new_sn_value)

        # verify test
        self.assertEquals(sw.sn, switch_bicoder_2.sn)
        self.assertEquals(sw.identifiers[0][1], switch_bicoder_2.sn)
        self.assertEquals(set_address_mock.call_count, 1)
        self.assertEquals(switch_bicoder_2.self_test_and_update_object_attributes.call_count, 1)

    def test_replace_event_switch_bicoder_pass2(self):
        """ Tests replacing a switch bicoder with a NEW bicoder. Verifies bicoder is loaded onto controller. """
        new_sn_value = "TSD1005"

        # Create and mock new switch bicoder
        sw = self.create_test_event_switch_object()
        switch_bicoder_2 = mock.MagicMock(spec=SwitchBicoder)
        switch_bicoder_2.sn = new_sn_value
        switch_bicoder_2.self_test_and_update_object_attributes = mock.MagicMock()

        # Setup controller switch bicoder dictionary
        sw.controller.switch_bicoders = dict()

        # mock both controller methods being called
        load_dv_mock = sw.controller.load_dv = mock.MagicMock()
        do_search_for_event_switches_mock = sw.controller.do_search_for_event_switches = mock.MagicMock()

        # Verifies that a key error is raised when self.bicoder is set to the new serial number. We are trying to
        # test when the serial number is not in the dictionary, and it won't be in the dictionary when
        # self.bicoder is set to the switcherature_bicoder of the new serial number.
        with self.assertRaises(KeyError):
            sw.replace_switch_bicoder(_new_serial_number=new_sn_value)

        self.assertEquals(load_dv_mock.call_count, 1)
        self.assertEquals(do_search_for_event_switches_mock.call_count, 1)

    #################################
    def test_verify_who_i_am_pass1(self):
        """
        Test Verify Who I Am Pass Case 1: Run through the method
        """
        # Create event switch object
        sw = self.create_test_event_switch_object()

        # Verify mock description on controller
        description = mock.MagicMock(return_value='Test Event Switch TPD0001')
        sw.verify_description = description

        #verify mock enable state on controller
        enable_state = mock.MagicMock
        sw.verify_enabled_state = enable_state

        # Verify mock serial number on controller
        serial_number = mock.MagicMock()
        sw.verify_serial_number = serial_number

        # Verify mock latitude on controller
        latitude = mock.MagicMock()
        sw.verify_latitude = latitude

        # Verify mock longitude on the controller
        longitude = mock.MagicMock
        sw.verify_longitude = longitude

        # Set expected status to NOT none (something besides none), then verify mock status on the controller
        expected_status = mock.MagicMock(return_value='CN')
        sw.verify_status = expected_status

        # Verify the mock temperature reading on the controller
        contact_state = mock.MagicMock()
        sw.verify_contact_state = contact_state

        # Verify the mock two wire drop value on the controller
        two_wire_drop_value = mock.MagicMock()
        sw.verify_two_wire_drop_value = two_wire_drop_value

        # Go into the method
        sw.verify_who_i_am(_expected_status='CN')
    # TODO Move below items to the switch bicder tests
    # #################################
    # def test_set_enable_state_pass1(self):
    #     """ Set Enable State On Controller Pass Case 1: Using Default Value """
    #     expected_command = "SET,{0}=TPD0001,{1}=TR".format(opcodes.event_switch, opcodes.enabled)
    #     switch = self.create_test_event_switch_object()
    #     switch.set_enable_state()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    #
    # #################################
    # def test_set_enable_state_pass2(self):
    #     """ Set Enable State On Controller Pass Case 2: Using Passed In Argument """
    #     expected_command = "SET,{0}=TPD0001,{1}=FA".format(opcodes.event_switch, opcodes.enabled)
    #     switch = self.create_test_event_switch_object()
    #     switch.set_enable_state("FA")
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    #
    # #################################
    # def test_set_enable_state_fail1(self):
    #     """ Set Enable State On Controller Fail Case 1: Invalid State Argument """
    #     switch = self.create_test_event_switch_object()
    #     with self.assertRaises(ValueError) as context:
    #         switch.set_enable_state("Foo")
    #     e_msg = "Invalid enabled state entered for Event Switch TPD0001 (1). Received: Foo, Expected: 'TR' or " \
    #             "'FA'"
    #     self.assertEqual(first=e_msg, second=context.exception.message)
    #
    # #################################
    # def test_set_enable_state_fail2(self):
    #     """ Set Enable State On Controller Fail Case 2: Failed Communication With Controller """
    #     switch = self.create_test_event_switch_object()
    #
    #     # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
    #     # raising an exception trying to set default values.
    #     self.mock_ser.send_and_wait_for_reply.side_effect = Exception
    #
    #     with self.assertRaises(Exception) as context:
    #         switch.set_enable_state("FA")
    #     e_msg = "Exception occurred trying to set Event Switch TPD0001 (1)'s Enable State"
    #     self.assertEqual(first=e_msg, second=context.exception.message)
    #
    # #################################
    # def test_set_contact_state_pass1(self):
    #     """ Set Contact State On Controller Pass Case 1: Using Default Value """
    #     expected_command = "SET,{0}=TPD0001,{1}=CL".format(opcodes.event_switch, opcodes.contacts_state)
    #     switch = self.create_test_event_switch_object()
    #     switch.set_contact_state()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO need to add a wrapper method to sw.py for bicoder.set_contact_state()
    # #################################
    # def test_set_contact_state_pass2(self):
    #     """ Set Contact State On Controller Pass Case 2: Using Passed In Argument """
    #     expected_command = "SET,{0}=TPD0001,{1}=OP".format(opcodes.event_switch, opcodes.contacts_state)
    #     switch = self.create_test_event_switch_object()
    #     switch.set_contact_state("OP")
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO need to add a wrapper method to sw.py for bicoder.set_contact_state()
    # #################################
    # def test_set_contact_state_fail1(self):
    #     """ Set Contact State On Controller Fail Case 1: Invalid State Argument """
    #     switch = self.create_test_event_switch_object()
    #     with self.assertRaises(ValueError) as context:
    #         switch.set_contact_state("Foo")
    #     e_msg = "Unable to set Event Switch TPD0001 (1)'s Contact State. Received: Foo, needs to be FA or TR"
    #     self.assertEqual(first=e_msg, second=context.exception.message)
    # TODO need to add a wrapper method to sw.py for bicoder.set_contact_state()
    # #################################
    # def test_set_contact_state_fail2(self):
    #     """ Set Contact State On Controller Fail Case 2: Failed Communication With Controller """
    #     switch = self.create_test_event_switch_object()
    #
    #     # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
    #     # raising an exception trying to set default values.
    #     self.mock_ser.send_and_wait_for_reply.side_effect = Exception
    #
    #     with self.assertRaises(Exception) as context:
    #         switch.set_contact_state("OP")
    #     e_msg = "Exception occurred trying to set Event Switch TPD0001 (1)'s 'Contact State' to: 'OP'"
    #     self.assertEqual(first=e_msg, second=context.exception.message)
    # TODO need to add a wrapper method to sw.py for bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_pass1(self):
    #     """ Set Two Wire Drop Value On Controller Pass Case 1: Using Default _vt value """
    #     switch = self.create_test_event_switch_object()
    #
    #     # Expected value is the _vt value set at object Zone object creation
    #     expected_value = switch.vt
    #     switch.set_two_wire_drop_value()
    #
    #     # _vt value is set during this method and should equal the original value
    #     actual_value = switch.vt
    #     self.assertEqual(first=expected_value, second=actual_value)
    # TODO need to add a wrapper method to sw.py for bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_pass2(self):
    #     """ Set Two Wire Drop Value On Controller Pass Case 2: Setting new _vt value = 6.0 """
    #     switch = self.create_test_event_switch_object()
    #
    #     # Expected _vt value is 6
    #     expected_value = 6.0
    #     switch.set_two_wire_drop_value(expected_value)
    #
    #     # _vt value is set during this method and should equal the value passed into the method
    #     actual_value = switch.vt
    #     self.assertEqual(expected_value, actual_value)
    # TODO need to add a wrapper method to sw.py for bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_pass3(self):
    #     """ Set Two Wire Drop Value On Controller Pass Case 3: Command with correct values sent to controller """
    #     switch = self.create_test_event_switch_object()
    #
    #     vt_value = str(switch.vt)
    #     expected_command = "SET,{0}=TPD0001,{1}={2}".format(opcodes.event_switch, opcodes.two_wire_drop, vt_value)
    #     switch.set_two_wire_drop_value()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO need to add a wrapper method to sw.py for bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_fail1(self):
    #     """ Set Two Wire Drop Value On Controller Fail Case 1: Pass String value as argument """
    #     switch = self.create_test_event_switch_object()
    #
    #     new_vt_value = "b"
    #     with self.assertRaises(Exception) as context:
    #         switch.set_two_wire_drop_value(new_vt_value)
    #     expected_message = "Failed trying to set Event Switch TPD0001 (1) two wire drop. Invalid argument type, " \
    #                        "expected an int or float, received: {0}".format(type(new_vt_value))
    #     self.assertEqual(expected_message, context.exception.message)
    # TODO need to add a wrapper method to sw.py for bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_fail2(self):
    #     """ Set Two Wire Drop Value On Controller Fail Case 2: Unable to send command to controller """
    #     switch = self.create_test_event_switch_object()
    #     _vt = 2
    #     self.mock_send_and_wait_for_reply.side_effect = Exception
    #     switch.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply
    #     with self.assertRaises(Exception) as context:
    #         switch.set_two_wire_drop_value(_value=_vt)
    #     expected_msg = "Exception occurred trying to set Event Switch {0} ({1})'s 'Two Wire Drop Value' to: '{2}'"\
    #         .format(switch.sn, switch.ad, _vt)
    #     self.assertEqual(first=expected_msg, second=context.exception.message)
    # TODO need to add a wrapper method to sw.py for bicoder.verify_contact_state()
    # #################################
    # def test_verify_contact_state_pass1(self):
    #     """ Verify Contact State On Controller Pass Case 1: Exception is not raised """
    #     switch = self.create_test_event_switch_object()
    #     switch.vc = 'OP'
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TPD0001,DS=Blah,{0}=OP".format(opcodes.contacts_state))
    #     switch.data = mock_data
    #
    #     try:
    #         # .assertRaises raises an Assertion Error if an Exception is not raised in the method
    #         with self.assertRaises(Exception):
    #             switch.verify_contact_state()
    #
    #     # Catches an Assertion Error from above, meaning the method did NOT raise an exception
    #     # meaning verification passed
    #     except AssertionError as ae:
    #         e_msg = ae.message
    #         expected_message = "Exception not raised"
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
    # TODO need to add a wrapper method to sw.py for bicoder.verify_contact_state()
    # #################################
    # def test_verify_contact_state_fail1(self):
    #     """ Verify Contact State On Controller Fail Case 1: Value on controller does not match what is
    #     stored in switch.vc """
    #     switch = self.create_test_event_switch_object()
    #     switch.vc = 'Cl'
    #     test_pass = False
    #     e_msg = ""
    #
    #     mock_data = status_parser.KeyValues("SN=TPD0001,DS=Blah,{0}=CL".format(opcodes.contacts_state))
    #     switch.data = mock_data
    #
    #     expected_message = "Unable verify Event Switch TPD0001 (1)'s contact state. Received: CL, " \
    #                        "Expected: Cl"
    #     try:
    #         switch.verify_contact_state()
    #
    #     # Catches an Exception from above, meaning the method did raise an exception
    #     # meaning verification failed
    #     except Exception as e:
    #         e_msg = e.message
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
    #                      .format(expected_message, e_msg))
    # TODO need to add a wrapper method to sw.py for bicoder.verify_two_wire_drop_value()
    # #################################
    # def test_verify_two_wire_drop_value_pass1(self):
    #     """ Verify Two Wire Drop Value On Controller Pass Case 1: Exception is not raised """
    #     switch = self.create_test_event_switch_object()
    #     switch.vt = 5.0
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TPD0001,DS=Blah,{0}=5.0".format(opcodes.two_wire_drop))
    #     switch.data = mock_data
    #
    #     try:
    #         # .assertRaises raises an Assertion Error if an Exception is not raised in the method
    #         with self.assertRaises(Exception):
    #             switch.verify_two_wire_drop_value()
    #
    #     # Catches an Assertion Error from above, meaning the method did NOT raise an exception
    #     # meaning verification passed
    #     except AssertionError as ae:
    #         e_msg = ae.message
    #         expected_message = "Exception not raised"
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
    # TODO need to add a wrapper method to sw.py for bicoder.verify_two_wire_drop_value()
    # #################################
    # def test_verify_two_wire_drop_value_fail1(self):
    #     """ Verify Two Wire Drop Value On Controller Fail Case 1: Value on controller does not match what is
    #     stored in switch.vt """
    #     switch = self.create_test_event_switch_object()
    #     switch.vt = 5.0
    #     test_pass = False
    #     e_msg = ""
    #
    #     mock_data = status_parser.KeyValues("SN=TPD0001,DS=Blah,{0}=6".format(opcodes.two_wire_drop))
    #     switch.data = mock_data
    #
    #     expected_message = "Unable verify Event Switch TPD0001 (1)'s two wire drop value. Received: 6.0, Expected: 5.0"
    #     try:
    #         switch.verify_two_wire_drop_value()
    #
    #     # Catches an Exception from above, meaning the method did raise an exception
    #     # meaning verification failed
    #     except Exception as e:
    #         e_msg = e.message
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
    #                      .format(expected_message, e_msg))
    # TODO need to move to base methods
    # #################################
    # def test_verify_enable_state_pass1(self):
    #     """ Verify Enable State On Controller Pass Case 1: Exception is not raised """
    #     switch = self.create_test_event_switch_object()
    #     switch.en = 'TR'
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TPD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
    #     switch.data = mock_data
    #
    #     try:
    #         # .assertRaises raises an Assertion Error if an Exception is not raised in the method
    #         with self.assertRaises(Exception):
    #             switch.verify_enable_state()
    #
    #     # Catches an Assertion Error from above, meaning the method did NOT raise an exception
    #     # meaning verification passed
    #     except AssertionError as ae:
    #         e_msg = ae.message
    #         expected_message = "Exception not raised"
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
    # TODO need to move to base methods
    # #################################
    # def test_verify_enable_state_fail1(self):
    #     """ Verify Enable State On Controller Fail Case 1: Value on controller does not match what is
    #     stored in switch.en """
    #     switch = self.create_test_event_switch_object()
    #     switch.en = 'Tr'
    #     test_pass = False
    #     e_msg = ""
    #
    #     mock_data = status_parser.KeyValues("SN=TPD0001,DS=Blah,{0}=TR".format(opcodes.enabled))
    #     switch.data = mock_data
    #
    #     expected_message = "Unable verify Event Switch TPD0001 (1)'s enabled state. Received: TR, " \
    #                        "Expected: {0}".format(str(switch.en))
    #     try:
    #         switch.verify_enable_state()
    #
    #     # Catches an Exception from above, meaning the method did raise an exception
    #     # meaning verification failed
    #     except Exception as e:
    #         e_msg = e.message
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
    #                      .format(expected_message, e_msg))
    #

    #


    if __name__ == "__main__":
        unittest.main()
