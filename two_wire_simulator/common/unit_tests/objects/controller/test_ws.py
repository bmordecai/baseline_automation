import unittest
import mock
import serial
import status_parser

from common.objects.base_classes.devices import BaseDevices
# you have to set the lat and long after the devices class is called so that they don't get skipped over
BaseDevices.controller_lat = float(43.609768)
BaseDevices.controller_long = float(-116.310569)
from common.objects.controllers.bl_fs import FlowStation
from common.objects.controllers.bl_32 import BaseStation3200
from common.imports import opcodes
from common.objects.programming.point_of_control import PointOfControl
from common.objects.programming.ws import WaterSource
from common.imports.types import ActionCommands
import common.object_factory


__author__ = 'Kent'


class TestWaterSourceObject(unittest.TestCase):

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_3200_ser = mock.MagicMock(spec=serial.Serial)
        self.mock_fs_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_3200_send_and_wait_for_reply = mock.MagicMock(side_effect=None)
        self.mock_fs_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_3200_ser.send_and_wait_for_reply = self.mock_3200_send_and_wait_for_reply
        self.mock_fs_ser.send_and_wait_for_reply = self.mock_fs_send_and_wait_for_reply

        # Create mock get method
        self.mock_3200_get_and_wait_for_reply = mock.MagicMock(side_effect=None)
        self.mock_fs_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_3200_ser.get_and_wait_for_reply = self.mock_3200_get_and_wait_for_reply
        self.mock_fs_ser.get_and_wait_for_reply = self.mock_fs_get_and_wait_for_reply

        # Create a mock 3200 to pass into the zone.
        self.bl_3200 = self.create_test_basestation_3200_object()

        # Create a mock 3200 to pass into the zone.
        self.bl_fs = self.create_test_flowstation_object()

        # Add 3200 to FlowStation (mocking "assigning")
        self.bl_fs.controllers = dict()
        self.bl_fs.controllers[1] = self.bl_3200

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_water_source_object(self, _diffaddress=1):
        """
        Creates a test water source object with the given address on the controller
        :param _diffaddress: Address of the water source on the controller. Must be between 1 and 8
        :return: Water Source object
        """

        if _diffaddress in range(1, 8):
            ws = WaterSource(self.bl_3200, _diffaddress)
            self.bl_3200.water_sources[_diffaddress] = ws
        else:
            e_msg = "Exception occurred creating test water source object. Address must be between 1 and 8." \
                    " Address given: {0}".format(_diffaddress)
            raise Exception(e_msg)

        return ws

    #################################
    def create_test_flowstation_object(self, _diffip="129.0.0.0", _diffmac="0004EE218C85", _diffsn="FX10001",
                                       _diffser=None, _diffvr="2.0.96", _diffportaddress=None):
        """
        Creates a new controller object for use in a unit test
        :rtype: FlowStation
        """
        if _diffser is not None:
            serial_port = _diffser
        else:
            serial_port = self.mock_fs_ser

        flowstation = FlowStation(_mac=_diffmac, _serial_port=serial_port, _serial_number=_diffsn,
                                  _firmware_version=_diffvr, _port_address=_diffportaddress, _socket_port=None,
                                  _ip_address=_diffip)

        return flowstation

    #################################
    def create_test_basestation_3200_object(self, _diffip="129.0.0.0", _diffmac="0008EE218C85", _diffsn="3K00001",
                                            _diffser=None, _diffvr="", _diffportaddress=None, _diff_socket=None):
        """ Creates a new controller object for use in a unit test """
        if _diffser is not None:
            serial_port = _diffser
        else:
            serial_port = self.mock_3200_ser
        controller = BaseStation3200(_mac=_diffmac,
                                     _serial_number=_diffser,
                                     _firmware_version=_diffvr,
                                     _serial_port=serial_port,
                                     _port_address=_diffportaddress,
                                     _socket_port=_diff_socket,
                                     _ip_address=_diffip)

        return controller

    #################################
    def test_build_obj_configuration_for_send_pass1(self):
        """ Set Obj Configuration for Send Pass Case 1: Correct Command Sent for controller type of '32'"""
        ws = self.create_test_water_source_object()

        # create the expected current config string
        expected_current_config = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14}".format(
            ActionCommands.SET,                 # {0}
            opcodes.water_source,               # {1}
            str(ws.ad),                         # {2}
            opcodes.description,                # {3}
            ws.ds,                              # {4}
            opcodes.enabled,                    # {5}
            ws.en,                              # {6}
            opcodes.monthly_water_budget,       # {7}
            str(ws.wb),                         # {8}
            opcodes.priority,                   # {9}
            str(ws.pr),                         # {10}
            opcodes.with_shut_down,             # {11}
            ws.ws,                              # {12}
            opcodes.water_rationing_enable,     # {13}
            str(ws.wr),                         # {14}
        )

        # create the actual current config string
        actual_current_config = ws.build_obj_configuration_for_send()

        # verify the expected current config matches the actual current config
        self.assertEqual(actual_current_config, expected_current_config)

    #################################
    def test_build_obj_configuration_for_send_pass2(self):
        """ Set Obj Configuration for Send Pass Case 1: Correct Command Sent for controller type other than '32'"""
        ws = self.create_test_water_source_object()
        ws.controller.controller_type = ''

        # create the expected current config string
        expected_current_config = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14}".format(
            ActionCommands.SET,                 # {0}
            opcodes.water_source,               # {1}
            str(ws.ad),                         # {2}
            opcodes.description,                # {3}
            ws.ds,                              # {4}
            opcodes.enabled,                    # {5}
            ws.en,                              # {6}
            opcodes.monthly_water_budget,       # {7}
            str(ws.wb),                         # {8}
            opcodes.priority,                   # {9}
            str(ws.pr),                         # {10}
            opcodes.with_shut_down,             # {11}
            ws.ws,                              # {12}
            opcodes.water_rationing_enable,     # {13}
            str(ws.wr),                         # {14}
        )

        # create the actual current config string
        actual_current_config = ws.build_obj_configuration_for_send()

        # verify the expected current config matches the actual current config
        self.assertEqual(actual_current_config, expected_current_config)

    #################################
    def test_build_obj_configuration_for_send_pass3(self):
        """ Set Obj Configuration for Send Pass Case 3: Correct Command Sent for controller type of '32' and PC set"""
        ws = self.create_test_water_source_object()
        ws.pc = 1

        # create the expected current config string
        expected_current_config = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14},{15}={16}".format(
            ActionCommands.SET,                 # {0}
            opcodes.water_source,               # {1}
            str(ws.ad),                         # {2}
            opcodes.description,                # {3}
            ws.ds,                              # {4}
            opcodes.enabled,                    # {5}
            ws.en,                              # {6}
            opcodes.monthly_water_budget,       # {7}
            str(ws.wb),                         # {8}
            opcodes.priority,                   # {9}
            str(ws.pr),                         # {10}
            opcodes.with_shut_down,             # {11}
            ws.ws,                              # {12}
            opcodes.water_rationing_enable,     # {13}
            str(ws.wr),                         # {14}
            opcodes.point_of_control,           # {13}
            str(ws.pc),                         # {14}
        )

        # create the actual current config string
        actual_current_config = ws.build_obj_configuration_for_send()

        # verify the expected current config matches the actual current config
        self.assertEqual(actual_current_config, expected_current_config)

    #################################
    def test_send_programming_pass1(self):
        """ Test send programming Pass Case 1: set the default values on the device and verify send command"""
        # create the test water source object
        ws = self.create_test_water_source_object()
        ws.controller.controller_type = '32'
        # create the expected current config string
        expected_current_config = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14}".format(
            ActionCommands.SET,                 # {0}
            opcodes.water_source,               # {1}
            str(ws.ad),                         # {2}
            opcodes.description,                # {3}
            ws.ds,                              # {4}
            opcodes.enabled,                    # {5}
            ws.en,                              # {6}
            opcodes.monthly_water_budget,       # {7}
            str(ws.wb),                         # {8}
            opcodes.priority,                   # {9}
            str(ws.pr),                         # {10}
            opcodes.with_shut_down,             # {11}
            ws.ws,                              # {12}
            opcodes.water_rationing_enable,     # {13}
            str(ws.wr),                         # {14}
        )

        # mock the build_obj_configuration_for_send
        ws.build_obj_configuration_for_send = mock.MagicMock(return_value=expected_current_config)

        # call send_programming()
        ws.send_programming()

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_3200_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_current_config)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values On Controller Fail Case 1: Handle Exception Raised From Controller """
        ws = self.create_test_water_source_object()
        expected_current_config = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14}".format(
            ActionCommands.SET,                 # {0}
            opcodes.water_source,               # {1}
            str(ws.ad),                         # {2}
            opcodes.description,                # {3}
            ws.ds,                              # {4}
            opcodes.enabled,                    # {5}
            ws.en,                              # {6}
            opcodes.monthly_water_budget,       # {7}
            str(ws.wb),                         # {8}
            opcodes.priority,                   # {9}
            str(ws.pr),                         # {10}
            opcodes.with_shut_down,             # {11}
            ws.ws,                              # {12}
            opcodes.water_rationing_enable,     # {13}
            str(ws.wr),                         # {14}
        )
        test_exception = "Test exception"
        e_msg = "Exception occurred trying to set Water Source {0}'s 'Values' to: '{1}' -> {2}".format(
            str(ws.ad),                 # {0}
            expected_current_config,    # {1}
            test_exception              # {2}
        )

        ws.build_obj_configuration_for_send = mock.MagicMock(return_value=expected_current_config)
        self.mock_3200_ser.send_and_wait_for_reply.side_effect = Exception(test_exception)
        with self.assertRaises(Exception) as context:
            ws.send_programming()
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_add_point_of_control_to_water_source_pass1(self):
        """ Test add point of control to water source Pass Case 1: add a poc to a water source and test the command"""
        # create the test water source object
        ws = self.create_test_water_source_object()

        # Set the controller type
        ws.controller.controller_type = '32'

        # Set the controller firmware version
        ws.controller.vr = "16.0.540"

        # Create the poc address
        poc_address = 1

        # Create the Point of Control
        poc = PointOfControl(self.bl_3200, _ad=poc_address)

        self.bl_3200.points_of_control[poc_address] = poc

        # create the expected current config string
        expected_command = "{0},{1}={2},{3}={4}".format(
            opcodes.set_action,
            opcodes.water_source,
            str(ws.ad),
            opcodes.point_of_control,
            poc_address
        )

        # call add_point_of_control_to_water_source()
        ws.add_point_of_control_to_water_source(_point_of_control_address=poc_address)

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_3200_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_add_multiple_point_of_control_to_water_source_pass1(self):
        """ Test add multiple point of control to water source Pass Case 1: add multiple poc to a water source on
        flow station and test the command sent to the flow station. """

        ws_address = 1

        # create the test water source object
        ws = self.create_test_water_source_object(_diffaddress=ws_address)

        # Create the poc address
        poc1_address = 1
        poc2_address = 2

        # Create the Point of Control
        poc1 = PointOfControl(self.bl_3200, _ad=poc1_address)
        poc2 = PointOfControl(self.bl_3200, _ad=poc2_address)

        # Add pocs to FlowStation
        self.bl_fs.points_of_control[poc1_address] = poc1
        self.bl_fs.points_of_control[poc2_address] = poc2

        # Add water source to FlowStation
        self.bl_fs.add_controller_water_source_to_flowstation(
            _controller_address=1,
            _controller_water_source_address=1,
            _flow_station_water_source_slot_number=1)

        # create the expected current config string
        expected_command_first_poc = "{0},{1}={2},{3}={4}".format(
            opcodes.set_action,
            opcodes.water_source,
            ws_address,
            opcodes.point_of_control,
            poc1_address
        )

        # create the expected current config string
        expected_command_second_poc = "{0},{1}={2},{3}={4}={5}".format(
            opcodes.set_action,
            opcodes.water_source,
            ws_address,
            opcodes.point_of_control,
            poc1_address,
            poc2_address
        )

        # call add_point_of_control_to_water_source()
        self.bl_fs.get_water_source(ws_address).add_point_of_control_to_water_source(_point_of_control_address=poc1_address)

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_fs_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command_first_poc)

        # call add_point_of_control_to_water_source() second time for second downstream poc
        self.bl_fs.get_water_source(ws_address).add_point_of_control_to_water_source(_point_of_control_address=poc2_address)

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_fs_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command_second_poc)

    #################################
    def test_add_point_of_control_to_water_source_fail1(self):
        """ Test add point of control to water source fail Case 1: test passing in a value other than an int """
        # Create the test water source object
        ws = self.create_test_water_source_object()

        # Set the poc address
        poc_address = 'b'

        # Create the expected exception string
        e_msg = "Exception occurred trying to assign point of control {0} to Water Source: '{1}' the value must " \
                "be an int".format(
                    str(poc_address),  # {0}
                    str(ws.ad)         # {1}
                )

        with self.assertRaises(Exception) as context:
            ws.add_point_of_control_to_water_source(_point_of_control_address=poc_address)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_add_point_of_control_to_water_source_fail2(self):
        """ Test add point of control to water source fail Case 2: test passing in a value not in the controller poc
         dictionary"""

        # Create the test water source object
        ws = self.create_test_water_source_object()

        # Set the poc address
        poc_address = 9

        # Create the expected exception string
        e_msg = "Invalid 'Point of Control' address entered for Water Source {0}. Available Points of Control are 1 - 8, user entered: {1}".format(
            str(ws.ad),                 # {0}
            str(poc_address)            # {1}
        )

        with self.assertRaises(ValueError) as context:
            ws.add_point_of_control_to_water_source(_point_of_control_address=poc_address)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_add_point_of_control_to_water_source_fail3(self):
        """ Test add point of control to water source fail Case 2: test communication error with controller"""
        # create the test water source object
        ws = self.create_test_water_source_object()

        # Set the controller type
        ws.controller.controller_type = '32'

        # Set the controller firmware version
        ws.controller.vr = "16.0.540"

        # Create the poc address
        poc_address = 1

        # Create the Point of Control
        poc = PointOfControl(self.bl_3200, _ad=poc_address)

        self.bl_3200.points_of_control[poc_address] = poc

        # create the expected error message string
        e_msg = "Exception occurred trying to assign a Point of control {0} to Water Source {1} -> {2}".format(
            str(poc_address),  # {0}
            str(ws.ad),        # {1}
            ""                 # {2}
        )

        self.mock_3200_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ws.add_point_of_control_to_water_source(_point_of_control_address=poc_address)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_priority_pass1(self):
        """ Test set priority Pass Case 1: set priority to first value range 1-10 and verify the send command"""
        # create the test water source object
        ws = self.create_test_water_source_object()

        # Set the controller type
        ws.controller.controller_type = '32'

        # Set the controller firmware version
        ws.controller.vr = "16.0.540"

        # Create the priority level variable
        priority_level = 1

        # create the expected current config string
        expected_command = "{0},{1}={2},{3}={4}".format(
            opcodes.set_action,
            opcodes.water_source,
            str(ws.ad),
            opcodes.priority,
            priority_level
        )

        # call add_point_of_control_to_water_source()
        ws.set_priority(_priority_for_water_source=priority_level)

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_3200_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_priority_pass2(self):
        """ Test set priority Pass Case 2: set priority to last value in range 1-10 and verify the send command"""
        # create the test water source object
        ws = self.create_test_water_source_object()

        # Set the controller type
        ws.controller.controller_type = '32'

        # Set the controller firmware version
        ws.controller.vr = "16.0.540"

        # Create the priority level variable
        priority_level = 10

        # create the expected current config string
        expected_command = "{0},{1}={2},{3}={4}".format(
            opcodes.set_action,
            opcodes.water_source,
            str(ws.ad),
            opcodes.priority,
            priority_level
        )

        # call add_point_of_control_to_water_source()
        ws.set_priority(_priority_for_water_source=priority_level)

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_3200_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_priority_fail1(self):
        """ Test set priority fail Case 1: test passing in a value other than an int """
        # Create the test water source object
        ws = self.create_test_water_source_object()

        # Create the priority level variable
        priority_level = 'b'

        # Create the expected exception string
        e_msg = "Exception occurred trying to assign a priority {0}'to Water Source: '{1}' the value must " \
                "be an int".format(
                    str(priority_level),    # {0}
                    str(ws.ad)              # {1}
                )

        with self.assertRaises(Exception) as context:
            ws.set_priority(_priority_for_water_source=priority_level)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_priority_fail2(self):
        """ Test set priority fail Case 2: test passing in a value not in the range 1-10 - value below lower bound"""

        # Create the test water source object
        ws = self.create_test_water_source_object()

        # Set the poc address
        priority_level = 0

        # Create the expected exception string
        e_msg = "Exception occurred trying to assign a priority {0}'to Water Source: '{1}' the value must " \
                "be between 1 and 10 ".format(
                    str(priority_level),    # {0}
                    str(ws.ad)              # {1}
                )

        with self.assertRaises(Exception) as context:
            ws.set_priority(_priority_for_water_source=priority_level)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_priority_fail3(self):
        """ Test set priority fail Case 2: test passing in a value not in the range 1-10 - value above upper bound"""

        # Create the test water source object
        ws = self.create_test_water_source_object()

        # Set the priority level
        priority_level = 11

        # Create the expected exception string
        e_msg = "Exception occurred trying to assign a priority {0}'to Water Source: '{1}' the value must " \
                "be between 1 and 10 ".format(
                    str(priority_level),    # {0}
                    str(ws.ad)              # {1}
                )

        with self.assertRaises(Exception) as context:
            ws.set_priority(_priority_for_water_source=priority_level)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_priority_fail4(self):
        """ Test test priority fail Case 4: test communication error with controller"""
        # Create the test water source object
        ws = self.create_test_water_source_object()
        test_exception_message = "Test Error Message"
        # Set the priority level
        priority_level = 5

        # Create the expected exception string
        e_msg = "Exception occurred trying to assign a priority to a Water Source {0}'s 'priority to:" \
                " '{1}' -> {2}".format(
                    str(ws.ad),             # {0}
                    priority_level,         # {1}
                    test_exception_message  # {2}
                )
        self.mock_3200_ser.send_and_wait_for_reply.side_effect = Exception(test_exception_message)

        with self.assertRaises(Exception) as context:
            ws.set_priority(_priority_for_water_source=priority_level)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_monthly_watering_budget_pass1(self):
        """ Test set monthly watering budget Pass Case 1: set budget to an int and shutdown enabled to true"""
        # create the test water source object
        ws = self.create_test_water_source_object()

        # Create new Monthly Budget variable
        monthly_budget = 50
        shutdown_enabled = opcodes.true

        # create the expected current config string
        expected_command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,
            opcodes.water_source,
            str(ws.ad),
            opcodes.monthly_water_budget,
            str(monthly_budget),
            str(opcodes.with_shut_down),
            shutdown_enabled)

        # call set_monthly_watering_budget()
        ws.set_monthly_watering_budget(_budget=monthly_budget, _with_shutdown_enabled=True)

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_3200_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_monthly_watering_budget_pass2(self):
        """ Test set monthly watering budget Pass Case 2: set budget to a float and shutdown enabled to true"""
        # create the test water source object
        ws = self.create_test_water_source_object()

        # Create new Monthly Budget variable
        monthly_budget = 25.5
        shutdown_enabled = opcodes.true

        # create the expected current config string
        expected_command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,
            opcodes.water_source,
            str(ws.ad),
            opcodes.monthly_water_budget,
            str(monthly_budget),
            str(opcodes.with_shut_down),
            shutdown_enabled)

        # call set_monthly_watering_budget()
        ws.set_monthly_watering_budget(_budget=monthly_budget, _with_shutdown_enabled=True)

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_3200_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_monthly_watering_budget_pass3(self):
        """ Test set monthly watering budget Pass Case 1: set budget to an int and shutdown enabled to false"""
        # create the test water source object
        ws = self.create_test_water_source_object()

        # Create new Monthly Budget variable
        monthly_budget = 50
        shutdown_enabled = opcodes.false

        # create the expected current config string
        expected_command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,
            opcodes.water_source,
            str(ws.ad),
            opcodes.monthly_water_budget,
            str(monthly_budget),
            str(opcodes.with_shut_down),
            shutdown_enabled)

        # call set_monthly_watering_budget()
        ws.set_monthly_watering_budget(_budget=monthly_budget, _with_shutdown_enabled=False)

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_3200_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_monthly_watering_budget_pass4(self):
        """ Test set monthly watering budget Pass Case 1: set default values"""
        # create the test water source object
        ws = self.create_test_water_source_object()
        ws.ws = opcodes.false

        # Create new Monthly Budget variable
        monthly_budget = 60
        shutdown_enabled = opcodes.false

        ws.wb = monthly_budget
        ws.ws = shutdown_enabled

        # create the expected current config string
        expected_command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,
            opcodes.water_source,
            str(ws.ad),
            opcodes.monthly_water_budget,
            str(monthly_budget),
            str(opcodes.with_shut_down),
            ws.ws
)

        # call set_monthly_watering_budget()
        ws.set_monthly_watering_budget()

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_3200_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_monthly_watering_budget_fail1(self):
        """ Test set priority fail Case 1: test passing in a value not an int or float"""

        # Create the test water source object
        ws = self.create_test_water_source_object()

        # Set the priority level
        monthly_budget = 'b'

        # Create the expected exception string
        e_msg = "Exception occurred trying to set Water Source{0}'s 'Monthly Budget' to: '{1}' the value must " \
                "be an int or a float".format(
                    str(ws.ad),             # {0}
                    str(monthly_budget)     # {1}
                )

        with self.assertRaises(Exception) as context:
            ws.set_monthly_watering_budget(_budget=monthly_budget)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_monthly_watering_budget_fail2(self):
        """ Test set priority fail Case 2: with shutdown enabled not true or false"""

        # Create the test water source object
        ws = self.create_test_water_source_object()

        # Set the priority level
        monthly_budget = 50
        with_shutdown_enabled = 'b'

        # Create the expected exception string
        e_msg = "Exception occurred trying to set Water Source{0}'s 'Monthly Budget with shut Down' to: " \
                "'{1}' the value must be True or False" .format(
                    str(ws.ad),                 # {0}
                    str(with_shutdown_enabled)  # {1}
                )

        with self.assertRaises(Exception) as context:
            ws.set_monthly_watering_budget(_budget=monthly_budget, _with_shutdown_enabled=with_shutdown_enabled)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_monthly_watering_budget_fail3(self):
        """ Test set monthly water budget fail Case 3: test communication error with controller"""
        # Create the test water source object
        ws = self.create_test_water_source_object()

        # Set the priority level
        monthly_budget = 50
        with_shutdown_enabled = True

        # Create the expected exception string
        e_msg = "Exception occurred trying to set Water Source {0}'s 'Monthly Budget' to: '{1}' and " \
                "'Shutdown Enabled' to {2} -> {3}".format(
                    str(ws.ad),             # {0}
                    str(monthly_budget),    # {1}
                    str(opcodes.true),      # {2}
                    ""                      # {3}
                )
        self.mock_3200_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ws.set_monthly_watering_budget(_budget=monthly_budget, _with_shutdown_enabled=with_shutdown_enabled)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_water_rationing_to_enabled_pass1(self):
        """ Test water rationing to enabled Pass Case 1: set water rationing to true"""
        # create the test water source object
        ws = self.create_test_water_source_object()

        # create the expected current config string
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,
            opcodes.water_source,
            str(ws.ad),
            opcodes.water_rationing_enable,
            str(opcodes.true))

        # call set_monthly_watering_budget()
        ws.set_water_rationing_to_enabled()

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_3200_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_rationing_to_enabled_fail1(self):
        """ Test set monthly water budget fail Case 1: test communication error with controller"""
        # Create the test water source object
        ws = self.create_test_water_source_object()

        # Create the expected exception string
        e_msg = "Exception occurred trying to set Water Source {0}'s 'Water Rationing' to: '{1}' -> {2}".format(
            str(ws.ad),         # {0}
            str(opcodes.true),  # {1}
            ""                  # {2}
        )

        self.mock_3200_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ws.set_water_rationing_to_enabled()

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_water_rationing_to_disabled_pass1(self):
        """ Test water rationing to disabled Pass Case 1: set water rationing to true"""
        # create the test water source object
        ws = self.create_test_water_source_object()

        # create the expected current config string
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,
            opcodes.water_source,
            str(ws.ad),
            opcodes.water_rationing_enable,
            str(opcodes.false))

        # call set_monthly_watering_budget()
        ws.set_water_rationing_to_disabled()

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_3200_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_rationing_to_disabled_fail1(self):
        """ Test water rationing to disabled Fail Case 1: test communication error with controller"""
        # Create the test water source object
        ws = self.create_test_water_source_object()

        # Create the expected exception string
        e_msg = "Exception occurred trying to set Water Source {0}'s 'Water Rationing' to: '{1}' -> {2}".format(
            str(ws.ad),          # {0}
            str(opcodes.false),  # {1}
            ""                   # {2}
        )

        self.mock_3200_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ws.set_water_rationing_to_disabled()

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_share_with_flow_station_to_true_pass1(self):
        """ Test set share with flow station to true Pass Case 1: set share with flow station to true"""
        # create the test water source object
        ws = self.create_test_water_source_object()
        ws.sh = opcodes.false    # This allows the method to perform its normal logic

        # create the expected current config string
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,
            opcodes.water_source,
            str(ws.ad),
            opcodes.share_with_flowstation,
            str(opcodes.true))

        # call set_monthly_watering_budget()
        ws.set_manage_by_flowstation()

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_3200_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_share_with_flow_station_to_true_fail1(self):
        """ Test set share with flow station to true Fail Case 1: Test communication error with controller"""
        # Create the test water source object
        ws = self.create_test_water_source_object()
        ws.sh = opcodes.false  # This allows the method to perform its normal logic

        # Create the expected exception string
        e_msg = "Exception occurred trying to set {0} {1}'s 'Share With FlowStation' to: '{2}' -> {3}".format(
            ws.identifiers[0][0],   # {0}
            str(ws.ad),             # {1}
            str(opcodes.true),      # {2}
            ""                      # {3}
        )

        self.mock_3200_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ws.set_manage_by_flowstation()

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_share_with_flow_station_to_false_pass1(self):
        """ Test set share with flow station to false Pass Case 1: set share with flow station to false"""
        # create the test water source object
        ws = self.create_test_water_source_object()
        ws.sh = opcodes.true  # This allows the method to perform its normal logic

        # create the expected current config string
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,
            opcodes.water_source,
            str(ws.ad),
            opcodes.share_with_flowstation,
            str(opcodes.false))

        # call set_monthly_watering_budget()
        ws.set_do_not_manage_by_flowstation()

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_3200_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_share_with_flow_station_to_false_fail1(self):
        """ Test set share with flow station to true Fail Case 1: Test communication error with controller"""
        # Create the test water source object
        ws = self.create_test_water_source_object()
        ws.sh = opcodes.true  # This allows the method to perform its normal logic

        # Create the expected exception string
        e_msg = "Exception occurred trying to set {0} {1}'s 'Share With FlowStation' to: '{2}' -> {3}".format(
            str(ws.identifiers[0][0]),  # {0}
            str(ws.ad),                 # {1}
            str(opcodes.false),         # {2}
            ""                          # {3}
        )

        self.mock_3200_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ws.set_do_not_manage_by_flowstation()

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    @mock.patch.object(common.object_factory, "create_moisture_empty_condition_object")
    #################################
    def test_add_moisture_empty_condition_pass1(self, mock_create_moisture_empty_condition_object):
        """ Test add moisture empty condition Pass Case 1: add moisture empty condition"""

        # create the test water source object
        ws = self.create_test_water_source_object()

        # create moisture sensor address
        moisture_sensor_address = 1

        # create a mock moisture sensor object and add it to the moisture sensor dictionary
        moisture_sensor_object = mock.MagicMock()
        ws.controller.moisture_sensors[moisture_sensor_address] = moisture_sensor_object
        ws.moisture_empty_conditions[moisture_sensor_address] = moisture_sensor_object

        ws.add_moisture_empty_condition(_moisture_sensor_address=moisture_sensor_address)

        mock_create_moisture_empty_condition_object.assert_called_once_with(controller=mock.ANY,
                                                                            empty_address=1,
                                                                            water_source_address=ws.ad,
                                                                            moisture_sensor_address=moisture_sensor_address)

    #################################
    def test_add_moisture_empty_condition_fail1(self):
        """ Test add moisture empty condition Fail Case 1: Test invalid moisture sensor address - float"""
        # Create the test water source object
        ws = self.create_test_water_source_object()

        # create moisture sensor address
        moisture_sensor_address = 1.0

        # Create the expected exception string
        e_msg = "Invalid address for Empty Condition tried to set it to: {0}. Needs to be an int" \
                .format(
                str(moisture_sensor_address),       # {0}
            )

        self.mock_3200_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            ws.add_moisture_empty_condition(_moisture_sensor_address=moisture_sensor_address)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

        #################################
    def test_add_moisture_empty_condition_fail2(self):
        """ Test add moisture empty condition Fail Case 2: Test invalid moisture sensor address - value not in
        moisture sensor keys"""
        # Create the test water source object
        ws = self.create_test_water_source_object()
        moisture_sensor_1 = mock.MagicMock()
        moisture_sensor_2 = mock.MagicMock()
        moisture_sensor_3 = mock.MagicMock()

        self.bl_3200.moisture_sensors[1] = moisture_sensor_1
        self.bl_3200.moisture_sensors[2] = moisture_sensor_2
        self.bl_3200.moisture_sensors[3] = moisture_sensor_3
        # create moisture sensor address
        moisture_sensor_address = 5

        # Create the expected exception string
        e_msg = "Invalid Moisture Sensor address. Verify address exists in object json configuration and/or in " \
                "current test. Received address: {0}, available addresses: {1}".format(
                    str(moisture_sensor_address),  # {0}
                    str(self.bl_3200.moisture_sensors.keys()),  # {1}
                )

        with self.assertRaises(Exception) as context:
            ws.add_moisture_empty_condition(_moisture_sensor_address=moisture_sensor_address)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    @mock.patch.object(common.object_factory, "create_pressure_empty_condition_object")
    #################################
    def test_add_pressure_empty_condition_pass1(self, mock_create_pressure_empty_condition_object):
        """ Test add pressure empty condition Pass Case 1: add pressure empty condition"""

        # create the test water source object
        ws = self.create_test_water_source_object()

        # create pressure sensor address
        pressure_sensor_address = 1

        # create a mock moisture sensor object and add it to the moisture sensor dictionary
        pressure_sensor_object = mock.MagicMock()
        ws.controller.pressure_sensors[pressure_sensor_address] = pressure_sensor_object
        ws.pressure_empty_conditions[pressure_sensor_address] = pressure_sensor_object

        ws.add_pressure_empty_condition(_pressure_sensor_address=pressure_sensor_address)

        mock_create_pressure_empty_condition_object.assert_called_once_with(controller=mock.ANY,
                                                                            empty_address=1,
                                                                            water_source_address=ws.ad,
                                                                            pressure_sensor_address=pressure_sensor_address)

    #################################
    def test_add_pressure_empty_condition_fail1(self):
        """ Test add pressure empty condition Fail Case 1: Test invalid pressure sensor address - float"""
        # Create the test water source object
        ws = self.create_test_water_source_object()

        # create moisture sensor address
        pressure_sensor_address = 1.0

        # Create the expected exception string
        e_msg = "Invalid address for Empty Condition tried to set it to: {0}. Needs to be an int" \
            .format(
                    str(pressure_sensor_address),       # {0}
                )

        with self.assertRaises(Exception) as context:
            ws.add_pressure_empty_condition(_pressure_sensor_address=pressure_sensor_address)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_add_pressure_empty_condition_fail2(self):
        """ Test add pressure empty condition Fail Case 2: Test invalid pressure sensor address - value not in
        moisture sensor keys"""
        # Create the test water source object
        ws = self.create_test_water_source_object()
        pressure_sensor_1 = mock.MagicMock()
        pressure_sensor_2 = mock.MagicMock()
        pressure_sensor_3 = mock.MagicMock()

        self.bl_3200.pressure_sensors[1] = pressure_sensor_1
        self.bl_3200.pressure_sensors[2] = pressure_sensor_2
        self.bl_3200.pressure_sensors[3] = pressure_sensor_3
        # create moisture sensor address
        pressure_sensor_address = 5

        # Create the expected exception string
        e_msg = "Invalid Pressure Sensor address. Verify address exists in object json configuration and/or in " \
                "current test. Received address: {0}, available addresses: {1}".format(
                    str(pressure_sensor_address),  # {0}
                    str(self.bl_3200.pressure_sensors.keys()),  # {1}
                )

        with self.assertRaises(Exception) as context:
            ws.add_pressure_empty_condition(_pressure_sensor_address=pressure_sensor_address)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    @mock.patch.object(common.object_factory, "create_switch_empty_condition_object")
    #################################
    def test_add_switch_empty_condition_pass1(self, mock_create_switch_empty_condition_object):
        """ Test add switch empty condition Pass Case 1: add switch empty condition"""

        # create the test water source object
        ws = self.create_test_water_source_object()

        # create pressure sensor address
        event_switch_address = 1

        # create a mock moisture sensor object and add it to the moisture sensor dictionary
        pressure_sensor_object = mock.MagicMock()
        ws.controller.event_switches[event_switch_address] = pressure_sensor_object
        ws.switch_empty_conditions[event_switch_address] = pressure_sensor_object

        ws.add_switch_empty_condition(_event_switch_address=event_switch_address)

        mock_create_switch_empty_condition_object.assert_called_once_with(controller=mock.ANY,
                                                                          empty_address=1,
                                                                          water_source_address=ws.ad,
                                                                          event_switch_address=event_switch_address)
    #################################
    def test_add_switch_empty_condition_fail1(self):
        """ Test add switch empty condition Fail Case 1: Test invalid event switch address - float"""
        # Create the test water source object
        ws = self.create_test_water_source_object()

        # create moisture sensor address
        pressure_sensor_address = 1.0

        # Create the expected exception string
        e_msg = "Invalid address for Empty Condition tried to set it to: {0}. Needs to be an int" \
            .format(
                str(pressure_sensor_address),       # {0}
            )

        with self.assertRaises(Exception) as context:
            ws.add_pressure_empty_condition(_pressure_sensor_address=pressure_sensor_address)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_add_switch_empty_condition_fail2(self):
        """ Test add switch empty condition Fail Case 2: Test invalid event switch address - value not in
        event switch keys"""
        # Create the test water source object
        ws = self.create_test_water_source_object()
        pressure_sensor_1 = mock.MagicMock()
        pressure_sensor_2 = mock.MagicMock()
        pressure_sensor_3 = mock.MagicMock()

        self.bl_3200.pressure_sensors[1] = pressure_sensor_1
        self.bl_3200.pressure_sensors[2] = pressure_sensor_2
        self.bl_3200.pressure_sensors[3] = pressure_sensor_3
        # create moisture sensor address
        pressure_sensor_address = 5

        # Create the expected exception string
        e_msg = "Invalid Pressure Sensor address. Verify address exists in object json configuration and/or in " \
                "current test. Received address: {0}, available addresses: {1}".format(
            str(pressure_sensor_address),  # {0}
            str(self.bl_3200.pressure_sensors.keys()),  # {1}
        )

        with self.assertRaises(Exception) as context:
            ws.add_pressure_empty_condition(_pressure_sensor_address=pressure_sensor_address)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_water_rationing_pass1(self):
        """ Verify Water Rationing Pass Case 1: Exception is not raised """
        ws = self.create_test_water_source_object()
        ws.wr = opcodes.true
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.water_rationing_enable,
            ws.wr
            )
        )

        ws.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ws.verify_water_rationing()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_water_rationing_fail1(self):
        """ Verify Water Rationing Fail Case 1: Value on controller does not match what is
        stored in ws.wr """
        ws = self.create_test_water_source_object()
        ws.wr = opcodes.false
        controller_value = opcodes.true
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.water_rationing_enable,
            controller_value
            )
        )

        ws.data = mock_data

        expected_message = "Unable verify Water Source {0}'s 'Water Rationing Enable State'. Received: {1}, Expected:" \
                           " {2}".format(
                                str(ws.ad),                 # {0}
                                str(controller_value),      # {1}
                                ws.wr                       # {2}
                            )

        try:
            ws.verify_water_rationing()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == e_msg.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_monthly_budget_pass1(self):
        """ Verify Monthly Budget Pass Case 1: Exception is not raised """
        ws = self.create_test_water_source_object()

        test_pass = False
        water_budget_controller_value = 100
        water_budget_shutdown_state_controller_value = opcodes.true

        ws.wb = water_budget_controller_value
        ws.ws = water_budget_shutdown_state_controller_value

        mock_data = status_parser.KeyValues("{0}={1},{2}={3}".format(
            opcodes.monthly_water_budget,
            water_budget_controller_value,
            opcodes.shutdown_on_over_budget,
            water_budget_shutdown_state_controller_value
            )
        )

        ws.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ws.verify_monthly_budget()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_monthly_budget_fail1(self):
        """ Verify Water Budget Fail Case 1: Value on controller does not match what is stored in ws.wb """
        ws = self.create_test_water_source_object()
        e_msg = ""

        test_pass = False
        water_budget_controller_value = 100
        water_budget_shutdown_state_controller_value = opcodes.true

        ws.wb = 200
        ws.ws = water_budget_shutdown_state_controller_value

        mock_data = status_parser.KeyValues("{0}={1},{2}={3}".format(
            opcodes.monthly_water_budget,
            water_budget_controller_value,
            opcodes.shutdown_on_over_budget,
            water_budget_shutdown_state_controller_value
            )
        )
        ws.data = mock_data

        expected_message = "Unable verify Water Source {0} 'Water Monthly Water Budget with or without " \
                           "'Shut Down Enabled'. Received: {1} for 'Water Budget', Expected: {2} Received {3} : " \
                           "for 'Shut Down Enable' Expected {4}:" \
                            .format(
                                str(ws.ad),                                         # {0}
                                str(water_budget_controller_value),                 # {1}
                                ws.wb,                                              # {2}
                                str(water_budget_shutdown_state_controller_value),  # {3}
                                ws.ws                                               # {4}
                            )

        try:
            ws.verify_monthly_budget()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == e_msg.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_monthly_budget_fail2(self):
        """ Verify Water Budget Fail Case 2: Value on controller does not match what is stored in ws.ws """
        ws = self.create_test_water_source_object()
        e_msg = ""

        test_pass = False
        water_budget_controller_value = 200
        water_budget_shutdown_state_controller_value = opcodes.true

        ws.wb = water_budget_controller_value
        ws.ws = opcodes.false

        mock_data = status_parser.KeyValues("{0}={1},{2}={3}".format(
            opcodes.monthly_water_budget,
            water_budget_controller_value,
            opcodes.shutdown_on_over_budget,
            water_budget_shutdown_state_controller_value
        )
        )
        ws.data = mock_data

        expected_message = "Unable verify Water Source {0} 'Water Monthly Water Budget with or without " \
                           "'Shut Down Enabled'. Received: {1} for 'Water Budget', Expected: {2} Received {3} : " \
                           "for 'Shut Down Enable' Expected {4}:" \
                                .format(
                                str(ws.ad),                                         # {0}
                                str(water_budget_controller_value),                 # {1}
                                ws.wb,                                              # {2}
                                str(water_budget_shutdown_state_controller_value),  # {3}
                                ws.ws                                               # {4}
                            )

        try:
            ws.verify_monthly_budget()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == e_msg.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_point_of_control_pass1(self):
        """ Verify Point of Control Pass Case 1: Exception is not raised """
        ws = self.create_test_water_source_object()

        test_pass = False
        poc_controller_value = 5

        ws.pc = poc_controller_value

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.point_of_control,
            poc_controller_value,
            )
        )

        ws.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ws.verify_point_of_control()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_point_of_control_fail1(self):
        """ Verify Water Budget Fail Case 1: Value on controller does not match what is stored in ws.pc """
        ws = self.create_test_water_source_object()
        e_msg = ""

        test_pass = False
        poc_controller_value = 5

        ws.pc = 6

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.point_of_control,
            poc_controller_value,
        )
        )

        ws.data = mock_data

        expected_message = "Unable verify Water Source {0}'s 'Point of Control Assignments' Received: '{1}' Expected: " \
                           "'{2}'" \
            .format(
                    str(ws.ad),                       # {0}
                    str(poc_controller_value),        # {1}
                    ws.pc                             # {2}
                )
        try:
            ws.verify_point_of_control()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == e_msg.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_multiple_points_of_control_pass1(self):
        """ Verify Point of Control Pass Case 1: Exception is not raised when verifying a water source with
         multiple downstream pocs assigned. """
        ws_address = 1

        # create the test water source object
        ws = self.create_test_water_source_object(_diffaddress=ws_address)

        # Create the poc address
        poc1_address = 1
        poc2_address = 2

        # Create the Point of Control
        poc1 = PointOfControl(self.bl_3200, _ad=poc1_address)
        poc2 = PointOfControl(self.bl_3200, _ad=poc2_address)

        # Add pocs to FlowStation
        self.bl_fs.points_of_control[poc1_address] = poc1
        self.bl_fs.points_of_control[poc2_address] = poc2

        # Add water source to FlowStation
        self.bl_fs.add_controller_water_source_to_flowstation(
            _controller_address=1,
            _controller_water_source_address=1,
            _flow_station_water_source_slot_number=1)

        # Add multiple poc assignments to WS for verification against.
        self.bl_fs.get_water_source(1).flow_station_points_of_control = [poc1_address, poc2_address]

        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1}={2}".format(
            opcodes.point_of_control,
            poc1_address,
            poc2_address
            )
        )

        ws.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ws.verify_flowstation_points_of_control()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_multiple_points_of_control_fail1(self):
        """ Verify Point of Control Fail Case 1: Assigned POC in object doesn't match what was
        received from the FlowStation. """

        ws_address = 1

        # create the test water source object
        ws = self.create_test_water_source_object(_diffaddress=ws_address)

        # Create the poc address
        poc1_address = 1
        poc2_address = 2

        # Create the Point of Control
        poc1 = PointOfControl(self.bl_3200, _ad=poc1_address)
        poc2 = PointOfControl(self.bl_3200, _ad=poc2_address)

        # Add pocs to FlowStation
        self.bl_fs.points_of_control[poc1_address] = poc1
        self.bl_fs.points_of_control[poc2_address] = poc2

        # Add water source to FlowStation
        self.bl_fs.add_controller_water_source_to_flowstation(
            _controller_address=1,
            _controller_water_source_address=1,
            _flow_station_water_source_slot_number=1)

        # Add multiple poc assignments to WS for verification against.
        self.bl_fs.get_water_source(1).flow_station_points_of_control = [poc1_address, poc2_address]

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.point_of_control,
            poc1_address))

        ws.data = mock_data

        expected_message = "Unable verify Water Source {0}'s 'Point(s) of Control Assignments' Received: '['{1}']' " \
                           "Expected: '['{2}', '{3}']'".format(
                               ws_address,      # {0}
                               poc1_address,    # {1}
                               poc1_address,    # {2}
                               poc2_address     # {3}
                           )

        with self.assertRaises(ValueError) as context:
            self.bl_fs.get_water_source(ws_address).verify_flowstation_points_of_control()

        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_verify_share_with_flow_station_pass1(self):
        """ Verify Share With Flow Station Pass Case 1: Exception is not raised """
        ws = self.create_test_water_source_object()

        test_pass = False
        share_with_flow_station = opcodes.true

        ws.sh = share_with_flow_station

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.share_with_flowstation,
            share_with_flow_station,
            )
        )

        ws.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                ws.verify_share_with_flow_station()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_share_with_flow_station_fail1(self):
        """ Verify Share With Flow Station Fail Case 1: Value on controller does not match what is stored in ws.sh """
        ws = self.create_test_water_source_object()
        e_msg = ""

        test_pass = False
        share_with_flow_station_controller_value = opcodes.false

        ws.sh = opcodes.true

        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.share_with_flowstation,
            share_with_flow_station_controller_value,
            )
        )

        ws.data = mock_data

        expected_message = "Unable verify Water Source {0}'s 'Share with Flow Station' Received: '{1}' " \
                           "for 'Share with FlowStation', Expected: '{2}'".format(
                                str(ws.ad),                                         # {0}
                                str(share_with_flow_station_controller_value),      # {1}
                                ws.sh                                               # {2}
                            )
        try:
            ws.verify_share_with_flow_station()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == e_msg.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Verify who i am Pass Case 1: Exception is not raised """
        # mock the methods in verify_who_i_am
        ws = self.create_test_water_source_object()
        ws.controller.controller_type = '32'
        mock_get_data = ws.get_data = mock.MagicMock()
        mock_verify_description = ws.verify_description = mock.MagicMock()
        mock_verify_enabled_state = ws.verify_enabled_state = mock.MagicMock()
        mock_verify_priority = ws.verify_priority = mock.MagicMock()
        mock_verify_monthly_budget = ws.verify_monthly_budget = mock.MagicMock()
        mock_verify_water_rationing = ws.verify_water_rationing = mock.MagicMock()

        mock_verify_point_of_control = ws.verify_point_of_control = mock.MagicMock()
        mock_verify_status = ws.verify_status = mock.MagicMock()
        mock_verify_share_with_flow_station = ws.verify_share_with_flow_station = mock.MagicMock()

        # call verify_who_i_am()
        ws.verify_who_i_am(expected_status=opcodes.okay)

        # verify the method calls in verify_who_i_am()
        self.assertEquals(mock_get_data.call_count, 1)
        self.assertEquals(mock_verify_description.call_count, 1)
        self.assertEquals(mock_verify_enabled_state.call_count, 1)
        self.assertEquals(mock_verify_priority.call_count, 1)
        self.assertEquals(mock_verify_monthly_budget.call_count, 1)
        self.assertEquals(mock_verify_water_rationing.call_count, 1)
        self.assertEquals(mock_verify_share_with_flow_station.call_count, 1)

        # verify the methods that were not called in verify_who_i_am()
        self.assertEquals(mock_verify_point_of_control.call_count, 1)
        mock_verify_status.assert_called_with(_expected_status=opcodes.okay)

    #################################
    def test_verify_who_i_am_pass2(self):
        """ Verify who i am Pass Case 2: Test when controller type is not '32' and expected_status is 'None' """
        # mock the methods in verify_who_i_am
        ws = self.create_test_water_source_object()
        ws.controller.controller_type = '10'
        mock_get_data = ws.get_data = mock.MagicMock()
        mock_verify_description = ws.verify_description = mock.MagicMock()
        mock_verify_enabled_state = ws.verify_enabled_state = mock.MagicMock()
        mock_verify_priority = ws.verify_priority = mock.MagicMock()
        mock_verify_monthly_budget = ws.verify_monthly_budget = mock.MagicMock()
        mock_verify_water_rationing = ws.verify_water_rationing = mock.MagicMock()

        mock_verify_point_of_control = ws.verify_point_of_control = mock.MagicMock()
        mock_verify_status = ws.verify_status = mock.MagicMock()

        # call verify_who_i_am()
        ws.verify_who_i_am()

        # verify the method calls in verify_who_i_am()
        self.assertEquals(mock_get_data.call_count, 1)
        self.assertEquals(mock_verify_description.call_count, 1)
        self.assertEquals(mock_verify_enabled_state.call_count, 1)
        self.assertEquals(mock_verify_priority.call_count, 1)
        self.assertEquals(mock_verify_monthly_budget.call_count, 1)
        self.assertEquals(mock_verify_water_rationing.call_count, 1)

        # verify the methods that were not called in verify_who_i_am()
        self.assertEquals(mock_verify_point_of_control.call_count, 0)
        self.assertEquals(mock_verify_status.call_count, 0)

    #################################
    def test_verify_who_i_am_pass3(self):
        """ Verify who i am Pass Case 3: Test when controller type is 'FS' and expected_status is 'None' """
        # mock the methods in verify_who_i_am
        ws = self.create_test_water_source_object()
        ws.controller.controller_type = 'FS'
        mock_get_data = ws.get_data = mock.MagicMock()
        mock_verify_description = ws.verify_description = mock.MagicMock()
        mock_verify_enabled_state = ws.verify_enabled_state = mock.MagicMock()
        mock_verify_priority = ws.verify_priority = mock.MagicMock()
        mock_verify_monthly_budget = ws.verify_monthly_budget = mock.MagicMock()
        mock_verify_water_rationing = ws.verify_water_rationing = mock.MagicMock()

        mock_verify_flowstation_points_of_control = ws.verify_flowstation_points_of_control = mock.MagicMock()
        mock_verify_status = ws.verify_status = mock.MagicMock()

        # call verify_who_i_am()
        ws.verify_who_i_am()

        # verify the method calls in verify_who_i_am()
        self.assertEquals(mock_get_data.call_count, 1)
        self.assertEquals(mock_verify_description.call_count, 1)
        self.assertEquals(mock_verify_enabled_state.call_count, 1)
        self.assertEquals(mock_verify_priority.call_count, 1)
        self.assertEquals(mock_verify_monthly_budget.call_count, 1)
        self.assertEquals(mock_verify_water_rationing.call_count, 1)
        self.assertEquals(mock_verify_flowstation_points_of_control.call_count, 1)

        # verify the methods that were not called in verify_who_i_am()
        self.assertEquals(mock_verify_status.call_count, 0)

    if __name__ == "__main__":
        unittest.main()
