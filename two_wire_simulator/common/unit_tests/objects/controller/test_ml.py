__author__ = 'Brice "Ajo Grande" Garlick'

import unittest

import mock
import serial
import status_parser
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.controllers.bl_fs import FlowStation
from common.objects.programming.ml import Mainline
from common.imports import opcodes


class TestMainLineObject(unittest.TestCase):

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        self.mock_general_method = mock.MagicMock(side_effect=None)

        # Set serial instance to mock serial
        Mainline.ser = self.mock_ser

        self.create_mock_3200()

        self.mainline_address = 1

        self.ml = self.create_test_mainline_object()

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """

        self.cleanup_mock_3200()
        
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_mainline_object(self):
        """ Creates a new Mainline object for testing purposes """
        ml = Mainline(_controller=self.bl_3200, _ad=self.mainline_address)
        ml.flowstation = self.create_mock_flowstation()

        return ml

    def create_mock_3200(self):
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)
        self.bl_3200.ser = self.mock_ser
        self.bl_3200.zones = {}
        self.bl_3200.programs = {}
        self.bl_3200.points_of_control = {}
        self.bl_3200.mainlines = {}
        self.bl_3200.moisture_sensors = {}

        zones_1 = mock.MagicMock()
        zones_2 = mock.MagicMock()

        self.bl_3200.zones[1] = zones_1
        zones_1.sn = "ZND0001"
        zones_1.ad = 1

        self.bl_3200.zones[2] = zones_2
        zones_2.sn = "ZND0002"
        zones_2.ad = 2

        program = mock.MagicMock()
        self.bl_3200.programs[1] = program
        program.ad = 1

        point_of_control = mock.MagicMock()
        self.bl_3200.points_of_control[1] = point_of_control
        point_of_control.ad = 1

        mainline = mock.MagicMock()
        self.bl_3200.mainlines[1] = mainline
        mainline.ad = 1

        moisture_sensor = mock.MagicMock()
        self.bl_3200.moisture_sensors[1] = moisture_sensor
        moisture_sensor.sn = "MSD0001"
        moisture_sensor.ad = 1

        self.bl_3200.controller_type = "32"
        
        return self.bl_3200

    def create_mock_flowstation(self):
        self.bl_fs = mock.MagicMock(spec=BaseStation3200)
        self.bl_fs.ser = self.mock_ser
        self.bl_fs.points_of_control = {}
        self.bl_fs.mainlines = {}
        self.bl_fs.water_sources = {}
        self.bl_fs.controller_type = "FS"
        return self.bl_fs

    def cleanup_mock_3200(self):
        del self.bl_3200.zones[1]
        del self.bl_3200.zones[2]
        del self.bl_3200.moisture_sensors[1]
        del self.bl_3200.points_of_control[1]
        del self.bl_3200.programs[1]
        
    #################################
    def test_set_description_pass1(self):
        """ Set Description On Controller Pass Case 1: Setting new _ds value = Test Mainline 2 """

        expected_value = "Test Mainline 2"
        self.ml.set_description(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = self.ml.ds
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_description_pass2(self):
        """ Set Description On Controller Pass Case 2: Command with correct values sent to controller """

        ds_value = str(self.ml.ds)
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.description, str(ds_value))
        self.ml.set_description(ds_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_description_fail1(self):
        """ Set Description On Controller Fail Case 1: Failed communication with controller """

        exception_msg = "Test Exception"

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_description(self.ml.ds)
        e_msg = "Exception occurred trying to set Test Mainline 1's description to: {0}".format(str(self.ml.ds))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_pipe_fill_time_pass1(self):
        """ Set Pipe Fill Time Pass Case 1: Setting new _ft value = 5 """
        # the value passed in is in minutes the actual value is no seconds
        expected_value_in_minutes = 5
        expected_value_in_seconds = 5
        self.ml.set_pipe_stabilization_time(expected_value_in_minutes)

        # value is set during this method and should equal the value passed into the method
        actual_value = self.ml.ft
        self.assertEqual(expected_value_in_seconds, actual_value)

    #################################
    def test_set_pipe_fill_time_pass2(self):
        """ Set Pipe Fill Time Pass Case 2: Command with correct values sent to controller """
        ft_value = 5
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.fill_time, str(ft_value*60))
        self.ml.set_pipe_stabilization_time(ft_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_pipe_fill_time_fail1(self):
        """ Set Pipe Fill Time Fail Case 1: Failed communication with controller """

        # A contrived Exception is thrown when communicating with the mock serial port

        self.ml.set_pipe_fill_units_to_time = mock.MagicMock()

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            self.ml.set_pipe_stabilization_time(self.ml.ft)
        # message has time in minutes
        _minutes = self.ml.ft
        e_msg = "Exception occurred trying to set Mainline 1's 'Pipe Fill Time' to: '{0}' -> ".format(str(_minutes))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_target_flow_pass1(self):
        """ Set Target Flow On Controller Pass Case 1: Setting new _fl value = 6.0 """

        expected_value = 6
        self.ml.set_target_flow(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = self.ml.fl
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_target_flow_pass2(self):
        """ Set Target Flow On Controller Pass Case 2: Command with correct values sent to controller """

        fl_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.mainline, opcodes.target_flow, str(fl_value))
        self.ml.set_target_flow(fl_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_target_flow_fail1(self):
        """ Set Target Flow On Controller Fail Case 1: Failed communication with controller """

        exception_msg = "Test Exception"

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_target_flow(6)
        e_msg = "Exception occurred trying to set ML 1's 'Target Flow' to: '{0}' -> {1}".format(str(self.ml.fl), exception_msg)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_dynamic_flow_allocation_to_true_pass1(self):
        """ Set Dynamic Flow Allocation To True Pass Case 1: Controller successfully sets value"""
        mainline = self.create_test_mainline_object()
        new_dynamic_flow_value = "TR"
        mainline.set_dynamic_flow_allocation_to_true()
        self.assertEqual(mainline.fp, new_dynamic_flow_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend="SET,ML=1,FP=TR")

    #################################
    def test_set_dynamic_flow_allocation_to_true_fail1(self):
        """ Set Dynamic Flow Allocation To True Fail Case 1: Controller returns a bad status """
        mainline = self.create_test_mainline_object()

        new_dynamic_flow_value = "TR"
        error_message = "BC command returned from mainline"
        self.mock_send_and_wait_for_reply.side_effect = Exception(error_message)
        with self.assertRaises(Exception) as context:
            mainline.set_dynamic_flow_allocation_to_true()
        expected_message = "Exception occurred trying to set Mainline {0}'s 'Dynamic Flow Allocation' to: '{1}'" \
                           " -> {2}".format(mainline.ad, new_dynamic_flow_value, error_message)
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_dynamic_flow_allocation_to_false_pass1(self):
        """ Set Dynamic Flow Allocation To True Pass Case 1: Controller successfully sets value"""
        mainline = self.create_test_mainline_object()
        new_dynamic_flow_value = "FA"
        mainline.set_dynamic_flow_allocation_to_false()
        self.assertEqual(mainline.fp, new_dynamic_flow_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend="SET,ML=1,FP=FA")

    #################################
    def test_set_dynamic_flow_allocation_to_false_fail1(self):
        """ Set Dynamic Flow Allocation To False Fail Case 1: Controller returns a bad status """
        mainline = self.create_test_mainline_object()

        new_dynamic_flow_value = "FA"
        error_message = "BC command returned from mainline"
        self.mock_send_and_wait_for_reply.side_effect = Exception(error_message)
        with self.assertRaises(Exception) as context:
            mainline.set_dynamic_flow_allocation_to_false()
        expected_message = "Exception occurred trying to set Mainline {0}'s 'Dynamic Flow Allocation' to: '{1}'" \
                           " -> {2}".format(mainline.ad, new_dynamic_flow_value, error_message)
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_message_pass1(self):
        """ Set Message On Controller Pass Case 1: Correct command is sent to controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to set because it goes outside of the method
            Return: The expected message
        Call the method"""

        expected_command = "SET,MG,{0}={1},{2}={3}".format(opcodes.mainline, str(self.ml.ad), opcodes.status_code,
                                                           opcodes.learn_flow_fail_flow_biCoders_disabled)

        with mock.patch('common.objects.base_classes.messages.message_to_set', return_value=expected_command):
            self.ml.set_message(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_message_fail1(self):
        """ Set Message On Controller Controller Fail Case 1: Exception is thrown when sending command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to set because it goes outside of the method
            Return: The expected message
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        """

        expected_command = "SET,MG,{0}={1},{2}={3}".format(opcodes.mainline, str(self.ml.ad), opcodes.status_code,
                                                           opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.mock_send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_set', return_value=expected_command):
                self.ml.set_message(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_priority_pass1(self):
        """ Test set priority Pass Case 1: set priority to first value range 1-10 and verify the send command"""
        # create the test mainline object
        ml = self.create_test_mainline_object()

        # Set the controller type
        ml.controller.controller_type = 'FS'
        ml.called_by_controller_type = 'FS'

        # Create the priority level variable
        priority_level = 1

        # create the expected current config string
        expected_command = "{0},{1}={2},{3}={4}".format(
            opcodes.set_action,
            opcodes.mainline,
            str(ml.ad),
            opcodes.priority,
            priority_level
        )

        # call add_point_of_control_to_mainline()
        ml.set_priority(_priority_for_mainline=priority_level)

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_priority_pass2(self):
        """ Test set priority Pass Case 2: set priority to last value in range 1-10 and verify the send command"""
        # create the test mainline object
        ml = self.create_test_mainline_object()

        # Set the controller type
        ml.controller.controller_type = 'FS'
        ml.called_by_controller_type = 'FS'

        # Create the priority level variable
        priority_level = 10

        # create the expected current config string
        expected_command = "{0},{1}={2},{3}={4}".format(
            opcodes.set_action,
            opcodes.mainline,
            str(ml.ad),
            opcodes.priority,
            priority_level
        )

        # call add_point_of_control_to_mainline()
        ml.set_priority(_priority_for_mainline=priority_level)

        # Verify send_and_wait_for_reply was called with the correct command
        self.mock_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_priority_fail1(self):
        """ Test set priority fail Case 1: test passing in a value other than an int """
        # Create the test mainline object
        ml = self.create_test_mainline_object()

        # Set the controller type
        ml.controller.controller_type = 'FS'
        ml.called_by_controller_type = 'FS'

        # Create the priority level variable
        priority_level = 'b'

        # Create the expected exception string
        e_msg = "Exception occurred trying to assign a priority {0} to mainline: '{1}' the value must " \
                "be an int".format(
            str(priority_level),    # {0}
            str(ml.ad)              # {1}
        )

        with self.assertRaises(Exception) as context:
            ml.set_priority(_priority_for_mainline=priority_level)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_priority_fail2(self):
        """ Test set priority fail Case 2: test passing in a value not in the range 1-10 - value below lower bound"""

        # Create the test mainline object
        ml = self.create_test_mainline_object()

        # Set the controller type
        ml.controller.controller_type = 'FS'
        ml.called_by_controller_type = 'FS'
        
        # Set the ml address
        priority_level = 0

        # Create the expected exception string
        e_msg = "Exception occurred trying to assign a priority {0} to mainline: '{1}' the value must " \
                "be between 1 and 10 ".format(
            str(priority_level),    # {0}
            str(ml.ad)              # {1}
        )

        with self.assertRaises(Exception) as context:
            ml.set_priority(_priority_for_mainline=priority_level)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_priority_fail3(self):
        """ Test set priority fail Case 2: test passing in a value not in the range 1-10 - value above upper bound"""

        # Create the test mainline object
        ml = self.create_test_mainline_object()

        # Set the controller type
        ml.controller.controller_type = 'FS'
        ml.called_by_controller_type = 'FS'

        # Set the priority level
        priority_level = 11

        # Create the expected exception string
        e_msg = "Exception occurred trying to assign a priority {0} to mainline: '{1}' the value must " \
                "be between 1 and 10 ".format(
            str(priority_level),    # {0}
            str(ml.ad)              # {1}
        )

        with self.assertRaises(Exception) as context:
            ml.set_priority(_priority_for_mainline=priority_level)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_priority_fail4(self):
        """ Test test priority fail Case 4: test communication error with controller"""
        # Create the test mainline object
        ml = self.create_test_mainline_object()
        test_exception_message = "Test Error Message"

        # Set the controller type
        ml.controller.controller_type = 'FS'
        ml.called_by_controller_type = 'FS'
        
        # Set the priority level
        priority_level = 5

        # Create the expected exception string
        e_msg = "Exception occurred trying to assign mainline {0}'s priority to:" \
                " {1} -> {2}".format(
            str(ml.ad),             # {0}
            priority_level,         # {1}
            test_exception_message  # {2}
        )
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception(test_exception_message)

        with self.assertRaises(Exception) as context:
            ml.set_priority(_priority_for_mainline=priority_level)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_priority_fail5(self):
        """ Test test priority fail Case 5: Incorrect controller type"""
        # Create the test mainline object
        ml = self.create_test_mainline_object()

        # Set the controller type
        ml.controller.controller_type = '32'

        # Set the priority level
        priority_level = 5

        # Create the expected exception string
        e_msg = "Exception occurred trying to assign a priority {0} to mainline: '{1}' the controller type must " \
                "be a flowstation".format(
            str(priority_level),    # {0}
            ml.ad                   # {1}
        )
        with self.assertRaises(Exception) as context:
            ml.set_priority(_priority_for_mainline=priority_level)

        # Verify the exception message matches the expected exception message
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_get_data_pass1(self):
        """ Get Data From Controller Pass Case 1: Correct command is sent to controller """

        expected_command = "GET,{0}={1}".format(opcodes.mainline, str(self.ml.ad))
        self.ml.get_data()
        self.mock_get_and_wait_for_reply.assert_called_with(called_by_get_data=True, tosend=expected_command)

    #################################
    def test_get_data_fail1(self):
        """ Get Data From Controller Fail Case 1: Failed communication with controller """

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_get_and_wait_for_reply.side_effect = AssertionError

        expected_command = "GET,{0}={1}".format(opcodes.mainline, str(self.ml.ad))
        with self.assertRaises(ValueError) as context:
            self.ml.get_data()
        e_msg = "Unable to get data for Test Mainline {0} using command: '{1}'. Exception raised: "\
                .format(str(self.ml.ad), expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_get_message_pass1(self):
        """ Get Message On Controller Pass Case 1: Correct command is sent to controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to get because it goes outside of the method
            Return: The expected message in a dictionary
        Call the method
        """

        expected_command = "GET,MG,{0}={1},{2}={3}".format(opcodes.mainline, str(self.ml.ad), opcodes.status_code,
                                                           opcodes.learn_flow_fail_flow_biCoders_disabled)

        with mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: expected_command}):
            self.ml.get_message(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_message_fail1(self):
        """ Get Message On Controller Fail Case 1: Exception is thrown when sending command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to get because it goes outside of the method
            Return: The expected message
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        """

        expected_command = "GET,MG,{0}={1},{2}={3}".format(opcodes.mainline, str(self.ml.ad), opcodes.status_code,
                                                           opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.mock_get_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: expected_command}):
                self.ml.get_message(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_clear_message_pass1(self):
        """ Clear Message On Controller Pass Case 1: Correct command is sent to controller
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: The expected message in a dictionary
        Call the method and verify what is was called with

        """

        expected_command = "GET,MG,{0}={1},{2}={3}".format(opcodes.mainline, str(self.ml.ad), opcodes.status_code,
                                                           opcodes.learn_flow_fail_flow_biCoders_disabled)

        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value=expected_command), \
             mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: expected_command}):
            self.ml.clear_message(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_clear_message_fail1(self):
        """ Clear Message On Controller Fail Case 1: Exception is thrown when sending command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: An Exception
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        """

        expected_msg = "Exception caught in messages.clear_message: "

        self.mock_send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_clear'):
                self.ml.clear_message(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_clear_message_fail2(self):
        """ Clear Message On Controller Fail Case 2: Exception is thrown when getting command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: The expected message
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method
        Cannot make an assertion since the exception is caught inside the method
        """

        expected_msg = "Message cleared, but was unable to verify if the message was gone."

        self.mock_get_and_wait_for_reply.side_effect = Exception

        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value=expected_msg):
            self.ml.clear_message(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

    #################################
    def test_clear_message_fail3(self):
        """ Clear Message On Controller Fail Case 3: Exception is thrown when getting command, no message was found
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: The expected message
        Mock: Messages to get because it goes outside of the method
            Return: the expected message in a dictionary
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method
        Cannot make an assertion since the exception is caught inside the method
        """

        no_message_found = "NM No Message Found"

        self.mock_get_and_wait_for_reply.side_effect = Exception(no_message_found)

        with mock.patch('common.objects.base_classes.messages.message_to_clear', return_value=no_message_found), \
             mock.patch('common.objects.base_classes.messages.message_to_get', return_value={opcodes.message: no_message_found}):
            self.ml.clear_message(_status_code=opcodes.learn_flow_fail_flow_biCoders_disabled)

    #################################
    def test_verify_description_pass1(self):
        """ Verify Description On Controller Pass Case 1: Exception is not raised """
        self.ml.ds = 'Test Mainline 2'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test Mainline 2".format(opcodes.description))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_description()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_description_fail1(self):
        """ Verify Description On Controller Fail Case 1: Value on controller does not match what is
        stored in ml.ds """
        self.ml.ds = 'Test Mainline'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test".format(opcodes.description))
        self.ml.data = mock_data

        expected_message = "Unable to verify Test Mainline 'Description'. Received: Test, Expected: Test Mainline"
        try:
            self.ml.verify_description()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_pipe_fill_time_pass1(self):
        """ Verify Pipe Fill Time On Controller Pass Case 1: Exception is not raised """
        in_minutes = self.ml.ft = 6
        received_in_seconds = 360

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.fill_time,received_in_seconds))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_pipe_fill_time()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_pipe_fill_time_fail1(self):
        """ Verify Pipe Fille Time On Controller Fail Case 1: Value on controller does not match what is
        stored in self.ml.ft """
        in_minutes = self.ml.ft = 5
        received_in_seconds = 360
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.fill_time, received_in_seconds))
        self.ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'Pipe Fill Time' value. Received: 6, Expected: 5"
        try:
            self.ml.verify_pipe_fill_time()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_target_flow_pass1(self):
        """ Verify Target Flow On Controller Pass Case 1: Exception is not raised """
        self.ml.fl = 6
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.target_flow))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_target_flow()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_target_flow_fail1(self):
        """ Verify Target Flow On Controller Fail Case 1: Value on controller does not match what is
        stored in ml.fl """
        self.ml.fl = 5.0
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.target_flow))
        self.ml.data = mock_data

        expected_message = "Unable to verify ML 1's 'Target Flow'. Received: 6.0, Expected: 5.0"
        try:
            self.ml.verify_target_flow()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_limit_zones_by_flow_state_pass1(self):
        """ Verify Limit Zones By Flow State On Controller Pass Case 1: Exception is not raised """
        self.ml.lc = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=TR".format(opcodes.limit_zones_by_flow))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_limit_zones_by_flow_state()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_limit_zones_by_flow_state_fail1(self):
        """ Verify Limit Zones By Flow State On Controller Fail Case 1: Value on controller does not match what is
        stored in ml.lc """
        self.ml.lc = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=TR".format(opcodes.limit_zones_by_flow))
        self.ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'Limit Zones By Flow State' value. Received: TR, Expected: Tr"
        try:
            self.ml.verify_limit_zones_by_flow_state()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_high_variance_limit_tier_one_pass(self):
        """ Verify High Variance Limit Tier One Pass Case: Exception is not raised """
        self.ml.ah = 6
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.high_variance_limit_tier_one))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_high_variance_limit_tier_one()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_high_variance_limit_tier_one_fail(self):
        """ Verify High Variance Limit Tier One Fail Case: Value on controller does not match what is
        stored in ml.ah """
        self.ml.ah = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.high_variance_limit_tier_one))
        self.ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'High Variance Limit Tier One' value. Received: 6.0, Expected: 5"
        try:
            self.ml.verify_high_variance_limit_tier_one()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_high_variance_limit_tier_two_pass(self):
        """ Verify High Variance Limit Tier Two Pass Case: Exception is not raised """
        self.ml.bh = 6
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.high_variance_limit_tier_two))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_high_variance_limit_tier_two()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_high_variance_limit_tier_two_fail(self):
        """ Verify High Variance Limit Tier Two Fail Case: Value on controller does not match what is
        stored in ml.bh """
        self.ml.bh = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.high_variance_limit_tier_two))
        self.ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'High Variance Limit Tier Two' value. Received: 6.0, Expected: 5"
        try:
            self.ml.verify_high_variance_limit_tier_two()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_high_variance_limit_tier_three_pass(self):
        """ Verify High Variance Limit Tier Three Pass Case: Exception is not raised """
        self.ml.ch = 6
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.high_variance_limit_tier_three))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_high_variance_limit_tier_three()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_high_variance_limit_tier_three_fail(self):
        """ Verify High Variance Limit Tier Three Fail Case: Value on controller does not match what is
        stored in ml.ch """
        self.ml.ch = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.high_variance_limit_tier_three))
        self.ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'High Variance Limit Tier Three' value. Received: 6.0, Expected: 5"
        try:
            self.ml.verify_high_variance_limit_tier_three()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_high_variance_limit_tier_four_pass(self):
        """ Verify High Variance Limit Tier Four Pass Case: Exception is not raised """
        self.ml.dh = 6
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.high_variance_limit_tier_four))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_high_variance_limit_tier_four()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_high_variance_limit_tier_four_fail(self):
        """ Verify High Variance Limit Tier Four Fail Case: Value on controller does not match what is
        stored in ml.dh """
        self.ml.dh = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.high_variance_limit_tier_four))
        self.ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'High Variance Limit Tier Four' value. Received: 6.0, Expected: 5"
        try:
            self.ml.verify_high_variance_limit_tier_four()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_high_variance_shut_down_pass1(self):
        """ Verify High Variance Shutdown On Controller Pass Case 1: Exception is not raised """
        self.ml.zh = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=TR".format(opcodes.zone_high_variance_detection_shutdown))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_standard_high_variance_detection_shutdown()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_high_variance_shut_down_fail1(self):
        """ Verify High Variance Shutdown On Controller Fail Case 1: Value on controller does not match what is
        stored in ml.hs """
        self.ml.zh = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=TR".format(opcodes.zone_high_variance_detection_shutdown))
        self.ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'High Variance Detection Shutdown' value. Received: TR, Expected: Tr"
        try:
            self.ml.verify_standard_high_variance_detection_shutdown()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_low_variance_limit_tier_one_happy_path(self):
        """ Verify Low Variance Limit Tier One: Exception is not raised """
        self.ml.al = 6
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.low_variance_limit_tier_one))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_low_variance_limit_tier_one()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_low_variance_limit_tier_one_fail(self):
        """ Verify Low Variance Limit Tier One failure case: Value on controller does not match what is
        stored in ml.al """
        self.ml.al = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.low_variance_limit_tier_one))
        self.ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'Low Variance Limit Tier One' value. Received: 6.0, Expected: 5"
        try:
            self.ml.verify_low_variance_limit_tier_one()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_low_variance_limit_tier_two_happy_path(self):
        """ Verify Low Variance Limit Tier Two: Exception is not raised """
        self.ml.bl = 6
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.low_variance_limit_tier_two))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_low_variance_limit_tier_two()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_low_variance_limit_tier_two_fail(self):
        """ Verify Low Variance Limit Tier One failure case: Value on controller does not match what is
        stored in ml.bl """
        self.ml.bl = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.low_variance_limit_tier_two))
        self.ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'Low Variance Limit Tier Two' value. Received: 6.0, Expected: 5"
        try:
            self.ml.verify_low_variance_limit_tier_two()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))
        
    #################################
    def test_verify_low_variance_limit_tier_three_happy_path(self):
        """ Verify Low Variance Limit Tier Three: Exception is not raised """
        self.ml.cl = 6
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.low_variance_limit_tier_three))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_low_variance_limit_tier_three()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_low_variance_limit_tier_three_fail(self):
        """ Verify Low Variance Limit Tier Three failure case: Value on controller does not match what is
        stored in ml.cl """
        self.ml.cl = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.low_variance_limit_tier_three))
        self.ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'Low Variance Limit Tier Three' value. Received: 6.0, Expected: 5"
        try:
            self.ml.verify_low_variance_limit_tier_three()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_low_variance_limit_tier_four_happy_path(self):
        """ Verify Low Variance Limit Tier Four: Exception is not raised """
        self.ml.dl = 6
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.low_variance_limit_tier_four))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_low_variance_limit_tier_four()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_low_variance_limit_tier_four_fail(self):
        """ Verify Low Variance Limit Tier One failure case: Value on controller does not match what is
        stored in ml.dl """
        self.ml.dl = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=6".format(opcodes.low_variance_limit_tier_four))
        self.ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'Low Variance Limit Tier Four' value. Received: 6.0, Expected: 5"
        try:
            self.ml.verify_low_variance_limit_tier_four()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_zone_low_variance_shut_down_happy_path(self):
        """ verify_zone_low_variance_shut_down happy path: Exception is not raised """
        self.ml.zl = 'TR'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=TR".format(opcodes.zone_low_variance_detection_shutdown))
        self.ml.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_standard_low_variance_detection_shutdown()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_low_variance_shut_down_exception(self):
        """ verify_zone_low_variance_shut_down: Value on controller does not match what is
        stored in ml.ls """
        self.ml.zl = 'Tr'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=TR".format(opcodes.zone_low_variance_detection_shutdown))
        self.ml.data = mock_data

        expected_message = "Unable to verify Mainline 1's 'Low Variance Detection Shutdown' value. Received: TR, Expected: Tr"
        try:
            self.ml.verify_standard_low_variance_detection_shutdown()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_pipe_fill_time_happy_path(self):
        """ verify_pipe_fill_time happy path: Exception is not raised """
        test_pass = False
        expected_in_minutes = 3
        expected_in_seconds = 180

        self.ml.ft = expected_in_minutes

        # the controller returns seconds but ml.ft is is in minutes
        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(expected_in_seconds))

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_pipe_fill_time()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_pipe_fill_time_exception(self):
        """ verify_pipe_fill_time: Value on controller does not match what is
        stored in ml.ft """
        test_pass = False
        expected_in_minutes = 3
        expected_in_seconds = 180
        unexpected_in_minutes = 5
        unexpected_in_seconds = 300
        e_msg = ""

        self.ml.ft = expected_in_minutes

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(unexpected_in_seconds))

        expected_message = "Unable to verify Mainline {0}'s 'Pipe Fill Time' value. Received: {1}, Expected: {2}".format(str(self.mainline_address), str(unexpected_in_minutes), str(expected_in_minutes))
        try:
            self.ml.verify_pipe_fill_time()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_share_with_flowstation_happy_path(self):
        """ verify_share_with_flowstation happy path: Exception is not raised """
        test_pass = False
        expected = 'TR'

        self.ml.sh = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(expected))

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_share_with_flowstation()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_share_with_flowstation_exception(self):
        """ verify_share_with_flowstation: Value on controller does not match what is
        stored in ml.sh """
        test_pass = False
        expected = 'TR'
        unexpected = 'FA'
        e_msg = ""

        self.ml.sh = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(unexpected))

        expected_message = "Unable to verify Mainline {0}'s 'Share With Flow Station' value. Received: {1}, Expected: {2}".format(str(self.mainline_address), str(unexpected), str(expected))
        try:
            self.ml.verify_share_with_flowstation()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))
        
    #################################
    def test_verify_standard_variance_shutdown_enabled_happy_path(self):
        """ verify_standard_variance_shutdown_enabled happy path: Exception is not raised """
        test_pass = False
        expected = 'TR'

        self.ml.fw = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(expected))

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_standard_variance_shutdown_enabled()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_standard_variance_shutdown_enabled_exception(self):
        """ verify_standard_variance_shutdown_enabled error handling: Value on controller does not match what is
         stored in ml.fw """
        test_pass = False
        expected = 'TR'
        unexpected = 'FA'
        e_msg = ""

        self.ml.fw = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(unexpected))

        expected_message = "Unable to verify Mainline 1's 'Standard Variance Shutdown Enabled' value. Received: {0}, Expected: {1}".format(str(unexpected), str(expected))
        try:
            self.ml.verify_standard_variance_shutdown_enabled()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_dynamic_flow_allocation_pass1(self):
        """ Verify Dynamic Flow Allocation Pass Case 1: Mainline successfully verifies value"""
        mainline = self.create_test_mainline_object()
        mock_data = status_parser.KeyValues("FP=FA")
        mainline.data = mock_data
        mainline.verify_dynamic_flow_allocation()

    #################################
    def test_verify_dynamic_flow_allocation_fail1(self):
        """ Verify Dynamic Flow Allocation Fail Case 1: Mainline verifies value is incorrect"""
        mainline = self.create_test_mainline_object()
        mainline.fp = "TR"      # Value in the test engine object
        returned_fp = "FA"      # Value returned by the controller
        mock_data = status_parser.KeyValues("FP={0}".format(returned_fp))
        mainline.data = mock_data
        with self.assertRaises(ValueError) as context:
            mainline.verify_dynamic_flow_allocation()
        expected_message = "Unable to verify Mainline {0}'s 'Dynamic Flow Allocation' value. Received: {1}, " \
                           "Expected: {2}".format(mainline.ad, returned_fp, mainline.fp)
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_verify_message_pass1(self):
        """
        verify_message pass case 1
        Create test mainline object
        Mock: get_message because it goes outside of the method
        Store: Expected values each build message string, so the values can be mocked
        Return: None so it does not fail
        Mock: mg.get_value_string_by_key because it goes outside of the method
        Return: Three separate strings, for expected_message_text, expected_message_id, and expected_date_time_from_cn
        Mock: build_message_string (note the [opcodes.message_id])
        Return: Four different strings, different than the ones used for expected_message_id, controller_date_time
        expected_date_time, and expected_message_text.
        Run: the test, and pass a status code in
        Compare: Actual strings from the build_message_string methods to the first expected string. All three strings
        are the same, so just comparing the first is okay.
        """

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Mock get message on cn
        mock_get_message = mock.MagicMock(side_effect=['thing'])
        self.ml.get_message = mock_get_message

        # Mock get_value_string_by_key
        mock_mg = mock.MagicMock()
        mock_mg.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, message_date])
        self.ml.get_message = mock.MagicMock(return_value=mock_mg)

        # Mock build_message_string
        # mock_build_message_string = mock.MagicMock(return_value='diffstring')
        # ml.build_message_string = mock_build_message_string
        # print "build message string is" + ml.build_message_string
        self.ml.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Run test
        self.ml.verify_message(_status_code='OK')

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual({'DT': '5/20/15 3:20:11', 'ID': 'blah2', 'TX': 'blah'}, self.ml.build_message_string)

    #################################
    def test_verify_message_fail1(self):
        """ Verify message on controller Fail Case 1: Created ID doesn't match controller ID, raise ValueError
        Create test mainline object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id (which will be wrong so an
            ValueError is thrown), and expected_date_time_from_cn
        Manually fill up the build_message_string variables in the mainline object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message ID (This is so we can purposefully force the ValueError)
        wrong_message_id = 'blah3'

        # Mock get_value_string_by_key
        mock_mg = mock.MagicMock()
        mock_mg.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, wrong_message_id, message_date])
        self.ml.get_message = mock.MagicMock(return_value=mock_mg)

        self.ml.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "Created ID message did not match the ID received from the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           self.ml.build_message_string[opcodes.message_id],    # {0} The ID that was built
                           wrong_message_id                                # {1} The ID returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.verify_message(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_fail2(self):
        """ Verify message on controller Fail Case 2: Created DT doesn't match controller DT, raise ValueError
        Create test mainline object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the mainline object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError)
        wrong_message_dt = '5/11/11 3:59:59'

        # Mock get_value_string_by_key
        mock_mg = mock.MagicMock()
        mock_mg.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, wrong_message_dt])
        self.ml.get_message = mock.MagicMock(return_value=mock_mg)

        self.ml.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "The date and time of the message didn't match the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           self.ml.build_message_string[opcodes.date_time],  # {0} The DT that was built
                           wrong_message_dt                             # {1} The DT returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.verify_message(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_fail3(self):
        """ Verify message on controller Fail Case 3: Created DT doesn't match controller DT, but are within 60 minutes, raise ValueError
        Create test mainline object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the mainline object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError, but still within 60 minutes)
        wrong_message_dt = '5/20/15 3:59:11'

        # Mock get_value_string_by_key
        mock_mg = mock.MagicMock()
        mock_mg.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, wrong_message_dt])
        self.ml.get_message = mock.MagicMock(return_value=mock_mg)

        self.ml.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "The date and time of the message didn't match the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           self.ml.build_message_string[opcodes.date_time],  # {0} The DT that was built
                           wrong_message_dt                             # {1} The DT returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.verify_message(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_fail4(self):
        """ Verify message on controller Fail Case 4: Created TX doesn't match controller TX, raise ValueError
        Create test mainline object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the mainline object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError, but still within 60 minutes)
        wrong_message_tx = 'blah3'

        # Mock get_value_string_by_key
        mock_mg = mock.MagicMock()
        mock_mg.get_value_string_by_key = mock.MagicMock(side_effect=[wrong_message_tx, message_id, message_date])
        self.ml.get_message = mock.MagicMock(return_value=mock_mg)

        self.ml.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "Created TX message did not match the TX received from the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           self.ml.build_message_string[opcodes.message_text],  # {0} The TX that was built
                           wrong_message_tx                                 # {1} The TX returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.verify_message(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_add_zone_to_mainline_happy_path(self):
        """ Test that add_zone_to_mainline happy path sends the correct test engine command
        """
        zone_address = 1

        expected_command = "SET,{0}={1},{2}={3}".format(opcodes.zone, zone_address, opcodes.mainline, str(self.mainline_address))

        self.ml.add_zone_to_mainline(_zone_address=zone_address)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_add_zone_to_mainline_wrong_argument_type(self):
        """ Test that add_zone_to_mainline will detect a non-integer zone address
        """

        zone_address = 10.0

        # Create the error message that will be compared to the actual error message
        expected_msg = "Exception occurred trying to add zone to mainline: '1' the zone address '10.0' must be an int"

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.add_zone_to_mainline(_zone_address=zone_address)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_add_zone_to_mainline_invalid_zone_id(self):
        """ Test that add_zone_to_mainline will detect assigning a zone that doesn't exist on the controller
        """
        zone_address = 1000

        # Create the error message that will be compared to the actual error message
        expected_msg = "Attempting to assign main line 1 to zone 1000. The zone does not exist on this controller."

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.add_zone_to_mainline(_zone_address=zone_address)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_add_zone_to_mainline_send_exception(self):
        """ Test that add_zone_to_mainline throws exception when sending to controller fails
        """
        zone_address = 1

        exception_msg = "Test Exception"

        expected_msg = "Exception occurred trying to add zone {0} to mainline: {1} -> {2}".format(str(zone_address), str(self.mainline_address), exception_msg)

        self.ml.add_zone_to_mainline(_zone_address=zone_address)

        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.add_zone_to_mainline(_zone_address=zone_address)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_add_mainline_to_mainline_happy_path(self):
        """ Test that add_mainline_to_mainline happy path sends the correct test engine command
        """
        ml = self.create_test_mainline_object()
        ml.called_by_controller_type = 'FS'

        ml_to_add = 2
        
        mock_ml_2 = mock.MagicMock(spec=Mainline)
        mock_ml_2.ad = ml_to_add
        mock_ml_2.flow_station_slot_number = ml_to_add
        ml.flowstation.mainlines[ml_to_add] = mock_ml_2
        
        expected_command = "SET,{0}={1},{2}={3}".format(opcodes.mainline, ml.ad, opcodes.mainline, ml_to_add)

        ml.add_mainline_to_mainline(_mainline_address=ml_to_add)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_add_mainline_to_mainline_wrong_argument_type(self):
        """ Test that add_mainline_to_mainline will detect a non-integer mainline address
        """
        mainline_address = 10.0
        
        ml = self.create_test_mainline_object()
        ml.called_by_controller_type = 'FS'

        # Create the error message that will be compared to the actual error message
        expected_msg = "Exception occurred trying to assign mainline {0} to mainline: '{1}' the value must be an " \
                       "int".format(mainline_address, ml.ad)

        # Run method that will tested
        with self.assertRaises(Exception) as context:
            ml.add_mainline_to_mainline(_mainline_address=mainline_address)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_add_mainline_to_mainline_invalid_mainline_id(self):
        """ Test that add_mainline_to_mainline will detect assigning a mainline that doesn't exist on the controller
        """
        mainline_address = 1000

        ml = self.create_test_mainline_object()
        ml.called_by_controller_type = 'FS'

        # Create the error message that will be compared to the actual error message
        expected_msg = "Attempting to assign mainline {0} to mainline {1}. The mainline does not exist on this " \
                       "FlowStation.".format(mainline_address, ml.ad)

        # Run method that will tested
        with self.assertRaises(Exception) as context:
            ml.add_mainline_to_mainline(_mainline_address=mainline_address)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_add_mainline_to_mainline_send_exception(self):
        """ Test that add_mainline_to_mainline throws exception when sending to controller fails
        """
        mainline_address = 2

        ml = self.create_test_mainline_object()
        ml.called_by_controller_type = 'FS'

        mock_ml_2 = mock.MagicMock(spec=Mainline)
        mock_ml_2.ad = mainline_address
        mock_ml_2.flow_station_slot_number = mainline_address
        ml.flowstation.mainlines[mainline_address] = mock_ml_2

        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to assign mainline {0} to " \
                       "mainline {1} -> {2}".format(mainline_address, ml.ad, exception_msg)

        ml.add_mainline_to_mainline(_mainline_address=mainline_address)

        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            ml.add_mainline_to_mainline(_mainline_address=mainline_address)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_convert_fill_time_to_seconds_happy_path(self):
        minutes = 25
        seconds = minutes * 60

        result = self.ml.convert_fill_time_minutes_to_seconds(minutes)

        self.assertEqual(seconds, result)

    #################################
    def test_build_obj_configuration_for_send_happy_path(self):
        expected_config = "SET,ML=1,DS=Test Mainline 1,EN=TR,FL=0,LC=FA,SH=FA,FW=FA,FV=0,UN=TM,FT=120"
        config = self.ml.build_obj_configuration_for_send()
        self.assertEqual(expected_config, config)

    #################################
    @mock.patch.object(Mainline,'build_obj_configuration_for_send')
    def test_send_programming_happy_path(self,
                                         build_obj_configuration_for_send_mock):
        """
        Test that send_programming sends a command generated by build_obj_configuration_for_send
        """
        expected_command = "SET,ML=1,DS=Test Mainline 1,EN=TR,FL=0,LC=FA,SH=FA,FW=FA,FV=0,UN=TM,FT=120"
        build_obj_configuration_for_send_mock.return_value = expected_command
        self.ml.send_programming()
        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    #################################
    @mock.patch.object(Mainline,'build_obj_configuration_for_send')
    def test_send_programming_exception(self,
                                        build_obj_configuration_for_send_mock):
        """
        Test handling of an exception in send_and_wait_for_reply in send_programming
        """
        expected_command = "SET,ML=1,DS=Test Mainline 1,EN=TR,FL=0,LC=FA,SH=FA,FW=FA,FV=0,UN=TM,FT=120"
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Default Values' to: '{1}' -> {2}".format(str(self.mainline_address), expected_command, exception_msg)
        build_obj_configuration_for_send_mock.return_value = expected_command
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.send_programming()

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_limit_zones_by_flow_to_true_happy_path(self):
        """
        Test that set_limit_zone_by_flow_to_true sends the expected command
        """
        expected_command = "SET,ML=1,LC=TR"
        self.ml.set_limit_zones_by_flow_to_true()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_limit_zones_by_flow_to_true_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_limit_zone_by_flow_to_true
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Limit Zones By Flow' to: 'TR' -> {1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_limit_zones_by_flow_to_true()

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_limit_zones_by_flow_to_false_happy_path(self):
        """
        Test that set_limit_zone_by_flow_to_false sends the expected command
        """
        expected_command = "SET,ML=1,LC=FA"
        self.ml.set_limit_zones_by_flow_to_false()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_limit_zones_by_flow_to_false_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_limit_zone_by_flow_to_false
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Limit Zones By Flow' to: 'FA' -> {1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_limit_zones_by_flow_to_false()

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_standard_variance_shutdown_enabled_to_true_happy_path(self):
        """
        Test that set_standard_variance_shutdown_enabled_to_true sends the expected command
        """
        expected_command = "SET,ML=1,FW=TR"
        self.ml.set_standard_variance_shutdown_enabled_to_true()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_standard_variance_shutdown_enabled_to_true_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_standard_variance_shutdown_enabled_to_true
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Standard Variance Shutdown Enabled' to: 'TR' -> {1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_standard_variance_shutdown_enabled_to_true()

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_standard_variance_shutdown_enabled_to_false_happy_path(self):
        """
        Test that set_standard_variance_shutdown_enabled_to_false sends the expected command
        """
        expected_command = "SET,ML=1,FW=FA"
        self.ml.set_standard_variance_shutdown_enabled_to_false()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_standard_variance_shutdown_enabled_to_false_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_standard_variance_shutdown_enabled_to_false
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Standard Variance Shutdown Enabled' to: 'FA' -> {1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_standard_variance_shutdown_enabled_to_false()

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_standard_variance_limit_happy_path(self):
        """
        Test that set_standard_variance_limit sends the expected command
        """
        limit = 10.0
        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=False)
        self.ml.set_standard_variance_shutdown_enabled_to_true = mock.MagicMock
        expected_command = "SET,ML=1,FV={0}".format(str(limit))
        self.ml.set_standard_variance_limit_with_shutdown(_percentage=limit)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_standard_variance_limit_invalid_argument(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_standard_variance_limit
        """
        limit = '10'
        exception_msg = "Test Exception"
        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=False)
        self.ml.set_standard_variance_shutdown_enabled_to_true = mock.MagicMock
        expected_msg = "Invalid 'Standard Variance Limit' state entered for Mainline {0}. Expects float or int. " \
                       "Received type: <type 'str'>".format(str(self.mainline_address))
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(ValueError) as context:
            self.ml.set_standard_variance_limit_with_shutdown(_percentage=limit)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_standard_variance_limit_send_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_standard_variance_limit
        """
        limit = 10.0
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Standard Variance Limit' to: '{1}' -> {2}".format(str(self.mainline_address), str(limit), exception_msg)
        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=False)
        self.ml.set_standard_variance_shutdown_enabled_to_true = mock.MagicMock
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_standard_variance_limit_with_shutdown(_percentage=limit)

        self.assertEqual(expected_msg, context.exception.message)
        
    #################################
    def test_set_mainline_variance_limit_happy_path(self):
        """
        Test that set_mainline_variance_limit sends the expected command
        """
        limit = 10.0
        expected_command = "SET,ML=1,MF={0}".format(str(limit))
        self.ml.set_mainline_variance_limit(_new_limit=limit)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_mainline_variance_limit_invalid_argument(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_mainline_variance_limit
        """
        limit = 10
        exception_msg = "Test Exception"
        expected_msg = "Invalid 'Mainline Variance Limit' state entered for Mainline {0}. " \
                       "Expects float. Received type: <type 'int'>".format(str(self.mainline_address))
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(ValueError) as context:
            self.ml.set_mainline_variance_limit(_new_limit=limit)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_mainline_variance_limit_send_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_mainline_variance_limit
        """
        limit = 10.0
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Mainline Variance Limit' to: '{1}' -> {2}".format(str(self.mainline_address), str(limit), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_mainline_variance_limit(_new_limit=limit)

        self.assertEqual(expected_msg, context.exception.message)
        
    #################################
    def test_set_pipe_fill_units_to_time_happy_path(self):
        """
        Test that set_pipe_fill_units_to_time sends the expected command
        """
        expected_command = "SET,ML=1,UT=TR"
        self.ml.set_pipe_fill_units_to_time()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_pipe_fill_units_to_time_send_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_pipe_fill_units_to_time
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Use Time For Stable Flow' to: 'TR' -> {1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_pipe_fill_units_to_time()

        self.assertEqual(expected_msg, context.exception.message)
        
    #################################
    def test_set_pipe_fill_units_to_pressure_happy_path(self):
        """
        Test that set_pipe_fill_units_to_pressure sends the expected command
        """
        expected_command = "SET,ML=1,UP=TR"
        self.ml.set_pipe_fill_units_to_pressure()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_pipe_fill_units_to_pressure_send_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_pipe_fill_units_to_pressure
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Use Pressure For Stable Flow' to: 'TR' -> {1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_pipe_fill_units_to_pressure()

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_use_time_for_stable_flow_to_false_happy_path(self):
        """
        Test that set_use_time_for_stable_flow_to_false sends the expected command
        """
        expected_command = "SET,ML=1,UT=FA"
        self.ml.set_use_time_for_stable_flow_to_false()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_use_time_for_stable_flow_to_false_send_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_use_time_for_stable_flow_to_false
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Use Time For Stable Flow' to: 'FA' -> {1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_use_time_for_stable_flow_to_false()

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_use_pressure_for_stable_flow_to_false_happy_path(self):
        """
        Test that set_use_pressure_for_stable_flow_to_false sends the expected command
        """
        expected_command = "SET,ML=1,UP=FA"
        self.ml.set_use_pressure_for_stable_flow_to_false()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_use_pressure_for_stable_flow_to_false_send_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_use_pressure_for_stable_flow_to_false
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Use Pressure For Stable Flow' to: 'FA' -> " \
                       "{1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_use_pressure_for_stable_flow_to_false()

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_delay_units_to_time_happy_path(self):
        """
        Test that set_delay_units_to_time sends the expected command
        """
        expected_command = "SET,ML=1,UN=TM"
        self.ml.set_delay_units_to_time()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_delay_units_to_time_send_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_delay_units_to_time
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Delay Units' to: 'TM' -> " \
                       "{1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_delay_units_to_time()

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_delay_units_to_pressure_happy_path(self):
        """
        Test that set_delay_units_to_pressure sends the expected command
        """
        expected_command = "SET,ML=1,UN=PS"
        self.ml.set_delay_units_to_pressure()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_delay_units_to_pressure_send_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_delay_units_to_pressure
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Delay Units' to: 'PS' -> " \
                       "{1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_delay_units_to_pressure()

        self.assertEqual(expected_msg, context.exception.message)
        
    #################################
    def test_set_delay_first_zone_happy_path(self):
        """
        Test that set_delay_first_zone sends the expected command
        """
        delay_as_minutes = 10
        delay_as_seconds = delay_as_minutes * 60
        expected_command = "SET,ML=1,TM={0}".format(str(delay_as_seconds))
        self.ml.set_time_delay_before_first_zone(_minutes=delay_as_minutes)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_delay_first_zone_invalid_argument(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_delay_first_zone
        """
        delay = 10.0
        exception_msg = "Test Exception"
        expected_msg = "Invalid 'Delay First Zone' state entered for Mainline {0}. Expects int. Received type: <type 'float'>".format(str(self.mainline_address))
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)
        # Mock out the method that is called inside the method we want to test
        self.ml.set_delay_units_to_time = mock.MagicMock()

        with self.assertRaises(ValueError) as context:
            self.ml.set_time_delay_before_first_zone(_minutes=delay)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_delay_first_zone_send_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_delay_first_zone
        """
        delay = 10
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Delay First Zone' to: '{1}' -> {2}".format(str(self.mainline_address), str(delay), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)
        # Mock out the method that is called inside the method we want to test
        self.ml.set_delay_units_to_time = mock.MagicMock()

        with self.assertRaises(Exception) as context:
            self.ml.set_time_delay_before_first_zone(_minutes=delay)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_delay_for_next_zone_happy_path(self):
        """
        Test that set_delay_next_zone sends the expected command
        """
        delay_as_minutes = 10
        delay_as_seconds = delay_as_minutes * 60
        expected_command = "SET,ML=1,TZ={0}".format(str(delay_as_seconds))
        self.ml.set_time_delay_between_zone(_minutes=delay_as_minutes)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_delay_for_next_zone_invalid_argument(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_delay_next_zone
        """
        delay = 10.0
        exception_msg = "Test Exception"
        expected_msg = "Invalid 'Delay For Next Zone' state entered for Mainline {0}. Expects int. Received type: <type 'float'>".format(str(self.mainline_address))
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)
        # Mock out the method that is called inside the method we want to test
        self.ml.set_delay_units_to_time = mock.MagicMock()

        with self.assertRaises(ValueError) as context:
            self.ml.set_time_delay_between_zone(_minutes=delay)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_delay_for_next_zone_send_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_delay_next_zone
        """
        delay = 10
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Delay For Next Zone' to: '{1}' -> {2}".format(str(self.mainline_address), str(delay), exception_msg)
        # Mock out the method that is called inside the method we want to test
        self.ml.set_delay_units_to_time = mock.MagicMock()
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_time_delay_between_zone(_minutes=delay)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_delay_after_zone_happy_path(self):
        """
        Test that set_delay_after_zone sends the expected command
        """
        delay_as_minutes = 10
        delay_as_seconds = delay_as_minutes * 60
        expected_command = "SET,ML=1,TL={0}".format(str(delay_as_seconds))
        self.ml.set_time_delay_after_zone(_minutes=delay_as_minutes)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_delay_after_zone_invalid_argument(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_delay_after_zone
        """
        delay = 10.0
        exception_msg = "Test Exception"
        expected_msg = "Invalid 'Delay After Zone' state entered for Mainline {0}. Expects int. Received type: <type 'float'>".format(str(self.mainline_address))
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)
        # Mock out the method that is called inside the method we want to test
        self.ml.set_delay_units_to_time = mock.MagicMock()

        with self.assertRaises(ValueError) as context:
            self.ml.set_time_delay_after_zone(_minutes=delay)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_delay_after_zone_send_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_delay_after_zone
        """
        delay = 10
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Delay After Zone' to: '{1}' -> {2}".format(str(self.mainline_address), str(delay), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)
        # Mock out the method that is called inside the method we want to test
        self.ml.set_delay_units_to_time = mock.MagicMock()

        with self.assertRaises(Exception) as context:
            self.ml.set_time_delay_after_zone(_minutes=delay)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_number_zones_to_delay_happy_path(self):
        """
        Test that set_number_zones_to_delay sends the expected command
        """
        zones = 3
        expected_command = "SET,ML=1,ZC={0}".format(str(zones))
        self.ml.set_number_zones_to_delay(_new_number=zones)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_number_zones_to_delay_invalid_argument(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_number_zones_to_delay
        """
        zones = 3.0
        exception_msg = "Test Exception"
        expected_msg = "Invalid 'Number Zones to Delay' state entered for Mainline {0}. Expects int. Received type: <type 'float'>".format(str(self.mainline_address))
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(ValueError) as context:
            self.ml.set_number_zones_to_delay(_new_number=zones)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_number_zones_to_delay_send_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_number_zones_to_delay
        """
        zones = 3
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Number Zones to Delay' to: '{1}' -> {2}".format(str(self.mainline_address), str(zones), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_number_zones_to_delay(_new_number=zones)

        self.assertEqual(expected_msg, context.exception.message)
        
    #################################
    def test_set_mainline_variance_shutdown_enabled_to_true_happy_path(self):
        """
        Test that set_mainline_variance_shutdown_enabled_to_true sends the expected command
        """
        expected_command = "SET,ML=1,MW=TR"
        self.ml.set_mainline_variance_shutdown_enabled_to_true()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_mainline_variance_shutdown_enabled_to_true_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_mainline_variance_shutdown_enabled_to_true
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Mainline Variance Shutdown Enabled' to: 'TR' -> {1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_mainline_variance_shutdown_enabled_to_true()

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_mainline_variance_shutdown_enabled_to_false_happy_path(self):
        """
        Test that set_mainline_variance_shutdown_enabled_to_false sends the expected command
        """
        expected_command = "SET,ML=1,MW=FA"
        self.ml.set_mainline_variance_shutdown_enabled_to_false()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_mainline_variance_shutdown_enabled_to_false_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_mainline_variance_shutdown_enabled_to_false
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Mainline Variance Shutdown Enabled' to: 'FA' -> {1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_mainline_variance_shutdown_enabled_to_false()

        self.assertEqual(expected_msg, context.exception.message)
        
    #################################
    def test_set_use_advanced_flow_to_true_happy_path(self):
        """
        Test that set_use_advanced_flow_to_true sends the expected command
        """
        expected_command = "SET,ML=1,AF=TR"
        self.ml.set_use_advanced_flow_to_true()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_use_advanced_flow_to_true_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_use_advanced_flow_to_true
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Use Advanced Flow' to: 'TR' -> {1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_use_advanced_flow_to_true()

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_use_advanced_flow_to_false_happy_path(self):
        """
        Test that set_use_advanced_flow_to_false sends the expected command
        """
        expected_command = "SET,ML=1,AF=FA"
        self.ml.set_use_advanced_flow_to_false()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_use_advanced_flow_to_false_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_use_advanced_flow_to_false
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Use Advanced Flow' to: 'FA' -> {1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_use_advanced_flow_to_false()

        self.assertEqual(expected_msg, context.exception.message)
        
    #################################
    def test_set_share_with_flow_station_to_true_happy_path(self):
        """
        Test that set_share_with_flow_station_to_true sends the expected command
        """
        expected_command = "SET,ML=1,SH=TR"
        self.ml.set_share_with_flow_station_to_true()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_share_with_flow_station_to_true_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_share_with_flow_station_to_true
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Share With Flow Station' to: 'TR' -> {1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_share_with_flow_station_to_true()

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_share_with_flow_station_to_false_happy_path(self):
        """
        Test that set_share_with_flow_station_to_false sends the expected command
        """
        expected_command = "SET,ML=1,SH=FA"
        self.ml.set_share_with_flow_station_to_false()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_share_with_flow_station_to_false_exception(self):
        """
        Test handling of an exception in send_and_wait_for_reply in set_share_with_flow_station_to_false
        """
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Share With Flow Station' to: 'FA' -> {1}".format(str(self.mainline_address), exception_msg)
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_share_with_flow_station_to_false()

        self.assertEqual(expected_msg, context.exception.message)
        
    #################################
    def test_set_high_flow_variance_tier_one_happy_path(self):
        """ Test that set_high_flow_variance_tier_one happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.high_variance_limit_tier_one,           # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_high_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_high_flow_variance_tier_one(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_flow_variance_tier_one_happy_path_2(self):
        """ Test that set_high_flow_variance_tier_one happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = False
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.high_variance_limit_tier_one,           # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_high_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_high_flow_variance_tier_one(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_flow_variance_tier_one_wrong_flow_limit_argument_type(self):
        """ Test that set_high_flow_variance_tier_one will detect a non-integer flow limit
        """
        flow_limit = '10.0'
        with_shutdown_enabled = True

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_high_flow_variance_tier_one(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_high_flow_variance_tier_one_wrong_flow_limit_argument_type_2(self):
        """ Test that set_high_flow_variance_tier_one will detect a non-boolean with_shutdown_enabled
        """
        flow_limit = 10
        with_shutdown_enabled = 'True'

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_high_flow_variance_tier_one(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_high_flow_variance_tier_one_send_exception(self):
        """ Test that set_high_flow_variance_tier_one throws exception when sending to controller fails
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        exception_msg = "Test Exception"

        expected_msg = "Exception occurred trying to set Mainline {0}'s 'High Variance Limit Tier One' to: '{1}'" \
                       " and 'Zone High Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'"\
            .format(str(self.mainline_address), float(flow_limit), str(mapped_boolean), exception_msg)

        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_high_flow_variance_tier_one(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_high_flow_variance_tier_two_happy_path(self):
        """ Test that set_high_flow_variance_tier_two happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.high_variance_limit_tier_two,           # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_high_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_high_flow_variance_tier_two(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_flow_variance_tier_two_happy_path_2(self):
        """ Test that set_high_flow_variance_tier_two happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = False
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.high_variance_limit_tier_two,           # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_high_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_high_flow_variance_tier_two(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_flow_variance_tier_two_wrong_flow_limit_argument_type(self):
        """ Test that set_high_flow_variance_tier_two will detect a non-integer flow limit
        """
        flow_limit = '10.0'
        with_shutdown_enabled = True

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_high_flow_variance_tier_two(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_high_flow_variance_tier_two_wrong_flow_limit_argument_type_2(self):
        """ Test that set_high_flow_variance_tier_two will detect a non-boolean with_shutdown_enabled
        """
        flow_limit = 10
        with_shutdown_enabled = 'True'

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_high_flow_variance_tier_two(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_high_flow_variance_tier_two_send_exception(self):
        """ Test that set_high_flow_variance_tier_two throws exception when sending to controller fails
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        exception_msg = "Test Exception"

        expected_msg = "Exception occurred trying to set Mainline {0}'s 'High Variance Limit Tier Two' to: '{1}' and " \
                       "'Zone High Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'"\
            .format(str(self.mainline_address), float(flow_limit), str(mapped_boolean), exception_msg)

        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_high_flow_variance_tier_two(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.assertEqual(expected_msg, context.exception.message)
        
    #################################
    def test_set_high_flow_variance_tier_three_happy_path(self):
        """ Test that set_high_flow_variance_tier_three happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.high_variance_limit_tier_three,           # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_high_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_high_flow_variance_tier_three(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_flow_variance_tier_three_happy_path_2(self):
        """ Test that set_high_flow_variance_tier_three happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = False
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false
        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.high_variance_limit_tier_three,           # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_high_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_high_flow_variance_tier_three(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_flow_variance_tier_three_wrong_flow_limit_argument_type(self):
        """ Test that set_high_flow_variance_tier_three will detect a non-integer flow limit
        """
        flow_limit = '10.0'
        with_shutdown_enabled = True

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_high_flow_variance_tier_three(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_high_flow_variance_tier_three_wrong_flow_limit_argument_type_2(self):
        """ Test that set_high_flow_variance_tier_three will detect a non-boolean with_shutdown_enabled
        """
        flow_limit = 10
        with_shutdown_enabled = 'True'

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_high_flow_variance_tier_three(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_high_flow_variance_tier_three_send_exception(self):
        """ Test that set_high_flow_variance_tier_three throws exception when sending to controller fails
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        exception_msg = "Test Exception"

        expected_msg = "Exception occurred trying to set Mainline {0}'s 'High Variance Limit Tier Three' to: '{1}' " \
                       "and 'Zone High Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'"\
            .format(str(self.mainline_address), float(flow_limit), str(mapped_boolean), exception_msg)

        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_high_flow_variance_tier_three(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.assertEqual(expected_msg, context.exception.message)
        
    #################################
    def test_set_high_flow_variance_tier_four_happy_path(self):
        """ Test that set_high_flow_variance_tier_four happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false
        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.high_variance_limit_tier_four,          # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_high_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_high_flow_variance_tier_four(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_flow_variance_tier_four_happy_path_2(self):
        """ Test that set_high_flow_variance_tier_four happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = False
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.high_variance_limit_tier_four,          # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_high_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_high_flow_variance_tier_four(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_high_flow_variance_tier_four_wrong_flow_limit_argument_type(self):
        """ Test that set_high_flow_variance_tier_four will detect a non-integer flow limit
        """
        flow_limit = '10'
        with_shutdown_enabled = True

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_high_flow_variance_tier_four(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_high_flow_variance_tier_four_wrong_flow_limit_argument_type_2(self):
        """ Test that set_high_flow_variance_tier_four will detect a non-boolean with_shutdown_enabled
        """
        flow_limit = 10
        with_shutdown_enabled = 'True'
        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. Received: <type 'str'>".format(self.mainline_address)


        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_high_flow_variance_tier_four(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_high_flow_variance_tier_four_send_exception(self):
        """ Test that set_high_flow_variance_tier_four throws exception when sending to controller fails
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        exception_msg = "Test Exception"

        expected_msg = "Exception occurred trying to set Mainline {0}'s 'High Variance Limit Tier Four' to: '{1}' and " \
                       "'Zone High Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'"\
            .format(str(self.mainline_address), float(flow_limit), str(mapped_boolean), exception_msg)

        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_high_flow_variance_tier_four(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.assertEqual(expected_msg, context.exception.message)
        
    #################################
    def test_set_low_flow_variance_tier_one_happy_path(self):
        """ Test that set_low_flow_variance_tier_one happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.low_variance_limit_tier_one,           # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_low_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_low_flow_variance_tier_one(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_low_flow_variance_tier_one_happy_path_2(self):
        """ Test that set_low_flow_variance_tier_one happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = False
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false
        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.low_variance_limit_tier_one,           # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_low_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_low_flow_variance_tier_one(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_low_flow_variance_tier_one_wrong_flow_limit_argument_type(self):
        """ Test that set_low_flow_variance_tier_one will detect a non-integer flow limit
        """
        flow_limit = '10.0'
        with_shutdown_enabled = True
        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_low_flow_variance_tier_one(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_low_flow_variance_tier_one_wrong_flow_limit_argument_type_2(self):
        """ Test that set_low_flow_variance_tier_one will detect a non-boolean with_shutdown_enabled
        """
        flow_limit = 10
        with_shutdown_enabled = 'True'
        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_low_flow_variance_tier_one(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_low_flow_variance_tier_one_send_exception(self):
        """ Test that set_low_flow_variance_tier_one throws exception when sending to controller fails
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        exception_msg = "Test Exception"

        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Low Variance Limit Tier One' to: '{1}' and" \
                       " 'Zone Low Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'"\
            .format(str(self.mainline_address), float(flow_limit), str(mapped_boolean), exception_msg)

        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_low_flow_variance_tier_one(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_low_flow_variance_tier_two_happy_path(self):
        """ Test that set_low_flow_variance_tier_two happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false
        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.low_variance_limit_tier_two,           # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_low_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_low_flow_variance_tier_two(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_low_flow_variance_tier_two_happy_path_2(self):
        """ Test that set_low_flow_variance_tier_two happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = False
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.low_variance_limit_tier_two,           # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_low_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_low_flow_variance_tier_two(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_low_flow_variance_tier_two_wrong_flow_limit_argument_type(self):
        """ Test that set_low_flow_variance_tier_two will detect a non-integer flow limit
        """
        flow_limit = '10.0'
        with_shutdown_enabled = True

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_low_flow_variance_tier_two(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_low_flow_variance_tier_two_wrong_flow_limit_argument_type_2(self):
        """ Test that set_low_flow_variance_tier_two will detect a non-boolean with_shutdown_enabled
        """
        flow_limit = 10
        with_shutdown_enabled = 'True'

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_low_flow_variance_tier_two(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_low_flow_variance_tier_two_send_exception(self):
        """ Test that set_low_flow_variance_tier_two throws exception when sending to controller fails
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        exception_msg = "Test Exception"

        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Low Variance Limit Tier Two' to: '{1}' and" \
                       " 'Zone Low Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'"\
            .format(str(self.mainline_address), float(flow_limit), str(mapped_boolean), exception_msg)

        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_low_flow_variance_tier_two(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.assertEqual(expected_msg, context.exception.message)
        
    #################################
    def test_set_low_flow_variance_tier_three_happy_path(self):
        """ Test that set_low_flow_variance_tier_three happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.low_variance_limit_tier_three,           # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_low_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_low_flow_variance_tier_three(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_low_flow_variance_tier_three_happy_path_2(self):
        """ Test that set_low_flow_variance_tier_three happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = False
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.low_variance_limit_tier_three,           # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_low_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_low_flow_variance_tier_three(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_low_flow_variance_tier_three_wrong_flow_limit_argument_type(self):
        """ Test that set_low_flow_variance_tier_three will detect a non-integer flow limit
        """
        flow_limit = '10.0'
        with_shutdown_enabled = True

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_low_flow_variance_tier_three(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_low_flow_variance_tier_three_wrong_flow_limit_argument_type_2(self):
        """ Test that set_low_flow_variance_tier_three will detect a non-boolean with_shutdown_enabled
        """
        flow_limit = 10
        with_shutdown_enabled = 'True'

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_low_flow_variance_tier_three(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_low_flow_variance_tier_three_send_exception(self):
        """ Test that set_low_flow_variance_tier_three throws exception when sending to controller fails
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        exception_msg = "Test Exception"

        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Low Variance Limit Tier Three' to: '{1}' and " \
                       "'Zone Low Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'"\
            .format(str(self.mainline_address), float(flow_limit), str(mapped_boolean), exception_msg)

        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_low_flow_variance_tier_three(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.assertEqual(expected_msg, context.exception.message)
        
    #################################
    def test_set_low_flow_variance_tier_four_happy_path(self):
        """ Test that set_low_flow_variance_tier_four happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.low_variance_limit_tier_four,          # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_low_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_low_flow_variance_tier_four(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_low_flow_variance_tier_four_happy_path_2(self):
        """ Test that set_low_flow_variance_tier_four happy path sends the correct test engine command
        """
        flow_limit = 10
        with_shutdown_enabled = False
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        expected_command = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.mainline,                               # {0}
            str(self.mainline_address),                     # {1}
            opcodes.low_variance_limit_tier_four,          # {2}
            float(flow_limit),                                # {3}
            opcodes.zone_low_variance_detection_shutdown,  # {4}
            mapped_boolean                                  # {5}
        )
        self.ml.set_low_flow_variance_tier_four(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_low_flow_variance_tier_four_wrong_flow_limit_argument_type(self):
        """ Test that set_low_flow_variance_tier_four will detect a non-integer flow limit
        """
        flow_limit = '10.0'
        with_shutdown_enabled = True

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_low_flow_variance_tier_four(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_low_flow_variance_tier_four_wrong_flow_limit_argument_type_2(self):
        """ Test that set_low_flow_variance_tier_four will detect a non-boolean with_shutdown_enabled
        """
        flow_limit = 10
        with_shutdown_enabled = 'True'

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        # Create the error message that will be compared to the actual error message
        expected_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. Received: <type 'str'>".format(self.mainline_address)

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.ml.set_low_flow_variance_tier_four(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_set_low_flow_variance_tier_four_send_exception(self):
        """ Test that set_low_flow_variance_tier_four throws exception when sending to controller fails
        """
        flow_limit = 10
        with_shutdown_enabled = True
        mapped_boolean = opcodes.true if with_shutdown_enabled else opcodes.false

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=True)

        exception_msg = "Test Exception"

        expected_msg = "Exception occurred trying to set Mainline {0}'s 'Low Variance Limit Tier Four' to: '{1}' and " \
                       "'Zone Low Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'"\
            .format(str(self.mainline_address), float(flow_limit), str(mapped_boolean), exception_msg)

        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.ml.set_low_flow_variance_tier_four(_percent=flow_limit, _with_shutdown_enabled=with_shutdown_enabled)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_standard_variance_limit_happy_path(self):
        """ Verify Standard Variance Limit: Exception is not raised """
        test_pass = False
        expected = 6.25

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=False)

        self.ml.fv = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(expected))

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_standard_variance_limit()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_standard_variance_limit_fail(self):
        """ Verify Standard Variance Limit error handling: Value on controller does not match what is
         stored in ml.fv """
        test_pass = False
        expected = 6.25
        unexpected = 5.0
        e_msg = ""

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=False)

        self.ml.fv = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(unexpected))

        expected_message = "Unable to verify Mainline 1's 'Standard Variance Limit' value. Received: {0}, Expected: {1}".format(str(unexpected), str(expected))
        try:
            self.ml.verify_standard_variance_limit()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_mainline_variance_limit_happy_path(self):
        """ Verify Mainline Variance Limit: Exception is not raised """
        test_pass = False
        expected = 6.25

        self.ml.is_advanced_flow_enabled = mock.MagicMock(return_value=False)

        self.ml.mf = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(expected))

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_mainline_variance_limit()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_mainline_variance_limit_fail(self):
        """ Verify Mainline Variance Limit error handling: Value on controller does not match what is
         stored in ml.mf """
        test_pass = False
        expected = 6.25
        unexpected = 5.0
        e_msg = ""

        self.ml.mf = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(unexpected))

        expected_message = "Unable to verify Mainline 1's 'Mainline Variance Limit' value. Received: {0}, Expected: {1}".format(str(unexpected), str(expected))
        try:
            self.ml.verify_mainline_variance_limit()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_mainline_variance_shutdown_enabled_happy_path(self):
        """ Verify Mainline Variance Shutdown Enabled: Exception is not raised """
        test_pass = False
        expected = 'TR'

        self.ml.mw = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(expected))

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_mainline_variance_shutdown_enabled()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_mainline_variance_shutdown_enabled_fail(self):
        """ Verify Mainline Variance Shutdown Enabled error handling: Value on controller does not match what is
         stored in ml.mw """
        test_pass = False
        expected = 'TR'
        unexpected = 'FA'
        e_msg = ""

        self.ml.mw = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(unexpected))

        expected_message = "Unable to verify Mainline 1's 'Mainline Variance Shutdown Enabled' value. Received: {0}, Expected: {1}".format(str(unexpected), str(expected))
        try:
            self.ml.verify_mainline_variance_shutdown_enabled()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_pipe_fill_units_happy_path(self):
        """ Verify Pipe Fill Units: Exception is not raised """
        test_pass = False
        expected = 'PS'

        self.ml.un = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(expected))

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_delay_units()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_pipe_fill_units_fail(self):
        """ Verify Pipe Fill Units error handling: Value on controller does not match what is
         stored in ml.un """
        test_pass = False
        expected = 'PS'
        unexpected = 'TM'
        e_msg = ""

        self.ml.un = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(unexpected))

        expected_message = "Unable to verify Mainline 1's 'Pipe Fill Units' value. Received: {0}, Expected: {1}".format(str(unexpected), str(expected))
        try:
            self.ml.verify_delay_units()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_delay_first_zone_happy_path(self):
        """ Verify Delay First Zone: Exception is not raised """
        test_pass = False
        expected_in_minutes = 20
        expected_in_seconds = expected_in_minutes * 60

        self.ml.tm = expected_in_minutes
        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(expected_in_seconds))

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_delay_first_zone()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_delay_first_zone_fail(self):
        """ Verify Delay First Zone error handling: Value on controller does not match what is
         stored in ml.tm """
        test_pass = False
        expected_in_minutes = 20
        unexpected_in_minutes = 10
        unexpected_in_seconds = unexpected_in_minutes * 60
        e_msg = ""

        self.ml.tm = expected_in_minutes
        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(unexpected_in_seconds))

        e_msg = "Unable to verify Mainline 1's 'Delay First Zone' value. Received: {0}, " \
                "Expected: {1}".format(str(unexpected_in_minutes), str(expected_in_minutes))

        # Raise an exception while running through the code
        with self.assertRaises(ValueError) as context:
            self.ml.verify_delay_first_zone()

        # Compare expected error message with actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_delay_for_next_zone_happy_path(self):
        """ Verify Delay For Next Zone: Exception is not raised """
        test_pass = False
        expected_in_minutes = 20
        expected_in_seconds = expected_in_minutes * 60

        self.ml.tz = expected_in_minutes
        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(expected_in_seconds))

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_delay_for_next_zone()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_delay_for_next_zone_fail(self):
        """ Verify Delay For Next Zone error handling: Value on controller does not match what is
         stored in ml.tz """
        expected_in_minutes = 20
        unexpected_in_minutes = 10
        unexpected_in_seconds = unexpected_in_minutes * 60

        self.ml.tz = expected_in_minutes
        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(unexpected_in_seconds))

        e_msg = "Unable to verify Mainline 1's 'Delay For Next Zone' value. Received: {0}, " \
                "Expected: {1}".format(str(unexpected_in_minutes), str(expected_in_minutes))

        # Raise an exception while running through the code
        with self.assertRaises(ValueError) as context:
            self.ml.verify_delay_for_next_zone()

        # Compare expected error message with actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_delay_after_zone_happy_path(self):
        """ Verify Delay After Zone: Exception is not raised """
        test_pass = False
        expected_in_minutes = 20
        expected_in_seconds = expected_in_minutes * 60

        self.ml.tl = expected_in_minutes

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(expected_in_seconds))

        try:

            with self.assertRaises(Exception):
                # .assertRaises raises an Assertion Error if an Exception is not raised in the method
                self.ml.verify_delay_after_zone()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_delay_after_zone_fail(self):
        """ Verify Delay After Zone error handling: Value on controller does not match what is
         stored in ml.tl """
        expected_in_minutes = 20
        unexpected_in_minutes = 10
        unexpected_in_seconds = unexpected_in_minutes * 60

        self.ml.tl = expected_in_minutes
        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(unexpected_in_seconds))

        e_msg = "Unable to verify Mainline 1's 'Delay After Zone' value. Received: {0}, " \
                "Expected: {1}".format(str(unexpected_in_minutes), str(expected_in_minutes))

        # Raise an exception while running through the code
        with self.assertRaises(ValueError) as context:
            self.ml.verify_delay_after_zone()

        # Compare expected error message with actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_number_of_zones_to_delay_happy_path(self):
        """ Verify Number Of Zones To Deplay: Exception is not raised """
        test_pass = False
        expected = 3

        self.ml.zc = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(expected))

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.ml.verify_number_of_zones_to_delay()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_number_of_zones_to_delay_fail(self):
        """ Verify Number Of Zones To Delay error handling: Value on controller does not match what is
         stored in ml.zc """
        test_pass = False
        expected = 3
        unexpected = 2
        e_msg = ""

        self.ml.zc = expected

        self.ml.data.get_value_string_by_key = mock.MagicMock(return_value=str(unexpected))

        expected_message = "Unable to verify Mainline 1's 'Number of Zones to Delay' value. Received: {0}, Expected: {1}".format(str(unexpected), str(expected))
        try:
            self.ml.verify_number_of_zones_to_delay()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))
        
    #################################
    def test_verify_who_i_am(self):
        self.ml.ad = 1
        self.ml.ds = "Test Mainline 1"
        self.ml.en = opcodes.true
        self.ml.fl = 100
        self.ml.ft = 5 # in minutes
        received_in_seconds = self.ml.ft * 60
        self.ml.lc = opcodes.true
        self.ml.sh = opcodes.false
        self.ml.fw = opcodes.true
        self.ml.fv = 5
        self.ml.mf = 5
        self.ml.mw = opcodes.true
        self.ml.un = 'TM'
        self.ml.tm = 3
        self.ml.tz = 1
        self.ml.tl = 4
        self.ml.zc = 7
        self.ml.af = opcodes.true
        self.ml.ah = 90
        self.ml.al = 95
        self.ml.bh = 80
        self.ml.bl = 85
        self.ml.ch = 70
        self.ml.cl = 75
        self.ml.dh = 60
        self.ml.dl = 65
        self.ml.zh = opcodes.true
        self.ml.zl = opcodes.true
        self.ml.up = opcodes.false
        self.ml.ut = opcodes.true
        self.ml.ml = opcodes.true
        self.ml.fp = opcodes.true

        mock_data = status_parser.KeyValues("{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13},{14}={15},"
                                            "{16}={17},{18}={19},{20}={21},{22}={23},{24}={25},{26}={27},{28}={29},"
                                            "{30}={31},{32}={33},{34}={35},{36}={37},{38}={39},{40}={41},{42}={43},"
                                            "{44}={45},{46}={47},{48}={49},{50}={51},{52}={53},{54}={55},{56}={57},"
                                            "{58}={59},{60}={61},{62}={63}".format(
            opcodes.mainline,
            str(self.ml.ad),
            opcodes.description,
            str(self.ml.ds),
            opcodes.enabled,
            str(self.ml.en),
            opcodes.target_flow,
            str(self.ml.fl),
            opcodes.fill_time,
            received_in_seconds,
            opcodes.limit_zones_by_flow,
            self.ml.lc,
            opcodes.share_with_flowstation,
            self.ml.sh,
            opcodes.standard_variance_shutdown,
            self.ml.fw,
            opcodes.standard_variance_limit,
            self.ml.fv,
            opcodes.mainline_variance_limit,
            self.ml.mf,
            opcodes.mainline_variance_shutdown,
            self.ml.mw,
            opcodes.pipe_fill_units,
            self.ml.un,
            opcodes.delay_first_zone,
            self.ml.tm * 60,
            opcodes.delay_for_next_zone,
            self.ml.tz * 60,
            opcodes.delay_after_zone,
            self.ml.tl * 60,
            opcodes.number_of_zones_to_delay,
            self.ml.zc,
            opcodes.use_advanced_flow,
            self.ml.af,
            opcodes.high_variance_limit_tier_one,
            self.ml.ah,
            opcodes.low_variance_limit_tier_one,
            self.ml.al,
            opcodes.high_variance_limit_tier_two,
            self.ml.bh,
            opcodes.low_variance_limit_tier_two,
            self.ml.bl,
            opcodes.high_variance_limit_tier_three,
            self.ml.ch,
            opcodes.low_variance_limit_tier_three,
            self.ml.cl,
            opcodes.high_variance_limit_tier_four,
            self.ml.dh,
            opcodes.low_variance_limit_tier_four,
            self.ml.dl,
            opcodes.zone_high_variance_detection_shutdown,
            self.ml.zh,
            opcodes.zone_low_variance_detection_shutdown,
            self.ml.zl,
            opcodes.use_pressure_for_stable_flow,
            self.ml.up,
            opcodes.use_time_for_stable_flow,
            self.ml.ut,
            opcodes.mainline,
            self.ml.ml,
            opcodes.point_of_control,
            self.ml.poc,
            opcodes.use_dynamic_flow_allocation,
            self.ml.fp
        )

        )

        self.ml.data = mock_data
        self.ml.get_data = mock.MagicMock(sideeffect=None)
        self.ml.verify_who_i_am()

    if __name__ == "__main__":
        unittest.main()
