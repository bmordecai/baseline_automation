from __future__ import absolute_import

from common.imports.types import ProgramCommands, ActionCommands

__author__ = 'Brice "Ajo Grande" Garlick'

import unittest

import mock
import serial
import status_parser
from common.imports import opcodes
import common.objects.programming.point_of_control as poc
from common.objects.base_classes.programs import BasePrograms
from common.objects.devices.mv import MasterValve
from common.objects.programming.ml import Mainline
from common.objects.programming.pg_3200 import PG3200
from common.objects.object_bucket import *
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.bicoders.valve_bicoder import ValveBicoder


class TestPG3200Object(unittest.TestCase):
    """   """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the moisture sensor
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)

        # Add the ser object
        self.bl_3200.ser = self.mock_ser

        # # Set serial instance to mock serial
        # BasePrograms.ser = self.mock_ser
        # poc.ser = self.mock_ser
        # Mainline.ser = self.mock_ser

        #mainlines32[5] = Mainline(5)
        self.bl_3200.mainlines = dict()
        mainline_address = 5
        self.bl_3200.mainlines[mainline_address] = Mainline(_controller=self.bl_3200, _ad=mainline_address)

        # master_valves[3] = MasterValve("MVD0001", 3)
        self.bl_3200.master_valves = dict()
        valve_bicoder_address = 3
        valve_bicoder = ValveBicoder(_sn="MVD0001", _controller=self.bl_3200, _id=opcodes.master_valve)
        self.bl_3200.master_valves[valve_bicoder_address] = MasterValve(_controller=self.bl_3200,
                                                    _valve_bicoder=valve_bicoder,
                                                    _address=valve_bicoder_address)


        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # when these objects are created in this test, they carry over to any test after this one because they are
        # just pointers and not test variables specific to just THIS module.
        del  self.bl_3200.master_valves[3]
        del self.bl_3200.mainlines[5]

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_prog_3200_object(self, _ww=1, _ci="WD", _fm_vr=None):
        """ Creates a new Master Valve object for testing purposes """
        # creates the program object to be used in all the tests
        prog = PG3200(_controller=self.bl_3200, _ad=1)
        if _ww != 1:
            ww_value = ['011111111111111111111111',
                        '001111111111111111111111',
                        '000111111111111111111111',
                        '000011111111111111111111',
                        '000001111111111111111111',
                        '000000111111111111111111',
                        '000000011111111111111111'
                        ]
            prog.ww = ww_value
        # prog = PG3200(_ad=1, _ww=ww_value, _ci=_ci, _ml=5, _bp=3, _cn_fm_version=_fm_vr)
        prog.bp = 3
        prog.ci = _ci

        return prog

    #################################
    def create_test_prog_3200_object_no_bp(self, _ww=1, _ci="WD", _fm_vr=None):
        """ Creates a new Master Valve object for testing purposes """
        prog = PG3200(_controller=self.bl_3200, _ad=1)
        # # creates the program object to be used in all the tests
        if _ww != 1:
            ww_value = ['011111111111111111111111',
                        '001111111111111111111111',
                        '000111111111111111111111',
                        '000011111111111111111111',
                        '000001111111111111111111',
                        '000000111111111111111111',
                        '000000011111111111111111'
                        ]
            prog.ww = ww_value
            prog.ci = _ci

        return prog

    #################################
    def test_create_full_prog_3200_object_pass1(self):
        """ Test creating prog_3200 object with all arguments passed-in """
        test_pass = False
        prog = PG3200(_controller=self.bl_3200, _ad=1)
        if prog is not None:
            test_pass = True
        self.assertEqual(test_pass, True)

    #################################
    def test_send_programming_pass1(self):
        """ Send Programming To Controller Pass Case 1: Correct Command Sent with 1 water window value using
        defaults, _ci = EV """
        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "WW=111111111111111111111111," \
                           "MC=1," \
                           "SA=100," \
                           "CI=EV," \
                           "WD=1111111," \
                           "ST=480," \
                           "PR=2" \

        prog = self.create_test_prog_3200_object(_ci="EV")

        prog.send_programming()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_pass2(self):
        """ Send Programming To Controller Pass Case 2: Correct Command Sent with 1 water window value using
        defaults, _ci = WD """
        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "WW=111111111111111111111111," \
                           "MC=1," \
                           "SA=100," \
                           "CI=WD," \
                           "WD=1111111," \
                           "ST=480," \
                           "PR=2"
        prog = self.create_test_prog_3200_object()

        prog.send_programming()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_pass3(self):
        """ Send Programming To Controller Pass Case 3: Correct Command Sent with 1 water window value using
        defaults, _ci = ID """
        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "WW=111111111111111111111111," \
                           "MC=1," \
                           "SA=100," \
                           "CI=ID," \
                           "WD=1111111," \
                           "ST=480," \
                           "PR=2"

        prog = self.create_test_prog_3200_object(_ci="ID")

        prog.send_programming()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_pass6(self):
        """ Send Programming To Controller Pass Case 6: Send programming when code version is greater than V16."""
        # Command but without Mainline key/value pair
        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "WW=111111111111111111111111," \
                           "MC=1," \
                           "SA=100," \
                           "CI=ID," \
                           "WD=1111111," \
                           "ST=480," \
                           "PR=2" \

        # Set all Programs firmware versions to be v16
        PG3200.cn_firmware_version = opcodes.firmware_version_V16
        prog = self.create_test_prog_3200_object(_ci="ID", _fm_vr="16.508")

        prog.send_programming()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

        # Reset the Programs firmware version to None
        PG3200.cn_firmware_version = None

    #################################
    def test_send_programming_fail1(self):
        """ Send Programming To Controller Fail Case 1: Failed communication with controller """
        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "WW=111111111111111111111111," \
                           "MC=1," \
                           "SA=100," \
                           "CI=WD," \
                           "WD=1111111," \
                           "ST=480," \
                           "PR=2" \

        prog = self.create_test_prog_3200_object()
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            prog.send_programming()
        e_msg = "Exception occurred trying to set Program 1's 'Values' to: '{0}' -> ".format(expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_learn_flow_to_start_advanced_1(self):
        """ do an advanced learn flow zone operation"""

        # Create program object
        prog = self.create_test_prog_3200_object()

        # Sets parameter values
        seconds = 180

        # this is what should be sent to the controller
        expected_command = "DO,LF,PG=1,TY=AD"

        # make the call
        prog.set_learn_flow_to_start_advanced()

        # test results
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_learn_flow_to_start_advanced_2(self):
        """ do an advanced learn flow zone operation"""

        # Create program object
        prog = self.create_test_prog_3200_object()

        # Sets parameter values
        seconds = 180

        # this is what should be sent to the controller
        expected_command = "DO,LF,PG=1,TY=AD,TM=180"

        # make the call
        prog.set_learn_flow_to_start_advanced(_time_delay=seconds)

        # test results
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_learn_flow_to_start_quick_1(self):
        """ do an advanced learn flow zone operation"""

        # Create program object
        prog = self.create_test_prog_3200_object()

        # Sets parameter values
        seconds = 180

        # this is what should be sent to the controller
        expected_command = "DO,LF,PG=1,TY=QK"

        # make the call
        prog.set_learn_flow_to_start_quick()

        # test results
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_learn_flow_to_start_quick_2(self):
        """ do an advanced learn flow zone operation"""

        # Create program object
        prog = self.create_test_prog_3200_object()

        # Sets parameter values
        seconds = 180

        # this is what should be sent to the controller
        expected_command = "DO,LF,PG=1,TY=QK,TM=180"

        # make the call
        prog.set_learn_flow_to_start_quick(_time_delay=seconds)

        # test results
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_learn_flow_to_start_standard_1(self):
        """ do an standard learn flow zone operation"""

        # Create program object
        prog = self.create_test_prog_3200_object()

        # Sets parameter values
        seconds = 180

        # this is what should be sent to the controller
        expected_command = "DO,LF,PG=1,TY=SD"

        # make the call
        prog.set_learn_flow_to_start_standard()

        # test results
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_learn_flow_to_start_standard_2(self):
        """ do an standard learn flow zone operation"""

        # Create program object
        prog = self.create_test_prog_3200_object()

        # Sets parameter values
        seconds = 180

        # this is what should be sent to the controller
        expected_command = "DO,LF,PG=1,TY=SD,TM=180"

        # make the call
        prog.set_learn_flow_to_start_standard(_time_delay=seconds)

        # test results
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_event_stop_date_pass1(self):
        """ Set Start Times On program Pass Case 1: Setting new event dates check that object
        variable _ed is set """
        prog = self.create_test_prog_3200_object()

        new_value = ["3/17/18", "5/5/18"]
        prog.set_event_stop_dates(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = prog.ed
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_event_stop_date_pass2(self):
        """ Set Event Dates On program Pass Case 2: Command with correct values is sent to controller """
        prog = self.create_test_prog_3200_object()

        new_value = ["3/17/18", "5/5/18"]
        expected_command = "SET,{0}=1,{1}=3/17/18;5/5/18".format(
            opcodes.program,
            opcodes.event_day
            )

        prog.set_event_stop_dates(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_start_times_pass1(self):
        """ Set Start Times On Controller Pass Case 1: Setting new _st value = [60, 120] check that object
        variable _st is set """
        prog = self.create_test_prog_3200_object()

        new_value = [60, 120]
        prog.set_start_times(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = prog.st
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_start_times_pass2(self):
        """ Set Start Times On Controller Pass Case 2: Command with correct values is sent to controller """
        prog = self.create_test_prog_3200_object()

        new_value = [60, 120]
        expected_command = "SET,{0}=1,{1}=60=120".format(
            opcodes.program,
            opcodes.start_times
            )

        prog.set_start_times(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_start_times_fail1(self):
        """ Set Start Times On Controller Fail Case 1: Failed communication with controller """
        prog = self.create_test_prog_3200_object()
        new_value = [60]

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.set_start_times(new_value)
        e_msg = "Exception occurred trying to set Program 1's 'Start Times' to: 60 -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_priority_level_pass1(self):
        """ Set Priority Level On Controller Pass Case 1: Setting new _pr value = 3 check that object
        variable _pr is set """
        prog = self.create_test_prog_3200_object()

        new_value = 3
        prog.set_priority_level(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = prog.pr
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_priority_level_pass2(self):
        """ Set Priority Level On Controller Pass Case 2: Command with correct values is sent to controller """
        prog = self.create_test_prog_3200_object()

        new_value = 3
        expected_command = "SET,{0}=1,{1}=3".format(
            opcodes.program,
            opcodes.priority
            )

        prog.set_priority_level(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_priority_level_fail1(self):
        """ Set Priority Level On Controller Fail Case 1: Failed communication with controller """
        prog = self.create_test_prog_3200_object()
        new_value = 1

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.set_priority_level(new_value)
        e_msg = "Exception occurred trying to set Program 1's 'Priority Level' to: 1 -> "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_booster_pump_pass1(self):
        """ Set Booster Pump On Controller Pass Case 1: Passed in address of a present Booster Pump """
        prog = self.create_test_prog_3200_object()

        expected_value = 3
        prog.add_booster_pump(expected_value)

        this_obj = self.bl_3200.master_valves[expected_value]
        actual_value = this_obj.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_booster_pump_pass2(self):
        """ Set Booster Pump On Controller Pass Case 2: Command with correct values sent to controller """
        prog = self.create_test_prog_3200_object()
        mv_address = 3
        this_obj = prog.controller.master_valves[mv_address]
        sn_value = this_obj.sn
        ad_value = this_obj.ad
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.booster_pump,
                                                      str(sn_value))

        prog.add_booster_pump(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_booster_pump_fail1(self):
        """ Set Booster Pump On Controller Fail Case 1: Failed communication with controller """
        prog = self.create_test_prog_3200_object()
        mv_address = 3
        this_obj = prog.controller.master_valves[mv_address]
        sn_value = this_obj.sn
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.add_booster_pump(ad_value)
        e_msg = "Exception occurred trying to set Program 1's 'Booster Pump' to: {0}({1}) " \
                "-> ".format(sn_value, str(ad_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_booster_pump_pass3(self):
        """ Set Booster Pump On Controller pass case : No booster pump assigned to program """
        # input can be a str/int/None; these are the values that bp can be
        # take in an int and covert to a string
        # take in a None and convert to an empty str
        # take in a str and convert to empty str

        prog = self.create_test_prog_3200_object_no_bp()
        # passing in an empty string
        sn_value = ''
        # expect to see: 'SET,PG=1,BP='
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.booster_pump,
                                                      str(sn_value))
        prog.add_booster_pump(sn_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_booster_pump_pass4(self):
        """ Set Booster Pump On Controller pass case : No booster pump assigned to program """
        # input can be a str/int/None; these are the values that bp can be
        # take in an int and covert to a string
        # take in a None and convert to an empty str
        # take in a str and convert to empty str

        prog = self.create_test_prog_3200_object_no_bp()
        # passing in None
        sn_value = None
        # expect to see: 'SET,PG=1,BP='
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program,
                                                      opcodes.booster_pump,
                                                      str(''))
        prog.add_booster_pump(sn_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_booster_pump_pass5(self):
        """ Set Booster Pump On Controller pass case : No booster pump assigned to program """
        # input can be a str/int/None; these are the values that bp can be
        # take in an int and covert to a string
        # take in a None and convert to an empty str
        # take in a str and convert to empty str

        prog = self.create_test_prog_3200_object_no_bp()
        # passing in an integer; this will cause the serial number to be looked up in prog.master_valves dictionary
        mv_value = 3
        # expect to see: 'SET,PG=1,BP=MVD0001'
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program,
                                                      opcodes.booster_pump,
                                                      prog.controller.master_valves[3].sn)
        prog.add_booster_pump(mv_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_booster_pump_pass6(self):
        """ Set Booster Pump On Controller pass case : No booster pump assigned to program """
        # input can be a str/int/None; these are the values that bp can be
        # take in an int and covert to a string
        # take in a None and convert to an empty str
        # take in a str and convert to empty str

        prog = self.create_test_prog_3200_object_no_bp()
        # passing in the string 'FA'
        sn_value = "FA"
        # expect to see: 'SET,PG=1,BP='
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program,
                                                      opcodes.booster_pump,
                                                      str(''))
        prog.add_booster_pump(sn_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
        
    #################################
    def test_set_watering_intervals_to_all_days_of_the_week_pass_1(self):
        """ Set PG watering interval to daily and enable all days On Controller pass case: Correct string sent to CN """
        prog = self.create_test_prog_3200_object()
        
        expected_interval_string = "1111111"
        
        # expect to see: 'SET,PG=1,BP='
        expected_command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            ProgramCommands.Program,                        # {1}
            prog.ad,                                        # {2}
            ProgramCommands.Attributes.CALENDAR_INTERVALS,  # {3}
            ProgramCommands.Attributes.WEEKDAYS,            # {4}
            ProgramCommands.Attributes.WEEKDAYS,            # {5}
            expected_interval_string                        # {6}
        )
        
        prog.set_watering_intervals_to_all_days_of_the_week()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
        
    #################################
    def test_set_watering_intervals_to_all_days_of_the_week_fail_1(self):
        """ Set PG watering interval to daily and enable all days On Controller fail case: CN rejects command for some
         reason. """
        prog = self.create_test_prog_3200_object()
    
        expected_interval_string = "1111111"
    
        # expect to see: 'SET,PG=1,BP='
        expected_command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            ProgramCommands.Program,                        # {1}
            prog.ad,                                        # {2}
            ProgramCommands.Attributes.CALENDAR_INTERVALS,  # {3}
            ProgramCommands.Attributes.WEEKDAYS,            # {4}
            ProgramCommands.Attributes.WEEKDAYS,            # {5}
            expected_interval_string                        # {6}
        )
        
        exception_msg = "BC Response Received From Controller"
        
        # Set up mocked "failure"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)
    
        with self.assertRaises(Exception) as context:
            prog.set_watering_intervals_to_all_days_of_the_week()
            
        expected_e_msg = ("Exception occurred trying to set Program {0}'s 'Calendar Interval' "
                          "to: {1} -> {2}").format(prog.ad, expected_command, exception_msg)
        
        self.assertEqual(expected_e_msg, context.exception.message)

    #################################
    def test_verify_start_times_pass1(self):
        """ Verify Start Times On Controller Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        prog.st = [60]
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=60".format(opcodes.start_times))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_start_times()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_start_times_fail1(self):
        """ Verify Start Times On Controller Fail Case 1: Value on controller does not match what is
        stored in prog.st """
        prog = self.create_test_prog_3200_object()
        prog.st = [60]
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.start_times))
        prog.data = mock_data

        expected_message = "Unable to verify 3200 Program 1's 'Start Times'. Received: [6], Expected: [60]"
        try:
            prog.verify_start_times()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_calendar_interval_pass1(self):
        """ Verify Calendar Interval On Controller Pass Case 1: Exception is not raised, _ci=ET, class variables
        match values on the controller """
        prog = self.create_test_prog_3200_object()
        prog.ci = "ET"
        prog.sm = [
            1, 1,       # January 1-15, January 16-31
            1, 1,       # February 1-15, February 16-End of month
            1, 1,       # March 1-15, March 16-31
            1, 1,       # April 1-15, April 16-30
            1, 1,       # May 1-15, May 16-31
            1, 1,       # June 1-15, June 16-30
            1, 1,       # July 1-15, July 16-31
            1, 1,       # August 1-15, August 16-31
            1, 1,       # September 1-15, September 16-30
            1, 1,       # October 1-15, October 16-31
            1, 1,       # November 1-15, November 16-30
            1, 1        # December 1-15, December 16-31
            ]
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}={1},"
                                            "{2}=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1=1"
                                            .format(opcodes.calendar_interval,
                                                    opcodes.historical_calendar,
                                                    opcodes.program_semi_month_interval))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_calendar_interval()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            print("**** Msg = {0}".format(e_msg))
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_calendar_interval_pass2(self):
        """ Verify Calendar Interval On Controller Pass Case 2: Exception is not raised, _ci=WD, class variables
        match values on the controller """
        prog = self.create_test_prog_3200_object()
        prog.ci = "WD"
        prog.wd = [
            1,          # Sunday
            1,          # Monday
            1,          # Tuesday
            1,          # Wednesday
            1,          # Thursday
            1,          # Friday
            1           # Saturday
            ]
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}={1},"
                                            "{2}=1111111"
                                            .format(opcodes.calendar_interval,
                                                    opcodes.week_days,
                                                    opcodes.week_days))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_calendar_interval()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_calendar_interval_pass3(self):
        """ Verify Calendar Interval On Controller Pass Case 3: Exception is not raised, _ci=ID, class variables
        match values on the controller """
        prog = self.create_test_prog_3200_object()
        prog.ci = "ID"
        prog.di = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}={1},"
                                            "{2}=5"
                                            .format(opcodes.calendar_interval,
                                                    opcodes.interval_days,
                                                    opcodes.day_interval))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_calendar_interval()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_calendar_interval_pass4(self):
        """ Verify Calendar Interval On Controller Pass Case 4: Exception is not raised, _ci=EV, class variables
        match values on the controller """
        prog = self.create_test_prog_3200_object()
        prog.ci = "EV"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}={1},".format(opcodes.calendar_interval,
                                                                                 opcodes.even_day))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_calendar_interval()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_calendar_interval_fail1(self):
        """ Verify Calendar Interval On Controller Fail Case 1: prog.ci=ET, sm value is None """
        prog = self.create_test_prog_3200_object()
        prog.ci = "ET"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=ET,{1}=None"
                                            .format(opcodes.calendar_interval, opcodes.program_semi_month_interval))
        prog.data = mock_data

        expected_message = "Unable to verify 'Calendar Interval' type: ET for current program. Unable to find a value " \
                           "associated with key: 'SM'. Current program doesn't have the expected 'Calendar Interval'"
        try:
            prog.verify_calendar_interval(unit_test_value=None)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_calendar_interval_fail2(self):
        """ Verify Calendar Interval On Controller Fail Case 2: Value on controller does not match what is
        stored in prog.di where prog.ci=ID """
        prog = self.create_test_prog_3200_object()
        prog.ci = "ID"
        prog.di = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=ID,{1}=6"
                                            .format(opcodes.calendar_interval, opcodes.day_interval))
        prog.data = mock_data

        expected_message = "Unable to verify 'Calendar Interval' type: ID, for current program. Expected:"\
                           "(Key: DI, Value: 5), Received: (Key: DI, Value: 6)"
        try:
            prog.verify_calendar_interval(unit_test_value=None)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_priority_level_pass1(self):
        """ Verify Priority Level On Controller Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        prog.pr = 2
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=2".format(opcodes.priority))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_priority_level()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_priority_level_fail1(self):
        """ Verify Priority Level On Controller Fail Case 1: Value on controller does not match what is
        stored in prog.pr """
        prog = self.create_test_prog_3200_object()
        prog.pr = 2
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=1".format(opcodes.priority))
        prog.data = mock_data

        expected_message = "Unable to verify 3200 Program 1's 'Priority Level'. Received: 1, Expected: 2"
        try:
            prog.verify_priority_level()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_booster_pump_pass1(self):
        """ Verify Booster Pump On Controller Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        prog.bp_type = opcodes.master_valve
        mv_address = 3
        sn_value = prog.controller.master_valves[mv_address].sn
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.booster_pump, sn_value))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_booster_pump()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_booster_pump_fail1(self):
        """ Verify Booster Pump On Controller Fail Case 1: Value on controller does not match what is
        stored in ad for the POC """
        prog = self.create_test_prog_3200_object()
        prog.bp_type = opcodes.master_valve
        mv_address = 3
        sn_value = prog.controller.master_valves[mv_address].sn
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=2".format(opcodes.booster_pump))
        prog.data = mock_data

        expected_message = "Unable to verify 3200 Program 1's 'Booster Pump'. Received: 2, Expected: {0}"\
            .format(sn_value)
        try:
            prog.verify_booster_pump()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_semi_month_interval_list_pass1(self):
        """ Verify Valid Semi Month Interval List Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_valid_semi_month_interval_list(test_value)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_valid_semi_month_interval_list_fail1(self):
        """ Verify Valid Semi Month Interval List Fail Case 1: value passed is more than 24 """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        test_pass = False
        e_msg = ""

        expected_message = "Invalid semi month interval argument passed in. List must contain 24 comma separated " \
                           "integers, instead found: 25"
        try:
            prog.verify_valid_semi_month_interval_list(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_semi_month_interval_list_fail2(self):
        """ Verify Valid Semi Month Interval List Fail Case 2: value passed is less than 24 """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        test_pass = False
        e_msg = ""

        expected_message = "Invalid semi month interval argument passed in. List must contain 24 comma separated " \
                           "integers, instead found: 23"
        try:
            prog.verify_valid_semi_month_interval_list(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_week_day_list_pass1(self):
        """ Verify Valid Week Day List Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1, 1]
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_valid_week_day_list(test_value)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_valid_week_day_list_fail1(self):
        """ Verify Valid Week Day List Fail Case 1: value passed is more than 7 """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1, 1, 1]
        test_pass = False
        e_msg = ""

        expected_message = "Invalid week day water schedule argument passed in. List must contain 7 comma separated " \
                           "integers, instead found: 8"
        try:
            prog.verify_valid_week_day_list(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_week_day_list_fail2(self):
        """ Verify Valid Week Day List Fail Case 2: value passed is less than 7 """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1]
        test_pass = False
        e_msg = ""

        expected_message = "Invalid week day water schedule argument passed in. List must contain 7 comma separated " \
                           "integers, instead found: 6"
        try:
            prog.verify_valid_week_day_list(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_week_day_list_fail3(self):
        """ Verify Valid Week Day List Fail Case 3: value passed contains ints other than 1 or 0 """
        prog = self.create_test_prog_3200_object()
        test_value = [1, 1, 1, 1, 1, 1, 2]
        test_pass = False
        e_msg = ""

        expected_message = "Invalid list of week day values for Program 1. Expects only 1's and/or 0's"
        try:
            prog.verify_valid_week_day_list(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_start_times_pass1(self):
        """ Verify Valid Start Times Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        test_value = [60, 120, 480]
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_valid_start_times(test_value)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_valid_start_times_fail1(self):
        """ Verify Valid Start Times Fail Case 1: value passed is more than 8 """
        prog = self.create_test_prog_3200_object()
        test_value = [60, 120, 480, 1, 1, 1, 1, 1, 1]
        test_pass = False
        e_msg = ""

        expected_message = "Invalid number of start times to set for Program 1. Expects 0 to 8 start times, " \
                           "received: 9"
        try:
            prog.verify_valid_start_times(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_booster_pump_pass1(self):
        """ Verify Valid Booster Pump Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        test_value = 3
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_valid_booster_pump(test_value)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_valid_booster_pump_fail1(self):
        """ Verify Valid Booster Pump Fail Case 1: Value passed is not an available address """
        prog = self.create_test_prog_3200_object()
        test_value = 2
        test_pass = False
        e_msg = ""

        expected_message = "Invalid Booster Pump address for 3200 Program. Valid address are: {0}"\
            .format(prog.controller.master_valves.keys())
        try:
            prog.verify_valid_booster_pump(test_value, master=True)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_valid_priority_level_pass1(self):
        """ Verify Valid Priority Level Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_3200_object()
        test_value = 2
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_valid_priority_level(test_value)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_valid_priority_level_fail1(self):
        """ Verify Valid Priority Level Fail Case 1: value passed is not 1, 2, or 3 """
        prog = self.create_test_prog_3200_object()
        test_value = 5
        test_pass = False
        e_msg = ""

        expected_message = "Invalid '_priority' (Priority Level) value for 'Set' function for Program 1: 5."\
                           " Accepted values are: 1=high | 2=medium | 3=low"
        try:
            prog.verify_valid_priority_level(test_value)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_who_i_am_pass1(self):
        prog = self.create_test_prog_3200_object()
        prog.bp_type = opcodes.master_valve

        mock_data = status_parser.KeyValues("PG=1,"
                                            "EN=TR,"
                                            "DS=Test Program 1,"
                                            "WW=111111111111111111111111,"
                                            "MC=1,"
                                            "SA=100,"
                                            "CI=WD,"
                                            "WD=1111111,"
                                            "ST=480,"
                                            "PR=2,"
                                            "ML=5,"
                                            "IC=FA,"
                                            "IG=FA,"
                                            "BP=MVD0001")
        prog.data = mock_data
        prog.get_data = mock.MagicMock(sideeffect=None)
        prog.verify_who_i_am()

    #################################
    def test_who_i_am_pass2(self):
        prog = self.create_test_prog_3200_object()
        prog.bp_type = opcodes.master_valve
        prog.cn_firmware_version = opcodes.firmware_version_V16
        # Have data for every variable except mainlines, because we changed the firmware version to V16
        mock_data = status_parser.KeyValues("PG=1,"
                                            "EN=TR,"
                                            "DS=Test Program 1,"
                                            "WW=111111111111111111111111,"
                                            "MC=1,"
                                            "SA=100,"
                                            "CI=WD,"
                                            "WD=1111111,"
                                            "ST=480,"
                                            "PR=2,"
                                            "IC=FA,"
                                            "IG=FA,"
                                            "BP=MVD0001")
        prog.data = mock_data
        prog.get_data = mock.MagicMock(sideeffect=None)
        prog.verify_who_i_am()

    #################################
    def test_who_i_am_pass3(self):
        prog = self.create_test_prog_3200_object_no_bp()
        prog.cn_firmware_version = opcodes.firmware_version_V16
        # Have data for every variable except mainlines, because we changed the firmware version to V16
        mock_data = status_parser.KeyValues("PG=1,"
                                            "EN=TR,"
                                            "DS=Test Program 1,"
                                            "WW=111111111111111111111111,"
                                            "MC=1,"
                                            "SA=100,"
                                            "CI=WD,"
                                            "WD=1111111,"
                                            "ST=480,"
                                            "PR=2,"
                                            "IC=FA,"
                                            "IG=FA,"
                                            "BP=")
        prog.data = mock_data
        prog.get_data = mock.MagicMock(sideeffect=None)
        prog.verify_who_i_am()

    if __name__ == "__main__":
        unittest.main()