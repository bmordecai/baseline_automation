from __future__ import absolute_import
__author__ = 'Brice "Ajo Grande" Garlick'

import unittest
import os

import mock
import serial
import status_parser
import datetime

from common.objects.base_classes.devices import BaseDevices
from common.objects.base_classes.controller import BaseController
from common.imports import opcodes
from common.date_package.date_resource import date_mngr
from common import helper_methods
from common.objects.programming.ws import WaterSource
from common.objects.programming.point_of_control import PointOfControl
from common.objects.programming.ml import Mainline

# Unittest the following methods:
# Todo: set_ai_for_cn
# Todo: verify_ip_address_state
# Todo: init_cn
# Todo: do_increment_clock
# Todo: do_reboot_controller


class TestBaseControllerObject(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.310569
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        BaseDevices.ser = self.mock_ser

        BaseDevices.controller_lat = float(43.609768)
        BaseDevices.controller_long = float(-116.310569)

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """ Cleaning up after the test. """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    #TODO need to add the serial number and the firmware version her so that we are testing it
    def create_test_controller_object(self, _diff_type=None, _diff_mac=None, _diff_serial_number=None,
                                      _diff_serial_port=None):
        """ Creates a new controller object for use in a unit test """
        if not _diff_serial_port:
            _diff_serial_port = self.mock_ser
        if _diff_type is not None:
            controller = BaseController(_type=_diff_type,
                                        _mac="0008EE218C85",
                                        _serial_port=_diff_serial_port,
                                        _ad=1,
                                        _serial_number=_diff_serial_number)
        elif _diff_mac is not None:
            controller = BaseController(_type="32", _mac=_diff_mac,
                                        _serial_port=_diff_serial_port,
                                        _ad=1,
                                        _serial_number=_diff_serial_number)
        else:
            controller = BaseController(_type="32",
                                        _mac="0008EE218C85",
                                        _serial_port=_diff_serial_port,
                                        _ad=1,
                                        _serial_number="3K10001")
        controller.send_command_with_reply = self.mock_send_and_wait_for_reply
        controller.get_command_with_reply = self.mock_get_and_wait_for_reply
        return controller

    #################################
    def test_set_serial_number_pass1(self):
        """ Sets the serial number of the controller pass case 1: able to set serial number"""
        # Create controller object
        cn = self.create_test_controller_object()

        # Store intended serial number
        ser_num = '3K34567'

        # Go into method, overwrite should be intended, thus serial number should be a string
        # Number of characters in string needs to be 7
        cn.set_serial_number(_serial_num=ser_num)

        # Check that serial number equals the new serial number
        self.assertEqual(ser_num, cn.sn)

    #################################
    def test_set_serial_number_fail1(self):
        """ Sets the serial number of the controller fail case 1: The controller needs a seven digit string passed
        in as a serial number """
        # Create controller object
        cn = self.create_test_controller_object()

        # Stores serial number
        bad_ser_num = '1234'

        # Because the method strips off the first to character and appends it to 3K the error message is as follows
        # Store the expected error message
        e_msg = "Controller serial number must be a seven digit string, instead was 3K34"

        # Go into method and pass in something besides a seven digit serial number as the serial number parameter
        # Prepare to have a value error
        with self.assertRaises(ValueError) as context:
            cn.set_serial_number(_serial_num=bad_ser_num)

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_serial_number_fail2(self):
        """
        set_serial_number fail case 2
        Create: Test controller object
        Store: Expected error message
        Run: Test, with _serial_num equal to an integer, expect a type error as it expects a string
        Compare: Expected error message to actual error message
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error message
        e_msg = "Serial number must be in a string format '3'"

        # Run the test, expect a type error
        with self.assertRaises(TypeError) as context:
            cn.set_serial_number(_serial_num=3)

        # Compare the actual error message to the expected error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_serial_number_fail3(self):
        """
        set_serial_number fail case 3
        force the exception
        Create: Test controller object
        Store: Expected error message
        Mock: send_and_wait_for_reply
        Return: Exception, so it will fail
        Run: Test, with exception expected
        Compare: Actual error message to expected error message
        """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store alternate bad serial number
        bad_ser_num = '1234567'
        # the method strips off the first to characters passed in and than appends that to 3k therefor the message
        # would be the following
        # Store expected error message
        e_msg = "Exception occurred trying to set serial number on controller command sent was: SET,CN,SN=3K34567"

        # Mock an exception on serial send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        # Go into the method and store the serial number as something besides a string, prepare to raise an exception
        with self.assertRaises(Exception) as context:
            cn.set_serial_number(_serial_num=bad_ser_num)

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_date_and_time_pass1(self):
        """ Set Date and Time On Controller Pass Case 1: Pass in 01/01/2015 and 10:30:00 """
        controller = self.create_test_controller_object()

        date = '01/01/2015'
        new_data = '10:30:00'
        expected_command = 'SET,DT=' + str(date) + ' ' + str(new_data)
        controller.set_date_and_time(date, new_data)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_date_and_time_fail1(self):
        """ Set Date and Time On Controller Fail Case 1: Use incorrect date format - 01-01-2015 """
        controller = self.create_test_controller_object()

        date = '01-01-2015'
        new_data = '10:30:00'
        with self.assertRaises(Exception) as context:
            controller.set_date_and_time(date, new_data)
        expected_message = "Incorrect data format, should be MM/DD/YYYY"
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_date_and_time_fail2(self):
        """ Set Date and Time On Controller Fail Case 2: Use incorrect time format - 10.30.00 """
        controller = self.create_test_controller_object()

        date = '01/01/2015'
        new_data = '10.30.00'
        with self.assertRaises(Exception) as context:
            controller.set_date_and_time(date, new_data)
        expected_message = "Incorrect data format, should be HH:MM:SS"
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_date_and_time_fail3(self):
        """ Set Date and Time On Controller Fail Case 3: Failed Communication with Controller """
        controller = self.create_test_controller_object()

        date = '01/01/2015'
        new_data = '10:30:00'

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.set_date_and_time(date, new_data)
        e_msg = ("Exception occurred trying to set Date and Time on the Controller command Sent was:"
                 " SET,DT=01/01/2015 10:30:00. Exception: ")
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_sim_mode_to_on_pass1(self):
        """ Set Sim Mode To On Pass Case 1: Controller is successfully set to sim mode """
        controller = self.create_test_controller_object()

        expected_command = 'DO,SM=TR'
        controller.set_sim_mode_to_on()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_sim_mode_to_on_fail1(self):
        """ Set Sim Mode To On Fail Case 1: Failed Communication with Controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.set_sim_mode_to_on()
        e_msg = "Exception occurred trying to turn on sim mode command Sent was: DO,SM=TR"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_sim_mode_to_off_pass1(self):
        """ Set Sim Mode To Off Pass Case 1: Controller is successfully set to sim mode """
        controller = self.create_test_controller_object()

        expected_command = 'DO,SM=FA'
        controller.set_sim_mode_to_off()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_sim_mode_to_off_fail1(self):
        """ Set Sim Mode To Off Fail Case 1: Failed Communication with Controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.set_sim_mode_to_off()
        e_msg = "Exception occurred trying to turn off sim mode command Sent was: DO,SM=FA"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_controller_to_run_pass1(self):
        """ Sets the controller to run pass case 1: Controller type is 1000 """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store the expected serial send and wait for reply value
        expected_value = 'KEY,DL=5'

        # Set controller type to 1000
        cn.ty = "10"

        # Increment the clock
        clock_incrementation = mock.MagicMock(return_value=1)
        cn.do_increment_clock = clock_incrementation

        # Go into method to test
        cn.set_controller_to_run()

        # Check if the serial send and wait for reply's value matches the expected value
        # self.assertEqual(expected_value, expected_value)

    #################################
    def test_set_controller_to_run_pass2(self):
        """ Sets the controller to run pass case 2: Controller type is set to default """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store the expected serial send and wait for reply value
        expected_value = 'KEY,DL=15'

        # Increment the clock
        clock_incrementation = mock.MagicMock(return_value=1)
        cn.do_increment_clock = clock_incrementation

        # Go into method to test
        cn.set_controller_to_run()

        # Check if the serial send and wait for reply's value matches the expected value
        # self.assertEqual(expected_value, expected_value)

    #################################
    def test_set_controller_to_run_fail1(self):
        """ Sets the controller to run fail case 1: Controller type is 1000, method raises an exception """
        cn = self.create_test_controller_object()

        # Store the expected serial send and wait for reply value
        expected_value = 'KEY,DL=5'

        # Store expected error message
        e_msg = "Setting Controller to Run Position Command Failed: "

        # Set controller type to 1000
        cn.ty = "10"

        # Increment the clock
        clock_incrementation = mock.MagicMock(return_value=1)
        cn.do_increment_clock = clock_incrementation

        # Mock a side effect throwing an exception
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Go into method to test
        with self.assertRaises(Exception) as context:
            cn.set_controller_to_run()

        # Compare expected error message against actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_controller_to_run_fail2(self):
        """ Sets the controller to run fail case 2: Controller type is default, method raises an exception """
        cn = self.create_test_controller_object()

        # Store the expected serial send and wait for reply value
        expected_value = 'KEY,DL=15'

        # Store expected error message
        e_msg = "Setting Controller to Run Position Command Failed: "

        # Increment the clock
        clock_incrementation = mock.MagicMock(return_value=1)
        cn.do_increment_clock = clock_incrementation

        # Mock a side effect throwing an exception
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Go into method to test
        with self.assertRaises(Exception) as context:
            cn.set_controller_to_run()

        # Compare expected error message against actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    # def test_set_ai_for_cn_pass1(self):
    #     """ Set AI For Controller Pass Case 1: Sends correct commands to controller to disconnect from and ping
    #      Base manager. """
    #     controller = self.create_test_controller_object()
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,SS=CN")
    #
    #     self.mock_get_and_wait_for_reply = mock.MagicMock(return_value=mock_data)
    #     self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply
    #
    #     disc_bm = 'DO,BM=DC'
    #     set_ip_msg = 'SET,BM,AI=129.0.0.0,UA=TR'
    #     ping_bm = 'DO,BM=PN'
    #
    #     controller.set_ai_for_cn(ip_address="129.0.0.0", unit_testing=True)
    #     send_error = ""
    #
    #     try:
    #         self.mock_send_and_wait_for_reply.assert_called_with(disc_bm)
    #     except AssertionError as ae:
    #         print(ae.message)
    #         send_error = disc_bm
    #         try:
    #             self.mock_send_and_wait_for_reply.assert_called_with(set_ip_msg)
    #         except AssertionError as ae2:
    #             print(ae2.message)
    #             send_error = set_ip_msg
    #         else:
    #             try:
    #                 self.mock_send_and_wait_for_reply.assert_called_with(ping_bm)
    #             except AssertionError as ae3:
    #                 print(ae3.message)
    #                 send_error = ping_bm
    #             else:
    #                 test_pass = True
    #     else:
    #         try:
    #             self.mock_send_and_wait_for_reply.assert_called_with(set_ip_msg)
    #         except AssertionError as ae2:
    #             print(ae2.message)
    #             send_error = set_ip_msg
    #         else:
    #             try:
    #                 self.mock_send_and_wait_for_reply.assert_called_with(ping_bm)
    #             except AssertionError as ae3:
    #                 print(ae3.message)
    #                 send_error = ping_bm
    #             else:
    #                 test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised. Error in sending {0} to "
    #                                       "controller, not in correct format".format(send_error))

    #################################
    def test_set_controller_to_off_pass1(self):
        """
        set_controller_to_off pass case 1:
        Create: Test controller object
        Set: Controller type to 1000
        Mock: Serial send and wait for reply
        Return: None, so it does not fail
        Mock: Do increment clock
        Return: None, so it does not fail
        Test return: Nothing, if it runs through the code and does not fail, then it passes
        Run: Test
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Set the controller type to the 1000
        cn.ty = "10"

        # Mock serial send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = None

        # Mock do increment clock
        mock_increment_clock = mock.MagicMock(side_effect=None)
        cn.do_increment_clock = mock_increment_clock

        # Run method
        cn.set_controller_to_off()

    #################################
    def test_set_controller_to_off_pass2(self):
        """
        set_controller_to_off pass case 2:
        Create: Test controller object
        Mock: Serial send and wait for reply
        Return: None, so it does not fail
        Mock: Do increment clock
        Return: None, so it does not fail
        Test return: Nothing, if it runs through the code and does not fail, then it passes
        Run: Test
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Mock serial send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = None

        # Mock do increment clock
        mock_increment_clock = mock.MagicMock(side_effect=None)
        cn.do_increment_clock = mock_increment_clock

        # Run method
        cn.set_controller_to_off()

    #################################
    def test_set_controller_to_off_fail1(self):
        """
        set_controller_to_off pass case 1:
        Create: Test controller object
        Store: Expected error message
        Set: Controller type to 1000
        Mock: Serial send and wait for reply
        Return: Exception, so it fails
        Run: Test, with an exception expected
        Compare: Expected error message to actual error message
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error message
        e_msg = "Setting Controller to Off Position Command Failed: "

        # Set the controller type to the 1000
        cn.ty = "10"

        # Mock serial send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Run test with exception expected
        with self.assertRaises(Exception) as context:
            cn.set_controller_to_off()

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_controller_to_off_fail2(self):
        """
        set_controller_to_off pass case 2:
        Create: Test controller object
        Store: Expected error message
        Mock: Serial send and wait for reply
        Return: Exception, so it fails
        Run: Test, with an exception expected
        Compare: Expected error message to actual error message
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected error message
        e_msg = "Setting Controller to Off Position Command Failed: "

        # Mock serial send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Run test with exception expected
        with self.assertRaises(Exception) as context:
            cn.set_controller_to_off()

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_serial_port_timeout_pass1(self):
        """ Resets the length of time before the serial port times out pass case 1: Pass in a new value for timeout """
        # Create controller object
        cn = self.create_test_controller_object()

        # Mock the serial connection
        serial_conn = mock.MagicMock()
        cn.ser.serial_conn = serial_conn
        settimeout = mock.MagicMock()
        cn.ser.serial_conn.setTimeout = settimeout

        # Pass in timeout value and run method
        cn.set_serial_port_timeout(timeout=300)

    #################################
    def test_get_date_and_time_pass1(self):
        """ Gets the date and the time pass case 1: Successfully sends a GET,DT string """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Mock the get and wait for reply
        cn_date_time = mock.MagicMock()
        cn.get_command_with_reply = cn_date_time

        # Run the method
        cn.get_date_and_time()

    #################################
    def test_get_date_and_time_fail1(self):
        """ Gets the date and the time fail case 1: Fails to send a GET, DT string """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Store expected message
        e_msg = "Exception occurred trying to get the date and time form the controller command set was: GET,DT"

        self.mock_ser.get_and_wait_for_reply.side_effect = Exception("Test Exception")

        # Run the method without mocking the get prepare for an exception to be thrown
        with self.assertRaises(Exception) as context:
            cn.get_date_and_time()

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_date_and_time_pass1(self):
        """ Verifies the date and the time pass case 1: Able to successfully verify the date and time """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Assign controller date and time
        date_mngr.controller_datetime.obj = datetime.datetime.strptime('12/30/2011 10:12:32',
                                                                       "%m/%d/%Y %H:%M:%S").date()
        date_mngr.controller_datetime.time_obj.obj = datetime.datetime.strptime('12/30/2011 10:12:32',
                                                                                "%m/%d/%Y %H:%M:%S").time()

        # Mock get date and time
        get_date_and_time = mock.MagicMock()
        get_date_and_time.get_value_string_by_key = mock.MagicMock(return_value='12/30/2011 10:12:32')
        cn.get_date_and_time = mock.MagicMock(return_value=get_date_and_time)

        # Run method
        cn.verify_date_and_time()

    #################################
    def test_verify_date_and_time_pass2(self):
        """ Verifies the date and the time pass case 2: Controller times are within 60 seconds of each other """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Assign controller date and time, only have them a minute apart maximum
        date_mngr.controller_datetime.obj = datetime.datetime.strptime('12/30/2011 10:12:32',
                                                                       "%m/%d/%Y %H:%M:%S").date()
        date_mngr.controller_datetime.time_obj.obj = datetime.datetime.strptime('12/30/2011 10:12:35',
                                                                                "%m/%d/%Y %H:%M:%S").time()

        # Mock get date and time
        get_date_and_time = mock.MagicMock()
        get_date_and_time.get_value_string_by_key = mock.MagicMock(return_value='12/30/2011 10:12:32')
        cn.get_date_and_time = mock.MagicMock(return_value=get_date_and_time)

        # Run method
        cn.verify_date_and_time()


    #################################
    def test_verify_date_and_time_fail1(self):
        """ Verifies the date and the time fail case 1: Controller times are outside 60 seconds of each other """
        # Create the controller object
        cn = self.create_test_controller_object()

        expected_datetime = datetime.datetime.strptime('12/30/2011 10:12:32', "%m/%d/%Y %H:%M:%S")
        actual_datetime = datetime.datetime.strptime('12/30/2011 10:42:32', "%m/%d/%Y %H:%M:%S")

        # Store the date and time objects
        date_mngr.controller_datetime.obj = datetime.datetime.strptime('12/30/2011 10:12:32',
                                                                       "%m/%d/%Y %H:%M:%S").date()
        date_mngr.controller_datetime.time_obj.obj = datetime.datetime.strptime('12/30/2011 10:42:32',
                                                                                "%m/%d/%Y %H:%M:%S").time()

        # Store the expected error in a variable
        e_msg = "The date and time of the controller didn't match the controller object:\n" \
                "\tController Object Date Time:       \t\t'{0}'\n" \
                "\tDate Time Received From Controller:\t\t'{1}'\n".format(
                actual_datetime,  # {0} The date of the controller object
                expected_datetime  # {1} The date the controller has
        )

        # Mock get date and time
        get_date_and_time = mock.MagicMock()
        get_date_and_time.get_value_string_by_key = mock.MagicMock(return_value='12/30/2011 10:12:32')
        cn.get_date_and_time = mock.MagicMock(return_value=get_date_and_time)

        # Run method, and prepare for a value error
        with self.assertRaises(ValueError) as context:
            cn.verify_date_and_time()

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_verify_code_version_pass1(self):
        """ Verify Code Version On Controller Pass Case 1: Exception is not raised """
        controller = self.create_test_controller_object()
        controller.vr = "2.1"
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,VR=2.1")

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                controller.verify_code_version(mock_data)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_code_version_fail1(self):
        """ Verify Code Version On Controller Fail Case 1: Value on controller does not match what is
        passed in as argument """
        controller = self.create_test_controller_object()
        controller.vr = "2.1"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,VR=2.2")

        expected_message = "Unable to verify Controller's 'Code Version'. Received: 2.2, Expected: 2.1"
        try:
            controller.verify_code_version(mock_data)

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Verifies all attributes for the controller object pass case 1: Verifies everything, should run through
         all tests as the controller will be set to a 3200. The 1000 does not verify flow jumper or pause jumper
         state. Expected status should be something other than none """
        # Create the controller object
        cn = self.create_test_controller_object()

        # Set controller type to 3200
        cn.controller_type = "32"

        # Verify get data
        get_data = mock.MagicMock()
        cn.get_data = get_data

        # Verify date and time
        date_and_time_verifier = mock.MagicMock()
        cn.verify_date_and_time = date_and_time_verifier

        # Verify description
        description_verifier = mock.MagicMock()
        cn.verify_description = description_verifier

        # Verify serial number
        serial_number = mock.MagicMock()
        cn.verify_serial_number = serial_number

        # Verify latitude
        latitude = mock.MagicMock()
        cn.verify_latitude = latitude

        # Verify longitude
        longitude = mock.MagicMock()
        cn.verify_longitude = longitude

        # Verify status
        status_verifier = mock.MagicMock()
        cn.verify_status = status_verifier

        # Verify code version
        code_version = mock.MagicMock()
        cn.verify_code_version = code_version

        # Verify rain jumper state
        rain_jumper = mock.MagicMock()
        cn.verify_rain_jumper_state = rain_jumper

        # Verify flow jumper state
        flow_jumper = mock.MagicMock()
        cn.verify_flow_jumper_state = flow_jumper

        # Verify pause jumper state
        pause_jumper = mock.MagicMock()
        cn.verify_pause_jumper_state = pause_jumper

        # Run program while setting expected status to something other than none
        cn.verify_who_i_am(_expected_status='OK')

    #################################
    def test_init_cn_fail1(self):
        """
        init_cn pass case 1:
        Mock: assertion error for turn of echo
        Raise: AssertionError for turn on echo failure
        Return Message for main AssertionError and turn on echo AssertionError
        """
        # Create controller object
        cn = self.create_test_controller_object()
        exception_msg = "Turn On Echo Test Exception"
        mock_turn_on_echo = mock.MagicMock(side_effect=AssertionError(exception_msg))
        cn.turn_on_echo = mock_turn_on_echo
        with self.assertRaises(AssertionError) as context:
            cn.initialize_for_test()
        e_msg = "Initiate Controller Command Failed: {0}".format(exception_msg)
        print(context.exception.message)
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_init_cn_fail2(self):
        """
        init_cn fail case 1:
        Mock: turn off echo
        Return: None, so it doesnt fail
        Raise: assertion error, for set_sim_mode_to_on
        Return: Message for main AssertionError and AssertionError for set_sim_mode_to_on failure
        """
        # Create controller object
        cn = self.create_test_controller_object()
        exception_msg = "Set Sim Mode To On Test Exception"
        mock_turn_on_echo = mock.MagicMock(side_effect=None)
        cn.turn_on_echo = mock_turn_on_echo
        mock_set_sim_mode_to_on = mock.MagicMock(side_effect=AssertionError(exception_msg))
        cn.set_sim_mode_to_on = mock_set_sim_mode_to_on
        with self.assertRaises(AssertionError) as context:
            cn.initialize_for_test()
        e_msg = "Initiate Controller Command Failed: {0}".format(exception_msg)
        print(context.exception.message)
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_init_cn_fail3(self):
        """
        init cn fail case 2:
        Mock: Turn on echo
        Return: None, so it does not fail
        Mock: Turn sim mode to on
        Return: None, so it does not fail
        Raise: AssertionError for stop_clock
        Return: Message for main AssertionError and AssertionError for stop_clock on failure
        """
        # Create test controller object
        cn = self.create_test_controller_object()
        exception_msg = "Stop Clock Test Exception"

        # Store the expected error
        e_msg = "Initiate Controller Command Failed: {0}".format(exception_msg)

        # Mock turn on echo, no side effect
        mock_turn_on_echo = mock.MagicMock(side_effect=None)
        cn.turn_on_echo = mock_turn_on_echo

        # Mock set sim mode to on, no side effect
        mock_set_sim_mode_to_on = mock.MagicMock(side_effect=None)
        cn.set_sim_mode_to_on = mock_set_sim_mode_to_on

        # Mock stop clock on the controller, return an assertion error
        mock_stop_clock = mock.MagicMock(side_effect=AssertionError(exception_msg))
        cn.stop_clock = mock_stop_clock

        # Run the test and prepare to catch an assertion error
        with self.assertRaises(AssertionError) as context:
            cn.initialize_for_test()

        # print the exception
        print(context.exception.message)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_init_cn_fail4(self):
        """
        init cn fail case 3:
        Mock: turn on echo
        Return: None, so it does not fail
        Mock: set sim mode to on
        Return: None, so it does not fail
        Mock: stop clock
        Return: None, so it does not fail
        Raise: AssertionError for set_date_and_time
        Return: Message for AssertionError for main and AssertionError for set date and time on controller
        """

        # Create test controller object
        cn = self.create_test_controller_object()

        exception_msg = "Set Date and Time Test Exception"

        # Store the expected error
        e_msg = "Initiate Controller Command Failed: {0}".format(exception_msg)

        # Mock turn on echo, no side effect
        mock_turn_on_echo = mock.MagicMock(side_effect=None)
        cn.turn_on_echo = mock_turn_on_echo

        # Mock set sim mode to on, no side effect
        mock_set_sim_mode_to_on = mock.MagicMock(side_effect=None)
        cn.set_sim_mode_to_on = mock_set_sim_mode_to_on

        # Mock stop clock on the controller, no side effect
        mock_stop_clock = mock.MagicMock(side_effect=None)
        cn.stop_clock = mock_stop_clock

        # Mock set date and time on controller, return an assertion error
        mock_set_date_and_time = mock.MagicMock(side_effect=AssertionError(exception_msg))
        cn.set_date_and_time = mock_set_date_and_time

        # Run the test, and catch an assertion error
        with self.assertRaises(AssertionError) as context:
            cn.initialize_for_test()

        print(context.exception.message)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_init_cn_fail5(self):
        """
        init cn fail case 4:
        Create: Test controller object, so the test does not need to use the actual controller
        Mock: turn on echo
        Return: None, so it does not fail
        Mock: set sim mode to on
        Return: None, so it does not fail
        Mock: stop clock
        Return: None, so it does not fail
        Mock: set date and time on controller
        Return: None, so it does not fail
        Mock: Turn on fauxIO
        Raise: AssertionError for fauxIO so the test will fail
        Return: Message for AssertionError for main and AssertionError for fauxIO on controller
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        exception_msg = "Turn On Faux I/O Test Exception"

        # Store expected error message
        e_msg = "Initiate Controller Command Failed: {0}".format(exception_msg)

        # Mock turn on echo, no side effect
        mock_turn_on_echo = mock.MagicMock(side_effect=None)
        cn.turn_on_echo = mock_turn_on_echo

        # Mock set sim mode to on, no side effect
        mock_set_sim_mode_to_on = mock.MagicMock(side_effect=None)
        cn.set_sim_mode_to_on = mock_set_sim_mode_to_on

        # Mock stop clock on the test controller object, no side effect
        mock_stop_clock = mock.MagicMock(side_effect=None)
        cn.stop_clock = mock_stop_clock

        # Mock date and time on the test controller object, no side effect
        mock_set_date_and_time = mock.MagicMock(side_effect=None)
        cn.set_date_and_time = mock_set_date_and_time

        # Mock FauxIO on the test controller object, raise an assertion error
        mock_turn_on_fauxio = mock.MagicMock(side_effect=AssertionError(exception_msg))
        cn.turn_on_faux_io = mock_turn_on_fauxio

        # Catch an AssertionError and run the test
        with self.assertRaises(AssertionError) as context:
            cn.initialize_for_test()

        print(context.exception.message)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_init_cn_fail6(self):
        """
        init cn fail case 5:
        Create: Test controller object, so the test does not need to use the actual controller
        Store: The error message for later comparison
        Mock: turn on echo
        Return: None, so it does not fail
        Mock: set sim mode to on
        Return: None, so it does not fail
        Mock: stop clock
        Return: None, so it does not fail
        Mock: set date and time on controller
        Return: None, so it does not fail
        Mock: Turn on fauxIO
        Return: None, so it will not fail
        Mock: serial send and wait for reply for devices
        Raise: ser.send_and_wait_for_reply for devices assertion error
        Return: Message for AssertionError for main and AssertionError for ser.send_and_wait_for_reply on controller
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        exception_msg = "Send And Wait For Reply Test Exception"

        # Store expected error message
        e_msg = ("Initiate Controller Command Failed: Exception occurred trying Clear"
                 " all Devices command Sent was: DEV,CL=AL")

        # Mock turn on echo, no side effect
        mock_turn_on_echo = mock.MagicMock(side_effect=None)
        cn.turn_on_echo = mock_turn_on_echo

        # Mock set sim mode to on, no side effect
        mock_set_sim_mode_to_on = mock.MagicMock(side_effect=None)
        cn.set_sim_mode_to_on = mock_set_sim_mode_to_on

        # Mock stop clock on the test controller object, no side effect
        mock_stop_clock = mock.MagicMock(side_effect=None)
        cn.stop_clock = mock_stop_clock

        # Mock date and time on the test controller object, no side effect
        mock_set_date_and_time = mock.MagicMock(side_effect=None)
        cn.set_date_and_time = mock_set_date_and_time

        # Mock FauxIO on the test controller object, no side effect
        mock_turn_on_fauxio = mock.MagicMock(side_effect=None)
        cn.turn_on_faux_io = mock_turn_on_fauxio

        # Mock serial send and wait for reply, AssertionError as a side effect
        self.mock_send_and_wait_for_reply.side_effect = AssertionError(exception_msg)

        # Catch an AssertionError and run the test
        with self.assertRaises(AssertionError) as context:
            cn.initialize_for_test()

        print(context.exception.message)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_init_cn_fail7(self):
        """
        init cn fail case 6:
        Create: Test controller object, so the test does not need to use the actual controller
        Store: The error message for later comparison
        Mock: turn on echo
        Return: None, so it does not fail
        Mock: set sim mode to on
        Return: None, so it does not fail
        Mock: stop clock
        Return: None, so it does not fail
        Mock: set date and time on controller
        Return: None, so it does not fail
        Mock: Turn on fauxIO
        Return: None, so it will not fail
        Mock: serial send and wait for reply (clear all for devices)
        Return: None, so it will not fail
        Mock: serial send and wait for reply (clear all for controller)
        Raise: ser.send_and_wait_for_reply assertion error
        Return: Message for AssertionError for main and AssertionError for ser.send_and_wait_for_reply on controller
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        exception_msg = "2nd Send And Wait For Reply Test Exception"

        # Store expected error message
        e_msg = ("Initiate Controller Command Failed: Exception occurred trying Clear all"
                 " Programing command Sent was: DO,CL=AL")

        # Mock turn on echo, no side effect
        mock_turn_on_echo = mock.MagicMock(side_effect=None)
        cn.turn_on_echo = mock_turn_on_echo

        # Mock set sim mode to on, no side effect
        mock_set_sim_mode_to_on = mock.MagicMock(side_effect=None)
        cn.set_sim_mode_to_on = mock_set_sim_mode_to_on

        # Mock stop clock on the test controller object, no side effect
        mock_stop_clock = mock.MagicMock(side_effect=None)
        cn.stop_clock = mock_stop_clock

        # Mock date and time on the test controller object, no side effect
        mock_set_date_and_time = mock.MagicMock(side_effect=None)
        cn.set_date_and_time = mock_set_date_and_time

        # Mock FauxIO on the test controller object, no side effect
        mock_turn_on_fauxio = mock.MagicMock(side_effect=None)
        cn.turn_on_faux_io = mock_turn_on_fauxio

        # Mock serial send and wait for reply, None for the first side effect, then AssertionError as a second
        # side effect
        self.mock_send_and_wait_for_reply.side_effect = [None, AssertionError(exception_msg)]

        # Catch an AssertionError and run the test
        with self.assertRaises(AssertionError) as context:
            cn.initialize_for_test()

        print(context.exception.message)

        # Compare expected error to actual error
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_turn_on_faux_io_pass1(self):
        """ Turn On Faux IO Pass Case 1: Communication with controller is successful """
        controller = self.create_test_controller_object()

        expected_command = "DO,FX=TR"
        controller.turn_on_faux_io()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_turn_on_faux_io_fail1(self):
        """ Turn On Faux IO Fail Case 3: Failed Communication with the controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception("Test Exception")

        with self.assertRaises(Exception) as context:
            controller.turn_on_faux_io()
        e_msg = "Exception occurred trying to Enable Faux IO Devices Command Sent was: DO,FX=TR"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_turn_on_echo_pass1(self):
        """
        turn_on_echo pass case 1:
        Create: Test controller object
        Store: Expected command
        Run: Test
        Mock: Serial send and wait for reply
        Return: Nothing, test only shows that expected command was sent
        """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store the expected command
        expected_command = 'DO,EC=TR'

        # Run the test
        cn.turn_on_echo()

        # Mock serial and wait for reply
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_turn_on_echo_fail1(self):
        """
        turn_on_echo fail case 1:
        Create: Test controller object
        Store: Expected exception
        Mock: Serial send and wait for reply, have a side effect of exception
        Run: Test
        Compare: Expected exception to actual exception
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected exception
        e_msg = "Exception occurred trying to turn on Echo command Sent was: DO,EC=TR"

        # Mock serial
        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        # Run test, and expect an exception
        with self.assertRaises(Exception) as context:
            cn.turn_on_echo()

        # Compare actual exception to expected exception
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_turn_off_echo_pass1(self):
        """
        turn_off_echo pass case 1:
        Create: Test controller object
        Store: Expected command
        Run: Test
        Mock: Serial send and wait for reply
        Return: Nothing, test only shows that expected command was sent
        """
        # Create controller object
        cn = self.create_test_controller_object()

        # Store the expected command
        expected_command = 'DO,EC=FA'

        # Run the test
        cn.turn_off_echo()

        # Mock serial and wait for reply
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_turn_off_echo_fail1(self):
        """
        turn_off_echo fail case 1:
        Create: Test controller object
        Store: Expected exception
        Mock: Serial send and wait for reply, have a side effect of exception
        Run: Test
        Compare: Expected exception to actual exception
        """
        # Create test controller object
        cn = self.create_test_controller_object()

        # Store expected exception
        e_msg = "Exception occurred trying to turn off Echo command Sent was: DO,EC=FA"

        # Mock serial
        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        # Run test, and expect an exception
        with self.assertRaises(Exception) as context:
            cn.turn_off_echo()

        # Compare actual exception to expected exception
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_stop_clock_pass1(self):
        """ Stop Clock Pass Case 1: Communication with controller is successful """
        controller = self.create_test_controller_object()

        expected_command = "DO,CK,TM=0"
        controller.stop_clock()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_stop_clock_fail1(self):
        """ Stop Clock Fail Case 1: Failed Communication with the controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.stop_clock()
        e_msg = "Exception occurred trying to Stop the Clock command Sent was: DO,CK,TM=0"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_start_clock_pass1(self):
        """ Start Clock Pass Case 1: Communication with controller is successful """
        controller = self.create_test_controller_object()

        expected_command = "DO,CK,TM=-1"
        controller.start_clock()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_start_clock_fail1(self):
        """ Start Clock Fail Case 1: Failed Communication with the controller """
        controller = self.create_test_controller_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            controller.start_clock()
        e_msg = "Exception occurred trying to Start the Clock command Sent was: DO,CK,TM=-1"
        self.assertEqual(first=e_msg, second=context.exception.message)

        #################################
    def test_get_water_source_pass1(self):
        """ test getting a valid water source on a controller. Serial port of Water Source is different than
        the serial port of the controller. The Water Source is on a FlowStation, so the Identifier should be the Flow Station
        Slot number.
        """
        controller = self.create_test_controller_object()
        controller.controller_type = opcodes.flow_station
        water_source_controller = mock.MagicMock()
        water_source_controller.ser = mock.MagicMock()
        fs_water_source_slot = 1
        controller_water_source_slot = 2
        test_water_source = WaterSource(water_source_controller, controller_water_source_slot)
        test_water_source.flow_station_slot_number = fs_water_source_slot
        test_water_source.sh = opcodes.true

        controller.water_sources[fs_water_source_slot] = test_water_source

        self.assertEquals(test_water_source.identifiers[0][1], controller_water_source_slot)
        controller.get_water_source(fs_water_source_slot)

        self.assertEquals(controller.ser, test_water_source.ser)
        self.assertEquals(test_water_source.identifiers[0][1], fs_water_source_slot)

        #################################
    def test_get_water_source_pass2(self):
        """ test getting a valid water source on a controller. Serial port of Water Source is the same as
        the serial port of the controller. The Water Source is on a FlowStation. Identifier should be the
        Flow Station slot address.
        """
        controller = self.create_test_controller_object()
        controller.controller_type = opcodes.flow_station
        fs_water_source_slot = 1
        controller_water_source_slot = 2
        test_water_source = WaterSource(controller, controller_water_source_slot)
        test_water_source.flow_station_slot_number = fs_water_source_slot
        test_water_source.sh = opcodes.true

        controller.water_sources[fs_water_source_slot] = test_water_source

        controller.get_water_source(fs_water_source_slot)

        self.assertEquals(controller.ser, test_water_source.ser)
        self.assertEquals(test_water_source.identifiers[0][1], fs_water_source_slot)

        #################################
    def test_get_water_source_pass3(self):
        """ test getting a valid water source on a controller. Serial port of Water Source is the same as
        the serial port of the controller. The Water Source is not shared. Identifier should be the same as
        the address of the address of the Water Source.
        """
        controller = self.create_test_controller_object()
        fs_water_source_slot = 1
        controller_water_source_slot = 2
        test_water_source = WaterSource(controller, controller_water_source_slot)
        test_water_source.flow_station_slot_number = fs_water_source_slot
        test_water_source.sh = opcodes.false

        controller.water_sources[fs_water_source_slot] = test_water_source

        controller.get_water_source(fs_water_source_slot)

        self.assertEquals(controller.ser, test_water_source.ser)
        self.assertEquals(test_water_source.identifiers[0][1], test_water_source.ad)

    #################################
    def test_get_water_source_fail1(self):
        """ Test exception when the Water Source is not in the Controller Water Source dictionary """
        controller = self.create_test_controller_object()
        controller_water_source_slot = 1

        e_msg = "Could not find Water Source {0} in the water_sources dictionary for controller: {1}".format(
            controller_water_source_slot,       # {0}
            controller.mac                      # {1}
        )

        with self.assertRaises(KeyError) as context:
            controller.get_water_source(controller_water_source_slot)

        self.assertEqual(first=e_msg, second=context.exception.message)

        #################################
    def test_get_point_of_control_pass1(self):
        """ test getting a valid Point of Control on a controller. Serial port of the Point of Control is different than
        the serial port of the controller. The Point of Control is on a FlowStation, so the Identifier should be the Flow Station
        Slot number.
        """
        controller = self.create_test_controller_object()
        controller.controller_type = opcodes.flow_station
        poc_controller = mock.MagicMock()
        poc_controller.ser = mock.MagicMock()
        fs_poc_slot = 1
        controller_poc_slot = 2
        test_poc = PointOfControl(poc_controller, controller_poc_slot)
        test_poc.flow_station_slot_number = fs_poc_slot
        test_poc.sh = opcodes.true

        controller.points_of_control[fs_poc_slot] = test_poc

        self.assertEquals(test_poc.identifiers[0][1], controller_poc_slot)
        controller.get_point_of_control(fs_poc_slot)

        self.assertEquals(controller.ser, test_poc.ser)
        self.assertEquals(test_poc.identifiers[0][1], fs_poc_slot)

        #################################
    def test_get_point_of_control_pass2(self):
        """ test getting a valid Point of Control on a controller. Serial port of Point of Control is the same as
        the serial port of the controller. The Point of Control is on a FlowStation. Identifier should be the
        Flow Station slot address.
        """
        controller = self.create_test_controller_object()
        controller.controller_type = opcodes.flow_station
        fs_poc_slot = 1
        controller_poc_slot = 2
        test_poc = PointOfControl(controller, controller_poc_slot)
        test_poc.flow_station_slot_number = fs_poc_slot
        test_poc.sh = opcodes.true

        controller.points_of_control[fs_poc_slot] = test_poc

        controller.get_point_of_control(fs_poc_slot)

        self.assertEquals(controller.ser, test_poc.ser)
        self.assertEquals(test_poc.identifiers[0][1], fs_poc_slot)

        #################################
    def test_get_point_of_control_pass3(self):
        """ test getting a valid Point of Control on a controller. Serial port of Point of Control is the same as
        the serial port of the controller. The Point of Control is not shared. Identifier should be the same as
        the address of the address of the Water Source.
        """
        controller = self.create_test_controller_object()
        fs_poc_slot = 1
        controller_poc_slot = 2
        test_poc = PointOfControl(controller, controller_poc_slot)
        test_poc.flow_station_slot_number = fs_poc_slot
        test_poc.sh = opcodes.false

        controller.points_of_control[fs_poc_slot] = test_poc

        controller.get_point_of_control(fs_poc_slot)

        self.assertEquals(controller.ser, test_poc.ser)
        self.assertEquals(test_poc.identifiers[0][1], test_poc.ad)

    #################################
    def test_get_point_of_control_fail1(self):
        """ Test exception when the Point of Control is not in the Controller point_of_control dictionary """
        controller = self.create_test_controller_object()
        controller_poc_slot = 1

        e_msg = "Could not find Point of Control {0} in the points_of_control dictionary for controller: {1}".format(
            controller_poc_slot,        # {0}
            controller.mac              # {1}
        )

        with self.assertRaises(KeyError) as context:
            controller.get_point_of_control(controller_poc_slot)

        self.assertEqual(first=e_msg, second=context.exception.message)

        #################################
    def test_get_mainline_pass1(self):
        """ test getting a valid Mainline on a controller. Serial port of the Mainline is different than
        the serial port of the controller. The Mainline is on a FlowStation, so the Identifier should be the Flow Station
        Slot number.
        """
        controller = self.create_test_controller_object()
        controller.controller_type = opcodes.flow_station
        mainline_controller = mock.MagicMock()
        mainline_controller.ser = mock.MagicMock()
        fs_mainline_slot = 1
        controller_mainline_slot = 2
        test_mainline = Mainline(mainline_controller, controller_mainline_slot)
        test_mainline.flow_station_slot_number = fs_mainline_slot
        test_mainline.sh = opcodes.true

        controller.mainlines[fs_mainline_slot] = test_mainline

        self.assertEquals(test_mainline.identifiers[0][1], controller_mainline_slot)
        controller.get_mainline(fs_mainline_slot)

        self.assertEquals(controller.ser, test_mainline.ser)
        self.assertEquals(test_mainline.identifiers[0][1], fs_mainline_slot)

        #################################
    def test_get_mainline_pass2(self):
        """ test getting a valid Mainline on a controller. Serial port of the Mainline is the same as
        the serial port of the controller. The Mainline is on a FlowStation. Identifier should be the
        Flow Station slot address.
        """
        controller = self.create_test_controller_object()
        controller.controller_type = opcodes.flow_station
        fs_mainline_slot = 1
        controller_mainline_slot = 2
        test_mainline = Mainline(controller, controller_mainline_slot)
        test_mainline.flow_station_slot_number = fs_mainline_slot
        test_mainline.sh = opcodes.true

        controller.mainlines[fs_mainline_slot] = test_mainline

        controller.get_mainline(fs_mainline_slot)

        self.assertEquals(controller.ser, test_mainline.ser)
        self.assertEquals(test_mainline.identifiers[0][1], fs_mainline_slot)

    #################################
    def test_get_mainline_pass3(self):
        """ test getting a valid Mainline on a controller. Serial port of the Mainline is the same as
        the serial port of the controller. The Mainline is not shared. Identifier should be the same as
        the address of the Mainline.
        """
        controller = self.create_test_controller_object()
        fs_mainline_slot = 1
        controller_mainline_slot = 2
        test_mainline = PointOfControl(controller, controller_mainline_slot)
        test_mainline.flow_station_slot_number = fs_mainline_slot
        test_mainline.sh = opcodes.false

        controller.mainlines[fs_mainline_slot] = test_mainline

        controller.get_mainline(fs_mainline_slot)

        self.assertEquals(controller.ser, test_mainline.ser)
        self.assertEquals(test_mainline.identifiers[0][1], test_mainline.ad)

    #################################
    def test_get_mainline_fail1(self):
        """ Test exception when the Mainline is not in the Controller mainlines dictionary """
        controller = self.create_test_controller_object()
        controller_mainline_slot = 1

        e_msg = "Could not find Mainline {0} in the mainlines dictionary for controller: {1}".format(
            controller_mainline_slot,        # {0}
            controller.mac              # {1}
        )

        with self.assertRaises(KeyError) as context:
            controller.get_mainline(controller_mainline_slot)

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    @mock.patch('file_transfer_handler.get_latest_firmware_version_available', return_value=True)
    def test_check_firmware_version_pass1(self, mocked_get_latest_firmware):
        """Check Firmware Version called with an argument that has a different firmware version than the controller"""
        # Create Initial variables
        controller = self.create_test_controller_object()   # controller to run the method
        new_firmware_version = "1.2.3"                      # firmware version that is different than the controller's
        controllers_firmware = "1.4.9"                      # firmware version on the controller to check against

        # Mocked values that we aren't explicitly testing
        controller.data.get_value_string_by_key = mock.MagicMock(return_value=controllers_firmware)
        mocked_get_latest_firmware.return_value = new_firmware_version

        # Return True because we expect the firmware version needs to update since it is different than the controller
        expected_result = True
        actual_result = controller.check_firmware_version(_firmware_version=new_firmware_version)

        self.assertEqual(expected_result, actual_result)

    #################################
    @mock.patch('file_transfer_handler.get_latest_firmware_version_available', return_value=True)
    def test_check_firmware_version_pass2(self, mocked_get_latest_firmware):
        """Check Firmware Version called with an argument that has latest firmware version"""
        # Create Initial variables
        controller = self.create_test_controller_object()   # controller to run the method
        string_to_update_to_latest = "latest"               # this triggers to the controller to update to latest fw
        new_firmware_version = "1.2.3"                      # firmware version that is different than the controller's
        controllers_firmware = "1.4.9"                      # firmware version on the controller to check against

        # Mocked values that we aren't explicitly testing
        controller.data.get_value_string_by_key = mock.MagicMock(return_value=controllers_firmware)
        mocked_get_latest_firmware.return_value = new_firmware_version

        # Return True because we expect the controller doesn't have the latest version of firmware, and needs updating
        expected_result = True
        actual_result = controller.check_firmware_version(_firmware_version=string_to_update_to_latest)

        self.assertEqual(expected_result, actual_result)

    #################################
    def test_check_firmware_version_pass3(self):
        """Check Firmware Version called with an empty argument, signifying to update shouldn't happen"""
        # Create Initial variables
        controller = self.create_test_controller_object()   # controller to run the method
        string_to_not_update = ""                           # this tells the controller not to update
        controllers_firmware = "1.4.9"                      # firmware version on the controller to check against

        # Mocked values that we aren't explicitly testing
        controller.data.get_value_string_by_key = mock.MagicMock(return_value=controllers_firmware)

        # Return False because no firmware version was specified for updating
        expected_result = False
        actual_result = controller.check_firmware_version(_firmware_version=string_to_not_update)

        self.assertEqual(expected_result, actual_result)

    #################################
    @mock.patch('file_transfer_handler.get_latest_firmware_version_available', return_value=True)
    def test_check_firmware_version_fail1(self, mocked_get_latest_firmware):
        """Check Firmware Version called with an argument that has the same firmware version than the controller"""
        # Create Initial variables
        controller = self.create_test_controller_object()   # controller to run the method
        new_firmware_version = "1.2.3"                      # firmware version that is different than the controller's
        controllers_firmware = "1.2.3"                      # firmware version on the controller to check against

        # Mocked values that we aren't explicitly testing
        controller.data.get_value_string_by_key = mock.MagicMock(return_value=controllers_firmware)
        mocked_get_latest_firmware.return_value = new_firmware_version

        # Return False because the controller has the same firmware version that the user specified they want
        expected_result = False
        actual_result = controller.check_firmware_version(_firmware_version=new_firmware_version)

        self.assertEqual(expected_result, actual_result)

    #################################
    @mock.patch('file_transfer_handler.get_latest_firmware_version_available', return_value=True)
    def test_check_firmware_version_fail2(self, mocked_get_latest_firmware):
        """Check Firmware Version called with latest firmware version when the controller is up to date"""
        # Create Initial variables
        controller = self.create_test_controller_object()   # controller to run the method
        string_to_update_to_latest = "latest"               # this triggers to the controller to update to latest fw
        new_firmware_version = "1.2.3"                      # firmware version that is different than the controller's
        controllers_firmware = "1.2.3"                      # firmware version on the controller to check against

        # Mocked values that we aren't explicitly testing
        controller.data.get_value_string_by_key = mock.MagicMock(return_value=controllers_firmware)
        mocked_get_latest_firmware.return_value = new_firmware_version

        # Return False because the controller already has the latest version of the firmware
        expected_result = False
        actual_result = controller.check_firmware_version(_firmware_version=string_to_update_to_latest)

        self.assertEqual(expected_result, actual_result)

    #################################
    @mock.patch('file_transfer_handler.get_latest_firmware_version_available', return_value=True)
    def test_check_firmware_version_fail3(self, mocked_get_latest_firmware):
        """Check Firmware Version called with a new firmware version but the zipped file that contains the firmware
        has a different version number in it's version.txt"""
        # Create Initial variables
        controller = self.create_test_controller_object()   # controller to run the method
        new_firmware_version = "1.2.3"                      # firmware version that is different than the controller's
        controllers_firmware = "1.4.9"                      # firmware version on the controller to check against
        incorrect_firmware_returned = "4.5.6"               # firmware version returned from the zip file

        # Mocked values that we aren't explicitly testing
        controller.data.get_value_string_by_key = mock.MagicMock(return_value=controllers_firmware)
        mocked_get_latest_firmware.return_value = incorrect_firmware_returned

        # Throw an exception because the file that has the firmware version we need has an incorrect version.txt
        expected_message = "Firmware in file doesnt match the firmware version passed in: Passed in firmware version " \
                           "{0} file contained firmware versions {1}".format(
                                new_firmware_version,
                                incorrect_firmware_returned
                            )
        with self.assertRaises(Exception) as context:
            controller.check_firmware_version(_firmware_version=new_firmware_version)

        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_check_and_update_firmware_pass1(self):
        """Check and Update Firmware called by 3200 with a request for the latest firmware when version > v16"""
        # Create Initial variables
        controller = self.create_test_controller_object()   # controller to run the method
        controller.controller_type = "32"                   # say the controller is a 3200 for the sake of testing
        controller.vr = "16.2.3"                             # firmware version on the controller to check against

        # Mocked values that we aren't explicitly testing
        controller.data.get_value_string_by_key = mock.MagicMock(return_value=controller.vr)
        firmware_update_mock = mock.MagicMock()
        controller.do_firmware_update = firmware_update_mock
        controller.check_firmware_version = mock.MagicMock(return_value=True)   # Return true to do firmware update

        # Know what variables were passed into do_firmware_update
        first_parameter = opcodes.local_directory
        second_parameter = "common" + os.sep + "firmware_update_files"
        third_parameter = "BL_32_vlatest"
        fourth_parameter = True

        # Call the method being tested
        controller.check_and_update_firmware(_firmware_version="latest", unit_testing=True)

        # Confirm that a do firmware update was called with the correct parameters
        firmware_update_mock.assert_called_with(were_from=first_parameter, directory=second_parameter,
                                                file_name=third_parameter, unit_testing=True)

    #################################
    def test_check_and_update_firmware_pass2(self):
        """Check and Update Firmware called by 3200 with a request for the latest firmware when version < v16"""
        # Create Initial variables
        controller = self.create_test_controller_object()   # controller to run the method
        controller.controller_type = "32"                   # say the controller is a 3200 for the sake of testing
        new_firmware_version = "16.4.9"                     # firmware version that is different than the controller's
        controller.vr = "12.34"                             # firmware version on the controller to check against

        # Mocked values that we aren't explicitly testing
        controller.data.get_value_string_by_key = mock.MagicMock(return_value=controller.vr)
        firmware_update_mock = mock.MagicMock()
        controller.add_basemanager_connection_to_controller = mock.MagicMock()
        controller.do_firmware_update = firmware_update_mock
        controller.check_firmware_version = mock.MagicMock(return_value=True)   # Return true to do firmware update

        # Know what variables were passed into do_firmware_update
        first_parameter = opcodes.latest_3200_basemanager_firmware_id
        second_parameter = opcodes.basemanager
        third_parameter = True

        # Call the method being tested
        controller.check_and_update_firmware(_firmware_version="latest", unit_testing=True)

        # Confirm that a do firmware update was called with the correct parameters
        firmware_update_mock.assert_called_with(bm_id_number=first_parameter, were_from=second_parameter,
                                                unit_testing=third_parameter)

    #################################
    def test_check_and_update_firmware_pass3(self):
        """Check and Update Firmware called by 3200 with a request for a firmware version not on the controller, and
        also not the latest"""
        # Create Initial variables
        controller = self.create_test_controller_object()   # controller to run the method
        controller.controller_type = "32"                   # say the controller is a 3200 for the sake of testing
        new_firmware_version = "1.4.9"                      # firmware version that is different than the controller's
        controller.vr = "1.2.3"                             # firmware version on the controller to check against

        # Mocked values that we aren't explicitly testing
        controller.data.get_value_string_by_key = mock.MagicMock(return_value=controller.vr)
        firmware_update_mock = mock.MagicMock()
        controller.do_firmware_update = firmware_update_mock
        controller.check_firmware_version = mock.MagicMock(return_value=True)   # Return true to do firmware update

        # Know what variables were passed into do_firmware_update
        first_parameter = opcodes.local_directory
        second_parameter = "common" + os.sep + "firmware_update_files"
        third_parameter = "BL_32_v{0}".format(new_firmware_version)
        fourth_parameter = True

        # Call the method being tested
        controller.check_and_update_firmware(_firmware_version=new_firmware_version, unit_testing=True)

        # Confirm that a do firmware update was called with the correct parameters
        firmware_update_mock.assert_called_with(were_from=first_parameter, directory=second_parameter,
                                                file_name=third_parameter, unit_testing=True)

    #################################
    def test_update_identifiers_pass1(self):
        """  This will test setting the identifiers object for a test engine object when controller_type is FlowStation. The
        identifiers should be the flow station slot number
        """
        controller = self.create_test_controller_object()
        controller.controller_type = opcodes.flow_station
        controller_slot_number = 1
        flowstation_slot_number = 5
        test_engine_object = WaterSource(_controller=controller, _ad=controller_slot_number)
        test_engine_object.sh = opcodes.true
        test_engine_object.flow_station_slot_number = flowstation_slot_number

        controller.update_identifiers(_test_engine_object=test_engine_object)

        self.assertEquals(test_engine_object.identifiers[0][1], flowstation_slot_number)

    #################################
    def test_update_identifiers_pass2(self):
        """  This will test setting the identifiers object for a test engine object when self.sh is False. The
        identifiers should be the object address
        """
        controller = self.create_test_controller_object()
        controller_slot_number = 1
        flowstation_slot_number = 5
        test_engine_object = WaterSource(_controller=controller, _ad=controller_slot_number)
        test_engine_object.sh = opcodes.false
        test_engine_object.flow_station_slot_number = flowstation_slot_number

        controller.update_identifiers(_test_engine_object=test_engine_object)

        self.assertEquals(test_engine_object.identifiers[0][1], test_engine_object.ad)

    if __name__ == "__main__":
        unittest.main()