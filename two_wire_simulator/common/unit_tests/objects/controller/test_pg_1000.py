__author__ = 'Brice "Ajo Grande" Garlick'

import unittest
import mock
import serial
import status_parser

import common.objects.base_classes.poc as poc
from common.imports import opcodes
from common.objects.base_classes.programs import BasePrograms
from common.objects.programming.mv import MasterValve
from common.objects.programming.poc_1000 import POC1000
from common.objects.programming.pg_1000 import PG1000
from common.objects.object_bucket import *
from common.epa_package.wbw_imports import WaterSenseCodes


class TestPG1000Object(unittest.TestCase):
    """   """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        BasePrograms.ser = self.mock_ser
        poc.POC.ser = self.mock_ser

        master_valves[3] = MasterValve("MVD0001", 3)
        poc10[2] = POC1000(2)

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # when these objects are created in this test, they carry over to any test after this one because they are
        # just pointers and not test variables specific to just THIS module.
        del master_valves[3]
        del poc10[2]

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_prog_1000_object(self, _ww=1):
        """
        Creates a new Master Valve object for testing purposes
        :param _ww:
        :type _ww:
        :return:
        :rtype: PG1000
        """
        if _ww == 1:
            prog = PG1000(_address=1, _description="Test Program 1", _point_of_connection_address=[2],
                          _master_valve_address=3, _program_cycles=1)
        else:
            ww_value = ['011111111111111111111111',
                        '001111111111111111111111',
                        '000111111111111111111111',
                        '000011111111111111111111',
                        '000001111111111111111111',
                        '000000111111111111111111',
                        '000000011111111111111111'
                        ]
            prog = PG1000(_address=1, _description="Test Program 1", _water_window=ww_value,
                          _point_of_connection_address=[2], _master_valve_address=3, _program_cycles=1)

        return prog

    #################################
    def test_send_programming_to_cn_pass1(self):
        """ Send Programming To Controller Pass Case 1: Correct Command Sent with 1 water window value """
        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "WW=111111111111111111111111," \
                           "MC=1," \
                           "SA=100," \
                           "PC=2," \
                           "SK=DS," \
                           "CT=2," \
                           "SO=600," \
                           "EE=FA," \
                           "MV=MVD0001," \
                           "CY=1"
        prog = self.create_test_prog_1000_object()

        prog.send_programming_to_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_send_programming_to_cn_pass2(self):
        """ Send Programming To Controller Pass Case 2: Correct Command Sent with 7 water window values """
        ww_value = ['011111111111111111111111',
                    '001111111111111111111111',
                    '000111111111111111111111',
                    '000011111111111111111111',
                    '000001111111111111111111',
                    '000000111111111111111111',
                    '000000011111111111111111'
                    ]

        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13}," \
                           "MC=1," \
                           "SA=100," \
                           "PC=2," \
                           "SK=DS," \
                           "CT=2," \
                           "SO=600,"\
                           "EE=FA," \
                           "MV=MVD0001," \
                           "CY=1".format(opcodes.sunday, str(ww_value[0]),
                                         opcodes.monday, str(ww_value[1]),
                                         opcodes.tuesday, str(ww_value[2]),
                                         opcodes.wednesday, str(ww_value[3]),
                                         opcodes.thursday, str(ww_value[4]),
                                         opcodes.friday, str(ww_value[5]),
                                         opcodes.saturday, str(ww_value[6]),
                                         )
        prog = self.create_test_prog_1000_object(_ww=7)

        prog.send_programming_to_cn()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_send_programming_to_cn_fail1(self):
        """ Send Programming To Controller Fail Case 1: Removed comma so that we're sending in a bad packet and getting
        a correct error """
        prog = self.create_test_prog_1000_object()

        expected_command = "SET," \
                           "PG=1," \
                           "EN=TR," \
                           "DS=Test Program 1," \
                           "WW=111111111111111111111111," \
                           "MC=1," \
                           "SA=100," \
                           "PC=2," \
                           "SK=DS," \
                           "CT=2," \
                           "SO=600," \
                           "EE=FA," \
                           "MV=MVD0001," \
                           "CY=1"

        e_msg = "Exception occurred trying to set 1000 Program 1's 'Values' to: '{0}'\n -> ".format(expected_command)

        self.mock_send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.send_programming_to_cn()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_point_of_connection_pass1(self):
        """ Set POC On Controller Pass Case 1: Passed in address of a present Mainline """
        prog = self.create_test_prog_1000_object()

        expected_value = 2
        prog.set_point_of_connection(expected_value)

        this_obj = prog.poc_objects[2]
        actual_value = this_obj.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_point_of_connection_pass2(self):
        """ Set POC On Controller Pass Case 2: Command with correct values sent to controller """
        prog = self.create_test_prog_1000_object()

        this_obj = prog.poc_objects[2]
        # sn_value = this_obj.sn
        ad_value = this_obj.ad
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.point_of_connection,
                                                      str(ad_value))

        prog.set_point_of_connection(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_point_of_connection_fail1(self):
        """ Set POC On Controller Fail Case 1: Pass an address of Moisture Sensor that is not present """
        prog = self.create_test_prog_1000_object()

        new_ad_value = [4]
        with self.assertRaises(Exception) as context:
            prog.set_point_of_connection(new_ad_value)
        expected_message = "Invalid Point of Connection address for 'SET' for 1000 Program 1. Valid address are: " \
                           "1 - 3. Received: {0}".format(str(new_ad_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_point_of_connection_fail2(self):
        """ Set POC On Controller Fail Case 2: Failed communication with controller """
        prog = self.create_test_prog_1000_object()
        this_obj = prog.poc_objects[2]
        # sn_value = this_obj.sn
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.set_point_of_connection(ad_value)
        e_msg = "Exception occurred trying to set 1000 Program 1's 'Point of Connection' to: [{0}] " \
                "-> ".format(str(ad_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_master_valve_pass1(self):
        """ Set Master Valve On Controller Pass Case 1: Passed in address of a present Master Valve """
        prog = self.create_test_prog_1000_object()

        expected_value = 3
        prog.set_master_valve(expected_value)

        this_obj = prog.mv_objects[3]
        actual_value = this_obj.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_master_valve_pass2(self):
        """ Set Master Valve On Controller Pass Case 2: Command with correct values sent to controller """
        prog = self.create_test_prog_1000_object()

        this_obj = prog.mv_objects[3]
        sn_value = this_obj.sn
        ad_value = this_obj.ad
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.master_valve,
                                                      str(sn_value))
        prog.set_master_valve(ad_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_master_valve_fail1(self):
        """ Set Master Valve On Controller Fail Case 1: Pass an address of Master Valve that is not present """
        prog = self.create_test_prog_1000_object()

        new_ad_value = 2
        with self.assertRaises(Exception) as context:
            prog.set_master_valve(new_ad_value)
        expected_message = "Invalid Master Valve address for 'SET' for 1000 Program 1. Valid address are: {0}. " \
                           "Received: {1}".format(prog.mv_objects.keys(), str(new_ad_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_master_valve_fail2(self):
        """ Set Master Valve On Controller Fail Case 2: Failed communication with controller """
        prog = self.create_test_prog_1000_object()
        this_obj = prog.mv_objects[3]
        ad_value = this_obj.ad

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.set_master_valve(ad_value)
        e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Master Valve' to: {1}({2}) " \
                "-> ".format(str(prog.ad), str(this_obj.sn), str(this_obj.ad))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_soak_cycle_mode_pass1(self):
        """ Set Soak Cycle Mode On Controller Pass Case 1: Using Default Value """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.soak_cycle_mode, opcodes.disabled)
        prog = self.create_test_prog_1000_object()
        prog.set_soak_cycle_mode("DS")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_soak_cycle_mode_pass2(self):
        """ Set Soak Cycle Mode On Controller Pass Case 2: Using Passed In Argument """
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.soak_cycle_mode,
                                                      opcodes.intelligent_soak)
        prog = self.create_test_prog_1000_object()
        prog.set_soak_cycle_mode("SM")
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_soak_cycle_mode_fail1(self):
        """ Set Soak Cycle Mode On Controller Fail Case 1: Invalid State Argument """
        prog = self.create_test_prog_1000_object()

        valid_modes = [opcodes.disabled, opcodes.intelligent_soak, opcodes.program_cycles, opcodes.zone_soak_cycles]

        with self.assertRaises(ValueError) as context:
            prog.set_soak_cycle_mode("Foo")
        e_msg = "Invalid mode passed in for 'SET' Soak Cycle Mode for 1000 Program {0}. Valid modes: " \
                "{1}, received: Foo".format(prog.ad,        # {0}
                                            valid_modes   # {1}
                                            )
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_soak_cycle_mode_fail2(self):
        """ Set Soak Cycle Mode On Controller Fail Case 2: Failed Communication With Controller """
        prog = self.create_test_prog_1000_object()

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.set_soak_cycle_mode("DS")
        e_msg = "Exception occurred trying to set 1000 Program 1's 'Soak Cycle Mode' to: DS"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_cycle_count_pass1(self):
        """ Set Cycle Count On Controller Pass Case 1: Setting new _ct value = 6.0, check that object
        variable _ct is set """
        prog = self.create_test_prog_1000_object()

        new_value = 6
        prog.set_cycle_count(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = prog.ct
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_cycle_count_pass2(self):
        """ Set Cycle Count On Controller Pass Case 2: Command with correct values sent to controller """
        prog = self.create_test_prog_1000_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.cycle_count,
                                                      str(new_value))
        prog.set_cycle_count(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_cycle_count_fail1(self):
        """ Set Cycle Count On Controller Fail Case 1: Failed communication with controller """
        prog = self.create_test_prog_1000_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.set_cycle_count(new_value)
        e_msg = "Exception occurred trying to set 1000 Program 1's 'Cycle Count' to: {0} -> ".format(
                str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_cycle_count_fail2(self):
        """ Set Cycle Count On Controller Fail Case 2: Trying to set _ct value to outside of acceptable range - low"""
        prog = self.create_test_prog_1000_object()
        new_value = 1

        with self.assertRaises(Exception) as context:
            prog.set_cycle_count(new_value)
        e_msg = "Invalid cycle count value passed in for 'SET' Cycle Count for 1000 Program 1. Valid values: " \
                "2 - 100, received: {0}".format(str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_cycle_count_fail3(self):
        """ Set Cycle Count On Controller Fail Case 3: Trying to set _ct value to outside of acceptable range - high"""
        prog = self.create_test_prog_1000_object()
        new_value = 101

        with self.assertRaises(Exception) as context:
            prog.set_cycle_count(new_value)
        e_msg = "Invalid cycle count value passed in for 'SET' Cycle Count for 1000 Program 1. Valid values: " \
                "2 - 100, received: {0}".format(str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_soak_time_in_seconds_pass1(self):
        """ Set Cycle Count On Controller Pass Case 1: Setting new _so value = 6.0, check that object
        variable _so is set """
        prog = self.create_test_prog_1000_object()

        new_value = 6
        prog.set_soak_time_in_seconds(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = prog.so
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_soak_time_in_seconds_pass2(self):
        """ Set Cycle Count On Controller Pass Case 2: Command with correct values sent to controller """
        prog = self.create_test_prog_1000_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.soak_cycle,
                                                      str(new_value))
        prog.set_soak_time_in_seconds(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_soak_time_in_seconds_fail1(self):
        """ Set Cycle Count On Controller Fail Case 1: Failed communication with controller """
        prog = self.create_test_prog_1000_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.set_soak_time_in_seconds(new_value)
        e_msg = "Exception occurred trying to set 1000 Program 1's 'Soak Time' to: {0} seconds -> ".format(
                str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_program_cycles_pass1(self):
        """ Set Program Cycles On Controller Pass Case 1: Setting new _cy value = 6.0, check that object
        variable _cy is set """
        prog = self.create_test_prog_1000_object()

        new_value = 6
        prog.set_program_cycles(new_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = prog.cy
        self.assertEqual(new_value, actual_value)

    #################################
    def test_set_program_cycles_pass2(self):
        """ Set Program Cycles On Controller Pass Case 2: Command with correct values sent to controller """
        prog = self.create_test_prog_1000_object()

        new_value = 6
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, opcodes.program_cycles,
                                                      str(new_value))
        prog.set_program_cycles(new_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_program_cycles_fail1(self):
        """ Set Program Cycles On Controller Fail Case 1: Failed communication with controller """
        prog = self.create_test_prog_1000_object()
        new_value = 6

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            prog.set_program_cycles(new_value)
        e_msg = "Exception occurred trying to set 1000 Program 1's 'Program Cycles' to: {0} cycles -> ".format(
                str(new_value))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_enable_et_state_pass1(self):
        """ Verifies valid set enable et state for program on controller with no arguments """
        prog = self.create_test_prog_1000_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = None

        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, WaterSenseCodes.Enable_ET_Runtime, opcodes.false)

        prog.set_enable_et_state()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_et_state_pass2(self):
        """ Verifies valid set enable et state for program on controller with new state argument """
        prog = self.create_test_prog_1000_object()
        new_state = opcodes.true

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = None

        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.program, WaterSenseCodes.Enable_ET_Runtime, new_state)

        prog.set_enable_et_state(_state=new_state)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_enable_et_state_fail1(self):
        """ Verifies valid exception when invalid state argument passed in """
        prog = self.create_test_prog_1000_object()
        new_state = "foo"

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = None

        with self.assertRaises(ValueError) as context:
            prog.set_enable_et_state(_state=new_state)

        expected_msg = "Invalid enabled state entered for Program 1. Received: foo, Expected: 'TR' or 'FA'"
        self.assertEqual(first=context.exception.message, second=expected_msg)

    #################################
    def test_set_enable_et_state_fail2(self):
        """ Verifies valid exception when failed to send command to controller or failed to receive reply """
        prog = self.create_test_prog_1000_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = AssertionError("Foo")

        with self.assertRaises(Exception) as context:
            prog.set_enable_et_state()

        expected_msg = "Exception occurred trying to set (Program 1)'s 'Enable ET Runtime state' to: FA, Foo"
        self.assertEqual(first=context.exception.message, second=expected_msg)

    #################################
    def test_verify_poc_pass1(self):
        """ Verify POC On Controller Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_1000_object()
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=2".format(opcodes.point_of_connection))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_poc()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_poc_fail1(self):
        """ Verify POC On Controller Fail Case 1: Value on controller does not match what is
        stored in ad for the POC """
        prog = self.create_test_prog_1000_object()
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=3".format(opcodes.point_of_connection))
        prog.data = mock_data

        expected_message = "Unable to verify 1000 Program 1's assigned 'POC'. Received: [3], Expected: [2]"
        try:
            prog.verify_poc()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_master_valve_pass1(self):
        """ Verify Master Valve On Controller Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_1000_object()
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=MVD0001".format(opcodes.master_valve))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_master_valve()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_master_valve_fail1(self):
        """ Verify Master valve On Controller Fail Case 1: Value on controller does not match what is
        stored in serial number for the Master Valve """
        prog = self.create_test_prog_1000_object()
        prog.ms = 3
        this_obj = prog.mv_objects[3]
        this_obj.sn = "MVD0005"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=MVD0001".format(opcodes.master_valve))
        prog.data = mock_data

        expected_message = "Unable to verify 1000 Program 1's assigned 'Master Valve'. Received: MVD0001, Expected: " \
                           "MVD0005"
        try:
            prog.verify_master_valve()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_soak_cycle_mode_pass1(self):
        """ Verify Soak Cycle Mode On Controller Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_1000_object()
        prog.sk = 'SM'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}={1}".format(opcodes.soak_cycle_mode,
                                                                                opcodes.intelligent_soak))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_soak_cycle_mode()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_soak_cycle_mode_fail1(self):
        """ Verify Soak Cycle Mode On Controller Fail Case 1: Value on controller does not match what is
        stored in prog.sk """
        prog = self.create_test_prog_1000_object()
        prog.sk = 'SM'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=Sm".format(opcodes.soak_cycle_mode))
        prog.data = mock_data

        expected_message = "Unable to verify 1000 Program 1's 'Soak Cycle Mode'. Received: Sm, Expected: SM"
        try:
            prog.verify_soak_cycle_mode()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_cycle_count_pass1(self):
        """ Verify Cycle Count On Controller Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_1000_object()
        prog.ct = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.cycle_count))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_cycle_count()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_cycle_count_fail1(self):
        """ Verify Cycle Count On Controller Fail Case 1: Value on controller does not match what is
        stored in prog.ct """
        prog = self.create_test_prog_1000_object()
        prog.ct = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.cycle_count))
        prog.data = mock_data

        expected_message = "Unable to verify 1000 Program 1's 'Cycle Count'. Received: 6, Expected: 5"
        try:
            prog.verify_cycle_count()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_soak_time_pass1(self):
        """ Verify Soak Time On Controller Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_1000_object()
        prog.so = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.soak_cycle))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_soak_time()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_soak_time_fail1(self):
        """ Verify Soak Time On Controller Fail Case 1: Value on controller does not match what is
        stored in prog.so """
        prog = self.create_test_prog_1000_object()
        prog.so = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.soak_cycle))
        prog.data = mock_data

        expected_message = "Unable to verify 1000 Program 1's 'Soak Time'. Received: 6 seconds, Expected: 5 seconds"
        try:
            prog.verify_soak_time()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_program_cycles_pass1(self):
        """ Verify Program Cycles On Controller Pass Case 1: Exception is not raised """
        prog = self.create_test_prog_1000_object()
        prog.cy = 5
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.program_cycles))
        prog.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                prog.verify_program_cycles()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_program_cycles_fail1(self):
        """ Verify Program Cycles On Controller Fail Case 1: Value on controller does not match what is
        stored in prog.cy """
        prog = self.create_test_prog_1000_object()
        prog.cy = 5
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.program_cycles))
        prog.data = mock_data

        expected_message = "Unable to verify 1000 Program 1's 'Program Cycles'. Received: 6, Expected: 5"
        try:
            prog.verify_program_cycles()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_who_i_am_pass1(self):
        prog = self.create_test_prog_1000_object()

        mock_data = status_parser.KeyValues("PG=1,"
                                            "EN=TR,"
                                            "DS=Test Program 1,"
                                            "WW=111111111111111111111111,"
                                            "MC=1,"
                                            "SA=100,"
                                            "PC=2,"
                                            "SK=DS,"
                                            "CT=2,"
                                            "SO=600,"
                                            "CY=1,"
                                            "MV=MVD0001,"
                                            "EE=FA")

        prog.ser.get_and_wait_for_reply = mock.MagicMock(side_effect=[mock_data])
        prog.verify_who_i_am()

    if __name__ == "__main__":
        unittest.main()