import unittest
import mock
import serial
from common.imports.types import ActionCommands
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.bicoders.moisture_bicoder import MoistureBicoder

from common.imports import opcodes
from common.objects.base_classes.devices import BaseDevices
# you have to set the lat and long after the devices class is called so that they don't get skipped over
BaseDevices.controller_lat = float(43.609768)
BaseDevices.controller_long = float(-116.310569)

from common.objects.devices.ms import MoistureSensor


__author__ = 'Brice "Ajo Grande" Garlick'


class TestMoistureSensorObject(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.309759

    Moisture Sensor Starting lat & lng:
    latitude = 43.609768
    longitude = -116.309354
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """

        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)
        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the moisture sensor
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)

        # Mock a moisture bicoder to pass into the zone
        # self.moisture_bicoder = mock.MagicMock(spec=MoistureBicoder)
        # self.moisture_bicoder.ser = self.mock_ser

        # Set serial instance to mock serial
        self.bl_3200.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_moisture_sensor_object(self, _diffserial=None, _diffaddress=None):
        """ Creates a new Moisture Sensor object for testing purposes """
        if _diffserial is not None:
            self.moisture_bicoder.sn = _diffserial
            ms = MoistureSensor(self.bl_3200, self.moisture_bicoder, _diffaddress )
            # ms = MoistureSensor(_serial="TSD0001", _address=_diffaddress)
        elif _diffaddress is not None:
            self.moisture_bicoder.sn = _diffserial
            ms = MoistureSensor(self.bl_3200, self.moisture_bicoder, _diffaddress )
            # ms = MoistureSensor(_serial=_diffserial, _address=1)
        else:
            address = 1
            self.moisture_bicoder = MoistureBicoder(_sn="TSD0001", _controller=self.bl_3200, _address=address)
            # self.moisture_bicoder.ser = self.mock_ser
            # self.moisture_bicoder.sn = "TSD0001"
            ms = MoistureSensor(_controller=self.bl_3200, _moisture_bicoder=self.moisture_bicoder, _address=address)

        return ms

    #################################
    def test_set_default_values_pass1(self):
        """ Set Default Values On Moisture Sensor Pass Case 1: Correct Command Sent """
        ms = self.create_test_moisture_sensor_object()
        expected_command = "{0}{1},{2}={3},{4}={5},{6}={7}".format(
                            ActionCommands.SET,         # {0}
                            ms.get_id(),                # {1}
                            opcodes.description,        # {2}
                            ms.ds,                      # {3}
                            opcodes.latitude,           # {4}
                            ms.la,                      # {5}
                            opcodes.longitude,          # {6}
                            ms.lg,                      # {7}
                        )

        ms.set_default_values()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values On Moisture Sensor Fail Case 1: Failed communication with controller """
        ms = self.create_test_moisture_sensor_object()
        expected_command = "{0},{1}={2},{3}={4},{5}={6},{7}={8}".format(
            ActionCommands.SET,         # {0}
            opcodes.moisture_sensor,    # {1}
            ms.bicoder.sn,              # {2}
            opcodes.description,        # {3}
            ms.ds,                      # {4}
            opcodes.latitude,           # {5}
            ms.la,                      # {6}
            opcodes.longitude,          # {7}
            ms.lg,                      # {8}
        )

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            ms = self.create_test_moisture_sensor_object()
            ms.set_default_values()
        e_msg = "Exception occurred trying to set Moisture Sensor TSD0001 (1)'s 'Default values' to: '{0}'".format(
                expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_moisture_percent()
    # #################################
    # def test_set_moisture_percent_pass1(self):
    #     """ Set Moisture Percent On Controller Pass Case 1: Using Default _vp value """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     #Expected value is the _vp value set at object Zone object creation
    #     expected_value = ms.bicoder.vp
    #     ms.set_moisture_percent()
    #
    #     #_vp value is set during this method and should equal the original value
    #     actual_value = ms.bicoder.vp
    #     self.assertEqual(first=expected_value, second=actual_value)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_moisture_percent()
    # #################################
    # def test_set_moisture_percent_pass2(self):
    #     """ Set Moisture Percent On Controller Pass Case 2: Setting new _vp value = 6.0 """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     #Expected _vp value is 6
    #     expected_value = 6.0
    #     ms.set_moisture_percent(_percent=expected_value)
    #
    #     #_vp value is set during this method and should equal the value passed into the method
    #     actual_value = ms.bicoder.vp
    #     self.assertEqual(expected_value, actual_value)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_moisture_percent()
    #################################
    # def test_set_moisture_percent_pass3(self):
    #     """ Set Moisture Percent On Controller Pass Case 3: Command with correct values sent to controller """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     ms_value = str(ms.bicoder.vp)
    #     expected_command = "SET,MS=TSD0001,{0}={1}".format(opcodes.moisture_percent, ms_value)
    #     ms.set_moisture_percent()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_moisture_percent()
    # # #################################
    # def test_set_moisture_percent_fail1(self):
    #     """ Set Moisture Percent On Controller Fail Case 1: Pass String value as argument """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     new_vp_value = "b"
    #     with self.assertRaises(Exception) as context:
    #         ms.set_moisture_percent(new_vp_value)
    #     expected_message = "Failed trying to set MS {0} ({1}) moisture percent. Invalid argument type, " \
    #                        "expected an int or float, received: {2}".format(str(ms.sn), str(ms.ad), type(new_vp_value))
    #     self.assertEqual(expected_message, context.exception.message)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_moisture_percent()
    # #################################
    # def test_set_moisture_percent_fail2(self):
    #     """ Set Moisture Percent On Controller Fail Case 2: Failed communication with controller """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     #A contrived Exception is thrown when communicating with the mock serial port
    #     self.mock_ser.send_and_wait_for_reply.side_effect = Exception
    #
    #     with self.assertRaises(Exception) as context:
    #         ms.set_moisture_percent()
    #     e_msg = "Exception occurred trying to set Moisture Sensor {0} ({1})'s 'Moisture Percent' to: '{2}'".format(
    #             ms.sn, str(ms.ad), str(ms.vp))
    #     self.assertEqual(first=e_msg, second=context.exception.message)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_temperature_reading()
    #################################
    # def test_set_temperature_reading_pass1(self):
    #     """ Set Temperature Reading On Controller Pass Case 1: Using Default _vd value """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     #Expected value is the _vd value set at object Zone object creation
    #     expected_value = ms.vd
    #     ms.set_temperature_reading()
    #
    #     #_vd value is set during this method and should equal the original value
    #     actual_value = ms.vd
    #     self.assertEqual(first=expected_value, second=actual_value)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_temperature_reading()
    # #################################
    # def test_set_temperature_reading_pass2(self):
    #     """ Set Temperature Reading On Controller Pass Case 2: Setting new _vd value = 6.0 """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     #Expected _vd value is 6
    #     expected_value = 6.0
    #     ms.set_temperature_reading(expected_value)
    #
    #     #_vd value is set during this method and should equal the value passed into the method
    #     actual_value = ms.vd
    #     self.assertEqual(expected_value, actual_value)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_temperature_reading()
    # #################################
    # def test_set_temperature_reading_pass3(self):
    #     """ Set Temperature Reading On Controller Pass Case 3: Command with correct values sent to controller """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     ms_value = str(ms.vd)
    #     expected_command = "SET,MS=TSD0001,{0}={1}".format(opcodes.temperature_value, ms_value)
    #     ms.set_temperature_reading()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_temperature_reading()
    # #################################
    # def test_set_temperature_reading_fail1(self):
    #     """ Set Temperature Reading On Controller Fail Case 1: Pass String value as argument """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     new_vd_value = "b"
    #     with self.assertRaises(Exception) as context:
    #         ms.set_temperature_reading(new_vd_value)
    #     expected_message = "Failed trying to set MS {0} ({1}) temperature reading. Invalid argument type, " \
    #                        "expected an int or float, received: {2}".format(str(ms.sn), str(ms.ad), type(new_vd_value))
    #     self.assertEqual(expected_message, context.exception.message)
    #
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_temperature_reading()
    # # #################################
    # def test_set_temperature_reading_fail2(self):
    #     """ Set Temperature Reading On Controller Fail Case 2: Failed communication with controller """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     #A contrived Exception is thrown when communicating with the mock serial port
    #     self.mock_ser.send_and_wait_for_reply.side_effect = Exception
    #
    #     with self.assertRaises(Exception) as context:
    #         ms.set_temperature_reading()
    #     e_msg = "Exception occurred trying to set Moisture Sensor {0} ({1})'s 'Temperature Reading' to: '{2}'".format(
    #             ms.sn, str(ms.ad), str(ms.vd))
    #     self.assertEqual(first=e_msg, second=context.exception.message)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_pass1(self):
    #     """ Set Two Wire Drop Value On Controller Pass Case 1: Using Default _vt value """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     #Expected value is the _vt value set at object Zone object creation
    #     expected_value = ms.vt
    #     ms.set_two_wire_drop_value()
    #
    #     #_vt value is set during this method and should equal the original value
    #     actual_value = ms.vt
    #     self.assertEqual(first=expected_value, second=actual_value)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_pass2(self):
    #     """ Set Two Wire Drop Value On Controller Pass Case 2: Setting new _vd value = 6.0 """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     #Expected _vt value is 6
    #     expected_value = 6.0
    #     ms.set_two_wire_drop_value(expected_value)
    #
    #     #_vt value is set during this method and should equal the value passed into the method
    #     actual_value = ms.vt
    #     self.assertEqual(expected_value, actual_value)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_two_wire_drop_value()
    #################################
    # def test_set_two_wire_drop_value_pass3(self):
    #     """ Set Two Wire Drop Value On Controller Pass Case 3: Command with correct values sent to controller """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     vt_value = str(ms.vt)
    #     expected_command = "SET,MS=TSD0001,{0}={1}".format(opcodes.two_wire_drop, vt_value)
    #     ms.set_two_wire_drop_value()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_fail1(self):
    #     """ Set Two Wire Drop Value On Controller Fail Case 1: Pass String value as argument """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     new_vt_value = "b"
    #     with self.assertRaises(Exception) as context:
    #         ms.set_two_wire_drop_value(new_vt_value)
    #     expected_message = "Failed trying to set MS {0} ({1}) two wire drop. Invalid argument type, " \
    #                        "expected an int or float, received: {2}".format(str(ms.sn), str(ms.ad), type(new_vt_value))
    #     self.assertEqual(expected_message, context.exception.message)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_fail2(self):
    #     """ Set Two Wire Drop Value On Controller Fail Case 2: Failed communication with controller """
    #     ms = self.create_test_moisture_sensor_object()
    #
    #     #A contrived Exception is thrown when communicating with the mock serial port
    #     self.mock_ser.send_and_wait_for_reply.side_effect = Exception
    #
    #     with self.assertRaises(Exception) as context:
    #         ms.set_two_wire_drop_value()
    #     e_msg = "Exception occurred trying to set Moisture Sensor {0} ({1})'s 'Two Wire Drop Value' to: '{2}'".format(
    #             ms.sn, str(ms.ad), str(ms.vt))
    #     self.assertEqual(first=e_msg, second=context.exception.message)
    # TODO need to add a wrapper method to ms.py to wrap bicoder.verify_moisture_percent()
    #################################
    # def test_verify_moisture_percent_pass1(self):
    #     """ Verify Moisture Percent On Controller Pass Case 1: Exception is not raised """
    #     ms = self.create_test_moisture_sensor_object()
    #     ms.vp = 22.4
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=22.4".format(opcodes.moisture_percent))
    #     ms.data = mock_data
    #
    #     try:
    #         #.assertRaises raises an Assertion Error if an Exception is not raised in the method
    #         with self.assertRaises(Exception):
    #             ms.verify_moisture_percent()
    #
    #     # Catches an Assertion Error from above, meaning the method did NOT raise an exception
    #     # meaning verification passed
    #     except AssertionError as ae:
    #         e_msg = ae.message
    #         expected_message = "Exception not raised"
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
    # TODO need to add a wrapper method to ms.py to wrap bicoder.verify_moisture_percent()
    #################################
    # def test_verify_moisture_percent_fail1(self):
    #     """ Verify Moisture Percent On Controller Fail Case 1: Value on controller does not match what is
    #     stored in ms.vp """
    #     ms = self.create_test_moisture_sensor_object()
    #     ms.vp = 22.4
    #     test_pass = False
    #     e_msg = ""
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=23.2".format(opcodes.moisture_percent))
    #     ms.data = mock_data
    #
    #     expected_message = "Unable verify Moisture Sensor TSD0001 (1)s moisture percent reading. Received: 23.2, " \
    #                        "Expected: 22.4"
    #     try:
    #         ms.verify_moisture_percent()
    #
    #     # Catches an Exception from above, meaning the method did raise an exception
    #     # meaning verification failed
    #     except Exception as e:
    #         e_msg = e.message
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
    #                      .format(expected_message, e_msg))
    # TODO need to add a wrapper method to ms.py to wrap bicoder.verify_temperature_reading()
    # #################################
    # def test_verify_temperature_reading_pass1(self):
    #     """ Verify Temperature Reading On Controller Pass Case 1: Exception is not raised """
    #     ms = self.create_test_moisture_sensor_object()
    #     ms.vd = 22.4
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=22.4".format(opcodes.temperature_value))
    #     ms.data = mock_data
    #
    #     try:
    #         #.assertRaises raises an Assertion Error if an Exception is not raised in the method
    #         with self.assertRaises(Exception):
    #             ms.verify_temperature_reading()
    #
    #     # Catches an Assertion Error from above, meaning the method did NOT raise an exception
    #     # meaning verification passed
    #     except AssertionError as ae:
    #         e_msg = ae.message
    #         expected_message = "Exception not raised"
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
    # TODO need to add a wrapper method to ms.py to wrap bicoder.verify_temperature_reading()
    #################################
    # def test_verify_temperature_reading_fail1(self):
    #     """ Verify Temperature Reading On Controller Fail Case 1: Value on controller does not match what is
    #     stored in ms.vd """
    #     ms = self.create_test_moisture_sensor_object()
    #     ms.vd = 22.4
    #     test_pass = False
    #     e_msg = ""
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=23.2".format(opcodes.temperature_value))
    #     ms.data = mock_data
    #
    #     expected_message = "Unable verify Moisture Sensor TSD0001 (1)'s temperature reading. Received: 23.2, " \
    #                        "Expected: 22.4"
    #     try:
    #         ms.verify_temperature_reading()
    #
    #     # Catches an Exception from above, meaning the method did raise an exception
    #     # meaning verification failed
    #     except Exception as e:
    #         e_msg = e.message
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
    #                      .format(expected_message, e_msg))
    # TODO need to add a wrapper method to ms.py to wrap bicoder.verify_two_wire_drop_value()
    #################################
    # def test_verify_two_wire_drop_value_pass1(self):
    #     """ Verify Two Wire Drop Value On Controller Pass Case 1: Exception is not raised """
    #     ms = self.create_test_moisture_sensor_object()
    #     ms.vt = 22.4
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=22.4".format(opcodes.two_wire_drop))
    #     ms.data = mock_data
    #
    #     try:
    #         #.assertRaises raises an Assertion Error if an Exception is not raised in the method
    #         with self.assertRaises(Exception):
    #             ms.verify_two_wire_drop_value()
    #
    #     # Catches an Assertion Error from above, meaning the method did NOT raise an exception
    #     # meaning verification passed
    #     except AssertionError as ae:
    #         e_msg = ae.message
    #         expected_message = "Exception not raised"
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
    # TODO need to add a wrapper method to ms.py to wrap bicoder.verify_two_wire_drop_value()
    # #################################
    # def test_verify_two_wire_drop_value_fail1(self):
    #     """ Verify Two Wire Drop Value On Controller Fail Case 1: Value on controller does not match what is
    #     stored in ms.vt """
    #     ms = self.create_test_moisture_sensor_object()
    #     ms.vt = 22.4
    #     test_pass = False
    #     e_msg = ""
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=23.2".format(opcodes.two_wire_drop))
    #     ms.data = mock_data
    #
    #     expected_message = "Unable verify Moisture Sensor TSD0001 (1)'s two wire drop value. Received: 23.2, " \
    #                        "Expected: 22.4"
    #     try:
    #         ms.verify_two_wire_drop_value()
    #
    #     # Catches an Exception from above, meaning the method did raise an exception
    #     # meaning verification failed
    #     except Exception as e:
    #         e_msg = e.message
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
    #                      .format(expected_message, e_msg))


    #################################
    def test_verify_who_i_am(self):
        """ Verify Who I Am Pass Case 1: Runs through the method successfully
        Create test moisture sensor object
        Mock: get_data because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_description because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_serial_number because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_latitude because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_longitude because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_status because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_moisture_percent because it goes outside the method
            Return: Nothing we just want to pass over it
        Mock: verify_temperature_reading because it goes outside the method
            Return: Nothing we just want to pass over it
        Call the method and make sure it runs through successfully
        """
        # Creates the moisture sensor object
        ms = self.create_test_moisture_sensor_object()

        # Mock the get_data method
        mock_get_data = mock.MagicMock()
        ms.get_data = mock_get_data
        ms.data = mock.MagicMock()

        # Mock the verify_description method
        mock_verify_description = mock.MagicMock()
        ms.verify_description = mock_verify_description

        # Mock the verify_serial_number method
        mock_verify_serial_number = mock.MagicMock()
        ms.verify_serial_number = mock_verify_serial_number

        # Mock the verify_latitude method
        mock_verify_latitude = mock.MagicMock()
        ms.verify_latitude = mock_verify_latitude

        # Mock the verify_longitude method
        mock_verify_longitude = mock.MagicMock()
        ms.verify_longitude = mock_verify_longitude

        # Mock the verify_status method
        mock_verify_status = mock.MagicMock()
        ms.verify_status = mock_verify_status

        # There is nothing to assert in this method call
        ms.verify_who_i_am(_expected_status=opcodes.bad_serial)

    #################################
    def test_replace_moisture_bicoder_pass1(self):
        """ Test replace moisture bicoder"""
        # set up values for test
        new_sn_value = "MSD1005"
        ms = self.create_test_moisture_sensor_object()
        moisture_bicoder_2 = mock.MagicMock(spec=MoistureBicoder)
        moisture_bicoder_2.sn = new_sn_value
        ms.controller.moisture_bicoders = dict()
        ms.controller.moisture_bicoders[new_sn_value] = moisture_bicoder_2
        set_address_mock = ms.set_address = mock.MagicMock()
        moisture_bicoder_2.self_test_and_update_object_attributes = mock.MagicMock()

        # run test
        ms.replace_moisture_bicoder(new_sn_value)

        # verify test
        self.assertEquals(ms.sn, moisture_bicoder_2.sn)
        self.assertEquals(ms.identifiers[0][1], moisture_bicoder_2.sn)
        self.assertEquals(set_address_mock.call_count, 1)
        self.assertEquals(moisture_bicoder_2.self_test_and_update_object_attributes.call_count, 1)

    #################################
    def test_replace_moisture_bicoder_pass2(self):
        """ Test replace moisture bicoder"""
        # set up values for test
        new_sn_value = "MSD1005"
        ms = self.create_test_moisture_sensor_object()
        moisture_bicoder_2 = mock.MagicMock(spec=MoistureBicoder)
        moisture_bicoder_2.sn = new_sn_value
        ms.controller.moisture_bicoders = dict()
        load_dv_mock = ms.controller.load_dv = mock.MagicMock()
        do_search_for_moisture_sensor_mock = ms.controller.do_search_for_moisture_sensor = mock.MagicMock()
        # run test

        # Verifies that a key error is raised when self.bicoder is set to the new serial number. We are trying to
        # test when the serial number is not in the dictionary, and it won't be in the dictionary when
        # self.bicoder is set to the moisture_bicoder of the new serial number.
        with self.assertRaises(KeyError):
            ms.replace_moisture_bicoder(new_sn_value)

        self.assertEquals(load_dv_mock.call_count, 1)
        self.assertEquals(do_search_for_moisture_sensor_mock.call_count, 1)

