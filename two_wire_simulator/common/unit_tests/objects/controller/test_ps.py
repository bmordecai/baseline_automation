import unittest
import mock
import serial
import status_parser

from common.objects.base_classes.devices import BaseDevices
# you have to set the lat and long after the devices class is called so that they don't get skipped over
BaseDevices.controller_lat = float(43.609768)
BaseDevices.controller_long = float(-116.310569)
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.devices.ps import PressureSensor
from common.objects.bicoders.analog_bicoder import AnalogBicoder
from common.imports import opcodes

__author__ = 'Kent'


class TestPressureSensorObject(unittest.TestCase):

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the zone.
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)

        # Attach the mocked serial to the controller ser
        self.bl_3200.ser = self.mock_ser

        # set the controller type
        self.bl_3200.controller_type = '32'

        # Mock a analog bicoder to pass into the zone
        self.analog_bicoder = mock.MagicMock(spec=AnalogBicoder)

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_pressure_sensor_object(self, _diffserial="TPD0001", _diffaddress=1):
        """ Creates a new Pressure Sensor object for testing purposes """
        self.analog_bicoder.sn = _diffserial
        self.analog_bicoder.vl = 0.00
        self.analog_bicoder.vh = 0.00
        self.analog_bicoder.va = 0.00
        self.analog_bicoder.vr = 0.00
        ps = PressureSensor(_controller=self.bl_3200, _analog_bicoder=self.analog_bicoder, _address=_diffaddress)

        return ps

    #################################
    def test_set_default_values_pass1(self):
        """ Set Default Values On Controller Pass Case 1: Correct Command Sent """
        ps = self.create_test_pressure_sensor_object()
        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13},{14}={15},{16}={17}" \
            .format(
                opcodes.analog_decoder,                 # {0}
                str(ps.sn),                             # {1}
                opcodes.enabled,                        # {2}
                str(ps.en),                             # {3}
                opcodes.description,                    # {4}
                str(ps.ds),                             # {5}
                opcodes.latitude,                       # {6}
                str(ps.la),                             # {7}
                opcodes.longitude,                      # {8}
                str(ps.lg),                             # {9}
                opcodes.low_milli_amp_setting,          # {10}
                str(ps.bicoder.vl),                     # {11}
                opcodes.high_milli_amp_setting,         # {12}
                str(ps.bicoder.vh),                     # {13}
                opcodes.raw_analog_readings,            # {14}
                str(ps.bicoder.va),                     # {15}
                opcodes.analog_units,                   # {16}
                str(ps.vu)                              # {17}
            )

        ps.set_default_values()
        self.mock_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values On Controller Fail Case 1: Handle Exception Raised From Controller """
        ps = self.create_test_pressure_sensor_object()
        expected_command = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13},{14}={15},{16}={17}" \
            .format(
                opcodes.analog_decoder,                 # {0}
                str(ps.sn),                             # {1}
                opcodes.enabled,                        # {2}
                str(ps.en),                             # {3}
                opcodes.description,                    # {4}
                str(ps.ds),                             # {5}
                opcodes.latitude,                       # {6}
                str(ps.la),                             # {7}
                opcodes.longitude,                      # {8}
                str(ps.lg),                             # {9}
                opcodes.low_milli_amp_setting,          # {10}
                str(ps.bicoder.vl),                     # {11}
                opcodes.high_milli_amp_setting,         # {12}
                str(ps.bicoder.vh),                     # {13}
                opcodes.raw_analog_readings,            # {14}
                str(ps.bicoder.va),                     # {15}
                opcodes.analog_units,                   # {16}
                str(ps.vu)                              # {17}
            )

        e_msg = "Exception occurred trying to set Pressure Sensor {0}'s 'Default values' to: '{1}'".format(
            str(ps.ad),         # {0}
            expected_command    # {1}
        )

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            ps.set_default_values()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_replace_analog_bicoder_pass1(self):
        """ Test replace analog bicoder"""
        # set up values for test
        new_sn_value = "MSD1005"
        ps = self.create_test_pressure_sensor_object()
        analog_bicoder_2 = mock.MagicMock(spec=AnalogBicoder)
        analog_bicoder_2.sn = new_sn_value
        ps.controller.analog_bicoders = dict()
        ps.controller.analog_bicoders[new_sn_value] = analog_bicoder_2
        set_address_mock = ps.set_address = mock.MagicMock()
        analog_bicoder_2.self_test_and_update_object_attributes = mock.MagicMock()

        # run test
        ps.replace_analog_bicoder(new_sn_value)

        # verify test
        self.assertEquals(ps.sn, analog_bicoder_2.sn)
        self.assertEquals(ps.identifiers[0][1], analog_bicoder_2.sn)
        self.assertEquals(set_address_mock.call_count, 1)
        self.assertEquals(analog_bicoder_2.self_test_and_update_object_attributes.call_count, 1)

    def test_replace_analog_bicoder_pass2(self):
        """ Test replace analog bicoder"""
        # set up values for test
        new_sn_value = "MSD1005"
        ps = self.create_test_pressure_sensor_object()
        analog_bicoder_2 = mock.MagicMock(spec=AnalogBicoder)
        analog_bicoder_2.sn = new_sn_value
        ps.controller.analog_bicoders = dict()
        analog_bicoder_2.self_test_and_update_object_attributes = mock.MagicMock()
        load_dv_mock = ps.controller.load_dv = mock.MagicMock()
        do_search_for_analog_bicoders_mock = ps.controller.do_search_for_analog_bicoders = mock.MagicMock()
        # run test

        # Verifies that a key error is raised when self.bicoder is set to the new serial number. We are trying to
        # test when the serial number is not in the dictionary, and it won't be in the dictionary when
        # self.bicoder is set to the analog_bicoder of the new serial number.
        with self.assertRaises(KeyError):
            ps.replace_analog_bicoder(new_sn_value)

        self.assertEquals(load_dv_mock.call_count, 1)
        self.assertEquals(do_search_for_analog_bicoders_mock.call_count, 1)

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Verify Who I Am Pass Case 1: Exception is not raised """
        ps = self.create_test_pressure_sensor_object()
        ps.ss = opcodes.okay
        # Set up mock data to return the default values of our biCoder
        mock_data = status_parser.KeyValues("{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11}".format(
            opcodes.description,
            ps.ds,
            opcodes.serial_number,
            ps.sn,
            opcodes.latitude,
            ps.la,
            opcodes.longitude,
            ps.lg,
            opcodes.status_code,
            ps.ss,
            opcodes.enabled,
            ps.en,
        ))

        # Mock the get data method so we don't attempt to send a packet to the controller
        ps.ser.get_and_wait_for_reply = mock.MagicMock(return_value = mock_data)

        self.assertEqual(ps.verify_who_i_am(_expected_status=opcodes.okay), True)

    if __name__ == "__main__":
        unittest.main()
