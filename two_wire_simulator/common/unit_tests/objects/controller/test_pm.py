import unittest
import mock
import serial
import status_parser

from common.objects.base_classes.devices import BaseDevices
# you have to set the lat and long after the devices class is called so that they don't get skipped over
BaseDevices.controller_lat = float(43.609768)
BaseDevices.controller_long = float(-116.310569)
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.devices.pm import Pump
from common.objects.bicoders.pump_bicoder import PumpBicoder
from common.objects.bicoders.valve_bicoder import ValveBicoder
from common.imports import opcodes

__author__ = 'Kent'


class TestPumpObject(unittest.TestCase):

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the zone.
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)

        # Attach the mocked serial to the controller ser
        self.bl_3200.ser = self.mock_ser

        # set the controller type
        self.bl_3200.controller_type = '32'

        # Mock a switch bicoder to pass into the zone
        self.pump_bicoder = mock.MagicMock(spec=PumpBicoder)

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_pump_object(self, _diffserial="TPD0001", _diffaddress=1):
        """ Creates a new Event Switch object for testing purposes """
        self.pump_bicoder.sn = _diffserial
        pm = Pump(_controller=self.bl_3200, _pump_bicoder=self.pump_bicoder, _address=_diffaddress)

        return pm

    #################################
    def test_set_default_values_pass1(self):
        """ Set Default Values On Controller Pass Case 1: Correct Command Sent for controller type of '32'"""
        pm = self.create_test_pump_object()
        expected_command = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12}".format(
            opcodes.set_action,     # {0}
            opcodes.pump,           # {1}
            str(pm.sn),             # {2}
            opcodes.enabled,        # {3}
            str(pm.en),             # {4}
            opcodes.description,    # {5}
            str(pm.ds),             # {6}
            opcodes.latitude,       # {7}
            str(pm.la),             # {8}
            opcodes.longitude,      # {9}
            str(pm.lg),             # {10}
            opcodes.booster_pump,   # {11}
            str(pm.bp)              # {12}
        )

        pm.set_default_values()
        self.mock_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_pass2(self):
        """ Set Default Values On Controller Pass Case 2: Correct Command Sent for controller type other than '32'"""
        pm = self.create_test_pump_object()
        pm.controller.controller_type = '10'
        expected_command = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10}".format(
            opcodes.set_action,     # {0}
            opcodes.pump,           # {1}
            str(pm.sn),             # {2}
            opcodes.enabled,        # {3}
            str(pm.en),             # {4}
            opcodes.description,    # {5}
            str(pm.ds),             # {6}
            opcodes.latitude,       # {7}
            str(pm.la),             # {8}
            opcodes.longitude,      # {9}
            str(pm.lg),             # {10}
        )

        pm.set_default_values()
        self.mock_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values On Controller Fail Case 1: Handle Exception Raised From Controller """
        pm = self.create_test_pump_object()
        expected_command = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12}".format(
            opcodes.set_action,     # {0}
            opcodes.pump,           # {1}
            str(pm.sn),             # {2}
            opcodes.enabled,        # {3}
            str(pm.en),             # {4}
            opcodes.description,    # {5}
            str(pm.ds),             # {6}
            opcodes.latitude,       # {7}
            str(pm.la),             # {8}
            opcodes.longitude,      # {9}
            str(pm.lg),             # {10}
            opcodes.booster_pump,   # {11}
            str(pm.bp)              # {12}
        )

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            pm.set_default_values()
        e_msg = "Exception occurred trying to set Pump {0} ({1})'s 'Default values' to: '{2}'".format(
            pm.sn, str(pm.ad), expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_booster_enabled_pass1(self):
        """ Set Enable State On Booster Pump Pass Case 1: Verify command sent to controller """
        pm = self.create_test_pump_object()
        expected_command = "{0},{1}={2},{3}={4}".format(
            opcodes.set_action,
            opcodes.pump,
            str(pm.sn),
            opcodes.booster_pump,
            opcodes.true)
        pm.set_booster_enabled()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_booster_enabled_fail2(self):
        """ Set Enable State On Booster Pump Fail Case 1: Failed Communication With Controller """
        pm = self.create_test_pump_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        e_msg = "Exception occurred trying to set Pump {0} ({1})'s 'Booster Pump State' to: {2}".format(
            pm.sn, str(pm.ad), str(opcodes.true))

        with self.assertRaises(Exception) as context:
            pm.set_booster_enabled()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_booster_disabled_pass1(self):
        """ Set Enable State On Booster Pump Pass Case 1: Set enabled to false """
        pm = self.create_test_pump_object()
        expected_command = "{0},{1}={2},{3}={4}".format(
            opcodes.set_action,
            opcodes.pump,
            str(pm.sn),
            opcodes.booster_pump,
            opcodes.false)
        pm.set_booster_disabled()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_booster_disabled_fail2(self):
        """ Set Enable State On Booster Pump Fail Case 1: Failed Communication With Controller """
        pm = self.create_test_pump_object()

        # Set the send_and_wait_for_reply method to raise an 'Exception' after master valve instance is created to avoid
        # raising an exception trying to set default values.
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        e_msg = "Exception occurred trying to set Pump {0} ({1})'s 'Booster Pump State' to: {2}".format(
            pm.sn, str(pm.ad), str(opcodes.false))

        with self.assertRaises(Exception) as context:
            pm.set_booster_disabled()

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_booster_enable_state_pass1(self):
        """ Verify Booster Enable State Pass Case 1: Exception is not raised """
        pm = self.create_test_pump_object()
        pm.bp = opcodes.true
        test_pass = False

        mock_data = status_parser.KeyValues("{0}={1},{2}={3},{4}={5}".format(
            opcodes.serial_number,
            pm.sn,
            opcodes.description,
            "blah",
            opcodes.booster_pump,
            pm.bp)
        )

        pm.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                pm.verify_booster_enable_state()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_booster_enable_state_fail1(self):
        """ Verify Booster Enable State Fail Case 1: Value on controller does not match what is
        stored in pm.bp """
        pm = self.create_test_pump_object()
        pm.bp = opcodes.false
        controller_value = opcodes.true
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}={1},{2}={3},{4}={5}".format(
            opcodes.serial_number,
            pm.sn,
            opcodes.description,
            "blah",
            opcodes.booster_pump,
            controller_value)
        )

        pm.data = mock_data

        expected_message = "Unable verify Pump {0} ({1})'s Booster pump enabled state. Received: {2}, Expected: {3}"\
                .format(
                    pm.sn,                  # {0}
                    str(pm.ad),             # {1}
                    str(controller_value),  # {2}
                    str(pm.bp)              # {3}
                )
        try:
            pm.verify_booster_enable_state()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    def test_replace_single_valve_bicoder_pass1(self):
        """ Test replace single valve bicoder"""
        # set up values for test
        new_sn_value = "MSD1005"
        pm = self.create_test_pump_object()
        valve_bicoder_2 = mock.MagicMock(spec=ValveBicoder)
        valve_bicoder_2.sn = new_sn_value
        pm.controller.valve_bicoders = dict()
        pm.controller.valve_bicoders[new_sn_value] = valve_bicoder_2
        set_address_mock = pm.set_address = mock.MagicMock()
        valve_bicoder_2.self_test_and_update_object_attributes = mock.MagicMock()

        # run test
        pm.replace_single_valve_bicoder(new_sn_value)

        # verify test
        self.assertEquals(pm.sn, valve_bicoder_2.sn)
        self.assertEquals(pm.identifiers[0][1], valve_bicoder_2.sn)
        self.assertEquals(set_address_mock.call_count, 1)
        self.assertEquals(valve_bicoder_2.self_test_and_update_object_attributes.call_count, 1)

    def test_replace_single_valve_bicoder_pass2(self):
        """ Test replace single valve bicoder"""
        # set up values for test
        new_sn_value = "MSD1005"
        pm = self.create_test_pump_object()
        valve_bicoder_2 = mock.MagicMock(spec=ValveBicoder)
        valve_bicoder_2.sn = new_sn_value
        pm.controller.valve_bicoders = dict()
        valve_bicoder_2.self_test_and_update_object_attributes = mock.MagicMock()
        load_dv_mock = pm.controller.load_dv = mock.MagicMock()
        do_search_for_pumps_mock = pm.controller.do_search_for_pumps = mock.MagicMock()
        # run test

        # Verifies that a key error is raised when self.bicoder is set to the new serial number. We are trying to
        # test when the serial number is not in the dictionary, and it won't be in the dictionary when
        # self.bicoder is set to the valve_bicoder of the new serial number.
        with self.assertRaises(KeyError):
            pm.replace_single_valve_bicoder(new_sn_value)

        self.assertEquals(load_dv_mock.call_count, 1)
        self.assertEquals(do_search_for_pumps_mock.call_count, 1)

    def test_replace_dual_valve_bicoder_pass1(self):
        """ Test replace dual valve bicoder"""
        # set up values for test
        new_sn_value = "MSD1005"
        pm = self.create_test_pump_object()
        valve_bicoder_2 = mock.MagicMock(spec=ValveBicoder)
        valve_bicoder_2.sn = new_sn_value
        pm.controller.valve_bicoders = dict()
        pm.controller.valve_bicoders[new_sn_value] = valve_bicoder_2
        set_address_mock = pm.set_address = mock.MagicMock()
        valve_bicoder_2.self_test_and_update_object_attributes = mock.MagicMock()

        # run test
        pm.replace_dual_valve_bicoder(new_sn_value)

        # verify test
        self.assertEquals(pm.sn, valve_bicoder_2.sn)
        self.assertEquals(pm.identifiers[0][1], valve_bicoder_2.sn)
        self.assertEquals(set_address_mock.call_count, 1)
        self.assertEquals(valve_bicoder_2.self_test_and_update_object_attributes.call_count, 1)

    def test_replace_dual_valve_bicoder_pass2(self):
        """ Test replace dual valve bicoder"""
        # set up values for test
        new_sn_value = "MSD1005"
        pm = self.create_test_pump_object()
        valve_bicoder_2 = mock.MagicMock(spec=ValveBicoder)
        valve_bicoder_2.sn = new_sn_value
        pm.controller.valve_bicoders = dict()
        valve_bicoder_2.self_test_and_update_object_attributes = mock.MagicMock()
        load_dv_mock = pm.controller.load_dv = mock.MagicMock()
        do_search_for_pumps_mock = pm.controller.do_search_for_pumps = mock.MagicMock()
        # run test

        # Verifies that a key error is raised when self.bicoder is set to the new serial number. We are trying to
        # test when the serial number is not in the dictionary, and it won't be in the dictionary when
        # self.bicoder is set to the valve_bicoder of the new serial number.
        with self.assertRaises(KeyError):
            pm.replace_dual_valve_bicoder(new_sn_value)

        self.assertEquals(load_dv_mock.call_count, 1)
        self.assertEquals(do_search_for_pumps_mock.call_count, 1)

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Verify Who I Am Pass Case 1: Exception is not raised """
        pm = self.create_test_pump_object()
        pm.ss = opcodes.okay
        # Set up mock data to return the default values of our biCoder
        mock_data = status_parser.KeyValues("{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13}".format(
            opcodes.description,
            pm.ds,
            opcodes.serial_number,
            pm.sn,
            opcodes.latitude,
            pm.la,
            opcodes.longitude,
            pm.lg,
            opcodes.status_code,
            pm.ss,
            opcodes.enabled,
            pm.en,
            opcodes.booster_pump,
            pm.bp
        ))

        # Mock the get data method so we don't attempt to send a packet to the controller
        pm.ser.get_and_wait_for_reply = mock.MagicMock(return_value = mock_data)

        self.assertEqual(pm.verify_who_i_am(_expected_status=opcodes.okay), True)

    if __name__ == "__main__":
        unittest.main()
