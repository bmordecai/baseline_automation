from __future__ import absolute_import
import unittest
import mock
import serial
import status_parser

from common.objects.base_classes.devices import BaseDevices
# you have to set the lat and long after the devices class is called so that they don't get skipped over
BaseDevices.controller_lat = float(43.609768)
BaseDevices.controller_long = float(-116.310569)

from common.objects.devices.ts import TemperatureSensor
from common.objects.bicoders.temp_bicoder import TempBicoder
from common.objects.controllers.bl_32 import BaseStation3200

__author__ = 'Ben'


class TestTemperatureSensorObject(unittest.TestCase):

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the zone.
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)

        # Attach the mocked serial to the controller ser
        self.bl_3200.ser = self.mock_ser

        # Mock a temp bicoder to pass into the temp sensor
        self.temp_bicoder = mock.MagicMock(spec=TempBicoder)

        # Set serial instance to mock serial
        BaseDevices.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_temperature_sensor_object(self, _diffserial="TSD0001", _diffaddress=1):
        """ Creates a new Event Switch object for testing purposes """
        self.temp_bicoder.sn = _diffserial
        self.temp_bicoder.ad = _diffaddress
        ts = TemperatureSensor(_controller=self.bl_3200, _temp_bicoder=self.temp_bicoder, _address=_diffaddress)
        return ts

    #################################
    def test_set_default_values_pass1(self):
        """ Set Default Values On Controller Pass Case 1: Correct Command Sent """
        ts = self.create_test_temperature_sensor_object()
        expected_command = "SET," \
                           "TS=TSD0001," \
                           "DS=Test Temperature Sensor TSD0001," \
                           "LA=43.609773," \
                           "LG=-116.309764" \

        ts.set_default_values()
        self.mock_ser.send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values On Controller Fail Case 1: Handle Exception Raised From Controller """
        ts = self.create_test_temperature_sensor_object()
        expected_command = "SET," \
                           "TS=TSD0001," \
                           "DS=Test Temperature Sensor TSD0001," \
                           "LA=43.609773," \
                           "LG=-116.309764" \

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            ts.set_default_values()
        e_msg = "Exception occurred trying to set Temperature Sensor TSD0001 1's 'Default values' to: '{0}'"\
            .format(expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    def test_replace_temperature_bicoder_pass1(self):
        """ Tests replacing a temp bicoder with an EXISTING bicoder. """
        new_sn_value = "MSD1005"

        ts = self.create_test_temperature_sensor_object()
        temp_bicoder_2 = mock.MagicMock(spec=TempBicoder)
        temp_bicoder_2.sn = new_sn_value

        # Setup controller temp bicoder dictionary
        ts.controller.temperature_bicoders = dict()
        ts.controller.temperature_bicoders[new_sn_value] = temp_bicoder_2

        set_address_mock = ts.set_address = mock.MagicMock()
        temp_bicoder_2.self_test_and_update_object_attributes = mock.MagicMock()

        # run test
        ts.replace_temperature_bicoder(_new_serial_number=new_sn_value)

        # verify test
        self.assertEquals(ts.sn, temp_bicoder_2.sn)
        self.assertEquals(ts.identifiers[0][1], temp_bicoder_2.sn)
        self.assertEquals(set_address_mock.call_count, 1)
        self.assertEquals(temp_bicoder_2.self_test_and_update_object_attributes.call_count, 1)

    def test_replace_temperature_bicoder_pass2(self):
        """ Tests replacing a temp bicoder with a NEW bicoder. Verifies bicoder is loaded onto controller. """
        new_sn_value = "TSD1005"

        # Create and mock new temp bicoder
        ts = self.create_test_temperature_sensor_object()
        temp_bicoder_2 = mock.MagicMock(spec=TempBicoder)
        temp_bicoder_2.sn = new_sn_value
        temp_bicoder_2.self_test_and_update_object_attributes = mock.MagicMock()

        # Setup controller temp bicoder dictionary
        ts.controller.temperature_bicoders = dict()

        # mock both controller methods being called
        load_dv_mock = ts.controller.load_dv = mock.MagicMock()
        do_search_for_temperature_sensor_mock = ts.controller.do_search_for_temperature_sensor = mock.MagicMock()

        # Verifies that a key error is raised when self.bicoder is set to the new serial number. We are trying to
        # test when the serial number is not in the dictionary, and it won't be in the dictionary when
        # self.bicoder is set to the temperature_bicoder of the new serial number.
        with self.assertRaises(KeyError):
            ts.replace_temperature_bicoder(_new_serial_number=new_sn_value)

        self.assertEquals(load_dv_mock.call_count, 1)
        self.assertEquals(do_search_for_temperature_sensor_mock.call_count, 1)

    #################################
    def test_verify_who_i_am_pass1(self):
        # Create temperature sensor object
        ts = self.create_test_temperature_sensor_object()

        # Verify mock description on controller
        description = mock.MagicMock(return_value='TSD0001 Test Temp Sensor 1')
        ts.verify_description = description

        # Verify mock serial number on controller
        serial_number = mock.MagicMock()
        ts.verify_serial_number = serial_number

        # Verify mock latitude on controller
        latitude = mock.MagicMock()
        ts.verify_latitude = latitude

        # Verify mock longitude on the controller
        longitude = mock.MagicMock
        ts.verify_longitude = longitude

        # Set expected status to NOT none (something besides none), then verify mock status on the controller
        expected_status = mock.MagicMock(return_value='CN')
        ts.verify_status = expected_status

        # Verify the mock temperature reading on the controller
        temperature = mock.MagicMock()
        ts.verify_temperature_reading = temperature

        # Verify the mock two wire drop value on the controller
        two_wire_drop_value = mock.MagicMock()
        ts.verify_two_wire_drop_value = two_wire_drop_value

        # Go into the method
        ts.verify_who_i_am(_expected_status='CN')
    # TODO add a wrapper method to ts.py to wrap bicoder.set_temperature_reading()
    # #################################
    # def test_set_temperature_reading_pass1(self):
    #     """ Set Temp Reading On Controller Pass Case 1: Using Default _vt value """
    #     ts = self.create_test_temperature_sensor_object()
    #
    #     # Expected value is the _vt value set at object Zone object creation
    #     expected_value = ts.vd
    #     ts.set_temperature_reading()
    #
    #     # _vt value is set during this method and should equal the original value
    #     actual_value = ts.vd
    #     self.assertEqual(first=expected_value, second=actual_value)
    # TODO add a wrapper method to ts.py to wrap bicoder.set_temperature_reading()
    # #################################
    # def test_set_temperature_reading_pass2(self):
    #     """ Set Temp Reading On Controller Pass Case 2: Setting new _vd value = 6.0 """
    #     ts = self.create_test_temperature_sensor_object()
    #
    #     # Expected _vt value is 6
    #     expected_value = 6.0
    #     ts.set_temperature_reading(expected_value)
    #
    #     # _vt value is set during this method and should equal the value passed into the method
    #     actual_value = ts.vd
    #     self.assertEqual(expected_value, actual_value)
    # TODO add a wrapper method to ts.py to wrap bicoder.set_temperature_reading()
    # #################################
    # def test_set_temperature_reading_pass3(self):
    #     """ Set Temp Reading On Controller Pass Case 3: Command with correct values sent to controller """
    #     ts = self.create_test_temperature_sensor_object()
    #
    #     vd_value = str(ts.vd)
    #     expected_command = "SET,{0}=TSD0001,{1}={2}".format(opcodes.temperature_sensor, opcodes.temperature_value,
    #                                                         vd_value)
    #     ts.set_temperature_reading()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO add a wrapper method to ts.py to wrap bicoder.set_temperature_reading()
    # #################################
    # def test_set_temperature_reading_fail1(self):
    #     """ Set Temp Reading On Controller Fail Case 1: Pass String value as argument """
    #     ts = self.create_test_temperature_sensor_object()
    #
    #     new_vt_value = "b"
    #     with self.assertRaises(Exception) as context:
    #         ts.set_temperature_reading(new_vt_value)
    #     expected_message = "Failed trying to set Temperature Sensor TSD0001 (1)'s temperature reading. Invalid type " \
    #                        "input as argument. Expected int or float, received: {0}".format(type(new_vt_value))
    #     self.assertEqual(expected_message, context.exception.message)
    # TODO add a wrapper method to ts.py to wrap bicoder.set_temperature_reading()
    # #################################
    # def test_set_temperature_reading_fail2(self):
    #     """ Set Temp Reading On Controller Fail Case 2: Unable to send command to controller """
    #     ts = self.create_test_temperature_sensor_object()
    #     _vd = 2
    #
    #     self.mock_send_and_wait_for_reply.side_effect = Exception
    #     ts.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply
    #
    #     with self.assertRaises(Exception) as context:
    #         ts.set_temperature_reading(_vd)
    #
    #     expected_msg = "Exception occurred trying to set Temperature Sensor {0} ({1})'s 'Temperature Reading' to: '{2}'"\
    #         .format(ts.sn, ts.ad, _vd)
    #     self.assertEqual(first=expected_msg, second=context.exception.message)
    # TODO add a wrapper method to ts.py to wrap bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_pass1(self):
    #     """ Set Two Wire Drop Value On Controller Pass Case 1: Using Default _vt value """
    #     ts = self.create_test_temperature_sensor_object()
    #
    #     # Expected value is the _vt value set at object Zone object creation
    #     expected_value = ts.vt
    #     ts.set_two_wire_drop_value()
    #
    #     # _vt value is set during this method and should equal the original value
    #     actual_value = ts.vt
    #     self.assertEqual(first=expected_value, second=actual_value)
    # TODO add a wrapper method to ts.py to wrap bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_pass2(self):
    #     """ Set Two Wire Drop Value On Controller Pass Case 2: Setting new _vt value = 6.0 """
    #     ts = self.create_test_temperature_sensor_object()
    #
    #     # Expected _vt value is 6
    #     expected_value = 6.0
    #     ts.set_two_wire_drop_value(expected_value)
    #
    #     # _vt value is set during this method and should equal the value passed into the method
    #     actual_value = ts.vt
    #     self.assertEqual(expected_value, actual_value)
    # TODO add a wrapper method to ts.py to wrap bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_pass3(self):
    #     """ Set Two Wire Drop Value On Controller Pass Case 3: Command with correct values sent to controller """
    #     ts = self.create_test_temperature_sensor_object()
    #
    #     vt_value = str(ts.vt)
    #     expected_command = "SET,{0}=TSD0001,{1}={2}".format(opcodes.temperature_sensor, opcodes.two_wire_drop, vt_value)
    #     ts.set_two_wire_drop_value()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
    # TODO add a wrapper method to ts.py to wrap bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_fail1(self):
    #     """ Set Two Wire Drop Value On Controller Fail Case 1: Pass String value as argument """
    #     ts = self.create_test_temperature_sensor_object()
    #
    #     new_vt_value = "b"
    #     with self.assertRaises(Exception) as context:
    #         ts.set_two_wire_drop_value(new_vt_value)
    #     expected_message = "Failed trying to set Temperature Sensor TSD0001 (1)'s two wire drop. Invalid argument type," \
    #                        " expected an int or float, received: {0}".format(type(new_vt_value))
    #     self.assertEqual(expected_message, context.exception.message)
    # TODO add a wrapper method to ts.py to wrap bicoder.set_two_wire_drop_value()
    # #################################
    # def test_set_two_wire_drop_value_fail2(self):
    #     """ Set Two Wire Drop Value On Controller Fail Case 2: Unable to send command to controller """
    #     ts = self.create_test_temperature_sensor_object()
    #     _vt = 2
    #
    #     self.mock_send_and_wait_for_reply.side_effect = Exception
    #     ts.ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply
    #
    #     with self.assertRaises(Exception) as context:
    #         ts.set_two_wire_drop_value(_value=_vt)
    #
    #     expected_msg = "Exception occurred trying to set Temperature Sensor {0} ({1})'s 'Two Wire Drop Value' to: '{2}'"\
    #         .format(ts.sn, ts.ad, _vt)
    #     self.assertEqual(first=expected_msg, second=context.exception.message)
    # TODO add a wrapper method to ts.py to wrap bicoder.verify_temperature_reading()
    # #################################
    # def test_verify_temperature_reading_pass1(self):
    #     """ Verify Temp Reading On Controller Pass Case 1: Exception is not raised """
    #     ts = self.create_test_temperature_sensor_object()
    #     ts.vd = 5
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.temperature_value))
    #     ts.data = mock_data
    #
    #     try:
    #         # .assertRaises raises an Assertion Error if an Exception is not raised in the method
    #         with self.assertRaises(Exception):
    #             ts.verify_temperature_reading()
    #
    #     # Catches an Assertion Error from above, meaning the method did NOT raise an exception
    #     # meaning verification passed
    #     except AssertionError as ae:
    #         e_msg = ae.message
    #         expected_message = "Exception not raised"
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
    # TODO add a wrapper method to ts.py to wrap bicoder.verify_temperature_reading()
    # #################################
    # def test_verify_temperature_reading_fail1(self):
    #     """ Verify Temp Reading On Controller Fail Case 1: Value on controller does not match what is
    #     stored in ts.vd """
    #     ts = self.create_test_temperature_sensor_object()
    #     ts.vd = 5
    #     test_pass = False
    #     e_msg = ""
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.temperature_value))
    #     ts.data = mock_data
    #
    #     expected_message = "Unable verify Temperature Sensor TSD0001 (1)'s temperature reading. Received: 6.0, " \
    #                        "Expected: 5"
    #     try:
    #         ts.verify_temperature_reading()
    #
    #     # Catches an Exception from above, meaning the method did raise an exception
    #     # meaning verification failed
    #     except Exception as e:
    #         e_msg = e.message
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
    #                      .format(expected_message, e_msg))
    # TODO add a wrapper method to ts.py to wrap bicoder.verify_two_wire_drop_value()
    # #################################
    # def test_verify_two_wire_drop_value_pass1(self):
    #     """ Verify Two Wire Drop Value On Controller Pass Case 1: Exception is not raised """
    #     ts = self.create_test_temperature_sensor_object()
    #     ts.vt = 5
    #     test_pass = False
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=5".format(opcodes.two_wire_drop))
    #     ts.data = mock_data
    #
    #     try:
    #         # .assertRaises raises an Assertion Error if an Exception is not raised in the method
    #         with self.assertRaises(Exception):
    #             ts.verify_two_wire_drop_value()
    #
    #     # Catches an Assertion Error from above, meaning the method did NOT raise an exception
    #     # meaning verification passed
    #     except AssertionError as ae:
    #         e_msg = ae.message
    #         expected_message = "Exception not raised"
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
    # TODO add a wrapper method to ts.py to wrap bicoder.verify_two_wire_drop_value()
    # #################################
    # def test_verify_two_wire_drop_value_fail1(self):
    #     """ Verify Two Wire Drop Value On Controller Fail Case 1: Value on controller does not match what is
    #     stored in ts.vt """
    #     ts = self.create_test_temperature_sensor_object()
    #     ts.vt = 5
    #     test_pass = False
    #     e_msg = ""
    #
    #     mock_data = status_parser.KeyValues("SN=TSD0001,DS=Blah,{0}=6".format(opcodes.two_wire_drop))
    #     ts.data = mock_data
    #
    #     expected_message = "Unable verify Temperature Sensor TSD0001 (1)'s two wire drop value. Received: 6.0, " \
    #                        "Expected: 5"
    #     try:
    #         ts.verify_two_wire_drop_value()
    #
    #     # Catches an Exception from above, meaning the method did raise an exception
    #     # meaning verification failed
    #     except Exception as e:
    #         e_msg = e.message
    #
    #         # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
    #         if e_msg.strip() == expected_message.strip():
    #             test_pass = True
    #
    #     self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
    #                      .format(expected_message, e_msg))
    #

    if __name__ == "__main__":
        unittest.main()

