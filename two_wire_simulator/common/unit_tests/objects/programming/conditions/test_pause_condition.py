from unittest import TestCase
import mock
from serial import Serial
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.programming.conditions.pause_condition import PauseCondition, SwitchPauseCondition, \
    TemperaturePauseCondition, MoisturePauseCondition, PressurePauseCondition
from common.imports.types import ActionCommands, ProgramConditionCommands
import status_parser
from common.imports import opcodes
import common.objects.programming.conditions.pause_condition


class TestPauseCondition(TestCase):

    ################################
    def setUp(self):
        mock_controller = mock.MagicMock(spec=BaseStation3200)
        mock_controller.ser = "HG00001"
        mock_ser = mock.MagicMock(spec=Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        ip_address = "192.168.100.100"
        port_address = "192.168.100.101"
        socket_port = 1067

        controller = BaseStation3200(_mac="12345",
                                     _serial_number="3K0001",
                                     _firmware_version="",
                                     _serial_port=mock_ser,
                                     _port_address=port_address,
                                     _socket_port=socket_port,
                                     _ip_address=ip_address)

        self.device_type = "SW"
        self.device_serial_number = "TPD0001"
        self.program_address = 3
        self.device_pause_time = 120

        self.uut = PauseCondition(controller,
                                  _program_ad=self.program_address,
                                  _device_type=self.device_type,
                                  _device_serial=self.device_serial_number)

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    ################################
    def test_verify_myself_happy_path1(self):
        """ Verify that verify_myself calls the expected internal methods """
        mock_get_data = self.uut.get_data = mock.MagicMock()

        mock_verify_status = self.uut.verify_status = mock.MagicMock()

        self.uut.verify_myself(expected_status=opcodes.okay)

        # verify the method calls in verify_who_i_am()
        self.assertEquals(mock_get_data.call_count, 1)
        mock_verify_status.assert_called_with(_expected_status=opcodes.okay)

    ################################
    def test_verify_myself_happy_path2(self):
        """ Verify that verify_myself calls the expected internal methods, and skips the others """
        mock_get_data = self.uut.get_data = mock.MagicMock()

        mock_verify_status = self.uut.verify_status = mock.MagicMock()

        self.uut.verify_myself(expected_status=opcodes.okay, skip_get_data=True)

        self.assertEquals(mock_get_data.call_count, 0)
        mock_verify_status.assert_called_with(_expected_status=opcodes.okay)

    ################################
    def test_verify_myself_happy_path3(self):
        """ Verify that verify_myself skips all internal methods """
        mock_get_data = self.uut.get_data = mock.MagicMock()

        mock_verify_status = self.uut.verify_status = mock.MagicMock()

        self.uut.verify_myself(skip_get_data=True)

        self.assertEquals(mock_get_data.call_count, 0)
        self.assertEquals(mock_verify_status.call_count, 0)


class TestSwitchPauseCondition(TestCase):

    ################################
    def setUp(self):
        mock_controller = mock.MagicMock(spec=BaseStation3200)
        mock_controller.ser = "HG00001"
        mock_ser = mock.MagicMock(spec=Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        ip_address = "192.168.100.100"
        port_address = "192.168.100.101"
        socket_port = 1067

        controller = BaseStation3200(_mac="12345",
                                     _serial_number="3K0001",
                                     _firmware_version="",
                                     _serial_port=mock_ser,
                                     _port_address=port_address,
                                     _socket_port=socket_port,
                                     _ip_address=ip_address)

        self.device_type = "SW"
        self.device_serial_number = "TPD0001"
        self.program_address = 3
        self.device_pause_time = 120

        self.uut = SwitchPauseCondition(controller,
                                        _program_ad=self.program_address,
                                        _device_serial=self.device_serial_number)

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    ################################
    def test_set_switch_mode_to_open_happy_path(self):
        """ Verify that set_switch_mode_to_open sends the correct command to the controller """
        expected_command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,                               # {0}
            self.uut.get_id(),                                # {1}
            opcodes.switch_mode,                              # {2}
            ProgramConditionCommands.Attributes.OPEN_MODE     # {3}
        )
        self.uut.set_switch_mode_to_open()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_switch_mode_to_open_fail(self):
        """ Verify exception when set_switch_mode_to_open fails """
        expected = ProgramConditionCommands.Attributes.OPEN_MODE

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s switch mode to:" \
                           " {2}".format(self.program_address,
                                         self.device_serial_number,
                                         expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_switch_mode_to_open()

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_set_switch_mode_to_closed_happy_path(self):
        """ Verify that set_switch_mode_to_closed sends the correct command to the controller """
        expected_command = "SET,PS={0},SW={1},SM=CL".format(self.program_address,
                                                            self.device_serial_number)
        self.uut.set_switch_mode_to_closed()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_switch_mode_to_closed_fail(self):
        """ Verify exception when set_switch_mode_to_closed fails """
        expected = 'CL'

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s switch mode to:" \
                           " {2}".format(self.program_address,
                                         self.device_serial_number,
                                         expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_switch_mode_to_closed()

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_set_switch_mode_to_off_happy_path(self):
        """ Verify that set_switch_mode_to_off sends the correct command to the controller """
        expected_command = "SET,PS={0},SW={1},SM=OF".format(self.program_address,
                                                            self.device_serial_number)
        self.uut.set_switch_mode_to_off()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_switch_mode_to_off_fail(self):
        """ Verify exception when set_switch_mode_to_off fails """
        expected = 'OF'

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s switch mode to:" \
                           " {2}".format(self.program_address,
                                         self.device_serial_number,
                                         expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_switch_mode_to_off()

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_set_switch_pause_time_happy_path(self):
        """ Verify that set_switch_pause_time sends the correct command to the controller """
        minutes_to_set = 120
        expected_command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,                               # {0}
            self.uut.get_id(),                                # {1}
            opcodes.switch_pause_time,                        # {2}
            minutes_to_set                                    # {3}
        )
        self.uut.set_switch_pause_time(_minutes=minutes_to_set)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_switch_pause_time_fail(self):
        """ Verify exception when set_switch_pause_time fails """
        expected = 120

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s switch pause time to:" \
                           " {2}".format(self.program_address,
                                         self.device_serial_number,
                                         expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_switch_pause_time(_minutes=expected)

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_verify_switch_mode_happy_path(self):
        """ Verify that verify_switch_mode passes when controller and test object switch modes match """
        self.uut.mode = 'CL'
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.switch_mode,
            self.uut.mode
        )
        )
        self.uut.data = mock_data

        self.uut.verify_switch_mode()

    ################################
    def test_verify_switch_mode_fail(self):
        """ Verify that verify_switch_mode fails when controller and test object switch modes don't match """
        self.uut.mode = 'CL'
        mismatched_mode = 'OF'
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.switch_mode,
            mismatched_mode
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Event Switch {1} switch mode. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_mode,
                                             self.uut.mode)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_switch_mode()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    def test_verify_switch_serial_number_happy_path(self):
        """ Verify that verify_switch_serial_number passes when controller and test object switch serial modes match """
        self.uut.device_serial = self.device_serial_number
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.event_switch,
            self.uut.device_serial
        )
        )
        self.uut.data = mock_data

        self.uut.verify_switch_serial_number()

    ################################
    def test_verify_switch_serial_number_fail(self):
        """ Verify that verify_switch_serial_number fails wen controller and test object switch serial modes
        don't match
        """
        self.uut.device_serial = self.device_serial_number
        mismatched_serial_number = 'TPD4567'
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.event_switch,
            mismatched_serial_number
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Event Switch {1} serial number. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_serial_number,
                                             self.uut.device_serial)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_switch_serial_number()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    def test_verify_switch_pause_time_happy_path(self):
        """ Verify that verify_switch_pause_time passes when controller and test object switch pause times match """
        self.uut.pause_time = self.device_pause_time
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.switch_pause_time,
            self.uut.pause_time
        )
        )
        self.uut.data = mock_data

        self.uut.verify_switch_pause_time()

    ################################
    def test_verify_switch_pause_time_fail(self):
        """ Verify that verify_switch_pause_time fails when controller and test object switch pause times
        don't match
        """
        self.uut.pause_time = self.device_pause_time
        mismatched_pause_time = 90
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.switch_pause_time,
            mismatched_pause_time
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Event Switch {1} switch pause time. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_pause_time,
                                             self.uut.pause_time)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_switch_pause_time()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    @mock.patch.object(common.objects.programming.conditions.pause_condition.PauseCondition, "verify_myself")
    def test_verify_who_i_am_happy_path(self, mock_verify_myself):
        """ Test that verify_who_i_am calls all expected internal verifiers """
        mock_get_data = self.uut.get_data = mock.MagicMock()

        mock_verify_switch_serial_number = self.uut.verify_switch_serial_number = mock.MagicMock()
        mock_verify_switch_mode = self.uut.verify_switch_mode = mock.MagicMock()
        mock_verify_switch_pause_time = self.uut.verify_switch_pause_time = mock.MagicMock()

        self.uut.verify_who_i_am(expected_status=opcodes.okay)

        # verify the method calls in verify_who_i_am()
        self.assertEquals(mock_get_data.call_count, 1)
        self.assertEquals(mock_verify_switch_serial_number.call_count, 1)
        self.assertEquals(mock_verify_switch_mode.call_count, 1)
        self.assertEquals(mock_verify_switch_pause_time.call_count, 1)
        mock_verify_myself.assert_called_with(mock.ANY, expected_status=opcodes.okay, skip_get_data=True)


class TestTemperaturePauseCondition(TestCase):

    ################################
    def setUp(self):
        mock_controller = mock.MagicMock(spec=BaseStation3200)
        mock_controller.ser = "HG00001"
        mock_ser = mock.MagicMock(spec=Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        ip_address = "192.168.100.100"
        port_address = "192.168.100.101"
        socket_port = 1067

        controller = BaseStation3200(_mac="12345",
                                     _serial_number="3K0001",
                                     _firmware_version="",
                                     _serial_port=mock_ser,
                                     _port_address=port_address,
                                     _socket_port=socket_port,
                                     _ip_address=ip_address)

        self.device_type = "TS"
        self.device_serial_number = "TAT0001"
        self.program_address = 5
        self.device_pause_time = 120

        self.uut = TemperaturePauseCondition(controller,
                                             _program_ad=self.program_address,
                                             _device_serial=self.device_serial_number)

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    ################################
    def test_set_temperature_threshold_happy_path(self):
        """ Verify that set_temperature_threshold sends the correct command to the controller """
        temperature_to_set = 100.2
        expected_command = "SET,PS={0},TS={1},TT={2}".format(self.program_address,
                                                             self.device_serial_number,
                                                             temperature_to_set)
        self.uut.set_temperature_threshold(_degrees=temperature_to_set)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_temperature_threshold_fail(self):
        """ Verify exception when set_temperature_threshold fails """
        expected = '55.4'

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s temperature threshold" \
                           " to: {2}".format(self.program_address,
                                             self.device_serial_number,
                                             expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_temperature_threshold(_degrees=expected)

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_set_temperature_mode_off_happy_path(self):
        """ Verify that set_temperature_mode_off sends the correct command to the controller """
        expected_command = "SET,PS={0},TS={1},TM=OF".format(self.program_address,
                                                            self.device_serial_number)
        self.uut.set_temperature_mode_off()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_temperature_mode_off_fail(self):
        """ Verify exception when set_temperature_mode_off fails """
        expected = ProgramConditionCommands.Attributes.OFF_MODE

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s temperature mode" \
                           " to: {2}".format(self.program_address,
                                             self.device_serial_number,
                                             expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_temperature_mode_off()

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_set_temperature_mode_to_upper_limit_happy_path(self):
        """ Verify that set_temperature_mode_to_upper_limit sends the correct command to the controller """
        expected_command = "SET,PS={0},TS={1},TM=UL".format(self.program_address,
                                                            self.device_serial_number)
        self.uut.set_temperature_mode_to_upper_limit()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_temperature_mode_to_upper_limit_fail(self):
        """ Verify exception when set_temperature_mode_to_upper_limit fails """
        expected = ProgramConditionCommands.Attributes.UPPER_LIMIT_MODE

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s temperature mode" \
                           " to: {2}".format(self.program_address,
                                             self.device_serial_number,
                                             expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_temperature_mode_to_upper_limit()

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_set_temperature_mode_to_lower_limit_happy_path(self):
        """ Verify that set_temperature_mode_to_lower_limit sends the correct command to the controller """
        expected_command = "SET,PS={0},TS={1},TM=LL".format(self.program_address,
                                                            self.device_serial_number)
        self.uut.set_temperature_mode_to_lower_limit()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_temperature_mode_to_lower_limit_fail(self):
        """ Verify exception when set_temperature_mode_to_lower_limit fails """
        expected = ProgramConditionCommands.Attributes.LOWER_LIMIT_MODE

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s temperature mode" \
                           " to: {2}".format(self.program_address,
                                             self.device_serial_number,
                                             expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_temperature_mode_to_lower_limit()

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_verify_temperature_threshold_happy_path(self):
        """ Verify that verify_temperature_threshold passes when the controller and the test object
        temperature thresholds match
        """
        self.uut.threshold = 55.3
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.temperature_trigger_threshold,
            self.uut.threshold
        )
        )
        self.uut.data = mock_data

        self.uut.verify_temperature_threshold()

    ################################
    def test_set_temperature_pause_time_happy_path(self):
        """ Verify that set_temperature_pause_time sends the correct command to the controller """
        minutes_to_set = 120
        expected_command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,                               # {0}
            self.uut.get_id(),                                # {1}
            opcodes.temperature_pause_time,                   # {2}
            minutes_to_set                                    # {3}
        )
        self.uut.set_temperature_pause_time(_minutes=minutes_to_set)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_temperature_pause_time_fail(self):
        """ Verify exception when set_temperature_pause_time fails """
        expected = 120

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s temperature pause " \
                           "time to: {2}".format(self.program_address,
                                                 self.device_serial_number,
                                                 expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_temperature_pause_time(_minutes=expected)

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_verify_temperature_threshold_fail(self):
        """ Verify that verify_temperature_threshold raises the expected exception when the controller and the
        test object temperature thresholds don't match
        """
        self.uut.threshold = 55.3
        mismatched_threshold = 55.4
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.temperature_trigger_threshold,
            mismatched_threshold
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Temperature Sensor {1} temperature threshold." \
                " Received: {2}, Expected: {3}".format(self.program_address,
                                                       self.device_serial_number,
                                                       mismatched_threshold,
                                                       self.uut.threshold)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_temperature_threshold()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    def test_verify_temperature_mode_happy_path(self):
        """ Verify that verify_temperature_mode passes when the controller and the test object
        temperature modes match
        """
        self.uut.mode = 'LL'
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.temperature_sensor_mode,
            self.uut.mode
        )
        )
        self.uut.data = mock_data

        self.uut.verify_temperature_mode()

    ################################
    def test_verify_temperature_mode_fail(self):
        """ Verify that verify_temperature_mode throws the expected exception when the controller and the test object
        temperature modes do not match
        """
        self.uut.mode = 'UL'
        mismatched_mode = 'LL'
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.temperature_sensor_mode,
            mismatched_mode
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Temperature Sensor {1} temperature mode. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_mode,
                                             self.uut.mode)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_temperature_mode()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    def test_verify_temperature_serial_number_happy_path(self):
        """ Verify that verify_temperature_serial_number passes when the controller and the test object temperature
        sensor serial numbers match
        """
        self.uut.device_serial = self.device_serial_number
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.temperature_sensor,
            self.uut.device_serial
        )
        )
        self.uut.data = mock_data

        self.uut.verify_temperature_serial_number()

    ################################
    def test_verify_temperature_serial_number_fail(self):
        """ Verify that verify_temperature_serial_number throws the expected exception when the controller and
        the test object temperature sensor serial numbers do not match
        """
        self.uut.device_serial = self.device_serial_number
        mismatched_serial_number = 'TAT1234'
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.temperature_sensor,
            mismatched_serial_number
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Temperature Sensor {1} serial number. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_serial_number,
                                             self.uut.device_serial)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_temperature_serial_number()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    def test_verify_temperature_pause_time_happy_path(self):
        """ Verify that verify_temperature_pause_time passes when the controller and the test object temperature
        sensor pause times match
        """
        self.uut.pause_time = self.device_pause_time
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.temperature_pause_time,
            self.uut.pause_time
        )
        )
        self.uut.data = mock_data

        self.uut.verify_temperature_pause_time()

    ################################
    def test_verify_temperature_pause_time_fail(self):
        """ Verify that verify_temperature_pause_time throws the expected exception when the controller and
        the test object temperature sensor pause times do not match
        """
        self.uut.pause_time = self.device_pause_time
        mismatched_pause_time = 90
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.temperature_pause_time,
            mismatched_pause_time
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Event Switch {1} temperature pause time. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_pause_time,
                                             self.uut.pause_time)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_temperature_pause_time()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    @mock.patch.object(common.objects.programming.conditions.pause_condition.PauseCondition, "verify_myself")
    def test_verify_who_i_am_happy_path(self, mock_verify_myself):
        """ Verify that verify_who_i_am calls all expected internal verifiers """
        mock_get_data = self.uut.get_data = mock.MagicMock()

        mock_verify_temperature_serial_number = self.uut.verify_temperature_serial_number = mock.MagicMock()
        mock_verify_temperature_mode = self.uut.verify_temperature_mode = mock.MagicMock()
        mock_verify_temperature_threshold = self.uut.verify_temperature_threshold = mock.MagicMock()
        mock_verify_temperature_pause_time = self.uut.verify_temperature_pause_time = mock.MagicMock()

        self.uut.verify_who_i_am(expected_status=opcodes.okay)

        # verify the method calls in verify_who_i_am()
        self.assertEquals(mock_get_data.call_count, 1)
        self.assertEquals(mock_verify_temperature_serial_number.call_count, 1)
        self.assertEquals(mock_verify_temperature_mode.call_count, 1)
        self.assertEquals(mock_verify_temperature_threshold.call_count, 1)
        self.assertEquals(mock_verify_temperature_pause_time.call_count, 1)
        mock_verify_myself.assert_called_with(mock.ANY, expected_status=opcodes.okay, skip_get_data=True)


class TestMoisturePauseCondition(TestCase):

    ################################
    def setUp(self):
        mock_controller = mock.MagicMock(spec=BaseStation3200)
        mock_controller.ser = "HG00001"
        mock_ser = mock.MagicMock(spec=Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        ip_address = "192.168.100.100"
        port_address = "192.168.100.101"
        socket_port = 1067

        controller = BaseStation3200(_mac="12345",
                                     _serial_number="3K0001",
                                     _firmware_version="",
                                     _serial_port=mock_ser,
                                     _port_address=port_address,
                                     _socket_port=socket_port,
                                     _ip_address=ip_address)

        self.device_type = "MS"
        self.device_serial_number = "SB05308"
        self.program_address = 7
        self.device_pause_time = 120

        self.uut = MoisturePauseCondition(controller,
                                          _program_ad=self.program_address,
                                          _device_serial=self.device_serial_number)

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    ################################
    def test_set_moisture_threshold_happy_path(self):
        """ Verify that set_moisture_threshold sends the correct command to the controller """
        moisture_percentage_to_set = 23.3
        expected_command = "SET,PS={0},MS={1},MT={2}".format(self.program_address,
                                                             self.device_serial_number,
                                                             moisture_percentage_to_set)
        self.uut.set_moisture_threshold(_percent=moisture_percentage_to_set)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_moisture_threshold_fail(self):
        """ Verify exception when set_moisture_threshold fails """
        expected = 25.4

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s moisture threshold" \
                           " to: {2}".format(self.program_address,
                                             self.device_serial_number,
                                             expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_moisture_threshold(_percent=expected)

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_set_moisture_mode_off_happy_path(self):
        """ Verify that set_moisture_mode_off sends the correct command to the controller """
        expected_command = "SET,PS={0},MS={1},MM=OF".format(self.program_address,
                                                            self.device_serial_number)
        self.uut.set_moisture_mode_off()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_moisture_mode_off_fail(self):
        """ Verify exception when set_moisture_mode_off fails """
        expected = ProgramConditionCommands.Attributes.OFF_MODE

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s moisture mode" \
                           " to: {2}".format(self.program_address,
                                             self.device_serial_number,
                                             expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_moisture_mode_off()

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_set_moisture_mode_to_upper_limit_happy_path(self):
        """ Verify that set_moisture_mode_to_upper_limit sends the correct command to the controller """
        expected_command = "SET,PS={0},MS={1},MM=UL".format(self.program_address,
                                                            self.device_serial_number)
        self.uut.set_moisture_mode_to_upper_limit()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_moisture_mode_to_upper_limit_fail(self):
        """ Verify exception when set_moisture_mode_to_upper_limit fails """
        expected = ProgramConditionCommands.Attributes.UPPER_LIMIT_MODE

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s moisture mode" \
                           " to: {2}".format(self.program_address,
                                             self.device_serial_number,
                                             expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_moisture_mode_to_upper_limit()

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_set_moisture_mode_to_lower_limit_happy_path(self):
        """ Verify that set_moisture_mode_to_lower_limit sends the correct command to the controller """
        expected_command = "SET,PS={0},MS={1},MM=LL".format(self.program_address,
                                                            self.device_serial_number)
        self.uut.set_moisture_mode_to_lower_limit()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_moisture_mode_to_lower_limit_fail(self):
        """ Verify exception when set_moisture_mode_to_lower_limit fails """
        expected = ProgramConditionCommands.Attributes.LOWER_LIMIT_MODE

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s moisture mode" \
                           " to: {2}".format(self.program_address,
                                             self.device_serial_number,
                                             expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_moisture_mode_to_lower_limit()

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_set_moisture_pause_time_happy_path(self):
        """ Verify that set_moisture_pause_time sends the correct command to the controller """
        minutes_to_set = 120
        expected_command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,                               # {0}
            self.uut.get_id(),                                # {1}
            opcodes.moisture_pause_time,                        # {2}
            minutes_to_set                                    # {3}
        )
        self.uut.set_moisture_pause_time(_minutes=minutes_to_set)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_moisture_pause_time_fail(self):
        """ Verify exception when set_moisture_pause_time fails """
        expected = 120

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s moisture pause" \
                           " time to: {2}".format(self.program_address,
                                                  self.device_serial_number,
                                                  expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_moisture_pause_time(_minutes=expected)

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_verify_moisture_threshold_happy_path(self):
        """ Verify that verify_moisture_threshold passes when the controller and the test object moisture
        thresholds match
        """
        self.uut.threshold = 23.3
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_trigger_threshold,
            self.uut.threshold
        )
        )
        self.uut.data = mock_data

        self.uut.verify_moisture_threshold()

    ################################
    def test_verify_moisture_threshold_fail(self):
        """ Verify that verify_moisture_threshold throws the expected exception when the controller and
        the test object moisture thresholds do not match
        """
        self.uut.threshold = 23.3
        mismatched_threshold = 23.4
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_trigger_threshold,
            mismatched_threshold
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Moisture Sensor {1} moisture threshold. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_threshold,
                                             self.uut.threshold)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_moisture_threshold()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    def test_verify_moisture_mode_happy_path(self):
        """ Verify that verify_moisture_mode passes when the controller and the test object moisture
        modes match
        """
        self.uut.mode = 'LL'
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_mode,
            self.uut.mode
        )
        )
        self.uut.data = mock_data

        self.uut.verify_moisture_mode()

    ################################
    def test_verify_moisture_mode_fail(self):
        """ Verify that verify_moisture_mode throws the expected exception when the controller and
        the test object moisture modes do not match
        """
        self.uut.mode = 'UL'
        mismatched_mode = 'LL'
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_mode,
            mismatched_mode
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Moisture Sensor {1} moisture mode. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_mode,
                                             self.uut.mode)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_moisture_mode()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    def test_verify_moisture_serial_number_happy_path(self):
        """ Verify that verify_moisture_serial_number passes when the controller and the test object moisture
        sensor serial numbers match
        """
        self.uut.device_serial = self.device_serial_number
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_sensor,
            self.uut.device_serial
        )
        )
        self.uut.data = mock_data

        self.uut.verify_moisture_serial_number()

    ################################
    def test_verify_moisture_serial_number_fail(self):
        """ Verify that verify_moisture_serial_number throws the expected exception when the controller and
        the test object temperature sensor serial numbers do not match
        """
        self.uut.device_serial = self.device_serial_number
        mismatched_serial_number = 'SB12345'
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_sensor,
            mismatched_serial_number
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Moisture Sensor {1} serial number. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_serial_number,
                                             self.uut.device_serial)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_moisture_serial_number()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    def test_verify_moisture_pause_time_happy_path(self):
        """ Verify that verify_moisture_pause_time passes when the controller and the test object moisture
        sensor pause times match
        """
        self.uut.pause_time = self.device_pause_time
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_pause_time,
            self.uut.pause_time
        )
        )
        self.uut.data = mock_data

        self.uut.verify_moisture_pause_time()

    ################################
    def test_verify_moisture_pause_time_fail(self):
        """ Verify that verify_moisture_pause_time throws the expected exception when the controller and
        the test object moisture sensor pause times do not match
        """
        self.uut.pause_time = self.device_pause_time
        mismatched_pause_time = 90
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.moisture_pause_time,
            mismatched_pause_time
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Moisture Sensor {1} moisture pause time. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_pause_time,
                                             self.uut.pause_time)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_moisture_pause_time()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    @mock.patch.object(common.objects.programming.conditions.pause_condition.PauseCondition, "verify_myself")
    def test_verify_who_i_am_happy_path(self, mock_verify_myself):
        """ Verify that verify_who_i_am calls the expected internal verifiers """
        mock_get_data = self.uut.get_data = mock.MagicMock()

        mock_verify_moisture_serial_number = self.uut.verify_moisture_serial_number = mock.MagicMock()
        mock_verify_moisture_mode = self.uut.verify_moisture_mode = mock.MagicMock()
        mock_verify_moisture_threshold = self.uut.verify_moisture_threshold = mock.MagicMock()
        mock_verify_moisture_pause_time = self.uut.verify_moisture_pause_time = mock.MagicMock()

        self.uut.verify_who_i_am(expected_status=opcodes.okay)

        # verify the method calls in verify_who_i_am()
        self.assertEquals(mock_get_data.call_count, 1)
        self.assertEquals(mock_verify_moisture_serial_number.call_count, 1)
        self.assertEquals(mock_verify_moisture_mode.call_count, 1)
        self.assertEquals(mock_verify_moisture_threshold.call_count, 1)
        self.assertEquals(mock_verify_moisture_pause_time.call_count, 1)
        mock_verify_myself.assert_called_with(mock.ANY, expected_status=opcodes.okay, skip_get_data=True)


class TestPressurePauseCondition(TestCase):

    #################################
    def setUp(self):
        mock_controller = mock.MagicMock(spec=BaseStation3200)
        mock_controller.ser = "HG00001"
        mock_ser = mock.MagicMock(spec=Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        ip_address = "192.168.100.100"
        port_address = "192.168.100.101"
        socket_port = 1067

        controller = BaseStation3200(_mac="12345",
                                     _serial_number="3K0001",
                                     _firmware_version="",
                                     _serial_port=mock_ser,
                                     _port_address=port_address,
                                     _socket_port=socket_port,
                                     _ip_address=ip_address)

        self.device_type = "PS"
        self.device_serial_number = "PSF0001"
        self.program_address = 9
        self.device_pause_time = 120

        self.uut = PressurePauseCondition(controller,
                                          _program_ad=self.program_address,
                                          _device_serial=self.device_serial_number)

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    ################################
    def test_set_pressure_limit_happy_path(self):
        """ Verify that set_pressure_limit sends the correct command to the controller """
        pressure_limit_to_set = 23.3
        expected_command = "SET,PS={0},PS={1},PL={2}".format(self.program_address,
                                                             self.device_serial_number,
                                                             pressure_limit_to_set)
        self.uut.set_pressure_limit(_limit=pressure_limit_to_set)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_pressure_limit_fail(self):
        """ Verify exception when set_pressure_limit fails """
        expected = 125.4

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s pressure threshold" \
                           " limit to: {2}".format(self.program_address,
                                                   self.device_serial_number,
                                                   expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_pressure_limit(_limit=expected)

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_set_pressure_mode_to_upper_limit_happy_path(self):
        """ Verify that set_pressure_mode_to_upper_limit sends the correct command to the controller """
        expected_command = "SET,PS={0},PS={1},LT=UL".format(self.program_address,
                                                            self.device_serial_number)
        self.uut.set_pressure_mode_to_upper_limit()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_pressure_mode_to_upper_limit_fail(self):
        """ Verify exception when set_pressure_mode_to_upper_limit fails """
        expected = ProgramConditionCommands.Attributes.UPPER_LIMIT_MODE

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s pressure mode" \
                           " to: {2}".format(self.program_address,
                                             self.device_serial_number,
                                             expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_pressure_mode_to_upper_limit()

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_set_pressure_mode_to_lower_limit_happy_path(self):
        """ Verify that set_pressure_mode_to_lower_limit sends the correct command to the controller """
        expected_command = "SET,PS={0},PS={1},LT=LL".format(self.program_address,
                                                            self.device_serial_number)
        self.uut.set_pressure_mode_to_lower_limit()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_pressure_mode_to_lower_limit_fail(self):
        """ Verify exception when set_pressure_mode_to_lower_limit fails """
        expected = ProgramConditionCommands.Attributes.LOWER_LIMIT_MODE

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s pressure mode" \
                           " to: {2}".format(self.program_address,
                                             self.device_serial_number,
                                             expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_pressure_mode_to_lower_limit()

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_set_pressure_pause_time_happy_path(self):
        """ Verify that set_pressure_pause_time sends the correct command to the controller """
        minutes_to_set = 120
        expected_command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,                               # {0}
            self.uut.get_id(),                                # {1}
            opcodes.pressure_pause_time,                      # {2}
            minutes_to_set                                    # {3}
        )
        self.uut.set_pressure_pause_time(_minutes=minutes_to_set)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    ################################
    def test_set_pressure_pause_time_fail(self):
        """ Verify exception when set_pressure_pause_time fails """
        expected = 120

        self.mock_send_and_wait_for_reply.side_effect = Exception("Test Exception")

        expected_message = "Exception occurred trying to set Program {0} Pause Condition {1}'s pressure pause" \
                           " time to: {2}".format(self.program_address,
                                                  self.device_serial_number,
                                                  expected)

        with self.assertRaises(Exception) as context:
            self.uut.set_pressure_pause_time(_minutes=expected)

        self.assertEqual(expected_message, context.exception.message)

    ################################
    def test_verify_pressure_limit_happy_path(self):
        """ Verify that verify_pressure_limit passes when the controller and the test object pressure limits
        match
        """
        self.uut.threshold = 23.3
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.pressure_limit,
            self.uut.threshold
        )
        )
        self.uut.data = mock_data

        self.uut.verify_pressure_limit()

    ################################
    def test_verify_pressure_limit_fail(self):
        """ Verify that verify_pressure_limit throws the expected exception when the controller and
        the test object pressure limits do not match
        """
        self.uut.threshold = 23.3
        mismatched_threshold = 23.4
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.pressure_limit,
            mismatched_threshold
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Pressure Sensor {1} pressure threshold. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_threshold,
                                             self.uut.threshold)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_pressure_limit()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    def test_verify_pressure_mode_happy_path(self):
        """ Verify that verify_pressure_mode passes when the controller and the test object pressure
        modes match
        """
        self.uut.mode = 'LL'
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.limit_type,
            self.uut.mode
        )
        )
        self.uut.data = mock_data

        self.uut.verify_pressure_mode()

    ################################
    def test_verify_pressure_mode_fail(self):
        """ Verify that verify_pressure_mode throws the expected exception when the controller and
        the test object pressure modes do not match
        """
        self.uut.mode = 'UL'
        mismatched_mode = 'LL'
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.limit_type,
            mismatched_mode
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Pressure Sensor {1} pressure mode. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_mode,
                                             self.uut.mode)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_pressure_mode()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    def test_verify_pressure_serial_number_happy_path(self):
        """ Verify that verify_pressure_serial_number passes when the controller and the test object pressure
        sensor serial numbers match
        """
        self.uut.device_serial = self.device_serial_number
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.pressure_sensor,
            self.uut.device_serial
        )
        )
        self.uut.data = mock_data

        self.uut.verify_pressure_serial_number()

    ################################
    def test_verify_pressure_serial_number_fail(self):
        """ Verify that verify_pressure_serial_number throws the expected exception when the controller and
        the test object pressure sensor serial numbers do not match
        """
        self.uut.device_serial = self.device_serial_number
        mismatched_serial_number = 'PFS4567'
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.pressure_sensor,
            mismatched_serial_number
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Pressure Sensor {1} serial number. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_serial_number,
                                             self.uut.device_serial)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_pressure_serial_number()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    def test_verify_pressure_pause_time_happy_path(self):
        """ Verify that verify_pause_time passes when the controller and the test object pressure
        sensor pause times match
        """
        self.uut.pause_time = self.device_pause_time
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.pressure_pause_time,
            self.uut.pause_time
        )
        )
        self.uut.data = mock_data

        self.uut.verify_pressure_pause_time()

    ################################
    def test_verify_pressure_pause_time_fail(self):
        """ Verify that verify_pressure_pause_time throws the expected exception when the controller and
        the test object pressure sensor pause times do not match
        """
        self.uut.pause_time = self.device_pause_time
        mismatched_pause_time = 90
        mock_data = status_parser.KeyValues("{0}={1}".format(
            opcodes.pressure_pause_time,
            mismatched_pause_time
        )
        )
        self.uut.data = mock_data

        e_msg = "Unable to verify Program {0} Pause Condition for Moisture Sensor {1} pressure pause time. Received:" \
                " {2}, Expected: {3}".format(self.program_address,
                                             self.device_serial_number,
                                             mismatched_pause_time,
                                             self.uut.pause_time)
        with self.assertRaises(ValueError) as context:
            self.uut.verify_pressure_pause_time()
        self.assertEqual(first=e_msg, second=context.exception.message)

    ################################
    @mock.patch.object(common.objects.programming.conditions.pause_condition.PauseCondition, "verify_myself")
    def test_verify_who_i_am_happy_path(self, mock_verify_myself):
        """ Verify that verify_who_i_am calls the expected internal verifiers """
        mock_get_data = self.uut.get_data = mock.MagicMock()

        mock_verify_pressure_serial_number = self.uut.verify_pressure_serial_number = mock.MagicMock()
        mock_verify_pressure_mode = self.uut.verify_pressure_mode = mock.MagicMock()
        mock_verify_pressure_limit = self.uut.verify_pressure_limit = mock.MagicMock()
        mock_verify_pressure_pause_time = self.uut.verify_pressure_pause_time = mock.MagicMock()

        self.uut.verify_who_i_am(expected_status=opcodes.okay)

        # verify the method calls in verify_who_i_am()
        self.assertEquals(mock_get_data.call_count, 1)
        self.assertEquals(mock_verify_pressure_serial_number.call_count, 1)
        self.assertEquals(mock_verify_pressure_mode.call_count, 1)
        self.assertEquals(mock_verify_pressure_limit.call_count, 1)
        self.assertEquals(mock_verify_pressure_pause_time.call_count, 1)
        mock_verify_myself.assert_called_with(mock.ANY, expected_status=opcodes.okay, skip_get_data=True)
