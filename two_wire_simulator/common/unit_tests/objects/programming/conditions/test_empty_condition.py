from unittest import TestCase
import mock
from serial import Serial
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.programming.conditions.empty_condition import EmptyCondition, SwitchEmptyCondition, MoistureEmptyCondition, PressureEmptyCondition
from common.imports import opcodes,types


class TestEmptyCondition(TestCase):
    def setUp(self):
        # Create the mock_ser object
        mock_ser = mock.MagicMock(spec=Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        # BaseDevices.ser = mock_ser

        # mock_controller = mock.MagicMock(spec=BaseStation3200)

        ip_address = "192.168.100.100"
        port_address = "192.168.100.101"
        socket_port = 1067

        self.controller = BaseStation3200(_mac="12345",
                                          _serial_number="3K0001",
                                          _firmware_version="",
                                          _serial_port=mock_ser,
                                          _port_address=port_address,
                                          _socket_port=socket_port,
                                          _ip_address=ip_address)

    def create_test_empty_condition(self, _device_type="SW",
                                    _device_serial_number="TPD0001",
                                    _empty_condition_address=1,
                                    _water_source_address=2):

        empty_condition = EmptyCondition(_controller=self.controller,
                                         _empty_condition_address=_empty_condition_address,
                                         _water_source_address=_water_source_address,
                                         _device_type=_device_type,
                                         _device_serial_number=_device_serial_number)

        return empty_condition

    def test_build_obj_configuration_for_send_happy_path(self):
        """ Verify that build_obj_configuration_for_send sends the correct command to the controller """
        empty_condition = self.create_test_empty_condition()
        expected_current_config = "{0},{1}={2},{3}={4},{5}={6},{7}={8}".format(
            types.ActionCommands.SET,                           # {0}
            opcodes.empty_condition,                            # {1}
            str(empty_condition.empty_condition_address),       # {2}
            opcodes.water_source_empty_condition,               # {3}
            str(empty_condition.water_source_address),          # {4}
            str(opcodes.type),                                  # {5}
            str(empty_condition.device_type),                   # {6}
            str(empty_condition.device_type),                   # {7}
            str(empty_condition.device_serial_number)           # {8}
        )

        actual_current_config = empty_condition.build_obj_configuration_for_send()
        self.assertEquals(expected_current_config, actual_current_config)

    def test_set_empty_wait_time_happy_path(self):
        """ Verify that set_empty_wait_time sends the correct command to the controller """
        ec = self.create_test_empty_condition()
        new_minutes = 10
        expected_command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,                   # {0}
            opcodes.empty_condition,                    # {1}
            ec.empty_condition_address,                 # {2}
            opcodes.water_source_empty_condition,       # {3}
            ec.water_source_address,                    # {4}
            opcodes.empty_wait_time_empty_condition,    # {5}
            new_minutes                                 # {6}
        )

        ec.set_empty_wait_time(_minutes=new_minutes)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_empty_wait_time_fail(self):
        """ Verify that message when set_empty_wait_time has an exception when sending the command to the controller """
        exception_message = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_message)
        ec = self.create_test_empty_condition()
        new_minutes = 10
        expected_message = "Exception occurred trying to set Water Source {0}'s Empty Condition {1} to {2} -> {3}"\
                           .format(
                                ec.water_source_address,      # {0}
                                ec.empty_condition_address,   # {1}
                                new_minutes,                  # {2}
                                exception_message             # {3}
                           )
        with self.assertRaises(Exception) as context:
            ec.set_empty_wait_time(_minutes=new_minutes)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_empty_wait_time_happy_path(self):
        """ Verify happy path where set_empty_wait_time on controller matches what's in this object """
        ec = self.create_test_empty_condition()
        on_controller = 100
        expected = 100
        ec.data = mock.MagicMock()
        ec.data.get_value_string_by_key = mock.MagicMock(return_value=on_controller)
        ec.tm = expected
        ec.verify_empty_wait_time()

    def test_verify_empty_wait_time_fail(self):
        """ Verify exception when verify_empty_wait_time value on controller doesn't match what's in this object """
        ec = self.create_test_empty_condition()
        on_controller = 100
        expected = 200
        ec.data = mock.MagicMock()
        ec.data.get_value_string_by_key = mock.MagicMock(return_value=on_controller)
        ec.tm = expected
        expected_message = "Unable to verify Water Source {0} Empty Condition {1}'s 'Empty Wait Time'. Received: {2}, " \
                           "Expected: {3}".format(
                                str(ec.water_source_address),       # {0}
                                str(ec.empty_condition_address),    # {1}
                                str(on_controller),                 # {2}
                                str(expected),                      # {3}
                            )
        with self.assertRaises(ValueError) as context:
            ec.verify_empty_wait_time()

        self.assertEqual(expected_message, context.exception.message)


class TestSwitchEmptyCondition(TestCase):
    def setUp(self):
        # Create the mock_ser object
        mock_ser = mock.MagicMock(spec=Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        # BaseDevices.ser = mock_ser

        # mock_controller = mock.MagicMock(spec=BaseStation3200)

        ip_address = "192.168.100.100"
        port_address = "192.168.100.101"
        socket_port = 1067

        self.controller = BaseStation3200(_mac="12345",
                                          _serial_number="3K0001",
                                          _firmware_version="",
                                          _serial_port=mock_ser,
                                          _port_address=port_address,
                                          _socket_port=socket_port,
                                          _ip_address=ip_address)

    def create_test_switch_empty_condition(self, _empty_condition_address=1, _water_source_address=2,
                                           _device_serial_number="TPD0001"):

        switch_empty_condition = SwitchEmptyCondition(_controller=self.controller,
                                                      _empty_condition_address=_empty_condition_address,
                                                      _water_source_address=_water_source_address,
                                                      _device_serial_number=_device_serial_number)

        return switch_empty_condition

    def test_set_switch_empty_condition_to_open_happy_path(self):
        """ Verify that set_switch_empty_condition_to_open sends the correct command to the controller """
        ec = self.create_test_switch_empty_condition()
        expected_command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,                     # {0}
            opcodes.empty_condition,                # {1}
            ec.empty_condition_address,             # {2}
            opcodes.water_source_empty_condition,   # {3}
            ec.water_source_address,                # {4}
            opcodes.switch_empty_condition,         # {5}
            opcodes.open                            # {6}
        )

        ec.set_switch_empty_condition_to_open()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_switch_empty_condition_to_open_fail(self):
        """ Verify that message when set_switch_empty_condition_to_open has an exception when sending the command
         to the controller """

        exception_message = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_message)
        ec = self.create_test_switch_empty_condition()

        expected_message = "Exception occurred trying to set Water Source {0}'s 'Event Switch Empty Condition {1} to " \
                           "open-> {2}".format(
                                ec.water_source_address,      # {0}
                                ec.empty_condition_address,   # {1}
                                exception_message             # {2}
                            )
        with self.assertRaises(Exception) as context:
            ec.set_switch_empty_condition_to_open()

        self.assertEqual(expected_message, context.exception.message)

    def test_set_switch_empty_condition_to_closed_happy_path(self):
        """ Verify that set_switch_empty_condition_to_closed sends the correct command to the controller """
        ec = self.create_test_switch_empty_condition()
        expected_command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,                     # {0}
            opcodes.empty_condition,                # {1}
            ec.empty_condition_address,             # {2}
            opcodes.water_source_empty_condition,   # {3}
            ec.water_source_address,                # {4}
            opcodes.switch_empty_condition,         # {5}
            opcodes.closed                          # {6}
        )

        ec.set_switch_empty_condition_to_closed()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_switch_empty_condition_to_closed_fail(self):
        """ Verify that message when set_switch_empty_condition_to_closed has an exception when sending
         the command to the controller """

        exception_message = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_message)
        ec = self.create_test_switch_empty_condition()

        expected_message = "Exception occurred trying to set Water Source {0}'s 'Event Switch Empty Condition {1}" \
                           " to closed-> " \
                           "{2}".format(
                                ec.water_source_address,        # {0}
                                ec.empty_condition_address,     # {1}
                                exception_message               # {2}
                            )
        with self.assertRaises(Exception) as context:
            ec.set_switch_empty_condition_to_closed()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_switch_empty_condition_happy_path(self):
        """ Verify happy path where switch_empty_condition on controller matches what's in this object """
        ec = self.create_test_switch_empty_condition()
        on_controller = opcodes.open
        expected = opcodes.open
        ec.data = mock.MagicMock()
        ec.data.get_value_string_by_key = mock.MagicMock(return_value=on_controller)
        ec.lt = expected
        ec.verify_switch_empty_condition()

    def test_verify_switch_empty_condition_fail(self):
        """ Verify exception when verify_switch_empty_condition value on controller doesn't match what's
        in this object """
        ec = self.create_test_switch_empty_condition()
        on_controller = 100
        expected = 200
        ec.data = mock.MagicMock()
        ec.data.get_value_string_by_key = mock.MagicMock(return_value=on_controller)
        ec.lt = expected
        expected_message = "Unable to verify Water Source {0}'s 'Switch Empty Condition'. Received: {1}, Expected: {2}".format(
            str(ec.water_source_address),         # {0}
            str(on_controller),                   # {1}
            str(ec.lt)                            # {2}
        )
        with self.assertRaises(ValueError) as context:
            ec.verify_switch_empty_condition()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_who_i_am_pass1(self):
        """ Verify that verify_who_i_am functions correctly when expected_status is None """
        ec = self.create_test_switch_empty_condition()

        ec.get_data = mock.MagicMock()
        ec.verify_switch_empty_condition = mock.MagicMock()
        ec.verify_empty_wait_time = mock.MagicMock()
        EmptyCondition.verify_myself = mock.MagicMock()

        ec.verify_who_i_am()

        self.assertEquals(ec.get_data.call_count, 1)
        self.assertEquals(ec.verify_switch_empty_condition.call_count, 1)
        self.assertEquals(ec.verify_empty_wait_time.call_count, 1)
        EmptyCondition.verify_myself.assert_called_with(mock.ANY, expected_status=None, skip_get_data=True)

    def test_verify_who_i_am_pass2(self):
        """ Verify that verify_who_i_am functions correctly when expected_status is 'Okay' """
        ec = self.create_test_switch_empty_condition()

        ec.get_data = mock.MagicMock()
        ec.verify_switch_empty_condition = mock.MagicMock()
        ec.verify_empty_wait_time = mock.MagicMock()
        EmptyCondition.verify_myself = mock.MagicMock()

        ec.verify_who_i_am(expected_status=opcodes.okay)

        self.assertEquals(ec.get_data.call_count, 1)
        self.assertEquals(ec.verify_switch_empty_condition.call_count, 1)
        self.assertEquals(ec.verify_empty_wait_time.call_count, 1)
        EmptyCondition.verify_myself.assert_called_with(mock.ANY, expected_status=opcodes.okay, skip_get_data=True)


class TestPressureEmptyCondition(TestCase):

    def setUp(self):
        # Create the mock_ser object
        mock_ser = mock.MagicMock(spec=Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        # BaseDevices.ser = mock_ser

        # mock_controller = mock.MagicMock(spec=BaseStation3200)

        ip_address = "192.168.100.100"
        port_address = "192.168.100.101"
        socket_port = 1067

        self.controller = BaseStation3200(_mac="12345",
                                          _serial_number="3K0001",
                                          _firmware_version="",
                                          _serial_port=mock_ser,
                                          _port_address=port_address,
                                          _socket_port=socket_port,
                                          _ip_address=ip_address)

    def create_test_pressure_empty_condition(self, _empty_condition_address=1, _water_source_address=2,
                                             _device_serial_number="TPD0001"):

        pressure_empty_condition = PressureEmptyCondition(_controller=self.controller,
                                                          _empty_condition_address=_empty_condition_address,
                                                          _water_source_address=_water_source_address,
                                                          _device_serial_number=_device_serial_number)

        return pressure_empty_condition

    def test_set_pressure_empty_limit_happy_path(self):
        """ Verify that set_pressure_empty_limit sends the correct command to the controller """
        ec = self.create_test_pressure_empty_condition()
        new_psi = 100
        expected_command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,                       # {0}
            opcodes.empty_condition,                        # {1}
            ec.empty_condition_address,                     # {2}
            opcodes.water_source_empty_condition,           # {3}
            ec.water_source_address,                        # {4}
            opcodes.pressure_empty_limit_empty_condition,   # {5}
            new_psi                                         # {6}
        )

        ec.set_pressure_empty_limit(_psi=new_psi)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_pressure_empty_limit_fail(self):
        """ Verify that message when set_switch_empty_condition_to_closed has an exception when sending
         the command to the controller """

        exception_message = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_message)
        ec = self.create_test_pressure_empty_condition()
        new_psi = 10
        expected_message = "Exception occurred trying to set Water Source {0}'s Pressure Sensor Empty Condition {1}'s" \
                           "pressure empty limit to {2} -> {3}".format(
                                ec.water_source_address,      # {0}
                                ec.empty_condition_address,   # {1}
                                new_psi,                      # {2}
                                exception_message             # {3}
                            )
        with self.assertRaises(Exception) as context:
            ec.set_pressure_empty_limit(_psi=new_psi)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_pressure_empty_limit_happy_path(self):
        """ Verify happy path where verify_pressure_empty_limit on controller matches what's in this object """
        ec = self.create_test_pressure_empty_condition()
        on_controller = 10
        expected = 10
        ec.data = mock.MagicMock()
        ec.data.get_value_string_by_key = mock.MagicMock(return_value=on_controller)
        ec.pl = expected
        ec.verify_pressure_empty_limit()

    def test_verify_pressure_empty_limit_fail(self):
        """ Verify exception when verify_pressure_empty_limit value on controller doesn't match what's
        in this object """
        ec = self.create_test_pressure_empty_condition()
        on_controller = 100
        expected = 200
        ec.data = mock.MagicMock()
        ec.data.get_value_string_by_key = mock.MagicMock(return_value=on_controller)
        ec.pl = expected
        expected_message = "Unable to verify Water Source {0} Empty Condition {1}'s 'Pressure Empty Limit'. Received:" \
                           " {2}, Expected: {3}".format(
                            str(ec.water_source_address),       # {0}
                            str(ec.empty_condition_address),    # {1}
                            str(on_controller),                 # {2}
                            str(ec.pl),                         # {3}
                           )
        with self.assertRaises(ValueError) as context:
            ec.verify_pressure_empty_limit()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_who_i_am_pass1(self):
        """ Verify that verify_who_i_am functions correctly when expected_status is None """
        ec = self.create_test_pressure_empty_condition()

        ec.get_data = mock.MagicMock()
        ec.verify_pressure_empty_limit = mock.MagicMock()
        ec.verify_empty_wait_time = mock.MagicMock()
        EmptyCondition.verify_myself = mock.MagicMock()

        ec.verify_who_i_am()

        self.assertEquals(ec.get_data.call_count, 1)
        self.assertEquals(ec.verify_pressure_empty_limit.call_count, 1)
        self.assertEquals(ec.verify_empty_wait_time.call_count, 1)
        EmptyCondition.verify_myself.assert_called_with(mock.ANY, expected_status=None, skip_get_data=True)

    def test_verify_who_i_am_pass2(self):
        """ Verify that verify_who_i_am functions correctly when expected_status is 'Okay' """
        ec = self.create_test_pressure_empty_condition()

        ec.get_data = mock.MagicMock()
        ec.verify_pressure_empty_limit = mock.MagicMock()
        ec.verify_empty_wait_time = mock.MagicMock()
        EmptyCondition.verify_myself = mock.MagicMock()

        ec.verify_who_i_am(expected_status=opcodes.okay)

        self.assertEquals(ec.get_data.call_count, 1)
        self.assertEquals(ec.verify_pressure_empty_limit.call_count, 1)
        self.assertEquals(ec.verify_empty_wait_time.call_count, 1)
        EmptyCondition.verify_myself.assert_called_with(mock.ANY, expected_status=opcodes.okay, skip_get_data=True)


class TestMoistureEmptyCondition(TestCase):
    def setUp(self):
        # Create the mock_ser object
        mock_ser = mock.MagicMock(spec=Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        # BaseDevices.ser = mock_ser

        # mock_controller = mock.MagicMock(spec=BaseStation3200)

        ip_address = "192.168.100.100"
        port_address = "192.168.100.101"
        socket_port = 1067

        self.controller = BaseStation3200(_mac="12345",
                                          _serial_number="3K0001",
                                          _firmware_version="",
                                          _serial_port=mock_ser,
                                          _port_address=port_address,
                                          _socket_port=socket_port,
                                          _ip_address=ip_address)

    def create_test_moisture_empty_condition(self, _empty_condition_address=1, _water_source_address=2,
                                             _device_serial_number="TPD0001"):

        moisture_empty_condition = MoistureEmptyCondition(_controller=self.controller,
                                                          _empty_condition_address=_empty_condition_address,
                                                          _water_source_address=_water_source_address,
                                                          _device_serial_number=_device_serial_number)

        return moisture_empty_condition

    def test_set_moisture_empty_limit_happy_path(self):
        """ Verify that set_moisture_empty_limit sends the correct command to the controller """
        ec = self.create_test_moisture_empty_condition()
        new_percent = 100
        expected_command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,                       # {0}
            opcodes.empty_condition,                        # {1}
            ec.empty_condition_address,                     # {2}
            opcodes.water_source_empty_condition,           # {3}
            ec.water_source_address,                        # {4}
            opcodes.moisture_empty_limit_empty_condition,   # {5}
            new_percent                                     # {6}
        )

        ec.set_moisture_empty_limit(_percent=new_percent)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_moisture_empty_limit_fail(self):
        """ Verify that message when set_moisture_empty_limit has an exception when sending
         the command to the controller """

        exception_message = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_message)
        ec = self.create_test_moisture_empty_condition()
        new_percent = 100
        expected_message = "Exception occurred trying to set Water Source {0}'s Moisture Sensor Empty Condition {1}'s" \
                           "moisture empty limit to {2} -> {3}".format(
                                ec.water_source_address,      # {0}
                                ec.empty_condition_address,   # {1}
                                new_percent,                  # {2}
                                exception_message             # {3}
                            )
        with self.assertRaises(Exception) as context:
            ec.set_moisture_empty_limit(_percent=new_percent)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_moisture_empty_limit_happy_path(self):
        """ Verify happy path where verify_moisture_empty_limit on controller matches what's in this object """
        ec = self.create_test_moisture_empty_condition()
        on_controller = 10
        expected = 10
        ec.data = mock.MagicMock()
        ec.data.get_value_string_by_key = mock.MagicMock(return_value=on_controller)
        ec.ml = expected
        ec.verify_moisture_empty_limit()

    def test_verify_pressure_empty_limit_fail(self):
        """ Verify exception when verify_moisture_empty_limit value on controller doesn't match what's
        in this object """
        ec = self.create_test_moisture_empty_condition()
        on_controller = 100
        expected = 200
        ec.data = mock.MagicMock()
        ec.data.get_value_string_by_key = mock.MagicMock(return_value=on_controller)
        ec.ml = expected
        expected_message = "Unable to verify Water Source {0} Empty Condition {1}'s 'Moisture Empty Limit'. Received:" \
                           " {2}, Expected: {3}".format(
                                str(ec.water_source_address),       # {0}
                                str(ec.empty_condition_address),    # {1}
                                str(on_controller),                 # {2}
                                str(expected),                      # {3}
                            )
        with self.assertRaises(ValueError) as context:
            ec.verify_moisture_empty_limit()

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_who_i_am_pass1(self):
        ec = self.create_test_moisture_empty_condition()

        ec.get_data = mock.MagicMock()
        ec.verify_moisture_empty_limit = mock.MagicMock()
        ec.verify_empty_wait_time = mock.MagicMock()
        EmptyCondition.verify_myself = mock.MagicMock()

        ec.verify_who_i_am()

        self.assertEquals(ec.get_data.call_count, 1)
        self.assertEquals(ec.verify_moisture_empty_limit.call_count, 1)
        self.assertEquals(ec.verify_empty_wait_time.call_count, 1)
        EmptyCondition.verify_myself.assert_called_with(mock.ANY, expected_status=None, skip_get_data=True)

    def test_verify_who_i_am_pass2(self):
        """ Verify that verify_who_i_am functions correctly when expected_status is 'Okay' """
        ec = self.create_test_moisture_empty_condition()

        ec.get_data = mock.MagicMock()
        ec.verify_moisture_empty_limit = mock.MagicMock()
        ec.verify_empty_wait_time = mock.MagicMock()
        EmptyCondition.verify_myself = mock.MagicMock()

        ec.verify_who_i_am(expected_status=opcodes.okay)

        self.assertEquals(ec.get_data.call_count, 1)
        self.assertEquals(ec.verify_moisture_empty_limit.call_count, 1)
        self.assertEquals(ec.verify_empty_wait_time.call_count, 1)
        EmptyCondition.verify_myself.assert_called_with(mock.ANY, expected_status=opcodes.okay, skip_get_data=True)
