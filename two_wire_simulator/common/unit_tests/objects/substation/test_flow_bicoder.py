# The following line is needed to prevent unwanted warning messages from Python.
from __future__ import absolute_import
import unittest
import mock
import serial
import status_parser

from common.objects.controllers.bl_32 import BaseStation3200
from common import helper_equations
from common.imports import opcodes
from common.objects.bicoders.flow_bicoder import FlowBicoder
from common.objects.devices.fm import FlowMeter

__author__ = 'Eldin'


class TestFlowBiCoderObject(unittest.TestCase):
    """
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the moisture sensor
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)

        # Add the ser object
        self.bl_3200.ser = self.mock_ser

        # Add faux io flag
        self.bl_3200.faux_io_enabled = True

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_flow_biCoder_object(self, _serial_number="TWF0001"):
        """ Creates a new Master Valve object for testing purposes """
        bicoder = FlowBicoder(_sn=_serial_number, _controller=self.bl_3200, _address=1)

        # Added when equations for calculating flow rate/usage counts implemented.
        bicoder.fm_on_cn = mock.MagicMock(spec=FlowMeter)
        bicoder.kv = 5.0

        return bicoder

    #################################
    def test_set_flow_rate_pass1(self):
        """ Set Flow Rate Pass Case 1: Using Default vr value """
        bicoder = self.create_test_flow_biCoder_object()

        # Expected value is the vr value set at object Valve BiCoder object creation
        bicoder.set_flow_rate()

        # Calculated expected flow rate count being sent
        expected_value = helper_equations.calculate_flow_rate_count_from_gpm(gpm=bicoder.vr, kval=bicoder.kv)

        # vr value is set during this method and should equal the original value
        expected_command = "SET,{0}={1},{2}={3}".format(bicoder.id, bicoder.sn, opcodes.flow_rate_count, expected_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_flow_rate_pass2(self):
        """ Set Flow Rate Pass Case 2: Using 6 as passed in value for vr """
        bicoder = self.create_test_flow_biCoder_object()

        # Expected value is the vr value set at object Valve BiCoder object creation
        new_flow_rate = 6
        bicoder.set_flow_rate(_gallons_per_minute=new_flow_rate)

        # Calculated expected flow rate count being sent
        expected_value = helper_equations.calculate_flow_rate_count_from_gpm(gpm=new_flow_rate,
                                                                             kval=bicoder.kv)

        # vr value is set during this method and should equal the original value
        expected_command = "SET,{0}={1},{2}={3}".format(bicoder.id, bicoder.sn, opcodes.flow_rate_count, expected_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_flow_rate_pass3(self):
        """ Set Flow Rate Pass Case 2: Using a float (6.0) as passed in value for vr """
        bicoder = self.create_test_flow_biCoder_object()

        # Expected value is the vr value set at object Valve BiCoder object creation
        new_flow_rate = 6.0
        bicoder.set_flow_rate(_gallons_per_minute=new_flow_rate)

        # Calculated expected flow rate count being sent
        expected_value = helper_equations.calculate_flow_rate_count_from_gpm(gpm=new_flow_rate,
                                                                             kval=bicoder.kv)

        # vr value is set during this method and should equal the original value
        expected_command = "SET,{0}={1},{2}={3}".format(bicoder.id, bicoder.sn, opcodes.flow_rate_count, expected_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_flow_rate_fail1(self):
        """ Set Flow Rate Fail Case 1: Pass String value as argument """
        bicoder = self.create_test_flow_biCoder_object()

        new_va_value = "b"
        with self.assertRaises(Exception) as context:
            bicoder.set_flow_rate(new_va_value)

        expected_message = "Failed trying to set Flow BiCoder ({0})'s Flow Rate. Invalid argument type, " \
                           "expected an int or float. Received type: {1}".format(
                               str(bicoder.sn),    # {0}
                               type(new_va_value)  # {1}
                           )
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_flow_rate_fail2(self):
        """ Set Flow Rate Fail Case 2: Failed communication with substation """
        bicoder = self.create_test_flow_biCoder_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        new_flow_rate = 5

        with self.assertRaises(Exception) as context:
            bicoder.set_flow_rate(_gallons_per_minute=new_flow_rate)

        e_msg = "Exception occurred trying to set {0} ({1})'s 'Flow Rate' to: '{2}'".format(
            str(bicoder.ty), str(bicoder.sn), str(new_flow_rate))

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_water_usage_pass1(self):
        """ Set Water Usage Pass Case 1: Using Default vg value """
        bicoder = self.create_test_flow_biCoder_object()

        # Expected value is the _va value set at object Valve BiCoder object creation
        expected_value = bicoder.vg
        bicoder.controller.is_substation = mock.MagicMock(return_value=False)
        bicoder.set_water_usage()

        # Calculated expected flow rate count being sent
        expected_value = helper_equations.calculate_flow_usage_count_from_gallons(gal=bicoder.vg,
                                                                                  kval=bicoder.kv)

        # The correct command should be built and passed into the send and wait for reply method
        expected_command = "SET,{0}={1},{2}={3}".format(bicoder.id, bicoder.sn, opcodes.total_usage, expected_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_usage_pass2(self):
        """ Set Water Usage Pass Case 2: Using 6 as passed in value for vg """
        bicoder = self.create_test_flow_biCoder_object()

        # Expected value is the vt value set at object Valve BiCoder object creation
        expected_value = 6
        bicoder.controller.is_substation = mock.MagicMock(return_value=True)
        bicoder.set_water_usage(_water_usage=expected_value)

        # Calculated expected flow rate count being sent
        expected_value = helper_equations.calculate_flow_usage_count_from_gallons(gal=bicoder.vg,
                                                                                  kval=bicoder.kv)

        # The correct command should be built and passed into the send and wait for reply method
        expected_command = "SET,{0}={1},{2}={3}".format(bicoder.id, bicoder.sn, opcodes.flow_usage_count, expected_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_usage_pass3(self):
        """ Set Water Usage Pass Case 2: Using a float (6.0) as passed in value for vg """
        bicoder = self.create_test_flow_biCoder_object()

        # Expected value is the vt value set at object Valve BiCoder object creation
        expected_value = 6.0
        bicoder.controller.is_substation = mock.MagicMock(return_value=True)
        bicoder.set_water_usage(_water_usage=expected_value)

        # Calculated expected flow rate count being sent
        expected_value = helper_equations.calculate_flow_usage_count_from_gallons(gal=bicoder.vg,
                                                                                  kval=bicoder.kv)

        # The correct command should be built and passed into the send and wait for reply method
        expected_command = "SET,{0}={1},{2}={3}".format(bicoder.id, bicoder.sn, opcodes.flow_usage_count, expected_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_water_usage_fail1(self):
        """ Set Water Usage Fail Case 1: Pass String value as argument """
        bicoder = self.create_test_flow_biCoder_object()
        bicoder.controller.is_substation = mock.MagicMock(return_value=True)
        new_vg_value = "b"
        with self.assertRaises(Exception) as context:
            bicoder.set_water_usage(new_vg_value)
        expected_message = "Failed trying to set {0} ({1})'s Water Usage. Invalid argument type, " \
                           "expected an int or float. Received type: {2}".format(
                                bicoder.ty,         # {0}
                                bicoder.sn,         # {1}
                                type(new_vg_value)  # {2}
                            )
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_water_usage_fail2(self):
        """ Set Water Usage Fail Case 2: Failed communication with substation """
        bicoder = self.create_test_flow_biCoder_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            bicoder.set_water_usage()
        e_msg = "Exception occurred trying to set {0} ({1})'s 'water usage' to: '{2}'".format(
            str(bicoder.ty), str(bicoder.sn), str(bicoder.vg))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_flow_rate_pass1(self):
        """ Verify Flow Rate When your controller type is a Substation Pass Case 1: Exception is not raised """
        bicoder = self.create_test_flow_biCoder_object()
        flow_rate_count = 5
        bicoder.vr = helper_equations.calculate_gpm_from_flow_rate_count(count=flow_rate_count,
                                                                         kval=bicoder.kv)
        test_pass = False

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.flow_rate_count, flow_rate_count))
        bicoder.data = mock_data
        
        bicoder.controller.is_substation = mock.MagicMock(return_value=True)

        test_pass = bicoder.verify_flow_rate(_data=mock_data)
        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_flow_rate_pass2(self):
        """ Verify Flow Rate of at least > 0 received from a real device when controller is not in faux io. Should pass
        regardless of the rate value in our FlowMeter object. Since we are using real device, the rate is unpredictable,
        but we can count on at least a value to be returned for the rate that is 0 or greater. """
        bicoder = self.create_test_flow_biCoder_object()
        bicoder.vr = 0.0
        valid_vr = 0.1

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.flow_rate, valid_vr))
        bicoder.data = mock_data

        bicoder.controller.is_substation = mock.MagicMock(return_value=False)
        self.bl_3200.faux_io_enabled = False

        test_pass = bicoder.verify_flow_rate(_data=mock_data)
        self.assertTrue(test_pass)

    #################################
    def test_verify_flow_rate_fail1(self):
        """ Verify Flow Rate Fail Case 1: Value on substation does not match what is stored in the object """
        bicoder = self.create_test_flow_biCoder_object()
        incorrect_flow_rate_count = 10
        bicoder.vr = 50
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.flow_rate_count,
                                                                        incorrect_flow_rate_count))
        bicoder.data = mock_data

        calculated_flow_rate = helper_equations.calculate_gpm_from_flow_rate_count(count=incorrect_flow_rate_count,
                                                                                   kval=bicoder.kv)

        expected_message = "Unable to verify {0} ({1})'s flow rate. Received: {2}, Expected: {3}".format(
            str(bicoder.ty),        # {0}
            str(bicoder.sn),        # {1}
            calculated_flow_rate,   # {2} calculated value
            str(bicoder.vr)         # {3}
        )
        try:
            bicoder.controller.is_substation = mock.MagicMock(return_value=True)
            bicoder.verify_flow_rate(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_flow_rate_fail2(self):
        """Verify value on substation does not match what is stored in the object, using floats instead of ints."""
        bicoder = self.create_test_flow_biCoder_object()
        incorrect_flow_rate_count = 10
        bicoder.vr = 50.0
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.flow_rate_count,
                                                                        incorrect_flow_rate_count))
        bicoder.data = mock_data
        bicoder.controller.is_substation = mock.MagicMock(return_value=True)

        calculated_flow_rate = helper_equations.calculate_gpm_from_flow_rate_count(count=incorrect_flow_rate_count,
                                                                                   kval=bicoder.kv)

        expected_message = "Unable to verify {0} ({1})'s flow rate. Received: {2}, Expected: {3}".format(
            str(bicoder.ty),        # {0}
            str(bicoder.sn),        # {1}
            calculated_flow_rate,   # {2} calculated value
            str(bicoder.vr)         # {3}
        )
        try:
            bicoder.verify_flow_rate(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_flow_rate_fail3(self):
        """Verify exception handling when no rate is received from controller using real devices. Expect at least a
        number. """
        bicoder = self.create_test_flow_biCoder_object()
        bicoder.vr = 50.0
        invalid_vr = ''

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.flow_rate_count, invalid_vr))
        bicoder.data = mock_data

        bicoder.controller.is_substation = mock.MagicMock(return_value=False)
        self.bl_3200.faux_io_enabled = False

        expected_message = "float() argument must be a string or a number"

        with self.assertRaises(Exception) as context:
            bicoder.verify_flow_rate(_data=mock_data)

        e_msg = context.exception.message
        self.assertEqual(expected_message, e_msg)

    #################################
    def test_verify_water_usage_pass1(self):
        """ Verify Water Usage Pass Case 1: Exception is not raised """
        bicoder = self.create_test_flow_biCoder_object()
        water_usage_count = 5
        bicoder.vg = helper_equations.calculate_gallons_from_flow_usage_count(count=water_usage_count,
                                                                              kval=bicoder.kv)
        test_pass = False

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.flow_usage_count, water_usage_count))
        bicoder.data = mock_data
        bicoder.controller.is_substation = mock.MagicMock(return_value=True)

        test_pass = bicoder.verify_water_usage(_data=mock_data)

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_water_usage_pass2(self):
        """ Verify Water Usage Pass Case 2: Exception is not raised when using real device on 3200."""
        bicoder = self.create_test_flow_biCoder_object()
        bicoder.vg = 0
        
        self.bl_3200.faux_io_enabled = False
        self.bl_3200.is_substation = mock.MagicMock(return_value=False)
    
        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.total_usage, '5280.1'))
        bicoder.data = mock_data
    
        test_pass = bicoder.verify_water_usage(_data=mock_data)
    
        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_water_usage_fail1(self):
        """ Verify Water Usage Fail Case 1: Value on substation does not match what is stored in the object"""
        bicoder = self.create_test_flow_biCoder_object()
        incorrect_water_usage_count = 10
        bicoder.vg = 50
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.flow_usage_count,
                                                                        incorrect_water_usage_count))
        bicoder.data = mock_data
        bicoder.controller.is_substation = mock.MagicMock(return_value=True)

        calcualted_water_usage = helper_equations.calculate_gallons_from_flow_usage_count(count=incorrect_water_usage_count,
                                                                                          kval=bicoder.kv)

        expected_message = "Unable to verify {0} ({1})'s water usage. Received: {2}, Expected: {3}".format(
            str(bicoder.ty),            # {0}
            str(bicoder.sn),            # {1}
            calcualted_water_usage,     # {2} calculated value with incorrect parameter
            str(bicoder.vg)             # {3}
        )
        try:
            bicoder.verify_water_usage(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_water_usage_fail2(self):
        """ Verify value on substation does not match what is stored in the object, using floats instead of ints."""
        bicoder = self.create_test_flow_biCoder_object()
        incorrect_water_usage_count = 10
        bicoder.vg = 50.0
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.flow_usage_count,
                                                                        incorrect_water_usage_count))
        bicoder.data = mock_data
        bicoder.controller.is_substation = mock.MagicMock(return_value=True)

        calcualted_water_usage = helper_equations.calculate_gallons_from_flow_usage_count(count=incorrect_water_usage_count,
                                                                                          kval=bicoder.kv)

        expected_message = "Unable to verify {0} ({1})'s water usage. Received: {2}, Expected: {3}".format(
            str(bicoder.ty),            # {0}
            str(bicoder.sn),            # {1}
            calcualted_water_usage,     # {2} calculated value with incorrect parameter
            str(bicoder.vg)             # {3}
        )
        try:
            bicoder.verify_water_usage(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_water_usage_fail3(self):
        """ Verify exception raised when a real device flow meter's usage  value returned from controller is not a
        positive value """
        bicoder = self.create_test_flow_biCoder_object()
        bicoder.vg = 50.0

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.total_usage, 0))
        bicoder.data = mock_data
        
        bicoder.controller.is_substation = mock.MagicMock(return_value=False)
        self.bl_3200.faux_io_enabled = False

        expected_message = "Unable to verify {0} ({1})'s (real device) water usage. Received: {2}, Expected: Value " \
                           "greater than zero.".format(
                               str(bicoder.ty),            # {0}
                               str(bicoder.sn),            # {1}
                               0,                          # {2} calculated value with incorrect parameter
                           )
        
        with self.assertRaises(ValueError) as context:
            bicoder.verify_water_usage(_data=mock_data)
            
        e_msg = context.exception.message
        self.assertEqual(e_msg, expected_message)

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Verify Who I Am Pass Case 1: Exception is not raised """
        bicoder = self.create_test_flow_biCoder_object()

        # Set up mock data to return the default values of our biCoder
        mock_data = status_parser.KeyValues("SN=TWF0001,{0}=0,{1}=0,VT=1.7,SS=OK".format(
            opcodes.flow_rate_count,
            opcodes.flow_usage_count
        ))

        # Mock the get data method so we don't attempt to send a packet to the controller
        bicoder.get_data = mock.MagicMock(return_value=mock_data)

        self.assertEqual(bicoder.verify_who_i_am(_expected_status=opcodes.okay), True)

    #################################
    def test_self_test_and_update_object_attributes_pass1(self):
        """
        self_test_and_update_object_attributes pass case 1:
        Create test flow meter object
        Store: Expected values that can be compared against actual values to make sure they can be reset
        Mock: do_self_test because it goes outside of the method
        Return: None so it does not fail
        Mock: get_data because it goes outside of the method
        Return: None, so it does not fail
        Mock: data.get_value_string_by_key because it goes outside of the method
        Return: Three integers, so they can be converted into floats
        Run: Test, with nothing passed into it
        Compare: expected values, such as self.ad or self.vr, to actual values stored in test
        """
        # Create test flow meter object
        fm = self.create_test_flow_biCoder_object()
        
        # Because bicoder is on a substation, assume we got raw counts and they get converted
        expected_flow_rate = 1.25
        expected_flow_usage = 0.0

        # Store expected values in test
        first_value = 3.0
        second_value = 0.0
        third_value = 1.7

        # Mock do_self_test
        mock_do_self_test = mock.MagicMock(side_effect=None)
        fm.do_self_test = mock_do_self_test

        # Mock get_data
        mock_get_data = mock.MagicMock(side_effect=None)
        fm.get_data = mock_get_data

        # Mock data.get_value_string_by_key, return three integers
        mock_get_value_string_by_key = mock.MagicMock(side_effect=[first_value, second_value, third_value])
        fm.data.get_value_string_by_key = mock_get_value_string_by_key

        # Run the test
        fm.self_test_and_update_object_attributes()

        # Compare expected values to actual values. Use assertIs instead of assertEquals in order to compare the type
        # (float to float) as well as the value that assertEqual would normally compare
        self.assertEqual(expected_flow_rate, fm.vr)
        self.assertEqual(expected_flow_usage, fm.vg)
        self.assertEqual(third_value, fm.vt)

    ################################
    def test_self_test_and_update_object_attributes_fail1(self):
        """
        self_test_and_update_object_attributes fail case 1:
        Create: test flow meter object
        Mock: do_self_test because it goes outside of the method
        Return: None so it does not fail at this point in the method
        Mock: get_data because it goes outside of the method
        Return: None, so it does not fail at this point in the method
        Mock: All three get_value_string_by_key methods
        Return: Strings for all of them, this way they cannot be type casted into floats, and will raise an exception
        Run: The method, and catch an exception, which will let us access the error message
        """
        # Create test flow meter object
        fm = self.create_test_flow_biCoder_object()

        # Store incorrect expected value in test, because the test has the get_value_string_by_key as strings,
        # the test cannot run with them.
        incorrect_value = 0.0

        e_msg = "Exception occurred trying to update attributes of the Flow Bicoder {0} object." \
                " Flow Rate: '{1}'," \
                " Total Usage: '{2}'," \
                " Two Wire Drop Value: '{3}'.\n\tException: {4}" \
                .format(
                    str(fm.sn),  # {0}
                    str(fm.vr),  # {1}
                    str(fm.vg),  # {2}
                    str(fm.vt),  # {3}
                    'could not convert string to float: this'      # {4}
                )

        # Mock do_self_test
        mock_do_self_test = mock.MagicMock(side_effect=None)
        fm.do_self_test = mock_do_self_test

        # Mock get_data
        mock_get_data = mock.MagicMock(side_effect=None)
        fm.get_data = mock_get_data

        # Mock self.data.get_value_string_by_key
        mock_get_value_string_by_key = mock.MagicMock(side_effect=['this', 'should', 'fail'])
        fm.data.get_value_string_by_key = mock_get_value_string_by_key

        # Run the method, and catch an exception
        with self.assertRaises(Exception) as context:
            fm.self_test_and_update_object_attributes()

        # Compare actual error message to expected error message in order to test that the correct message was reached
        self.assertEqual(e_msg, context.exception.message)
        
    #################################
    def test_set_k_value_pass1(self):
        """ Set K Value On Controller Pass Case 1: Using Default _kv value """
        fm = self.create_test_flow_biCoder_object()

        # Expected value is the _df value set at object Zone object creation
        expected_value = fm.kv
        # fm.set_k_value()

        # _df value is set during this method and should equal the original value
        actual_value = fm.kv
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_k_value_pass2(self):
        """ Set K Value On Controller Pass Case 2: Setting new _kv value = 6.0 """
        fm = self.create_test_flow_biCoder_object()

        # Expected _df value is 6
        expected_value = 6.0
        fm.set_k_value(expected_value)

        # _df value is set during this method and should equal the value passed into the method
        actual_value = fm.kv
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_k_value_pass3(self):
        """ Set K Value On Controller Pass Case 3: Command with correct values sent to controller """
        fm = self.create_test_flow_biCoder_object()

        df_value = str(fm.kv)
        expected_command = "SET,FM=TWF0001,KV=" + df_value
        
        # Create mock FlowMeter object reference
        mock_fm_on_cn = mock.MagicMock(spec=FlowMeter)
        # Mock send command
        mock_send_command_with_reply = mock.MagicMock()
        mock_fm_on_cn.send_command_with_reply = mock_send_command_with_reply
        # Update flow bicoder flowmeter pointer to mocked flowmeter
        fm.fm_on_cn = mock_fm_on_cn
        
        fm.set_k_value()
        # self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)
        mock_send_command_with_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_k_value_fail1(self):
        """ Set K Value On Controller Fail Case 1: Pass String value as argument """
        fm = self.create_test_flow_biCoder_object()

        new_kv_value = "b"
        with self.assertRaises(Exception) as context:
            fm.set_k_value(new_kv_value)
        expected_message = "Failed trying to set FM {0} ({1})'s k value. Invalid argument type, expected a float, " \
                           "received: {2}".format(str(fm.sn), str(fm.ad), type(new_kv_value))
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_k_value_fail2(self):
        """ Set K Value On Controller Fail Case 2: Failed communication with controller """
        fm = self.create_test_flow_biCoder_object()

        # Create mock FlowMeter object reference
        mock_fm_on_cn = mock.MagicMock(spec=FlowMeter)
        # Mock send command
        mock_send_command_with_reply = mock.MagicMock(side_effect=Exception)
        mock_fm_on_cn.send_command_with_reply = mock_send_command_with_reply
        # Update flow bicoder flowmeter pointer to mocked flowmeter
        fm.fm_on_cn = mock_fm_on_cn

        with self.assertRaises(Exception) as context:
            fm.set_k_value()
            
        e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'K Value' to: '{2}'".format(
                fm.sn, str(fm.ad), str(fm.kv))
        self.assertEqual(first=e_msg, second=context.exception.message)
        
if __name__ == "__main__":
    unittest.main()
