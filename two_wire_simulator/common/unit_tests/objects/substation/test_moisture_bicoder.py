# The following line is needed to prevent unwanted warning messages from Python.
from __future__ import absolute_import
import unittest
import mock
import serial
import status_parser

from common.imports import opcodes
from common.objects.bicoders.moisture_bicoder import MoistureBicoder
from common.objects.controllers.bl_32 import BaseStation3200

__author__ = 'Eldin'


class TestMoistureBiCoderObject(unittest.TestCase):
    """
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the moisture sensor
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)

        # Add the ser object
        self.bl_3200.ser = self.mock_ser

        # Add faux io flag
        self.bl_3200.faux_io_enabled = True

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_moisture_biCoder_object(self, _serial_number="SB00001"):
        """ Creates a new Master Valve object for testing purposes """
        bicoder = MoistureBicoder(_sn=_serial_number, _controller=self.bl_3200, _address=1)

        return bicoder

    #################################
    def test_set_moisture_percent_pass1(self):
        """ Set Moisture Percent Pass Case 1: Using Default vp value """
        bicoder = self.create_test_moisture_biCoder_object()

        # Expected value is the vp value set at object Valve BiCoder object creation
        expected_value = bicoder.vp
        bicoder.set_moisture_percent()

        # vp value is set during this method and should equal the original value
        actual_value = bicoder.vp
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_moisture_percent_pass2(self):
        """ Set Moisture Percent Pass Case 2: Using 6 as passed in value for vp """
        bicoder = self.create_test_moisture_biCoder_object()

        # Expected value is the vp value set at object Valve BiCoder object creation
        expected_value = 6
        bicoder.set_moisture_percent(_percent=expected_value)

        # vt value is set during this method and should equal the original value
        actual_value = bicoder.vp
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_moisture_percent_pass3(self):
        """ Set Moisture Percent Pass Case 2: Using a float (6.0) as passed in value for vp """
        bicoder = self.create_test_moisture_biCoder_object()

        # Expected value is the vp value set at object Valve BiCoder object creation
        expected_value = 6.0
        bicoder.set_moisture_percent(_percent=expected_value)

        # vt value is set during this method and should equal the original value
        actual_value = bicoder.vp
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_moisture_percent_pass4(self):
        """ Set Moisture Percent Pass Case 3: Command with correct values sent to controller """
        bicoder = self.create_test_moisture_biCoder_object()

        vp_value = str(bicoder.vp)
        expected_command = "SET,{0}={1},VP={2}".format(bicoder.id, bicoder.sn, vp_value)
        bicoder.set_moisture_percent()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_moisture_percent_fail1(self):
        """ Set Moisture Percent Fail Case 1: Pass String value as argument """
        bicoder = self.create_test_moisture_biCoder_object()

        new_va_value = "b"
        with self.assertRaises(Exception) as context:
            bicoder.set_moisture_percent(new_va_value)
        expected_message = "Failed trying to set {0} ({1})'s moisture percent. Invalid argument type, " \
                           "expected an int or float, received: {2}".format(
                                bicoder.ty,         # {0}
                                str(bicoder.sn),    # {1}
                                type(new_va_value)  # {2}
                            )
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_moisture_percent_fail2(self):
        """ Set Moisture Percent Fail Case 2: Failed communication with substation """
        bicoder = self.create_test_moisture_biCoder_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            bicoder.set_moisture_percent()
        e_msg = "Exception occurred trying to set {0} ({1})'s 'Moisture Percent' to: '{2}'".format(
            str(bicoder.ty), str(bicoder.sn), str(bicoder.vp))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_temperature_reading_pass1(self):
        """ Set Temperature Reading Pass Case 1: Using Default vd value """
        bicoder = self.create_test_moisture_biCoder_object()

        # Expected value is the _va value set at object Valve BiCoder object creation
        expected_value = bicoder.vd
        bicoder.set_temperature_reading()

        # vd value is set during this method and should equal the original value
        actual_value = bicoder.vd
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_temperature_reading_pass2(self):
        """ Set Temperature Reading Pass Case 2: Using 6 as passed in value for vd """
        bicoder = self.create_test_moisture_biCoder_object()

        # Expected value is the vt value set at object Valve BiCoder object creation
        expected_value = 6
        bicoder.set_temperature_reading(_temp=expected_value)

        # vt value is set during this method and should equal the original value
        actual_value = bicoder.vd
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_temperature_reading_pass3(self):
        """ Set Temperature Reading Pass Case 2: Using a float (6.0) as passed in value for vd """
        bicoder = self.create_test_moisture_biCoder_object()

        # Expected value is the vt value set at object Valve BiCoder object creation
        expected_value = 6.0
        bicoder.set_temperature_reading(_temp=expected_value)

        # vt value is set during this method and should equal the original value
        actual_value = bicoder.vd
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_temperature_reading_pass4(self):
        """ Set Temperature Reading Pass Case 3: Command with correct values sent to controller """
        bicoder = self.create_test_moisture_biCoder_object()

        vd_value = str(bicoder.vd)
        expected_command = "SET,{0}={1},VD={2}".format(bicoder.id, bicoder.sn, vd_value)
        bicoder.set_temperature_reading()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_temperature_reading_fail1(self):
        """ Set Temperature Reading Fail Case 1: Pass String value as argument """
        bicoder = self.create_test_moisture_biCoder_object()

        new_vd_value = "b"
        with self.assertRaises(Exception) as context:
            bicoder.set_temperature_reading(new_vd_value)
        expected_message = "Failed trying to set {0} ({1})'s temperature reading. Invalid argument type, " \
                           "expected an int or float, received: {2}".format(
                                bicoder.ty,         # {0}
                                bicoder.sn,         # {1}
                                type(new_vd_value)  # {2}
                            )
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_temperature_reading_fail2(self):
        """ Set Temperature Reading Fail Case 2: Failed communication with substation """
        bicoder = self.create_test_moisture_biCoder_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            bicoder.set_temperature_reading()
        e_msg = "Exception occurred trying to set {0} ({1})'s 'Temperature Reading' to: '{2}'".format(
            str(bicoder.ty), str(bicoder.sn), str(bicoder.vd))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_moisture_percent_pass1(self):
        """ Verify Moisture Percent Pass Case 1: Exception is not raised """
        bicoder = self.create_test_moisture_biCoder_object()
        bicoder.vp = 5.0
        test_pass = False

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VP=5")
        bicoder.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                bicoder.verify_moisture_percent(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_moisture_percent_pass2(self):
        """ Verify Moisture Percent Pass Case 2: Verifies any moisture reading received from a real device is at least 0
        percent or higher."""
        bicoder = self.create_test_moisture_biCoder_object()
        bicoder.vp = 0.0
        valid_vp = 0.5

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VP={0}".format(valid_vp))
        bicoder.data = mock_data

        self.bl_3200.faux_io_enabled = False

        success = bicoder.verify_moisture_percent(_data=mock_data)
        self.assertTrue(success)

    #################################
    def test_verify_moisture_percent_fail1(self):
        """ Verify Moisture Percent Fail Case 1: Value on substation does not match what is stored in the object """
        bicoder = self.create_test_moisture_biCoder_object()
        bicoder.vp = 5.0
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VP=6")
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} ({1})'s moisture percent. Received: {2}, Expected: {3}".format(
            str(bicoder.ty),    # {0}
            str(bicoder.sn),    # {1}
            "6.0",              # {2} it is 6 because that is what we declared in our mock data
            str(bicoder.vp)     # {3}
        )
        try:
            bicoder.verify_moisture_percent(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_moisture_percent_fail2(self):
        """ Verify Moisture Percent Fail Case 2: Moisture reading received from real device exceeded allowable
        variance. """
        bicoder = self.create_test_moisture_biCoder_object()
        bicoder.vp = 0.0
        invalid_vp = -1.0

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VP={0}".format(invalid_vp))
        bicoder.data = mock_data

        self.bl_3200.faux_io_enabled = False

        expected_message = "Unable to verify {0} ({1})'s (real device) moisture percent. Received: {2}, " \
                           "Expected: {3}".format(
                               bicoder.ty,                      # {0}
                               bicoder.sn,                      # {1}
                               invalid_vp,                      # {2}
                               "Value between 0 and 5 degrees."  # {3}
                           )

        with self.assertRaises(ValueError) as context:
            bicoder.verify_moisture_percent(_data=mock_data)

        e_msg = context.exception.message
        self.assertEqual(expected_message, e_msg)

    #################################
    def test_verify_temperature_reading_pass1(self):
        """ Verify Temperature Reading Pass Case 1: Exception is not raised """
        bicoder = self.create_test_moisture_biCoder_object()
        bicoder.vd = 5.0
        test_pass = False

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VD=5")
        bicoder.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                bicoder.verify_temperature_reading(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_temperature_reading_pass2(self):
        """ Verify Temperature Reading Pass Case 2: Verifies any temperature reading received from a real device is at
         least 1 degree or higher. """
        bicoder = self.create_test_moisture_biCoder_object()
        bicoder.vd = 0.0
        valid_vd = 5.0

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VD={0}".format(valid_vd))
        bicoder.data = mock_data

        self.bl_3200.faux_io_enabled = False

        success = bicoder.verify_temperature_reading(_data=mock_data)
        self.assertTrue(success)

    #################################
    def test_verify_temperature_reading_fail1(self):
        """ Verify Temperature Reading Fail Case 1: Value on substation does not match what is stored in the object"""
        bicoder = self.create_test_moisture_biCoder_object()
        bicoder.vd = 5.0
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VD=6")
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} ({1})'s temperature reading. Received: {2}, Expected: {3}".format(
            str(bicoder.ty),    # {0}
            str(bicoder.sn),    # {1}
            "6.0",              # {2} it is 6 because that is what we declared in our mock data
            str(bicoder.vd)     # {3}
        )
        try:
            bicoder.verify_temperature_reading(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_temperature_reading_fail2(self):
        """ Verify Temperature Reading Fail Case 2: Temperature reading received from real device exceeded allowable
        variance. """
        bicoder = self.create_test_moisture_biCoder_object()
        bicoder.vd = 0.0
        invalid_vd = 0.0

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VD={0}".format(invalid_vd))
        bicoder.data = mock_data

        self.bl_3200.faux_io_enabled = False

        expected_message = "Unable to verify {0} ({1})'s (real device) temperature reading. Received: {2}, " \
                           "Expected: {3}".format(
                               bicoder.ty,                      # {0}
                               bicoder.sn,                      # {1}
                               invalid_vd,                      # {2}
                               "Value greater than 1 degrees."  # {3}
                           )

        with self.assertRaises(ValueError) as context:
            bicoder.verify_temperature_reading(_data=mock_data)

        e_msg = context.exception.message
        self.assertEqual(expected_message, e_msg)

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Verify Who I Am Pass Case 1: Exception is not raised """
        bicoder = self.create_test_moisture_biCoder_object()

        # Set up mock data to return the default values of our biCoder
        mock_data = status_parser.KeyValues("SN=SB00001,VP=21.3,VD=71.5,VT=1.7,SS=OK")

        # Mock the get data method so we don't attempt to send a packet to the controller
        bicoder.get_data = mock.MagicMock(return_value=mock_data)

        self.assertEqual(bicoder.verify_who_i_am(_expected_status=opcodes.okay), True)

    #################################
    def test_self_test_and_update_object_attributes_pass1(self):
        """ Verify Self Test and Update Object Attributes Pass Case 1: Get data and then assign the new values to the objects attributes
        Create test moisture sensor object
        Create the variables required to test the method
        Mock: do_self_test because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: get_data because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: data and get_value_string_by_key so we can output the values we want
            Return: Three values, one each for moisture_percent, temperature_value, and two_wire_drop
        Call the method
        """
        # Create the moisture sensor object
        bicoder = self.create_test_moisture_biCoder_object()

        vp_value = 2.5
        vd_value = 5
        vt_value = 26.5

        # Mock the do_self_test method
        mock_do_self_test = mock.MagicMock()
        bicoder.do_self_test = mock_do_self_test

        # Mock the get_data method
        mock_get_data = mock.MagicMock()
        bicoder.get_data = mock_get_data

        # Mock get_value_string_by_key and the data object that is in the moisture sensor object
        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(side_effect=[vp_value, vd_value, vt_value])
        bicoder.data = mock_data

        # Call the method
        bicoder.self_test_and_update_object_attributes()

        # Verify the values were set
        self.assertEqual(vp_value, bicoder.vp)
        self.assertEqual(vd_value, bicoder.vd)
        self.assertEqual(vt_value, bicoder.vt)

    #################################
    def test_self_test_and_update_object_attributes_fail1(self):
        """ Verify Self Test and Update Object Attributes Fail Case 1: Catch Exception that is thrown
        Create test moisture sensor object
        Create the variables required to test the method
        Mock: do_self_test because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: get_data because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: data and get_value_string_by_key so we can output the values we want
            Return: Three values, one each for moisture_percent, temperature_value, and two_wire_drop
        Create the expected message
        Call the method
        Compare the expected error message with the actual error message
        """
        # Create the moisture sensor object
        bicoder = self.create_test_moisture_biCoder_object()

        # Mock the do_self_test method
        mock_do_self_test = mock.MagicMock()
        bicoder.do_self_test = mock_do_self_test

        # Mock the get_data method
        mock_get_data = mock.MagicMock()
        bicoder.get_data = mock_get_data

        # Mock get_value_string_by_key and the data object that is in the moisture sensor object
        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(side_effect=Exception)
        bicoder.data = mock_data

        # Create the expected message
        expected_msg = "Exception occurred trying to update attributes of the Moisture Bicoder {0} object." \
                       " Moisture Percent: '{1}'," \
                       " Temperature: '{2}'," \
                       " Two Wire Drop Value: '{3}'.\n\tException: ".format(
                            str(bicoder.sn),   # {0}
                            str(bicoder.vp),   # {1}
                            str(bicoder.vd),   # {2}
                            str(bicoder.vt)    # {3}
                        )

        # Run method that will tested
        with self.assertRaises(Exception) as context:
            bicoder.self_test_and_update_object_attributes()

        # Compare the expected with the actual
        self.assertEqual(expected_msg, context.exception.message)

    if __name__ == "__main__":
        unittest.main()
