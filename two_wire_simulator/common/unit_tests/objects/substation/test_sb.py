# The following line is needed to prevent unwanted warning messages from Python.
from __future__ import absolute_import

__author__ = 'John'

import unittest

import mock
import serial
import status_parser
import datetime

from common.objects.base_classes.devices import BaseDevices
from common.objects.controllers.bl_sb import Substation
from common.imports import opcodes
from common.date_package.date_resource import date_mngr
from common.variables.common import dictionary_for_status_codes

class TestSubstationObject(unittest.TestCase):
    """
    Substation Lat & Lng
    latitude = 43.609768
    longitude = -116.310569
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock for get and send methods
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Set serial instance to mock serial
        BaseDevices.ser = self.mock_ser
        Substation.check_and_update_firmware = mock.MagicMock()

        BaseDevices.substation_lat = float(43.609768)
        BaseDevices.substation_long = float(-116.310569)

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """ Cleaning up after the test. """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_substation_object(self, _diffip="129.0.0.0", _diffmac="0008EE218C85", _diffsn="3K00001",
                                      _diffser=None, _diffvr="16.0.519", _diffportaddress=None, _diffsocketport=None):
        """ Creates a new substation object for use in a unit test """

        if _diffser is not None:
            serial_port = _diffser
        else:
            serial_port = self.mock_ser

        sb = Substation(_mac=_diffmac, _serial_port=serial_port, _serial_number=_diffsn, _firmware_version=_diffvr,
                        _port_address=_diffportaddress, _socket_port=_diffsocketport, _ip_address=_diffip,)
        return sb

    #################################
    def test_set_default_values_pass1(self):
        """ Verify Default Values On Substation. """
        sb = self.create_test_substation_object()

        # Expected values for the properties set at substation object creation
        expected_lat_value = 43.609768
        expected_long_value = -116.310569
        expected_ip_address = "129.0.0.0"
        expected_mac_address = "0008EE218C85"

        # Property values are set during this method and should equal the original value
        actual_lat_value = sb.la
        self.assertEqual(first=expected_lat_value, second=actual_lat_value)

        actual_long_value = sb.lg
        self.assertEqual(first=expected_long_value, second=actual_long_value)

        actual_ip_address = sb.ad
        self.assertEqual(first=expected_ip_address, second=actual_ip_address)

        actual_mac_address = sb.mac
        self.assertEqual(first=expected_mac_address, second=actual_mac_address)

    #################################
    def test_set_default_values_pass2(self):
        """ Change the Default Values On Substation. """
        sb = self.create_test_substation_object()
        sb.sn = "TSD0002"
        sb.ds = "TSD0002 Test Substation"
        sb.la = 44.609768
        sb.lg = -117.310569

        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                sb.set_default_values()

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_set_message_pass1(self):
        """ Set a message on the Substation; pass in a valid status code for ct_type of CN. """
        sb = self.create_test_substation_object()

        status_code = "OC"
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                sb.set_message(status_code)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_set_message_fail1(self):
        """ Set a message on the Substation; pass in an invalid status code for ct_type of CN. """
        sb = self.create_test_substation_object()

        status_code = "XX"

        error_msg = "Exception caught in messages.set_message: Status Code 'XX' is not valid for object CN."

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            sb.set_message(status_code)
        self.assertEqual(first=error_msg, second=context.exception.message)

    #################################
    def test_verify_date_and_time_pass1(self):
        """ Get the date and time from the Substation. """
        sb = self.create_test_substation_object()

        # Assign controller date and time
        date_mngr.controller_datetime.obj = datetime.datetime.strptime('12/30/2011 10:12:32',
                                                                       "%m/%d/%Y %H:%M:%S").date()
        date_mngr.controller_datetime.time_obj.obj = datetime.datetime.strptime('12/30/2011 10:12:32',
                                                                                "%m/%d/%Y %H:%M:%S").time()
        # Mock get date and time
        get_date_and_time = mock.MagicMock()
        get_date_and_time.get_value_string_by_key = mock.MagicMock(return_value='12/30/2011 10:12:32')
        sb.get_date_and_time = mock.MagicMock(return_value=get_date_and_time)

        test_pass = False
        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                sb.get_date_and_time()

        # self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_status_fail1(self):
        """ Get the status from the Substation. """
        sb = self.create_test_substation_object()

        # Set up mock data that our method will use
        mock_data = status_parser.KeyValues("SS=OK")
        sb.data = mock_data
        status_code = "XX"

        error_msg = "Unable verify {0} 'Status'. Received: {1}, Expected: {2}".format(
                sb.ds,                  # {0}
                str('OK'),              # {1}
                str(status_code)        # {2}
            )

        self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        with self.assertRaises(Exception) as context:
            sb.verify_status(status_code)
        self.assertEqual(first=error_msg, second=context.exception.message)

    #################################
    def test_load_dv_to_cn_pass1(self):
        """ Load Device to Substation. Exception is not raised for correct decoder type:
         Single Valve Decoder"""
        dv_type = opcodes.single_valve_decoder
        dv_list = ["TSD0001"]
        sb = self.create_test_substation_object()

        expected_command = "DEV,{0}=TSD0001".format(dv_type)
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                sb.load_dv_to_cn(dv_type, dv_list)

        # self.mock_ser.send_and_wait_for_reply.side_effect = Exception
        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_load_dv_to_cn_pass2(self):
        """ Load multiple serial numbers to Substation. """
        dv_type = opcodes.single_valve_decoder
        dv_list = ["TSD0001", "TSD0002", "TSD0003"]
        sb = self.create_test_substation_object()
        test_pass = False

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                sb.load_dv_to_cn(dv_type, dv_list)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_load_dv_to_cn_fail1(self):
        """ Load Device to Substation: Incorrect device type passed as argument """
        dv_type = opcodes.booster_pump
        dv_list = ["TSD0001"]
        sb = self.create_test_substation_object()

        with self.assertRaises(Exception) as context:
            sb.load_dv_to_cn(dv_type, dv_list)
        e_msg = "[CONTROLLER] incorrect decoder type: {0}".format(str(dv_type))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_load_dv_to_cn_fail2(self):
        """ Incorrect serial number size passed as argument to Substation """
        dv_type = opcodes.single_valve_decoder
        dv_list = ["TSD0001", "TSD002"]
        sb = self.create_test_substation_object()

        with self.assertRaises(Exception) as context:
            sb.load_dv_to_cn(dv_type, dv_list)
        e_msg = "Device id is not a valid serial number: TSD002"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_load_dv_to_cn_fail3(self):
        """ Failed Communication with the Substation """
        dv_type = opcodes.single_valve_decoder
        dv_list = ["TSD0001", "TSD0002"]
        sb = self.create_test_substation_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            sb.load_dv_to_cn(dv_type, dv_list)
        e_msg = "Load device(s) failed.. "
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Verifies all attributes for the substation object pass case 1: Verifies everything, should run through
         all tests. Expected status should be something other than none """
        # Create the substation object
        sb = self.create_test_substation_object()

        # Verify get data
        get_data = mock.MagicMock()
        sb.get_data = get_data

        # Verify description
        description_verifier = mock.MagicMock()
        sb.verify_description = description_verifier

        # Verify serial number
        serial_number = mock.MagicMock()
        sb.verify_serial_number = serial_number

        # Verify status
        status_verifier = mock.MagicMock()
        sb.verify_status = status_verifier

        # Verify code version
        code_version = mock.MagicMock()
        sb.verify_code_version = code_version

        # Run program while setting expected status to something other than none
        sb.verify_who_i_am(_expected_status='OK')

    #################################
    def test_verify_full_configuration_pass1(self):
        """ Get the date and time from the Substation. """
        sb = self.create_test_substation_object()

        # Mock get date and time
        self.get_and_wait_for_reply = mock.MagicMock()
        sb.verify_who_i_am = mock.MagicMock()

        sb.verify_full_configuration()

    #################################
    def test_load_all_dv_to_cn_pass1(self):
        """
        load_all_dv_to_cn pass case 1:
        Mock: load dv to cn d1
        Return: None because so it does not fail
        Mock: load dv to cn mv d1
        Return: None because so it does not fail
        Mock: load dv to cn d2
        Return: None because so it does not fail
        Mock: load dv to cn mv d2
        Return: None because so it does not fail
        Mock: load dv to cn mv d2
        Return: None because so it does not fail
        Mock: load dv to cn d4
        Return: None because so it does not fail
        Mock: load dv to cn DD
        Return: None because so it does not fail
        Mock: load dv to cn MS
        Return: None because so it does not fail
        Mock: load dv to cn FM
        Return: None because so it does not fail
        Mock: load dv to cn TS
        Return: None because so it does not fail
        Mock: load dv to cn SW
        Return: None because so it does not fail
        Run test: Have values for all ten decoders loaded onto controller
        Return: Should not return anything, as this is the case where it does not return an exception.
        """
        # Create test substation object
        sb = self.create_test_substation_object()

        # Mock each of the load dv to cn methods
        mock_load_dv_to_cn_d1 = mock.MagicMock(side_effect=None)
        sb.load_dv_to_cn = mock_load_dv_to_cn_d1
        mock_load_dv_to_cn_mv_d1 = mock.MagicMock(side_effect=None)
        sb.load_dv_to_cn = mock_load_dv_to_cn_mv_d1
        mock_load_dv_to_cn_d2 = mock.MagicMock(side_effect=None)
        sb.load_dv_to_cn = mock_load_dv_to_cn_d2
        mock_load_dv_to_cn_mv_d2 = mock.MagicMock(side_effect=None)
        sb.load_dv_to_cn = mock_load_dv_to_cn_mv_d2
        mock_load_dv_to_cn_d4 = mock.MagicMock(side_effect=None)
        sb.load_dv_to_cn = mock_load_dv_to_cn_d4
        mock_load_dv_to_cn_dd = mock.MagicMock(side_effect=None)
        sb.load_dv_to_cn = mock_load_dv_to_cn_dd
        mock_load_dv_to_cn_ms = mock.MagicMock(side_effect=None)
        sb.load_dv_to_cn = mock_load_dv_to_cn_ms
        mock_load_dv_to_cn_fm = mock.MagicMock(side_effect=None)
        sb.load_dv_to_cn = mock_load_dv_to_cn_fm
        mock_load_dv_to_cn_ts = mock.MagicMock(side_effect=None)
        sb.load_dv_to_cn = mock_load_dv_to_cn_ts
        mock_load_dv_to_cn_sw = mock.MagicMock(side_effect=None)
        sb.load_dv_to_cn = mock_load_dv_to_cn_sw

        # Run method with all of the lists filled correctly
        sb.load_all_dv_to_cn(d1_list=["TSD0001"],
                             mv_d1_list=["TSD0001"],
                             d2_list=["TSD0001"],
                             mv_d2_list=["TSD0001"],
                             d4_list=["TSD0001"],
                             dd_list=["TSD0001"],
                             ms_list=["TSD0001"],
                             fm_list=["TSD0001"],
                             ts_list=["TSD0001"],
                             sw_list=["TSD0001"],
                             is_list=["TSD0001"],
                             ar_list=["TSD0001"],
                             unit_testing=True)

    #################################
    def test_load_all_dv_to_cn_fail1(self):
        """
        load_all_dv_to_cn fail case 1:
        Mock: load dv to cn d1
        Return: An exception so it will fail
        Store: An expected error message
        Run: Test with all of the lists filled in (not necessarily with correct values)
        Compare: The expected error message with the actual error message
        """
        # Create test substation object
        sb = self.create_test_substation_object()

        # Expected error message
        e_msg = "Exception occurred loading all available devices to Substation: "

        # Mock failed load dv to cn method
        mock_load_dv_to_cn_d1 = mock.MagicMock(side_effect=Exception)
        sb.load_dv_to_cn = mock_load_dv_to_cn_d1

        # Run method with all of the lists filled correctly
        with self.assertRaises(Exception) as context:
            sb.load_all_dv_to_cn(d1_list=["TSD0001"],
                                 mv_d1_list=["TSD0001"],
                                 d2_list=["TSD0001"],
                                 mv_d2_list=["TSD0001"],
                                 d4_list=["TSD0001"],
                                 dd_list=["TSD0001"],
                                 ms_list=["TSD0001"],
                                 fm_list=["TSD0001"],
                                 ts_list=["TSD0001"],
                                 sw_list=["TSD0001"],
                                 is_list=["TSD0001"],
                                 ar_list=["TSD0001"],
                                 unit_testing=True)

        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_load_assigned_devices_pass1(self):
        """ Get the date and time from the Substation. """
        sb = self.create_test_substation_object()

        sb.load_assigned_devices(unit_testing=True)





