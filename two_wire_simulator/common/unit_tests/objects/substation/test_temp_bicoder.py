import unittest
import mock
import serial
import status_parser

from common.objects.controllers.bl_32 import BaseStation3200
from common.imports import opcodes
from common.objects.bicoders.temp_bicoder import TempBicoder

__author__ = 'Eldin'


class TestTempBiCoderObject(unittest.TestCase):
    """
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the moisture sensor
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)

        # Add the ser object
        self.bl_3200.ser = self.mock_ser

        # Add faux io flag
        self.bl_3200.faux_io_enabled = True

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_temp_biCoder_object(self, _serial_number="SB00001"):
        """ Creates a new Master Valve object for testing purposes """
        bicoder = TempBicoder(_sn=_serial_number, _controller=self.bl_3200, _address=1)

        return bicoder

    #################################
    def test_set_temperature_reading_pass1(self):
        """ Set Temperature Reading Pass Case 1: Using Default vv value """
        bicoder = self.create_test_temp_biCoder_object()

        # Expected value is the _va value set at object Valve BiCoder object creation
        expected_value = bicoder.vd
        bicoder.set_temperature_reading()

        # vd value is set during this method and should equal the original value
        actual_value = bicoder.vd
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_temperature_reading_pass2(self):
        """ Set Water Usage Pass Case 2: Using 6 as passed in value for vv """
        bicoder = self.create_test_temp_biCoder_object()

        # Expected value is the vt value set at object Valve BiCoder object creation
        expected_value = 6
        bicoder.set_temperature_reading(_degrees=expected_value)

        # vt value is set during this method and should equal the original value
        actual_value = bicoder.vd
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_temperature_reading_pass3(self):
        """ Set Temperature Reading Pass Case 3: Command with correct values sent to controller """
        bicoder = self.create_test_temp_biCoder_object()

        vd_value = str(bicoder.vd)
        expected_command = "SET,{0}={1},VD={2}".format(bicoder.id, bicoder.sn, vd_value)
        bicoder.set_temperature_reading()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_temperature_reading_fail1(self):
        """ Set Temperature Reading Fail Case 1: Pass String value as argument """
        bicoder = self.create_test_temp_biCoder_object()

        new_vd_value = "b"
        with self.assertRaises(Exception) as context:
            bicoder.set_temperature_reading(new_vd_value)
        expected_message = "Failed trying to set {0} ({1})'s temperature reading. Invalid argument type, " \
                           "expected an int or float, received: {2}".format(
            bicoder.ty,         # {0}
            bicoder.sn,         # {1}
            type(new_vd_value)  # {2}
        )
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_temperature_reading_fail2(self):
        """ Set Temperature Reading Fail Case 2: Failed communication with substation """
        bicoder = self.create_test_temp_biCoder_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            bicoder.set_temperature_reading()
        e_msg = "Exception occurred trying to set {0} ({1})'s 'Temperature Reading' to: '{2}'".format(
            str(bicoder.ty), str(bicoder.sn), str(bicoder.vd))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_temperature_reading_pass1(self):
        """ Verify Temperature Reading Pass Case 1: Exception is not raised """
        bicoder = self.create_test_temp_biCoder_object()
        bicoder.vd = 5.0
        test_pass = False

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VD=5")
        bicoder.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                bicoder.verify_temperature_reading(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_temperature_reading_pass2(self):
        """ Verify Temperature Reading Pass Case 2: Verifies any temperature reading received from a real device is at
         least 1 degree or higher. """
        bicoder = self.create_test_temp_biCoder_object()
        bicoder.vd = 0.0
        valid_vd = 5.0

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VD={0}".format(valid_vd))
        bicoder.data = mock_data

        self.bl_3200.faux_io_enabled = False

        success = bicoder.verify_temperature_reading(_data=mock_data)
        self.assertTrue(success)

    #################################
    def test_verify_temperature_reading_fail1(self):
        """ Verify Temperature Reading Fail Case 1: Value on substation does not match what is stored in the object"""
        bicoder = self.create_test_temp_biCoder_object()
        bicoder.vd = 5.0
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VD=6")
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} ({1})'s temperature reading. Received: {2}, Expected: {3}".format(
            str(bicoder.ty),    # {0}
            str(bicoder.sn),    # {1}
            "6.0",              # {2} it is 6 because that is what we declared in our mock data
            str(bicoder.vd)     # {3}
        )
        try:
            bicoder.verify_temperature_reading(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_temperature_reading_fail2(self):
        """ Verify Temperature Reading Fail Case 2: Temperature reading received from real device exceeded allowable
        variance. """
        bicoder = self.create_test_temp_biCoder_object()
        bicoder.vd = 0.0
        invalid_vd = 0.0

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VD={0}".format(invalid_vd))
        bicoder.data = mock_data

        self.bl_3200.faux_io_enabled = False

        expected_message = "Unable to verify {0} ({1})'s (real device) temperature reading. Received: {2}, " \
                           "Expected: {3}".format(
                               bicoder.ty,                      # {0}
                               bicoder.sn,                      # {1}
                               invalid_vd,                      # {2}
                               "Value greater than 1 degrees."  # {3}
                           )

        with self.assertRaises(ValueError) as context:
            bicoder.verify_temperature_reading(_data=mock_data)

        e_msg = context.exception.message
        self.assertEqual(expected_message, e_msg)

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Verify Who I Am Pass Case 1: Exception is not raised """
        bicoder = self.create_test_temp_biCoder_object()

        # Set up mock data to return the default values of our biCoder
        mock_data = status_parser.KeyValues("SN=SB00001,VD=93.1,VT=1.7,SS=OK")

        # Mock the get data method so we don't attempt to send a packet to the controller
        bicoder.get_data = mock.MagicMock(return_value=mock_data)

        self.assertEqual(bicoder.verify_who_i_am(_expected_status=opcodes.okay), True)

    #################################
    def test_self_test_and_update_object_attributes_pass1(self):
        """ Does a self test on the device, then does a get date a resets the attributes of the device to match the
        controller pass case 1: Able to successfully set the attributes of the temperature sensor object """
        # Create a temperature sensor object
        bicoder = self.create_test_temp_biCoder_object()

        # Set expected temperature and two wire drop values
        temperature_value = 1.0
        two_wire_drop_value = 1.0

        # Mock temperature value
        temperature = mock.MagicMock(return_value=temperature_value)
        bicoder.data.get_value_string_by_key = temperature

        # Mock two wire drop value
        two_wire_drop = mock.MagicMock(return_value=two_wire_drop_value)
        bicoder.data.get_value_string_by_key = two_wire_drop

        # Go into method
        bicoder.self_test_and_update_object_attributes()

        # Compare expected and actual temperature and two wire drop values
        self.assertEqual(temperature_value, bicoder.vd)
        self.assertEqual(two_wire_drop_value, bicoder.vt)

    #################################
    def test_self_test_and_update_object_attributes_fail1(self):
        """ Does a self test on the device, then does a get date a resets the attributes of the device to match the
        controller pass case 1: Raises an exception when updating attributes of the temperature sensor"""
        # Create temperature sensor object
        bicoder = self.create_test_temp_biCoder_object()

        # Store expected error message

        e_msg = "Exception occurred trying to update attributes of the temperature sensor {0} object." \
                "Temperature Value: '{1}'," \
                "Two Wire Drop Value: '{2}'.\n\tException: {3}".format(
                    str(bicoder.sn),   # {0}
                    str(bicoder.vd),   # {1}
                    str(bicoder.vt),   # {2}
                    'Exception occurred trying to do a self test Temperature BiCoder SB00001: '  # {3}
                )

        # Mock temperature value
        temperature = mock.MagicMock(return_value=bicoder.vd)
        bicoder.data.get_value_string_by_key = temperature

        # Mock two wire drop value
        two_wire_drop = mock.MagicMock(return_value=bicoder.vt)
        bicoder.data.get_value_string_by_key = two_wire_drop

        # Raise an exception as a side effect

        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Run the method expecting an exception
        with self.assertRaises(Exception) as context:
            bicoder.self_test_and_update_object_attributes()

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    if __name__ == "__main__":
        unittest.main()
