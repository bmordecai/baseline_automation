import unittest
import mock
import serial
import status_parser

from common.imports import opcodes
from common.objects.bicoders.switch_bicoder import SwitchBicoder
from common.objects.controllers.bl_32 import BaseStation3200

__author__ = 'Eldin'


class TestSwitchBiCoderObject(unittest.TestCase):
    """
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the moisture sensor
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)

        # Add the ser object
        self.bl_3200.ser = self.mock_ser

        # Add faux io flag
        self.bl_3200.faux_io_enabled = True

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_switch_biCoder_object(self, _serial_number="TPD0001"):
        """ Creates a new Master Valve object for testing purposes """
        bicoder = SwitchBicoder(_sn=_serial_number, _controller=self.bl_3200, _address=1)

        return bicoder

    #################################
    def test_set_contact_state_pass1(self):
        """ Set Contact State Pass Case 1: Using Default vc value """
        bicoder = self.create_test_switch_biCoder_object()

        # Expected value is the _va value set at object Valve BiCoder object creation
        expected_value = bicoder.vc
        bicoder.set_contact_state()

        # vd value is set during this method and should equal the original value
        actual_value = bicoder.vc
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_contact_state_pass2(self):
        """ Set Contact State Pass Case 2: Using 'OP' as passed in value for vc """
        bicoder = self.create_test_switch_biCoder_object()

        # Expected value is the vt value set at object Valve BiCoder object creation
        expected_value = opcodes.open
        bicoder.set_contact_state(_contact_state=expected_value)

        # vt value is set during this method and should equal the original value
        actual_value = bicoder.vc
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_contact_state_pass3(self):
        """ Set Contact State Pass Case 3: Command with correct values sent to controller """
        bicoder = self.create_test_switch_biCoder_object()

        vd_value = str(bicoder.vc)
        expected_command = "SET,{0}={1},VC={2}".format(bicoder.id, bicoder.sn, vd_value)
        bicoder.set_contact_state()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_contact_state_fail1(self):
        """ Set Contact State Fail Case 1: Pass integer (which is invalid) value as argument """
        bicoder = self.create_test_switch_biCoder_object()

        new_vd_value = 123
        with self.assertRaises(Exception) as context:
            bicoder.set_contact_state(new_vd_value)
        expected_message = "Failed trying to set {0} ({1})'s contact state. Invalid argument type, " \
                           "expected a str, received: {2}".format(
                                bicoder.ty,         # {0}
                                bicoder.sn,         # {1}
                                type(new_vd_value)  # {2}
                            )
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_contact_state_fail2(self):
        """ Set Contact State Fail Case 2: Failed communication with substation """
        bicoder = self.create_test_switch_biCoder_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            bicoder.set_contact_state()
        e_msg = "Exception occurred trying to set {0} ({1})'s 'Contact State' to: '{2}'".format(
            str(bicoder.ty), str(bicoder.sn), str(bicoder.vc))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_contact_state_pass1(self):
        """ Verify Contact State Pass Case 1: Exception is not raised """
        bicoder = self.create_test_switch_biCoder_object()
        bicoder.vc = opcodes.open
        test_pass = False

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VC=OP")
        bicoder.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                bicoder.verify_contact_state(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_contact_state_pass2(self):
        """ Verify Contact State Pass Case 2: Verifies accepting any value for contact state when controller is using
         real devices. We only care about receiving a value which verifies we are communicating to the device. """
        bicoder = self.create_test_switch_biCoder_object()
        bicoder.vc = opcodes.open

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VC=OP")
        bicoder.data = mock_data

        self.bl_3200.faux_io_enabled = False

        success = bicoder.verify_contact_state(_data=mock_data)
        self.assertTrue(success)

    #################################
    def test_verify_contact_state_fail1(self):
        """ Verify Contact State Fail Case 1: Value on substation does not match what is stored in the object"""
        bicoder = self.create_test_switch_biCoder_object()
        bicoder.vc = opcodes.open
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VC=CL")
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} ({1})'s contact state reading. Received: {2}, Expected: {3}".format(
            str(bicoder.ty),    # {0}
            str(bicoder.sn),    # {1}
            opcodes.closed,     # {2} it is closed because that is what we declared in our mock data
            str(bicoder.vc)     # {3}
        )
        try:
            bicoder.verify_contact_state(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_contact_state_fail2(self):
        """ Verify Contact State Fail Case 2: Verifies exception when receiving no value for contact state when
         controller is using real devices. """
        bicoder = self.create_test_switch_biCoder_object()
        bicoder.vc = opcodes.open

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VC={0}".format(opcodes.contacts_closed))
        bicoder.data = mock_data

        self.bl_3200.faux_io_enabled = False

        expected_message = "Unable to verify {0} ({1})'s (real device) contact state reading. Received: {2}, " \
                           "Expected: {3}".format(
                               str(bicoder.ty),          # {0}
                               str(bicoder.sn),          # {1}
                               opcodes.contacts_closed,  # {2}
                               bicoder.vc                # {3}
                           )

        with self.assertRaises(ValueError) as context:
            bicoder.verify_contact_state(_data=mock_data)

        e_msg = context.exception.message
        self.assertEqual(expected_message, e_msg)

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Verify Who I Am Pass Case 1: Exception is not raised """
        bicoder = self.create_test_switch_biCoder_object()

        # Set up mock data to return the default values of our biCoder
        mock_data = status_parser.KeyValues("SN=TPD0001,VC=CL,VT=1.7,SS=OK")

        # Mock the get data method so we don't attempt to send a packet to the controller
        bicoder.get_data = mock.MagicMock(return_value=mock_data)

        self.assertEqual(bicoder.verify_who_i_am(_expected_status=opcodes.okay), True)

    #################################
    def test_self_test_and_update_object_attributes_pass1(self):
        """ Does a self test on the device, then does a get date a resets the attributes of the device to match the
        controller pass case 1: Able to successfully set the attributes of the event switch object """
        # Create an event switch object
        sw = self.create_test_switch_biCoder_object()

        # Mock do self test
        self_test = mock.MagicMock()
        sw.do_self_test = self_test

        # Mock get data
        data_get = mock.MagicMock()
        sw.get_data = data_get

        # Set expected contact state and two wire drop values
        contact_state_value = 'OK'
        two_wire_drop_value = 1.9

        # Mock return value for contact state and two wire drop as they both use the get_value_string_by_key method
        contact_and_drop_values = mock.MagicMock()
        sw.data.get_value_string_by_key = contact_and_drop_values
        contact_and_drop_values.side_effect = ['OK', 1.9]

        # Go into method
        sw.self_test_and_update_object_attributes()

        # Compare expected and actual contact_state and two wire drop values
        self.assertEqual(contact_state_value, sw.vc)
        self.assertEqual(two_wire_drop_value, sw.vt)

    #################################
    def test_self_test_and_update_object_attributes_fail1(self):
        """ Does a self test on the device, then does a get date a resets the attributes of the device to match the
        controller pass case 1: Raises an exception when updating attributes of the event switch"""
        # Create event switch sensor object
        sw = self.create_test_switch_biCoder_object()

        # Store expected error message
        e_msg = "Exception occurred trying to update attributes of the Switch Bicoder {0} object." \
                "Contact State: '{1}'," \
                "Two Wire Drop Value: '{2}'.\n\tException: {3}".format(
                    str(sw.sn),   # {0}
                    str(sw.vc),   # {1}
                    str(sw.vt),   # {2}
                    'Exception occurred trying to do a self test Switch BiCoder TPD0001: '  # {3}
                )

        # Mock event switch value
        event_switch = mock.MagicMock()
        sw.data.get_value_string_by_key = event_switch

        # Mock two wire drop value
        two_wire_drop = mock.MagicMock()
        sw.data.get_value_string_by_key = two_wire_drop

        # Raise an exception as a side effect

        self.mock_send_and_wait_for_reply.side_effect = Exception

        # Run the method expecting an exception
        with self.assertRaises(Exception) as context:
            sw.self_test_and_update_object_attributes()

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    if __name__ == "__main__":
        unittest.main()
