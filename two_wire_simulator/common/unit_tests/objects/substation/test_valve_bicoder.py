import unittest
import mock
import serial
import status_parser

from common.objects.controllers.bl_32 import BaseStation3200
from common.imports import opcodes
from common.objects.bicoders.valve_bicoder import ValveBicoder

__author__ = 'Eldin'


class TestValveBiCoderObject(unittest.TestCase):
    """
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the moisture sensor
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)

        # Add the ser object
        self.bl_3200.ser = self.mock_ser
        
        # Add faux io flag
        self.bl_3200.faux_io_enabled = True

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_valve_biCoder_object(self, _serial_number="TSD0001"):
        """ Creates a new Master Valve object for testing purposes """
        bicoder = ValveBicoder(_sn=_serial_number, _controller=self.bl_3200, _id=opcodes.master_valve, _address=1)

        return bicoder

    #################################
    def test_set_solenoid_current_pass1(self):
        """ Set Solenoid Current Pass Case 1: Using Default va value """
        bicoder = self.create_test_valve_biCoder_object()

        # Expected value is the _va value set at object Valve BiCoder object creation
        expected_value = bicoder.va
        bicoder.set_solenoid_current()

        # _va value is set during this method and should equal the original value
        actual_value = bicoder.va
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_solenoid_current_pass2(self):
        """ Set Solenoid Current Pass Case 2: Using 6 as passed in value for va """
        bicoder = self.create_test_valve_biCoder_object()

        # Expected value is the vt value set at object Valve BiCoder object creation
        expected_value = 6
        bicoder.set_solenoid_current(_solenoid_current=expected_value)

        # vt value is set during this method and should equal the original value
        actual_value = bicoder.va
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_solenoid_current_pass3(self):
        """ Set Solenoid Current Pass Case 3: Command with correct values sent to controller """
        bicoder = self.create_test_valve_biCoder_object()

        va_value = str(bicoder.va)
        expected_command = "SET,{0}={1},VA={2}".format(bicoder.id, bicoder.sn, va_value)
        bicoder.set_solenoid_current()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_solenoid_current_fail1(self):
        """ Set Solenoid Current Fail Case 1: Pass String value as argument """
        bicoder = self.create_test_valve_biCoder_object()

        new_va_value = "b"
        with self.assertRaises(Exception) as context:
            bicoder.set_solenoid_current(new_va_value)
        expected_message = "Failed trying to set Valve BiCoder {0}'s solenoid current. Invalid type passed in, " \
                           "expected int or float. Received type: {1}".format(
                                str(bicoder.sn),    # {0}
                                type(new_va_value)  # {1}
                            )
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_solenoid_current_fail2(self):
        """ Set Solenoid Current Fail Case 2: Failed communication with substation """
        bicoder = self.create_test_valve_biCoder_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            bicoder.set_solenoid_current()
        e_msg = "Exception occurred trying to set {0} {1}'s 'Solenoid Current' to: '{2}'".format(
            str(bicoder.ty), str(bicoder.sn), str(bicoder.va))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_solenoid_voltage_pass1(self):
        """ Set Solenoid Voltage Pass Case 1: Using Default vv value """
        bicoder = self.create_test_valve_biCoder_object()

        # Expected value is the _va value set at object Valve BiCoder object creation
        expected_value = bicoder.vv
        bicoder.set_solenoid_voltage()

        # _va value is set during this method and should equal the original value
        actual_value = bicoder.vv
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_solenoid_voltage_pass2(self):
        """ Set Solenoid Voltage Pass Case 2: Using 6 as passed in value for vv """
        bicoder = self.create_test_valve_biCoder_object()

        # Expected value is the vt value set at object Valve BiCoder object creation
        expected_value = 6
        bicoder.set_solenoid_voltage(_solenoid_voltage=expected_value)

        # vt value is set during this method and should equal the original value
        actual_value = bicoder.vv
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_solenoid_voltage_pass3(self):
        """ Set Solenoid Voltage Pass Case 3: Command with correct values sent to controller """
        bicoder = self.create_test_valve_biCoder_object()

        vv_value = str(bicoder.vv)
        expected_command = "SET,{0}={1},VV={2}".format(bicoder.id, bicoder.sn, vv_value)
        bicoder.set_solenoid_voltage()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_solenoid_voltage_fail1(self):
        """ Set Solenoid Voltage Fail Case 1: Pass String value as argument """
        bicoder = self.create_test_valve_biCoder_object()

        new_vv_value = "b"
        with self.assertRaises(Exception) as context:
            bicoder.set_solenoid_voltage(new_vv_value)
        expected_message = "Failed trying to set Valve BiCoder {0}'s solenoid voltage. Invalid type passed in, " \
                           "expected int or float. Received type: {1}".format(
                                str(bicoder.sn),    # {0}
                                type(new_vv_value)  # {1}
                            )
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_solenoid_voltage_fail2(self):
        """ Set Solenoid Voltage Fail Case 2: Failed communication with substation """
        bicoder = self.create_test_valve_biCoder_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            bicoder.set_solenoid_voltage()
        e_msg = "Exception occurred trying to set {0} ({1})'s 'Solenoid Voltage' to: '{2}'".format(
            str(bicoder.ty), str(bicoder.sn), str(bicoder.vv))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_solenoid_current_pass1(self):
        """ Verify Solenoid Current Pass Case 1: Exception is not raised """
        bicoder = self.create_test_valve_biCoder_object()
        bicoder.va = 5.0
        test_pass = False

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VA=5")
        bicoder.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                bicoder.verify_solenoid_current(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_solenoid_current_pass2(self):
        """ Verify Solenoid Current Pass Case 2: Tests allowing higher allowance boundaries when controller is using real
        devices."""
        bicoder = self.create_test_valve_biCoder_object()
        bicoder.va = 5.0

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VA=0.2")
        bicoder.data = mock_data

        self.bl_3200.faux_io_enabled = False

        verified = bicoder.verify_solenoid_current(_data=mock_data)
        self.assertTrue(verified)

    #################################
    def test_verify_solenoid_current_fail1(self):
        """ Verify Solenoid Current Fail Case 1: Value on substation does not match what is stored in the obejct """
        bicoder = self.create_test_valve_biCoder_object()
        bicoder.va = 5.0
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VA=6.0")
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} {1}'s solenoid current. Received: {2}, Expected: {3}".format(
            str(bicoder.ty),    # {0}
            str(bicoder.sn),    # {1}
            "6.0",              # {2} it is 6 because that is what we declared in our mock data
            str(bicoder.va)     # {3}
        )
        try:
            bicoder.verify_solenoid_current(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_solenoid_current_fail2(self):
        """ Verify Solenoid Current Fail Case 2: Value returned for a real device is greater than the allowance allowed
        for the use case. """
        bicoder = self.create_test_valve_biCoder_object()
        bicoder.va = 5.0
        invalid_va = 0.35

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VA={0}".format(invalid_va))
        bicoder.data = mock_data

        self.bl_3200.faux_io_enabled = False

        expected_message = "Unable to verify {0} {1}'s (real device) solenoid current. Received: {2}, " \
                           "Expected: {3}".format(
                               bicoder.ty,    # {0}
                               bicoder.sn,    # {1}
                               invalid_va,    # {2}
                               bicoder.va     # {3}
                           )

        with self.assertRaises(ValueError) as context:
            bicoder.verify_solenoid_current(_data=mock_data)

        e_msg = context.exception.message
        self.assertEqual(expected_message, e_msg)

    #################################
    def test_verify_solenoid_current_fail3(self):
        """ Verify Solenoid Current Fail Case 3: Value returned for a real device is less than the allowance allowed
        for the use case. """
        bicoder = self.create_test_valve_biCoder_object()
        bicoder.va = 5.0
        invalid_va = 0.07

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VA={0}".format(invalid_va))
        bicoder.data = mock_data

        self.bl_3200.faux_io_enabled = False

        expected_message = "Unable to verify {0} {1}'s (real device) solenoid current. Received: {2}, " \
                           "Expected: {3}".format(
                               bicoder.ty,    # {0}
                               bicoder.sn,    # {1}
                               invalid_va,    # {2}
                               bicoder.va     # {3}
                           )

        with self.assertRaises(ValueError) as context:
            bicoder.verify_solenoid_current(_data=mock_data)

        e_msg = context.exception.message
        self.assertEqual(expected_message, e_msg)

    #################################
    def test_verify_solenoid_voltage_pass1(self):
        """ Verify Solenoid Voltage Pass Case 1: Exception is not raised """
        bicoder = self.create_test_valve_biCoder_object()
        bicoder.vv = 5.0
        test_pass = False

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VV=5")
        bicoder.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                bicoder.verify_solenoid_voltage(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_solenoid_voltage_pass2(self):
        """ Verify Solenoid Voltage Pass Case 2: Tests allowing higher allowance boundaries when controller is using real
        devices."""
        bicoder = self.create_test_valve_biCoder_object()
        bicoder.vv = 5.0

        # Make a mock data that our method will pull from
        # Use 5.4 to test the > 0.4 boundary setting.
        mock_data = status_parser.KeyValues("SN=TSD0001,VV=27.5")
        bicoder.data = mock_data

        self.bl_3200.faux_io_enabled = False
        success = bicoder.verify_solenoid_voltage(_data=mock_data)

        self.assertTrue(success)

    #################################
    def test_verify_solenoid_voltage_fail1(self):
        """ Verify Solenoid Voltage Fail Case 1: Value on substation does not match what is stored in the object"""
        bicoder = self.create_test_valve_biCoder_object()
        bicoder.vv = 5.0
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VV=6.0")
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} {1}'s solenoid voltage. Received: {2}, Expected: {3}".format(
            str(bicoder.ty),    # {0}
            str(bicoder.sn),    # {1}
            "6.0",              # {2} it is 6 because that is what we declared in our mock data
            str(bicoder.vv)     # {3}
        )
        try:
            bicoder.verify_solenoid_voltage(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_solenoid_voltage_fail2(self):
        """ Verify Solenoid Voltage Fail Case 2: Value returned for a real device is greater than the allowance allowed
        for the use case."""
        bicoder = self.create_test_valve_biCoder_object()
        bicoder.vv = 5.0
        invalid_vv = 6.1

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,VV={0}".format(invalid_vv))
        bicoder.data = mock_data

        # Force controller to be using real devices
        self.bl_3200.faux_io_enabled = False

        expected_message = "Unable to verify {0} {1}'s (real device) solenoid voltage. Received: {2}, " \
                           "Expected: {3}".format(
                               bicoder.ty,    # {0}
                               bicoder.sn,    # {1}
                               invalid_vv,    # {2}
                               bicoder.vv     # {3}
                           )

        with self.assertRaises(ValueError) as context:
            bicoder.verify_solenoid_voltage(_data=mock_data)

        e_msg = context.exception.message
        self.assertEqual(expected_message, e_msg)

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Verify Who I Am Pass Case 1: Exception is not raised """
        bicoder = self.create_test_valve_biCoder_object()

        # Set up mock data to return the default values of our biCoder
        mock_data = status_parser.KeyValues("SN=TSD0001,VA=.23,VV=28.7,VT=1.7,SS=OK")

        # Mock the get data method so we don't attempt to send a packet to the controller
        bicoder.get_data = mock.MagicMock(return_value=mock_data)

        self.assertEqual(bicoder.verify_who_i_am(_expected_status=opcodes.okay), True)

    #################################
    def test_self_test_and_update_object_attributes_pass1(self):
        """ Verify Self Test and Update Object Attributes Pass Case 1: Get data and then assign the new value to the object's attribute
        Create test master valve object
        Create the variable required to test the method
        Mock: do_self_test because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: get_data because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: data and get_value_string_by_key so we can output the values we want
            Return: Two wire drop value
        Call the method
        """
        # Create the moisture sensor object
        bicoder = self.create_test_valve_biCoder_object()

        va_value = 2.0
        vv_value = 3.0
        vt_value = 3.0

        # Mock the do_self_test method
        mock_do_self_test = mock.MagicMock()
        bicoder.do_self_test = mock_do_self_test

        # Mock the get_data method
        mock_get_data = mock.MagicMock()
        bicoder.get_data = mock_get_data

        # Mock get_value_string_by_key and the data object that is in the moisture sensor object
        mock_data = mock.MagicMock(spec=status_parser.KeyValues)
        mock_data.get_value_string_by_key = mock.MagicMock(side_effect=[va_value, vv_value, vt_value])
        bicoder.data = mock_data

        # Call the method
        bicoder.self_test_and_update_object_attributes()

        # Verify the value was set
        self.assertEqual(va_value, bicoder.va)
        self.assertEqual(vv_value, bicoder.vv)
        self.assertEqual(vt_value, bicoder.vt)

    #################################
    def test_self_test_and_update_object_attributes_fail1(self):
        """ Verify Self Test and Update Object Attributes Fail Case 1: Catch Exception that is thrown
        Create test master valve object
        Create the variables required to test the method
        Mock: do_self_test because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: get_data because it goes outside of the method
            Return: Nothing we just want to pass over it
        Mock: data and get_value_string_by_key so we can output the values we want
            Return: Three values, one each for moisture_percent, temperature_value, and two_wire_drop
        Create the expected message
        Call the method
        Compare the expected error message with the actual error message
        """
        bicoder = self.create_test_valve_biCoder_object()

        # Mock the do_self_test method
        mock_do_self_test = mock.MagicMock()
        bicoder.do_self_test = mock_do_self_test

        # Mock the get_data method
        mock_get_data = mock.MagicMock()
        bicoder.get_data = mock_get_data

        # Mock get_value_string_by_key and the data object that is in the moisture sensor object
        mock_data = mock.MagicMock()
        mock_data.get_value_string_by_key = mock.MagicMock(side_effect=Exception)
        bicoder.data = mock_data

        # Create the expected message
        expected_msg = "Exception occurred trying to updating attributes of the Valve Bicoder {0} object." \
                       " Solenoid Current: '{1}', Solenoid Voltage '{2}'.\n\tException: " \
                       .format(
                           str(bicoder.sn),    # {0}
                           str(bicoder.va),    # {1}
                           str(bicoder.vv)     # {1}
                       )

        # Run method that will tested
        with self.assertRaises(Exception) as context:
            bicoder.self_test_and_update_object_attributes()

        # Compare the expected with the actual
        self.assertEqual(expected_msg, context.exception.message)

    if __name__ == "__main__":
        unittest.main()
