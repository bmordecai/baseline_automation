import unittest

import mock
import serial
import status_parser
from common.objects.controllers.bl_fs import FlowStation
from common.objects.controllers.bl_32 import BaseStation3200


class TestFlowStationObject(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.310569
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        self.uut = self.create_test_flowstation_object()

        # test_name = self.shortDescription()
        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """ Cleaning up after the test. """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_flowstation_object(self, _diffip="129.0.0.0", _diffmac="0008EE218C85", _diffsn="3K00001",
                                      _diffser=None, _diffvr="", _diffportaddress=None):
        """ Creates a new controller object for use in a unit test """
        if _diffser is not None:
            serial_port = _diffser
        else:
            serial_port = self.mock_ser

        flowstation = FlowStation(_mac=_diffmac, _serial_port=serial_port, _serial_number=_diffsn,
                                  _firmware_version=_diffvr, _port_address=_diffportaddress, _socket_port=None,
                                  _ip_address=_diffip)
        return flowstation

    #################################
    def create_test_basestation_3200_object(self, _diffip="129.0.0.0", _diffmac="0008EE218C85", _diffsn="3K00001",
                                      _diffser=None, _diffvr="", _diffportaddress=None, _diff_socket=None):
        """ Creates a new controller object for use in a unit test """
        if _diffser is not None:
            serial_port = _diffser
        else:
            serial_port = self.mock_ser
        controller = BaseStation3200(_mac=_diffmac,
                                     _serial_number=_diffser,
                                     _firmware_version=_diffvr,
                                     _serial_port=serial_port,
                                     _port_address=_diffportaddress,
                                     _socket_port=_diff_socket,
                                     _ip_address=_diffip)

        return controller


    # def test_set_default_values(self):
    #     self.fail()
    #
    # def test_set_address_for_objects(self):
    #     self.fail()
    #
    # def test_set_address_for_cn(self):
    #     self.fail()
    #
    # def test_assign_mainline_to_controller(self):
    #     self.fail()
    #
    # def test_add_water_source_from_controller(self):
    #     self.fail()
    #
    # def test_add_mainline_from_controller(self):
    #     self.fail()
    #
    # def test_add_point_of_control_from_controller(self):
    #     self.fail()
    #
    # def test_initialize_for_test(self):
    #     self.fail()
    #
    # def test_wait_for_reboot(self):
    #     self.fail()
    #
    # def test_do_reboot(self):
    #     self.fail()
    #
    # def test_verify_who_i_am(self):
    #     self.fail()
    #
    # def test_add_controller_to_flowstation(self):
    #     self.fail()
    #
    # def test_wait_for_flowstation_and_controllers_connection_status(self):
    #     self.fail()
    #
    # def test_set_connection_to_controller(self):
    #     self.fail()

    #################################
    def test_assign_3200_to_flowstation_happy_path(self):
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        slot_to_assign = 1
        mock_cn = mock.MagicMock()
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac

        self.uut.assign_base_station_to_flow_station_slot_number(
            _controller_object=mock_cn,
            _slot_number=slot_to_assign
        )
        expected = "DO,AC={0},MC={1},NU={2}".format(controller_sn, controller_mac, slot_to_assign)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected)

    #################################
    def test_add_3200_to_flowstation_happy_path(self):
        self.controller = self.create_test_basestation_3200_object()
        controller_address = 1
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        slot_to_assign = 1
        self.controller.sn = controller_sn
        self.controller.mac = controller_mac
        self.controller.ad = controller_address
        self.uut.controllers[controller_address] = self.controller
        self.uut.basestation_controllers[controller_address] = self.controller

        # Mock the other object's methods that we aren't testing
        self.uut.wait_for_flowstation_and_controllers_connection_status = mock.MagicMock()
        self.uut.assign_base_station_to_flow_station_slot_number = mock.MagicMock()
        self.controller.set_connection_to_flowstation = mock.MagicMock()

        self.uut.add_controller_to_flow_station(
            _controller_address=controller_address,
            _flow_station_slot_number=slot_to_assign
        )
        # expected = "DO,AC={0},MC={1},NU={2}".format(controller_sn, controller_mac, slot_to_assign)
        self.uut.wait_for_flowstation_and_controllers_connection_status.assert_called_with(
                controller_address=slot_to_assign,
                max_wait=10)
        self.uut.assign_base_station_to_flow_station_slot_number.assert_called_with(
            _controller_object=self.controller,
            _slot_number=slot_to_assign)

    #################################
    def test_assign_3200_to_flowstation_bad_slot(self):
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        slot_to_assign = 21
        mock_cn = mock.MagicMock()
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac

        with self.assertRaises(ValueError) as context:
            self.uut.assign_base_station_to_flow_station_slot_number(
                _controller_object=mock_cn,
                _slot_number=slot_to_assign
            )
        e_msg = "The address range for the controllers must be between 0- 20: Invalid address {0}.".format(
            slot_to_assign
        )
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_assign_3200_to_flowstation_send_exception(self):
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        slot_to_assign = 2
        mock_cn = mock.MagicMock()
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac

        # A contrived Exception is thrown when communicating with the mock serial port
        exception_msg = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.uut.assign_base_station_to_flow_station_slot_number(
                _controller_object=mock_cn,
                _slot_number=slot_to_assign
            )
        e_msg = ("Exception occurred trying to assign controller with serial number {0} and mac {1} to flowstation "
                 "address {2} -> {3}".format(controller_sn, controller_mac, slot_to_assign, exception_msg))
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_remove_controller_from_flowstation_pass(self):
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 5
        self.uut.controllers[controller_slot] = mock_cn

        self.uut.remove_controller_from_flowstation(_flowstation_slot_number=controller_slot)
        expected = "DO,AC={0},MC={1},NU=0".format(controller_sn, controller_mac)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected)

    #################################
    def test_remove_controller_from_flowstation_send_exception(self):
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 5
        self.uut.controllers[controller_slot] = mock_cn

        # A contrived Exception is thrown when communicating with the mock serial port
        exception_msg = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.uut.remove_controller_from_flowstation(_flowstation_slot_number=controller_slot)
        e_msg = ("Exception occurred trying to assign controller with serial number {0} and mac {1} to "
                 "flowstation address {2} -> {3}".format(controller_sn, controller_mac, 0, exception_msg))
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_add_controller_water_source_to_flowstation_happy_path(self):
        slot_to_assign = 1
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 5
        self.uut.controllers[controller_slot] = mock_cn

        # Create a mocked water source object and give the controller a reference to it
        mock_water_source = mock.MagicMock()
        mock_water_source.set_share_with_flow_station_to_false = mock.MagicMock()
        water_source_address = 8
        mock_cn.water_sources = {water_source_address: mock_water_source}
        self.uut.water_sources = {water_source_address: mock_water_source}

        self.uut.add_controller_water_source_to_flowstation(
            _controller_address=controller_slot,
            _controller_water_source_address=water_source_address,
            _flow_station_water_source_slot_number=slot_to_assign
        )
        expected = "DO,AW={0},MC={1},WS={2},NU={3}".format(controller_sn, mock_cn.mac, water_source_address, slot_to_assign)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected)

    #################################
    def test_add_controller_water_source_to_flowstation_bad_controller_slot(self):
        controller_sn = "3K00001"
        controller_slot = 1
        slot_to_assign = 1
        water_source_address = 9
        mock_cn = mock.MagicMock()
        mock_cn.sn = controller_sn

        with self.assertRaises(ValueError) as context:
            self.uut.add_controller_water_source_to_flowstation(
                _controller_address=controller_slot,
                _controller_water_source_address=water_source_address,
                _flow_station_water_source_slot_number=slot_to_assign
            )
        e_msg = ("Can not assign a water source to a controller with a controller address that is not in the range "
                 "1 through 8. Value passed in: {0}.".format(water_source_address))
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_add_controller_water_source_to_flowstation_bad_flowstation_slot(self):
        slot_to_assign = 21
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 5
        self.uut.controllers[controller_slot] = mock_cn

        # Create a mocked water source object and give the controller a reference to it
        mock_water_source = mock.MagicMock()
        mock_water_source.set_share_with_flow_station_to_false = mock.MagicMock()
        water_source_address = 1
        mock_cn.water_sources = {water_source_address: mock_water_source}
        self.uut.water_sources = {water_source_address: mock_water_source}

        with self.assertRaises(ValueError) as context:
            self.uut.add_controller_water_source_to_flowstation(
                _controller_address=controller_slot,
                _controller_water_source_address=water_source_address,
                _flow_station_water_source_slot_number=slot_to_assign
            )
        e_msg = ("Can not assign a water source to a FlowStation with a water source slot address that is not in "
                 "the range 0 through 20. Value passed in: {0}.".format(slot_to_assign))
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_add_controller_water_source_to_flowstation_send_exception(self):
        controller_sn = "3K00001"
        controller_slot = 5
        slot_to_assign = 1
        water_source_address = 1
        mock_cn = mock.MagicMock()
        mock_cn.sn = controller_sn
        self.uut.controllers[controller_slot] = mock_cn

        # A contrived Exception is thrown when communicating with the mock serial port
        exception_msg = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.uut.add_controller_water_source_to_flowstation(
                _controller_address=controller_slot,
                _controller_water_source_address=water_source_address,
                _flow_station_water_source_slot_number=slot_to_assign
            )
        e_msg = ("Exception occurred trying to assign water source {0} on controller {1} to FlowStation water source "
                 "{2} -> {3}".format(water_source_address, controller_sn, slot_to_assign, exception_msg))
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_remove_controller_water_source_from_flowstation_happy_path(self):
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 1
        self.uut.controllers[controller_slot] = mock_cn

        # Create a mocked water source object and give the controller a reference to it
        mock_water_source = mock.MagicMock()
        mock_water_source.set_share_with_flow_station_to_false = mock.MagicMock()
        water_source_address = 5
        mock_cn.water_sources = {water_source_address: mock_water_source}
        self.uut.water_sources = {water_source_address: mock_water_source}

        self.uut.remove_controller_water_source_from_flowstation(
            _controller_address=controller_slot,
            _controller_water_source_address=water_source_address,
            _flow_station_water_source_slot_number=water_source_address
        )
        expected = "DO,AW={0},MC={1},WS={2},NU=0".format(controller_sn, mock_cn.mac, water_source_address)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected)

    #################################
    def test_remove_controller_water_source_from_flowstation_bad_controller_slot(self):

        controller_slot = 22
        water_source_address = 5

        with self.assertRaises(KeyError) as context:
            self.uut.remove_controller_water_source_from_flowstation(
                _controller_address=controller_slot,
                _controller_water_source_address=water_source_address,
                _flow_station_water_source_slot_number=water_source_address
            )
        e_msg = "Controller '{0}' is not shared with FloStation '{1}'".format(controller_slot,      # {0}
                                                                              self.uut.mac)         # {1}
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_remove_controller_water_source_from_flowstation_bad_water_source_slot(self):
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_slot = 2
        self.uut.controllers[controller_slot] = mock_cn

        water_source_address = 11

        with self.assertRaises(KeyError) as context:
            self.uut.remove_controller_water_source_from_flowstation(
                _controller_address=controller_slot,
                _controller_water_source_address=water_source_address,
                _flow_station_water_source_slot_number=water_source_address
            )
        e_msg = ("Water Source '{0}' is not on FlowStation '{1}'"
                 .format(water_source_address,
                         self.uut.mac))
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_remove_controller_water_source_from_flowstation_send_exception(self):
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 1
        self.uut.controllers[controller_slot] = mock_cn

        # Create a mocked water source object and give the controller a reference to it
        mock_water_source = mock.MagicMock()
        self.uut.send_command_with_reply = mock.MagicMock()
        water_source_address = 5
        mock_cn.water_sources = {water_source_address: mock_water_source}
        self.uut.water_sources = {water_source_address: mock_water_source}

        # A contrived Exception is thrown when communicating with the mock serial port
        exception_msg = "Test Exception"
        self.uut.send_command_with_reply.side_effect = Exception(exception_msg)

        e_msg = ("Exception occurred trying to un-assign water source {0} on controller {1} to FlowStation water "
                 "source {2} -> {3}".format(water_source_address, controller_sn,
                                            water_source_address, exception_msg))

        with self.assertRaises(Exception) as context:
            self.uut.remove_controller_water_source_from_flowstation(
                _controller_address=controller_slot,
                _controller_water_source_address=water_source_address,
                _flow_station_water_source_slot_number=water_source_address
            )

        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_add_controller_point_of_control_to_flowstation_bad_flowstation_slot(self):
        slot_to_assign = 21
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 5
        self.uut.controllers[controller_slot] = mock_cn

        # Create a mocked point of control object and give the controller a reference to it
        mock_point_of_control = mock.MagicMock()
        mock_point_of_control.set_share_with_flow_station_to_false = mock.MagicMock()
        point_of_control_address = 1
        mock_cn.points_of_control = {point_of_control_address: mock_point_of_control}
        self.uut.points_of_control = {point_of_control_address: mock_point_of_control}

        with self.assertRaises(ValueError) as context:
            self.uut.add_controller_point_of_control_to_flowstation(
                _controller_address=controller_slot,
                _controller_point_of_control_address=point_of_control_address,
                _flow_station_point_of_control_slot_number=slot_to_assign
            )
        e_msg = ("The FlowStation Slot number must be between 0 through 20. Value passed in: {0}.".format(
            slot_to_assign))
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_add_controller_point_of_control_to_flowstation_send_exception(self):
        controller_sn = "3K00001"
        fs_controller_address = 5
        fs_poc_slot_number = 1
        controller_poc_address = 1
        mock_cn = mock.MagicMock()
        mock_cn.sn = controller_sn
        self.uut.controllers[fs_controller_address] = mock_cn

        # A contrived Exception is thrown when communicating with the mock serial port
        exception_msg = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.uut.add_controller_point_of_control_to_flowstation(
                _controller_address=fs_controller_address,
                _controller_point_of_control_address=controller_poc_address,
                _flow_station_point_of_control_slot_number=fs_poc_slot_number
            )
        e_msg = ("Exception occurred trying to assign Point of Control {0} on controller {1} to FlowStation Point "
                                     "of Control {2} -> {3}".format(controller_poc_address,
                                                                    controller_sn,
                                                                    fs_poc_slot_number,
                                                                    exception_msg))
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_remove_controller_point_of_control_from_flowstation_happy_path(self):
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 1
        self.uut.controllers[controller_slot] = mock_cn

        # Create a mocked point of control object and give the controller a reference to it
        mock_point_of_control = mock.MagicMock()
        mock_point_of_control.set_share_with_flow_station_to_false = mock.MagicMock()
        point_of_control_address = 5
        mock_cn.points_of_control = {point_of_control_address: mock_point_of_control}
        self.uut.points_of_control = {point_of_control_address: mock_point_of_control}

        self.uut.remove_controller_point_of_control_from_flowstation(
            _controller_address=controller_slot,
            _controller_point_of_control_address=point_of_control_address,
            _flowstation_point_of_control_slot_number=point_of_control_address
        )
        expected = "DO,AP={0},MC={1},PC={2},NU=0".format(controller_sn, mock_cn.mac, point_of_control_address)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected)

    #################################
    def test_remove_controller_point_of_control_from_flowstation_bad_controller_slot(self):

        controller_slot = 22
        point_of_control_address = 5

        with self.assertRaises(KeyError) as context:
            self.uut.remove_controller_point_of_control_from_flowstation(
                _controller_address=controller_slot,
                _controller_point_of_control_address=point_of_control_address,
                _flowstation_point_of_control_slot_number=point_of_control_address
            )
        e_msg = ("Controller '{0}' is not shared with FloStation '{1}'".format(controller_slot,      # {0}
                                                                               self.uut.mac))        # {1}
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_remove_controller_point_of_control_from_flowstation_bad_point_of_control_slot(self):
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        mock_cn.points_of_control = dict()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 2
        self.uut.controllers[controller_slot] = mock_cn
        point_of_control_address = 11

        with self.assertRaises(KeyError) as context:
            self.uut.remove_controller_point_of_control_from_flowstation(
                _controller_address=controller_slot,
                _controller_point_of_control_address=point_of_control_address,
                _flowstation_point_of_control_slot_number=point_of_control_address
            )
        e_msg = ("Point of Control '{0}' on controller '{1}' does not exist. Can't Un-assign from FlowStation '{2}'"
                 .format(point_of_control_address,
                         controller_mac,
                         self.uut.mac))
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_remove_controller_point_of_control_from_flowstation_send_exception(self):
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 1
        self.uut.controllers[controller_slot] = mock_cn

        # Create a mocked point of control object and give the controller a reference to it
        mock_point_of_control = mock.MagicMock()
        controller_point_of_control_address = 5
        flowstation_poc_slot_number = 4
        mock_cn.points_of_control = {controller_point_of_control_address: mock_point_of_control}
        self.uut.points_of_control = {flowstation_poc_slot_number: mock_point_of_control}

        self.uut.send_command_with_reply = mock.MagicMock()

        # A contrived Exception is thrown when communicating with the mock serial port
        exception_msg = "Test Exception"
        self.uut.send_command_with_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.uut.remove_controller_point_of_control_from_flowstation(
                _controller_address=controller_slot,
                _controller_point_of_control_address=controller_point_of_control_address,
                _flowstation_point_of_control_slot_number=flowstation_poc_slot_number
            )
        e_msg = ("Exception occurred trying to un-assign point of control {0} on controller {1} to FlowStation point "
                 "of control {2} -> {3}".format(controller_point_of_control_address, controller_sn,
                                                flowstation_poc_slot_number,  exception_msg))
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_add_controller_mainline_to_flowstation_bad_flowstation_slot(self):
        slot_to_assign = 21
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 5
        self.uut.controllers[controller_slot] = mock_cn

        # Create a mocked mainline object and give the controller a reference to it
        mock_mainline = mock.MagicMock()
        mock_mainline.set_share_with_flow_station_to_false = mock.MagicMock()
        mainline_address = 1
        mock_cn.mainlines = {mainline_address: mock_mainline}
        self.uut.mainlines = {mainline_address: mock_mainline}

        with self.assertRaises(ValueError) as context:
            self.uut.add_controller_mainline_to_flowstation(
                _controller_address=controller_slot,
                _controller_mainline_address=mainline_address,
                _flow_station_mainline_slot_number=slot_to_assign
            )
        e_msg = ("Can not assign a mainline to a FlowStation with a mainline slot address that is not in "
                 "the range 0 through 20. Value passed in: {0}.".format(slot_to_assign))
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_add_controller_mainline_to_flowstation_send_exception(self):
        controller_sn = "3K00001"
        controller_slot = 5
        slot_to_assign = 1
        mainline_address = 1
        mock_cn = mock.MagicMock()
        mock_cn.sn = controller_sn
        self.uut.controllers[controller_slot] = mock_cn

        # A contrived Exception is thrown when communicating with the mock serial port
        exception_msg = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.uut.add_controller_mainline_to_flowstation(
                _controller_address=controller_slot,
                _controller_mainline_address=mainline_address,
                _flow_station_mainline_slot_number=slot_to_assign
            )
        e_msg = ("Exception occurred trying to assign mainline {0} on controller {1} to FlowStation mainline "
                 "{2} -> {3}".format(mainline_address, controller_sn, slot_to_assign, exception_msg))
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_remove_controller_mainline_from_flowstation_happy_path(self):
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 1
        self.uut.controllers[controller_slot] = mock_cn

        # Create a mocked mainline object and give the controller a reference to it
        mock_mainline = mock.MagicMock()
        mock_mainline.set_share_with_flow_station_to_false = mock.MagicMock()
        mainline_address = 5
        mock_cn.mainlines = {mainline_address: mock_mainline}
        self.uut.mainlines = {mainline_address: mock_mainline}

        self.uut.remove_controller_mainline_from_flowstation(
            _controller_address=controller_slot,
            _controller_mainline_address=mainline_address,
            _flowstation_mainline_slot_number=mainline_address
        )
        expected = "DO,AM={0},MC={1},ML={2},NU=0".format(controller_sn,mock_cn.mac,mainline_address)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected)

    #################################
    def test_remove_controller_mainline_from_flowstation_bad_controller_slot(self):
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 22
        # self.uut.controllers[controller_slot] = mock_cn

        # Create a mocked mainline object and give the controller a reference to it
        mock_mainline = mock.MagicMock()
        mock_mainline.set_share_with_flow_station_to_false = mock.MagicMock()
        mainline_address = 5
        mock_cn.mainlines = {mainline_address: mock_mainline}
        self.uut.mainlines = {mainline_address: mock_mainline}

        with self.assertRaises(KeyError) as context:
            self.uut.remove_controller_mainline_from_flowstation(
                _controller_address=controller_slot,
                _controller_mainline_address=mainline_address,
                _flowstation_mainline_slot_number=mainline_address
            )
        e_msg = "Controller '{0}' is not shared with FlowStation '{1}'".format(controller_slot,         # {0}
                                                                               self.uut.mac)            # {1}
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_remove_controller_mainline_from_flowstation_bad_mainline_slot(self):
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        mock_cn.mainlines = dict()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 2
        self.uut.controllers[controller_slot] = mock_cn

        # Create a mocked mainline object and give the controller a reference to it
        mock_mainline = mock.MagicMock()
        mock_mainline.set_share_with_flow_station_to_false = mock.MagicMock()
        mainline_address = 11
        # mock_cn.mainlines = {mainline_address: mock_mainline}
        self.uut.mainlines = {mainline_address: mock_mainline}

        with self.assertRaises(KeyError) as context:
            self.uut.remove_controller_mainline_from_flowstation(
                _controller_address=controller_slot,
                _controller_mainline_address=mainline_address,
                _flowstation_mainline_slot_number=mainline_address
            )
        e_msg = ("Mainline '{0}' on controller '{1}' does not exist. Can't Un-assign from FlowStation '{2}'"
                 .format(mainline_address,
                         controller_mac,
                         self.uut.mac))
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_remove_controller_mainline_from_flowstation_send_exception(self):
        # Create a mocked controller object and give the flowstation a reference to it
        mock_cn = mock.MagicMock()
        controller_sn = "3K00001"
        controller_mac = "0008EE218C85"
        mock_cn.sn = controller_sn
        mock_cn.mac = controller_mac
        controller_slot = 1
        self.uut.controllers[controller_slot] = mock_cn

        # Create a mocked mainline object and give the controller a reference to it
        mock_mainline = mock.MagicMock()
        self.uut.send_command_with_reply = mock.MagicMock()
        mainline_address = 5
        mock_cn.mainlines = {mainline_address: mock_mainline}
        self.uut.mainlines = {mainline_address: mock_mainline}

        # A contrived Exception is thrown when communicating with the mock serial port
        exception_msg = "Test Exception"
        self.uut.send_command_with_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.uut.remove_controller_mainline_from_flowstation(
                _controller_address=controller_slot,
                _controller_mainline_address=mainline_address,
                _flowstation_mainline_slot_number=mainline_address
            )
        e_msg = ("Exception occurred trying to un-assign point of control {0} on controller {1} to FlowStation "
                 "mainline {2} -> {3}".format(mainline_address, mock_cn.sn,
                                              mainline_address, exception_msg))

        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_set_dynamic_flow_allocation_to_true_pass1(self):
        """ Set Dynamic Flow Allocation To True On Controller Pass Case 1: Controller successfully sets value"""
        controller = self.create_test_flowstation_object()
        new_dynamic_flow_value = "TR"
        controller.set_dynamic_flow_allocation_to_true()
        self.assertEqual(controller.fp, new_dynamic_flow_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend="SET,FS,FP=TR")

    #################################
    def test_set_dynamic_flow_allocation_to_true_fail1(self):
        """ Set Dynamic Flow Allocation To True On Controller Fail Case 1: Controller returns a bad status """
        controller = self.create_test_flowstation_object()

        new_dynamic_flow_value = "TR"
        error_message = "BC command returned from controller"
        self.mock_send_and_wait_for_reply.side_effect = Exception(error_message)
        with self.assertRaises(Exception) as context:
            controller.set_dynamic_flow_allocation_to_true()
        expected_message = "Exception occurred trying to set FlowStation {0}'s 'Dynamic Flow Allocation' to: '{1}'" \
                           " -> {2}".format(controller.mac, new_dynamic_flow_value, error_message)
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_dynamic_flow_allocation_to_false_pass1(self):
        """ Set Dynamic Flow Allocation To True On Controller Pass Case 1: Controller successfully sets value"""
        controller = self.create_test_flowstation_object()
        new_dynamic_flow_value = "FA"
        controller.set_dynamic_flow_allocation_to_false()
        self.assertEqual(controller.fp, new_dynamic_flow_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend="SET,FS,FP=FA")

    #################################
    def test_set_dynamic_flow_allocation_to_false_fail1(self):
        """ Set Dynamic Flow Allocation To False On Controller Fail Case 1: Controller returns a bad status """
        controller = self.create_test_flowstation_object()

        new_dynamic_flow_value = "FA"
        error_message = "BC command returned from controller"
        self.mock_send_and_wait_for_reply.side_effect = Exception(error_message)
        with self.assertRaises(Exception) as context:
            controller.set_dynamic_flow_allocation_to_false()
        expected_message = "Exception occurred trying to set FlowStation {0}'s 'Dynamic Flow Allocation' to: '{1}'" \
                           " -> {2}".format(controller.mac, new_dynamic_flow_value, error_message)
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_verify_dynamic_flow_allocation_pass1(self):
        """ Verify Dynamic Flow Allocation On Controller Pass Case 1: Controller successfully verifies value"""
        controller = self.create_test_flowstation_object()
        mock_data = status_parser.KeyValues("FP=FA")
        controller.data = mock_data
        controller.verify_dynamic_flow_allocation()

    #################################
    def test_verify_dynamic_flow_allocation_fail1(self):
        """ Verify Dynamic Flow Allocation On Controller Fail Case 1: Controller verifies value is incorrect"""
        controller = self.create_test_flowstation_object()
        controller.fp = "TR"    # Value in the test engine object
        returned_fp = "FA"      # Value returned by the controller
        mock_data = status_parser.KeyValues("FP={0}".format(returned_fp))
        controller.data = mock_data
        with self.assertRaises(ValueError) as context:
            controller.verify_dynamic_flow_allocation()
        expected_message = "Unable to verify FlowStation {0}'s 'Dynamic Flow Allocation' value. Received: {1}, " \
                           "Expected: {2}".format(controller.mac, returned_fp, controller.fp)
        self.assertEqual(expected_message, context.exception.message)

    # def test_verify_connection_to_controller(self):
    #     self.fail()
    #
    # def test_do_unassign_controller(self):
    #     self.fail()
    #
    # def test_do_assign_water_source(self):
    #     self.fail()
    #
    # def test_do_unassign_water_source(self):
    #     self.fail()
    #
    # def test_do_assign_poc(self):
    #     self.fail()
    #
    # def test_do_unassign_poc(self):
    #     self.fail()
    #
    # def test_do_assign_mainline(self):
    #     self.fail()
    #
    # def test_do_unassign_mainline(self):
    #     self.fail()
    
    def test_verify_full_configuration(self):
        pass

