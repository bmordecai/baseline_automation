# The following line is needed to prevent unwanted warning messages from Python.
from __future__ import absolute_import
import unittest
import mock
import serial
import status_parser

from common.objects.controllers.tw_sim import TWSimulator
from common.imports import opcodes
from common.objects.simulated_bicoders.analog_bicoder import SimulatedAnalogBicoder

from decimal import Decimal

__author__ = 'Kent'


class TestAnalogBiCoderObject(unittest.TestCase):
    """
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the moisture sensor
        self.tw_sim = mock.MagicMock(spec=TWSimulator)

        # Add the ser object
        self.tw_sim.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_analog_biCoder_object(self, _serial_number="TWF0001"):
        """ Creates a new Master Valve object for testing purposes """
        bicoder = SimulatedAnalogBicoder(_sn=_serial_number, _controller=self.tw_sim, _address=1)

        return bicoder

    #################################
    def test_set_analog_four_milli_reading_pass1(self):
        """ Set Analog Four Milli Reading Pass Case 1: Using Default vl value """
        bicoder = self.create_test_analog_biCoder_object()

        # Expected value is the vl value set at object Analog BiCoder object creation
        expected_value = bicoder.vl

        # set the analog four milli reading
        bicoder.write_analog_four_milli_reading()

        # vl value is set during this method and should equal the original value
        expected_command = "{0},{1}={2},{3}={4}".format(opcodes.set_action, bicoder.id, bicoder.sn,
                                                        opcodes.low_milli_amp_setting, expected_value)

        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_analog_four_milli_reading_pass2(self):
        """ Set Analog Four Milli Reading Pass Case 2: Using 6 as passed in value for vl """
        bicoder = self.create_test_analog_biCoder_object()

        # Expected value is the vl value set at object Analog BiCoder object creation
        new_four_milli_reading = 6
        bicoder.write_analog_four_milli_reading(_four_milli_amps=new_four_milli_reading)

        # Create expected command string
        expected_command = "{0},{1}={2},{3}={4}".format(opcodes.set_action, bicoder.id, bicoder.sn,
                                                        opcodes.low_milli_amp_setting, new_four_milli_reading)

        # Verify command string sent to the controller
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_analog_four_milli_reading_pass3(self):
        """ Set Analog Four Milli Reading Pass Case 2: Using a float (6.0) as passed in value for vr """
        bicoder = self.create_test_analog_biCoder_object()

        # Expected value is the vr value set at object Valve BiCoder object creation
        new_analog_four_milli_reading = 6.0
        bicoder.write_analog_four_milli_reading(_four_milli_amps=new_analog_four_milli_reading)

        # Expected value should be the value used as a parameter in set_analog_four_milli_reading()
        expected_value = new_analog_four_milli_reading

        # Create the expected command string
        expected_command = "{0},{1}={2},{3}={4}".format(opcodes.set_action,
                                                        bicoder.id,
                                                        bicoder.sn,
                                                        opcodes.low_milli_amp_setting,
                                                        expected_value)
        # Verify command string sent to the controller
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_analog_four_milli_reading_fail1(self):
        """ Set Analog Four Milli Reading Case 1: Pass String value as argument """
        bicoder = self.create_test_analog_biCoder_object()

        new_va_value = "b"
        with self.assertRaises(Exception) as context:
            bicoder.write_analog_four_milli_reading(new_va_value)
        # Create the expected message string

        expected_message = "Failed trying to set {0} ({1})'s analog percent. Invalid argument type, expected an int " \
                           "or float, received: {2}".format(bicoder.ty,
                                                            bicoder.sn,
                                                            type(new_va_value))

        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_analog_four_milli_reading_fail2(self):
        """ Set Analog Four Milli Reading Fail Case 2: Failed communication with substation """
        bicoder = self.create_test_analog_biCoder_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        new_flow_rate = 5

        with self.assertRaises(Exception) as context:
            bicoder.write_analog_four_milli_reading(_four_milli_amps=new_flow_rate)

        e_msg = "Exception occurred trying to set {0} ({1})'s '_four_milli_amp reading' to: '{2}'".format(
                                        bicoder.ty,         # {0}
                                        str(bicoder.sn),    # {1}
                                        str(bicoder.vl))    # {2}

        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_analog_twenty_milli_reading_pass1(self):
        """ Set Analog Twenty Milli Reading Pass Case 1: Using Default vh value """
        bicoder = self.create_test_analog_biCoder_object()

        # Expected value is the _va value set at object Valve BiCoder object creation
        expected_value = bicoder.vh

        # The correct command should be built and passed into the send and wait for reply method
        expected_command = "{0},{1}={2},{3}={4}".format(opcodes.set_action,                 # {0}
                                                        bicoder.id,                         # {1}
                                                        bicoder.sn,                         # {2}
                                                        opcodes.high_milli_amp_setting,     # {3}
                                                        expected_value)                     # {4}

        # Set the value
        bicoder.write_analog_twenty_milli_reading()

        # Verify the command
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_analog_twenty_milli_reading_pass2(self):
        """ Set Analog Twenty Milli Reading Pass Case 2: Using 6 as passed in value for vh """
        bicoder = self.create_test_analog_biCoder_object()

        # Expected value is the vt value set at object Valve BiCoder object creation
        expected_value = 6
        bicoder.write_analog_twenty_milli_reading(_twenty_milli_amps=expected_value)

        # The correct command should be built and passed into the send and wait for reply method
        expected_command = "{0},{1}={2},{3}={4}".format(opcodes.set_action,                 # {0}
                                                        bicoder.id,                         # {1}
                                                        bicoder.sn,                         # {2}
                                                        opcodes.high_milli_amp_setting,     # {3}
                                                        expected_value)                     # {4}

        # Verify the command
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_analog_twenty_milli_reading_pass3(self):
        """ Set Analog Twenty Milli Reading Pass Case 2: Using a float (6.0) as passed in value for vh """
        bicoder = self.create_test_analog_biCoder_object()

        # Expected value is the vt value set at object Valve BiCoder object creation
        expected_value = 6.0
        bicoder.write_analog_twenty_milli_reading(_twenty_milli_amps=expected_value)

        # The correct command should be built and passed into the send and wait for reply method
        expected_command = "{0},{1}={2},{3}={4}".format(opcodes.set_action,                 # {0}
                                                        bicoder.id,                         # {1}
                                                        bicoder.sn,                         # {2}
                                                        opcodes.high_milli_amp_setting,     # {3}
                                                        expected_value)                     # {4}

        # Verify the command
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_analog_twenty_milli_reading_fail1(self):
        """ Set Analog Twenty Milli Reading Case 1: Pass String value as argument """
        bicoder = self.create_test_analog_biCoder_object()

        new_vg_value = "b"
        with self.assertRaises(Exception) as context:
            bicoder.write_analog_twenty_milli_reading(new_vg_value)

        # Create the expected message
        expected_message = "Failed trying to set {0} ({1})'s 'twenty_milli_amp reading'. " \
                                   "Invalid argument type, expected an int " \
                                   "or float, received: {2}".format(
                                    bicoder.ty,
                                    bicoder.sn,
                                    type(new_vg_value))

        # Verify the test
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_analog_twenty_milli_reading_fail2(self):
        """ Set Analog Twenty Milli Reading Fail Case 2: Failed communication with substation """
        bicoder = self.create_test_analog_biCoder_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            bicoder.write_analog_twenty_milli_reading()
        e_msg = "Exception occurred trying to set {0} ({1})'s 'twenty_milli_amp reading' to: '{2}'".format(
                                    str(bicoder.ty),    # {0}
                                    str(bicoder.sn),    # {1}
                                    str(bicoder.vh))    # {2}

        # Verify the test
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_analog_four_milli_reading_pass1(self):
        """ Verify Analog Four Milli Reading Pass Case 1: Exception is not raised """
        bicoder = self.create_test_analog_biCoder_object()
        analog_four_milli_reading = 5.27
        bicoder.vl = analog_four_milli_reading

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.low_milli_amp_setting,      # {0}
                                                                        analog_four_milli_reading))         # {1}
        bicoder.data = mock_data

        test_pass = bicoder.verify_analog_four_milli_reading(_data=mock_data)

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_analog_four_milli_reading_pass2(self):
        """ Verify Analog Four Milli Reading Pass Case 2: Test upper range of allowable range """
        bicoder = self.create_test_analog_biCoder_object()
        analog_four_milli_reading = 5.06
        analog_four_milli_reading_return_value = 5.08

        bicoder.vl = analog_four_milli_reading

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.low_milli_amp_setting,             # {0}
                                                                        analog_four_milli_reading_return_value))   # {1}
        bicoder.data = mock_data

        test_pass = bicoder.verify_analog_four_milli_reading(_data=mock_data)

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_analog_four_milli_reading_pass3(self):
        """ Verify Analog Four Milli Reading Pass Case 3: Test lower range of allowable range """
        bicoder = self.create_test_analog_biCoder_object()
        analog_four_milli_reading = 6.09
        analog_four_milli_reading_return_value = 6.07

        bicoder.vl = analog_four_milli_reading

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.low_milli_amp_setting,             # {0}
                                                                        analog_four_milli_reading_return_value))   # {1}
        bicoder.data = mock_data

        test_pass = bicoder.verify_analog_four_milli_reading(_data=mock_data)

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_analog_four_milli_reading_fail1(self):
        """ Verify Analog Four Milli Reading fail Case 1: Value on substation does not match what is stored in
        the object """
        bicoder = self.create_test_analog_biCoder_object()
        incorrect_four_milli_reading = 5.6
        bicoder.vl = 6.9
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.low_milli_amp_setting,
                                                                        incorrect_four_milli_reading))
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} ({1})'s analog four milli_amp setting. Received: {2}, Expected: {3}".\
                            format(
                            bicoder.ty,                         # {0}
                            str(bicoder.sn),                    # {1}
                            str(incorrect_four_milli_reading),  # {2}
                            str(bicoder.vl)                     # {3}
        )
        try:
            bicoder.verify_analog_four_milli_reading(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_analog_four_milli_reading_fail2(self):
        """Verify value on substation does not match what is stored in the object, difference right on boundary."""
        bicoder = self.create_test_analog_biCoder_object()
        incorrect_four_milli_reading = 10.0
        bicoder.vl = 10.21
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.low_milli_amp_setting,
                                                                        incorrect_four_milli_reading))
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} ({1})'s analog four milli_amp setting. Received: {2}, Expected: {3}"\
            .format(
            str(bicoder.ty),                # {0}
            str(bicoder.sn),                # {1}
            incorrect_four_milli_reading,   # {2}
            str(bicoder.vl)                 # {3}
        )
        try:
            bicoder.verify_analog_four_milli_reading(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_analog_twenty_milli_reading_pass1(self):
        """ Verify Analog Twenty Milli Reading Pass Case 1: Exception is not raised """
        bicoder = self.create_test_analog_biCoder_object()
        analog_twenty_milli_reading = 5.27
        bicoder.vh = analog_twenty_milli_reading

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.high_milli_amp_setting,     # {0}
                                                                        analog_twenty_milli_reading))       # {1}
        bicoder.data = mock_data

        test_pass = bicoder.verify_analog_twenty_milli_reading(_data=mock_data)

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_analog_twenty_milli_reading_pass2(self):
        """ Verify Analog Twenty Milli Reading Pass Case 2: Test upper limit of allowable range """
        bicoder = self.create_test_analog_biCoder_object()
        analog_twenty_milli_reading = 5.06
        analog_twenty_milli_reading_return_value = 5.08

        bicoder.vh = analog_twenty_milli_reading

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.high_milli_amp_setting,            # {0}
                                                                        analog_twenty_milli_reading_return_value)) # {1}
        bicoder.data = mock_data

        test_pass = bicoder.verify_analog_twenty_milli_reading(_data=mock_data)

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_analog_twenty_milli_reading_pass3(self):
        """ Verify Analog Twenty Milli Reading Pass Case 3: Test lower limit of allowable range """
        bicoder = self.create_test_analog_biCoder_object()
        analog_twenty_milli_reading = 6.09
        analog_twenty_milli_reading_return_value = 6.07

        bicoder.vh = analog_twenty_milli_reading

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.high_milli_amp_setting,            # {0}
                                                                        analog_twenty_milli_reading_return_value)) # {1}
        bicoder.data = mock_data

        test_pass = bicoder.verify_analog_twenty_milli_reading(_data=mock_data)

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_analog_twenty_milli_reading_fail1(self):
        """ Verify Analog Twenty Milli Reading fail Case 1: Value on substation does not match what is stored in
        the object """
        bicoder = self.create_test_analog_biCoder_object()
        incorrect_twenty_milli_reading = 5.6
        bicoder.vh = 6.9
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.high_milli_amp_setting,
                                                                        incorrect_twenty_milli_reading))
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} ({1})'s analog twenty milli_amp setting. Received: {2}, Expected: {3}". \
            format(
                bicoder.ty,                             # {0}
                str(bicoder.sn),                        # {1}
                str(incorrect_twenty_milli_reading),    # {2}
                str(bicoder.vh)                         # {3}
            )
        try:
            bicoder.verify_analog_twenty_milli_reading(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_analog_twenty_milli_reading_fail2(self):
        """Verify value on substation does not match what is stored in the object, difference right on boundary."""
        bicoder = self.create_test_analog_biCoder_object()
        incorrect_twenty_milli_reading = 10.0
        bicoder.vl = 10.21
        test_pass = False
        e_msg = ""

        # Make a mock data that our method will pull from
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.high_milli_amp_setting,
                                                                        incorrect_twenty_milli_reading))
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} ({1})'s analog twenty milli_amp setting. Received: {2}, Expected: {3}" \
            .format(
                str(bicoder.ty),                    # {0}
                str(bicoder.sn),                    # {1}
                incorrect_twenty_milli_reading,     # {2}
                str(bicoder.vh)                     # {3}
            )
        try:
            bicoder.verify_analog_twenty_milli_reading(_data=mock_data)

        # Catches an Assertion Error, meaning the method did NOT raise an exception meaning verification passed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_who_i_am_pass1(self):
        """ Verify Who I Am Pass Case 1: Exception is not raised """
        bicoder = self.create_test_analog_biCoder_object()
        bicoder.sn = "testSN"
        bicoder.vl = 10.2
        bicoder.vh = 12.5
        bicoder.vt = 15.5
        bicoder.ss = opcodes.okay
        bicoder.controller.faux_io_enabled = True

        # Set up mock data to return the default values of our biCoder
        mock_data = status_parser.KeyValues("{0}={1},{2}={3},{4}={5},{6}={7},{8}={9}".format(
            opcodes.serial_number,
            bicoder.sn,
            opcodes.low_milli_amp_setting,
            bicoder.vl,
            opcodes.high_milli_amp_setting,
            bicoder.vh,
            opcodes.two_wire_drop,
            bicoder.vt,
            opcodes.status_code,
            bicoder.ss
        ))

        # Mock the get data method so we don't attempt to send a packet to the controller
        bicoder.read_data = mock.MagicMock(return_value=mock_data)

        self.assertEqual(bicoder.verify_who_i_am(_expected_status=opcodes.okay), True)

    #################################
    def test_self_test_and_update_object_attributes_pass1(self):
        """
        self_test_and_update_object_attributes pass case 1:
        Create test flow meter object
        Store: Expected values that can be compared against actual values to make sure they can be reset
        Mock: do_self_test because it goes outside of the method
        Return: None so it does not fail
        Mock: get_data because it goes outside of the method
        Return: None, so it does not fail
        Mock: data.get_value_string_by_key because it goes outside of the method
        Return: Three integers, so they can be converted into floats
        Run: Test, with nothing passed into it
        Compare: expected values, such as self.ad or self.vr, to actual values stored in test
        """
        # Create test flow meter object
        bicoder = self.create_test_analog_biCoder_object()

        # Store expected values in test
        first_value = 3.0
        second_value = 4.0
        third_value = 5.0
        fourth_value = 6.0
        fifth_value = 7.0

        # Mock do_self_test
        mock_do_self_test = mock.MagicMock(side_effect=None)
        bicoder.do_self_test = mock_do_self_test

        # Mock get_data
        mock_get_data = mock.MagicMock(side_effect=None)
        bicoder.read_data = mock_get_data

        # Mock data.get_value_string_by_key, return three integers
        mock_get_value_string_by_key = mock.MagicMock(side_effect=[first_value, second_value, third_value, fourth_value,
                                                                   fifth_value])
        bicoder.data.get_value_string_by_key = mock_get_value_string_by_key

        # Run the test
        bicoder.read_and_update_object_attributes()

        # Compare expected values to actual values. Use assertIs instead of assertEquals in order to compare the type
        # (float to float) as well as the value that assertEqual would normally compare
        self.assertIs(first_value, bicoder.vl)
        self.assertIs(second_value, bicoder.vh)
        self.assertEqual(third_value, bicoder.vr)   # We use assert equals here because we need a Decimal to do ".00"
        self.assertIs(fourth_value, bicoder.va)

    ################################
    def test_self_test_and_update_object_attributes_fail1(self):
        """
        self_test_and_update_object_attributes fail case 1:
        Create: test flow meter object
        Mock: do_self_test because it goes outside of the method
        Return: None so it does not fail at this point in the method
        Mock: get_data because it goes outside of the method
        Return: None, so it does not fail at this point in the method
        Mock: All three get_value_string_by_key methods
        Return: Strings for all of them, this way they cannot be type casted into floats, and will raise an exception
        Run: The method, and catch an exception, which will let us access the error message
        """
        # Create test flow meter object
        bicoder = self.create_test_analog_biCoder_object()

        # Store incorrect expected value in test, because the test has the get_value_string_by_key as strings,
        # the test cannot run with them.
        incorrect_value = 0.0

        e_msg = "Exception occurred trying to update attributes of the pressure sensor {0} object." \
                " Units for 4 mA reading: '{1}'," \
                " Units for 20 mA reading: '{2}'," \
                " Reading converted to Units: '{3}'," \
                " Raw value of mA reading: '{4}'".format(
                    str(bicoder.sn),  # {0}
                    str(bicoder.vl),  # {1}
                    str(bicoder.vh),  # {2}
                    str(bicoder.vr),  # {3}
                    str(bicoder.va),  # {4}
                )

        # Mock do_self_test
        mock_do_self_test = mock.MagicMock(side_effect=None)
        bicoder.do_self_test = mock_do_self_test

        # Mock get_data
        mock_get_data = mock.MagicMock(side_effect=None)
        bicoder.read_data = mock_get_data

        # Mock self.data.get_value_string_by_key
        mock_get_value_string_by_key = mock.MagicMock(side_effect=['this', 'should', 'fail'])
        bicoder.data.get_value_string_by_key = mock_get_value_string_by_key

        # Run the method, and catch an exception
        with self.assertRaises(Exception) as context:
            bicoder.read_and_update_object_attributes()

        # Compare actual error message to expected error message in order to test that the correct message was reached
        self.assertEqual(e_msg, context.exception.message)

    if __name__ == "__main__":
        unittest.main()
