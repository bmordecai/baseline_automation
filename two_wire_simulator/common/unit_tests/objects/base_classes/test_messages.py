from __future__ import absolute_import
__author__ = 'Eldinner Plate'

import unittest
import mock
import serial
import datetime

from common.imports import opcodes
from common.imports.types import Message, MessageCategory, status_plus_priority_code_dict_3200, status_code_dict_1000

from common.objects.base_classes import messages
from common.objects.base_classes.devices import BaseDevices
import unittest

import mock
import serial
import status_parser
from common.objects.base_classes.programs import BasePrograms
from common.objects.base_classes import messages
from common.imports import opcodes
from common.objects.controllers.bl_32 import BaseStation3200

from common.objects.controllers.bl_10 import BaseStation1000
from common.objects.controllers.bl_fs import FlowStation
from common.objects.controllers.bl_sb import Substation

from common.date_package.date_resource import date_mngr



class TestMessages(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.309759

    Master Valve Starting lat & lng:
    latitude = 43.609768
    longitude = -116.310359
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        BaseDevices.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        BaseDevices.controller_lat = float(43.609768)
        BaseDevices.controller_long = float(-116.309759)

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def test_message_to_set_pass_1(self):
        """ Test Message To Set Pass Case 1: Successfully returned controller message
        Create: Test devices object
        Mock: start_stop_pause so we can use it as a helper object
        Return: None, so it does not fail
        Store: Expected value
        Mock: create_controller message so it does not go into the method, use patch to temporarily mock it
            Return: The expected value for the test to pass
        Compare: The expected value and the actual value returned from the test. If they are equal, the test passes """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        # device = self.create_test_device_object(dev_type=opcodes.controller)
        controller = mock.MagicMock()
        controller.sn = "3K10001"
        controller.controller_object = "32"

        # Set the expected value
        expected_value = "SET,FOO,BAR"

        # Mock start_stop_pause
        start_stop_pause = mock.MagicMock(side_effect=None)

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_controller_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_set(_object=controller, _status_code="BL", _ct_type=opcodes.controller,
                                               _helper_object=start_stop_pause)

        self.assertEqual(expected_value, actual_value)
        #################################

    def test_message_to_set_pass_1_1(self):
        """
        ADD to this test a firmware version of the device and it still passed
        Test Message To Set Pass Case 1: Successfully returned controller message
        Create: Test devices object
        Mock: start_stop_pause so we can use it as a helper object
        Return: None, so it does not fail
        Store: Expected value
        Mock: create_controller message so it does not go into the method, use patch to temporarily mock it
            Return: The expected value for the test to pass
        Compare: The expected value and the actual value returned from the test. If they are equal, the test passes """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        controller = mock.MagicMock()
        controller.sn = "3K10001"
        controller.controller_object = "32"

        # Set the expected value
        expected_value = "SET,FOO,BAR"

        # Mock start_stop_pause
        start_stop_pause = mock.MagicMock(side_effect=None)

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_controller_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_set(_object=controller, _status_code="BL", _ct_type=opcodes.controller,
                                               _helper_object=start_stop_pause)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_set_pass_2(self):
        """ Test Message To Set Pass Case 2: Successfully returned zone message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        zone = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_zone_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_set(_object=zone, _status_code="BS", _ct_type=opcodes.zone)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_set_pass_3(self):
        """ Test Message To Set Pass Case 3: Successfully returned event switch message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        sw = mock.MagicMock()
        sw.ad = 1

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_event_switch_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_set(_object=sw, _status_code="BS", _ct_type=opcodes.event_switch)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_set_pass_4(self):
        """ Test Message To Set Pass Case 4: Successfully returned flow meter message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        fm = mock.MagicMock()
        fm.ad = 1

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_flow_meter_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_set(_object=fm, _status_code="BS", _ct_type=opcodes.flow_meter)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_set_pass_5(self):
        """ Test Message To Set Pass Case 5: Successfully returned mainline message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        ml = mock.MagicMock()
        ml.ad = 1

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_mainline_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_set(_object=ml, _status_code="FD", _ct_type=opcodes.mainline)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_set_pass_6(self):
        """ Test Message To Set Pass Case 6: Successfully returned moisture sensor message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        # device = self.create_test_device_object(dev_type=opcodes.moisture_sensor)
        ms = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_moisture_sensor_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_set(_object=ms, _status_code="BS", _ct_type=opcodes.moisture_sensor)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_set_pass_7(self):
        """ Test Message To Set Pass Case 7: Successfully returned master valve message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        mv = mock.MagicMock()
        mv.sn = 'TMV0001'

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_master_valve_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_set(_object=mv, _status_code="BS", _ct_type=opcodes.master_valve)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_set_pass_8(self):
        """ Test Message To Set Pass Case 8: Successfully returned temperature sensor message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        obj = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_temp_sensor_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_set(_object=obj, _status_code="BS", _ct_type=opcodes.temperature_sensor)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_set_pass_9(self):
        """ Test Message To Set Pass Case 9: Successfully returned program message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        obj = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_program_3200_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_set(_object=obj, _status_code="BS", _ct_type=opcodes.program)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_set_pass_10(self):
        """ Test Message To Set Pass Case 10: Successfully returned point of connection message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        obj = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_poc_3200_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_set(_object=obj, _status_code="EV", _ct_type=opcodes.point_of_connection)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_set_pass_11(self):
        """ Test Message To Set Pass Case 11: Successfully returned zone program message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        obj = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_zone_program_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_set(_object=obj, _status_code="BD", _ct_type=opcodes.zone_program)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_set_fail_1(self):
        """ Test Message To Set Fail Case 1: Status code for 3200 is invalid """
        # Create a mocked object and make sure that it has a 'controller type' attribute so it can be checked against
        # the library
        obj = mock.MagicMock()
        obj.controller_type = "32"

        expected_msg = "Status Code 'ZZ' is not valid for object CN."

        with self.assertRaises(ValueError) as context:
            messages.message_to_set(_object=obj, _status_code="ZZ", _ct_type=opcodes.controller)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_message_to_set_fail_2(self):
        """ Test Message To Set Fail Case 2: Status code for 1000 is invalid """
        # Create a mocked object and make sure that it has a 'controller type' attribute so it can be checked against
        # the library
        obj = mock.MagicMock()
        obj.controller_type = "10"

        expected_msg = "Status Code 'ZZ' is not valid for object CN."

        with self.assertRaises(ValueError) as context:
            messages.message_to_set(_object=obj, _status_code="ZZ", _ct_type=opcodes.controller)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_message_to_get_pass_1(self):
        """ Test Message To Get Pass Case 1: Successfully returned controller message
        """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        cn = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_controller_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_get(_object=cn, _status_code="BL", _ct_type=opcodes.controller)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_get_pass_2(self):
        """ Test Message To Get Pass Case 2: Successfully returned zone message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        zone = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_zone_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_get(_object=zone, _status_code="BS", _ct_type=opcodes.zone)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_get_pass_3(self):
        """ Test Message To Get Pass Case 3: Successfully returned event switch message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        sw = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_event_switch_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_get(_object=sw, _status_code="BS", _ct_type=opcodes.event_switch)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_get_pass_4(self):
        """ Test Message To Get Pass Case 4: Successfully returned flow meter message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        fm = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_flow_meter_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_get(_object=fm, _status_code="BS", _ct_type=opcodes.flow_meter)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_get_pass_5(self):
        """ Test Message To Get Pass Case 5: Successfully returned mainline message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        ml = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_mainline_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_get(_object=ml, _status_code="FD", _ct_type=opcodes.mainline)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_get_pass_6(self):
        """ Test Message To Get Pass Case 6: Successfully returned moisture sensor message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        ms = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_moisture_sensor_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_get(_object=ms, _status_code="BS", _ct_type=opcodes.moisture_sensor)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_get_pass_7(self):
        """ Test Message To Get Pass Case 7: Successfully returned master valve message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        mv = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_master_valve_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_get(_object=mv, _status_code="BS", _ct_type=opcodes.master_valve)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_get_pass_8(self):
        """ Test Message To Get Pass Case 8: Successfully returned temperature sensor message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        obj = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_temp_sensor_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_get(_object=obj, _status_code="BS", _ct_type=opcodes.temperature_sensor)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_get_pass_9(self):
        """ Test Message To Get Pass Case 9: Successfully returned program message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        obj = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_program_3200_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_get(_object=obj, _status_code="BS", _ct_type=opcodes.program)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_get_pass_10(self):
        """ Test Message To Get Pass Case 10: Successfully returned point of connection message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        obj = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_poc_3200_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_get(_object=obj, _status_code="EV", _ct_type=opcodes.point_of_connection)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_get_pass_11(self):
        """ Test Message To Get Pass Case 11: Successfully returned zone program message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        obj = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_zone_program_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_get(_object=obj, _status_code="BD", _ct_type=opcodes.zone_program)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_get_fail_1(self):
        """ Test Message To Get Fail Case 1: Status code for 3200 is invalid """
        # Create a mocked object and make sure that it has a 'controller type' attribute so it can be checked against
        # the library
        obj = mock.MagicMock()
        obj.controller_type = "32"

        expected_msg = "Status Code 'ZZ' is not valid for object CN."

        with self.assertRaises(ValueError) as context:
            messages.message_to_get(_object=obj, _status_code="ZZ", _ct_type=opcodes.controller)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_message_to_get_fail_2(self):
        """ Test Message To Get Fail Case 2: Status code for 1000 is invalid """
        # Create a mocked object and make sure that it has a 'controller type' attribute so it can be checked against
        # the library
        obj = mock.MagicMock()
        obj.controller_type = "10"

        expected_msg = "Status Code 'ZZ' is not valid for object CN."

        with self.assertRaises(ValueError) as context:
            messages.message_to_get(_object=obj, _status_code="ZZ", _ct_type=opcodes.controller)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_message_to_clear_pass_1(self):
        """ Test Message To Clear Pass Case 1: Successfully returned controller message
        """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        cn = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_controller_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_clear(_object=cn, _status_code="BL", _ct_type=opcodes.controller)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_clear_pass_2(self):
        """ Test Message To Clear Pass Case 2: Successfully returned zone message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        zone = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_zone_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_clear(_object=zone, _status_code="BS", _ct_type=opcodes.zone)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_clear_pass_3(self):
        """ Test Message To Clear Pass Case 3: Successfully returned event switch message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        sw = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_event_switch_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_clear(_object=sw, _status_code="BS", _ct_type=opcodes.event_switch)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_clear_pass_4(self):
        """ Test Message To Clear Pass Case 4: Successfully returned flow meter message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        fm = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_flow_meter_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_clear(_object=fm, _status_code="BS", _ct_type=opcodes.flow_meter)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_clear_pass_5(self):
        """ Test Message To Clear Pass Case 5: Successfully returned mainline message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        ml = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_mainline_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_clear(_object=ml, _status_code="FD", _ct_type=opcodes.mainline)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_clear_pass_6(self):
        """ Test Message To Clear Pass Case 6: Successfully returned moisture sensor message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        ms = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_moisture_sensor_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_clear(_object=ms, _status_code="BS", _ct_type=opcodes.moisture_sensor)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_clear_pass_7(self):
        """ Test Message To Clear Pass Case 7: Successfully returned master valve message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        mv = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_master_valve_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_clear(_object=mv, _status_code="BS", _ct_type=opcodes.master_valve)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_clear_pass_8(self):
        """ Test Message To Clear Pass Case 8: Successfully returned temperature sensor message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        obj = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_temp_sensor_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_clear(_object=obj, _status_code="BS", _ct_type=opcodes.temperature_sensor)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_clear_pass_9(self):
        """ Test Message To Clear Pass Case 9: Successfully returned program message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        obj = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_program_3200_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_clear(_object=obj, _status_code="BS", _ct_type=opcodes.program)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_clear_pass_10(self):
        """ Test Message To Clear Pass Case 10: Successfully returned point of connection message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        obj = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_poc_3200_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_clear(_object=obj, _status_code="EV", _ct_type=opcodes.point_of_connection)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_clear_pass_11(self):
        """ Test Message To Clear Pass Case 11: Successfully returned zone program message """
        # Note that it does not matter whether the object is a device, program, ect. as that is not what we are testing
        obj = mock.MagicMock()

        expected_value = "SET,FOO,BAR"

        # Mocked so we don't worry about the return value, we only care about our program actually hitting this method
        messages.create_zone_program_message = mock.MagicMock(return_value=expected_value)

        actual_value = messages.message_to_clear(_object=obj, _status_code="BD", _ct_type=opcodes.zone_program)

        self.assertEqual(expected_value, actual_value)

    #################################
    def test_message_to_clear_fail_1(self):
        """ Test Message To Clear Fail Case 1: Status code for 3200 is invalid """
        # Create a mocked object and make sure that it has a 'controller type' attribute so it can be checked against
        # the library
        obj = mock.MagicMock()
        obj.controller_type = "32"

        expected_msg = "Status Code 'ZZ' is not valid for object CN."

        with self.assertRaises(ValueError) as context:
            messages.message_to_clear(_object=obj, _status_code="ZZ", _ct_type=opcodes.controller)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_message_to_clear_fail_2(self):
        """ Test Message To Clear Fail Case 2: Status code for 1000 is invalid """
        # Create a mocked object and make sure that it has a 'controller type' attribute so it can be checked against
        # the library
        obj = mock.MagicMock()
        obj.controller_type = "10"

        expected_msg = "Status Code 'ZZ' is not valid for object CN."

        with self.assertRaises(ValueError) as context:
            messages.message_to_clear(_object=obj, _status_code="ZZ", _ct_type=opcodes.controller)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_controller_message_pass_1(self):
        """ Test Create Controller Message Pass Case 1: Successfully created basic set message """
        # Create a mock controller object and give it the necessary attributes to get the correct message returned
        controller = mock.MagicMock()
        controller.controller_type = "32"

        expected_msg = "SET,MG,CN,SS=BL"

        actual_msg = messages.create_controller_message(_controller_object=controller,
                                                        _status_code="BL",
                                                        _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_controller_message_pass_2(self):
        """ Test Create Controller Message Pass Case 2: Successfully created 'rain delay stopped' set message """
        # Create a mock controller object and give it the necessary attributes to get the correct message returned
        controller = mock.MagicMock()
        controller.controller_type = "32"
        controller.rp = 2

        # The message that we expect to get back from the controller, accounting for the variables we pass in
        expected_msg = "SET,MG,CN,SS={0},V1={1}".format(opcodes.rain_delay_stopped, controller.rp)

        actual_msg = messages.create_controller_message(_controller_object=controller,
                                                        _status_code=opcodes.rain_delay_stopped,
                                                        _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_controller_message_pass_3(self):
        """ Test Create Controller Message Pass Case 3: Successfully created 'two wire high current shutdown' set
        message
        Mock: controller
        Set: Controller type to 3200
        Mock: temperature sensor sensor
        Set: the mocked temperature sensor serial number equal to something (in this case, just an integer)
        Mock: start_stop_pause to so we don't have to use the original.
        Set: The first temperature sensor equal to the mocked temperature sensor"""
        # Create a mock controller object and give it the necessary attributes to get the correct message returned
        controller = mock.MagicMock()
        controller.controller_type = "32"

        temperature_sensor = mock.MagicMock()
        # controller.r_va = 3
        temperature_sensor.sn = "65403241"
        temperature_sensor.vd = 15
        start_stop_pause = mock.MagicMock()
        start_stop_pause._device_serial = temperature_sensor.sn
        start_stop_pause._treshold = 5
        controller.temperature_sensors = {1: temperature_sensor}

        # The message that we expect to get back from the controller, accounting for the variables we pass in
        expected_msg = "SET,MG,CN,SS={0},V2={1}".format(opcodes.two_wire_high_current_shutdown, controller.r_va)

        actual_msg = messages.create_controller_message(_controller_object=controller,
                                                        _status_code=opcodes.two_wire_high_current_shutdown,
                                                        _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_controller_message_pass_4(self):
        """ Test Create Controller Message Pass Case 4: Successfully created 'pause moisture sensor' set message
        mock controller object and give it the necessary attributes to get the correct message returned
        Set controller type to a 3200
        mock a moisture sensor so that we dont have to create a moister sensor object
        set ms serial number
        mock a start stop pause so that we dont have to create a start stop pause object
        assign moisture sensor serial number to the devicer serail in the mocked start stop pause
        set the threshold to 5
        add the mocked moisture sensor to the object bucket because the method loops through the moisture sensor until
         in finds one so we gave it one to find
         Create a message to compare against
         return the message created by the messages module
         compare the two messages if they are the same we pass
        """
        controller = mock.MagicMock()
        controller.controller_type = "32"

        # Mock a moisture sensor and then assign it to a mocked stop start pause condition.
        moisture_sensor = mock.MagicMock()
        moisture_sensor.sn = "SB00001"
        moisture_sensor.vp = 10
        start_stop_pause = mock.MagicMock()
        start_stop_pause._device_serial = moisture_sensor.sn
        start_stop_pause._threshold = 5
        controller.moisture_sensors = {1: moisture_sensor}

        # The message that we expect to get back from the controller, accounting for the variables we pass in
        expected_msg = "SET,MG,CN,SS={0},DV={1},V1={2},V2={3}".format(opcodes.pause_moisture_sensor,
                                                                      moisture_sensor.sn,
                                                                      start_stop_pause._threshold,
                                                                      moisture_sensor.vp)

        actual_msg = messages.create_controller_message(_controller_object=controller,
                                                        _status_code=opcodes.pause_moisture_sensor,
                                                        _command_type=opcodes.set_action,
                                                        _helper_object=start_stop_pause)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_controller_message_pass_5(self):
        """ Test Create Controller Message Pass Case 5: Successfully created 'pause event switch' set message
        Mock: controller object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Mock: an event switch so that we don't have to create a moisture sensor object
        Set: event switch serial number
        Mock: A start_stop_pause method so that we don't have to create a start_stop_pause object
        Set: The _mode to 5
        Add: The mocked event switch to the object bucket because the original method looks for an event switch
            in the object bucket
        Create: An expected message to compare against
        Return: The method created by the messages module
        Compare: The expected message versus the actual message, if they are the same, then we pass
        """
        # Create a mock controller object and give it the necessary attributes to get the correct message returned
        controller = mock.MagicMock()
        controller.controller_type = "32"

        # Mock an event switch and then assign it to a mocked stop start pause condition.
        event_switch = mock.MagicMock()
        event_switch.sn = "RP00001"
        event_switch.vc = 10
        start_stop_pause = mock.MagicMock()
        start_stop_pause._device_serial = event_switch.sn
        start_stop_pause._mode = 5
        controller.event_switches = {3: event_switch}

        # The message that we expect to get back from the controller, accounting for the variables we pass in
        expected_msg = "SET,MG,CN,SS={0},DV={1},V1={2},V2={3}".format(opcodes.pause_event_switch,
                                                                      event_switch.sn,
                                                                      start_stop_pause._mode,
                                                                      event_switch.vc)

        actual_msg = messages.create_controller_message(_controller_object=controller,
                                                        _status_code=opcodes.pause_event_switch,
                                                        _command_type=opcodes.set_action,
                                                        _helper_object=start_stop_pause)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_controller_message_pass_6(self):
        """ Test Create Controller Message Pass Case 6: Successfully created 'pause temp sensor' set message
        Mock: controller object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Mock: an temperature sensor so that we don't have to create a moisture sensor object
        Set: temperature sensor serial number
        Mock: A start_stop_pause method so that we don't have to create a start_stop_pause object
        Set: The _threshold to 5
        Add: The mocked event switch to the object bucket because the original method looks for a temperature sensor
            in the object bucket
        Create: An expected message to compare against
        Return: The method created by the messages module
        Compare: The expected message versus the actual message, if they are the same, then we pass
        """
        # Create a mock controller object and give it the necessary attributes to get the correct message returned
        controller = mock.MagicMock()
        controller.controller_type = "32"

        # Mock a temp sensor and then assign it to a mocked stop start pause condition.
        temp_sensor = mock.MagicMock()
        temp_sensor.sn = "SB00001"
        temp_sensor.vd = 10
        start_stop_pause = mock.MagicMock()
        start_stop_pause._device_serial = temp_sensor.sn
        start_stop_pause._threshold = 5
        controller.temperature_sensors = {1: temp_sensor}

        # The message that we expect to get back from the controller, accounting for the variables we pass in
        expected_msg = "SET,MG,CN,SS={0},DV={1},V1={2},V2={3}".format(opcodes.pause_temp_sensor,
                                                                      temp_sensor.sn,
                                                                      start_stop_pause._threshold,
                                                                      temp_sensor.vd)

        actual_msg = messages.create_controller_message(_controller_object=controller,
                                                        _status_code=opcodes.pause_temp_sensor,
                                                        _command_type=opcodes.set_action,
                                                        _helper_object=start_stop_pause)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_controller_message_pass_7(self):
        """ Test Create Controller Message Pass Case 7: Successfully created a get message for a 3200 message
        Mock: controller object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Mock: msg_date_string_for_controller_3200
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_3200
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_CN_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock controller object and give it the necessary attributes to get the correct message returned
        controller = mock.MagicMock()
        controller.controller_type = "32"

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_3200 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_3200 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,CN,SS=BL",
                        opcodes.message_id: "000_CN_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_CN_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_controller_message(_controller_object=controller,
                                                            _status_code="BL",
                                                            _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_controller_message_pass_8(self):
        """ Test Create Controller Message Pass Case 7: Successfully created a get message for a 1000 controller
        Mock: controller object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Mock: msg_date_string_for_controller_3200
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_3200
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_CN_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock controller object and give it the necessary attributes to get the correct message returned
        controller = mock.MagicMock()
        controller.controller_type = "10"

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_1000 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_1000 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,CN,SS=BL",
                        opcodes.message_id: "000_CN_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_CN_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_controller_message(_controller_object=controller,
                                                            _status_code="BL",
                                                            _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_controller_message_fail_1(self):
        """ create_controller_message fail case 1: _helper object in moisture sensor is set to None
        Mock: A controller object
        Set: Controller type equal to 3200
        Store: Status code
        Store: Expected error message
        """
        # Mock controller
        controller = mock.MagicMock()
        controller.controller_type = "32"

        # Store status code
        status_code = opcodes.pause_moisture_sensor

        # Store expected error message
        expected_err_msg = "Status code '{0}' requires you to pass in an object that inherits from BaseStartStopPause " \
                            "and that it be set to have a MoistureSensor object attached to it.".format(
                            status_code)

        # Run test, and expect a ValueError as the type of error raised
        with self.assertRaises(ValueError) as context:
            messages.create_controller_message(_controller_object=controller,
                                               _status_code=status_code,
                                               _command_type=opcodes.set_action)

        # Compare the two error messages
        self.assertEqual(expected_err_msg, context.exception.message)

    #################################
    def test_create_controller_message_fail_2(self):
        """ Create Controller Message Fail Case 2: The device serial on the helper object is set to none
        Mock: A controller object
        Set: Controller type equal to 3200
        Mock: helper_object to be passed into the method
        Store: Status code
        Store: Expected error message
        """
        # Mock controller
        controller = mock.MagicMock()
        controller.controller_type = "32"

        helper_object = mock.MagicMock()
        helper_object._device_serial = None

        # Store status code
        status_code = opcodes.pause_moisture_sensor

        # Store expected error message
        expected_err_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                           "MoistureSensor serial number. This can be done with the 'set' methods."

        # Run test, and expect a ValueError as the type of error raised
        with self.assertRaises(ValueError) as context:
            messages.create_controller_message(_controller_object=controller,
                                               _status_code=status_code,
                                               _command_type=opcodes.set_action,
                                               _helper_object=helper_object)

        self.assertEqual(expected_err_msg, context.exception.message)

    #################################
    def test_create_controller_message_fail_3(self):
        """ Create Controller Message Fail Case 3: The helper object device serial number not found in object bucket
        Mock: A controller object
        Set: Controller type equal to 3200
        Mock: helper_object to be passed into the method
        Store: Status code
        Store: Expected error message
        """
        # Mock controller
        controller = mock.MagicMock()
        controller.controller_type = "32"

        helper_object = mock.MagicMock()
        helper_object._device_serial = 'abc'

        # Store status code
        status_code = opcodes.pause_moisture_sensor

        # Store expected error message
        expected_err_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                           "MoistureSensor serial number. This can be done with the 'set' methods."

        # Run test, and expect a ValueError as the type of error raised
        with self.assertRaises(ValueError) as context:
            messages.create_controller_message(_controller_object=controller,
                                               _status_code=status_code,
                                               _command_type=opcodes.set_action,
                                               _helper_object=helper_object)

        self.assertEqual(expected_err_msg, context.exception.message)

    #################################
    def test_create_controller_message_fail_4(self):
        """ create_controller_message fail case 4: _helper object in moisture sensor is set to None
        Mock: A controller object
        Set: Controller type equal to 3200
        Store: Status code
        Store: Expected error message
        """
        # Mock controller
        controller = mock.MagicMock()
        controller.controller_type = "32"

        # Store status code
        status_code = opcodes.pause_event_switch

        # Store expected error message
        expected_err_msg = "Status code '{0}' requires you to pass in an object that inherits from BaseStartStopPause " \
                            "and that it be set to have a EventSwitch object attached to it.".format(
                            status_code)

        # Run test, and expect a ValueError as the type of error raised
        with self.assertRaises(ValueError) as context:
            messages.create_controller_message(_controller_object=controller,
                                               _status_code=status_code,
                                               _command_type=opcodes.set_action)

        # Compare the two error messages
        self.assertEqual(expected_err_msg, context.exception.message)

    #################################
    def test_create_controller_message_fail_5(self):
        """ Create Controller Message Fail Case 5: The device serial on the helper object is set to none
        Mock: A controller object
        Set: Controller type equal to 3200
        Mock: helper_object to be passed into the method
        Store: Status code
        Store: Expected error message
        """
        # Mock controller
        controller = mock.MagicMock()
        controller.controller_type = "32"

        helper_object = mock.MagicMock()
        helper_object._device_serial = None

        # Store status code
        status_code = opcodes.pause_event_switch

        # Store expected error message
        expected_err_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                           "EventSwitch serial number. This can be done with the 'set' methods."

        # Run test, and expect a ValueError as the type of error raised
        with self.assertRaises(ValueError) as context:
            messages.create_controller_message(_controller_object=controller,
                                               _status_code=status_code,
                                               _command_type=opcodes.set_action,
                                               _helper_object=helper_object)

        self.assertEqual(expected_err_msg, context.exception.message)

    #################################
    def test_create_controller_message_fail_6(self):
        """ Create Controller Message Fail Case 6: The helper object device serial number not found in object bucket
        Mock: A controller object
        Set: Controller type equal to 3200
        Mock: helper_object to be passed into the method
        Store: Status code
        Store: Expected error message
        """
        # Mock controller
        controller = mock.MagicMock()
        controller.controller_type = "32"

        helper_object = mock.MagicMock()
        helper_object._device_serial = 'abc'

        # Store status code
        status_code = opcodes.pause_event_switch

        # Store expected error message
        expected_err_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                           "EventSwitch serial number. This can be done with the 'set' methods."

        # Run test, and expect a ValueError as the type of error raised
        with self.assertRaises(ValueError) as context:
            messages.create_controller_message(_controller_object=controller,
                                               _status_code=status_code,
                                               _command_type=opcodes.set_action,
                                               _helper_object=helper_object)

        self.assertEqual(expected_err_msg, context.exception.message)

    #################################
    def test_create_controller_message_fail_7(self):
        """ create_controller_message fail case 7: _helper object in moisture sensor is set to None
        Mock: A controller object
        Set: Controller type equal to 3200
        Store: Status code
        Store: Expected error message
        """
        # Mock controller
        controller = mock.MagicMock()
        controller.controller_type = "32"

        # Store status code
        status_code = opcodes.pause_temp_sensor

        # Store expected error message
        expected_err_msg = "Status code '{0}' requires you to pass in an object that inherits from BaseStartStopPause " \
                            "and that it be set to have a TemperatureSensor object attached to it.".format(
                            status_code)

        # Run test, and expect a ValueError as the type of error raised
        with self.assertRaises(ValueError) as context:
            messages.create_controller_message(_controller_object=controller,
                                               _status_code=status_code,
                                               _command_type=opcodes.set_action)

        # Compare the two error messages
        self.assertEqual(expected_err_msg, context.exception.message)

    #################################
    def test_create_controller_message_fail_8(self):
        """ Create Controller Message Fail Case 8: The device serial on the helper object is set to none
        Mock: A controller object
        Set: Controller type equal to 3200
        Mock: helper_object to be passed into the method
        Store: Status code
        Store: Expected error message
        """
        # Mock controller
        controller = mock.MagicMock()
        controller.controller_type = "32"

        helper_object = mock.MagicMock()
        helper_object._device_serial = None

        # Store status code
        status_code = opcodes.pause_temp_sensor

        # Store expected error message
        expected_err_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                           "TemperatureSensor serial number. This can be done with the 'set' methods."

        # Run test, and expect a ValueError as the type of error raised
        with self.assertRaises(ValueError) as context:
            messages.create_controller_message(_controller_object=controller,
                                               _status_code=status_code,
                                               _command_type=opcodes.set_action,
                                               _helper_object=helper_object)

        self.assertEqual(expected_err_msg, context.exception.message)

    #################################
    def test_create_controller_message_fail_9(self):
        """ Create Controller Message Fail Case 9: The helper object device serial number not found in object bucket
        Mock: A controller object
        Set: Controller type equal to 3200
        Mock: helper_object to be passed into the method
        Store: Status code
        Store: Expected error message
        """
        # Mock controller
        controller = mock.MagicMock()
        controller.controller_type = "32"

        helper_object = mock.MagicMock()
        helper_object._device_serial = 'abc'

        # Store status code
        status_code = opcodes.pause_temp_sensor

        # Store expected error message
        expected_err_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                           "TemperatureSensor serial number. This can be done with the 'set' methods."

        # Run test, and expect a ValueError as the type of error raised
        with self.assertRaises(ValueError) as context:
            messages.create_controller_message(_controller_object=controller,
                                               _status_code=status_code,
                                               _command_type=opcodes.set_action,
                                               _helper_object=helper_object)

        self.assertEqual(expected_err_msg, context.exception.message)

    #################################
    def test_create_event_switch_message_pass_1(self):
        """ Test Create Event Switch Message Pass Case 1: Successfully created basic set message for 3200
        Create a mock event switch
        Give the event switch the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variables
        """
        # Create a mock event switch object and give it the necessary attributes to get the correct message returned
        event_switch = mock.MagicMock()
        event_switch.controller_type = "32"
        event_switch.sn = 'SB0000'

        expected_msg = "SET,MG,SW=SB0000,SS=BL"

        actual_msg = messages.create_event_switch_message(_event_switch_object=event_switch,
                                                          _status_code="BL",
                                                          _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_event_switch_message_pass_2(self):
        """ Test Create Event Switch Message Pass Case 2: Successfully created basic set message for 1000
        Create a mock event switch
        Give the event switch the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variables
        """
        # Create a mock event switch object and give it the necessary attributes to get the correct message returned
        event_switch = mock.MagicMock()
        event_switch.controller_type = "10"
        event_switch.ad = 1

        expected_msg = "SET,MG,SW=1,SS=BL"

        actual_msg = messages.create_event_switch_message(_event_switch_object=event_switch,
                                                          _status_code="BL",
                                                          _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_event_switch_message_pass_3(self):
        """ Test Create Event Switch Message Pass Case 3: Successfully created a get message for a 3200 message
        Mock: event_switch object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Mock: msg_date_string_for_controller_3200
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_3200
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_SW_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock event switch object and give it the necessary attributes to get the correct message returned
        event_switch = mock.MagicMock()
        event_switch.controller_type = "32"
        event_switch.sn = 'SB0000'

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_3200 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_3200 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,SW=SB0000,SS=BL",
                        opcodes.message_id: "000_SW_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_SW_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_event_switch_message(_event_switch_object=event_switch,
                                                              _status_code="BL",
                                                              _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_event_switch_message_pass_4(self):
        """ Test Create Event Switch Message Pass Case 4: Successfully created a get message for a 1000 message
        Mock: event_switch object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 1000
        Mock: msg_date_string_for_controller_1000
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_1000
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_SW_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock event switch object and give it the necessary attributes to get the correct message returned
        event_switch = mock.MagicMock()
        event_switch.controller_type = "10"
        event_switch.ad = 1

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_1000 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_1000 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,SW=1,SS=BL",
                        opcodes.message_id: "000_SW_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_SW_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_event_switch_message(_event_switch_object=event_switch,
                                                              _status_code="BL",
                                                              _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_flow_meter_message_pass_1(self):
        """ Test Create Flow Meter Message Pass Case 1: Successfully created basic set message for 1000
        Create a mock flow meter
        Give the flow meter the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variables
        """
        # Create a mock flow meter object and give it the necessary attributes to get the correct message returned
        flow_meter = mock.MagicMock()
        flow_meter.controller_type = "10"
        flow_meter.ad = 1

        expected_msg = "SET,MG,FM=1,SS=BL"

        actual_msg = messages.create_flow_meter_message(_flow_meter_object=flow_meter,
                                                        _status_code="BL",
                                                        _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_flow_meter_message_pass_2(self):
        """ Test Create Flow Meter Message Pass Case 2: Successfully created basic set message for 3200
        Create a mock flow meter and mock poc
        Give the flow meter serial number, and address
        Create an expected message

        """
        # Create a mock flow meter object and give it the necessary attributes to get the correct message returned
        flow_meter = mock.MagicMock()
        flow_meter.controller_type = "32"
        flow_meter.sn = 'FM0000'
        flow_meter.ad = 1

        expected_msg = "SET,MG,FM=FM0000,SS=BS,PC=3"

        # Mock a POC and place it in a dict under the flow meter test object
        poc = mock.MagicMock()
        poc.fm = 1
        poc.ad = 3
        flow_meter.controller.points_of_control = {0: poc}

        actual_msg = messages.create_flow_meter_message(_flow_meter_object=flow_meter,
                                                        _status_code=opcodes.bad_serial,
                                                        _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_flow_meter_message_pass_3(self):
        """ Test Create Flow Meter Message Pass Case 3: Successfully created a get message for a 3200 message
        Mock: flow_meter object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Mock: msg_date_string_for_controller_3200
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_3200
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_FM_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock flow meter object and give it the necessary attributes to get the correct message returned
        flow_meter = mock.MagicMock()
        flow_meter.controller_type = "32"
        flow_meter.sn = 'FM0000'

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_3200 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_3200 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,FM=FM0000,SS=UF",
                        opcodes.message_id: "000_FM_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_FM_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_flow_meter_message(_flow_meter_object=flow_meter,
                                                            _status_code=opcodes.set_upper_limit_failed,
                                                            _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_flow_meter_message_pass_4(self):
        """ Test Create Flow Meter Message Pass Case 4: Successfully created a get message for a 1000 message
        Mock: flow_meter object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 1000
        Mock: msg_date_string_for_controller_1000
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_1000
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_FM_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock flow meter object and give it the necessary attributes to get the correct message returned
        flow_meter = mock.MagicMock()
        flow_meter.controller_type = "10"
        flow_meter.ad = 1

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_1000 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_1000 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,FM=1,SS=BL",
                        opcodes.message_id: "000_FM_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_FM_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_flow_meter_message(_flow_meter_object=flow_meter,
                                                            _status_code="BL",
                                                            _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_mainline_message_pass_1(self):
        """ Test Create Mainline Message Pass Case 1: Successfully created basic set message for 3200, SS=FD/FE
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Mock the objects that will be stored in the object bucket so they can be looped through in the method
        Run the method and store the returned message as a variables
        """
        # Create a mock mainline object and give it the necessary attributes to get the correct message returned
        mainline = mock.MagicMock()
        mainline.controller_type = "32"
        mainline.ad = 1

        expected_msg = "SET,MG,ML=1,SS=FD,PC=1,DV=FM0000"

        # Mock a POC and then assign it to the object bucket so we have something to loop through
        poc = mock.MagicMock()
        poc.ml = 1
        poc.fm = 1
        poc.ad = 1
        mainline.controller.points_of_control = {0: poc}

        # Mock a flow meter and then assign it to the object bucket so we have something to loop through
        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'FM0000'
        mainline.controller.flow_meters = {0: fm}

        actual_msg = messages.create_mainline_message(_mainline_object=mainline,
                                                      _status_code=opcodes.learn_flow_fail_flow_biCoders_disabled,
                                                      _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_mainline_message_pass_2(self):
        """ Test Create Mainline Message Pass Case 2: Successfully created basic set message for 3200, SS=HV/LV
        Create a mock test object
        Give the object necessary attributes to run through the method
        Create an expected message
        Mock the objects that will be stored in the object bucket so they can be looped through in the method
        Run the method and store the returned message as a variables
        """
        # Create a mock mainline object and give it the necessary attributes to get the correct message returned
        mainline = mock.MagicMock()
        mainline.controller_type = "32"
        mainline.ad = 1

        expected_msg = "SET,MG,ML=1,SS=LV,PC=1,ZN=1,V1=22.0,V2=21.4"

        # Mock a POC and then assign it to the object bucket so we have something to loop through
        poc = mock.MagicMock()
        poc.ml = 1
        poc.fm = 1
        poc.ad = 1
        mainline.controller.points_of_control = {0: poc}

        # Mock a flow meter and then assign it to the object bucket so we have something to loop through
        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'FM0000'
        fm.vr = 21.4
        mainline.controller.flow_meters = {0: fm}

        # Mock a zone and then assign it to the object bucket so we have something to loop through
        zp = mock.MagicMock()
        mock_programs = mock.MagicMock()
        mock_programs.ml = 1
        zp.program = mock_programs
        zp.zone.df = 22.0
        mainline.controller.zone_programs = {0: zp}

        actual_msg = messages.create_mainline_message(_mainline_object=mainline,
                                                      _status_code=opcodes.low_flow_variance_detected,
                                                      _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_mainline_message_pass_3(self):
        """ Test Create Mainline Message Pass Case 3: Successfully created a get message for a 3200 message
        Mock: mainline object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Mock: msg_date_string_for_controller_3200
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_3200
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_ML_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Mock the objects that will be stored in the object bucket so they can be looped through in the method
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock mainline object and give it the necessary attributes to get the correct message returned
        mainline = mock.MagicMock()
        mainline.controller_type = "32"
        mainline.ad = 1

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_3200 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_3200 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # Mock a POC and then assign it to the object bucket so we have something to loop through
        poc = mock.MagicMock()
        poc.ml = 1
        poc.fm = 1
        poc.ad = 1
        mainline.controller.points_of_control = {0: poc}

        # Mock a flow meter and then assign it to the object bucket so we have something to loop through
        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'FM0000'
        fm.vr = 21.4
        mainline.controller.flow_meters = {0: fm}

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,ML=1,SS=UF",
                        opcodes.message_id: "000_ML_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_ML_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_mainline_message(_mainline_object=mainline,
                                                          _status_code=opcodes.set_upper_limit_failed,
                                                          _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_mainline_message_fail_1(self):
        """ Test Create Mainline Message Fail Case 1: No valid point of connection found in the object bucket
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Mock the objects that will be stored in the object bucket so they can be looped through in the method
        Run the method and expect a ValueError to be raised
        Compare the message raised with the value error to the message that we expected
        """
        # Create a mock mainline object and give it the necessary attributes to get the correct message returned
        mainline = mock.MagicMock()
        mainline.controller_type = "32"
        mainline.ad = 1

        expected_msg = "The address of the mainline object ({0}) that was passed in did not match any of the " \
                       "addresses of mainline objects that were stored in the POC objects bucket. Status code '{1}' " \
                       "failed to create a message.".format(
                           mainline.ad,                                     # {0} Mainline address
                           opcodes.learn_flow_fail_flow_biCoders_disabled   # {1} Status code passed in
                       )

        with self.assertRaises(ValueError) as context:
            messages.create_mainline_message(_mainline_object=mainline,
                                             _status_code=opcodes.learn_flow_fail_flow_biCoders_disabled,
                                             _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_mainline_message_fail_2(self):
        """ Test Create Mainline Message Fail Case 2: No valid flow meter found in the object bucket
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Mock the objects that will be stored in the object bucket so they can be looped through in the method
        Run the method and expect a ValueError to be raised
        Compare the message raised with the value error to the message that we expected
        """
        # Create a mock mainline object and give it the necessary attributes to get the correct message returned
        mainline = mock.MagicMock()
        mainline.controller_type = "32"
        mainline.ad = 1

        # Mock a POC and then assign it to the object bucket so we have something to loop through
        poc = mock.MagicMock()
        poc.ml = 1
        poc.fm = 1
        poc.ad = 1
        mainline.controller.points_of_control = {0: poc}

        expected_msg = "The address of the flow meter object ({0}) that was associated with the mainline object " \
                       "did not match any of the flow meter objects in the object bucket. Status code '{1} failed " \
                       "to create a message.".format(
                           poc.fm,                                             # {0} Flow meter address associated with the passed in mainline object
                           opcodes.learn_flow_fail_flow_biCoders_disabled)     # {1} Status code passed in

        with self.assertRaises(ValueError) as context:
            messages.create_mainline_message(_mainline_object=mainline,
                                             _status_code=opcodes.learn_flow_fail_flow_biCoders_disabled,
                                             _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_mainline_message_fail_3(self):
        """ Test Create Mainline Message Fail Case 3: No valid zone program found in the object bucket
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Mock the objects that will be stored in the object bucket so they can be looped through in the method
        Run the method and expect a ValueError to be raised
        Compare the message raised with the value error to the message that we expected
        """
        # Create a mock mainline object and give it the necessary attributes to get the correct message returned
        mainline = mock.MagicMock()
        mainline.controller_type = "32"
        mainline.ad = 1
        poc = mock.MagicMock()
        poc.ml = 1
        poc.fm = 1
        poc.ad = 1
        mainline.controller.points_of_control = {0: poc}

        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'FM0000'
        fm.vr = 21.4
        mainline.controller.flow_meters = {0: fm}

        expected_msg = "Could not find an association for a mainline and a zone. Please configure the objects so that" \
                       "there is a relationship between mainline and zone. Status code {0} failed.".format(
                           opcodes.low_flow_variance_detected   # {1} Status code passed in
                       )

        with self.assertRaises(ValueError) as context:
            messages.create_mainline_message(_mainline_object=mainline,
                                             _status_code=opcodes.low_flow_variance_detected,
                                             _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_moisture_sensor_message_pass_1(self):
        """ Test Create Moisture Sensor Message Pass Case 1: Successfully created basic set message for 3200
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock moisture sensor object and give it the necessary attributes to get the correct message returned
        moisture_sensor = mock.MagicMock()
        moisture_sensor.controller_type = "32"
        moisture_sensor.sn = 'SB0000'

        expected_msg = "SET,MG,MS=SB0000,SS=BL"

        actual_msg = messages.create_moisture_sensor_message(_moisture_sensor_object=moisture_sensor,
                                                             _status_code="BL",
                                                             _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_moisture_sensor_message_pass_2(self):
        """ Test Create Moisture Sensor Message Pass Case 2: Successfully created basic set message for 1000
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock event switch object and give it the necessary attributes to get the correct message returned
        moisture_sensor = mock.MagicMock()
        moisture_sensor.controller_type = "10"
        moisture_sensor.ad = 1

        expected_msg = "SET,MG,MS=1,SS=BL"

        actual_msg = messages.create_moisture_sensor_message(_moisture_sensor_object=moisture_sensor,
                                                             _status_code="BL",
                                                             _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_moisture_sensor_message_pass_3(self):
        """ Test Create Moisture Sensor Message Pass Case 3: Successfully created a get message for a 3200 message
        Mock: moisture_sensor object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Mock: msg_date_string_for_controller_3200
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_3200
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_MS_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock moisture sensor object and give it the necessary attributes to get the correct message returned
        moisture_sensor = mock.MagicMock()
        moisture_sensor.controller_type = "32"
        moisture_sensor.sn = 'SB0000'

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_3200 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_3200 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,MS=SB0000,SS=BL",
                        opcodes.message_id: "000_MS_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_MS_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_moisture_sensor_message(_moisture_sensor_object=moisture_sensor,
                                                                 _status_code="BL",
                                                                 _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_moisture_sensor_message_pass_4(self):
        """ Test Create Moisture Sensor Message Pass Case 4: Successfully created a get message for a 1000 message
        Mock: moisture_sensor object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 1000
        Mock: msg_date_string_for_controller_1000
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_1000
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_MS_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock moisture sensor object and give it the necessary attributes to get the correct message returned
        moisture_sensor = mock.MagicMock()
        moisture_sensor.controller_type = "10"
        moisture_sensor.ad = 1

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_1000 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_1000 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,MS=1,SS=BL",
                        opcodes.message_id: "000_MS_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_MS_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_moisture_sensor_message(_moisture_sensor_object=moisture_sensor,
                                                                 _status_code="BL",
                                                                 _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_master_valve_message_pass_1(self):
        """ Test Create Master Valve Message Pass Case 1: Successfully created basic set message for 1000
        Create a mock master valve
        Give the master valve the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variables
        """
        # Create a mock master valve object and give it the necessary attributes to get the correct message returned
        master_valve = mock.MagicMock()
        master_valve.controller_type = "10"
        master_valve.ad = 1

        expected_msg = "SET,MG,MV=1,SS=BL"

        actual_msg = messages.create_master_valve_message(_master_valve_object=master_valve,
                                                          _status_code="BL",
                                                          _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_master_valve_message_pass_2(self):
        """ Test Create Master Valve Message Pass Case 2: Successfully created basic set message for 3200
        Create a mock master valve
        Give the master valve the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variables
        """
        # Create a mock master valve object and give it the necessary attributes to get the correct message returned
        master_valve = mock.MagicMock()
        master_valve.controller_type = "32"
        master_valve.sn = 'D00000'
        master_valve.ad = 1

        expected_msg = "SET,MG,MV=D00000,SS=OC,ZN=201"

        actual_msg = messages.create_master_valve_message(_master_valve_object=master_valve,
                                                          _status_code=opcodes.open_circuit,
                                                          _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_master_valve_message_pass_3(self):
        """ Test Create Master Valve Message Pass Case 3: Successfully created a get message for a 3200 message
        Mock: master_valve object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Mock: msg_date_string_for_controller_3200
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_3200
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_MV_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock master valve object and give it the necessary attributes to get the correct message returned
        master_valve = mock.MagicMock()
        master_valve.controller_type = "32"
        master_valve.sn = 'D00000'

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_3200 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_3200 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,MV=D00000,SS=UF",
                        opcodes.message_id: "000_MV_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_MV_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_master_valve_message(_master_valve_object=master_valve,
                                                              _status_code=opcodes.set_upper_limit_failed,
                                                              _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_master_valve_message_pass_4(self):
        """ Test Create Master Valve Message Pass Case 4: Successfully created a get message for a 1000 message
        Mock: master_valve object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 1000
        Mock: msg_date_string_for_controller_1000
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_1000
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_MV_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock master valve object and give it the necessary attributes to get the correct message returned
        master_valve = mock.MagicMock()
        master_valve.controller_type = "10"
        master_valve.ad = 1

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_1000 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_1000 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,MV=1,SS=BL",
                        opcodes.message_id: "000_MV_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_MV_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_master_valve_message(_master_valve_object=master_valve,
                                                              _status_code="BL",
                                                              _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_temp_sensor_message_pass_1(self):
        """ Test Create Temperature Sensor Message Pass Case 1: Successfully created basic set message for 3200
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock moisture sensor object and give it the necessary attributes to get the correct message returned
        temperature_sensor = mock.MagicMock()
        temperature_sensor.controller_type = "32"
        temperature_sensor.sn = 'SB0000'

        expected_msg = "SET,MG,TS=SB0000,SS=BL"

        actual_msg = messages.create_temp_sensor_message(_temp_sensor_object=temperature_sensor,
                                                         _status_code="BL",
                                                         _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_temp_sensor_message_pass_2(self):
        """ Test Create Temperature Sensor Message Pass Case 2: Successfully created basic set message for 1000
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock event switch object and give it the necessary attributes to get the correct message returned
        temperature_sensor = mock.MagicMock()
        temperature_sensor.controller_type = "10"
        temperature_sensor.ad = 1

        expected_msg = "SET,MG,TS=1,SS=BL"

        actual_msg = messages.create_temp_sensor_message(_temp_sensor_object=temperature_sensor,
                                                         _status_code="BL",
                                                         _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_temp_sensor_message_pass_3(self):
        """ Test Create Temperature Sensor Message Pass Case 3: Successfully created a get message for a 3200 message
        Mock: moisture_sensor object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Mock: msg_date_string_for_controller_3200
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_3200
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_TS_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock temperature sensor object and give it the necessary attributes to get the correct message returned
        temperature_sensor = mock.MagicMock()
        temperature_sensor.controller_type = "32"
        temperature_sensor.sn = 'SB0000'

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_3200 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_3200 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,TS=SB0000,SS=BL",
                        opcodes.message_id: "000_TS_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_TS_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_temp_sensor_message(_temp_sensor_object=temperature_sensor,
                                                             _status_code="BL",
                                                             _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_temp_sensor_message_pass_4(self):
        """ Test Create Temperature Sensor Message Pass Case 4: Successfully created a get message for a 1000 message
        Mock: moisture_sensor object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 1000
        Mock: msg_date_string_for_controller_1000
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_1000
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_TS_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock temperature sensor object and give it the necessary attributes to get the correct message returned
        temperature_sensor = mock.MagicMock()
        temperature_sensor.controller_type = "10"
        temperature_sensor.ad = 1

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_1000 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_1000 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,TS=1,SS=BL",
                        opcodes.message_id: "000_TS_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_TS_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_temp_sensor_message(_temp_sensor_object=temperature_sensor,
                                                             _status_code="BL",
                                                             _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_poc_3200_message_pass_1(self):
        """ Test Create Point of Connection Message Pass Case 1: Create a 3200 message where SS=BD/BS
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Create a mock flow meter object and put it in the object bucket so it can be looped through in the method
        Run the method and store the returned message as a variable
        """
        # Create a mock moisture sensor object and give it the necessary attributes to get the correct message returned
        poc = mock.MagicMock()
        poc.controller_type = "32"
        poc.fm = 1
        poc.ad = 1
        poc.wb = 10

        expected_msg = "SET,MG,PO=1,SS=BD,V1=10,V2=11,DV=FM0000"

        # Mock a flow meter and then assign it to the object bucket so we have something to loop through
        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'FM0000'
        fm.vg = 11
        poc.controller.flow_meters = {0: fm}

        actual_msg = messages.create_poc_3200_message(_poc_object=poc,
                                                      _status_code=Message.budget_exceeded,
                                                      _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_poc_3200_message_pass_2(self):
        """ Test Create Point of Connection Message Pass Case 2: Successfully created basic set message for 1000
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock event switch object and give it the necessary attributes to get the correct message returned
        poc = mock.MagicMock()
        poc.controller_type = "10"
        poc.ad = 1

        expected_msg = "SET,MG,PC=1,SS=BL"

        actual_msg = messages.create_poc_3200_message(_poc_object=poc,
                                                      _status_code="BL",
                                                      _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_poc_3200_message_pass_3(self):
        """ Test Create Point of Connection Message Pass Case 3: Create a 3200 message where SS=HD/HS
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Create a mock flow meter object and put it in the object bucket so it can be looped through in the method
        Run the method and store the returned message as a variable
        """
        # Create a mock moisture sensor object and give it the necessary attributes to get the correct message returned
        poc = mock.MagicMock()
        poc.controller_type = "32"
        poc.fm = 1
        poc.ad = 1
        poc.hf = 10

        expected_msg = "SET,MG,PO=1,SS=HD,V1=10,V2=11,DV=FM0000"

        # Mock a flow meter and then assign it to the object bucket so we have something to loop through
        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'FM0000'
        fm.vr = 11
        poc.controller.flow_meters = {0: fm}

        actual_msg = messages.create_poc_3200_message(_poc_object=poc,
                                                      _status_code=Message.high_flow_detected,
                                                      _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_poc_3200_message_pass_4(self):
        """ Test Create Point of Connection Message Pass Case 4: Create a 3200 message where SS=MT
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Create a mock flow meter object and put it in the object bucket so it can be looped through in the method
        Create a mock event switch object and put it in the object bucket so it can be looped through in the method
        Run the method and store the returned message as a variable
        """
        # Create a mock moisture sensor object and give it the necessary attributes to get the correct message returned
        poc = mock.MagicMock()
        poc.controller_type = "32"
        poc.fm = 1
        poc.ad = 1
        poc.sw = 1
        poc.se = 'OP'

        expected_msg = "SET,MG,PO=1,SS=MT,DV=RP0000,V1=Open,V2=Open"

        # Mock a flow meter and then assign it to the object bucket so we have something to loop through
        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'FM0000'
        poc.controller.flow_meters = {0: fm}

        # Mock a flow meter and then assign it to the object bucket so we have something to loop through
        sw = mock.MagicMock()
        sw.ad = 1
        sw.sn = 'RP0000'
        sw.vc = 'OP'
        poc.controller.event_switches = {0: sw}

        actual_msg = messages.create_poc_3200_message(_poc_object=poc,
                                                      _status_code=Message.empty_shutdown,
                                                      _command_type=opcodes.set_action,
                                                      _use_to_verify=True)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_poc_3200_message_pass_5(self):
        """ Test Create Point of Connection Message Pass Case 5: Successfully created a get message for a 3200 message
        Mock: moisture_sensor object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Create a mock flow meter object and put it in the object bucket so it can be looped through in the method
        Mock: msg_date_string_for_controller_3200
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_3200
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_PO_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock point of connection object and give it the necessary attributes to get the correct message returned
        poc = mock.MagicMock()
        poc.controller_type = "32"
        poc.fm = 1
        poc.ad = 1

        # Mock a flow meter and then assign it to the object bucket so we have something to loop through
        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'FM0000'
        poc.controller.flow_meters = {0: fm}

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_3200 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_3200 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,PO=1,SS=BL,DV=FM0000",
                        opcodes.message_id: "000_PO_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_PO_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_poc_3200_message(_poc_object=poc,
                                                          _status_code="BL",
                                                          _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_poc_3200_message_pass_6(self):
        """ Test Create Point of Connection Message Pass Case 6: Successfully created a get message for a 1000 message
        Mock: moisture_sensor object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 1000
        Mock: msg_date_string_for_controller_1000
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_1000
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_PO_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock point of connection object and give it the necessary attributes to get the correct message returned
        poc = mock.MagicMock()
        poc.controller_type = "10"
        poc.ad = 1

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_1000 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_1000 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,PC=1,SS=BL",
                        opcodes.message_id: "000_PC_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_PC_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_poc_3200_message(_poc_object=poc,
                                                          _status_code="BL",
                                                          _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_poc_3200_message_fail_1(self):
        """ Test Create Point of Connection Message Fail Case 1: Run when there are no flow meters in the object bucket
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Run the method and expect a value error to be raised
        Compare the error message to our expected message
        """
        # Create a mock moisture sensor object and give it the necessary attributes to get the correct message returned
        poc = mock.MagicMock()
        poc.controller_type = "32"
        poc.fm = 'invalid'
        poc.ad = 1

        expected_msg = "The address of the flow meter object ({0}) contained in the POC object did not match any " \
                       "of the flow meter object's addresses in the object bucket. Status code '{1}' failed to " \
                       "create a message.".format(
                           poc.fm,                  # {0} Flow meter address associated with the passed in POC object
                           opcodes.empty_shutdown   # {1} Status code passed in
                       )

        with self.assertRaises(ValueError) as context:
            messages.create_poc_3200_message(_poc_object=poc,
                                             _status_code=Message.empty_shutdown,
                                             _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_poc_3200_message_fail_2(self):
        """ Test Create Point of Connection Message Fail Case 2: Run when there is no event switch in the object bucket
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Create a mock flow meter object and put it in the object bucket so it can be looped through in the method
        Run the method and store the returned message as a variable
        """
        # Create a mock moisture sensor object and give it the necessary attributes to get the correct message returned
        poc = mock.MagicMock()
        poc.controller_type = "32"
        poc.fm = 1
        poc.ad = 1
        poc.sw = 1
        poc.se = 'OP'
        status_code = Message.empty_shutdown
        poc.controller.flow_meters = {0: poc}

        expected_msg = "None of the devices passed in str in the object bucket associated with this POC object " \
                       "({0}). Status code '{1}' failed.".format(
                        poc.sw,       # {0} Event switch address associated with the passed in POC object
                        status_code   # {1} Status code passed in
                       )

        # Mock a flow meter and then assign it to the object bucket so we have something to loop through
        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'FM0000'
        # controller.flow_meters = {1: fm}

        with self.assertRaises(ValueError) as context:
            messages.create_poc_3200_message(_poc_object=poc,
                                             _status_code=Message.empty_shutdown,
                                             _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_program_3200_message_pass_1(self):
        """ Test Create Program Message Pass Case 1: Successfully created basic set message for 1000
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "10"
        program.ad = 1

        expected_msg = "SET,MG,PG=1,SS=ST,WO=US"

        actual_msg = messages.create_program_3200_message(_program_object=program,
                                                          _status_code=Message.start,
                                                          _command_type=opcodes.set_action,
                                                          _helper_object=opcodes.user)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_program_3200_message_pass_2(self):
        """ Test Create Program Message Pass Case 2: Create a 3200 message where SS=KM/MB/PM/SM/TM
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create a mock start/stop/pause object and put it in the object bucket so it can be looped through in the method
        Create a mock moisture sensor object and put it in the object bucket so it can be looped through in the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        # Create the 'helper object' and assign values
        mock_start_stop_pause = mock.MagicMock()
        mock_start_stop_pause._device_serial = 'SB0000'
        mock_start_stop_pause._threshold = 123

        # Mock a moisture sensor and then assign it to the object bucket so we have something to loop through
        ms = mock.MagicMock()
        ms.ad = 1
        ms.sn = 'SB0000'
        ms.vp = 456
        program.controller.moisture_sensors = {0: ms}

        expected_msg = "SET,MG,PG={0},SS={1},DV={2},V1={3},V2={4}".format(
            program.ad,
            Message.skipped_by_moisture_sensor,
            mock_start_stop_pause._device_serial,
            mock_start_stop_pause._threshold,
            ms.vp
        )

        actual_msg = messages.create_program_3200_message(_program_object=program,
                                                          _status_code=Message.skipped_by_moisture_sensor,
                                                          _command_type=opcodes.set_action,
                                                          _helper_object=mock_start_stop_pause)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_program_3200_message_pass_3(self):
        """ Test Create Program Message Pass Case 3: Create a 3200 message where SS=PE/SE/TE
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create a mock start/stop/pause object and put it in the object bucket so it can be looped through in the method
        Create a mock temp sensor object and put it in the object bucket so it can be looped through in the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        # Create the 'helper object' and assign values
        mock_start_stop_pause = mock.MagicMock()
        mock_start_stop_pause._device_serial = 'PR0000'
        mock_start_stop_pause._mode = 'Open'

        # Mock a event switch and then assign it to the object bucket so we have something to loop through
        sw = mock.MagicMock()
        sw.ad = 1
        sw.sn = 'PR0000'
        sw.vc = 'Open'
        program.controller.event_switches = {0: sw}

        expected_msg = "SET,MG,PG={0},SS={1},DV={2},V1={3},V2={4}".format(
            program.ad,
            Message.started_event_switch,
            mock_start_stop_pause._device_serial,
            mock_start_stop_pause._mode,
            sw.vc
        )

        actual_msg = messages.create_program_3200_message(_program_object=program,
                                                          _status_code=Message.started_event_switch,
                                                          _command_type=opcodes.set_action,
                                                          _helper_object=mock_start_stop_pause)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_program_3200_message_pass_4(self):
        """ Test Create Program Message Pass Case 4: Create a 3200 message where SS=PT/ST/TT
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create a mock start/stop/pause object and put it in the object bucket so it can be looped through in the method
        Create a mock temp sensor object and put it in the object bucket so it can be looped through in the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        # Create the 'helper object' and assign values
        mock_start_stop_pause = mock.MagicMock()
        mock_start_stop_pause._device_serial = 'AT0000'
        mock_start_stop_pause._threshold = 123

        # Mock a temp sensor and then assign it to the object bucket so we have something to loop through
        ts = mock.MagicMock()
        ts.ad = 1
        ts.sn = 'AT0000'
        ts.vd = 456
        program.controller.temperature_sensors = {0: ts}

        expected_msg = "SET,MG,PG={0},SS={1},DV={2},V1={3},V2={4}".format(
            program.ad,
            Message.started_temp_sensor,
            mock_start_stop_pause._device_serial,
            mock_start_stop_pause._threshold,
            ts.vd
        )

        actual_msg = messages.create_program_3200_message(_program_object=program,
                                                          _status_code=Message.started_temp_sensor,
                                                          _command_type=opcodes.set_action,
                                                          _helper_object=mock_start_stop_pause)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_program_3200_message_pass_5(self):
        """ Test Create Program Message Pass Case 5: Create a 3200 message where SS=RR
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create a mock temp sensor object and put it in the object bucket so it can be looped through in the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1
        program.ml = 1
        program.wr = 123

        # Mock a point of connection and then assign it to the object bucket so we have something to loop through
        poc = mock.MagicMock()
        poc.ad = 1
        poc.ml = 1
        program.controller.points_of_control = {0: poc}

        expected_msg = "SET,MG,PG={0},SS={1},V1={2},PC={3}".format(
            program.ad,
            Message.restricted_time_by_water_ration,
            program.wr,
            poc.ad
        )

        actual_msg = messages.create_program_3200_message(_program_object=program,
                                                          _status_code=Message.restricted_time_by_water_ration,
                                                          _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_program_3200_message_pass_6(self):
        """ Test Create Program Message Pass Case 6: Create a 3200 message where SS=LE/LS
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create a mock temp sensor object and put it in the object bucket so it can be looped through in the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1
        program.ml = 1
        program.wr = 123

        # Mock a temp sensor and then assign it to the object bucket so we have something to loop through
        poc = mock.MagicMock()
        poc.ad = 1
        poc.ml = 1
        program.controller.points_of_control = {0: poc}

        expected_msg = "SET,MG,PG={0},SS={1},PC={2}".format(
            program.ad,
            Message.learn_flow_with_errors,
            poc.ad
        )

        actual_msg = messages.create_program_3200_message(_program_object=program,
                                                          _status_code=Message.learn_flow_with_errors,
                                                          _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_poc_3200_message_pass_7(self):
        """ Test Create Program Message Pass Case 7: Successfully created a get message for a 3200 message
        Mock: moisture_sensor object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Create a mock flow meter object and put it in the object bucket so it can be looped through in the method
        Mock: msg_date_string_for_controller_3200
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_3200
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_PG_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_3200 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_3200 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,PG=1,SS=BL",
                        opcodes.message_id: "000_PG_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_PG_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_program_3200_message(_program_object=program,
                                                              _status_code="BL",
                                                              _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_poc_3200_message_pass_8(self):
        """ Test Create Point of Connection Message Pass Case 8: Successfully created a get message for a 1000 message
        Mock: moisture_sensor object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 1000
        Mock: msg_date_string_for_controller_1000
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_1000
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_PG_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock point of connection object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "10"
        program.ad = 1

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_1000 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_1000 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,PG=1,SS=BL",
                        opcodes.message_id: "000_PG_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_PG_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_program_3200_message(_program_object=program,
                                                              _status_code="BL",
                                                              _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_program_3200_message_fail_1(self):
        """ Test Create Program Message Fail Case 1: Build a MS start/stop/pause message without passing in a helper object
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        expected_msg = "Status code '{0}' requires you to pass in an object that inherits from BaseStartStopPause " \
                       "and that it be set to have a MoistureSensor object attached to it.".format(
                           Message.skipped_by_moisture_sensor
                       )

        with self.assertRaises(ValueError) as context:
            messages.create_program_3200_message(_program_object=program,
                                                 _status_code=Message.skipped_by_moisture_sensor,
                                                 _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_program_3200_message_fail_2(self):
        """ Test Create Program Message Fail Case 2: A MS start/stop/pause message has a helper object without a serial
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Create a mock start/stop/pause object and put it in the object bucket so it can be looped through in the method
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        # Create the 'helper object' and assign values
        mock_start_stop_pause = mock.MagicMock()
        mock_start_stop_pause._device_serial = None
        mock_start_stop_pause._threshold = 123

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "MoistureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.create_program_3200_message(_program_object=program,
                                                 _status_code=Message.skipped_by_moisture_sensor,
                                                 _command_type=opcodes.set_action,
                                                 _helper_object=mock_start_stop_pause)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_program_3200_message_fail_3(self):
        """ Test Create Program Message Fail Case 3: The moisture sensor object bucket is empty when we access it
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Create a mock start/stop/pause object and put it in the object bucket so it can be looped through in the method
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        # Create the 'helper object' and assign values
        mock_start_stop_pause = mock.MagicMock()
        mock_start_stop_pause._device_serial = 'SB0000'
        mock_start_stop_pause._threshold = 123

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "MoistureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.create_program_3200_message(_program_object=program,
                                                 _status_code=Message.skipped_by_moisture_sensor,
                                                 _command_type=opcodes.set_action,
                                                 _helper_object=mock_start_stop_pause)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_program_3200_message_fail_4(self):
        """ Test Create Program Message Fail Case 4: Build a SW start/stop/pause message without passing in a helper object
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        expected_msg = "Status code '{0}' requires you to pass in an object that inherits from BaseStartStopPause " \
                       "and that it be set to have a EventSwitch object attached to it.".format(
                           Message.started_event_switch
                       )

        with self.assertRaises(ValueError) as context:
            messages.create_program_3200_message(_program_object=program,
                                                 _status_code=Message.started_event_switch,
                                                 _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_program_3200_message_fail_5(self):
        """ Test Create Program Message Fail Case 2: A SW start/stop/pause message has a helper object without a serial
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Create a mock start/stop/pause object and put it in the object bucket so it can be looped through in the method
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        # Create the 'helper object' and assign values
        mock_start_stop_pause = mock.MagicMock()
        mock_start_stop_pause._device_serial = None
        mock_start_stop_pause._threshold = 123

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "EventSwitch serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.create_program_3200_message(_program_object=program,
                                                 _status_code=Message.started_event_switch,
                                                 _command_type=opcodes.set_action,
                                                 _helper_object=mock_start_stop_pause)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_program_3200_message_fail_6(self):
        """ Test Create Program Message Fail Case 6: The event switch object bucket is empty when we access it
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Create a mock start/stop/pause object and put it in the object bucket so it can be looped through in the method
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        # Create the 'helper object' and assign values
        mock_start_stop_pause = mock.MagicMock()
        mock_start_stop_pause._device_serial = 'PR0000'
        mock_start_stop_pause._threshold = 123

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "EventSwitch serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.create_program_3200_message(_program_object=program,
                                                 _status_code=Message.started_event_switch,
                                                 _command_type=opcodes.set_action,
                                                 _helper_object=mock_start_stop_pause)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_program_3200_message_fail_7(self):
        """ Test Create Program Message Fail Case 7: Build a TS start/stop/pause message without passing in a helper object
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        expected_msg = "Status code '{0}' requires you to pass in an object that inherits from BaseStartStopPause " \
                       "and that it be set to have a TemperatureSensor object attached to it.".format(
                           Message.started_temp_sensor
                       )

        with self.assertRaises(ValueError) as context:
            messages.create_program_3200_message(_program_object=program,
                                                 _status_code=Message.started_temp_sensor,
                                                 _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_program_3200_message_fail_8(self):
        """ Test Create Program Message Fail Case 8: A TS start/stop/pause message has a helper object without a serial
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Create a mock start/stop/pause object and put it in the object bucket so it can be looped through in the method
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        # Create the 'helper object' and assign values
        mock_start_stop_pause = mock.MagicMock()
        mock_start_stop_pause._device_serial = None
        mock_start_stop_pause._threshold = 123

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "TemperatureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.create_program_3200_message(_program_object=program,
                                                 _status_code=Message.started_temp_sensor,
                                                 _command_type=opcodes.set_action,
                                                 _helper_object=mock_start_stop_pause)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_program_3200_message_fail_9(self):
        """ Test Create Program Message Fail Case 9: The temperature sensor object bucket is empty when we access it
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Create a mock start/stop/pause object and put it in the object bucket so it can be looped through in the method
        Create a mock moisture sensor object and put it in the object bucket so it can be looped through in the method
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        # Create the 'helper object' and assign values
        mock_start_stop_pause = mock.MagicMock()
        mock_start_stop_pause._device_serial = 'TA0000'
        mock_start_stop_pause._threshold = 123

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "TemperatureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.create_program_3200_message(_program_object=program,
                                                 _status_code=Message.started_temp_sensor,
                                                 _command_type=opcodes.set_action,
                                                 _helper_object=mock_start_stop_pause)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_program_3200_message_fail_10(self):
        """ Test Create Program Message Fail Case 10: The POC object bucket is empty when we access it
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create a mock temp sensor object and put it in the object bucket so it can be looped through in the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock program object and give it the necessary attributes to get the correct message returned
        program = mock.MagicMock()
        program.controller_type = "32"
        program.ad = 1

        expected_msg = "The address of the program object's mainline ({0}) did not match any of the POC's mainline " \
                       "addresses. Status code '{1}' failed to create a message.".format(
                           program.ml,                              # {0} Mainline number for program
                           opcodes.restricted_time_by_water_ration  # {1} Status code passed in
                       )

        with self.assertRaises(ValueError) as context:
            messages.create_program_3200_message(_program_object=program,
                                                 _status_code=Message.restricted_time_by_water_ration,
                                                 _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_zone_program_message_pass_1(self):
        """ Test Create Program Message Pass Case 1: Create a 3200 message where SS=CF/CT
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create a mock moisture sensor object and put it in the object bucket so it can be looped through in the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock zone program object and give it the necessary attributes to get the correct message returned
        zone_program = mock.MagicMock()
        zone_program.controller_type = "32"
        zone_program.zone.ad = 1
        zone_program.program.ad = 1
        zone_program.ms = 1

        # Mock a moisture sensor and then assign it to the object bucket so we have something to loop through
        ms = mock.MagicMock()
        ms.ad = 1
        ms.sn = 'SB0000'
        ms.vp = 123
        zone_program.controller.moisture_sensors = {0: ms}

        expected_msg = "SET,MG,ZN={0},SS={1},PG={2},DV={3},V1={4}".format(
            zone_program.zone.ad,
            Message.calibrate_failure_no_change,
            zone_program.program.ad,
            ms.sn,
            ms.vp
        )

        actual_msg = messages.create_zone_program_message(_zone_program_object=zone_program,
                                                          _status_code=Message.calibrate_failure_no_change,
                                                          _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_zone_program_message_pass_2(self):
        """ Test Create Program Message Pass Case 2: Create a 3200 message where SS=CS
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create a mock moisture sensor object and put it in the object bucket so it can be looped through in the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock zone program object and give it the necessary attributes to get the correct message returned
        zone_program = mock.MagicMock()
        zone_program.controller_type = "32"
        zone_program.zone.ad = 1
        zone_program.program.ad = 1
        zone_program.ms = 1
        zone_program.ll = 25
        zone_program.ul = 30

        # Mock a moisture sensor and then assign it to the object bucket so we have something to loop through
        ms = mock.MagicMock()
        ms.ad = 1
        ms.sn = 'SB0000'
        ms.vp = 123
        zone_program.controller.moisture_sensors = {0: ms}

        expected_msg = "SET,MG,ZN={0},SS={1},PG={2},DV={3},V1={4},V2={5}".format(
            zone_program.zone.ad,
            Message.calibrate_successful,
            zone_program.program.ad,
            ms.sn,
            zone_program.ll,
            zone_program.ul
        )

        actual_msg = messages.create_zone_program_message(_zone_program_object=zone_program,
                                                          _status_code=Message.calibrate_successful,
                                                          _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_zone_program_message_pass_3(self):
        """ Test Create Program Message Pass Case 3: Create a 3200 message where SS=HF/LS
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock zone program object and give it the necessary attributes to get the correct message returned
        zone_program = mock.MagicMock()
        zone_program.controller_type = "32"
        zone_program.zone.ad = 1
        zone_program.program.ad = 1

        expected_msg = "SET,MG,ZN={0},SS={1},PG={2}".format(
            zone_program.zone.ad,
            Message.high_flow_shutdown_by_flow_station,
            zone_program.program.ad
        )

        actual_msg = messages.create_zone_program_message(_zone_program_object=zone_program,
                                                          _status_code=Message.high_flow_shutdown_by_flow_station,
                                                          _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_zone_program_message_pass_4(self):
        """ Test Create Program Message Pass Case 4: Create a 3200 message where SS=DX
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create a mock POC object and put it in the object bucket so it can be looped through in the method
        Create a mock flow meter object and put it in the object bucket so it can be looped through in the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock zone program object and give it the necessary attributes to get the correct message returned
        zone_program = mock.MagicMock()
        zone_program.controller_type = "32"
        zone_program.zone.ad = 1
        zone_program.zone.df = 50
        zone_program.program.ad = 1
        zone_program.program.ml = 1

        poc = mock.MagicMock()
        poc.ad = 1
        poc.ml = 1
        poc.fm = 1
        zone_program.controller.points_of_control = {0: poc}

        fm = mock.MagicMock()
        fm.ad = 1
        fm.ml = 1
        fm.vr = 55
        zone_program.controller.flow_meters = {0: fm}

        expected_msg = "SET,MG,ZN={0},SS={1},PC={2},PG={3},V1={4},V2={5}".format(
            zone_program.zone.ad,
            Message.exceeds_design_flow,
            poc.ad,
            zone_program.program.ad,
            fm.vr,
            zone_program.zone.df
        )

        actual_msg = messages.create_zone_program_message(_zone_program_object=zone_program,
                                                          _status_code=Message.exceeds_design_flow,
                                                          _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_zone_program_message_pass_5(self):
        """ Test Create Program Message Pass Case 5: Create a 3200 message where SS=FE/FK
        Set message zone learn flow with errors
        wer use the flow meter value for the set "fm.vr
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create a mock POC object and put it in the object bucket so it can be looped through in the method
        Create a mock flow meter object and put it in the object bucket so it can be looped through in the method
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock zone program object and give it the necessary attributes to get the correct message returned
        zone_program = mock.MagicMock()
        zone_program.controller_type = "32"
        zone_program.zone.ad = 1
        zone_program.zone.df = 50
        zone_program.program.ad = 1
        zone_program.program.ml = 1

        # Mock a point of connection and then assign it to the object bucket so we have something to loop through
        poc = mock.MagicMock()
        poc.ad = 1
        poc.ml = 1
        poc.fm = 1
        zone_program.controller.points_of_control = {0: poc}

        # Mock a point of connection and then assign it to the object bucket so we have something to loop through
        fm = mock.MagicMock()
        fm.ad = 1
        fm.ml = 1
        fm.vr = 55
        zone_program.controller.flow_meters = {0: fm}

        expected_msg = "SET,MG,ZN={0},SS={1},PC={2},PG={3},V2={4}".format(
            zone_program.zone.ad,
            Message.flow_learn_errors,
            poc.ad,
            zone_program.program.ad,
            fm.vr
        )

        actual_msg = messages.create_zone_program_message(_zone_program_object=zone_program,
                                                          _status_code=Message.flow_learn_errors,
                                                          _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_zone_program_message_pass_6(self):
        """ Test Create Zone Program Message Pass Case 6: Successfully created a get message for a 3200 message
        Mock: zone_programs object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Create a mock POC object and put it in the object bucket so it can be looped through in the method
        Create a mock flow meter object and put it in the object bucket so it can be looped through in the method
        Mock: msg_date_string_for_controller_3200
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_3200
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_ZN_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock zone program object and give it the necessary attributes to get the correct message returned
        zone_program = mock.MagicMock()
        zone_program.controller_type = "32"
        zone_program.zone.ad = 1
        zone_program.zone.df = 50
        zone_program.program.ad = 1
        zone_program.program.ml = 1

        poc = mock.MagicMock()
        poc.ad = 1
        poc.ml = 1
        poc.fm = 1
        zone_program.controller.points_of_control = {0: poc}

        fm = mock.MagicMock()
        fm.ad = 1
        fm.ml = 1
        fm.vr = 55
        zone_program.controller.flow_meters = {0: fm}

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_3200 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_3200 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,ZN=1,SS=HS,PC=1,PG=1,V1=50,V2=55",
                        opcodes.message_id: "000_ZN_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_ZN_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_zone_program_message(_zone_program_object=zone_program,
                                                              _status_code=opcodes.high_flow_variance_shutdown,
                                                              _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_zone_program_message_fail_1(self):
        """ Test Create Program Message Fail Case 1: No moisture sensor in the object bucket when it gets looped through
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Clear the moisture sensor object bucket dictionary to make sure a ValueError is thrown
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock zone program object and give it the necessary attributes to get the correct message returned
        zone_program = mock.MagicMock()
        zone_program.controller_type = "32"
        zone_program.zone.ad = 1
        zone_program.program.ad = 1

        # moisture_sensors.clear()

        expected_msg = "Zone {0} in Program {1} does not have a moisture sensor attached. To successfully run the " \
                       "{2}, {3}, {4}, or {5} commands, there must be a moisture sensor assigned to the primary " \
                       "zone.".format(
                           zone_program.zone.ad,
                           zone_program.program.ad,
                           Message.calibrate_failure_no_change,
                           Message.calibrate_successful,
                           Message.calibrate_failure_no_saturation,
                           Message.requires_soak_cycle
                       )

        with self.assertRaises(ValueError) as context:
            messages.create_zone_program_message(_zone_program_object=zone_program,
                                                 _status_code=Message.calibrate_failure_no_change,
                                                 _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_zone_program_message_fail_2(self):
        """ Test Create Program Message Fail Case 2: No POC in the object bucket when it gets looped through
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Clear the point of connection object bucket dictionary to make sure a ValueError is raised
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock zone program object and give it the necessary attributes to get the correct message returned
        zone_program = mock.MagicMock()
        zone_program.controller_type = "32"
        zone_program.zone.ad = 1
        zone_program.program.ad = 1
        zone_program.program.ml = 1

        # poc32.clear()

        expected_msg = "The address of the mainline object ({0}) that was associated with the passed in zone program " \
                       "could not be associated with any of the POCs in the object bucket. Status code '{1}' failed to " \
                       "create a message.".format(
                           zone_program.program.ml,     # {0} Mainline associated with this zone program
                           Message.exceeds_design_flow  # {1} Status code passed in
                       )

        with self.assertRaises(ValueError) as context:
            messages.create_zone_program_message(_zone_program_object=zone_program,
                                                 _status_code=Message.exceeds_design_flow,
                                                 _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_zone_program_message_fail_3(self):
        """ Test Create Program Message Fail Case 3: No POC in the object bucket when it gets looped through
        Create a mock test object
        Give the object the necessary attributes to run through the method
        Create a mock POC object and put it in the object bucket so it can be looped through in the method
        Clear the flow meters object bucket dictionary to make sure a ValueError is raised
        Create an expected message
        Run the method and store the returned message as a variable
        """
        # Create a mock zone program object and give it the necessary attributes to get the correct message returned
        zone_program = mock.MagicMock()
        zone_program.controller_type = "32"
        zone_program.zone.ad = 1
        zone_program.program.ad = 1
        zone_program.program.ml = 1
        zone_program.fm = 1

        # Mock a point of connection and then assign it to the object bucket so we have something to loop through
        poc = mock.MagicMock()
        poc.ad = 1
        poc.ml = 1
        poc.fm = 1
        zone_program.controller.points_of_control = {0: poc}

        expected_msg = "The address of the flow meter object ({0}) that was associated with the passed in zone program " \
                       "could not be associated with any of the flow meter objects in the object bucket. Status code " \
                       "'{1}' failed to create a message.".format(
                           zone_program.fm,             # {0} Flow meter associated with this zone program
                           Message.exceeds_design_flow  # {1} Status code passed in
                       )

        with self.assertRaises(ValueError) as context:
            messages.create_zone_program_message(_zone_program_object=zone_program,
                                                 _status_code=Message.exceeds_design_flow,
                                                 _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_create_zone_message_pass_1(self):
        """ Test Create Zone Message Pass Case 1: Successfully created basic set message for 3200 where SS=BS
        Create a mock event switch
        Give the event switch the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variables
        """
        # Create a mock zone object and give it the necessary attributes to get the correct message returned
        zone = mock.MagicMock()
        zone.controller_type = "32"
        zone.ad = 1
        zone.sn = 'E00000'

        expected_msg = "SET,MG,ZN=1,SS=BS,DV=E00000"

        actual_msg = messages.create_zone_message(_zone_object=zone,
                                                  _status_code=opcodes.bad_serial,
                                                  _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_zone_message_pass_2(self):
        """ Test Create Zone Message Pass Case 2: Successfully created basic set message for 3200 where SS=OC,SC
        Create a mock zone
        Give the zone the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variables
        """
        # Create a mock zone object and give it the necessary attributes to get the correct message returned
        zone = mock.MagicMock()
        zone.controller_type = "32"
        zone.ad = 1
        zone.sn = 'D00000'

        expected_msg = "SET,MG,ZN=1,SS=OC,DV=D00000"

        actual_msg = messages.create_zone_message(_zone_object=zone,
                                                  _status_code=opcodes.open_circuit,
                                                  _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_zone_message_pass_3(self):
        """ Test Create Zone Message Pass Case 3: Successfully created basic set message for 1000
        Create a mock zone
        Give the zone the necessary attributes to run through the method
        Create an expected message
        Run the method and store the returned message as a variables
        """
        # Create a mock event switch object and give it the necessary attributes to get the correct message returned
        zone = mock.MagicMock()
        zone.controller_type = "10"
        zone.ad = 1

        expected_msg = "SET,MG,ZN=1,SS=BL"

        actual_msg = messages.create_zone_message(_zone_object=zone,
                                                  _status_code="BL",
                                                  _command_type=opcodes.set_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_zone_message_pass_4(self):
        """ Test Create Zone Message Pass Case 4: Successfully created a get message for a 3200 message
        Mock: event_switch object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 3200
        Mock: msg_date_string_for_controller_3200
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_3200
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_ZN_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock zone object and give it the necessary attributes to get the correct message returned
        zone = mock.MagicMock()
        zone.controller_type = "32"
        zone.ad = 1

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_3200 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_3200 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,ZN=1,SS=BL",
                        opcodes.message_id: "000_ZN_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_ZN_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_zone_message(_zone_object=zone,
                                                      _status_code="BL",
                                                      _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_create_event_switch_message_pass_5(self):
        """ Test Create Event Switch Message Pass Case 5: Successfully created a get message for a 1000 message
        Mock: event_switch object, and give it the necessary attributes in order to get the correct message
        Set: controller type to the 1000
        Mock: msg_date_string_for_controller_1000
            Return: The string "01/01/01"
        Mock: msg_time_string_for_controller_1000
            Return: The string "00:00:00"
        PATCH:
            Mock: build_the_message_id
                Return: The string "000_ZN_Test"
            Mock: build_message_title
                Return: The string "Testing program messages"
        Store: Expected message
        Run: Test, and store the actual message
        Compare: The actual message and expected message, if they are the same, then the test passes
        """
        # Create a mock zone object and give it the necessary attributes to get the correct message returned
        zone = mock.MagicMock()
        zone.controller_type = "10"
        zone.ad = 1

        # Mock the build methods as we are not testing them in this method
        mock_controller_datetime = mock.MagicMock()
        mock_controller_datetime.msg_date_string_for_controller_1000 = mock.MagicMock(return_value="01/01/01")
        mock_controller_datetime.msg_time_string_for_controller_1000 = mock.MagicMock(return_value="00:00:00")
        date_mngr.controller_datetime = mock_controller_datetime

        # We expect the method to return a dictionary consisting of the variables built by the build methods
        expected_msg = {opcodes.message: "GET,MG,ZN=1,SS=BL",
                        opcodes.message_id: "000_ZN_Test",
                        opcodes.date_time: "01/01/01 00:00:00",
                        opcodes.message_text: "Testing messages"}

        with mock.patch('common.objects.base_classes.messages.build_the_message_id', return_value="000_ZN_Test"), \
                mock.patch('common.objects.base_classes.messages.build_message_title', return_value="Testing messages"):
            actual_msg = messages.create_zone_message(_zone_object=zone,
                                                      _status_code="BL",
                                                      _command_type=opcodes.get_action)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_the_message_id_pass_1(self):
        """ Test Build The Message ID Pass Case 1: Create ID for a 3200 base manager
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.sn = '3K12345'
        status_code = opcodes.message
        # because a 701 serious was passed in the device type must be basemanager
        expected_id = '{0}_{1}_{2}_{3}'.format(
            str('701'),             # {0}
            opcodes.basemanager,   # {1}
            test_obj.sn,            # {2}
            status_code             # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.basemanager,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_2(self):
        """ Test Build The Message ID Pass Case 2: Create ID for a 3200 controller, helper object is none
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.sn = 'D00000'
        status_code = opcodes.commander_paused

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str('000'),             # {0}
            opcodes.device_value,   # {1}
            test_obj.sn,            # {2} device sn for stop and pause
            status_code             # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.controller,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_3(self):
        """ Test Build The Message ID Pass Case 3: Create ID for a 3200 controller, helper object is filled
        Create a mock test object
        Create the variables required to run the method
        Create a helper object that will be passed into the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.sn = 'D00000'
        status_code = opcodes.commander_paused

        helper_obj = mock.MagicMock()
        helper_obj._device_serial = 'D12345'

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str('000'),                 # {0}
            opcodes.device_value,       # {1}
            helper_obj._device_serial,  # {2} device sn for stop and pause
            status_code                 # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.controller,
                                                  _status_code=status_code,
                                                  _helper_object=helper_obj)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_4(self):
        """ Test Build The Message ID Pass Case 4: Create ID for a 3200 event switch
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(230).zfill(3),              # {0} event switch ID number
            opcodes.event_switch,           # {1}
            test_obj.sn,                    # {2} this is the serial number to the event switch
            status_code                     # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.event_switch,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_5(self):
        """ Test Build The Message ID Pass Case 5: Create ID for a 3200 flow meter
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(220).zfill(3),              # {0} flow meter ID number
            opcodes.flow_meter,             # {1}
            test_obj.sn,                    # {2} this is the serial number to the event switch
            status_code                     # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.flow_meter,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_6(self):
        """ Test Build The Message ID Pass Case 6: Create ID for a 3200 moisture sensor
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(210).zfill(3),              # {0} moisture sensor ID number
            opcodes.moisture_sensor,        # {1}
            test_obj.sn,                    # {2} this is the serial number to the event switch
            status_code                     # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.moisture_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_7(self):
        """ Test Build The Message ID Pass Case 7: Create ID for a 3200 master valve
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(test_obj.ad + 200).zfill(3),    # {0} master valve ID number
            opcodes.master_valve,               # {1}
            test_obj.ad,                        # {2} this is the serial number to the event switch
            status_code                         # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.master_valve,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_8(self):
        """ Test Build The Message ID Pass Case 8: Create ID for a 3200 temperature sensor
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(240).zfill(3),              # {0} this is the temperature sensor ID number
            opcodes.temperature_sensor,     # {1}
            test_obj.sn,                    # {2} this is the serial number to the event switch
            status_code                     # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.temperature_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_9(self):
        """ Test Build The Message ID Pass Case 9: Create ID for a 3200 flow station
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ser.s_port = 'socket://10.11.12.242:10001'
        status_code = opcodes.error

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str('801'),                     # {0}
            opcodes.flow_station,           # {1}
            test_obj.ser.s_port[9:21],      # {2} this is the serial number of the device
            status_code                     # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.flow_station,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_10(self):
        """ Test Build The Message ID Pass Case 10: Create ID for a 3200 mainline
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.learn_flow_fail_flow_biCoders_disabled

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(int(500 + test_obj.ad)).zfill(3),   # {0} this is the mainline ID number
            opcodes.mainline,                       # {1}
            test_obj.ad,                            # {2} this is the serial number to the event switch
            status_code                             # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.mainline,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_11(self):
        """ Test Build The Message ID Pass Case 11: Create ID for a 3200 point of connection
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.budget_exceeded

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(int(400 + test_obj.ad)).zfill(3),   # {0} this is the poc ID number
            opcodes.point_of_connection,            # {1}
            test_obj.ad,                            # {2} this is the serial number to the event switch
            status_code                             # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.point_of_connection,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_12(self):
        """ Test Build The Message ID Pass Case 12: Create ID for a 3200 program
        Create a mock test object
        Create the variables required to run the method
        Create a helper object that will be passed into the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.event_date_stop

        helper_obj = mock.MagicMock()
        helper_obj._device_serial = 'D12345'

        expected_id = '{0}_{1}_{2}_{3}_{4}_{5}'.format(
            str(int(300 + test_obj.ad)).zfill(3),   # {0} this is the program ID number
            opcodes.program,                        # {1}
            test_obj.ad,                            # {2} this is the program address
            opcodes.device_value,                   # {3}
            str(helper_obj._device_serial),         # {4} this is the serial number of the device  start, stop, pause. Empty string otherwise.
            status_code                             # {5}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_obj)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_13(self):
        """ Test Build The Message ID Pass Case 13: Create ID for a 3200 pump station
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.event_date_stop

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str('601'),                     # {0}
            opcodes.master_pump_station,    # {1}
            test_obj.ad,                    # {2} this is the program number
            status_code                     # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.pump_station,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_14(self):
        """ Test Build The Message ID Pass Case 14: Create ID for a 3200 zone
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(test_obj.ad).zfill(3),      # {0} Zone ID number
            opcodes.zone,                   # {1}
            test_obj.ad,                    # {2} this is the program number
            status_code                     # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_15(self):
        """ Test Build The Message ID Pass Case 15: Create ID for a 3200 zone program
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(test_obj.zone.ad).zfill(3),     # {0} Zone ID number
            opcodes.zone,                       # {1}
            test_obj.zone.ad,                   # {2} this is the program number
            status_code                         # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.zone_program,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_16(self):
        """ Test Build The Message ID Pass Case 16: Create ID for a 3200 undefined category
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str('999'),             # {0}
            opcodes.device_value,   # {1}
            test_obj.sn,            # {2} this is the serial number of the device causing the alarm
            status_code             # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type='undefined',
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_17(self):
        """ Test Build The Message ID Pass Case 17: Create ID for a 1000 controller
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str('0'),       # {0}
            "SY",           # {1}
            "SY",           # {2}
            status_code     # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.controller,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_18(self):
        """ Test Build The Message ID Pass Case 18: Create ID for a 1000 zone
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
                test_obj.ad,        # {0} this is the zone number address
                opcodes.zone,       # {1}
                test_obj.ad,        # {2} this is the zone number address
                status_code         # {3}
            )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_19(self):
        """ Test Build The Message ID Pass Case 19: Create ID for a 1000 master valve
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(test_obj.ad + 150),     # {0} this is the master valve number address
            opcodes.master_valve,       # {1}
            test_obj.ad,                # {2} this is the master valve number address
            status_code                 # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.master_valve,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_20(self):
        """ Test Build The Message ID Pass Case 20: Create ID for a 1000 moisture sensor
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(test_obj.ad + 158),     # {0} this is the moisture sensor number address
            opcodes.moisture_sensor,    # {1}
            test_obj.sn,                # {2} this is the moisture sensor serial number
            status_code                 # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.moisture_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_21(self):
        """ Test Build The Message ID Pass Case 21: Create ID for a 1000 flow meter
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(test_obj.ad + 198),     # {0} this is the flow meter number address
            opcodes.flow_meter,         # {1}
            test_obj.sn,                # {2} this is the flow meter serial number
            status_code                 # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.flow_meter,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_22(self):
        """ Test Build The Message ID Pass Case 22: Create ID for a 1000 event switch
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(test_obj.ad + 206),     # {0} this is the event switch number address
            opcodes.event_switch,       # {1}
            test_obj.sn,                # {2} this is the event switch serial number
            status_code                 # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.event_switch,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_23(self):
        """ Test Build The Message ID Pass Case 23: Create ID for a 1000 temperature sensor
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.bad_serial

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(test_obj.ad + 216),         # {0} this is the temperature sensor number address
            opcodes.temperature_sensor,     # {1}
            test_obj.sn,                    # {2} this is the temperature sensor serial number
            status_code                     # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.temperature_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_24(self):
        """ Test Build The Message ID Pass Case 24: Create ID for a 1000 program when status isn't start/stop/pause
        Create a mock test object
        Create the variables required to run the method
        Create a mock helper object and give it variables
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = 'Not Start/Stop/Pause'

        helper_obj = mock.MagicMock()
        helper_obj._device_serial = 'D12345'

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(test_obj.ad + 299),     # {0} this is the program number address
            opcodes.program,            # {1}
            helper_obj._device_serial,  # {2} the serial number of the object attached to this program. Zeros by default
            status_code                 # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_obj)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_25(self):
        """ Test Build The Message ID Pass Case 25: Create ID for a 1000 program when status is start/stop/pause
        Create a mock test object
        Create the variables required to run the method
        Create a mock helper object and give it variables
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.start

        helper_obj = mock.MagicMock()
        helper_obj._device_serial = 'D12345'

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(test_obj.ad + 299),     # {0} this is the program number address
            opcodes.program,            # {1}
            helper_obj._device_serial,  # {2} the serial number of the object attached to this program. Zeros by default
            "OK"                        # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_obj)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_26(self):
        """ Test Build The Message ID Pass Case 26: Create ID for a 1000 point of connection
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.device_error

        expected_id = '{0}_{1}_{2}_{3}'.format(
            str(test_obj.ad + 399),         # {0} this is the zone number address
            opcodes.point_of_connection,    # {1}
            test_obj.ad,                    # {2} this is the zone number address
            status_code                     # {3}
        )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type=opcodes.point_of_connection,
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_the_message_id_pass_27(self):
        """ Test Build The Message ID Pass Case 27: Create ID for a 1000 undefined category
        Create a mock test object
        Create the variables required to run the method
        Create an id that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = opcodes.device_error

        expected_id = '{0}_{1}_{2}_{3}'.format(
                str('999'),             # {0}
                opcodes.device_value,   # {1}
                test_obj.sn,            # {2} this is the serial number of the device causing the alarm
                status_code             # {3}
            )

        actual_id = messages.build_the_message_id(_object=test_obj,
                                                  ct_type='invalid',
                                                  _status_code=status_code)

        self.assertEqual(expected_id, actual_id)

    #################################
    def test_build_message_title_pass_1(self):
        """ Test Build Message Title Pass Case 1: 3200 Message created for a base manager
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.commander_paused

        expected_msg = "BaseManager:  Message"

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.basemanager,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_2(self):
        """ Test Build Message Title Pass Case 2: 3200 Message created for a controller, status commander paused
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.commander_paused

        expected_msg = "Controller: {0}n{1}{2}{3}".format(
            test_obj.ds,
            "BL-Commander: ",
            "Run ZonenAll watering has been pausedn",
            "for manual operations."
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_3(self):
        """ Test Build Message Title Pass Case 3: 3200 Message created for a controller, status boot up
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.boot_up

        expected_msg = "Controller: {0}n{1}{2}{3}".format(
            test_obj.ds,
            "Controller has been reset:n",
            "Either from a power failure orn",
            "a controller reset operation."
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_4(self):
        """ Test Build Message Title Pass Case 4: 3200 Message created for a controller, status event date stop
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.event_date_stop

        expected_msg = "Controller: {0}n{1}{2}{3}".format(
            test_obj.ds,
            "Event Date Blocked:n",
            "All watering has been stoppedn",
            "for this date."
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_5(self):
        """ Test Build Message Title Pass Case 5: 3200 Message created for a controller, status usb flash storage
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.usb_flash_storage_failure

        expected_msg = "Controller: {0}n{1}{2}".format(
            test_obj.ds,
            "USB Error:n",
            "Unable to access the USB flash drive."
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_6(self):
        """ Test Build Message Title Pass Case 6: 3200 Message created for a controller, status flow jumper stopped
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.flow_jumper_stopped

        expected_msg = "Controller: {0}n{1}{2}{3}".format(
            test_obj.ds,
            "Flow Contacts Triggered:n",
            "All watering has been stopped.n",
            "Value = Open"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_7(self):
        """ Test Build Message Title Pass Case 7: 3200 Message created for a controller, status two wire high current shutdown with r_va variable
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        test_obj.r_va = 1.7
        status_code = opcodes.two_wire_high_current_shutdown

        expected_msg = "Controller: {0}n{1}{2}{3}{4}{5}".format(
            test_obj.ds,
            "Two-wire Failure:n",
            "High current indicates an",
            "short circuit.n",
            "Current = ",
            test_obj.r_va
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_8(self):
        """ Test Build Message Title Pass Case 8: 3200 Message created for a controller, status two wire high current shutdown with r_va variable
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        test_obj.r_va = None
        status_code = opcodes.two_wire_high_current_shutdown

        expected_msg = "Controller: {0}n{1}{2}{3}{4}{5}".format(
            test_obj.ds,
            "Two-wire Failure:n",
            "High current indicates an",
            "short circuit.n",
            "Current = ",
            "None"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_9(self):
        """ Test Build Message Title Pass Case 9: 3200 Message created for a controller, status pause event switch
        Create a mock test object
        Create the variables required to run the method
        Create a helper object
        Create an mock event switch
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.pause_event_switch

        helper_obj = mock.MagicMock()
        helper_obj._device_serial = 'D12345'
        helper_obj._mode = 'Open'

        sw = mock.MagicMock()
        sw.vc = 'Open'
        sw.sn = 'D12345'
        test_obj.controller.event_switches = {0: sw}

        expected_msg = "Controller: {0}n{1}{2}{3}{4}{5}{6}{7}".format(
            test_obj.ds,
            "Event biCoder Triggered:n",
            "All watering has been paused.n",
            "Event biCoder = ",
            str(helper_obj._device_serial) + "n",
            "Limit/Value = ",
            str(helper_obj._mode) + " / ",
            str(sw.vc)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code,
                                                  _helper_object=helper_obj)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_10(self):
        """ Test Build Message Title Pass Case 10: 3200 Message created for a controller, status pause moisture sensor
        Create a mock test object
        Create the variables required to run the method
        Create a helper object
        Create an mock moisture sensor and store it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D12345'
        test_obj.ds = 'Testing description'
        test_obj.vp = 27
        test_obj.controller.moisture_sensors = {0: test_obj}
        status_code = opcodes.pause_moisture_sensor

        helper_obj = mock.MagicMock()
        helper_obj._device_serial = 'D12345'
        helper_obj._threshold = 27

        ms = mock.MagicMock()
        ms.vp = 27
        ms.sn = 'D12345'
        # moisture_sensors[1] = ms

        expected_msg = "Controller: {0}n{1}{2}{3}{4}{5}{6}{7}".format(
            test_obj.ds,
            "Moisture Limit Reached:n",
            "All watering has been paused.n",
            "Moisture biSensor = ",
            str(helper_obj._device_serial) + "n",
            "Limit/Value = ",
            str(helper_obj._threshold) + " / ",
            str(ms.vp)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code,
                                                  _helper_object=helper_obj)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_11(self):
        """ Test Build Message Title Pass Case 11: 3200 Message created for a controller, status pause temp sensor
        Create a mock test object
        Create the variables required to run the method
        Create a helper object
        Create an mock temperature sensor and store it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D12345'
        test_obj.ds = 'Testing description'
        test_obj.vd = 98
        test_obj.controller.temperature_sensors = {0: test_obj}
        status_code = opcodes.pause_temp_sensor

        helper_obj = mock.MagicMock()
        helper_obj._device_serial = 'D12345'
        helper_obj._threshold = 97

        ts = mock.MagicMock()
        ts.vd = 98
        ts.sn = 'D12345'
        # temperature_sensors[1] = ts

        expected_msg = "Controller: {0}n{1}{2}{3}{4}{5}{6}{7}".format(
            test_obj.ds,
            "Temperature Limit Reached:n",
            "All watering has been paused.n",
            "Temperature biSensor = ",
            str(helper_obj._device_serial) + "n",
            "Limit/Value = ",
            str(helper_obj._threshold) + " / ",
            str(ts.vd)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code,
                                                  _helper_object=helper_obj)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_12(self):
        """ Test Build Message Title Pass Case 12: 3200 Message created for a controller, status pause jumper
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.pause_jumper

        expected_msg = "Controller: {0}n{1}{2}{3}".format(
            test_obj.ds,
            "Pause Contacts Triggered:n",
            "All watering has been pausedn",
            "for four hours minimum."
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_13(self):
        """ Test Build Message Title Pass Case 13: 3200 Message created for a controller, status rain delay stopped
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        test_obj.rp = 2
        status_code = opcodes.rain_delay_stopped

        _date = datetime.date(year=2016, month=6, day=29)
        _time = datetime.time(hour=1, minute=1, second=1)

        # Mock the build methods as we are not testing them in this method
        mock_curr_computer_date = mock.MagicMock()
        mock_curr_computer_date.obj = _date
        mock_curr_computer_date.time_obj.obj = _time
        date_mngr.curr_computer_date = mock_curr_computer_date

        expected_msg = "Controller: {0}n{1}{2}{3}{4}{5}{6}{7}".format(
            test_obj.ds,
            "Rain Days Active:n",
            "All watering has been stopped.n",
            "Value = ",
            str(test_obj.rp) + " daysn",
            "Until = ",
            '07/01/16' + " ",
            '12:00:00 AM'
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_14(self):
        """ Test Build Message Title Pass Case 14: 3200 Message created for a controller, status stop event switch
        Create a mock test object
        Create the variables required to run the method
        Create a helper object
        Create an mock event switch and store it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D12345'
        test_obj.ds = 'Testing description'
        test_obj.vc = 'Open'
        test_obj.controller.event_switches = {0: test_obj}
        status_code = opcodes.stop_event_switch

        helper_obj = mock.MagicMock()
        helper_obj._device_serial = 'D12345'
        helper_obj._mode = 'Open'

        sw = mock.MagicMock()
        sw.vc = 'Open'
        sw.sn = 'D12345'

        expected_msg = "Controller: {0}n{1}{2}{3}{4}{5}{6}{7}".format(
            test_obj.ds,
            "Event biCoder Triggered:n",
            "All watering has been stopped.n",
            "Event biCoder = ",
            str(helper_obj._device_serial) + "n",
            "Limit/Value = ",
            str(helper_obj._mode) + " / ",
            str(sw.vc)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code,
                                                  _helper_object=helper_obj)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_15(self):
        """ Test Build Message Title Pass Case 15: 3200 Message created for a controller, status stop moisture sensor
        Create a mock test object
        Create the variables required to run the method
        Create a helper object
        Create an mock moisture sensor and store it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D12345'
        test_obj.ds = 'Testing description'
        test_obj.controller.moisture_sensors = {0: test_obj}
        test_obj.vp = 27
        status_code = opcodes.stop_moisture_sensor

        helper_obj = mock.MagicMock()
        helper_obj._device_serial = 'D12345'
        helper_obj._threshold = 26

        ms = mock.MagicMock()
        ms.vp = 27
        ms.sn = 'D12345'
        # moisture_sensors[1] = ms

        expected_msg = "Controller: {0}n{1}{2}{3}{4}{5}{6}{7}".format(
            test_obj.ds,
            "Moisture Limit Reached:n",
            "All watering has been stopped.n",
            "Moisture biSensor = ",
            str(helper_obj._device_serial) + "n",
            "Limit/Value = ",
            str(helper_obj._threshold) + " / ",
            str(ms.vp)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code,
                                                  _helper_object=helper_obj)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_16(self):
        """ Test Build Message Title Pass Case 16: 3200 Message created for a controller, status stop temp sensor
        Create a mock test object
        Create the variables required to run the method
        Create a helper object
        Create an mock temperature sensor and store it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.vd = 98
        test_obj.ad = 1
        test_obj.sn = 'D12345'
        test_obj.ds = 'Testing description'
        test_obj.controller.temperature_sensors = {0: test_obj}
        status_code = opcodes.stop_temp_sensor

        helper_obj = mock.MagicMock()
        helper_obj._device_serial = 'D12345'
        helper_obj._threshold = 97

        ts = mock.MagicMock()
        ts.vd = 98
        ts.sn = 'D12345'

        expected_msg = "Controller: {0}n{1}{2}{3}{4}{5}{6}{7}".format(
            test_obj.ds,
            "Temperature Limit Reached:n",
            "All watering has been stopped.n",
            "Temperature biSensor = ",
            str(helper_obj._device_serial) + "n",
            "Limit/Value = ",
            str(helper_obj._threshold) + " / ",
            str(ts.vd)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code,
                                                  _helper_object=helper_obj)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_17(self):
        """ Test Build Message Title Pass Case 17: 3200 Message created for an event switch, status bad serial
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.bad_serial

        expected_msg = "Event biCoder: {0}n{1}{2}{3}{4}".format(
            test_obj.ds,
            "Device Error:n",
            "Wrong device at this address.n",
            "Device = ",
            test_obj.sn
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.event_switch,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_18(self):
        """ Test Build Message Title Pass Case 18: 3200 Message created for an event switch, status no response
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.no_response

        expected_msg = "Event biCoder: {0}n{1}{2}{3}{4}".format(
            test_obj.ds,
            "Two-wire Communication Failure:n",
            "Unable to talk with this device.n",
            "Device = ",
            test_obj.sn
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.event_switch,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_19(self):
        """ Test Build Message Title Pass Case 19: 3200 Message created for a flow meter, status bad serial
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.bad_serial

        expected_msg = "Flow biCoder: {0}n{1}{2}{3}{4}".format(
            test_obj.ds,
            "Device Error:n",
            "Wrong device at this address.n",
            "Device = ",
            test_obj.sn
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.flow_meter,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_20(self):
        """ Test Build Message Title Pass Case 20: 3200 Message created for a flow meter, status no response
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.no_response

        expected_msg = "Flow biCoder: {0}n{1}{2}{3}{4}".format(
            test_obj.ds,
            "Two-wire Communication Failure:n",
            "Unable to talk with this device.n",
            "Device = ",
            test_obj.sn
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.flow_meter,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_21(self):
        """ Test Build Message Title Pass Case 21: 3200 Message created for a flow meter, status set upper limit failed
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.set_upper_limit_failed

        expected_msg = "Flow biCoder: {0}n{1}{2}{3}".format(
            test_obj.ds,
            "Flow biCoder Failure:n",
            "Unable to set high flow shutdownn",
            "limit in Flow biCoder."
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.flow_meter,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_22(self):
        """ Test Build Message Title Pass Case 22: 3200 Message created for a mainline, status learn flow fail flow biCoder disabled
        Create a mock test object
        Create the variables required to run the method
        Create a mock poc object and give it variables then store it in the object bucket
        Create a mock flow meter object and give it variables then store it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D12345'
        test_obj.ds = 'Testing description'
        test_obj.ml = 1

        status_code = opcodes.learn_flow_fail_flow_biCoders_disabled

        poc = mock.MagicMock()
        poc.ml = 1
        poc.fm = 1

        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'D12345'
        test_obj.controller.points_of_control = {0: poc}
        test_obj.controller.flow_meters = {0: fm}

        expected_msg = "Mainline: {0}n{1}{2}{3}{4}".format(
            test_obj.ad,
            "Flow biCoder Disabledn",
            "Unable to do a Learn Flow operation.n",
            "Flow biCoder = ",
            fm.sn
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.mainline,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_23(self):
        """ Test Build Message Title Pass Case 23: 3200 Message created for a mainline, status learn flow fail flow biCoder error
        Create a mock test object
        Create the variables required to run the method
        Create a mock poc object and give it variables then store it in the object bucket
        Create a mock flow meter object and give it variables then store it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D12345'
        test_obj.ds = 'Testing description'
        test_obj.ml = 1
        status_code = opcodes.learn_flow_fail_flow_biCoders_error

        poc = mock.MagicMock()
        poc.ml = 1
        poc.fm = 1

        test_obj.controller.points_of_control = {0: poc}

        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'D12345'

        test_obj.controller.flow_meters = {0: fm}

        expected_msg = "Mainline: {0}n{1}{2}{3}{4}{5}".format(
            test_obj.ad,
            "Flow biCoder Error:n",
            "Unable to read Flow biCoder.n",
            "Learn Flow operation terminated.n",
            "Flow biCoder = ",
            fm.sn
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.mainline,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_24(self):
        """ Test Build Message Title Pass Case 24: 3200 Message created for a mainline, status high flow variance detect
        Create a mock test object
        Create the variables required to run the method
        Create a mock poc object and give it variables then store it in the object bucket
        Create a mock flow meter object and give it variables then store it in the object bucket
        Create a mock zone program object and give it variables then store it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D12345'
        test_obj.ds = 'Testing description'
        test_obj.ml = 1
        status_code = opcodes.high_flow_variance_detected

        poc = mock.MagicMock()
        poc.ml = 1
        poc.fm = 1
        test_obj.controller.points_of_control = {0: poc}

        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'D12345'
        fm.vr = 20
        test_obj.controller.flow_meters = {0: fm}

        zp = mock.MagicMock()
        zp.ad = 1
        zp.program.ml = 1
        zp.zone.ad = 1
        zp.zone.df = 18
        test_obj.controller.programs.zone_programs = {0: zp}

        expected_msg = "Mainline: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}".format(
            test_obj.ad,
            "High Flow Variance Detected:n",
            "Measured flow is above limit.n",
            "Mainline = ",
            str(test_obj.ad) + "n",
            "Zone = ",
            str(zp.zone.ad) + "n",
            "Limit/Value = ",
            str(zp.zone.df),
            " / ",
            str(fm.vr)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.mainline,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_25(self):
        """ Test Build Message Title Pass Case 25: 3200 Message created for a mainline, status low flow variance detect
        Create a mock test object
        Create the variables required to run the method
        Create a mock poc object and give it variables then store it in the object bucket
        Create a mock flow meter object and give it variables then store it in the object bucket
        Create a mock zone program object and give it variables then store it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D12345'
        test_obj.ds = 'Testing description'
        test_obj.ml = 1
        status_code = opcodes.low_flow_variance_detected

        poc = mock.MagicMock()
        poc.ml = 1
        poc.fm = 1
        test_obj.controller.points_of_control = {0: poc}

        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'D12345'
        fm.vr = 20
        test_obj.controller.flow_meters = {0: fm}

        zp = mock.MagicMock()
        zp.ad = 1
        zp.program.ml = 1
        zp.zone.ad = 1
        zp.zone.df = 18
        test_obj.controller.programs.zone_programs = {0: zp}

        expected_msg = "Mainline: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}".format(
            test_obj.ad,
            "Low Flow Variance Detected:n",
            "Measured flow is below limit.n",
            "Mainline = ",
            str(test_obj.ad) + "n",
            "Zone = ",
            str(zp.zone.ad) + "n",
            "Limit/Value = ",
            str(zp.zone.df),
            " / ",
            str(fm.vr)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.mainline,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_26(self):
        """ Test Build Message Title Pass Case 26: 3200 Message created for a moisture sensor, status bad serial
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.bad_serial

        expected_msg = "biSensor: {0}n{1}{2}{3}{4}".format(
            test_obj.ds,
            "Device Error:n",
            "Wrong device at this address.n",
            "Device = ",
            test_obj.sn
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.moisture_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_27(self):
        """ Test Build Message Title Pass Case 27: 3200 Message created for a moisture sensor, status sensor disabled
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.sensor_disabled

        expected_msg = "biSensor: {0}n{1}{2}{3}{4}{5}{6}".format(
            test_obj.ds,
            "Moisture biSensor Disabled:n",
            "Unable to communicate withn",
            "moisture biSensor.n",
            "Moisture biSensor = ",
            str(test_obj.sn) + "n",
            "Watering = Timed"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.moisture_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_28(self):
        """ Test Build Message Title Pass Case 28: 3200 Message created for a moisture sensor, status no response
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.no_response

        expected_msg = "biSensor: {0}n{1}{2}{3}{4}".format(
            test_obj.ds,
            "Two-wire Communication Failure:n",
            "Unable to talk with this device.n",
            "Device = ",
            test_obj.sn
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.moisture_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_29(self):
        """ Test Build Message Title Pass Case 29: 3200 Message created for a moisture sensor, status zero reading
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.zero_reading

        expected_msg = "biSensor: {0}n{1}{2}{3}{4}{5}".format(
            test_obj.ds,
            "Moisture biSensor Failure:n",
            "Unable to get a valid moisturen",
            "reading from biSensor.n",
            "Moisture biSensor = ",
            str(test_obj.sn) + "n"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.moisture_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_30(self):
        """ Test Build Message Title Pass Case 30: 3200 Message created for a master valve, status bad serial
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.bad_serial

        expected_msg = "MV{0}: {1}n{2}{3}{4}{5}".format(
            test_obj.ad,
            test_obj.ds,
            "Device Error:n",
            "Wrong device at this address.n",
            "Device = ",
            test_obj.sn
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.master_valve,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_31(self):
        """ Test Build Message Title Pass Case 31: 3200 Message created for a master valve, status no response
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.no_response

        expected_msg = "MV{0}: {1}n{2}{3}{4}{5}".format(
            test_obj.ad,
            test_obj.ds,
            "Two-wire Communication Failure:n",
            "Unable to talk with this device.n",
            "Device = ",
            test_obj.sn
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.master_valve,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_32(self):
        """ Test Build Message Title Pass Case 32: 3200 Message created for a master valve, status open circuit
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.open_circuit

        expected_msg = "MV{0}: {1}n{2}{3}{4}{5}{6}{7}{8}".format(
            test_obj.ad,
            test_obj.ds,
            "Zone Failure:n",
            "Open Circuit Solenoid.n",
            "Unable to operate this valve.n",
            "Zone = ",
            str(test_obj.ad + 200) + "n",
            "biCoder = ",
            str(test_obj.sn) + "n"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.master_valve,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_33(self):
        """ Test Build Message Title Pass Case 33: 3200 Message created for a master valve, status short circuit
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.short_circuit

        expected_msg = "MV{0}: {1}n{2}{3}{4}{5}{6}{7}{8}".format(
            test_obj.ad,
            test_obj.ds,
            "Zone Failure:n",
            "Short Circuit Solenoid.n",
            "Unable to operate this valve.n",
            "Zone = ",
            str(test_obj.ad + 200) + "n",
            "biCoder = ",
            str(test_obj.sn) + "n"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.master_valve,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_34(self):
        """ Test Build Message Title Pass Case 34: 3200 Message created for a poc, status budget exceeded
        Create a mock test object
        Create the variables required to run the method
        Create a mock flow meter and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        test_obj.wb = 123
        test_obj.fm = 1
        status_code = Message.budget_exceeded

        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'D12345'
        fm.vg = 456
        test_obj.controller.flow_meters = {0: fm}

        expected_msg = "POC: {0}n{1}{2}{3}{4}{5}{6}{7}{8}".format(
            test_obj.ds,
            "Monthly Budget Exceeded:n",
            "Watering will continue.n",
            "POC = ",
            str(test_obj.ad) + "n",
            "Budget/Used = ",
            test_obj.wb,
            " / ",
            fm.vg
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.point_of_connection,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_35(self):
        """ Test Build Message Title Pass Case 35: 3200 Message created for a poc, status budget exceeded shutdown
        Create a mock test object
        Create the variables required to run the method
        Create a mock flow meter and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        test_obj.wb = 123
        test_obj.fm = 1
        status_code = Message.budget_exceeded_shutdown

        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'D12345'
        fm.vg = 456
        test_obj.controller.flow_meters = {0: fm}

        expected_msg = "POC: {0}n{1}{2}{3}{4}{5}{6}{7}{8}".format(
            test_obj.ds,
            "Monthly Budget Exceeded:n",
            "Watering has been shut down.n",
            "POC = ",
            str(test_obj.ad) + "n",
            "Budget/Used = ",
            test_obj.wb,
            " / ",
            fm.vg,
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.point_of_connection,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_36(self):
        """ Test Build Message Title Pass Case 36: 3200 Message created for a poc, status empty shutdown
        Create a mock test object
        Create the variables required to run the method
        Create a mock flow meter and assign it variables then put it in the object bucket
        Create a mock event switch and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D12345'
        test_obj.ds = 'Testing description'
        test_obj.se = 'CL'
        test_obj.fm = 1
        test_obj.sw = 1
        test_obj.vr = 456
        status_code = Message.empty_shutdown

        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'D12345'
        fm.vr = 456
        test_obj.controller.flow_meters = {0: fm}

        sw = mock.MagicMock()
        sw.ad = 1
        sw.bicoder.sn = 'D12345'
        sw.vc = 'OP'
        test_obj.controller.event_switches = {0: sw}

        expected_msg = "POC: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}".format(
            test_obj.ds,
            "POC Empty Condition: n",
            "The POC has been shut down.n",
            "POC = ",
            str(test_obj.ad) + "n",
            "Device = ",
            str(sw.bicoder.sn) + "n",
            "Limit/Value = ",
            'Closed',
            " / ",
            'Open'
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.point_of_connection,
                                                  _status_code=status_code,
                                                  _use_to_verify=True)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_37(self):
        """ Test Build Message Title Pass Case 37: 3200 Message created for a poc, status high flow detected
        Create a mock test object
        Create the variables required to run the method
        Create a mock flow meter and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        test_obj.hf = 123
        test_obj.fm = 1
        status_code = Message.high_flow_detected

        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'D12345'
        fm.vr = 456
        test_obj.controller.flow_meters = {0: fm}

        expected_msg = "POC: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}".format(
            test_obj.ds,
            "POC High Flow Detected:n",
            "Measured flow exceeds limit.n",
            "POC = ",
            str(test_obj.ad) + "n",
            "Flow biCoder = ",
            str(fm.sn) + "n",
            "Limit/Value = ",
            test_obj.hf,
            " / ",
            fm.vr,
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.point_of_connection,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_38(self):
        """ Test Build Message Title Pass Case 38: 3200 Message created for a poc, status high flow shutdown
        Create a mock test object
        Create the variables required to run the method
        Create a mock flow meter and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        test_obj.hf = 123
        test_obj.fm = 1
        status_code = Message.high_flow_shutdown

        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'D12345'
        fm.vr = 456
        test_obj.controller.flow_meters = {0: fm}

        expected_msg = "POC: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}".format(
            test_obj.ds,
            "POC High Flow Detected:n",
            "POC water has been shut down.n",
            "POC = ",
            str(test_obj.ad) + "n",
            "Flow biCoder = ",
            str(fm.sn) + "n",
            "Limit/Value = ",
            test_obj.hf,
            " / ",
            fm.vr
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.point_of_connection,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_39(self):
        """ Test Build Message Title Pass Case 39: 3200 Message created for a program, status event date stop
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.event_date_stop

        expected_msg = "Program: {0}n{1}{2}{3}{4}".format(
            test_obj.ds,
            "Program Stopped:n",
            "Event Day is active for today.n",
            "Program = ",
            str(test_obj.ad)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_40(self):
        """ Test Build Message Title Pass Case 40: 3200 Message created for a program, status learn flow with errors
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.learn_flow_with_errors

        expected_msg = "Program: {0}n{1}{2}{3}{4}".format(
            test_obj.ds,
            "Program Learn Flow Done:n",
            "Completed with Errors.n",
            "Program = ",
            str(test_obj.ad)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_41(self):
        """ Test Build Message Title Pass Case 41: 3200 Message created for a program, status learn flow success
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.learn_flow_success

        expected_msg = "Program: {0}n{1}{2}{3}{4}".format(
            test_obj.ds,
            "Program Learn Flow Done:n",
            "Completed successfully.n",
            "Program = ",
            str(test_obj.ad)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_42(self):
        """ Test Build Message Title Pass Case 42: 3200 Message created for a program, status started by bad moisture
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.started_by_bad_moisture_sensor

        helper_object = mock.MagicMock()
        helper_object._device_serial = "D12345"

        expected_msg = "Program: {0}n{1}{2}{3}{4}{5}{6}".format(
            test_obj.ds,
            "Program Started:n",
            "The limit was reached, due to n"
            "a failed moisture biSensor.n",
            "Program = ",
            str(test_obj.ad) + "n",
            "Moisture biSensor = ",
            str(helper_object._device_serial) + "n"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_object)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_43(self):
        """ Test Build Message Title Pass Case 43: 3200 Message created for a program, status over run start event
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.over_run_start_event

        expected_msg = "Program: {0}n{1}{2}{3}{4}{5}".format(
            test_obj.ds,
            "Program Overrun:n",
            "Program was running when an",
            "start time was hit.n",
            "Program = ",
            str(test_obj.ad) + "n"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_44(self):
        """ Test Build Message Title Pass Case 44: 3200 Message created for a program, status priority paused
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.priority_paused

        expected_msg = "Program: {0}n{1}{2}{3}{4}".format(
            test_obj.ds,
            "Program Paused:n",
            "A higher priority program hasn"
            "preempted this program.n",
            "Program = ",
            str(test_obj.ad)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_45(self):
        """ Test Build Message Title Pass Case 45: 3200 Message created for a program, status water window paused
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.water_window_paused

        expected_msg = "Program: {0}n{1}{2}{3}{4}".format(
            test_obj.ds,
            "Program Paused:n",
            "A water window is closedn",
            "Program = ",
            str(test_obj.ad)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_46(self):
        """ Test Build Message Title Pass Case 46: 3200 Message created for a program, status restricted time by water
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.restricted_time_by_water_ration

        expected_msg = "Program: {0}n{1}{2}{3}{4}{5}{6}{7}{8}".format(
            test_obj.ds,
            "Program Time Restricted:n",
            "Water Rationing is Activen",
            "Program water time has been reduced.n",
            "Program = ",
            str(test_obj.ad) + "n",
            "Run Times = ",
            str(test_obj.wr),
            " %"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_47(self):
        """ Test Build Message Title Pass Case 47: 3200 Message created for a program, status skipped by moisture sensor
        Create a mock test object
        Create the variables required to run the method
        Create a mock helper object and assign it variables then put it in the object bucket
        Create a mock moisture sensor and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.skipped_by_moisture_sensor

        helper_object = mock.MagicMock()
        helper_object._device_serial = 'D12345'
        helper_object._threshold = 123

        ms = mock.MagicMock()
        ms.sn = 'D12345'
        ms.vp = 456
        test_obj.controller.moisture_sensors = {0: ms}

        expected_msg = "Program: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
            test_obj.ds,
            "Program Not Started:n",
            "Moisture limit has not been reachedn",
            "Program = ",
            str(test_obj.ad) + "n",
            "Moisture biSensor = ",
            str(helper_object._device_serial) + "n",
            "Limit/Value = ",
            str(helper_object._threshold) + " / ",
            str(ms.vp)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_object)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_48(self):
        """ Test Build Message Title Pass Case 48: 3200 Message created for a program, status pause moisture sensor
        Create a mock test object
        Create the variables required to run the method
        Create a mock helper object and assign it variables then put it in the object bucket
        Create a mock moisture sensor and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.pause_moisture_sensor

        helper_object = mock.MagicMock()
        helper_object._device_serial = 'D12345'
        helper_object._threshold = 123

        ms = mock.MagicMock()
        ms.sn = 'D12345'
        ms.vp = 456
        test_obj.controller.moisture_sensors = {0: ms}

        expected_msg = "Program: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
            test_obj.ds,
            "Program Paused:n",
            "Moisture limit has been reached.n",
            "Program = ",
            str(test_obj.ad) + "n",
            "Moisture biSensor = ",
            str(helper_object._device_serial) + "n",
            "Limit/Value = ",
            str(helper_object._threshold) + " / ",
            str(ms.vp)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_object)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_49(self):
        """ Test Build Message Title Pass Case 49: 3200 Message created for a program, status pause event switch
        Create a mock test object
        Create the variables required to run the method
        Create a mock helper object and assign it variables then put it in the object bucket
        Create a mock event switch and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.pause_event_switch

        helper_object = mock.MagicMock()
        helper_object._device_serial = 'D12345'
        helper_object._mode = 'OP'

        sw = mock.MagicMock()
        sw.sn = 'D12345'
        sw.vc = 'OP'
        test_obj.controller.event_switches = {0: sw}

        expected_msg = "Program: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
            test_obj.ds,
            "Program Paused:n",
            "Event biCoder has been triggered.n",
            "Program = ",
            str(test_obj.ad) + "n",
            "Event biCoder = ",
            str(helper_object._device_serial) + "n",
            "Limit/Value = ",
            str(helper_object._mode) + " / ",
            str(sw.vc)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_object)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_50(self):
        """ Test Build Message Title Pass Case 50: 3200 Message created for a program, status pause temperature sensor
        Create a mock test object
        Create the variables required to run the method
        Create a mock helper object and assign it variables then put it in the object bucket
        Create a mock temperature sensor and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.pause_temp_sensor

        helper_object = mock.MagicMock()
        helper_object._device_serial = 'D12345'
        helper_object._threshold = 123

        ts = mock.MagicMock()
        ts.sn = 'D12345'
        ts.vd = 456
        test_obj.controller.temperature_sensors = {0: ts}

        expected_msg = "Program: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
            test_obj.ds,
            "Program Paused:n",
            "Temperature limit has been reached.n",
            "Program = ",
            str(test_obj.ad) + "n",
            "Temperature biSensor = ",
            str(helper_object._device_serial) + "n",
            "Limit/Value = ",
            str(helper_object._threshold) + " / ",
            str(ts.vd)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_object)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_51(self):
        """ Test Build Message Title Pass Case 51: 3200 Message created for a program, status stop moisture sensor
        Create a mock test object
        Create the variables required to run the method
        Create a mock helper object and assign it variables then put it in the object bucket
        Create a mock moisture sensor and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.stop_moisture_sensor

        helper_object = mock.MagicMock()
        helper_object._device_serial = 'D12345'
        helper_object._threshold = 123

        ms = mock.MagicMock()
        ms.sn = 'D12345'
        ms.vp = 456
        test_obj.controller.moisture_sensors = {0: ms}

        expected_msg = "Program: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
            test_obj.ds,
            "Program Stopped:n",
            "Moisture limit has been reachedn",
            "Program = ",
            str(test_obj.ad) + "n",
            "Moisture biSensor = ",
            str(helper_object._device_serial) + "n",
            "Limit/Value = ",
            str(helper_object._threshold) + " / ",
            str(ms.vp)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_object)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_52(self):
        """ Test Build Message Title Pass Case 52: 3200 Message created for a program, status stop event switch
        Create a mock test object
        Create the variables required to run the method
        Create a mock helper object and assign it variables then put it in the object bucket
        Create a mock event switch and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.stop_event_switch

        helper_object = mock.MagicMock()
        helper_object._device_serial = 'D12345'
        helper_object._mode = 123

        sw = mock.MagicMock()
        sw.sn = 'D12345'
        sw.vc = 456
        test_obj.controller.event_switches = {0: sw}

        expected_msg = "Program: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
            test_obj.ds,
            "Program Stopped:n",
            "Event biCoder has been triggeredn",
            "Program = ",
            str(test_obj.ad) + "n",
            "Event biCoder = ",
            str(helper_object._device_serial) + "n",
            "Limit/Value = ",
            str(helper_object._mode) + " / ",
            str(sw.vc)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_object)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_53(self):
        """ Test Build Message Title Pass Case 53: 3200 Message created for a program, status stop temperature sensor
        Create a mock test object
        Create the variables required to run the method
        Create a mock helper object and assign it variables then put it in the object bucket
        Create a mock temperature sensor and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.stop_temp_sensor

        helper_object = mock.MagicMock()
        helper_object._device_serial = 'D12345'
        helper_object._threshold = 123

        ts = mock.MagicMock()
        ts.sn = 'D12345'
        ts.vd = 456
        test_obj.controller.temperature_sensors = {0: ts}

        expected_msg = "Program: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
            test_obj.ds,
            "Program Stopped:n",
            "Temperature limit has been reachedn",
            "Program = ",
            str(test_obj.ad) + "n",
            "Temperature biSensor = ",
            str(helper_object._device_serial) + "n",
            "Limit/Value = ",
            str(helper_object._threshold) + " / ",
            str(ts.vd)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_object)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_54(self):
        """ Test Build Message Title Pass Case 54: 3200 Message created for a program, status start moisture sensor
        Create a mock test object
        Create the variables required to run the method
        Create a mock helper object and assign it variables then put it in the object bucket
        Create a mock moisture sensor and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.started_moisture_sensor

        helper_object = mock.MagicMock()
        helper_object._device_serial = 'D12345'
        helper_object._threshold = 123

        ms = mock.MagicMock()
        ms.sn = 'D12345'
        ms.vp = 456
        test_obj.controller.moisture_sensors = {0: ms}

        expected_msg = "Program: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
            test_obj.ds,
            "Program Started:n",
            "Moisture limit has been reachedn",
            "Program = ",
            str(test_obj.ad) + "n",
            "Moisture biSensor = ",
            str(helper_object._device_serial) + "n",
            "Limit/Value = ",
            str(helper_object._threshold) + " / ",
            str(ms.vp)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_object)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_55(self):
        """ Test Build Message Title Pass Case 55: 3200 Message created for a program, status start event switch
        Create a mock test object
        Create the variables required to run the method
        Create a mock helper object and assign it variables then put it in the object bucket
        Create a mock event switch and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.started_event_switch

        helper_object = mock.MagicMock()
        helper_object._device_serial = 'D12345'
        helper_object._mode = 123

        sw = mock.MagicMock()
        sw.sn = 'D12345'
        sw.vc = 456
        test_obj.controller.event_switches = {0: sw}

        expected_msg = "Program: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
            test_obj.ds,
            "Program Started:n",
            "Event biCoder has been triggeredn",
            "Program = ",
            str(test_obj.ad) + "n",
            "Event biCoder = ",
            str(helper_object._device_serial) + "n",
            "Limit/Value = ",
            str(helper_object._mode) + " / ",
            str(sw.vc)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_object)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_56(self):
        """ Test Build Message Title Pass Case 56: 3200 Message created for a program, status start temperature sensor
        Create a mock test object
        Create the variables required to run the method
        Create a mock helper object and assign it variables then put it in the object bucket
        Create a mock temperature sensor and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.started_temp_sensor

        helper_object = mock.MagicMock()
        helper_object._device_serial = 'D12345'
        helper_object._threshold = 123

        ts = mock.MagicMock()
        ts.sn = 'D12345'
        ts.vd = 456
        test_obj.controller.temperature_sensors = {0: ts}

        expected_msg = "Program: {0}n{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
            test_obj.ds,
            "Program Started:n",
            "Temperature limit has been reachedn",
            "Program = ",
            str(test_obj.ad) + "n",
            "Temperature biSensor = ",
            str(helper_object._device_serial) + "n",
            "Limit/Value = ",
            str(helper_object._threshold) + " / ",
            str(ts.vd)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=helper_object)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_57(self):
        """ Test Build Message Title Pass Case 57: 3200 Message created for a temperature sensor, status bad serial
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = Message.bad_serial

        expected_msg = "Temperature biSensor: {0}n{1}{2}{3}{4}".format(
            test_obj.ds,
            "Device Error:n",
            "Wrong device at this address.n",
            "Device = ",
            test_obj.sn
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.temperature_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_58(self):
        """ Test Build Message Title Pass Case 58: 3200 Message created for a temperature sensor, status no response
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = Message.no_response

        expected_msg = "Temperature biSensor: {0}n{1}{2}{3}{4}".format(
            test_obj.ds,
            "Two-wire Communication Failure:n",
            "Unable to talk with this device.n",
            "Device = ",
            test_obj.sn
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.temperature_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_59(self):
        """ Test Build Message Title Pass Case 59: 3200 Message created for a zone, status bad serial
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = Message.bad_serial

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}".format(
            test_obj.ad,
            test_obj.ds,
            "Device Error:n",
            "Wrong device at this address.n",
            "Device = ",
            str(test_obj.sn)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_60(self):
        """ Test Build Message Title Pass Case 60: 3200 Message created for a zone, status no 24 vac
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = Message.no_24_vac

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}".format(
            test_obj.ad,
            test_obj.ds,
            "Powered Valve biCoder Failure:n",
            "Missing 24 VAC.n",
            "Unable to operate this valve.n",
            "Zone = ",
            str(test_obj.ad)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)


    #################################
    def test_build_message_title_pass_61(self):
        """ Test Build Message Title Pass Case 61: 3200 Message created for a zone, status no response
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = Message.no_response

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}".format(
            test_obj.ad,
            test_obj.ds,
            "Two-wire Communication Failure:n",
            "Unable to talk with this device.n",
            "Device = ",
            str(test_obj.sn)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_62(self):
        """ Test Build Message Title Pass Case 62: 3200 Message created for a zone, status open circuit
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = Message.open_circuit

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}{7}{8}".format(
            test_obj.ad,
            test_obj.ds,
            "Valve Failure:n",
            "Open Circuit Solenoid.n",
            "Unable to operate this valve.n",
            "Zone = ",
            str(test_obj.ad) + "n",
            "biCoder = ",
            str(test_obj.sn) + "n"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_63(self):
        """ Test Build Message Title Pass Case 63: 3200 Message created for a zone, status short circuit
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = Message.short_circuit

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}{7}{8}".format(
            test_obj.ad,
            test_obj.ds,
            "Valve Failure:n",
            "Short Circuit Solenoid.n",
            "Unable to operate this valve.n",
            "Zone = ",
            str(test_obj.ad) + "n",
            "biCoder = ",
            str(test_obj.sn) + "n"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_64(self):
        """ Test Build Message Title Pass Case 64: 3200 Message created for a zone program, status calibrate failure no change
        Create a mock test object
        Create the variables required to run the method
        Create a mock moisture sensor and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ad = 1
        test_obj.zone.ds = 'Testing description'
        test_obj.ms = 1
        ct_type = opcodes.zone_program
        status_code = Message.calibrate_failure_no_change

        ms = mock.MagicMock()
        ms.ad = 1
        ms.sn = 'D12345'
        ms.vp = 456
        test_obj.controller.moisture_sensors = {0: ms}

        # message = 'Zone 1: Testing descriptionn
        # Moisture biSensor Calibration Failed:n
        # Moisture did not increase.nPrimary Zone = 1n
        # Moisture biSensor = D12345nMoisture = 456'

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}{7}{8}{9}".format(
            test_obj.zone.ad,  # {0} Zone address
            test_obj.zone.ds,  # zone description
            "Moisture biSensor Calibration Failed:n",
            "Moisture did not increase.n",
            "Primary Zone = ",
            str(test_obj.zone.ad) + "n",
            "Moisture biSensor = ",
            str(ms.sn) + "n",
            "Moisture = ",
            str(ms.vp)           # moisture percentage returns from moisture sensor
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=ct_type,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_65(self):
        """ Test Build Message Title Pass Case 65: 3200 Message created for a zone program, status calibrate successful
        Create a mock test object
        Create the variables required to run the method
        Create a mock moisture sensor and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ad = 1
        test_obj.zone.ds = 'Testing description'
        test_obj.ms = 1
        ct_type = opcodes.zone_program
        test_obj.ll = 10
        test_obj.ul = 20
        status_code = Message.calibrate_successful

        ms = mock.MagicMock()
        ms.ad = 1
        ms.sn = 'D12345'
        ms.vp = 456
        test_obj.controller.moisture_sensors = {0: ms}

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}".format(
            test_obj.zone.ad,  # {0} Zone address
            test_obj.zone.ds,  # zone description
            "Moisture biSensor Calibration Done:n",
            "New watering limits set.n",
            "Primary Zone = ",
            str(test_obj.zone.ad) + "n",
            "Moisture biSensor = ",
            str(ms.sn) + "n",
            "Limit Lower/Upper = ",
            str(test_obj.ll) + "n",
            " / ",
            str(test_obj.ul)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=ct_type,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_66(self):
        """ Test Build Message Title Pass Case 66: 3200 Message created for a zone program, status calibrate failure no saturation
        Create a mock test object
        Create the variables required to run the method
        Create a mock moisture sensor and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ad = 1
        test_obj.zone.ds = 'Testing description'
        test_obj.ms = 1
        ct_type = opcodes.zone_program
        status_code = Message.calibrate_failure_no_saturation

        ms = mock.MagicMock()
        ms.ad = 1
        ms.sn = 'D12345'
        ms.vp = 456
        test_obj.controller.moisture_sensors = {0: ms}

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}{7}{8}{9}".format(
            test_obj.zone.ad,  # {0} Zone address
            test_obj.zone.ds,  # zone description
            "Moisture biSensor Calibration Failed:n",
            "Did not reach Saturation.n",
            "Primary Zone = ",
            str(test_obj.zone.ad) + "n",
            "Moisture biSensor = ",
            str(ms.sn) + "n",
            "Moisture = ",
            str(ms.vp) + "n"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=ct_type,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_67(self):
        """ Test Build Message Title Pass Case 67: 3200 Message created for a zone program, status requires soak cycle
        Create a mock test object
        Create the variables required to run the method
        Create a mock moisture sensor and assign it variables then put it in the object bucket
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ad = 1
        test_obj.zone.ds = 'Testing description'
        test_obj.ms = 1
        ct_type = opcodes.zone_program
        status_code = Message.requires_soak_cycle

        ms = mock.MagicMock()
        ms.ad = 1
        ms.sn = 'D12345'
        ms.vp = 456
        test_obj.controller.moisture_sensors = {0: ms}

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}{7}{8}{9}".format(
            test_obj.zone.ad,   # {0} Zone address
            test_obj.zone.ds,   # zone description
            "Moisture biSensor Requirement:n",
            "Soak Cycles must be used.n",
            "Primary Zone = ",
            str(test_obj.zone.ad) + "n",
            "Program = ",
            str(test_obj.program.ad) + "n",
            "Moisture biSensor = ",
            str(ms.sn)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=ct_type,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_68(self):
        """ Test Build Message Title Pass Case 68: 3200 Message created for a zone program, status exceeds design flow
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ad = 1
        test_obj.zone.ds = 'Testing description'
        test_obj.zone.df = 20
        status_code = Message.exceeds_design_flow

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}{7}{8}".format(
            test_obj.zone.ad,    # {0} Zone address
            test_obj.zone.ds,    # {1} Zone description
            "Zone High Flow:n",
            "Zone design flow exceeds limit.n",
            "It will be run by itself.n",
            "Zone = ",
            str(test_obj.zone.ad) + "n",
            "Design Flow = ",
            str(test_obj.zone.df)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone_program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_69(self):
        """ Test Build Message Title Pass Case 69: 3200 Message created for a zone program, status flow learn errors
        the message is "the zone fail to learn flow":

        - Tests that we receive the correct message from our messages module after encountering a learn flow errors
          message
        - Need to have poc and fm's mocked out so the messages module can grab the needed information for constructing
          the correct message
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ds = 'Testing description'
        test_obj.df = 20
        test_obj.program.ml = 1
        test_obj.zone.ad = 1

        poc = mock.MagicMock()
        poc.ad = 1
        poc.ml = 1
        poc.fm = 1
        test_obj.controller.points_of_control = {0: poc}

        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'D12345'
        fm.vr = 30
        test_obj.controller.flow_meters = {0: fm}
        status_code = Message.flow_learn_errors

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}{7}".format(
            test_obj.zone.ad,    # {0} Zone address
            test_obj.zone.ds,    # {1} Zone description
            "Zone Learn Flow Done:n",
            "This zone had errors.n",
            "Zone = ",
            str(test_obj.zone.ad) + "n",
            "Learn Flow = ",
            str(fm.vr)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone_program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_70(self):
        """ Test Build Message Title Pass Case 70: 3200 Message created for a zone program, status flow learn ok:

         - Tests that we receive the correct message from our messages module after encountering a learn flow errors
          message
        - Need to have poc and fm's mocked out so the messages module can grab the needed information for constructing
          the correct message
        """

        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ds = 'Testing description'
        test_obj.df = 20
        test_obj.program.ml = 1
        test_obj.zone.ad = 1

        poc = mock.MagicMock()
        poc.ad = 1
        poc.ml = 1
        poc.fm = 1
        test_obj.controller.points_of_control = {0: poc}

        fm = mock.MagicMock()
        fm.ad = 1
        fm.sn = 'D12345'
        fm.vr = 30
        test_obj.controller.flow_meters = {0: fm}

        status_code = Message.flow_learn_ok

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}{7}".format(
            test_obj.zone.ad,    # {0} Zone address
            test_obj.zone.ds,    # {1} Zone description
            "Zone Learn Flow Done:n",
            "Completed successfully.n",
            "Zone = ",
            str(test_obj.zone.ad) + "n",
            "Learn Flow = ",
            str(fm.vr)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone_program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_71(self):
        """ Test Build Message Title Pass Case 71: 3200 Message created for a zone program, status high flow shutdown by flow station
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ad = 1
        test_obj.zone.ds = 'Testing description'
        test_obj.zone.df = 20
        status_code = Message.high_flow_shutdown_by_flow_station

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}".format(
            test_obj.zone.ad,    # {0} Zone address
            test_obj.zone.ds,    # {1} Zone description
            "FlowStation High Flow:n",
            "Measured flow exceeds limit.n",
            "Zone has been shut down.n",
            "Zone = ",
            str(test_obj.zone.ad) + "n"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone_program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_72(self):
        """ Test Build Message Title Pass Case 72: 3200 Message created for a zone program, status high flow variance shutdown(zone has a design flow)
        Create a mock test object
        Create the variables required to run the method
        Create a mock poc object with mainline and flow meter attributes
        Create a mock flow meter object and give it address and  vr attributes
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ad = 1
        test_obj.zone.ds = 'Testing description'
        test_obj.zone.df = 20
        test_obj.program.ml = 1
        status_code = Message.high_flow_variance_shutdown

        poc = mock.MagicMock()
        poc.ml = 1
        poc.fm = 1
        test_obj.controller.points_of_control = {0: poc}

        fm = mock.MagicMock()
        fm.ad = 1
        fm.vr = 10
        test_obj.controller.flow_meters = {0: fm}

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}{7}{8}{9}{10}".format(
            test_obj.zone.ad,    # {0} Zone address
            test_obj.zone.ds,    # {1} Zone description
            "Zone High Flow Variance:n",
            "Measured flow exceeds limit.n",
            "Zone has been shut down.n",
            "Zone = ",
            str(test_obj.zone.ad) + "n",
            "Limit/Value = ",
            str(test_obj.zone.df),
            " / ",
            str(fm.vr)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone_program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_73(self):
        """ Test Build Message Title Pass Case 73: 3200 Message created for a zone program, status high flow variance shutdown(zone doesn't have a design flow)
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ad = 1
        test_obj.zone.ds = 'Testing description'
        test_obj.zone.df = 0
        test_obj.program.ml = 1
        status_code = Message.high_flow_variance_shutdown

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}{7}".format(
            test_obj.zone.ad,    # {0} Zone address
            test_obj.zone.ds,    # {1} Zone description
            "Zone High Flow Variance:n",
            "Measured flow exceeds limit.n",
            "Zone has been shut down.n",
            "Zone = ",
            str(test_obj.zone.ad) + "n",
            "Limit = "
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone_program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_74(self):
        """ Test Build Message Title Pass Case 74: 3200 Message created for a zone program, status low flow variance shutdown(zone has a design flow)
        Create a mock test object
        Create the variables required to run the method
        Create a mock poc object and give it mainline and flow meter variables
        Create a mock flow meter object and give it variables
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ad = 1
        test_obj.zone.ds = 'Testing description'
        test_obj.zone.df = 20
        test_obj.program.ml = 1
        status_code = Message.low_flow_variance_shutdown

        poc = mock.MagicMock()
        poc.ml = 1
        poc.fm = 1
        test_obj.controller.points_of_control = {0: poc}

        fm = mock.MagicMock()
        fm.ad = 1
        fm.vr = 10
        test_obj.controller.flow_meters = {0: fm}

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}{7}{8}{9}{10}".format(
            test_obj.zone.ad,    # {0} Zone address
            test_obj.zone.ds,    # {1} Zone description
            "Zone Low Flow Variance:n",
            "Measured flow is below lower limit.n",
            "Zone has been shut down.n",
            "Zone = ",
            str(test_obj.zone.ad) + "n",
            "Limit/Value = ",
            str(test_obj.zone.df),
            " / ",
            str(fm.vr)
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone_program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_75(self):
        """ Test Build Message Title Pass Case 75: 3200 Message created for a zone program, status low flow variance shutdown(zone doesn't have a design flow)
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ad = 1
        test_obj.zone.ds = 'Testing description'
        test_obj.zone.df = 0
        test_obj.program.ml = 1
        status_code = Message.low_flow_variance_shutdown

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}{7}".format(
            test_obj.zone.ad,    # {0} Zone address
            test_obj.zone.ds,    # {1} Zone description
            "Zone Low Flow Variance:n",
            "Measured flow is below lower limit.n",
            "Zone has been shut down.n",
            "Zone = ",
            str(test_obj.zone.ad) + "n",
            "Limit = "
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone_program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_76(self):
        """ Test Build Message Title Pass Case 76: 3200 Message created for a zone program, status low flow shutdown by flow station
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.zone.ad = 1
        test_obj.zone.ds = 'Testing description'
        status_code = Message.low_flow_shutdown_by_flow_station

        expected_msg = "Zone {0}: {1}n{2}{3}{4}{5}{6}".format(
            test_obj.zone.ad,    # {0} Zone address
            test_obj.zone.ds,    # {1} Zone description
            "FlowStation Low Flow:n",
            "Measured flow is below lower limit.n",
            "Zone has been shut down.n",
            "Zone = ",
            str(test_obj.zone.ad) + "n"
        )

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone_program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_77(self):
        """ Test Build Message Title Pass Case 77: 1000 Message created for a controller, status two wire over current
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        status_code = Message.two_wire_over_current

        expected_msg = "Two-Wire Over Current/r/nOld ETo Used"

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.controller,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_78(self):
        """ Test Build Message Title Pass Case 78: 1000 Message created for an event switch, status checksum
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.checksum

        expected_msg = "Event Switch {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Comm Error")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.event_switch,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_79(self):
        """ Test Build Message Title Pass Case 79: 1000 Message created for an event switch, status no response
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.no_response

        expected_msg = "Event Switch {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "No Reply")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.event_switch,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_80(self):
        """ Test Build Message Title Pass Case 80: 1000 Message created for an event switch, status bad serial
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.bad_serial

        expected_msg = "Event Switch {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Serial N. Mismatch")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.event_switch,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_81(self):
        """ Test Build Message Title Pass Case 81: 1000 Message created for a flow meter, status checksum
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.checksum

        expected_msg = "Flow Sensor {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Comm Error")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.flow_meter,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_82(self):
        """ Test Build Message Title Pass Case 82: 1000 Message created for a flow meter, status no response
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.no_response

        expected_msg = "Flow Sensor {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "No Reply")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.flow_meter,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_83(self):
        """ Test Build Message Title Pass Case 83: 1000 Message created for a flow meter, status bad serial
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.bad_serial

        expected_msg = "Flow Sensor {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Serial N. Mismatch")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.flow_meter,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_84(self):
        """ Test Build Message Title Pass Case 84: 1000 Message created for a moisture sensor, status checksum
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.checksum

        expected_msg = "Moisture Sensor {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Comm Error")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.moisture_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_85(self):
        """ Test Build Message Title Pass Case 85: 1000 Message created for a moisture sensor, status no response
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.no_response

        expected_msg = "Moisture Sensor {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "No Reply")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.moisture_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_86(self):
        """ Test Build Message Title Pass Case 86: 1000 Message created for an moisture sensor, status bad serial
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.bad_serial

        expected_msg = "Moisture Sensor {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Serial N. Mismatch")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.moisture_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_87(self):
        """ Test Build Message Title Pass Case 87: 1000 Message created for a master valve, status checksum
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.checksum

        expected_msg = "MV/Pump {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Comm Error")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.master_valve,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_88(self):
        """ Test Build Message Title Pass Case 88: 1000 Message created for a master valve, status no response
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.no_response

        expected_msg = "MV/Pump {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "No Reply")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.master_valve,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_89(self):
        """ Test Build Message Title Pass Case 89: 1000 Message created for a master valve, status bad serial
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.bad_serial

        expected_msg = "MV/Pump {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Serial N. Mismatch")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.master_valve,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_90(self):
        """ Test Build Message Title Pass Case 90: 1000 Message created for a master valve, status low voltage
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.low_voltage

        expected_msg = "MV/Pump {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Voltage Too Low")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.master_valve,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_91(self):
        """ Test Build Message Title Pass Case 91: 1000 Message created for a master valve, status open circuit
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.open_circuit_1000

        expected_msg = "MV/Pump {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Solenoid Open")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.master_valve,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_92(self):
        """ Test Build Message Title Pass Case 92: 1000 Message created for a master valve, status short circuit
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.short_circuit_1000

        expected_msg = "MV/Pump {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Solenoid Short")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.master_valve,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_93(self):
        """ Test Build Message Title Pass Case 93: 1000 Message created for a poc, status device error
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.device_error

        expected_msg = "Water Source WS-{0}n{1}".format(test_obj.ad, "Device Error")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.point_of_connection,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_94(self):
        """ Test Build Message Title Pass Case 94: 1000 Message created for a poc, status high flow shutdown
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.high_flow_shutdown

        expected_msg = "Water Source WS-{0}n{1}".format(test_obj.ad, "High Flow Fault")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.point_of_connection,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_95(self):
        """ Test Build Message Title Pass Case 95: 1000 Message created for a poc, status unscheduled flow shutdown
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.unscheduled_flow_shutdown

        expected_msg = "Water Source WS-{0}n{1}".format(test_obj.ad, "Unexpected Flow Fault")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.point_of_connection,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_96(self):
        """ Test Build Message Title Pass Case 96: 1000 Message created for a temperature sensor, status checksum
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.checksum

        expected_msg = "Temp. Sensor {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Comm Error")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.temperature_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_97(self):
        """ Test Build Message Title Pass Case 97: 1000 Message created for a temperature sensor, status no response
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.no_response

        expected_msg = "Temp. Sensor {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "No Reply")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.temperature_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_98(self):
        """ Test Build Message Title Pass Case 98: 1000 Message created for a temperature sensor, status bad serial
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.bad_serial

        expected_msg = "Temp. Sensor {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Serial N. Mismatch")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.temperature_sensor,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_99(self):
        """ Test Build Message Title Pass Case 99: 1000 Message created for a zone, status checksum
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.checksum

        expected_msg = "Zone {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Comm Error")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_100(self):
        """ Test Build Message Title Pass Case 100: 1000 Message created for a zone, status no response
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.no_response

        expected_msg = "Zone {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "No Reply")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_101(self):
        """ Test Build Message Title Pass Case 101: 1000 Message created for a zone, status bad serial
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.bad_serial

        expected_msg = "Zone {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Serial N. Mismatch")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_102(self):
        """ Test Build Message Title Pass Case 102: 1000 Message created for a zone, status low voltage
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.low_voltage

        expected_msg = "Zone {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Voltage Too Low")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_103(self):
        """ Test Build Message Title Pass Case 103: 1000 Message created for a zone, status open circuit
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.open_circuit_1000

        expected_msg = "Zone {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Solenoid Open")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_104(self):
        """ Test Build Message Title Pass Case 104: 1000 Message created for a zone, status short circuit
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.short_circuit_1000

        expected_msg = "Zone {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "Solenoid Short")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_105(self):
        """ Test Build Message Title Pass Case 105: 1000 Message created for a zone, status high flow variance shutdown
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        status_code = Message.high_flow_variance_shutdown

        expected_msg = "Zone {0}-{1}n{2}".format(test_obj.ad, test_obj.sn, "High Flow Variance")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.zone,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_106(self):
        """ Test Build Message Title Pass Case 106: 1000 Message created for a program, status over run start event
        Create a mock test object
        Create the variables required to run the method
        Mock the controller date time object in the date manager so we can control what it's methods will return
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        status_code = Message.over_run_start_event

        mock_controller_dt = mock.MagicMock()
        mock_controller_dt.time_obj.obj.strftime = mock.MagicMock(return_value='7:15PM')
        mock_controller_dt.formatted_date_string = mock.MagicMock(return_value='June 29')
        mock_controller_dt.formatted_time_string = mock.MagicMock(return_value='7:15PM')
        date_mngr.controller_datetime = mock_controller_dt

        expected_msg = "Program {0}n{1}".format(test_obj.ad, "Status: Program Overrun June 29, 7:15P")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_107(self):
        """ Test Build Message Title Pass Case 107: 1000 Message created for a program, status learn flow with errors
        Create a mock test object
        Create the variables required to run the method
        Mock the controller date time object in the date manager so we can control what it's methods will return
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        status_code = Message.learn_flow_with_errors

        mock_controller_dt = mock.MagicMock()
        mock_controller_dt.time_obj.obj.strftime = mock.MagicMock(return_value='7:15PM')
        mock_controller_dt.formatted_date_string = mock.MagicMock(return_value='June 29')
        mock_controller_dt.formatted_time_string = mock.MagicMock(return_value='7:15PM')
        date_mngr.controller_datetime = mock_controller_dt

        expected_msg = "Program {0}n{1}".format(test_obj.ad, "Status: Learn Flow Failed June 29, 7:15P")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_108(self):
        """ Test Build Message Title Pass Case 108: 1000 Message created for a program, status calibrate failure no change
        Create a mock test object
        Create the variables required to run the method
        Mock the controller date time object in the date manager so we can control what it's methods will return
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        status_code = Message.calibrate_failure_no_change

        mock_controller_dt = mock.MagicMock()
        mock_controller_dt.time_obj.obj.strftime = mock.MagicMock(return_value='7:15PM')
        mock_controller_dt.formatted_date_string = mock.MagicMock(return_value='June 29')
        mock_controller_dt.formatted_time_string = mock.MagicMock(return_value='7:15PM')
        date_mngr.controller_datetime = mock_controller_dt

        expected_msg = "Program {0}n{1}".format(test_obj.ad, "Status: Calibration Failed June 29, 7:15P")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_109(self):
        """ Test Build Message Title Pass Case 109: 1000 Message created for a program, status start
        Create a mock test object
        Create the variables required to run the method
        Mock the controller date time object in the date manager so we can control what it's methods will return
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        status_code = Message.start

        mock_controller_dt = mock.MagicMock()
        mock_controller_dt.time_obj.obj.strftime = mock.MagicMock(return_value='7:15PM')
        mock_controller_dt.formatted_date_string = mock.MagicMock(return_value='June 29')
        mock_controller_dt.formatted_time_string = mock.MagicMock(return_value='7:15PM')
        date_mngr.controller_datetime = mock_controller_dt

        expected_msg = "Program {0}n{1}".format(
            test_obj.ad,
            "Status: Not Tested Yet June 29, 7:15PnStart: User June 29, 7:15P")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=opcodes.user)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_110(self):
        """ Test Build Message Title Pass Case 110: 1000 Message created for a program, status pause
        Create a mock test object
        Create the variables required to run the method
        Mock the controller date time object in the date manager so we can control what it's methods will return
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        status_code = Message.pause

        mock_controller_dt = mock.MagicMock()
        mock_controller_dt.time_obj.obj.strftime = mock.MagicMock(return_value='7:15PM')
        mock_controller_dt.formatted_date_string = mock.MagicMock(return_value='June 29')
        mock_controller_dt.formatted_time_string = mock.MagicMock(return_value='7:15PM')
        date_mngr.controller_datetime = mock_controller_dt

        expected_msg = "Program {0}n{1}".format(
            test_obj.ad,
            "Status: Not Tested Yet June 29, 7:15PnPause: Event Day June 29, 7:15P")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=opcodes.event_day)

        self.assertEqual(expected_msg, actual_msg)

    #################################
    def test_build_message_title_pass_111(self):
        """ Test Build Message Title Pass Case 111: 1000 Message created for a program, status stop
        Create a mock test object
        Create the variables required to run the method
        Mock the controller date time object in the date manager so we can control what it's methods will return
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '10'
        test_obj.ad = 1
        status_code = Message.stop

        mock_controller_dt = mock.MagicMock()
        mock_controller_dt.time_obj.obj.strftime = mock.MagicMock(return_value='07:15PM')
        mock_controller_dt.formatted_date_string = mock.MagicMock(return_value='June 29')
        mock_controller_dt.formatted_time_string = mock.MagicMock(return_value='07:15PM')
        date_mngr.controller_datetime = mock_controller_dt

        expected_msg = "Program {0}n{1}".format(
            test_obj.ad,
            "Status: Not Tested Yet June 29, 7:15PnStop: User June 29, 7:15P")

        actual_msg = messages.build_message_title(_object=test_obj,
                                                  _ct_type=opcodes.program,
                                                  _status_code=status_code,
                                                  _helper_object=opcodes.user)

        self.assertEqual(expected_msg, actual_msg)


    # TODO Fails

    #################################
    def test_build_message_title_fail_1(self):
        """ Test Build Message Title Fail 1: 3200 Message created for a controller, status pause event switch without an event switch in the object bucket
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.pause_event_switch

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "EventSwitch serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.controller,
                                         _status_code=status_code,
                                         _helper_object=None)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_2(self):
        """ Test Build Message Title Fail 2: 3200 Message created for a controller, status pause event switch without a moisture sensor in the object bucket
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.pause_moisture_sensor

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "MoistureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.controller,
                                         _status_code=status_code,
                                         _helper_object=None)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_3(self):
        """ Test Build Message Title Fail 3: 3200 Message created for a controller, status pause event switch without a temperature sensor in the object bucket
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.pause_temp_sensor

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "TemperatureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.controller,
                                         _status_code=status_code,
                                         _helper_object=None)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_4(self):
        """ Test Build Message Title Fail 1: 3200 Message created for a controller, status stop event switch without an event switch in the object bucket
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.stop_event_switch

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "EventSwitch serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.controller,
                                         _status_code=status_code,
                                         _helper_object=None)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_5(self):
        """ Test Build Message Title Fail 5: 3200 Message created for a controller, status stop moisture sensor without a moisture sensor in the object bucket
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.stop_moisture_sensor

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "MoistureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.controller,
                                         _status_code=status_code,
                                         _helper_object=None)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_6(self):
        """ Test Build Message Title Fail 6: 3200 Message created for a controller, status pause temp sensor without a temperature sensor in the object bucket
        Create a mock test object
        Create the variables required to run the method
        Create an TX message that we are expecting to be returned
        Call the method and store the message that it returns
        Make an assertion that our expected is equal to what the method returned
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.sn = 'D00000'
        test_obj.ds = 'Testing description'
        status_code = opcodes.stop_temp_sensor

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "TemperatureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.controller,
                                         _status_code=status_code,
                                         _helper_object=None)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_7(self):
        """ Test Build Message Title Fail Case 7: 3200 Message created for a program, status event date stop
        Create a mock test object
        Create the variables required to run the method
        Create an error message that we are expecting to be returned
        Call the method and expect a ValueError to be thrown
        Make an assertion that our expected message is the same as the actual error message that was raised
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.skipped_by_moisture_sensor

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "MoistureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.program,
                                         _status_code=status_code)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_8(self):
        """ Test Build Message Title Fail Case 8: 3200 Message created for a program, status pause moisture sensor
        Create a mock test object
        Create the variables required to run the method
        Create an error message that we are expecting to be returned
        Call the method and expect a ValueError to be thrown
        Make an assertion that our expected message is the same as the actual error message that was raised
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.pause_moisture_sensor

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "MoistureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.program,
                                         _status_code=status_code)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_9(self):
        """ Test Build Message Title Fail Case 9: 3200 Message created for a program, status pause event switch
        Create a mock test object
        Create the variables required to run the method
        Create an error message that we are expecting to be returned
        Call the method and expect a ValueError to be thrown
        Make an assertion that our expected message is the same as the actual error message that was raised
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.pause_event_switch

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "EventSwitch serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.program,
                                         _status_code=status_code)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_10(self):
        """ Test Build Message Title Fail Case 10: 3200 Message created for a program, status pause temperature sensor
        Create a mock test object
        Create the variables required to run the method
        Create an error message that we are expecting to be returned
        Call the method and expect a ValueError to be thrown
        Make an assertion that our expected message is the same as the actual error message that was raised
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.pause_temp_sensor

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "TemperatureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.program,
                                         _status_code=status_code)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_11(self):
        """ Test Build Message Title Fail Case 11: 3200 Message created for a program, status stop moisture sensor
        Create a mock test object
        Create the variables required to run the method
        Create an error message that we are expecting to be returned
        Call the method and expect a ValueError to be thrown
        Make an assertion that our expected message is the same as the actual error message that was raised
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.stop_moisture_sensor

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "MoistureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.program,
                                         _status_code=status_code)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_12(self):
        """ Test Build Message Title Fail Case 12: 3200 Message created for a program, status stop event switch
        Create a mock test object
        Create the variables required to run the method
        Create an error message that we are expecting to be returned
        Call the method and expect a ValueError to be thrown
        Make an assertion that our expected message is the same as the actual error message that was raised
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.stop_event_switch

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "EventSwitch serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.program,
                                         _status_code=status_code)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_13(self):
        """ Test Build Message Title Fail Case 13: 3200 Message created for a program, status stop temperature sensor
        Create a mock test object
        Create the variables required to run the method
        Create an error message that we are expecting to be returned
        Call the method and expect a ValueError to be thrown
        Make an assertion that our expected message is the same as the actual error message that was raised
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.stop_temp_sensor

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "TemperatureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.program,
                                         _status_code=status_code)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_14(self):
        """ Test Build Message Title Fail Case 14: 3200 Message created for a program, status start moisture sensor
        Create a mock test object
        Create the variables required to run the method
        Create an error message that we are expecting to be returned
        Call the method and expect a ValueError to be thrown
        Make an assertion that our expected message is the same as the actual error message that was raised
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.started_moisture_sensor

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "MoistureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.program,
                                         _status_code=status_code)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_15(self):
        """ Test Build Message Title Fail Case 15: 3200 Message created for a program, status start event switch
        Create a mock test object
        Create the variables required to run the method
        Create an error message that we are expecting to be returned
        Call the method and expect a ValueError to be thrown
        Make an assertion that our expected message is the same as the actual error message that was raised
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.started_event_switch

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "EventSwitch serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.program,
                                         _status_code=status_code)

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_build_message_title_fail_16(self):
        """ Test Build Message Title Fail Case 16: 3200 Message created for a program, status start temperature sensor
        Create a mock test object
        Create the variables required to run the method
        Create an error message that we are expecting to be returned
        Call the method and expect a ValueError to be thrown
        Make an assertion that our expected message is the same as the actual error message that was raised
        """
        test_obj = mock.MagicMock()
        test_obj.controller_type = '32'
        test_obj.ad = 1
        test_obj.ds = 'Testing description'
        status_code = Message.started_temp_sensor

        expected_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                       "TemperatureSensor serial number. This can be done with the 'set' methods."

        with self.assertRaises(ValueError) as context:
            messages.build_message_title(_object=test_obj,
                                         _ct_type=opcodes.program,
                                         _status_code=status_code)

        self.assertEqual(expected_msg, context.exception.message)


if __name__ == "__main__":
    unittest.main()

