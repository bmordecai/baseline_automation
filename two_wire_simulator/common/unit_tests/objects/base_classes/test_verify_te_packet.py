from unittest import TestCase
import common.objects.base_classes.verify_te_packet as packet_verifier


class TestVerifyTePacket(TestCase):

    def setUp(self):
        """ Setting up for the test. """
        # mock items

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_verify_packet_for_dg_302_single_moisture_sensor(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = "0000000004A37650540112TE^DG:302/ID=100/SQ=0/MX=" \
                 "^MS:SB100005/DS=SB100005 test moisture sensor"
        packet +="/LA=43.61870/LG=116.2146/EN=TR^SL:TR^DN:DN"
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_302_multiple_moisture_sensors(self):
        """ This test verifies that a list of key value objects can be created from a packet string """
        packet =  "0000000008EE4081150272TE^DG:302/ID=12345/SQ=0/MX=25"
        packet += "^MS:SB05308/DS=Test Moisture Sensor SB05308/EN=TR/LA=43.609773/LG=-116.310064"
        packet += "^MS:SB06300/DS=Test Moisture Sensor SB06300/EN=TR/LA=43.609778/LG=-116.310059"
        packet += "^MS:SB07258/DS=Test Moisture Sensor SB07258/EN=TR/LA=43.609783/LG=-116.310054"
        packet += "^SL:TR^DN:DN"
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(3, len(packet_key_values))

    def test_verify_packet_for_dg_302_empty_results(self):
        """ This test verifies that an empty result list throws an exception """
        packet = "000000000CC67BC7520038TE^DG:302/ID=541/SQ=0/MX=8^SL:TR^DN:DN"

        expected_message = "verify_te_packet: data packet 302 contains no data for parameter: MS"

        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_302_empty_results_dn_dn_missing(self):
        """ This test verifies that an exception will be thrown if the parameter ^DN is not in the packet string """
        packet = "000000000CC67BC7520038TE^DG:302/ID=541/SQ=0/MX=8^SL:TR"
        expected_message = "verify_te_packet: ^DN not detected."
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_302_empty_results_sl_missing(self):
        """ This test verifies that an exception will be thrown if the parameter ^SL is not in the packet string """
        packet = "000000000CC67BC7520038TE^DG:302/ID=541/SQ=0/MX=8^DN:DN"
        expected_message = "verify_te_packet: ^SL not detected."
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_999_invalid_data_group(self):
        """ This test verifies an exception when there is no method in verify_te_packet to handle the data group """
        expected_message = "verify_te_packet error: DG 999 is under construction"
        packet = "000000000CC67BC7520038TE^DG:999/ID=541/SQ=0/MX=8^SL:TR^DN:DN"
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_302_invalid_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid property: XX"
        packet = "0000000004A37650540112TE^DG:302/ID=100/SQ=0/MX=^MS:SB100005"
        packet +="/DS=SB100005 test moisture sensor/LA=43.61870/LG=116.2146/EN=TR/XX=FA^SL:TR^DN:DN"
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_302_invalid_command_opcode(self):
        """ This test verifies that verify_te_packet.py will only check a packet if it is of type TE. """
        expected_message = "verify_te_packet: Invalid opcode - TS. Should be 'TE' for packet verifier"
        packet = "0000000004A37650540112TS^DG:302/ID=100/SQ=0/MX=^MS:SB100005/DS=SB100005 test moisture sensor"
        packet +="/LA=43.61870/LG=116.2146/EN=TR^SL:TR^DN:DN"
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_parameter_missing(self):
        """ This test verifies that verify_te_packet.py will throw an exception if the dg parameter is missing """
        expected_message = "verify_te_packet: Error - first parameter is not 'DG'"
        packet = "0000000004A37650540112TE^ID=100/SQ=0/MX=^MS:SB100005/DS=SB100005 test moisture sensor"
        packet +="/LA=43.61870/LG=116.2146/EN=TR^SL:TR^DN:DN"
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_empty_parameter_list_empty(self):
        """ This test verifies that an exception will be thrown if there is no value list for the dg parameter """
        expected_message = "verify_te_packet: Error - first parameter value list is EMPTY"
        packet = "0000000004A37650540112TE^DG:/ID=100/SQ=0/MX=^MS:SB100005/DS=SB100005 test moisture sensor"
        packet +="/LA=43.61870/LG=116.2146/EN=TR^SL:TR^DN:DN"
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_empty_parameters_list(self):
        """ This test verifies that an exception will be thrown if there are no parameters in the data group """
        expected_message = "verify_te_packet: Invalid packet: key_values_dictionary is NONE"
        packet = "0000000004A37650540112TE^"

        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_302_invalid_parameter_id(self):
            """ This test verifies that a list of key value objects can be created from a packet string"""
            packet = "0000000004A37650540112TE^DG:302/ID=100/SQ=0/MX=^TS:SB100005/DS=SB100005 test moisture sensor"
            packet +="/LA=43.61870/LG=116.2146/EN=TR^SL:TR^DN:DN"

            expected_message = "verify_te_packet: Invalid parameter ID: TS"
            with self.assertRaises(Exception) as context:
                packet_key_values = packet_verifier.verify_packet(packet=packet)
            self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_332_single_flowstation_settings(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = "000000000CC67BC7520070TE^DG:332/ID=549/SQ=0^FS:1/DS=Flowstation 1/EN=TR/IP=10.11.12.36^DN:DN"
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_332_multiple_flowstation_settings(self):
        """ This test verifies that a list of key value objects can be created from a packet string """
        packet =  "000000000CC67BC7520500TE^DG:332/ID=549/SQ=0"
        packet += "^FS:1/DS=Flowstation 1/EN=TR/IP=10.11.12.36"
        packet += "^FS:2/DS=Flowstation 2/EN=TR/IP=10.11.12.37"
        packet += "^FS:3/DS=Flowstation 3/EN=TR/IP=10.11.12.38"
        packet += "^DN:DN"
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(3, len(packet_key_values))

    def test_verify_packet_for_dg_332_invalid_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid property: XX"
        packet = "000000000CC67BC7520070TE^DG:332/ID=549/SQ=0^FS:1/DS=Flowstation 1/EN=TR/XX=10.11.12.36^DN:DN"
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)


    def test_verify_packet_for_dg_402_single_water_source(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = "0000000008EE4081150106TE^DG:402/ID=600/SQ=0/MX=8^WS:1/DS=test water source/EN=FA/PR=3/WB=5000"
        packet +="/WS=FA/WR=FA/FS=TR/PC=1^SL:TR^DN:DN"
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_402_multiple_water_sources(self):
        """ This test verifies that a list of key value objects can be created from a packet string """
        packet =  "0000000008EE4081150600TE^DG:402/ID=600/SQ=0/MX=8"
        packet += "^WS:1/DS=test water source1/EN=FA/PR=3/WB=5000/WS=FA/WR=FA/FS=TR/PC=1"
        packet += "^WS:2/DS=test water source2/EN=TR/PR=4/WB=5000/WS=FA/WR=FA/FS=TR/PC=1"
        packet += "^WS:3/DS=test water source3/EN=FA/PR=5/WB=5000/WS=FA/WR=FA/FS=TR/PC=1"
        packet += "^WS:4/DS=test water source4/EN=TR/PR=6/WB=5000/WS=FA/WR=FA/FS=TR/PC=1"
        packet += "^WS:5/DS=test water source5/EN=FA/PR=7/WB=5000/WS=FA/WR=FA/FS=TR/PC=1"
        packet += "^WS:6/DS=test water source6/EN=TR/PR=8/WB=5000/WS=FA/WR=FA/FS=TR/PC=1"
        packet += "^WS:7/DS=test water source7/EN=FA/PR=9/WB=5000/WS=FA/WR=FA/FS=TR/PC=1"
        packet += "^SL:TR^DN:DN"
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(7, len(packet_key_values))

    def test_verify_packet_for_dg_402_invalid_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid property: XX"
        packet = "0000000008EE4081150106TE^DG:402/ID=600/SQ=0/MX=8^WS:1/DS=test water source/XX=FA/PR=3/WB=5000"
        packet +="/WS=FA/WR=FA/FS=TR/PC=1^SL:TR^DN:DN"
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_412_single_water_source(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = "0000000008EE4081150600TE^DG:412/ID=600/SQ=0/MX=8"
        packet += "^PC:7/DS=Point of Control 7/EN=TR/FL=7.0/FS=FA/HF=7/HS=FA/UF=0/US=FA/FM=WF02132/MV=PR00345/PP=PP00345/ML=1/PS=PS12345/HP=0/HE=FA/LP=0/LE=FA"
        packet += "^SL:TR^DN:DN"
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_412_multiple_water_sources(self):
        """ This test verifies that a list of key value objects can be created from a packet string """
        packet =  "0000000008EE4081152000TE^DG:412/ID=600/SQ=0/MX=8"
        packet += "^PC:1/DS=Point of Control 1/EN=TR/FL=1.0/FS=FA/HF=1/HS=FA/UF=0/US=FA/FM=WF02132/MV=PR00345/PP=PP00345/ML=1/PS=PS12345/HP=0/HE=FA/LP=0/LE=FA/GR=8/HF=7"
        packet += "^PC:2/DS=Point of Control 2/EN=FA/FL=2.0/FS=TR/HF=2/HS=FA/UF=1/US=FA/FM=WF02132/MV=PR00345/PP=PP00345/ML=1/PS=PS12345/HP=0/HE=FA/LP=0/GR=8/HF=7"
        packet += "^PC:3/DS=Point of Control 3/EN=TR/FL=3.0/FS=FA/HF=3/HS=FA/UF=0/US=FA/FM=WF02132/MV=PR00345/PP=PP00345/ML=1/PS=PS12345/HP=0/HE=FA/LP=0/GR=8/HF=7"
        packet += "^PC:4/DS=Point of Control 4/EN=FA/FL=4.0/FS=TR/HF=4/HS=FA/UF=0/US=FA/FM=WF02132/MV=PR00345/PP=PP00345/ML=1/PS=PS12345/HP=0/HE=FA/LP=0/GR=8/HF=7"
        packet += "^PC:5/DS=Point of Control 5/EN=TR/FL=5.0/FS=FA/HF=5/HS=FA/UF=0/US=FA/FM=WF02132/MV=PR00345/PP=PP00345/ML=1/PS=PS12345/HP=0/HE=FA/LP=0/GR=8/HF=7"
        packet += "^PC:6/DS=Point of Control 6/EN=FA/FL=6.0/FS=TR/HF=6/HS=FA/UF=0/US=FA/FM=WF02132/MV=PR00345/PP=PP00345/ML=1/PS=PS12345/HP=0/HE=FA/LP=0/GR=8/HF=7"
        packet += "^PC:7/DS=Point of Control 7/EN=TR/FL=7.0/FS=FA/HF=7/HS=FA/UF=0/US=FA/FM=WF02132/MV=PR00345/PP=PP00345/ML=1/PS=PS12345/HP=0/HE=FA/LP=0/GR=8/HF=7"
        packet += "^SL:TR^DN:DN"
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(7, len(packet_key_values))

    def test_verify_packet_for_dg_412_invalid_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid property: XX"
        packet = "0000000008EE4081150600TE^DG:412/ID=600/SQ=0/MX=8"
        packet += "^PC:7/DS=Point of Control 7/EN=TR/FL=7.0/FS=FA/HF=7/HS=FA/UF=0/US=FA/FM=WF02132/MV=PR00345/PP=PP00345/ML=1/PS=PS12345/HP=0/HE=FA/LP=0/XX=FA"
        packet += "^SL:TR^DN:DN"
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_500_single_flowstation_settings(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = ("000000D880391A50CF0123TE^DG:500/ID=0/SQ=0^FS:FX00038/DS=CN-UseCase43-verify_data_group_/LA=0/LG=0"
                  "/FP=FA/MX=CN|WS=20|PC=20|ML=20|CN=20^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_500_invalid_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid property: XX"
        packet = ("000000D880391A50CF0123TE^DG:500/ID=0/SQ=0^FS:FX00038/XX=CN-UseCase43-verify_data_group_/LA=0/LG=0"
                  "/FP=FA/MX=CN|WS=20|PC=20|ML=20|=20^SL:TR^DN:DN")
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_501_single_water_source_assignment(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = "000000D880391A50CF0065TE^DG:501/ID=551/SQ=0/MX=20^WS:2/CN=000CC67BC752/WS=1^SL:TR^DN:DN"
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_501_multiple_water_source_assignments(self):
        """ This test verifies that a list of key value objects can be created from a packet string """
        packet =  "000000D880391A50CF0500TE^DG:501/ID=551/SQ=0/MX=20"
        packet += "^WS:1/CN=000CC67BC752/WS=1"
        packet += "^WS:2/CN=000CC67BC753/WS=2"
        packet += "^WS:3/CN=000CC67BC754/WS=3"
        packet += "^WS:4/CN=000CC67BC755/WS=4"
        packet += "^WS:5/CN=000CC67BC756/WS=5"
        packet += "^WS:6/CN=000CC67BC757/WS=6"
        packet += "^WS:7/CN=000CC67BC758/WS=7"
        packet += "^SL:TR^DN:DN"
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(7, len(packet_key_values))

    def test_verify_packet_for_dg_501_invalid_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid property: XX"
        packet = "000000D880391A50CF0065TE^DG:501/ID=551/SQ=0/MX=20^WS:1/CN=000CC67BC752/XX=1^SL:TR^DN:DN"
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_511_single_point_of_control_assignment(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = "000000D880391A50CF0065TE^DG:511/ID=511/SQ=0/MX=20^PC:2/CN=000CC67BC752/PC=1^SL:TR^DN:DN"
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_511_multiple_point_of_control_assignments(self):
        """ This test verifies that a list of key value objects can be created from a packet string """
        packet =  "000000D880391A50CF0500TE^DG:511/ID=551/SQ=0/MX=20"
        packet += "^PC:1/CN=000CC67BC752/PC=1"
        packet += "^PC:2/CN=000CC67BC753/PC=2"
        packet += "^PC:3/CN=000CC67BC754/PC=3"
        packet += "^PC:4/CN=000CC67BC755/PC=4"
        packet += "^PC:5/CN=000CC67BC756/PC=5"
        packet += "^PC:6/CN=000CC67BC757/PC=6"
        packet += "^PC:7/CN=000CC67BC758/PC=7"
        packet += "^SL:TR^DN:DN"
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(7, len(packet_key_values))

    def test_verify_packet_for_dg_511_invalid_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid property: XX"
        packet = "000000D880391A50CF0065TE^DG:511/ID=551/SQ=0/MX=20^PC:1/CN=000CC67BC752/XX=1^SL:TR^DN:DN"
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_422_single_point_of_control_assignment(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = ("000000000CC67BC7520245TE^DG:422/ID=557/SQ=0/MX=8^ML:1/DS=FlowStion Mainline ABC/EN=TR/FT=200/UT=TR"
                  "/SP=0/UP=FA/FL=30000/FP=FA/FS=TR/LC=FA/FV=60/FW=FA/AF=FA/AH=45/BH=55/CH=65/DH=75/AL=85/BL=95/CL=100"
                  "/DL=100/ZH=TR/ZL=FA/ZC=0/TM=120.00/TZ=70.00/TL=70.00/UN=PS^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_422_multiple_point_of_control_assignments(self):
        """ This test verifies that a list of key value objects can be created from a packet string """
        packet = ("000000000CC67BC7529999TE^DG:422/ID=557/SQ=0/MX=8"
                  "^ML:1/DS=FlowStion Mainline ABC/EN=TR/FT=200/UT=TR/SP=0/UP=FA/FL=300.0/FP=FA/FS=TR/LC=FA/FV=60/FW=FA/"
                  "AF=FA/AH=45/BH=55/CH=65/DH=75/AL=85/BL=95/CL=100/DL=100/ZH=TR/ZL=FA/ZC=0/TM=120.00/TZ=70.00/"
                  "TL=70.00/UN=PS"
                  "^ML:2/DS=FlowStion Mainline ABC/EN=TR/FT=200/UT=TR/SP=0/UP=FA/FL=300.0/FP=FA/FS=TR/LC=FA/FV=60/FW=FA/"
                  "AF=FA/AH=45/BH=55/CH=65/DH=75/AL=85/BL=95/CL=100/DL=100/ZH=TR/ZL=FA/ZC=0/TM=120.00/TZ=70.00/"
                  "TL=70.00/UN=PS"
                  "^ML:3/DS=FlowStion Mainline ABC/EN=TR/FT=200/UT=TR/SP=0/UP=FA/FL=300.0/FP=FA/FS=TR/LC=FA/FV=60/FW=FA/"
                  "AF=FA/AH=45/BH=55/CH=65/DH=75/AL=85/BL=95/CL=100/DL=100/ZH=TR/ZL=FA/ZC=0/TM=120.00/TZ=70.00/"
                  "TL=70.00/UN=PS"
                  "^ML:4/DS=FlowStion Mainline ABC/EN=TR/FT=200/UT=TR/SP=0/UP=FA/FL=300.0/FP=FA/FS=TR/LC=FA/FV=60/FW=FA/"
                  "AF=FA/AH=45/BH=55/CH=65/DH=75/AL=85/BL=95/CL=100/DL=100/ZH=TR/ZL=FA/ZC=0/TM=120.00/TZ=70.00/"
                  "TL=70.00/UN=PS"
                  "^ML:5/DS=FlowStion Mainline ABC/EN=TR/FT=200/UT=TR/SP=0/UP=FA/FL=300.0/FP=FA/FS=TR/LC=FA/FV=60/FW=FA/"
                  "AF=FA/AH=45/BH=55/CH=65/DH=75/AL=85/BL=95/CL=100/DL=100/ZH=TR/ZL=FA/ZC=0/TM=120.00/TZ=70.00/"
                  "TL=70.00/UN=PS"
                  "^ML:6/DS=FlowStion Mainline ABC/EN=TR/FT=200/UT=TR/SP=0/UP=FA/FL=300.0/FP=FA/FS=TR/LC=FA/FV=60/FW=FA/"
                  "AF=FA/AH=45/BH=55/CH=65/DH=75/AL=85/BL=95/CL=100/DL=100/ZH=TR/ZL=FA/ZC=0/TM=120.00/TZ=70.00/"
                  "TL=70.00/UN=PS"
                  "^ML:7/DS=FlowStion Mainline ABC/EN=TR/FT=200/UT=TR/SP=0/UP=FA/FL=300.0/FP=FA/FS=TR/LC=FA/FV=60/FW=FA/"
                  "AF=FA/AH=45/BH=55/CH=65/DH=75/AL=85/BL=95/CL=100/DL=100/ZH=TR/ZL=FA/ZC=0/TM=120.00/TZ=70.00/"
                  "TL=70.00/UN=PS"
                  "^ML:8/DS=FlowStion Mainline ABC/EN=TR/FT=200/UT=TR/SP=0/UP=FA/FL=300.0/FP=FA/FS=TR/LC=FA/FV=60/FW=FA/"
                  "AF=FA/AH=45/BH=55/CH=65/DH=75/AL=85/BL=95/CL=100/DL=100/ZH=TR/ZL=FA/ZC=0/TM=120.00/TZ=70.00/"
                  "TL=70.00/UN=PS"
                  "^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(8, len(packet_key_values))

    def test_verify_packet_for_dg_422_invalid_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid property: XX"
        packet = ("000000000CC67BC7520500TE^DG:422/ID=557/SQ=0/MX=8^ML:1/DS=FlowStion Mainline ABC/EN=TR/FT=200/UT=TR"
                  "/SP=0/UP=FA/FL=300.0/FP=FA/FS=TR/LC=FA/FV=60/FW=FA/AF=FA/AH=45/BH=55/CH=65/DH=75/AL=85/BL=95/CL=100"
                  "/DL=100/ZH=TR/ZL=FA/ZC=0/TM=120.00/TZ=70.00/TL=70.00/XX=PS^SL:TR^DN:DN")
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)
        
    def test_verify_packet_for_dg_570_single_controller_assignment(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = "000000D880391A50CF0069TE^DG:570/ID=0/SQ=0/MX=20^MC:000CC67BC752/CN=1/SN=3X00001^SL:TR^DN:DN"
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_570_multiple_controller_assignments(self):
        """ This test verifies that a list of key value objects can be created from a packet string """
        packet =  ("000000D880391A50CF0293TE^DG:570/ID=0/SQ=0/MX=20"
                   "^MC:000CC67BC752/CN=1/SN=3X00001"
                   "^MC:000CC67BC753/CN=2/SN=3X00001"
                   "^MC:000CC67BC754/CN=3/SN=3X00001"
                   "^MC:000CC67BC755/CN=4/SN=3X00001"
                   "^MC:000CC67BC756/CN=5/SN=3X00001"
                   "^MC:000CC67BC757/CN=6/SN=3X00001"
                   "^MC:000CC67BC758/CN=7/SN=3X00001"
                   "^MC:000CC67BC759/CN=8/SN=3X00001"
                   "^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(8, len(packet_key_values))

    def test_verify_packet_for_dg_570_invalid_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid property: XX"
        packet = "000000D880391A50CF0069TE^DG:570/ID=0/SQ=0/MX=20^MC:000CC67BC752/CN=1/XX=3X00001^SL:TR^DN:DN"
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_571_single_controller_setting(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = ("000000AABBCCDDEEFF0092TE^DG:571/ID=12345/SQ=0/MX=20^CN:000CC67FA801/SN=3X00001/DS=BS3200 3X00001"
                  "/EN=TR^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_571_multiple_controller_settings(self):
        """ This test verifies that a list of key value objects can be created from a packet string """
        packet = ("000000AABBCCDDEEFF0449TE^DG:571/ID=12345/SQ=0/MX=20"
                  "^CN:000CC67FA801/SN=3X00001/DS=BS3200 3X00001/EN=TR"
                  "^CN:000CC67FA802/SN=3X00001/DS=BS3200 3X00001/EN=TR"
                  "^CN:000CC67FA803/SN=3X00001/DS=BS3200 3X00001/EN=TR"
                  "^CN:000CC67FA804/SN=3X00001/DS=BS3200 3X00001/EN=TR"
                  "^CN:000CC67FA805/SN=3X00001/DS=BS3200 3X00001/EN=TR"
                  "^CN:000CC67FA806/SN=3X00001/DS=BS3200 3X00001/EN=TR"
                  "^CN:000CC67FA807/SN=3X00001/DS=BS3200 3X00001/EN=TR"
                  "^CN:000CC67FA808/SN=3X00001/DS=BS3200 3X00001/EN=TR"
                  "^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(8, len(packet_key_values))

    def test_verify_packet_for_dg_571_invalid_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid property: XX"
        packet = ("000000AABBCCDDEEFF0092TE^DG:571/ID=12345/SQ=0/MX=20^CN:000CC67FA801/SN=3X00001/XX=BS3200 3X00001"
                  "/EN=TR^SL:TR^DN:DN")
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_403_invalid_sub_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid sub property: XX"
        packet = ("000000D880391A50CF0234TE^DG:403/ID=0/SQ=0/MX=8"
        "^WS:1"
        "/EM=1|EN=TR|TY=MS|MS=SB05308|ML=2|XX=60"
        "^SL:TR^DN:DN")
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)
    def test_verify_packet_for_dg_403_single_empty_condition(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = ("000000D880391A50CF0234TE^DG:403/ID=0/SQ=0/MX=8"
                  "^WS:1"
                  "/EM=1|EN=TR|TY=MS|MS=SB05308|ML=2|TM=60"
                  "^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_403_empty_condition(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = ("000000D880391A50CF1000TE^DG:403/ID=0/SQ=0/MX=8"
              "^WS:2"
              "/EM=1|EN=TR|TY=MS|MS=SB05308|ML=2|TM=60"
              "/EM=2|EN=TR|TY=SW|SW=SB05309|LT=OP|TM=70"
              "/EM=3|EN=TR|TY=PS|PS=SB05307|PL=2|TM=80"
              "/EM=4|EN=TR|TY=DS|"
              "^WS:3"
              "/EM=1|EN=TR|TY=MS|MS=SB05301|ML=6|TM=10"
              "/EM=2|EN=TR|TY=SW|SW=SB05302|LT=CL|TM=20"
              "/EM=3|EN=TR|TY=PS|PS=SB05303|PL=3|TM=30"
              "/EM=4|EN=TR|TY=DS"
              "^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(2, len(packet_key_values))

    def test_verify_packet_for_dg_101_single_zone(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = ("000000000CC67BC7521084TE^DG:101/ID=996/SQ=0/MX=200"
                  "^ZN:1/DS=Test Zone 1/EN=TR/LA=43.609773/LG=-116.310264/DF=0.0/SN=TSD0001/FN=0.0/FD=20000101/KC=0.00/PR=0.00/EF=0.00/RZ=0.00/ML=1"
                  "^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_101_multiple_zones(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = ("000000000CC67BC7521084TE^DG:101/ID=996/SQ=0/MX=200"
                  "^ZN:1/DS=Test Zone 1/EN=TR/LA=43.609773/LG=-116.310264/DF=0.0/SN=TSD0001/FN=0.0/FD=20000101/KC=0.00/PR=0.00/EF=0.00/RZ=0.00/ML=1"
                  "^ZN:2/DS=Test Zone 2/EN=TR/LA=43.609778/LG=-116.310259/DF=0.0/SN=TSQ0071/FN=0.0/FD=20000101/KC=0.00/PR=0.00/EF=0.00/RZ=0.00/ML=1"
                  "^ZN:49/DS=Test Zone 49/EN=TR/LA=43.610013/LG=-116.310024/DF=0.0/SN=TSQ0072/FN=0.0/FD=20000101/KC=0.00/PR=0.00/EF=0.00/RZ=0.00/ML=1"
                  "^ZN:50/DS=Test Zone 50/EN=TR/LA=43.610018/LG=-116.310019/DF=0.0/SN=TSQ0073/FN=0.0/FD=20000101/KC=0.00/PR=0.00/EF=0.00/RZ=0.00/ML=1"
                  "^ZN:197/DS=Test Zone 197/EN=TR/LA=43.610753/LG=-116.309284/DF=0.0/SN=TSQ0074/FN=0.0/FD=20000101/KC=0.00/PR=0.00/EF=0.00/RZ=0.00/ML=1"
                  "^ZN:198/DS=Test Zone 198/EN=TR/LA=43.610758/LG=-116.309279/DF=0.0/SN=TSQ0081/FN=0.0/FD=20000101/KC=0.00/PR=0.00/EF=0.00/RZ=0.00/ML=1"
                  "^ZN:199/DS=Test Zone 199/EN=TR/LA=43.610763/LG=-116.309274/DF=0.0/SN=TSQ0082/FN=0.0/FD=20000101/KC=0.00/PR=0.00/EF=0.00/RZ=0.00/ML=1"
                  "^ZN:200/DS=Test Zone 200/EN=TR/LA=43.610768/LG=-116.309269/DF=0.0/SN=TSQ0083/FN=0.0/FD=20000101/KC=0.00/PR=0.00/EF=0.00/RZ=0.00/ML=1"
                  "^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(8, len(packet_key_values))

    def test_verify_packet_for_dg_101_invalid_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid property: XX"
        packet = ("000000000CC67BC7521084TE^DG:101/ID=996/SQ=0/MX=200"
                  "^ZN:1/DS=Test Zone 1/XX=TR/LA=43.609773/LG=-116.310264/DF=0.0/SN=TSD0001/FN=0.0/FD=20000101/KC=0.00/PR=0.00/EF=0.00/RZ=0.00/ML=1"
                  "^SL:TR^DN:DN")
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_111_single_master_valve(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = ("000000000CC67BC7520294TE^DG:111/ID=1000/SQ=0/MX=8"
                  "^MV:TMV0003/DS=Test Master Valve TMV0003/NU=1/EN=TR/LA=43.609773/LG=-116.309964/NO=FA"
                  "^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_111_multiple_master_valves(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = ("000000000CC67BC7520294TE^DG:111/ID=1000/SQ=0/MX=8"
                  "^MV:TMV0003/DS=Test Master Valve TMV0003/NU=1/EN=TR/LA=43.609773/LG=-116.309964/NO=FA"
                  "^MV:TMV0004/DS=Test Master Valve TMV0004/NU=2/EN=TR/LA=43.609778/LG=-116.309959/NO=FA"
                  "^MV:TMV0005/DS=Test Master Valve TMV0005/NU=8/EN=TR/LA=43.609808/LG=-116.309929/NO=FA"
                  "^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(3, len(packet_key_values))

    def test_verify_packet_for_dg_111_invalid_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid property: XX"
        packet = ("000000000CC67BC7520294TE^DG:111/ID=1000/SQ=0/MX=8"
                  "^MV:TMV0003/XX=Test Master Valve TMV0003/NU=1/EN=TR/LA=43.609773/LG=-116.309964/NO=FA"
                  "^SL:TR^DN:DN")
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_packet_for_dg_131_single_flow_meter(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = ("000000000CC67BC7520294TE^DG:131/ID=1000/SQ=0/MX=8"
                  "^FM:XXX0003/DS=Test Flow Meter XXX0003/NU=1/EN=TR/LA=43.609773/LG=-116.309964/KV=1.12"
                  "^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(1, len(packet_key_values))

    def test_verify_packet_for_dg_131_multiple_flow_meters(self):
        """ This test verifies that a list of key value objects can be created from a packet string"""
        packet = ("000000000CC67BC7520294TE^DG:131/ID=1000/SQ=0/MX=8"
                  "^FM:XXX0003/DS=Test Flow Meter XXX0003/NU=1/EN=TR/LA=43.609773/LG=-116.309964/KV=1.12"
                  "^FM:XXX0004/DS=Test Flow Meter XXX0004/NU=2/EN=TR/LA=45.609773/LG=-117.309964/KV=2.12"
                  "^FM:XXX0005/DS=Test Flow Meter XXX0005/NU=3/EN=TR/LA=46.609773/LG=-118.309964/KV=2.12"
                  "^SL:TR^DN:DN")
        packet_key_values = packet_verifier.verify_packet(packet=packet)
        self.assertIsNotNone(packet_key_values)
        self.assertEqual(3, len(packet_key_values))

    def test_verify_packet_for_dg_131_invalid_property(self):
        """ This test verifies an exception is thrown when there is an invalid parameter in the data packet """
        expected_message = "verify_te_packet: Invalid property: XX"
        packet = ("000000000CC67BC7520294TE^DG:131/ID=1000/SQ=0/MX=8"
                  "^FM:XXX0003/XX=Test Flow Meter XXX0003/NU=1/EN=TR/LA=43.609773/LG=-116.309964/KV=1.12"
                  "^SL:TR^DN:DN")
        with self.assertRaises(Exception) as context:
            packet_key_values = packet_verifier.verify_packet(packet=packet)

        self.assertEqual(expected_message, context.exception.message)