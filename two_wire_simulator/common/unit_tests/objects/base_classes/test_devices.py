import unittest
import mock
import serial
import status_parser

from common.imports import opcodes
from common import helper_methods
from common.objects.base_classes.devices import BaseDevices
from common.objects.base_classes.bicoders import BiCoder
# from nose.tools import nottest

__author__ = 'Brice "Ajo Grande" Garlick'


class TestBaseDeviceObject(unittest.TestCase):
    """
    Controller Lat & Lng
    latitude = 43.609768
    longitude = -116.309759

    Master Valve Starting lat & lng:
    latitude = 43.609768
    longitude = -116.310359
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        BaseDevices.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        self.create_test_device_object(type=opcodes.zone, address=1, serial_number = "TSD0001")

        # BaseDevices.controller_lat = float(43.609768)
        # BaseDevices.controller_long = float(-116.309759)

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    # #################################
    # @nottest
    #################################
    def create_test_device_object(self, type, address, serial_number):
        """ Creates a new object for testing purposes """
        self.uut = BaseDevices(_ser=self.mock_ser, _identifiers=[[type, address]],
                               _la=float(43.609768), _lg=float(-116.309759), _sn=serial_number,
                               _ad=address, _ty=type)

        self.uut.bicoder = mock.MagicMock(spec=BiCoder)

    #################################
    def test_get_data_if_necessary_requested(self):
        """ Make sure get_data makes the expected GET through to get_and_wait_for_reply when _get_data is True """
        expected_command = "GET,{0}={1}".format(self.uut.ty, self.uut.ad)
        self.uut.get_data_if_necessary(_get_data=True)
        self.mock_get_and_wait_for_reply.assert_called_with(called_by_get_data=True, tosend=expected_command)

    #################################
    def test_get_data_if_necessary_not_requested(self):
        """ Make sure get_data doesn't call get_and_wait_for_reply when _get_data is False """
        self.uut.get_data_if_necessary(_get_data=False)
        self.assertEquals(self.mock_get_and_wait_for_reply.call_count, 0)

    #################################
    def test_get_data_if_necessary_not_requested_but_needed(self):
        """ Make sure get_data makes the expected GET through to get_and_wait_for_reply when _get_data is False,
         but there is currently no data """
        self.uut.data = None
        expected_command = "GET,{0}={1}".format(self.uut.ty, self.uut.ad)
        self.uut.get_data_if_necessary(_get_data=False)
        self.mock_get_and_wait_for_reply.assert_called_with(called_by_get_data=True, tosend=expected_command)

    #################################
    def test_set_address_pass1(self):
        """ Set Address On Controller Pass Case 1: Using Default _ad value, check that object variable _ad is set """
        # dev = self.create_test_device_object()

        expected_value = self.uut.ad
        self.uut.set_address()

        actual_value = self.uut.ad
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_address_pass2(self):
        """ Set Address On Controller Pass Case 2: Setting new _ad value = 2, check that object variable _ad is set """
        # dev = self.create_test_device_object()

        expected_value = 2
        self.uut.set_address(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = self.uut.ad
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_address_pass3(self):
        """ Set Address On Controller Pass Case 3: Command with correct values sent to controller """
        # dev = self.create_test_device_object()

        ad_value = str(self.uut.ad)
        expected_command = "DO,AZ=TSD0001,NU={0}".format(str(ad_value))
        self.uut.set_address()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_address_fail1(self):
        """ Set Address On Controller Fail Case 1: Failed communication with controller """
        # dev = self.create_test_device_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            self.uut.set_address()
        e_msg = "Exception occurred trying to address ZN 1 with serial: TSD0001 to the controller"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_latitude_pass1(self):
        """ Set Latitude On Controller Pass Case 1: Using Default _la value, check that object variable _la is set"""
        # dev = self.create_test_device_object()

        expected_value = self.uut.la
        self.uut.set_latitude()

        actual_value = self.uut.la
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_latitude_pass2(self):
        """ Set Description On Controller Pass Case 2: Setting new _la value = 5.000001, check that
        object variable _la is set on a ZN"""
        # dev = self.create_test_device_object()

        expected_value = 5.000001
        self.uut.set_latitude(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = self.uut.la
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_latitude_pass3(self):
        """ Set Latitude On Controller Pass Case 3: Command with correct values sent to controller with a ZN
        dev type"""
        # dev = self.create_test_device_object()

        la_value = 5.000001
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.zone, opcodes.latitude, str(la_value))
        self.uut.set_latitude(la_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_latitude_pass4(self):
        """ Set Latitude On Controller Pass Case 4: Command with correct values sent to controller with an MV
        dev type"""
        # dev = self.create_test_device_object(dev_type="MV")
        self.uut.dv_type = 'MV'
        la_value = 5.000001
        expected_command = "SET,{0}=TSD0001,{1}={2}".format(opcodes.master_valve, opcodes.latitude, str(la_value))
        self.uut.set_latitude(la_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_latitude_fail1(self):
        """ Set Description On Controller Fail Case 1: Failed communication with controller """
        # dev = self.create_test_device_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            self.uut.set_latitude()
        e_msg = "Exception occurred trying to set ZN's latitude to: {0}".format(str(self.uut.la))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_latitude_fail2(self):
        """ Set Latitude On Controller Fail Case 2: Incorrect latitude format passed as argument """
        # dev = self.create_test_device_object()

        new_la_value = 6
        placeholder = helper_methods.format_lat_long
        self.mock_format_lat_long = mock.MagicMock(sideeffect=Exception)
        helper_methods.format_lat_long = self.mock_format_lat_long
        self.mock_format_lat_long.side_effect = Exception

        with self.assertRaises(Exception) as context:
            self.uut.set_latitude(new_la_value)
        e_msg = "Exception occurred trying to set latitude for device: ZN\n"
        helper_methods.format_lat_long = placeholder
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_latitude_fail3(self):
        """ Set Latitude On Controller Fail Case 3: Incorrect dev type """
        # dev = self.create_test_device_object(dev_type="BG")
        self.uut.dv_type = 'BG'
        with self.assertRaises(Exception) as context:
            self.uut.set_latitude()
        e_msg = "Invalid device type: BG entered while trying to send latitude to cn"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_longitude_pass1(self):
        """ Set Longitude On Controller Pass Case 1: Using Default _lg value, check that object variable _lg is set"""
        # dev = self.create_test_device_object()

        expected_value = self.uut.lg
        self.uut.set_longitude()

        actual_value = self.uut.lg
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_longitude_pass2(self):
        """ Set Longitude On Controller Pass Case 2: Setting new _lg value = 5.000001, check that
        object variable _lg is set on a ZN"""
        # dev = self.create_test_device_object()

        expected_value = 5.000001
        self.uut.set_longitude(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = self.uut.lg
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_longitude_pass3(self):
        """ Set Longitude On Controller Pass Case 3: Command with correct values sent to controller with a ZN
        dev type"""
        # dev = self.create_test_device_object()

        lg_value = 5.000001
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.zone, opcodes.longitude, str(lg_value))
        self.uut.set_longitude(lg_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_longitude_pass4(self):
        """ Set Longitude On Controller Pass Case 4: Command with correct values sent to controller with an MV
        dev type"""
        # dev = self.create_test_device_object(dev_type="MV")
        self.uut.dv_type = 'MV'
        la_value = 5.000001
        expected_command = "SET,{0}=TSD0001,{1}={2}".format(opcodes.master_valve, opcodes.longitude, str(la_value))
        self.uut.set_longitude(la_value)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_longitude_fail1(self):
        """ Set Longitude On Controller Fail Case 1: Failed communication with controller """
        # dev = self.create_test_device_object()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            self.uut.set_longitude()
        e_msg = "Exception occurred trying to set ZN's lg to: {0}".format(str(self.uut.lg))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_longitude_fail2(self):
        """ Set Longitude On Controller Fail Case 2: Incorrect longitude format passed as argument """
        # dev = self.create_test_device_object()

        new_lg_value = 6
        placeholder = helper_methods.format_lat_long
        self.mock_format_lat_long = mock.MagicMock(sideeffect=Exception)
        helper_methods.format_lat_long = self.mock_format_lat_long
        self.mock_format_lat_long.side_effect = Exception

        with self.assertRaises(Exception) as context:
            self.uut.set_longitude(new_lg_value)
        e_msg = "Exception occurred trying to set longitude for device: ZN\n"
        helper_methods.format_lat_long = placeholder
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_longitude_fail3(self):
        """ Set Longitude On Controller Fail Case 3: Incorrect dev type """
        # dev = self.create_test_device_object(dev_type="BG")

        self.uut.dv_type = 'BG'
        with self.assertRaises(Exception) as context:
            self.uut.set_longitude()
        e_msg = "Invalid device type: BG entered while trying to send longitude to cn"
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_latitude_pass1(self):
        """ Verify Latitude On Controller Pass Case 1: Exception is not raised """
        # dev = self.create_test_device_object()
        self.uut.la = 5.000001
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5.000001".format(opcodes.latitude))
        self.uut.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.uut.verify_latitude()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertTrue(test_pass, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_latitude_fail1(self):
        """ Verify Latitude On Controller Fail Case 1: Value on controller does not match what is
        stored in dev.la for dev type ZN """
        # dev = self.create_test_device_object()
        self.uut.la = 5.000001
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5.000002".format(opcodes.latitude))
        self.uut.data = mock_data

        expected_message = "Unable verify ZN: 1's 'latitude'. Received: 5.000002, Expected: 5.000001"
        try:
            self.uut.verify_latitude()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertTrue(test_pass, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_latitude_fail2(self):
        """ Verify Latitude On Controller Fail Case 2: Value on controller does not match what is
        stored in dev.la for dev type MV """
        # dev = self.create_test_device_object(dev_type="MV")
        self.uut.dv_type = 'MV'
        self.uut.la = 5.000001
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5.000002".format(opcodes.latitude))
        self.uut.data = mock_data

        expected_message = "Unable verify MV: TSD0001's 'latitude'. Received: 5.000002, Expected: 5.000001"
        try:
            self.uut.verify_latitude()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertTrue(test_pass, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_longitude_pass1(self):
        """ Verify Longitude On Controller Pass Case 1: Exception is not raised """
        # dev = self.create_test_device_object()
        self.uut.lg = 5.000001
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5.000001".format(opcodes.longitude))
        self.uut.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.uut.verify_longitude()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertTrue(test_pass, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_longitude_fail1(self):
        """ Verify Longitude On Controller Fail Case 1: Value on controller does not match what is
        stored in dev.lg for dev type ZN """
        # dev = self.create_test_device_object()
        self.uut.lg = 5.000001
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5.000002".format(opcodes.longitude))
        self.uut.data = mock_data

        expected_message = "Unable verify ZN: 1's 'longitude'. Received: 5.000002, Expected: 5.000001"
        try:
            self.uut.verify_longitude()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertTrue(test_pass, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_longitude_fail2(self):
        """ Verify Longitude On Controller Fail Case 2: Value on controller does not match what is
        stored in dev.lg for dev type MV """
        # dev = self.create_test_device_object(dev_type="MV")
        self.uut.dv_type = 'MV'
        self.uut.lg = 5.000001
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=5.000002".format(opcodes.longitude))
        self.uut.data = mock_data

        expected_message = "Unable verify MV: TSD0001's 'longitude'. Received: 5.000002, Expected: 5.000001"
        try:
            self.uut.verify_longitude()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertTrue(test_pass, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_serial_number_pass1(self):
        """ Verify Serial Number On Controller Pass Case 1: Exception is not raised """
        # dev = self.create_test_device_object()
        self.uut.sn = "TSD0002"
        test_pass = False

        mock_data = status_parser.KeyValues("{0}=TSD0002".format(opcodes.serial_number))
        self.uut.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.uut.verify_serial_number()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertTrue(test_pass, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_serial_number_fail1(self):
        """ Verify Serial Number On Controller Fail Case 1: Value on controller does not match what is
        stored in dev.sn for dev type ZN """
        # dev = self.create_test_device_object()
        self.uut.sn = "TSD0002"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}=TSD0003".format(opcodes.serial_number))
        self.uut.data = mock_data

        expected_message = "Unable verify ZN: 1's 'Serial Number'. Received: TSD0003, Expected: TSD0002"
        try:
            self.uut.verify_serial_number()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertTrue(test_pass, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    if __name__ == "__main__":
        unittest.main()
