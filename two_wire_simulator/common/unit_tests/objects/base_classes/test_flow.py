from unittest import TestCase
import mock
from serial import Serial
from common.objects.controllers.bl_32 import BaseStation3200
from common.imports import opcodes
from common.objects.base_classes.flow import BaseFlow
from common.imports.types import ActionCommands


class TestBaseFlow(TestCase):
    def setUp(self):
        # Create the mock_ser object
        mock_ser = mock.MagicMock(spec=Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        # BaseDevices.ser = mock_ser

        # mock_controller = mock.MagicMock(spec=BaseStation3200)

        ip_address = "192.168.100.100"
        port_address = "192.168.100.101"
        socket_port = 1067

        self.controller = BaseStation3200(_mac="12345",
                                          _serial_number="3K0001",
                                          _firmware_version="",
                                          _serial_port=mock_ser,
                                          _port_address=port_address,
                                          _socket_port=socket_port,
                                          _ip_address=ip_address)

    def create_test_base_flow_object(self, _address=1):
        water_source_address = 2
        identifier = [[opcodes.water_source, water_source_address]]
        base_flow_object = BaseFlow(_controller=self.controller, _ad=_address, _identifiers=identifier, _ty="WS")

        return base_flow_object

    def test_set_target_flow_happy_path(self):
        """ Verify that set_target_flow sends the correct command to the controller """
        base_flow_object = self.create_test_base_flow_object()
        new_flow = 10
        expected_command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            base_flow_object.identifiers[0][0],     # {1}
            base_flow_object.identifiers[0][1],     # {2}
            opcodes.target_flow,                    # {3}
            str(new_flow)                           # {4}
        )

        base_flow_object.set_target_flow(_gpm=new_flow)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_target_flow_fail(self):
        """ Verify the message when set_target_flow has an exception when sending the command to the controller """
        exception_message = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_message)
        base_flow_object = self.create_test_base_flow_object()
        new_flow = 10
        expected_message = "Exception occurred trying to set {0} {1}'s 'Target Flow' to: '{2}' -> {3}".format(
                                str(base_flow_object.identifiers[0][0]),    # {0}
                                str(base_flow_object.identifiers[0][1]),    # {1}
                                str(new_flow),                              # {2}
                                exception_message                           # {3}
                            )
        with self.assertRaises(Exception) as context:
            base_flow_object.set_target_flow(_gpm=new_flow)

        self.assertEqual(expected_message, context.exception.message)

    def test_verify_target_flow_happy_path(self):
        """ Verify happy path where verify_target_flow on controller matches what's in this object """
        base_flow_object = self.create_test_base_flow_object()
        on_controller = 50.50
        expected = 50.50
        base_flow_object.data = mock.MagicMock()
        base_flow_object.data.get_value_string_by_key = mock.MagicMock(return_value=on_controller)
        base_flow_object.fl = expected
        base_flow_object.verify_target_flow()

    def test_verify_target_flow_fail(self):
        """ Verify exception when verify_empty_wait_time value on controller doesn't match what's in this object """
        base_flow_object = self.create_test_base_flow_object()
        on_controller = 50.50
        expected = 60.60
        base_flow_object.data = mock.MagicMock()
        base_flow_object.data.get_value_string_by_key = mock.MagicMock(return_value=on_controller)
        base_flow_object.fl = expected
        expected_message = "Unable to verify {0} {1}'s 'Target Flow'. Received: {2}, Expected: {3}".format(
                                str(base_flow_object.identifiers[0][0]),        # {0}
                                str(base_flow_object.ad),                       # {1}
                                str(on_controller),                             # {2}
                                str(expected)                                   # {3}
                            )
        with self.assertRaises(ValueError) as context:
            base_flow_object.verify_target_flow()

        self.assertEqual(expected_message, context.exception.message)

    def test_set_priority_fail(self):
        """ Verify exception when set_priority value on controller doesn't match what's in this object """
        base_flow_object = self.create_test_base_flow_object()

        expected_exception = "This method is being overridden in Mainline, Point of Control, and Water Source"
        with self.assertRaises(NotImplementedError) as context:
            base_flow_object.set_priority(_priority_for_water_source=5)

        self.assertEqual(expected_exception, context.exception.message)
