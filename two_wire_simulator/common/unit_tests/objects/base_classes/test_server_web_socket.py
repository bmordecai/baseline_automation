import unittest2 as unittest
from common.objects.base_classes.websocket_test_client import TestClient
from common.server_handler import ServerHandler
import mock


class TestControllerWebsocketObject(unittest.TestCase):
    def setUp(self):
        """ Setting up for the test. """
        # mock items

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_get_server_web_socket_client_connected(self):
        # this test verifies that the server side of the web socket can be retrieved from the server_handler
        server_ip_address = '127.0.0.1'
        server_port = 9000
        server_handler = ServerHandler(server_ip_address, server_port)

        # create the test_client.
        test_client = TestClient(server_ip_address, server_port)
        test_client.start_client()
        server_web_socket = server_handler.get_server_web_socket()
        test_client.close_connection()
        server_handler.shutdown_server()
        self.assertIsNotNone(server_web_socket)

    def test_get_server_web_socket_no_client_connected(self):
        # this test verifies that a web socket will not be returned if there is no client connected to the server
        server_ip_address = '127.0.0.1'
        server_port = 9000
        server_handler = ServerHandler(server_ip_address, server_port)

        server_web_socket = server_handler.get_server_web_socket()
        # shut down server
        server_handler.shutdown_server()
        # verify the test
        self.assertIsNone(server_web_socket)

    def test_get_server_web_socket_no_server(self):
        # this test verifies that a web socket will not be returned if there is no server started
        server_ip_address = '127.0.0.1'
        server_port = 9000

        # create and start the server.
        server_handler = ServerHandler(server_ip_address, server_port)

        # shut down server
        server_handler.shutdown_server()

        # get the server web socket - this should return the current server web socket - should be null
        server_web_socket = server_handler.get_server_web_socket()

        # make sure the server_web_socket is Null
        self.assertIsNone(server_web_socket)

    def test_server_web_socket_ping_test_client(self):
        # this test verifies that the server side of the web socket can be retrieved from the server_handler

        server_ip_address = '127.0.0.1'
        server_port = 9000

        # create and start the server.
        server_handler = ServerHandler(server_ip_address, server_port)

        # create the test_client
        test_client = TestClient(server_ip_address, server_port)

        # start the test_client
        test_client.start_client()

        # get the server web socket - this should return the current server web socket
        server_web_socket = server_handler.get_server_web_socket()

        # verify the web socket is not null
        self.assertIsNotNone(server_web_socket)

        # ping the client and wait for a response
        ping_result = server_web_socket.ping_client()

        # verify ping_result
        self.assertTrue(ping_result)

        # shut down the test_client
        test_client.close_connection()

        # shutdown the server
        server_handler.shutdown_server()

    def test_server_web_socket_send_a_packet_wait_for_response_none_ts_packet(self):
        # this test verifies that a TQ packet can be sent from the server and a response comes back.

        message = "TQ^DG:403/ID=559^WS:AL^DN:DN"
        controller_mac = '0004A3765054'
        server_ip_address = '127.0.0.1'
        server_port = 9000

        #### confguration.py ####
        # create and start the server.
        server_handler = ServerHandler(server_ip_address, server_port)

        # verify the server is running
        server_running = server_handler.verify_server_running()
        self.assertTrue(server_running)

        if server_running:
            # start the test_client
            test_client = TestClient(server_ip_address, server_port)

            # start the test_client and connect to the server
            test_client.start_client()

            # get the server_web_socket
            server_web_socket = server_handler.get_server_web_socket()

            # create a packet from the message above.
            packet = server_web_socket.make_a_packet_string(controller_mac, message)

            ### test_case.py ###
            # send a packet to the test client and wait for a response.
            test_client_response = server_web_socket.send_packet_to_controller_wait_for_response(packet)

            # create the expected response.
            expected_response_dg_403 = "0000000004A37650540248TE^DG:403/ID=559/SQ=0/MX=8^WS:1/EM=1|EN=TR|TM=3600.0^WS:2"
            expected_response_dg_403 += "/EM=1|EN=FA|TM=3600.0^WS:3/EM=1|EN=FA|TM=3600.0^WS:4/EM=1|EN=FA|TM=3600.0^WS:5/EM=1"
            expected_response_dg_403 += "|EN=FA|TM=3600.0^WS:6/EM=1|EN=FA|TM=3600.0^WS:7/EM=1|EN=FA|TM=3600.0^WS:8/EM=1|EN=FA"
            expected_response_dg_403 += "|TM=3600.0^SL:TR^DN:DN"

            # verify the expected response matches the actual response
            self.assertEquals(test_client_response, expected_response_dg_403)

            # close the test client connection
            test_client.close_connection()

            # shutdown the server
            server_handler.shutdown_server()

    def test_parse_client_response(self):
        # this test verifies that parse_client_response will create the same Packet object from the same packet string.

        message = "TQ^DG:403/ID=559^WS:AL^DN:DN"
        controller_mac = '0004A3765054'
        server_ip_address = '127.0.0.1'
        server_port = 9000

        #### confguration.py ####
        # create and start the server.
        server_handler = ServerHandler(server_ip_address, server_port)

        # verify the server is running
        server_running = server_handler.verify_server_running()
        self.assertTrue(server_running)

        if server_running:
            # start the test_client
            test_client = TestClient(server_ip_address, server_port)

            # start the test_client and connect to the server
            test_client.start_client()

            # get the server_web_socket
            server_web_socket = server_handler.get_server_web_socket()

            # create a packet from the message above.
            packet = server_web_socket.make_a_packet_string(controller_mac, message)

            ### test_case.py ###
            # send the packet to the controller and wait for a response. The controller here is the test client.
            test_client_response = server_web_socket.send_packet_to_controller_wait_for_response(packet)

            # create the expected response
            expected_response_dg_403  = "0000000004A37650540248TE^DG:403/ID=559/SQ=0/MX=8^WS:1/EM=1|EN=TR|TM=3600.0^WS:2"
            expected_response_dg_403 += "/EM=1|EN=FA|TM=3600.0^WS:3/EM=1|EN=FA|TM=3600.0^WS:4/EM=1|EN=FA|TM=3600.0^WS:5/EM=1"
            expected_response_dg_403 += "|EN=FA|TM=3600.0^WS:6/EM=1|EN=FA|TM=3600.0^WS:7/EM=1|EN=FA|TM=3600.0^WS:8/EM=1|EN=FA"
            expected_response_dg_403 += "|TM=3600.0^SL:TR^DN:DN"

            # parse the expected response and the client response into a Packet object
            parsed_expected_client_response = server_web_socket.parse_client_response(expected_response_dg_403)
            parsed_client_response = server_web_socket.parse_client_response(test_client_response)

            # This test only works because we are getting the response in a known order. In the real-case scenario, this
            # is not certain
            self.assertEquals(parsed_client_response, parsed_expected_client_response)

            # close the test client connection
            test_client.close_connection()

            # shutdown the server
            server_handler.shutdown_server()

    def test_send_packet_to_controller_wait_for_response_ts_packet(self):
        # this test verifies the happy path for send_packet_to_controller_wait_for_response works for a TS packet

        message = "TS^DG:302/ID=559/SQ=0^MS:xxx0001/DS=test moisture sensor/LA=45.000/LG=116.44565/EN=TR^SL:TR^DN:DN"
        controller_mac = '0004A3765054'
        server_ip_address = '127.0.0.1'
        server_port = 9000

        #### confguration.py ####
        # create and start the server.
        server_handler = ServerHandler(server_ip_address, server_port)

        # verify the server is running
        server_running = server_handler.verify_server_running()
        self.assertTrue(server_running)
        if server_running:
            # create the test client
            test_client = TestClient(server_ip_address, server_port)

            # start the test_client and connect to the server
            test_client.start_client()

            # get the server web socket
            server_web_socket = server_handler.get_server_web_socket()

            # create a packet from the message above
            packet = server_web_socket.make_a_packet_string(controller_mac, message)

            ### test_case.py ###
            # send the packet to the controller and wait for a response. The controller here is the test client.
            test_client_response = server_web_socket.send_packet_to_controller_wait_for_response(packet, controller_mac)

            # verify the response from the test client
            expected_response_ts_packet_dg_302 = "TS packet sent successfully"

            # assert that the client response matches the expected response
            self.assertEquals(test_client_response, expected_response_ts_packet_dg_302)

            # close the test client connection
            test_client.close_connection()

            # shutdown the server
            server_handler.shutdown_server()

    def test_send_packet_to_controller_wait_for_response_ts_packet_error_in_enter_table_programming_error(self):
        # this test verifies that an exception will be thrown when there is an error with enter table programming
        message = "TS^DG:302/ID=559/SQ=0^MS:xxx0001/DS=test moisture sensor/LA=45.000/LG=116.44565/EN=TR^SL:TR^DN:DN"
        controller_mac = '0004A3765054'
        server_ip_address = '127.0.0.1'
        server_port = 9000

        #### confguration.py ####
        # create and start the server
        server_handler = ServerHandler(server_ip_address, server_port)

        # verify the server is running
        server_running = server_handler.verify_server_running()
        self.assertTrue(server_running)
        if server_running:

            # create the test_client
            test_client = TestClient(server_ip_address, server_port)

            # start the test_client and connect to the server
            test_client.start_client()

            # get the server_web_socket
            server_web_socket = server_handler.get_server_web_socket()

            # mock server_web_socket.send_message_wait_for_client_response
            mock_send_message = server_web_socket.send_message_wait_for_client_response = mock.MagicMock()

            # set the side effects
            mock_send_message.side_effect = ['', '', '']

            # make a packet from a message
            packet = server_web_socket.make_a_packet_string(controller_mac, message)

            ### test_case.py ###

            # make sure that server_web_socket.send_packet_to_controller_wait_for_response throws an error since
            # no response came back for entering table programming
            with self.assertRaises(Exception):
                test_client_response = server_web_socket.send_packet_to_controller_wait_for_response(packet, controller_mac)

            # close the test client connection
            test_client.close_connection()

            # shut down the server
            server_handler.shutdown_server()

    def test_send_packet_to_controller_wait_for_response_ts_packet_error_in_ts_packet_response(self):
        # this test verifies that when no success response comes back after sending a ts packet,
        #  an exception is thrown
        message = "TS^DG:302/ID=559/SQ=0^MS:xxx0001/DS=test moisture sensor/LA=45.000/LG=116.44565/EN=TR^SL:TR^DN:DN"
        controller_mac = '0004A3765054'
        server_ip_address = '127.0.0.1'
        server_port = 9000

        #### confguration.py ####
        # create and start the server
        server_handler = ServerHandler(server_ip_address, server_port)

        # verify the server is running
        server_running = server_handler.verify_server_running()
        self.assertTrue(server_running)
        if server_running:
            # create the test_client
            test_client = TestClient(server_ip_address, server_port)

            # start the test_client and connect to the server
            test_client.start_client() # ask Andy about mock registration

            # get the server web socket
            server_web_socket = server_handler.get_server_web_socket()

            # create the mock send_message_wait_for_client_response
            mock_send_message = server_web_socket.send_message_wait_for_client_response = mock.MagicMock()

            # set the side effect
            mock_send_message.side_effect = ["0000000004A37650540037TU^MO:ET/ID=25^ST:SU/TX=Success^DN:DN", '', '']

            # make a packet from a message
            packet = server_web_socket.make_a_packet_string(controller_mac, message)

            ### test_case.py ###
            # send the packet to the test client. An exception is thrown since there is no response to the TS packet
            with self.assertRaises(Exception):
                test_client_response = server_web_socket.send_packet_to_controller_wait_for_response(packet, controller_mac)

            # close the test client connection
            test_client.close_connection()

            # shutdown the server
            server_handler.shutdown_server()

    def test_send_packet_to_controller_wait_for_response_ts_packet_error_in_exit_table_programming_error(self):
        # this test verifies that when a TS packet is sent and there is an error exiting table programming, an exception
        # is thrown.
        message = "TS^DG:302/ID=559/SQ=0^MS:xxx0001/DS=test moisture sensor/LA=45.000/LG=116.44565/EN=TR^SL:TR^DN:DN"
        controller_mac = '0004A3765054'
        server_ip_address = '127.0.0.1'
        server_port = 9000

        #### confguration.py ####
        # create and start the server
        server_handler = ServerHandler(server_ip_address, server_port)

        # verify the server is running
        server_running = server_handler.verify_server_running()
        self.assertTrue(server_running)
        if server_running:

            # create the test client
            test_client = TestClient(server_ip_address, server_port)

            # start the test_client and connect to the server
            test_client.start_client()

            # get the server web socket
            server_web_socket = server_handler.get_server_web_socket()

            # create the mock server_web_socket.send_message_wait_for_client_response
            mock_send_message = server_web_socket.send_message_wait_for_client_response = mock.MagicMock()

            # set the return values for the mock_send_message
            mock_send_message.side_effect = ["0000000004A37650540037TU^MO:ET/ID=25^ST:SU/TX=Success^DN:DN",
                                             "0000000004A37650540037TU^DG:403/ID=559/SQ=0/MX=8^ST:SU/TX=Success^DN:DN",
                                             '']

            # make a packet from the message above
            packet = server_web_socket.make_a_packet_string(controller_mac, message)
            ### test_case.py ###
            # verify that an exception is thrown when a success response is not returned for exit table programming
            with self.assertRaises(Exception):
                test_client_response = server_web_socket.send_packet_to_controller_wait_for_response(packet,
                                                                                                     controller_mac)

            # close the test client connection
            test_client.close_connection()

            # shutdown the server
            server_handler.shutdown_server()

    def test_send_packet_to_controller_wait_for_response_ts_packet_error_in_calling_method(self):
        # this test verifies that when a TS packet is sent and no controller mac address is given to
        # send_packet_to_controller_wait_for_response, an exception is thrown.

        message = "TS^DG:302/ID=559/SQ=0^MS:xxx0001/DS=test moisture sensor/LA=45.000/LG=116.44565/EN=TR^SL:TR^DN:DN"
        controller_mac = '0004A3765054'
        server_ip_address = '127.0.0.1'
        server_port = 9000

        #### confguration.py ####
        # create and start a server
        server_handler = ServerHandler(server_ip_address, server_port)

        # verify server is running
        server_running = server_handler.verify_server_running()
        self.assertTrue(server_running)
        if server_running:
            # create a test_client
            test_client = TestClient(server_ip_address, server_port)

            # start the test_client and connect to the server
            test_client.start_client()

            # get the server web socket - this should return the current server web socket
            server_web_socket = server_handler.get_server_web_socket()

            # create a mock method for server_web_socket.send_message_wait_for_client_response
            mock_send_message = server_web_socket.send_message_wait_for_client_response = mock.MagicMock()

            # set the side effect
            mock_send_message.side_effect = ["", "", ""]

            # make a packet from a message
            packet = server_web_socket.make_a_packet_string(controller_mac, message)

            ### test_case.py ###
            # verify that an exception is thrown when send_packet_to_controller_wait_for_response is called
            # with no controller mac address
            with self.assertRaises(Exception):
                test_client_response = server_web_socket.send_packet_to_controller_wait_for_response(packet)

            # close the test client connection
            test_client.close_connection()
            # shutdown the server
            server_handler.shutdown_server()