try:
    import unittest2 as unittest
except ImportError:
    import unittest
from common.objects.base_classes.old_data_group_packets import DataGroupPackets
import common.imports.opcodes as opcodes
import mock


class TestPacketBuilder(unittest.TestCase):
    server_address = "000000"
    controller_mac = '0004A3765054'

    def setUp(self):
        """ Setting up for the test. """
        # mock items

        test_name = self._testMethodName
        self.controller = mock.MagicMock()
        self.controller.mac = '123456ABC'
        self.controller.controller_type = '32'
        username = ''
        password = ''
        self.data_group_packets = DataGroupPackets(self.controller, username, password)
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")


    def test_build_tp_packet_enter_table_programming(self):
        header_id = 900
        # expected packet string
        expected_message_string = "TP^MO:ET/{0}={1}".format(
            opcodes.message_id,
            str(header_id)
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tp_packet_enter_table_programming()

        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_tp_packet_exit_table_programming(self):
        header_id = 950
        # expected packet string
        expected_message_string = "TP^MO:EX/{0}={1}".format(
            opcodes.message_id,
            str(header_id)
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tp_packet_exit_table_programming()

        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_tu_packet_exit_table_programming_success(self):
        header_id = 951
        # expected packet string
        expected_message_string = "TU^MO:EX/{0}={1}^{2}:{3}/{4}=Success".format(
            opcodes.message_id,
            str(header_id),
            opcodes.status,
            opcodes.status_success,
            opcodes.message_text
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tu_packet_exit_table_programming_success()

        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_tu_packet_enter_table_programming_success(self):
        header_id = 952
        # expected packet string
        expected_message_string = "TU^MO:ET/{0}={1}^{2}:{3}/{4}=Success^DN:DN".format(
            opcodes.message_id,
            str(header_id),
            opcodes.status,
            opcodes.status_success,
            opcodes.message_text
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tu_packet_enter_table_programming_success()

        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    ###################
    # Test packet 302 #
    ###################

    def test_build_request_packet_moisture_sensor_dg_302_all_moisture_sensors(self):
        header_id = 599
        # expected packet string
        expected_message_string = "TQ^DG:302/{0}={1}^{2}:AL".format(
            opcodes.message_id,
            str(header_id),
            opcodes.moisture_sensor
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_dg_302_get_all_moisture_sensors()
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_request_packet_moisture_sensor_dg_302_single_moisture_sensor(self):
        header_id = 600
        moisture_sensor = mock.MagicMock()
        moisture_sensor.sn = '12345'

        # expected packet string
        expected_message_string = "TQ^DG:302/{0}={1}^{2}:{3}".format(
            opcodes.message_id,
            str(header_id),
            opcodes.moisture_sensor,
            moisture_sensor.sn
        )
        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_dg_302_get_single_moisture_sensor(
            moisture_sensor=moisture_sensor)
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_ts_packet_moisture_sensor_dg_302_single_moisture_sensor(self):
        header_id = 500
        # expected packet string

        moisture_sensor_obj = mock.MagicMock()
        moisture_sensor_obj.ds = 'test table programming'
        moisture_sensor_obj.en = opcodes.false
        moisture_sensor_obj.la = 45.607999
        moisture_sensor_obj.lg = -119.2356
        moisture_sensor_obj.sn = '12345'

        ms_data = {}
        moisture_sensor_obj.data = ms_data

        # build the TS packet string
        packet_string = self.data_group_packets.build_ts_packet_dg_302_single_moisture_sensor(
                                                                            moisture_sensor_obj=moisture_sensor_obj)

        expected_message = "{0}^DG:302/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}".format(
            "TS",                           # {0}
            opcodes.message_id,             # {1}
            str(header_id),                 # {2}
            opcodes.header_sequence,        # {3}
            opcodes.moisture_sensor,        # {4}
            str(moisture_sensor_obj.sn),    # {5}
            opcodes.description,            # {6}
            str(moisture_sensor_obj.ds),    # {7}
            opcodes.latitude,               # {8}
            str(moisture_sensor_obj.la),    # {9}
            opcodes.longitude,              # {10}
            str(moisture_sensor_obj.lg),    # {11}
            opcodes.enabled,                # {12}
            moisture_sensor_obj.en,         # {13}
        )

        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message)

    def test_verify_packet_302_key_values(self):

        test_engine_object = mock.MagicMock()
        test_engine_object.data = mock.MagicMock()
        parsed_packet = mock.MagicMock()
        test_engine_object.verify_description = mock.MagicMock()
        test_engine_object.verify_serial_number = mock.MagicMock()
        test_engine_object.verify_latitude = mock.MagicMock()
        test_engine_object.verify_longitude = mock.MagicMock()

        self.data_group_packets.verify_packet_dg_302_key_values(parsed_packet=parsed_packet,
                                                                test_engine_moisture_sensor=test_engine_object)

        self.assertEquals(1, test_engine_object.verify_description.call_count)
        self.assertEquals(1, test_engine_object.verify_serial_number.call_count)
        self.assertEquals(1, test_engine_object.verify_latitude.call_count)
        self.assertEquals(1, test_engine_object.verify_longitude.call_count)

    ##############
    # Packet 332 #
    ##############

    def test_build_tq_packet_dg_332_get_all_flowstation_settings(self):
        header_id = 415
        # expected packet string
        packet_type = "TQ"
        expected_message_string = "{0}^DG:332/{1}={2}^{3}:{4}".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.flow_station,       # {3}
            opcodes.all                 # {4}
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_dg_332_get_all_flowstation_settings()
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_tq_packet_dg_332_get_one_flowstation_settings(self):
        header_id = 403
        flow_station_number = 1
        # expected packet string
        packet_type = "TQ"
        expected_message_string = "{0}^DG:332/{1}={2}^{3}:{4}".format(
            packet_type,                        # {0}
            opcodes.message_id,                 # {1}
            str(header_id),                     # {2}
            opcodes.flow_station,               # {3}
            flow_station_number                 # {4}
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_dg_332_get_one_flowstation_settings(flow_station_number)
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_ts_packet_dg_332_flowstation_settings(self):
        header_id = 712
        # expected packet string

        mock_flow_station = mock.MagicMock()
        mock_flow_station.ad = '10.11.12.245'
        mock_flow_station.la = '45.654452'
        mock_flow_station.lg = '-116.155568'

        flow_station_address= 5
        flow_station_description = 'flowstation 12345'
        flow_station_enabled = opcodes.false

        packet_type = "TS"
        # build the message
        expected_message_string = "{0}^DG:332/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}/{14}={15}^{16}:{17}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.flow_station,                   # {4}
            str(flow_station_address),              # {5}
            opcodes.description,                    # {6}
            str(flow_station_description),          # {7}
            opcodes.enabled,                        # {8}
            str(flow_station_enabled),              # {9}
            opcodes.ip_address,                     # {10}
            str(mock_flow_station.ad),              # {11}
            opcodes.latitude,                       # {12}
            str(mock_flow_station.la),              # {13}
            opcodes.longitude,                      # {14}
            str(mock_flow_station.lg),              # {15}
            opcodes.sequence_last,                  # {16}
            opcodes.true,                           # {17}
        )

        # build the TS packet string
        packet_string = self.data_group_packets.build_ts_packet_dg_332_flowstation_settings(flow_station=mock_flow_station,
                                                                                            flowstation_address=flow_station_address,
                                                                                            description=flow_station_description,
                                                                                            enabled=flow_station_enabled)
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    ##############
    # Packet 402 #
    ##############

    def test_build_tq_packet_moisture_sensor_dg_402_get_all_water_sources(self):
        header_id = 500
        # expected packet string
        expected_message_string = "TQ^DG:402/{0}={1}^{2}:AL".format(
            opcodes.message_id,
            str(header_id),
            opcodes.water_source
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_dg_402_get_all_water_sources()
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_tq_packet_water_source_dg_402_get_single_water_source(self):
        header_id = 600
        water_source_address = 1
        water_source = mock.MagicMock()
        water_source.ad = water_source_address

        # expected packet string
        expected_message_string = "TQ^DG:402/{0}={1}^{2}:{3}".format(
            opcodes.message_id,
            str(header_id),
            opcodes.water_source,
            water_source_address
        )
        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_dg_402_get_single_water_source(
                                                                                                water_source)
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_ts_packet_water_source_dg_402_single_water_source(self):
        header_id = 700
        # expected packet string

        water_source = mock.MagicMock()
        water_source.ds = 'test water source'
        water_source.en = opcodes.false
        water_source.pr = 3
        water_source.wb = 5000
        water_source.wr = opcodes.false
        water_source.pc = 1
        water_source.sh = opcodes.true
        water_source.ws = opcodes.false
        water_source.controller = self.controller

        # build the TS packet string
        packet_string = self.data_group_packets.build_ts_packet_dg_402_single_water_source(
                                                            water_source_obj=water_source)

        message = "{0}^DG:402/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}/{14}={15}/{16}={17}".format(
            'TS',                               # {0}
            opcodes.message_id,                 # {1}
            str(header_id),                     # {2}
            opcodes.header_sequence,            # {3}
            opcodes.water_source,               # {4}
            str(water_source.ad),               # {5}
            opcodes.description,                # {6}
            str(water_source.ds),               # {7}
            opcodes.enabled,                    # {8}
            water_source.en,                    # {9}
            opcodes.monthly_water_budget,       # {10}
            str(water_source.wb),               # {11}
            opcodes.water_rationing_enable,     # {12}
            str(water_source.wr),               # {13}
            opcodes.shutdown_on_over_budget,    # {14}
            water_source.ws,                    # {15}
            opcodes.priority,                   # {16}
            str(water_source.pr),               # {17}
        )

        message += "/{0}={1}/{2}={3}".format(
            opcodes.flow_station,           # {0}
            water_source.sh,                # {1}
            opcodes.point_of_control,       # {2}
            water_source.pc                 # {3}
        )

        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, message)

    def test_verify_packet_402_key_values(self):

        test_engine_object = mock.MagicMock()
        test_engine_object.controller = self.controller

        test_engine_object.data = mock.MagicMock()
        parsed_packet = mock.MagicMock()
        test_engine_object.verify_description = mock.MagicMock()
        test_engine_object.verify_enabled_state = mock.MagicMock()
        test_engine_object.verify_priority = mock.MagicMock()
        test_engine_object.verify_monthly_budget = mock.MagicMock()
        test_engine_object.verify_water_rationing = mock.MagicMock()
        test_engine_object.verify_point_of_control = mock.MagicMock()
        test_engine_object.verify_share_with_flow_station = mock.MagicMock()

        self.data_group_packets.verify_packet_dg_402_key_values(parsed_packet=parsed_packet,
                                                                test_engine_water_source=test_engine_object)

        self.assertEquals(1, test_engine_object.verify_description.call_count)
        self.assertEquals(1, test_engine_object.verify_enabled_state.call_count)
        self.assertEquals(1, test_engine_object.verify_priority.call_count)
        self.assertEquals(1, test_engine_object.verify_monthly_budget.call_count)
        self.assertEquals(1, test_engine_object.verify_water_rationing.call_count)
        self.assertEquals(1, test_engine_object.verify_point_of_control.call_count)
        self.assertEquals(1, test_engine_object.verify_share_with_flow_station.call_count)

    ##############
    # Packet 412 #
    ##############

    def test_build_tq_packet_dg_412_get_all_points_of_control(self):
        header_id = 400
        # expected packet string
        expected_message_string = "TQ^DG:412/{0}={1}^{2}:AL".format(
            opcodes.message_id,
            str(header_id),
            opcodes.point_of_control
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_dg_412_get_all_points_of_control()
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_tq_packet_dg_412_get_single_point_of_control(self):
        header_id = 401
        poc_address = 1
        poc = mock.MagicMock()
        poc.ad = poc_address

        # expected packet string
        expected_message_string = "TQ^DG:412/{0}={1}^{2}:{3}".format(
            opcodes.message_id,
            str(header_id),
            opcodes.point_of_control,
            str(poc_address)
        )
        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_dg_412_get_single_point_of_control(poc)
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_ts_packet_dg_412_point_of_control(self):
        header_id = 701
        # expected packet string

        flow_meter_serial_number = 'FM12345'
        pump_serial_number = 'PM12345'
        master_valve_serial_number = 'MV12345'
        pressure_sensor_serial_number = 'PS12345'

        flow_meter = mock.MagicMock()
        flow_meter.sn = flow_meter_serial_number
        flow_meters = {}
        flow_meters[1] = flow_meter
        self.controller.flow_meters = flow_meters

        pump = mock.MagicMock()
        pump.sn = pump_serial_number
        pumps = {}
        pumps[1] = pump
        self.controller.pumps = pumps

        master_valve = mock.MagicMock()
        master_valve.sn =  master_valve_serial_number
        master_valves = {}
        master_valves[1] = master_valve
        self.controller.master_valves = master_valves

        pressure_sensor = mock.MagicMock()
        pressure_sensor.sn = pressure_sensor_serial_number
        pressure_sensors = {}
        pressure_sensors[1] = pressure_sensor
        self.controller.pressure_sensors = pressure_sensors


        poc = mock.MagicMock()
        poc.controller = self.controller
        poc.ad = '1'
        poc.ds = 'test water source'
        poc.en = opcodes.false
        poc.fl = 100.0
        poc.fm = 1
        poc.pp = 1
        poc.hf = 500.6
        poc.hs = opcodes.false
        poc.mv = 1
        poc.uf = 100.2
        poc.us = opcodes.true
        poc.pr = 3
        poc.wb = 5000
        poc.wr = opcodes.true
        poc.pc = 1
        poc.sh = opcodes.true
        poc.ws = opcodes.true
        poc.ps = 1
        poc.hp = 100
        poc.he = opcodes.true
        poc.lp = 600
        poc.le = opcodes.true
        poc.hf = 700
        poc.sh = opcodes.true
        poc.ml = 9

        # build the TS packet string
        actual_message = self.data_group_packets.build_ts_packet_dg_412_point_of_control(point_of_control_obj=poc)
        packet_type = "TS"
        expected_message =   "{0}^DG:412/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}/{14}={15}" \
                             "/{16}={17}/{18}={19}/{20}={21}/{22}={23}/{24}={25}/{26}={27}/{28}={29}/{30}={31}" \
                             "/{32}={33}/{34}={35}/{36}={37}".format(
            packet_type,                        # {0}
            opcodes.message_id,                 # {1}
            str(header_id),                     # {2}
            opcodes.header_sequence,            # {3}
            opcodes.point_of_control,           # {4}
            str(poc.ad),                        # {5}
            opcodes.description,                # {6}
            str(poc.ds),                        # {7}
            opcodes.enabled,                    # {8}
            str(poc.en),                        # {9}
            opcodes.target_flow,                # {10}
            str(poc.fl),                        # {11}
            opcodes.flow_meter,                 # {12}
            str(flow_meter_serial_number),      # {13}
            opcodes.pump_on_poc,                # {14}
            str(pump_serial_number),            # {15}
            opcodes.high_flow_limit,            # {16}
            str(poc.hf),                        # {17}
            opcodes.shutdown_on_high_flow,      # {18}
            str(poc.hs),                        # {19}
            opcodes.master_valve,               # {20}
            str(master_valve_serial_number),    # {21}
            opcodes.unscheduled_flow_limit,     # {22}
            str(poc.uf),                        # {23}
            opcodes.unscheduled_flow_shutdown,  # {24}
            str(poc.us),                        # {25}
            opcodes.pressure_sensor_ps,         # {26}
            str(pressure_sensor_serial_number), # {27}
            opcodes.high_pressure_limit,        # {28}
            str(poc.hp),                        # {29}
            opcodes.high_pressure_shutdown,     # {30}
            str(poc.he),                        # {31}
            opcodes.low_pressure_limit,         # {32}
            str(poc.lp),                        # {33}
            opcodes.low_pressure_shutdown,      # {34}
            str(poc.le),                        # {35}
            opcodes.high_flow_limit,            # {36}
            str(poc.hf),                        # {37}
        )

        if poc.controller.controller_type == '32':
            expected_message += "/{0}={1}/{2}={3}".format(
                opcodes.flow_station,           # {0}
                poc.sh,                         # {1}
                opcodes.mainline,               # {2}
                poc.ml                          # {3}
            )

        # verify the built packet string against the expected packet string
        self.assertEquals(actual_message, expected_message)

    def test_verify_packet_412_key_values(self):

        test_engine_poc = mock.MagicMock()
        test_engine_poc.controller = self.controller
        test_engine_poc.ad = 1
        parsed_packet = mock.MagicMock()

        test_engine_poc.verify_description = mock.MagicMock()
        test_engine_poc.verify_enabled_state = mock.MagicMock()
        test_engine_poc.verify_target_flow = mock.MagicMock()
        test_engine_poc.verify_flow_meter_serial_number = mock.MagicMock()
        test_engine_poc.verify_pump_serial_number = mock.MagicMock()
        test_engine_poc.verify_high_flow_limit = mock.MagicMock()
        test_engine_poc.verify_shutdown_on_high_flow = mock.MagicMock()

        test_engine_poc.verify_master_valve_serial_number = mock.MagicMock()
        test_engine_poc.verify_unscheduled_flow_limit = mock.MagicMock()
        test_engine_poc.verify_shutdown_on_unscheduled = mock.MagicMock()
        test_engine_poc.verify_pressure_sensor_serial_number = mock.MagicMock()
        test_engine_poc.verify_high_pressure_limit = mock.MagicMock()
        test_engine_poc.verify_high_pressure_shutdown = mock.MagicMock()
        test_engine_poc.verify_low_pressure_shutdown = mock.MagicMock()
        test_engine_poc.verify_mainline_on_point_of_control = mock.MagicMock()

        self.data_group_packets.verify_packet_dg_412_key_values(parsed_packet=parsed_packet,
                                                                test_engine_poc=test_engine_poc)

        self.assertEquals(1, test_engine_poc.verify_description.call_count)
        self.assertEquals(1, test_engine_poc.verify_enabled_state.call_count)
        self.assertEquals(1, test_engine_poc.verify_target_flow.call_count)
        self.assertEquals(1, test_engine_poc.verify_flow_meter_serial_number.call_count)
        self.assertEquals(1, test_engine_poc.verify_pump_serial_number.call_count)
        self.assertEquals(1, test_engine_poc.verify_high_flow_limit.call_count)
        self.assertEquals(1, test_engine_poc.verify_master_valve_serial_number.call_count)
        self.assertEquals(1, test_engine_poc.verify_unscheduled_flow_limit.call_count)
        self.assertEquals(1, test_engine_poc.verify_shutdown_on_unscheduled.call_count)
        self.assertEquals(1, test_engine_poc.verify_pressure_sensor_serial_number.call_count)
        self.assertEquals(1, test_engine_poc.verify_high_pressure_limit.call_count)
        self.assertEquals(1, test_engine_poc.verify_high_pressure_shutdown.call_count)
        self.assertEquals(1, test_engine_poc.verify_low_pressure_shutdown.call_count)
        self.assertEquals(1, test_engine_poc.verify_mainline_on_point_of_control.call_count)

    ##############
    # Packet 501 #
    ##############

    def test_build_tq_dg_501_get_all_water_source_assignments(self):
        header_id = 400
        # expected packet string
        packet_type = "TQ"
        expected_message_string = "{0}^DG:501/{1}={2}^{3}:{4}^DN:DN".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.water_source,       # {3}
            opcodes.all                 # {4}
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_dg_501_get_all_water_source_assignments()
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_ts_dg_501_water_source_assignment(self):
        header_id = 701
        # expected packet string

        controller = mock.MagicMock()
        controller.mac = '123456ABC'
        flow_station_slot = 5
        controller_slot = 5


        # build the TS packet string
        packet_string = self.data_group_packets.build_ts_packet_dg_501_water_source_assignment(controller=controller,
                                                                                             flow_station_slot=flow_station_slot,
                                                                                               controller_slot=controller_slot
                                                                                               )
        packet_type = "TS"
        # build the message
        expected_message_string = "{0}^DG:501/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}^{10}:{11}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.water_source,                   # {4}
            str(flow_station_slot),                 # {5}
            opcodes.controller,                     # {6}
            str(controller.mac),                    # {7}
            opcodes.water_source,                   # {8}
            str(controller_slot),                   # {9}
            opcodes.sequence_last,                  # {10}
            opcodes.true,                           # {11}
        )

        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    ##############
    # Packet 511 #
    ##############

    def test_build_tq_dg_511_get_all_point_of_control_assignments(self):
        header_id = 402
        # expected packet string
        packet_type = "TQ"
        expected_message_string = "{0}^DG:511/{1}={2}^{3}:{4}^DN:DN".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.point_of_control,   # {3}
            opcodes.all                 # {4}
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_dg_511_get_all_point_of_control_assignments()
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_tq_dg_511_get_one_point_of_control_assignment(self):
        header_id = 403
        point_of_control_slot_number = 1
        # expected packet string
        packet_type = "TQ"
        expected_message_string = "{0}^DG:511/{1}={2}^{3}:{4}^DN:DN".format(
            packet_type,                        # {0}
            opcodes.message_id,                 # {1}
            str(header_id),                     # {2}
            opcodes.point_of_control,           # {3}
            point_of_control_slot_number        # {4}
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_dg_511_get_one_point_of_control_assignment(point_of_control_slot_number)
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_ts_dg_511_point_of_control_assignment(self):
        header_id = 712
        # expected packet string

        controller = mock.MagicMock()
        controller.mac = '123456ABC'
        flow_station_slot = 5
        controller_slot = 5

        packet_type = "TS"
        # build the message
        expected_message_string = "{0}^DG:511/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}^{10}:{11}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.point_of_control,               # {4}
            str(flow_station_slot),                 # {5}
            opcodes.controller,                     # {6}
            str(controller.mac),                    # {7}
            opcodes.point_of_control,               # {8}
            str(controller_slot),                   # {9}
            opcodes.sequence_last,                  # {10}
            opcodes.true,                           # {11}
        )



        # build the TS packet string
        packet_string = self.data_group_packets.build_ts_packet_dg_511_point_of_control_assignment(controller=controller,
                                                                                                   flow_station_slot=flow_station_slot,
                                                                                                   controller_slot=controller_slot
                                                                                                   )
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_tq_packet_mainline_dg_422_get_all_mainlines(self):
        header_id = 2500
        # expected packet string
        expected_message_string = "TQ^DG:422/{0}={1}^{2}:AL".format(
            opcodes.message_id,
            str(header_id),
            opcodes.mainline
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_mainline_dg_422_get_all_mainlines()
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_tq_packet_mainline_dg_422_get_single_mainline(self):
        header_id = 2600
        mainline_address = 1
        mainline = mock.MagicMock()
        mainline.ad = mainline_address

        # expected packet string
        expected_message_string = "TQ^DG:422/{0}={1}^{2}:{3}".format(
            opcodes.message_id,
            str(header_id),
            opcodes.mainline,
            mainline_address
        )
        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_mainline_dg_422_get_single_mainline(mainline_address)
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_ts_packet_mainline_dg_422_single_mainline(self):
        header_id = 2700
        # expected packet string
        pipe_fill_time_minutes = 2
        pipe_fill_time_seconds = pipe_fill_time_minutes * 60
        mainline = mock.MagicMock()
        # Address
        mainline.ad = 6
        # Description
        mainline.ds = 'test mainline'
        # Enabled
        mainline.en = opcodes.false
        # Target flow / design flow value
        mainline.fl = 556688
        # Pipe fill time
        mainline.ft = pipe_fill_time_minutes
        # Stable flow pressure value
        mainline.sp = 10
        # Use pressure for stable flow
        mainline.up = opcodes.false
        # Use time for stable flow
        mainline.ut = opcodes.true
        # Dynamic flow allocation (Flowstation only)
        mainline.fp = opcodes.false
        # Standard variance shutdown enabled
        mainline.fw = opcodes.false
        # Standard variance limit
        mainline.fv = 0
        # Mainline variance limit
        mainline.mf = 0
        # Mainline variance shutdown enabled
        mainline.mw = opcodes.false
        # Priority (Flowstation only)
        mainline.pr = 3
        # Water out to these mainlines
        mainline.ml = 1
        # Water out to these POCs
        mainline.pc = 1
        # Time delay between MVs and zone
        mainline.tm = 0
        # Time delay between zones
        mainline.tz = 0
        # Share with Flow Station
        mainline.sh = opcodes.false
        # Managed by flowstation (3200 only)
        mainline.fs = opcodes.false
        # Time delay after last zone off to MV off
        mainline.tl = 0
        # Number of zones to delay
        mainline.zc = 0
        # Limit concurrent zones by flow
        mainline.lc = opcodes.false
        # Use advanced flow
        mainline.af = opcodes.false
        # High variance limit
        mainline.ah = 0
        # Low variance limit
        mainline.al = 0
        # High variance limit
        mainline.bh = 0
        # Low variance limit
        mainline.bl = 0
        # High variance limit
        mainline.ch = 0
        # Low variance limit
        mainline.cl = 0
        # High variance limit
        mainline.dh = 0
        # Low variance limit
        mainline.dl = 0
        # Zone high variance detection & shutdown
        mainline.zh = opcodes.false
        # Zone low variance detection & shutdown
        mainline.zl = opcodes.false
        # Delay Units (PS = Pressure, TM = Time)
        mainline.un = opcodes.time

        mainline.controller = self.controller

        # build the TS packet string
        packet_string = self.data_group_packets.build_ts_packet_mainline_dg_422_single_mainline(
                                                            mainline_obj=mainline)

        message = ("{0}^DG:422/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}/{14}={15}/{16}={17}/{18}={19}"
                  "/{20}={21}/{22}={23}/{24}={25}/{26}={27}/{28}={29}/{30}={31}/{32}={33}/{34}={35}/{36}={37}/{38}={39}"
                  "/{40}={41}/{42}={43}/{44}={45}/{46}={47}"
                  "/{48}={49}/{50}={51}/{52}={53}/{54}={55}/{56}={57}/{58}={59}/{60}={61}".format(
            'TS',                                            # {0}
            opcodes.message_id,                              # {1}
            str(header_id),                                  # {2}
            opcodes.header_sequence,                         # {3}
            opcodes.mainline,                                # {4}
            str(mainline.ad),                                # {5}
            opcodes.description,                             # {6}
            str(mainline.ds),                                # {7}
            opcodes.enabled,                                 # {8}
            mainline.en,                                     # {9}
            opcodes.target_flow,                             # {10}
            str(mainline.fl),                                # {11}
            opcodes.fill_time,                               # {12}
            str(pipe_fill_time_seconds),                     # {13}
            opcodes.stable_flow_pressure_value,              # {14}
            str(mainline.sp),                                # {15}
            opcodes.use_pressure_for_stable_flow,            # {16}
            str(mainline.up),                                # {17}
            opcodes.use_time_for_stable_flow,                # {18}
            str(mainline.ut),                                # {19}
            opcodes.standard_variance_shutdown,              # {20}
            str(mainline.fw),                                # {21}
            opcodes.standard_variance_limit,                 # {22}
            str(mainline.fv),                                # {23}
            opcodes.mainline_variance_limit,                 # {24}
            str(mainline.mf),                                # {25}
            opcodes.mainline_variance_shutdown,              # {26}
            str(mainline.mw),                                # {27}
            opcodes.delay_first_zone,                        # {28}
            str(mainline.tm),                                # {29}
            opcodes.delay_for_next_zone,                     # {30}
            str(mainline.tz),                                # {31}
            opcodes.delay_after_zone,                        # {32}
            str(mainline.tl),                                # {33}
            opcodes.number_of_zones_to_delay,                # {34}
            str(mainline.zc),                                # {35}
            opcodes.limit_zones_by_flow,                     # {36}
            str(mainline.lc),                                # {37}
            opcodes.use_advanced_flow,                       # {38}
            str(mainline.af),                                # {39}
            opcodes.high_variance_limit_tier_one,            # {40}
            str(mainline.ah),                                # {41}
            opcodes.high_variance_limit_tier_two,            # {42}
            str(mainline.bh),                                # {43}
            opcodes.high_variance_limit_tier_three,          # {44}
            str(mainline.ch),                                # {45}
            opcodes.high_variance_limit_tier_four,           # {46}
            str(mainline.dh),                                # {47}
            opcodes.low_variance_limit_tier_one,             # {48}
            str(mainline.al),                                # {49}
            opcodes.low_variance_limit_tier_two,             # {50}
            str(mainline.bl),                                # {51}
            opcodes.low_variance_limit_tier_three,           # {52}
            str(mainline.cl),                                # {53}
            opcodes.low_variance_limit_tier_four,            # {54}
            str(mainline.dl),                                # {55}
            opcodes.zone_high_variance_detection_shutdown,   # {56}
            str(mainline.zh),                                # {57}
            opcodes.zone_low_variance_detection_shutdown,    # {58}
            str(mainline.zl),                                # {59}
            opcodes.pipe_fill_units,                         # {60}
            str(mainline.un),                                # {61}
        ))

        if mainline.controller.controller_type == '32':
            message += "/{0}={1}".format(
                opcodes.flow_station,       # {0}
                mainline.sh                           # {1}
            )

        if mainline.controller.controller_type == 'FS':
            message += "/{0}={1}/{2}={3}/{4}={5}/{6}={7}".format(
                opcodes.priority,                     # {0}
                str(mainline.pr),                     # {1}
                opcodes.mainline,                     # {2}
                str(mainline.ml),                     # {3}
                opcodes.point_of_control,             # {4}
                str(mainline.pc),                     # {5}
                opcodes.use_dynamic_flow_allocation,  # {6}
                str(mainline.fp)                      # {7}
            )

        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, message)

    def test_verify_packet_422_key_values(self):

        test_engine_object = mock.MagicMock()
        test_engine_object.controller = self.controller

        test_engine_object.data = mock.MagicMock()
        parsed_packet = mock.MagicMock()

        test_engine_object.verify_enabled_state = mock.MagicMock()
        test_engine_object.verify_description = mock.MagicMock()

        test_engine_object.verify_target_flow = mock.MagicMock()
        test_engine_object.verify_limit_zones_by_flow_state = mock.MagicMock()
        test_engine_object.verify_share_with_flowstation = mock.MagicMock()
        test_engine_object.verify_standard_variance_shutdown_enabled = mock.MagicMock()
        test_engine_object.verify_standard_variance_limit = mock.MagicMock()

        test_engine_object.verify_pipe_fill_units = mock.MagicMock()

        test_engine_object.verify_delay_first_zone = mock.MagicMock()
        test_engine_object.verify_delay_for_next_zone = mock.MagicMock()
        test_engine_object.verify_delay_after_zone = mock.MagicMock()
        test_engine_object.verify_number_of_zones_to_delay = mock.MagicMock()

        test_engine_object.verify_advanced_flow_variance_setting = mock.MagicMock()

        test_engine_object.verify_high_variance_limit_tier_one = mock.MagicMock()
        test_engine_object.verify_low_variance_limit_tier_one = mock.MagicMock()
        test_engine_object.verify_high_variance_limit_tier_two = mock.MagicMock()
        test_engine_object.verify_low_variance_limit_tier_two = mock.MagicMock()
        test_engine_object.verify_high_variance_limit_tier_three = mock.MagicMock()
        test_engine_object.verify_low_variance_limit_tier_three = mock.MagicMock()
        test_engine_object.verify_high_variance_limit_tier_four = mock.MagicMock()
        test_engine_object.verify_low_variance_limit_tier_four = mock.MagicMock()
        test_engine_object.verify_standard_low_variance_detection_shutdown = mock.MagicMock()
        test_engine_object.verify_standard_high_variance_detection_shutdown = mock.MagicMock()

        self.data_group_packets.verify_packet_422_key_values(parsed_packet=parsed_packet,
                                                             mainline_address=5,
                                                             test_engine_mainline=test_engine_object)

        test_engine_object.verify_enabled_state.assert_called_once()
        test_engine_object.verify_description.assert_called_once()

        test_engine_object.verify_target_flow.assert_called_once()
        test_engine_object.verify_limit_zones_by_flow_state.assert_called_once()
        test_engine_object.verify_share_with_flowstation.assert_called_once()
        test_engine_object.verify_standard_variance_shutdown_enabled.assert_called_once()
        test_engine_object.verify_standard_variance_limit.assert_called_once()

        test_engine_object.verify_pipe_fill_units.assert_called_once()

        test_engine_object.verify_delay_first_zone.assert_called_once()
        test_engine_object.verify_delay_for_next_zone.assert_called_once()
        test_engine_object.verify_delay_after_zone.assert_called_once()
        test_engine_object.verify_number_of_zones_to_delay.assert_called_once()

        test_engine_object.verify_advanced_flow_variance_setting.assert_called_once()

        test_engine_object.verify_high_variance_limit_tier_one.assert_called_once()
        test_engine_object.verify_low_variance_limit_tier_one.assert_called_once()
        test_engine_object.verify_high_variance_limit_tier_two.assert_called_once()
        test_engine_object.verify_low_variance_limit_tier_two.assert_called_once()
        test_engine_object.verify_high_variance_limit_tier_three.assert_called_once()
        test_engine_object.verify_low_variance_limit_tier_three.assert_called_once()
        test_engine_object.verify_high_variance_limit_tier_four.assert_called_once()
        test_engine_object.verify_low_variance_limit_tier_four.assert_called_once()
        test_engine_object.verify_standard_low_variance_detection_shutdown.assert_called_once()
        test_engine_object.verify_standard_high_variance_detection_shutdown.assert_called_once()


    ##############
    # Packet 521 #
    ##############

    def test_build_tq_dg_521_get_all_mainline_assignments(self):
        header_id = 400
        # expected packet string
        packet_type = "TQ"
        expected_message_string = "{0}^DG:521/{1}={2}^{3}:{4}^DN:DN".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.mainline,       # {3}
            opcodes.all                 # {4}
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_dg_521_get_all_mainline_assignments()
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_ts_dg_521_mainline_assignment(self):
        header_id = 701
        # expected packet string

        controller = mock.MagicMock()
        controller.mac = '123456ABC'
        flow_station_slot = 5
        controller_slot = 5


        # build the TS packet string
        packet_string = self.data_group_packets.build_ts_dg_521_mainline_assignment(controller=controller,
                                                                                    flow_station_slot=flow_station_slot,
                                                                                    controller_slot=controller_slot
                                                                                   )
        packet_type = "TS"
        # build the message
        expected_message_string = "{0}^DG:521/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}^{10}:{11}^{12}:{13}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.mainline,                       # {4}
            str(flow_station_slot),                 # {5}
            opcodes.controller,                     # {6}
            str(controller.mac),                    # {7}
            opcodes.mainline,                       # {8}
            str(controller_slot),                   # {9}
            opcodes.sequence_last,                  # {10}
            opcodes.true,                           # {11}
            opcodes.done,                           # {12}
            opcodes.done                            # {13}
        )

        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)


    ##############
    # Packet 500 #
    ##############

    def test_build_tq_dg_500_get_flowstation_setting(self):
        header_id = 400
        # expected packet string
        packet_type = "TQ"
        expected_message_string = "{0}^DG:500/{1}={2}^DN:DN".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_dg_500_get_flowstation_settings()
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_ts_dg_500_flowstation_settings(self):
        header_id = 701
        # expected packet string

        flowstation = mock.MagicMock()
        flowstation.sn = '1X12345'
        flowstation.latitude = 90.1234
        flowstation.longitude = -90.6789


        # build the TS packet string
        packet_string = self.data_group_packets.build_ts_dg_500_flowstation_settings(flowstation=flowstation)
        packet_type = "TS"
        # build the message
        expected_message_string = "{0}^DG:500/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}/{10}={11}^{12}:{13}^{14}:{15}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.flow_station,                   # {4}
            str(flowstation.sn),                    # {5}
            opcodes.description,                    # {6}
            str(flowstation.ds),                    # {7}
            opcodes.latitude,                       # {8}
            str(flowstation.latitude),              # {9}
            opcodes.longitude,                      # {10}
            flowstation.longitude,                  # {11}
            opcodes.sequence_last,                  # {12}
            opcodes.true,                           # {13}
            opcodes.done,                           # {14}
            opcodes.done                            # {15}
        )

        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)


    ##############
    # Packet 570 #
    ##############
    
    def test_build_tq_dg_570_get_all_controller_assignment(self):
        header_id = 400
        # expected packet string
        packet_type = "TQ"
        expected_message_string = "{0}^DG:570/{1}={2}".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
        )
    
        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_dg_570_get_all_controller_assignments()
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_ts_dg_570_controller_assignment(self):
        header_id = 500
        # expected packet string
    
        controller = mock.MagicMock()
        controller.mac = '000812345678'
        controller.sn = '3X1234'
        assigned_flowstation_slot = 5

        packet_type = "TS"
        # build the message
        expected_message_string  = "{0}^DG:570/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}^{10}:{11}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.mac_address,                    # {4}
            controller.mac,                         # {5}
            opcodes.controller,                     # {6}
            assigned_flowstation_slot,              # {7}
            opcodes.serial_number,                  # {8}
            controller.sn,                          # {9}
            opcodes.sequence_last,                  # {10}
            opcodes.true,                           # {11}
        )
    
        packet_string = self.data_group_packets.build_ts_packet_dg_570_controller_assignment(controller=controller,
                                                                                             controller_slot=assigned_flowstation_slot)

        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)


    ##############
    # Packet 571 #
    ##############
    
    def test_build_tq_dg_571_get_all_controller_settings(self):
        header_id = 400
        # expected packet string
        packet_type = "TQ"
        expected_message_string = "{0}^DG:571/{1}={2}^{3}:{4}^DN:DN".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.controller,         # {3}
            opcodes.all                 # {4}
        )
    
        # build the packet string
        packet_string = self.data_group_packets.build_tq_dg_571_get_all_controller_settings()
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)
    
    def test_build_tq_dg_571_get_controller_settings(self):
        header_id = 600
        controller = mock.MagicMock()
        controller.mac = "000812345678"
        # expected packet string
        packet_type = "TQ"
        expected_message_string = "{0}^DG:571/{1}={2}^{3}:{4}^DN:DN".format(
            packet_type,             # {0}
            opcodes.message_id,      # {1}
            str(header_id),          # {2}
            opcodes.controller,      # {3}
            controller.mac           # {4}
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_packet_dg_571_get_single_controller_setting(controller=controller)
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_ts_dg_571_controller_settings(self):
        header_id = 701
        # expected packet string
    
        controller = mock.MagicMock()
        controller.mac = '000812345678'
        enabled = True

        packet_type = "TS"
        enabled_str = "TR" if enabled else "FA"
        # build the message
        expected_message_string  = "{0}^DG:571/{1}={2}/{3}=0^{4}:{5}/{6}={7}^{8}:{9}^{10}:{11}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.controller,                     # {4}
            controller.mac,                         # {5}
            opcodes.enabled,                        # {6}
            enabled_str,                            # {7}
            opcodes.sequence_last,                  # {8}
            opcodes.true,                           # {9}
            opcodes.done,                           # {10}
            opcodes.done                            # {11}
        )
    
        packet_string = self.data_group_packets.build_ts_dg_571_controller_settings(controller=controller, enabled=enabled)

        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    ##############
    # Packet 403 #
    ##############

    def test_build_tq_dg_403_get_all_empty_conditions(self):
        header_id = 1500
        # expected packet string
        packet_type = "TQ"
        expected_message_string = "{0}^DG:403/{1}={2}/SQ=1^{3}:{4}".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.water_source,       # {3}
            opcodes.all                 # {4}
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_dg_403_get_Empty_Conditions_for_all_water_sources()
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

    def test_build_tq_dg_403_get_all_empty_conditions_for_a_water_source(self):
        header_id = 1600
        water_source_address = 1
        # expected packet string
        packet_type = "TQ"
        expected_message_string = "{0}^DG:403/{1}={2}/SQ=1^{3}:{4}".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.water_source,       # {3}
            water_source_address        # {4}
        )

        # build the packet string
        packet_string = self.data_group_packets.build_tq_dg_403_get_Empty_Conditions_for_a_water_source(
                                                                            water_source_address=water_source_address)
        # verify the built packet string against the expected packet string
        self.assertEquals(packet_string, expected_message_string)

