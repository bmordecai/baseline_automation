from unittest import TestCase
import mock
from serial import Serial
from common.objects.controllers.bl_32 import BaseStation3200
from common.imports import opcodes
from common.objects.base_classes.basemanager_connections import BaseManagerConnection
from common.imports.types import ControllerCommands
from common.imports.types import ActionCommands


class TestBaseManagerConnections(TestCase):

    def setUp(self):
        # Create the mock_ser object
        self.mock_ser = mock.MagicMock(spec=Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        ip_address = "192.168.100.100"
        port_address = "192.168.100.101"
        socket_port = 1067

        self.controller = BaseStation3200(_mac="12345",
                                          _serial_number="3K0001",
                                          _firmware_version="",
                                          _serial_port=self.mock_ser,
                                          _port_address=port_address,
                                          _socket_port=socket_port,
                                          _ip_address=ip_address)

    def create_test_base_manager_connection(self):

        base_manager_connection = BaseManagerConnection(self.controller)

        return base_manager_connection

    def test_set_default_values_happy_path(self):
        """ Verify that set_default_values sends the correct command to the controller with a value in self.ai
        and self.ua = True"""
        bm_connection = self.create_test_base_manager_connection()
        bm_connection.ai = "127.0.256"
        bm_connection.ua = opcodes.true
        expected_command = "{0},{1},{2}={3},{4}={5}".format(
            ActionCommands.SET,                     # {0}
            opcodes.basemanager,                    # {1}
            opcodes.alternate_server_ip,            # {2}
            bm_connection.ai,                       # {3}
            opcodes.use_alternate_server_ip,        # {4}
            opcodes.true,                           # {5}
        )

        bm_connection.set_default_values()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    def test_set_default_values_fail(self):
        """ Verify that message when set_default_values has an exception when sending a command to the controller """
        exception_message = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_message)
        bm_connection = self.create_test_base_manager_connection()
        ai = ""
        expected_command = "{0},{1},{2}={3},{4}={5}".format(
            ActionCommands.SET,                     # {0}
            str(opcodes.basemanager),               # {1}
            str(opcodes.alternate_server_ip),       # {2}
            str(ai),                                # {3}
            str(opcodes.use_alternate_server_ip),   # {4}
            str(opcodes.false)                      # {5}
        )
        expected_message = "Exception occurred trying to set {0}'s 'Default values' to {1}".format(
            opcodes.basemanager,               # {0}
            expected_command                   # {1}
        )
        with self.assertRaises(Exception) as context:
            bm_connection.set_default_values()

        self.assertEqual(expected_message, context.exception.message)

    def test_set_ai_for_cn_happy_path(self):
        """ Verify happy path for set_ai_for_cn by verifying send_and_wait_for_reply calls """
        # create the test base manager connection object
        bm_connection = self.create_test_base_manager_connection()

        # create a new ip address
        new_ip_address = "127.08.125"

        # mock key value object to be returned in bm_connection.ser.get_and_wait_for_reply()
        controller_reply = mock.MagicMock()

        # create return value for the mocked controller_reply.get_value_string_by_key
        controller_reply.get_value_string_by_key = mock.MagicMock(return_value=opcodes.controller)

        # create call strings
        expected_call1_string = "SET,CN,SN=3K0001,DS=Test Controller 3K0001,LA=0.0,LG=0.0,MC=1,RP=0,JR=CL,JF=CL,JP=CL"
        expected_call2_string = "SET,BM,AI=127.08.125,UA=TR"

        # create mock calls for send_and_wait_for_reply
        call1 = mock.call(tosend=expected_call1_string)
        call2 = mock.call(expected_call2_string)

        # create expected send_and_wait_for_reply list
        expected_send_and_wait_for_reply_calls = [call1, call2]

        # call set_ai_for_cn
        bm_connection.set_ai_for_cn(ip_address=new_ip_address, unit_testing=True)

        # assert that the calls were made to send_and_wait_for_reply
        bm_connection.ser.send_and_wait_for_reply.assert_has_calls(expected_send_and_wait_for_reply_calls)

    def test_set_ai_for_cn_happy_path_2(self):
        """ Verify the string that gets passed in from the user configurations can pass make it through the method """
        # create the test base manager connection object
        bm_connection = self.create_test_base_manager_connection()

        # This is the URL for p2.basemanager.net, the method should parse out just the url
        new_url_address = "http://p2.basemanager.net/login.php?debug=true"
        # This is the IP address of p2.basemanager.net = "104.130.246.18"

        # mock key value object to be returned in bm_connection.ser.get_and_wait_for_reply()
        controller_reply = mock.MagicMock()

        # create return value for the mocked controller_reply.get_value_string_by_key
        controller_reply.get_value_string_by_key = mock.MagicMock(return_value=opcodes.controller)

        # create call strings
        expected_call1_string = "SET,CN,SN=3K0001,DS=Test Controller 3K0001,LA=0.0,LG=0.0,MC=1,RP=0,JR=CL,JF=CL,JP=CL"
        expected_call2_string = "SET,BM,AI=104.130.246.18,UA=TR"

        # create mock calls for send_and_wait_for_reply
        call1 = mock.call(tosend=expected_call1_string)
        call2 = mock.call(expected_call2_string)

        # create expected send_and_wait_for_reply list
        expected_send_and_wait_for_reply_calls = [call1, call2]

        # call set_ai_for_cn
        bm_connection.set_ai_for_cn(ip_address=new_url_address, unit_testing=True)

        # assert that the calls were made to send_and_wait_for_reply
        bm_connection.ser.send_and_wait_for_reply.assert_has_calls(expected_send_and_wait_for_reply_calls)

    def test_set_ai_for_cn_fail1(self):
        """
        Verify the message when set_ai_for_cn has an exception when sending the command 'DO,BM=DC'
        to the controller
        """

        exception_message = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_message)

        # Create new base manager connection test object
        bm_connection = self.create_test_base_manager_connection()

        # create a new ip address
        new_ip_address = "127.0.8.125"

        # Setting the controller type to 1000, so we hit the first exception in set_ai_for_cn
        self.controller.controller_type = ControllerCommands.Type.BASESTATION_1000

        # created expected exception message
        expected_message = " <- unable to send disconnect from BaseManager to controller: 'DO,BM=DC' -> "

        # Call set_ai_for_cn. Expect an exception
        with self.assertRaises(Exception) as context:
            bm_connection.set_ai_for_cn(ip_address=new_ip_address, unit_testing=True)

        # assert the exception messages are correct
        self.assertEqual(expected_message, context.exception.message)

    def test_set_ai_for_cn_fail2(self):
        """
        Verify the message when set_ai_for_cn has an exception for the set command we issue. This gives us code coverage
        for set_ai_for_cn
        """

        exception_message = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_message)

        # create the test base manager connection object
        bm_connection = self.create_test_base_manager_connection()

        # create a new ip address
        new_ip_address = "127.08.125"

        # Create expected exception message
        expected_message = " <- BC received after issuing command: SET,BM,AI=127.08.125,UA=TR -> "

        # Call set_ai_for_cn. Expect an exception
        with self.assertRaises(Exception) as context:
            bm_connection.set_ai_for_cn(ip_address=new_ip_address, unit_testing=True)

        # assert the exception messages are correct
        self.assertEqual(expected_message, context.exception.message)

    def test_connect_cn_to_bm_happy_path(self):
        """ Verify that connect_cn_to_bm sends the correct command to the controller """

        # create the test base manager connection object
        bm_connection = self.create_test_base_manager_connection()

        expected_command = 'DO,{0}={1}'.format(
            str(opcodes.basemanager),                   # {0}
            str(opcodes.connect_to_basemanager)         # {1}
        )

        bm_connection.connect_cn_to_bm()

        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    def test_connect_cn_to_bm_fail(self):
        """ Verify that message when connect_cn_to_bm has an exception when sending a command to the controller """

        exception_message = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_message)

        # create the test base manager connection object
        bm_connection = self.create_test_base_manager_connection()

        expected_message = "Connect the controller to Basemanager Command Failed: " + exception_message

        with self.assertRaises(Exception) as context:
            bm_connection.connect_cn_to_bm()

        self.assertEqual(expected_message, context.exception.message)

    def test_disconnect_cn_to_bm_happy_path(self):
        """ Verify that disconnect_cn_to_bm sends the correct command to the controller """

        # create the test base manager connection object
        bm_connection = self.create_test_base_manager_connection()

        expected_command = 'DO,{0}={1}'.format(
            str(opcodes.basemanager),                       # {0}
            str(opcodes.disconnect_from_basemanager)        # {1}
        )

        bm_connection.disconnect_cn_from_bm()

        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    def test_disconnect_cn_to_bm_fail(self):
        """ Verify that message when connect_cn_to_bm has an exception when sending a command to the controller """

        exception_message = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_message)

        # create the test base manager connection object
        bm_connection = self.create_test_base_manager_connection()

        expected_message = "disconnect the controller to Basemanager Command Failed: " + exception_message

        with self.assertRaises(Exception) as context:
            bm_connection.disconnect_cn_from_bm()

        self.assertEqual(expected_message, context.exception.message)

    def test_ping_bm_from_cn_happy_path(self):
        """ Verify that ping_bm_from_cn sends the correct command to the controller """

        # create the test base manager connection object
        bm_connection = self.create_test_base_manager_connection()

        expected_command = 'DO,{0}={1}'.format(
            str(opcodes.basemanager),       # {0}
            str(opcodes.ping_server)        # {1}
        )

        bm_connection.ping_bm_from_cn()

        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    def test_ping_bm_from_cn_fail(self):
        """ Verify that message when ping_bm_from_cn has an exception when sending a command to the controller """

        exception_message = "Test Exception"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_message)

        # create base manager connection object
        bm_connection = self.create_test_base_manager_connection()

        expected_message = "Pinging Basemanager from the controller Command Failed: " + exception_message

        with self.assertRaises(Exception) as context:
            bm_connection.ping_bm_from_cn()

        self.assertEqual(expected_message, context.exception.message)

    def test_connect_cn_to_ethernet_happy_path(self):
        """ Verify that connect_cn_to_ethernet sends the correct command to the controller """

        # create base manager connection object
        bm_connection = self.create_test_base_manager_connection()

        # mock verify_cn_connected_to_bm
        bm_connection.verify_cn_connected_to_bm = mock.MagicMock()

        # set side effect for verify_cn_connected_to_bm to enter the while loop one and then exit
        bm_connection.verify_cn_connected_to_bm.side_effect = [False, True]
        expected_command = '{0},{1},{2}={3}'.format(
            opcodes.set_action,                 # {0}
            str(opcodes.basemanager),           # {1}
            str(opcodes.connect_to_ethernet),   # {2}
            str(opcodes.ethernet)               # {3}
        )

        bm_connection.set_controller_to_connect_using_ethernet(unit_testing=True)

        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    def test_connect_cn_to_ethernet_fail1(self):
        """ Verify message when connection to controller fails """

        exception_message = "Test Exception"

        bm_connection = self.create_test_base_manager_connection()
        bm_connection.ser.send_and_wait_for_reply.side_effect = Exception(exception_message)
        bm_connection.verify_cn_connected_to_bm = mock.MagicMock()

        expected_message = "Connect the controller to Ethernet Command Failed: " + exception_message

        with self.assertRaises(Exception) as context:
            bm_connection.set_controller_to_connect_using_ethernet(unit_testing=True)

        self.assertEquals(expected_message, context.exception.message)
        self.assertEquals(bm_connection.verify_cn_connected_to_bm.call_count, 0)

    # def test_connect_cn_to_ethernet_fail2(self):
    #     """ Verify message when verify_cn_connected_to_bm fails after 60 seconds """
    #
    #     bm_connection = self.create_test_base_manager_connection()
    #
    #     bm_connection.verify_cn_connected_to_bm = mock.MagicMock(return_value=False)
    #
    #     expected_message = "Your controller is not connected the Ethernet! Please connect it."
    #
    #     with self.assertRaises(Exception) as context:
    #         bm_connection.set_controller_to_connect_using_ethernet(unit_testing=True)
    #
    #     self.assertEquals(expected_message, context.exception.message)
    #     self.assertEquals(bm_connection.verify_cn_connected_to_bm.call_count, 25)

    def test_disconnect_cn_to_ethernet_happy_path(self):
        """ Verify that disconnect_cn_to_ethernet sends the correct command to the controller """

        bm_connection = self.create_test_base_manager_connection()
        bm_connection.controller.vr = "16.1"

        expected_command = '{0},{1},{2}={3}'.format(
            opcodes.set_action,                             # {0}
            str(opcodes.basemanager),                       # {1}
            str(opcodes.connect_to_ethernet),               # {2}
            str(opcodes.disconnect_basemanager_ethernet)    # {3}
        )

        bm_connection.set_dont_connect_to_bm()

        self.mock_send_and_wait_for_reply.assert_called_with(expected_command)

    def test_disconnect_cn_to_ethernet_fail1(self):
        """ Verify message when connection to controller fails """

        exception_message = "Test Exception"

        bm_connection = self.create_test_base_manager_connection()
        bm_connection.ser.send_and_wait_for_reply.side_effect = Exception(exception_message)

        expected_message = "Disconnect the controller to Ethernet Command Failed: " + exception_message

        with self.assertRaises(Exception) as context:
            bm_connection.set_dont_connect_to_bm()

        self.assertEquals(expected_message, context.exception.message)

    def test_verify_ip_address_state_happy_path1(self):
        """ Verify that verify_ip_address_state calls correct methods when configured_ai_keys = True,
        controller_type = 32, and controllers_configured_ad_key_value != current_users_bm_dns_address"""

        get_and_wait_for_reply = mock.MagicMock()
        controller_reply_mock = mock.MagicMock()
        controller_reply_mock.get_value_string_by_key = mock.MagicMock()

        # side effects for get_value_string_by_key for the controller_reply to get_and_wait_for_reply
        controller_reply_mock.get_value_string_by_key.side_effect = [opcodes.disconnect_from_basemanager,
                                                                     'p1.basemanager.net',
                                                                     opcodes.true,
                                                                     '111.111.111.111']

        # return the controller_reply_mock object each time get_and_wait_for_reply is called
        get_and_wait_for_reply.side_effect = [controller_reply_mock, controller_reply_mock]

        self.mock_ser.get_and_wait_for_reply = get_and_wait_for_reply

        # create the base manager connection object and create the necessary additional mocks
        bm_connection = self.create_test_base_manager_connection()
        bm_connection.set_controller_to_connect_using_ethernet = mock.MagicMock()
        bm_connection.verify_cn_connected_to_bm = mock.MagicMock()

        # side effects so code enters while loop once and then exits the second time
        bm_connection.verify_cn_connected_to_bm.side_effect = [False, True]
        bm_connection.set_ai_for_cn = mock.MagicMock()

        # call function
        bm_connection.verify_ip_address_state(unit_testing=True)

        self.assertEquals(bm_connection.set_controller_to_connect_using_ethernet.call_count, 1)
        bm_connection.set_ai_for_cn.assert_called_with(ip_address='104.130.159.113')

    def test_verify_ip_address_state_happy_path2(self):
        """ Verify that verify_ip_address_state calls correct methods when configured_ai_keys = False,
        controller_type = 10, and controllers_configured_ad_key_value != current_users_bm_dns_address"""

        get_and_wait_for_reply = mock.MagicMock()
        controller_reply_mock = mock.MagicMock()

        # side effects for get_value_string_by_key for the controller_reply to get_and_wait_for_reply
        controller_reply_mock.get_value_string_by_key.side_effect = [opcodes.disconnect_from_basemanager,
                                                                     'p1.basemanager.net',
                                                                     opcodes.false,
                                                                     '111.111.111.111']

        # return the controller_reply_mock object each time get_and_wait_for_reply is called
        get_and_wait_for_reply.side_effect = [controller_reply_mock, controller_reply_mock]

        self.mock_ser.get_and_wait_for_reply = get_and_wait_for_reply

        # create the base manager connection object and create the necessary additional mocks
        bm_connection = self.create_test_base_manager_connection()
        bm_connection.controller.controller_type = '10'
        bm_connection.set_controller_to_connect_using_ethernet = mock.MagicMock()
        bm_connection.verify_cn_connected_to_bm = mock.MagicMock()

        # side effects so code enters while loop once and then exits the first time
        bm_connection.verify_cn_connected_to_bm.side_effect = [True]
        bm_connection.set_ai_for_cn = mock.MagicMock()

        # call function
        bm_connection.verify_ip_address_state(unit_testing=True)

        self.assertEquals(bm_connection.set_controller_to_connect_using_ethernet.call_count, 1)
        bm_connection.set_ai_for_cn.assert_called_with(ip_address='104.130.159.113')
        self.assertEquals(bm_connection.set_ai_for_cn.call_count, 1)

    def test_verify_ip_address_state_happy_path3(self):
        """ Verify that verify_ip_address_state calls correct methods when configured_ai_keys = False,
        controller_type = 10, and controllers_configured_ad_key_value == current_users_bm_dns_address"""
        get_and_wait_for_reply = mock.MagicMock()
        controller_reply_mock = mock.MagicMock()

        # side effects for get_value_string_by_key for the controller_reply to get_and_wait_for_reply
        controller_reply_mock.get_value_string_by_key.side_effect = [opcodes.disconnect_from_basemanager,
                                                                     '104.130.246.18',
                                                                     opcodes.false,
                                                                     '104.130.246.18']

        # return the controller_reply_mock object each time get_and_wait_for_reply is called
        get_and_wait_for_reply.side_effect = [controller_reply_mock, controller_reply_mock]
        self.mock_ser.get_and_wait_for_reply = get_and_wait_for_reply

        # create the base manager connection object and create the necessary additional mocks
        bm_connection = self.create_test_base_manager_connection()
        bm_connection.controller.controller_type = '10'
        bm_connection.set_controller_to_connect_using_ethernet = mock.MagicMock()
        bm_connection.verify_cn_connected_to_bm = mock.MagicMock()
        bm_connection.verify_cn_connected_to_bm.side_effect = [True]
        bm_connection.set_ai_for_cn = mock.MagicMock()

        bm_connection.verify_ip_address_state(unit_testing=True)

        self.assertEquals(bm_connection.set_controller_to_connect_using_ethernet.call_count, 1)
        self.assertEquals(bm_connection.set_ai_for_cn.call_count, 0)

    def test_verify_ip_address_state_happy_path4(self):
        """ Verify that verify_ip_address_state calls correct methods when configured_ai_keys = False,
        controller_type = 32, and controllers_configured_ad_key_value != current_users_bm_dns_address"""
        get_and_wait_for_reply = mock.MagicMock()
        controller_reply_mock = mock.MagicMock()

        # side effects for get_value_string_by_key for the controller_reply to get_and_wait_for_reply
        controller_reply_mock.get_value_string_by_key.side_effect = [opcodes.disconnect_from_basemanager,
                                                                     'p1.basemanager.net',
                                                                     opcodes.false,
                                                                     '111.111.111.111']

        # return the controller_reply_mock object each time get_and_wait_for_reply is called
        get_and_wait_for_reply.side_effect = [controller_reply_mock, controller_reply_mock]
        self.mock_ser.get_and_wait_for_reply = get_and_wait_for_reply

        # create the base manager connection object and create the necessary additional mocks
        bm_connection = self.create_test_base_manager_connection()
        bm_connection.set_controller_to_connect_using_ethernet = mock.MagicMock()
        bm_connection.verify_cn_connected_to_bm = mock.MagicMock()
        bm_connection.verify_cn_connected_to_bm.side_effect = [False, True]
        bm_connection.set_ai_for_cn = mock.MagicMock()

        bm_connection.verify_ip_address_state(unit_testing=True)

        self.assertEquals(bm_connection.set_controller_to_connect_using_ethernet.call_count, 1)
        bm_connection.set_ai_for_cn.assert_called_with(ip_address='104.130.159.113')

    def test_verify_ip_address_state_happy_path5(self):
        """ Verify that verify_ip_address_state calls correct methods when configured_ai_keys = False,
        controller_type = 32, and controllers_configured_ad_key_value == current_users_bm_dns_address"""

        get_and_wait_for_reply = mock.MagicMock()
        controller_reply_mock = mock.MagicMock()

        # side effects for get_value_string_by_key for the controller_reply to get_and_wait_for_reply
        controller_reply_mock.get_value_string_by_key.side_effect = [opcodes.disconnect_from_basemanager,
                                                                     '104.130.246.18',
                                                                     opcodes.false,
                                                                     '104.130.246.18']

        # return the controller_reply_mock object each time get_and_wait_for_reply is called
        get_and_wait_for_reply.side_effect = [controller_reply_mock, controller_reply_mock]
        self.mock_ser.get_and_wait_for_reply = get_and_wait_for_reply

        # create the base manager connection object and create the necessary additional mocks
        bm_connection = self.create_test_base_manager_connection()
        bm_connection.set_controller_to_connect_using_ethernet = mock.MagicMock()
        bm_connection.verify_cn_connected_to_bm = mock.MagicMock()
        bm_connection.verify_cn_connected_to_bm.side_effect = [False, True]
        bm_connection.set_ai_for_cn = mock.MagicMock()

        bm_connection.verify_ip_address_state(unit_testing=True)

        self.assertEquals(bm_connection.set_controller_to_connect_using_ethernet.call_count, 1)
        self.assertEquals(bm_connection.set_ai_for_cn.call_count, 0)

    def test_verify_ip_address_state_happy_path6(self):
        """ Verify that verify_ip_address_state calls correct methods when configured_ai_keys = False,
        controller_type = 32, and controllers_configured_ad_key_value != current_users_bm_dns_address
        "AD" value is not in dictionary_for_basemanager_address_pointers"""

        get_and_wait_for_reply = mock.MagicMock()
        controller_reply_mock_1 = mock.MagicMock()

        # side effects for get_value_string_by_key for the controller_reply to get_and_wait_for_reply
        controller_reply_mock_1.get_value_string_by_key.side_effect = [opcodes.disconnect_from_basemanager,
                                                                       "123.123.123.123",
                                                                       opcodes.true,
                                                                       "111.111.111.111"]

        # return the controller_reply_mock object each time get_and_wait_for_reply is called
        get_and_wait_for_reply.side_effect = [controller_reply_mock_1, controller_reply_mock_1]
        self.mock_ser.get_and_wait_for_reply = get_and_wait_for_reply

        # create the base manager connection object and create the necessary additional mocks
        bm_connection = self.create_test_base_manager_connection()
        bm_connection.set_controller_to_connect_using_ethernet = mock.MagicMock()
        bm_connection.verify_cn_connected_to_bm = mock.MagicMock(return_value=True)
        bm_connection.set_ai_for_cn = mock.MagicMock()

        bm_connection.verify_ip_address_state(unit_testing=True)

        self.assertEquals(bm_connection.set_controller_to_connect_using_ethernet.call_count, 1)
        bm_connection.set_ai_for_cn.assert_called_with(ip_address="123.123.123.123")

    def test_verify_ip_address_state_fail1(self):
        """ Verify exception message when sending "GET,BM" causes a communication error with the controller """
        exception_message = "Test Exception"
        bm_connection = self.create_test_base_manager_connection()
        bm_connection.ser.get_and_wait_for_reply.side_effect = AssertionError(exception_message)

        # expected exception message
        expected_message = "Exception occurred after sending command to controller: 'GET,BM'.\n" + \
                           "Controller Response: [" + str(exception_message) + "]"
        with self.assertRaises(AssertionError) as context:
            bm_connection.verify_ip_address_state(unit_testing=True)

        self.assertEquals(expected_message, context.exception.message)

    def test_verify_ip_address_state_fail2(self):
        """ Verify exception message when verify_cn_connected_to_bm never returns true during the first 10 seconds """

        bm_connection = self.create_test_base_manager_connection()

        bm_connection.verify_cn_connected_to_bm = mock.MagicMock(return_value=False)

        # expected exception message
        expected_message = "Your controller is not connected to BaseManager! Please connect it."

        with self.assertRaises(AttributeError) as context:
            bm_connection.verify_ip_address_state(unit_testing=True)

        self.assertEquals(expected_message, context.exception.message)

    def test_verify_ip_address_state_fail3(self):
        """ Verify exception message when controller_reply "GET,BM" causes a communication error with the controller """
        get_and_wait_for_reply = mock.MagicMock()
        controller_reply_mock = mock.MagicMock()
        controller_reply_mock.get_value_string_by_key = mock.MagicMock()
        exception_error = "Test exception"
        controller_reply_mock.get_value_string_by_key.side_effect = [opcodes.disconnect_from_basemanager]

        get_and_wait_for_reply.side_effect = [controller_reply_mock, AssertionError(exception_error)]
        self.mock_ser.get_and_wait_for_reply = get_and_wait_for_reply

        # expected exception message
        expected_exception_message = "Exception occurred after sending command to controller: 'GET,BM'.\n" + \
                                     "Controller Response: [" + str(exception_error) + "]"

        bm_connection = self.create_test_base_manager_connection()
        bm_connection.set_controller_to_connect_using_ethernet = mock.MagicMock()
        bm_connection.verify_cn_connected_to_bm = mock.MagicMock(return_value=True)

        with self.assertRaises(AssertionError) as context:
            bm_connection.verify_ip_address_state(unit_testing=True)

        self.assertEquals(expected_exception_message, context.exception.message)

    def test_verify_cn_connected_to_bm_happy_path1(self):
        """ Confirm that verify_cn_connected_to_bm returns True when the status connection status from the controller is
        'CN'."""
        get_and_wait_for_reply = mock.MagicMock()
        controller_reply_mock = mock.MagicMock()
        controller_reply_mock.get_value_string_by_key = mock.MagicMock()

        controller_reply_mock.get_value_string_by_key.side_effect = [opcodes.connect_to_basemanager]

        get_and_wait_for_reply.side_effect = [controller_reply_mock]
        self.mock_ser.get_and_wait_for_reply = get_and_wait_for_reply

        bm_connection = self.create_test_base_manager_connection()

        # call function
        verify_result = bm_connection.verify_cn_connected_to_bm()

        self.assertTrue(verify_result)

    def test_verify_cn_connected_to_bm_happy_path2(self):
        """ Confirm that verify_cn_connected_to_bm returns False when the status connection status from the controller is
        something other than 'CN'."""
        get_and_wait_for_reply = mock.MagicMock()
        controller_reply_mock = mock.MagicMock()

        controller_reply_mock.get_value_string_by_key.side_effect = [opcodes.disconnect_from_basemanager]

        get_and_wait_for_reply.side_effect = [controller_reply_mock]
        self.mock_ser.get_and_wait_for_reply = get_and_wait_for_reply

        bm_connection = self.create_test_base_manager_connection()

        # call function
        verify_result = bm_connection.verify_cn_connected_to_bm()

        self.assertFalse(verify_result)

    def test_verify_cn_connected_to_bm_fail1(self):
        """ Verify exception message when command "GET,BM" causes a communication error with the controller """
        get_and_wait_for_reply = mock.MagicMock()
        exception_error_message = "Test Exception"
        get_and_wait_for_reply.side_effect = [AssertionError(exception_error_message)]
        self.mock_ser.get_and_wait_for_reply = get_and_wait_for_reply

        # expected exception message
        expected_exception_message = "Exception occurred after sending command to controller: 'GET,BM'.\n" + \
                                     "Controller Response: [" + str(exception_error_message) + "]"

        bm_connection = self.create_test_base_manager_connection()

        with self.assertRaises(AssertionError) as context:
            bm_connection.verify_cn_connected_to_bm()

        self.assertEquals(expected_exception_message, context.exception.message)

    def test_wait_for_bm_connection_happy_path1(self):
        """ Confirm that wait_for_bm_connection does not throw an an exception on one try."""
        # create the test base manager connection object
        bm_connection = self.create_test_base_manager_connection()

        bm_connection.verify_cn_connected_to_bm = mock.MagicMock(return_value=True)

        # call function. No exceptions should be thrown
        bm_connection.wait_for_bm_connection(unit_testing=True)

    def test_wait_for_bm_connection_happy_path2(self):
        """ Confirm that wait_for_bm_connection does not throw an an exception on fifth try."""
        # create the test base manager connection object
        bm_connection = self.create_test_base_manager_connection()

        bm_connection.verify_cn_connected_to_bm = mock.MagicMock()

        bm_connection.verify_cn_connected_to_bm.side_effect = [False, False, False, False, False, True]

        # call function. No exceptions should be thrown
        bm_connection.wait_for_bm_connection(unit_testing=True)

    def test_wait_for_bm_connection_fail1(self):
        """ Confirm that wait_for_bm_connection throws an exception on sixth try."""
        # create the test base manager connection object
        bm_connection = self.create_test_base_manager_connection()

        bm_connection.verify_cn_connected_to_bm = mock.MagicMock()

        bm_connection.verify_cn_connected_to_bm.side_effect = [False, False, False, False, False, False]

        # expected exception message
        expected_exception_message = "The Connection to Basemanager failed was unable to connect after 3 minutes and 25 seconds: "

        bm_connection = self.create_test_base_manager_connection()

        # call method and assert the correct exception is thrown
        with self.assertRaises(Exception) as context:
            bm_connection.wait_for_bm_connection(unit_testing=True)

        self.assertEquals(expected_exception_message, context.exception.message)
