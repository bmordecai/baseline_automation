import unittest2 as unittest
from common.server_handler import ServerHandler


class TestServerHandlerObject(unittest.TestCase):
    def setUp(self):
        """ Setting up for the test. """
        # mock items

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_server_lifecycle_happy_path(self):
        # this test verifies that a server can be set up, a connection can be made to it, and communication can
        # go between the server and client.

        server_ip_address = '127.0.0.1'
        server_port = 9000
        server_handler = ServerHandler(server_ip_address, server_port)
        test_result = server_handler.verify_server_running()
        server_handler.shutdown_server()
        self.assertTrue(test_result)

    def test_server_creation_error_invalid_ip_address(self):
        # this tests how the ServerHandler processes an error when creating the server if the given IP address is
        # invalid

        server_ip_address = '128.0.0.1'
        server_port = 9000
        # Verify that starting a server with a bogus IP address throws an exception
        with self.assertRaises(Exception):
            server_handler = ServerHandler(server_ip_address, server_port)

    def test_server_creation_error_invalid_port_number(self):
        # this tests how the ServerHandler processes an error when creating the server if the given port does not
        # match the port on the created server.

        server_ip_address = '127.0.0.1'
        server_port = 0
        # Verify that starting a server with a bogus port throws an exception
        with self.assertRaises(Exception):
            server_handler = ServerHandler(server_ip_address, server_port)

    def test_verify_server_running_error(self):
        # this tests how the ServerHandler processes verify_server_running when no server exists

        server_ip_address = '127.0.0.1'
        server_port = 9000
        server_handler = ServerHandler(server_ip_address, server_port)
        server_handler.shutdown_server()
        # With the server shut down, expect that verify_server_running returns false
        self.assertFalse(server_handler.verify_server_running())

    def test_shutdown_server_error(self):
        # this tests how the ServerHandler processes shutdown_server when no server exists

        server_ip_address = '127.0.0.1'
        server_port = 9000
        server_handler = ServerHandler(server_ip_address, server_port)
        server_handler.shutdown_server()
        # Verify that shutting down the server when it isn't running throws an exception
        with self.assertRaises(Exception):
            server_handler.shutdown_server()



