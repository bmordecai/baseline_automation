from unittest import TestCase
import mock
from serial import Serial
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.base_classes.conditions import BaseConditions
from common.imports.types import EmptyConditionCommands
from common.imports import opcodes


class TestBaseConditions(TestCase):
    def setUp(self):
        # Create the mock_ser object
        mock_ser = mock.MagicMock(spec=Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        # BaseDevices.ser = mock_ser

        # mock_controller = mock.MagicMock(spec=BaseStation3200)

        ip_address = "192.168.100.100"
        port_address = "192.168.100.101"
        socket_port = 1067

        self.controller = BaseStation3200(_mac="12345",
                                          _serial_number="3K0001",
                                          _firmware_version="",
                                          _serial_port=mock_ser,
                                          _port_address=port_address,
                                          _socket_port=socket_port,
                                          _ip_address=ip_address)

    def create_test_base_condition(self):
        program_ad = 2
        water_source_ad = 3
        empty_condition_address = 5
        event_type = EmptyConditionCommands.Type.EMPTY
        identifiers = [[EmptyConditionCommands.Type.EMPTY, empty_condition_address],
                       [opcodes.water_source_empty_condition, water_source_ad]]

        empty_condition = BaseConditions(_controller=self.controller,
                                         _event_type=event_type,
                                         _identifiers=identifiers,
                                         _program_ad=program_ad,
                                         _watersource_ad=water_source_ad)

        return empty_condition

    def test_verify_myself_pass1(self):
        """ Test Verify Myself Pass Case 1: Verify default values """
        ec = self.create_test_base_condition()

        ec.get_data = mock.MagicMock()
        ec.verify_status = mock.MagicMock()
        ec.verify_enabled_state = mock.MagicMock()

        ec.verify_myself()

        self.assertEquals(ec.get_data.call_count, 1)
        self.assertEquals(ec.verify_enabled_state.call_count, 1)
        self.assertEquals(ec.verify_status.call_count, 0)

    def test_verify_myself_pass2(self):
        """ Test Verify Myself Pass Case 2: Verify function with expected_status=None and skip_get_data=True  """
        ec = self.create_test_base_condition()

        ec.get_data = mock.MagicMock()
        ec.verify_status = mock.MagicMock()
        ec.verify_enabled_state = mock.MagicMock()

        ec.verify_myself(skip_get_data=True)

        self.assertEquals(ec.get_data.call_count, 0)
        self.assertEquals(ec.verify_enabled_state.call_count, 1)
        self.assertEquals(ec.verify_status.call_count, 0)

    def test_verify_myself_pass3(self):
        """ Test Verify Myself Pass Case 3: Verify function with expected_status="test" and skip_get_data=False  """
        ec = self.create_test_base_condition()

        ec.get_data = mock.MagicMock()
        ec.verify_status = mock.MagicMock()
        ec.verify_enabled_state = mock.MagicMock()
        expected_status_text = "test"

        ec.verify_myself(expected_status=expected_status_text)

        self.assertEquals(ec.get_data.call_count, 1)
        self.assertEquals(ec.verify_enabled_state.call_count, 1)
        ec.verify_status.assert_called_with(_expected_status=expected_status_text)

    def test_verify_myself_pass4(self):
        """ Test Verify Myself Pass Case 4: Verify function with expected_status="test" and skip_get_data=True  """
        ec = self.create_test_base_condition()

        ec.get_data = mock.MagicMock()
        ec.verify_status = mock.MagicMock()
        ec.verify_enabled_state = mock.MagicMock()
        expected_status_text = "test"

        ec.verify_myself(expected_status=expected_status_text, skip_get_data=True)

        self.assertEquals(ec.get_data.call_count, 0)
        self.assertEquals(ec.verify_enabled_state.call_count, 1)
        ec.verify_status.assert_called_with(_expected_status=expected_status_text)