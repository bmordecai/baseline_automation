__author__ = 'Brice "Ajo Grande" Garlick'

import unittest
import mock
import serial
import status_parser

from common.imports import opcodes
from common.variables import common as common_vars
from common.objects.base_classes.base_methods import BaseMethods
from common.objects.base_classes import messages


class TestBaseMethodsObject(unittest.TestCase):

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Set serial instance to mock serial
        BaseMethods.ser = self.mock_ser

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        self.create_test_base_methods_object()

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_base_methods_object(self):
        """ Creates a new Master Valve object for testing purposes """
        self.default_device_type = opcodes.zone
        self.default_device_address = 1
        self.uut = BaseMethods(_ser=self.mock_ser, _identifiers=[[self.default_device_type, self.default_device_address]], _type=self.default_device_type)

    #################################
    def test_set_description_pass1(self):
        """ Set Description On Controller Pass Case 1: Using Default _ds value, check that object variable _ds is set"""
        # dev = self.create_test_device_object()

        expected_value = self.uut.ds
        self.uut.set_description()

        actual_value = self.uut.ds
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_description_pass2(self):
        """ Set Description On Controller Pass Case 2: Setting new _ds value = "Test Programs 2", check that
        object variable _ds is set on a ZN"""
        # dev = self.create_test_device_object()

        expected_value = "Test Device 2"
        self.uut.set_description(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = self.uut.ds
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_description_pass3(self):
        """ Set Description On Controller Pass Case 3: Setting new _ds value = "Test Programs 2", check that
        object variable _ds is set on a 3200"""
        # dev = self.create_test_device_object(dev_type="CN")

        expected_value = "Test Device 2"
        self.uut.set_description(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = self.uut.ds
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_description_pass4(self):
        """ Set Description On Controller Pass Case 4: Setting new _ds value = "Test Programs 2", check that
        object variable _ds is set on a 1000"""
        # dev = self.create_test_device_object(dev_type="CN", cn_type="10")

        self.uut.dv_type = "CN"
        self.uut.cn_type = "10"
        expected_value = "Test Device 2"
        self.uut.set_description(expected_value)

        # value is set during this method and should equal the value passed into the method
        actual_value = self.uut.ds
        self.assertEqual(expected_value, actual_value)

    #################################
    def test_set_description_pass5(self):
        """ Set Description On Controller Pass Case 5: Command with correct values sent to controller with a ZN
        dev type"""
        # dev = self.create_test_device_object()

        ds_value = str(self.uut.ds)
        expected_command = "SET,{0}=1,{1}={2}".format(opcodes.zone, opcodes.description, str(ds_value))
        self.uut.set_description()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_description_pass6(self):
        """ Set Description On Controller Pass Case 6: Command with correct values sent to controller with an MV
        dev type"""
        # dev = self.create_test_device_object(dev_type="MV")

        self.uut.dv_type = opcodes.master_valve
        self.uut.identifiers = [[opcodes.master_valve, "TSD0001"]]
        ds_value = str(self.uut.ds)
        expected_command = "SET,{0}=TSD0001,{1}={2}".format(opcodes.master_valve, opcodes.description, str(ds_value))
        self.uut.set_description()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_description_fail1(self):
        """ Set Description On Controller Fail Case 1: Failed communication with controller """
        # dev = self.create_test_device_object()

        exception_msg = "Test Exception"
        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.uut.set_description()
        e_msg = "Exception occurred trying to set {0}'s description to: {0}".format(self.uut.ds)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_message_pass1(self):
        """
        set_message pass case 1
        Create: Test devices object
        Mock: message_to_set method
        Return: None, so it does not fail
        Mock: ser.send_and_wait_for_reply
        Return: None, so it does not fail
        Run: Test, should not return an error if it passes
        """
        # Create test devices object
        # dev = self.create_test_device_object()

        # Mock serial send and wait for reply method
        self.mock_send_and_wait_for_reply.side_effect = None

        # Run test
        with mock.patch('common.objects.base_classes.messages.message_to_set'):
            self.uut.set_message(_status_code='OK')

    #################################
    def test_set_message_fail1(self):
        """
        set_message fail case 1
        Create: test devices object
        Store: Expected error message
        Mock: message_to_set
        Return: Exception, so the test fails
        Run: Test
        Compare: Expected error message to actual error message
        """
        # Create test devices object
        # dev = self.create_test_device_object()

        # Expected error message
        e_msg = "Exception caught in messages.set_message: "

        # Run test
        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_set', side_effect=Exception):
                self.uut.set_message(_status_code='OK')

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_get_data_pass1(self):
        """ Get Data From Controller Pass Case 1: Correct command is sent to controller with ZN dev type """
        # dev = self.create_test_device_object()

        expected_command = "GET,{0}={1}".format(opcodes.zone, str(self.default_device_address))
        self.uut.get_data()
        self.mock_get_and_wait_for_reply.assert_called_with(called_by_get_data=True, tosend=expected_command)

    #################################
    def test_get_data_pass2(self):
        """ Get Data From Controller Pass Case 2: Correct command is sent to controller with CN dev type """
        # dev = self.create_test_device_object(dev_type="CN")
        # TODO: Why does get_data use ty instead of dv_type?  What's the difference?
        self.uut.ty = opcodes.basestation_3200
        self.uut.dv_type = opcodes.controller
        self.uut.identifiers = [[opcodes.controller, ""]]
        expected_command = "GET,{0}".format(opcodes.controller)
        self.uut.get_data()
        self.mock_get_and_wait_for_reply.assert_called_with(called_by_get_data=True, tosend=expected_command)

    #################################
    def test_get_data_pass3(self):
        """ Get Data From Controller Pass Case 3: Correct command is sent to controller with MV dev type """
        # dev = self.create_test_device_object(_dev_type="MV")
        self.uut.dv_type = opcodes.master_valve
        self.uut.identifiers = [[opcodes.master_valve, "TSD0001"]]
        expected_command = "GET,{0}=TSD0001".format(opcodes.master_valve)
        self.uut.get_data()
        self.mock_get_and_wait_for_reply.assert_called_with(called_by_get_data=True, tosend=expected_command)

    #################################
    def test_get_data_fail1(self):
        """ Get Data From Controller Fail Case 1: Failed communication with controller """
        # dev = self.create_test_device_object()

        exception_msg = "Test Exception"
        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_get_and_wait_for_reply.side_effect = AssertionError(exception_msg)

        expected_command = "GET,{0}={1}".format(opcodes.zone, str(self.default_device_address))
        with self.assertRaises(ValueError) as context:
            self.uut.get_data()
        e_msg = "Unable to get data for {0} using command: '{1}'. Exception raised: {2}"\
                .format(str(self.uut.ds), expected_command, exception_msg)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_get_message_pass1(self):
        """
        get_message pass case 1
        Create: Test devices object
        Mock: message_to_get method
        Return: None, so it does not fail
        Mock: ser.get_and_wait_for_reply
        Return: None, so it does not fail
        Run: Test, should not return an error if it passes
        """
        # Create test devices object
        # dev = self.create_test_device_object()

        # Mock serial get and wait for reply
        self.mock_get_and_wait_for_reply.side_effect = None

        # Run test
        with mock.patch('common.objects.base_classes.messages.message_to_get'):
            self.uut.get_message(_status_code='OK')

    #################################
    def test_get_message_fail1(self):
        """
        get_message fail case 1
        Create: test devices object
        Store: Expected error message
        Mock: messages_to_get
        Return: Exception, so the test will fail
        Run: test, expect an exception
        Compare: Actual error message to expected error message
        """
        # Create test devices object
        # dev = self.create_test_device_object()

        # Store expected error message
        e_msg = "Exception caught in messages.get_message: "

        # Run test, expect an exception
        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_get', side_effect=Exception):
                self.uut.get_message(_status_code='OK')

        # Compare expected error message to actual error message
        self.assertEqual(e_msg, context.exception.message)

    #################################
    def test_clear_message_pass1(self):
        """
        clear_message pass case 1:
        Create: Test devices object
        Mock: messages_to_clear
        Return: None, so it does not fail
        Mock: ser.send_and_wait_for_reply
        Return: None, so it does not fail
        Mock: messages_to_get
        Return: None, so it does not fail
        Mock: ser.get_and_wait_for_reply
        Return: Exception, so the "NM No Message Found" error message is triggered
        Run: test
        """
        # Todo: does this method need an exception to catch the error message?
        # Create test devices object
        # dev = self.create_test_device_object()

        # Mock send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = None

        # Mock get and wait for reply
        self.mock_get_and_wait_for_reply.side_effect = Exception("NM No Message Found")

        # Run test, expect an exception
        with mock.patch('common.objects.base_classes.messages.message_to_clear'), \
            mock.patch('common.objects.base_classes.messages.message_to_get'):
            self.uut.clear_message(_status_code='OK')

    #################################
    def test_clear_message_fail1(self):
        """
        clear_message pass case 1:
        Create: Test devices object
        Mock: messages_to_clear
        Return: None, so it does not fail
        Mock: ser.send_and_wait_for_reply
        Return: None, so it does not fail
        Mock: messages_to_get
        Return: None, so it does not fail
        Mock: ser.get_and_wait_for_reply
        Return: Exception, but no "NM No Message Found' with it
        Run: test
        """
        # Todo: does this method need an exception to catch the error message?
        # Create test devices object
        # dev = self.create_test_device_object()

        # Mock send and wait for reply
        self.mock_send_and_wait_for_reply.side_effect = None

        # Mock get and wait for reply
        self.mock_get_and_wait_for_reply.side_effect = Exception

        # Run test, expect an exception
        with mock.patch('common.objects.base_classes.messages.message_to_clear'), \
            mock.patch('common.objects.base_classes.messages.message_to_get'):
            self.uut.clear_message(_status_code='OK')

    #################################
    def test_clear_message_fail2(self):
        """ Clear Message On Controller Fail Case 2: Exception is thrown when sending command
        Create the object
        Create the expected command that will be passed into the method
        Mock: Messages to clear because it goes outside of the method
            Return: An Exception
        Set up the mock_send_and_wait_for_reply to have a side effect that throws an exception
        Call the method and expect an exception to be raised
        """
        # dv = self.create_test_device_object()

        expected_msg = "Exception caught in messages.clear_message: "

        self.mock_send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            with mock.patch('common.objects.base_classes.messages.message_to_clear'):
                self.uut.clear_message(_status_code='OK')

        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_description_pass1(self):
        """ Verify Description On Controller Pass Case 1: Exception is not raised """
        # dev = self.create_test_device_object()
        self.uut.ds = 'Test Device 2'
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test Device 2".format(opcodes.description))
        self.uut.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.uut.verify_description()

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_description_fail1(self):
        """ Verify Description On Controller Fail Case 1: Value on controller does not match what is
        stored in dev.ds for dev type ZN """
        # dev = self.create_test_device_object()
        self.uut.ds = 'Test Device'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test".format(opcodes.description))
        self.uut.data = mock_data

        expected_message = "Unable to verify {0} 'Description'. Received: Test, Expected: Test Device".format(
            self.uut.ds)
        try:
            self.uut.verify_description()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_description_fail2(self):
        """ Verify Description On Controller Fail Case 2: Value on controller does not match what is
        stored in dev.ds for dev type MV """
        # dev = self.create_test_device_object(dev_type="MV")
        self.uut.dv_type = 'MV'
        self.uut.ds = 'Test Device'
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=Test".format(opcodes.description))
        self.uut.data = mock_data

        expected_message = "Unable to verify Test Device 'Description'. Received: Test, Expected: Test Device"
        try:
            self.uut.verify_description()

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_status_pass1(self):
        """ Verify Status On Controller Pass Case 1: Exception is not raised """
        # dev = self.create_test_device_object()
        expected_ss = opcodes.okay
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=OK".format(opcodes.status_code))
        self.uut.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                self.uut.verify_status(expected_ss)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_status_fail1(self):
        """ Verify Status On Controller Fail Case 1: Value on controller does not match what is
        passed into method for dev type ZN """
        # dev = self.create_test_device_object()
        expected_ss = opcodes.disabled
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=OK".format(opcodes.status_code))
        self.uut.data = mock_data

        expected_message = "Unable verify {0} 'Status'. Received: OK, Expected: DS".format(self.uut.ds)
        try:
            self.uut.verify_status(expected_ss)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    # @unittest.skip("Still not passing")
    def test_verify_status_fail2(self):
        """ Verify Status On Controller Fail Case 2: Value on controller does not match what is
        stored in dev.ds for dev type MV """
        # dev = self.create_test_device_object(dev_type="MV")
        self.uut.dv_type = 'MV'
        self.uut.identifiers = [[opcodes.master_valve, "TSD0001"]]
        self.uut.ds = "Test "
        expected_ss = opcodes.disabled
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=OK".format(opcodes.status_code))
        self.uut.data = mock_data

        expected_message = "Unable verify {0} 'Status'. Received: OK, Expected: DS".format(self.uut.ds)
        try:
            self.uut.verify_status(expected_ss)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_message_pass1(self):
        """
        verify_message pass case 1
        Create test object
        Mock: get_message because it goes outside of the method
            Return: None so it does not fail
        Mock: mg.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id, and expected_date_time_from_cn
        Mock: build_message_string (note the [opcodes.message_id])
            Return: Four different strings, different than the ones used for expected_message_id, controller_date_time
            expected_date_time, and expected_message_text.
        Run: the test, and pass a status code in
        Compare: All the strings in each dictionary, and make sure it was stored properly in the objects build string
        """
        # Create test mainline object
        # dv = self.create_test_device_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Mock get message on cn
        mock_get_message = mock.MagicMock(side_effect=['thing'])
        self.uut.get_message = mock_get_message

        # Mock get_value_string_by_key
        mock_mg = mock.MagicMock()
        mock_mg.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, message_date])
        self.uut.get_message = mock.MagicMock(return_value=mock_mg)

        self.uut.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Run test
        self.uut.verify_message(_status_code='OK')

        # Compare actual strings to expected string
        self.assertEqual({'DT': '5/20/15 3:20:11', 'ID': 'blah2', 'TX': 'blah'}, self.uut.build_message_string)

    #################################
    def test_verify_message_pass2(self):
        """ Verify message on controller Pass Case 2: Created DT doesn't match controller DT, but are within +/-30 minutes
        Create test object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the object to return the three strings
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: There is nothing to compare, we just want it to run through successfully since it just verifies
        """
        # Create test mainline object
        # dv = self.create_test_device_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError, but still within 60 minutes)
        wrong_message_dt = '5/20/15 3:49:11'

        # Mock get_value_string_by_key
        mock_mg = mock.MagicMock()
        mock_mg.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, wrong_message_dt])
        self.uut.get_message = mock.MagicMock(return_value=mock_mg)

        self.uut.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Run method that will tested
        self.uut.verify_message(_status_code=opcodes.bad_serial)

    #################################
    def test_verify_message_fail1(self):
        """ Verify message on controller Fail Case 1: Created ID doesn't match controller ID, raise ValueError
        Create test object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id (which will be wrong so an
            ValueError is thrown), and expected_date_time_from_cn
        Manually fill up the build_message_string variables in the object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        # dv = self.create_test_device_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message ID (This is so we can purposefully force the ValueError)
        wrong_message_id = 'blah3'

        # Mock get_value_string_by_key
        mock_mg = mock.MagicMock()
        mock_mg.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, wrong_message_id, message_date])
        self.uut.get_message = mock.MagicMock(return_value=mock_mg)

        self.uut.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "Created ID message did not match the ID received from the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           self.uut.build_message_string[opcodes.message_id],    # {0} The ID that was built
                           wrong_message_id                                # {1} The ID returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.uut.verify_message(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_fail2(self):
        """ Verify message on controller Fail Case 2: Created DT doesn't match controller DT, raise ValueError
        Create test object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        # dv = self.create_test_device_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError)
        wrong_message_dt = '5/11/11 3:59:59'

        # Mock get_value_string_by_key
        mock_mg = mock.MagicMock()
        mock_mg.get_value_string_by_key = mock.MagicMock(side_effect=[message_text, message_id, wrong_message_dt])
        self.uut.get_message = mock.MagicMock(return_value=mock_mg)

        self.uut.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "The date and time of the message didn't match the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           self.uut.build_message_string[opcodes.date_time],  # {0} The DT that was built
                           wrong_message_dt                             # {1} The DT returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.uut.verify_message(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    #################################
    def test_verify_message_fail3(self):
        """ Verify message on controller Fail Case 3: Created TX doesn't match controller TX, raise ValueError
        Create test object
        Create the variables required to test the method
        Create the message to make the method fail
        Mock: get_message because it goes outside of the method
            Return: returns the mocked KeyValue pairs
        Mock: mg.get_value_string_by_key because it goes outside of the method
            Return: Three separate strings, for expected_message_text, expected_message_id,
            and expected_date_time_from_cn(which will be wrong so an ValueError is thrown)
        Manually fill up the build_message_string variables in the object to return the three strings
        Create an expected message that should be raised along with the ValueError
        Run: the test, and pass a status code in, and catch the expected ValueError
        Compare: The error message returned with the ValueError to the expected error message that we created
        """
        # Create test mainline object
        # dv = self.create_test_device_object()

        # Store expected values for verified message
        message_text = 'blah'
        message_id = 'blah2'
        message_date = '5/20/15 3:20:11'

        # Wrong message DT (This is so we can purposefully force the ValueError, but still within 60 minutes)
        wrong_message_tx = 'blah3'

        # Mock get_value_string_by_key
        mock_mg = mock.MagicMock()
        mock_mg.get_value_string_by_key = mock.MagicMock(side_effect=[wrong_message_tx, message_id, message_date])
        self.uut.get_message = mock.MagicMock(return_value=mock_mg)

        self.uut.build_message_string = {'ID': message_id, 'DT': message_date, 'TX': message_text}

        # Create the error message that will be compared to the actual error message
        expected_msg = "Created TX message did not match the TX received from the controller:\n" \
                       "\tCreated: \t\t'{0}'\n" \
                       "\tReceived:\t\t'{1}'\n".format(
                           self.uut.build_message_string[opcodes.message_text],  # {0} The TX that was built
                           wrong_message_tx                                 # {1} The TX returned from controller
                       )

        # Run method that will tested
        with self.assertRaises(ValueError) as context:
            self.uut.verify_message(_status_code=opcodes.bad_serial)

        # Compare actual strings to expected string. Only need the first one because all three are the same
        self.assertEqual(expected_msg, context.exception.message)

    def test_build_default_description(self):
        expected = "Test Zone 1"
        result = self.uut.build_default_description()
        self.assertEqual(result, expected)

    def test_get_id(self):
        expected = ",ZN=1"
        result = self.uut.get_id()
        self.assertEqual(result, expected)

    def test_get_id_3200(self):
        self.uut.ty = opcodes.basestation_3200
        self.uut.identifiers = [["CN"]]
        expected = ",CN"
        result = self.uut.get_id()
        self.assertEqual(result, expected)

    def test_build_obj_configuration_for_send(self):
        expected_msg = "build_obj_configuration_for_send should be implemented in each concrete device class"
        with self.assertRaises(NotImplementedError) as context:
            self.uut.build_obj_configuration_for_send()
        self.assertEqual(expected_msg, context.exception.message)

    @mock.patch.object(BaseMethods,'build_obj_configuration_for_send')
    def test_send_programming_happy_path(self,
                              build_obj_configuration_for_send_mock):
        """
        Test that send_programming sends a command generated by build_obj_configuration_for_send
        """
        expected_command = "SET,ML=1,DS=Test Mainline 1,EN=TR,FL=0,LC=FA,SH=FA,FW=FA,FV=0,MF=0,MW=FA,UN=TM,FT=120"
        build_obj_configuration_for_send_mock.return_value = expected_command
        self.uut.send_programming()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    @mock.patch.object(BaseMethods,'build_obj_configuration_for_send')
    def test_send_programming_exception(self,
                                        build_obj_configuration_for_send_mock):
        """
        Test handling of an exception in send_and_wait_for_reply in send_programming
        """
        expected_command = "SET,ML=1,DS=Test Mainline 1,EN=TR,FL=0,LC=FA,SH=FA,FW=FA,FV=0,MF=0,MW=FA,UN=TM,FT=120"
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Test Zone 1's 'Values' to: '{0}'\n -> {1}".format(expected_command, exception_msg)
        build_obj_configuration_for_send_mock.return_value = expected_command
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.uut.send_programming()

        self.assertEqual(expected_msg, context.exception.message)

    def test_set_enabled_happy_path(self):
        self.uut.en = opcodes.false
        expected_command = "SET,ZN=1,EN=TR"
        self.uut.set_enabled()
        self.mock_send_and_wait_for_reply.assert_called_once_with(tosend=expected_command)

    def test_set_enabled_exception(self):
        self.uut.en = opcodes.false
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Test Zone 1 enabled state to: TR"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.uut.set_enabled()

        self.assertEqual(expected_msg, context.exception.message)

    def test_set_disabled_happy_path(self):
        self.uut.en = opcodes.true
        expected_command = "SET,ZN=1,EN=FA"
        self.uut.set_disabled()
        self.mock_send_and_wait_for_reply.assert_called_once_with(tosend=expected_command)

    def test_set_disabled_exception(self):
        self.uut.en = opcodes.false
        exception_msg = "Test Exception"
        expected_msg = "Exception occurred trying to set Test Zone 1 enabled state to: FA"
        self.mock_send_and_wait_for_reply.side_effect = Exception(exception_msg)

        with self.assertRaises(Exception) as context:
            self.uut.set_disabled()

        self.assertEqual(expected_msg, context.exception.message)

    def test_get_data_and_verify_status(self):
        expected_status = opcodes.okay
        mock_data = status_parser.KeyValues("SN=TSD0001,{0}={1}".format(opcodes.status_code,opcodes.okay))
        self.mock_ser.get_and_wait_for_reply.return_value = mock_data
        # self.uut.data = mock_data
        # self.uut.data.get_value_string_by_key = mock.MagicMock(return_value = 'FA')
        self.uut.get_data_and_verify_status(expected_status)

    @mock.patch.object(messages, 'message_to_get')
    def test_verify_message_not_present_1(self,
                                        message_to_get_mock):
        mock_data = "ID=1_ZN_1_OK,DT=3/15/16 15:49:51,TX=Zone 1-TSQ0031\\\nSolenoid Short"
        message_to_get_mock.return_value = mock_data
        self.mock_ser.get_and_wait_for_reply.side_effect = Exception("NM No Message Found")
        self.uut.verify_message_not_present(opcodes.watering)

    @mock.patch.object(messages, 'message_to_get')
    def test_verify_message_not_present_2(self,
                                        message_to_get_mock):
        mock_data = "ID=1_ZN_1_OK,DT=3/15/16 15:49:51,TX=Zone 1-TSQ0031\\\nSolenoid Short"
        message_to_get_mock.return_value = mock_data
        self.mock_ser.get_and_wait_for_reply.side_effect = Exception("Some other exception message")
        self.uut.verify_message_not_present(opcodes.watering)

    def test_verify_enabled_state_verified(self):
        self.uut.data = mock.MagicMock()
        self.uut.data.get_value_string_by_key = mock.MagicMock(return_value = 'TR')

        self.uut.verify_enabled_state()

    def test_verify_enabled_state_not_verified(self):
        expected_msg = "Unable verify Test Zone 1 'Enabled State'. Received: FA, Expected: TR"
        self.uut.data = mock.MagicMock()
        self.uut.data.get_value_string_by_key = mock.MagicMock(return_value = 'FA')

        with self.assertRaises(ValueError) as context:
            self.uut.verify_enabled_state()

        self.assertEqual(expected_msg, context.exception.message)

    def test_build_base_string_for_getters(self):
        expected = "GET,ZN=1"
        result = self.uut.build_base_string_for_getters()
        self.assertEqual(expected, result)

    def test_build_base_string_for_setters(self):
        expected = "SET,ZN=1"
        result = self.uut.build_base_string_for_setters()
        self.assertEqual(expected, result)

    if __name__ == "__main__":
        unittest.main()
