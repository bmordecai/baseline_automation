import unittest2 as unittest
# handles the test server operations
from common.server_handler import ServerHandler

# handles the server web socket which sends and receives messages
from common.objects.base_classes.websocket_test_client import TestClient

# handles building TQ and TS packets
import common.objects.base_classes.old_data_group_packets as packet_builder

# handles verifiying a packet
import common.objects.base_classes.verify_te_packet as packet_verifier

import mock



class TestPacketVerificationSystem(unittest.TestCase):
    def setUp(self):
        """ Setting up for the test. """
        # mock items

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def test_packet_verification_system_dg_302(self):
        pass
        server_ip_address = '127.0.0.1'
        server_port = 900
        test_controller = mock.MagicMock()


        # start server
        server_handler = ServerHandler(server_ip_address, server_port)
        test_result = server_handler.verify_server_running()


        # connect a controller to the server
        # this will be done through the test commands
        test_controller.client = TestClient(server_ip_address, server_port)

        # connect the controller to the server
        # need to figure out how the server performs the handshake
        test_controller.client.start_client()

        # get the server web socket
        test_controller.server_web_socket = server_handler.get_server_web_socket()

        # set the connection type on the controller
        test_controller.connection_type = "web_socket"
        test_controller.mac = '0004A3765054'


        # create the test engine object


            # set the data group being tested on the test engine object in base class?

            # set parameters on the test engine object and use table programming to set the attributes in the controller.
            # The set commands will only go through the web socket to set parameters.

        # call "verify who I am" on the test engine object which sends a TQ packet to the controller,
        # sets self.data to the values returned in the TE packet and verifies that everything equals
        # what it should be.

        # shutdown server
        server_handler.shutdown_server()
