__author__ = 'baseline'

import unittest
import mock
import status_parser
import serial
import serial.urlhandler.protocol_socket as eth_serial
import platform

from common.objects.base_classes.devices import BaseDevices
from common.imports import *
from common.objects.baseunit.bu_packet import BUSerialPacket
from common.objects.baseunit.bu_imports import *

from common.objects.base_classes.ser import Ser
Ser.unit_testing = True


class TestBUSerialObject(unittest.TestCase):
    """
    This is testing the ser.py object but it is specific to methods that are used by the BaseUnit.
    """

    def setUp(self):
        """ Setting up for the test. """
        # Create a mock variable for a serial connection
        self.mock_serial_conn = mock.MagicMock(spec=serial.Serial)
        self.mock_serial_conn._isOpen = False

        self.mock_sendln = mock.MagicMock(side_effect=None)

        self.mock_waitln = mock.MagicMock(side_effect=None)

        self.mock_controller_reply = mock.MagicMock(side_effect=None)

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    def create_test_serial_object(self, _diff_comport=None, _diff_eth_port=None):
        """ Creates a new serial object for use in a unit test """
        # Create a serial object for communicating over the comport
        if _diff_comport is not None:
            ser = Ser(comport=_diff_comport, eth_port=None)

        # Create a serial object for communicating over the ethernet port
        elif _diff_eth_port is not None:
            ser = Ser(comport=None, eth_port=_diff_eth_port)

        # Create a default serial object which defaults to communicating over the ethernet port.
        else:
            ser = Ser(comport=None, eth_port=None)

        ser.serial_conn = self.mock_serial_conn
        ser.unit_testing = True
        return ser

    def create_send_packet(self, _address, _command):
        """
        :param _address: Address for BaseUnit
        :type _address: int
        :param _command: Command to send to BaseUnit
        :type _command: str
        :return: A BU Serial Packet ready to send over serial port
        :rtype: BUSerialPacket
        """
        bu_send_packet = BUSerialPacket(_address_base=_address, _command=_command)
        return bu_send_packet

    def create_return_packet(self, status):
        """
        :return: A BU Serial Packet returned from base_unit
        :rtype: BUSerialPacket
        """
        bu_return_packet = BUSerialPacket(_address_base=BaseUnitConstants.NULL_ADDRESS, _command=1)
        bu_return_packet.status = status
        return bu_return_packet

    def test_bu_send_and_wait_for_reply_pass_1(self):
        """ Successful communication through serial port to and from Base Unit with no retries """
        test_pass = False
        ser = self.create_test_serial_object()
        s_packet = self.create_send_packet(_address=1, _command=BaseUnitCommands.BASE_ECHO)
        r_packet = self.create_return_packet(status=BaseErrors.STATUS_OK)
        returned_packet = None
        ser.sendln = self.mock_sendln

        # Mock the controller's reply
        # Here, we use a list to return a new value each time ser.serial_conn.read is called
        # first iteration, .read returns a packet
        # second iteration, .read returns a None (breaking out of the while loop)
        mock_read = mock.MagicMock(side_effect=[r_packet.pack(), None])
        ser.serial_conn.read = mock_read

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                returned_packet = ser.bu_send_and_wait_for_reply(new_packet=s_packet)

        # Catches an Assertion Error from above, meaning 'send_and_wait_for_reply' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertIsInstance(returned_packet, BUSerialPacket)
        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    def test_bu_send_and_wait_for_reply_pass_2(self):
        """ Successful communication through serial port to and from Base Unit with 1 retry """
        test_pass = False
        ser = self.create_test_serial_object()
        s_packet = self.create_send_packet(_address=1, _command=BaseUnitCommands.BASE_ECHO)
        r_packet_1 = self.create_return_packet(status=BaseErrors.CHECKSUM_BAD)
        r_packet_2 = self.create_return_packet(status=BaseErrors.STATUS_OK)
        returned_packet = None
        ser.sendln = self.mock_sendln

        # Mock the controller's reply
        mock_read = mock.MagicMock(side_effect=[r_packet_1.pack(), r_packet_2.pack(), None])
        ser.serial_conn.read = mock_read

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                returned_packet = ser.bu_send_and_wait_for_reply(new_packet=s_packet)

        # Catches an Assertion Error from above, meaning 'send_and_wait_for_reply' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertIsInstance(returned_packet, BUSerialPacket)
        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    def test_bu_send_and_wait_for_reply_fail_1(self):
        """ Verifies exception and message raised after 3 retry attempts. """
        test_pass = False
        ser = self.create_test_serial_object()
        s_packet = self.create_send_packet(_address=1, _command=BaseUnitCommands.BASE_ECHO)

        error_to_receive = BaseErrors.CHECKSUM_BAD
        r_packet_1 = self.create_return_packet(status=error_to_receive)
        r_packet_2 = self.create_return_packet(status=error_to_receive)
        r_packet_3 = self.create_return_packet(status=error_to_receive)
        r_packet_4 = self.create_return_packet(status=error_to_receive)
        ser.sendln = self.mock_sendln

        # Mock the controller's reply
        mock_read = mock.MagicMock(side_effect=[r_packet_1.pack(), None, r_packet_2.pack(), None,
                                                r_packet_3.pack(), None, r_packet_4.pack(), None])
        ser.serial_conn.read = mock_read

        with self.assertRaises(ValueError) as context:
            returned_packet = ser.bu_send_and_wait_for_reply(new_packet=s_packet)

        expected_msg = "Error occurred waiting for reply from Base Unit: Error Code: {error}".format(
            error=error_to_receive
        )
        self.assertEqual(first=expected_msg, second=context.exception.message)

    def test_bu_send_with_no_reply_pass_1(self):
        """ Successful communication through serial port to and from Base Unit with no retry """
        test_pass = False
        ser = self.create_test_serial_object()
        s_packet = self.create_send_packet(_address=1, _command=BaseUnitCommands.BASE_ECHO)
        ser.sendln = self.mock_sendln

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                ser.bu_send_with_no_reply(new_packet=s_packet)

        # Catches an Assertion Error from above, meaning 'send_and_wait_for_reply' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    def test_bu_send_with_no_reply_pass_2(self):
        """ Successful communication through serial port to and from Base Unit with 1 retry """
        test_pass = False
        ser = self.create_test_serial_object()
        s_packet = self.create_send_packet(_address=1, _command=BaseUnitCommands.BASE_ECHO)
        self.mock_sendln.side_effect = [Exception, None]
        ser.sendln = self.mock_sendln

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised
            with self.assertRaises(Exception):
                ser.bu_send_with_no_reply(new_packet=s_packet)

        # Catches an Assertion Error from above, meaning 'send_and_wait_for_reply' method did NOT raise an exception
        # This means the method executed successfully
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    def test_bu_send_with_no_reply_fail_1(self):
        """ Verifies exception and message raised after 3 retry attempts. """
        test_pass = False
        ser = self.create_test_serial_object()
        s_packet = self.create_send_packet(_address=1, _command=BaseUnitCommands.BASE_ECHO)
        exception = Exception("Foo")
        self.mock_sendln.side_effect = [Exception, Exception, exception]
        ser.sendln = self.mock_sendln

        with self.assertRaises(Exception) as context:
            ser.bu_send_with_no_reply(new_packet=s_packet)

        expected_msg = "Foo"
        self.assertEqual(first=expected_msg, second=context.exception.message)

