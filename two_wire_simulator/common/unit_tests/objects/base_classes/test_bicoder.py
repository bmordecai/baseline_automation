import unittest
import mock
import serial
import status_parser

from common.imports import opcodes
from common.objects.base_classes.bicoders import BiCoder
from common.variables import common as common_vars
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.controllers.bl_sb import Substation
from common.imports.types import BiCoderCommands

__author__ = 'Eldin'


class TestBiCoderObject(unittest.TestCase):
    """
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = mock.MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = mock.MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        # Create a mock 3200 to pass into the zone.
        self.bl_3200 = mock.MagicMock(spec=BaseStation3200)
        self.bl_3200.ser = self.mock_ser
        
        # Add faux io flag
        self.bl_3200.faux_io_enabled = True

        # test_name = self.shortDescription()
        test_name = self._testMethodName

        print("------------------------------------------------------------------------------------------------")
        # print("Starting test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        # test_name = self.shortDescription()
        test_name = self._testMethodName
        # print("Ending test: '" + (test_name if test_name is not None else self._testMethodName)) + "'"
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    #################################
    def create_test_biCoder_object_for_3200(self, _serial_number="TSD0001"):
        """ Creates a new Master Valve object for testing purposes """
        bicoder = BiCoder(_serial_number=_serial_number, _controller=self.bl_3200, _address=1)
        return bicoder
    
    #################################
    def create_test_biCoder_object_for_substation(self, _serial_number="TSD0001"):
        """ Creates a new Master Valve object for testing purposes """
        # Creating a mock serial.
        self.mock_sub_ser = mock.MagicMock(spec=serial.Serial)
        self.mock_substation = mock.MagicMock(spec=Substation)
        self.mock_substation.ser = self.mock_sub_ser
        bicoder = BiCoder(_serial_number=_serial_number, _controller=self.mock_substation, _address=1)
        return bicoder

    #################################
    def test_set_default_values_pass1(self):
        """ Set Default Values Pass Case 1: Normal successful run-through """
        bicoder = self.create_test_biCoder_object_for_3200()
        bicoder.id = "ZN"

        # The command we expected our method to be called with
        expected_command = "SET,{0}={1},{2}={3}".format(bicoder.id, bicoder.sn, opcodes.two_wire_drop, "1.7")

        # Method call for the method we are testing
        bicoder.set_default_values()

        # Confirm that our value was called for
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_default_values_fail1(self):
        """ Set Default Values Fail Case 1: Error while sending a command through serial port """
        bicoder = self.create_test_biCoder_object_for_3200()
        bicoder.ty = opcodes.zone
        bicoder.id = BiCoderCommands.Type.VALVE

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_send_and_wait_for_reply.side_effect = AssertionError

        expected_command = "SET,{0}={1},{2}={3}".format(BiCoderCommands.Type.VALVE, str(bicoder.sn),
                                                        opcodes.two_wire_drop, "1.7")
        with self.assertRaises(Exception) as context:
            bicoder.set_default_values()
        e_msg = "Exception occurred trying to set {0} ({1})'s 'Default values' to: '{2}'".format(
            bicoder.ty, str(bicoder.sn), expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_set_two_wire_drop_value_pass1(self):
        """ Set Two Wire Drop Value Pass Case 1: Using Default _vt value """
        bicoder = self.create_test_biCoder_object_for_3200()

        # Expected value is the _va value set at object BiCoder object creation
        expected_value = bicoder.vt
        bicoder.set_two_wire_drop_value()

        # _va value is set during this method and should equal the original value
        actual_value = bicoder.vt
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_two_wire_drop_value_pass2(self):
        """ Set Two Wire Drop Value Pass Case 2: Using 6 as passed in value for vt """
        bicoder = self.create_test_biCoder_object_for_3200()

        # Expected value is the vt value set at object BiCoder object creation
        expected_value = 6
        bicoder.set_two_wire_drop_value(expected_value)

        # vt value is set during this method and should equal the original value
        actual_value = bicoder.vt
        self.assertEqual(first=expected_value, second=actual_value)

    #################################
    def test_set_two_wire_drop_value_pass3(self):
        """ Set Two Wire Drop Value Controller Pass Case 3: Command with correct values sent to controller """
        bicoder = self.create_test_biCoder_object_for_3200()
        # Set the bicoder ID so that the command populates correctly
        bicoder.id = "ZN"

        vt_value = str(bicoder.vt)
        expected_command = "SET,ZN={0},VT={1}".format(bicoder.sn, vt_value)
        bicoder.set_two_wire_drop_value()
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_two_wire_drop_value_fail1(self):
        """ Set Two Wire Drop Value Fail Case 1: Pass String value as argument """
        bicoder = self.create_test_biCoder_object_for_3200()

        new_vt_value = "b"
        with self.assertRaises(Exception) as context:
            bicoder.set_two_wire_drop_value(new_vt_value)
        expected_message = "Failed trying to set BiCoder {0}'s two wire drop value. Invalid type passed in, expected " \
                           "int or float. Received type: {1}".format(
                                str(bicoder.sn),    # {0}
                                type(new_vt_value)  # {1}
                            )
        self.assertEqual(expected_message, context.exception.message)

    #################################
    def test_set_two_wire_drop_value_flow_fail2(self):
        """ Set Two Wire Drop Value Fail Case 2: Failed communication with substation """
        bicoder = self.create_test_biCoder_object_for_3200()

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_ser.send_and_wait_for_reply.side_effect = Exception

        with self.assertRaises(Exception) as context:
            bicoder.set_two_wire_drop_value()
        e_msg = "Exception occurred trying to set {0} {1}'s 'Two Wire Drop Value' to: '{2}'".format(
            str(bicoder.ty), str(bicoder.sn), str(bicoder.vt))
        self.assertEqual(first=e_msg, second=context.exception.message)

    #################################
    def test_verify_two_wire_drop_value_pass1(self):
        """ Verify Two-wire Drop Pass Case 1: Exception is not raised """
        bicoder = self.create_test_biCoder_object_for_3200()
        bicoder.vt = 5.0
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,VT=5")
        bicoder.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the verify_high_flow... method
            with self.assertRaises(Exception):
                bicoder.verify_two_wire_drop_value(_data=mock_data)

        # Catches an Assertion Error from above, meaning the verify_high_flow... method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")
        
    #################################
    def test_verify_two_wire_drop_value_pass2(self):
        """ Verify Two-wire Drop Pass Case 2: Exception is not raised when verifying a real device """
        bicoder = self.create_test_biCoder_object_for_3200()
        bicoder.vt = 0.9

        self.bl_3200.faux_io_enabled = False
    
        mock_data = status_parser.KeyValues("SN=TSD0001,VT=1.3")
        bicoder.data = mock_data

        test_pass = bicoder.verify_two_wire_drop_value(_data=mock_data)
        self.assertTrue(test_pass)

    #################################
    def test_verify_two_wire_drop_value_fail1(self):
        """ Verify Solenoid Voltage Fail Case 1: Value on substation does not match what is
        stored in vt """
        bicoder = self.create_test_biCoder_object_for_3200()
        bicoder.vt = 5.0
        bicoder.ty = "BiCoder"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,VT=6")
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} {1}'s two wire drop value. Received: {2}, Expected: {3}".format(
            str(bicoder.ty),    # {0}
            str(bicoder.sn),    # {1}
            "6.0",              # {2} it is 6 because that is what we declared in our mock data
            str(bicoder.vt)     # {3}
        )
        try:
            bicoder.verify_two_wire_drop_value(_data=mock_data)

        # Catches an Exception from above, meaning the verify_high_flow... method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_two_wire_drop_value_fail2(self):
        """ Verify Solenoid Voltage Fail Case 2: Value on controller does not match what is stored in vt using a real
            device"""
        bicoder = self.create_test_biCoder_object_for_3200()
        bicoder.vt = 5.0
        bicoder.ty = "BiCoder"
    
        mock_data = status_parser.KeyValues("SN=TSD0001,VT=5.4")
        bicoder.data = mock_data
        
        self.bl_3200.faux_io_enabled = False
    
        expected_message = "Unable to verify {0} {1}'s (real device) two wire drop value. " \
                           "Received: {2}, Expected: {3}".format(
                               bicoder.ty,    # {0}
                               bicoder.sn,    # {1}
                               5.4,           # {2} it is 6 because that is what we declared in our mock data
                               bicoder.vt     # {3}
                           )
        
        with self.assertRaises(ValueError) as context:
            bicoder.verify_two_wire_drop_value(_data=mock_data)
        
        e_msg = context.exception.message
        self.assertEqual(expected_message, e_msg)

    #################################
    def test_verify_serial_number_pass1(self):
        """ Verify Serial Number Pass Case 1: Exception is not raised """
        bicoder = self.create_test_biCoder_object_for_3200()
        bicoder.sn = "TSD0002"
        test_pass = False

        mock_data = status_parser.KeyValues("{0}=TSD0002".format(opcodes.serial_number))
        bicoder.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                bicoder.verify_serial_number(_data=mock_data)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_serial_number_fail1(self):
        """ Verify Serial Number Fail Case 1: Value on substation does not match what is
        stored in dev.sn for dev type ZN """
        bicoder = self.create_test_biCoder_object_for_3200()
        bicoder.sn = "TSD0002"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("{0}=TSD0003".format(opcodes.serial_number))
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} ({1})'s 'Serial Number'. Received: {2}, Expected: {3}".format(
            bicoder.ty,         # {0}
            bicoder.sn,         # {1}
            "TSD0003",          # {2} It is TSD0003 so that it is the same as our mock data value
            str(bicoder.sn)     # {3}
        )
        try:
            bicoder.verify_serial_number(_data=mock_data)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_status_pass1(self):
        """ Verify Status Pass Case 1: Exception is not raised """
        bicoder = self.create_test_biCoder_object_for_3200()
        expected_ss = opcodes.okay
        test_pass = False

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=OK".format(opcodes.status_code))
        bicoder.data = mock_data

        try:
            # .assertRaises raises an Assertion Error if an Exception is not raised in the method
            with self.assertRaises(Exception):
                bicoder.verify_status(_status=expected_ss, _data=mock_data)

        # Catches an Assertion Error from above, meaning the method did NOT raise an exception
        # meaning verification passed
        except AssertionError as ae:
            e_msg = ae.message
            expected_message = "Exception not raised"

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected no Exception to be raised, Exception raised.")

    #################################
    def test_verify_status_fail1(self):
        """ Verify Status Fail Case 1: Value on substation does not match what is passed into method """
        bicoder = self.create_test_biCoder_object_for_3200()
        bicoder.ty = "biCoder"
        expected_ss = opcodes.disabled
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=OK".format(opcodes.status_code))
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} ({1})'s status. Received: {2}, Expected: {3}".format(
                                str(bicoder.ty),    # {0}
                                str(bicoder.sn),    # {1}
                                "OK",               # {2}
                                expected_ss         # {3}
                            )
        try:
            bicoder.verify_status(_status=expected_ss, _data=mock_data)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_verify_status_fail2(self):
        """ Verify Status Fail Case 2: Argument passed in is not a valid status code """
        bicoder = self.create_test_biCoder_object_for_3200()
        expected_ss = "Invalid Status"
        test_pass = False
        e_msg = ""

        mock_data = status_parser.KeyValues("SN=TSD0001,{0}=OK".format(opcodes.status_code))
        bicoder.data = mock_data

        expected_message = "Unable to verify {0} ({1})'s status. Received invalid expected status: ({2}),\n" \
                           "Expected one of the following: ({3})".format(
                                str(bicoder.ty),    # {0}
                                str(bicoder.sn),    # {1}
                                expected_ss,        # {2}
                                common_vars.dictionary_for_status_codes.keys()  # {3}
                            )
        try:
            bicoder.verify_status(_status=expected_ss, _data=mock_data)

        # Catches an Exception from above, meaning the method did raise an exception
        # meaning verification failed
        except Exception as e:
            e_msg = e.message

            # Checks the assertion error caught to be sure an Exception was not raised, meaning verification passed
            if e_msg.strip() == expected_message.strip():
                test_pass = True

        self.assertEqual(test_pass, True, "Expected Exception: '{0}', Exception raised: '{1}'."
                         .format(expected_message, e_msg))

    #################################
    def test_get_data_pass1(self):
        """ Get Data From Substation Pass Case 1: Correct command is sent to substation with ZN dev type """
        bicoder = self.create_test_biCoder_object_for_3200()
        bicoder.id = opcodes.zone

        expected_command = "GET,{0}={1}".format(opcodes.zone, str(bicoder.sn))
        bicoder.get_data()
        self.mock_get_and_wait_for_reply.assert_called_with(called_by_get_data=True, tosend=expected_command)

    #################################
    def test_get_data_fail1(self):
        """ Get Data From Substation Fail Case 1: Failed communication with substation """
        bicoder = self.create_test_biCoder_object_for_3200()
        bicoder.ty = "biCoder"
        bicoder.id = BiCoderCommands.Type.VALVE

        # A contrived Exception is thrown when communicating with the mock serial port
        self.mock_get_and_wait_for_reply.side_effect = AssertionError

        expected_command = "GET,{0}={1}".format(bicoder.id, str(bicoder.sn))
        with self.assertRaises(ValueError) as context:
            bicoder.get_data()
        e_msg = "Unable to get data for {0} ({1}) using command: '{2}'. Exception raised: ".format(
            bicoder.ty, bicoder.sn, expected_command)
        self.assertEqual(first=e_msg, second=context.exception.message)
        
    # #################################
    # def test_move_from_3200_to_substation_pass1(self):
    #     """ Move bicoder from 3200 to substation Pass Case 1: Successfully moving a single-valve bicoder """
    #     # -------------
    #     # Setup Bicoder
    #     # -------------
    #     bicoder = self.create_test_biCoder_object_for_3200()
    #     # Set the bicoder ID so that the command populates correctly
    #     bicoder.id = "ZN"
    #     bicoder.decoder_type = 'D1'
    #
    #     # ----------------
    #     # Setup Substation
    #     # ----------------
    #     mock_load_dv_to_cn = mock.MagicMock()
    #     self.mock_substation.load_dv_to_cn = mock_load_dv_to_cn
    #
    #     vt_value = str(bicoder.vt)
    #     expected_command = "SET,ZN={0},VT={1}".format(bicoder.sn, vt_value)
    #     bicoder.set_two_wire_drop_value()
    #     self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)


if __name__ == "__main__":
    unittest.main()
