from __future__ import absolute_import
__author__ = 'Ben'

import unittest
from mock import create_autospec, Mock, MagicMock, patch, NonCallableMagicMock, NonCallableMock
import serial
import datetime

from common.imports import opcodes
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.programming.ws import WaterSource

# from common.objects.controllers.bl_fs import FlowStation
# from common.objects.controllers.bl_sb import Substation

from common.date_package.date_resource import date_mngr


class TestWaterSourceMessages(unittest.TestCase):
    """
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + self._testMethodName + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        self.mock_send_and_wait_for_reply.side_effect = None
        self.mock_send_and_wait_for_reply.return_value = None

        self.mock_get_and_wait_for_reply.side_effect = None
        self.mock_get_and_wait_for_reply.return_value = None

        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

        # :type mocked_base_station_3200: common.objects.controllers.bl_32.BaseStation3200

    # TODO:

    #################################
    @patch('common.objects.programming.ws.WaterSource', autospec=True)
    @patch('common.objects.controllers.bl_32.BaseStation3200', autospec=True)
    def create_test_object(self, mocked_bl_3200_class, mocked_ws_class, ws_address=1):
        """

        :param ws_address: Address of WS to create
        :type ws_address: int
        :return:
        """
        mock_3200 = MagicMock(spec=mocked_bl_3200_class)
        mock_ws = MagicMock(spec=mocked_ws_class)
        mock_ws.controller = mock_3200
        # mock_ws_msg_obj = MagicMock
