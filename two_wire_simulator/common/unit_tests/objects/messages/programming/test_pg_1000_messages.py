from __future__ import absolute_import
__author__ = 'Ben'

import unittest
from mock import create_autospec, Mock, MagicMock, patch, NonCallableMagicMock, NonCallableMock
import serial
import datetime

from common.imports import opcodes
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.messages.programming.pg_1000_messages import ProgramMessages
from common.imports.types import ActionCommands, ProgramCommands

from common.date_package.date_resource import date_mngr


class TestPG1000Messages(unittest.TestCase):
    """
    """

    #################################
    def setUp(self):
        """ Setting up for the test. """
        # Creating a mock serial.
        self.mock_ser = MagicMock(spec=serial.Serial)

        # Create mock send method
        self.mock_send_and_wait_for_reply = MagicMock(side_effect=None)

        # Assign mock send method to what it was trying to mock
        self.mock_ser.send_and_wait_for_reply = self.mock_send_and_wait_for_reply

        # Create mock get method
        self.mock_get_and_wait_for_reply = MagicMock(side_effect=None)

        # Assign mock get method to what it was trying to mock
        self.mock_ser.get_and_wait_for_reply = self.mock_get_and_wait_for_reply

        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + self._testMethodName + "'")
        print("Covers: " + str(self.shortDescription()))

    #################################
    def tearDown(self):
        """
        Cleaning up after the test.
        """
        self.mock_send_and_wait_for_reply.side_effect = None
        self.mock_send_and_wait_for_reply.return_value = None

        self.mock_get_and_wait_for_reply.side_effect = None
        self.mock_get_and_wait_for_reply.return_value = None

        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

        # :type mocked_base_station_3200: common.objects.controllers.bl_32.BaseStation3200

    #################################
    @patch('common.objects.programming.pg_1000.PG1000', autospec=True)
    @patch('common.objects.controllers.bl_10.BaseStation1000', autospec=True)
    def create_test_object(self, mocked_bl_1000_class, mocked_pg_class, pg_address=1):
        """

        :param ws_address: Address of WS to create
        :type ws_address: int
        :return:
        """
        mock_1000 = MagicMock(spec=mocked_bl_1000_class)
        mock_1000.ser = self.mock_ser
        mock_program = MagicMock(spec=mocked_pg_class)
        mock_program.controller = mock_1000
        mock_program.ad = pg_address
        program_message = ProgramMessages(_program_object=mock_program)
        return program_message

    #################################
    def test_set_start_message_pass1(self):
        """Tests the sending of the command that will trigger the controller to create a program start message"""
        pg_message = self.create_test_object()
        who_started_program = opcodes.basemanager

        # Call the method we are testing
        pg_message.set_start_message(_who_started_the_program=who_started_program)

        # Create an expected message and compare it with what was actually sent through the serial port
        expected_command = "{0},{1},{2}={3},{4}={5},{6}={7}".format(ActionCommands.SET,
                                                                    opcodes.message,
                                                                    ProgramCommands.Program,
                                                                    pg_message.program.ad,
                                                                    opcodes.status_code,
                                                                    opcodes.start,
                                                                    opcodes.who,
                                                                    who_started_program)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_start_message_pass1(self):
        """Tests the sending of the command that will trigger the controller to send back the message if they have it"""
        pg_message = self.create_test_object()
        who_started_program = opcodes.basemanager

        # Call the method we are testing
        pg_message.get_start_message(_who_started_the_program=who_started_program)

        # Create an expected message and compare it with what was actually sent through the serial port
        expected_command = "{0},{1},{2}={3},{4}={5},{6}={7}".format(ActionCommands.GET,
                                                                    opcodes.message,
                                                                    ProgramCommands.Program,
                                                                    pg_message.program.ad,
                                                                    opcodes.status_code,
                                                                    opcodes.start,
                                                                    opcodes.who,
                                                                    who_started_program)
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_verify_start_message_pass1(self):
        """Tests the sending of the command that will return us values we need to verify against"""
        pg_message = self.create_test_object()
        who_started_program = opcodes.basemanager

        # Mock the data we get back from the controller, like the text, ID, and date
        mock_data = MagicMock()
        mock_get_value_string_by_key = MagicMock()
        returned_message_text = "Program {0}nStart: BaseManager Nov 20, 2:44P".format(pg_message.program.ad)
        returned_message_id = "{0}_{1}_{2}_{3}".format(300, opcodes.program, pg_message.program.ad, opcodes.start)
        returned_message_date = "11/20/18 14:44:00"
        mock_get_value_string_by_key.side_effect = [returned_message_text, returned_message_id, returned_message_date]
        self.mock_get_and_wait_for_reply.return_value = mock_data
        mock_data.get_value_string_by_key = mock_get_value_string_by_key

        # Mock the datetime in our objects.
        date_mngr.controller_datetime.time_obj.obj = datetime.time(hour=14, minute=44, second=0)
        date_mngr.controller_datetime.obj = datetime.date(year=2018, month=11, day=20)

        # Call the method we are testing
        pg_message.verify_start_message(_who_started_the_program=who_started_program)

        # Create an expected message and compare it with what was actually sent through the serial port
        expected_command = "{0},{1},{2}={3},{4}={5},{6}={7}".format(ActionCommands.GET,
                                                                    opcodes.message,
                                                                    ProgramCommands.Program,
                                                                    pg_message.program.ad,
                                                                    opcodes.status_code,
                                                                    opcodes.start,
                                                                    opcodes.who,
                                                                    who_started_program)
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_pause_message_pass1(self):
        """Tests the sending of the command that will trigger the controller to create a program pause message"""
        pg_message = self.create_test_object()
        who_paused_program = opcodes.moisture_sensor

        # Call the method we are testing
        pg_message.set_pause_message(_who_paused_the_program=who_paused_program)

        # Create an expected message and compare it with what was actually sent through the serial port
        expected_command = "{0},{1},{2}={3},{4}={5},{6}={7}".format(ActionCommands.SET,
                                                                    opcodes.message,
                                                                    ProgramCommands.Program,
                                                                    pg_message.program.ad,
                                                                    opcodes.status_code,
                                                                    opcodes.pause,
                                                                    opcodes.who,
                                                                    who_paused_program)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_pause_message_pass1(self):
        """Tests the sending of the command that will trigger the controller to send back the message if they have it"""
        pg_message = self.create_test_object()
        who_paused_program = opcodes.moisture_sensor

        # Call the method we are testing
        pg_message.get_pause_message(_who_paused_the_program=who_paused_program)

        # Create an expected message and compare it with what was actually sent through the serial port
        expected_command = "{0},{1},{2}={3},{4}={5},{6}={7}".format(ActionCommands.GET,
                                                                    opcodes.message,
                                                                    ProgramCommands.Program,
                                                                    pg_message.program.ad,
                                                                    opcodes.status_code,
                                                                    opcodes.pause,
                                                                    opcodes.who,
                                                                    who_paused_program)
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_verify_pause_message_pass1(self):
        """Tests the sending of the command that will return us values we need to verify against"""
        pg_message = self.create_test_object()
        who_paused_program = opcodes.moisture_sensor

        # Mock the data we get back from the controller, like the text, ID, and date
        mock_data = MagicMock()
        mock_get_value_string_by_key = MagicMock()
        returned_message_text = "Program {0}nPause: Moisture Nov 20, 2:44P".format(pg_message.program.ad)
        returned_message_id = "{0}_{1}_{2}_{3}".format(300, opcodes.program, pg_message.program.ad, opcodes.pause)
        returned_message_date = "11/20/18 14:44:00"
        mock_get_value_string_by_key.side_effect = [returned_message_text, returned_message_id, returned_message_date]
        self.mock_get_and_wait_for_reply.return_value = mock_data
        mock_data.get_value_string_by_key = mock_get_value_string_by_key

        # Mock the datetime in our objects.
        date_mngr.controller_datetime.time_obj.obj = datetime.time(hour=14, minute=44, second=0)
        date_mngr.controller_datetime.obj = datetime.date(year=2018, month=11, day=20)

        # Call the method we are testing
        pg_message.verify_pause_message(_who_paused_the_program=who_paused_program)

        # Create an expected message and compare it with what was actually sent through the serial port
        expected_command = "{0},{1},{2}={3},{4}={5},{6}={7}".format(ActionCommands.GET,
                                                                    opcodes.message,
                                                                    ProgramCommands.Program,
                                                                    pg_message.program.ad,
                                                                    opcodes.status_code,
                                                                    opcodes.pause,
                                                                    opcodes.who,
                                                                    who_paused_program)
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_set_stop_message_pass1(self):
        """Tests the sending of the command that will trigger the controller to create a program stop message"""
        pg_message = self.create_test_object()
        who_stopped_program = opcodes.moisture_sensor

        # Call the method we are testing
        pg_message.set_stop_message(_who_stopped_the_program=who_stopped_program)

        # Create an expected message and compare it with what was actually sent through the serial port
        expected_command = "{0},{1},{2}={3},{4}={5},{6}={7}".format(ActionCommands.SET,
                                                                    opcodes.message,
                                                                    ProgramCommands.Program,
                                                                    pg_message.program.ad,
                                                                    opcodes.status_code,
                                                                    opcodes.stop,
                                                                    opcodes.who,
                                                                    who_stopped_program)
        self.mock_send_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_get_stop_message_pass1(self):
        """Tests the sending of the command that will trigger the controller to send back the message if they have it"""
        pg_message = self.create_test_object()
        who_stopped_program = opcodes.moisture_sensor

        # Call the method we are testing
        pg_message.get_stop_message(_who_stopped_the_program=who_stopped_program)

        # Create an expected message and compare it with what was actually sent through the serial port
        expected_command = "{0},{1},{2}={3},{4}={5},{6}={7}".format(ActionCommands.GET,
                                                                    opcodes.message,
                                                                    ProgramCommands.Program,
                                                                    pg_message.program.ad,
                                                                    opcodes.status_code,
                                                                    opcodes.stop,
                                                                    opcodes.who,
                                                                    who_stopped_program)
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)

    #################################
    def test_verify_stop_message_pass1(self):
        """Tests the sending of the command that will return us values we need to verify against"""
        pg_message = self.create_test_object()
        who_stopped_program = opcodes.moisture_sensor

        # Mock the data we get back from the controller, like the text, ID, and date
        mock_data = MagicMock()
        mock_get_value_string_by_key = MagicMock()
        returned_message_text = "Program {0}nStop: Moisture Nov 20, 2:44P".format(pg_message.program.ad)
        returned_message_id = "{0}_{1}_{2}_{3}".format(300, opcodes.program, pg_message.program.ad, opcodes.stop)
        returned_message_date = "11/20/18 14:44:00"
        mock_get_value_string_by_key.side_effect = [returned_message_text, returned_message_id, returned_message_date]
        self.mock_get_and_wait_for_reply.return_value = mock_data
        mock_data.get_value_string_by_key = mock_get_value_string_by_key

        # Mock the datetime in our objects.
        date_mngr.controller_datetime.time_obj.obj = datetime.time(hour=14, minute=44, second=0)
        date_mngr.controller_datetime.obj = datetime.date(year=2018, month=11, day=20)

        # Call the method we are testing
        pg_message.verify_stop_message(_who_stopped_the_program=who_stopped_program)

        # Create an expected message and compare it with what was actually sent through the serial port
        expected_command = "{0},{1},{2}={3},{4}={5},{6}={7}".format(ActionCommands.GET,
                                                                    opcodes.message,
                                                                    ProgramCommands.Program,
                                                                    pg_message.program.ad,
                                                                    opcodes.status_code,
                                                                    opcodes.stop,
                                                                    opcodes.who,
                                                                    who_stopped_program)
        self.mock_get_and_wait_for_reply.assert_called_with(tosend=expected_command)
