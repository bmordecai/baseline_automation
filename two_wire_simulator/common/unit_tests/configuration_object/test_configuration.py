from unittest import TestCase

__author__ = 'john'

import os
import mock

import common.imports.opcodes as opcodes
from common.configuration import Configuration
import common.user_configuration as user_conf_module

from common.objects.controllers.bl_10 import BaseStation1000
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.controllers.bl_fs import FlowStation
from common.objects.controllers.bl_sb import Substation


class TestConfiguration(TestCase):
    """
    These test cases load data from a json file and create zones using the serial numbers that are generated.
    The serial numbers depend on the type of decorder (single, dual, quad or twelve valve decorders).
    Serial numbers are sequential, starting with the base serial number given in the json file.
    Depending on the type of device, either one, two, four or twelve serial numbers are generated.
    The json file must have the zone range correctly configured (test case 3 is an example of a bad range).
    """

    directory = 'json_files'
    file_name = 'test_user_credentials_json.json'
    dir_path = os.path.dirname(os.path.realpath(__file__))
    path_to_file = os.path.join(dir_path, directory, file_name)

    user_conf = user_conf_module.UserConfiguration(path_to_file)

    #################################
    def setUp(self):
        """ Setting up for the test. """

        configuration_dir = os.path.dirname(os.path.realpath(__file__)) + os.sep + 'json_configurations'

        self.uut = Configuration(test_name="test_configuration_object_1",
                                 user_conf_file=self.user_conf,
                                 data_json_file='test_configuration_object1.json',
                                 configuration_dir=configuration_dir)

        test_name = self._testMethodName
        print("------------------------------------------------------------------------------------------------")
        print("Starting test: '" + test_name + "'")
        print("Covers: " + str(self.shortDescription()))

    def tearDown(self):
        """ Cleaning up after the test. """
        test_name = self._testMethodName
        print("Ending test: '" + test_name + "'")
        print("------------------------------------------------------------------------------------------------\n")

    @mock.patch('common.configuration.substations')
    @mock.patch('common.configuration.flowstations')
    @mock.patch('common.configuration.basestations_3200')
    @mock.patch('common.configuration.basestations_1000')
    @mock.patch('common.configuration.server_connections')
    def test_reset_all(self,
                       mock_server_connections,
                       mock_basestations_1000,
                       mock_basestations_3200,
                       mock_flowstations,
                       mock_substations):

        self.uut.reset_all()

        self.assertEquals(mock_server_connections.clear.call_count, 1)
        self.assertEquals(mock_basestations_1000.clear.call_count, 1)
        self.assertEquals(mock_basestations_3200.clear.call_count, 1)
        self.assertEquals(mock_flowstations.clear.call_count, 1)
        self.assertEquals(mock_substations.clear.call_count, 1)

    # def test_initialize_server_for_test(self):
    #     self.fail()

    @mock.patch('common.configuration.ResourceHandler')
    @mock.patch('common.configuration.helper_methods')
    def test_initialize_for_test_1(self,
                                 mock_helper_methods,
                                 mock_resource_handler_constructor):
        """ Verify that initialize_for_test with default arguments both calls the expected methods, and doesn't call
            any unexpected methods """
        self.uut.reset_all = mock.MagicMock()
        self.uut.create_cn_objects = mock.MagicMock()
        mock_3200 = mock.MagicMock(spec=BaseStation3200)
        mock_3200.vr = "16.1.200"
        mock_3200.mac = "112233445566"
        mock_3200.data = mock.MagicMock()
        mock_3200.data.get_value_string_by_key = mock.MagicMock(return_value="16.1.200")
        mock_3200.basemanager_connection = {1 : mock.MagicMock()}
        mock_3200s = {1: mock_3200}
        self.uut.BaseStation3200 = mock_3200s

        self.uut.initialize_for_test()

        mock_helper_methods.print_test_started.assert_called_with(self.uut.test_name)
        self.assertEquals(self.uut.reset_all.call_count, 1)
        mock_resource_handler_constructor.assert_called_with(configuration=self.uut, user_conf=self.uut.user_conf)
        self.assertEquals(self.uut.create_cn_objects.call_count, 1)
        self.assertEquals(mock_3200.add_basemanager_connection_to_controller.call_count, 1)
        self.assertEquals(mock_3200.basemanager_connection[1].connect_cn_to_ethernet.call_count, 0)
        self.assertEquals(mock_3200.basemanager_connection[1].verify_ip_address_state.call_count, 0)

    @mock.patch('common.configuration.ResourceHandler')
    @mock.patch('common.configuration.helper_methods')
    def test_initialize_for_test_2(self,
                                   mock_helper_methods,
                                   mock_resource_handler_constructor):
        """ Verify that, when initialize_for_test with connect_to_basemanager set to True has an exception in
        add_basemanager_connection_to_controller, connect_cn_to_ethernet, or verify_ip_address_state, the expected
        exception is raised """
        self.uut.reset_all = mock.MagicMock()
        self.uut.create_cn_objects = mock.MagicMock()
        mock_3200 = mock.MagicMock(spec=BaseStation3200)
        mock_3200.mac = '0008EE408EA7'
        mock_3200.data = mock.MagicMock()
        mock_3200.add_basemanager_connection_to_controller = mock.MagicMock()
        exception_msg = "Test Exception"
        mock_3200.add_basemanager_connection_to_controller.side_effect = Exception(exception_msg)
        mock_3200.basemanager_connection = {1 : mock.MagicMock()}
        mock_3200s = {1: mock_3200}
        self.uut.BaseStation3200 = mock_3200s
        e_msg = ("Exception occurred trying to connect Controller To BaseManager: Message '{0}'."
                 .format(exception_msg))

        with self.assertRaises(Exception) as context:
            self.uut.initialize_for_test(connect_to_basemanager=True)

        self.assertEqual(first=e_msg, second=context.exception.message)


    # def test_create_server_object(self):
    #     self.fail()

    @mock.patch('common.configuration.time.sleep')
    @mock.patch('common.configuration.BaseStation3200')
    def test_create_cn_objects_3200(self, mock_3200_constructor, mock_sleep):
        configuration_dir = os.path.dirname(os.path.realpath(__file__)) + os.sep + 'json_configurations'
        test_name = "test_configuration_object_single_3200"
        self.uut = Configuration(test_name=test_name,
                                 user_conf_file=self.user_conf,
                                 data_json_file=test_name + '.json',
                                 configuration_dir=configuration_dir)

        self.uut.resource_handler = mock.MagicMock()
        self.uut.resource_handler.create_serial_connection = mock.MagicMock()
        controller_ser = mock.MagicMock()
        self.uut.resource_handler.create_serial_connection.return_value = controller_ser
        self.uut.load_data_from_json_file = mock.MagicMock()
        mock_3200 = mock.MagicMock(spec=BaseStation3200)
        mock_3200_constructor.return_value = mock_3200
        list_of_device_types = mock.MagicMock()
        self.uut.build_list_of_device_types_present_in_json = mock.MagicMock()
        self.uut.build_list_of_device_types_present_in_json.return_value = list_of_device_types
        self.uut.create_bl_3200_devices_objects = mock.MagicMock()

        self.uut.create_cn_objects()

        self.assertEquals(self.uut.resource_handler.create_serial_connection.call_count, 2)
        # comport and ethernet_port come from test_user_credentials_json.json
        self.uut.resource_handler.create_serial_connection.assert_called_with(comport='None',
                                                                              ethernet_port='socket://10.11.12.240:10002')
        self.assertEquals(controller_ser.init_ser_connection.call_count, 2)
        self.assertEquals(controller_ser.check_if_cn_connected.call_count, 2)
        self.uut.load_data_from_json_file.assert_called_with(_controller_type=opcodes.basestation_3200,
                                                             _controller_number=1)
        # serial_nuber, firmware_version, mac, port_address, socket_port, and ip_address come from
        # test_user_credentials_json.json
        mock_3200_constructor.assert_called_with(_address=1,
                                                 _description=test_name,
                                                 _serial_port=controller_ser,
                                                 _serial_number='3K10001',
                                                 _firmware_version='16.0.527',
                                                 _mac='0008EE218C85',
                                                 _port_address='None',
                                                 _socket_port='socket://10.11.12.240:10002',
                                                 _ip_address='10.11.12.57')
        self.assertEquals(self.uut.BaseStation3200[1], mock_3200)
        self.assertEquals(mock_3200.initialize_for_test.call_count, 1)
        # mock_3200.load_all_dv.assert_called_with()
        self.assertEquals(self.uut.build_list_of_device_types_present_in_json.call_count, 2)
        mock_3200.do_search_for_all_devices.assert_called_with(_list_of_device_types=list_of_device_types)
        mock_sleep.assert_called_with(5)
        self.uut.create_bl_3200_devices_objects.assert_called_with(_controller=mock_3200)
        self.assertEquals(mock_3200.assign_all_devices.call_count, 1)
        
    @mock.patch('common.configuration.time.sleep')
    @mock.patch('common.configuration.BaseStation3200')
    def test_is_configured_for_multiple_bl_3200s_pass_1(self, mock_3200_constructor, mock_sleep):
        """ Determining if multiple 3200s need to be initialized, verifies configuration for single 3200. """
        directory = 'json_files'
        file_name = 'test_user_credentials_single_32_FS.json'
        dir_path = os.path.dirname(os.path.realpath(__file__))
        path_to_file = os.path.join(dir_path, directory, file_name)
        user_conf = user_conf_module.UserConfiguration(path_to_file)
        
        configuration_dir = os.path.dirname(os.path.realpath(__file__)) + os.sep + 'json_configurations'
        test_name = "test_configuration_object_single_3200"
        self.uut = Configuration(test_name=test_name,
                                 user_conf_file=user_conf,
                                 data_json_file=test_name + '.json',
                                 configuration_dir=configuration_dir)
        
        self.assertFalse(self.uut.is_configured_for_multiple_bl_3200s())
        
    @mock.patch('common.configuration.time.sleep')
    @mock.patch('common.configuration.BaseStation3200')
    def test_is_configured_for_multiple_bl_3200s_pass_2(self, mock_3200_constructor, mock_sleep):
        """ Determining if multiple 3200s need to be initialized, verifies configuration for multiple 3200s. """
        directory = 'json_files'
        file_name = 'test_user_credentials_two_32_FS.json'
        dir_path = os.path.dirname(os.path.realpath(__file__))
        path_to_file = os.path.join(dir_path, directory, file_name)
        user_conf = user_conf_module.UserConfiguration(path_to_file)
        
        configuration_dir = os.path.dirname(os.path.realpath(__file__)) + os.sep + 'json_configurations'
        test_name = "test_configuration_object_two_3200"
        self.uut = Configuration(test_name=test_name,
                                 user_conf_file=user_conf,
                                 data_json_file=test_name + '.json',
                                 configuration_dir=configuration_dir)
        
        self.assertTrue(self.uut.is_configured_for_multiple_bl_3200s())
        
    

    # TODO: Uncomment and finish when BaseStation1000 object is ready for prime time
    # @mock.patch('common.configuration.time.sleep')
    # @mock.patch('common.configuration.BaseStation3200')
    # def test_create_cn_objects_1000(self, mock_1000_constructor, mock_sleep):
    #     configuration_dir = os.path.dirname(os.path.realpath(__file__)) + os.sep + 'json_configurations'
    #     self.uut = Configuration(test_name="test_configuration_object_single_1000",
    #                              user_conf_file=self.user_conf,
    #                              data_json_file='test_configuration_object_single_1000.json',
    #                              configuration_dir=configuration_dir)
    #
    #     self.uut.resource_handler = mock.MagicMock()
    #     self.uut.resource_handler.create_serial_connection = mock.MagicMock()
    #     controller_ser = mock.MagicMock()
    #     self.uut.resource_handler.create_serial_connection.return_value = controller_ser
    #     self.uut.load_data_from_json_file = mock.MagicMock()
    #     mock_1000 = mock.MagicMock(spec=BaseStation1000)
    #     mock_1000_constructor.return_value = mock_1000
    #     list_of_device_types = mock.MagicMock()
    #     self.uut.build_list_of_device_types_present_in_json = mock.MagicMock()
    #     self.uut.build_list_of_device_types_present_in_json.return_value = list_of_device_types
    #     self.uut.create_bl_3200_devices_objects = mock.MagicMock()
    #
    #     self.uut.create_cn_objects()
    #
    #     self.assertEquals(self.uut.resource_handler.create_serial_connection.call_count, 1)
    #     # comport and ethernet_port come from test_user_credentials_json.json
    #     self.uut.resource_handler.create_serial_connection.assert_called_with(comport='None',
    #                                                                           ethernet_port='socket://10.11.12.240:10002')
    #     self.assertEquals(controller_ser.init_ser_connection.call_count, 1)
    #     self.assertEquals(controller_ser.check_if_cn_connected.call_count, 1)
    #     self.uut.load_data_from_json_file.assert_called_with(_controller_type=opcodes.basestation_1000,
    #                                                          _controller_number=1)
    #     # serial_nuber, firmware_version, mac, port_address, socket_port, and ip_address come from
    #     # test_user_credentials_json.json
    #     mock_1000_constructor.assert_called_with(_serial_port=controller_ser,
    #                                              _serial_number='3K10001',
    #                                              _firmware_version='16.0.527',
    #                                              _mac='0008EE218C85',
    #                                              _port_address='None',
    #                                              _socket_port='socket://10.11.12.240:10002',
    #                                              _ip_address='10.11.12.57')
    #     self.assertEquals(self.uut.BaseStation3200[1], mock_1000)
    #     self.assertEquals(mock_1000.initialize_for_test.call_count, 1)
    #     # mock_3200.load_all_dv.assert_called_with()
    #     self.assertEquals(self.uut.build_list_of_device_types_present_in_json.call_count, 1)
    #     mock_1000.do_search_for_all_devices.assert_called_with(_list_of_device_types=list_of_device_types)
    #     mock_sleep.assert_called_with(5)
    #     self.uut.create_bl_3200_devices_objects.assert_called_with(_controller=mock_1000)
    #     self.assertEquals(mock_1000.assign_all_devices.call_count, 1)

    # def test_create_bicoder_objects(self):
    #     self.fail()

    # def test_create_bl_1000_programing_objects(self):
    #     self.fail()

    @mock.patch('common.configuration.create_zone_objects')
    @mock.patch('common.configuration.create_master_valve_objects')
    @mock.patch('common.configuration.create_pump_objects')
    @mock.patch('common.configuration.create_moisture_sensor_objects')
    @mock.patch('common.configuration.create_flow_meter_objects')
    @mock.patch('common.configuration.create_event_switch_objects')
    @mock.patch('common.configuration.create_temperature_sensor_objects')
    @mock.patch('common.configuration.create_pressure_sensor_objects')
    def test_create_bl_3200_devices_objects(self,
                                            mock_create_pressure_sensor_objects,
                                            mock_create_temperature_sensor_objects,
                                            mock_create_event_switch_objects,
                                            mock_create_flow_meter_objects,
                                            mock_create_moisture_sensor_objects,
                                            mock_create_pump_objects,
                                            mock_create_master_valve_objects,
                                            mock_create_zone_objects):
        mock_3200 = mock.MagicMock(spec=BaseStation3200)

        self.uut.create_bl_3200_devices_objects(mock_3200)

        # TODO: Test should populate address range, serial number, etc for a more meaningful test
        mock_create_pressure_sensor_objects.assert_called_with(address_range=[], controller=mock_3200, serial_numbers=[])
        mock_create_temperature_sensor_objects.assert_called_with(address_range=[], controller=mock_3200, serial_numbers=[])
        mock_create_event_switch_objects.assert_called_with(address_range=[], controller=mock_3200, serial_numbers=[])
        mock_create_flow_meter_objects.assert_called_with(address_range=[], controller=mock_3200, serial_numbers=[])
        mock_create_moisture_sensor_objects.assert_called_with(address_range=[], controller=mock_3200, serial_numbers=[])
        mock_create_pump_objects.assert_called_with(address_range=[], controller=mock_3200, serial_numbers=[])
        mock_create_master_valve_objects.assert_called_with(address_range=[], controller=mock_3200, serial_numbers=[])
        mock_create_zone_objects.assert_called_with(controller=mock_3200, d1=[], d2=[], d4=[], dd=[], zn_ad_range=[])


    # def test_create_substation_bicoders(self):
    #     self.fail()

    def test_load_data_from_json_file(self):
        self.uut.load_data_from_json_file(_controller_type=opcodes.basestation_3200, _controller_number=1)

    def test_build_list_of_device_types_present_in_json(self):
        self.uut.build_list_of_device_types_present_in_json()

    def test_list_is_empty_1(self):
        empty_list = []
        self.assertTrue(self.uut.list_is_empty(empty_list))

    def test_list_is_empty_2(self):
        non_empty_list = [1,2,3,4]
        self.assertFalse(self.uut.list_is_empty(non_empty_list))

    def test_load_eto_and_rain_fall_values(self):
        """ Verify that ETo and Rainfall data can be extracted from the configuration json and stored in the
        configuration object """
        configuration_dir = os.path.dirname(os.path.realpath(__file__)) + os.sep + 'json_configurations'
        self.uut = Configuration(test_name="test_eto_and_rain_fall",
                                 user_conf_file=self.user_conf,
                                 data_json_file='test_eto_and_rain_fall.json',
                                 configuration_dir=configuration_dir)
        expected_daily_eto_values = [0.0, 0.0481485, 0.0382179, 0.00930509, 0.0139129, 0.0789182, 0.0787714, 0.00575393, 0.0182287, 0.0079818]
        expected_daily_rainfall_values = [0, 0.098425, 0, 0, 0, 0, 0, 0.338582, 0.011811, 0]

        self.uut.load_eto_and_rain_fall_values()

        self.assertListEqual(self.uut.daily_eto_value, expected_daily_eto_values)
        self.assertListEqual(self.uut.daily_rainfall_value, expected_daily_rainfall_values)

    def test_verify_full_configuration_1(self):
        mock_3200s = {1: mock.MagicMock(spec=BaseStation3200), 2: mock.MagicMock(spec=BaseStation3200),
                      3: mock.MagicMock(spec=BaseStation3200), 4: mock.MagicMock(spec=BaseStation3200),
                      5: mock.MagicMock(spec=BaseStation3200), 6: mock.MagicMock(spec=BaseStation3200),
                      7: mock.MagicMock(spec=BaseStation3200), 8: mock.MagicMock(spec=BaseStation3200),
                      9: mock.MagicMock(spec=BaseStation3200), 10: mock.MagicMock(spec=BaseStation3200)}
        self.uut.BaseStation3200 = mock_3200s

        self.uut.verify_full_configuration()

        for mock_3200 in mock_3200s.values():
            self.assertEquals(mock_3200.verify_full_configuration.call_count, 1)

    def test_verify_full_configuration_2(self):
        mock_substations = {1: mock.MagicMock(spec=Substation), 2: mock.MagicMock(spec=Substation),
                            3: mock.MagicMock(spec=Substation), 4: mock.MagicMock(spec=Substation),
                            5: mock.MagicMock(spec=Substation), 6: mock.MagicMock(spec=Substation),
                            7: mock.MagicMock(spec=Substation), 8: mock.MagicMock(spec=Substation),
                            9: mock.MagicMock(spec=Substation), 10: mock.MagicMock(spec=Substation)}
        self.uut.SubStations = mock_substations

        self.uut.verify_full_configuration()

        for mock_substation in mock_substations.values():
            self.assertEquals(mock_substation.verify_full_configuration.call_count, 1)

    # TODO: Uncomment once BaseStation1000 objects implement verify_full_configuration
    # def test_verify_full_configuration_3(self):
    #     mock_1000s = {1: mock.MagicMock(spec=BaseStation1000), 2: mock.MagicMock(spec=BaseStation1000),
    #                         3: mock.MagicMock(spec=BaseStation1000), 4: mock.MagicMock(spec=BaseStation1000),
    #                         5: mock.MagicMock(spec=BaseStation1000), 6: mock.MagicMock(spec=BaseStation1000),
    #                         7: mock.MagicMock(spec=BaseStation1000), 8: mock.MagicMock(spec=BaseStation1000),
    #                         9: mock.MagicMock(spec=BaseStation1000), 10: mock.MagicMock(spec=BaseStation1000)}
    #     self.uut.BaseStation1000 = mock_1000s
    #
    #     self.uut.verify_full_configuration()
    #
    #     for mock_1000 in mock_1000s.values():
    #         self.assertEquals(mock_1000.verify_full_configuration.call_count, 1)

    # TODO: Uncomment once FlowStation objects implement verify_full_configuration
    # def test_verify_full_configuration_4(self):
    #     mock_flowstations = {1: mock.MagicMock(spec=FlowStation), 2: mock.MagicMock(spec=FlowStation),
    #                         3: mock.MagicMock(spec=FlowStation), 4: mock.MagicMock(spec=FlowStation),
    #                         5: mock.MagicMock(spec=FlowStation), 6: mock.MagicMock(spec=FlowStation),
    #                         7: mock.MagicMock(spec=FlowStation), 8: mock.MagicMock(spec=FlowStation),
    #                         9: mock.MagicMock(spec=FlowStation), 10: mock.MagicMock(spec=FlowStation)}
    #     self.uut.FlowStations = mock_flowstations
    #
    #     self.uut.verify_full_configuration()
    #
    #     for mock_flowstation in mock_flowstations.values():
    #         self.assertEquals(mock_flowstation.verify_full_configuration.call_count, 1)

    # def test_share_sb_device_with_fail1(self):
    #     """ Verify that trying a share a SubStation device type that is invalid throws the expected exception """
    #     mock_3200s = {1: mock.MagicMock(spec=BaseStation3200)}
    #     self.uut.BaseStation3200 = mock_3200s
    #     invalid_device_type = 'ZZ'
    #     e_msg = ("Attempted to share an invalid device type ('{0}') with a controller. Valid device types are: "
    #              "['ZN', 'FM', 'MV', 'SW', 'TS', 'MS']".format(invalid_device_type))
    #
    #     with self.assertRaises(Exception) as context:
    #         self.uut.share_sb_device_with_cn(_device_type=invalid_device_type, _device_serial='SB12345', _address=1)
    #
    #     self.assertEqual(first=e_msg, second=context.exception.message)
    #
    # def test_share_sb_device_with_fail2(self):
    #     """ Verify that trying to assign a SubStation valve a zone number on a controller that already has a zone
    #     with that number throws the expected exception """
    #     mock_3200 = mock.MagicMock(spec=BaseStation3200)
    #     mock_3200.zones = {1 : mock.MagicMock()}  # 3200 has a single zone at address 1
    #     mock_3200s = {1: mock_3200}     # Only one 3200 in the dict, assigned as controller 1
    #     self.uut.BaseStation3200 = mock_3200s
    #     valve_serial = 'V123456'
    #     assign_to_zone_address = 1
    #
    #     e_msg = ("Attempted to share a Substation valve serial number: {0} at _address: {1} which is already "
    #              "addressed on the Controller. Current valve addresses are: {2}"
    #              .format(valve_serial, assign_to_zone_address, mock_3200.zones.keys()))
    #
    #     with self.assertRaises(Exception) as context:
    #         self.uut.share_sb_device_with_cn(_device_type=opcodes.zone,
    #                                          _device_serial=valve_serial,
    #                                          _address=assign_to_zone_address)
    #
    #     self.assertEqual(first=e_msg, second=context.exception.message)