import json
import platform
import os
from common.imports import opcodes

__author__ = 'Baseline'


class UserConfiguration(object):
    """
    Online Documentation Area: \n
    The main use of this class is to allow the user to pass in different user configurations as far as log in or
    other data. \n
    On class __init__ invoke: \n
    1. Opens the .json file for file read access. \n
    2. Loads the .json file into a recognizable Python 'object' giving us access to the contained key value pairs. \n
    3. Initializes test suite variables to None for information storing. \n
    4. Gets user login and other BaseManager needed items from the file. \n
    5. Turns on/off logging based on value read in from file with key-value 'logging'. \n
    6. Lastly, loads user specific Selenium Web Driver Paths in from file.
    :param user_file: A .json formatted file populated with current user specific test suite configurations. \n
    """
    firefox_supported_platforms = ['Windows', 'Darwin', 'Linux']
    chrome_supported_platforms = ['Windows', 'Darwin']
    ie_supported_platforms = ['Windows']
    browser_dictionary = {'chrome': chrome_supported_platforms, 'firefox': firefox_supported_platforms,
                          'ie': ie_supported_platforms}

    def __init__(self, user_file):
        """

        :param user_file:
        :type user_file:
        """
        # import os
        # print os.getcwd()
        # Open user credential file (.json) for access
        opened_user_file = open(user_file)

        # Converts the .json file into a Python readable 'Object' so we can access it's information.
        loaded_user_json_file = json.load(opened_user_file)

        # Initiate non .json dependent variables to None (These variables are manipulated by the test scripts on a per
        # test basis setting these values as needed.)
        self.name_of_browser_under_test = None
        self.browser_view = None
        self.web_driver = None
        self.mac_address = None
        self.socket_port = None
        self.eventListener = None

        # This variable is set in test_base_browser_common to true, that way all sleeps are ignored for
        # faster testing response
        self.unit_testing = False

        # Get developer specific information per user configuration file
        self.user_name = loaded_user_json_file["client_login_info"]['username']
        self.user_password = loaded_user_json_file["client_login_info"]['password']
        
        # TODO ask Tige about this line of code
        self.url = loaded_user_json_file["server_connections"]['server_url']
        self.fixed_ip = loaded_user_json_file["server_connections"]['fixed_ip_address']

        # Get a server alternate IP address
        try:
            self.use_alternate_ip = loaded_user_json_file["server_connections"]['use_alternate_ip_address']
            self.alternate_ip = loaded_user_json_file["server_connections"]['alternate_ip_address']
        except Exception:
            self.use_alternate_ip = None
            self.alternate_ip = None

        # In Selenium Webdriver standards, a valid url must start with 'http' or 'https', so check if substring [
        # http] exists in the url, if not the url is considered invalid so add 'https://' before the url.
        # i.e.,
        #   webdriver.get("google.com") throws a selenium webdriver exception (invalid url).
        #   webdriver.get("http://google.com") is considered valid and opens correctly.
        if "http" not in self.url:
            new_url = "https://" + str(self.url)
            self.url = new_url

        self.site_name = loaded_user_json_file["client_login_info"]['site_name']
        self.company = loaded_user_json_file["client_login_info"]['company']

        # Attempts to get logging information, if no logging entry in .json config file, system defaults logging to
        # the "on".
        try:
            self.debug = loaded_user_json_file['logging']
        except Exception:
            self.debug = "on"
            
        # Get controller information from the user's .json credential file
        self.type_for_controller = {}
        self.serial_number_for_controller = {}
        self.mac_address_for_controller = {}
        self.port_address_for_controller = {}
        self.socket_port_for_controller = {}
        self.ip_address_for_controller = {}
        self.firmware_version_for_controller = {}

        self.controllers = []

        try:

            for index, controllers_json in enumerate(loaded_user_json_file['controllers']):
                controller_obj_dict = {}
                controller_obj_dict['type'] = controllers_json['type']
                controller_obj_dict['serial_number'] = controllers_json['serial_number']
                controller_obj_dict['mac_address'] = controllers_json['mac_address']
                controller_obj_dict['port_address'] = controllers_json['port_address']
                controller_obj_dict['socket_port'] = controllers_json['ethernet_to_serial_connection']
                try:
                    controller_obj_dict['logging_port'] = controllers_json['logging_ethernet_to_serial_connection']
                except KeyError:
                    pass
                controller_obj_dict['ip_address'] = controllers_json['local_ip_address']
                controller_obj_dict['firmware_version'] = controllers_json['firmware_version']

                self.controllers.append(controller_obj_dict)

                # LEGACY SUPPORT
                self.mac_address_for_3200 = controller_obj_dict['mac_address']
                self.port_address_for_3200 = controller_obj_dict['port_address']
                self.socket_port_for_3200 = controller_obj_dict['socket_port']

        except KeyError:
            e_msg = "\nUser credentials file is missing controllers area. Insert (COPY/PASTE) the following lines in " \
                    "your user configuration file below n\"" \
                    "type: Example: BaseStation1000,BaseStation3200,FlowStation,SubStation," \
                    "mac_address: Example: 0004A3765147",\
                    "port_address: socket:10002",\
                    "socket_port:  Example: //10.11.12.241:10001",\
                    "ip_address:  Example: 10.11.12.56",\
                    "firmware_version:  Example: 1.19",
            raise Exception(e_msg)

        # Get base unit configuration stuff
        try:
            self.bu_comport = loaded_user_json_file['base_unit']['comport']
        except KeyError:
            pass

        # Get browsers for testing, .json returns the "list" of browsers in unicode format, which shouldn't affect
        # the string comparisons.
        self.browsers_for_testing = loaded_user_json_file['browsers']

        # --------
        # Start fail fast for browser support vs current os system.
        supported_browsers = {
            "Windows": ["chrome", "firefox", "ie"],
            "Darwin": ["chrome", "firefox"],
            "Linux": ["chrome", "firefox"]
        }
        list_of_supported_browsers = supported_browsers[platform.system()]

        for browser_to_use in self.browsers_for_testing:
            if browser_to_use not in list_of_supported_browsers:
                e_msg = "Browser to use for testing: {0} is not currently supported for your operating system: {1}. " \
                        "Supported browsers for your operating system are: {2}".format(
                            browser_to_use,
                            platform.system(),
                            list_of_supported_browsers
                        )
                raise ValueError(e_msg)

        # PATH VARIABLES
        self.windows_chrome_webdriver_exe_path = loaded_user_json_file['windows']['chrome']
        self.windows_ie_webdriver_exe_path = loaded_user_json_file['windows']['ie']
        self.mac_chrome_webdriver_exe_path = loaded_user_json_file['mac']['chrome']
        self.mac_ie_webdriver_exe_path = loaded_user_json_file['mac']['ie']
        self.linux_chrome_webdriver_exe_path = loaded_user_json_file['linux']['chrome']
        self.linux_ie_webdriver_exe_path = loaded_user_json_file['linux']['ie']

        try:
            self.bacnet_server_ip = loaded_user_json_file["server_connections"]['bacnet']
        except Exception:
            self.bacnet_server_ip = "10.11.12.27"

        opened_user_file.close()


if __name__ == "__main__":
    tige = UserConfiguration(os.path.join('user_credentials', 'user_credentials_ben.json'))
