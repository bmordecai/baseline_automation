from SocketServer import ThreadingMixIn
from threading import Thread
from wsgiref.simple_server import make_server
from common.objects.base_classes.server_web_socket import ServerWebSocket
from ws4py.server.wsgirefserver import WSGIServer, WebSocketWSGIRequestHandler
from ws4py.server.wsgiutils import WebSocketWSGIApplication
from common.objects.base_classes.websocket_test_client import TestClient

class ServerHandler(object):

    """
    This class handles setting up a basic server for communicating over a web socket to a controller
    """

    def __init__(self, server_ip_address, server_port):
        """
        ServerHandler initializer. Creates a new instance of ServerHandler. Sets server_ip_address and server_port.

        :param _server_ip_address:      IP address of of the server. This can be local host or another ip address
        :param _server_port:            Port on the server through which the web sockets should communicate over
        """
        self.server_ip_address = server_ip_address
        self.server_port = server_port
        self.server = None
        self.start_server()

    def start_server(self):
        """

        Creates a WSGI server and then starts it in a new thread
        1. Create new server
        2. Start server in a thread that will run in the background listening for requests
        :return:
        """

        # create ThreaddedWSGIServer class that will be used by the new server
        class ThreaddedWSGIServer(ThreadingMixIn, WSGIServer):
            pass

        # create the server
        try:
            self.server = make_server(self.server_ip_address, self.server_port, server_class=ThreaddedWSGIServer,
                                      handler_class=WebSocketWSGIRequestHandler,
                                      app=WebSocketWSGIApplication(handler_cls=ServerWebSocket))
        except Exception, e:
            print "Failed to create server. Exception is: " + str(e)
            raise

        # verify port on the server matches requested port.
        port_on_created_server = self.server.server_port
        if port_on_created_server != self.server_port:
            print "Port on created server: " + str(self.server.server_port) + " does not equal server port requested: "\
                  + str(self.server_port)
            raise Exception

        # start the web sockets manager
        self.server.initialize_websockets_manager()

        # create a new thread for the server
        server_thread = Thread(target=self.server.serve_forever)

        # start the server_thread
        server_thread.start()

    def verify_server_running(self):
        """
        Public
        This verifies that a connection can be made with the server by a client, and the two can communicate with
            each other.
        1. Create test client
        2. Connect test client to server
        3. Test client sends a message to the server
        4. Server responds to test client
        5. Shut down test client
        6. Return status of connection

        :return: connection_ok
        :rtype: bool
        """

        # create the test_client.
        test_client = TestClient(self.server_ip_address, self.server_port)

        # start the test_client thread
        connection_ok = test_client.start_client()
        #
        # # send a test message to the server from the client
        # wait for server to receive the test message and respond back to the test_client.
        # This will wait 5 seconds until it gets an 'ok' message from the server.
        # If nothing comes back after 5 seconds, then connection_ok will be returned as false.
        if connection_ok:
            connection_ok = test_client.ping_server()

        # close test client connection
        test_client.close_connection()
        # return status of connection
        return connection_ok

    def shutdown_server(self):
        """
        Public: this will shut down the server and server_thread
        :return:
        """

        # verify that the server exists before shutting it down.
        if self.server is not None:
            self.server.shutdown()
            self.server = None
            print "Successfully shut down server"
        else:
            print "Error shutting down server. Server is NULL"
            raise Exception

    def get_server_web_socket(self):
        """
        Public: This will return the server web socket to be used for sending and receiving messages
        on the server side
        """
        web_socket = None
        if self.server is not None:
            server_web_sockets = self.server.manager.websockets
            if len(server_web_sockets) > 0:
                web_socket = list(server_web_sockets.values())[0]
        if web_socket is not None:
            # ping the client and wait for a response
            ping_result = web_socket.ping_client()
            # assert the the ping_result is true meaning that the ping was successful
            if not ping_result:
                web_socket = None
        return web_socket

