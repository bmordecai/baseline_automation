__author__ = 'baseline'
# -------------------------------------------------------------------------------------------------------------------- #
# Communication specific object buckets
# -------------------------------------------------------------------------------------------------------------------- #
server_connections = {}
""":type: dict[int, common.objects.base_classes.server_connections.ServerConnection]"""
# -------------------------------------------------------------------------------------------------------------------- #
# Controller object buckets
# -------------------------------------------------------------------------------------------------------------------- #
flowstations = {}
""":type: dict[int, common.objects.controllers.bl_fs]"""
basestations_1000 = {}
""":type: dict[int, common.objects.controllers.bl_10]"""
basestations_3200 = {}
""":type: dict[int, common.objects.controllers.bl_32]"""
substations = {}
""":type: dict[int, common.objects.controllers.bl_sb]"""
twsimualator = {}
""":type: dict[int, common.objects.controllers.tw_sim]"""


def reset_all():
    """
    Resets all object bucket dictionaries to clean state.
    """
    server_connections.clear()
    # controller classes
    basestations_1000.clear()
    basestations_3200.clear()
    flowstations.clear()
    substations.clear()
    twsimualator.clear()



