__author__ = 'Tige'

import common.objects.base_classes.web_driver as driver


class MobileAccess(object):
    """
    Mobile Access main page object.
    """
    
    def __init__(self, web_driver):
        """
        Init base page instance
        :param web_driver:  WebDriver instance \n
        :type web_driver:   driver.WebDriver
        """
        self.driver = web_driver

        # main login page
        self.login_page = None
        """:type: common.objects.mobile_access.login_page.LoginPage"""

        # main page of all of basemanager
        self.main_page = None
        """:type: common.objects.mobile_access.main_page.MainPage"""

        # device Tab Area
        self.manual_run_zone_page = None
        """:type: common.objects.mobile_access.manual_run_zone_page.ManualRunZonePage"""
        self.start_stop_page = None
        """:type: common.objects.mobile_access.start_stop_program_page.StartStopProgramPage"""
        self.test_device_page = None
        """:type: common.objects.mobile_access.test_device_page.TestDevicePage"""
        self.flow_status_page = None
        """:type: common.objects.mobile_access.manual_run_zone_page.ManualRunZonePage"""
        self.rain_delay_page = None
        """:type: common.objects.mobile_access.manual_run_zone_page.ManualRunZonePage"""
        self.geo_locate_device_page = None
        """:type: common.objects.mobile_access.manual_run_zone_page.ManualRunZonePage"""
        self.messages_page = None
        """:type: common.objects.mobile_access.manual_run_zone_page.ManualRunZonePage"""

    def back_button(self):
        """
        Hit the back button in the browser. \n
        """
        self.driver.web_driver.back()
