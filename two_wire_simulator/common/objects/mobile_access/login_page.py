__author__ = 'Tige'
from common.imports import *
from common.objects.basemanager.locators import *
from time import sleep

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoSuchAttributeException


class LoginPage(object):
    """
    Mobile login Object \n
    """
    def __init__(self, web_driver):
        self.web_driver = web_driver
        """:type: common.objects.base_classes.web_driver.WebDriver"""

    def enter_login_info(self, _user_name=None, _password=None):
        """
        Types in username and password and clicks the login button.
        :return:
        """
        # user = _user_name if _user_name is not None else self.web_driver.conf.user_name
        if _user_name is not None:
            user = _user_name
        else:
            user = self.web_driver.conf.user_name
        # Clear input and enter in username
        self.web_driver.send_text_to_element(LoginLocators.USER_NAME_INPUT, text_to_send=user)

        # password = _password if _password is not None else self.web_driver.conf.user_password
        if _password is not None:
            password = _password
        else:
            password = self.web_driver.conf.user_password
        # clear input and enter in password
        self.web_driver.send_text_to_element(LoginLocators.PASSWORD_INPUT, text_to_send=password)

    def verify_open(self):
        # Wait for the page to load and the main menu to be visible.
        self.web_driver.wait_for_element_visible(MobileAccessLocators.GO_TO_DESKTOP_VERSION)

    def verify_login_error(self):
        """
        this looks at the text displayed when an incorrect username or password is entered
        :return:
        :rtype:     str\n
        """
        login_error_text = self.web_driver.find_element(LoginLocators.LOGIN_ERROR).text
        expected = 'Your username or password is invalid'
        if login_error_text != expected:
            e_msg = "login error '{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected),           # {0}
                str(login_error_text),   # {1}
            )
            raise ValueError(e_msg)

    def go_from_desktop_login_to_mobile_login(self):
        """
        Clicks the login button and wait's 5 seconds
        :return:
        :rtype:
        """
        # Finds and selects the login button in order to login
        self.web_driver.find_element_then_click(LoginLocators.GO_TO_MOBILE_VERSION)

        # Wait for the page to load and the main menu to be visible.
        self.web_driver.wait_for_element_visible(MobileAccessLocators.LOGIN_FORM)

    def click_login_button(self):
        """
        Clicks the login button and wait until we can see the controller menu populate
        :return:
        :rtype:
        """
        # Finds and selects the login button in order to login
        self.web_driver.find_element_then_click(MobileAccessLocators.LOGIN_SUBMIT)

        # Wait for the page to successfully load displaying "Connected" status in the upper right.
        self.web_driver.wait_for_element_visible(MobileAccessLocators.SELECT_CONTROLLER_MENU)

