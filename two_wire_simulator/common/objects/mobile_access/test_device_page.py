from common.imports import *
from common.objects.basemanager.locators import *

from selenium.common.exceptions import ElementNotVisibleException, NoSuchElementException
import common.imports.opcodes as opcodes
from time import sleep

__author__ = 'Eldin'


class TestDevicePage(object):
    """
    Test Device Page Tab Object \n
    """
    def __init__(self, web_driver):
        self.web_driver = web_driver
        """:type: common.objects.base_classes.web_driver.WebDriver"""

    def toggle_zones(self):
        """
        When in the devices tab, we have a list of all devices the controller has on it, and if you click them they
        expand to show all devices of that type. This method will expand and collapse that list.
        """
        try:
            # Look for the Zone List Header
            self.web_driver.wait_for_element_clickable(MobileAccessLocators.ZONE_LIST_TOGGLE)
            self.web_driver.find_element_then_click(MobileAccessLocators.ZONE_LIST_TOGGLE)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.ZONE_LIST_TOGGLE,  # {0}
                e.message                               # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def toggle_master_valves(self):
        """
        When in the devices tab, we have a list of all devices the controller has on it, and if you click them they
        expand to show all devices of that type. This method will expand and collapse that list.
        """
        try:
            # Look for the Master Valve List Header
            self.web_driver.wait_for_element_clickable(MobileAccessLocators.MASTER_VALVE_LIST_TOGGLE)
            self.web_driver.find_element_then_click(MobileAccessLocators.MASTER_VALVE_LIST_TOGGLE)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.MASTER_VALVE_LIST_TOGGLE,  # {0}
                e.message                                       # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def toggle_moisture_sensors(self):
        """
        When in the devices tab, we have a list of all devices the controller has on it, and if you click them they
        expand to show all devices of that type. This method will expand and collapse that list.
        """
        try:
            # Look for the Moisture Sensor List Header
            self.web_driver.wait_for_element_clickable(MobileAccessLocators.MOISTURE_SENSOR_LIST_TOGGLE)
            self.web_driver.find_element_then_click(MobileAccessLocators.MOISTURE_SENSOR_LIST_TOGGLE)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.MOISTURE_SENSOR_LIST_TOGGLE,   # {0}
                e.message                                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def toggle_temperature_sensors(self):
        """
        When in the devices tab, we have a list of all devices the controller has on it, and if you click them they
        expand to show all devices of that type. This method will expand and collapse that list.
        """
        try:
            # Look for the Temperature Sensor List Header
            self.web_driver.wait_for_element_clickable(MobileAccessLocators.TEMPERATURE_SENSOR_LIST_TOGGLE)
            self.web_driver.find_element_then_click(MobileAccessLocators.TEMPERATURE_SENSOR_LIST_TOGGLE)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.TEMPERATURE_SENSOR_LIST_TOGGLE,    # {0}
                e.message                                               # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def toggle_pressure_sensors(self):
        """
        When in the devices tab, we have a list of all devices the controller has on it, and if you click them they
        expand to show all devices of that type. This method will expand and collapse that list.
        """
        try:
            # Look for the Pressure Sensor List Header
            self.web_driver.wait_for_element_clickable(MobileAccessLocators.PRESSURE_SENSOR_LIST_TOGGLE)
            self.web_driver.find_element_then_click(MobileAccessLocators.PRESSURE_SENSOR_LIST_TOGGLE)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.PRESSURE_SENSOR_LIST_TOGGLE,   # {0}
                e.message                                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def toggle_flow_meters(self):
        """
        When in the devices tab, we have a list of all devices the controller has on it, and if you click them they
        expand to show all devices of that type. This method will expand and collapse that list.
        """
        try:
            # Look for the Flow Meter List Header
            self.web_driver.wait_for_element_clickable(MobileAccessLocators.FLOW_METER_LIST_TOGGLE)
            self.web_driver.find_element_then_click(MobileAccessLocators.FLOW_METER_LIST_TOGGLE)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.FLOW_METER_LIST_TOGGLE,    # {0}
                e.message                                       # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def toggle_event_switches(self):
        """
        When in the devices tab, we have a list of all devices the controller has on it, and if you click them they
        expand to show all devices of that type. This method will expand and collapse that list.
        """
        try:
            # Look for the Event Switch List Header
            self.web_driver.wait_for_element_clickable(MobileAccessLocators.EVENT_SWITCH_LIST_TOGGLE)
            self.web_driver.find_element_then_click(MobileAccessLocators.EVENT_SWITCH_LIST_TOGGLE)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.EVENT_SWITCH_LIST_TOGGLE,  # {0}
                e.message                                       # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def toggle_pumps(self):
        """
        When in the devices tab, we have a list of all devices the controller has on it, and if you click them they
        expand to show all devices of that type. This method will expand and collapse that list.
        """
        try:
            # Look for the Pump List Header
            self.web_driver.wait_for_element_clickable(MobileAccessLocators.PUMP_LIST_TOGGLE)
            self.web_driver.find_element_then_click(MobileAccessLocators.PUMP_LIST_TOGGLE)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.PUMP_LIST_TOGGLE,  # {0}
                e.message                               # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def select_zone(self, _address):
        """
        When in the devices tab, when the zone device list has been expanded (using toggle_zones()), we can now select
        one of the zones in the list. Pass in the address of the zone you wish to select to bring up the self test
        menu.
        """
        try:
            # Look for the Zone List Header
            zone_id = (By.ID, "zone_{0}".format(_address))
            self.web_driver.wait_for_element_clickable(zone_id)
            self.web_driver.find_element_then_click(zone_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,  # {0}
                e.message                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def select_master_valve(self, _address):
        """
        When in the devices tab, when the master valve device list has been expanded (using toggle_master_valves()),
        we can now select one of the master valves in the list. Pass in the address of the master valve you wish to
        select to bring up the self test menu.
        """
        try:
            # Look for the Master Valve List Header
            master_valve_id = (By.ID, "mastervalve_{0}".format(self.web_driver.controller_conf.BaseStation3200[1].
                                                               master_valves[_address].sn))
            self.web_driver.wait_for_element_clickable(master_valve_id)
            self.web_driver.find_element_then_click(master_valve_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,  # {0}
                e.message                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def select_pump(self, _address):
        """
        When in the devices tab, when the master valve device list has been expanded (using toggle_pumps()),
        we can now select one of the master valves in the list. Pass in the address of the master valve you wish to
        select to bring up the self test menu.
        """
        try:
            # Look for the Master Valve List Header
            pump_id = (By.ID, "pump_{0}".format(self.web_driver.controller_conf.BaseStation3200[1].
                                                       pumps[_address].sn))
            self.web_driver.wait_for_element_clickable(pump_id)
            self.web_driver.find_element_then_click(pump_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,  # {0}
                e.message                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def select_moisture_sensor(self, _address):
        """
        When in the devices tab, when the moisture sensor device list has been expanded (using toggle_master_valves()),
        we can now select one of the moisture sensors in the list. Pass in the address of the moisture sensor you wish
        to select to bring up the self test menu.
        """
        try:
            # Look for the Moisture Sensor List Header
            moisture_sensor_id = (By.ID, "moisturesensor_{0}".format(self.web_driver.controller_conf.BaseStation3200[1].
                                                                     moisture_sensors[_address].sn))
            self.web_driver.wait_for_element_clickable(moisture_sensor_id)
            self.web_driver.find_element_then_click(moisture_sensor_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,  # {0}
                e.message                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def select_temperature_sensor(self, _address):
        """
        When in the devices tab, when the temperature sensor device list has been expanded (using
        toggle_master_valves()), we can now select one of the temperature sensors in the list. Pass in the address of
        the temperature sensor you wish to select to bring up the self test menu.
        """
        try:
            # Look for the Temperature Sensor List Header
            temperature_sensor_id = (By.ID, "temperaturesensor_{0}".format(
                self.web_driver.controller_conf.BaseStation3200[1].temperature_sensors[_address].sn))
            self.web_driver.wait_for_element_clickable(temperature_sensor_id)
            self.web_driver.find_element_then_click(temperature_sensor_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,  # {0}
                e.message                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def select_pressure_sensor(self, _address):
        """
        When in the devices tab, when the pressure sensor device list has been expanded (using
        toggle_master_valves()), we can now select one of the pressure sensors in the list. Pass in the address of
        the pressure sensor you wish to select to bring up the self test menu.
        """
        try:
            # Look for the Pressure Sensor List Header
            pressure_sensor_id = (By.ID, "pressuresensor_{0}".format(
                self.web_driver.controller_conf.BaseStation3200[1].pressure_sensors[_address].sn))
            self.web_driver.wait_for_element_clickable(pressure_sensor_id)
            self.web_driver.find_element_then_click(pressure_sensor_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,  # {0}
                e.message                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def select_flow_meter(self, _address):
        """
        When in the devices tab, when the flow meter device list has been expanded (using
        toggle_master_valves()), we can now select one of the flow meters in the list. Pass in the address of
        the flow meter you wish to select to bring up the self test menu.
        """
        try:
            # Look for the Flow Meter List Header
            flow_meter_id = (By.ID, "flowmeter_{0}".format(
                self.web_driver.controller_conf.BaseStation3200[1].flow_meters[_address].sn))
            self.web_driver.wait_for_element_clickable(flow_meter_id)
            self.web_driver.find_element_then_click(flow_meter_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,  # {0}
                e.message                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def select_event_switch(self, _address):
        """
        When in the devices tab, when the flow meter device list has been expanded (using
        toggle_master_valves()), we can now select one of the flow meters in the list. Pass in the address of
        the flow meter you wish to select to bring up the self test menu.
        """
        try:
            # Look for the Flow Meter List Header
            event_switch_id = (By.ID, "switch_{0}".format(
                self.web_driver.controller_conf.BaseStation3200[1].event_switches[_address].sn))
            self.web_driver.wait_for_element_clickable(event_switch_id)
            self.web_driver.find_element_then_click(event_switch_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,  # {0}
                e.message                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def do_self_test(self):
        """
        This will hit the 'Test' button, sending packets down to the controller
        """
        try:
            # Look for the test device button
            self.web_driver.find_element_then_click(MobileAccessLocators.TEST_DEVICE)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,  # {0}
                e.message                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(3)

    def next_device(self):
        """
        This will hit the 'Next Device' button on the self test menu
        """
        try:
            # Look for the next device button and click it
            self.web_driver.find_element_then_click(MobileAccessLocators.NEXT_DEVICE)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,  # {0}
                e.message                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def previous_device(self):
        """
        This will hit the 'Previous Device' button on the self test menu
        """
        try:
            # Look for the previous device button
            self.web_driver.find_element_then_click(MobileAccessLocators.PREVIOUS_DEVICE)

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,  # {0}
                e.message                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def verify_test_result_values_for_zone(self, _address):
        """
        After we do a self test on the a zone, if we are on the same screen, we can compare the values on the
        web page to the values that the 3200 returns to us, that is what we will do here.

        :param _address:    Address of the zone we did a self test on and want to verify. \n
        :type _address:     int \n
        """
        try:
            # Get the HTML table that contains all of the self test results
            table_results = self.web_driver.web_driver.find_element(*MobileAccessLocators.TEST_DEVICE_RESULTS)
            table_rows = table_results.find_elements(*TagNames.TABLE_ROW)

            # Parse the actual values out of the HTML text
            solenoid_current_string = table_rows[0].text
            solenoid_current_decimal = [float(s) for s in solenoid_current_string.split() if s.replace('.', '').isdecimal()][0]
            solenoid_voltage_string = table_rows[1].text
            solenoid_voltage_decimal = [float(s) for s in solenoid_voltage_string.split() if s.replace('.', '').isdecimal()][0]
            two_wire_drop_string = table_rows[2].text
            two_wire_drop_decimal = [float(s) for s in two_wire_drop_string.split() if s.replace('.', '',).isdecimal()][0]

            # Get the same values from the controller and verify they are equal
            self.web_driver.controller_conf.BaseStation3200[1].zones[_address].bicoder.va = solenoid_current_decimal
            self.web_driver.controller_conf.BaseStation3200[1].zones[_address].bicoder.verify_solenoid_current()
            self.web_driver.controller_conf.BaseStation3200[1].zones[_address].bicoder.vv = solenoid_voltage_decimal
            self.web_driver.controller_conf.BaseStation3200[1].zones[_address].bicoder.verify_solenoid_voltage()
            self.web_driver.controller_conf.BaseStation3200[1].zones[_address].bicoder.vt = two_wire_drop_decimal
            self.web_driver.controller_conf.BaseStation3200[1].zones[_address].bicoder.verify_two_wire_drop_value()

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,  # {0}
                e.message                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def verify_test_result_values_for_master_valve(self, _address):
        """
        After we do a self test on the a master valve, if we are on the same screen, we can compare the values on the
        web page to the values that the 3200 returns to us, that is what we will do here.

        :param _address:    Address of the master valve we did a self test on and want to verify. \n
        :type _address:     int \n
        """
        try:
            # Get the HTML table that contains all of the self test results
            table_results = self.web_driver.web_driver.find_element(*MobileAccessLocators.TEST_DEVICE_RESULTS)
            table_rows = table_results.find_elements(*TagNames.TABLE_ROW)

            # Parse the actual values out of the HTML text
            solenoid_current_string = table_rows[0].text
            solenoid_current_decimal = [float(s) for s in solenoid_current_string.split() if s.replace('.', '').isdecimal()][0]
            solenoid_voltage_string = table_rows[1].text
            solenoid_voltage_decimal = [float(s) for s in solenoid_voltage_string.split() if s.replace('.', '').isdecimal()][0]
            two_wire_drop_string = table_rows[2].text
            two_wire_drop_decimal = [float(s) for s in two_wire_drop_string.split() if s.replace('.', '',).isdecimal()][0]

            # Get the same values from the controller and verify they are equal
            self.web_driver.controller_conf.BaseStation3200[1].master_valves[_address].bicoder.va = solenoid_current_decimal
            self.web_driver.controller_conf.BaseStation3200[1].master_valves[_address].bicoder.verify_solenoid_current()
            self.web_driver.controller_conf.BaseStation3200[1].master_valves[_address].bicoder.vv = solenoid_voltage_decimal
            self.web_driver.controller_conf.BaseStation3200[1].master_valves[_address].bicoder.verify_solenoid_voltage()
            self.web_driver.controller_conf.BaseStation3200[1].master_valves[_address].bicoder.vt = two_wire_drop_decimal
            self.web_driver.controller_conf.BaseStation3200[1].master_valves[_address].bicoder.verify_two_wire_drop_value()

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.TEST_DEVICE_RESULTS,   # {0}
                e.message                                   # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def verify_test_result_values_for_pump(self, _address):
        """
        After we do a self test on the a pump, if we are on the same screen, we can compare the values on the
        web page to the values that the 3200 returns to us, that is what we will do here.

        :param _address:    Address of the pump we did a self test on and want to verify. \n
        :type _address:     int \n
        """
        try:
            # Get the HTML table that contains all of the self test results
            table_results = self.web_driver.web_driver.find_element(*MobileAccessLocators.TEST_DEVICE_RESULTS)
            table_rows = table_results.find_elements(*TagNames.TABLE_ROW)

            # Parse the actual values out of the HTML text
            solenoid_current_string = table_rows[0].text
            solenoid_current_decimal = [float(s) for s in solenoid_current_string.split() if s.replace('.', '').isdecimal()][0]
            solenoid_voltage_string = table_rows[1].text
            solenoid_voltage_decimal = [float(s) for s in solenoid_voltage_string.split() if s.replace('.', '').isdecimal()][0]
            two_wire_drop_string = table_rows[2].text
            two_wire_drop_decimal = [float(s) for s in two_wire_drop_string.split() if s.replace('.', '',).isdecimal()][0]

            # Get the same values from the controller and verify they are equal
            self.web_driver.controller_conf.BaseStation3200[1].pumps[_address].bicoder.va = solenoid_current_decimal
            self.web_driver.controller_conf.BaseStation3200[1].pumps[_address].bicoder.verify_solenoid_current()
            self.web_driver.controller_conf.BaseStation3200[1].pumps[_address].bicoder.vv = solenoid_voltage_decimal
            self.web_driver.controller_conf.BaseStation3200[1].pumps[_address].bicoder.verify_solenoid_voltage()
            self.web_driver.controller_conf.BaseStation3200[1].pumps[_address].bicoder.vt = two_wire_drop_decimal
            self.web_driver.controller_conf.BaseStation3200[1].pumps[_address].bicoder.verify_two_wire_drop_value()

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.TEST_DEVICE_RESULTS,   # {0}
                e.message                                   # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def verify_test_result_values_for_moisture_sensor(self, _address):
        """
        After we do a self test on the a moisture sensor, if we are on the same screen, we can compare the values on the
        web page to the values that the 3200 returns to us, that is what we will do here.

        :param _address:    Address of the moisture sensor we did a self test on and want to verify. \n
        :type _address:     int \n
        """
        try:
            # Get the HTML table that contains all of the self test results
            table_results = self.web_driver.web_driver.find_element(*MobileAccessLocators.TEST_DEVICE_RESULTS)
            table_rows = table_results.find_elements(*TagNames.TABLE_ROW)

            # Parse the actual values out of the HTML text
            moisture_string = table_rows[0].text
            moisture_decimal = [float(s) for s in moisture_string.split() if s.replace('.', '').isdecimal()][0]
            temperature_string = table_rows[1].text
            temperature_decimal = [float(s) for s in temperature_string.split() if s.replace('.', '').isdecimal()][0]
            two_wire_drop_string = table_rows[2].text
            two_wire_drop_decimal = [float(s) for s in two_wire_drop_string.split() if s.replace('.', '',).isdecimal()][0]

            # Get the same values from the controller and verify they are equal
            self.web_driver.controller_conf.BaseStation3200[1].moisture_sensors[_address].bicoder.vp = moisture_decimal
            self.web_driver.controller_conf.BaseStation3200[1].moisture_sensors[_address].bicoder.verify_moisture_percent()
            self.web_driver.controller_conf.BaseStation3200[1].moisture_sensors[_address].bicoder.vd = temperature_decimal
            self.web_driver.controller_conf.BaseStation3200[1].moisture_sensors[_address].bicoder.verify_temperature_reading()
            self.web_driver.controller_conf.BaseStation3200[1].moisture_sensors[_address].bicoder.vt = two_wire_drop_decimal
            self.web_driver.controller_conf.BaseStation3200[1].moisture_sensors[_address].bicoder.verify_two_wire_drop_value()

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.TEST_DEVICE_RESULTS,   # {0}
                e.message                                   # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def verify_test_result_values_for_temperature_sensor(self, _address):
        """
        After we do a self test on the a temperature sensor, if we are on the same screen, we can compare the values on the
        web page to the values that the 3200 returns to us, that is what we will do here.

        :param _address:    Address of the temperature sensor we did a self test on and want to verify. \n
        :type _address:     int \n
        """
        try:
            # Get the HTML table that contains all of the self test results
            table_results = self.web_driver.web_driver.find_element(*MobileAccessLocators.TEST_DEVICE_RESULTS)
            table_rows = table_results.find_elements(*TagNames.TABLE_ROW)

            # Parse the actual values out of the HTML text
            temperature_string = table_rows[0].text
            temperature_decimal = [float(s) for s in temperature_string.split() if s.replace('.', '').isdecimal()][0]
            two_wire_drop_string = table_rows[1].text
            two_wire_drop_decimal = [float(s) for s in two_wire_drop_string.split() if s.replace('.', '',).isdecimal()][0]

            # Get the same values from the controller and verify they are equal
            self.web_driver.controller_conf.BaseStation3200[1].temperature_sensors[_address].bicoder.vd = temperature_decimal
            self.web_driver.controller_conf.BaseStation3200[1].temperature_sensors[_address].bicoder.verify_temperature_reading()
            self.web_driver.controller_conf.BaseStation3200[1].temperature_sensors[_address].bicoder.vt = two_wire_drop_decimal
            self.web_driver.controller_conf.BaseStation3200[1].temperature_sensors[_address].bicoder.verify_two_wire_drop_value()

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.TEST_DEVICE_RESULTS,   # {0}
                e.message                                   # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def verify_test_result_values_for_pressure_sensor(self, _address):
        """
        After we do a self test on the a pressure sensor, if we are on the same screen, we can compare the values on the
        web page to the values that the 3200 returns to us, that is what we will do here.

        :param _address:    Address of the pressure sensor we did a self test on and want to verify. \n
        :type _address:     int \n
        """
        try:
            # Get the HTML table that contains all of the self test results
            table_results = self.web_driver.web_driver.find_element(*MobileAccessLocators.TEST_DEVICE_RESULTS)
            table_rows = table_results.find_elements(*TagNames.TABLE_ROW)

            # Parse the actual values out of the HTML text
            pressure_string = table_rows[0].text
            pressure_decimal = [float(s) for s in pressure_string.split() if s.replace('.', '').isdecimal()][0]
            two_wire_drop_string = table_rows[1].text
            two_wire_drop_decimal = [float(s) for s in two_wire_drop_string.split() if s.replace('.', '',).isdecimal()][0]

            # Get the same values from the controller and verify they are equal
            self.web_driver.controller_conf.BaseStation3200[1].pressure_sensors[_address].bicoder.vr = pressure_decimal
            self.web_driver.controller_conf.BaseStation3200[1].pressure_sensors[_address].bicoder.verify_pressure_reading()
            self.web_driver.controller_conf.BaseStation3200[1].pressure_sensors[_address].bicoder.vt = two_wire_drop_decimal
            self.web_driver.controller_conf.BaseStation3200[1].pressure_sensors[_address].bicoder.verify_two_wire_drop_value()

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.TEST_DEVICE_RESULTS,   # {0}
                e.message                                   # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def verify_test_result_values_for_flow_meter(self, _address):
        """
        After we do a self test on the a flow meter, if we are on the same screen, we can compare the values on the
        web page to the values that the 3200 returns to us, that is what we will do here.

        :param _address:    Address of the flow meter we did a self test on and want to verify. \n
        :type _address:     int \n
        """
        try:
            # Get the HTML table that contains all of the self test results
            table_results = self.web_driver.web_driver.find_element(*MobileAccessLocators.TEST_DEVICE_RESULTS)
            table_rows = table_results.find_elements(*TagNames.TABLE_ROW)

            # Parse the actual values out of the HTML text
            flow_rate_string = table_rows[0].text
            flow_rate_decimal = [float(s) for s in flow_rate_string.split() if s.replace('.', '').isdecimal()][0]
            flow_usage_string = table_rows[1].text
            flow_usage_decimal = [float(s.replace(',', '')) for s in flow_usage_string.split() if s.replace(',', '').replace('.', '').isdecimal()][0]
            two_wire_drop_string = table_rows[2].text
            two_wire_drop_decimal = [float(s) for s in two_wire_drop_string.split() if s.replace('.', '',).isdecimal()][0]

            # Get the same values from the controller and verify they are equal
            self.web_driver.controller_conf.BaseStation3200[1].flow_meters[_address].bicoder.vr = flow_rate_decimal
            self.web_driver.controller_conf.BaseStation3200[1].flow_meters[_address].bicoder.verify_flow_rate()
            self.web_driver.controller_conf.BaseStation3200[1].flow_meters[_address].bicoder.vg = flow_usage_decimal
            self.web_driver.controller_conf.BaseStation3200[1].flow_meters[_address].bicoder.verify_water_usage()
            self.web_driver.controller_conf.BaseStation3200[1].flow_meters[_address].bicoder.vt = two_wire_drop_decimal
            self.web_driver.controller_conf.BaseStation3200[1].flow_meters[_address].bicoder.verify_two_wire_drop_value()

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.TEST_DEVICE_RESULTS,   # {0}
                e.message                                   # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def verify_test_result_values_for_event_switch(self, _address):
        """
        After we do a self test on the a event switch, if we are on the same screen, we can compare the values on the
        web page to the values that the 3200 returns to us, that is what we will do here.

        :param _address:    Address of the event switch we did a self test on and want to verify. \n
        :type _address:     int \n
        """
        try:
            # Get the HTML table that contains all of the self test results
            table_results = self.web_driver.web_driver.find_element(*MobileAccessLocators.TEST_DEVICE_RESULTS)
            table_rows = table_results.find_elements(*TagNames.TABLE_ROW)

            # Parse the actual values out of the HTML text
            contact_state_string = table_rows[0].text
            contact_state_string = [s for s in contact_state_string.split() if s in ['Closed', 'Open']][0]
            two_wire_drop_string = table_rows[1].text
            two_wire_drop_decimal = [float(s) for s in two_wire_drop_string.split() if s.replace('.', '',).isdecimal()][0]

            # Do an extra step of converting 'Closed' to 'CL' and 'Open' to 'OP'
            if contact_state_string == 'Closed':
                contact_state_string = opcodes.contacts_closed
            elif contact_state_string == 'Open':
                contact_state_string = opcodes.contacts_open

            # Get the same values from the controller and verify they are equal
            self.web_driver.controller_conf.BaseStation3200[1].event_switches[_address].bicoder.vc = contact_state_string
            self.web_driver.controller_conf.BaseStation3200[1].event_switches[_address].bicoder.verify_contact_state()
            self.web_driver.controller_conf.BaseStation3200[1].event_switches[_address].bicoder.vt = two_wire_drop_decimal
            self.web_driver.controller_conf.BaseStation3200[1].event_switches[_address].bicoder.verify_two_wire_drop_value()

        except NoSuchElementException as e:
            e_msg = "Unable to locate test device menu element to click using id: {0}. {1}".format(
                MobileAccessLocators.TEST_DEVICE_RESULTS,   # {0}
                e.message                                   # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def verify_open(self):
        """
        Verifies that the Test Device page opens successfully by verifying text at the top of the page. \n
        """
        try:
            # Gets the text in the menu title
            menu_title_text = self.web_driver.get_text_from_web_element(MobileAccessLocators.MENU_TITLE)
            if menu_title_text != opcodes.ma_test_device:
                raise Exception('ERROR: The current menu is not "{0}"'.format(opcodes.ma_test_device))
        except Exception as ve:
            caught_text = ve.message
            e_msg = "Unable to verify Test Device page is open. Exception received: {0}".format(
                caught_text
            )
            raise Exception(e_msg)
        else:
            print "Successfully verified Test Device page opened"
