__author__ = 'Tige'

from common.imports import *
from common.objects.basemanager.locators import *
from common.variables import browser
from time import sleep

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoSuchAttributeException
from selenium.common.exceptions import TimeoutException


class MainPage(object):
    """
    Mobile Main Page Object \n
    """
    def __init__(self, web_driver):
        self.web_driver = web_driver
        """:type: common.objects.base_classes.web_driver.WebDriver"""

    def select_a_controller_operation(self, _menu_name_locators):
        """
        Pass in the locator you want to use to pick which page you want to navigate to from the 'Controller
        Operations' page in Mobile Access.

        :param _menu_name_locators:     The locator for the page you want to select.
                                        Ex: MobileAccessLocators.MANUAL_RUN_ZONE_PAGE
        """
        if _menu_name_locators not in [MobileAccessLocators.MANUAL_RUN_ZONE_PAGE,
                                       MobileAccessLocators.START_STOP_PROGRAM_PAGE,
                                       MobileAccessLocators.TEST_DEVICE_PAGE,
                                       MobileAccessLocators.FLOW_STATUS_PAGE,
                                       MobileAccessLocators.RAIN_DELAY_PAGE,
                                       MobileAccessLocators.GEO_LOCATE_DEVICE_PAGE,
                                       MobileAccessLocators.MESSAGES_PAGE]:
            raise ValueError("Menu Name is incorrect:"+' ' + str(_menu_name_locators))

        # Try and locate the indicated menu item. If not found, handle exception raised by web driver
        try:
            self.web_driver.find_element_then_click(_menu_name_locators)

            # Give animation time
            sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except NoSuchElementException:
            raise ValueError("Unable to locate menu item: " + str(_menu_name_locators))
        else:
            print "-------------------------------------------------------------------"
            print "| - Successfully Selected Main Menu > " + str(_menu_name_locators)
            print "-------------------------------------------------------------------"

    def select_operation_manual_run_zone(self):
        """
        Select the 'Manual Run Zone' from the main 'Controller Operations' menu in Mobile Access
        """
        try:
            self.web_driver.find_element_then_click(MobileAccessLocators.MANUAL_RUN_ZONE_PAGE)

            # Give animation time
            sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except NoSuchElementException:
            raise ValueError("Unable to locate menu item: " + str(MobileAccessLocators.MANUAL_RUN_ZONE_PAGE))
        else:
            print "-------------------------------------------------------------------"
            print "| - Successfully Selected Main Menu > " + str(MobileAccessLocators.MANUAL_RUN_ZONE_PAGE)
            print "-------------------------------------------------------------------"

    def select_operation_start_stop_program(self):
        """
        Select the 'Start Stop Program' from the main 'Controller Operations' menu in Mobile Access
        """
        try:
            self.web_driver.find_element_then_click(MobileAccessLocators.START_STOP_PROGRAM_PAGE)

            # Give animation time
            sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except NoSuchElementException:
            raise ValueError("Unable to locate menu item: " + str(MobileAccessLocators.START_STOP_PROGRAM_PAGE))
        else:
            print "-------------------------------------------------------------------"
            print "| - Successfully Selected Main Menu > " + str(MobileAccessLocators.START_STOP_PROGRAM_PAGE)
            print "-------------------------------------------------------------------"

    def select_operation_test_device(self):
        """
        Select the 'Test Device' from the main 'Controller Operations' menu in Mobile Access
        """
        try:
            self.web_driver.find_element_then_click(MobileAccessLocators.TEST_DEVICE_PAGE)

            # Give animation time
            sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except NoSuchElementException:
            raise ValueError("Unable to locate menu item: " + str(MobileAccessLocators.TEST_DEVICE_PAGE))
        else:
            print "-------------------------------------------------------------------"
            print "| - Successfully Selected Main Menu > " + str(MobileAccessLocators.TEST_DEVICE_PAGE)
            print "-------------------------------------------------------------------"

    def select_operation_flow_status(self):
        """
        Select the 'Flow Status' from the main 'Controller Operations' menu in Mobile Access
        """
        try:
            self.web_driver.find_element_then_click(MobileAccessLocators.FLOW_STATUS_PAGE)

            # Give animation time
            sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except NoSuchElementException:
            raise ValueError("Unable to locate menu item: " + str(MobileAccessLocators.FLOW_STATUS_PAGE))
        else:
            print "-------------------------------------------------------------------"
            print "| - Successfully Selected Main Menu > " + str(MobileAccessLocators.FLOW_STATUS_PAGE)
            print "-------------------------------------------------------------------"

    def select_operation_rain_delay(self):
        """
        Select the 'Rain Delay' from the main 'Controller Operations' menu in Mobile Access
        """
        try:
            self.web_driver.find_element_then_click(MobileAccessLocators.RAIN_DELAY_PAGE)

            # Give animation time
            sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except NoSuchElementException:
            raise ValueError("Unable to locate menu item: " + str(MobileAccessLocators.RAIN_DELAY_PAGE))
        else:
            print "-------------------------------------------------------------------"
            print "| - Successfully Selected Main Menu > " + str(MobileAccessLocators.RAIN_DELAY_PAGE)
            print "-------------------------------------------------------------------"

    def select_operation_geo_locate_device(self):
        """
        Select the 'Geo-Locate Device' from the main 'Controller Operations' menu in Mobile Access
        """
        try:
            self.web_driver.find_element_then_click(MobileAccessLocators.GEO_LOCATE_DEVICE_PAGE)

            # Give animation time
            sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except NoSuchElementException:
            raise ValueError("Unable to locate menu item: " + str(MobileAccessLocators.GEO_LOCATE_DEVICE_PAGE))
        else:
            print "-------------------------------------------------------------------"
            print "| - Successfully Selected Main Menu > " + str(MobileAccessLocators.GEO_LOCATE_DEVICE_PAGE)
            print "-------------------------------------------------------------------"

    def select_operation_messages(self):
        """
        Select the 'Messages' from the main 'Controller Operations' menu in Mobile Access
        """
        try:
            self.web_driver.find_element_then_click(MobileAccessLocators.MESSAGES_PAGE)

            # Give animation time
            sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except NoSuchElementException:
            raise ValueError("Unable to locate menu item: " + str(MobileAccessLocators.MESSAGES_PAGE))
        else:
            print "-------------------------------------------------------------------"
            print "| - Successfully Selected Main Menu > " + str(MobileAccessLocators.MESSAGES_PAGE)
            print "-------------------------------------------------------------------"

    def select_operation_stop_all_manual_operations(self):
        """
        Select the 'Stop All Manual Operations' from the main 'Controller Operations' menu in Mobile Access
        """
        try:
            self.web_driver.find_element_then_click(MobileAccessLocators.STOP_ALL_MENU_OPERATIONS_PAGE)

            # Give animation time
            sleep(0.5)
        # Web driver unable to locate menu item indicated, handle exception raised.
        except NoSuchElementException:
            raise ValueError("Unable to locate menu item: " + str(MobileAccessLocators.STOP_ALL_MENU_OPERATIONS_PAGE))
        else:
            print "-------------------------------------------------------------------"
            print "| - Successfully Selected Main Menu > " + str(MobileAccessLocators.STOP_ALL_MENU_OPERATIONS_PAGE)
            print "-------------------------------------------------------------------"

    def select_a_sub_menu(self, sub_menu_locators):
        """
        Selects a sub menu from a known list of available sub menu's. If a sub menu name isn't found,
        a ValueError is raised, signifying a misspelled sub menu name or an invalid sub menu selection.
        When the user specifies the 'markers' sub menu option, they must also specify which 'marker'
        sub menu they would like to access, i.e., 'marker_menu_options' shown below.
        :param sub_menu_locators:            Sub menu options available.
        :return:
        """
        # Iterate through supported sub menu options, if found a match, get the respective web_id associated
        if sub_menu_locators not in [MainPageLocators.COMPANY,
                                     MainPageLocators.CURRENT_SITE,
                                     MainPageLocators.CURRENT_CONTROLLER,
                                     MainPageLocators.ZONES,
                                     MainPageLocators.MOISTURE_SENSORS,
                                     MainPageLocators.FLOW,
                                     MainPageLocators.MASTER_VALVES,
                                     MainPageLocators.TEMPERATURE,
                                     MainPageLocators.EVENT_SWITCHES,
                                     MainPageLocators.POINT_OF_CONNECTION,
                                     MainPageLocators.MAINLINES]:
            raise ValueError("Bad Sub Menu Name" + ' ' + sub_menu_locators)

        try:
            # Attempt to locate indicated sub menu, if found and 'click-able', click it.
            self.web_driver.find_element_then_click(sub_menu_locators)
            
            # give animation some time to finish
            sleep(0.5)
            
        # Handle exception when we are unable to locate sub menu item before clicking
        except StaleElementReferenceException:
            raise StaleElementReferenceException("Unable to locate sub menu item: " + str(sub_menu_locators))

    def select_main_menu(self):
        """
        Selects the main upper left menu.
        """
        try:
            # Look for the upper left main menu and click it
            self.web_driver.wait_for_element_clickable(MainPageLocators.MAIN_MENU_BUTTON)
            self.web_driver.find_element_then_click(MainPageLocators.MAIN_MENU_BUTTON)

        except NoSuchElementException as e:
            e_msg = "Unable to locate main menu element to click using id: {0}. {1}".format(
                MainPageLocators.MAIN_MENU_BUTTON,     # {0}
                e.message                       # {1}
            )
            raise NoSuchAttributeException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def wait_for_selected_controller_to_reconnect(self):
        """
        Attempts to wait for the controller to go offline and to come back online.
        :return:
        :rtype:
        """
        status = self.web_driver.get_status(MainPageLocators.FOOTER_CONTROLLER_STATUS)

        while status != "No Connection":
            sleep(2)
            status = self.web_driver.get_status(MainPageLocators.FOOTER_CONTROLLER_STATUS)

        while status != "Done":
            sleep(2)
            status = self.web_driver.get_status(MainPageLocators.FOOTER_CONTROLLER_STATUS)

        print "Selected controller has reconnected to BaseManager."

    def select_site(self, site_name=None):
        """
        Helper method for selecting a site from upper left menu. Navigation is as follows:

        :param site_name: Site to be selected. If none, we will use whatever is specified in the user config
        :return:
        """
        # This XPATH looks for an HTML element that has text that is the same as your site name, so that we can click it
        if site_name is None:
            site_name = self.web_driver.conf.site_name

        locator = (By.XPATH, "//*[contains(text(), \"{0}\")]".format(site_name))

        try:
            # Look for 'Sites and Controllers' sub-menu option and click it to display sites
            self.web_driver.wait_for_element_clickable(MobileAccessLocators.SELECT_CONTROLLER_MENU)
            self.web_driver.find_element_then_click(locator)

            print "Successfully Selected Site: {0}".format(str(site_name))

        except NoSuchElementException as e:
            e_msg = "Unable to locate site name element and click WebElement ID: {0}. {1}".format(
                locator,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def select_a_controller(self, mac_address=None):
        """
        Pass in the controller mac address as a string the method waits for the controller status to change colors
        before selecting the ID. We will use the first 3200 in the config if none is specified.

        :param mac_address:     Mac Address of controller to select. \n
        :type mac_address:      str \n
        """
        if mac_address is None:
            mac_address = self.web_driver.conf.mac_address_for_3200

        locator = (By.ID, mac_address)
        try:
            # Look for the last needed option, the controller being selected with the specified mac add
            self.web_driver.wait_for_element_clickable(locator)

            # Click on the controller
            self.web_driver.find_element_then_click(locator)

            # Look for the last needed option, the controller being selected with the specified mac add
            self.web_driver.wait_for_element_clickable(MobileAccessLocators.CONTROLLER_MENU)

            print "Successfully Selected Controller [mac_address = {0}]".format(mac_address)

            # Wait for animations to finish
            sleep(2)

        except NoSuchElementException:
            raise NoSuchElementException("Unable to locate controller with mac_address: %s" % str(mac_address))
    
    def select_a_company(self, company=None):
        """
        Method assumes Super User is signing in to Base Manager. \n
        :param company:     A company to select. \n
        :type company:      str \n
        """
        locator = (By.ID, '')
        if company is None:
            company = self.web_driver.conf.company

        try:
            # Look for 'Companies' sub-menu option and click it to display sites
            self.web_driver.wait_for_element_clickable(MainMenuLocators.COMPANIES)
            self.web_driver.find_element_then_click(MainMenuLocators.COMPANIES)

            # Look for indicated company, company_name
            company_list_index = 0
            company_selected = None

            # Loop through each company and compare company names, if equal click the company.
            while not company_selected:

                # Construct company 'id' for locating
                current_web_id = 'menu-companies-{0}'.format(company_list_index)

                # Wait for element to be present, and attempt to click
                locator = (By.ID, current_web_id)
                self.web_driver.wait_for_element_clickable(locator)

                # Get the company to compare the name against what we are looking for.
                company_selected = self.web_driver.web_driver.find_element(*locator)

                # correct company?
                if str(company_selected.text).lstrip() == company:
                    # yes
                    company_selected.click()
                    sleep(1)
                else:
                    # Keep looping, company label text didn't match company_name
                    company_list_index += 1
                    # set to None to remain in the while loop.
                    company_selected = None

            print "Successfully Selected Company: {0}".format(str(company))

        except NoSuchElementException as e:
            e_msg = "Unable to locate Company name element and click WebElement ID: {0}. {1}".format(
                locator,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def click_logout_button(self):
        pass

    def wait_for_main_menu_close(self):
        """
        Waits for the main menu to close (sliding out of view).
        """

        is_open = True
        count = 1
        while is_open:

            # returns true if the menu element has the "slidein" css class, this means that the menu is still showing.
            is_open = "slidein" in self.web_driver.web_driver.find_element(*MainPageLocators.MAIN_MENU)\
                .get_attribute("class")

            # 30 seconds elapsed?
            if count == 30 and is_open:
                e_msg = "Timed out waiting for main menu to close after 30 seconds. Still trying to download company " \
                        "data?"
                raise TimeoutException(e_msg)

            # < 30 seconds elapsed
            else:
                sleep(1)
                count += 1

        # tell us how we did
        print "Successfully waited for main menu to close."

        # Give client a change to catch up, wait for animation to finish
        sleep(2)

    def count_number_of_controllers_online(self):
        """
        First, get the list of controller elements
        Next, for each element check whether the controllers name is in black (online) text color or in gray (offline)
        Return number of controllers online
        :return:
        """
        controllers_online = 0
        controller_web_elements = self.web_driver.web_driver.find_elements_by_class_name('controller')

        for each_controller in controller_web_elements:
            self.web_driver.total_number_of_controllers += 1
            try:
                current_mac = each_controller.get_attribute('mac')
                self.verify_controller_is_online(mac_address=current_mac)
                controllers_online += 1
            except AssertionError:
                pass

        return controllers_online

    def verify_controller_is_online(self, mac_address):
        """
        Waits for the controller to come online. If the controller element label is Gray in color, the controller is
        offline, so keep checking the color of the controller element label until it is not Gray, meaning it's
        online.\n
        In these terms, the label is the visible name on the controller, and we are refering to the color of the
        text. \n
        :param mac_address:
        :return:
        """
        controller_web_id = 'status_CN_' + str(mac_address)
        locator = (By.ID, str(controller_web_id))

        is_offline = True
        count = 1
        while is_offline:

            # returns true if controller has the css class 'status-disconnected' otherwise, returns false
            is_offline = "status disconnected" in self.web_driver.web_driver.find_element(*locator).get_attribute("class")

            if count == 30 and is_offline:
                raise AssertionError("Waited for controller with mac_address: %s and timed out after 30 seconds."
                                     % str(mac_address))
            else:
                sleep(1)
                count += 1

        print "Controller: " + str(mac_address) + " is online."

    def get_color_of_element_in_hex(self, css_property, css_selector=None, web_id=None):
        """
        Finds an element by it's css_selector
        Strips string of non-integer values and assigns to r, g, b respectively
        Formats the values into a Hex value represented as a string
            Example color returned from browser:    "rgba(72, 72, 72, 1)"
            Example output converted by method:     "#484848" (Black)
        :param css_selector:
        :param css_property:
        :return:
        """
        if not self.web_driver.conf.unit_testing:
            pass
            # time.sleep(5)
        color = ''

        if web_id is not None:
            color = str(self.web_driver.find_element_by_id(id_=web_id).value_of_css_property(css_property))

        if css_selector is not None:
            color = str(self.web_driver.find_element_by_css_selector(css_selector).value_of_css_property(
                str(css_property)))

        # Waits up to 60 seconds for a pop-up dialogue box to display.
        count = 1
        if color == 'transparent':
            while color == 'transparent':
                # Added below so it doesn't print so many "Alert is present" messages to console
                if web_id is not None:
                    color = str(self.web_driver.find_element_by_id(id_=web_id).value_of_css_property(
                        css_property))
                else:
                    color = str(self.web_driver.find_element_by_css_selector(css_selector).value_of_css_property(
                        str(css_property)))
                if count % 100 == 0:
                    print "Waiting for status color to not be transparent"
                    count = 1
                else:
                    count += 1

        print (color)
        strip_leading_characters = color.split('(')[1:]
        strip_ending_characters = strip_leading_characters[0].split(')')[:1]
        color_numbers = strip_ending_characters[0].split(',')
        r = int(color_numbers[0])
        g = int(color_numbers[1])
        b = int(color_numbers[2])
        color_in_hex = '#%02x%02x%02x' % (r, g, b)

        try:
            color_temp_conversion = browser.dictionary_reverse_status_color_dictionary[color_in_hex]
            color_name_converted = browser.dictionary_for_status_colors[color_temp_conversion]
        except KeyError:
            raise KeyError("Found color with hex value: " + str(color_in_hex) + ", not recognized by test.")
        else:
            print "Color in hex = [%s], %s" % (color_in_hex, color_name_converted)
            return color_in_hex
