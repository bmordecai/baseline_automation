__author__ = 'Tige'
import main_page
from common.objects.basemanager.locators import *
from selenium.common.exceptions import NoSuchElementException
import time


# from common.objects.basemanager.main_page import BasePage
# import page_factory


class LoginPage(main_page.MainPage):
    """
    login Object \n
    """
    def __init__(self, web_driver):
        super(LoginPage, self).__init__(web_driver=web_driver)

    def enter_login_info(self, _user_name=None, _password=None):
        """
        Types in username and password and clicks the login button.
        :return:
        """
        # user = _user_name if _user_name is not None else self.driver.conf.user_name
        if _user_name is not None:
            user = _user_name
        else:
            user = self.driver.conf.user_name
        # Clear input and enter in username
        self.driver.send_text_to_element(LoginLocators.USER_NAME_INPUT, text_to_send=user)

        # password = _password if _password is not None else self.driver.conf.user_password
        if _password is not None:
            password = _password
        else:
            password = self.driver.conf.user_password

        # clear input and enter in password
        self.driver.send_text_to_element(LoginLocators.PASSWORD_INPUT, text_to_send=password)

    def verify_login_error(self):
        """
        this looks at the text displayed when an incorrect username or password is entered
        :return:
        :rtype:     str\n
        """
        login_error_text = self.driver.web_driver.find_element(*LoginLocators.LOGIN_ERROR).text
        expected = 'Your username or password is invalid.'
        if login_error_text != expected:
            e_msg = "login error '{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected),           # {0}
                str(login_error_text),   # {1}
            )
            raise ValueError(e_msg)

    def click_login_button(self):
        """
        Clicks the login button and wait's 5 seconds
        :return:
        :rtype: main_page.MainPage
        """
        # Finds and selects the login button in order to login
        self.driver.find_element_then_click(LoginLocators.SIGN_IN_BUTTON)
        # Wait for page to fully load
        time.sleep(1)
        return main_page.MainPage(web_driver=self.driver)

    def click_use_previous_version_button(self):
        """
        Clicks the use previous version button and wait's 5 seconds
        :return:
        :rtype:
        """
        # Finds and selects the use previous version button and select it
        self.driver.find_element_then_click(LoginLocators.USE_PREVIOUS_VERSION)
        # Wait for page to fully load
        time.sleep(1)

    def click_go_to_mobile_version(self):
        """
        Clicks the go to mobile version button and wait's 5 seconds
        :return:
        :rtype:
        """
        # Finds and selects the go to mobile version button and select it
        self.driver.find_element_then_click(LoginLocators.GO_TO_MOBILE_VERSION)
        # Wait for page to fully load
        time.sleep(1)

    def select_retrieve_forgotten_password_link(self):
        """
        Selects 'Retrieve forgotten password' on the login page and verifies correct alert text
        is present in the popup dialogue box.
        :return:
        """
        # Locate the forgotten password retrieval link
        self.driver.find_element_then_click(LoginLocators.RETRIEVE_FROGOTTEN_PASSWORD)
        # Wait for page to fully load
        time.sleep(1)
        try:
            # Attempt to switch to the alert if present, if not present, web driver raises a NoSuchFrameException
            popup_alert_box = self.driver.web_driver.switch_to_alert()
            expected_message = "Please contact your company's Baseline administrator to reset your password"
            # This if statement is only used when calling self.login_into_browser, just to confirm that the
            # link is active and displaying properly.
            if str(popup_alert_box.text) == expected_message:
                popup_alert_box.accept()
        # Alert not found, NoSuchFrameException was raised by the web driver and caught here.
        except NoSuchElementException:
            pass

    def select_set_up_a_new_account_link(self):
        """
        Selects 'Set up a new account' link on the login page and verifies base manager account
        setup page opens with the account setup form being displayed.
        On a successful link click, a web element with id:'on' can be found
        :return:
        """
        try:
            #this is keeping track of the current window
            parent_window_handles = self.driver.web_driver.current_window_handle
            # Locate the set up a new account link
            self.driver.find_element_then_click(LoginLocators.SET_UP_A_NEW_ACCOUNT)
            # Move the web driver current window handler to the new tab opened by the link
            self.driver.web_driver.switch_to_window(self.driver.web_driver.window_handles[-1])
            # Compare active tabs to make sure correct form is open
            selected_sub_menu_tab = self.driver.web_driver.find_element_by_link_text("BaseManager Account Setup")
            expected_sub_menu_tab = "BaseManager Account Setup"
            # Correct tabs have been "clicked", the new account form should be visible
            if str(selected_sub_menu_tab.text) == expected_sub_menu_tab:
                # Close the newly opened window
                self.driver.web_driver.close()
                # Switch web driver current window handle back to BaseManager login page
                self.driver.web_driver.switch_to_window(parent_window_handles)
            # Unable to find the BaseManager Account Setup tab, must not be visible. Re-raise with message.
        except NoSuchElementException:
            pass
    #TODO need to look at if we need to pass  in user configuration

    def verify_login_page(self):
        """
        Opens the web browser, maximizes the window, goes to the provided url for bmw3
        and searches for the title, Sign in to BaseManager's web element id. If found,
        continue, otherwise, unsuccessful browser initialization.
        :return:
        """
        pass
        # TODO need to finish
        # if self.conf.browser_view.lower() == 'mobile':
        #     self.driver.find_element_by_link_text('Go to mobile version').click()
        # # Try to search for a known web element id:'login', which is the Sign In button
        # # located on the login page
        # try:
        #     # If accessing mobile site, check for link to desktop version of BMW 3 at the bottom.
        #     if self.conf.browser_view.lower() == 'mobile':
        #         self.driver.find_element_by_id('desktopPost')
        #     # Else, using desktop/tablet view, look for the login button.
        #     else:
        #         self.driver.find_element_by_id('login')
        # # If an exception has occurred, we expect it to be NoSuchElementException returned
        # # by the selenium web driver. Catch it here, then re-raise with a overwritten message
        # except NoSuchElementException:
        #     pass

    def check_login_response(self):
        """
        This is an automated browser login test that will check for browser responsiveness and verify
        the login modules are working correctly. Method runs as follows. \n
            -   Opens the web browser indicated below \n
            -   A set timer will verify how long it took for the web browser to load, if it takes longer
                than 10 seconds, log time to file with an error test will not stop and it will log how
                long it takes for the Map to fully load. \n
            -   Maximizes the window of the opened browser \n
            -
            - Finds both the username and password text boxes located on the login page and clears
                their contents, ensuring there is no garbage left over, and then enters the username
                and password credentials provided by the user configuration \n
            -   Selects the viewing device being used/tested \n
            -   Selects the login button and looks for the main menu to confirm successful login. \n
            -   !! New !! Prior to logging out, the login test now verifies the default controller selection behavior:\n
                - On initial login, the first controller is supposed to be selected. (first attempts to verify this) \n
                - Last, after initial verification, the test selects a second (different) controller and verifies that
                the active selected icon (the check mark) displays with the respective controller. \n
            -   Logs out \n
            -   Closes web browser. \n
        :return:
        """
        pass
        # TODO need to finish
        # print "-------------------------------"
        # print "| Desktop login test started. |"
        # print "-------------------------------"
        #
        #
        # print "~~ Login to %s ~~" % self.config.user_conf.name_of_browser_under_test.capitalize()
        # self.start_browser()
        # start = datetime.datetime.now()  # a set timer will start time once the firefox web browser opens
        # self.login_into_browser()
        # stop = datetime.datetime.now()
        # output1 = stop - start
        # self.config.resource_handler.release()
        #
        # print "------------------------------"
        # print "| Desktop Login Test Passed. |"
        # print "------------------------------"

