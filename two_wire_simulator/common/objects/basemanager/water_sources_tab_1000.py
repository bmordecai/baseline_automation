__author__ = 'baseline'

import common.objects.basemanager.main_page as main_page
from common.imports import *
from common.objects.basemanager.locators import *
from selenium.webdriver import ActionChains
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoSuchAttributeException
from selenium.common.exceptions import TimeoutException
from common.variables import browser


class WaterSourcesTab1000(main_page.MainPage):
    """
    BL_1000 Water Source Tab Object \n
    """
    def __init__(self, web_driver):
        super(WaterSourcesTab1000, self).__init__(web_driver=web_driver)

    def verify_open(self):
        """
        Verifies that the Water Sources Tab opens successfully by verifying text at the top of the page. \n
        """
        try:
            locator = (By.ID, "water_view")
            water_source_page = self.driver.web_driver.find_element(*locator)
            if not water_source_page.is_displayed():
                raise ElementNotVisibleException("Unable to locate Water Sources page.")
        except Exception as ve:
            caught_text = ve.message
            e_msg = "Unable to verify Water Sources tab is open. Exception received: {0}".format(
                caught_text
            )
            raise Exception(e_msg)
        else:
            print "Successfully verified Water Sources Tab opened"