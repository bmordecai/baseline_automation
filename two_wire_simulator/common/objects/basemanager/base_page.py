__author__ = 'Tige'

import common.objects.base_classes.web_driver as driver


class BaseManager(object):
    def __init__(self, web_driver):
        """
        Init base page instance
        :param web_driver:  WebDriver instance \n
        :type web_driver:   driver.WebDriver
        """
        self.driver = web_driver

        # main login page
        self.login_page = None
        """:type: common.objects.basemanager.login_page.LoginPage"""

        # main page of all of basemanager
        self.main_page = None
        """:type: common.objects.basemanager.main_page.MainPage"""
        self.main_page_main_menu = None
        """:type: common.objects.basemanager.main_page.MainPageMenu"""
        self.controller_dialog_box = None
        """:type: common.objects.basemanager.main_page.ControllerSettingsDialogBox"""

        # main admin tools page
        self.admin_tools_page = None
        """:type: common.objects.basemanager.admin_tools_page.AdminToolsPage"""

        # device Tab Area
        self.devices_tab_page = None
        """:type: common.objects.basemanager.dv_tab.DevicesTabPage"""
        self.zones_tab_page = None
        """:type: common.objects.basemanager.dv_tab.ZonesTabPage"""
        self.moisture_sensor_tab_page = None
        """:type: common.objects.basemanager.dv_tab.MoistureSensorsTabPage"""
        self.flow_sensor_tab_page = None
        """:type: common.objects.basemanager.dv_tab.FlowMetersTabPage"""
        self.master_valve_tab_page = None
        """:type: common.objects.basemanager.dv_tab.MasterValvesTabPa"""
        self.pump_tab_page = None
        """:type: common.objects.basemanager.dv_tab.PumpsTabPage"""
        self.pressure_sensor_tab_page = None
        """:type: common.objects.basemanager.dv_tab.PressureSensorsTabPage"""
        self.temperature_sensor_tab_page = None
        """:type: common.objects.basemanager.dv_tab.TemperatureSensorsTabPage"""
        self.event_switch_tab_page = None
        """:type: common.objects.basemanager.dv_tab.EventSwitchTabPage"""

        # main liveview page
        self.live_view_page = None
        """:type: common.objects.basemanager.live_view_tab.LiveViewTabPage"""

        # main maps tab page
        self.maps_page = None
        """:type: common.objects.basemanager.maps_tab.MapsTabPage"""

        # main programs page
        self.programs_page = None
        """:type: common.objects.basemanager.pg_tab.ProgramsTabPage"""

        # main quickview page
        self.quick_view_page = None
        """:type: common.objects.basemanager.qv_tab.QuickViewTabPage"""

        # main subscriptions page
        self.subscriptions_page = None
        """:type: common.objects.basemanager.subscriptions_page.SubscriptionsPage"""

        # main water source page area for the 3200
        self.water_source_3200_page = None
        """:type: common.objects.basemanager.water_sources_tab_3200.WaterSourcesPage3200"""
        self.point_of_control_3200_page = None
        """:type: common.objects.basemanager.water_sources_tab_3200.PointOfConnectionPage3200"""
        self.mainline_3200_page = None
        """:type: common.objects.basemanager.water_sources_tab_3200.MainlinesPage3200"""

        # main water source page area for the 3200
        self.water_source_1000_page = None
        """:type: common.objects.basemanager.water_source_tab_1000.WaterSourcesPage1000"""
        self.point_of_control_1000_page = None
        """:type: common.objects.basemanager.water_source_tab_1000.PointOfConnectionPage1000"""
        self.mainline_1000_page = None
        """:type: common.objects.basemanager.water_source_tab_1000.MainlinesPage1000"""
