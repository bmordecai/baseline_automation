__author__ = 'Tige'
from common.objects.basemanager.locators import *
# import common.objects.basemanager.main_page as main_page
from selenium.common.exceptions import *
import main_page


class QuickViewTabPage(main_page.MainPage):
    """
    QuickView Tab Object \n
    """
    def __init__(self, web_driver, controller):
        """
        :param web_driver: Web driver instance
        :type web_driver: common.objects.base_classes.web_driver.WebDriver
        :param controller: BaseStation 3200 reference.
        :type controller: common.objects.controllers.bl_32.BaseStation3200
        """
        super(QuickViewTabPage, self).__init__(web_driver=web_driver)

        self.controller = controller
        """:type: common.objects.controllers.bl_32.BaseStation3200"""

    #################################
    def select_program(self, _address):
        """
        Selects a program box in BaseManager quick view tab.
        """
        locator = (By.ID, "idProgramStatus_{0}".format(_address))
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
        else:
            raise WebDriverException("QuickView Tab: Unable to locate Program {0} in the QuickView Program list. "
                                     "Wasn't visible.".format(_address))

    #################################
    def select_water_source(self, _address):
        """
        Selects a water source box in BaseManager quick view tab.
        """
        locator = (By.ID, "idWSStatus_{0}".format(_address))
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
        else:
            raise WebDriverException("QuickView Tab: Unable to locate Water Source {0} in the QuickView WS list. "
                                     "Wasn't visible.".format(_address))

    #################################
    def select_control_point(self, _address):
        """
        Selects a control point box in BaseManager quick view tab.
        """
        locator = (By.ID, "idPCStatus_{0}".format(_address))
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
        else:
            raise WebDriverException("QuickView Tab: Unable to locate Control Point {0} in the QuickView CP list. "
                                     "Wasn't visible.".format(_address))

    #################################
    def select_mainline(self, _address):
        """
        Selects a mainline box in BaseManager quick view tab.
        """
        locator = (By.ID, "idMLStatus_{0}".format(_address))
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
        else:
            raise WebDriverException("QuickView Tab: Unable to locate Mainline {0} in the QuickView ML list. "
                                     "Wasn't visible.".format(_address))

    def verify_open(self):
        """
        Verifies that the QuickView Tab opens successfully by verifying text at the top of the page. \n
        """
        try:
            self.verify_zone_status_header()
        except ValueError as ve:
            caught_text = ve.message
            e_msg = "Unable to verify QuickView tab is open. Exception received: {0}".format(
                caught_text
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified QuickView Tab opened"

    def verify_not_present(self):
        """
        Verifies that the QuickView Tab doesnt display on the UI. \n
        """
        # TODO need to Clean this up this is call the wrong thing need to catch a NoSuchElementfound
        try:
            self.driver.web_driver.find_element(MainPageLocators.QUICK_VIEW_TAB)
        except NoSuchElementException:
            print "Successfully verified quickview tab is not present."
        else:
            raise ValueError("QuickView tab was present when we expected it not to be.")

    def verify_zone_status_header(self):
        """
        this looks at the text displayed above the zone
        :return:
        :rtype:     str\n
        """
        zone_status_header = self.driver.web_driver.find_element(*QuickViewLocators.ZONE_STATUS_HEADER).text
        expected = 'Zone Status'
        if zone_status_header != expected:
            e_msg = "zone_status_header'{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected),              # {0}
                str(zone_status_header),   # {1}
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified Zone Status Header on QuickView Tab"

    #################################
    def verify_tooltip_program_description(self, _program_address):
        """
        Verifies the description when looking at a tooltip for program in QuickView.

        :param _program_address:    Address of the program we want to verify.
        :type _program_address:     int
        """
        locator = QuickViewLocators.TOOLTIP_DESCRIPTION
        if self.driver.is_visible(_locator=locator):
            # Verify the description we got from the client against our object
            actual_description = self.driver.get_text_from_web_element(_locator=locator)
            expected_description = self.controller.programs[_program_address].ds
            if expected_description in actual_description:
                print "Successfully verified Program {0}'s description on BaseManager QuickView to be '{1}'".format(
                    _program_address,
                    actual_description
                )
            else:
                e_msg = "QuickView: Description on BaseManager QuickView did not match what the test engine " \
                        "thinks it should be:\n\tExpected: {0}\n\tActual: {1}".format(
                            expected_description,
                            actual_description
                        )
                raise ValueError(e_msg)
        else:
            e_msg = "QuickView: Unable to find the Description of Program {0} using locator: {1}".format(
                _program_address,
                locator
            )
            raise NoSuchElementException(e_msg)

    #################################
    def verify_tooltip_water_source_description(self, _address):
        """
        Verifies the description when looking at a tooltip for water source in QuickView.

        :param _address:    Address of the water source we want to verify.
        :type _address:     int
        """
        locator = QuickViewLocators.TOOLTIP_DESCRIPTION
        if self.driver.is_visible(_locator=locator):
            # Verify the description we got from the client against our object
            actual_description = self.driver.get_text_from_web_element(_locator=locator)
            expected_description = self.controller.water_sources[_address].ds
            if expected_description in actual_description:
                print "Successfully verified WS {0}'s description on BaseManager QuickView to be '{1}'".format(
                    _address,
                    expected_description
                )
            else:
                e_msg = "QuickView: Description of WS {0} on BaseManager QuickView did not match what the test " \
                        "engine thinks it should be:\n\tCould not find the description {1} in the row: {2}".format(
                            _address,
                            expected_description,
                            actual_description
                        )
                raise ValueError(e_msg)
        else:
            e_msg = "QuickView: Unable to find the Description of WS {0} using locator: {1}".format(
                _address,
                locator
            )
            raise NoSuchElementException(e_msg)

    #################################
    def verify_tooltip_control_point_description(self, _address):
        """
        Verifies the description when looking at a tooltip for control point in QuickView.

        :param _address:    Address of the control point we want to verify.
        :type _address:     int
        """
        locator = QuickViewLocators.TOOLTIP_DESCRIPTION
        if self.driver.is_visible(_locator=locator):
            # Verify the description we got from the client against our object
            actual_description = self.driver.get_text_from_web_element(_locator=locator)
            expected_description = self.controller.points_of_control[_address].ds
            if expected_description in actual_description:
                print "Successfully verified CP {0}'s description on BaseManager QuickView to be '{1}'".format(
                    _address,
                    actual_description
                )
            else:
                e_msg = "QuickView: Description of CP {0} on BaseManager QuickView did not match what the test " \
                        "engine thinks it should be:\n\tCould not find the description {1} in the row: {2}".format(
                            _address,
                            expected_description,
                            actual_description
                        )
                raise ValueError(e_msg)
        else:
            e_msg = "QuickView: Unable to find the Description of CP {0} using locator: {1}".format(
                _address,
                locator
            )
            raise NoSuchElementException(e_msg)

    #################################
    def verify_tooltip_mainline_description(self, _address):
        """
        Verifies the description when looking at a tooltip for mainline in QuickView.

        :param _address:    Address of the mainline we want to verify.
        :type _address:     int
        """
        locator = QuickViewLocators.TOOLTIP_DESCRIPTION
        if self.driver.is_visible(_locator=locator):
            # Verify the description we got from the client against our object
            actual_description = self.driver.get_text_from_web_element(_locator=locator)
            expected_description = self.controller.mainlines[_address].ds
            if expected_description in actual_description:
                print "Successfully verified ML {0}'s description on BaseManager QuickView to be '{1}'".format(
                    _address,
                    actual_description
                )
            else:
                e_msg = "QuickView: Description of ML {0} on BaseManager QuickView did not match what the test " \
                        "engine thinks it should be:\n\tCould not find the description {1} in the row: {2}".format(
                            _address,
                            expected_description,
                            actual_description
                        )
                raise ValueError(e_msg)
        else:
            e_msg = "QuickView: Unable to find the Description of ML {0} using locator: {1}".format(
                _address,
                locator
            )
            raise NoSuchElementException(e_msg)


    def verify_qv_flow_meter_description(self, _flow_meter_address):
        """
        This looks up a description based on the given flow meter sn and description
        :param _flow_meter_address: flow meter address \n
        :type _flow_meter_address:    int \n
        """
        if _flow_meter_address in self.controller.flow_meters.keys():
            flow_meter = self.controller.flow_meters[_flow_meter_address]
        else:
            e_msg = "Flow Meter with address {0} not on controller.".format(
                _flow_meter_address
            )
            raise ElementNotVisibleException(e_msg)

        flow_meter_locator = (By.ID, "qv_flowmeter1_{0}".format(flow_meter.sn))
        if self.driver.is_visible(_locator=flow_meter_locator):
            flow_meter_description = self.driver.get_text_from_web_element(flow_meter_locator)
            expected_description = flow_meter.ds
        else:
            e_msg = "Unable to locate flow meter {0}. Not visible to user from page: Quick View".format(
                _flow_meter_address
            )
            raise ElementNotVisibleException(e_msg)

        if flow_meter_description != expected_description:
            e_msg = "Flow Meter Description'{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected_description),              # {0}
                str(flow_meter_description),            # {1}
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified Flow Meter Description on Quick View Tab"

    def verify_qv_moisture_sensor_description(self, _moisture_sensor_address):
        """
        This looks up a description based on the given Moisture Sensor Serial Number and verifies the description
        :param _moisture_sensor_address: moisture sensor address \n
        :type _moisture_sensor_address: int \n

        """

        if _moisture_sensor_address in self.controller.moisture_sensors.keys():
            moisture_sensor = self.controller.moisture_sensors[_moisture_sensor_address]
        else:
            e_msg = "Moisture Sensor with address {0} not on controller.".format(
                _moisture_sensor_address
            )
            raise ElementNotVisibleException(e_msg)

        moisture_sensor_locator = (By.ID, "qv_moisturesensor1_{0}".format(moisture_sensor.sn))
        if self.driver.is_visible(_locator=moisture_sensor_locator):
            moisture_sensor_description = self.driver.get_text_from_web_element(moisture_sensor_locator)
            expected_description = moisture_sensor.ds
        else:
            e_msg = "Unable to locate flow meter {0}. Not visible to user from page: Quick View".format(
                _moisture_sensor_address
            )
            raise ElementNotVisibleException(e_msg)

        if moisture_sensor_description != expected_description:
            e_msg = "Moisture Sensor Description '{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected_description),                  # {0}
                str(moisture_sensor_description),           # {1}
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified Moisture Sensor Description in Quick View Tab"

    def verify_qv_temperature_sensor_description(self, _temperature_sensor_address):
        """
        This looks up a description based on the given Temperature Sensor address and verifies the description
        :param temperature_sensor_address:   temperature sensor address \n
        :type temperature_sensor_address:    int \n
        """

        if _temperature_sensor_address in self.controller.temperature_sensors.keys():
            temperature_sensor = self.controller.temperature_sensors[_temperature_sensor_address]
        else:
            e_msg = "Temperature Sensor with address {0} not on controller.".format(
                _temperature_sensor_address
            )
            raise ElementNotVisibleException(e_msg)

        temperature_sensor_locator = (By.ID, "qv_temperaturesensor1_{0}".format(temperature_sensor.sn))
        if self.driver.is_visible(_locator=temperature_sensor_locator):
            temperature_sensor_description = self.driver.get_text_from_web_element(temperature_sensor_locator)
            expected_description = temperature_sensor.ds
        else:
            e_msg = "Unable to locate temperature {0}. Not visible to user from page: Quick View".format(
               _temperature_sensor_address
            )
            raise ElementNotVisibleException(e_msg)

        if temperature_sensor_description != expected_description:
            e_msg = "Temperature Description '{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected_description),                     # {0}
                str(temperature_sensor_description),           # {1}
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified Temperature Sensor Description on QuickView Tab"

    def verify_qv_event_switch_description(self, _event_switch_address):
        """
        This looks up a description based on the given Event Swith Address and verifies the description
        :param _event_switch_address:   Event Switch address \n
        :type _event_switch_address:    int \n

        """

        if _event_switch_address in self.controller.event_switches.keys():
            event_switch = self.controller.event_switches[_event_switch_address]
        else:
            e_msg = "Event Switch with address {0} not on controller.".format(
                _event_switch_address
            )
            raise ElementNotVisibleException(e_msg)

        event_switch_locator = (By.ID, "qv_switch1_{0}".format(event_switch.sn))
        if self.driver.is_visible(_locator=event_switch_locator):
            event_switch_description = self.driver.get_text_from_web_element(event_switch_locator)
            expected_description = event_switch.ds
        else:
            e_msg = "Unable to locate Event Switch {0}. Not visible to user from page: Quick View".format(
                _event_switch_address
            )
            raise ElementNotVisibleException(e_msg)

        if event_switch_description != expected_description:
            e_msg = "Event Switch Description'{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected_description),                      # {0}
                str(event_switch_description),           # {1}
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified Event Switch Description on QuickView Tab"

    def verify_qv_pressure_sensor_description(self, _pressure_sensor_address):
        """
        This looks up a description based on the given Pressure Sensor address and verifies the description
        :param _pressure_sensor_address:   Pressure Sensor address \n
        :type _pressure_sensor_address:    int \n
        """

        if _pressure_sensor_address in self.controller.pressure_sensors.keys():
            pressure_sensor = self.controller.pressure_sensors[_pressure_sensor_address]
        else:
            e_msg = "Pressure Sensor with address {0} not on controller.".format(
                _pressure_sensor_address
            )
            raise ElementNotVisibleException(e_msg)

        pressure_sensor_locator = (By.ID, "qv_analog1_{0}".format(pressure_sensor.sn))
        if self.driver.is_visible(_locator=pressure_sensor_locator):
            pressure_sensor_description = self.driver.get_text_from_web_element(pressure_sensor_locator)
            expected_description = pressure_sensor.ds
        else:
            e_msg = "Unable to locate Pressure Sensor {0}. Not visible to user from page: Quick View".format(
                _pressure_sensor_address
            )
            raise ElementNotVisibleException(e_msg)

        if pressure_sensor_description != expected_description:
            e_msg = "Pressure Sensor Description'{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected_description),                          # {0}
                str(pressure_sensor_description),                   # {1}
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified Pressure Sensor Description on QuickView Tab"

    def verify_qv_zone_description(self, _zone_address):
        """
        This looks up a zone description in Quick View based on the given zone address and verifies the description
        :param _zone_address:   address of the zone \n
        :type _zone_address:    int \n
        """

        if _zone_address in self.controller.zones.keys():
            zone = self.controller.zones[_zone_address]
        else:
            e_msg = "Zone with address {0} not on controller.".format(
                _zone_address
            )
            raise ElementNotVisibleException(e_msg)

        zone_locator = (By.ID, "idZoneStatus_{0}".format(zone.ad))
        if self.driver.is_visible(_locator=zone_locator):
            self.driver.find_element_then_click(zone_locator)
            zone_description_id = (By.ID, "qtip-tooltip-title")
            if self.driver.is_visible(_locator=zone_description_id):
                zone_description = self.driver.get_text_from_web_element(zone_description_id)
                expected_description = zone.ds
                # extract the zone description from the zone tooltip header.
                # it is of the form <zone number> - <zone description> <status>
                # the string manipulations below remove the <zone description> from the string in
                # zone_description.
                expected_length = len(expected_description)
                start_point = zone_description.find("-") + 2  # 2 accounts for the space after the '-'
                actual_length = len(zone_description) - start_point
                foo = actual_length - expected_length  # caculate how many places from the right we should remove from the description
                zone_description = zone_description[start_point:-foo]
            else:
                e_msg = "Unable to locate zone {0} description in Zone Status tooltip.".format(
                    _zone_address
                )
                raise ElementNotVisibleException(e_msg)
        else:
            e_msg = "Unable to locate Zone Status {0}. Not visible to user from page: Quick View".format(
                _zone_address
            )
            raise ElementNotVisibleException(e_msg)

        if zone_description != expected_description:
            e_msg = "Zone Description '{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected_description),                      # {0}
                str(zone_description),                          # {1}
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified Zone Description on QuickView Tab"

    def verify_qv_master_valve_description(self, _master_valve_address):
        """
        This looks up a master valve description in Quick View based on the given master valve address and verifies the
        description
        :param _master_valve_address:   address of the master valve \n
        :type _master_valve_address:    int \n
        """

        if _master_valve_address in self.controller.master_valves.keys():
            master_valve = self.controller.master_valves[_master_valve_address]
        else:
            e_msg = "Master Valve with address {0} not on controller.".format(
                _master_valve_address
            )
            raise ElementNotVisibleException(e_msg)

        master_valve_locator = (By.ID, "idMVStatus_{0}_{1}".format(
                                    master_valve.ad,
                                    master_valve.sn))
        if self.driver.is_visible(_locator=master_valve_locator):
            self.driver.find_element_then_click(master_valve_locator)
            master_valve_description_id = (By.ID, "qtip-tooltip-title")
            if self.driver.is_visible(_locator=master_valve_description_id):
                master_valve_description = self.driver.get_text_from_web_element(master_valve_description_id)
                expected_description = master_valve.sn + " - " + master_valve.ds
                # extract the zone description from the zone tooltip header.
                # it is of the form <zone number> - <zone description> <status>
                # the string manipulations below remove the <zone description> from the string in
                # zone_description.

                expected_length = len(expected_description)
                start_point = 0  # start at the beginning of the string
                actual_length = len(master_valve_description) - start_point
                foo = actual_length - expected_length  # caculate how many places from the right we should remove from the description
                master_valve_description = master_valve_description[start_point:-foo]
            else:
                e_msg = "Unable to locate master valve {0} description in Master Valve Status tooltip.".format(
                    _master_valve_address
                )
                raise ElementNotVisibleException(e_msg)
        else:
            e_msg = "Unable to locate Master Valve Status {0}. Not visible to user from page: Quick View".format(
                _master_valve_address
            )
            raise ElementNotVisibleException(e_msg)

        if master_valve_description != expected_description:
            e_msg = "Master Valve Description '{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected_description),                              # {0}
                str(master_valve_description),                          # {1}
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified Master Valve Description on QuickView Tab"

    def verify_qv_pump_description(self, _pump_address):
        """
        This looks up a pump description in Quick View based on the given pump address and verifies the
        description
        :param _pump_address:   address of the pump \n
        :type _pump_address:    int \n
        """

        if _pump_address in self.controller.pumps.keys():
            pump = self.controller.pumps[_pump_address]
        else:
            e_msg = "Pump with address {0} not on controller.".format(
                _pump_address
            )
            raise ElementNotVisibleException(e_msg)

        pump_locator = (By.ID, "idPMStatus_{0}_{1}".format(
            pump.ad,
            pump.sn))
        if self.driver.is_visible(_locator=pump_locator):
            self.driver.find_element_then_click(pump_locator)
            pump_description_id = (By.ID, "qtip-tooltip-title")
            if self.driver.is_visible(_locator=pump_description_id):
                master_valve_description = self.driver.get_text_from_web_element(pump_description_id)
                expected_description = pump.sn + " - " + pump.ds
                # extract the pump description from the pump tooltip header.
                # it is of the form <pump serial number> - <pump description> <status>
                # the string manipulations below remove the <pump serial number> - <pump description> from the string in
                # pump_description.
                expected_length = len(expected_description)
                start_point = 0  # start at the beginning of the string
                actual_length = len(master_valve_description) - start_point
                foo = actual_length - expected_length  # calculate how many places from the right we should remove
                #  from the description
                master_valve_description = master_valve_description[start_point:-foo]
            else:
                e_msg = "Unable to locate Pump {0} description in Pump Status tooltip.".format(
                    _pump_address
                )
                raise ElementNotVisibleException(e_msg)
        else:
            e_msg = "Unable to locate Pump Status {0}. Not visible to user from page: Quick View".format(
                _pump_address
            )
            raise ElementNotVisibleException(e_msg)

        if master_valve_description != expected_description:
            e_msg = "Pump Description '{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected_description),                              # {0}
                str(master_valve_description),                          # {1}
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified Pump Description on QuickView Tab"