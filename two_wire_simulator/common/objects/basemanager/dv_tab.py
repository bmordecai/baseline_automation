__author__ = 'bens'

# Selenium Imports
from selenium.common.exceptions import *
from selenium.webdriver.common.by import By

# import bm_enums
import locators
import main_page
from common.imports import bm_enums
from common.imports import opcodes
from time import sleep

device_type_dict = {
    "ZN": "Zones Tab",
    "MS": "Moisture Sensor Tab",
    "FM": "Flow Meter Tab",
    "TS": "Temperature Sensor Tab",
    "MV": "Master Valve Tab",
    "SW": "Event Switch Tab"
}


class DevicesTabPage(main_page.MainPage):
    """
    Device Tab Object \n
    """
    def __init__(self, web_driver, controller):
        super(DevicesTabPage, self).__init__(web_driver=web_driver)
        self.controller = controller
        """:type: common.objects.controllers.bl_32.BaseStation3200"""

    """
    Base Devices Tab Class

    Theory:
        -   If you need a method in the Zones tab to do something, chances are you will need to use the same method
            throughout all of the devices.
        -   Same can be said for attributes.

    Purpose:
        -   This class provides methods for all device tabs to use to simulate on BaseManager.
        -   Any methods that are needed in the Zones tab or Event Switches tab should be considered in this base class
            first, for inheritance by all devices.
        -   Any attribute needed by the Zones tab or any other device tab should have a blank place holder added to
            this base class as well, again for inheritance by all classes.
    """
    from common.objects.base_classes import devices

    # ==== START OF DEVICE ATTRIBUTES ==== #
    #   - Add attributes as needed across inheriting classes. Must be added here and set to None or ''
    #       i.e.: some_new_attribute = ''

    # Device Page Attributes (ID's, other constants)
    page_title = ''
    device_type = ''

    # Base id string placeholder for all inheriting classes
    summary_view_base_id = ''

    # Holder for each child's objects
    objects = dict()
    """:type: dict[int, common.objects.base_classes.devices.BaseDevices]"""

    # Locator attributes for each child
    refresh_button_locator = ''
    assign_bicoder_locator = ''
    device_view_locator = ''


    # ==== END OF DEVICE ATTRIBUTES ==== #

    def verify_is_open(self):
        """
        Verifies that the devices tab of the specified type is open by verifying the refresh button at the top of the
        screen
        """
        if self.driver.is_visible(_locator=self.refresh_button_locator):
            print "Successfully verified {page_title} is open".format(page_title=self.page_title)
        else:
            e_msg = "Unable to verify {page_title} is open. Couldn't find element with id: {web_element}".format(
                page_title=self.page_title,
                web_element=self.refresh_button_locator
            )
            raise ElementNotVisibleException(e_msg)

    def select_assign_bicoders(self):
        """
        Selects the Assign biCoders option.
        """
        if self.driver.is_visible(_locator=self.assign_bicoder_locator):
            self.driver.find_element_then_click(_locator=self.assign_bicoder_locator)

        else:
            e_msg = "Unable to locate 'Assign biCoders' button for {page}. Wasn't visible.".format(
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)

        self.driver.handle_dialog_box()

    def select_device_view_button(self):
        """
        Selects the device view button
        """
        if self.driver.is_visible(_locator=self.device_view_locator):
            self.driver.find_element_then_click(_locator=self.device_view_locator)

        else:
            e_msg = "Unable to locate {device_view} button for {page}. Wasn't visible.".format(
                device_view=self.device_view_locator,
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)

    def save_device_assignment(self):
        """
        Selects the save button at the bottom of the Assign biCoder
        :return:
        :rtype:
        """
        locator = locators.DeviceTabLocators.SAVE_BUTTON
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
        else:
            e_msg = "Unable to locate 'Assign biCoders' SAVE button. Not visible to user from page: {page}".format(
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)

        self.driver.handle_dialog_box()

        if self.device_type == "ZN":
            locator = (By.ID, self.summary_view_base_id.format(header_index=2, serial=self.objects[1].ad))

        else:
            locator = (By.ID, self.summary_view_base_id.format(header_index=2, serial=self.objects[1].sn))

        self.driver.wait_for_element_visible(_locator=locator)
        self.driver.wait_for_element_text_change(_locator=locator, _text=self.objects[1].ds)

    def save_device_description_edit(self):
        """
        Selects the save button at the bottom of the Device Edit page
        :return:
        :rtype:
        """
        if self.device_type == "ZN":
            locator = locators.DeviceTabLocators.ZONE_EDIT_SAVE_BUTTON
        else:
            locator = locators.DeviceTabLocators.DEVICE_EDIT_SAVE_BUTTON
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
        else:
            e_msg = "Unable to locate 'Device Edit' SAVE button. Not visible to user from page: {page}".format(
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)

        self.driver.handle_dialog_box()
        sleep(opcodes.seconds_for_server_to_process_packets)
        self.controller.do_increment_clock(minutes=2)
        if self.device_type == "ZN":
            locator = (By.ID, self.summary_view_base_id.format(header_index=2, serial=self.objects[1].ad))

        else:
            locator = (By.ID, self.summary_view_base_id.format(header_index=2, serial=self.objects[1].sn))

        self.driver.wait_for_element_visible(_locator=locator)
        self.driver.wait_for_element_text_change(_locator=locator, _text=self.objects[1].ds)

    def cancel_device_assignment(self):
        """
        Selects the cancel button at the bottom of the Assign biCoder
        :return:
        :rtype:
        """
        locator = locators.DeviceTabLocators.CANCEL_BUTTON
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
        else:
            e_msg = "Unable to locate 'Assign biCoders' CANCEL button. Not visible to user from page: {page}".format(
                page=self.page_title
            )
            raise ElementNotVisibleException(e_msg)

    def set_description(self, address):
        """
        Sets the description of the addressed object on BaseManager. This assumes we are in the 'assign bicoder' screen.
        :param address: Address of device to set description for
        :type address: int
        """
        locator = (By.ID, "decoderdesc_" + str(address))
        current_obj = self.objects[address]

        if self.driver.is_visible(_locator=locator):
            self.driver.send_text_to_element(_locator=locator, text_to_send=current_obj.ds, clear_field=True)
        else:
            e_msg = "Unable to set description for {d_type} {addr}: {desc}".format(
                d_type=self.device_type,
                addr=address,
                desc=current_obj.ds
            )
            raise ElementNotVisibleException(e_msg)

    def set_device_description(self, description, address):
        """
        Sets the description of the addressed object on BaseManager. This assumes we are in the device 'Edit' screen.
        :param description: Description for this device
        :type description: str
        :param address: Device Serial Number
        :type description: str
        """
        if self.device_type == "ZN":
            locator = locators.DeviceTabLocators.ZONE_EDIT_DESCRIPTION
        else:
            locator = locators.DeviceTabLocators.DEVICE_EDIT_DESCRIPTION

        self.objects[address].ds = description

        if self.driver.is_visible(_locator=locator):
            self.driver.send_text_to_element(_locator=locator, text_to_send=description, clear_field=True)
        else:
            e_msg = "Unable to set description for {d_type} {addr}: {desc}".format(
                d_type=self.device_type,
                addr=address,
                desc=description
            )
            raise ElementNotVisibleException(e_msg)

    def set_assigned_serial_number(self, address):
        """
        Assigns the serial number to the device with the specified address. This assumes we are in the 'assign
        bicoder' screen.
        :param address: Address of device to set serial number for
        :type address: int
        """
        locator = (By.ID, "decoderselect_" + str(address))
        current_obj = self.objects[address]

        if self.driver.is_visible(_locator=locator):
            self.driver.send_text_to_element(_locator=locator, text_to_send=current_obj.sn, drop_down_element=True)
        else:
            e_msg = "Unable to set serial number for {d_type} {addr}: {serial}".format(
                d_type=self.device_type,
                addr=address,
                serial=current_obj.sn
            )
            raise ElementNotVisibleException(e_msg)

    def verify_address(self, address):
        """
        Verifies the address/sensor sn for the device with the given address. This method assumes we are in the
        regular device "view" / "summary list view"
        :param address: Address of device to set serial number for
        :type address: int
        """
        current_obj = self.objects[address]

        # if we are a zone, use the address as "serial" for formatting the string, other devices use actual serial
        # numbers in the id's
        if self.device_type == "ZN":
            header_id = bm_enums.DeviceSummaryViewHeaders.ZONE
            serial = current_obj.ad
        else:
            header_id = bm_enums.DeviceSummaryViewHeaders.SENSOR
            serial = current_obj.sn

        # insert the header index and device identifier (address for zones, otherwise is serial)
        locator = (By.ID, self.summary_view_base_id.format(
            header_index=header_id,
            serial=serial
        ))

        # Get the text from the web element
        if self.driver.is_visible(_locator=locator):
            address_serial_from_bm = self.driver.get_text_from_web_element(_locator=locator)
        else:
            e_msg = "Unable to get address/serial for {d_type} {addr}: {serial}. Unable to locate element.".format(
                d_type=self.device_type,
                addr=address,
                serial=current_obj.sn
            )
            raise ElementNotVisibleException(e_msg)

        # Equal?
        if str(serial) != str(address_serial_from_bm):
            e_msg = "Unable to verify address/serial on BM for {d_type} {addr}: {serial}. Expected: {exp_ad}, " \
                    "Received: {rec_ad}".format(
                        d_type=self.device_type,
                        addr=address,
                        serial=current_obj.sn,
                        exp_ad=serial,
                        rec_ad=address_serial_from_bm
                    )
            raise ValueError(e_msg)

        # Yes equal
        else:
            print "Successfully verified {d_type} {addr} ({serial}) address/serial of: {ad}".format(
                d_type=self.device_type,
                addr=address,
                serial=current_obj.sn,
                ad=address_serial_from_bm
            )

    def verify_description(self, address):
        """
        Verifies the selected device description based on the given address. This method assumes we are in the
        regular device "view" / "summary list view"
        :param address: Address of device to set serial number for
        :type address: int
        """
        current_obj = self.objects[address]
        # wait for packets to come up from the controller
        # if we are a zone, use the address as "serial" for formatting the string, other devices use actual serial
        # numbers in the id's
        if self.device_type == "ZN":
            serial = current_obj.ad
        else:
            serial = current_obj.sn

        # insert the header index and device identifier (address for zones, otherwise is serial)
        locator = (By.ID, self.summary_view_base_id.format(
            header_index=bm_enums.DeviceSummaryViewHeaders.DESCRIPTION,
            serial=serial
        ))

        # Get the text from the web element
        if self.driver.is_visible(_locator=locator):
            description = self.driver.get_text_from_web_element(_locator=locator)
        else:
            e_msg = "Unable to get description for {d_type} {addr}: {serial}. Unable to locate element.".format(
                d_type=self.device_type,
                addr=address,
                serial=current_obj.sn
            )
            raise ElementNotVisibleException(e_msg)

        # Equal?
        if current_obj.ds != description:
            e_msg = "Unable to verify description on BM for {d_type} {addr}: {serial}. Expected: {exp_desc}, " \
                    "Received: {rec_desc}".format(
                        d_type=self.device_type,
                        addr=address,
                        serial=current_obj.sn,
                        exp_desc=current_obj.ds,
                        rec_desc=description
                    )
            raise ValueError(e_msg)

        # Yes equal
        else:
            print "Successfully verified {d_type} {addr} ({serial}) description of: {desc}".format(
                d_type=self.device_type,
                addr=address,
                serial=current_obj.sn,
                desc=description
            )


class ZonesTabPage(DevicesTabPage):

    def __init__(self, driver, controller):
        """
        :param driver: Web driver instance. \n
        :type driver: common.objects.base_classes.web_driver.WebDriver
        :param controller: Controller instance \n
        :type controller: common.objects.controllers.bl_32.BaseStation3200
        """
        # init super class
        DevicesTabPage.__init__(self, web_driver=driver, controller=controller)

        # get reference to Zone objects
        # from common.objects import object_bucket
        # self.objects = object_bucket.zones
        
        self.objects = controller.zones
        """:type: dict[int, common.objects.devices.zn.Zone]"""
        
        # Page Specific Attributes
        self.page_title = "Zones Tab"
        self.device_type = "ZN"

        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.ZONE_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.ZONE_REFRESH_BUTTON
        self.device_view_locator = locators.DeviceTabLocators.ZONE_VIEW_BUTTON

        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "zonedetail_gridzone1_TSD0001"
        self.summary_view_base_id = "zonedetail_gridzone{header_index}_{serial}"

    def select_basemanager_zone(self, _address):
        """
        When in the devices tab, when in the zone list view, this function will select a zone in the used using its ID.
        """
        try:
            # Look for the specific Pump
            zone_id = (By.ID, "zonedetail_gridzone2_{0}".format(_address))
            self.driver.wait_for_element_clickable(zone_id)
            self.driver.find_element_then_click(zone_id)

        except NoSuchElementException as e:
            e_msg = "Unable to edit Zone using address: {0}. {1}".format(
                _address,                   # {0}
                e.message                   # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)

    def select_basemanager_zone_edit_button(self, _address):
        """
        When in the devices tab, when the Zone list has been expanded by selecting Devices -> Zone),
        we can now select one of the Zones in the list. Pass in the address of the Zone you wish to
        select to bring up in the device edit view.
        """
        try:
            # Look for the specific Pump
            zone_edit_id = locators.DeviceTabLocators.ZONE_EDIT_BUTTON
            self.driver.wait_for_element_clickable(zone_edit_id)
            self.driver.find_element_then_click(zone_edit_id)

        except NoSuchElementException as e:
            e_msg = "Unable to edit Zone using address: {0}. {1}".format(
                _address,                   # {0}
                e.message                   # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)


class MoistureSensorsTabPage(DevicesTabPage):

    def __init__(self, driver, controller):
        """
        :param driver: Web driver instance. \n
        :type driver: common.objects.base_classes.web_driver.WebDriver
        :param controller: Controller instance \n
        :type controller: common.objects.controllers.bl_32.BaseStation3200
        """
        # init super class
        DevicesTabPage.__init__(self, web_driver=driver, controller=controller)

        # get reference to Moisture Sensor objects
        # from common.objects import object_bucket
        # self.objects = object_bucket.moisture_sensors
        self.objects = controller.moisture_sensors
        """:type: dict[int, common.objects.devices.ms.MoistureSensor]"""

        # Page Specific Attributes
        self.page_title = "Moisture Sensors Tab"
        self.device_type = "MS"

        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.MOISTURE_SENSOR_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.MOISTURE_SENSOR_REFRESH_BUTTON
        self.device_view_locator = locators.DeviceTabLocators.MOISTURE_SENSOR_VIEW_BUTTON

        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "moisturedetail_sensor1_TSD0001"
        self.summary_view_base_id = "moisturedetail_sensor{header_index}_{serial}"

    def select_basemanager_moisture_sensor_edit_button(self, _address):
        """
        When in the devices tab, when the moisture device list has been expanded by selecting Devices -> Moisture Sensor),
        we can now select one of the moisture senor in the list. Pass in the address of the moisture you wish to
        select to bring up in the device edit view.
        """
        try:
            # Look for the Flow Meter List Header
            moisture_sensor_id = (By.ID, "MS-{0}-edit".format(self.driver.controller_conf.BaseStation3200[1].
                                                         moisture_sensors[_address].sn))
            self.driver.wait_for_element_clickable(moisture_sensor_id)
            self.driver.find_element_then_click(moisture_sensor_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate flow meter edit button to click using id: {0}. {1}".format(
                _address,                       # {0}
                e.message                       # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)


class FlowMetersTabPage(DevicesTabPage):

    def __init__(self, driver, controller):
        """
        :param driver: Web driver instance. \n
        :type driver: common.objects.base_classes.web_driver.WebDriver
        :param controller: Controller instance \n
        :type controller: common.objects.controllers.bl_32.BaseStation3200
        """
        # init super class
        DevicesTabPage.__init__(self, web_driver=driver, controller=controller)

        # get reference to Flow Meter objects
        # from common.objects import object_bucket
        # self.objects = object_bucket.flow_meters
        
        self.objects = controller.flow_meters
        """:type: dict[int, common.objects.devices.fm.FlowMeter]"""

        # Page Specific Attributes
        self.page_title = "Flow Meters Tab"
        self.device_type = "FM"

        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.FLOW_METER_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.FLOW_METER_REFRESH_BUTTON
        self.device_view_locator = locators.DeviceTabLocators.FLOW_METER_VIEW_BUTTON

        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "flowmeterdetail_sensor1_TSD0001"
        self.summary_view_base_id = "flowmeterdetail_sensor{header_index}_{serial}"

    def select_basemanager_flow_meter_edit_button(self, _address):
        """
        When in the devices tab, when the flow meter device list has been expanded by selecting Devices -> Flow Meter),
        we can now select one of the flow meters in the list. Pass in the address of the flow meter you wish to
        select to bring up in the device edit view.
        """
        try:
            # Look for the Flow Meter List Header
            flow_meter_id = (By.ID, "FM-{0}-edit".format(self.driver.controller_conf.BaseStation3200[1].
                                                           flow_meters[_address].sn))
            self.driver.wait_for_element_clickable(flow_meter_id)
            self.driver.find_element_then_click(flow_meter_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate flow meter edit button to click using id: {0}. {1}".format(
                _address,               # {0}
                e.message               # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)


class MasterValvesTabPage(DevicesTabPage):

    def __init__(self, driver, controller):
        """
        :param driver: Web driver instance. \n
        :type driver: common.objects.base_classes.web_driver.WebDriver
        :param controller: Controller instance \n
        :type controller: common.objects.controllers.bl_32.BaseStation3200
        """
        # init super class
        DevicesTabPage.__init__(self, web_driver=driver, controller=controller)

        # get reference to Master Valve objects
        # from common.objects import object_bucket
        # self.objects = object_bucket.master_valves
        self.objects = controller.master_valves
        """:type: dict[int, common.objects.devices.mv.MasterValve]"""

        # Page Specific Attributes
        self.page_title = "Master Valves Tab"
        self.device_type = "MV"

        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.MASTER_VALVE_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.MASTER_VALVE_REFRESH_BUTTON
        self.device_view_locator = locators.DeviceTabLocators.MASTER_VALVE_VIEW_BUTTON

        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "mastervalvedetail_sensor1_TSD0001"
        self.summary_view_base_id = "mastervalvedetail_sensor{header_index}_{serial}"

    def select_basemanager_master_valve_edit_button(self, _address):
        """
        When in the devices tab, when the master valve device list has been expanded by selecting Devices -> Master Valves),
        we can now select one of the master valves in the list. Pass in the address of the master valve you wish to
        select to bring up in the device edit view.
        """
        try:
            # Look for the Master Valve List Header
            master_valve_id = (By.ID, "MV-{0}-edit".format(self.driver.controller_conf.BaseStation3200[1].
                                                           master_valves[_address].sn))
            self.driver.wait_for_element_clickable(master_valve_id)
            self.driver.find_element_then_click(master_valve_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate master valve edit button to click using id: {0}. {1}".format(
                _address,                           # {0}
                e.message                           # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)


class PumpsTabPage(DevicesTabPage):
    
    def __init__(self, driver, controller):
        """
        :param driver: Web driver instance. \n
        :type driver: common.objects.base_classes.web_driver.WebDriver
        :param controller: Controller instance \n
        :type controller: common.objects.controllers.bl_32.BaseStation3200
        """
        # init super class
        DevicesTabPage.__init__(self, web_driver=driver, controller=controller)
        
        # get reference to Master Valve objects
        # from common.objects import object_bucket
        # self.objects = object_bucket.master_valves
        self.objects = controller.pumps
        """:type: dict[int, common.objects.devices.pm.Pump]"""
        
        # Page Specific Attributes
        self.page_title = "Pumps Tab"
        self.device_type = opcodes.pump
        
        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.PUMP_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.PUMP_REFRESH_BUTTON
        self.device_view_locator = locators.DeviceTabLocators.PUMP_VIEW_BUTTON

        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "mastervalvedetail_sensor1_TSD0001"
        self.summary_view_base_id = "pumpdetail_sensor{header_index}_{serial}"

    def select_basemanager_pump_edit_button(self, _address):
        """
        When in the devices tab, when the Pump list has been expanded by selecting Devices -> Pump),
        we can now select one of the Pump in the list. Pass in the address of the Pump you wish to
        select to bring up in the device edit view.
        """
        try:
            # Look for the specific Pump
            pumps_id = (By.ID, "PM-{0}-edit".format(self.driver.controller_conf.BaseStation3200[1].
                                                                  pumps[_address].sn))
            self.driver.wait_for_element_clickable(pumps_id)
            self.driver.find_element_then_click(pumps_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate Pump edit button to click using address: {0}. {1}".format(
                _address,                   # {0}
                e.message                   # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)


class PressureSensorsTabPage(DevicesTabPage):
    
    def __init__(self, driver, controller):
        """
        :param driver: Web driver instance. \n
        :type driver: common.objects.base_classes.web_driver.WebDriver
        :param controller: Controller instance \n
        :type controller: common.objects.controllers.bl_32.BaseStation3200
        """
        # init super class
        DevicesTabPage.__init__(self, web_driver=driver, controller=controller)
        
        # get reference to Master Valve objects
        # from common.objects import object_bucket
        # self.objects = object_bucket.master_valves
        self.objects = controller.pressure_sensors
        """:type: dict[int, common.objects.devices.ps.PressureSensor]"""
        
        # Page Specific Attributes
        self.page_title = "Pressure Sensors Tab"
        self.device_type = opcodes.pressure_sensor
        
        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.PRESSURE_SENSOR_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.PRESSURE_SENSOR_REFRESH_BUTTON
        self.device_view_locator = locators.DeviceTabLocators.PRESSURE_SENSOR_VIEW_BUTTON
        
        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "mastervalvedetail_sensor1_TSD0001"
        self.summary_view_base_id = "analogbicoderdetail_sensor{header_index}_{serial}"

    def select_basemanager_pressure_sensor_edit_button(self, _address):
        """
        When in the devices tab, when the pressure sensor device list has been expanded by selecting Devices -> Pressure Sensor),
        we can now select one of the Pressure Sensors in the list. Pass in the address of the Pressure Sensor you wish to
        select to bring up in the device edit view.
        """
        try:
            # Look for the specific Pressure Sensor
            pressure_sensor_id = (By.ID, "AN-{0}-edit".format(self.driver.controller_conf.BaseStation3200[1].
                                                                 pressure_sensors[_address].sn))
            self.driver.wait_for_element_clickable(pressure_sensor_id)
            self.driver.find_element_then_click(pressure_sensor_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate Pressure Sensor edit button to click using address: {0}. {1}".format(
                _address,                       # {0}
                e.message                       # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)



class TemperatureSensorsTabPage(DevicesTabPage):

    def __init__(self, driver, controller):
        """
        :param driver: Web driver instance. \n
        :type driver: common.objects.base_classes.web_driver.WebDriver
        :param controller: Controller instance \n
        :type controller: common.objects.controllers.bl_32.BaseStation3200
        """
        # init super class
        DevicesTabPage.__init__(self, web_driver=driver, controller=controller)

        # get reference to Temperature Sensor objects
        # from common.objects import object_bucket
        # self.objects = object_bucket.temperature_sensors
        self.objects = controller.temperature_sensors
        """:type: dict[int, common.objects.devices.ts.TemperatureSensor]"""

        # Page Specific Attributes
        self.page_title = "Temperature Sensors Tab"
        self.device_type = "TS"

        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.TEMPERATURE_SENSOR_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.TEMPERATURE_REFRESH_BUTTON
        self.device_view_locator = locators.DeviceTabLocators.TEMPERATURE_SENSOR_VIEW_BUTTON

        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "tempdetail_sensor1_TSD0001"
        self.summary_view_base_id = "tempdetail_sensor{header_index}_{serial}"

    def select_basemanager_temperature_sensor_edit_button(self, _address):
        """
        When in the devices tab, when the temperature sensor device list has been expanded by selecting Devices -> Temperature Sensor),
        we can now select one of the Temperature Sensors in the list. Pass in the address of the Temperature Sensor you wish to
        select to bring up in the device edit view.
        """
        try:
            # Look for the specific Temperature Sensor
            temperature_sensor_id = (By.ID, "TS-{0}-edit".format(self.driver.controller_conf.BaseStation3200[1].
                                                           temperature_sensors[_address].sn))
            self.driver.wait_for_element_clickable(temperature_sensor_id)
            self.driver.find_element_then_click(temperature_sensor_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate Temperature Sensor edit button to click using address: {0}. {1}".format(
                _address,                       # {0}
                e.message                       # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)


class EventSwitchTabPage(DevicesTabPage):

    def __init__(self, driver, controller):
        """
        :param driver: Web driver instance. \n
        :type driver: common.objects.base_classes.web_driver.WebDriver
        :param controller: Controller instance \n
        :type controller: common.objects.controllers.bl_32.BaseStation3200
        """
        
        # init super class
        DevicesTabPage.__init__(self, web_driver=driver, controller=controller)

        # get reference to Event Switch objects
        # from common.objects import object_bucket
        # self.objects = object_bucket.event_switches
        self.objects = controller.event_switches
        """:type: dict[int, common.objects.devices.sw.EventSwitch]"""

        # Page Specific Attributes
        self.page_title = "Event Switch Tab"
        self.device_type = "SW"

        # Page Specific Locator's
        self.assign_bicoder_locator = locators.DeviceTabLocators.EVENT_SWITCH_ASSIGN
        self.refresh_button_locator = locators.DeviceTabLocators.EVENT_SWITCH_REFRESH_BUTTON
        self.device_view_locator = locators.DeviceTabLocators.EVENT_SWITCH_VIEW_BUTTON

        # Summary View Base ID - Used for verification on device summary page
        # usage: self.summary_view_base_id.format(header_index="1",serial="TSD0001")
        # output: "eventswitchdetail_sensor1_TSD0001"
        self.summary_view_base_id = "eventswitchdetail_sensor{header_index}_{serial}"

    def select_basemanager_event_switch_edit_button(self, _address):
        """
        When in the devices tab, when the Event Switch list has been expanded by selecting Devices -> Event Switch),
        we can now select one of the Event Switches in the list. Pass in the address of the Event Switch you wish to
        select to bring up in the device edit view.
        """
        try:
            # Look for the specific Event Switch
            event_switch_sensor_id = (By.ID, "SW-{0}-edit".format(self.driver.controller_conf.BaseStation3200[1].
                                                                 event_switches[_address].sn))
            self.driver.wait_for_element_clickable(event_switch_sensor_id)
            self.driver.find_element_then_click(event_switch_sensor_id)

        except NoSuchElementException as e:
            e_msg = "Unable to locate Event Switch edit button to click using address: {0}. {1}".format(
                _address,                   # {0}
                e.message                   # {1}
            )
            raise NoSuchElementException(e_msg)

        else:
            # for animation to finish
            sleep(1)