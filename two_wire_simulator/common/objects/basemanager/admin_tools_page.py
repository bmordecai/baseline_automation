__author__ = 'Eldin'

from common.imports import *
from common.objects.basemanager.locators import *
# import common.objects.basemanager.main_page as main_page
import main_page
import time
from common.objects.basemanager.locators import *
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchAttributeException
from selenium import webdriver


class AdminToolsPage(main_page.MainPage):
    """
    Admin Tools Object \n
    """
    def __init__(self, web_driver):
        super(AdminToolsPage, self).__init__(web_driver=web_driver)

    def verify_open(self):
        # TODO move this to the baseclass
        # Wait for the page to successfully load displaying "Subscriptions" in the header
        self.driver.wait_for_element_visible(AdminToolsLocators.ADMIN_TOOLS_HEADER)
        time.sleep(3)

    def select_change_password_menu(self):
        """
        select the change passord menu
        """
        try:
            # Finds the element
            self.driver.find_element_then_click(AdminToolsLocators.CHANGE_PASSWORD_MENU)

            print "Successfully Selected Change Password Window."

        except NoSuchElementException as e:
            e_msg = "Unable to locate the change password menu and click WebElement ID: {0}. {1}".format(
                AdminToolsLocators.CHANGE_PASSWORD_BUTTON,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def click_change_password_button(self):
        """
        Clicks the change password button if the current page is Admin Tools
        """
        try:
            # Finds the element
            self.driver.find_element_then_click(AdminToolsLocators.CHANGE_PASSWORD_BUTTON)

            print "Successfully Selected Change Password Window."

        except NoSuchElementException as e:
            e_msg = "Unable to locate the change password menu and click WebElement ID: {0}. {1}".format(
                AdminToolsLocators.CHANGE_PASSWORD_BUTTON,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def enter_old_password(self, _old_password=''):
        """
        enter old pass word
        """
        try:
            # Finds the element
            self.driver.send_text_to_element(AdminToolsLocators.OLD_PASSWORD, text_to_send=_old_password)

            print "Successfully Entered Old Password."

        except NoSuchElementException as e:
            e_msg = "Unable to locate the Old Password Element WebElement ID: {0}. {1}".format(
                AdminToolsLocators.OLD_PASSWORD,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def enter_new_password(self, _new_password=''):
        """
        enter new pass word
        """
        try:
            # Finds the element
            self.driver.send_text_to_element(AdminToolsLocators.NEW_PASSWORD, text_to_send=_new_password)

            print "Successfully Entered New Password."

        except NoSuchElementException as e:
            e_msg = "Unable to locate the New Password Element WebElement ID: {0}. {1}".format(
                AdminToolsLocators.OLD_PASSWORD,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def enter_confirmed_new_password(self, _confirm_password=''):
        """
        enter new pass word again to confirm
        """
        try:
            # Finds the element
            self.driver.send_text_to_element(AdminToolsLocators.CONFIRM_NEW_PASSWORD, text_to_send=_confirm_password)

            print "Successfully Entered Confirmed New Password."

        except NoSuchElementException as e:
            e_msg = "Unable to locate the Confrim Password Element WebElement ID: {0}. {1}".format(
                AdminToolsLocators.CONFIRM_NEW_PASSWORD,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def close_window(self):
        """
        close browser window
        """
        try:
            self.driver.close_window()
            # Finds the element
            self.driver.web_driver.switch_to.window(self.driver.web_driver.window_handles[0])

            print "Successfully Entered Confirmed New Password."

        except NoSuchElementException as e:
            e_msg = "Unable to locate the Confrim Password Element WebElement ID: {0}. {1}".format(
                AdminToolsLocators.CONFIRM_NEW_PASSWORD,
                e.message
            )
            raise NoSuchElementException(e_msg)

    def switch_to_admin_tools_window(self):
        # This line will make the web browser focused
        self.driver.web_driver.switch_to.window(self.driver.web_driver.window_handles[1])