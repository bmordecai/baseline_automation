__author__ = 'baseline'

import locators
import main_page


class MapsTabPage(main_page.MainPage):
    """
    Maps Tab Object \n
    """
    def __init__(self, web_driver):
        super(MapsTabPage, self).__init__(web_driver=web_driver)

    def verify_open(self):
        """
        Verifies that the Maps tab is open by looking for the edit button
        :return:
        :rtype:
        """
        if self.driver.is_visible(_locator=locators.MapTabLocators.MAP_EDIT_BUTTON):
            print "Successfully verified the Maps Tab is open."
        else:
            e_msg = "Unable to verify Maps Tab is open. Couldn't find the edit button in the bottom right"
            raise Exception(e_msg)
