__author__ = 'baseline'

import common.objects.basemanager.main_page as main_page
from common.imports import *
from common.objects.basemanager.locators import *
from selenium.common.exceptions import *
from selenium.webdriver.common.by import By


class WaterSourcesPage3200(main_page.MainPage):
    """
    BL_3200 Water Source Tab Object \n
    """
    def __init__(self, web_driver, controller):
        super(WaterSourcesPage3200, self).__init__(web_driver=web_driver)

        self.water_sources = controller.water_sources
        """:type: dict[int, common.objects.programming.ws.WaterSource]"""

    #################################
    def select_water_source(self, _address):
        """
        Selects the Water Source row with respect to the WS number passed in.

        :param _address:    Address of the Water Source to Select. \n
        :type _address:     int
        """
        ws_row_locator = (By.ID, "waterSourceRow-{0}".format(_address))

        if self.driver.is_visible(_locator=ws_row_locator):
            self.driver.find_element_then_click(_locator=ws_row_locator)
        else:
            e_msg = "Unable to locate row for Water Source: {0} in Water Source List View. Wasn't visible.".format(
                _address
            )
            raise ElementNotVisibleException(e_msg)
    
    #################################
    def verify_is_open(self, _locator):
        """
        Verifies that the devices tab of the specified type is open by verifying the refresh button at the top of the
        screen
        """
        # try to get the page element
        try:
            water_source_page = self.driver.web_driver.find_element(*_locator)
        
        # exception occurred trying to locate the element
        except (NoSuchElementException, StaleElementReferenceException, WebDriverException) as error:
            caught_err_text = error.msg
            e_msg = "Exception occurred trying to verify Water Sources > List View is open. Exception Received: " \
                    "{exception_text}".format(
                        exception_text=caught_err_text
                    )
            raise WebDriverException(e_msg)
        
        # no exception locating element
        else:
            
            # check to see if it is displayed
            page_is_displayed = water_source_page.is_displayed()
            if page_is_displayed:
                print "Successfully verified Water Sources > {page_title} is open".format(page_title=self.page_title)
            
            # not displayed
            else:
                e_msg = "Unable to verify Water Sources > {page_title} is open. Couldn't find element with id: " \
                        "{web_element}".format(
                            page_title=self.page_title,
                            web_element=_locator
                        )
                raise ElementNotVisibleException(e_msg)
    
    #################################
    def verify_header_text_is_displayed(self, _locator):
        """
        Verifies the header displayed for the respective water source view
        :param _locator: Selector object to use to find the header element
        :type _locator: (selenium.webdriver.common.by.By, str)
        """
        # try to get the page element
        try:
            self.driver.wait_for_element_visible(_locator)
            header_el = self.driver.web_driver.find_element(*_locator)
        # exception occurred trying to locate the element
        except WebDriverException as error:
            caught_err_text = error.msg
            e_msg = "Exception occurred trying to find Water Sources > {page_title}'s header element. Exception " \
                    "Received: {exception_text}".format(
                        page_title=self.page_title,
                        exception_text=caught_err_text
                    )
            raise WebDriverException(e_msg)
        
        header_el_text = header_el.text
        
        if self.page_header_text != header_el_text:
            e_msg = "Unable to verify Water Sources > {0}'s header text. Received: {1}, Expected: {2}".format(
                self.page_title,
                header_el_text,
                self.page_header_text
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified Water Sources > {0}'s header text: {1}".format(
                self.page_title,
                self.page_header_text
            )
    
    #################################
    def get_element(self, _locator):
        """
        Returns the element specified by the _locator tuple.

        :param _locator: Selector object to use to find the header element. \n
        :type _locator: (selenium.webdriver.common.by.By, str)

        :rtype: selenium.webdriver.remote.webelement.WebElement
        """
        # try to get the page element
        try:
            return self.driver.web_driver.find_element(*_locator)
        # exception occurred trying to locate the element
        except WebDriverException as error:
            caught_err_text = error.msg
            e_msg = "Exception occurred trying to find an element located in {page_title} View. Exception " \
                    "Received: {exception_text}".format(
                        page_title=self.page_title,
                        exception_text=caught_err_text
                    )
            raise WebDriverException(e_msg)

    #################################
    def click_edit_button(self):
        """
        Clicks the edit button for the water sources page
        """
        try:
            # Finds the element
            self.driver.find_element_then_click(_locator=WaterSourcesLocators3200.DetailView.EDIT_BUTTON)
        
            print "Water Source - Detail View: Successfully Selected Edit Button"
        except NoSuchElementException as e:
            e_msg = "Unable to locate the water source edit button and click WebElement ID: {0}. {1}".format(
                WaterSourcesLocators3200.DetailView.EDIT_BUTTON,
                e.message
            )
            raise NoSuchElementException(e_msg)

    #################################
    def save_edits(self):
        """
        Selects the save button and handles the TP pop-up that explains what data is being sent/received from
        the controller.
        """
        _locator = WaterSourcesLocators3200.DetailView.SAVE_BUTTON
        if self.driver.is_visible(_locator=_locator):
            self.driver.find_element_then_click(_locator=_locator)
        else:
            e_msg = "Water Source Detail View: Unable to locate SAVE button. Not visible to user."
            raise ElementNotVisibleException(e_msg)

    #################################
    def set_description(self, _water_source_address, _description):
        """
        Sets the description of a Water Source in detail view.

        :param _water_source_address:    Address of the water source we want to verify.
        :type _water_source_address:     int

        :param _description:        Description we want to set on BaseManager that will be sent to the controller.
        :type _description:         str
        """
        locator = WaterSourcesLocators3200.DetailView.DESCRIPTION
        if self.driver.is_visible(_locator=locator):
            self.driver.send_text_to_element(_locator=locator, text_to_send=_description, clear_field=True)
            self.water_sources[_water_source_address].ds = _description
        else:
            raise WebDriverException("Flow Setup > Water Sources Tab: Unable to locate Edit Description in the "
                                     "Water Sources Detail View. Wasn't visible.")

    #################################
    def verify_description(self, _water_source_address):
        """
        Verifies the description when in a detailed view for a Water Source.

        :param _water_source_address:    Address of the water source we want to verify.
        :type _water_source_address:     int
        """
        locator = WaterSourcesLocators3200.DetailView.DESCRIPTION
        if self.driver.is_visible(_locator=locator):
            # Verify the description we got from the client against our object
            # Since the description is hiding inside an 'input' tag, we need to call 'get_attribute' to get the text
            actual_description = self.driver.web_driver.find_element(*locator).get_attribute("value")
            expected_description = self.water_sources[_water_source_address].ds
            if expected_description == actual_description:
                print "Successfully verified Water Source {0}'s description on BaseManager to be '{1}'".format(
                    _water_source_address,
                    actual_description
                )
            else:
                e_msg = "Water Source Tab - Detail View: Description on BaseManager did not match what the test " \
                        "engine thinks it should be:\n\t\tExpected: \t{0}\n\t\tActual: \t{1}".format(
                            expected_description,
                            actual_description
                        )
                raise ValueError(e_msg)
        else:
            e_msg = "Water Source Tab - Detail View: Unable to find the Description of Water Source {0} using " \
                    "locator: {1}".format(
                        _water_source_address,
                        locator
                    )
            raise NoSuchElementException(e_msg)

    #################################
    def verify_description_in_water_source_list(self, _address):
        """
        Verifies the description when in a detailed view for a Water Source.

        :param _address:    Address of the Water Source we want to verify.
        :type _address:     int
        """
        locator = (By.ID, "waterSourceRow-{0}".format(_address))
        if self.driver.is_visible(_locator=locator):
            # Verify the description we got from the client against our object
            actual_description = self.driver.get_text_from_web_element(_locator=locator)
            expected_description = self.water_sources[_address].ds
            if expected_description in actual_description:
                print "Successfully verified Water Source {0}'s description on BaseManager to be '{1}'".format(
                    _address,
                    actual_description
                )
            else:
                e_msg = "Water Source Tab - Detail View: Description on BaseManager did not match what the test " \
                        "engine thinks it should be:\n\t\tExpected: \t{0}\n\t\tActual: \t{1}".format(
                            expected_description,
                            actual_description
                        )
                raise ValueError(e_msg)
        else:
            e_msg = "Water Source Tab - Detail View: Unable to find the Description of Water Source {0} using" \
                    " locator: {1}".format(
                        _address,
                        locator
                    )
            raise NoSuchElementException(e_msg)


class PointOfConnectionPage3200(WaterSourcesPage3200):
    """
    BL_3200 Control Point Tab Object \n
    """
    
    page_title = 'Points Of Control'
    page_header_text = 'Control Points'
    edit_button_locator = PocLocators3200.DetailView.EDIT_BUTTON
    
    def __init__(self, web_driver, controller):
        super(WaterSourcesPage3200, self).__init__(web_driver=web_driver)

        self.points_of_control = controller.points_of_control   # Points of control objects
        """:type: dict[int, common.objects.programming.point_of_control.PointOfControl]"""
    
    #################################
    def select_poc(self, poc_number):
        """
        Selects the POC row with respect to the POC number passed in.
        :param poc_number: POC number of POC to select. \n
        :type poc_number: int
        """
        poc_row_locator = (By.ID, "pocRow-{0}".format(poc_number))
        
        if self.driver.is_visible(_locator=poc_row_locator):
            self.driver.find_element_then_click(_locator=poc_row_locator)
        else:
            e_msg = "Unable to locate row for POC: {0} in {1} List View. Wasn't visible.".format(
                poc_number,
                self.page_title
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def click_edit_button(self):
        """
        Clicks the edit button for the POC page
        """
        try:
            # Finds the element
            self.driver.find_element_then_click(_locator=PocLocators3200.DetailView.EDIT_BUTTON)

            print "Control Point - Detail View: Successfully Selected Edit Button"
        except NoSuchElementException as e:
            e_msg = "Unable to locate the control point edit button and click WebElement ID: {0}. {1}".format(
                PocLocators3200.DetailView.EDIT_BUTTON,
                e.message
            )
            raise NoSuchElementException(e_msg)

    #################################
    def save_edits(self):
        """
        Selects the save button and handles the TP pop-up that explains what data is being sent/received from
        the controller.
        """
        _locator = PocLocators3200.DetailView.SAVE_BUTTON
        if self.driver.is_visible(_locator=_locator):
            self.driver.find_element_then_click(_locator=_locator)
        else:
            e_msg = "Control Point Detail View: Unable to locate SAVE button. Not visible to user."
            raise ElementNotVisibleException(e_msg)

    #################################
    def set_description(self, _control_point_address, _description):
        """
        Sets the description of a Control Point in detail view.

        :param _control_point_address:    Address of the control point we want to verify.
        :type _control_point_address:     int

        :param _description:        Description we want to set on BaseManager that will be sent to the controller.
        :type _description:         str
        """
        locator = PocLocators3200.DetailView.DESCRIPTION
        if self.driver.is_visible(_locator=locator):
            self.driver.send_text_to_element(_locator=locator, text_to_send=_description, clear_field=True)
            self.points_of_control[_control_point_address].ds = _description
        else:
            raise WebDriverException("Flow Setup > Control Point Tab: Unable to locate Edit Description in the CP "
                                     "Detail View. Wasn't visible.")
    
    #################################
    def verify_open(self):
        """
        Verifies that the Water Sources Tab opens successfully by verifying text at the top of the page. \n
        """
        WaterSourcesPage3200.verify_is_open(self, _locator=WaterSourcesLocators3200.POINT_OF_CONNECTION_VIEW)
    
    #################################
    def verify_header_text(self):
        """
        Verifies that the Points of Control view displays the correct header.
        """
        WaterSourcesPage3200.verify_header_text_is_displayed(self, _locator=PocLocators3200.ListView.HEADER)
    
    #################################
    def verify_list_view_table_id_column_displayed(self):
        """
        Verifies the ID column text displayed for the POC list table.
        """
        column_el = self.get_element(_locator=PocLocators3200.ListView.ID_COLUMN)
        column_el_text = column_el.text
        expected_el_text = 'ID'
        
        if column_el_text != expected_el_text:
            e_msg = "Unable to verify {0}'s List View ID Column Text. Received: {1}, " \
                    "Expected: {2}".format(
                        self.page_title,
                        column_el_text,
                        expected_el_text
                    )
            raise ValueError(e_msg)
        else:
            print "Successfully verified {0}'s List View ID Column Text: {1}".format(
                self.page_title,
                expected_el_text
            )
    
    #################################
    def verify_list_view_table_description_column_displayed(self):
        """
        Verifies the description column text displayed for the POC list table.
        """
        column_el = self.get_element(_locator=PocLocators3200.ListView.DESCRIPTION_COLUMN)
        column_el_text = column_el.text
        expected_el_text = 'Description'
        
        if column_el_text != expected_el_text:
            e_msg = "Unable to verify {0}'s List View Description Column Text. Received: {1}, " \
                    "Expected: {2}".format(
                        self.page_title,
                        column_el_text,
                        expected_el_text
                    )
            raise ValueError(e_msg)
        else:
            print "Successfully verified {0}'s List View Description Column Text: {1}".format(
                self.page_title,
                expected_el_text
            )
    
    #################################
    def verify_list_view_table_mainline_column_displayed(self):
        """
        Verifies the description column text displayed for the POC list table.
        """
        column_el = self.get_element(_locator=PocLocators3200.ListView.MAINLINE_COLUMN)
        column_el_text = column_el.text
        expected_el_text = 'Mainline'
        
        if column_el_text != expected_el_text:
            e_msg = "Unable to verify {0}'s List View Mainline Column Text. Received: {1}, " \
                    "Expected: {2}".format(
                self.page_title,
                column_el_text,
                expected_el_text
            )
            raise ValueError(e_msg)
        else:
            print "Successfully verified {0}'s List View Mainline Column Text: {1}".format(
                self.page_title,
                expected_el_text
            )
    
    #################################
    def verify_detail_view_displayed(self, poc_number):
        """
        :return:
        :rtype:
        """
        detail_view = self.driver.wait_for_element_visible(_locator=PocLocators3200.DetailView.MAIN_VIEW)
        poc_id_displayed = self.get_element(_locator=PocLocators3200.DetailView.POC_ID)
        
        poc_id_displayed_text = poc_id_displayed.get_attribute("value")
        
        if detail_view.is_displayed() and str(poc_number) == poc_id_displayed_text:
            print "Successfully verified {0} {1}'s Detail View is Displayed.".format(
                self.page_title,
                poc_number
            )
        else:
            e_msg = "Unable to verify {0} {1}'s Detail View is Displayed.".format(
                self.page_title,
                poc_number
            )
            raise ElementNotVisibleException(e_msg)
    
    #################################
    def verify_low_pressure_shutdown_label_text_jira_bm_1922(self):
        """
        Added for regression testing against a v12 POC detail view bug where "Share this POC" option was
        being displayed under the POC Priority field. It shouldn't be visible.
        """
        what_is_being_verified = "Low Pressure Shutdown Label Text"
        jira_bug_ref = "BM-1922 (https://baseline.atlassian.net/browse/BM-1922)"
        
        element = self.get_element(_locator=PocLocators3200.DetailView.LOW_PRESSURE_SHUTDOWN_LABEL)
        
        displayed_low_pressure_shutdown_label_text = element.text
        expected_low_pressure_shutdown_label_text = "Shutdown"
        
        if displayed_low_pressure_shutdown_label_text != expected_low_pressure_shutdown_label_text:
            e_msg = "\n{0} Detail View: Unable to verify '{1}'.\n->\tCurrent text: '{2}', Expected: '{3}' " \
                    "for v16 3200 on BaseManager.\n->\tSee JIRA Bug: {4}\n".format(
                        self.page_title,
                        what_is_being_verified,
                        displayed_low_pressure_shutdown_label_text,
                        expected_low_pressure_shutdown_label_text,
                        jira_bug_ref
                    )
            raise AssertionError(e_msg)
        else:
            print "{0} Detail View: Successfully verified '{1}' ['{2}'].\n->\tThis verifies " \
                  "JIRA Bug: {3}".format(
                      self.page_title,
                      what_is_being_verified,
                      expected_low_pressure_shutdown_label_text,
                      jira_bug_ref
                  )
            
    #################################
    def verify_master_valve_enable_is_visible_jira_bm_1926(self):
        """
        Added for regression testing against a v16 POC detail view bug where Master Valve "Enable" flag was being
        displayed for v12 3200. This is only available for v16 3200. This verifies the fix for v12 didn't break
        v16.
        """
        what_is_being_verified = "Master Valve Enable"
        jira_bug_ref = "BM-1926 (https://baseline.atlassian.net/browse/BM-1926)"
    
        if self.driver.is_visible(_locator=PocLocators3200.DetailView.MV_ENABLE):
            print "{0} Detail View: Successfully verified '{1}' is visible for v16 3200.\n->\tThis verifies " \
                  "JIRA Bug: {2}".format(
                      self.page_title,
                      what_is_being_verified,
                      jira_bug_ref
                  )
        else:
            e_msg = "\n{0} Detail View: Unable to verify '{1}' is visible for v16 3200.\n->\tIt should be visible " \
                    "for v16 3200 on BaseManager.\n->\tSee JIRA Bug: {2}\n".format(
                        self.page_title,
                        what_is_being_verified,
                        jira_bug_ref
                    )
            raise AssertionError(e_msg)

    #################################
    def verify_description(self, _control_point_address):
        """
        Verifies the description when in a detailed view for a Control Point.

        :param _control_point_address:    Address of the Control Point we want to verify.
        :type _control_point_address:     int
        """
        locator = PocLocators3200.DetailView.DESCRIPTION
        if self.driver.is_visible(_locator=locator):
            # Verify the description we got from the client against our object
            # Since the description is hiding inside an 'input' tag, we need to call 'get_attribute' to get the text
            actual_description = self.driver.web_driver.find_element(*locator).get_attribute("value")
            expected_description = self.points_of_control[_control_point_address].ds
            if expected_description == actual_description:
                print "Successfully verified Control Point {0}'s description on BaseManager to be '{1}'".format(
                    _control_point_address,
                    expected_description
                )
            else:
                e_msg = "Control Point Tab - Detail View: Description on BaseManager did not match what the test " \
                        "engine thinks it should be:\n\t\tExpected: \t{0}\n\t\tActual: \t{1}".format(
                            expected_description,
                            actual_description
                        )
                raise ValueError(e_msg)
        else:
            e_msg = "Control Point Tab - Detail View: Unable to find the Description of Control Point {0} using " \
                    "locator: {1}".format(
                        _control_point_address,
                        locator
                    )
            raise NoSuchElementException(e_msg)

    #################################
    def verify_description_in_control_point_list(self, _address):
        """
        Verifies the description when in a detailed view for a Control Point.

        :param _address:    Address of the Control Point we want to verify.
        :type _address:     int
        """
        locator = (By.ID, "pocRow-{0}".format(_address))
        if self.driver.is_visible(_locator=locator):
            # Verify the description we got from the client against our object
            actual_description = self.driver.get_text_from_web_element(_locator=locator)
            expected_description = self.points_of_control[_address].ds
            if expected_description in actual_description:
                print "Successfully verified Control Point {0}'s description on BaseManager to be '{1}'".format(
                    _address,
                    expected_description
                )
            else:
                e_msg = "Control Point Tab - Detail View: Description on BaseManager did not match what the test " \
                        "engine thinks it should be:\n\t\tExpected: \t{0}\n\t\tActual: \t{1}".format(
                            expected_description,
                            actual_description
                        )
                raise ValueError(e_msg)
        else:
            e_msg = "Control Point Tab - Detail View: Unable to find the Description of Control Point {0} using" \
                    " locator: {1}".format(
                        _address,
                        locator
                    )
            raise NoSuchElementException(e_msg)


class MainlinesPage3200(WaterSourcesPage3200):
    page_title = "Mainlines"
    page_header_text = 'Mainlines'
    edit_button_locator = MainlineLocators3200.DetailView.EDIT_BUTTON

    def __init__(self, web_driver, controller):
        super(WaterSourcesPage3200, self).__init__(web_driver=web_driver)

        self.mainlines = controller.mainlines
        """:type: dict[int, common.objects.programming.ml.Mainline]"""

    #################################
    def select_mainline(self, _address):
        """
        Selects the mainline row with respect to the mainline number passed in.
        :param _address: mainline number of mainline to select. \n
        :type _address: int
        """
        mainline_row_locator = (By.ID, "mainlineRow-{0}".format(_address))

        if self.driver.is_visible(_locator=mainline_row_locator):
            self.driver.find_element_then_click(_locator=mainline_row_locator)
        else:
            e_msg = "Unable to locate row for mainline: {0} in {1} List View. Wasn't visible.".format(
                _address,
                self.page_title
            )
            raise ElementNotVisibleException(e_msg)

    #################################
    def click_edit_button(self):
        """
        Clicks the edit button for the ML page
        """
        try:
            # Finds the element
            self.driver.find_element_then_click(_locator=MainlineLocators3200.DetailView.EDIT_BUTTON)

            print "Mainline - Detail View: Successfully Selected Edit Button"
        except NoSuchElementException as e:
            e_msg = "Unable to locate the mainline edit button and click WebElement ID: {0}. {1}".format(
                MainlineLocators3200.DetailView.EDIT_BUTTON,
                e.message
            )
            raise NoSuchElementException(e_msg)

    #################################
    def save_edits(self):
        """
        Selects the save button and handles the TP pop-up that explains what data is being sent/received from
        the controller.
        """
        _locator = MainlineLocators3200.DetailView.SAVE_BUTTON
        if self.driver.is_visible(_locator=_locator):
            self.driver.find_element_then_click(_locator=_locator)
        else:
            e_msg = "Mainline Detail View: Unable to locate SAVE button. Not visible to user."
            raise ElementNotVisibleException(e_msg)

    #################################
    def set_description(self, _mainline_address, _description):
        """
        Sets the description of a Mainline in detail view.

        :param _mainline_address:    Address of the mainline we want to verify.
        :type _mainline_address:     int

        :param _description:        Description we want to set on BaseManager that will be sent to the controller.
        :type _description:         str
        """
        locator = MainlineLocators3200.DetailView.DESCRIPTION
        if self.driver.is_visible(_locator=locator):
            self.driver.send_text_to_element(_locator=locator, text_to_send=_description, clear_field=True)
            self.mainlines[_mainline_address].ds = _description
        else:
            raise WebDriverException("Flow Setup > Mainline Tab: Unable to locate Edit Description in the ML "
                                     "Detail View. Wasn't visible.")
    
    def verify_open(self):
        """
        Verifies that the Water Sources Tab opens successfully by verifying text at the top of the page. \n
        """
        WaterSourcesPage3200.verify_is_open(self, _locator=WaterSourcesLocators3200.MAINLINES_VIEW)

    #################################
    def verify_description(self, _mainline_address):
        """
        Verifies the description when in a detailed view for a Mainline.

        :param _mainline_address:    Address of the Mainline we want to verify.
        :type _mainline_address:     int
        """
        locator = MainlineLocators3200.DetailView.DESCRIPTION
        if self.driver.is_visible(_locator=locator):
            # Verify the description we got from the client against our object
            # Since the description is hiding inside an 'input' tag, we need to call 'get_attribute' to get the text
            actual_description = self.driver.web_driver.find_element(*locator).get_attribute("value")
            expected_description = self.mainlines[_mainline_address].ds
            if expected_description == actual_description:
                print "Successfully verified Mainline {0}'s description on BaseManager to be '{1}'".format(
                    _mainline_address,
                    expected_description
                )
            else:
                e_msg = "Mainline Tab - Detail View: Description on BaseManager did not match what the test " \
                        "engine thinks it should be:\n\t\tExpected: \t{0}\n\t\tActual: \t{1}".format(
                            expected_description,
                            actual_description
                        )
                raise ValueError(e_msg)
        else:
            e_msg = "Mainline Tab - Detail View: Unable to find the Description of Mainline {0} using " \
                    "locator: {1}".format(
                        _mainline_address,
                        locator
                    )
            raise NoSuchElementException(e_msg)

    #################################
    def verify_description_in_mainline_list(self, _address):
        """
        Verifies the description when in a detailed view for a Mainline.

        :param _address:    Address of the Mainline we want to verify.
        :type _address:     int
        """
        locator = (By.ID, "mainlineRow-{0}".format(_address))
        if self.driver.is_visible(_locator=locator):
            # Verify the description we got from the client against our object
            actual_description = self.driver.get_text_from_web_element(_locator=locator)
            expected_description = self.mainlines[_address].ds
            if expected_description in actual_description:
                print "Successfully verified Mainline {0}'s description on BaseManager to be '{1}'".format(
                    _address,
                    expected_description
                )
            else:
                e_msg = "Mainline Tab - Detail View: Description on BaseManager did not match what the test " \
                        "engine thinks it should be:\n\t\tExpected: \t{0}\n\t\tActual: \t{1}".format(
                            expected_description,
                            actual_description
                        )
                raise ValueError(e_msg)
        else:
            e_msg = "Mainline Tab - Detail View: Unable to find the Description of Mainline {0} using" \
                    " locator: {1}".format(
                        _address,
                        locator
                    )
            raise NoSuchElementException(e_msg)
