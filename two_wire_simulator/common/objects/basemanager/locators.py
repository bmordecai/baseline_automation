__author__ = 'baseline'

from selenium.webdriver.common.by import By


class AdministrationMenuLocators(object):
    """
    Locators that are specific to the administration functions
    """
    ADMINISTRATION = (By.ID,"menu-administration")
    MANAGE_SUBSCRIPTIONS = (By.ID, "menu-subscriptions")
    MANAGE_WEATHER_STATIONS = (By.ID, "menu-weather_stations")


class AdminToolsLocators(object):
    """
    Locators that are specific to the admin tools area
    """
    ADMIN_TOOLS_HEADER = (By.ID, "")
    # CHANGE_PASSWORD_MENU =(By.CLASS_NAME, 'yuimenuitemlabel')
    CHANGE_PASSWORD_MENU =(By.ID, 'yui-gen0')
    OLD_PASSWORD = (By.ID,'opwd')
    NEW_PASSWORD = (By.ID,'npwd')
    CONFIRM_NEW_PASSWORD = (By.ID,'cpwd')
    SELECT_COMPANY = (By.NAME,)
    CHANGE_PASSWORD_BUTTON = (By.ID,'btnsubmit')


class ControllerSettingsLocators(object):
    """
    Locators that are specific to the controller setting info dialog box
    """
    CLOSE_BUTTON = (By.ID, "trEditControllerSerial")
    CANCEL_BUTTON = (By.ID, "trEditControllerSerial")
    EDIT_BUTTON = (By.ID, "editControllerDesc")
    INPUT_BOX_FOR_DATABASE_ID = (By.ID, "user-update-input")
    INPUT_BOX_FOR_NAME = (By.ID, "controllerEditName")
    SAVE_BUTTON = (By.ID, "editControllerDesc")
    SEND_FIRMWARE_VERSION_BUTTON = (By.ID, "upgradeFwSpecific")
    SERIAL_NUMBER_TEXT_LOCATION = (By.ID, "trEditControllerSerial")


class DeviceTabLocators(object):
    """
    Locators for the device tab. \n
    """
    CANCEL_BUTTON = (By.ID, "sacancel")

    DEVICE_DESCRIPTION = (By.ID, "decoderdesc_")
    DEVICE_EDIT_DESCRIPTION = (By.ID, "tpzone-description")
    ZONE_EDIT_DESCRIPTION = (By.ID, "zone-description")

    EVENT_SWITCH_ASSIGN = (By.ID, "switchAssign")
    EVENT_SWITCH_REFRESH_BUTTON = (By.ID, "switchRefreshButton")
    EVENT_SWITCH_VIEW_BUTTON = (By.ID, "switchView")
    
    FLOW_METER_REFRESH_BUTTON = (By.ID, "flowRefreshButton")
    FLOW_METER_ASSIGN = (By.ID, "flowAssign")
    FLOW_METER_VIEW_BUTTON = (By.ID, "flowView")
    
    MASTER_VALVE_ASSIGN = (By.ID, "masterAssign")
    MASTER_VALVE_REFRESH_BUTTON = (By.ID, "masterRefreshButton")
    MASTER_VALVE_VIEW_BUTTON = (By.ID, "masterView")

    MOISTURE_SENSOR_ASSIGN = (By.ID, "moistureAssign")
    MOISTURE_SENSOR_REFRESH_BUTTON = (By.ID, "moistureRefreshButton")
    MOISTURE_SENSOR_VIEW_BUTTON = (By.ID, "moistureView")
    
    PRESSURE_SENSOR_ASSIGN = (By.ID, "analogAssign")
    PRESSURE_SENSOR_REFRESH_BUTTON = (By.ID, "analogRefreshButton")
    PRESSURE_SENSOR_VIEW_BUTTON = (By.ID, "analogView")

    PUMP_ASSIGN = (By.ID, "pumpAssign")
    PUMP_REFRESH_BUTTON = (By.ID, "pumpRefreshButton")
    PUMP_VIEW_BUTTON = (By.ID, "pumpView")

    SAVE_BUTTON = (By.ID, "sasave")
    DEVICE_EDIT_SAVE_BUTTON = (By.ID, "dvsave")
    
    TEMPERATURE_REFRESH_BUTTON = (By.ID, "temperatureRefreshButton")
    TEMPERATURE_SENSOR_ASSIGN = (By.ID, "temperatureAssign")
    TEMPERATURE_SENSOR_VIEW_BUTTON = (By.ID, "temperatureView")
    
    ZONE_ASSIGN = (By.ID, "zoneAssign")
    ZONE_REFRESH_BUTTON = (By.ID, "zoneRefreshButton")
    ZONE_VIEW_BUTTON = (By.ID, "zoneView")
    ZONE_EDIT_BUTTON = (By.ID, "editZoneButton")
    ZONE_EDIT_SAVE_BUTTON = (By.ID, "zoneEditSave")


class DialogBoxLocators(object):
    """
    Dialog box locators. \n
    """
    INFO_DIALOG = (By.ID, "infoDialog")


class LoginLocators(object):
    USER_NAME_INPUT = (By.ID, "username")
    PASSWORD_INPUT = (By.ID, "password")
    SIGN_IN_BUTTON = (By.ID, "login")
    LOGIN_ERROR = (By.ID, "loginError")
    USE_PREVIOUS_VERSION = (By.ID, "desktop")
    GO_TO_MOBILE_VERSION = (By.ID, "mobile")
    RETRIEVE_FROGOTTEN_PASSWORD = (By.ID, "lostpwd")
    SET_UP_A_NEW_ACCOUNT = (By.ID, "support")


class MainPageLocators(object):
    """
    Locator's that are not tab specific, but include tabs
    """
    COLOR_BUTTON = (By.ID, "colorbutton")
    COMPANY = (By.ID, "global_view_l")
    CONTROLLER_SETTINGS = (By.ID, "cnteditbutton")
    CURRENT_CONTROLLER = (By.ID, "controller_view_l")
    CURRENT_SITE = (By.ID, "company_view_l")
    DEVICES_TAB = (By.ID, "zonesensor_view_l")
    EVENT_SWITCHES = (By.ID, "switch_view_l")
    FLOW = (By.ID, "flow_view_l")
    FLOW_SETUP = (By.ID, "water_view_l")
    FOOTER_CONTROLLER_STATUS = (By.ID, "footer_controller_status")
    LIVEVIEW_TAB = (By.ID, "remote_view_l")
    MAINLINES = (By.ID, "mainline_view_l")
    MAIN_MENU = (By.ID, "menu")
    MAIN_MENU_BUTTON = (By.ID, "mainmenu")
    MAPS_TAB = (By.ID, "maptype")
    MASTER_VALVES = (By.ID, "mv_view_l")
    MOISTURE_SENSORS = (By.ID, "moisture_view_l")
    POINT_OF_CONNECTION = (By.ID, "poc_view_l")
    PRESSURE_SENSORS = (By.ID, "analog_view_l")
    PROGRAMS_TAB = (By.ID, "programs_view_l")
    PUMPS = (By.ID, "pump_view_l")
    QUICK_VIEW_TAB = (By.ID, "manage_view_l")
    TEMPERATURE = (By.ID, "temperature_view_l")
    WATER_SOURCES = (By.ID, "water_source_view_l")
    WATER_SOURCES_TAB = (By.ID, "water_view_l")
    ZONES = (By.ID, "zones_view_l")
    LOGOUT = (By.ID, "logout")  # This is for the old website


class MainMenuLocators(object):
    """
    Locators that are specific to the menu
    """
    ADMINISTRATION = (By.ID, "menu-admin")
    COMPANIES = (By.ID, "menu-companies")
    RAIN_DELAY = (By.ID, "menu-rain-delay")
    REPORTS = (By.ID, "menu-reports")
    SITES = (By.ID, "menu-sites-and-controllers")
    SIGN_OUT = (By.ID, "menu-logout")
    SUBSCRIPTIONS = (By.ID, "menu-subscriptions")


class MapTabLocators(object):
    """
    All locators associated with map tab.
    """
    MENU_TAB_ITEM = (By.ID, "map_view_li")
    MAP_EDIT_BUTTON = (By.ID, "mapEditButton")
    COMPANY_VIEW = (By.ID, "global_view_l")
    SITE_VIEW = (By.ID, "company_view_l")
    CONTROLLER_VIEW = (By.ID, "controller_view_l")
    MARKERS_MENU_OPTION = (By.ID, "map-menu-markers")
    FLOWSTATION_MARKERS_MENU_OPTION = (By.ID, "map-menu-markers-type-flowStations")


class MobileAccessLocators(object):
    CONTROLLER_MENU = (By.ID, "controllerMenu")
    EVENT_SWITCH_LIST_TOGGLE = (By.ID, "switchListHeader")
    FLOW_METER_LIST_TOGGLE = (By.CSS_SELECTOR, "#flowList #flowListHeader")
    FLOW_STATUS_PAGE = (By.CLASS_NAME, "accessFlowStatus")
    GEO_LOCATE_DEVICE_PAGE = (By.CLASS_NAME, "accessMarkDevice")
    GO_TO_DESKTOP_VERSION = (By.ID, "td-desktop")
    LOGIN_FORM = (By.ID, "wizardform")
    LOGIN_SUBMIT = (By.XPATH, "//button[@value='login']")
    MANUAL_RUN_ZONE_PAGE = (By.CLASS_NAME, "accessManualRun")
    MASTER_VALVE_LIST_TOGGLE = (By.ID, "masterValveList")
    MENU_TITLE = (By.ID, "menuTitle")
    MESSAGES_PAGE = (By.CLASS_NAME, "accessMessages")
    NEXT_DEVICE = (By.CSS_SELECTOR, "#buttomTestButtonsWrapper .right-rounded")
    MOISTURE_SENSOR_LIST_TOGGLE = (By.ID, "moistureList")
    PRESSURE_SENSOR_LIST_TOGGLE = (By.ID, "zoneListHeader")
    PREVIOUS_DEVICE = (By.XPATH, "//span[@onclick='previousDevice();']")
    PUMP_LIST_TOGGLE = (By.ID, "pumpListHeader")
    RAIN_DELAY_PAGE = (By.CLASS_NAME, "accessRainDelay")
    SELECT_CONTROLLER_MENU = (By.ID, "selectController")
    START_STOP_PROGRAM_PAGE = (By.CLASS_NAME, "accessStartStopProgram")
    STOP_ALL_MENU_OPERATIONS_PAGE = (By.CLASS_NAME, "listNoArrow")
    TEMPERATURE_SENSOR_LIST_TOGGLE = (By.ID, "temperatureListHeader")
    TEST_DEVICE = (By.XPATH, "//span[@onclick='testDevice();']")
    TEST_DEVICE_RESULTS = (By.ID, "testDeviceResults")
    TEST_DEVICE_PAGE = (By.CLASS_NAME, "accessTestDevice")
    ZONE_LIST_TOGGLE = (By.ID, "zoneListHeader")


class ProgramsTabLocators(object):
    """
    Locator's for the programs tab
    """

    class DetailView(object):
        CANCEL_BUTTON = (By.ID, "programEditCancel")
        DESCRIPTION = (By.ID, "programDesc")
        ENABLE_ET_CHECKBOX = (By.ID, "program-enable-et-label")
        HEADER_TEXT = (By.CSS_SELECTOR, "#programsListDetail > div.scrollwrapper > h1")
        PROGRAM_DESCRIPTION = (By.ID, "program-description")
        PROGRAM_EDIT_BUTTON = (By.ID, "programEditButton")
        SAVE_BUTTON = (By.ID, "programEditSave")
        VIEW = (By.ID, "programsListDetail")
        ZONE_ADD_ROW = (By.ID, "zoneAdd")

    class ListView(object):
        ADD_PROGRAM_ROW = (By.ID, "programAdd")
        SEARCH_FIELD = (By.ID, "programsListSearch")


class QuickViewLocators(object):
    """
    Locator's that are specific to the QuickView Tab menu
    """
    TOOLTIP_DESCRIPTION = (By.ID, "qtip-tooltip-title")
    ZONE_STATUS_HEADER = (By.ID, "quickview_zone_status_header")


class SubscriptionPageLocators(object):
    """
    Locator's that are specific to the Subscriptions page
    """
    SUBSCRIPTION_HEADER = (By.ID, "subscriptions_view_header")
    SUBSCRIPTION_EDIT = (By.ID, "subscriptionEditButton")
    SUBSCRIPTION_TYPE_LITE = (By.ID, "subscriptionsLabelLite")
    SUBSCRIPTION_TYPE_BASEMANAGER = (By.ID, "subscriptionsLabelBM")
    SUBSCRIPTION_TYPE_PLUS = (By.ID, "subscriptionsLabelBMP")
    SUBSCRIPTION_RENEWAL_DATE = (By.ID, "renewalDate")
    SUBSCRIPTION_SAVE_EDIT = (By.ID, "subscriptionSaveButton")


class WaterSourcesLocators3200(object):
    """
    Locator's that are specific to the 3200 Water Sources Tab
    """
    POINT_OF_CONNECTION_VIEW = (By.ID, "poc_view")
    MAINLINES_VIEW = (By.ID, "mainline_view")

    class DetailView(object):
        DESCRIPTION = (By.ID, "water_source_description")
        EDIT_BUTTON = (By.ID, "water_source_edit_button")
        SAVE_BUTTON = (By.ID, "water_source_save_button")


class PocLocators3200(object):
    """
    Locator's that are specific to the 3200 POC Tab
    """
    
    class ListView(object):
        DESCRIPTION_COLUMN = (By.ID, "poc_status_table_desc_header")
        HEADER = (By.ID, "pocListHeaderText")
        ID_COLUMN = (By.ID, "poc_status_table_id_header")
        MAINLINE_COLUMN = (By.ID, "poc_status_table_mainline_header")
    
    class DetailView(object):
        DESCRIPTION = (By.ID, "poc_description")
        EDIT_BUTTON = (By.ID, "poc_edit_button")
        LOW_PRESSURE_SHUTDOWN_LABEL = (By.ID, "poc_low_pressure_enabled_label_text")
        MAIN_VIEW = (By.ID, "pocDetail")
        MV_ENABLE = (By.ID, "poc_master_valve_enabled_value")
        POC_ID = (By.ID, "poc_id")
        SAVE_BUTTON = (By.ID, "poc_save_button")
        SHARE_THIS_POC = (By.ID, "poc_flowstation_assigned")
        SW_EMPTY_LIMIT_ELEMENT = (By.ID, "poc_event_switch_empty_limit_value")
        SW_EMPTY_TIME_ELEMENT = (By.ID, "poc_event_switch_empty_time_value")
        
        
class MainlineLocators3200(object):
    """
    Locator's that are specific to the 3200 Mainlines Tab
    """
    
    class ListView(object):
        HEADER = (By.ID, "mainlineListHeaderText")
        ID_COLUMN_HEADER = (By.ID, "mainline_status_table_id_header")
        DESCRIPTION_COLUMN_HEADER = (By.ID, "mainline_status_table_desc_header")
        ZONE_COLUMN_HEADER = (By.ID, "mainline_status_table_zones_header")
    
    class DetailView(object):
        DESCRIPTION = (By.ID, "mainline_description")
        EDIT_BUTTON = (By.ID, "mainline_edit_button")
        MAIN_VIEW = (By.ID, "mainlineDetail")
        MAINLINE_ID = (By.ID, "mainline_id")
        SAVE_BUTTON = (By.ID, "mainline_save_button")


class TagNames(object):
    """
    Locator's that are generic to HTML
    """
    TABLE_ROW = (By.TAG_NAME, "tr")
