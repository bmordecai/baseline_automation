__author__ = 'baseline'

from selenium.common.exceptions import *
from locators import *
import main_page as main_page


class ProgramsTabPage(main_page.MainPage):
    """
    Programs Tab Object \n
    """
    def __init__(self, web_driver, controller):
        super(ProgramsTabPage, self).__init__(web_driver=web_driver)

        self.programs = controller.programs
        """:type: dict[int, common.objects.programming.pg_3200.PG3200]"""

        self.page_title = 'Programs Tab'

    #################################
    def select_program(self, _address):
        """
        Selects a program row on BaseManager in Programs Tab.
        """
        locator = (By.ID, "programRow-{0}".format(_address))
        if self.driver.is_visible(_locator=locator):
            self.driver.find_element_then_click(_locator=locator)
        else:
            raise WebDriverException("Programs Tab: Unable to locate Program {0} in the Program List. Wasn't "
                                     "visible.".format(_address))

    #################################
    def select_add_program(self):
        """
        Selects the "Add Program" row on BaseManager in Programs Tab.
        """
        if self.driver.is_visible(_locator=ProgramsTabLocators.ListView.ADD_PROGRAM_ROW):
            self.driver.find_element_then_click(_locator=ProgramsTabLocators.ListView.ADD_PROGRAM_ROW)
            self.driver.handle_dialog_box()
        else:
            raise WebDriverException("Programs Tab: Unable to locate and click Add Program row. Wasn't visible.")

    #################################
    def select_add_zone(self):
        """
        Selects the "Add Zone" row on BaseManager in Programs Tab - Detail View.
        """
        if self.driver.is_visible(_locator=ProgramsTabLocators.DetailView.PROGRAM_EDIT_BUTTON):
            self.driver.find_element_then_click(_locator=ProgramsTabLocators.DetailView.PROGRAM_EDIT_BUTTON)
        else:
            raise WebDriverException("Programs Tab - Detail View: Unable to locate and click edit button. "
                                     "Wasn't visible.")

    #################################
    def select_edit_button(self):
        """
        Selects the "Edit" button on BaseManager in Programs Tab - Detail View.
        """
        if self.driver.is_visible(_locator=ProgramsTabLocators.DetailView.PROGRAM_EDIT_BUTTON):
            self.driver.find_element_then_click(_locator=ProgramsTabLocators.DetailView.PROGRAM_EDIT_BUTTON)
        else:
            raise WebDriverException("Programs Tab - Detail View: Unable to locate and click Program Edit Button. "
                                     "Wasn't visible.")

    #################################
    def set_description(self, _program_address, _description):
        """
        Selects a program row on BaseManager in Programs Tab.

        :param _program_address:    Address of the program we want to verify.
        :type _program_address:     int

        :param _description:        Description we want to set on BaseManager that will be sent to the controller.
        :type _description:         str
        """
        locator = ProgramsTabLocators.DetailView.PROGRAM_DESCRIPTION
        if self.driver.is_visible(_locator=locator):
            self.driver.send_text_to_element(_locator=locator, text_to_send=_description, clear_field=True)
            self.programs[_program_address].ds = _description
        else:
            raise WebDriverException("Programs Tab: Unable to locate Edit Description in the Program Detail View. "
                                     "Wasn't visible.")

    #################################
    def verify_open(self):
        """
        Verifies that the Programs Tab opens successfully by looking for the program search field. \n
        """
        try:
            self.driver.wait_for_element_clickable(_locator=ProgramsTabLocators.ListView.SEARCH_FIELD)
        except Exception as ve:
            caught_text = ve.message
            e_msg = "Unable to verify Programs tab is open. Exception received: {0}".format(
                caught_text
            )
            raise Exception(e_msg)
        else:
            print "Successfully verified Programs Tab opened"

    #################################
    def verify_program_add_or_detail_view_displayed(self):
        """
        Verifies the header displayed at the top of the program detail view. \n
        For new programs, "Add Program" is displayed, otherwise, the program's description is displayed. \n
        """
        if not self.driver.is_visible(_locator=ProgramsTabLocators.DetailView.VIEW):
            raise Exception("Unable to verify Programs tab - Add/Detail view is open.")
        else:
            print "Successfully verified Programs Tab - Add/Detail view opened."

    #################################
    def save_new_program(self):
        """
        Selects the save button and handles the TP pop-up that explains what data is being sent/received from
        the controller.
        """
        if self.driver.is_visible(_locator=ProgramsTabLocators.DetailView.SAVE_BUTTON):
            self.driver.find_element_then_click(_locator=ProgramsTabLocators.DetailView.SAVE_BUTTON)
        else:
            e_msg = "{0}: Unable to locate SAVE button. Not visible to user.".format(
                self.page_title
            )
            raise ElementNotVisibleException(e_msg)

        self.driver.handle_dialog_box()

    #################################
    def verify_newly_added_program(self, new_pg_number, new_pg_description):
        """
        Waits for and verifies the new program with address `new_pg_number` is added to the list of displayed programs
        and verifies the new program's description `new_pg_description` is displayed.

        :param new_pg_number: Program number of newly added program.
        :param new_pg_description: Description of newly added program.
        :return:
        """
        row_locator = (By.ID, "programRow-{0}".format(new_pg_number))
        text_locator = (By.CSS_SELECTOR, "#programRow-{0} > td.desc".format(new_pg_number))
        try:
            self.driver.wait_for_element_visible(_locator=row_locator)
            self.driver.wait_for_element_text_change(_locator=text_locator, _text=new_pg_description)
        except Exception:
            e_msg = "{0}: Unable locate newly added program: {1}. Didn't appear in list after save".format(
                self.page_title,
                new_pg_description
            )
            raise Exception(e_msg)
        else:
            print "{0}: Successfully verified new program showed up in Program List view after save.".format(
                self.page_title
            )

    #################################
    def verify_description(self, _program_address):
        """
        Verifies the description when in a detailed view for a program.

        :param _program_address:    Address of the program we want to verify.
        :type _program_address:     int
        """
        locator = ProgramsTabLocators.DetailView.DESCRIPTION
        if self.driver.is_visible(_locator=locator):
            # Verify the description we got from the client against our object
            actual_description = self.driver.get_text_from_web_element(_locator=locator)
            expected_description = self.programs[_program_address].ds
            if expected_description == actual_description:
                print "Successfully verified Program {0}'s description on BaseManager to be '{1}'".format(
                    _program_address,
                    actual_description
                )
            else:
                e_msg = "Program Tab - Detail View: Description on BaseManager did not match what the test engine " \
                        "thinks it should be:\n\t\tExpected: \t{0}\n\t\tActual: \t{1}".format(
                            expected_description,
                            actual_description
                        )
                raise ValueError(e_msg)
        else:
            e_msg = "Program Tab - Detail View: Unable to find the Description of Program {0} using locator: {1}".format(
                _program_address,
                locator
            )
            raise NoSuchElementException(e_msg)

    #################################
    def verify_description_in_program_list(self, _address):
        """
        Verifies the description when in a detailed view for a program.

        :param _address:    Address of the program we want to verify.
        :type _address:     int
        """
        locator = (By.ID, "programRow-{0}".format(_address))
        if self.driver.is_visible(_locator=locator):
            # Verify the description we got from the client against our object
            actual_description = self.driver.get_text_from_web_element(_locator=locator)
            expected_description = self.programs[_address].ds
            if expected_description in actual_description:
                print "Successfully verified Program {0}'s description on BaseManager to be '{1}'".format(
                    _address,
                    actual_description
                )
            else:
                e_msg = "Program Tab - Detail View: Description on BaseManager did not match what the test engine " \
                        "thinks it should be:\n\t\tExpected: \t{0}\n\t\tActual: \t{1}".format(
                            expected_description,
                            actual_description
                        )
                raise ValueError(e_msg)
        else:
            e_msg = "Program Tab - Detail View: Unable to find the Description of Program {0} using locator: {1}".format(
                _address,
                locator
            )
            raise NoSuchElementException(e_msg)
