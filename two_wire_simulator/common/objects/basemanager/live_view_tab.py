__author__ = 'baseline'

from common.imports import *
from common.objects.basemanager.locators import *
# import common.objects.basemanager.main_page as main_page
import main_page
from selenium.common.exceptions import ElementNotVisibleException


class LiveViewTabPage(main_page.MainPage):
    """
    LiveView Tab Object \n
    """
    def __init__(self, web_driver):
        super(LiveViewTabPage, self).__init__(web_driver=web_driver)

    def verify_open(self):
        """
        Verifies that the Live View Tab opens successfully by verifying text at the top of the page. \n
        """
        # LiveView panel selector
        locator = (By.ID, "remote_content")
        try:
            live_view_tab = self.driver.web_driver.find_element(*locator)

            if not live_view_tab.is_displayed():
                raise ElementNotVisibleException("Unable to locate Live View Panel.")

        except Exception as ve:
            caught_text = ve.message
            e_msg = "Unable to verify LiveView tab is open. Exception received: {0}".format(
                caught_text
            )
            raise Exception(e_msg)
        else:
            print "Successfully verified LiveView Tab opened"