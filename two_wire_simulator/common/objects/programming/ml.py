from common.imports import opcodes
from common.objects.base_classes.flow import BaseFlow
from common.objects.messages.programming.mainline_messages import MainlineMessages
from common.objects.statuses.programming.mainline_statuses import MainlineStatuses
from common.imports.types import ActionCommands, MainlineCommands
from decimal import Decimal
import warnings
__author__ = 'baseline'


class Mainline(BaseFlow):
    """
    Mainline Object. \n
    """
    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    # Browser Instance
    browser = ''

    # Controller type for controller specific methods across all devices
    controller_type = ''

    #################################
    def __init__(self, _controller, _ad):
        """
        Mainline constructor. Accepts no arguments to create a default mainline with default values. \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 \n

        :param _ad:     Mainline address (number) \n
        :type _ad:      int \n
        """
        # Give access to the controller
        self.controller = _controller

        self.zones = dict()    # Zone objects
        """:type: dict[int, common.objects.device.zn.Zone]"""

        # create message reference to object
        self.messages = MainlineMessages(_mainline_object=self)  # Mainline messages
        """:type: common.objects.messages.programming.mainline_messages.MainlineMessages"""

        # Init parent class to inherit respective attributes
        BaseFlow.__init__(self, _controller=_controller, _ad=_ad, _identifiers=[[opcodes.mainline, _ad]],
                          _ty=opcodes.mainline)

        # The address of the mainline on a 3200. This is value is stored so that if we un-assign this mainline from a
        # FlowStation, it will get it's old 3200 address back.
        self.saved_3200_address = _ad
        self.flow_station_slot = _ad

        # Pipe fill time
        self.ft = 2

        # Limit zones by flow
        self.lc = opcodes.false

        # Share with Flow Station
        self.sh = opcodes.false

        # Standard variance shutdown enabled
        self.fw = opcodes.false

        # Standard variance limit
        self.fv = 0

        # Use pressure for stable flow
        self.up = opcodes.false

        # Use time for stable flow
        self.ut = opcodes.true

        # Mainline variance limit
        self.mf = 0

        # Mainline variance shutdown enabled
        self.mw = opcodes.false

        # Delay first zone
        self.tm = 0

        # Delay for next zone
        self.tz = 0

        # Delay after zone
        self.tl = 0

        # Delay Units (PS = Pressure, TM = Time)
        self.un = opcodes.time

        # Number of zones to delay
        self.zc = 0

        # Use advanced flow
        self.af = opcodes.false

        # High variance limit (tier 1)
        self.ah = 0

        # Low variance limit (tier 1)
        self.al = 0

        # High variance limit (tier 2)
        self.bh = 0

        # Low variance limit (tier 2)
        self.bl = 0

        # High variance limit (tier 3)
        self.ch = 0

        # Low variance limit (tier 3)
        self.cl = 0

        # High variance limit (tier 4)
        self.dh = 0

        # Low variance limit (tier 4)
        self.dl = 0

        # Zone high variance detection & shutdown
        self.zh = opcodes.false

        # Zone low variance detection & shutdown
        self.zl = opcodes.false

        # Dynamic Flow Allocation
        self.fp = opcodes.false

        # Mainline to Mainline assignment (FlowStation only)
        self.ml = []

        # POC to Mainline assignment (FlowStation only)
        self.poc = []

        # Priority (FlowStation only)
        self.pr = 1

        # messages
        self.build_message_string = ''  # place to store the message that is compared to the controller message

        # create status reference to object
        self.statuses = MainlineStatuses(_mainline_object=self)  # Water Source Statuses
        """:type: common.objects.statuses.programming.mainline_statuses.MainlineStatuses"""

    #################################
    def add_zone_to_mainline(self, _zone_address):
        """
        Assigns the zone addressed at _zone_address to this Main Line. \n
        :param _zone_address:     Address of zone to assign to this main line \n
        :type _zone_address:      int \n
        """

        if not isinstance(_zone_address, int):
            e_msg = "Exception occurred trying to add zone to mainline: '{1}' the zone address '{0}' must " \
                    "be an int".format(
                        str(_zone_address),             # {0}
                        str(self.identifiers[0][1])     # {1}
                    )
            raise ValueError(e_msg)
        else:
            # make sure this zone exists in the controller before making an assignment

            if _zone_address not in self.controller.zones.keys():
                e_msg = "Attempting to assign main line {0} to zone {1}. The zone does not exist on this " \
                        "controller.".format(
                            self.identifiers[0][1],     # {0}
                            str(_zone_address)          # {1}
                        )
                raise ValueError(e_msg)

            self.controller.zones[_zone_address].ml = self.identifiers[0][1]
            self.zones[_zone_address] = self.controller.zones[_zone_address]

            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,             # {0}
                opcodes.zone,                   # {1}
                str(_zone_address),             # {2}
                opcodes.mainline,               # {3}
                str(self.identifiers[0][1]))    # {4}

            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception as e:
                e_msg = "Exception occurred trying to add zone {0} to mainline: {1} -> {2}".format(
                    str(_zone_address),             # {0}
                    str(self.identifiers[0][1]),    # {1}
                    e.message                       # {3}
                )
                raise Exception(e_msg)
            else:
                print("Successfully added zone: {0} to mainline: {1}".format(
                    str(_zone_address),             # {0}
                    str(self.identifiers[0][1])     # {1}
                ))

    #################################
    def remove_zone_from_mainline(self, _zone_address):
        """
        Assigns the zone addressed at _zone_address to this Main Line. \n
        :param _zone_address:     Address of zone to assign to this main line \n
        :type _zone_address:      int \n
        """

        if not isinstance(_zone_address, int):
            e_msg = "Exception occurred trying to add zone to mainline: '{1}' the zone address '{0}' must " \
                    "be an int".format(
                        str(_zone_address),             # {0}
                        str(self.identifiers[0][1])     # {1}
                    )
            raise ValueError(e_msg)
        else:
            # make sure this zone exists in the controller before making an assignment

            if _zone_address not in self.controller.zones.keys():
                e_msg = "Attempting to assign main line {0} to zone {1}. The zone does not exist on this " \
                        "controller.".format(
                            self.identifiers[0][1],     # {0}
                            str(_zone_address)          # {1}
                        )
                raise ValueError(e_msg)

            self.controller.zones[_zone_address].ml = 0
            self.zones[_zone_address] = self.controller.zones[_zone_address]
            # removing a zone from the mainline. A value of 0 for a mainline removes the zone from a mainline.
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,             # {0}
                opcodes.zone,                   # {1}
                str(_zone_address),             # {2}
                opcodes.mainline,               # {3}
                str(0))                         # {4}

            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception as e:
                e_msg = "Exception occurred trying to add zone {0} to mainline: {1} -> {2}".format(
                    str(_zone_address),             # {0}
                    str(self.identifiers[0][1]),    # {1}
                    e.message                       # {3}
                )
                raise Exception(e_msg)
            else:
                print("Successfully added zone: {0} to mainline: {1}".format(
                    str(_zone_address),             # {0}
                    str(self.identifiers[0][1])     # {1}
                ))
    # TODO this should only be for the flowstaion
    #################################
    def add_mainline_to_mainline(self, _mainline_address):
        """
        Assign a Mainline to a mainline
        :param _mainline_address:            address of the mainline \n
        :type _mainline_address:             int \n
        :return:
        :rtype:
        """
        # The mainline must be managed by a FlowStation
        if self.called_by_controller_type != opcodes.flow_station:
            e_msg = "Trying to set a mainline to mainline connection on a controller that is not a FlowStation"
            raise Exception(e_msg)

        # Assign mainline to object attribute
        if not isinstance(_mainline_address, int):
            e_msg = "Exception occurred trying to assign mainline {0} to mainline: '{1}' the value must " \
                    "be an int".format(
                        str(_mainline_address),             # {0}
                        str(self.identifiers[0][1])         # {1}
                    )
            raise Exception(e_msg)

        # Check if there is a mainline with the passed in address already on the FlowStation
        if _mainline_address not in self.flowstation.mainlines.keys():
            e_msg = "Attempting to assign mainline {0} to mainline {1}. The mainline does not exist on this " \
                    "FlowStation.".format(
                        _mainline_address,         # {0}
                        self.identifiers[0][1]     # {1}
                    )
            raise ValueError(e_msg)

        # Set the Flow Station and Controller Mainline Address for this Mainline
        self.ml.append(_mainline_address)
        if self.called_by_controller_type == opcodes.flow_station:
            self.downstream_pipes.append(self.flowstation.get_mainline(_mainline_address))
        else:
            self.downstream_pipes.append(self.controller.get_mainline(_mainline_address))

        # Convert all assigned mainlines to a string separated by '=' signs
        all_mainlines_string = '='.join([str(mainline) for mainline in self.ml])
        
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                                 # {0}
            MainlineCommands.Mainline,                          # {1}
            str(self.identifiers[0][1]),                        # {2}
            MainlineCommands.Attributes.MAINLINE,               # {3}
            all_mainlines_string                                # {4}
        )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to assign mainline {0} to mainline {1} -> {2}".format(
                str(_mainline_address),         # {0}
                str(self.identifiers[0][1]),    # {1}
                e.message                       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully assigned mainline {0} to mainline {1}".format(
                str(_mainline_address),     # {0}
                self.identifiers[0][1])     # {1}
            )

    #################################
    def add_poc_to_mainline(self, _poc_address):
        """
        Assign a point of connection to a mainline
        :param _poc_address:            address of the poc to add to this mainline \n
        :type _poc_address:             int \n
        :return:
        :rtype:
        """
        # The mainline must be managed by a FlowStation
        if self.called_by_controller_type != opcodes.flow_station:
            e_msg = "Trying to set a POC to mainline connection on a controller that is not a FlowStation"
            raise Exception(e_msg)

        # Assign mainline to object attribute
        if not isinstance(_poc_address, int):
            e_msg = "Exception occurred trying to assign POC {0} to mainline: '{1}' the value must " \
                    "be an int".format(
                        str(_poc_address),             # {0}
                        str(self.identifiers[0][1])    # {1}
                    )
            raise Exception(e_msg)

        # Set the Flow Station and Controller Mainline Address for this Mainline
        self.poc.append(_poc_address)

        # Convert all assigned mainlines to a string separated by '=' signs
        all_pocs_string = '='.join([str(poc) for poc in self.poc])

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                                 # {0}
            MainlineCommands.Mainline,                          # {1}
            str(self.identifiers[0][1]),                        # {2}
            MainlineCommands.Attributes.POINT_OF_CONTROL,       # {3}
            all_pocs_string                                     # {4}
        )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to assign poc {0} to mainline {1} -> {2}".format(
                str(_poc_address),         # {0}
                str(self.identifiers[0][1]),    # {1}
                e.message                       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully assigned poc {0} to mainline {1}".format(
                str(_poc_address),     # {0}
                self.identifiers[0][1])     # {1}
            )

    #################################
    def convert_fill_time_minutes_to_seconds(self, fill_time):
        """
        Converts the fill time from minutes to total seconds.
        :param fill_time:   Fill time in minutes to convert to seconds. \n
        :type fill_time:    int \n
        """
        return fill_time * 60

    #################################
    def send_programming(self):
        """
        Sends the current mainline instance programming to the controller. \n
        """
        command = self.build_obj_configuration_for_send()
        try:
            # Attempt to set program default values at the controller in faux io
            self.ser.send_and_wait_for_reply(command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Default Values' to: '{1}' -> {2}".format(
                    str(self.identifiers[0][1]),    # {0}
                    command,                        # {1}
                    e.message                       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Default Values' to: {1}".format(
                str(self.identifiers[0][1]),        # {0}
                command,                            # {1}
            ))

    #################################
    def build_obj_configuration_for_send(self):
        """
        Builds the 'SET' string for sending this Main Line Instance to the Controller. This method ensures
        that attributes with blank values don't get sent to the controller. This helps avoid 'BC' response's from
        controller. \n
        :return:        Formatted 'Set' string for the Main Line \n
        :rtype:         str \n
        """
        # fill time is expected in seconds
        converted_fill_time = self.convert_fill_time_minutes_to_seconds(fill_time=self.ft)

        # Starting string, always will need this, 'SET,PC=Blah'
        current_config = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14},{15}={16},{17}={18}," \
                         "{19}={20}".format(
                            ActionCommands.SET,                 # {0}
                            opcodes.mainline,                   # {1}
                            str(self.identifiers[0][1]),        # {2}
                            opcodes.description,                # {3}
                            self.ds,                            # {4}
                            opcodes.enabled,                    # {5}
                            self.en,                            # {6}
                            opcodes.target_flow,                # {7}
                            str(self.fl),                       # {8}
                            opcodes.limit_zones_by_flow,        # {9}
                            str(self.lc),                       # {10}
                            opcodes.share_with_flowstation,     # {11}
                            self.sh,                            # {12}
                            opcodes.standard_variance_shutdown,  # {13}
                            str(self.fw),                       # {14}
                            opcodes.standard_variance_limit,    # {15}
                            str(self.fv),                       # {16}
                            opcodes.pipe_fill_units,            # {17}
                            str(self.un),                       # {18}
                            opcodes.fill_time,                  # {19}
                            str(converted_fill_time)            # {20}
                            )

        return current_config

    #################################
    def set_priority(self, _priority_for_mainline):
        """
        Set priority of mainline (this is only relevant for a flowstation-managed mainline)

        :param _priority_for_mainline:  priority of mainline range 1- 10 \n
        :type _priority_for_mainline:   int \n

        :return:
        """
        if self.called_by_controller_type != opcodes.flow_station:
            e_msg = "Exception occurred trying to assign a priority {0} to mainline: '{1}' the controller type must " \
                    "be a flowstation".format(
                        str(_priority_for_mainline),    # {0}
                        str(self.identifiers[0][1])     # {1}
                    )
            raise Exception(e_msg)

        if not isinstance(_priority_for_mainline, int):
            e_msg = "Exception occurred trying to assign a priority {0} to mainline: '{1}' the value must " \
                    "be an int".format(
                        str(_priority_for_mainline),    # {0}
                        str(self.identifiers[0][1])     # {1}
                    )
            raise Exception(e_msg)

        if _priority_for_mainline not in range(1, 11):
            e_msg = "Exception occurred trying to assign a priority {0} to mainline: '{1}' the value must " \
                    "be between 1 and 10 ".format(
                        str(_priority_for_mainline),    # {0}
                        str(self.identifiers[0][1])     # {1}
                    )
            raise Exception(e_msg)

        self.pr = _priority_for_mainline
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                      # {0}
            MainlineCommands.Mainline,               # {1}
            str(self.identifiers[0][1]),             # {2}
            MainlineCommands.Attributes.PRIORITY,    # {3}
            self.pr                                  # {4}
        )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to assign mainline {0}'s priority to:" \
                    " {1} -> {2}".format(
                        str(self.identifiers[0][1]),   # {0}
                        self.pr,                       # {1}
                        e.message                      # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully assigned priority {0} to mainline {0}".format(
                str(self.pr),
                self.identifiers[0][1])
            )

    #################################
    def set_limit_zones_by_flow_to_true(self):
        """
        Sets the 'Limit Zones By Flow' for this Mainline to true. \n
        """
        self.lc = opcodes.true
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,             # {0}
            opcodes.mainline,               # {1}
            str(self.identifiers[0][1]),    # {2}
            opcodes.limit_zones_by_flow,    # {3}
            str(self.lc)                    # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Limit Zones By Flow' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.lc),                   # {1}
                e.message                       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Limit Zones By Flow' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.lc),                   # {1}
            ))

    #################################
    def set_limit_zones_by_flow_to_false(self):
        """
        Sets the 'Limit Zones By Flow' for this Mainline to false. \n
        """
        self.lc = opcodes.false
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,             # {0}
            opcodes.mainline,               # {1}
            str(self.identifiers[0][1]),    # {2}
            opcodes.limit_zones_by_flow,    # {3}
            str(self.lc)                    # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Limit Zones By Flow' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.lc),                   # {1}
                e.message                       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Limit Zones By Flow' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.lc),                   # {1}
            ))

    #################################
    def set_pipe_stabilization_time(self, _minutes):
        """
        Sets the 'Pipe Fill Time' for this Mainline. \n

        :param _minutes:    New 'Pipe Fill Time' for Mainline (minutes) \n
        :type _minutes:     int \n
        """
        self.set_pipe_fill_units_to_time()
        # self.ft is stored in minuted but the controller takes the information in seconds so a convertion is made
        # convert fill time from minutes to seconds for sending to controller it needs seconds
        # also now fill time is stored in seconds
        self.ft = _minutes
        converted_ft_to_seconds = self.convert_fill_time_minutes_to_seconds(fill_time=self.ft)
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,             # {0}
            opcodes.mainline,               # {1}
            str(self.identifiers[0][1]),    # {2}
            opcodes.fill_time,              # {3}
            str(converted_ft_to_seconds)    # {4}
        )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Pipe Fill Time' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),    # {0}
                str(_minutes),                  # {1}
                e.message                       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Pipe Fill Time' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(_minutes),                  # {1}
            ))

    #################################
    def set_pipe_stabilization_pressure(self, _pressure):
        """
        Sets the 'Pipe Fill Time' for this Mainline. \n

        :param _pressure:    New 'Pipe Fill time' for Mainline (pressure) \n
        :type _pressure:     int \n
        """
        self.set_pipe_fill_units_to_pressure()
        # Assign new Pipe Fill Pressure to object attribute
        self.ft = _pressure

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,             # {0}
            opcodes.mainline,               # {1}
            str(self.identifiers[0][1]),    # {2}
            opcodes.fill_time,              # {3}
            self.ft                         # {4}
        )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Pipe Fill Pressure' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.ft),                   # {1}
                e.message                       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Pipe Fill Pressure' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.ft),                   # {1}
            ))

    #################################
    def set_pipe_fill_units_to_time(self):
        """
        Sets the 'Pipe Fill Units' for this Mainline to time. \n
        """
        self.set_use_time_for_stable_flow_to_true()

    #################################
    def set_use_time_for_stable_flow_to_true(self):
        """
        Sets the 'Use Time for Stable Flow' for this Mainline to true. \n
        """
        # Only one of these values can be true, once one is set to TR on the controller, it will set the other to FA
        self.ut = opcodes.true
        self.up = opcodes.false

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),            # {2}
            opcodes.use_time_for_stable_flow,       # {3}
            str(self.ut)                            # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Use Time For Stable Flow' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(self.ut),                   # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Use Time For Stable Flow' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.ut),                   # {1}
            ))

    #################################
    def set_use_time_for_stable_flow_to_false(self):
        """
        Sets the 'Use Time for Stable Flow' for this Mainline to false. \n
        """
        # One of these has to be true, once one is set to FA on the controller, it will set the other to TR
        self.ut = opcodes.false
        self.up = opcodes.true

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),            # {2}
            opcodes.use_time_for_stable_flow,       # {3}
            str(self.ut)                            # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Use Time For Stable Flow' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(self.ut),                   # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Use Time For Stable Flow' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.ut),                   # {1}
            ))

    #################################
    def set_use_pressure_for_stable_flow_to_true(self):
        """
        Sets the 'Use Pressure for Stable Flow' for this Mainline to true. Setting to use pressure for stable flow will
        cause controller to set using time for stabilization to false.
        """
        # Only one of these values can be true, once one is set to TR on the controller, it will set the other to FA
        self.up = opcodes.true
        self.ut = opcodes.false

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),            # {2}
            opcodes.use_pressure_for_stable_flow,   # {3}
            str(self.up)                            # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Use Pressure For Stable Flow' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(self.up),                   # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Use Pressure For Stable Flow' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.up),                   # {1}
            ))

    #################################
    def set_use_pressure_for_stable_flow_to_false(self):
        """
        Sets the 'Use Pressure for Stable Flow' for this Mainline to false. \n
        """
        # One of these has to be true, once one is set to FA on the controller, it will set the other to TR
        self.up = opcodes.false
        self.ut = opcodes.true

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),            # {2}
            opcodes.use_pressure_for_stable_flow,   # {3}
            str(self.up)                            # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Use Pressure For Stable Flow' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(self.up),                   # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Use Pressure For Stable Flow' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.up),                   # {1}
            ))

    #################################
    def set_pipe_fill_units_to_pressure(self):
        """
        Sets the 'Pipe Fill Units' for this Mainline to pressure. \n
        """
        self.set_use_pressure_for_stable_flow_to_true()

    #################################
    def set_time_delay_before_first_zone(self, _minutes):
        """
        Sets the 'Delay First Zone' for this Mainline . \n

        :param _minutes:    New 'Delay First Zone' state for Mainline \n
        :type _minutes:     int \n
        """
        # Validate argument type
        self.set_delay_units_to_time()
        if not isinstance(_minutes, int):
            e_msg = "Invalid 'Delay First Zone' state entered for Mainline {0}. Expects " \
                    "int. Received type: {1}".format(
                        str(self.identifiers[0][1]),    # {0}
                        type(_minutes)                  # {1}
                    )
            raise ValueError(e_msg)

        else:
            self.tm = _minutes

            # Controller requires this in seconds, so convert from mins to secs
            mins_converted_to_secs = self.tm * 60

            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                     # {0}
                opcodes.mainline,                       # {1}
                str(self.identifiers[0][1]),            # {2}
                opcodes.delay_first_zone,               # {3}
                mins_converted_to_secs                  # {4}
            )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Delay First Zone' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(self.tm),                   # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Delay First Zone' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.tm),                   # {1}
            ))

    #################################
    def set_pressure_delay_before_first_zone(self, _psi):
        """
        Sets the 'Pressure Delay First Zone' for this Mainline . \n

        :param _psi:    New 'Delay First Zone' state for Mainline \n
        :type _psi:     float \n
        """
        self.set_delay_units_to_pressure()
        # Validate argument type
        if isinstance(_psi, int):
            _psi = float(_psi)
        if not isinstance(_psi, float):
            e_msg = "Invalid 'Delay First Zone' state entered for Mainline {0}. Expects " \
                    "float. Received type: {1}".format(
                        str(self.identifiers[0][1]),    # {0}
                        type(_psi)                      # {1}
                    )
            raise ValueError(e_msg)

        else:
            # round to the second decimal
            self.tm = round(_psi, 2)

            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,             # {0}
                opcodes.mainline,               # {1}
                str(self.identifiers[0][1]),    # {2}
                opcodes.delay_first_zone,       # {3}
                self.tm                         # {4}
            )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Delay First Zone' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(self.tm),                   # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Delay First Zone' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.tm),                   # {1}
            ))

    #################################
    def set_time_delay_between_zone(self, _minutes):
        """
        Sets the 'Delay For Next Zone' for this Mainline on the controller. \n

        :param _minutes:    New 'Delay For Next Zone' state for Mainline \n
        :type _minutes:     int \n
        """
        self.set_delay_units_to_time()
        # Validate argument type
        if not isinstance(_minutes, int):
            e_msg = "Invalid 'Delay For Next Zone' state entered for Mainline {0}. Expects " \
                    "int. Received type: {1}".format(
                        str(self.identifiers[0][1]),    # {0}
                        type(_minutes)                  # {1}
                    )
            raise ValueError(e_msg)

            # Assign new Delay For Next Zone to object attribute
        else:
            self.tz = _minutes

            # Controller requires this in seconds, so convert from mins to secs
            mins_converted_to_secs = self.tz * 60

            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                     # {0}
                opcodes.mainline,                       # {1}
                str(self.identifiers[0][1]),            # {2}
                opcodes.delay_for_next_zone,            # {3}
                mins_converted_to_secs                  # {4}
            )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Delay For Next Zone' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(self.tz),                   # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Delay For Next Zone' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.tz),                   # {1}
            ))

    #################################
    def set_pressure_delay_between_zone(self, _psi):
        """
        Sets the 'Delay For Next Zone' for this Mainline on the controller. \n

        :param _psi:    New 'Delay For Next Zone' state for Mainline \n
        :type _psi:     float \n
        """
        self.set_delay_units_to_pressure()
        if isinstance(_psi, int):
            _psi = float(_psi)
        # Validate argument type
        if not isinstance(_psi, float):
            e_msg = "Invalid 'Delay For Next Zone' state entered for Mainline {0}. Expects " \
                    "int. Received type: {1}".format(
                        str(self.identifiers[0][1]),    # {0}
                        type(_psi)                      # {1}
                    )
            raise ValueError(e_msg)

            # Assign new Delay For Next Zone to object attribute
        else:
            # round to the second decimal
            self.tz = round(_psi, 2)

            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,             # {0}
                opcodes.mainline,               # {1}
                str(self.identifiers[0][1]),    # {2}
                opcodes.delay_for_next_zone,    # {3}
                self.tz                         # {4}
            )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Delay For Next Zone' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(self.tz),                   # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Delay For Next Zone' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.tz),                   # {1}
            ))

    #################################
    def set_time_delay_after_zone(self, _minutes):
        """
        Sets the 'Delay After Zone' for this Mainline on the controller. \n

        :param _minutes:    New 'Delay After Zone' state for Mainline \n
        :type _minutes:     int \n
        """
        self.set_delay_units_to_time()
        # Validate argument type
        if not isinstance(_minutes, int):
            e_msg = "Invalid 'Delay After Zone' state entered for Mainline {0}. Expects " \
                    "int. Received type: {1}".format(
                        str(self.identifiers[0][1]),    # {0}
                        type(_minutes)                  # {1}
                    )
            raise ValueError(e_msg)

            # Assign new Delay After Zone to object attribute
        else:
            self.tl = _minutes

            # Controller requires this in seconds, so convert from mins to secs
            mins_converted_to_secs = self.tl * 60

            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                     # {0}
                opcodes.mainline,                       # {1}
                str(self.identifiers[0][1]),            # {2}
                opcodes.delay_after_zone,               # {3}
                mins_converted_to_secs                  # {4}
            )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Delay After Zone' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(self.tl),                   # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Delay After Zone' to: {1}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.tl),   # {1}
            ))

    #################################
    def set_pressure_delay_after_zone(self, _psi):
        """
        Sets the 'Delay After Zone' for this Mainline on the controller. \n

        :param _psi:    New 'Delay After Zone' state for Mainline \n
        :type _psi:     float \n
        """
        self.set_delay_units_to_pressure()
        if isinstance(_psi, int):
            _psi = float(_psi)
        # Validate argument type
        if not isinstance(_psi, float):
            e_msg = "Invalid 'Delay After Zone' state entered for Mainline {0}. Expects " \
                    "int. Received type: {1}".format(
                        str(self.identifiers[0][1]),    # {0}
                        type(_psi)                      # {1}
                    )
            raise ValueError(e_msg)

            # Assign new Delay After Zone to object attribute
        else:
            # round to the second decimal
            self.tl = round(_psi, 2)

            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,             # {0}
                opcodes.mainline,               # {1}
                str(self.identifiers[0][1]),    # {2}
                opcodes.delay_after_zone,       # {3}
                self.tl                         # {4}
            )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Delay After Zone' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(self.tl),                   # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Delay After Zone' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.tl),                   # {1}
            ))

    #################################
    def set_number_zones_to_delay(self, _new_number):
        """
        Sets the 'Number Zones to Delay' for this Mainline on the controller. \n

        :param _new_number:    New 'Number Zones to Delay' state for Mainline \n
        :type _new_number:     int \n
        """
        # Validate argument type
        if not isinstance(_new_number, int):
            e_msg = "Invalid 'Number Zones to Delay' state entered for Mainline {0}. Expects " \
                    "int. Received type: {1}".format(
                        str(self.identifiers[0][1]),    # {0}
                        type(_new_number)               # {1}
                    )
            raise ValueError(e_msg)

            # Assign new Number Zones to Delay to object attribute
        else:
            self.zc = _new_number
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                     # {0}
                opcodes.mainline,                       # {1}
                str(self.identifiers[0][1]),            # {2}
                opcodes.number_of_zones_to_delay,       # {3}
                str(self.zc)                            # {4}
            )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Number Zones to Delay' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(self.zc),                   # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Number Zones to Delay' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.zc),                   # {1}
            ))

    #################################
    def set_delay_units_to_time(self):
        """
        Sets the 'Delay Units' for this Mainline to time. \n
        """
        self.un = opcodes.time
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),            # {2}
            opcodes.pipe_fill_units,                # {3}
            str(self.un)                            # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Delay Units' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(self.un),                   # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Delay Units' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.un),                   # {1}
            ))

    #################################
    def set_delay_units_to_pressure(self):
        """
        Sets the 'Delay Units' for this Mainline to pressure. \n
        """
        self.un = opcodes.pressure

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),            # {2}
            opcodes.pipe_fill_units,                # {3}
            str(self.un)                            # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Delay Units' to: '{1}'" \
                        " -> {2}".format(
                            str(self.identifiers[0][1]),    # {0}
                            str(self.un),                   # {1}
                            e.message                       # {2}
                        )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Delay Units' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.un),                   # {1}
            ))

    #################################
    def set_dynamic_flow_allocation_to_true(self):
        """
        Sets the 'Dynamic Flow Allocation' for this Mainline to 'TR' (True). \n
        """
        self.fp = opcodes.true

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),            # {2}
            opcodes.use_dynamic_flow_allocation,    # {3}
            str(self.fp)                            # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Dynamic Flow Allocation' to: '{1}'" \
                        " -> {2}".format(
                            str(self.identifiers[0][1]),    # {0}
                            str(self.fp),                   # {1}
                            e.message                       # {2}
                        )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Dynamic Flow Allocation' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.fp),                   # {1}
            ))

    #################################
    def set_dynamic_flow_allocation_to_false(self):
        """
        Sets the 'Dynamic Flow Allocation' for this Mainline to 'FA' (False). \n
        """
        self.fp = opcodes.false

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),            # {2}
            opcodes.use_dynamic_flow_allocation,    # {3}
            str(self.fp)                            # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Dynamic Flow Allocation' to: '{1}'" \
                        " -> {2}".format(
                            str(self.identifiers[0][1]),    # {0}
                            str(self.fp),                   # {1}
                            e.message                       # {2}
                        )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Dynamic Flow Allocation' to: {1}".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.fp),                   # {1}
            ))

    # ----------------------------------------------------------------------------------------------------------------#
    # ---------------------------------        Standard Flow Variance Methods        ---------------------------------#
    # ----------------------------------------------------------------------------------------------------------------#

    #################################
    def set_standard_variance_shutdown_enabled_to_true(self):
        """
        Sets the 'Standard Variance Shutdown Enabled' for this Mainline to true. \n
        """

        self.fw = opcodes.true

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,  # {0}
            opcodes.mainline,  # {1}
            str(self.identifiers[0][1]),  # {2}
            opcodes.standard_variance_shutdown,  # {3}
            str(self.fw)  # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Standard Variance Shutdown Enabled' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),  # {0}
                        str(self.fw),  # {1}
                        e.message  # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Standard Variance Shutdown Enabled' to: {1}".format(
                str(self.identifiers[0][1]),  # {0}
                str(self.fw),  # {1}
            ))

    #################################
    def set_standard_variance_shutdown_enabled_to_false(self):
        """
        Sets the 'Standard Variance Shutdown Enabled' for this Mainline to false. \n
        """

        self.fw = opcodes.false

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,  # {0}
            opcodes.mainline,  # {1}
            str(self.identifiers[0][1]),  # {2}
            opcodes.standard_variance_shutdown,  # {3}
            str(self.fw)  # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Standard Variance Shutdown Enabled' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.fw),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Standard Variance Shutdown Enabled' to: {1}".format(
                str(self.identifiers[0][1]),  # {0}
                str(self.fw),  # {1}
            ))

    #################################
    def set_standard_variance_limit_with_shutdown(self, _percentage):
        """
        Sets the 'Standard Variance Limit' for this Mainline. \n

        :param _percentage:    New 'Standard Variance Limit' state for Mainline \n
        :type _percentage:     float \n
        """
        # if advanced flow is enabled than disable it
        if self.is_advanced_flow_enabled():
            self.set_use_advanced_flow_to_false()

        # Validate argument type
        self.set_standard_variance_shutdown_enabled_to_true()

        if isinstance(_percentage, int):
            _percentage = float(_percentage)
        if not isinstance(_percentage, float):
            e_msg = "Invalid 'Standard Variance Limit' state entered for Mainline {0}. Expects " \
                    "float or int. Received type: {1}".format(
                        str(self.identifiers[0][1]),       # {0}
                        type(_percentage)   # {1}
                    )
            raise ValueError(e_msg)

        # Assign new Standard Variance Shutdown Enabled to object attribute
        else:
            self.fv = _percentage
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                 # {0}
                opcodes.mainline,                   # {1}
                str(self.identifiers[0][1]),                       # {2}
                opcodes.standard_variance_limit,    # {3}
                str(self.fv)                        # {4}
            )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Standard Variance Limit' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.fv),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Standard Variance Limit' to: {1}".format(
                str(self.identifiers[0][1]),  # {0}
                str(self.fv),  # {1}
            ))

            # send shut down command
            self.set_standard_variance_shutdown_enabled_to_true()

    #################################
    def set_standard_variance_limit_without_shutdown(self, _percentage):
        """
        Sets the 'Standard Variance Limit' for this Mainline. \n

        :param _percentage:    New 'Standard Variance Limit' state for Mainline \n
        :type _percentage:     float \n
        """
        # if advanced flow is enabled than disable it
        if self.is_advanced_flow_enabled():
            self.set_use_advanced_flow_to_false()

        # Validate argument type
        self.set_standard_variance_shutdown_enabled_to_true()
        # Validate argument type
        if isinstance(_percentage, int):
            _percentage = float(_percentage)
        if not isinstance(_percentage, float):
            e_msg = "Invalid 'Standard Variance Limit' state entered for Mainline {0}. Expects " \
                    "Int or a float. Received type: {1}".format(
                        str(self.identifiers[0][1]),       # {0}
                        type(_percentage)   # {1}
                    )
            raise ValueError(e_msg)

        # Assign new Standard Variance Shutdown Enabled to object attribute
        else:
            self.fv = _percentage
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                 # {0}
                opcodes.mainline,                   # {1}
                str(self.identifiers[0][1]),                       # {2}
                opcodes.standard_variance_limit,    # {3}
                str(self.fv)                        # {4}
            )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Standard Variance Limit' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.fv),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Standard Variance Limit' to: {1}".format(
                str(self.identifiers[0][1]),  # {0}
                str(self.fv),  # {1}
            ))
            # send shut down command
            self.set_standard_variance_shutdown_enabled_to_false()

    # ----------------------------------------------------------------------------------------------------------------#
    # ---------------------------------        Advance Flow Variance Methods         ---------------------------------#
    # ----------------------------------------------------------------------------------------------------------------#

    #################################
    def is_advanced_flow_enabled(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        Returns whether or not the device has a status of waiting to water.

        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_waiting_to_water():
                /* DO SOMETHING */

        :rtype: bool
        """
        self.get_data_if_necessary(_get_data)
        return self.af == opcodes.true

    #################################
    def set_use_advanced_flow_to_true(self):
        """
        Sets the 'Use Advanced Flow' for this Mainline to true. \n
        """
        self.af = opcodes.true
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),                           # {2}
            opcodes.use_advanced_flow,              # {3}
            str(self.af)                            # {4}
        )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Use Advanced Flow' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.af),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Use Advanced Flow' to: {1}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.af),   # {1}
            ))

    #################################
    def set_use_advanced_flow_to_false(self):
        """
        Sets the 'Use Advanced Flow' attribute for this Mainline to false. \n
        """
        self.af = opcodes.false
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),                           # {2}
            opcodes.use_advanced_flow,              # {3}
            str(self.af)                            # {4}
        )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Use Advanced Flow' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.af),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Use Advanced Flow' to: {1}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.af),   # {1}
            ))

    #################################
    def set_high_flow_variance_tier_one(self, _percent, _with_shutdown_enabled):
        """
        Sets the 'High Variance Limit Tier One and shut down enabled' for this Mainline on the controller. \n

        :param _percent:    New 'High Variance Limit Tier One' for Mainline:  \n
        :type _percent:     float \n

        :param _with_shutdown_enabled:    New 'High variance detected shutdown' for Mainline:  \n
        :type _with_shutdown_enabled:     bool \n
        """
        # if advanced flow is not enabled than enable it
        if not self.is_advanced_flow_enabled():
            self.set_use_advanced_flow_to_true()

        # Validate argument type
        if isinstance(_percent, int):
            _percent = float(_percent)
        if not isinstance(_percent, float):
            e_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),       # {0}
                        type(_percent)      # {1}
                    )
            raise ValueError(e_msg)

        if not isinstance(_with_shutdown_enabled, bool):
            e_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),                  # {0}
                        type(_with_shutdown_enabled)   # {1}
                    )
            raise ValueError(e_msg)

        # Assign new High Variance Limit Tier One to object attribute
        self.ah = _percent

        if _with_shutdown_enabled:
            self.zh = opcodes.true
        else:
            self.zh = opcodes.false

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            opcodes.mainline,                               # {1}
            str(self.identifiers[0][1]),                                   # {2}
            opcodes.high_variance_limit_tier_one,           # {3}
            str(self.ah),                                   # {4}
            opcodes.zone_high_variance_detection_shutdown,  # {5}
            str(self.zh)                                    # {6}
        )
            
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'High Variance Limit Tier One' to: '{1}' and " \
                    "'Zone High Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.ah),   # {1}
                        self.zh,        # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)

        else:
            print("Successfully set Mainline {0}'s 'High Variance Limit Tier One' to: '{1}' and "
                  "'Zone High Variance Shutdown' to '{2}'".format(
                    str(self.identifiers[0][1]),   # {0}
                    str(self.ah),   # {1}
                    self.zh         # {2}
                    )
                  )

    #################################
    def set_low_flow_variance_tier_one(self, _percent, _with_shutdown_enabled):
        """
        Sets the 'Low Variance Limit Tier One and shut down enabled' for this Mainline on the controller. \n

        :param _percent:    New 'Low Variance Limit Tier One' for Mainline:  \n
        :type _percent:     float \n

        :param _with_shutdown_enabled:    New 'High variance detected shutdown' for Mainline:  \n
        :type _with_shutdown_enabled:     bool \n
        """
        # if advanced flow is not enabled than enable it
        if not self.is_advanced_flow_enabled():
            self.set_use_advanced_flow_to_true()

        # Validate argument type
        if isinstance(_percent, int):
            _percent = float(_percent)
        if not isinstance(_percent, float):
            e_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),       # {0}
                        type(_percent)      # {1}
                    )
            raise ValueError(e_msg)

        if _with_shutdown_enabled not in [True, False]:
            e_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),                   # {0}
                        type(_with_shutdown_enabled)    # {1}
                    )
            raise ValueError(e_msg)

        # Assign new Low Variance Limit Tier One to object attribute
        self.al = _percent

        if _with_shutdown_enabled:
            self.zl = opcodes.true
        else:
            self.zl = opcodes.false

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            opcodes.mainline,                               # {1}
            str(self.identifiers[0][1]),                                   # {2}
            opcodes.low_variance_limit_tier_one,            # {3}
            str(self.al),                                   # {4}
            opcodes.zone_low_variance_detection_shutdown,   # {5}
            str(self.zl)                                    # {6}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Low Variance Limit Tier One' to: '{1}' and " \
                    "'Zone Low Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.al),   # {1}
                        str(self.zl),   # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Low Variance Limit Tier One' to: '{1}'"
                  " and 'Zone Low Variance Detection Shutdown' to '{2}'".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.al),   # {1}
                        str(self.zl)    # {2}
                        )
                  )

    #################################
    def set_high_flow_variance_tier_two(self, _percent, _with_shutdown_enabled):
        """
        Sets the 'High Variance Limit Tier Two and shut down enabled' for this Mainline on the controller. \n

        :param _percent:    New 'High Variance Limit Tier Two' for Mainline:  \n
        :type _percent:     float \n

        :param _with_shutdown_enabled:    New 'High variance detected shutdown' for Mainline:  \n
        :type _with_shutdown_enabled:     bool \n
        """
        # if advanced flow is not enabled than enable it
        if not self.is_advanced_flow_enabled():
            self.set_use_advanced_flow_to_true()

        # Validate argument type
        if isinstance(_percent, int):
            _percent = float(_percent)
        if not isinstance(_percent, float):
            e_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),       # {0}
                        type(_percent)      # {1}
                    )
            raise ValueError(e_msg)

        if _with_shutdown_enabled not in [True, False]:
            e_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),                  # {0}
                        type(_with_shutdown_enabled)   # {1}
                    )
            raise ValueError(e_msg)

        # Assign new High Variance Limit Tier Two to object attribute
        self.bh = _percent

        if _with_shutdown_enabled:
            self.zh = opcodes.true
        else:
            self.zh = opcodes.false

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            opcodes.mainline,                               # {1}
            str(self.identifiers[0][1]),                                   # {2}
            opcodes.high_variance_limit_tier_two,           # {3}
            str(self.bh),                                   # {4}
            opcodes.zone_high_variance_detection_shutdown,  # {5}
            str(self.zh)                                    # {6}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'High Variance Limit Tier Two' to: '{1}' and " \
                    "'Zone High Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.bh),   # {1}
                        self.zh,        # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)

        else:
            print("Successfully set Mainline {0}'s 'High Variance Limit Tier Two' to: '{1}' and "
                  "'Zone High Variance Detection Shutdown' to '{2}'".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.bh),   # {1}
                        self.zh         # {2}
                    )
                  )

    #################################
    def set_low_flow_variance_tier_two(self, _percent, _with_shutdown_enabled):
        """
        Sets the 'Low Variance Limit Tier Two and shut down enabled' for this Mainline on the controller. \n

        :param _percent:    New 'Low Variance Limit Tier Two' for Mainline:  \n
        :type _percent:     float \n

        :param _with_shutdown_enabled:    New 'High variance detected shutdown' for Mainline:  \n
        :type _with_shutdown_enabled:     bool \n
        """
        # if advanced flow is not enabled than enable it
        if not self.is_advanced_flow_enabled():
            self.set_use_advanced_flow_to_true()

        # Validate argument type
        if isinstance(_percent, int):
            _percent = float(_percent)
        if not isinstance(_percent, float):
            e_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),       # {0}
                        type(_percent)      # {1}
                    )
            raise ValueError(e_msg)

        if _with_shutdown_enabled not in [True, False]:
            e_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),                   # {0}
                        type(_with_shutdown_enabled)    # {1}
                    )
            raise ValueError(e_msg)

        # Assign new Low Variance Limit Tier Two to object attribute
        self.bl = _percent

        if _with_shutdown_enabled:
            self.zl = opcodes.true
        else:
            self.zl = opcodes.false

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            opcodes.mainline,                               # {1}
            str(self.identifiers[0][1]),                                   # {2}
            opcodes.low_variance_limit_tier_two,            # {3}
            str(self.bl),                                   # {4}
            opcodes.zone_low_variance_detection_shutdown,   # {5}
            str(self.zl)                                    # {6}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Low Variance Limit Tier Two' to: '{1}' and " \
                    "'Zone Low Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.bl),   # {1}
                        str(self.zl),   # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Low Variance Limit Tier Two' to: '{1}'"
                  " and 'Zone Low Variance Detection Shutdown' to '{2}'".format(
                    str(self.identifiers[0][1]),   # {0}
                    str(self.bl),   # {1}
                    str(self.zl)    # {2}
                    )
                  )

    #################################
    def set_high_flow_variance_tier_three(self, _percent, _with_shutdown_enabled):
        """
        Sets the 'High Variance Limit Tier Three and shut down enabled' for this Mainline on the controller. \n

        :param _percent:    New 'High Variance Limit Tier Three' for Mainline:  \n
        :type _percent:     float \n

        :param _with_shutdown_enabled:    New 'High variance detected shutdown' for Mainline:  \n
        :type _with_shutdown_enabled:     bool \n
        """
        # if advanced flow is not enabled than enable it
        if not self.is_advanced_flow_enabled():
            self.set_use_advanced_flow_to_true()

        # Validate argument type
        if isinstance(_percent, int):
            _percent = float(_percent)
        if not isinstance(_percent, float):
            e_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),       # {0}
                        type(_percent)      # {1}
                    )
            raise ValueError(e_msg)

        if _with_shutdown_enabled not in [True, False]:
            e_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),                   # {0}
                        type(_with_shutdown_enabled)    # {1}
                    )
            raise ValueError(e_msg)

        # Assign new High Variance Limit Tier Three to object attribute
        self.ch = _percent

        if _with_shutdown_enabled:
            self.zh = opcodes.true
        else:
            self.zh = opcodes.false

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            opcodes.mainline,                               # {1}
            str(self.identifiers[0][1]),                                   # {2}
            opcodes.high_variance_limit_tier_three,         # {3}
            str(self.ch),                                   # {4}
            opcodes.zone_high_variance_detection_shutdown,  # {5}
            str(self.zh)                                    # {6}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'High Variance Limit Tier Three' to: '{1}' and " \
                    "'Zone High Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.ch),   # {1}
                        self.zh,        # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)

        else:
            print("Successfully set Mainline {0}'s 'High Variance Limit Tier Three' to: '{1}' and "
                  "'Zone High Variance Detection Shutdown' to '{2}'".format(
                    str(self.identifiers[0][1]),   # {0}
                    str(self.ch),   # {1}
                    self.zh         # {2}
                    )
                  )

    #################################
    def set_low_flow_variance_tier_three(self, _percent, _with_shutdown_enabled):
        """
        Sets the 'Low Variance Limit Tier Three and shut down enabled' for this Mainline on the controller. \n

        :param _percent:    New 'Low Variance Limit Tier Three' for Mainline:  \n
        :type _percent:     float \n

        :param _with_shutdown_enabled:    New 'High variance detected shutdown' for Mainline:  \n
        :type _with_shutdown_enabled:     bool \n
        """
        # if advanced flow is not enabled than enable it
        if not self.is_advanced_flow_enabled():
            self.set_use_advanced_flow_to_true()

        # Validate argument type
        if isinstance(_percent, int):
            _percent = float(_percent)
        if not isinstance(_percent, float):
            e_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),       # {0}
                        type(_percent)      # {1}
                    )
            raise ValueError(e_msg)

        if _with_shutdown_enabled not in [True, False]:
            e_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),                   # {0}
                        type(_with_shutdown_enabled)    # {1}
                    )
            raise ValueError(e_msg)

        # Assign new Low Variance Limit Tier Three to object attribute
        self.cl = _percent

        if _with_shutdown_enabled:
            self.zl = opcodes.true
        else:
            self.zl = opcodes.false

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            opcodes.mainline,                               # {1}
            str(self.identifiers[0][1]),                                   # {2}
            opcodes.low_variance_limit_tier_three,          # {3}
            str(self.cl),                                   # {4}
            opcodes.zone_low_variance_detection_shutdown,   # {5}
            str(self.zl)                                    # {6}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Low Variance Limit Tier Three' to: '{1}' and " \
                    "'Zone Low Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.cl),   # {1}
                        str(self.zl),   # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Low Variance Limit Tier Three' to: '{1}'"
                  " and 'Zone Low Variance Detection Shutdown' to '{2}'".format(
                    str(self.identifiers[0][1]),   # {0}
                    str(self.cl),   # {1}
                    str(self.zl)    # {2}
                    )
                  )

    #################################
    def set_high_flow_variance_tier_four(self, _percent, _with_shutdown_enabled):
        """
        Sets the 'High Variance Limit Tier Four and shut down enabled' for this Mainline on the controller. \n

        :param _percent:    New 'High Variance Limit Tier Four' for Mainline:  \n
        :type _percent:     float \n

        :param _with_shutdown_enabled:    New 'High variance detected shutdown' for Mainline:  \n
        :type _with_shutdown_enabled:     bool \n
        """
        # if advanced flow is not enabled than enable it
        if not self.is_advanced_flow_enabled():
            self.set_use_advanced_flow_to_true()

        # Validate argument type
        if isinstance(_percent, int):
            _percent = float(_percent)
        if not isinstance(_percent, float):
            e_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),       # {0}
                        type(_percent)      # {1}
                    )
            raise ValueError(e_msg)

        if _with_shutdown_enabled not in [True, False]:
            e_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),                   # {0}
                        type(_with_shutdown_enabled)    # {1}
                    )
            raise ValueError(e_msg)

        # Assign new High Variance Limit Tier Four to object attribute
        self.dh = _percent

        if _with_shutdown_enabled:
            self.zh = opcodes.true
        else:
            self.zh = opcodes.false

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            opcodes.mainline,                               # {1}
            str(self.identifiers[0][1]),                                   # {2}
            opcodes.high_variance_limit_tier_four,          # {3}
            str(self.dh),                                   # {4}
            opcodes.zone_high_variance_detection_shutdown,  # {5}
            str(self.zh)                                    # {6}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'High Variance Limit Tier Four' to: '{1}' and " \
                    "'Zone High Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.dh),   # {1}
                        self.zh,        # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)

        else:
            print("Successfully set Mainline {0}'s 'High Variance Limit Tier Four' to: '{1}' and "
                  "'Zone High Variance Detection Shutdown' to '{2}'".format(
                    str(self.identifiers[0][1]),   # {0}
                    str(self.dh),   # {1}
                    self.zh         # {2}
                   )
                  )

    #################################
    def set_low_flow_variance_tier_four(self, _percent, _with_shutdown_enabled):
        """
        Sets the 'Low Variance Limit Tier Four and shut down enabled' for this Mainline on the controller. \n

        :param _percent:    New 'Low Variance Limit Tier Four' for Mainline:  \n
        :type _percent:     float \n

        :param _with_shutdown_enabled:    New 'High variance detected shutdown' for Mainline:  \n
        :type _with_shutdown_enabled:     bool \n
        """
        # if advanced flow is not enabled than enable it
        if not self.is_advanced_flow_enabled():
            self.set_use_advanced_flow_to_true()

        # Validate argument type
        if isinstance(_percent, int):
            _percent = float(_percent)
        if not isinstance(_percent, float):
            e_msg = "Invalid 'flow limit' entered for Mainline {0}. Expects Float or Int. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),       # {0}
                        type(_percent)      # {1}
                    )
            raise ValueError(e_msg)

        if _with_shutdown_enabled not in [True, False]:
            e_msg = "Invalid 'with shutdown enabled' entered for Mainline {0}. Expects bool. " \
                    "Received: {1}".format(
                        str(self.identifiers[0][1]),                   # {0}
                        type(_with_shutdown_enabled)    # {1}
                    )
            raise ValueError(e_msg)

        # Assign new Low Variance Limit Tier Four to object attribute
        self.dl = _percent

        if _with_shutdown_enabled:
            self.zl = opcodes.true
        else:
            self.zl = opcodes.false

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            opcodes.mainline,                               # {1}
            str(self.identifiers[0][1]),                                   # {2}
            opcodes.low_variance_limit_tier_four,           # {3}
            str(self.dl),                                   # {4}
            opcodes.zone_low_variance_detection_shutdown,   # {5}
            str(self.zl)                                    # {6}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Low Variance Limit Tier Four' to: '{1}' and " \
                    "'Zone Low Variance Detection Shutdown' to: '{2}'. Exception was: '{3}'".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.dl),   # {1}
                        str(self.zl),   # {2}
                        e.message       # {3}
                     )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Low Variance Limit Tier Four' to: {1}"
                  " and 'Zone Low Variance Detection Shutdown' to {2}".format(
                    str(self.identifiers[0][1]),   # {0}
                    str(self.dl),   # {1}
                    str(self.zl)    # {2}
                   )
                  )

    #################################
    def set_mainline_variance_limit(self, _new_limit):
        """
        Sets the 'Mainline Variance Limit' for this Mainline. \n

        :param _new_limit:    New 'Mainline Variance Limit' state for Mainline \n
        :type _new_limit:     float \n
        """
        warnings.warn('This attribute is not currently being used in the 3200 but '
                      'may be needed in the flowstation l', DeprecationWarning)
        # Validate argument type
        if not isinstance(_new_limit, float):
            e_msg = "Invalid 'Mainline Variance Limit' state entered for Mainline {0}. Expects " \
                    "float. Received type: {1}".format(
                        str(self.identifiers[0][1]),       # {0}
                        type(_new_limit)    # {1}
                    )
            raise ValueError(e_msg)

        # Assign new Mainline Variance Limit Enabled to object attribute
        else:
            self.mf = _new_limit
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                 # {0}
                opcodes.mainline,                   # {1}
                str(self.identifiers[0][1]),                       # {2}
                opcodes.mainline_variance_limit,    # {3}
                str(self.mf)                        # {4}
            )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Mainline Variance Limit' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.mf),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Mainline Variance Limit' to: {1}".format(
                str(self.identifiers[0][1]),  # {0}
                str(self.mf),  # {1}
            ))

    #################################
    def set_mainline_variance_shutdown_enabled_to_true(self):
        """
        Sets the 'Mainline Variance Shutdown Enabled' for this Mainline to true. \n
        """
        warnings.warn('This attribute is not currently being used in the 3200 but '
                      'may be needed in the flowstation l', DeprecationWarning)
        self.mw = opcodes.true

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),                           # {2}
            opcodes.mainline_variance_shutdown,     # {3}
            str(self.mw)                            # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Mainline Variance Shutdown Enabled' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.mw),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Mainline Variance Shutdown Enabled' to: {1}".format(
                str(self.identifiers[0][1]),  # {0}
                str(self.mw),  # {1}
            ))

    #################################
    def set_mainline_variance_shutdown_enabled_to_false(self):
        """
        Sets the 'Mainline Variance Shutdown Enabled' for this Mainline to false. \n
        """
        warnings.warn('This attribute is not currently being used in the 3200 but '
                      'may be needed in the flowstation l', DeprecationWarning)
        self.mw = opcodes.false

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),                           # {2}
            opcodes.mainline_variance_shutdown,     # {3}
            str(self.mw)                            # {4}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Mainline Variance Shutdown Enabled' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.mw),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Mainline Variance Shutdown Enabled' to: {1}".format(
                str(self.identifiers[0][1]),  # {0}
                str(self.mw),  # {1}
            ))

    #################################
    def set_share_with_flow_station_to_true(self):
        """
        Sets the 'Share With Flow Station' for this Mainline on the controller to true. \n
        """
        self.sh = opcodes.true

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),                           # {2}
            opcodes.share_with_flowstation,         # {3}
            str(self.sh)                            # {4}
        )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Share With Flow Station' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.sh),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Share With Flow Station' to: {1}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.sh),   # {1}
            ))

    #################################
    def set_share_with_flow_station_to_false(self):
        """
        Sets the 'Share With Flow Station' for this Mainline on the controller to false. \n
        """
        # Set the mainline's address to the old address it had on the controller
        self.identifiers[0][1] = self.saved_3200_address
        self.ad = self.saved_3200_address
        self.sh = opcodes.false

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            opcodes.mainline,                       # {1}
            str(self.identifiers[0][1]),                           # {2}
            opcodes.share_with_flowstation,         # {3}
            str(self.sh)                            # {4}
        )
        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Mainline {0}'s 'Share With Flow Station' to: '{1}'" \
                    " -> {2}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.sh),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Mainline {0}'s 'Share With Flow Station' to: {1}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.sh),   # {1}
            ))

    #################################
    def verify_limit_zones_by_flow_state(self):
        """
        Verifies the 'Limit Zones By Flow State' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Limit Zones By Flow State' value from the controller reply.
        -   Lastly, compares the instance 'Limit Zones By Flow State' value with the controller's programming. 
        """

        limit_zones_by_flow_state_from_controller = self.data.get_value_string_by_key(opcodes.limit_zones_by_flow)

        # Compare status versus what is on the controller
        if self.lc != limit_zones_by_flow_state_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'Limit Zones By Flow State' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(limit_zones_by_flow_state_from_controller),     # {1}
                        str(self.lc)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Limit Zones By Flow State' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.lc)    # {1}
            ))

    #################################
    def verify_advanced_flow_variance_setting(self):
        """
        Verifies that advanced high flow variance is being used. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'High Variance enabled from the controller reply.
        -   Lastly, compares the instance 'High variance' value with the controller's programming.
        """

        advanced_variance_setting = self.data.get_value_string_by_key(opcodes.use_advanced_flow)

        # Compare status versus what is on the controller
        if self.af != advanced_variance_setting:
            e_msg = "Unable to verify Mainline {0}'s 'Advanced Variance enabled state' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                       # {0}
                        str(advanced_variance_setting),     # {1}
                        str(self.af)                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Advanced Variance Enabled State: '{1}' on controller".format(
                str(self.identifiers[0][1]),  # {0}
                str(self.af)   # {1}
            ))

    #################################
    def verify_high_variance_limit_tier_one(self):
        """
        Verifies the 'High Variance Limit Tier One' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'High Variance Limit Tier One' value from the controller reply.
        -   Lastly, compares the instance 'High Variance Limit Tier One' value with the controller's programming.
        """

        high_variance_limit_from_controller = float(self.data.get_value_string_by_key(
            opcodes.high_variance_limit_tier_one))

        # Compare status versus what is on the controller
        if self.ah != high_variance_limit_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'High Variance Limit Tier One' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                 # {0}
                        str(high_variance_limit_from_controller),     # {1}
                        str(self.ah)                                  # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'High Variance Limit Tier One' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.ah)    # {1}
            ))

    #################################
    def verify_high_variance_limit_tier_two(self):
        """
        Verifies the 'High Variance Limit Tier Two' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'High Variance Limit Tier Two' value from the controller reply.
        -   Lastly, compares the instance 'High Variance Limit Tier Two' value with the controller's programming.
        """

        high_variance_limit_from_controller = float(self.data.get_value_string_by_key(
            opcodes.high_variance_limit_tier_two))

        # Compare status versus what is on the controller
        if self.bh != high_variance_limit_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'High Variance Limit Tier Two' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                 # {0}
                        str(high_variance_limit_from_controller),     # {1}
                        str(self.bh)                                  # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'High Variance Limit Tier Two' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.bh)    # {1}
            ))

    #################################
    def verify_high_variance_limit_tier_three(self):
        """
        Verifies the 'High Variance Limit Tier Three' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'High Variance Limit Tier Three' value from the controller reply.
        -   Lastly, compares the instance 'High Variance Limit Tier Three' value with the controller's programming.
        """

        high_variance_limit_from_controller = float(self.data.get_value_string_by_key(
            opcodes.high_variance_limit_tier_three))

        # Compare status versus what is on the controller
        if self.ch != high_variance_limit_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'High Variance Limit Tier Three' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                 # {0}
                        str(high_variance_limit_from_controller),     # {1}
                        str(self.ch)                                  # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'High Variance Limit Tier Three' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.ch)    # {1}
            ))

    #################################
    def verify_high_variance_limit_tier_four(self):
        """
        Verifies the 'High Variance Limit Tier Four' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'High Variance Limit Tier Four' value from the controller reply.
        -   Lastly, compares the instance 'High Variance Limit Tier Four' value with the controller's programming.
        """

        high_variance_limit_from_controller = float(self.data.get_value_string_by_key(
            opcodes.high_variance_limit_tier_four))

        # Compare status versus what is on the controller
        if self.dh != high_variance_limit_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'High Variance Limit Tier Four' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                 # {0}
                        str(high_variance_limit_from_controller),     # {1}
                        str(self.dh)                                  # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'High Variance Limit Tier Four' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.dh)    # {1}
            ))

    #################################
    def verify_standard_low_variance_detection_shutdown(self):
        """
        Verifies the 'Low Variance Detection' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Low Variance Detection' value from the controller reply.
        -   Lastly, compares the instance 'Low Variance Detection' value with the controller's programming.
        """

        low_variance_shutdown = self.data.get_value_string_by_key(opcodes.zone_low_variance_detection_shutdown)

        # Compare status versus what is on the controller
        if self.zl != low_variance_shutdown:
            e_msg = "Unable to verify Mainline {0}'s 'Low Variance Detection Shutdown' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                   # {0}
                        str(low_variance_shutdown),     # {1}
                        str(self.zl)                    # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Low Variance Detection Shutdown' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),  # {0}
                str(self.zl)   # {1}
            ))

    #################################
    def verify_standard_high_variance_detection_shutdown(self):
        """
        Verifies the 'Zone High Variance Shutdown' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'High Variance Detection' value from the controller reply.
        -   Lastly, compares the instance 'High Variance Shutdown' value with the controller's programming. 
        """

        high_variance_detection = self.data.get_value_string_by_key(opcodes.zone_high_variance_detection_shutdown)

        # Compare status versus what is on the controller
        if self.zh != high_variance_detection:
            e_msg = "Unable to verify Mainline {0}'s 'High Variance Detection Shutdown' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                   # {0}
                        str(high_variance_detection),                   # {1}
                        str(self.zh)                                    # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'High Variance Shutdown' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.zh)    # {1}
            ))
                                                   
    #################################
    def verify_low_variance_limit_tier_one(self):
        """
        Verifies the 'Low Variance Limit Tier One' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Low Variance Limit Tier One' value from the controller reply.
        -   Lastly, compares the instance 'Low Variance Limit Tier One' value with the controller's programming.
        """

        low_variance_limit_from_controller = float(self.data.get_value_string_by_key(
            opcodes.low_variance_limit_tier_one))

        # Compare status versus what is on the controller
        if self.al != low_variance_limit_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'Low Variance Limit Tier One' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                 # {0}
                        str(low_variance_limit_from_controller),      # {1}
                        str(self.al)                                  # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Low Variance Limit Tier One' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.al)    # {1}
            ))

    #################################
    def verify_low_variance_limit_tier_two(self):
        """
        Verifies the 'Low Variance Limit Tier Two' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Low Variance Limit Tier Two' value from the controller reply.
        -   Lastly, compares the instance 'LLow Variance Limit Tier Two' value with the controller's programming.
        """

        low_variance_limit_from_controller = float(self.data.get_value_string_by_key(
            opcodes.low_variance_limit_tier_two))

        # Compare status versus what is on the controller
        if self.bl != low_variance_limit_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'Low Variance Limit Tier Two' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                 # {0}
                        str(low_variance_limit_from_controller),      # {1}
                        str(self.bl)                                  # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Low Variance Limit Tier Two' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.bl)    # {1}
            ))

    #################################
    def verify_low_variance_limit_tier_three(self):
        """
        Verifies the 'Low Variance Limit Tier Three' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Low Variance Limit Tier Three' value from the controller reply.
        -   Lastly, compares the instance 'Low Variance Limit Tier Three' value with the controller's programming.
        """

        low_variance_limit_from_controller = float(self.data.get_value_string_by_key(
            opcodes.low_variance_limit_tier_three))

        # Compare status versus what is on the controller
        if self.cl != low_variance_limit_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'Low Variance Limit Tier Three' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                 # {0}
                        str(low_variance_limit_from_controller),      # {1}
                        str(self.cl)                                  # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Low Variance Limit Tier Three' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.cl)    # {1}
            ))

    #################################
    def verify_low_variance_limit_tier_four(self):
        """
        Verifies the 'Low Variance Limit Tier Four' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Low Variance Limit Tier Four' value from the controller reply.
        -   Lastly, compares the instance 'Low Variance Limit Tier Four' value with the controller's programming.
        """

        low_variance_limit_from_controller = float(self.data.get_value_string_by_key(
            opcodes.low_variance_limit_tier_four))

        # Compare status versus what is on the controller
        if self.dl != low_variance_limit_from_controller:
            e_msg = "Unable to verify Mainline {0}'s 'Low Variance Limit Tier Four' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                 # {0}
                        str(low_variance_limit_from_controller),      # {1}
                        str(self.dl)                                  # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Low Variance Limit Tier Four' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.dl)    # {1}
            ))

    def verify_priority(self):
        """
        Verifies the 'Priority' for a mainline on the controller. \n
        """
        if self.controller.controller_type != opcodes.flow_station:
            e_msg = "Exception occurred trying to verify a priority on Mainline {0} on something that isn't a " \
                    "FlowStation.".format(
                        str(self.identifiers[0][1])     # {0}
                    )
            raise Exception(e_msg)

        # expect int type
        priority = int(self.data.get_value_string_by_key(opcodes.priority))

        # Compare priority
        if self.pr != priority:
            e_msg = "Unable verify Mainline {0}'s 'Priority'." \
                    " Received: {1} for Priority, Expected: {2}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(priority),  # {1}
                        self.pr         # {2}
                    )
            raise ValueError(e_msg)

        else:
            print("Verified Mainline {0}'s 'Priority': '{1}' on controller ".format(
                str(self.identifiers[0][1]),   # {0}
                self.pr         # {1}
            ))

    #################################
    def verify_pipe_fill_time(self):
        """
        Verifies the 'Pipe Fill Time' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Pipe Fill Time' value from the controller reply.
        -   Lastly, compares the instance 'Pipe Fill Time' value with the controller's programming.
        """
        pipe_fill_value_to_compare = ''
        if self.ut == opcodes.true:
            pipe_fill_value_in_seconds = int(self.data.get_value_string_by_key(opcodes.fill_time))
            pipe_fill_value_in_minutes = pipe_fill_value_in_seconds/60
            pipe_fill_value_to_compare = pipe_fill_value_in_minutes
        elif self.up == opcodes.true:
            pipe_fill_value_in_pressure = int(self.data.get_value_string_by_key(opcodes.fill_time))
            pipe_fill_value_to_compare = pipe_fill_value_in_pressure

        # Compare status versus what is on the controller
        if self.ft != pipe_fill_value_to_compare:
            e_msg = "Unable to verify Mainline {0}'s 'Pipe Fill Time' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                       # {0}
                        str(pipe_fill_value_to_compare),    # {1}
                        str(self.ft)                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Pipe Fill Time' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.ft)    # {1}
            ))

    #################################
    def verify_share_with_flowstation(self):
        """
        Verifies the 'Share With Flow Station' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Share With Flow Station' value from the controller reply.
        -   Lastly, compares the instance 'Share With Flow Station' value with the controller's programming.
        """

        share_with_flow_station = self.data.get_value_string_by_key(opcodes.share_with_flowstation)
        
        if share_with_flow_station is not None:
            # Compare status versus what is on the controller
            if self.sh != share_with_flow_station:
                e_msg = "Unable to verify Mainline {0}'s 'Share With Flow Station' value. Received: {1}, " \
                        "Expected: {2}".format(
                            str(self.identifiers[0][1]),                   # {0}
                            str(share_with_flow_station),   # {1}
                            str(self.sh)                    # {2}
                        )
                raise ValueError(e_msg)
            else:
                print("Verified Mainline {0}'s 'Share With Flow Station' value: '{1}' on controller".format(
                    str(self.identifiers[0][1]),   # {0}
                    str(self.sh)    # {1}
                ))

    #################################
    def verify_standard_variance_shutdown_enabled(self):
        """
        Verifies the 'Standard Variance Shutdown Enabled' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Standard Variance Shutdown Enabled' value from the controller reply.
        -   Lastly, compares the instance 'Standard Variance Shutdown Enabled' value with the controller's programming.
        """

        standard_variance_shutdown = self.data.get_value_string_by_key(opcodes.standard_variance_shutdown)

        # Compare status versus what is on the controller
        if self.fw != standard_variance_shutdown:
            e_msg = "Unable to verify Mainline {0}'s 'Standard Variance Shutdown Enabled' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                       # {0}
                        str(standard_variance_shutdown),    # {1}
                        str(self.fw)                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Standard Variance Shutdown Enabled' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.fw)    # {1}
            ))

    #################################
    def verify_standard_variance_limit(self):
        """
        Verifies the 'Standard Variance Limit' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Standard Variance Limit' value from the controller reply.
        -   Lastly, compares the instance 'Standard Variance Limit' value with the controller's programming.
        """

        standard_variance_limit = Decimal(self.data.get_value_string_by_key(opcodes.standard_variance_limit))

        # Compare status versus what is on the controller
        if self.fv != standard_variance_limit:
            e_msg = "Unable to verify Mainline {0}'s 'Standard Variance Limit' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                       # {0}
                        str(standard_variance_limit),       # {1}
                        str(self.fv)                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Standard Variance Limit' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.fv)    # {1}
            ))

    #################################
    def verify_mainline_variance_limit(self):
        """
        Verifies the 'Mainline Variance Limit' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Mainline Variance Limit' value from the controller reply.
        -   Lastly, compares the instance 'Mainline Variance Limit' value with the controller's programming.
        """
        warnings.warn('This attribute is not currently being used in the 3200 but '
                      'may be needed in the flowstation l', DeprecationWarning)

        mainline_variance_limit = Decimal(self.data.get_value_string_by_key(opcodes.mainline_variance_limit))

        # Compare status versus what is on the controller
        if self.mf != mainline_variance_limit:
            e_msg = "Unable to verify Mainline {0}'s 'Mainline Variance Limit' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                       # {0}
                        str(mainline_variance_limit),       # {1}
                        str(self.mf)                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Mainline Variance Limit' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.mf)    # {1}
            ))

    #################################
    def verify_mainline_variance_shutdown_enabled(self):
        """
        Verifies the 'Mainline Variance Shutdown Enabled' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Mainline Variance Shutdown Enabled' value from the controller reply.
        -   Lastly, compares the instance 'Mainline Variance Shutdown Enabled' value with the controller's programming.
        """
        warnings.warn('This attribute is not currently being used in the 3200 but '
                      'may be needed in the flowstation l', DeprecationWarning)

        mainline_variance_shutdown = self.data.get_value_string_by_key(opcodes.mainline_variance_shutdown)

        # Compare status versus what is on the controller
        if self.mw != mainline_variance_shutdown:
            e_msg = "Unable to verify Mainline {0}'s 'Mainline Variance Shutdown Enabled' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                       # {0}
                        str(mainline_variance_shutdown),    # {1}
                        str(self.mw)                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Mainline Variance Shutdown Enabled' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.mw)    # {1}
            ))

    #################################
    def verify_delay_units(self):
        """
        Verifies the 'Pipe Fill Units' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Pipe Fill Units' value from the controller reply.
        -   Lastly, compares the instance 'Pipe Fill Units' value with the controller's programming.
        """

        mainline_pipe_fill_units = self.data.get_value_string_by_key(opcodes.pipe_fill_units)

        # Compare status versus what is on the controller
        if self.un != mainline_pipe_fill_units:
            e_msg = "Unable to verify Mainline {0}'s 'Pipe Fill Units' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                       # {0}
                        str(mainline_pipe_fill_units),      # {1}
                        str(self.un)                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Pipe Fill Units' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.un)    # {1}
            ))

    #################################
    def verify_delay_first_zone(self):
        """
        Verifies the 'Delay First Zone' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Delay First Zone' value from the controller reply.
        -   Lastly, compares the instance 'Delay First Zone' value with the controller's programming.
        """

        delay_first_zone = float(self.data.get_value_string_by_key(opcodes.delay_first_zone))

        if self.un == opcodes.time:
            # Controller stores this as seconds, need to convert to minutes for verifying
            delay_first_zone = int(delay_first_zone) / 60

        # Compare status versus what is on the controller
        if self.tm != delay_first_zone:
            e_msg = "Unable to verify Mainline {0}'s 'Delay First Zone' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                       # {0}
                        str(delay_first_zone),   # {1}
                        str(self.tm)                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Delay First Zone' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.tm)    # {1}
            ))

    #################################
    def verify_delay_for_next_zone(self):
        """
        Verifies the 'Delay For Next Zone' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Delay For Next Zone' value from the controller reply.
        -   Lastly, compares the instance 'Delay For Next Zone' value with the controller's programming.
        """

        delay_for_next_zone = int(self.data.get_value_string_by_key(opcodes.delay_for_next_zone))

        if self.un == opcodes.time:
            # Controller stores this as seconds, need to convert to minutes for verifying
            delay_for_next_zone = int(delay_for_next_zone) / 60

        # Compare status versus what is on the controller
        if self.tz != delay_for_next_zone:
            e_msg = "Unable to verify Mainline {0}'s 'Delay For Next Zone' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                           # {0}
                        str(delay_for_next_zone),               # {1}
                        str(self.tz)                            # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Delay For Next Zone' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.tz)    # {1}
            ))

    #################################
    def verify_delay_after_zone(self):
        """
        Verifies the 'Delay After Zone' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Delay After Zone' value from the controller reply.
        -   Lastly, compares the instance 'Delay After Zone' value with the controller's programming.
        """

        delay_after_zone = int(self.data.get_value_string_by_key(opcodes.delay_after_zone))

        if self.un == opcodes.time:
            # Controller stores this as seconds, need to convert to minutes for verifying
            delay_after_zone = int(delay_after_zone) / 60

        # Compare status versus what is on the controller
        if self.tl != delay_after_zone:
            e_msg = "Unable to verify Mainline {0}'s 'Delay After Zone' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                       # {0}
                        str(delay_after_zone),              # {1}
                        str(self.tl)                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Delay After Zone' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.tl)    # {1}
            ))

    #################################
    def verify_number_of_zones_to_delay(self):
        """
        Verifies the 'Number of Zones to Delay' value for the Mainline on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Number of Zones to Delay' value from the controller reply.
        -   Lastly, compares the instance 'Number of Zones to Delay' value with the controller's programming.
        """

        number_of_zones_to_delay = int(self.data.get_value_string_by_key(opcodes.number_of_zones_to_delay))

        # Compare status versus what is on the controller
        if self.zc != number_of_zones_to_delay:
            e_msg = "Unable to verify Mainline {0}'s 'Number of Zones to Delay' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                       # {0}
                        str(number_of_zones_to_delay),      # {1}
                        str(self.zc)                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Number of Zones to Delay' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.zc)    # {1}
            ))

    #################################
    def verify_dynamic_flow_allocation(self):
        """
        Verifies the 'Dynamic Flow Allocation' value on this mainline. \n
        """

        dynamic_flow_enabled = str(self.data.get_value_string_by_key(opcodes.use_dynamic_flow_allocation))

        # Compare status versus what is on the controller
        if self.fp != dynamic_flow_enabled:
            e_msg = "Unable to verify Mainline {0}'s 'Dynamic Flow Allocation' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(dynamic_flow_enabled),      # {1}
                        str(self.fp)                    # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Dynamic Flow Allocation' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.fp)                    # {1}
            ))

    #################################
    def verify_use_pressure_for_stable_flow(self):
        """
        Verifies the 'Use Pressure For Stable Flow' value on this mainline. \n
        """
        use_pressure_for_stable_flow = str(self.data.get_value_string_by_key(opcodes.use_pressure_for_stable_flow))

        # Compare status versus what is on the controller
        if self.up != use_pressure_for_stable_flow:
            e_msg = "Unable to verify Mainline {0}'s 'Use Pressure For Stable Flow' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),        # {0}
                        str(use_pressure_for_stable_flow),  # {1}
                        str(self.up)                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Use Pressure For Stable Flow' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.up)                    # {1}
            ))

    #################################
    def verify_use_time_for_stable_flow(self):
        """
        Verifies the 'Use Time For Stable Flow' value on this mainline. \n
        """
        use_time_for_stable_flow = str(self.data.get_value_string_by_key(opcodes.use_time_for_stable_flow))

        # Compare status versus what is on the controller
        if self.ut != use_time_for_stable_flow:
            e_msg = "Unable to verify Mainline {0}'s 'Use Time For Stable Flow' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(use_time_for_stable_flow),  # {1}
                        str(self.ut)                    # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Use Time For Stable Flow' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.ut)                    # {1}
            ))

    #################################
    def verify_mainline_outputs(self):
        """
        Verifies the 'Mainline outputs' value on this mainline. \n
        """
        # Get the response from the controller and split it on '='. (EX: string 'ML=1=2=3' becomes list ['1', '2', '3']
        mainline_outputs = self.data.get_value_string_by_key(opcodes.mainline)

        if mainline_outputs:
            # Turn the string 'ML=1=2=3' into a list, then convert every string in the list to an int
            mainline_outputs = str(mainline_outputs).split('=')
            mainline_outputs = [int(mainline_address) for mainline_address in mainline_outputs]
        else:
            # If there were no mainline outputs, we get a None, so convert to an empty list instead
            mainline_outputs = []

        # Compare status versus what is on the controller
        # Use the python built in 'set' method to compare because it doesn't care about order (EX: [1, 2, 3] = [3, 2, 1]
        if set(self.ml) != set(mainline_outputs):
            e_msg = "Unable to verify Mainline {0}'s 'Mainline Outputs' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(mainline_outputs),          # {1}
                        str(self.ml)                    # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Mainline outputs' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.ml)                    # {1}
            ))

    #################################
    def verify_point_of_control_outputs(self):
        """
        Verifies the 'Point of Control outputs' value on this mainline. \n
        """
        # Get the response from the controller and split it on '='. (EX: string 'CP=1=2=3' becomes list ['1', '2', '3']
        point_of_control_outputs = self.data.get_value_string_by_key(opcodes.point_of_connection)

        if point_of_control_outputs:
            # Turn the string 'PC=1=2=3' into a list, then convert every string in the list to an int
            point_of_control_outputs = str(point_of_control_outputs).split('=')
            point_of_control_outputs = [int(poc_address) for poc_address in point_of_control_outputs]
        else:
            # If there were no poc outputs, we get a None, so convert to an empty list instead
            point_of_control_outputs = []

        # Compare status versus what is on the controller
        # Use the python built in 'set' method to compare because it doesn't care about order (EX: [1, 2, 3] = [3, 2, 1]
        if set(self.poc) != set(point_of_control_outputs):
            e_msg = "Unable to verify Mainline {0}'s 'Point of Control Outputs' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),    # {0}
                        str(point_of_control_outputs),  # {1}
                        str(self.poc)                   # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Mainline {0}'s 'Point of Control Outputs' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),    # {0}
                str(self.poc)                   # {1}
            ))

    #################################
    def verify_who_i_am(self):
        """
        Verifier wrapper which verifies all attributes for this 'Mainline'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()
        
        self.verify_enabled_state()
        self.verify_description()
        self.verify_pipe_fill_time()
        self.verify_target_flow()
        self.verify_limit_zones_by_flow_state()
        self.verify_share_with_flowstation()
        self.verify_standard_variance_shutdown_enabled()
        self.verify_use_pressure_for_stable_flow()
        self.verify_use_time_for_stable_flow()
        self.verify_standard_variance_limit()
        self.verify_delay_units()

        self.verify_delay_first_zone()
        self.verify_delay_for_next_zone()
        self.verify_delay_after_zone()
        self.verify_number_of_zones_to_delay()

        self.verify_advanced_flow_variance_setting()

        self.verify_high_variance_limit_tier_one()
        self.verify_low_variance_limit_tier_one()
        self.verify_high_variance_limit_tier_two()
        self.verify_low_variance_limit_tier_two()
        self.verify_high_variance_limit_tier_three()
        self.verify_low_variance_limit_tier_three()
        self.verify_high_variance_limit_tier_four()
        self.verify_low_variance_limit_tier_four()
        self.verify_standard_low_variance_detection_shutdown()
        self.verify_standard_high_variance_detection_shutdown()
        if self.controller.controller_type == opcodes.basestation_3200:
            self.verify_dynamic_flow_allocation()
        if self.controller.controller_type == opcodes.flow_station:
            self.verify_priority()
            self.verify_mainline_outputs()
            self.verify_point_of_control_outputs()
