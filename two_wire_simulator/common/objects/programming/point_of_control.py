from common.imports import opcodes
from common.objects.programming.ml import Mainline as mainline_objects
from common.objects.devices.fm import FlowMeter
from common.objects.base_classes.flow import BaseFlow
from common.imports.types import ActionCommands, PointofControlCommands
from common.objects.messages.programming.point_of_control_messages import PointOfControlMessages
from common.objects.statuses.programming.point_of_control_statuses import PointOfControlStatuses
import warnings
from decimal import *
__author__ = 'baseline'


########################################################################################################################
# 3200 Point of Control Class
########################################################################################################################
class PointOfControl(BaseFlow):
    """
    3200 Point of Control Object \n
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    mainline_objects = None

    #################################

    def __init__(self,
                 _controller,
                 _ad
                 ):
        """
        3200 Point of Control Constructor. \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 \n

        :param _ad:     Address (Point of Control number) \n
        :type _ad:      int \n
        """
        # Init parent class to inherit respective attributes
        BaseFlow.__init__(self, _controller=_controller, _ad=_ad, _identifiers=[[opcodes.point_of_control, _ad]],
                          _ty=opcodes.point_of_control)

        # create message reference to object
        self.messages = PointOfControlMessages(_point_of_control_object=self)  # point of control messages
        """:type: common.objects.messages.programming.point_of_control_messages.PointOfControlMessages"""
        # create status reference to object

        # The address of the point of control on a 3200. This is value is stored so that if we un-assign this point of
        # control from a FlowStation, it will get it's old 3200 address back.
        self.saved_3200_address = _ad

        # Target Flow
        self.fl = 0

        # High Flow Limit
        self.hf = 0

        # Shutdown on high flow
        self.hs = opcodes.false

        # Unscheduled flow limit
        self.uf = 0

        # Shutdown on unscheduled
        self.us = opcodes.false

        # Flow meter address
        self.fm = ''

        # Master valve address
        self.mv = ''

        # Pump decoder address
        self.pp = ''

        # mainline number
        self.ml = 0

        # Flow Station Mainline
        self.flow_station_mainline = 0

        # Controller Mainline
        self.controller_mainline = 0

        # POC group
        self.gr = 0

        # 3200 POC Specific Attributes
        if self.controller.controller_type == "32":

            self.sh = opcodes.false

            self.ps = ''

            self.hp = 0

            self.he = opcodes.false

            self.lp = 0

            self.le = opcodes.false

        # initialize V12 attributes
        if self.controller.vr.startswith('12'):

            # monthly water budget
            self.wb = 0

            # shutdown on over budget
            self.ws = opcodes.false

            # water rationing enable
            self.wr = opcodes.false

            # moisture sensor empty sn
            self.ms = ''

            # moisture empty limit
            self.me = 0

            # moisture sensor empty enable
            self.mn = opcodes.false

            # empty wait time
            self.ew = 0

            # event switch empty serial number
            self.sw = ''

            # switch empty condition
            self.se = opcodes.closed

            # switch empty enable
            self.sn = opcodes.false

        self.statuses = PointOfControlStatuses(_point_of_control_object=self)  # Water Source Statuses
        """:type: common.objects.statuses.programming.point_of_control_statuses.PointOfControlStatuses"""
    #################################
    def send_programming(self):
        """
        Sends the current water source instance programming to the controller. \n
        """
        command = self.build_obj_configuration_for_send()
        # "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13},{14}={15},{16}={17}".format(
        #     opcodes.water_source,               # {0}
        #     str(self.ad),                       # {1}
        #     opcodes.description,                # {2}
        #     str(self.ds),                       # {3}
        #     opcodes.target_flow,                # {4}
        #     str(self.fl),                       # {5}
        #     opcodes.high_flow_limit,            # {6}
        #     str(self.hf),                       # {7}
        #     opcodes.shutdown_on_high_flow,      # {8}
        #     str(self.hs),                       # {9}
        #     opcodes.unscheduled_flow_limit,     # {10}
        #     str(self.uf),                       # {11}
        #     opcodes.shutdown_on_unscheduled,    # {12}
        #     str(self.us),                       # {13}
        #     opcodes.flow_meter,                 # {14}
        #     str(self.fm),                       # {15}
        #     opcodes.master_valve,               # {16}
        #     str(self.mv),                       # {17}
        #     opcodes.pump,                       # {16}
        #     str(self.pp)                        # {17}
        # )
        #
        # if self.controller.controller_type == "32":
        #     command += ",{0}={1},{2}={3},{4}={5},{6}={7}".format(
        #         # TODO find out if this is a bug, 3200 returns a bc when we try to set FS attribute
        #         # opcodes.flow_station,               # {0}
        #         # str(self.sh),                       # {1}
        #         opcodes.high_pressure_limit,        # {2}
        #         str(self.hp),                       # {3}
        #         opcodes.high_pressure_shutdown,     # {4}
        #         str(self.he),                       # {5}
        #         opcodes.low_pressure_limit,         # {6}
        #         str(self.lp),                       # {7}
        #         opcodes.low_pressure_shutdown,      # {8}
        #         str(self.le),                       # {9}
        #     )
        try:
            # Attempt to set point of control default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Point of Control {0}'s 'Default Values' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                command,        # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Point of Control {0}'s 'Default Values' to: {1}".format(
                str(self.identifiers[0][1]),       # {0}
                command,            # {1}
            ))

    #################################
    def build_obj_configuration_for_send(self):
        """
        Builds the 'SET' string for sending this Point of Control Instance to the Controller. This method ensures
        that attributes with blank values don't get sent to the controller. This helps avoid 'BC' response's from
        controller. \n
        :return:        Formatted 'Set' string for the Point of Control \n
        :rtype:         str \n
        """
        # Starting string, always will need this, 'SET,PC=Blah'

        current_config = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14},{15}={16}".format(
            ActionCommands.SET,                 # {0}
            opcodes.point_of_control,           # {1}
            str(self.identifiers[0][1]),                       # {2}
            opcodes.description,                # {3}
            self.ds,                            # {4}
            opcodes.enabled,                    # {5}
            self.en,                            # {6}
            opcodes.target_flow,                # {7}
            str(self.fl),                       # {8}
            opcodes.high_flow_limit,            # {9}
            str(self.hf),                       # {10}
            opcodes.shutdown_on_high_flow,      # {11}
            self.hs,                            # {12}
            opcodes.unscheduled_flow_limit,     # {13}
            str(self.uf),                       # {14}
            opcodes.shutdown_on_unscheduled,    # {15}
            self.us,                            # {16}
        )

        # 3200 Point of Control attribute list of tuples (each tuple represents the OpCode string and associated
        # value)
        poc_attr_list = [
            (opcodes.flow_meter, self.fm),
            (opcodes.master_valve, self.mv),
            (opcodes.pump, self.pp),
            # (opcodes.priority, self.pr),
        ]
        # add attributes specific to a 3200 controller running any firm ware version
        if self.controller.controller_type == "32":
            poc_attr_list.append((opcodes.mainline, self.ml))
            poc_attr_list.append(("PS", self.ps))
            poc_attr_list.append((opcodes.high_pressure_limit, self.hp))
            poc_attr_list.append((opcodes.high_pressure_shutdown, self.he))
            poc_attr_list.append((opcodes.low_pressure_limit, self.lp))
            poc_attr_list.append((opcodes.low_pressure_shutdown, self.le))
        # add attributes specific to a 3200 controller running firm ware version V12
        if self.controller.vr.startswith("12") and self.controller.controller_type == "32":
            poc_attr_list.append((opcodes.monthly_water_budget, self.wb))
            poc_attr_list.append((opcodes.shutdown_on_over_budget, self.ws))
            poc_attr_list.append((opcodes.water_rationing_enable, self.wr))
            poc_attr_list.append((opcodes.moisture_sensor, self.ms))
            poc_attr_list.append((opcodes.moisture_empty_limit, self.me))
            poc_attr_list.append((opcodes.moisture_sensor_empty_enable, self.mn))
            poc_attr_list.append((opcodes.empty_wait_time, self.ew))
            poc_attr_list.append((opcodes.event_switch, self.sw))
            poc_attr_list.append(("SE", self.se))
            poc_attr_list.append((opcodes.switch_empty_enable, self.sn))

        # if self.controller.controller_type == "10":
        #     poc_attr_list = [
        #         (opcodes.flow_meter, self.fm),
        #         (opcodes.master_valve, self.mv),
        #         (opcodes.pump, self.pp)
        #     ]

        # For each attribute, check if the attribute has a value that isn't 'None' or '' (empty string).
        for attr_tuple in poc_attr_list:

            # If the attribute value isn't 'None' or '', append it to the current_config string
            if attr_tuple[1]:
                arg1 = attr_tuple[0]
                arg2 = attr_tuple[1]

                current_config += ",{0}={1}".format(
                    arg1,  # {0}
                    arg2   # {1}
                )

        return current_config

    def set_priority(self, _priority_for_water_source):
        """
        This not an attribute for a point if control it is only used on the water source
        The main method is in Base Flow Class
        """
        warnings.warn('This not an attribute that can be set on a point of control', DeprecationWarning)
        pass

    #################################
    def set_high_flow_limit(self, _limit=None, with_shutdown_enabled=False):
        """
        Sets the 'High Flow Limit' for the Point of Control on the controller. \n

        :param _limit:    New 'High Flow Limit' for Point Of Connection \n
        :type _limit:     int| float \n

        :param with_shutdown_enabled:    Shutdown a point of connection when the limit is hit. \n
        :type with_shutdown_enabled:     bool \n
        """

        if _limit is not None:
            # Assign new 'High Flow Limit' to object attribute
            self.hf = _limit
        else:
            self.hf = 0

        if isinstance(with_shutdown_enabled, bool):
            if with_shutdown_enabled:
                self.hs = opcodes.true
            else:
                self.hs = opcodes.false
        else:
            e_msg = "High Flow Limit' {0} entered for Point of Control {1}. Must be True or False." \
                    "You passed in an incorrect type of: {2}.".format(
                        str(with_shutdown_enabled),  # {0} this is if shutdown will be enabled
                        str(self.identifiers[0][1]),                # {1} point of control address
                        type(with_shutdown_enabled)  # {2} type of passed in value
                    )
            raise ValueError(e_msg)

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
                                                    ActionCommands.SET,                 # {0}
                                                    opcodes.point_of_connection,        # {1}
                                                    str(self.identifiers[0][1]),                       # {2}
                                                    opcodes.high_flow_limit,            # {3}
                                                    str(self.hf),                       # {4}
                                                    opcodes.high_flow_shutdown,         # {5}
                                                    self.hs)                            # {6}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'High Flow Limit' to: '{1}' and 'High Flow Shutdown'" \
                    "to {2} -> {3}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.hf),   # {1}
                        str(self.hs),   # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'High Flow Limit' to: {1} and 'High Flow Shutdown'" 
                  "to {2}".format(
                            str(self.identifiers[0][1]),   # {0}
                            str(self.hf),   # {1}
                            str(self.hs)))  # {2}

    #################################
    def set_unscheduled_flow_limit(self, _gallons=None, with_shutdown_enabled=False):
        """
        Sets the 'Unscheduled Flow Limit' for the Point of Control on the controller. \n

        :param _gallons:    New 'Unscheduled Flow Limit' for Point Of Connection \n
        :type _gallons:     float|int \n

        :param with_shutdown_enabled:    Shutdown a point of connection when the limit is hit. \n
        :type with_shutdown_enabled:     bool \n
        """
        if _gallons is not None:
            # Assign new 'Unscheduled Flow Limit' to object attribute
            self.uf = _gallons
        else:
            self.uf = 0

        if not isinstance(self.uf, float) and not isinstance(self.uf, int):
            raise TypeError('Gallons must be a float or int value')

        elif isinstance(self.uf, int):
            self.uf = float(self.uf)

        if isinstance(with_shutdown_enabled, bool):
            if with_shutdown_enabled:
                self.us = opcodes.true
            else:
                self.us = opcodes.false
        else:
            e_msg = "Invalid 'shutdown enabled' {0} entered for Point of Control {1}. Must be True or False." \
                    "You passed in an incorrect type of: {2}.".format(
                        str(with_shutdown_enabled),  # {0} this is if shutdown will be enabled
                        str(self.identifiers[0][1]),                # {1} point of control address
                        type(with_shutdown_enabled)  # {2} type of passed in value
                    )
            raise ValueError(e_msg)

        command = "{0},{1}={2},{3}={4},{5}={6}".format(ActionCommands.SET,                  # {0}
                                                       opcodes.point_of_connection,         # {1}
                                                       str(self.identifiers[0][1]),                        # {2}
                                                       opcodes.unscheduled_flow_limit,      # {3}
                                                       str(self.uf),                        # {4}
                                                       opcodes.unscheduled_flow_shutdown,   # {5}
                                                       self.us)                             # {6}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Unscheduled Flow Limit' to: '{1}' and" \
                    " 'With Shutdown Enabled' to {2} -> {3}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.uf),   # {1}
                        str(self.us),   # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Unscheduled Flow Limit' to: {1}".format(str(self.identifiers[0][1]), str(self.uf)))

    #################################
    def set_high_pressure_limit(self, _limit=None, with_shutdown_enabled=False):
        """
        Sets the 'High Pressure Limit' for the Point of Control on the controller. \n

        :param _limit:    New 'High Pressure Limit' for Point Of Connection \n
        :type _limit:     int \n

        :param with_shutdown_enabled:    Shutdown a point of connection when the limit is hit. \n
        :type with_shutdown_enabled:     int \n
        """
        if _limit is not None:
            # Assign new 'High Pressure Limit' to object attribute
            if isinstance(_limit, int):
                _limit = float(_limit)
            # Round the float to the hundredths place
            self.hp = round(_limit, 2)
        else:
            self.hp = 0.0

        # Ensure that this value will be sent with a number in the tenths and hundredths place
        self.hp = Decimal(self.hp).quantize(Decimal(".00"))

        if isinstance(with_shutdown_enabled, bool):
            if with_shutdown_enabled:
                self.he = opcodes.true
            else:
                self.he = opcodes.false
        else:
            e_msg = "Invalid 'shutdown enabled' {0} entered for Point of Control {1}. Must be True or False." \
                    "You passed in an incorrect type of: {2}.".format(
                        str(with_shutdown_enabled),  # {0} this is if shutdown will be enabled
                        str(self.identifiers[0][1]),                # {1} point of control address
                        type(with_shutdown_enabled)  # {2} type of passed in value
                    )
            raise ValueError(e_msg)

        command = "{0},{1}={2},{3}={4},{5}={6}".format(ActionCommands.SET,              # {0}
                                                       opcodes.point_of_connection,     # {1}
                                                       str(self.identifiers[0][1]),                    # {2}
                                                       opcodes.high_pressure_limit,     # {3}
                                                       str(self.hp),                    # {4}
                                                       opcodes.high_pressure_shutdown,  # {5}
                                                       self.he)                         # {6}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'High Pressure Limit' to: '{1}' and " \
                    "'With Shutdown Enabled' to {2} -> ".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.hp),   # {1}
                        str(self.he)    # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'High Pressure Limit' to: {1}".format(str(self.identifiers[0][1]), str(self.hp)))

    #################################
    def set_low_pressure_limit(self, _limit=None, with_shutdown_enabled=False):
        """
        Sets the 'Low Pressure Limit' for the Point of Control on the controller. \n

        :param _limit:    New 'Low Pressure Limit' for Point Of Connection \n
        :type _limit:     int \n

        :param with_shutdown_enabled:    Shutdown a point of connection when the limit is hit. \n
        :type with_shutdown_enabled:     int \ns
        """

        if _limit is not None:
            # Assign new 'Low Pressure Limit' to object attribute
            if isinstance(_limit, int):
                _limit = float(_limit)
            # Round the float to the hundredths place
            self.lp = round(_limit, 2)
        else:
            self.lp = 0.0

        # Ensure that this value will be sent with a number in the tenths and hundredths place
        self.lp = Decimal(self.lp).quantize(Decimal(".00"))

        if isinstance(with_shutdown_enabled, bool):
            if with_shutdown_enabled:
                self.le = opcodes.true
            else:
                self.le = opcodes.false
        else:
            e_msg = "Invalid 'shutdown enabled' {0} entered for Point of Control {1}. Must be True or False." \
                    "You passed in an incorrect type of: {2}.".format(
                        str(with_shutdown_enabled),  # {0} this is if shutdown will be enabled
                        str(self.identifiers[0][1]),                # {1} point of control address
                        type(with_shutdown_enabled)  # {2} type of passed in value
                    )
            raise ValueError(e_msg)

        command = "{0},{1}={2},{3}={4},{5}={6}".format(ActionCommands.SET,              # {0}
                                                       opcodes.point_of_connection,     # {1}
                                                       str(self.identifiers[0][1]),                    # {2}
                                                       opcodes.low_pressure_limit,      # {3}
                                                       str(self.lp),                    # {4}
                                                       opcodes.low_pressure_shutdown,   # {5}
                                                       self.le)                         # {6}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Low Pressure Limit' to: '{1}' and " \
                    "'With Shutdown Enabled' to {2} -> ".format(
                        str(self.identifiers[0][1]),     # {0}
                        str(self.lp),     # {1}
                        str(self.le)      # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Low Pressure Limit' to: {1} and 'With Shutdown Enabled' to: "
                  .format(str(self.identifiers[0][1]),
                          str(self.lp),
                          str(self.le)))

    #################################
    def add_flow_meter_to_point_of_control(self, _flow_meter_address):
        """
        Sets the 'Flow meter' for the Point of Control on the controller. \n

        :param _flow_meter_address:    New 'flow meter' for Point Of Connection \n
        :type _flow_meter_address:     int \n
        """
        # Validate Flow meter address
        if _flow_meter_address not in self.controller.flow_meters.keys():
            e_msg = "Invalid 'flow meter address' {0} entered for Point of Control {1}" \
                .format(
                        str(_flow_meter_address),   # {0} this should be the flow meter address
                        str(self.identifiers[0][1])       # {1} point of control address
                    )
            raise ValueError(e_msg)

        # Assign new 'Flow Meter' to object attribute
        else:
            _sn = self.controller.flow_meters[_flow_meter_address].sn
            self.fm = _flow_meter_address
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,
                opcodes.point_of_control,   # {0}
                str(self.identifiers[0][1]),               # {1}
                opcodes.flow_meter,         # {2}
                str(_sn)                    # {3} serial number
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set flow meter {0} on point of control {1}. Exception: {2}".format(
                str(self.fm),   # {0} flow meter address
                str(self.identifiers[0][1]),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Flow Meter' to: {1}".format(
                str(self.identifiers[0][1]),  # {0}
                str(self.fm),  # {1}
            ))

    #################################
    def add_pressure_sensor_to_point_of_control(self, _pressure_sensor_address):
        """
        Sets the 'Pressure Sensor' for the Point of Control on the controller. \n

        :param _pressure_sensor_address:    New 'pressure sensor' for Point Of Connection \n
        :type _pressure_sensor_address:     int \n
        """
        # Validate Pressure Sensor address
        if _pressure_sensor_address not in self.controller.pressure_sensors.keys():
            e_msg = "Invalid 'pressure sensor address' {0} entered for Point of Control {1}" \
                .format(
                str(_pressure_sensor_address),      # {0} this should be the pressure sensor address
                str(self.identifiers[0][1])                        # {1} point of control address
            )
            raise ValueError(e_msg)

        # Assign new 'Pressure Sensor' to object attribute
        else:
            _sn = self.controller.pressure_sensors[_pressure_sensor_address].sn
            self.ps = _pressure_sensor_address
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,         # {0}
                opcodes.point_of_control,   # {1}
                str(self.identifiers[0][1]),               # {2}
                "PS",                       # {3}
                str(_sn)                    # {4} this needs to be the serial number
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set pressure sensor {0} on point of control {1}. Exception: {2}".format(
                str(_pressure_sensor_address),  # {0}
                str(self.identifiers[0][1]),                   # {1}
                e.message                       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Pressure sensor' to: {1}".format(
                str(self.identifiers[0][1]),                   # {0}
                str(_pressure_sensor_address),  # {1}
            ))

    #################################
    def add_pump_to_point_of_control(self, _pump_address):
        """
        Sets the 'Pump' for the Point of Control on the controller. \n

        :param _pump_address:    New 'pump' for Point Of Connection \n
        :type _pump_address:     int \n
        """
        # Validate pump address
        if _pump_address not in self.controller.pumps.keys():
            e_msg = "Invalid 'pump address' {0} entered for Point of Control {1}" \
                .format(
                str(_pump_address),                 # {0} pump address
                str(self.identifiers[0][1])                        # {1} point of control address
            )
            raise ValueError(e_msg)

        # Assign new 'Pump' to object attribute
        else:
            _sn = self.controller.pumps[_pump_address].sn
            self.pp = _pump_address
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,
                opcodes.point_of_control,                   # {0}
                str(self.identifiers[0][1]),                               # {1}
                PointofControlCommands.Attributes.PUMP,     # {2}
                str(_sn)                                    # {3} this needs to be the serial number
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set pump {0} on point of control {1}. Exception: {2}".format(
                str(_pump_address),             # {0}
                str(self.identifiers[0][1]),                   # {1}
                e.message                       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Pump' to: {1}".format(
                str(self.identifiers[0][1]),                   # {0}
                str(_pump_address),  # {1}
            ))

    #################################
    def add_master_valve_to_point_of_control(self, _master_valve_address):
        """
        Sets the 'Master Valve' for the Point of Control on the controller. \n

        :param _master_valve_address:    New 'master_valve' for Point Of Connection \n
        :type _master_valve_address:     int \n
        """
        # Validate master valve address
        if _master_valve_address not in self.controller.master_valves.keys():
            e_msg = "Invalid 'master valve address' {0} entered for Point of Control {1}" \
                .format(
                str(_master_valve_address),                 # {0} master_valve address
                str(self.identifiers[0][1])                        # {1} point of control address
            )
            raise ValueError(e_msg)

        # Assign new 'Master Valve' to object attribute
        else:
            _sn = self.controller.master_valves[_master_valve_address].sn
            self.mv = _master_valve_address
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,
                opcodes.point_of_control,   # {0}
                str(self.identifiers[0][1]),               # {1}
                opcodes.master_valve,       # {2}
                str(_sn)                    # {3} this needs to be the serial number
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 'Master Valve' {0} on point of control {1}. Exception: {2}".format(
                str(_master_valve_address),     # {0}
                str(self.identifiers[0][1]),                   # {1}
                e.message                       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Master Valve' to: {1}".format(
                str(self.identifiers[0][1]),                   # {0}
                str(_master_valve_address),     # {1}
            ))

    #################################
    def add_mainline_to_point_of_control(self, _mainline_address):
        """
        Sets the 'Mainline' for the Point of Control on the controller. \n

        :param _mainline_address:    New 'Mainline' for Point Of Connection \n
        :type _mainline_address:     int \n
        """
        # Validate mainline number
        if self.called_by_controller_type == opcodes.flow_station:
            max_mainline_address = 20
        else:
            max_mainline_address = 8
        if _mainline_address not in range(1, max_mainline_address+1):
            e_msg = "Invalid 'Mainline' address entered for POC {0}. Available mainlines are 1 - {1}, user entered: {2}" \
                .format(
                    str(self.identifiers[0][1]),    # {0}
                    str(max_mainline_address),      # {1}
                    str(_mainline_address)          # {2}
                )
            raise ValueError(e_msg)

        # Assign new 'Mainline' to object attribute
        else:
            # Set the Flow Station and Controller Mainline Address for the Point of Control
            if self.called_by_controller_type == opcodes.flow_station:
                self.downstream_pipes.append(self.flowstation.get_mainline(_mainline_address))
                self.flow_station_mainline = _mainline_address
            else:
                self.downstream_pipes.append(self.controller.get_mainline(_mainline_address))
                self.controller_mainline = _mainline_address

            self.ml = _mainline_address
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                 # {0}
                opcodes.point_of_control,           # {1}
                str(self.identifiers[0][1]),        # {2}
                opcodes.mainline,                   # {3}
                str(self.ml)                        # {4}
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Mainline' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.ml),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Mainline' to: {1}".format(
                str(self.identifiers[0][1]),  # {0}
                str(self.ml),  # {1}
            ))

    #################################
    def set_monthly_water_budget(self, _gallons):
        """
        Sets the 'Monthly Water Budget' for the Point of Control on the controller. \n

        :param _gallons:    New 'Monthly Water Budget' for Point Of Connection \n
        :type _gallons:     int \n
        """

        self.wb = _gallons

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,              # {0}
                                               opcodes.point_of_connection,     # {1}
                                               str(self.identifiers[0][1]),                    # {2}
                                               opcodes.monthly_water_budget,    # {3}
                                               str(self.wb))                    # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Monthly Water Budget' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.wb),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Monthly Water Budget' to: {1}".format(str(self.identifiers[0][1]), str(self.wb)))

    #################################
    def set_shutdown_on_over_budget_to_true(self):
        """
        Sets the 'Shutdown on Over Budget' to 'TR' for the Point of Control on the controller. \n
        """

        self.ws = opcodes.true

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,              # {0}
                                               opcodes.point_of_connection,     # {1}
                                               str(self.identifiers[0][1]),                    # {2}
                                               opcodes.shutdown_on_over_budget, # {3}
                                               str(self.ws))                    # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Shutdown on Over Budget' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.ws),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Shutdown on Over Budget' to: {1}".format(str(self.identifiers[0][1]), str(self.ws)))

    #################################
    def set_shutdown_on_over_budget_to_false(self):
        """
        Sets the 'Shutdown on Over Budget' to 'FA' for the Point of Control on the controller. \n
        """

        self.ws = opcodes.false

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,               # {0}
                                               opcodes.point_of_connection,      # {1}
                                               str(self.identifiers[0][1]),                     # {2}
                                               opcodes.shutdown_on_over_budget,  # {3}
                                               str(self.ws))                     # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Shutdown on Over Budget' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.ws),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Shutdown on Over Budget' to: {1}".format(str(self.identifiers[0][1]), str(self.ws)))

    #################################
    def set_water_rationing_enable_to_true(self):
        """
        Sets the 'Water Rationing Enable' to 'TR' for the Point of Control on the controller. \n
        """

        self.wr = opcodes.true

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,              # {0}
                                               opcodes.point_of_connection,     # {1}
                                               str(self.identifiers[0][1]),                    # {2}
                                               opcodes.water_rationing_enable,  # {3}
                                               str(self.wr))                    # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Water Rationing Enable' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.wr),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Water Rationing Enable' to: {1}".format(str(self.identifiers[0][1]), str(self.wr)))

    #################################
    def set_water_rationing_enable_to_false(self):
        """
        Sets the 'Water Rationing Enable' to 'FA' for the Point of Control on the controller. \n
        """

        self.wr = opcodes.false

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,              # {0}
                                               opcodes.point_of_connection,     # {1}
                                               str(self.identifiers[0][1]),                    # {2}
                                               opcodes.water_rationing_enable,  # {3}
                                               str(self.wr))                    # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Water Rationing Enable' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.wr),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Water Rationing Enable' to: {1}".format(str(self.identifiers[0][1]), str(self.wr)))

    # TODO this should only be in the flowstaion
    #################################
    def set_group(self, _group_number):
        """
        Set the POC group on this POC (flowstation only)
        :param _group_number:            POC group (1-10) \n
        :type _group_number:             int \n
        :return:
        :rtype:
        """
        # The POC must be managed by a FlowStation
        if self.called_by_controller_type != opcodes.flow_station:
            e_msg = "Trying to set a POC group a controller that is not a FlowStation"
            raise Exception(e_msg)

        # Check for integer-ness
        if not isinstance(_group_number, int):
            e_msg = "Exception occurred trying to set group {0} on POC {1}, the value must " \
                    "be an int".format(
                str(_group_number),                 # {0}
                str(self.identifiers[0][1])         # {1}
            )
            raise Exception(e_msg)

        # Check for integer-ness
        if _group_number < 0 or _group_number > 10:
            e_msg = "Exception occurred trying to set group {0} on POC {1}, the value must " \
                    "be between 0 and 10, inclusive".format(
                str(_group_number),                 # {0}
                str(self.identifiers[0][1])         # {1}
            )
            raise Exception(e_msg)

        # Set the group attribute to the new value
        self.gr = _group_number

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                                 # {0}
            PointofControlCommands.PointOfControl,              # {1}
            str(self.identifiers[0][1]),                        # {2}
            PointofControlCommands.Attributes.GROUP,            # {3}
            str(self.gr)                                        # {4}
        )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set group {0} on POC {1}".format(
                str(self.gr),                   # {0}
                str(self.identifiers[0][1]),    # {1}
                e.message                       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set group {0} on POC {1}".format(
                str(self.gr),               # {0}
                self.identifiers[0][1])     # {1}
            )

    #################################
    def add_moisture_sensor_to_point_of_control(self, _moisture_sensor_address):
        """
        Sets the 'Moisture Sensor' for the Point of Control on the controller. \n

        :param _moisture_sensor_address:    New 'moisture_sensor' for Point Of Connection \n
        :type _moisture_sensor_address:     int \n
        """
        # Validate moisture sensor address
        if _moisture_sensor_address not in self.controller.moisture_sensors.keys():
            e_msg = "Invalid 'moisture sensor address' {0} entered for Point of Control {1}" \
                .format(
                        str(_moisture_sensor_address),  # {0} moisture_sensor address
                        str(self.identifiers[0][1])                    # {1} point of control address
                    )
            raise ValueError(e_msg)

        # Assign new 'Moisture Sensor to object attribute
        else:
            _sn = self.controller.moisture_sensors[_moisture_sensor_address].sn
            self.ms = _moisture_sensor_address
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,         # {0}
                opcodes.point_of_control,   # {1}
                str(self.identifiers[0][1]),               # {2}
                opcodes.moisture_sensor,    # {3}
                str(_sn)                    # {4} this needs to be the serial number
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 'Moisture Sensor' {0} on point of control {1}. Exception: {2}"\
                .format(str(_moisture_sensor_address),  # {0}
                        str(self.identifiers[0][1]),                   # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Moisture Sensor' to: {1}".format(
                str(self.identifiers[0][1]),                   # {0}
                str(_moisture_sensor_address),  # {1}
            ))

    #################################
    def set_moisture_empty_limit(self, _new_percent):
        """
        Sets the 'Moisture Empty Limit' for the Point of Control on the controller. \n

        :param _new_percent:    New 'Moisture Empty Limit' for Point Of Connection \n
        :type _new_percent:    float \n
        """

        self.me = _new_percent

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,              # {0}
                                               opcodes.point_of_connection,     # {1}
                                               str(self.identifiers[0][1]),                    # {2}
                                               opcodes.moisture_empty_limit,    # {3}
                                               str(self.me))                    # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Moisture Empty Limit' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.me),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Moisture Empty Limit' to: {1}".format(str(self.identifiers[0][1]), str(self.me)))

    #################################
    def set_moisture_sensor_empty_enable_to_true(self):
        """
        Sets the 'Moisture Sensor Empty Enable' to true for the Point of Control on the controller. \n
        """

        self.mn = opcodes.true

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,                      # {0}
                                               opcodes.point_of_connection,             # {1}
                                               str(self.identifiers[0][1]),                            # {2}
                                               opcodes.moisture_sensor_empty_enable,    # {3}
                                               str(self.mn))                            # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Moisture Sensor Empty Enable' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.mn),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Moisture Sensor Empty Enable' to: {1}"
                  .format(str(self.identifiers[0][1]), str(self.mn)))

    #################################
    def set_moisture_sensor_empty_enable_to_false(self):
        """
        Sets the 'Moisture Sensor Empty Enable' to false for the Point of Control on the controller. \n
        """

        self.mn = opcodes.false

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,                      # {0}
                                               opcodes.point_of_connection,             # {1}
                                               str(self.identifiers[0][1]),                            # {2}
                                               opcodes.moisture_sensor_empty_enable,    # {3}
                                               str(self.mn))                            # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Moisture Sensor Empty Enable' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.mn),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Moisture Sensor Empty Enable' to: {1}"
                  .format(str(self.identifiers[0][1]), str(self.mn)))

    #################################
    def set_empty_wait_time(self, _new_empty_wait_time):
        """
        Sets the 'Empty Wait Time' for the Point of Control on the controller. \n

        :param _new_empty_wait_time:    New 'Empty Wait Time' for Point Of Connection \n
        :type _new_empty_wait_time:    int \n
        """

        self.ew = _new_empty_wait_time

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,              # {0}
                                               opcodes.point_of_connection,     # {1}
                                               str(self.identifiers[0][1]),                    # {2}
                                               opcodes.empty_wait_time,         # {3}
                                               str(self.ew))                    # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Empty Wait Time' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.ew),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Empty Wait Time' to: {1}".format(str(self.identifiers[0][1]), str(self.ew)))

    #################################
    def add_event_switch_to_point_of_control(self, _event_switch_address):
        """
        Sets the 'Event Switch' for the Point of Control on the controller. \n

        :param _event_switch_address:    New 'Event Switch' for Point Of Connection \n
        :type _event_switch_address:     int \n
        """
        # Validate moisture sensor address
        if _event_switch_address not in self.controller.event_switches.keys():
            e_msg = "Invalid 'Event Switch address' {0} entered for Point of Control {1}" \
                .format(
                        str(_event_switch_address),     # {0} event switch address
                        str(self.identifiers[0][1])                    # {1} point of control address
                    )
            raise ValueError(e_msg)

        # Assign new 'Event Switch' to object attribute
        else:
            _sn = self.controller.event_switches[_event_switch_address].sn
            self.sw = _event_switch_address
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,         # {0}
                opcodes.point_of_control,   # {1}
                str(self.identifiers[0][1]),               # {2}
                opcodes.event_switch,       # {3}
                str(_sn)                    # {4} this needs to be the serial number
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 'Event Switch' {0} on point of control {1}. Exception: {2}" \
                .format(str(_event_switch_address),     # {0}
                        str(self.identifiers[0][1]),                   # {1}
                        e.message                       # {2}
                        )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Event Switch' to: {1}".format(
                str(self.identifiers[0][1]),                   # {0}
                str(_event_switch_address),     # {1}
            ))

    #################################
    def set_switch_empty_condition_to_op(self):
        """
        Sets the 'Switch Empty Condition' to 'OP' for the Point of Control on the controller. \n
        """

        self.se = opcodes.open

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,                      # {0}
                                               opcodes.point_of_connection,             # {1}
                                               str(self.identifiers[0][1]),                            # {2}
                                               opcodes.switch_empty_condition,          # {3}
                                               str(self.se))                            # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Switch Empty Condition' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.se),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Switch Empty Condition' to: {1}"
                  .format(str(self.identifiers[0][1]), str(self.se)))

    #################################
    def set_switch_empty_condition_to_cl(self):
        """
        Sets the 'Switch Empty Condition' to 'CL' for the Point of Control on the controller. \n
        """

        self.se = opcodes.closed

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,                      # {0}
                                               opcodes.point_of_connection,             # {1}
                                               str(self.identifiers[0][1]),                            # {2}
                                               opcodes.switch_empty_condition,          # {3}
                                               str(self.se))                            # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Switch Empty Condition' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.se),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Switch Empty Condition' to: {1}"
                  .format(str(self.identifiers[0][1]), str(self.se)))

    #################################
    def set_switch_empty_enable_to_true(self):
        """
        Sets the 'Switch Empty Enable' to 'TR' for the Point of Control on the controller. \n
        """

        self.sn = opcodes.true

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,                      # {0}
                                               opcodes.point_of_connection,             # {1}
                                               str(self.identifiers[0][1]),                            # {2}
                                               opcodes.switch_empty_enable,             # {3}
                                               str(self.sn))                            # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Switch Empty Enable' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.sn),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Switch Empty Enable' to: {1}"
                  .format(str(self.identifiers[0][1]), str(self.sn)))

    #################################
    def set_switch_empty_enable_to_false(self):
        """
        Sets the 'Switch Empty Enable' to 'FA' for the Point of Control on the controller. \n
        """

        self.sn = opcodes.false

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,                      # {0}
                                               opcodes.point_of_connection,             # {1}
                                               str(self.identifiers[0][1]),                            # {2}
                                               opcodes.switch_empty_enable,             # {3}
                                               str(self.sn))                            # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Switch Empty Enable' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.sn),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Switch Empty Enable' to: {1}"
                  .format(str(self.identifiers[0][1]), str(self.sn)))

    #################################
    def set_share_with_flow_station_to_true(self):
        """
        Sets the 'Share With FlowStation' to 'TR' for the Point of Control on the controller. \n
        """

        self.sh = opcodes.true

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,                      # {0}
                                               opcodes.point_of_connection,             # {1}
                                               str(self.identifiers[0][1]),                            # {2}
                                               opcodes.share_with_flowstation,          # {3}
                                               str(self.sh))                            # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Share With FlowStation' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.sh),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Share With FlowStation' to: {1}"
                  .format(str(self.identifiers[0][1]), str(self.sh)))

    #################################
    def set_share_with_flow_station_to_false(self):
        """
        Sets the 'Share With FlowStation' to 'FA' for the Point of Control on the controller. \n
        """
        # Set the point of control's address to the old address it had on the controller
        self.identifiers[0][1] = self.saved_3200_address
        self.sh = opcodes.false

        command = "{0},{1}={2},{3}={4}".format(ActionCommands.SET,                      # {0}
                                               opcodes.point_of_connection,             # {1}
                                               str(self.identifiers[0][1]),                            # {2}
                                               opcodes.share_with_flowstation,          # {3}
                                               str(self.sh))                            # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Share With FlowStation' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.sh),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Share With FlowStation' to: {1}"
                  .format(str(self.identifiers[0][1]), str(self.sh)))

    #################################
    def verify_mainline_on_point_of_control(self):
        """
        Verifies the 'Mainline' for the Point of Control set on the controller. \n
        :return:
        """
        # expect int type
        main_line_from_controller = int(self.data.get_value_string_by_key(opcodes.mainline))

        # Comparison of values between controller and object
        if self.ml != main_line_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Mainline'. Received: {1}, Expected: {2}".format(
                str(self.identifiers[0][1]),                      # {0}
                str(main_line_from_controller),    # {1}
                str(self.ml)                       # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Mainline': '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.ml)    # {1}
            ))

    #################################
    def verify_high_flow_limit(self):
        """
        Verifies the 'High Flow Limit' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'High Flow Limit' value from the controller reply.
        -   Lastly, compares the instance 'High Flow Limit' value with the controller's programming.
        """

        high_flow_limit_from_controller = int(self.data.get_value_string_by_key(opcodes.high_flow_limit))

        # Compare status versus what is on the controller
        if self.hf != high_flow_limit_from_controller:
            e_msg = "Unable to verify POC {0}'s 'High Flow Limit' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(high_flow_limit_from_controller),               # {1}
                        str(self.hf)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'High Flow Limit' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.hf)    # {1}
            ))

    #################################
    def verify_shutdown_on_high_flow(self):
        """
        Verifies the 'Shutdown On High Flow' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Shutdown On High Flow' value from the controller reply.
        -   Lastly, compares the instance 'Shutdown On High Flow' value with the controller's programming.
        """

        shutdown_on_high_flow_from_controller = str(self.data.get_value_string_by_key(opcodes.shutdown_on_high_flow))

        # Compare status versus what is on the controller
        if self.hs != shutdown_on_high_flow_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Shutdown On High Flow' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(shutdown_on_high_flow_from_controller),         # {1}
                        str(self.hs)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Shutdown On High Flow' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.hs)    # {1}
            ))

    #################################
    def verify_unscheduled_flow_limit(self):
        """
        Verifies the 'Unscheduled Flow Limit' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Unscheduled Flow Limit' value from the controller reply.
        -   Lastly, compares the instance 'Unscheduled Flow Limit' value with the controller's programming.
        """

        unscheduled_flow_limit_from_controller = int(self.data.get_value_string_by_key(opcodes.unscheduled_flow_limit))

        # Compare status versus what is on the controller
        if self.uf !=  unscheduled_flow_limit_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Unscheduled Flow Limit' value. Received: {1}, " \
                                "Expected: {2}".format(
                                    str(self.identifiers[0][1]),                                       # {0}
                                    str( unscheduled_flow_limit_from_controller),       # {1}
                                    str(self.uf)                                        # {2}
                                )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Unscheduled Flow Limit' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.uf)    # {1}
            ))

    #################################
    def verify_shutdown_on_unscheduled(self):
        """
        Verifies the 'Shutdown on Unscheduled' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Shutdown on Unscheduled' value from the controller reply.
        -   Lastly, compares the instance 'Shutdown on Unscheduled' value with the controller's programming.
        """

        unscheduled_flow_shutdown_from_controller = str(self.data.get_value_string_by_key(
                                                    opcodes.unscheduled_flow_shutdown))

        # Compare status versus what is on the controller
        if self.us != unscheduled_flow_shutdown_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Shutdown on Unscheduled' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(unscheduled_flow_shutdown_from_controller),     # {1}
                        str(self.us)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Shutdown on Unscheduled' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.us)    # {1}
            ))

    #################################
    def verify_flow_meter_serial_number(self):
        """
        Verifies the 'Flow Meter Serial Number' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Flow Meter Serial Number' value from the controller reply.
        -   Lastly, compares the instance 'Flow Meter Serial Number' value with the controller's programming.
        """

        flow_meter_from_controller = self.data.get_value_string_by_key(opcodes.flow_meter)

        # Get the current assigned serial number
        if self.fm:
            current_assigned_fm_serial = self.controller.flow_meters[self.fm].sn
        else:
            current_assigned_fm_serial = None

        # Compare status versus what is on the controller
        if current_assigned_fm_serial != flow_meter_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Flow Meter Serial Number' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(flow_meter_from_controller),                    # {1}
                        str(current_assigned_fm_serial)                     # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Flow Meter Serial Number' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),                       # {0}
                str(current_assigned_fm_serial)     # {1}
            ))

    #################################
    def verify_master_valve_serial_number(self):
        """
        Verifies the 'Master Valve Serial Number' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Master Valve Serial Number' value from the controller reply.
        -   Lastly, compares the instance 'Master Valve Serial Number' value with the controller's programming.
        """

        master_valve_from_controller = self.data.get_value_string_by_key(opcodes.master_valve)

        # Get the current assigned serial number
        if self.mv:
            current_assigned_mv_serial = self.controller.master_valves[self.mv].sn
        else:
            current_assigned_mv_serial = None

        # Compare status versus what is on the controller
        if current_assigned_mv_serial != master_valve_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Master Valve Serial Number' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(master_valve_from_controller),                  # {1}
                        str(current_assigned_mv_serial)                     # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Master Valve Serial Number' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),                       # {0}
                str(current_assigned_mv_serial)     # {1}
            ))

    #################################
    def verify_pump_serial_number(self):
        """
        Verifies the 'Pump Serial Number' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Pump Serial Number' value from the controller reply.
        -   Lastly, compares the instance 'Pump Serial Number' value with the controller's programming.
        """

        pump_from_controller = self.data.get_value_string_by_key("PP")

        # Get the current assigned serial number
        if self.pp:
            current_assigned_pp_serial = self.controller.pumps[self.pp].sn
        else:
            current_assigned_pp_serial = None

        # Compare status versus what is on the controller
        if pump_from_controller != current_assigned_pp_serial:
            e_msg = "Unable to verify POC {0}'s 'Pump Serial Number' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(pump_from_controller),                          # {1}
                        str(current_assigned_pp_serial)                     # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Pump Serial Number' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),                       # {0}
                str(current_assigned_pp_serial)     # {1}
            ))

    #################################
    def verify_share_with_flow_station(self):
        """
        Verifies the 'Share With Flow Station' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Share With Flow Station' value from the controller reply.
        -   Lastly, compares the instance 'Share With Flow Station' value with the controller's programming.
        """
        share_with_flow_station_from_controller = str(self.data.get_value_string_by_key(opcodes.share_with_flowstation))

        if share_with_flow_station_from_controller is not None:
            # Compare status versus what is on the controller
            if self.sh != share_with_flow_station_from_controller:
                e_msg = "Unable to verify POC {0}'s 'Share With Flow Station' value. Received: {1}, " \
                        "Expected: {2}".format(
                            str(self.identifiers[0][1]),                                       # {0}
                            str(share_with_flow_station_from_controller),       # {1}
                            str(self.sh)                                        # {2}
                        )
                raise ValueError(e_msg)
            else:
                print("Verified POC {0}'s 'Share With Flow Station' value: '{1}' on controller".format(
                    str(self.identifiers[0][1]),   # {0}
                    str(self.sh)    # {1}
                ))

    #################################
    def verify_pressure_sensor_serial_number(self):
        """
        Verifies the 'Pressure Sensor Serial Number' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Pressure Sensor Serial Number' value from the controller reply.
        -   Lastly, compares the instance 'Pressure Sensor Serial Number' value with the controller's programming.
        """

        pressure_sensor_from_controller = self.data.get_value_string_by_key("PS")

        # Get the current assigned serial number
        if self.ps:
            current_assigned_ps_serial = self.controller.pressure_sensors[self.ps].sn
        else:
            current_assigned_ps_serial = None

        # Compare status versus what is on the controller
        if pressure_sensor_from_controller != current_assigned_ps_serial:
            e_msg = "Unable to verify POC {0}'s 'Pressure Sensor Serial Number' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(pressure_sensor_from_controller),               # {1}
                        str(current_assigned_ps_serial)                     # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Pressure Sensor Serial Number' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),                       # {0}
                str(current_assigned_ps_serial)     # {1}
            ))

    #################################
    def verify_high_pressure_limit(self):
        """
        Verifies the 'High Pressure Limit' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'High Pressure Limit' value from the controller reply.
        -   Lastly, compares the instance 'High Pressure Limit' value with the controller's programming.
        """

        high_pressure_limit_from_controller = float(self.data.get_value_string_by_key(opcodes.high_pressure_limit))

        # Compare status versus what is on the controller
        if self.hp != high_pressure_limit_from_controller:
            e_msg = "Unable to verify POC {0}'s 'High Pressure Limit' value. Received: {1}, " \
                    "Expected: {2}".format(
                            str(self.identifiers[0][1]),                                       # {0}
                            str(high_pressure_limit_from_controller),           # {1}
                            str(self.hp)                                        # {2}
                        )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'High Pressure Limit' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.hp)    # {1}
            ))

    #################################
    def verify_high_pressure_shutdown(self):
        """
        Verifies the 'High Pressure Shutdown' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'High Pressure Shutdown' value from the controller reply.
        -   Lastly, compares the instance 'High Pressure Shutdown' value with the controller's programming.
        """

        high_pressure_shutdown_from_controller = str(self.data.get_value_string_by_key(opcodes.high_pressure_shutdown))

        # Compare status versus what is on the controller
        if self.he != high_pressure_shutdown_from_controller:
            e_msg = "Unable to verify POC {0}'s 'High Pressure Shutdown' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(high_pressure_shutdown_from_controller),        # {1}
                        str(self.he)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'High Pressure Shutdown' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.he)    # {1}
            ))

    #################################
    def verify_low_pressure_limit(self):
        """
        Verifies the 'Low Pressure Limit' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Low Pressure Limit' value from the controller reply.
        -   Lastly, compares the instance 'Low Pressure Limit' value with the controller's programming.
        """

        low_pressure_limit_from_controller = float(self.data.get_value_string_by_key(opcodes.low_pressure_limit))

        # Compare status versus what is on the controller
        if self.lp != low_pressure_limit_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Low Pressure Limit' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(low_pressure_limit_from_controller),            # {1}
                        str(self.lp)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Low Pressure Limit' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.lp)    # {1}
            ))

    #################################
    def verify_low_pressure_shutdown(self):
        """
        Verifies the 'Low Pressure Shutdown' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Low Pressure Shutdown' value from the controller reply.
        -   Lastly, compares the instance 'Low Pressure Shutdown' value with the controller's programming.
        """

        low_pressure_shutdown_from_controller = str(self.data.get_value_string_by_key(opcodes.low_pressure_shutdown))

        # Compare status versus what is on the controller
        if self.le != low_pressure_shutdown_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Low Pressure Shutdown' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(low_pressure_shutdown_from_controller),         # {1}
                        str(self.le)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Low Pressure Shutdown' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.le)    # {1}
            ))

    #################################
    def verify_monthly_water_budget(self):
        """
        Verifies the 'Monthly Water Budget' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Monthly Water Budget' value from the controller reply.
        -   Lastly, compares the instance 'Monthly Water Budget' value with the controller's programming.
        """
        # if self.controller.vr.startswith('12'):
        monthly_water_budget_from_controller = int(self.data.get_value_string_by_key(opcodes.monthly_water_budget))

        # Compare status versus what is on the controller
        if self.wb != monthly_water_budget_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Monthly Water Budget' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(monthly_water_budget_from_controller),          # {1}
                        str(self.wb)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Monthly Water Budget' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.wb)    # {1}
            ))

    #################################
    def verify_shutdown_on_over_budget(self):
        """
        Verifies the 'Shutdown On Over Budget' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Shutdown On Over Budget' value from the controller reply.
        -   Lastly, compares the instance 'Shutdown On Over Budget' value with the controller's programming.
        """
        # if self.controller.vr.startswith('12'):
        shutdown_on_over_budget_from_controller = str(self.data.get_value_string_by_key(opcodes.shutdown_on_over_budget))

        # Compare status versus what is on the controller
        if self.ws != shutdown_on_over_budget_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Shutdown On Over Budget State' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(shutdown_on_over_budget_from_controller),       # {1}
                        str(self.ws)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Shutdown On Over Budget' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.ws)    # {1}
            ))

    #################################
    def verify_water_rationing_enable(self):
        """
        Verifies the 'Water Rationing Enable' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Water Rationing Enable' value from the controller reply.
        -   Lastly, compares the instance 'Water Rationing Enable' value with the controller's programming.
        """
        water_rationing_enable_from_controller = self.data.get_value_string_by_key(opcodes.water_rationing_enable)

        # Compare status versus what is on the controller
        if self.wr != water_rationing_enable_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Water Rationing Enable' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(water_rationing_enable_from_controller),        # {1}
                        str(self.wr)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Water Rationing Enable' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.wr)    # {1}
            ))

    #################################
    def verify_group(self):
        """
        Verifies the Group value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the Group value from the controller reply.
        -   Lastly, compares the instance Group value with the controller's programming.
        """

        group_from_controller = int(self.data.get_value_string_by_key(opcodes.group))

        # Compare expected Group versus what is on the controller
        if self.gr != group_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Group' value. Received: {1}, " \
                    "Expected: {2}".format(
                str(self.identifiers[0][1]),           # {0}
                str(group_from_controller),            # {1}
                str(self.gr)                           # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Group' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.gr)                   # {1}
            ))

    #################################
    def verify_moisture_sensor_serial_number(self):
        """
        Verifies the 'Moisture Sensor Serial Number' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Moisture Sensor Serial Number' value from the controller reply.
        -   Lastly, compares the instance 'Moisture Sensor Serial Number' value with the controller's programming.
        """

        moisture_sensor_from_controller = str(self.data.get_value_string_by_key(opcodes.moisture_sensor))

        # Get the current assigned serial number
        current_assigned_ms_serial = self.controller.moisture_sensors[self.ms].sn

        # Compare status versus what is on the controller
        if moisture_sensor_from_controller != current_assigned_ms_serial:
            e_msg = "Unable to verify POC {0}'s 'Moisture Sensor Serial Number' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(moisture_sensor_from_controller),               # {1}
                        str(current_assigned_ms_serial)                     # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Moisture Sensor Serial Number' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),                       # {0}
                str(current_assigned_ms_serial)     # {1}
            ))

    #################################
    def verify_moisture_empty_limit(self):
        """
        Verifies the 'Moisture Empty Limit' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Moisture Empty Limit' value from the controller reply.
        -   Lastly, compares the instance 'Moisture Empty Limit' value with the controller's programming.
        """
        moisture_empty_limit_from_controller = float(self.data.get_value_string_by_key(opcodes.moisture_empty_limit))

        # Compare status versus what is on the controller
        if self.me != moisture_empty_limit_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Moisture Empty Limit' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(moisture_empty_limit_from_controller),          # {1}
                        str(self.me)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Moisture Empty Limit' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.me)    # {1}
            ))

    #################################
    def verify_moisture_sensor_empty_enable(self):
        """
        Verifies the 'Moisture Sensor Empty Enable' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Moisture Sensor Empty Enable' value from the controller reply.
        -   Lastly, compares the instance 'Moisture Sensor Empty Enable' value with the controller's programming.
        """
        moisture_sensor_empty_enable_from_controller = str(self.data.get_value_string_by_key(
                                                                                  opcodes.moisture_sensor_empty_enable))

        # Compare status versus what is on the controller
        if self.mn != moisture_sensor_empty_enable_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Moisture Sensor Empty Enable' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(moisture_sensor_empty_enable_from_controller),  # {1}
                        str(self.mn)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Moisture Sensor Empty Enable' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.mn)    # {1}
            ))

    #################################
    def verify_empty_wait_time(self):
        """
        Verifies the 'Empty Wait Time' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Empty Wait Time' value from the controller reply.
        -   Lastly, compares the instance 'Empty Wait Time' value with the controller's programming.
        """
        empty_wait_time_from_controller = int(self.data.get_value_string_by_key(opcodes.empty_wait_time))

        # Compare status versus what is on the controller
        if self.ew != empty_wait_time_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Empty Wait Time' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(empty_wait_time_from_controller),               # {1}
                        str(self.ew)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Empty Wait Time' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.ew)    # {1}
            ))

    #################################
    def verify_event_switch_serial_number(self):
        """
        Verifies the 'Event Switch Serial Number' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Event Switch Serial Number' value from the controller reply.
        -   Lastly, compares the instance 'Event Switch Serial Number' value with the controller's programming.
        """

        event_switch_from_controller = str(self.data.get_value_string_by_key(opcodes.event_switch))

        # Get the current assigned serial number
        current_assigned_sw_serial = self.controller.event_switches[self.sw].sn

        # Compare status versus what is on the controller
        if event_switch_from_controller != current_assigned_sw_serial:
            e_msg = "Unable to verify POC {0}'s 'Event Switch Serial Number' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(event_switch_from_controller),                  # {1}
                        str(current_assigned_sw_serial)                     # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Event Switch Serial Number' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),                       # {0}
                str(event_switch_from_controller)   # {1}
            ))

    #################################
    def verify_switch_empty_condition(self):
        """
        Verifies the 'Switch Empty Condition' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Switch Empty Condition' value from the controller reply.
        -   Lastly, compares the instance 'Switch Empty Condition' value with the controller's programming.
        """
        switch_empty_condition_from_controller = str(self.data.get_value_string_by_key("SE"))

        # Compare status versus what is on the controller
        if self.se != switch_empty_condition_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Switch Empty Condition' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(switch_empty_condition_from_controller),        # {1}
                        str(self.se)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Switch Empty Condition' value: '{1}' ".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.se)    # {1}
            ))

    #################################
    def verify_switch_empty_enable(self):
        """
        Verifies the 'Switch Empty Enable' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Switch Empty Enable' value from the controller reply.
        -   Lastly, compares the instance 'Switch Empty Enable' value with the controller's programming.
        """
        switch_empty_enable_from_controller = str(self.data.get_value_string_by_key(opcodes.switch_empty_enable))

        # Compare status versus what is on the controller
        if self.sn != switch_empty_enable_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Switch Empty Enable' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(switch_empty_enable_from_controller),           # {1}
                        str(self.sn)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Switch Empty Enable' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.sn)    # {1}
            ))

    #################################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies all attributes associated with this Point of Control . \n
        :param expected_status:     An expected status to verify for POC \n
        :type expected_status:      str \n
        """
        self.get_data()

        #### This verifier should NOT be setting mainlines. Only setters should be setting these.

        # verify attributes common to all controllers
        self.verify_description()
        self.verify_enabled_state()
        self.verify_target_flow()
        self.verify_high_flow_limit()
        self.verify_shutdown_on_high_flow()
        self.verify_unscheduled_flow_limit()
        self.verify_shutdown_on_unscheduled()
        self.verify_flow_meter_serial_number()
        self.verify_master_valve_serial_number()
        self.verify_pump_serial_number()

        if expected_status is not None:
            self.verify_status(_expected_status=expected_status)

        # verify attributes when managed by Flow Station
        if self.controller.controller_type == opcodes.flow_station:
            self.verify_group()
            self.ml = self.flow_station_mainline
            self.verify_mainline_on_point_of_control()
            self.verify_share_with_flow_station()
            self.verify_pressure_sensor_serial_number()
            self.verify_high_pressure_limit()
            self.verify_high_pressure_shutdown()
            self.verify_low_pressure_limit()
            self.verify_low_pressure_shutdown()

        # verify attributes specific to the 3200
        if self.controller.controller_type == opcodes.basestation_3200:
            self.verify_share_with_flow_station()
            if not self.is_managed_by_flowstation():
                # self.ml will always be 0 if it is shared with a Flow Station. Just want to check when it is
                # not managed by the FlowStation
                self.ml = self.controller_mainline
                self.verify_mainline_on_point_of_control()
            self.verify_pressure_sensor_serial_number()
            self.verify_high_pressure_limit()
            self.verify_high_pressure_shutdown()
            self.verify_low_pressure_limit()
            self.verify_low_pressure_shutdown()

            # verify attributes specific to firmware V12
            if self.controller.vr.startswith("12"):
               # implemented in water source
               # self.verify_priority()
                self.verify_monthly_water_budget()
                self.verify_shutdown_on_over_budget()
                self.verify_water_rationing_enable()
                self.verify_moisture_sensor_serial_number()
                self.verify_moisture_empty_limit()
                self.verify_moisture_sensor_empty_enable()
                self.verify_empty_wait_time()
                self.verify_event_switch_serial_number()
                self.verify_switch_empty_condition()
                self.verify_switch_empty_enable()
