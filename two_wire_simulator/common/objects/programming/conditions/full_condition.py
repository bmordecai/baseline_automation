import status_parser
from common.objects.base_classes.conditions import BaseConditions
from common.imports.types import ActionCommands, FullConditionCommands
from common.imports import opcodes, types


__author__ = 'Kent'


########################################################################################################################
# 3200 Water Source Full Condition Class
########################################################################################################################
class FullCondition(BaseConditions):
    """
    3200 Water Source Full Condition Object \n
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    #################################
    def __init__(self, _controller, _full_condition_address, _water_source_address, _device_type, _device_serial_number):
        """
        3200 Water Source Full Condition Constructor. \n

        :param _controller:              The controller that will be passed in to each water source made by this method. \n
        :type _controller:               common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

        :param full_condition_address, :  Address (full condition number) \n
        :type full_condition_address, :   int \n

        :param _water_source_address:   The address of the water source on this controller \n
        :type _water_source_address:    int \n

        :param _device_type:            The two digit string that represents the device attached to this condition. \n
        :type _device_type:             str \n

        :param _device_serial_number:   The serial number of the device attached to this full condition. \n
        :type _device_serial_number:    str \n
        """
        # Init parent class to inherit respective attributes
        # init base class
        # create message reference to object
        # Give access to the controller
        self.controller = _controller

        # self.messages = FullMessages(_full_condition_object=self)  # full Conditions messages
        """:type: common.objects.messages.programming.WaterSourceMessages"""

        BaseConditions.__init__(
            self,
            _controller=_controller,
            _watersource_ad=_water_source_address,
            _event_type=FullConditionCommands.Type.FULL,
            _identifiers=[
                [FullConditionCommands.Type.FULL, _full_condition_address],
                [opcodes.water_source, _water_source_address]

            ]
        )
        # ----- Instance Variables ----- #
        self.ser = _controller.ser
        self.full_condition_address = _full_condition_address
        self.water_source_address = _water_source_address
        self.device_type = _device_type
        self.device_serial_number = _device_serial_number

        # Container to hold Water Source Full Condition programming from controller when a 'self.get_data()'
        # call is made.
        self.data = status_parser.KeyValues('')

    #################################
    def build_obj_configuration_for_send(self):
        """
        Builds the 'SET' string for sending this Water Source Instance to the Controller. This method ensures
        that attributes with blank values don't get sent to the controller. This helps avoid 'BC' response's from
        controller. \n
        :return:        Formatted 'Set' string for the Water Source \n
        :rtype:         str \n
        """
        # Starting string, always will need this, 'SET,PC=Blah'
        current_config = "{0},{1}={2},{3}={4},{5}={6},{7}={8}".format(
            types.ActionCommands.SET,               # {0}
            opcodes.full_condition,                 # {1}
            str(self.full_condition_address),       # {2}
            opcodes.water_source,                   # {3}
            str(self.water_source_address),         # {4}
            str(opcodes.type),                      # {5}
            str(self.device_type),                  # {6}
            str(self.device_type),                  # {7}
            str(self.device_serial_number)          # {8}
           )
        return current_config


class SwitchFullCondition(FullCondition):
    """
    Switch Full Condition for 3200
    """

    def __init__(self, _controller, _full_condition_address, _water_source_address, _device_serial_number):
        """
        Switch Full Condition Init \n

        :param _controller:     The _controller that will be passed in to each water source made by this method. \n
        :type _controller:      common.objects._controllers.bl_32.BaseStation3200 |
                                common.objects._controllers.bl_10.BaseStation1000 \n

        :param _full_condition_address:  Address (full condition number) \n
        :type _full_condition_address:   int \n

        :param _water_source_address:     Current water source address for _controller \n
        :type _water_source_address:      int \n

        :param _device_serial_number:  Serial number of event switch to attach to full condition. \n
        :type _device_serial_number:   str \n
        """
        # init base class
        FullCondition.__init__(
            self,
            _controller=_controller,
            _full_condition_address=_full_condition_address,
            _water_source_address=_water_source_address,
            _device_type=opcodes.event_switch,
            _device_serial_number=_device_serial_number
        )
        self.lt = None

    #################################
    def set_switch_full_condition_to_open(self):
        """
        Sets the 'Event Switch Condition State' for the Water Source on the controller to open ('OP'). \n
        """
        self.lt = opcodes.open
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                    # {0}
            opcodes.full_condition,                # {1}
            self.full_condition_address,           # {2}
            opcodes.water_source,                  # {3}
            self.water_source_address,             # {4}
            opcodes.switch_full_condition,         # {5}
            self.lt                                # {6}
        )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Water Source {0}'s 'Event Switch Full Condition {1} to open-> " \
                    "{2}".format(
                        self.water_source_address,         # {0}
                        self.full_condition_address,      # {1}
                        e.message                          # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Water Source {0}'s 'Event Switch Full Condition {1} to open".format(
                self.water_source_address,         # {0}
                self.full_condition_address,      # {1}
            ))

    #################################
    def set_switch_full_condition_to_closed(self):
        """
        Sets the 'Event Switch Condition State' for the Water Source on the controller to closed ('CL'). \n
        """
        self.lt = opcodes.closed
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                     # {0}
            opcodes.full_condition,                 # {1}
            self.full_condition_address,            # {2}
            opcodes.water_source,                   # {3}
            self.water_source_address,              # {4}
            opcodes.switch_full_condition,          # {5}
            self.lt                                 # {6}
        )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Water Source {0}'s 'Event Switch Full Condition {1} to closed-> " \
                    "{2}".format(
                        self.water_source_address,      # {0}
                        self.full_condition_address,    # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Water Source {0}'s 'Event Switch Full Condition {1} to closed".format(
                self.water_source_address,      # {0}
                self.full_condition_address,    # {1}
            ))

    #################################
    def verify_switch_full_condition(self):
        """
        Verifies the 'Switch Full Condition' for the Water Source set on the controller. \n
        :return:
        """
        # expect string type
        full_condition_from_controller = self.data.get_value_string_by_key(opcodes.switch_full_condition)

        # Comparison of values between controller and object
        if self.lt != full_condition_from_controller:
            e_msg = "Unable to verify Water Source {0}'s 'Switch Full Condition'. Received: {1}, Expected: {2}".format(
                str(self.water_source_address),         # {0}
                str(full_condition_from_controller),   # {1}
                str(self.lt)                            # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Water Source {0}'s 'Switch Full Condition': '{1}' on controller".format(
                str(self.water_source_address),     # {0}
                str(self.lt)                        # {1}
            ))

    ###############################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this condition's values against what the controller has. \n

        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()
        self.verify_switch_full_condition()

        # Call StartCondition verifier to verify the rest of the full condition attributes
        self.verify_myself(expected_status=expected_status, skip_get_data=True)


class PressureFullCondition(FullCondition):
    """
    Pressure Full Condition for 3200
    """

    def __init__(self, _controller, _full_condition_address, _water_source_address, _device_serial_number):
        """
        Pressure Full Condition Init \n

        :param _controller:     The _controller that will be passed in to each water source made by this method. \n
        :type _controller:      common.objects._controllers.bl_32.BaseStation3200 |
                                common.objects._controllers.bl_10.BaseStation1000 \n

        :param _full_condition_address:  Address (full condition number) \n
        :type _full_condition_address:   int \n

        :param _water_source_address:     Current water source address for _controller \n
        :type _water_source_address:      int \n

        :param _device_serial_number:  Serial number of pressure sensor to attach to full condition. \n
        :type _device_serial_number:   str \n
        """
        # init base class
        FullCondition.__init__(
            self,
            _controller=_controller,
            _full_condition_address=_full_condition_address,
            _water_source_address=_water_source_address,
            _device_type=opcodes.pressure_sensor_ps,
            _device_serial_number=_device_serial_number
        )

        self.pl = None

    #################################
    def set_pressure_full_limit(self, _psi):
        """
        Sets the 'Pressure Full Limit' for the Water Source on the controller. \n

        :param _psi:    New 'pressure full limit' (PSI) for Water Source \n
        :type _psi:     int \n
        """
        # Assign new 'Pressure EMPTY Limit' to object attribute
        self.pl = _psi
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                            # {0}
            opcodes.full_condition,                        # {1}
            self.full_condition_address,                   # {2}
            opcodes.water_source,                          # {3}
            self.water_source_address,                     # {4}
            opcodes.pressure_full_limit_full_condition,    # {5}
            self.pl                                        # {6}
        )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Water Source {0}'s Pressure Sensor Full Condition {1}'s" \
                    "pressure full limit to {2} -> {3}".format(
                        self.water_source_address,      # {0}
                        self.full_condition_address,    # {1}
                        self.pl,                        # {2}
                        e.message                       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Water Source {0} Event Switch Full Condition {1}'s pressure full limit"
                  " to {2}".format(
                        self.water_source_address,      # {0}
                        self.full_condition_address,    # {1}
                        self.pl,                        # {2}
                    ))

    #################################
    def verify_pressure_full_limit(self):
        """
        Verifies the 'Pressure Full Limit' for the Water Source set on the controller. \n
        :return:
        """
        # expect int type
        pressure_full_limit_from_controller = int(self.data.get_value_string_by_key(
                                                                            opcodes.pressure_full_limit_full_condition))

        # Comparison of values between controller and object
        if self.pl != pressure_full_limit_from_controller:
            e_msg = "Unable to verify Water Source {0} Full Condition {1}'s 'Pressure Full Limit'. Received: {2}," \
                    " Expected: {3}".format(
                        str(self.water_source_address),                 # {0}
                        str(self.full_condition_address),               # {1}
                        str(pressure_full_limit_from_controller),       # {2}
                        str(self.pl),                                   # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Water Source {0} Full Condition {1}'s 'Pressure Full Limit': '{2}' on controller".format(
                str(self.water_source_address),     # {0}
                str(self.full_condition_address),   # {1}
                str(self.pl)                        # {2}
            ))

    ###############################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this condition's values against what the controller has. \n

        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()
        self.verify_pressure_full_limit()

        # Call StartCondition verifier to verify the rest of the full condition attributes
        self.verify_myself(expected_status=expected_status, skip_get_data=True)


class MoistureFullCondition(FullCondition):
    """
    Moisture Full Condition for 3200
    """

    def __init__(self, _controller, _full_condition_address, _water_source_address, _device_serial_number):
        """
        Moisture Full Condition Init \n

        :param _controller:     The _controller that will be passed in to each water source made by this method. \n
        :type _controller:      common.objects._controllers.bl_32.BaseStation3200 |
                                common.objects._controllers.bl_10.BaseStation1000 \n

        :param _full_condition_address:  Address (full condition number) \n
        :type _full_condition_address:   int \n

        :param _water_source_address:     Current water source address for _controller \n
        :type _water_source_address:      int \n

        :param _device_serial_number:  Serial number of moisture sensor to attach to full condition. \n
        :type _device_serial_number:   str \n
        """
        # init base class
        FullCondition.__init__(
            self,
            _controller=_controller,
            _full_condition_address=_full_condition_address,
            _water_source_address=_water_source_address,
            _device_type=opcodes.moisture_sensor,
            _device_serial_number=_device_serial_number
        )

        self.ml = None

    #################################
    def set_moisture_full_limit(self, _percent):
        """
        Sets the 'Moisture Full Limit' for the Water Source on the controller. \n

        :param _percent:   New 'moisture full limit' (%) for Water Source \n
        :type _percent:    int \n
        """
        # Assign new 'Moisture EMPTY Limit' to object attribute
        self.ml = _percent
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            opcodes.full_condition,                         # {1}
            self.full_condition_address,                    # {2}
            opcodes.water_source,                           # {3}
            self.water_source_address,                      # {4}
            opcodes.moisture_full_limit_full_condition,     # {5}
            self.ml                                         # {6}
        )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Water Source {0}'s Moisture Sensor Full Condition {1}'s" \
                    "moisture full limit to {2} -> {3}".format(
                        self.water_source_address,      # {0}
                        self.full_condition_address,    # {1}
                        self.ml,                        # {2}
                        e.message                       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Water Source {0} Event Switch Full Condition {1}'s moisture full limit"
                  " to {2}".format(
                        self.water_source_address,      # {0}
                        self.full_condition_address,    # {1}
                        self.ml,                        # {2}
                  ))

    #################################
    def verify_moisture_full_limit(self):
        """
        Verifies the 'Moisture Full Limit' for the Water Source set on the controller. \n
        :return:
        """
        # expect int type
        wait_time_from_controller = int(self.data.get_value_string_by_key(opcodes.moisture_full_limit_full_condition))

        # Comparison of values between controller and object
        if self.ml != wait_time_from_controller:
            e_msg = "Unable to verify Water Source {0} Full Condition {1}'s 'Moisture Full Limit'. Received: {2}," \
                    " Expected: {3}".format(
                        str(self.water_source_address),     # {0}
                        str(self.full_condition_address),   # {1}
                        str(wait_time_from_controller),     # {2}
                        str(self.ml),                       # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Water Source {0} Full Condition {1}'s 'Moisture Full Limit': '{2}' on controller".format(
                str(self.water_source_address),     # {0}
                str(self.full_condition_address),  # {1}
                str(self.ml)                        # {2}
            ))

    ###############################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this condition's values against what the controller has. \n

        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()
        self.verify_moisture_full_limit()

        # Call StartCondition verifier to verify the rest of the full condition attrs
        self.verify_myself(expected_status=expected_status, skip_get_data=True)

