import status_parser
from common.objects.base_classes.conditions import BaseConditions
from common.imports.types import ActionCommands, EmptyConditionCommands
from common.objects.messages.conditions.empty_messages import EmptyMessages
from common.objects.base_classes.ser import Ser
from common.imports import opcodes,types
# from common.objects.object_bucket import moisture_sensors, event_switches, analog_bicoder


__author__ = 'Tige'


########################################################################################################################
# 3200 Water Source Empty Condition Class
########################################################################################################################
class EmptyCondition(BaseConditions):
    """
    3200 Water Source Empty Condition Object \n
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    #################################
    def __init__(self, _controller, _empty_condition_address, _water_source_address, _device_type, _device_serial_number):
        """
        3200 Water Source Empty Condition Constructor. \n

        :param _controller:              The controller that will be passed in to each water source made by this method. \n
        :type _controller:               common.objects.controllers.bl_32.BaseStation3200 |
                                        common.objects.controllers.bl_10.BaseStation1000 \n

        :param empty_condition_address, :  Address (empty condition number) \n
        :type empty_condition_address, :   int \n

        :param _water_source_address:   The address of the water source on this controller \n
        :type _water_source_address:    int \n

        :param _device_type:            The two digit string that represents the device attached to this condition. \n
        :type _device_type:             str \n

        :param _device_serial_number:   The serial number of the device attached to this empty condition. \n
        :type _device_serial_number:    str \n
        """
        # Init parent class to inherit respective attributes
        # init base class
        # create message reference to object
        # Give access to the controller
        self.controller = _controller

        self.messages = EmptyMessages(_empty_condition_object=self)  # empty Conditions messages
        """:type: common.objects.messages.programming.WaterSourceMessages"""

        BaseConditions.__init__(
            self,
            _controller=_controller,
            _watersource_ad=_water_source_address,
            _event_type=EmptyConditionCommands.Type.EMPTY,
            _identifiers=[
                [EmptyConditionCommands.Type.EMPTY, _empty_condition_address],
                [opcodes.water_source_empty_condition, _water_source_address]

            ]
        )
        # ----- Instance Variables ----- #
        self.ser = _controller.ser
        self.empty_condition_address = _empty_condition_address
        self.water_source_address = _water_source_address
        self.device_type = _device_type
        self.device_serial_number = _device_serial_number
        self.tm = None

        # Container to hold Water Source Empty Condition programming from controller when a 'self.get_data()'
        # call is made.
        self.data = status_parser.KeyValues('')

    #################################
    def build_obj_configuration_for_send(self):
        """
        Builds the 'SET' string for sending this Water Source Instance to the Controller. This method ensures
        that attributes with blank values don't get sent to the controller. This helps avoid 'BC' response's from
        controller. \n
        :return:        Formatted 'Set' string for the Water Source \n
        :rtype:         str \n
        """
        # Starting string, always will need this, 'SET,PC=Blah'
        current_config = "{0},{1}={2},{3}={4},{5}={6},{7}={8}".format(
            types.ActionCommands.SET,               # {0}
            opcodes.empty_condition,                # {1}
            str(self.empty_condition_address),      # {2}
            opcodes.water_source_empty_condition,   # {3}
            str(self.water_source_address),         # {4}
            str(opcodes.type),                      # {5}
            str(self.device_type),                  # {6}
            str(self.device_type),                  # {7}
            str(self.device_serial_number)          # {8}
           )
        return current_config

    #################################
    def set_empty_wait_time(self, _minutes):
        """
        Sets the 'EMPTY Wait Time' for the Water Source on the controller. \n

        :param _minutes:    New 'EMPTY Wait Time' (minutes) for Water Source \n
        :type _minutes:     int \n
        """
        # Assign new 'EMPTY Wait Time' to object attribute
        self.tm = _minutes
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                         # {0}
            opcodes.empty_condition,                    # {1}
            self.empty_condition_address,               # {2}
            opcodes.water_source_empty_condition,       # {3}
            self.water_source_address,                  # {4}
            opcodes.empty_wait_time_empty_condition,    # {5}
            self.tm                                     # {6}
        )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Water Source {0}'s Empty Condition {1} to {2} -> " \
                    "{3}".format(
                        self.water_source_address,      # {0}
                        self.empty_condition_address,   # {1}
                        self.tm,                        # {2}
                        e.message                       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Water Source {0}'s Empty Condition {1} to {2}".format(
                self.water_source_address,     # {0}
                self.empty_condition_address,  # {1}
                self.tm,                       # {2}
            ))

    #################################
    def verify_empty_wait_time(self):
        """
        Verifies the 'Empty Wait Time' for the Water Source set on the controller. \n
        :return:
        """
        # expect int type
        wait_time_from_controller = int(self.data.get_value_string_by_key(opcodes.empty_wait_time_empty_condition))

        # Comparison of values between controller and object
        if self.tm != wait_time_from_controller:
            e_msg = "Unable to verify Water Source {0} Empty Condition {1}'s 'Empty Wait Time'. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self.water_source_address),     # {0}
                        str(self.empty_condition_address),  # {1}
                        str(wait_time_from_controller),     # {2}
                        str(self.tm),                       # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Water Source {0} Empty Condition {1}'s 'Empty Wait Time': '{2}' on controller".format(
                str(self.water_source_address),     # {0}
                str(self.empty_condition_address),  # {1}
                str(self.tm)                        # {2}
            ))


class SwitchEmptyCondition(EmptyCondition):
    """
    Switch Empty Condition for 3200
    """

    def __init__(self, _controller, _empty_condition_address, _water_source_address, _device_serial_number):
        """
        Switch Empty Condition Init \n

        :param _controller:     The _controller that will be passed in to each water source made by this method. \n
        :type _controller:      common.objects._controllers.bl_32.BaseStation3200 |
                                common.objects._controllers.bl_10.BaseStation1000 \n

        :param _empty_condition_address:  Address (empty condition number) \n
        :type _empty_condition_address:   int \n

        :param _water_source_address:     Current water source address for _controller \n
        :type _water_source_address:      int \n

        :param _device_serial_number:  Serial number of event switch to attach to empty condition. \n
        :type _device_serial_number:   str \n
        """
        # init base class
        EmptyCondition.__init__(
            self,
            _controller=_controller,
            _empty_condition_address=_empty_condition_address,
            _water_source_address=_water_source_address,
            _device_type=opcodes.event_switch,
            _device_serial_number=_device_serial_number
        )
        self.lt = None

    #################################
    def set_switch_empty_condition_to_open(self):
        """
        Sets the 'Event Switch Condition State' for the Water Source on the controller to open ('OP'). \n
        """
        self.lt = opcodes.open
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                     # {0}
            opcodes.empty_condition,                # {1}
            self.empty_condition_address,           # {2}
            opcodes.water_source_empty_condition,   # {3}
            self.water_source_address,              # {4}
            opcodes.switch_empty_condition,         # {5}
            self.lt                                 # {6}
        )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Water Source {0}'s 'Event Switch Empty Condition {1} to open-> " \
                    "{2}".format(
                        self.water_source_address,         # {0}
                        self.empty_condition_address,      # {1}
                        e.message                          # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Water Source {0}'s 'Event Switch Empty Condition {1} to open".format(
                self.water_source_address,         # {0}
                self.empty_condition_address,      # {1}
            ))

    #################################
    def set_switch_empty_condition_to_closed(self):
        """
        Sets the 'Event Switch Condition State' for the Water Source on the controller to closed ('CL'). \n
        """
        self.lt = opcodes.closed
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                     # {0}
            opcodes.empty_condition,                # {1}
            self.empty_condition_address,           # {2}
            opcodes.water_source_empty_condition,   # {3}
            self.water_source_address,              # {4}
            opcodes.switch_empty_condition,         # {5}
            self.lt                                 # {6}
        )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Water Source {0}'s 'Event Switch Empty Condition {1} to closed-> " \
                    "{2}".format(
                        self.water_source_address,  # {0}
                        self.empty_condition_address,  # {1}
                        e.message  # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Water Source {0}'s 'Event Switch Empty Condition {1} to closed".format(
                self.water_source_address,  # {0}
                self.empty_condition_address,  # {1}
            ))

    #################################
    def verify_switch_empty_condition(self):
        """
        Verifies the 'Switch Empty Condition' for the Water Source set on the controller. \n
        :return:
        """
        # expect string type
        empty_condition_from_controller = self.data.get_value_string_by_key(opcodes.switch_empty_condition)

        # Comparison of values between controller and object
        if self.lt != empty_condition_from_controller:
            e_msg = "Unable to verify Water Source {0}'s 'Switch Empty Condition'. Received: {1}, Expected: {2}".format(
                str(self.water_source_address),         # {0}
                str(empty_condition_from_controller),   # {1}
                str(self.lt)                            # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Water Source {0}'s 'Switch Empty Condition': '{1}' on controller".format(
                str(self.water_source_address),     # {0}
                str(self.lt)                        # {1}
            ))

    ###############################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this condition's values against what the controller has. \n

        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()
        self.verify_switch_empty_condition()
        self.verify_empty_wait_time()

        # Call StartCondition verifier to verify the rest of the empty condition attrs
        EmptyCondition.verify_myself(self, expected_status=expected_status, skip_get_data=True)


class PressureEmptyCondition(EmptyCondition):
    """
    Pressure Empty Condition for 3200
    """

    def __init__(self, _controller, _empty_condition_address, _water_source_address, _device_serial_number):
        """
        Pressure Empty Condition Init \n

        :param _controller:     The _controller that will be passed in to each water source made by this method. \n
        :type _controller:      common.objects._controllers.bl_32.BaseStation3200 |
                                common.objects._controllers.bl_10.BaseStation1000 \n

        :param _empty_condition_address:  Address (empty condition number) \n
        :type _empty_condition_address:   int \n

        :param _water_source_address:     Current water source address for _controller \n
        :type _water_source_address:      int \n

        :param _device_serial_number:  Serial number of pressure sensor to attach to empty condition. \n
        :type _device_serial_number:   str \n
        """
        # init base class
        EmptyCondition.__init__(
            self,
            _controller=_controller,
            _empty_condition_address=_empty_condition_address,
            _water_source_address=_water_source_address,
            _device_type=opcodes.pressure_sensor_ps,
            _device_serial_number=_device_serial_number
        )

        self.pl = None

    #################################
    def set_pressure_empty_limit(self, _psi):
        """
        Sets the 'Pressure Empty Limit' for the Water Source on the controller. \n

        :param _psi:    New 'pressure empty limit' (PSI) for Water Source \n
        :type _psi:     int \n
        """
        # Assign new 'Pressure EMPTY Limit' to object attribute
        self.pl = _psi
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            opcodes.empty_condition,                        # {1}
            self.empty_condition_address,                   # {2}
            opcodes.water_source_empty_condition,           # {3}
            self.water_source_address,                      # {4}
            opcodes.pressure_empty_limit_empty_condition,   # {5}
            self.pl                                         # {6}
        )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Water Source {0}'s Pressure Sensor Empty Condition {1}'s" \
                    "pressure empty limit to {2} -> {3}".format(
                        self.water_source_address,      # {0}
                        self.empty_condition_address,   # {1}
                        self.pl,                        # {2}
                        e.message                       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Water Source {0} Event Switch Empty Condition {1}'s pressure empty limit"
                  " to {2}".format(
                        self.water_source_address,     # {0}
                        self.empty_condition_address,  # {1}
                        self.pl,                       # {2}
                    ))

    #################################
    def verify_pressure_empty_limit(self):
        """
        Verifies the 'Pressure Empty Limit' for the Water Source set on the controller. \n
        :return:
        """
        # expect int type
        wait_time_from_controller = int(self.data.get_value_string_by_key(opcodes.pressure_empty_limit_empty_condition))

        # Comparison of values between controller and object
        if self.pl != wait_time_from_controller:
            e_msg = "Unable to verify Water Source {0} Empty Condition {1}'s 'Pressure Empty Limit'. Received: {2}," \
                    " Expected: {3}".format(
                        str(self.water_source_address),     # {0}
                        str(self.empty_condition_address),  # {1}
                        str(wait_time_from_controller),     # {2}
                        str(self.pl),                       # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Water Source {0} Empty Condition {1}'s 'Pressure Empty Limit': '{2}' on controller".format(
                str(self.water_source_address),     # {0}
                str(self.empty_condition_address),  # {1}
                str(self.pl)                        # {2}
            ))

    ###############################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this condition's values against what the controller has. \n

        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()
        self.verify_pressure_empty_limit()
        self.verify_empty_wait_time()

        # Call StartCondition verifier to verify the rest of the empty condition attrs
        EmptyCondition.verify_myself(self, expected_status=expected_status, skip_get_data=True)


class MoistureEmptyCondition(EmptyCondition):
    """
    Moisture Empty Condition for 3200
    """

    def __init__(self, _controller, _empty_condition_address, _water_source_address, _device_serial_number):
        """
        Moisture Empty Condition Init \n

        :param _controller:     The _controller that will be passed in to each water source made by this method. \n
        :type _controller:      common.objects._controllers.bl_32.BaseStation3200 |
                                common.objects._controllers.bl_10.BaseStation1000 \n

        :param _empty_condition_address:  Address (empty condition number) \n
        :type _empty_condition_address:   int \n

        :param _water_source_address:     Current water source address for _controller \n
        :type _water_source_address:      int \n

        :param _device_serial_number:  Serial number of moisture sensor to attach to empty condition. \n
        :type _device_serial_number:   str \n
        """
        # init base class
        EmptyCondition.__init__(
            self,
            _controller=_controller,
            _empty_condition_address=_empty_condition_address,
            _water_source_address=_water_source_address,
            _device_type=opcodes.moisture_sensor,
            _device_serial_number=_device_serial_number
        )

        self.ml = None

    #################################
    def set_moisture_empty_limit(self, _percent):
        """
        Sets the 'Moisture Empty Limit' for the Water Source on the controller. \n

        :param _percent:   New 'moisture empty limit' (%) for Water Source \n
        :type _percent:    int or float\n
        """
        # Assign new 'Moisture EMPTY Limit' to object attribute
        self.ml = _percent
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            opcodes.empty_condition,                        # {1}
            self.empty_condition_address,                   # {2}
            opcodes.water_source_empty_condition,           # {3}
            self.water_source_address,                      # {4}
            opcodes.moisture_empty_limit_empty_condition,   # {5}
            self.ml                                         # {6}
        )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Water Source {0}'s Moisture Sensor Empty Condition {1}'s" \
                    "moisture empty limit to {2} -> {3}".format(
                        self.water_source_address,      # {0}
                        self.empty_condition_address,   # {1}
                        self.ml,                        # {2}
                        e.message                       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Water Source {0} Event Switch Empty Condition {1}'s moisture empty limit"
                  " to {2}".format(
                        self.water_source_address,     # {0}
                        self.empty_condition_address,  # {1}
                        self.ml,                       # {2}
                  ))

    #################################
    def verify_moisture_empty_limit(self):
        """
        Verifies the 'Moisture Empty Limit' for the Water Source set on the controller. \n
        :return:
        """
        # expect int type
        wait_time_from_controller = int(self.data.get_value_string_by_key(opcodes.moisture_empty_limit_empty_condition))

        # Comparison of values between controller and object
        if self.ml != wait_time_from_controller:
            e_msg = "Unable to verify Water Source {0} Empty Condition {1}'s 'Moisture Empty Limit'. Received: {2}," \
                    " Expected: {3}".format(
                        str(self.water_source_address),     # {0}
                        str(self.empty_condition_address),  # {1}
                        str(wait_time_from_controller),     # {2}
                        str(self.ml),                       # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Water Source {0} Empty Condition {1}'s 'Moisture Empty Limit': '{2}' on controller".format(
                str(self.water_source_address),     # {0}
                str(self.empty_condition_address),  # {1}
                str(self.ml)                        # {2}
            ))

    ###############################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this condition's values against what the controller has. \n

        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()
        self.verify_moisture_empty_limit()
        self.verify_empty_wait_time()

        # Call StartCondition verifier to verify the rest of the empty condition attrs
        EmptyCondition.verify_myself(self, expected_status=expected_status, skip_get_data=True)

