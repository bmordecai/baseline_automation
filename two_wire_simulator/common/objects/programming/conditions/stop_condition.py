from common.imports import opcodes
from common.imports.types import ActionCommands, ProgramConditionCommands
from common.objects.base_classes.conditions import BaseConditions
from common.objects.messages.conditions.stop_condition_messages import StopConditionMessages

__author__ = 'ben'


class StopCondition(BaseConditions):
    """
    Stop Condition for 3200
    """

    def __init__(self, _controller, _program_ad, _device_type, _device_serial):
        """
        Stop Condition Init \n

        :param _controller:     The _controller that will be passed in to each water source made by this method. \n
        :type _controller:      common.objects._controllers.bl_32.BaseStation3200 |
                                common.objects._controllers.bl_10.BaseStation1000 \n

        :param _program_ad:     Current condition number for _controller \n
        :type _program_ad:      int \n

        :param _device_type:    The type of device attached to this stop condition. \n
        :type _device_type:     str \n

        :param _device_serial:  Current condition number for _controller. \n
        :type _device_serial:   str \n
        """
        # 1000 controllers require the 'TY=MS' key value pair
        if _controller.is1000():
            _identifiers = [
                [ProgramConditionCommands.Type.STOP, _program_ad],
                [opcodes.type, _device_type],
                [_device_type, _device_serial]
            ]
        else:
            _identifiers = [
                [ProgramConditionCommands.Type.STOP, _program_ad],
                [_device_type, _device_serial]
            ]

        # init base class
        BaseConditions.__init__(
            self,
            _controller=_controller,
            _program_ad=_program_ad,
            _event_type=ProgramConditionCommands.Type.STOP,
            _identifiers=_identifiers
        )
        # create message reference to object
        self.messages = StopConditionMessages(_stop_condition_object=self)  # Stop Condition messages
        """:type: common.objects.messages.conditions.stop_condition_messages.StopConditionMessages"""

        # Regardless of what device is passed in, these attribute are pretty global
        self.device_serial = _device_serial     # The serial number of the device that will be doing to setting
        self.mode = None                        # For moisture/temp/pressure sensor: (OF | UL | LL), for event switch: (OF | OP | CL)
        self.threshold = None                   # For temperature and moisture sensor only
        self.en = None                          # Enabled is only for pressure sensor
        self.stop_immediately = opcodes.false   # opcode: SI

    ###############################
    def set_stop_immediately_true(self):
        """
        Generic setter for enabling stop immediately value for stop condition.
        """
        self.stop_immediately = opcodes.true

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,         # {0}
            self.get_id(),              # {1}
            opcodes.stop_immediately,   # {2}
            self.stop_immediately       # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s stop immediately to: {2}".format(
                self.program_ad,
                self.device_serial,
                opcodes.true
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s stop immediately to: {2}".format(
                self.program_ad,
                self.device_serial,
                opcodes.true
            ))

    ###############################
    def set_stop_immediately_false(self):
        """
        Generic setter for disabling stop immediately value for stop condition.
        """
        self.stop_immediately = opcodes.false

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,         # {0}
            self.get_id(),              # {1}
            opcodes.stop_immediately,   # {2}
            self.stop_immediately       # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s stop immediately to: {2}".format(
                self.program_ad,
                self.device_serial,
                opcodes.false
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s stop immediately to: {2}".format(
                self.program_ad,
                self.device_serial,
                opcodes.false
            ))

    ###############################
    def verify_stop_immediately(self):
        """
        Generic verifier to verify stop condition's stop immediately flag value.
        """
        # expect string type
        stop_immediately_from_controller = self.data.get_value_string_by_key(opcodes.stop_immediately)

        # Comparison of values between controller and object
        if self.stop_immediately != stop_immediately_from_controller:
            e_msg = "Unable to verify Program {0} Stop Condition for Event Switch {1} stop immediately. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.program_ad),                   # {0}
                        self.device_serial,                     # {1}
                        str(stop_immediately_from_controller),  # {2}
                        str(self.stop_immediately)              # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0} Stop Condition for Event Switch {1} stop immediately: '{2}' on "
                  "controller".format(
                        str(self.program_ad),       # {0}
                        self.device_serial,         # {1}
                        str(self.stop_immediately)  # {2}
                    ))

    ###############################
    def verify_myself(self, expected_status=None, skip_get_data=False):
        """
        Verifies this condition's values against what the controller has. \n

        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n

        :param skip_get_data:     Value that tells us if we need to call get_data again or not. \n
        :type skip_get_data:      bool \n
        """
        if not skip_get_data:
            self.get_data()

        self.verify_stop_immediately()

        if expected_status is not None:
            self.verify_status(_expected_status=expected_status)


class SwitchStopCondition(StopCondition):
    """
    Switch Stop Condition for 3200
    """

    def __init__(self, _controller, _program_ad, _device_serial):
        """
        Switch Stop Condition Init \n

        :param _controller:     The _controller that will be passed in to each water source made by this method. \n
        :type _controller:      common.objects._controllers.bl_32.BaseStation3200 |
                                common.objects._controllers.bl_10.BaseStation1000 \n

        :param _program_ad:     Current condition number for _controller \n
        :type _program_ad:      int \n

        :param _device_serial:  Current condition number for _controller. \n
        :type _device_serial:   str \n
        """
        # init base class
        StopCondition.__init__(
            self,
            _controller=_controller,
            _program_ad=_program_ad,
            _device_type=opcodes.event_switch,
            _device_serial=_device_serial
        )

    ###############################
    def set_switch_mode_to_open(self):
        """
        Sets the mode of the switch stop condition to OP (Open).
        """
        self.mode = ProgramConditionCommands.Attributes.OPEN_MODE

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,     # {0}
            self.get_id(),          # {1}
            opcodes.switch_mode,    # {2}
            self.mode               # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s switch mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.OPEN_MODE
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s switch mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.OPEN_MODE
            ))

    ###############################
    def set_switch_mode_to_closed(self):
        """
        Sets the mode of the switch stop condition to CL (Closed).
        """
        self.mode = ProgramConditionCommands.Attributes.CLOSED_MODE

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,     # {0}
            self.get_id(),          # {1}
            opcodes.switch_mode,    # {2}
            self.mode               # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s switch mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.CLOSED_MODE
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s switch mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.CLOSED_MODE
            ))

    ###############################
    def set_switch_mode_to_off(self):
        """
        Sets the mode of the switch stop condition to OF (Off).
        """
        self.mode = ProgramConditionCommands.Attributes.OFF_MODE

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,     # {0}
            self.get_id(),          # {1}
            opcodes.switch_mode,    # {2}
            self.mode               # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s switch mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.OFF_MODE
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s switch mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.OFF_MODE
            ))

    ###############################
    def verify_switch_mode(self):
        """
        Verifies the mode of this object on the controller.
        """
        # expect string type
        switch_mode_from_controller = self.data.get_value_string_by_key(opcodes.switch_mode)

        # Comparison of values between controller and object
        if self.mode != switch_mode_from_controller:
            e_msg = "Unable to verify Program {0} Stop Condition for Event Switch {1} switch mode. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self.program_ad),               # {0}
                        self.device_serial,                 # {1}
                        str(switch_mode_from_controller),   # {2}
                        str(self.mode)                      # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0} Stop Condition for Event Switch {1} switch mode: '{2}' on controller".format(
                str(self.program_ad),   # {0}
                self.device_serial,     # {1}
                str(self.mode)          # {2}
            ))

    ###############################
    def verify_switch_serial_number(self):
        """
        Verifies the serial nuber of this object on the controller.
        """
        # expect string type
        switch_serial_from_controller = self.data.get_value_string_by_key(opcodes.event_switch)

        # Comparison of values between controller and object
        if self.device_serial != switch_serial_from_controller:
            e_msg = "Unable to verify Program {0} Stop Condition for Event Switch {1} switch serial number. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.program_ad),                   # {0}
                        self.device_serial,                     # {1}
                        str(switch_serial_from_controller),     # {2}
                        str(self.device_serial)                 # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0} Stop Condition for Event Switch {1} switch serial number: '{2}' on "
                  "controller".format(
                        str(self.program_ad),       # {0}
                        self.device_serial,         # {1}
                        str(self.device_serial)     # {2}
                    ))

    ###############################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this condition's values against what the controller has. \n

        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()

        self.verify_switch_mode()
        self.verify_switch_serial_number()

        # Call StopCondition verifier to verify the rest of the stop condition attrs
        StopCondition.verify_myself(self, expected_status=expected_status, skip_get_data=True)


class TemperatureStopCondition(StopCondition):
    """
    Temperature Stop Condition for 3200
    """

    def __init__(self, _controller, _program_ad, _device_serial):
        """
        Temperature Stop Condition Init \n

        :param _controller:     The _controller that will be passed in to each water source made by this method. \n
        :type _controller:      common.objects._controllers.bl_32.BaseStation3200 |
                                common.objects._controllers.bl_10.BaseStation1000 \n

        :param _program_ad:     Current condition number for _controller \n
        :type _program_ad:      int \n

        :param _device_serial:  Current condition number for _controller. \n
        :type _device_serial:   str \n
        """
        # init base class
        StopCondition.__init__(
            self,
            _controller=_controller,
            _program_ad=_program_ad,
            _device_type=opcodes.temperature_sensor,
            _device_serial=_device_serial
        )

    ###############################
    def set_temperature_threshold(self, _degrees):
        """
        Sets the temperature threshold limit of the stop condition.
        :param _degrees:      \n
        :type _degrees:     float
        """
        if isinstance(_degrees, int):
            _degrees = float(_degrees)
        if not isinstance(_degrees, float):
            raise TypeError('Degrees must be a float or int')
        self.threshold = _degrees

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,                     # {0}
            self.get_id(),                          # {1}
            opcodes.temperature_trigger_threshold,  # {2}
            self.threshold                          # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s temperature threshold " \
                    "to: {2}".format(
                        self.program_ad,
                        self.device_serial,
                        self.threshold
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s temperature threshold to: {2}".format(
                self.program_ad,
                self.device_serial,
                self.threshold
            ))

    ###############################
    def set_temperature_mode_off(self):
        """
        Sets the mode of the stop condition to "OF" which effectively disables the stop condition.
        """
        self.mode = ProgramConditionCommands.Attributes.OFF_MODE

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,                 # {0}
            self.get_id(),                      # {1}
            opcodes.temperature_sensor_mode,    # {2}
            self.mode                           # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s temperature mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.OFF_MODE
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s temperature mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.OFF_MODE
            ))

    ###############################
    def set_temperature_mode_to_upper_limit(self):
        """
        Sets the mode of the stop condition to use upper limit threshold (UL).
        """
        self.mode = ProgramConditionCommands.Attributes.UPPER_LIMIT_MODE

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,                 # {0}
            self.get_id(),                      # {1}
            opcodes.temperature_sensor_mode,    # {2}
            self.mode                           # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s temperature mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.UPPER_LIMIT_MODE
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s temperature mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.UPPER_LIMIT_MODE
            ))

    ###############################
    def set_temperature_mode_to_lower_limit(self):
        """
        Sets the mode of the stop condition to use lower limit threshold (LL).
        """
        self.mode = ProgramConditionCommands.Attributes.LOWER_LIMIT_MODE

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,                 # {0}
            self.get_id(),                      # {1}
            opcodes.temperature_sensor_mode,    # {2}
            self.mode                           # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s temperature mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.LOWER_LIMIT_MODE
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s temperature mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.LOWER_LIMIT_MODE
            ))

    ###############################
    def verify_temperature_threshold(self):
        """
        Verifies the temperature threshold limit of the stop condition.
        """
        # expect float type
        temperature_threshold_from_controller = float(self.data.get_value_string_by_key(opcodes.temperature_trigger_threshold))

        # Comparison of values between controller and object
        if not self.isclose(self.threshold, temperature_threshold_from_controller):
            e_msg = "Unable to verify Program {0} Stop Condition for Temperature Sensor {1} temperature threshold. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.program_ad),                        # {0}
                        self.device_serial,                          # {1}
                        str(temperature_threshold_from_controller),  # {2}
                        str(self.threshold)                          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0} Stop Condition for Temperature Sensor {1} temperature threshold: '{2}' "
                  "on controller".format(
                        str(self.program_ad),   # {0}
                        self.device_serial,     # {1}
                        str(self.threshold)     # {2}
                    ))

    ###############################
    def verify_temperature_mode(self):
        """
        Verifies the mode of this object on the controller.
        """
        # expect string type
        temperature_mode_from_controller = self.data.get_value_string_by_key(opcodes.temperature_sensor_mode)

        # Comparison of values between controller and object
        if self.mode != temperature_mode_from_controller:
            e_msg = "Unable to verify Program {0} Stop Condition for Temperature Sensor {1} temperature mode. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.program_ad),                   # {0}
                        self.device_serial,                     # {1}
                        str(temperature_mode_from_controller),  # {2}
                        str(self.mode)                          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0} Stop Condition for Temperature Sensor {1} temperature mode: '{2}' "
                  "on controller".format(
                        str(self.program_ad),   # {0}
                        self.device_serial,     # {1}
                        str(self.mode)          # {2}
                    ))

    ###############################
    def verify_temperature_serial_number(self):
        """
        Verifies the serial nuber of this object on the controller.
        """
        # expect string type
        tempearture_sensor_serial_from_controller = self.data.get_value_string_by_key(opcodes.temperature_sensor)

        # Comparison of values between controller and object
        if self.device_serial != tempearture_sensor_serial_from_controller:
            e_msg = "Unable to verify Program {0} Stop Condition for Temperature Sensor {1} serial " \
                    "number. Received: {2}, Expected: {3}".format(
                        str(self.program_ad),                               # {0}
                        self.device_serial,                                 # {1}
                        str(tempearture_sensor_serial_from_controller),     # {2}
                        str(self.device_serial)                             # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0} Stop Condition for Temperature Sensor {1} serial number: '{2}' on "
                  "controller".format(
                        str(self.program_ad),       # {0}
                        self.device_serial,         # {1}
                        str(self.device_serial)     # {2}
                    ))

    ###############################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this condition's values against what the controller has. \n

        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()

        self.verify_temperature_serial_number()
        self.verify_temperature_threshold()
        self.verify_temperature_mode()

        # Call StopCondition verifier to verify the rest of the stop condition attrs
        StopCondition.verify_myself(self, expected_status=expected_status, skip_get_data=True)


class MoistureStopCondition(StopCondition):
    """
    Moisture Stop Condition for 3200
    """

    def __init__(self, _controller, _program_ad, _device_serial):
        """
        Moisture Stop Condition Init \n

        :param _controller:     The _controller that will be passed in to each water source made by this method. \n
        :type _controller:      common.objects._controllers.bl_32.BaseStation3200 |
                                common.objects._controllers.bl_10.BaseStation1000 \n

        :param _program_ad:     Current condition number for _controller \n
        :type _program_ad:      int \n

        :param _device_serial:  Current condition number for _controller. \n
        :type _device_serial:   str \n
        """
        # init base class
        StopCondition.__init__(
            self,
            _controller=_controller,
            _program_ad=_program_ad,
            _device_type=opcodes.moisture_sensor,
            _device_serial=_device_serial
        )

    ###############################
    def set_moisture_threshold(self, _percent):
        """
        Sets the moisture threshold limit of the stop condition.
        """
        self.threshold = _percent

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,                     # {0}
            self.get_id(),                          # {1}
            opcodes.moisture_trigger_threshold,     # {2}
            self.threshold                          # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s moisture " \
                    "threshold to: {2}".format(
                        self.program_ad,
                        self.device_serial,
                        self.threshold
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s moisture threshold to: {2}".format(
                self.program_ad,
                self.device_serial,
                self.threshold
            ))

    ###############################
    def set_moisture_mode_off(self):
        """
        Sets the mode of the stop condition to "OF" which effectively disables the stop condition.
        """
        self.mode = ProgramConditionCommands.Attributes.OFF_MODE

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,         # {0}
            self.get_id(),              # {1}
            opcodes.moisture_mode,      # {2}
            self.mode                   # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s moisture mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.OFF_MODE
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s moisture mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.OFF_MODE
            ))

    ###############################
    def set_moisture_mode_to_upper_limit(self):
        """
        Sets the mode of the stop condition to use upper limit threshold (UL).
        """
        self.mode = ProgramConditionCommands.Attributes.UPPER_LIMIT_MODE

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,     # {0}
            self.get_id(),          # {1}
            opcodes.moisture_mode,  # {2}
            self.mode               # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s moisture mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.UPPER_LIMIT_MODE
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s moisture mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.UPPER_LIMIT_MODE
            ))

    ###############################
    def set_moisture_mode_to_lower_limit(self):
        """
        Sets the mode of the stop condition to use lower limit threshold (LL).
        """
        self.mode = ProgramConditionCommands.Attributes.LOWER_LIMIT_MODE

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,     # {0}
            self.get_id(),          # {1}
            opcodes.moisture_mode,  # {2}
            self.mode               # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s moisture mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.LOWER_LIMIT_MODE
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s moisture mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.LOWER_LIMIT_MODE
            ))

    ###############################
    def verify_moisture_threshold(self):
        """
        Verifies the moisture threshold limit of the stop condition.
        """
        # expect float type
        moisture_threshold_from_controller = float(self.data.get_value_string_by_key(opcodes.moisture_trigger_threshold))

        # Comparison of values between controller and object
        if not self.isclose(self.threshold, moisture_threshold_from_controller):
            e_msg = "Unable to verify Program {0} Stop Condition for Moisture Sensor {1} moisture threshold. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.program_ad),                       # {0}
                        self.device_serial,                         # {1}
                        str(moisture_threshold_from_controller),    # {2}
                        str(self.threshold)                         # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0} Stop Condition for Moisture Sensor {1} moisture threshold: '{2}' "
                  "on controller".format(
                        str(self.program_ad),   # {0}
                        self.device_serial,     # {1}
                        str(self.threshold)     # {2}
                    ))

    ###############################
    def verify_moisture_mode(self):
        """
        Verifies the mode of this object on the controller.
        """
        # expect string type
        moisture_mode_from_controller = self.data.get_value_string_by_key(opcodes.moisture_mode)

        # Comparison of values between controller and object
        if self.mode != moisture_mode_from_controller:
            e_msg = "Unable to verify Program {0} Stop Condition for Moisture Sensor {1} moisture mode. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.program_ad),                   # {0}
                        self.device_serial,                     # {1}
                        str(moisture_mode_from_controller),     # {2}
                        str(self.mode)                          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0} Stop Condition for Moisture Sensor {1} moisture mode: '{2}' "
                  "on controller".format(
                        str(self.program_ad),   # {0}
                        self.device_serial,     # {1}
                        str(self.mode)          # {2}
                    ))

    ###############################
    def verify_moisture_serial_number(self):
        """
        Verifies the serial nuber of this object on the controller.
        """
        # expect string type
        moisture_sensor_serial_from_controller = self.data.get_value_string_by_key(opcodes.moisture_sensor)

        # Comparison of values between controller and object
        if self.device_serial != moisture_sensor_serial_from_controller:
            e_msg = "Unable to verify Program {0} Stop Condition for Moisture Sensor {1} serial number. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.program_ad),                           # {0}
                        self.device_serial,                             # {1}
                        str(moisture_sensor_serial_from_controller),    # {2}
                        str(self.device_serial)                         # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0} Stop Condition for Moisture Sensor {1} serial number: '{2}' on "
                  "controller".format(
                        str(self.program_ad),       # {0}
                        self.device_serial,         # {1}
                        str(self.device_serial)     # {2}
                    ))

    ###############################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this condition's values against what the controller has. \n

        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()

        self.verify_moisture_serial_number()
        self.verify_moisture_threshold()
        self.verify_moisture_mode()

        # Call StopCondition verifier to verify the rest of the stop condition attrs
        StopCondition.verify_myself(self, expected_status=expected_status, skip_get_data=True)


class PressureStopCondition(StopCondition):
    """
    Pressure Stop Condition for 3200
    """

    def __init__(self, _controller, _program_ad, _device_serial):
        """
        Pressure Stop Condition Init \n

        :param _controller:     The _controller that will be passed in to each water source made by this method. \n
        :type _controller:      common.objects._controllers.bl_32.BaseStation3200 |
                                common.objects._controllers.bl_10.BaseStation1000 \n

        :param _program_ad:     Current condition number for _controller \n
        :type _program_ad:      int \n

        :param _device_serial:  Current condition number for _controller. \n
        :type _device_serial:   str \n
        """
        # init base class
        StopCondition.__init__(
            self,
            _controller=_controller,
            _program_ad=_program_ad,
            _device_type=opcodes.pressure_sensor_ps,
            _device_serial=_device_serial
        )

    ###############################
    def set_pressure_limit(self, _limit):
        """
        Sets the pressure threshold limit of the stop condition.
        """
        self.threshold = _limit

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,         # {0}
            self.get_id(),              # {1}
            opcodes.pressure_limit,     # {2}
            self.threshold              # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s pressure " \
                    "threshold limit to: {2}".format(
                self.program_ad,
                self.device_serial,
                self.threshold
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s pressure threshold limit to: {2}".format(
                self.program_ad,
                self.device_serial,
                self.threshold
            ))

    ###############################
    def set_pressure_mode_to_upper_limit(self):
        """
        Sets the mode of the stop condition to use upper limit threshold (UL).
        """
        self.mode = ProgramConditionCommands.Attributes.UPPER_LIMIT_MODE

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,     # {0}
            self.get_id(),          # {1}
            opcodes.limit_type,     # {2}
            self.mode               # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s pressure mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.UPPER_LIMIT_MODE
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s pressure mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.UPPER_LIMIT_MODE
            ))

    ###############################
    def set_pressure_mode_to_lower_limit(self):
        """
        Sets the mode of the stop condition to use lower limit threshold (LL).
        """
        self.mode = ProgramConditionCommands.Attributes.LOWER_LIMIT_MODE

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,     # {0}
            self.get_id(),          # {1}
            opcodes.limit_type,     # {2}
            self.mode               # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} Stop Condition {1}'s pressure mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.LOWER_LIMIT_MODE
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} Stop Condition {1}'s pressure mode to: {2}".format(
                self.program_ad,
                self.device_serial,
                ProgramConditionCommands.Attributes.LOWER_LIMIT_MODE
            ))

    ###############################
    def verify_pressure_limit(self):
        """
        Verifies the pressure threshold limit of the stop condition.
        """
        # expect float type
        pressure_threshold_from_controller = float(self.data.get_value_string_by_key(opcodes.pressure_limit))

        # Comparison of values between controller and object
        if not self.isclose(self.threshold, pressure_threshold_from_controller):
            e_msg = "Unable to verify Program {0} Stop Condition for Pressure Sensor {1} pressure threshold. " \
                    "Received: {2}, Expected: {3}".format(
                str(self.program_ad),                       # {0}
                self.device_serial,                         # {1}
                str(pressure_threshold_from_controller),    # {2}
                str(self.threshold)                         # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0} Stop Condition for Pressure Sensor {1} pressure threshold: '{2}' "
                  "on controller".format(
                str(self.program_ad),   # {0}
                self.device_serial,     # {1}
                str(self.threshold)     # {2}
            ))

    ###############################
    def verify_pressure_mode(self):
        """
        Verifies the mode of this object on the controller.
        """
        # expect string type
        pressure_mode_from_controller = self.data.get_value_string_by_key(opcodes.limit_type)

        # Comparison of values between controller and object
        if self.mode != pressure_mode_from_controller:
            e_msg = "Unable to verify Program {0} Stop Condition for Pressure Sensor {1} pressure mode. " \
                    "Received: {2}, Expected: {3}".format(
                str(self.program_ad),                   # {0}
                self.device_serial,                     # {1}
                str(pressure_mode_from_controller),     # {2}
                str(self.mode)                          # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0} Stop Condition for Pressure Sensor {1} pressure mode: '{2}' "
                  "on controller".format(
                str(self.program_ad),   # {0}
                self.device_serial,     # {1}
                str(self.mode)          # {2}
            ))

    ###############################
    def verify_pressure_serial_number(self):
        """
        Verifies the serial nuber of this object on the controller.
        """
        # expect string type
        pressure_sensor_serial_from_controller = self.data.get_value_string_by_key(opcodes.pressure_sensor)

        # Comparison of values between controller and object
        if self.device_serial != pressure_sensor_serial_from_controller:
            e_msg = "Unable to verify Program {0} Stop Condition for Pressure Sensor {1} serial number. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.program_ad),                           # {0}
                        self.device_serial,                             # {1}
                        str(pressure_sensor_serial_from_controller),    # {2}
                        str(self.device_serial)                         # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0} Stop Condition for Pressure Sensor {1} serial number: '{2}' on "
                  "controller".format(
                        str(self.program_ad),       # {0}
                        self.device_serial,         # {1}
                        str(self.device_serial)     # {2}
                    ))

    ###############################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this condition's values against what the controller has. \n

        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()

        self.verify_pressure_serial_number()
        self.verify_pressure_mode()
        self.verify_pressure_limit()

        # Call StopCondition verifier to verify the rest of the stop condition attrs
        StopCondition.verify_myself(self, expected_status=expected_status, skip_get_data=True)


        # TODO this was here, ask Tige about removal
        # def set_as_et_event(self, mode, threshold=0.05):
        #     """
        #     Sets the current condition to use deficit trigger thresholds events.
        #     :param mode: Deficit mode
        #     :type mode: str
        #     :param threshold: Threshold to trigger event
        #     :type threshold: float
        #     """
        #     self._ty = StartStopPauseEvent.weather_based  # event type
        #
        #     # valid mode entered
        #     if mode in [opcodes.true, opcodes.false]:
        #         self._em = mode
        #     else:
        #         e_msg = "Invalid Deficit trigger threshold mode for 3200 stop condition #{cond_num}: {mode}. Valid " \
        #                 "options are: 'TR' | 'FA'".format(
        #                     cond_num=self.program_ad,
        #                     mode=mode
        #                 )
        #         raise ValueError(e_msg)
        #
        #     self._et = threshold



