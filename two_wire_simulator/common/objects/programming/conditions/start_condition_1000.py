import re
from common.imports import opcodes
from common.objects.programming.conditions.start_condition import StartCondition
from common.imports.types import ProgramConditionCommands

__author__ = 'Eldin'


#
# START_COND Program Condition
#
class StartConditionFor1000(StartCondition):
    """
    Start Condition for 1000: \n

    Usage (must be done in this sequence): \n
        - creating start condition instance: \n
            new_instance_name = StartConditionFor1000(program_ad=1) \n

        - sending the programming to the controller: \n
            new_instance_name.set_moisture_condition_on_pg(serial_number="XXX", mode="XXX", threshold="XXX") \n
    """

    def __init__(self, _controller, _program_ad, _device_type, _device_serial):
        """
        Start Condition initializer

        :param _controller:     The _controller that will be passed in to each water source made by this method. \n
        :type _controller:      common.objects._controllers.bl_32.BaseStation3200 |
                                common.objects._controllers.bl_10.BaseStation1000 \n

        :param _program_ad:     Current condition number for _controller \n
        :type _program_ad:      int \n

        :param _device_type:    The type of device attached to this start condition. \n
        :type _device_type:     str \n

        :param _device_serial:  Current condition number for _controller. \n
        :type _device_serial:   str \n
        """
        # init base class
        StartCondition.__init__(self,
                                _controller,
                                _program_ad,
                                _device_type,
                                _device_serial)


class DateStartCondition1000(StartCondition):
    """
    Date/Calendar Start Condition for 3200. This inherits off just start condition since it is 1000 specific.
    """

    def __init__(self, _controller, _program_ad, _condition_address):
        """
        Temperature Start Condition Init \n

        :param _controller:     The _controller that will be passed in to each water source made by this method. \n
        :type _controller:      common.objects._controllers.bl_32.BaseStation3200 |
                                common.objects._controllers.bl_10.BaseStation1000 \n

        :param _program_ad:     Current condition number for _controller \n
        :type _program_ad:      int \n

        :param _condition_address:  Current condition number for _controller. \n
        :type _condition_address:   int \n
        """
        # init base class
        StartCondition.__init__(
            self,
            _controller=_controller,
            _program_ad=_program_ad,
            _device_type=opcodes.date_time,
            _device_serial="NO SERIAL NUMBER",
            _identifiers=[[ProgramConditionCommands.Type.START, _condition_address],
                          [opcodes.type, opcodes.date_time]]
        )

    def set_day_time_start(self, _dt='TR', _st_list=None,  _interval_type=None, _interval_args=None):
        """
        Sets the date time start condition \n
        Adds ability to start at a date & time for the condition. \n
        :param _dt:                 Start on date & time (i.e., 'TR'=opcodes.true | 'FA'=opcodes.false)
        :type _dt:                  str \n
        :param _st_list:            A list of start times
        :type _st_list:             list[int] \n
        :param _interval_type:      Type of interval - 'CI' | 'DI' | 'WD' | 'SI'
                                    i.e.:
                                        - 'CI': Calendar Interval - 'EV' | 'OD' | 'OS'
                                        - 'DI': Day Interval - 1 to xx days
                                        - 'WD': Week Days (Sunday-Saturday) - binary days of week:
                                            - WD='0111110' says water during the week only
                                        - 'SI': Semi-month Interval - list of 24 integers
        :type _interval_type:       str \n
        :param _interval_args:      Respective data for the corresponding interval type.
        :type _interval_args:       str | list[int] | int
        :return:                    A string of key value pairs to be sent to the controller \n
        :rtype:                     str \n

        Example Usage:
        1. Calendar Interval / Even Days
            - Below are equivalent:
                1.  some_start_condition.add_start_on_date_time_to_condition(interval_type=opcodes.calendar_interval,
                                                                             interval_args=opcodes.even_day)
                2.  some_start_condition.add_start_on_date_time_to_condition(interval_type='CI',
                                                                             interval_args='EV')

        2. Semi-Month Interval
            -   semi_month_list = [
                    1, 1,       # January 1-15, January 16-31
                    1, 1,       # February 1-15, February 16-End of month
                    1, 1,       # March 1-15, March 16-31
                    1, 1,       # April 1-15, April 16-30
                    1, 1,       # May 1-15, May 16-31
                    1, 1,       # June 1-15, June 16-30
                    1, 1,       # July 1-15, July 16-31
                    1, 1,       # August 1-15, August 16-31
                    1, 1,       # September 1-15, September 16-30
                    1, 1,       # October 1-15, October 16-31
                    1, 1,       # November 1-15, November 16-30
                    1, 1        # December 1-15, December 16-31
                ]
            -   some_start_condition.add_start_on_date_time_to_condition(interval_type=opcodes.semi_month_interval,
                                                                         interval_args=[semi_month_list])

        3. Week Day - Water only on weekends
            -   week_day_list = [
                    1,      # Sunday \n
                    1,      # Monday \n
                    1,      # Tuesday \n
                    1,      # Wednesday \n
                    1,      # Thursday \n
                    1,      # Friday \n
                    1       # Saturday \n
                ] \n
            -   some_start_condition.add_start_on_date_time_to_condition(interval_type=opcodes.week_days,
                                                                         interval_args=weed_day_list)
        """
        if _dt not in [opcodes.true, opcodes.false]:
            e_msg = "The 'dt' value passed in is not valid. Expected either '{0}' or '{1}', got '{2}'.".format(
                opcodes.true,
                opcodes.false,
                _dt
            )
            raise ValueError(e_msg)
        base_cmd_str = "SET,{0}={1},{2}={3},{4}={5}".format(
            opcodes.program_start_condition,
            self.program_ad,
            opcodes.type,
            opcodes.date_time,
            opcodes.date_time,
            _dt
        )

        self._device_type = opcodes.date_time
        self._dt = _dt

        if _st_list is not None:
            if self.verify_valid_start_times(_list_of_times=_st_list):
                st_list = self.build_start_time_string(_st_list)
                base_cmd_str += ",{0}={1}".format(
                    opcodes.start_times,
                    st_list
                )
                self._st = st_list

        _int_ty = ''
        # Calendar Interval
        if _interval_type == opcodes.calendar_interval:

            # check against valid calendar intervals
            if _interval_args in [opcodes.even_day, opcodes.odd_day, opcodes.odd_days_skip_31]:
                _int_ty = _interval_args
                self._ci = _int_ty
            else:
                e_msg = "Invalid calendar interval arg for start time: {entered}. Valid arguments: 'EV' | 'OD' | 'OS'" \
                    .format(
                        entered=_interval_args
                    )
                raise ValueError(e_msg)

        # Day Interval
        elif _interval_type == opcodes.day_interval:

            # must be at least 1
            if _interval_args >= 1:
                _int_ty = _interval_args
                self._di = _int_ty
            else:
                e_msg = "Invalid day interval arg for start time: {entered}. Valid arguments: 1 to xx days" \
                    .format(
                            entered=_interval_args
                    )
                raise ValueError(e_msg)

        # Week Day Interval
        elif _interval_type == opcodes.week_days:

            # verify list has 7 elements of only a 0 or 1
            if self.verify_valid_week_day_list(_week_day_list=_interval_args):
                _int_ty = self.build_week_day_string(_wd_list=_interval_args)
                self._wd = _int_ty

        # Semi-month Interval
        elif _interval_type == opcodes.semi_month_interval:

            # verify there is at least 1 start time, but not more than 8 (max)
            if self.verify_valid_semi_month_interval_list(_semi_month_interval_list=_interval_args):
                _int_ty = self.build_semi_month_interval_string(_sm_list=_interval_args)
                self._smi = _int_ty

        if _interval_type is not None and _int_ty is not None:
            base_cmd_str += ",{0}={1}".format(
                _interval_type,
                _int_ty
            )

        self._interval_ty = _interval_type

        try:
            self.ser.send_and_wait_for_reply(tosend=base_cmd_str)
        except Exception as e:
            e_msg = "Exception occurred when trying to set a date/time condition. Command: {0}".format(
                base_cmd_str
            )
            raise Exception(e_msg)
        else:
            print "Successfully set condition for program '{0}'. Command: {1}.".format(
                self.program_ad,
                base_cmd_str
            )

    #################################
    def build_start_time_string(self, _st_list):
        """
        Builds the correct start time string in correct format for sending to the controller. \n
        :param _st_list:    List of start times as integers for sending to the controller. \n
        :type _st_list:     list[int] \n
        :return:            Returns a string of start times for sending to the controller. \n
        :rtype:             str \n
        """
        string_for_return = ''

        # Iterate through each start time
        for index, each_time in enumerate(_st_list):
            # Don't want to append an equal sign at the front of this string
            if index == 0:
                string_for_return += str(each_time)
            else:
                # adding/appending '=400' to the string for example
                string_for_return += ("=" + str(each_time))

        # Example output for 3 start times: '480=720=900'
        return string_for_return

    #################################
    def build_semi_month_interval_string(self, _sm_list):
        """
        Builds the semi month interval string for sending to controller in the correct format. \n
        :param _sm_list:    List of 24 integers to set for semi month interval. \n
        :type _sm_list:     list[int] \n
        :return:            Returns properly formatted string for sending to controller. \n
        :rtype:             String \n
        """
        formatted_string = ''

        # Loop through the list of integers
        for index, each_int in enumerate(_sm_list):

            # Don't want to append an equal sign before the first integer in the string
            if index == 0:
                formatted_string += str(each_int)
            else:
                formatted_string += "=" + str(each_int)

        # expected returned string format example: '1=1=9=1=29=1=3=2=4=1=1=1=1=2=3=4=2=1=1=9=15=19=1=1'
        return formatted_string

    #################################
    def build_week_day_string(self, _wd_list):
        """
        Builds the week day string for sending to controller in correct format. \n
        :param _wd_list:    List containing 7 values (1 for each day of the week), first value being sunday \n
        :type _wd_list:     list[int]  \n
        :return:            Returns properly formatted string for week day water schedule. \n
        :rtype:             String \n
        """
        formatted_string = ''

        # Loop through the list of integers
        for each_int in _wd_list:
            formatted_string += str(each_int)

        # expected returned string format example: '1100111'
        return formatted_string

    #################################
    def verify_valid_week_day_list(self, _week_day_list):
        """
        Verifies a valid week day list of integers for setting the calendar interval. \n
        :param _week_day_list:  A list of 7 integers, the first being Sunday, second being Monday and so on. \n
        :type _week_day_list:   list[int] \n
        :return:                A valid list of week day integers for program \n
        :rtype:                     list[int] \n
        """
        # Check for proper list length
        if len(_week_day_list) != 7:
            e_msg = "Invalid week day water schedule argument passed in. List must contain 7 comma separated " \
                    "integers, instead found: {0}".format(
                        str(len(_week_day_list))  # {0}
                    )
            raise ValueError(e_msg)

        # Check all list elements to make sure they are only 0's and 1's using a regular expression. If a non '0' or
        # '1' is found, then an exception raised.
        elif any(re.search(r'\D|[2-9]+|[0-9][0-9]+', str(each_day)) for each_day in _week_day_list):
            e_msg = "Invalid list of week day values for Program {0}. Expects only 1's and/or 0's".format(
                str(self.program_ad))
            raise ValueError(e_msg)

        # Valid week day list
        else:
            return _week_day_list

    #################################
    def verify_valid_semi_month_interval_list(self, _semi_month_interval_list):
        """
        Verifies a valid semi month interval list of 24 integers, two for each month. \n
        :param _semi_month_interval_list:   A list of 24 integers. \n
        :type _semi_month_interval_list:        list[int]  \n
        :return: Whether or not the semi-month interval list is correct syntax
        :rtype: bool
        """
        # Check for proper list length
        if len(_semi_month_interval_list) != 24:
            e_msg = "Invalid semi month interval argument passed in. List must contain 24 comma separated integers, " \
                    "instead found: {0}".format(
                        str(len(_semi_month_interval_list))  # {0}
                    )
            raise ValueError(e_msg)

        # Else statement represents valid
        else:
            return True

    #################################
    def verify_valid_start_times(self, _list_of_times):
        """
        Verifies a valid list of start times. \n
        :param _list_of_times:      List of up to 8 start time integers \n
        :type _list_of_times:       list[int]  \n
        :return: If the list is valid
        :rtype: bool
        """
        # Check for correct number of start times
        if len(_list_of_times) == 0 or len(_list_of_times) > 8:
            e_msg = "Invalid number of start times to set for Start condition: {0}. Expects 1 to 8 start times, " \
                    "received: {1}".format(
                        str(self.program_ad),       # {0}
                        str(len(_list_of_times))    # {1}
                    )
            raise ValueError(e_msg)

        else:
            # Valid list of start times, return
            return True

    #################################
    def verify_interval_on_cn(self):
        """
        Verifies the interval associated with this StartStopPause object that was set on the controller. \n
        :return:
        """
        it_on_cn = self.data.get_value_string_by_key(self._interval_ty)

        if self._interval_ty == opcodes.calendar_interval:
            interval = self._ci
        elif self._interval_ty == opcodes.day_interval:
            interval = self._di
        elif self._interval_ty == opcodes.week_days:
            interval = self._wd
        elif self._interval_ty == opcodes.semi_month_interval:
            interval = self._smi
        else:
            e_msg = "The interval type '{0} is not valid. The valid interval types are ({1}, {2}, {3}, {4}). ".format(
                self._interval_ty,
                opcodes.calendar_interval,
                opcodes.day_interval,
                opcodes.week_days,
                opcodes.semi_month_interval
            )
            raise ValueError(e_msg)

        # Compare intervals
        if interval != it_on_cn:
            e_msg = "Unable to verify StartStopPause Condition: {0}'s interval. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.program_ad),       # {0}
                        str(it_on_cn),              # {1}
                        str(interval))              # {2}
            raise ValueError(e_msg)
        else:
            print("Verified StartStopPause({0}) Condition: {1}'s interval: '{2}' on controller".format(
                str(self._interval_ty),      # {0}
                str(self.program_ad),       # {1}
                interval                    # {2}
            ))

    #################################
    def verify_date_time_enabled_on_cn(self):
        """
        Verifies the date time associated with this StartStopPause object that was set on the controller. \n
        :return:
        """
        dt_en_on_cn = self.data.get_value_string_by_key(opcodes.date_time)

        # Compare date time enabled
        if self._dt != dt_en_on_cn:
            e_msg = "Unable to verify StartStopPause({0}) Condition: {1}'s date time. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self.interval_ty),      # {0}
                        str(self.program_ad),       # {1}
                        str(dt_en_on_cn),           # {2}
                        str(self._dt))              # {3}
            raise ValueError(e_msg)
        else:
            print "Verified StartStopPause({0}) Condition: {1}'s date time: '{2}' on controller".format(
                str(self.interval_ty),      # {0}
                str(self.program_ad),       # {1}
                dt_en_on_cn                 # {2}
            )

    #################################
    def verify_start_times_on_cn(self):
        """
        Verifies the start times associated with this StartStopPause object that was set on the controller. \n
        :return:
        """
        st_on_cn = self.data.get_value_string_by_key(opcodes.start_times)

        # Compare date time enabled
        if self._st != st_on_cn:
            e_msg = "Unable to verify StartStopPause({0}) Condition: {1}'s start times. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self.interval_ty),      # {0}
                        str(self.program_ad),       # {1}
                        str(st_on_cn),              # {2}
                        str(self._st))              # {3}
            raise ValueError(e_msg)
        else:
            print "Verified StartStopPause({0}) Condition: {1}'s start times: '{2}' on controller".format(
                str(self.interval_ty),      # {0}
                str(self.program_ad),       # {1}
                st_on_cn                    # {2}
            )

    #################################
    def verify_who_i_am(self):
        """
        Verifies this program's values against what the controller has. \n
        """
        self.get_data()

        if self._dt is not None:
            self.verify_date_time_enabled_on_cn()
        if self._st is not None:
            self.verify_start_times_on_cn()
        if self._interval_ty is not None:
            self.verify_interval_on_cn()
