import re
from common.imports import opcodes
from common.objects.base_classes.programs import BasePrograms
from common.imports.types import ActionCommands, ProgramCommands
from common.objects.messages.programming.pg_3200_messages import ProgramMessages


__author__ = 'baseline'


########################################################################################################################
# 3200 Program Class
########################################################################################################################
class PG3200(BasePrograms):
    """
    3200 Program Object \n
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    mainline_objects = None

    # Controller firmware version
    cn_firmware_version = None

    # obj_type = opcodes.program + "32"

    #################################
    def __init__(self, _controller, _ad):
        """
        3200 Program constructor. \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.base_classes.controller.BaseController \n

        :param _ad:     Program number \n
        :type _ad:      int \n

        :param _controller:
        :type _controller:

        """
        # Init parent class
        BasePrograms.__init__(self, _controller=_controller, _ad=_ad)

        # create message reference to object
        self.messages = ProgramMessages(_program_object=self)  # program messages
        """:type: common.objects.messages.programming.pg_3200_messages.ProgramMessages"""

        # ----- Instance Variables ----- #
        # Calendar Interval
        self.ci = ProgramCommands.Attributes.WEEKDAYS
        # Day Interval
        self.di = 1
        # Week Days (in binary, 1's and 0's)
        self.wd = [
            1,          # Sunday
            1,          # Monday
            1,          # Tuesday
            1,          # Wednesday
            1,          # Thursday
            1,          # Friday
            1           # Saturday
        ]
        # Semi-Monthly Watering (opcode: CI='ET')
        self.sm = [
            1, 1,       # January 1-15, January 16-31
            1, 1,       # February 1-15, February 16-End of month
            1, 1,       # March 1-15, March 16-31
            1, 1,       # April 1-15, April 16-30
            1, 1,       # May 1-15, May 16-31
            1, 1,       # June 1-15, June 16-30
            1, 1,       # July 1-15, July 16-31
            1, 1,       # August 1-15, August 16-31
            1, 1,       # September 1-15, September 16-30
            1, 1,       # October 1-15, October 16-31
            1, 1,       # November 1-15, November 16-30
            1, 1        # December 1-15, December 16-31
        ]
        # Start times (minutes past midnight)
        self.st = [
            480
        ]
        # event stop dates, 0 to 8
        self.ed = [
        ]
        # Priority Level
        self.pr = 2                 # Priority
        # Booster Pump
        self.bp = None              # Booster Pump (Serial number of MV | None)
        # Water Ration Percentage
        self.wr = 20    # Arbitrary value between 0-100
        self.bp_type = None # can be opcodes.pump or opcodes.master_valves
        # Calendar Interval Case
        self.calendar_interval_dict = {
            'EV': None,
            'OD': None,
            'OS': None,
            'ET': self.sm,
            'ID': self.di,
            'WD': self.wd
        }
        self.ic = opcodes.false  # ignore concurrent zones
        self.ig = opcodes.false  # ignore global conditions

    #################################
    def send_programming(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """

        command = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14},{15}={16},{17}={18},{19}={20}".format(
            ActionCommands.SET,                                 # {0}
            ProgramCommands.Program,                            # {1}
            str(self.ad),                                       # {2}
            ProgramCommands.Attributes.ENABLED,                 # {3}
            str(self.en),                                       # {4}
            ProgramCommands.Attributes.DESCRIPTION,             # {5}
            str(self.ds),                                       # {6}
            ProgramCommands.Attributes.WATER_WINDOW,            # {7}
            str(''.join(str(x) for x in self.ww)),              # {8}
            ProgramCommands.Attributes.MAX_CONCURRENT_ZONES,    # {9}
            str(self.mc),                                       # {10}
            ProgramCommands.Attributes.SEASONAL_ADJUST,         # {11}
            str(self.sa),                                       # {12}
            ProgramCommands.Attributes.CALENDAR_INTERVALS,      # {13}
            str(self.ci),                                       # {14}
            ProgramCommands.Attributes.WEEKDAYS,                # {15}
            str(''.join(str(x) for x in self.wd)),              # {16}
            ProgramCommands.Attributes.START_TIMES,             # {17}
            str('='.join(str(x) for x in self.st)),             # {18}
            ProgramCommands.Attributes.PRIORITY,                # {19}
            str(self.pr),                                       # {20}
        )
        try:
            # Attempt to set program default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Values' to: '{1}' -> {2}".format(
                str(self.ad),   # {0}
                command,        # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0}'s 'Values' to: {1}".format(
                  str(self.ad),     # {0}
                  command           # {1}
                  ))

    #################################
    def set_watering_intervals_to_semi_monthly(self, _sm=None):
        """
        Sets the calendar interval for the program on the controller. \n

        :param _sm:     Semi month interval days \n
        :type _sm:      list[int] \n

        """

        # -------------------------------------------------------------------------------------------------------------
        # case 1: the historical et calendar setting
        self.ci = ProgramCommands.Attributes.HISTORICAL_ET_CALENDER

        if _sm is not None:
            self.sm = self.verify_valid_semi_month_interval_list(_semi_month_interval_list=_sm)
        else:
            self.sm = [0, 0, 0, 0 , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        value_for_key = self.build_semi_month_interval_string(_sm_list=self.sm)

        # create command string
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                                 # {0}
            ProgramCommands.Program,                            # {1}
            str(self.ad),                                       # {2}
            ProgramCommands.Attributes.CALENDAR_INTERVALS,      # {3}
            str(self.ci),                                       # {4}
            ProgramCommands.Attributes.SEMI_MOTHLY_WATERING,    # {5}
            value_for_key                                       # {6}
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Calendar Interval' to: {1} -> {2}".format(
                str(self.ad),  # {0}
                command,  # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Calendar Interval' for Program: {0} to: {1}".format(
                str(self.ad),  # {0}
                command  # {1}
            ))
            #################################

    def set_watering_intervals_to_day_intervals(self, _di=1):
        """
        Sets the calendar interval for the program on the controller. \n

        :param _ci:     Calendar Interval, accepted values are: 'ID'\n
        :type _ci:      str \n

        :param _di:     Number of days for regular intervals \n
        :type _di:      int \n

        """

        # case 2: daily interval
        self.ci = ProgramCommands.Attributes.INTERVAL_DAYS

        if not isinstance(_di, int):
            e_msg = 'intervals must be and integer'
            raise ValueError(e_msg)
        self.di = _di
        # create command string
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            ProgramCommands.Program,                        # {1}
            str(self.ad),                                   # {2}
            ProgramCommands.Attributes.CALENDAR_INTERVALS,  # {3}
            str(self.ci),                                   # {4}
            ProgramCommands.Attributes.DAY_INTERVALS,       # {5}
            str(self.di)                                    # {6}
            )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Day Interval' to: {1} -> {2}".format(
                str(self.ad),  # {0}
                command,  # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Day Interval' for Program: {0} to: {1}".format(
                str(self.ad),  # {0}
                command  # {1}
            ))
            
    #################################
    def set_watering_intervals_to_all_days_of_the_week(self):
        """
        Sets the calendar interval for the program on the controller to water "Daily" and enable all days in the week:
        Sun, Mon, Tue, Wed, Thu, Fri, Sat \n
        """
        sun = 1
        mon = 1
        tue = 1
        wed = 1
        thu = 1
        fri = 1
        sat = 1
    
        # Convert all days in week into binary string of 1's and 0's
        weekday_string = '{0}{1}{2}{3}{4}{5}{6}'.format(sun, mon, tue, wed, thu, fri, sat)
    
        self.ci = ProgramCommands.Attributes.WEEKDAYS
    
        # create command string
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            ProgramCommands.Program,                        # {1}
            str(self.ad),                                   # {2}
            ProgramCommands.Attributes.CALENDAR_INTERVALS,  # {3}
            self.ci,                                        # {4}
            ProgramCommands.Attributes.WEEKDAYS,            # {5}
            weekday_string                                  # {6}
        )
    
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Calendar Interval' to: {1} -> {2}".format(
                str(self.ad),  # {0}
                command,  # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Calendar Interval' for Program: {0} to: {1}".format(
                str(self.ad),  # {0}
                command        # {1}
            ))

    #################################
    def set_watering_intervals_to_selected_days_of_the_week(self, _sun=True, _mon=True, _tues=True, _wed=True,
                                                            _thurs=True, _fri=True, _sat=True):
        """
        Sets the calendar interval for the program on the controller. \n
        :param _sun:
        :type _sun:
        :param _mon:
        :type _mon:
        :param _tues:
        :type _tues:
        :param _wed:
        :type _wed:
        :param _thurs:
        :type _thurs:
        :param _fri:
        :type _fri:
        :param _sat:
        :type _sat:
        """
        if _sun and _mon and _tues and _wed and _thurs and _fri and _sat not in [True, False]:
            e_msg = 'all values must be true or false'
            raise ValueError(e_msg)

        if _sun is False:
            _sun = 0
        else:
            _sun = 1
        if _mon is False:
            _mon = 0
        else:
            _mon = 1
        if _tues is False:
            _tues = 0
        else:
            _tues = 1
        if _wed is False:
            _wens = 0
        else:
            _wens = 1
        if _thurs is False:
            _thurs = 0
        else:
            _thurs = 1
        if _fri is False:
            _fri = 0
        else:
            _fri = 1
        if _sat is False:
            _sat = 0
        else:
            _sat = 1
        self.wd = [_sun, _mon, _tues, _wens, _thurs, _fri, _sat]

        weekday_string = '{0}{1}{2}{3}{4}{5}{6}'.format(_sun, _mon, _tues, _wens, _thurs, _fri, _sat)

        self.ci = ProgramCommands.Attributes.WEEKDAYS

        # create command string
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                             # {0}
            ProgramCommands.Program,                        # {1}
            str(self.ad),                                   # {2}
            ProgramCommands.Attributes.CALENDAR_INTERVALS,  # {3}
            self.ci,                                        # {4}
            ProgramCommands.Attributes.WEEKDAYS,            # {5}
            weekday_string                                  # {6}
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Calendar Interval' to: {1} -> {2}".format(
                str(self.ad),  # {0}
                command,  # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Calendar Interval' for Program: {0} to: {1}".format(
                str(self.ad),  # {0}
                command  # {1}
            ))

    def set_watering_intervals_to_even_days(self):
        """
        Sets the calendar interval for the program on the controller. \n

        :param _ci:     Calendar Interval, accepted values are: 'EV'.\n
        :type _ci:      str \n

        """
        self.ci = ProgramCommands.Attributes.EVEN_DAYS

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                             # {0}
            ProgramCommands.Program,                        # {1}
            str(self.ad),                                   # {2}
            ProgramCommands.Attributes.CALENDAR_INTERVALS,  # {3}
            str(self.ci),                                   # {4}

            )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Calendar Interval' to: {1} -> {2}".format(
                str(self.ad),  # {0}
                command,  # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Calendar Interval' for Program: {0} to: {1}".format(
                str(self.ad),  # {0}
                command  # {1}
            ))

    def set_watering_intervals_to_odd_days(self):
        """
        Sets the calendar interval for the program on the controller. \n

        :param _ci:     Calendar Interval, 'OD', \n
        :type _ci:      str \n

        """
        self.ci = ProgramCommands.Attributes.ODD_DAYS

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                             # {0}
            ProgramCommands.Program,                        # {1}
            str(self.ad),                                   # {2}
            ProgramCommands.Attributes.CALENDAR_INTERVALS,  # {3}
            str(self.ci),                                   # {4}

            )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Calendar Interval' to: {1} -> {2}".format(
                str(self.ad),  # {0}
                command,  # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Calendar Interval' for Program: {0} to: {1}".format(
                str(self.ad),  # {0}
                command  # {1}
            ))

    def set_watering_intervals_to_odd_days_but_skipping_the_31st_day_of_the_month(self):
        """
        Sets the calendar interval for the program on the controller. \n

        :param _ci:     Calendar Interval, accepted values are:'OS'.\n
        :type _ci:      str \n

        """
        self.ci = ProgramCommands.Attributes.ODD_DAYS_SKIP_31

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                             # {0}
            ProgramCommands.Program,                        # {1}
            str(self.ad),                                   # {2}
            ProgramCommands.Attributes.CALENDAR_INTERVALS,  # {3}
            str(self.ci),
            )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Calendar Interval' to: {1} -> {2}".format(
                str(self.ad),  # {0}
                command,  # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Calendar Interval' for Program: {0} to: {1}".format(
                str(self.ad),  # {0}
                command  # {1}
                ))

    #################################
    def set_start_times(self, _st_list=None):
        """
        Sets the start times for the 3200 program on the controller. \n
        :param _st_list:    List of up to 8 integers representing the time in minutes past midnight \n
        :type _st_list:     list[int] \n
        :return:
        """
        # check to see if a start time list is passed in to overwrite current start time list
        if _st_list is not None:
            self.st = self.verify_valid_start_times(_list_of_times=_st_list)

        # Build start time string for sending to controller
        built_st_string = self.build_start_time_string(_st_list=self.st)

        # Command for sending to controller
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                         # {0}
            ProgramCommands.Program,                    # {1}
            str(self.ad),                               # {2}
            ProgramCommands.Attributes.START_TIMES,     # {3}
            built_st_string                             # {4}
        )

        # Attempt to send command to controller
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Start Times' to: {1} -> {2}".format(
                str(self.ad),       # {0}
                built_st_string,    # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Start Times' for Program: {0} to: {1}".format(
                  str(self.ad),     # {0}
                  built_st_string   # {1}
                  ))

    #################################
    def set_event_stop_dates(self, _date_list=None):
        """
        Sets the event stop dates on a program. \n
        :param _date_list:    List of up to 8 strings representing the date: mm/dd/yy \n
        :type _date_list:     list[str] \n
        :return:
        """
        # check to see if a start time list is passed in to overwrite current start time list
        if _date_list is not None:
            self.ed = self.verify_valid_event_stop_dates(_list_of_dates=_date_list)

        # Build event date string for sending to controller
        build_event_dates_string = self.build_event_stop_dates_string(_ed_list=self.ed)

        # Command for sending to controller
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                         # {0}
            ProgramCommands.Program,                    # {1}
            str(self.ad),                               # {2}
            opcodes.event_day,                          # {3}
            build_event_dates_string                    # {4}
        )

        # Attempt to send command to controller
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Event Dates' to: {1} -> {2}".format(
                str(self.ad),       # {0}
                build_event_dates_string,    # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Event Dates' for Program: {0} to: {1}".format(
                  str(self.ad),     # {0}
                  build_event_dates_string   # {1}
                  ))

    #################################
    def set_priority_level(self, _pr_level=None):
        """
        Sets the water priority for the current program on the controller using the serial object. \n
        :param _pr_level:   Water priority level to overwrite current instance value. \n
        :type _pr_level:    int \n
        """
        # Check for overwrite
        if _pr_level is not None:
            self.pr = self.verify_valid_priority_level(_priority=_pr_level)

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                         # {0}
            ProgramCommands.Program,                    # {1}
            str(self.ad),                               # {2}
            ProgramCommands.Attributes.PRIORITY,        # {3}
            str(self.pr)                                # {4}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Priority Level' to: {1} -> {2}".format(
                str(self.ad),   # {0}
                str(self.pr),   # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Priority Level' for Program: {0} to: {1}".format(
                str(self.ad),   # {0}
                str(self.pr)    # {1}
            ))



    #################################
    def set_ignore_zone_concurrency(self):
        """
        this will cause the zones in this program to not be added to the zone count of the controller
        :return:
        """
        self.ic = opcodes.true

        # Command for sending to controller.
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                                     # {0}
            ProgramCommands.Program,                                # {1}
            str(self.ad),                                           # {2}
            ProgramCommands.Attributes.IGNORE_ZONE_CONCURRENCY,     # {3}
            str(self.ic)                                            # {4}
        )
        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} to ignore zone concurrency to: {1}"\
                .format(
                    self.ds,
                    self.ic
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} to ignore zone concurrency to: {1}"
                .format(
                    self.ds,
                    self.ic
                    ))

    #################################
    def set_to_obey_zone_concurrency(self):
        """
        this will cause the zones in this program to not be added to the zone count of the controller
        :return:
        """
        self.ic = opcodes.false

        # Command for sending to controller.
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                                     # {0}
            ProgramCommands.Program,                                # {1}
            str(self.ad),                                           # {2}
            ProgramCommands.Attributes.IGNORE_ZONE_CONCURRENCY,     # {3}
            str(self.ic)                                            # {4}
        )
        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} to ignore zone concurrency to: {1}"\
                .format(
                    self.ds,
                    self.ic
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} to not ignore zone concurrency to: {1}"
                .format(
                    self.ds,
                    self.ic
                    ))

    #################################
    def set_ignore_global_conditions(self):
        """
        this will cause the program to ignore condition like global rain delay
        :return:
        """
        self.ig = opcodes.true

        # Command for sending to controller.
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                                     # {0}
            ProgramCommands.Program,                                # {1}
            str(self.ad),                                           # {2}
            ProgramCommands.Attributes.IGNORE_GLOBAL_CONDITIONS,    # {3}
            str(self.ig)                                            # {4}
        )
        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} to ignore global conditions to: {1}"\
                .format(
                    self.ds,
                    self.ig
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} to ignore global conditions to: {1}"
                .format(
                    self.ds,
                    self.ic
                    ))

    #################################
    def set_to_obey_global_conditions(self):
        """
        this will cause the program to obey condition like global rain delay
        :return:
        """
        self.ig = opcodes.false

        # Command for sending to controller.
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                                     # {0}
            ProgramCommands.Program,                                # {1}
            str(self.ad),                                           # {2}
            ProgramCommands.Attributes.IGNORE_GLOBAL_CONDITIONS,    # {3}
            str(self.ig)                                            # {4}
        )
        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0} to obey global conditions to: {1}"\
                .format(
                    self.ds,
                    self.ig
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0} to obey global conditions to: {1}"
                .format(
                    self.ds,
                    self.ig
                    ))

    #################################
    def set_disabled(self):
        """
        Disable object. \n

        :return:
        """
        self.en = opcodes.false

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,     # {0}
            self.get_id(),          # {1} Builds the identifier for this object
            opcodes.enabled,        # {2}
            str(self.en)            # {3}
            )

        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} enabled state to: {1}".format(self.ds, self.en)
            raise Exception(e_msg)
        else:
            print( "Successfully set {0} enabled state to: {1}".format(self.ds, self.en))
    #################################
    def add_booster_pump(self, _mv_address=None, _pm_address=None):
        """
        Sets the booster pump for the program to the master valve based on the address of the master valve passed in
        as an argument on the controller. \n
        :param _mv_address:  address of a master valve to assign to program as booster pump. \n
        :type _mv_address:   int | str | None \n

        :param _pm_address:  address of a pump to assign to program as booster pump. \n
        :type _pm_address:   int | str | None \n
        """
        object_serial_number = ''
        # Check for overwrite
        if _mv_address is not None and isinstance(_mv_address, int):
            self.bp = _mv_address
            self.bp = self.verify_valid_booster_pump(_booster=_mv_address, master=True)
            bp_obj = self.controller.master_valves[self.bp]
            object_serial_number = bp_obj.sn
            self.bp_type = opcodes.master_valve
        # Check for overwrite
        elif _pm_address is not None and isinstance(_pm_address, int):
            self.bp = _pm_address
            self.bp = self.verify_valid_booster_pump(_booster=_pm_address, pump=True)
            bp_obj = self.controller.pumps[self.bp]
            object_serial_number = bp_obj.sn
            self.bp_type = opcodes.pump

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                         # {0}
            ProgramCommands.Program,                    # {1}
            str(self.ad),                               # {2}
            ProgramCommands.Attributes.BOOSTER_PUMP,    # {3}
            object_serial_number                        # {4}
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Program {0}'s 'Booster Pump' to: {1}({2}) -> {3}".format(
                str(self.ad),   # {0}
                object_serial_number,      # {1}
                self.bp,        # {2}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Booster Pump' for Program: {0} to: {1}({2})".format(
                str(self.ad),   # {0}
                object_serial_number,      # {1}
                self.bp         # {2}
            ))

    def set_learn_flow_date(self,date=None):
        # TODO Still need to code
        pass

    #################################
    def verify_start_times(self):
        """
        Verifies the list of start times returned from the controller. \n
        """
        # When you pass the controller a [''] it sets the controller to not have a start time
        list_of_start_times_from_cn = []
        start_times = self.data.get_value_string_by_key(opcodes.start_times)
        if start_times:
            list_of_start_times_from_cn = map(int, start_times.split('='))

        # Compare versus what is on the controller
        if self.st != list_of_start_times_from_cn:
            e_msg = "Unable to verify 3200 Program {0}'s 'Start Times'. Received: {1}, Expected: {2}".format(
                str(self.ad),                       # {0}
                str(list_of_start_times_from_cn),   # {1}
                str(self.st)                        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 3200 Program {0}'s 'Start Times': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.st))   # {1}
            )

    #################################
    def verify_calendar_interval(self, unit_test_value=1):
        """
        Verifies the calendar interval for the instance against what the controller has programed. \n
        """

        # Case 1: Historical ET Calendar, compare Semi-Month list values
        if self.ci == opcodes.historical_calendar:
            # Two things happen,
            # First, self.data.get_key_value_by_key() returns a string with integers separated by '='
            # Second, ".split('=')" creates a list from the string returned with each list element being type 'str'
            # Last, use 'map(int, blah)' to cast all list elements to the 'int' type.
            # This allows us to compare the 'self.sm' list of 'int' types against the result of this string,
            # lists should be equal
            key = opcodes.program_semi_month_interval
            expected_value = self.sm
            if unit_test_value is not 1:
                received_value = unit_test_value
            else:
                received_value = map(int, self.data.get_value_string_by_key(
                    opcodes.program_semi_month_interval).split("="))

        # Case 2: Week days, compare Weekday list values
        elif self.ci == opcodes.week_days:
            # Get the expected weekday list for comparison
            expected_value = self.wd

            # First, the output of 'self.data.get_key_value_by_key('WD')' is changed into list form by the 'list()' cast
            # Next, map(int, blah) converts all the list elements from strings to ints
            key = opcodes.week_days
            received_value = map(int, list(self.data.get_value_string_by_key(opcodes.week_days)))

        # Case 3: Day Interval, compare day interval values
        elif self.ci == opcodes.interval_days:
            # Get the expected Daily interval value from the local object
            expected_value = self.di

            # Get the Daily interval value from the data returned from the controller
            key = opcodes.day_interval
            received_value = int(self.data.get_value_string_by_key(opcodes.day_interval))

        # Case 4: Even, Odd or Odd Skip 31, verify
        else:
            # Get expected 'Even', 'Odd' or 'Odd Skip 31' calendar interval value
            expected_value = self.ci

            # Get the value for calendar interval set at the controller
            key = opcodes.calendar_interval
            received_value = self.data.get_value_string_by_key(opcodes.calendar_interval)

        # If received_value is None, then the key was not found in the data send by the controller located in self.data
        # Thus, the program must not have the expected programming.
        if received_value is None:
            e_msg = "Unable to verify 'Calendar Interval' type: {0} for current program. Unable to find a value " \
                    "associated with key: '{1}'. Current program doesn't have the expected 'Calendar Interval'".format(
                        self.ci,    # {0}
                        key         # {1}
                    )
            raise ValueError(e_msg)

        # Compare values
        elif expected_value != received_value:
            e_msg = "Unable to verify 'Calendar Interval' type: {0}, for current program. Expected:"\
                    "(Key: {1}, Value: {2}), Received: (Key: {3}, Value: {4})".format(
                        self.ci,            # {0}
                        key,                # {1}
                        expected_value,     # {2}
                        key,                # {3}
                        received_value      # {4}
                    )
            raise ValueError(e_msg)

        # Successful verification
        else:
            print("Verified 3200 Program {0}'s 'Calendar Interval': Type: '{1}', Value: {2}, on controller".format(
                  str(self.ad),   # {0}
                  self.ci,        # {1}
                  key             # {2}
                  ))

    #################################
    def verify_priority_level(self):
        """
        Verifies the Priority Level set for the 3200 program on the controller. \n
        """
        priority_level_from_cn = int(self.data.get_value_string_by_key(opcodes.priority))

        # Compare versus what is on the controller
        if self.pr != priority_level_from_cn:
            e_msg = "Unable to verify 3200 Program {0}'s 'Priority Level'. Received: {1}, Expected: {2}".format(
                    str(self.ad),                   # {0}
                    str(priority_level_from_cn),    # {1}
                    str(self.pr)                    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 3200 Program {0}'s 'Priority Level': '{1}' on controller".format(
                str(self.ad),   # {0}
                str(self.pr))   # {1}
            )


    #################################
    def verify_booster_pump(self):
        """
        Verifies the Booster Pump set for the 3200 program on the controller. \n
        """
        booster_pump = self.data.get_value_string_by_key(opcodes.booster_pump)
        curr_bp_obj_sn = ''

        # TODO...
        # We cannot set a booster pump value of 'FA' currently, thus we have to leave our object's attribute as ''
        # instead of 'FA'. When the controller sends back information from the self.get_data() call, the controller
        # responds with 'BP=FA' when no booster pump is assigned. Thus this is a temp fix.

        if self.bp is None:
            curr_bp_obj_sn = opcodes.false

        elif self.bp_type == opcodes.master_valve:
            curr_bp_object = self.controller.master_valves[self.bp]
            curr_bp_obj_sn = curr_bp_object.sn

        elif self.bp_type == opcodes.pump:
            curr_bp_object = self.controller.pumps[self.bp]
            curr_bp_obj_sn = curr_bp_object.sn

        # Compare versus what is on the controller
        if curr_bp_obj_sn != booster_pump:
            e_msg = "Unable to verify 3200 Program {0}'s 'Booster Pump'. Received: {1}, Expected: {2}".format(
                    str(self.ad),       # {0}
                    str(booster_pump),  # {1}
                    curr_bp_obj_sn      # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 3200 Program {0}'s 'Booster Pump': '{1}' on controller".format(
                str(self.ad),   # {0}
                curr_bp_obj_sn  # {1}
            ))

    #################################
    def verify_valid_semi_month_interval_list(self, _semi_month_interval_list):
        """
        Verifies a valid semi month interval list of 24 integers, two for each month. \n
        :param _semi_month_interval_list:   A list of 24 integers. \n
        :type _semi_month_interval_list:        list[int]  \n
        :return:                            A valid semi-month interval list of 24 integers
        :rtype:                                 list[int]  \n
        """
        # Check for proper list length
        if _semi_month_interval_list and len(_semi_month_interval_list) != 24:
            e_msg = "Invalid semi month interval argument passed in. List must contain 24 comma separated integers, " \
                    "instead found: {0}".format(
                        str(len(_semi_month_interval_list))  # {0}
                    )
            raise ValueError(e_msg)

        # Else statement represents valid
        else:
            return _semi_month_interval_list

    #################################
    def verify_valid_week_day_list(self, _week_day_list):
        """
        Verifies a valid week day list of integers for setting the calendar interval. \n
        :param _week_day_list:  A list of 7 integers, the first being Sunday, second being Monday and so on. \n
        :type _week_day_list:       list[int] \n
        :return:                A valid list of week day integers for program \n
        :rtype:                     list[int] \n
        """
        # Check for proper list length
        if len(_week_day_list) != 7:
            e_msg = "Invalid week day water schedule argument passed in. List must contain 7 comma separated " \
                    "integers, instead found: {0}".format(
                        str(len(_week_day_list))  # {0}
                    )
            raise ValueError(e_msg)

        # Check all list elements to make sure they are only 0's and 1's using a regular expression. If a non '0' or
        # '1' is found, then an exception raised.
        elif any(re.search(r'\D|[2-9]+|[0-9][0-9]+', str(each_day)) for each_day in _week_day_list):
            e_msg = "Invalid list of week day values for Program {0}. Expects only 1's and/or 0's".format(str(self.ad))
            raise ValueError(e_msg)

        # Valid week day list
        else:
            return _week_day_list
    #
    # #################################
    # def verify_valid_day_interval(self, _day_interval):
    #     """
    #     Verifies a valid day interval value entered for a program. \n
    #     :param _day_interval:   Day interval for program to water \n
    #     :type _day_interval:        Integer \n
    #     :return:                A valid day interval value for program \n
    #     :rtype:                     Integer \n
    #     """
    #     # Check that the interval passed in is an integer
    #     if not isinstance(_day_interval, int):
    #         e_msg = "Invalid interval argument ('_day_interval') type for Program {0}. Expected type: int, " \
    #                 "Received type: {1}".format(
    #                     str(self.ad),  # {0}
    #                     type(_day_interval)  # {1}
    #                 )
    #         raise TypeError(e_msg)
    #     else:
    #         return _day_interval

    #################################
    def verify_valid_start_times(self, _list_of_times):
        """
        Verifies a valid list of start times. \n
        :param _list_of_times:      List of up to 8 start time integers \n
        :type _list_of_times:       list[int]  \n
        :type _list_of_times:       Optional list[str]  \n
        :return:                    A valid list of up to 8 start times as integers. \n
        :rtype:                     list[int]  \n
        """
        # Check for correct number of start times
        if _list_of_times != '':
            if len(_list_of_times) > 8:
                e_msg = "Invalid number of start times to set for Program {0}. Expects 0 to 8 start times, received: " \
                        "{1}".format(
                            str(self.ad),               # {0}
                            str(len(_list_of_times))    # {1}
                        )
                raise ValueError(e_msg)

            else:
                # Valid list of start times, return
                return _list_of_times

    #################################
    def verify_valid_event_stop_dates(self, _list_of_dates):
        """
        Verifies a valid list of event stop dates. \n
        :param _list_of_dates:      List of up to 8 string dates: format=mm/dd/yy
        :return:                    Valid list of string dates
        """

        # Check for valid number of times
        if _list_of_dates != '':
            if len(_list_of_dates) > 8:
                e_msg = "Invalid number of event dates to set for Program {0}. Expects 0 to 8 event dates, received: " \
                        "{1}".format(
                            str(self.ad),               # {0}
                            str(len(_list_of_dates))    # {1}
                        )
                raise ValueError(e_msg)

            else:
                # Valid list of event dates, return
                return _list_of_dates


    #################################
    def verify_valid_booster_pump(self, _booster, master=False, pump=False, ):
        """
        Verifies a valid booster pump address by checking that it is in the dictionary. \n
        :param _booster:    A master valve serial number or 'FA'. \n
        :type _booster:     int | str\n
        :param master:
        :type master:       bool
        :param pump:
        :type pump:         bool
        :return:            A valid booster pump value. \n
        :rtype:             int \n
        """
    # Check valid type
        if master:
            if int(_booster) not in self.controller.master_valves.keys():
                e_msg = "Invalid Booster Pump address for 3200 Program. Valid address are: {bp_address}".format(
                    bp_address=self.controller.master_valves.keys()
                )
                raise IndexError(e_msg)
        # Check valid type
        if pump:
            if int(_booster) not in self.controller.pumps.keys():
                e_msg = "Invalid Booster Pump address for 3200 Program. Valid address are: {bp_address}".format(
                    bp_address=self.controller.pumps.keys()
                )
                raise IndexError(e_msg)
        # Valid booster pump
        return int(_booster)

    #################################
    def verify_valid_priority_level(self, _priority):
        """
        Verifies a valid priority level for program. \n
        :param _priority:   Priority level \n
        :type _priority:    Integer \n
        :return:            A valid priority level \n
        :rtype:             Integer \n
        """
        # Check valid value
        if _priority not in [1, 2, 3]:
            e_msg = "Invalid '_priority' (Priority Level) value for 'Set' function for Program {0}: {1}."\
                    " Accepted values are: 1=high | 2=medium | 3=low".format(
                        str(self.ad),       # {0}
                        str(_priority)      # {1}
                    )
            raise ValueError(e_msg)

        # Overwrite current value
        else:
            return _priority

    #################################
    def verify_ignore_zone_concurrency_state(self):
        """
        Verifies the Object's 'Enabled State' set on the controller. \n
        """
        # expect string type
        ic_state = self.data.get_value_string_by_key(opcodes.ignore_concurrent_zones)

        # Compare enabled state
        if self.ic != ic_state:
            e_msg = "Unable verify {0} 'ignore zone concurrency'. Received: {1}, Expected: {2}".format(
                self.ds,        # {0}
                str(ic_state),  # {1}
                self.ic         # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0}'s 'ignore zone concurrency state ': '{1}' on controller".format(
                self.ds,  # {0}
                self.ic   # {1}
            ))

    #################################
    def verify_ignore_global_conditions_state(self):
        """
        Verifies the Object's 'Enabled State' set on the controller. \n
        """
        # expect string type
        ig_state = self.data.get_value_string_by_key(opcodes.ignore_global_conditions)

        # Compare enabled state
        if self.ig != ig_state:
            e_msg = "Unable verify {0} 'ignore global conditions state '. Received: {1}, Expected: {2}".format(
                self.ds,        # {0}
                str(ig_state),  # {1}
                self.ig         # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0}'s 'ignore global conditons state ': '{1}' on controller".format(
                self.ds,  # {0}
                self.ig   # {1}
            ))

    #################################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this program's values against what the controller has. \n
        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()

        self.verify_description()
        self.verify_enabled_state()
        self.verify_water_window()
        self.verify_max_concurrent_zones()
        self.verify_seasonal_adjust()

        if expected_status is not None:
            self.verify_status(_expected_status=expected_status)

        self.verify_calendar_interval()
        self.verify_start_times()
        self.verify_priority_level()
        self.verify_ignore_zone_concurrency_state()
        self.verify_ignore_global_conditions_state()

        if self.bp:
            self.verify_booster_pump()

    #################################
    def build_start_time_string(self, _st_list):
        """
        Builds the correct start time string in correct format for sending to the controller. \n
        :param _st_list:    List of start times as integers for sending to the controller. \n
        :type _st_list:     list[int] \n
        :type _st_list:     Optional list[str] \n
        :return:            Returns a string of start times for sending to the controller. \n
        :rtype:             str \n
        """
        string_for_return = ''

        # Iterate through each start time
        for index, each_time in enumerate(_st_list):
            # Don't want to append an equal sign at the front of this string
            if index == 0:
                string_for_return += str(each_time)
            else:
                # adding/appending '=400' to the string for example
                string_for_return += ("=" + str(each_time))

        # Example output for 3 start times: '480=720=900'
        return string_for_return

    #################################
    def build_event_stop_dates_string(self, _ed_list):
        """
        Builds a list of event dates for sending to the controller \n
        :param _ed_list:        List of event stop dates \n
        :return:                Returns a formated list of semi-colon separated dates
        """
        string_for_return = ''

        # Iterate through each event stop date
        for index, each_date in enumerate(_ed_list):
            # ignore null strings
            if each_date != '':
                # Don't want to append a semi-colon at the front of this string
                if len(string_for_return) > 0:
                    string_for_return += str(";")

                string_for_return += str(each_date)

        # Example output for 3 event stop dates: '3/3/18;4/4/18;5/5/18'
        return string_for_return

    #################################
    def build_week_day_string(self, _wd_list):
        """
        Builds the week day string for sending to controller in correct format. \n
        :param _wd_list:    List containing 7 values (1 for each day of the week), first value being sunday \n
        :type _wd_list:     list[int]  \n
        :return:            Returns properly formatted string for week day water schedule. \n
        :rtype:             String \n
        """
        formatted_string = ''

        # Loop through the list of integers
        for each_int in _wd_list:
            formatted_string += str(each_int)

        # expected returned string format example: '1100111'
        return formatted_string

    #################################
    def build_semi_month_interval_string(self, _sm_list):
        """
        Builds the semi month interval string for sending to controller in the correct format. \n
        :param _sm_list:    List of 24 integers to set for semi month interval. \n
        :type _sm_list:     list[int] \n
        :return:            Returns properly formatted string for sending to controller. \n
        :rtype:             String \n
        """
        formatted_string = ''

        # If the sm list is none, return empty string
        if not _sm_list:
            formatted_string = "0=0=0=0=0=0=0=0=0=0=0=0=0=0=0=0=0=0=0=0=0=0=0=0"
            return formatted_string

        # Loop through the list of integers
        for index, each_int in enumerate(_sm_list):
            # Don't want to append an equal sign before the first integer in the string
            if index == 0:
                formatted_string += str(each_int)
            else:
                formatted_string += "=" + str(each_int)

        # expected returned string format example: '1=1=9=1=29=1=3=2=4=1=1=1=1=2=3=4=2=1=1=9=15=19=1=1'
        return formatted_string
