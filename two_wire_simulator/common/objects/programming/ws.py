import status_parser
from common.imports.types import ActionCommands
from common.object_factory import *
from common.objects.base_classes.flow import BaseFlow
from common.objects.messages.programming.water_source_messages import WaterSourceMessages
from common.objects.statuses.programming.water_source_statuses import WaterSourceStatuses
from common.imports.types import ActionCommands, WaterSourceCommands

import warnings
__author__ = 'Tige'


########################################################################################################################
# Water Source Base Class
########################################################################################################################
class WaterSource(BaseFlow):
    """
    Water Source Base Class.
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    #################################
    def __init__(self, _controller, _ad):
        """
        Water Source Constructor. \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.bl_32.BaseStation3200 \n

        :param _ad:     Water Source Number (address) \n
        :type _ad:      int \n
        """

        # Init parent class to inherit respective attributes
        BaseFlow.__init__(self, _controller=_controller, _ad=_ad, _identifiers=[[opcodes.water_source, _ad]],
                          _ty=opcodes.water_source)

        # create message reference to object
        self.messages = WaterSourceMessages(_water_source_object=self)  # Water Source messages
        """:type: common.objects.messages.programming.water_source_messages.WaterSourceMessages"""

        # # The address of the water source on a 3200. This is value is stored so that if we un-assign this water source
        # # from a FlowStation, it will get it's old 3200 address back.
        # self.saved_3200_address = _ad

        self.wb = 0                 # monthly budget
        self.pr = 2                 # priority
        self.ws = opcodes.false     # monthly budget shut down |TR/FA|
        self.wr = opcodes.false     # water rationing
        self.pc = 0                 # point of control address
        self.sh = opcodes.false     # share with flow station

        # Flow Station point of control
        self.flow_station_points_of_control = []

        # Controller point of control
        self.controller_point_of_control = 0

        # dictionary place holder
        self.switch_empty_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.empty_condition.SwitchEmptyCondition]"""
        self.pressure_empty_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.empty_condition.PressureEmptyCondition]"""
        self.moisture_empty_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.empty_condition.MoistureEmptyCondition]"""
        self.empty_conditions_count = 0

        self.full_conditions = {}

        # Container to hold water source programming from controller when a 'self.get_data()' call is made.
        self.data = status_parser.KeyValues('')

        # Holds the messages return value
        self.build_message_string = ''

        # create status reference to object
        self.statuses = WaterSourceStatuses(_water_source_object=self)  # Water Source Statuses
        """:type: common.objects.statuses.programming.water_source_statuses.WaterSourceStatuses"""

    #################################
    def build_obj_configuration_for_send(self):
        """
        Builds the 'SET' string for sending this Water Source Instance to the Controller. This method ensures
        that attributes with blank values don't get sent to the controller. This helps avoid 'BC' response's from
        controller. \n
        :return:        Formatted 'Set' string for the Point of Connection \n
        :rtype:         str \n
        """

        # Starting string, always will need this, 'SET,PC=Blah'
        current_config = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14}".format(
            ActionCommands.SET,                                 # {0}
            WaterSourceCommands.Water_source,                   # {1}
            str(self.identifiers[0][1]),                                       # {2}
            WaterSourceCommands.Attributes.DESCRIPTION,         # {3}
            self.ds,                                            # {4}
            WaterSourceCommands.Attributes.ENABLED,             # {5}
            self.en,                                            # {6}
            WaterSourceCommands.Attributes.MONTHLY_BUDGET,      # {7}
            str(self.wb),                                       # {8}
            WaterSourceCommands.Attributes.PRIORITY,            # {9}
            str(self.pr),                                       # {10}
            opcodes.with_shut_down,                             # {11}
            self.ws,                                            # {12}
            WaterSourceCommands.Attributes.WATER_RATIONING,     # {13}
            str(self.wr),                                       # {14}
        )

        if self.controller.controller_type == '32':
            if self.pc:
                current_config += ",{0}={1}".format(
                    WaterSourceCommands.Attributes.POINT_OF_CONTROL,        # {0}
                    self.pc                                                 # {1}
                )

        return current_config

    #################################
    def send_programming(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """

        command = self.build_obj_configuration_for_send()
        try:
            # Attempt to set program default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Water Source {0}'s 'Values' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                command,        # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Water Source {0}'s 'Values' to: {1}".format(
                str(self.identifiers[0][1]),     # {0}
                command           # {1}
            ))

    #################################
    def add_point_of_control_to_water_source(self, _point_of_control_address):
        """
        Assign a Point of control to a water Sources
        :param _point_of_control_address:            address of the point of control \n
        :type _point_of_control_address:             int \n
        :return:
        :rtype:
        """

        # Assign point of control' to object attribute
        if not isinstance(_point_of_control_address, int):
            e_msg = "Exception occurred trying to assign point of control {0} to Water Source: '{1}' the value must " \
                    "be an int".format(
                        str(_point_of_control_address),  # {0}
                        str(self.identifiers[0][1])                     # {1}
                    )
            raise Exception(e_msg)

        # Check if there is a poc with the passed in address
        if self.called_by_controller_type == opcodes.flow_station:
            max_poc_address = 20
        else:
            max_poc_address = 8

        if _point_of_control_address not in range(1, max_poc_address+1):
            e_msg = "Invalid 'Point of Control' address entered for Water Source {0}. Available Points of Control " \
                    "are 1 - {1}, user entered: {2}" \
                .format(
                    str(self.identifiers[0][1]),     # {0}
                    str(max_poc_address),            # {1}
                    str(_point_of_control_address)   # {2}
                )
            raise ValueError(e_msg)

        else:
            poc_string = ''

            # Set the Flow Station and Controller Point of Control Address for this Water Source
            if self.called_by_controller_type == opcodes.flow_station:
                self.downstream_pipes.append(self.flowstation.get_point_of_control(_point_of_control_address))
                self.flow_station_points_of_control.append(_point_of_control_address)
                # Create string of assigned pocs for water source. FlowStation requires them to be
                # separated by '=' sign. This outputs '1=2=3=4'...
                poc_string = '='.join([str(poc) for poc in self.flow_station_points_of_control])
            else:
                self.downstream_pipes.append(self.controller.get_point_of_control(_point_of_control_address))
                self.controller_point_of_control = _point_of_control_address
                self.pc = _point_of_control_address
                poc_string = self.pc

            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                                 # {0}
                WaterSourceCommands.Water_source,                   # {1}
                str(self.identifiers[0][1]),                        # {2}
                WaterSourceCommands.Attributes.POINT_OF_CONTROL,    # {3}
                poc_string                                          # {4}
            )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to assign a Point of control {0} to Water Source {1} -> {2}".format(
                      _point_of_control_address,   # {0}
                      str(self.identifiers[0][1]),   # {1}
                      e.message       # {2}
                      )
            raise Exception(e_msg)
        else:
            print("Successfully assign Point of Control {0} to  Water Source {1}".format(
                _point_of_control_address,   # {0}
                self.identifiers[0][1])        # {1}
            )

    #################################
    def set_target_flow(self, _new_flow=None):
        """
        This not an attribute for the Water Source it is only used on the mainline and point of control
        The main method is in Base Flow Class
        """
        warnings.warn('This not an attribute that can be set on the Water Source', DeprecationWarning)
        pass

    #################################
    def set_priority(self, _priority_for_water_source):
        """
        Set priority of water source

        :param _priority_for_water_source:  priority of water source range 1- 10 \n
        :type _priority_for_water_source:   int \n

        :return:
        """
        # Assign point of control' to object attribute
        if not isinstance(_priority_for_water_source, int):
            e_msg = "Exception occurred trying to assign a priority {0}'to Water Source: '{1}' the value must " \
                    "be an int".format(
                        str(_priority_for_water_source),    # {0}
                        str(self.identifiers[0][1])                        # {1}
                    )
            raise Exception(e_msg)
        if _priority_for_water_source not in range(1, 11):
            e_msg = "Exception occurred trying to assign a priority {0}'to Water Source: '{1}' the value must " \
                    "be between 1 and 10 ".format(
                        str(_priority_for_water_source),    # {0}
                        str(self.identifiers[0][1])                        # {1}
                    )
            raise Exception(e_msg)
        self.pr = _priority_for_water_source  # point of control address
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                         # {0}
            WaterSourceCommands.Water_source,           # {1}
            str(self.identifiers[0][1]),                               # {2}
            WaterSourceCommands.Attributes.PRIORITY,    # {3}
            self.pr                                     # {4}
        )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to assign a priority to a Water Source {0}'s 'priority to:" \
                    " '{1}' -> {2}".format(
                        str(self.identifiers[0][1]),               # {0}
                        self.pr,                    # {1}
                        e.message                   # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully assign a priority {0} to  Water Source {0}".format(
                str(self.pr),
                self.identifiers[0][1])
            )

    #################################
    def set_monthly_watering_budget(self, _budget=None, _with_shutdown_enabled=False):
        """
        Sets the 'Monthly Budget ' for the Water Source on the controller. \n

        :param _budget:     New 'Monthly Budget' Water Source \n
        :type _budget:      int | float \n
        :param _with_shutdown_enabled:  Monthly budget shutdown (True: 'TR', False: 'FA') - Default: 'FA' \n
        :type _with_shutdown_enabled:   bool \n
        """

        if _budget is not None:
            # Assign new 'Monthly Budget' to object attribute
            if not (isinstance(_budget, float) or isinstance(_budget, int)):
                e_msg = "Exception occurred trying to set Water Source{0}'s 'Monthly Budget' to: '{1}' the value must " \
                        "be an int or a float".format(
                         str(self.identifiers[0][1]),  # {0}
                         str(_budget)   # {1}
                         )
                raise Exception(e_msg)
            else:
                self.wb = _budget

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                             # {0}
            WaterSourceCommands.Water_source,               # {1}
            str(self.identifiers[0][1]),                                   # {2}
            WaterSourceCommands.Attributes.MONTHLY_BUDGET,  # {3}
            str(self.wb))                                   # {4}

        if _budget is not None:
            if _with_shutdown_enabled not in [True, False]:
                e_msg = "Exception occurred trying to set Water Source{0}'s 'Monthly Budget with shut Down' to: " \
                        "'{1}' the value must be True or False" .format(
                         str(self.identifiers[0][1]),                # {0}
                         str(_with_shutdown_enabled)  # {1}
                         )
                raise Exception(e_msg)

            if _with_shutdown_enabled is False:
                self.ws = opcodes.false
            else:
                self.ws = opcodes.true

        command += ',{0}={1}'.format(
            str(opcodes.with_shut_down),    # {0}
            self.ws                         # {1}
            )

        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Water Source {0}'s 'Monthly Budget' to: '{1}' and " \
                    "'Shutdown Enabled' to {2} -> {3}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.wb),   # {1}
                        str(self.ws),   # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set Water Source {0}'s 'Monthly Budget' to: {1} and 'Shutdown Enabled' to {2}".format(
                str(self.identifiers[0][1]),
                str(self.wb),
                str(self.ws)
                )
            )

    #################################
    def set_water_rationing_to_enabled(self):
        """
        Sets the 'Enable Water Rationing' for the Water Source on the controller. \n
        """

        self.wr = opcodes.true

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                                 # {0}
            WaterSourceCommands.Water_source,                   # {1}
            str(self.identifiers[0][1]),                                       # {2}
            WaterSourceCommands.Attributes.WATER_RATIONING,     # {3}
            str(self.wr))                                       # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Water Source {0}'s 'Water Rationing' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),  # {0}
                str(self.wr),  # {1}
                e.message      # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Water Source {0}'s 'Water Rationing' to: {1}".format(
                str(self.identifiers[0][1]),
                str(self.wr))
            )

    #################################
    def set_water_rationing_to_disabled(self):
        """
        Sets the 'disabled Water Rationing' for the Water Source on the controller. \n
        """

        self.wr = opcodes.false

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                                 # {0}
            WaterSourceCommands.Water_source,                   # {1}
            str(self.identifiers[0][1]),                                       # {2}
            WaterSourceCommands.Attributes.WATER_RATIONING,     # {3}
            str(self.wr))                                       # {4}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Water Source {0}'s 'Water Rationing' to: '{1}' -> {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.wr),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Water Source {0}'s 'Water Rationing' to: {1}".format(
                str(self.identifiers[0][1]),
                str(self.wr))
            )

    #################################
    def add_moisture_empty_condition(self, _moisture_sensor_address=None):
        """

        3200 Water Source Empty Condition Constructor. \n
        :param _moisture_sensor_address:     Moisture Sensor empty Object Address - Default: '' \n
        :type _moisture_sensor_address:      int \n

        """
        from common.object_factory import create_moisture_empty_condition_object

        if isinstance(_moisture_sensor_address, int):

            # Validate argument value is within accepted values
            if _moisture_sensor_address not in self.controller.moisture_sensors.keys():
                e_msg = "Invalid Moisture Sensor address. Verify address exists in object json configuration and/or in " \
                        "current test. Received address: {0}, available addresses: {1}".format(
                         str(_moisture_sensor_address),  # {0}
                         str(self.controller.moisture_sensors.keys()),  # {1}
                         )
                raise ValueError(e_msg)

            # Assign new 'Moisture Sensor' to object attribute
            else:
                # Assign address to poc ms attributes
                self.empty_conditions_count += 1
                create_moisture_empty_condition_object(
                    controller=self.controller,
                    water_source_address=self.identifiers[0][1],
                    moisture_sensor_address=_moisture_sensor_address,
                    empty_address=self.empty_conditions_count
                )
                self.moisture_empty_conditions[_moisture_sensor_address].send_programming()

        else:
            e_msg = "Invalid address for Empty Condition tried to set it to: {0}. Needs to be an int"\
                        .format(
                         str(_moisture_sensor_address),       # {0}
                        )
            raise ValueError(e_msg)

    #################################
    def add_pressure_empty_condition(self, _pressure_sensor_address=None):
        """

        3200 Water Source Empty Condition Constructor. \n
        :param _pressure_sensor_address:     Pressure Sensor empty Object Address - Default: '' \n
        :type _pressure_sensor_address:      int \n
        """
        from common.object_factory import create_pressure_empty_condition_object

        if isinstance(_pressure_sensor_address, int):
            # Validate argument value is within accepted values
            if _pressure_sensor_address not in self.controller.pressure_sensors.keys():
                e_msg = "Invalid Pressure Sensor address. Verify address exists in object json configuration and/or in " \
                        "current test. Received address: {0}, available addresses: {1}".format(
                         str(_pressure_sensor_address),  # {0}
                         str(self.controller.pressure_sensors.keys()),  # {1}
                        )
                raise ValueError(e_msg)
            else:
                # Increment the empty condition counter and use it as the 'empty address'
                self.empty_conditions_count += 1

                create_pressure_empty_condition_object(
                    controller=self.controller,
                    water_source_address=self.identifiers[0][1],
                    pressure_sensor_address=_pressure_sensor_address,
                    empty_address=self.empty_conditions_count
                )
                self.pressure_empty_conditions[_pressure_sensor_address].send_programming()
        else:
            e_msg = "Invalid address for Empty Condition tried to set it to: {0}. Needs to be an int" \
                .format(
                str(_pressure_sensor_address),  # {0}
            )
            raise ValueError(e_msg)

    #################################
    def add_switch_empty_condition(self, _event_switch_address=None):
        """

        3200 Water Source Empty Condition Constructor. \n
        :param _event_switch_address:     Event Switch empty Object Address - Default: '' \n
        :type _event_switch_address:      int \n

        """
        from common.object_factory import create_switch_empty_condition_object

        if isinstance(_event_switch_address, int):
            # Validate argument value is within accepted values
            if _event_switch_address not in self.controller.event_switches.keys():
                e_msg = "Invalid Event Switch address. Verify address exists in object json configuration and/or in " \
                        "current test. Received address: {0}, available addresses: {1}".format(
                         str(_event_switch_address),  # {0}
                         str(self.controller.event_switches.keys()),  # {1}
                         )
                raise ValueError(e_msg)

            # Assign new 'Event Switch Empty SN' to object attribute
            else:
                # Increment the empty condition counter and use it as the 'empty address'
                self.empty_conditions_count += 1

                create_switch_empty_condition_object(
                    controller=self.controller,
                    water_source_address=self.identifiers[0][1],
                    event_switch_address=_event_switch_address,
                    empty_address=self.empty_conditions_count
                )
                self.switch_empty_conditions[_event_switch_address].send_programming()
        else:
            e_msg = "Invalid address for Empty Condition tried to set it to: {0}. Need to be a int" \
                .format(
                str(_event_switch_address),  # {0}
            )
            raise ValueError(e_msg)

    #################################
    def verify_water_rationing(self):
        """
        Verifies the 'Enabled State' for water rationing of a water source set on the controller. \n
        """
        # expect string type
        water_rationing_en_state = str(self.data.get_value_string_by_key(opcodes.water_rationing_enable))

        # Compare water rationing value
        if self.wr != water_rationing_en_state:
            e_msg = "Unable verify Water Source {0}'s 'Water Rationing Enable State'. Received: {1}, Expected: {2}"\
                .format(
                 str(self.identifiers[0][1]),                      # {0}
                 str(water_rationing_en_state),     # {1}
                 self.wr                            # {2}
                 )
            raise ValueError(e_msg)
        else:
            print("Verified Water Source  {0}'s 'Water Rationing Enable State': '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                self.wr         # {1}
            ))

    #################################
    def verify_monthly_budget(self):
        """
        Verifies the 'Monthly Water budget' for water of a water source on the controller. \n
        Verified the Enable shutdown state for the water source using Monthly water budget \n
        """
        # expect string type for water_budget_shutdown_state, and int type for water_budget
        water_budget = int(self.data.get_value_string_by_key(opcodes.monthly_water_budget))
        water_budget_shutdown_state = str(self.data.get_value_string_by_key(opcodes.shutdown_on_over_budget))

        # Compare monthly water budget with or without shutdown
        if self.wb != water_budget or self.ws != water_budget_shutdown_state:
            e_msg = "Unable verify Water Source {0} 'Water Monthly Water Budget with or without 'Shut Down Enabled'. " \
                    "Received: {1} for 'Water Budget', Expected: {2} Received {3} : " \
                    "for 'Shut Down Enable' Expected {4}:" \
                    .format(
                     str(self.identifiers[0][1]),                       # {0}
                     str(water_budget),                  # {1}
                     self.wb,                            # {2}
                     str(water_budget_shutdown_state),   # {3}
                     self.ws                             # {4}
                     )
            raise ValueError(e_msg)

        else:
            print("Verified Water Source {0}'s 'Monthly Water Budget': '{1}' and 'Shutdown Enable State': '{2}' "
                    .format(
                        str(self.identifiers[0][1]),   # {0}
                        self.wb,        # {1}
                        self.ws         # {2}
                    )
                )

    #################################
    def verify_point_of_control(self):
        """
        Verifies the 'Points of Control' assigned to a water source on the controller. \n
        """
        # expect int type
        point_of_control_address = int(self.data.get_value_string_by_key(opcodes.point_of_control))

        # Compare point of control
        if self.pc != point_of_control_address:
            e_msg = "Unable verify Water Source {0}'s 'Point of Control Assignments' Received: '{1}' Expected: '{2}'"\
                .format(
                     str(self.identifiers[0][1]),                      # {0}
                     str(point_of_control_address),     # {1}
                     self.pc                            # {2}
                     )
            raise ValueError(e_msg)

        else:
            print("Verified Water Source {0} assigned to 'Point of Control': '{1}' ".format(
                str(self.identifiers[0][1]),  # {0}
                self.pc        # {1}
            ))

    #################################
    def verify_flowstation_points_of_control(self):
        """
        Verifies multiple 'Points of Control' assigned to a water source on the FlowStation. \n
        """
        # If multiple POCs are assigned to the water source, this will be a single string value with all poc addresses.
        # For example, if WS had two pocs, 1 and 2, then `points_of_control` will be "1=2".split('=') => ['1', '2']
        points_of_control_address_as_str = str(self.data.get_value_string_by_key(opcodes.point_of_control))

        points_of_control_address_as_list = points_of_control_address_as_str.split('=')
        points_of_control_address_as_list.sort()

        # Get local "known" assigned poc list
        expected_points_of_control_address_list = [str(poc) for poc in self.flow_station_points_of_control]
        expected_points_of_control_address_list.sort()

        # Compare point of control
        if expected_points_of_control_address_list != points_of_control_address_as_list:
            e_msg = "Unable verify Water Source {0}'s 'Point(s) of Control Assignments' Received: '{1}' Expected: '{2}'"\
                .format(
                    self.identifiers[0][1],                     # {0}
                    points_of_control_address_as_list,          # {1}
                    expected_points_of_control_address_list     # {2}
                    )
            raise ValueError(e_msg)

        else:
            print("Verified Water Source {0} assigned to 'Point(s) of Control': '{1}' ".format(
                self.identifiers[0][1],                     # {0}
                expected_points_of_control_address_list     # {1}
            ))

    #################################
    def verify_share_with_flow_station(self):
        """
        Verifies the 'share with flow station' of the water source on the controller. \n
        """
        # expect string type
        share_with_flow_station_from_controller = str(self.data.get_value_string_by_key(opcodes.share_with_flowstation))

        if share_with_flow_station_from_controller is not None:
            # Compare share with flow station
            if self.sh != share_with_flow_station_from_controller:
                e_msg = "Unable verify Water Source {0}'s 'Share with Flow Station' Received: '{1}' " \
                        "for 'Share with FlowStation', Expected: '{2}'".format(
                            str(self.identifiers[0][1]),                                       # {0}
                            str(share_with_flow_station_from_controller),       # {1}
                            self.sh                                             # {2}
                        )
                raise ValueError(e_msg)

            else:
                print("Verified Water Source {0} 'Share with Flow Station': '{1}' ".format(
                    str(self.identifiers[0][1]),  # {0}
                    self.sh        # {1}
                ))

    def verify_priority(self):
        """
        Verifies the 'Priority' for water of a water source on the controller. \n
        """
        # expect string type
        priority = int(self.data.get_value_string_by_key(opcodes.priority))

        # Compare priority
        if self.pr != priority:
            e_msg = "Unable verify Water Source {0}'s 'Priority'." \
                    " Received: {1} for Priority, Expected: {2}".format(
                str(self.identifiers[0][1]),   # {0}
                str(priority),  # {1}
                self.pr         # {2}

            )
            raise ValueError(e_msg)

        else:
            print("Verified Water Source {0}'s 'Priority': '{1}' on controller ".format(
                str(self.identifiers[0][1]),   # {0}
                self.pr         # {1}

            ))

    #################################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies all attributes associated with this Water Source. \n
        :param expected_status:     An expected status to verify for POC \n
        :type expected_status:      str \n
        """

        self.get_data()

        self.verify_description()
        self.verify_enabled_state()
        self.verify_priority()
        self.verify_monthly_budget()
        self.verify_water_rationing()

        # Verify Flow Station Water Source specific attributes
        if self.controller.controller_type == opcodes.flow_station:
            self.verify_flowstation_points_of_control()

        # verify Controller Water Source specific attributes
        if self.controller.controller_type == opcodes.basestation_3200:
            if not self.is_managed_by_flowstation():
                # self.pc will always be 0 if it is shared with a Flow Station. Just want to check when it is
                # not managed by the FlowStation
                self.pc = self.controller_point_of_control
                # Commenting this because we don't want to verify this until we get the add multiple POCs for water
                # source working
                self.verify_point_of_control()
            self.verify_share_with_flow_station()

        # this verifies status if one is given
        if expected_status is not None:
            self.verify_status(_expected_status=expected_status)