from common.imports import opcodes
from common.objects.base_classes.programs import BasePrograms
from common.objects.messages.programming.pg_1000_messages import ProgramMessages
# from common.objects.object_bucket import poc10

__author__ = 'baseline'


########################################################################################################################
# 1000 Program Class
########################################################################################################################
class PG1000(BasePrograms):
    """
    1000 Program Object \n
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    mainline_objects = None

    # Controller firmware version
    cn_firmware_version = None

    # obj_type = opcodes.program + "32"

    #################################
    def __init__(self, _controller, _ad):
        """
        1000 Program constructor. \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.base_classes.controller.BaseController \n

        :param _ad:     Program number \n
        :type _ad:      int \n

        """
        # Init parent class
        BasePrograms.__init__(self, _controller=_controller, _ad=_ad)
        self.poc_objects = []

        # create message reference to object
        self.messages = ProgramMessages(_program_object=self)  # program messages
        """:type: common.objects.messages.programming.pg_1000_messages.ProgramMessages"""

        self.mv = ''
        self.sk = opcodes.disabled
        self.ct = 0
        self.so = 0
        self.cy = None
        self.ee = opcodes.false

        # Start Conditions
        self.date_time_start_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.start_condition_1000.DateStartCondition1000]"""

    #################################
    def send_programming(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        # Water Window Case
        if len(self.ww) == 1:
            # A len(self.ww) == 1 says we are a weekly water window with a single value
            water_window_weekly_str = "{0}={1}".format(
                opcodes.weekly_water_window,    # {0}
                self.ww[0]                      # {1}
            )
        else:
            water_window_weekly_str = "{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13}".format(
                opcodes.sunday,     # {0}
                self.ww[0],         # {1}
                opcodes.monday,     # {2}
                self.ww[1],         # {3}
                opcodes.tuesday,    # {4}
                self.ww[2],         # {5}
                opcodes.wednesday,  # {6}
                self.ww[3],         # {7}
                opcodes.thursday,   # {8}
                self.ww[4],         # {9}
                opcodes.friday,     # {10}
                self.ww[5],         # {11}
                opcodes.saturday,   # {12}
                self.ww[6]          # {13}
            )

        poc_string = ''
        if len(self.poc_objects) > 0:
            poc_string = str(self.poc_objects[self.poc_objects[0]].ad)
            for poc_ad in self.poc_objects[1:]:
                poc_string += "={0}".format(self.poc_objects[poc_ad].ad)

        command = "SET,{0}={1},{2}={3},{4}={5},{6},{7}={8},{9}={10},{11}={12},{13}={14},{15}={16},{17}={18}" \
                  ",{19}={20}".format(
                      opcodes.program,                      # {0}
                      str(self.ad),                         # {1}
                      opcodes.enabled,                      # {2}
                      str(self.en),                         # {3}
                      opcodes.description,                  # {4}
                      str(self.ds),                         # {5}
                      water_window_weekly_str,              # {6}
                      opcodes.max_concurrent_zones,         # {7}
                      str(self.mc),                         # {8}
                      opcodes.seasonal_adjust,              # {9}
                      str(self.sa),                         # {10}
                      opcodes.point_of_connection,          # {11}
                      poc_string,                           # {12}
                      opcodes.soak_cycle_mode,              # {13}
                      self.sk,                              # {14}
                      opcodes.cycle_count,                  # {15}
                      self.ct,                              # {16}
                      opcodes.soak_cycle,                   # {17}
                      self.so,                              # {18}
                      opcodes.enable_et_runtime,            # {19}
                      self.ee                               # {20}
                  )
        
        # Only add master valve if we have an address assigned to it
        
        if self.mv:
            add_mv_to_command = ",{0}={1}".format(
                opcodes.master_valve,           # {0}
                self.controller.master_valves[self.mv].sn    # {1}
            )
            command += add_mv_to_command

        # Only add program cycles if we have assigned and integer

        if self.cy is not None:
            add_cy_to_command = ",{0}={1}".format(
                opcodes.program_cycles,       # {0}
                self.cy                       # {1}
            )
            command += add_cy_to_command

        try:
            # Attempt to set program default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Values' to: '{1}'\n -> {2}".format(
                self.ad,   # {0}
                command,   # {1}
                e.message  # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Program {0}'s 'Values' to: {1}".format(
                self.ad,     # {0}
                command      # {1}
            ))

    #################################
    def add_point_of_connection(self, _poc_address):
        """
        Sets the 'Point of Connection' for the current program on the controller. \n
        :param _poc_address:   Point of Connection address to overwrite current instance value. \n
        :type _poc_address:    list | int \n
        """
        # If an int is passed in, we define it in a list
        if isinstance(_poc_address, int):
            _poc_address = [_poc_address]

        # Valid poc address?
        for i in range(0, len(_poc_address)):
            if _poc_address[i] not in self.controller.points_of_control.keys():
                e_msg = "Invalid Point of Connection address for 'SET' for 1000 Program {0}. Valid address are: " \
                        "1 - 3. Received: {1}".format(
                            self.ad,        # {0}
                            _poc_address    # {1}
                        )
                raise ValueError(e_msg)
        self.poc_objects.extend(_poc_address)
        self.poc_objects = list(set(self.poc_objects))  # This ensures only 1 of each address can exist

        # Create a string of the different poc addresses, eg. "1=2=3"
        poc_string = str(self.poc_objects[0])
        for poc_ad in self.poc_objects[1:]:
            poc_string += "={0}".format(poc_ad)

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,                # {0}
            str(self.ad),                   # {1}
            opcodes.point_of_connection,    # {2}
            str(poc_string)                 # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Point of Connection' to: {1} -> {2}".format(
                self.ad,            # {0}
                self.poc_objects,   # {1}
                e.message           # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Point of Connection' for 1000 Program: {0} to: {1}".format(
                self.ad,            # {0}
                self.poc_objects    # {1}
            ))

    #################################
    def remove_point_of_connection(self, _poc_address):
        """
        Removes a 'Point of Connection' for the current program on the controller. \n
        :param _poc_address:   Point of Connection address to overwrite current instance value. \n
        :type _poc_address:    int \n
        """
        # Is this POC even in our list?
        if _poc_address not in self.poc_objects:
            e_msg = "The control point you tried to remove is not in Program {0}'s list of Control Points. You" \
                    "tried to removed Control Point {1}, the current control points in the list are {2}.".format(
                        self.ad,            # {0}
                        _poc_address,       # {1}
                        self.poc_objects    # {2}
                    )
            raise ValueError(e_msg)
        self.poc_objects.remove(_poc_address)

        # Create a string of the different poc addresses, eg. "1=2=3"
        if len(self.poc_objects) > 0:
            poc_string = str(self.poc_objects[0])
            for poc_ad in self.poc_objects[1:]:
                poc_string += "={0}".format(poc_ad)
        else:
            poc_string = ''     # If there is nothing in our list, pass an empty string to the controller

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,                # {0}
            str(self.ad),                   # {1}
            opcodes.point_of_connection,    # {2}
            str(poc_string)                 # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Point of Connection' to: {1} -> {2}".format(
                self.ad,            # {0}
                self.poc_objects,   # {1}
                e.message           # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Point of Connection' for 1000 Program: {0} to: {1}".format(
                self.ad,            # {0}
                self.poc_objects    # {1}
            ))

    #################################
    def add_date_time_start_condition(self, _condition_number):
        """
        Add a date/time start condition on the program. \n

        :param _condition_number:   The number that will be used as an index for your date/time start condition. \n
        :type _condition_number:    int \n
        """
        from common.object_factory import create_date_time_start_condition_object

        # create program zone object
        create_date_time_start_condition_object(controller=self.controller,
                                                program_address=self.ad,
                                                condition_address=_condition_number)
            
    #################################
    def add_master_valve(self, _mv_address):
        """
        Sets the 'Master Valve' for the current program on the controller. \n
        :param _mv_address:   Master Valve address to overwrite current instance value. \n
        :type _mv_address:    int \n
        """
        # Valid address?
        if _mv_address not in self.controller.master_valves.keys():
            e_msg = "Invalid Master Valve address for 'SET' for 1000 Program {0}. Valid address are: {1}. " \
                    "Received: {2}".format(
                        self.ad,                                # {0}
                        self.controller.master_valves.keys(),   # {1}
                        _mv_address                             # {2}
                    )
            raise ValueError(e_msg)
            
        # valid address
        else:
            # Assign address to mv attribute
            self.mv = _mv_address

            # Get serial from Master Valve with specified address
            mv_serial_for_send = self.controller.master_valves[self.mv].sn

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,         # {0}
            self.ad,                 # {1}
            opcodes.master_valve,    # {2}
            mv_serial_for_send       # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Master Valve' to: {1}({2}) -> {3}".format(
                self.ad,                # {0}
                mv_serial_for_send,     # {1}
                self.mv,                # {2}
                e.message               # {3}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Master Valve' for 1000 Program: {0} to: {1}({2})".format(
                self.ad,                # {0}
                mv_serial_for_send,     # {1}
                self.mv                 # {2}
            ))

    #################################
    def set_master_valve(self, _mv_address):
        """
        Sets the 'Master Valve' for the current program on the controller. \n
        :param _mv_address:   Master Valve address to overwrite current instance value. \n
        :type _mv_address:    int \n
        """
        # Valid address?
        if _mv_address not in self.controller.master_valves:
            e_msg = "Invalid Master Valve address for 'SET' for 1000 Program {0}. Valid address are: {1}. " \
                    "Received: {2}".format(
                        self.ad,                    # {0}
                        self.controller.master_valves.keys(),     # {1}
                        _mv_address                 # {2}
                    )
            raise ValueError(e_msg)

        # valid address
        else:
            # Assign address to mv attribute
            self.mv = _mv_address

            # Get serial from Master Valve with specified address
            mv_serial_for_send = self.controller.master_valves[self.mv].sn

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,         # {0}
            self.ad,                 # {1}
            opcodes.master_valve,    # {2}
            mv_serial_for_send       # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Master Valve' to: {1}({2}) -> {3}".format(
                self.ad,                # {0}
                mv_serial_for_send,     # {1}
                self.mv,                # {2}
                e.message               # {3}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Master Valve' for 1000 Program: {0} to: {1}({2})".format(
                self.ad,                # {0}
                mv_serial_for_send,     # {1}
                self.mv                 # {2}
            ))
            
    #################################
    def set_soak_cycle_mode(self, _mode):
        """
        Sets the 'Soak Cycle Mode' for the current program on the controller. \n
        :param _mode:   Soak Cycle Mode to overwrite current instance value. \n
        :type _mode:    str \n
        """
        valid_modes = [opcodes.disabled, opcodes.intelligent_soak, opcodes.program_cycles, opcodes.zone_soak_cycles]

        # Valid argument type?
        if _mode not in valid_modes:
            e_msg = "Invalid mode passed in for 'SET' Soak Cycle Mode for 1000 Program {0}. Valid modes: " \
                    "{1}, received: {2}".format(
                        self.ad,        # {0}
                        valid_modes,    # {1}
                        _mode           # {2}
                    )
            raise ValueError(e_msg)
            
        # valid mode
        else:
            self.sk = _mode

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,            # {0}
            self.ad,                    # {1}
            opcodes.soak_cycle_mode,    # {2}
            self.sk                     # {3}
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Soak Cycle Mode' to: {1}".format(
                self.ad,                # {0}
                self.sk                 # {1}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Soak Cycle Mode' for 1000 Program: {0} to: {1}".format(
                self.ad,                # {0}
                self.sk                 # {1}
            ))

    #################################
    def set_soak_cycle_mode_enabled(self):
        """
        Sets the 'Soak Cycle Mode' for the current program on the controller to enabled. \n
        """
        self.set_soak_cycle_mode(_mode=opcodes.enabled)

    #################################
    def set_soak_cycle_mode_disabled(self):
        """
        Sets the 'Soak Cycle Mode' for the current program on the controller to disabled. \n
        """
        self.set_soak_cycle_mode(_mode=opcodes.disabled)
            
    #################################
    def set_cycle_count(self, _new_count):
        """
        Sets the 'Cycle Count' for the current program on the controller. \n
        :param _new_count:   Cycle Count to overwrite current instance value. \n
        :type _new_count:    int \n
        """
        valid_cycle_count_range = range(2, 101)

        # Valid argument range?
        if _new_count not in valid_cycle_count_range:
            e_msg = "Invalid cycle count value passed in for 'SET' Cycle Count for 1000 Program {0}. Valid values: " \
                    "2 - 100, received: {1}".format(
                        self.ad,    # {0}
                        _new_count  # {2}
                    )
            raise ValueError(e_msg)
            
        # valid mode
        else:
            self.ct = _new_count

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,        # {0}
            self.ad,                # {1}
            opcodes.cycle_count,    # {2}
            self.ct                 # {3}
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Cycle Count' to: {1} -> {2}".format(
                self.ad,                # {0}
                self.ct,                # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Cycle Count' for 1000 Program: {0} to: {1}".format(
                self.ad,                # {0}
                self.ct                 # {1}
            ))
                        
    #################################
    def set_soak_time_in_seconds(self, _seconds):
        """
        Sets the 'Soak Time' for the current program on the controller. \n
        :param _seconds:   Soak Time to overwrite current instance value. \n
        :type _seconds:    int \n
        """
        # valid mode
        self.so = _seconds
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,        # {0}
            self.ad,                # {1}
            opcodes.soak_cycle,      # {2}
            self.so                 # {3}
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Soak Time' to: {1} seconds -> {2}".format(
                self.ad,                # {0}
                self.so,                # {1}
                e.message               # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Soak Time' for 1000 Program: {0} to: {1} seconds".format(
                self.ad,                # {0}
                self.so                 # {1}
            ))
                                    
    #################################
    def set_program_cycles(self, _number_of_cycles):
        """
        Sets the 'Program Cycles' for the current program on the controller. \n
        :param _number_of_cycles:   Program Cycles to overwrite current instance value. \n
        :type _number_of_cycles:    int \n
        """
        # Valid type
        self.cy = _number_of_cycles
        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,            # {0}
            self.ad,                    # {1}
            opcodes.program_cycles,     # {2}
            self.cy                     # {3}
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 1000 Program {0}'s 'Program Cycles' to: {1} cycles -> {2}".format(
                self.ad,                # {0}
                self.cy,                # {1}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Program Cycles' for 1000 Program: {0} to: {1} cycles".format(
                self.ad,                # {0}
                self.cy                 # {1}
            ))

    #################################
    def set_enable_et_state(self, _state=None):
        """
        Sets the Enabled State for ET on a program . \n
        :return:
        """
        # If trying to overwrite current value
        if _state is not None:

            # If program isn't enabled, make sure to update instance variable to match what the controller will show.
            if _state not in [opcodes.true, opcodes.false]:
                e_msg = "Invalid enabled state entered for Program {0}. Received: {1}, Expected: 'TR' or 'FA'".format(
                        str(self.ad),   # {0}
                        _state)         # {1}
                raise ValueError(e_msg)
            else:
                # Overwrite current attribute value
                self.ee = _state

        command = "SET,{0}={1},{2}={3}".format(
            opcodes.program,                        # {0}
            self.ad,                                # {1}
            opcodes.enable_et_runtime,      # {2}
            self.ee                                 # {3}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Program {0})'s 'Enable ET Runtime state' " \
                    "to: {1}, {2}".format(
                        self.ad,                # {0}
                        self.ee,                # {1}
                        str(ae.message)         # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Enable ET runtime state' for (Program {0} to: {1}"
                  .format(
                    self.ad,                # {0}
                    self.ee,                # {1}
                  ))

    #################################
    def verify_poc(self):
        """
        Verifies the assigned POC returned from the controller. \n
        """
        assigned_poc_from_controller = self.data.get_value_string_by_key(opcodes.point_of_connection)

        # If no POC's are present on the program, verify our list is empty
        if assigned_poc_from_controller is None:
            if len(self.poc_objects) == 0:
                print "Verified 1000 Program {0}'s has no assigned POCs.".format(self.ad)
            else:
                e_msg = "Unable to verify 1000's POCs on program {0}. We got back no POCs but our" \
                        "object contains {1} POCs".format(self.ad, len(self.poc_objects))
                raise ValueError(e_msg)
        else:
            assigned_poc_from_controller = map(int, assigned_poc_from_controller.split('='))

        # We must sort our list first because the controller returns it's poc addresses in order
        self.poc_objects.sort()
        # Compare versus what is on the controller, both are lists so we must iterate through them
        for i in range(0, len(self.poc_objects)):
            if self.poc_objects[i] != assigned_poc_from_controller[i]:
                e_msg = "Unable to verify 1000 Program {0}'s assigned 'POC'. Received: {1}, Expected: {2}".format(
                    self.ad,                        # {0}
                    assigned_poc_from_controller,   # {1}
                    self.poc_objects                         # {2}
                )
                raise ValueError(e_msg)

        print("Verified 1000 Program {0}'s assigned 'POC': '{1}' on controller".format(
            self.ad,    # {0}
            self.poc_objects     # {1}
        ))
            
    #################################
    def verify_master_valve(self):
        """
        Verifies the assigned Master Valve returned from the controller. \n
        """
        assigned_mv_sn_from_controller = self.data.get_value_string_by_key(opcodes.master_valve)

        # Get serial number from our assigned master valve object
        sn_from_assigned_mv_object = self.controller.master_valves[self.mv].sn
        
        # Compare versus what is on the controller
        if sn_from_assigned_mv_object != assigned_mv_sn_from_controller:
            e_msg = "Unable to verify 1000 Program {0}'s assigned 'Master Valve'. Received: {1}, Expected: {2}".format(
                self.ad,                            # {0}
                assigned_mv_sn_from_controller,     # {1}
                sn_from_assigned_mv_object          # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 1000 Program {0}'s assigned 'Master Valve': '{1}' on controller".format(
                self.ad,                    # {0}
                sn_from_assigned_mv_object  # {1}
            ))            
            
    #################################
    def verify_soak_cycle_mode(self):
        """
        Verifies the assigned Soak Cycle Mode returned from the controller. \n
        """
        # expect string value
        mode_from_controller = self.data.get_value_string_by_key(opcodes.soak_cycle_mode)

        # Compare versus what is on the controller
        if self.sk != mode_from_controller:
            e_msg = "Unable to verify 1000 Program {0}'s 'Soak Cycle Mode'. Received: {1}, Expected: {2}".format(
                self.ad,                  # {0}
                mode_from_controller,     # {1}
                self.sk                   # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 1000 Program {0}'s 'Soak Cycle Mode': '{1}' on controller".format(
                self.ad,    # {0}
                self.sk     # {1}
            ))            
            
    #################################
    def verify_cycle_count(self):
        """
        Verifies the assigned Cycle Count returned from the controller. \n
        """
        # expect int value
        cycle_count_from_controller = int(self.data.get_value_string_by_key(opcodes.cycle_count))

        # Compare versus what is on the controller
        if self.ct != cycle_count_from_controller:
            e_msg = "Unable to verify 1000 Program {0}'s 'Cycle Count'. Received: {1}, Expected: {2}".format(
                self.ad,                        # {0}
                cycle_count_from_controller,    # {1}
                self.ct                         # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 1000 Program {0}'s 'Cycle Count': '{1}' on controller".format(
                self.ad,    # {0}
                self.ct     # {1}
            ))            
            
    #################################
    def verify_soak_time(self):
        """
        Verifies the assigned Soak Time returned from the controller. \n
        """
        # expect int value
        soak_time_from_controller = int(self.data.get_value_string_by_key(opcodes.soak_cycle))

        # Compare versus what is on the controller
        if self.so != soak_time_from_controller:
            e_msg = "Unable to verify 1000 Program {0}'s 'Soak Time'. Received: {1} seconds, Expected: {2} " \
                    "seconds".format(
                        self.ad,                        # {0}
                        soak_time_from_controller,      # {1}
                        self.so                         # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified 1000 Program {0}'s 'Soak Time': '{1}' seconds on controller".format(
                self.ad,    # {0}
                self.so     # {1}
            ))            
            
    #################################
    def verify_program_cycles(self):
        """
        Verifies the assigned Program Cycles returned from the controller. \n
        """
        # expect int value
        pg_cycles_from_controller = int(self.data.get_value_string_by_key(opcodes.program_cycles))

        # Compare versus what is on the controller
        if self.cy != pg_cycles_from_controller:
            e_msg = "Unable to verify 1000 Program {0}'s 'Program Cycles'. Received: {1}, Expected: {2}".format(
                    self.ad,                        # {0}
                    pg_cycles_from_controller,      # {1}
                    self.cy                         # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 1000 Program {0}'s 'Program Cycles': '{1}' on controller".format(
                self.ad,    # {0}
                self.cy     # {1}
            ))

    #################################
    def verify_enable_et_state(self):
        """
        Verifies the 'Enable ET Runtime State' value for the Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'ET Runtime State' value from the controller reply.
        -   Lastly, compares the instance 'ET Runtime State' value with the controller's programming.
        """

        et_state_from_controller = self.data.get_value_string_by_key(opcodes.enable_et_runtime)

        # Compare status versus what is on the controller
        if self.ee != et_state_from_controller:
            e_msg = "Unable to verify (Program {0})'s 'Enable Et Runtime State' value. " \
                    "Received: {1}, Expected: {2}".format(
                            self.ad,                                     # {0}
                            str(et_state_from_controller),                  # {1}
                            str(self.ee)                                    # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Program {0})'s 'Enable Et Runtime State' value: '{1}' on controller"
                  .format(
                        self.ad,                # {0}
                        self.ee                 # {1}
                  ))

    #################################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies this program's values against what the controller has. \n
        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        self.get_data()

        self.verify_description()
        self.verify_enabled_state()
        self.verify_water_window()
        self.verify_max_concurrent_zones()
        self.verify_seasonal_adjust()

        if expected_status is not None:
            self.verify_status(_expected_status=expected_status)

        if len(self.poc_objects) > 0:
            self.verify_poc()

        if self.mv:
            self.verify_master_valve()

        self.verify_soak_cycle_mode()
        self.verify_cycle_count()
        self.verify_soak_time()

        self.verify_enable_et_state()
        if self.cy is not None:
            self.verify_program_cycles()
