import math
import status_parser
from common.imports import opcodes, types
from common.objects.base_classes.ser import Ser
from common.epa_package.wbw_imports import *
from common.epa_package import equations
from common.objects.base_classes.base_methods import BaseMethods
from common.objects.messages.programming.zone_program_messages import ZoneProgramMessages
# from common.objects.object_bucket import moisture_sensors, zone_programs
# from common.objects.programming.pg_1000 import PG1000
# from common.objects.programming.pg_3200 import PG3200V16
# from common.objects.controller.zn import Zone
from common.objects.base_classes import messages
from datetime import datetime, timedelta


__author__ = 'baseline'


class ZoneProgram(BaseMethods):
    """
    Zone Program Object. \n
    """
    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    # Browser Instance
    browser = ''

    # Moisture sensor object reference which is pointed from object_bucket.py

    moisture_sensor_objects = None
    zone_objects = None
    zone_program_objects = None

    #################################
    def __init__(self, _controller, zone_ad, prog_ad):
        """
        Zone Program object constructor. The same object is used for the 1000 and 3200 controllers. \n
        -   A run time is required for the zone program for use on both the 1000 and
            3200 controller.
        -   1000 controller parameters: _rt
        -   3200 controller parameters: _rt, _ct, _so, _pz, _ra, _ws, _ms, _ll, _ul, _cc, _ee

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 \n

        :param zone_ad: Address of one of the zone objects to convert into a program zone \n
        :type zone_ad:  int \n

        :param prog_ad: Program address from one of the program objects to associate this zone program to. \n
        :type prog_ad:  int \n
        """
        # Give access to the controller
        self.controller = _controller

        # create message reference to object
        self.messages = ZoneProgramMessages(_zone_program_object=self)  # Zone program messages
        """:type: common.objects.messages.programming.zone_program_messages.ZoneProgramMessages"""

        # Init parent class to inherit respective attributes
        BaseMethods.__init__(self,
                             _identifiers=[[opcodes.zone_program,zone_ad], [opcodes.program,prog_ad]],
                             _type=opcodes.zone_program,
                             _ser=_controller.ser)
        self.controller = _controller

        self.data = status_parser.KeyValues('')

        # Address of a zone (from a zone object) to associate this zone program with. This could be different from the
        # primary zone number parameter (_pz) if this zone program is linked to a different zone or has a strategy of
        # timed.
        self.zone = self.controller.zones[zone_ad]
        self.program = self.controller.programs[prog_ad]

        # Address of a program (from a program object) to associate this zone program with.
        # self.program.ad = prog_num

        # Run time
        self.rt = 60

        # Check to see if we are a 3200 controller
        if self.controller.controller_type == "32":

            # Runtime Tracking Ratio
            self.ra = 100   # default to 100%

            # Cycle Time Passed In
            self.ct = 0     # default to 0 seconds

            # Soak time passed in
            self.so = 0     # default to 0 seconds

            # TODO if zone is a linked need to check what program the primary is in and set it to that or raise an
            # exeception
            # Zone number
            self.pz = 0     # default to '0' - Timed

            # self.linked = self.pz != self.zone.ad and self.pz != 0
            # self.timed = self.pz == 0

            # Water Strategy
            self.ws = 'TM'  # default to 'TM':  Timed TM = Timed | LL = Lower Limit |UL = Upper Limit

            # Primary Zone Moisture Sensor
            self.ms = 0    # default to ''

            # Lower limit threshold
            self.ll = 26.0    # default to 26.0

            # Upper limit threshold
            self.ul = 30.0   # default to 30.0

            # Calibration Cycle
            self.cc = 'NV'  # default to 'NV':  NV = Never | SG = One Time | MO = Monthly

            # Enable et Calculated Runtime
            self.ee = 'FA'  # default to 'FA' Enabled false True of False

            # Re-calculate run time based off of runtime tracking ratio (this needs to be done in case the program
            # zone is linked and have the run times be a percentage of the primary zone)
            self.rt = self.calculate_3200_runtime(runtime=self.rt)
            # Re-calculate cycle time based off of runtime tracking ratio (this needs to be done in case the program
            # zone is linked and have the cycle times be a percentage of the primary zone)
            self.ct = self.calculate_3200_cycletime(cycletime=self.ct)
            # if a zone is a linked zone the soak time is equal to the primary zone soak time
            self.so = self.calculate_3200_soaktime(soaktime=self.so)
            # If the zone has a firmware version above V16, give the mainline that used to be on the program to the zone
            # We do this because on 3200 code V16 and above, mainlines now reside on zones instead of Programs
            # removed this call for v16 controllers
            # if self.controller.vr >= opcodes.firmware_version_V16:
            #     self.move_mainline_assignments(program=self.program.ad, zone=self.zone.ad, mainline=self.program.ml)

        # Holds the messages return value
        # self.build_message_string = ''

    #################################
    def is_timed(self):
        """
        Returns if program zone is timed.
        """
        return self.pz == 0

    #################################
    def is_primary(self):
        """
        Returns if program zone is primary.
        """
        return self.pz == self.zone.ad

    #################################
    def is_linked(self):
        """
        Returns if program zone is linked.
        """
        return not self.is_timed() and not self.is_primary()

    #################################
    def calculate_3200_runtime(self, runtime):
        """
        Calculates the runtime to return based on the 'Runtime Tracking Ratio' set for the 3200.
        if et is enabled on a zone that the runtime gets rounded to the nearest second
        :param runtime:     Run time / cycle time to recalculate \n
        :type runtime:      int
        """
        # TODO do need to check if et is enabled
        if self.pz != 0 and self.pz != self.zone.ad:
            for zone_program in self.program.zone_programs.values():
                if zone_program.zone.ad == self.pz and zone_program.program.ad == self.program.ad:
                    runtime = zone_program.rt
        if runtime < 60:
            return 60
        seconds = runtime * (float(self.ra) / float(100))
        # this calculates if we are divisible by 60 or not, if not we want to add it to the total (for rounding)
        if self.ee == opcodes.true:
            # TODO the controller is not rounding
            remainder = seconds % 60.0

            # if odd minutes
            if remainder != 0:
                seconds += remainder
                return int(seconds)
        else:
            return int(seconds)

    #################################
    def calculate_3200_cycletime(self, cycletime):
        """
        Calculates the cycletime to return based on the 'Runtime Tracking Ratio' set for the 3200.
        if et is enabled on a zone that the cycletime gets rounded to the nearest second
        :param cycletime:     Run time / cycle time to recalculate \n
        :type cycletime:      int
        """
        if self.pz != 0 and self.pz != self.zone.ad:
            for zone_program in self.program.zone_programs.values():
                if zone_program.zone.ad == self.pz and zone_program.program.ad == self.program.ad:
                    cycletime = zone_program.ct
        if self.ee is opcodes.true:
            if cycletime < 60:
                return 60
        # TODO need to look if RA is = to zero
        seconds = cycletime * (float(self.ra) / float(100))
        # this calculates if we are divisible by 60 or not, if not we want to add it to the total (for rounding)
        # if self.ee == opcodes.true:
        # Overwrite current attribute value
        # take the value in seconds divide by 60 to convert it to minutes convert it to an integer and than
        # convert back to seconds this way you are dropping of the fractional part of the seconds
        seconds = 60 * (int(seconds / 60))

        return int(seconds)


    #################################
    def calculate_3200_soaktime(self, soaktime):
            """
            Calculates the soaktime based on the primary zone
            :param soaktime:     soaktime
            :type soaktime:      int
            """
            if self.pz != 0 and self.pz != self.zone.ad:
                for zone_program in self.program.zone_programs.values():
                    if zone_program.zone.ad == self.pz and zone_program.program.ad == self.program.ad:
                        soaktime = int(60 * (math.ceil(zone_program.so / 60.0)))
            else:
                soaktime = self.so
            return int(soaktime)


    #################################
    def build_obj_configuration_for_send(self):
        """
        Builds the 'SET' string for sending this Zone Program Instance to the Controller. This method was created in
        this case only for the 3200 Zone Program because there are numerous attributes that could have no value when
        trying to set at the controller, thus we want to make sure to not send any empty attributes. \n
        :return:
        :rtype:
        """
        # If 1000 controller
        if self.controller.controller_type == "10":
            current_config = "{0},{1}={2},{3}={4},{5}={6}".format(
                types.ActionCommands.SET,       # {0}
                opcodes.zone_program,           # {1}
                str(self.zone.ad),              # {2}
                opcodes.program,                # {3}
                str(self.program.ad),           # {4}
                str(opcodes.run_time),          # {5}
                str(self.rt)                    # {6}
            )

        # Else, 3200 controller
        else:
            # Starting string, always will need this, 'SET,PZ=Blah,PG=Blah'
            current_config = "{0},{1}={2},{3}={4}".format(
                types.ActionCommands.SET,   # {0}
                opcodes.zone_program,       # {1}
                str(self.zone.ad),          # {2}
                opcodes.program,            # {3}
                str(self.program.ad),       # {4}
            )

            # 3200 Zone Program attribute list of tuples (each tuple represents the OpCode string and associated value)
            if self.pz != 0 and self.pz != self.zone.ad:
                if self.program.ad != self.program.zone_programs[self.pz].program.ad:
                    zone_with_matching_program_found = False
                    # Loops through all zone Programs and checks if there are any zone Programs with a zone matching
                    # this primary zone, and a program that matches this objects program
                    for zone_program in self.program.zone_programs.values():
                        if zone_program.zone.ad == self.pz:
                            if self.program.ad != zone_program.program.ad:
                                zone_with_matching_program_found = True
                    if not zone_with_matching_program_found:
                        e_msg = "Zone must be in the same program as the primary zone"
                        raise Exception(e_msg)
                zone_program_attr = [
                    (opcodes.runtime_tracking_ratio, self.ra),
                    (opcodes.zone_program, self.pz),
                ]
            else:
                zone_program_attr = [
                    (opcodes.run_time, self.rt),
                    (WaterSenseCodes.Enable_ET_Runtime, self.ee),
                    (opcodes.cycle_time, self.ct),
                    (opcodes.soak_cycle, self.so),
                    (opcodes.runtime_tracking_ratio, self.ra),
                    (opcodes.zone_program, self.pz),
                    (opcodes.water_strategy, self.ws),
                    (opcodes.moisture_sensor, self.ms),
                    (opcodes.lower_limit, self.ll),
                    (opcodes.upper_limit, self.ul),
                    (opcodes.calibrate_cycle, self.cc)

                ]
            # For each attribute, check if the attribute has a value that isn't 'None' or '' (empty string).
            for attr_tuple in zone_program_attr:
                arg1 = attr_tuple[0]
                arg2 = attr_tuple[1]

                # If the attribute value isn't 'None' or '', append it to the current_config string
                if attr_tuple[1]:

                    # Check if current attribute being processed is moisture sensor, if so, go grab that moisture sensor
                    # serial number
                    if arg1 == opcodes.moisture_sensor:
                        if isinstance(self.ms, int):
                            arg2 = self.controller.moisture_sensors[self.ms].sn
                        else:
                            arg2 = ''

                    current_config += ",{0}={1}".format(
                        arg1,   # {0}
                        arg2    # {1}
                    )

        return current_config

    #################################
    def __str__(self):
        """
        Returns string representation of this Zone Program Object. \n
        :return:    String representation of this Zone Program Object
        :rtype:     str
        """
        return_str = "\n-----------------------------------------\n" \
                     "Zone Program {0} associated with Program {1}: \n" \
                     "Run Time:         {2} seconds\n" \
                     "Cycle Time:       {3} seconds\n" \
                     "Soak Time:        {4} seconds\n" \
                     "Zone Number:      {5} \n" \
                     "Runtime ratio:    {6}% \n" \
                     "Water Strategy:   {7} \n" \
                     "Moisture Sensor:  {8} \n" \
                     "Lower limit:      {9}% \n" \
                     "Upper limit:      {10}% \n" \
                     "Calibrate Cycle:  {11} \n" \
                     "Enable Et Runtime: {12} \n"\
                     "-----------------------------------------\n".format(
                         str(self.zone.ad),     # {0}
                         str(self.program.ad),  # {1}
                         str(self.rt),              # {2}
                         str(self.ct),              # {3}
                         str(self.so),              # {4}
                         str(self.pz),              # {5}
                         str(self.ra),              # {6}
                         self.ws,                   # {7}
                         self.ms,                   # {8}
                         str(self.ll),              # {9}
                         str(self.ul),              # {10}
                         self.cc,                   # {11}
                         self.ee                    # {12}
                     )
        return return_str

    #################################
    def add_moisture_sensor_to_primary_zone(self, _moisture_sensor_address=None):
        """
        Sets the 'Moisture Sensor Pointer' for the Zone program to the specified moisture sensor serial number. \n
        :param _moisture_sensor_address:  Address of moisture sensor to assign to Zone program \n
        :type _moisture_sensor_address:   int \n
        """
        if self.controller.controller_type == "32":

            # If trying to overwrite current value
            if _moisture_sensor_address is not None:

                # Validate argument value is within accepted values
                if _moisture_sensor_address not in self.controller.moisture_sensors.keys():
                    e_msg = "Invalid Moisture Sensor address. " \
                            "Verify address exists in object json configuration and/or in " \
                            "current test. Received address: {0}, available addresses: {1}".format(
                                str(_moisture_sensor_address),                       # {0}
                                str(self.controller.moisture_sensors.keys()),   # {1}
                            )
                    raise ValueError(e_msg)

                else:
                    # Overwrite current attribute value
                    self.ms = _moisture_sensor_address

            # Get MS Serial from object
            ms_serial_from_obj = self.controller.moisture_sensors[self.ms].sn

            command = "{0},{1}={2},{3}={4},{5}={6}".format(
                types.ActionCommands.SET,       # {0}
                opcodes.zone_program,           # {1}
                str(self.zone.ad),              # {2}
                opcodes.program,                # {3}
                str(self.program.ad),           # {4}
                opcodes.moisture_sensor,        # {5}
                ms_serial_from_obj              # {6}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Moisture Sensor Serial " \
                        "Number' to: {2} ({3})\n{4}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            ms_serial_from_obj,         # {2}
                            str(self.ms),               # {3}
                            str(ae.message)             # {4}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Moisture Sensor Serial Number' for (Zone Program {0}, Program {1}) to: {2} "
                      "({3})".format(
                          str(self.zone.ad),      # {0}
                          str(self.program.ad),   # {1}
                          ms_serial_from_obj,         # {2}
                          str(self.ms),               # {3}
                      ))
        else:
            raise ValueError("Attempting to set Moisture Sensor Serial Number for a Zone Program for 1000 which is "
                             "NOT SUPPORTED")

    #################################
    def set_run_time(self, _minutes=None):
        """
        Sets the 'Run Time' for the Zone program on the controller. \n
        :param _minutes:   Run time for zone program in seconds \n
        :type _minutes:    int \n
        """
        # If trying to overwrite current run time
        if _minutes is not None:
            convert_to_seconds = _minutes*60
            # Overwrite current attribute value
            self.rt = convert_to_seconds

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,       # {0}
                  opcodes.zone_program,     # {1}
                  str(self.zone.ad),        # {2}
                  opcodes.program,          # {3}
                  str(self.program.ad),     # {4}
                  opcodes.run_time,         # {5}
                  str(self.rt)              # {6}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Run Time' to: {2}\n " \
                    "{3}".format(
                        str(self.zone.ad),          # {0}
                        str(self.program.ad),       # {1}
                        str(self.rt),               # {2}
                        str(ae.message)             # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Run Time' for (Zone Program {0}, Program {1}) to: {2}".format(
                  str(self.zone.ad),      # {0}
                  str(self.program.ad),   # {1}
                  str(self.rt)            # {2}
                  ))

    #################################
    def set_cycle_time(self, _minutes=None, _use_calculated_cycle_time=False):
        """
        Sets the 'Cycle Time' for the Zone Program on the controller. \n
        :param _minutes:                     Cycle time to set \n
        :type _minutes:                      int \n

        :param _use_calculated_cycle_time:      Cycle time to set \n
        :type _use_calculated_cycle_time:       bool  | float\n
        """

        if self.controller.controller_type == "32":
            # this goes out to the equations module and returns a calculated Cycle time to the zp.py object
            seconds = 0
            if _use_calculated_cycle_time is True:
                seconds = self.zone.get_calculated_cycle_time()
            # If trying to overwrite current value
            elif _minutes is not None:
                seconds = _minutes * 60

            # Overwrite current attribute value
            # take the value in seconds divide by 60 to convert it to minutes convert it to an integer and than
            # convert back to seconds this way you are dropping of the fractional part of the seconds
            self.ct = 60*(int(seconds/60))

            # self.ct = seconds

            command = "{0},{1}={2},{3}={4},{5}={6}".format(
                      types.ActionCommands.SET,     # {0}
                      opcodes.zone_program,         # {1}
                      str(self.zone.ad),            # {2}
                      opcodes.program,              # {3}
                      str(self.program.ad),         # {4}
                      opcodes.cycle_time,           # {5}
                      str(self.ct)                  # {6}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Cycle Time' to: {2}\n " \
                        "{3}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            str(self.ct),               # {2}
                            str(ae.message)             # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Cycle Time' for (Zone Program {0}, Program {1}) to: {2}".format(
                      str(self.zone.ad),      # {0}
                      str(self.program.ad),   # {1}
                      str(self.ct)                # {2}
                      ))
        else:
            raise ValueError("Attempting to set Cycle Time for a Zone Program for 1000 which is NOT SUPPORTED")

    #################################
    def set_soak_time(self, _minutes=None, _use_calculated_soak_time=False):
        """
        Sets the 'Soak Time' for the Zone Program on the controller. \n
        :param _minutes:                      Soak time to set \n
        :type _minutes:                       int  | float\n

        :param _use_calculated_soak_time:       Soak time to set \n
        :type _use_calculated_soak_time:        bool  | float\n
        """
        if self.controller.controller_type == "32":
            # this goes out to the equations module and returns a calculated soak time to the zp.py object
            seconds = 0
            if _use_calculated_soak_time is True:
                seconds = self.zone.get_calculated_soak_time()
            # If trying to overwrite current value
            elif _minutes is not None:
                seconds = _minutes * 60
            # Overwrite current attribute value
            # take the value in seconds divide by 60 to convert it to minutes convert it to an integer and than
            # add 1 to the value to always round up and then convert back to seconds
            if seconds == 0:
                self.so = 0
            else:
                self.so = int(60 * (math.ceil(seconds / 60.0)))
            # self.so = seconds

            command = "{0},{1}={2},{3}={4},{5}={6}".format(
                      types.ActionCommands.SET,     # {0}
                      opcodes.zone_program,         # {1}
                      str(self.zone.ad),            # {2}
                      opcodes.program,              # {3}
                      str(self.program.ad),         # {4}
                      opcodes.soak_cycle,           # {5}
                      str(self.so)                  # {6}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Soak Time' to: {2}\n " \
                        "{3}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            str(self.so),               # {2}
                            str(ae.message)             # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Soak Time' for (Zone Program {0}, Program {1}) to: {2}".format(
                      str(self.zone.ad),      # {0}
                      str(self.program.ad),   # {1}
                      str(self.so)                # {2}
                      ))
        else:
            raise ValueError("Attempting to set Soak Time for a Zone Program for 1000 which is NOT SUPPORTED")

    #################################
    def set_as_timed_zone(self):
        """
        Set's the Zone Program's mode on the controller. \n
        """

        self.pz = 0

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,       # {0}
            opcodes.zone_program,           # {1}
            str(self.zone.ad),              # {2}
            opcodes.program,                # {3}
            str(self.program.ad),           # {4}
            opcodes.primary_zone_number,    # {5}
            str(self.pz)                    # {6}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Primary Zone' to: {2}\n " \
                    "{3}".format(
                        str(self.zone.ad),  # {0}
                        str(self.program.ad),  # {1}
                        str(self.pz),  # {2}
                        str(ae.message)  # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Primary Zone' for (Zone Program {0}, Program {1}) to: {2}".format(
                str(self.zone.ad),  # {0}
                str(self.program.ad),  # {1}
                str(self.pz)  # {2}
            ))

    #################################
    def set_as_primary_zone(self):
        """
        Set's the Zone Program's mode on the controller. \n
        """
        self.pz = self.zone.ad

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
                  types.ActionCommands.SET,     # {0}
                  opcodes.zone_program,         # {1}
                  str(self.zone.ad),            # {2}
                  opcodes.program,              # {3}
                  str(self.program.ad),         # {4}
                  opcodes.primary_zone_number,  # {5}
                  str(self.pz)                  # {6}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Primary Zone' to: {2}\n " \
                    "{3}".format(
                        str(self.zone.ad),      # {0}
                        str(self.program.ad),   # {1}
                        str(self.pz),           # {2}
                        str(ae.message)         # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Zone' {0} as Primary zone on Program {1}".format(
                str(self.zone.ad),          # {0}
                str(self.program.ad),       # {1}
            ))

    #################################
    def set_as_linked_zone(self, _primary_zone, _tracking_ratio=100):
        """
        Set's the Zone Program's mode on the controller. \n
        """
        self.pz = _primary_zone
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,       # {0}
            opcodes.zone_program,           # {1}
            str(self.zone.ad),              # {2}
            opcodes.program,                # {3}
            str(self.program.ad),           # {4}
            opcodes.primary_zone_number,    # {5}
            str(self.pz)                    # {6}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Primary Zone' to: {2}\n " \
                    "{3}".format(
                        str(self.zone.ad),          # {0}
                        str(self.program.ad),       # {1}
                        str(self.pz),               # {2}
                        str(ae.message)             # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully linked 'Zone' {0} to Primary zone {1} on Program {2}".format(
                str(self.zone.ad),          # {0}
                str(self.pz),               # {1}
                str(self.program.ad)        # {2}

            ))
            self.set_tracking_ratio(_runtime_ratio=_tracking_ratio)

    #################################
    def set_tracking_ratio(self, _runtime_ratio=None):
        """
        Sets the 'Runtime Ratio' for the current Zone Program \n
        :param _runtime_ratio:  Runtime ratio as an integer \n
        :type _runtime_ratio:   int \n
        """
        if self.controller.controller_type == "32":

            # If trying to overwrite current value
            if _runtime_ratio is not None:

                # Overwrite current attribute value
                self.ra = _runtime_ratio
                # TODO need to add this to the unit test
                # this updates rt and ct with the new values that have been adjusted do to the tracking ration
                self.rt = self.calculate_3200_runtime(runtime=self.rt)
                self.ct = self.calculate_3200_cycletime(cycletime=self.ct)
                self.so = self.calculate_3200_soaktime(soaktime=self.so)

            command = "{0},{1}={2},{3}={4},{5}={6}".format(
                types.ActionCommands.SET,         # {0}
                opcodes.zone_program,             # {1}
                str(self.zone.ad),                # {2}
                opcodes.program,                  # {3}
                str(self.program.ad),             # {4}
                opcodes.runtime_tracking_ratio,   # {5}
                str(self.ra)                      # {6}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Runtime Ratio' to: {2}\n" \
                        "{3}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            str(self.ra),               # {2}
                            str(ae.message)             # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Runtime Ratio' for (Zone {0} on Program {1}) to: {2}".format(
                      str(self.zone.ad),      # {0}
                      str(self.program.ad),   # {1}
                      str(self.ra)            # {2}
                      ))

        else:
            raise ValueError("Attempting to set Runtime Ratio for a Zone Program for 1000 which is NOT SUPPORTED")

    #################################
    def set_water_strategy_to_timed(self):
        """
        Sets the 'Water Strategy to Timed ' for the Zone Program. \n

        """
        # Overwrite current attribute value
        self.ws = opcodes.timed

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,           # {0}
                  opcodes.zone_program,         # {1}
                  str(self.zone.ad),            # {2}
                  opcodes.program,              # {3}
                  str(self.program.ad),         # {4}
                  opcodes.water_strategy,       # {5}
                  str(self.ws)                  # {6}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Watering Strategy' to: " \
                    "{2}\n{3}".format(
                        str(self.zone.ad),          # {0}
                        str(self.program.ad),       # {1}
                        str(self.ws),               # {2}
                        str(ae.message)             # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Watering Strategy' for (Zone Program {0}, Program {1}) to: {2}".format(
                  str(self.zone.ad),      # {0}
                  str(self.program.ad),   # {1}
                  str(self.ws)            # {2}
                  ))

    #################################
    def set_water_strategy_to_lower_limit(self):
        """
        Sets the 'Water Strategy to lower Limit watering' for the Zone Program. \n

        """

        # Overwrite current attribute value
        self.ws = opcodes.lower_limit

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,           # {0}
                  opcodes.zone_program,         # {1}
                  str(self.zone.ad),            # {2}
                  opcodes.program,              # {3}
                  str(self.program.ad),         # {4}
                  opcodes.water_strategy,       # {5}
                  str(self.ws)                  # {6}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Watering Strategy' to: " \
                    "{2}\n{3}".format(
                        str(self.zone.ad),      # {0}
                        str(self.program.ad),   # {1}
                        str(self.ws),           # {2}
                        str(ae.message)         # {3}
                        )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Watering Strategy' for (Zone Program {0}, Program {1}) to: {2}".format(
                str(self.zone.ad),      # {0}
                str(self.program.ad),   # {1}
                str(self.ws)            # {2}
            ))

    #################################
    def set_water_strategy_to_upper_limit(self):
        """
        Sets the 'Water Strategy' for the Zone Program. \n
        """
        # Overwrite current attribute value
        self.ws = opcodes.upper_limit

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,       # {0}
            opcodes.zone_program,           # {1}
            str(self.zone.ad),              # {2}
            opcodes.program,                # {3}
            str(self.program.ad),           # {4}
            opcodes.water_strategy,         # {5}
            str(self.ws)                    # {6}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Watering Strategy' to: " \
                    "{2}\n{3}".format(
                        str(self.zone.ad),  # {0}
                        str(self.program.ad),  # {1}
                        str(self.ws),  # {2}
                        str(ae.message)  # {3}
                        )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Watering Strategy' for (Zone Program {0}, Program {1}) to: {2}".format(
                str(self.zone.ad),  # {0}
                str(self.program.ad),  # {1}
                str(self.ws)  # {2}
            ))

    #################################
    def set_lower_limit_threshold(self, _percent=None):
        """
        Sets the 'Lower Limit Threshold' for the current Zone program \n
        :param _percent:    New lower limit to set to \n
        :type _percent:     float \n
        """
        if self.controller.controller_type == "32":

            # If trying to overwrite current value
            if _percent is not None:

                # Overwrite current attribute value
                self.ll = _percent

            command = "{0},{1}={2},{3}={4},{5}={6}".format(
                types.ActionCommands.SET,       # {0}
                opcodes.zone_program,           # {1}
                str(self.zone.ad),              # {2}
                opcodes.program,                # {3}
                str(self.program.ad),           # {4}
                opcodes.lower_limit,            # {5}
                str(self.ll)                    # {6}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Lower Limit Threshold' " \
                        "to: {2}\n{3}".format(
                            str(self.zone.ad),      # {0}
                            str(self.program.ad),   # {1}
                            str(self.ll),               # {2}
                            str(ae.message)             # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Lower Limit Threshold' for (Zone Program {0}, Program {1}) to: {2}"
                      .format(
                          str(self.zone.ad),      # {0}
                          str(self.program.ad),   # {1}
                          str(self.ll)                # {2}
                      ))
        else:
            raise ValueError("Attempting to set Lower Limit Threshold for a Zone Program for 1000 which is "
                             "NOT SUPPORTED")
        
    #################################
    def set_upper_limit_threshold(self, _upper_limit=None):
        """
        Sets the 'Upper Limit Threshold' for the current Zone program \n
        :param _upper_limit:    New upper limit to set to \n
        :type _upper_limit:     float \n
        """
        if self.controller.controller_type == "32":

            # If trying to overwrite current value
            if _upper_limit is not None:

                # Overwrite current attribute value
                self.ul = _upper_limit

            command = "{0},{1}={2},{3}={4},{5}={6}".format(
                types.ActionCommands.SET,       # {0}
                opcodes.zone_program,           # {1}
                str(self.zone.ad),              # {2}
                opcodes.program,                # {3}
                str(self.program.ad),           # {4}
                opcodes.upper_limit,            # {5}
                str(self.ul)                    # {6}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Upper Limit Threshold' " \
                        "to: {2}\n{3}".format(
                            str(self.zone.ad),          # {0}
                            str(self.program.ad),       # {1}
                            str(self.ul),               # {2}
                            str(ae.message)             # {3}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Upper Limit Threshold' for (Zone Program {0}, Program {1}) to: {2}"
                      .format(
                          str(self.zone.ad),        # {0}
                          str(self.program.ad),     # {1}
                          str(self.ul)              # {2}
                      ))
        else:
            raise ValueError("Attempting to set Upper Limit Threshold for a Zone Program for 1000 which is "
                             "NOT SUPPORTED")
                
    #################################
    def set_one_time_calibration_cycle(self):
        """
        Set to 'Calibrate One Time' for the current Zone program \n
        """
        # Overwrite current attribute value
        self.cc = opcodes.calibrate_one_time

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,       # {0}
            opcodes.zone_program,           # {1}
            str(self.zone.ad),              # {2}
            opcodes.program,                # {3}
            str(self.program.ad),           # {4}
            opcodes.calibrate_cycle,        # {5}
            self.cc                         # {6}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s " \
                    "'Calibrate One Time' to: {2}\n{3}".format(
                         str(self.zone.ad),     # {0}
                         str(self.program.ad),  # {1}
                         self.cc,               # {2}
                         str(ae.message)        # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set to 'Calibrate One Time' for (Zone Program {0}, Program {1}) to: {2}"
                  .format(
                   str(self.zone.ad),  # {0}
                   str(self.program.ad),  # {1}
                   self.cc  # {2}
                  ))

    #################################
    def set_monthly_calibration_cycle(self):
        """
        Set to 'Calibrate Monthly' for the current Zone program \n
        """
        # Overwrite current attribute value
        self.cc = opcodes.calibrate_monthly

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,       # {0}
            opcodes.zone_program,           # {1}
            str(self.zone.ad),              # {2}
            opcodes.program,                # {3}
            str(self.program.ad),           # {4}
            opcodes.calibrate_cycle,        # {5}
            self.cc                         # {6}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Calibrate Monthly'" \
                    " to: {2}\n{3}".format(
                        str(self.zone.ad),      # {0}
                        str(self.program.ad),   # {1}
                        self.cc,                # {2}
                        str(ae.message)         # {3}
                        )
            raise Exception(e_msg)
        else:
            print("Successfully set to 'Calibrate Monthly' for (Zone Program {0}, Program {1}) to: {2}"
                  .format(
                   str(self.zone.ad),       # {0}
                   str(self.program.ad),    # {1}
                   self.cc                  # {2}
                  ))

    #################################
    def set_never_run_calibration_cycle(self):
        """
        Set to 'Never Calibrate' for the current Zone program \n
        """
        # Overwrite current attribute value
        self.cc = opcodes.calibrate_never

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,       # {0}
            opcodes.zone_program,           # {1}
            str(self.zone.ad),              # {2}
            opcodes.program,                # {3}
            str(self.program.ad),           # {4}
            opcodes.calibrate_cycle,        # {5}
            self.cc                         # {6}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Never Calibrate' " \
                    "to: {2}\n{3}".format(
                        str(self.zone.ad),      # {0}
                        str(self.program.ad),   # {1}
                        self.cc,                # {2}
                        str(ae.message)         # {3}
                        )
            raise Exception(e_msg)
        else:
            print("Successfully set to 'Never Calibrate' for (Zone Program {0}, Program {1}) to: {2}"
                  .format(
                   str(self.zone.ad),       # {0}
                   str(self.program.ad),    # {1}
                   self.cc                  # {2}
                  ))

    #################################
    def enable_et_calculated_run_times(self):
        """
        Sets the Enabled State for ET on a program . \n
        :return:
        """

        self.ee = opcodes.true

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,               # {0}
            opcodes.zone_program,                   # {1}
            str(self.zone.ad),                      # {2}
            opcodes.program,                        # {3}
            str(self.program.ad),                   # {4}
            WaterSenseCodes.Enable_ET_Runtime,      # {5}
            self.ee                                 # {6}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)

            # this goes to the zone object and sets an attribute so that the verifier can word
            self.zone.enable_et = self.ee

        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Enable ET Runtime' " \
                    "to: {2}\n{3}".format(
                        str(self.zone.ad),          # {0}
                        str(self.program.ad),       # {1}
                        self.ee,                    # {2}
                        str(ae.message)             # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Enable ET runtime' for (Zone Program {0}, Program {1}) to: {2}"
                  .format(
                      str(self.zone.ad),      # {0}
                      str(self.program.ad),   # {1}
                      self.ee                 # {2}
                  ))

    #################################
    def disable_et_calculated_run_times(self):
        """
        Sets disabled for ET on a program . \n
        :return:
        """
        self.ee = opcodes.false

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            types.ActionCommands.SET,               # {0}
            opcodes.zone_program,                   # {1}
            str(self.zone.ad),                      # {2}
            opcodes.program,                        # {3}
            str(self.program.ad),                   # {4}
            WaterSenseCodes.Enable_ET_Runtime,      # {5}
            self.ee                                 # {6}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)

            # this goes to the zone object and sets an attribute so that the verifier can word
            self.zone.enable_et = self.ee

        except AssertionError as ae:
            e_msg = "Exception occurred trying to set (Zone Program {0}, Program {1})'s 'Enable ET Runtime' " \
                    "to: {2}\n{3}".format(
                        str(self.zone.ad),      # {0}
                        str(self.program.ad),   # {1}
                        self.ee,                # {2}
                        str(ae.message)         # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Enable ET runtime' for (Zone Program {0}, Program {1}) to: {2}".format(
                    str(self.zone.ad),      # {0}
                    str(self.program.ad),   # {1}
                    self.ee                 # {2}
                ))

    #################################
    def verify_runtime(self):
        """
        First Check to see if ET based watering is being used to calculate rt by checking to see if ee is 'TR'.
        Than Verify the 'Runtime' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Runtime' value from the controller reply.
        -   Lastly, compares the instance 'Runtime' value with the controller's programming. 
        # """
        if self.controller.controller_type == "32":
            if self.ee != opcodes.false:
                # goes to equations and returns to object a calculated total runtime to compare against the
                # controller rt

                self.rt = equations.return_todays_calculated_runtime(_du=self.zone.du,
                                                                     _pr=self.zone.pr,
                                                                     _mb=self.zone.mb
                                                                     )
        else:
            # goes to equations and returns to object a calculated total runtime to compare against the controller rt
            # this is for the 1000
            if self.program.ee == opcodes.true:
                self.rt = equations.return_todays_calculated_runtime(_du=self.zone.du,
                                                                     _pr=self.zone.pr,
                                                                     _mb=self.zone.mb
                                                                     )

        print("Print numbers used to Calculate RT:  du: '{0}' pr: '{1}' mb: '{2}' Calculated rt: '{3}' on controller"
              .format(
                  str(self.zone.du),       # {0}
                  str(self.zone.pr),       # {1}
                  str(self.zone.mb),       # {2}
                  str(self.rt),))          # {3}
        # get runtime data from the controller
        runtime_from_controller = int(self.data.get_value_string_by_key(opcodes.run_time))
        # take the value in rt and add .00001 to it to make it a float with at least 6 significant digits
        rounded_rt = int(round(self.rt+.00001))
        # if the 1000 controller calculates a runtime less than 4 minutes it will return a value of zero
        if self.controller.controller_type == "10":
            # if rounded_rt < 240:
            #     rounded_rt = 0
            #     print "The 1000 calculated a runtime less than 4 minutes. Calculated runtime was: "\
            #           + str(rounded_rt) + " seconds, so it is set to zero."
            # Compare status versus what is on the controller
            # because of the tolerances of the number the runtime can be a difference of 29 seconds
            if abs(rounded_rt - runtime_from_controller) > 29:
                e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Runtime' value. Received: {2}, " \
                        "Expected: {3}".format(
                            str(self.zone.ad),              # {0}
                            str(self.program.ad),           # {1}
                            str(runtime_from_controller),   # {2}
                            str(rounded_rt)                 # {3}
                        )
                raise ValueError(e_msg)
        if self.controller.controller_type == "32":
            if abs(rounded_rt - runtime_from_controller) > 1:
                e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Runtime' value. Received: {2}, " \
                        "Expected: {3}".format(
                            str(self.zone.ad),              # {0}
                            str(self.program.ad),           # {1}
                            str(runtime_from_controller),   # {2}
                            str(rounded_rt)                 # {3}
                        )
                raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Calculated Runtime' value: '{2}' against controller"
                  "Runtime value: '{3}' ".format(
                    str(self.zone.ad),              # {0}
                    str(self.program.ad),           # {1}
                    str(rounded_rt),                # {2}
                    str(runtime_from_controller)    # {3}
                    ))

    #################################
    def verify_cycle_time(self):
        """
        Verifies the 'Cycle Time' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Cycle Time' value from the controller reply.
        -   Lastly, compares the instance 'Cycle Time' value with the controller's programming. 
        """

        cycle_time_from_controller = int(self.data.get_value_string_by_key(opcodes.cycle_time))

        # if using weather base water and cycle time for the controller is set to less than 240 seconds than the
        # controller will return 240 seconds

        ct_compare = self.ct

        if self.ee != opcodes.false:
            if 0 < ct_compare <= 239:
                # check to see if controller is 240
                ct_compare = 240

        # Compare status versus what is on the controller
        if ct_compare != cycle_time_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Cycle Time' value. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self.zone.ad),                  # {0}
                        str(self.program.ad),               # {1}
                        str(cycle_time_from_controller),    # {2}
                        str(self.ct)                        # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Cycle Time' value: '{2}' on controller".format(
                str(self.zone.ad),          # {0}
                str(self.program.ad),       # {1}
                str(self.ct)                # {2}
            ))            
            
    #################################
    def verify_soak_time(self):
        """
        Verifies the 'Soak Time' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Soak Time' value from the controller reply.
        -   Lastly, compares the instance 'Soak Time' value with the controller's programming. 
        """

        soak_time_from_controller = int(self.data.get_value_string_by_key(opcodes.soak_cycle))

        # Compare status versus what is on the controller
        if self.so != soak_time_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Soak Time' value. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self.zone.ad),                  # {0}
                        str(self.program.ad),               # {1}
                        str(soak_time_from_controller),     # {2}
                        str(self.so)                        # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Soak Time' value: '{2}' on controller".format(
                str(self.zone.ad),          # {0}
                str(self.program.ad),       # {1}
                str(self.so)                # {2}
            ))
            
    #################################
    def verify_program_zones_mode(self):
        """
        Verifies the 'Primary Zone Number' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Primary Zone Number' value from the controller reply.
        -   Lastly, compares the instance 'Primary Zone Number' value with the controller's programming. 
        """

        zone_pointer_from_controller = int(self.data.get_value_string_by_key(opcodes.zone_program))
    
        # Compare status versus what is on the controller
        if self.pz != zone_pointer_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Primary Zone Number' value. Received: {2}, " \
                    "Expected: {3}".format(
                        str(self.zone.ad),                      # {0}
                        str(self.program.ad),                   # {1}
                        str(zone_pointer_from_controller),      # {2}
                        str(self.pz)                            # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Primary Zone Number' value: '{2}' on controller".format(
                str(self.zone.ad),          # {0}
                str(self.program.ad),       # {1}
                str(self.pz)                # {2}
            ))

    #################################
    def verify_runtime_tracking_ratio(self):
        """
        Verifies the 'Runtime Tracking Ratio' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Runtime Tracking Ratio' value from the controller reply.
        -   Lastly, compares the instance 'Runtime Tracking Ratio' value with the controller's programming. 
        """
        runtime_ratio_from_controller = int(self.data.get_value_string_by_key(opcodes.runtime_tracking_ratio))

        # Compare status versus what is on the controller
        if self.ra != runtime_ratio_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Runtime Tracking Ratio' value. Received: {2}" \
                    ", Expected: {3}".format(
                        str(self.zone.ad),                      # {0}
                        str(self.program.ad),                   # {1}
                        str(runtime_ratio_from_controller),     # {2}
                        str(self.ra)                            # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Runtime Tracking Ratio' value: '{2}' on controller"
                  .format(
                      str(self.zone.ad),          # {0}
                      str(self.program.ad),       # {1}
                      str(self.ra)                # {2}
                  ))
            
    #################################
    def verify_water_strategy(self):
        """
        Verifies the 'Water Strategy' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Water Strategy' value from the controller reply.
        -   Lastly, compares the instance 'Water Strategy' value with the controller's programming. 
        """

        water_strategy_from_controller = str(self.data.get_value_string_by_key(opcodes.water_strategy))
    
        # Compare status versus what is on the controller
        if self.ws != water_strategy_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Water Strategy' value. Received: {2}" \
                    ", Expected: {3}".format(
                        str(self.zone.ad),                      # {0}
                        str(self.program.ad),                   # {1}
                        str(water_strategy_from_controller),    # {2}
                        str(self.ws)                            # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Water Strategy' value: '{2}' on controller".format(
                  str(self.zone.ad),            # {0}
                  str(self.program.ad),         # {1}
                  str(self.ws)                  # {2}
                  ))
            
    #################################
    def verify_primary_zone_moisture_sensor(self):
        """
        Verifies the 'Primary Zone Moisture Sensor' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Primary Zone Moisture Sensor' value from the controller reply.
        -   Lastly, compares the instance 'Primary Zone Moisture Sensor' value with the controller's programming. 
        """

        primary_zone_moisture_from_controller = str(self.data.get_value_string_by_key(opcodes.moisture_sensor))

        # Get the current assigned serial number
        current_assigned_ms_serial = self.controller.moisture_sensors[self.ms].sn

        # Compare status versus what is on the controller
        if current_assigned_ms_serial != primary_zone_moisture_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Primary Zone Moisture Sensor' value. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.zone.ad),                              # {0}
                        str(self.program.ad),                           # {1}
                        str(primary_zone_moisture_from_controller),     # {2}
                        current_assigned_ms_serial                      # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Primary Zone Moisture Sensor' value: '{2}' on controller"
                  .format(
                      str(self.zone.ad),            # {0}
                      str(self.program.ad),         # {1}
                      current_assigned_ms_serial    # {3}
                  ))
            
    #################################
    def verify_lower_limit_threshold(self):
        """
        Verifies the 'Lower Limit Threshold' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Lower Limit Threshold' value from the controller reply.
        -   Lastly, compares the instance 'Lower Limit Threshold' value with the controller's programming. 
        """

        lower_limit_threshold_from_controller = float(self.data.get_value_string_by_key(opcodes.lower_limit))
    
        # Compare status versus what is on the controller
        if self.ll != lower_limit_threshold_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Lower Limit Threshold' value. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.zone.ad),                              # {0}
                        str(self.program.ad),                           # {1}
                        str(lower_limit_threshold_from_controller),     # {2}
                        str(self.ll)                                    # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Lower Limit Threshold' value: '{2}' on controller"
                  .format(
                      str(self.zone.ad),            # {0}
                      str(self.program.ad),         # {1}
                      str(self.ll)                  # {2}
                  ))
            
    #################################
    def verify_upper_limit_threshold(self):
        """
        Verifies the 'Upper Limit Threshold' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Upper Limit Threshold' value from the controller reply.
        -   Lastly, compares the instance 'Upper Limit Threshold' value with the controller's programming. 
        """

        upper_limit_threshold_from_controller = float(self.data.get_value_string_by_key(opcodes.upper_limit))
    
        # Compare status versus what is on the controller
        if self.ul != upper_limit_threshold_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Upper Limit Threshold' value. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.zone.ad),                              # {0}
                        str(self.program.ad),                           # {1}
                        str(upper_limit_threshold_from_controller),     # {2}
                        str(self.ul)                                    # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Upper Limit Threshold' value: '{2}' on controller"
                  .format(
                      str(self.zone.ad),          # {0}
                      str(self.program.ad),       # {1}
                      str(self.ul)                # {2}
                  ))
            
    #################################
    def verify_calibrate_cycle(self):
        """
        Verifies the 'Calibrate Cycle' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into 
            'self.data'.
        -   Second, retrieves the 'Calibrate Cycle' value from the controller reply.
        -   Lastly, compares the instance 'Calibrate Cycle' value with the controller's programming. 
        """

        calibrate_cycle_from_controller = self.data.get_value_string_by_key(opcodes.calibrate_cycle)
    
        # Compare status versus what is on the controller
        if self.cc != calibrate_cycle_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Calibrate Cycle' value. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.zone.ad),                              # {0}
                        str(self.program.ad),                           # {1}
                        str(calibrate_cycle_from_controller),           # {2}
                        str(self.cc)                                    # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Calibrate Cycle' value: '{2}' on controller"
                  .format(
                      str(self.zone.ad),          # {0}
                      str(self.program.ad),       # {1}
                      str(self.cc)                # {2}
                  ))

    #################################
    def verify_enable_et_state(self):
        """
        Verifies the 'Enable ET Runtime State' value for the Zone Program on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'ET Runtime State' value from the controller reply.
        -   Lastly, compares the instance 'ET Runtime State' value with the controller's programming.
        """

        et_state_from_controller = self.data.get_value_string_by_key(WaterSenseCodes.Enable_ET_Runtime)

        # Compare status versus what is on the controller
        if self.ee != et_state_from_controller:
            e_msg = "Unable to verify (Zone Program {0}, Program {1})'s 'Enable Et Runtime State' value. " \
                    "Received: {2}, Expected: {3}".format(
                        str(self.zone.ad),                          # {0}
                        str(self.program.ad),                       # {1}
                        str(et_state_from_controller),              # {2}
                        str(self.ee)                                # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified (Zone Program {0}, Program {1})'s 'Enable Et Runtime State' value: '{2}' on controller"
                  .format(
                      str(self.zone.ad),          # {0}
                      str(self.program.ad),       # {1}
                      str(self.ee)                # {2}
                  ))

    #################################
    def verify_who_i_am(self):
        """
        Verifier wrapper which verifies all attributes for this 'Zone Program'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify the one attribute that both 1000 and 3200 zone Programs share
        self.verify_runtime()

        # Verify 3200 zone program specific attributes
        if self.controller.controller_type == "32":
            self.verify_cycle_time()
            self.verify_soak_time()
            self.verify_program_zones_mode()
            self.verify_enable_et_state()

            # Check if we are a linked zone, if so, verify runtime tracking ratio
            if self.pz != 0 and self.pz != self.zone.ad:
                self.verify_runtime_tracking_ratio()

            # Primary zone and timed watering strategy
            elif self.pz == self.zone.ad:
                self.verify_water_strategy()

                # Not a timed water strategy? Need to verify more things
                if self.ws != opcodes.timed:

                    # Do we have an assigned ms? If so verify it
                    if self.ms:
                        self.verify_primary_zone_moisture_sensor()

                    self.verify_lower_limit_threshold()
                    self.verify_upper_limit_threshold()
                    self.verify_calibrate_cycle()
