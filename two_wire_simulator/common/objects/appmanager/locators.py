__author__ = 'baseline'

from selenium.webdriver.common.by import By


class AppManagerLoginLocators(object):
    USER_NAME_INPUT = (By.ID, "bl-username-input")
    PASSWORD_INPUT = (By.ID, "bl-password-input")
    SIGN_IN_BUTTON = (By.ID, "login-button")
    LOGIN_ERROR_MESSAGE = (By.ID, "invalid-credentials")





