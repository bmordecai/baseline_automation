__author__ = 'Tige'
import main_page
from common.objects.appmanager.locators import *
from selenium.common.exceptions import NoSuchElementException
import time


# from common.objects.basemanager.main_page import BasePage
# import page_factory


class LoginPage(main_page.MainPage):
    """
    login Object \n
    """
    def __init__(self, web_driver):
        super(LoginPage, self).__init__(web_driver=web_driver)

    def enter_login_info(self, _user_name=None, _password=None):
        """
        Types in username and password and clicks the login button.
        :return:
        """
        # user = _user_name if _user_name is not None else self.driver.conf.user_name
        if _user_name is not None:
            user = _user_name
        else:
            user = self.driver.conf.user_name
        # Clear input and enter in username
        self.driver.send_text_to_element(AppManagerLoginLocators.USER_NAME_INPUT, text_to_send=user)

        # password = _password if _password is not None else self.driver.conf.user_password
        if _password is not None:
            password = _password
        else:
            password = self.driver.conf.user_password

        # clear input and enter in password
        self.driver.send_text_to_element(AppManagerLoginLocators.PASSWORD_INPUT, text_to_send=password)

    def verify_login_error(self):
        """
        this looks at the text displayed when an incorrect username or password is entered
        :return:
        :rtype:     str\n
        """
        login_error_text = self.driver.web_driver.find_element(*AppManagerLoginLocators.LOGIN_ERROR_MESSAGE).text
        expected = 'Your username or password is invalid.'
        if login_error_text != expected:
            e_msg = "login error '{0}' is not displayed on page. Instead found '{1}' " .format(
                str(expected),           # {0}
                str(login_error_text),   # {1}
            )
            raise ValueError(e_msg)

    def click_login_button(self):
        """
        Clicks the login button and wait's 5 seconds
        :return:
        :rtype: main_page.MainPage
        """
        # Finds and selects the login button in order to login
        self.driver.find_element_then_click(AppManagerLoginLocators.SIGN_IN_BUTTON)
        # Wait for page to fully load
        time.sleep(1)
        return main_page.MainPage(web_driver=self.driver)
