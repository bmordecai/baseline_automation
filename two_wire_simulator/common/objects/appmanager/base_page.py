__author__ = 'Tige'

import common.objects.base_classes.web_driver as driver


class AppManager(object):
    def __init__(self, web_driver):
        """
        Init base page instance
        :param web_driver:  WebDriver instance \n
        :type web_driver:   driver.WebDriver

        """
        self.driver = web_driver

        # main login page
        self.login_page = None
        """:type: common.objects.appmanager.login_page.LoginPage"""

        self.main_page = None
        """:type: common.objects.appmanager.main_page.MainPage"""

        self.main_page_main_menu = None
        """:type: common.objects.appmanager.main_page.MainPageMenu"""