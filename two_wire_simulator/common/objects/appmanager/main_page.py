__author__ = 'Tige'

from common.imports import *
from common.objects.appmanager.locators import *

import common.objects.base_classes.web_driver as driver
import time
from selenium.webdriver import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoSuchAttributeException

class MainPage(object):
    """
    Main Page Object \n
    """
    def __init__(self, web_driver):
        self.driver = web_driver



    def verify_open(self):
        pass
        # # TODO move this to the baseclass
        # # Wait for the page to successfully load displaying "Connected" status in the upper right.
        # self.driver.wait_for_element_visible(MainPageLocators.MAIN_MENU_BUTTON)
        # time.sleep(3)

    def verify_closed(self):
        pass
        # TODO move this to the baseclass
        # Wait for the page to successfully load displaying "Connected" status in the upper right.
        # self.driver.wait_for_element_visible(MainPageLocators.MAIN_MENU_BUTTON)
        # time.sleep(3)


    def select_a_menu_tab(self, _menu_name_locator, _sub_menu_locator=None):
        """
        pass in the value for the main tab menus by name if the tab has sub menus pass in the name of the sub menu
        when a sub menu is used the hover over command drops the menu down so that the sub menu can be selected
        the hover command must execute before the the sub menu can be selected
        if an incorrect  value is passed in a value error is raised and the program stops
        :param, menu_name:, quick view, Programs, liveview, devices, watersource
        :param, sub_menu:, zones, moisture, flow, master valve, temperature, events switches, point of connections,
        mainlines
        :rtype: str
        """
        pass
        # if _menu_name_locator not in [MainPageLocators.MAPS_TAB,
        #                               MainPageLocators.QUICK_VIEW_TAB,
        #                               MainPageLocators.PROGRAMS_TAB,
        #                               MainPageLocators.DEVICES_TAB,
        #                               MainPageLocators.WATER_SOURCES_TAB,
        #                               MainPageLocators.LIVEVIEW_TAB]:
        #     raise ValueError("Menu Name is incorrect:"+' ' + str(_menu_name_locator))
        #
        # # Try and locate the indicated menu item. If not found, handle exception raised by web driver
        # try:
        #     # If the menu name is within the set below, the menu has sub-menus that we need to hover to access
        #     if _menu_name_locator in (MainPageLocators.MAPS_TAB,
        #                               MainPageLocators.DEVICES_TAB,
        #                               MainPageLocators.WATER_SOURCES_TAB):
        #         self.driver.find_element_then_click(_menu_name_locator)
        #
        #         # Give animation time
        #         time.sleep(0.5)
        #
        #         # select a sub Menu
        #         if _sub_menu_locator is not None:
        #             self.select_a_sub_menu(sub_menu_locator=_sub_menu_locator)
        #     # A menu tab without a sub-list of menu items has been requested
        #     else:
        #         # Get the menu tab id from the dictionary
        #         self.driver.find_element_then_click(_menu_name_locator)
        #         # Give animation time
        #         time.sleep(0.5)
        # # Web driver unable to locate menu item indicated, handle exception raised.
        # except NoSuchElementException:
        #     raise ValueError("Unable to locate Main Menu: {0}, Sub Menu: {1}".format(
        #         _menu_name_locator,
        #         _sub_menu_locator
        #     ))
        # else:
        #     print "Successfully Selected Main Menu > {0} > {1}".format(
        #         _menu_name_locator,
        #         _sub_menu_locator
        #     )

    def select_a_sub_menu(self, sub_menu_locator):
        """
        Selects a sub menu from a known list of available sub menu's. If a sub menu name isn't found,
        a ValueError is raised, signifying a misspelled sub menu name or an invalid sub menu selection.
        When the user specifies the 'markers' sub menu option, they must also specify which 'marker'
        sub menu they would like to access, i.e., 'marker_menu_options' shown below.
        :param sub_menu_locator:            Sub menu options available.
        :return:
        """
        pass
        # # Iterate through supported sub menu options, if found a match, get the respective web_id associated
        # if sub_menu_locator not in [MainPageLocators.COMPANY,
        #                             MainPageLocators.CURRENT_SITE,
        #                             MainPageLocators.CURRENT_CONTROLLER,
        #                             MainPageLocators.ZONES,
        #                             MainPageLocators.MOISTURE_SENSORS,
        #                             MainPageLocators.FLOW,
        #                             MainPageLocators.MASTER_VALVES,
        #                             MainPageLocators.TEMPERATURE,
        #                             MainPageLocators.EVENT_SWITCHES,
        #                             MainPageLocators.POINT_OF_CONNECTION,
        #                             MainPageLocators.MAINLINES]:
        #     raise ValueError("Bad Sub Menu Name" + ' ' + sub_menu_locator)
        #
        # try:
        #     # Attempt to locate indicated sub menu, if found and 'click-able', click it.
        #     self.driver.find_element_then_click(sub_menu_locator)
        #
        #     # give animation some time to finish
        #     time.sleep(0.5)
        #
        # # Handle exception when we are unable to locate sub menu item before clicking
        # except StaleElementReferenceException:
        #     raise StaleElementReferenceException("Unable to locate sub menu item: " + str(sub_menu_locator))

    def select_main_menu(self):
        """
        Selects the main upper left menu.
        """
        pass
        # try:
        #     # Look for the upper left main menu and click it
        #     self.driver.wait_for_element_clickable(MainPageLocators.MAIN_MENU_BUTTON)
        #     self.driver.find_element_then_click(MainPageLocators.MAIN_MENU_BUTTON)
        #
        # except NoSuchElementException as e:
        #     e_msg = "Unable to locate main menu element to click using id: {0}. {1}".format(
        #         MainPageLocators.MAIN_MENU_BUTTON,     # {0}
        #         e.message                       # {1}
        #     )
        #     raise NoSuchAttributeException(e_msg)
        #
        # else:
        #     # for animation to finish
        #     time.sleep(2)

    def wait_for_selected_controller_to_reconnect(self):
        """
        Attempts to wait for the controller to go offline and to come back online.
        :return:
        :rtype:
        """
        pass
        # status = self.driver.get_status(MainPageLocators.FOOTER_CONTROLLER_STATUS)
        #
        # while status != "No Connection":
        #     time.sleep(2)
        #     status = self.driver.get_status(MainPageLocators.FOOTER_CONTROLLER_STATUS)
        #
        # while status != "Done":
        #     time.sleep(2)
        #     status = self.driver.get_status(MainPageLocators.FOOTER_CONTROLLER_STATUS)
        #
        # print "Selected controller has reconnected to BaseManager."

    def click_logout_button(self):
        """
        this is for the old version of the web site
        Clicks the logout button and wait's 5 seconds
        :return:
        :rtype:
        """
        pass
        # Finds and selects the login button in order to login
        # self.driver.find_element_then_click(MainPageLocators.LOGOUT)
        #
        # # # Wait for page to fully load
        # # time.sleep(5)
        #
        # # Wait for the login page to successfully load.
        # self.driver.wait_for_element_visible(LoginLocators.SIGN_IN_BUTTON)


class MainPageMenu(MainPage):

    def select_site(self, site_name=None):
        """
        Helper method for selecting a site from upper left menu. Navigation is as follows:
        menu -> sites and controllers -> (Next available options are selectable sites)
        :param site_name: Site to be selected.
        :return:
        """
        pass

    def select_a_controller(self, mac_address):
        """
        pass in the controller mac address as a string the method waits for the controller status to change colors
        before selecting the ID
        :param mac_address:     Mac Address of controller to select. \n
        :type mac_address:      str \n
        :rtype:                 str \n
        """
        pass

    def select_a_company(self, company=None):
        """
        Method assumes Super User is signing in to Base Manager. \n
        :param company:     A company to select. \n
        :type company:      str \n
        """
        pass

    def select_administration(self, _sub_menu=None):
        """
        Method assumes Super User is signing in to Base Manager. \n
        """
        pass



    def click_sign_out_button(self):
        """
        Clicks the Sign Out button and wait's 5 seconds
        :return:
        :rtype:
        """
        pass


    def click_close_button(self):
        pass
        # self.driver.find_element_then_click(ControllerSettingsLocators.CLOSE_BUTTON)