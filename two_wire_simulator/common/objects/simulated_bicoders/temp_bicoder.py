from common.objects.base_classes.simulated_biCoder import SimulatedbiCoderBaseClass

from common.imports import opcodes
from common.objects.statuses.bicoders.temp_statuses import TempStatuses
from common.imports.types import ActionCommands, TemperatureSensorCommands

__author__ = 'Tige'


class SimulatedTempBicoder(SimulatedbiCoderBaseClass):

    def __init__(self, _sn, _controller, _address=0, _two_wire_address=255):
        """
        :param _sn:         Serial that is set to the BiCoder. \n
        :type _sn:          str \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.tw_sim.TWSimulator

        :param _address:    The address assigned by the controller for the bicoder. \n
        :type _address:     int \n

        :param _two_wire_address:    The address assigned to the two-wire for the bicoder. \n
        :type _two_wire_address:     int \n
        """

        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(SimulatedTempBicoder, self).__init__(_serial_number=_sn,
                                                   _controller=_controller,
                                                   _address=_address,
                                                   _two_wire_address=_two_wire_address
                                                   )
        self.ty = self.bicoder_types.TEMPERATURE
        self.id = self.device_type.TEMPERATURE_SENSOR

        # Used when moving device from 3200 two-wire to substation two-wire
        self.decoder_type = self.id  # TS

        self.vd = 93.1      # Temperature

        self.default_attributes = [
            (opcodes.temperature_value, 'vd'),
            (opcodes.two_wire_drop, 'vt')
        ]

        # Temp biCoder status' module
        self.statuses = TempStatuses(_bicoder_object=self)
        """:type: common.objects.statuses.bicoders.temp_statuses.TempStatuses"""

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Methods that write or set the value of the simulated biCoders                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def write_temperature_reading(self, _degrees=None):
        """
        Set the temperature reading for the temperature BiCoder (Faux IO Only) \n

        :param _degrees: Value to set the temperature BiCoder temperature reading to \n
        :type _degrees: Integer, Float
        """
        # If a temperature value is passed in for overwrite
        if _degrees is not None:

            # Verifies the temperature reading passed in is an int or float value
            if not isinstance(_degrees, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s temperature reading. Invalid argument type, expected an " \
                        "int or float, received: {2}".format(
                            self.ty,            # {0}
                            str(self.sn),       # {1}
                            type(_degrees)      # {2}
                        )
                raise TypeError(e_msg)
            else:
                # Overwrite current value
                self.vd = _degrees

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,             # {0}
            self.id,                        # {1}
            str(self.sn),                   # {2}
            opcodes.temperature_value,      # {3}
            str(self.vd)                    # {4}
        )

        try:
            # Attempt to set temperature reading for temperature biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Temperature Reading' to: '{2}'". \
                format(
                    self.ty,        # {0}
                    str(self.sn),   # {1}
                    str(self.vd)    # {2}
                )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Temperature Reading' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vd))   # {2}
            )

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_temperature_reading(self, _data=None):
        """
        Verifies the temperature reading for this Temperature BiCoder. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.read_data()

        # Gets the temperature from the data object
        temperature = float(data.get_value_string_by_key(opcodes.temperature_value))
        rounded_vd = round(number=self.vd, ndigits=2)

        # Since simulated devices are treated as real, give them higher tolerance
        if abs(rounded_vd - temperature) > 1.0:
            # Compare status versus what is on the substation
            if self.vd != temperature:
                e_msg = "Unable to verify {0} ({1})'s temperature reading. Received: {2}, Expected: {3}". \
                    format(
                        self.ty,            # {0}
                        str(self.sn),       # {1}
                        str(temperature),   # {2}
                        str(self.vd)        # {3}
                    )
                raise ValueError(e_msg)
        # If no exception is thrown, print a success message
        print("Verified {0} ({1})'s temperature reading: '{2}' on substation".format(
            self.ty,          # {0}
            str(self.sn),     # {1}
            str(self.vd)      # {2}
        ))
        return True

    #################################
    def verify_who_i_am(self, _expected_status=None, _data=None):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.read_data()

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify temperature specific attributes
        self.verify_serial_number(_data=data)
        self.verify_temperature_reading(_data=data)

        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def read_and_update_object_attributes(self):
        """
        Does a get date a resets the attributes of the device to match the controller.
        """
        try:
            self.read_data()
            self.vd = float(self.data.get_value_string_by_key(opcodes.temperature_value))
        except Exception as e:
            e_msg = "Exception occurred trying to update attributes of the temperature sensor {0} object." \
                    "Temperature Value: '{1}'.\n\tException: {2}".format(
                        str(self.sn),   # {0}
                        str(self.vd),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the temperature sensor {0} object. "
                  "Temperature Value: '{1}'".format(
                        str(self.sn),  # {0}
                        str(self.vd),  # {1}
                    ))

