from common.objects.base_classes.simulated_biCoder import SimulatedbiCoderBaseClass

from common.imports import opcodes
from common.objects.statuses.bicoders.moisture_statuses import MoistureStatuses

from common.imports.types import ActionCommands, MoistureSensorCommands

__author__ = 'Tige'


class SimulatedMoistureBicoder(SimulatedbiCoderBaseClass):

    def __init__(self, _sn, _controller, _address=0, _two_wire_address=255):
        """
        :param _sn:         Serial that is set to the BiCoder. \n
        :type _sn:          str \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.tw_sim.TWSimulator

        :param _address:    The address assigned by the controller for the bicoder. \n
        :type _address:     int \n

        :param _two_wire_address:    The address assigned to the two-wire for the bicoder. \n
        :type _two_wire_address:     int \n
        """

        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(SimulatedMoistureBicoder, self).__init__(_serial_number=_sn,
                                                       _controller=_controller,
                                                       _address=_address,
                                                       _two_wire_address=_two_wire_address
                                                       )
        self.ty = self.bicoder_types.MOISTURE
        self.id = self.device_type.MOISTURE_SENSOR
        
        # Used when moving device from 3200 two-wire to substation two-wire
        self.decoder_type = self.id  # MS

        self.vp = 21.3  # Moisture Percent
        self.vd = 71.5  # Temperature

        # Attributes that we will send to the controller by default
        self.default_attributes = [
            (opcodes.moisture_percent, 'vp'),
            (opcodes.temperature_value, 'vd')
        ]

        # Moisture biCoder status' module
        self.statuses = MoistureStatuses(_bicoder_object=self)
        """:type: common.objects.statuses.bicoders.moisture_statuses.MoistureStatuses"""

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Methods that write or set the value of the simulated biCoders                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def write_moisture_percent(self, _percent=None):
        """
        Set the moisture percent for the moisture BiCoder (Faux IO Only) \n

        :param _percent: Value to set the moisture BiCoder moisture reading to (in percent) \n
        :type _percent: Integer, Float
        """
        # If a moisture percent is passed in for overwrite
        if _percent is not None:

            # Verifies the moisture biCoder reading passed in is an int or float value
            if not isinstance(_percent, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s moisture percent. Invalid argument type, expected an int " \
                        "or float, received: {2}".format(
                            self.ty,        # {0}
                            self.sn,        # {1}
                            type(_percent)  # {2}
                            )
                raise TypeError(e_msg)
            else:
                # Overwrite current value with new value
                self.vp = _percent

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            opcodes.moisture_percent,   # {3}
            str(self.vp)                # {4}
        )

        try:
            # Attempt to set moisture percent for moisture biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Moisture Percent' to: '{2}'".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vp)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Moisture Percent' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vp))   # {2}
            )

    #################################
    def write_temperature_reading(self, _temp=None):
        """
        Set the temperature reading for the moisture BiCoder (Faux IO Only) \n

        :param _temp: Value to set the moisture BiCoder temperature reading to \n
        :type _temp: Integer, Float
        """
        # If a temperature value is passed in for overwrite
        if _temp is not None:

            # Verifies the temperature reading passed in is an int or float value
            if not isinstance(_temp, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s temperature reading. Invalid argument type, expected an " \
                        "int or float, received: {2}".format(
                            self.ty,        # {0}
                            str(self.sn),   # {1}
                            type(_temp)     # {2}
                        )
                raise TypeError(e_msg)
            else:
                # Overwrite current value
                self.vd = _temp

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            opcodes.temperature_value,  # {3}
            str(self.vd)                # {4}
        )

        try:
            # Attempt to set temperature reading for moisture biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Temperature Reading' to: '{2}'". \
                format(
                    self.ty,        # {0}
                    str(self.sn),   # {1}
                    str(self.vd)    # {2}
                )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Temperature Reading' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vd))   # {2}
            )

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_moisture_percent(self, _data=None):
        """
        Verifies the moisture percent set for this Moisture BiCoder. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.read_data()

        # Gets the moisture percent from the data object
        moisture_percent = float(data.get_value_string_by_key(opcodes.moisture_percent))
        rounded_vp = round(number=self.vp, ndigits=2)
        # Simulated devices are treated as real devices so we need a tolerance between the reading and controller value
        if abs(rounded_vp - moisture_percent) > 3:
            # Compare status versus what is on the substation
            if self.vp != moisture_percent:
                e_msg = "Unable to verify {0} ({1})'s moisture percent. Received: {2}, Expected: {3}".format(
                            self.ty,                # {0}
                            str(self.sn),           # {1}
                            str(moisture_percent),  # {2}
                            str(self.vp)            # {3}
                        )
                raise ValueError(e_msg)
        else:
            # If no exception is thrown, print a success message
            print("Verified {0} ({1})'s moisture reading: '{2}' on substation".format(
                self.ty,          # {0}
                str(self.sn),     # {1}
                str(self.vp)      # {2}
            ))
            return True

    #################################
    def verify_temperature_reading(self, _data=None):
        """
        Verifies the temperature reading for this Moisture BiCoder. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.read_data()

        # Gets the temperature from the data object
        temperature = float(data.get_value_string_by_key(opcodes.temperature_value))
        rounded_vd = round(number=self.vd, ndigits=2)

        # Simulated devices are treated as real devices so we need a tolerance between the reading and controller value
        if abs(rounded_vd - temperature) > 1:
            # Compare status versus what is on the substation
            if self.vd != temperature:
                e_msg = "Unable to verify {0} ({1})'s temperature reading. Received: {2}, Expected: {3}". \
                    format(
                        self.ty,            # {0}
                        str(self.sn),       # {1}
                        str(temperature),   # {2}
                        str(self.vd)        # {3}
                    )
                raise ValueError(e_msg)
        # If no exception is thrown, print a success message
        print("Verified {0} ({1})'s temperature reading: '{2}' on substation".format(
            self.ty,          # {0}
            str(self.sn),     # {1}
            str(self.vd)      # {2}
        ))
        return True

    #################################
    def verify_who_i_am(self, _expected_status=None, _data=None):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        """
        data = self.read_data()

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify moisture specific attributes
        self.verify_serial_number(_data=data)
        self.verify_moisture_percent(_data=data)
        self.verify_temperature_reading(_data=data)

        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def read_and_update_object_attributes(self):
        """
        does a get date a resets the attributes of the device to match the controller
        """
        try:
            self.read_data()
            self.vp = float(self.data.get_value_string_by_key(opcodes.moisture_percent))
            self.vd = float(self.data.get_value_string_by_key(opcodes.temperature_value))
        except Exception as e:
            e_msg = "Exception occurred trying to update attributes of the Moisture Bicoder {0} object." \
                    " Moisture Percent: '{1}'," \
                    " Temperature: '{2}'.\n\tException: {3}".format(
                        str(self.sn),   # {0}
                        str(self.vp),   # {1}
                        str(self.vd),   # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the Moisture Bicoder {0} object. "
                  "Moisture Percent: '{1}', "
                  "Temperature: '{2}'".format(
                        str(self.sn),  # {0}
                        str(self.vp),  # {1}
                        str(self.vd),  # {2}
                    ))