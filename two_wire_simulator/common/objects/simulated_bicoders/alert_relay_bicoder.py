from common.objects.base_classes.simulated_biCoder import SimulatedbiCoderBaseClass

from common.imports import opcodes
from common.imports.types import ActionCommands

__author__ = 'Tige'


class SimulatedAlertRelayBicoder(SimulatedbiCoderBaseClass):

    def __init__(self, _sn, _controller, _address=0, _two_wire_address=255):
        """
        :param _sn:         Serial that is set to the BiCoder. \n
        :type _sn:          str \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.tw_sim.TWSimulator

        :param _address:    The address assigned by the controller for the bicoder. \n
        :type _address:     int \n

        :param _two_wire_address:    The address assigned to the two-wire for the bicoder. \n
        :type _two_wire_address:     int \n
        """

        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(SimulatedAlertRelayBicoder, self).__init__(_serial_number=_sn,
                                                         _controller=_controller,
                                                         _address=_address,
                                                         _two_wire_address=_two_wire_address
                                                         )
        self.ty = self.bicoder_types.ALERT
        self.id = self.device_type.ALERT_RELAY

        # Used when moving device from 3200 two-wire to substation two-wire
        self.decoder_type = self.id  # SW

        # Attributes that we will send to the controller by default
        self.default_attributes = [
            (opcodes.contacts_state, 'vc')
        ]

        self.vc = opcodes.contacts_closed       # Contact State

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Methods that write or set the value of the simulated biCoders                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def write_contact_state(self, _contact_state=None):
        """
        Set the contact for the alert relay BiCoder (Faux IO Only) \n

        :param _contact_state: Open ('OP') or Closed ('CL') \n
        :type _contact_state: str
        """
        # If a contact alert relay value is passed in for overwrite
        if _contact_state is not None:

            # Verifies the contact alert relay value passed in is an int or float value
            if not isinstance(_contact_state, str):
                e_msg = "Failed trying to set {0} ({1})'s contact state. Invalid argument type, expected a " \
                        "str, received: {2}".format(
                            self.ty,                # {0}
                            str(self.sn),           # {1}
                            type(_contact_state)    # {2}
                        )
                raise TypeError(e_msg)
            else:
                # Overwrite current value
                self.vc = _contact_state

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            opcodes.contacts_state,     # {3}
            str(self.vc)                # {4}
        )

        try:
            # Attempt to set contact state for alert relay biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Contact State' to: '{2}'". \
                format(
                    self.ty,        # {0}
                    str(self.sn),   # {1}
                    str(self.vc)    # {2}
                )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Contact State' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vc))   # {2}
            )

    #################################
    def write_contact_open(self):
        """
        Set the contact for the alert relay BiCoder (Faux IO Only) to 'Closed' \n

        """
        # Overwrite current value
        self.vc = opcodes.open

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            opcodes.contacts_state,     # {3}
            str(self.vc)                # {4}
        )

        try:
            # Attempt to set contact state for alert relay biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Contact State' to: '{2}'". \
                format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vc)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Contact State' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vc))   # {2}
            )

    #################################
    def write_contact_closed(self):
        """
        Set the contact for the alert relay BiCoder (Faux IO Only) to 'Closed' \n

        """
        # Overwrite current value
        self.vc = opcodes.closed

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            opcodes.contacts_state,     # {3}
            str(self.vc)                # {4}
        )

        try:
            # Attempt to set contact state for alert relay biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Contact State' to: '{2}'". \
                format(
                    self.ty,        # {0}
                    str(self.sn),   # {1}
                    str(self.vc)    # {2}
                )
            raise Exception(e_msg)
        print("Successfully set {0} ({1})'s 'Contact State' to: {2}".format(
            self.ty,        # {0}
            str(self.sn),   # {1}
            str(self.vc))   # {2}
        )
    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_contact_state(self, _data=None):
        """
        Verifies the contact state for this Alert relay BiCoder. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.read_data()

        # Gets the contact state from the data object
        contact_state = data.get_value_string_by_key(opcodes.contacts_state)

        # Compare status versus what is on the controller
        if self.vc != contact_state:
            e_msg = "Unable to verify {0} ({1})'s contact state reading. Received: {2}, Expected: {3}".format(
                self.ty,                # {0}
                str(self.sn),           # {1}
                str(contact_state),     # {2}
                str(self.vc)            # {3}
            )
            raise ValueError(e_msg)
        # Else if controller is using real devices, if we don't at least get a value, assume unable to communicate with
        # device, and bomb out
        elif self.vc != contact_state:
            e_msg = "Unable to verify {0} ({1})'s (real device) contact state reading. Received: {2}, " \
                    "Expected: {3}".format(
                        self.ty,                # {0}
                        str(self.sn),           # {1}
                        str(contact_state),     # {2}
                        self.vc                 # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified {0} ({1})'s contact state: '{2}' on substation".format(
                self.ty,          # {0}
                str(self.sn),     # {1}
                str(self.vc)      # {2}
            ))
            return True

    #################################
    def verify_who_i_am(self, _expected_status=None, _data=None):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.read_data()

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify alert relay specific attributes
        self.verify_serial_number(_data=data)
        self.verify_contact_state(_data=data)

        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def read_and_update_object_attributes(self):
        """
        This does a self test on the device.
        Than does a get date a resets the attributes of the device to match the controller.
        """
        try:
            self.read_data()
            self.vc = self.simulated_data.get_value_string_by_key(opcodes.contacts_state)
        except Exception as e:
            e_msg = "Exception occurred trying to update attributes of the Alert relay Bicoder {0} object." \
                    "Contact State: '{1}',.\n\tException: {2}".format(
                        str(self.sn),   # {0}
                        str(self.vc),   # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the Alert relay Bicoder {0} object. "
                  "Contact State: '{1}', ".format(
                        str(self.sn),  # {0}
                        str(self.vc),  # {1}
                    ))