from common.objects.base_classes.simulated_biCoder import SimulatedbiCoderBaseClass
from common.imports import opcodes
from common.objects.statuses.bicoders.pump_statuses import PumpStatuses

from common.imports.types import ActionCommands, PumpCommands

__author__ = 'Tige'


class SimulatedPumpBicoder(SimulatedbiCoderBaseClass):

    def __init__(self, _sn, _controller, _address=0, _two_wire_address=255):
        """
        :param _sn:         Serial that is set to the BiCoder. \n
        :type _sn:          str \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.tw_sim.TWSimulator

        :param _address:    The address assigned by the controller for the bicoder. \n
        :type _address:     int \n

        :param _two_wire_address:    The address assigned to the two-wire for the bicoder. \n
        :type _two_wire_address:     int \n
        """

        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(SimulatedbiCoderBaseClass, self).__init__(_serial_number=_sn,
                                                        _controller=_controller,
                                                        _address=_address,
                                                        _two_wire_address=_two_wire_address
                                                        )
        self.ty = self.bicoder_types.PUMP
        self.id = self.device_type.PUMP

        self.va = 0.23  # Solenoid Current
        self.vv = 28.7  # Solenoid Voltage

        # Pump biCoder status' module
        self.statuses = PumpStatuses(_bicoder_object=self)
        """:type: common.objects.statuses.bicoders.pump_statuses.PumpStatuses"""

    #################################
    def self_test_and_update_object_attributes(self):
        """
        this does a self test on the device
        than does a get date a resets the attributes of the device to match the controller
        self.va         # Solenoid Current
        self.vv         # Solenoid Voltage
        self.vt         # Two wire drop
        """
        try:
            self.read_data()
            self.va = float(self.data.get_value_string_by_key(opcodes.solenoid_current))
            self.vv = float(self.data.get_value_string_by_key(opcodes.solenoid_voltage))
        except Exception as e:
            e_msg = "Exception occurred trying to update attributes of the pump {0} object." \
                    " Solenoid Current Value: '{1}'," \
                    " Solenoid Voltage'Value: '{2}'".format(
                        str(self.sn),   # {0}
                        str(self.va),   # {1}
                        str(self.vv),   # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the pump {0} object. "
                  "Solenoid Current Value: '{1}', "
                  "Solenoid Voltage'Value: '{2}',".format(
                        str(self.sn),  # {0}
                        str(self.va),  # {1}
                        str(self.vv),  # {2}
                    ))

