from common.objects.base_classes.simulated_biCoder import SimulatedbiCoderBaseClass

from common.imports import opcodes
from common.imports.types import ActionCommands, FlowMeterCommands
from common import helper_equations
from common.objects.statuses.bicoders.flow_statuses import FlowStatuses

from common.imports.types import ActionCommands, FlowBiCoderCommands


__author__ = 'Tige'


class SimulatedFlowBicoder(SimulatedbiCoderBaseClass):
    """
    :type fm_on_cn: common.objects.controller.fm.FlowMeter
    """

    def __init__(self, _sn, _controller, _address=0, _two_wire_address=255):
        """
        :param _sn:         Serial that is set to the BiCoder. \n
        :type _sn:          str \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.tw_sim.TWSimulator

        :param _address:    The address assigned by the controller for the bicoder. \n
        :type _address:     int \n

        :param _two_wire_address:    The address assigned to the two-wire for the bicoder. \n
        :type _two_wire_address:     int \n
        """

        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(SimulatedFlowBicoder, self).__init__(_serial_number=_sn,
                                                   _controller=_controller,
                                                   _address=_address,
                                                   _two_wire_address=_two_wire_address
                                                   )
        self.ty = self.bicoder_types.FLOW
        self.id = self.device_type.FLOW_METER
        
        # Used when moving device from 3200 two-wire to substation two-wire
        self.decoder_type = self.id  # FM

        self.kv = 6.0  # K Value

        self.vr = 0.0   # Flow rate in Gallons Per Minute
        self.vg = 0.0   # Flow reported as usage in Total Gallons

        self.cr = 0     # Flow Rate Count
        self.cp = 0     # Flow Pulse Count
        self.cu = 0     # Total Usage Count

        # Attributes that we will send to the controller by default
        self.default_attributes = [
            (opcodes.flow_rate, 'vr'),
            (opcodes.total_usage, 'vg')
        ]

        # Flow biCoder status' module
        self.statuses = FlowStatuses(_bicoder_object=self)
        """:type: common.objects.statuses.bicoders.flow_statuses.FlowStatuses"""

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Methods that write or set the value of the simulated biCoders                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def write_k_value(self, _value=None):
        """
        Set the k_value for the Flow Meter (Faux IO Only) \n
        :param _value:        Value to set the Flow Meter k value as a float \n
        :return:
        """

        # if is None, use current value
        if _value is not None:

            # Verifies the k_value passed in is an float value
            if not isinstance(_value, float):
                e_msg = "Failed trying to set FM {0} ({1})'s k value. Invalid argument type, expected a float, " \
                        "received: {2}".format(
                            self.sn,        # {0}
                            str(self.ad),   # {1}
                            type(_value)    # {2}
                        )
                raise TypeError(e_msg)
            else:
                self.kv = _value

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            FlowMeterCommands.Flow_Meter,           # {1}
            self.sn,                                # {2}
            FlowMeterCommands.Attributes.K_VALUE,   # {3}
            str(self.kv)                            # {4}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'K Value' to: '{2}'".format(
                self.sn,        # {0}
                str(self.ad),   # {1}
                str(self.kv)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Flow Meter {0} ({1})'s 'K Value' to: {2}".format(
                self.sn,        # {0}
                str(self.ad),   # {1}
                str(self.kv))   # {2}
            )

    #################################
    def write_flow_rate(self, _gallons_per_minute=None):
        """
        Set the flow rate for the Flow Meter (Faux IO Only) \n

        :param _gallons_per_minute: Value to set the Flow Meter flow rate as a float in gallon per minute\n
        :type _gallons_per_minute: int, float
        """

        # If a flow rate is passed in for overwrite
        if _gallons_per_minute is not None:

            # Verifies the flow rate passed in is an int or float value
            if not isinstance(_gallons_per_minute, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s Flow Rate. Invalid argument type, expected an int " \
                        "or float. Received type: {2}".format(
                            self.ty,                    # {0}
                            self.sn,                    # {1}
                            type(_gallons_per_minute)   # {2}
                        )
                raise TypeError(e_msg)
            else:
                self.vr = _gallons_per_minute
        # if the flow bicoder is attached to a substation than you must send pulses not gpm
        if self.controller.is_substation():

            # Calculate flow rate count
            flow_rate_count = helper_equations.calculate_flow_rate_count_from_gpm(gpm=self.vr, kval=self.kv)
            new_vr = flow_rate_count
            flow_opcode = opcodes.flow_rate_count
            
        else:
            new_vr = self.vr
            flow_opcode = opcodes.flow_rate
            
        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,             # {0}
            self.id,                        # {1}
            self.sn,                        # {2}
            flow_opcode,                    # {3}
            new_vr)                         # {4}

        try:
            # Attempt to set flow rate for Flow Meter at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Flow Rate' to: '{2}'".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(self.vr)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Flow Rate' to: {2}".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(self.vr))   # {2}
            )

    #################################
    def write_water_usage(self, _water_usage=None):
        """
        Set the water usage for the Flow Meter (Faux IO Only) \n

        :param _water_usage: Value to set the Flow Meter total water used as a float in gallons\n
        :type _water_usage: int | float
        """
        # If a water usage is passed in for overwrite
        if _water_usage is not None:

            # Verifies the water usage passed in is an int or float value
            if not isinstance(_water_usage, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s Water Usage. Invalid argument type, expected an int or " \
                        "float. Received type: {2}".format(
                            self.ty,            # {0}
                            self.sn,            # {1}
                            type(_water_usage)  # {2}
                        )
                raise TypeError(e_msg)
            else:
                self.vg = _water_usage
        # if the flow bicoder is attached to a substation than you must send pulses not gpm
        if self.controller.is_substation():

            # Calculate flow usage count
            flow_usage_count = helper_equations.calculate_flow_usage_count_from_gallons(gal=self.vg, kval=self.kv)
            new_usage = flow_usage_count
            usage_opcode = opcodes.flow_usage_count
        else:
            new_usage = int(self.vg)
            usage_opcode = opcodes.total_usage

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            usage_opcode,               # {3}
            new_usage)                  # {4}

        try:
            # Attempt to set water usage for Flow Meter at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'water usage' to: '{2}'".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(self.vg)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'water usage' to: {2}".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(self.vg))   # {2}
            )

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_k_value(self, _data=None):
        """
        Verifies k value for this Flow Meter on the Controller. Expects the controller's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return:
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.read_data()

        k_value = float(data.get_value_string_by_key(opcodes.k_value))

        # Compare status versus what is on the controller
        if self.kv != k_value:
            e_msg = "Unable verify Flow Meter {0} ({1})'s k_value. Received: {2}, Expected: {3}".format(
                self.sn,        # {0}
                str(self.ad),   # {1}
                str(k_value),   # {2}
                str(self.kv)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Flow Meter {0} ({1})'s k_value: '{2}' on controller".format(
                self.sn,        # {0}
                str(self.ad),   # {1}
                str(self.kv)    # {2}
            ))
            return True

    #################################
    def verify_flow_rate(self, _data=None):
        """
        Verifies flow rate for this Flow Meter on the Substation. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.read_data()

        # if the flow bicoder is attached to a substation than you must send pulses not gpm
        flow_rate = float(data.get_value_string_by_key(opcodes.flow_rate))

        # If controller is in Faux IO, use 0.5 GPM allowance
        if abs(float(self.vr) - float(flow_rate)) > 1:
            e_msg = "Unable to verify {0} ({1})'s flow rate. Received: {2}, Expected: {3}".format(
                self.ty,                    # {0}
                self.sn,                    # {1}
                str(flow_rate),             # {2}
                str(self.vr)                # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} ({1})'s flow rate: '{2}' on substation".format(
                self.ty,          # {0}
                self.sn,          # {1}
                str(self.vr)      # {2}
            ))
            return True

    #################################
    def verify_water_usage(self, _data=None):
        """
        Verifies water usage for this Flow Meter on the Substation. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        :return:
        """

        # Calls get data if a data object was not passed in.
        data = _data if _data else self.read_data()

        # if the flow bicoder is attached to a substation than you must send pulses not gpm
        water_usage = float(data.get_value_string_by_key(opcodes.total_usage))

        # Compare status versus what is on the substation
        if abs(self.vg - water_usage) > 1:
            e_msg = "Unable to verify {0} ({1})'s water usage. Received: {2}, Expected: {3}".format(
                self.ty,                        # {0}
                self.sn,                        # {1}
                str(water_usage),               # {2}
                str(self.vg)                    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} ({1})'s water usage: '{2}'".format(
                self.ty,          # {0}
                self.sn,          # {1}
                str(self.vg)      # {2}
            ))
            return True

    #################################
    def verify_who_i_am(self, _expected_status=None,_data=None):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.read_data()

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify flow specific attributes
        self.verify_serial_number(_data=data)
        self.verify_flow_rate(_data=data)
        self.verify_water_usage(_data=data)

        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def read_and_update_object_attributes(self):
        """
        than does a get date a resets the attributes of the device to match the controller
        """
        try:
            self.read_data()

            # Get the flow rate from the data object (we cast to float first because a string with '0.00' cannot be
            # converted to an int
            self.vr = float(self.data.get_value_string_by_key(opcodes.flow_rate))
            self.vg = float(self.data.get_value_string_by_key(opcodes.sensor_usage))
        except Exception as e:
            e_msg = "Exception occurred trying to update attributes of the Flow Bicoder {0} object." \
                    " Flow Rate: '{1}'," \
                    " Total Usage: '{2}'.\n\tException: {3}".format(
                        str(self.sn),   # {0}
                        str(self.vr),   # {1}
                        str(self.vg),   # {2}
                        e.message       # {4}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the Flow Bicoder {0} object. "
                  "Flow Rate: '{1}', "
                  "Total Usage: '{2}' ".format(
                        str(self.sn),  # {0}
                        str(self.vr),  # {1}
                        str(self.vg),  # {2}
                    ))

