from datetime import datetime
import time
from common.imports import opcodes
from common.objects.base_classes.controller import BaseController
from common.objects.messages.controllers.bl_fs_messages import FlowStationMessages
from common.objects.statuses.controllers.bl_fs_statuses import FlowStationStatuses
from common.date_package.date_resource import date_mngr
from common.imports.types import ControllerCommands, ActionCommands, DeviceCommands, FlowStationCommands
from common.objects.base_classes.basemanager_connections import BaseManagerConnection
from common import object_factory
from common import helper_methods
import file_transfer_handler


__author__ = 'Eldin'


########################################################################################################################
# FlowStation CLASS
########################################################################################################################
class FlowStation(BaseController):
    """
    FlowStation Object \n
    """

    #############################
    def __init__(self, _mac, _serial_port, _serial_number, _port_address, _socket_port, _ip_address, _firmware_version=None,
                 _description=''):
        """
        Initialize a controller instance with the specified parameters \n
        :param _mac:                        Mac address for controller. \n
        :type _mac:                         str \n
        :param _serial_port:                Serial port of the controller to communicate with \n
        :type _serial_port:                 common.objects.base_classes.ser.Ser \n
        :param _serial_number:              this is the serial number of the controller \n
        :type _serial_number:               str | None \n
        :param _firmware_version:           this is the firmware version of the controller \n
        :type _firmware_version:            str | None \n
        :param _port_address:
        :type _port_address:
        :param _socket_port:
        :type _socket_port:
        :param _ip_address:
        :type _ip_address:
        :param _description:                Description to put on the controller. \n
        :type _description:                 str
        """

        # Initialize parent class for inheritance
        BaseController.__init__(self,
                                _type=ControllerCommands.Type.FLOWSTATION,
                                _mac=_mac,
                                _serial_port=_serial_port,
                                _serial_number=_serial_number,
                                _ad=_ip_address,
                                _description=_description,
                                _firmware_version=_firmware_version
                                )

        self.identifiers = [[opcodes.flow_station, _serial_number]]

        self.ss = None                  # Controller Status - GET ONLY

        self.mu = 0.0                       # Memory_Usage  - GET ONLY
        self.fp = opcodes.false             # Dynamic Flow Allocation (global setting for mainlines on the FlowStation)

        # FlowStation attributes
        self.max_controllers = 20           # int | Max controllers default to 20
        self.max_water_sources = 20         # int | Max water sources default to 20
        self.max_mainlines = 20             # int | Max mainlines default to 20
        self.max_points_of_control = 20     # int | Max points of control default to 20

        # create message reference to object
        self.messages = FlowStationMessages(_controller_object=self)  # Controller messages
        """:type: common.objects.messages.controllers.bl_fs_messages.FlowStationMessages"""
        #
        # #  Hydraulic Specific objects
        # self.water_sources = dict()    # WaterSource objects
        # """:type: dict[int, common.objects.programming.ws.WaterSource]"""
        # self.points_of_control = dict()   # Points of control objects
        # """:type: dict[int, common.objects.programming.point_of_control.PointOfControl]"""
        # self.mainlines = dict()    # Mainline objects
        # """:type: dict[int, common.objects.programming.ml.Mainline]"""

        # Basemanager connection object

        self.basemanager_connection = dict()
        """:type: dict[int, common.objects.base_classes.basemanager_connections.BaseManagerConnection]"""

        # Controllers assigned to FlowStation
        self.controllers = dict()
        """:type: dict[int, common.objects.controllers.bl_32.BaseStation3200]"""

        # Controllers assigned to FlowStation
        self.basestation_controllers = dict()
        """:type: dict[int, common.objects.controllers.bl_32.BaseStation3200]"""

        # Date and time on the controller
        self.flow_station_object_current_date_time = date_mngr.controller_datetime

        # set default values for the controller
        if self.ser is not None:
            self.set_default_values()

        # Date and time on the controller
        self.controller_object_current_date_time = date_mngr.controller_datetime

        # create status reference to objects
        self.statuses = FlowStationStatuses(_controller_object=self)  # Controller statuses
        """:type: common.objects.statuses.controllers.bl_fs_statuses.FlowStationStatuses"""

    #################################
    def __str__(self):
        """
        Returns string representation of this Flow Station Object. \n
        :return:    String representation of this Flow Station Object
        :rtype:     str
        """
        return_str = "\n-----------------------------------------\n" \
                     "FlowStation Object:\n" \
                     "Type:                     {0}\n" \
                     "Serial Number:            {1}\n" \
                     "Mac Address:              {2}\n" \
                     "Description:              {3}\n" \
                     "Latitude:                 {4}\n" \
                     "Longitude:                {5}\n" \
                     "Code Version:             {6}\n" \
                     "Status:                   {7}\n" \
                     "-----------------------------------------\n".format(
                        self.ty,       # {0}
                        self.sn,       # {1}
                        self.mac,      # {2}
                        self.ds,       # {3}
                        str(self.la),  # {4}
                        str(self.lg),  # {5}
                        str(self.vr),  # {6}
                        self.ss,       # {7}
                     )
        return return_str

    #################################
    def set_default_values(self):
        """
        Set the default values of the device on the flow station. Sends the built command through a serial port. \n
        :return:
        :rtype:
        """
        command = "{0},{1},{2}={3},{4}={5},{6}={7},{8}={9}"\
            .format(
                ActionCommands.SET,                                     # {0}
                opcodes.flow_station,                                   # {1}
                DeviceCommands.Attributes.SERIAL_NUMBER,                # {2}
                self.sn,                                                # {3}
                ControllerCommands.Attributes.DESCRIPTION,              # {4}
                str(self.ds),                                           # {5}
                ControllerCommands.Attributes.LATITUDE,                 # {6}
                str(self.la),                                           # {7}
                ControllerCommands.Attributes.LONGITUDE,                # {8}
                str(self.lg),                                           # {9}

                )
        try:
            # Attempt to set Controller default values
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Flow Station {0}'s 'Default values' to: '{1}'. " \
                    "Exception received: {2}".format(self.mac, command, e.message)
            raise Exception(e_msg)
        else:
            print("Successfully set Flow Stations {0}'s 'Default values' to: {1}".format(self.mac, command))

    #################################
    def set_max_controller_count(self, _max_controller_count):
        """
        Sets the max number of controller that can be add to the FlowStation. The current instance value can be
        overwritten by passing in a value for max controller count. \ns
        :param _max_controller_count:  Max number of controllers. \n
        :type _max_controller_count:   Int \n
        :return:
        """

        # Check if overwrite is intended
        # Verify integer is passed in for max number of concurrent zones to be set
        if not isinstance(_max_controller_count, int):
            e_msg = "Failed trying to set max controller count on the for controller. Invalid type passed in," \
                    " expected integer.  Type Received: {0}".format(
                        type(_max_controller_count)
                    )
            raise TypeError(e_msg)
        elif _max_controller_count not in range(1, 21):
            e_msg = "The max controller count must between between 1 and 20, you passed in {0}.".format(
                _max_controller_count
            )
            raise ValueError(e_msg)
        else:
            # Overwrite existing max controller count with new value
            self.max_controllers = _max_controller_count
        # SET,FS,MX=CN;CN=Max_Controllers
        # Command for sending
        command = "{0},{1},{2}={3};{4}={5}".format(
            ActionCommands.SET,                                 # {0}
            ControllerCommands.Type.FLOWSTATION,                # {1}
            FlowStationCommands.Attributes.MAX_COUNTS,          # {2}
            FlowStationCommands.Attributes.MAX_DEVICES,         # {3}
            FlowStationCommands.Attributes.MAX_COUNTS,          # {4}
            FlowStationCommands.Attributes.MAX_CONTROLLERS,     # {5}
            str(self.max_controllers))                          # {6}
        try:
            # Attempt to set max concurrent zones at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set FlowStations 'Max Controller Count' to: '{0}'".format(
                str(self.max_controllers))
            raise Exception(e_msg)
        else:
            print("Successfully set controller 'Max Controller Count on the Flow Station ' to: {0}".format(
                str(self.max_controllers))
            )

    #################################
    def set_max_water_source_count(self, _max_water_source_count):
        """
        Sets the max number of water source that can be add to the FlowStation. The current instance value can be
        overwritten by passing in a value for _max_water source count. \ns
        :param _max_water_source_count:  Max number of Water Sources. \n
        :type _max_water_source_count:   Int \n
        :return:
        """

        # Check if overwrite is intended
        # Verify integer is passed in for max number of concurrent zones to be set
        if not isinstance(_max_water_source_count, int):
            e_msg = "Failed trying to set max water source count on the for controller. Invalid type passed in, " \
                    "expected integer.  Type Received: {0}".format(
                        type(_max_water_source_count)
                    )
            raise TypeError(e_msg)
        elif _max_water_source_count not in range(1, 21):
            e_msg = "The max water source count must between between 1 and 20, you passed in {0}.".format(
                _max_water_source_count
            )
            raise ValueError(e_msg)
        else:
            # Overwrite existing max concurrent zones with new value
            self.max_water_sources = _max_water_source_count

        # SET,FS,MX=CN;WS=Max_WaterSources
        # Command for sending
        command = "{0},{1},{2}={3};{4}={5}".format(
            ActionCommands.SET,                                 # {0}
            ControllerCommands.Type.FLOWSTATION,                # {1}
            FlowStationCommands.Attributes.MAX_COUNTS,          # {2}
            FlowStationCommands.Attributes.MAX_DEVICES,         # {3}
            FlowStationCommands.Attributes.MAX_WATER_SOURCES,   # {4}
            str(self.max_water_sources))                        # {5}
        try:
            # Attempt to set max concurrent zones at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set FlowStations 'Max water source Count' to: '{0}'".format(
                str(self.max_water_sources))
            raise Exception(e_msg)
        else:
            print("Successfully set controller 'Max water source Count on the Flow Station ' to: {0}".format(
                str(self.max_water_sources))
            )

    #################################
    def set_max_point_of_control_count(self, _max_point_of_control_count):
        """
        Sets the max number of point_of_controls that can be add to the FlowStation. The current instance value can be
        overwritten by passing in a value for _max_point_of_control count. \ns
        :param _max_point_of_control_count:  Max number of point_of_controls. \n
        :type _max_point_of_control_count:   Int \n
        :return:
        """

        # Check if overwrite is intended
        # Verify integer is passed in for max number of concurrent zones to be set
        if not isinstance(_max_point_of_control_count, int):
            e_msg = "Failed trying to set max point_of_control count on the for controller. Invalid type passed in," \
                    " expected integer.  Type Received: {0}".format(
                        type(_max_point_of_control_count)
                    )
            raise TypeError(e_msg)
        elif _max_point_of_control_count not in range(1, 21):
            e_msg = "The max point_of_control count must between between 1 and 20, you passed in {0}.".format(
                _max_point_of_control_count
            )
            raise ValueError(e_msg)
        else:
            # Overwrite existing max concurrent zones with new value
            self.max_points_of_control = _max_point_of_control_count
        # SET,FS,MX=CN;PC=Max_PointsOfControl
        # Command for sending
        command = "{0},{1},{2}={3};{4}={5}".format(
            ActionCommands.SET,                                         # {0}
            ControllerCommands.Type.FLOWSTATION,                        # {1}
            FlowStationCommands.Attributes.MAX_COUNTS,                  # {2}
            FlowStationCommands.Attributes.MAX_DEVICES,                 # {3}
            FlowStationCommands.Attributes.MAX_POINTS_OF_CONTROL,       # {4}
            str(self.max_points_of_control))                            # {5}
        try:
            # Attempt to set max concurrent zones at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set FlowStations 'Max Device Count' to: '{0}'".format(
                str(self.max_points_of_control))
            raise Exception(e_msg)
        else:
            print("Successfully set controller 'Max Device Count on the Flow Station ' to: {0}".format(
                str(self.max_points_of_control))
            )

    #################################
    def set_max_mainline_count(self, _max_mainline_count):
        """
        Sets the max number of mainlines that can be add to the FlowStation. The current instance value can be
        overwritten by passing in a value for _max_mainline count. \ns
        :param _max_mainline_count:  Max number of mainlines. \n
        :type _max_mainline_count:   Int \n
        :return:
        """

        # Check if overwrite is intended
        # Verify integer is passed in for max number of concurrent zones to be set
        if not isinstance(_max_mainline_count, int):
            e_msg = "Failed trying to set max mainline count on the for controller. Invalid type passed in, expected " \
                    "integer.  Type Received: {0}".format(type(_max_mainline_count))
            raise TypeError(e_msg)
        elif _max_mainline_count not in range(1, 21):
            e_msg = "The max mainline count must between between 1 and 20, you passed in {0}.".format(
                _max_mainline_count
            )
            raise ValueError(e_msg)
        else:
            # Overwrite existing max concurrent zones with new value
            self.max_mainlines = _max_mainline_count
        # SET,FS,MX=CN;ML=Max_Mainlines
        # Command for sending
        command = "{0},{1},{2}={3};{4}={5}".format(
            ActionCommands.SET,                                 # {0}
            ControllerCommands.Type.FLOWSTATION,                # {1}
            FlowStationCommands.Attributes.MAX_COUNTS,          # {2}
            FlowStationCommands.Attributes.MAX_DEVICES,         # {3}
            FlowStationCommands.Attributes.MAX_MAINLINES,       # {4}
            str(self.max_mainlines))                            # {5}
        try:
            # Attempt to set max concurrent zones at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set FlowStations 'Max Device Count' to: '{0}'".format(
                str(self.max_mainlines))
            raise Exception(e_msg)
        else:
            print("Successfully set controller 'Max Device Count on the Flow Station ' to: {0}".format(
                str(self.max_mainlines))
                )

    #################################
    def set_dynamic_flow_allocation_to_true(self):
        """
        Sets the 'Dynamic Flow Allocation' for this FlowStation to 'TR' (True). \n
        """
        self.fp = opcodes.true

        command = "{0},{1},{2}={3}".format(
            ActionCommands.SET,                     # {0}
            opcodes.flow_station,                   # {1}
            opcodes.use_dynamic_flow_allocation,    # {2}
            str(self.fp)                            # {3}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set FlowStation {0}'s 'Dynamic Flow Allocation' to: '{1}'" \
                    " -> {2}".format(
                        str(self.mac),      # {0}
                        str(self.fp),       # {1}
                        e.message           # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set FlowStation {0}'s 'Dynamic Flow Allocation' to: {1}".format(
                str(self.mac),      # {0}
                str(self.fp),       # {1}
            ))

    #################################
    def set_dynamic_flow_allocation_to_false(self):
        """
        Sets the 'Dynamic Flow Allocation' for this FlowStation to 'FA' (False). \n
        """
        self.fp = opcodes.false

        command = "{0},{1},{2}={3}".format(
            ActionCommands.SET,                     # {0}
            opcodes.flow_station,                   # {1}
            opcodes.use_dynamic_flow_allocation,    # {2}
            str(self.fp)                            # {3}
        )

        try:
            # Attempt to send command to controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set FlowStation {0}'s 'Dynamic Flow Allocation' to: '{1}'" \
                    " -> {2}".format(
                        str(self.mac),      # {0}
                        str(self.fp),       # {1}
                        e.message           # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set FlowStation {0}'s 'Dynamic Flow Allocation' to: {1}".format(
                str(self.mac),      # {0}
                str(self.fp),       # {1}
            ))

    #############################
    def wait_for_reboot(self):
        self.set_serial_port_timeout(timeout=480)
        result = self.ser.wait(waitfors=['OK'], _maxchars=13800)

        self.set_serial_port_timeout(timeout=120)

        if result == 0:
            self.ser.working()
            print "Successfully waited for flow station after reboot."
        else:
            raise ValueError("Reconnection to flow station failed")

    #############################
    def do_reboot(self):
        """
        Reboot flow station then wait for 30 seconds for the flow station to wake back up \n
        Flush the input buffer \n
        Listen for the flow station to tell you its serial port is back up and running wait for the flow station to be
            full back up and running before  a new command is sent the flow station will send a "ok /n/r" when is ready
            to start taking command. \n
        :return: 0 if timeout, position of string in list (starting from 1) if the string is received,
                 -1 if we received more than the max # of characters (currently 500) without getting a
                 match.
        """
        try:
            # Get the version before the FlowStation reboots so we know how to communicate with it when it comes up
            self.get_data()
            self.vr = self.data.get_value_string_by_key(opcodes.firmware_version)
            # Reboot FS
            self.ser.send_and_wait_for_reply('DO,RB=TR')
            # Wait for the FlowStation to come back up, it will respond with an 'OK' in versions 2.1.2 and up
            if helper_methods.compare_versions(self.vr, opcodes.new_version_with_ok_after_reboot_fs) <= 0:
                self.ser.controller_reply()
            else:
                time.sleep(10)  # If the FlowStation isn't talking through the serial port, we have to just wait

        finally:
            # make sure FS returns in SIM mode with clock stopped
            self.set_sim_mode_to_on()
            self.stop_clock()
            # The FlowStation does not remember its date and time on a reboot
            self.set_date_and_time(_date=self.date_mngr.controller_datetime.date_string_for_controller(),
                                   _time=self.date_mngr.controller_datetime.time_string_for_controller())

    #################################
    def verify_dynamic_flow_allocation(self):
        """
        Verifies the 'Dynamic Flow Allocation' value on this FlowStation. \n
        """
        dynamic_flow_enabled = str(self.data.get_value_string_by_key(opcodes.use_dynamic_flow_allocation))

        # Compare status versus what is on the controller
        if self.fp != dynamic_flow_enabled:
            e_msg = "Unable to verify FlowStation {0}'s 'Dynamic Flow Allocation' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.mac),                  # {0}
                        str(dynamic_flow_enabled),      # {1}
                        str(self.fp)                    # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified FlowStation {0}'s 'Dynamic Flow Allocation' value: '{1}' on controller".format(
                str(self.mac),  # {0}
                str(self.fp)    # {1}
            ))

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Flow Station'. \n
        :param _expected_status:    Expected status to compare against. \n
        :type _expected_status:     str \n
        :return:
        """
        # Get all information about the device from the flow station.
        # self.get_data()
        # Get data for a Flow Station does not use identifiers.
        command = "{0},{1}".format(
            ActionCommands.GET,     # {0}
            opcodes.flow_station,   # {1}
        )
        self.data = self.ser.get_and_wait_for_reply(tosend=command)

        # Verify base attributes
        self.verify_description()

        # These verifiers are inherited from 'device.py'
        self.verify_serial_number()
        self.verify_latitude()
        self.verify_longitude()
        # TODO uncomment this line once 'FP' is added to the controller
        # self.verify_dynamic_flow_allocation()

        if _expected_status is not None:
            self.verify_status(_expected_status=_expected_status)

    #################################
    def add_controller_to_flow_station(self, _controller_address, _flow_station_slot_number):
        """
        Add (assign) a controller to the FlowStation \n

        :param _controller_address:  Controller object to put on the FlowStation. \n
        :type _controller_address:   int

        :param _flow_station_slot_number: Address (to set) of the controller in the flowstation. \n
        :type _flow_station_slot_number:  int \n
        """
        # look up the controller object using the address
        controller_object = self.basestation_controllers[_controller_address]

        if not isinstance(_flow_station_slot_number, int):
            e_msg = ("Exception occurred trying to add Controller {0} to FlowStation {1}: the value must "
                     "be an int".format(
                        str(controller_object.mac),     # {0}
                        str(self.mac)                   # {1}
                     ))
            raise ValueError(e_msg)
        elif _flow_station_slot_number not in range(1, 21):
            e_msg = "A controller's address on a FlowStation must be between 1 and 20, you passed in {0}.".format(
                _flow_station_slot_number
            )
            raise ValueError(e_msg)
        else:

            # Give each object a reference to the other
            self.controllers[_flow_station_slot_number] = controller_object
            controller_object.flow_station = self

            # The amount of times we want to attempt for our controller + FlowStation to connect
            retries_allowed = 2
            retry_amount = 0

            while retry_amount <= retries_allowed:
                try:
                    # this sets the address in the controller
                    self.assign_base_station_to_flow_station_slot_number(_controller_object=controller_object,
                                                                         _slot_number=_flow_station_slot_number)

                    # Send the command for the controller to connect to the flowstation
                    if controller_object.controller_type == '32':
                        # The controller can only have one FlowStation, so currently the address is only 1
                        controller_object.set_connection_to_flowstation(_address=1, _ip_address=self.ad)

                    # wait for a "CN" response
                    self.wait_for_flowstation_and_controllers_connection_status(
                        controller_address=_flow_station_slot_number,
                        max_wait=10)
                    break
                except Exception as e:
                    retry_amount += 1
                    # If we are over our allotted amount of retries to connect, throw the error
                    if retry_amount > retries_allowed:
                        raise

    #################################
    def remove_controller_from_flowstation(self, _flowstation_slot_number):
        """
                Add (assign) a controller to the FlowStation \n


                :param _flowstation_slot_number:  Controller object to put on the FlowStation. \n
                :type _flowstation_slot_number:    common.objects.controller.cn.Controller \n

                :return:
                :rtype:
                """
        _slot_number = 0

        _controller_object = self.controllers[_flowstation_slot_number]

        # this sets the address in the controller
        self.assign_base_station_to_flow_station_slot_number(_controller_object=_controller_object,
                                                             _slot_number=_slot_number)

        # Send the command for the controller to connect to the flowstation
        if _controller_object.controller_type == '32':
            _controller_object.set_connection_to_flowstation(_address=_controller_object.ad,
                                                             _enabled=opcodes.false)

        # Give each object a reference to the other
        del self.controllers[_flowstation_slot_number]
        _controller_object.flow_station = None

    #############################
    def assign_base_station_to_flow_station_slot_number(self, _controller_object, _slot_number):
        """
        This sends the flow station the Controller to an address in the slot number on the flow station. \n

        :param _controller_object: The controller object you want to set the address for on the flow station.
        :type _controller_object: common.objects.controller.cn.Controller

        :param _slot_number: The address of the controller on the flow station. \n
        :type _slot_number: int
        :return:
        """
        # Check if overwrite is intended
        # Verify integer is passed in
        if not isinstance(_slot_number, int):
            e_msg = "Failed trying to add controller to the flow station. Invalid type passed in," \
                    " expected integer.  Type Received: {0}".format(
                        type(_slot_number)
                    )
            raise TypeError(e_msg)
        if _slot_number not in range(0, 21):
            e_msg = "The address range for the controllers must be between 0- 20: Invalid address {0}.".format(
                _slot_number
            )
            raise ValueError(e_msg)

        # Example command: 'DO,AC=SK00001,NU=1,MC=112233445566'
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.DO,                                  # {0}
            ActionCommands.ASSIGN_CONTROLLER,                   # {1}
            str(_controller_object.sn),                         # {2} Controller Serial Number
            ControllerCommands.Attributes.MAC_ADDRESS,          # {3}
            str(_controller_object.mac),                        # {4} MAC Address
            ActionCommands.ASSIGN,                              # {5}
            str(_slot_number),                                  # {6} Address
        )
        try:
            print("Sending command: " + command)
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to assign controller with serial number {0} and mac {1} to flowstation" \
                    " address {2} -> {3}".format(
                        _controller_object.sn,
                        _controller_object.mac,
                        _slot_number,
                        e.message
                    )
            raise Exception(e_msg)
        else:
            print("Successfully addressed Controller, Serial {0}, Address {1} on Flow Station {2}".format(
                _controller_object.sn, _slot_number, self.sn))

    #################################
    def wait_for_flowstation_and_controllers_connection_status(self, controller_address, max_wait,
                                                               expected_status=opcodes.connected):
        """
        Waits for the FlowStation and controller to establish a connection. \n

        :param controller_address: Address of the controller. \n
        :type  controller_address: int \n

        :param max_wait: Number of minutes to wait before raising exception.
        :type max_wait: int \n

        :param expected_status:
        :param expected_status:

        :return:
        """
        max_wait_for_connection = max_wait * 60     # Maximum number of seconds to wait for reconnection to succeed
        current_wait_for_connection = 1         # Current number of seconds waited for reconnection
        self.save_programming_to_flow_station()
        # get current date and time on the FlowStation
        saved_date, saved_time = self.get_date_and_time().get_key_value_by_key("DT").get_value_string().split(" ")
        # date,time = date_time.get_value_string().split(" ")

        while current_wait_for_connection <= max_wait_for_connection:

            try:
                self.do_increment_clock(seconds=1)

                # Verify flowstation is connected to controller
                if expected_status == opcodes.connected:
                    self.controllers[controller_address].statuses.verify_connected_to_flowstation()
                    self.statuses.verify_connected_to_controller(_controller_number=controller_address)
                else:
                    self.controllers[controller_address].statuses.verify_not_connected_to_flowstation()
                    self.statuses.verify_not_connected_to_controller(_controller_number=controller_address)

                # If we hit this point we verified the connection successfully
                # Setting the time back to what it was before attempting to connect a controller to the FlowStation.
                self.set_date_and_time(_date=saved_date, _time=saved_time)
                break

            # Catch ValueError exception for when status is not connected
            except ValueError as e:

                # If we have waited for specified time and controllers haven't reconnected, raise exception.
                if current_wait_for_connection == max_wait_for_connection:
                    e_msg = "Connection timed out (waited up to {0} minutes) attempting to connect flowstation " \
                            "to controller.\n {1}".format(max_wait, e.message)
                    raise Exception(e_msg)
                else:
                    current_wait_for_connection += 1
                    time.sleep(1)  # wait for one second

    #################################
    def wait_for_flowstation_and_controllers_to_be_connected(self, max_wait=5):
        """
        Waits for the FlowStation and controller to establish a connection.

        :param max_wait: Number of minutes to wait before raising exception.
        :type max_wait: int
        """
        # Maximum number of seconds to wait for reconnection to succeed
        max_wait_for_connection = max_wait
        
        # Current number of seconds waited for reconnection
        current_wait_for_connection = 0
        
        # save the current date and time on the FlowStation to set after verifying
        # connection status
        saved_date, saved_time = self.get_date_and_time().get_key_value_by_key("DT").get_value_string().split(" ")
    
        # While we haven't waited for max wait time
        while current_wait_for_connection <= max_wait_for_connection:
        
            try:
                helper_methods.increment_controller_flowstation_clocks(flowstation=self,
                                                                       controllers=self.controllers,
                                                                       minutes=1)
                
                # For each controller try to verify they are connected.
                for controller_num in self.controllers.keys():
                    self.controllers[controller_num].statuses.verify_connected_to_flowstation()
                    self.statuses.verify_connected_to_controller(_controller_number=controller_num)
                    
                # If we hit this point we verified the connection successfully. Setting the time back to what it was
                # before attempting to connect a controller to the FlowStation.
                self.set_date_and_time(_date=saved_date, _time=saved_time)
                
                # Sync up the 3200 and FlowStation clocks
                helper_methods.sync_controller_clocks_with_flowstation(flowstation=self)
                break
        
            # Catch ValueError exception for when status is not connected
            except Exception as e:
            
                # If we have waited for specified time and controllers haven't reconnected, raise exception.
                if current_wait_for_connection == max_wait_for_connection:
                    e_msg = "Connection timed out (waited up to {0} minutes) attempting to connect flowstation " \
                            "to controller.\n {1}".format(max_wait, e.message)
                    raise Exception(e_msg)
                else:
                    current_wait_for_connection += 1
                    time.sleep(5)

    #################################
    def add_controller_mainline_to_flowstation(self,
                                                   _controller_address,
                                                   _controller_mainline_address,
                                                   _flow_station_mainline_slot_number):
        """
        This pass the mainline from the controller to the flow station by assigning it to an address in the flow
        station if the controller doesn't have this address than we create the mainline object\n

        :param _controller_address:  Shared Controller address on the FlowStation. \n
        :type _controller_address:    common.objects.controller.cn.Controller \n

        :param _controller_mainline_address: The address (slot) of the mainline in the controller
        :type _controller_mainline_address:  int

        :param _flow_station_mainline_slot_number:  The address (slot) to assign the mainline to in the FlowStation
        :type _flow_station_mainline_slot_number:   int
        """

        # Verify the ranges for addressing
        if _controller_address not in range(1, 21):
            e_msg = ("The controller address must be between 1 through 20. Value passed in: {0}.".format(
                _controller_address))
            raise ValueError(e_msg)

        # Verify the ranges for addressing
        if _controller_mainline_address not in range(1, 9):
            e_msg = ("Can not assign a water source to a controller with a controller address that is not "
                     "in the range 1 through 8. Value passed in: {0}.".format(_controller_mainline_address))
            raise ValueError(e_msg)

        # Verify the ranges for addressing
        if _flow_station_mainline_slot_number not in range(0, 21):
            e_msg = ("Can not assign a mainline to a FlowStation with a mainline slot address that is not "
                     "in the range 0 through 20. Value passed in: {0}.".format(_flow_station_mainline_slot_number))
            raise ValueError(e_msg)

        # Get the controller based on the controller address on the FlowStation
        controller = self.controllers[_controller_address]

        # Check to see if the water source exists. If it does not, create it.
        try:
            controller_mainline = controller.mainlines[_controller_mainline_address]
        except KeyError:
            controller.add_mainline_to_controller(_mainline_address=_controller_mainline_address)
            controller_mainline = controller.mainlines[_controller_mainline_address]
        # Set the Flow Station slot number on the water source
        controller_mainline.flow_station_slot_number = _flow_station_mainline_slot_number

        # Add the water source to the Flow Station shared mainlines dictionary.
        self.mainlines[_flow_station_mainline_slot_number] = controller_mainline
        controller_mainline.flowstation = self

        # Build the command string
        command = "{0},{1}={2},{3}={4},{5}={6},{7}={8}".format(
            ActionCommands.DO,                              # {0}
            opcodes.assign_mainline_to_flowstation,         # {1}
            controller.sn,                                  # {2} serial number of the controller
            opcodes.mac_address,                            # {3}
            controller.mac,                                 # {4}
            opcodes.mainline,                               # {5}
            _controller_mainline_address,                   # {6} address of the water source on the controller
            opcodes.address_number,                         # {7}
            _flow_station_mainline_slot_number              # {8} slot number to assign in the FlowStation
        )

        try:
            # Send the command through the serial port
            self.ser.send_and_wait_for_reply(tosend=command)

        except Exception as e:
            e_msg = ("Exception occurred trying to assign mainline {0} on controller {1} to FlowStation mainline "
                     "{2} -> {3}".format(_controller_mainline_address, controller.sn,
                                         _flow_station_mainline_slot_number, e.message))
            raise Exception(e_msg)
        else:
            print("Successfully assigned mainline {0} on controller {1} to FlowStation mainline {2}"
                    .format(_controller_mainline_address, controller.sn,
                    _flow_station_mainline_slot_number))

    #################################
    def add_controller_point_of_control_to_flowstation(self,
                                                       _controller_address,
                                                       _controller_point_of_control_address,
                                                       _flow_station_point_of_control_slot_number):
        """
        This assigns a point of control on the controller to an address in the FlowStation.
        If the controller doesnt have a Point of Control at the given address,
        then we create the point of control object and then assign it to the FlowStation.\n

        :param _controller_address:  Shared Controller address on the FlowStation. \n
        :type _controller_address:   common.objects.controller.cn.Controller \n

        :param _controller_point_of_control_address: The address (slot) of the point of control in the controller
        :type _controller_point_of_control_address:  int

        :param _flow_station_point_of_control_slot_number: The address (slot) to assign the water source to in the
                                                           FlowStation
        :type _flow_station_point_of_control_slot_number: int
        """
        # Verify the ranges for addressing
        if _controller_address not in range(1, 21):
            e_msg = ("The controller address must be between 1 through 20. Value passed in: {0}.".format(
                _controller_address))
            raise ValueError(e_msg)

        if _controller_point_of_control_address not in range(1, 9):
            e_msg = ("The point of control address must be between 1 through 8. Value passed in: {0}.".format(
                _controller_point_of_control_address))
            raise ValueError(e_msg)

        if _flow_station_point_of_control_slot_number not in range(0, 21):
            e_msg = ("The FlowStation Slot number must be between 0 through 20. Value passed in: {0}.".format(
                _flow_station_point_of_control_slot_number))

            raise ValueError(e_msg)

        # Get the controller based on the controller address on the FlowStation
        try:
            controller = self.controllers[_controller_address]
        except KeyError:
            e_msg = "Controller: " + _controller_address + " is not shared on FlowStation: " + self.mac
            raise KeyError(e_msg)

        # Check to see if the water source exists. If it does not, create it.
        try:
            controller_point_of_control = controller.points_of_control[_controller_point_of_control_address]
        except KeyError:
            controller.add_point_of_control_to_controller(_point_of_control_address=
                                                          _controller_point_of_control_address)

            controller_point_of_control = controller.points_of_control[_controller_point_of_control_address]

        # Set the Flow Station slot number on the water source
        controller_point_of_control.flow_station_slot_number = _flow_station_point_of_control_slot_number

        # Add the water source to the Flow Station shared water_sources dictionary.
        self.points_of_control[_flow_station_point_of_control_slot_number] = controller_point_of_control
        controller_point_of_control.flowstation = self

        # Build the command string
        command = "{0},{1}={2},{3}={4},{5}={6},{7}={8}".format(
            ActionCommands.DO,                                  # {0}
            opcodes.assign_point_of_control_to_flowstation,     # {1}
            controller.sn,                                      # {2} serial number of the controller
            opcodes.mac_address,                                # {3}
            controller.mac,                                     # {4}
            opcodes.point_of_control,                           # {5}
            _controller_point_of_control_address,               # {6} address of the Point of Control on the controller
            opcodes.address_number,                             # {7}
            _flow_station_point_of_control_slot_number          # {8} slot number to assign in the FlowStation
        )

        try:
            # Send the command through the serial port
            self.ser.send_and_wait_for_reply(tosend=command)

        except Exception as e:
            e_msg = ("Exception occurred trying to assign Point of Control {0} on controller {1} to FlowStation Point "
                     "of Control {2} -> {3}".format(_controller_point_of_control_address,
                                                    controller.sn,
                                                    _flow_station_point_of_control_slot_number , e.message))
            raise Exception(e_msg)
        else:
            print("Successfully assigned Point of Control {0} on controller {1} to FlowStation Point "
                  "of Control {2}".format(_controller_point_of_control_address,
                                          controller.sn,
                                          _flow_station_point_of_control_slot_number))

    #################################
    def add_controller_water_source_to_flowstation(self,
                                                   _controller_address,
                                                   _controller_water_source_address,
                                                   _flow_station_water_source_slot_number):
        """
        This pass the water source from the controller to the flow station by assigning it to an address in the flow
        station if the controller doesn't have this address than we create the water source object\n

        :param _controller_address:  Shared Controller address on the FlowStation. \n
        :type _controller_address:    common.objects.controller.cn.Controller \n

        :param _controller_water_source_address: The address (slot) of the water source in the controller
        :type _controller_water_source_address:  int

        :param _flow_station_water_source_slot_number:  The address (slot) to assign the water source to in the
                                                        FlowStation
        :type _flow_station_water_source_slot_number:   int
        """

        # Verify the ranges for addressing
        if _controller_address not in range(1, 21):
            e_msg = ("The controller address must be between 1 through 20. Value passed in: {0}.".format(
                _controller_address))
            raise ValueError(e_msg)

        # Verify the ranges for addressing
        if _controller_water_source_address not in range(1, 9):
            e_msg = ("Can not assign a water source to a controller with a controller address that is not "
                     "in the range 1 through 8. Value passed in: {0}.".format(_controller_water_source_address))
            raise ValueError(e_msg)

        # Verify the ranges for addressing
        if _flow_station_water_source_slot_number not in range(0, 21):
            e_msg = ("Can not assign a water source to a FlowStation with a water source slot address that is not "
                     "in the range 0 through 20. Value passed in: {0}.".format(_flow_station_water_source_slot_number))
            raise ValueError(e_msg)

        # Get the controller based on the controller address on the FlowStation
        controller = self.controllers[_controller_address]

        # Check to see if the water source exists. If it does not, create it.
        try:
            controller_water_source = controller.water_sources[_controller_water_source_address]
        except KeyError:
            controller.add_water_source_to_controller(_water_source_address=_controller_water_source_address)
            controller_water_source = controller.water_sources[_controller_water_source_address]

        # Set the Flow Station slot number on the water source
        controller_water_source.flow_station_slot_number = _flow_station_water_source_slot_number

        # Add the water source to the Flow Station shared water_sources dictionary.
        self.water_sources[_flow_station_water_source_slot_number] = controller_water_source
        controller_water_source.flowstation = self

        # Build the command string
        command = "{0},{1}={2},{3}={4},{5}={6},{7}={8}".format(
            ActionCommands.DO,                              # {0}
            opcodes.assign_water_source_to_flowstation,     # {1}
            controller.sn,                                  # {2} serial number of the controller
            opcodes.mac_address,                            # {3}
            controller.mac,                                 # {4}
            opcodes.water_source,                           # {5}
            _controller_water_source_address,               # {6} address of the water source on the controller
            opcodes.address_number,                         # {7}
            _flow_station_water_source_slot_number          # {8} slot number to assign in the FlowStation
        )

        try:
            # Send the command through the serial port
            self.ser.send_and_wait_for_reply(tosend=command)

        except Exception as e:
            e_msg = ("Exception occurred trying to assign water source {0} on controller {1} to FlowStation water "
                     "source {2} -> {3}".format(_controller_water_source_address, controller.sn,
                                                _flow_station_water_source_slot_number, e.message))
            raise Exception(e_msg)
        else:
            print("Successfully assigned water source {0} on controller {1} to FlowStation water "
                  "source {2}".format(_controller_water_source_address, controller.sn,
                                      _flow_station_water_source_slot_number))

    #################################
    def remove_controller_water_source_from_flowstation(self, _controller_address,
                                                        _controller_water_source_address,
                                                        _flow_station_water_source_slot_number):
        """
        This pass the water source from the controller to the flow station by assigning it to an address in the flow
        station if the controller doesn't have this address than we create the water source object\n

        :param _controller_address:     Controller object to put on the FlowStation. \n
        :type _controller_address:      common.objects.controller.cn.Controller \n

        :param _controller_water_source_address: The address (slot) of the water source in the controller
        :type _controller_water_source_address:  int

        :param _flow_station_water_source_slot_number: The address (slot) of the water source on the flowstation
        :type _flow_station_water_source_slot_number:  int

        """

        # Get the controller based on the controller address on the FlowStation
        try:
            controller = self.controllers[_controller_address]
        except KeyError:
            e_msg = "Controller '{0}' is not shared with FloStation '{1}'".format(_controller_address,      # {0}
                                                                                  self.mac)                 # {1}
            raise KeyError(e_msg)

        # Verify the ranges for addressing
        try:
            water_source = self.water_sources[_flow_station_water_source_slot_number]
        except KeyError:
            e_msg = ("Water Source '{0}' is not on FlowStation '{1}'"
                     .format(_flow_station_water_source_slot_number,
                             self.mac))
            raise KeyError(e_msg)
        # by setting the slot number to zero the water source is removed from the flow station

        # Delete the water source from the water_sources dictionary
        del self.water_sources[_flow_station_water_source_slot_number]
        # Set the water flow station water source slot number to zero.
        unassign_flow_station_water_source_slot_number = 0
        # Set the water source slot number to 0.
        water_source.flow_station_slot_number = unassign_flow_station_water_source_slot_number

        # Build the command string
        command = "{0},{1}={2},{3}={4},{5}={6},{7}={8}".format(
            ActionCommands.DO,                              # {0}
            opcodes.assign_water_source_to_flowstation,     # {1}
            controller.sn,                                  # {2} serial number of the controller
            opcodes.mac_address,                            # {3}
            controller.mac,                                 # {4}
            opcodes.water_source,                           # {5}
            _controller_water_source_address,               # {6} address of the water source on the controller
            opcodes.address_number,                         # {7}
            unassign_flow_station_water_source_slot_number  # {8} slot number to assign in the FlowStation
        )

        try:
            # Send the command through the serial port
            self.send_command_with_reply(tosend=command)

        except Exception as e:
            e_msg = ("Exception occurred trying to un-assign water source {0} on controller {1} to FlowStation water "
                     "source {2} -> {3}".format(_controller_water_source_address, controller.sn,
                                                _flow_station_water_source_slot_number, e.message))
            raise Exception(e_msg)
        else:
            print("Successfully un-assigned water source {0} on controller {1} to FlowStation water "
                  "source {2}".format(_controller_water_source_address, controller.sn,
                                      _flow_station_water_source_slot_number))

    #################################
    def remove_controller_point_of_control_from_flowstation(self, _controller_address,
                                                        _controller_point_of_control_address,
                                                        _flowstation_point_of_control_slot_number):
        """
        This pass the water source from the controller to the flow station by assigning it to an address in the flow
        station if the controller doesn't have this address than we create the water source object\n

        :param _controller_address:     Controller object to put on the FlowStation. \n
        :type _controller_address:      common.objects.controller.cn.Controller \n

        :param _controller_point_of_control_address: The address (slot) of the point of control in the controller
        :type _controller_point_of_control_address:  int

        :param _flowstation_point_of_control_slot_number: The address (slot) of the point of control on the flowstation
        :type _flowstation_point_of_control_slot_number:  int

        """

        # Get the controller based on the controller address on the FlowStation
        try:
            controller = self.controllers[_controller_address]
        except KeyError:
            e_msg = "Controller '{0}' is not shared with FloStation '{1}'".format(
                _controller_address,    # {0}
                self.mac                # {1}
            )
            raise KeyError(e_msg)

        # Verify that the given controller_point_of_control is in the points_of_control on the FlowStation.
        try:
            controller_point_of_control = controller.points_of_control[_controller_point_of_control_address]
        except KeyError:
            e_msg = ("Point of Control '{0}' on controller '{1}' does not exist. Can't Un-assign from FlowStation "
                     "'{2}'".format(
                          _controller_point_of_control_address,
                          controller.mac,
                          self.mac)
                     )
            raise KeyError(e_msg)

        # Verify the given flowstation_point_of_control_slot_number is shared with the FlowStation.
        try:
            point_of_control = self.points_of_control[_flowstation_point_of_control_slot_number]
        except KeyError:
            e_msg = ("Point of Control '{0}' is not on FlowStation '{1}'".format(
                _flowstation_point_of_control_slot_number,
                self.mac)
            )
            raise KeyError(e_msg)

        # Delete the point of control from the points_of_control dictionary
        del self.points_of_control[_flowstation_point_of_control_slot_number]

        # by setting the slot number to zero, the point of control will be removed from the flow station
        unassign_flow_station_point_of_control_slot_number = 0

        # Set the point of control slot number to 0.
        point_of_control.flow_station_slot_number = unassign_flow_station_point_of_control_slot_number

        # Build the command string
        command = "{0},{1}={2},{3}={4},{5}={6},{7}={8}".format(
            ActionCommands.DO,                                  # {0}
            opcodes.assign_point_of_control_to_flowstation,     # {1}
            controller.sn,                                      # {2} serial number of the controller
            opcodes.mac_address,                                # {3}
            controller.mac,                                     # {4}
            opcodes.point_of_control,                           # {5}
            _controller_point_of_control_address,               # {6} address of the point of control on the controller
            opcodes.address_number,                             # {7}
            unassign_flow_station_point_of_control_slot_number  # {8} slot number to assign in the FlowStation
        )

        try:
            # Send the command through the serial port
            self.send_command_with_reply(tosend=command)

        except Exception as e:
            e_msg = ("Exception occurred trying to un-assign point of control {0} on controller {1} to FlowStation point "
                     "of control {2} -> {3}".format(_controller_point_of_control_address, controller.sn,
                                                _flowstation_point_of_control_slot_number, e.message))
            raise Exception(e_msg)
        else:
            print("Successfully un-assigned point of control {0} on controller {1} to FlowStation point of control "
                  " {2}".format(_controller_point_of_control_address, controller.sn,
                                      _flowstation_point_of_control_slot_number))

    #################################
    def remove_controller_mainline_from_flowstation(self, _controller_address,
                                                          _controller_mainline_address,
                                                          _flowstation_mainline_slot_number):
        """
        This pass the water source from the controller to the flow station by assigning it to an address in the flow
        station if the controller doesn't have this address than we create the water source object\n

        :param _controller_address:     Controller object to put on the FlowStation. \n
        :type _controller_address:      common.objects.controller.cn.Controller \n

        :param _controller_mainline_address: The address (slot) of the point of control in the controller
        :type _controller_mainline_address:  int

        :param _flowstation_mainline_slot_number: The address (slot) of the point of control on the flowstation
        :type _flowstation_mainline_slot_number:  int

        """

        # Get the controller based on the controller address on the FlowStation
        try:
            controller = self.controllers[_controller_address]
        except KeyError:
            e_msg = "Controller '{0}' is not shared with FlowStation '{1}'".format(_controller_address,         # {0}
                                                                                  self.mac)                     # {1}
            raise KeyError(e_msg)

        # Verify that the given controller_mainline is in the points_of_control on the FlowStation.
        try:
            controller_mainline = controller.mainlines[_controller_mainline_address]
        except KeyError:
            e_msg = ("Mainline '{0}' on controller '{1}' does not exist. Can't Un-assign from FlowStation '{2}'"
                     .format(_controller_mainline_address,
                             controller.mac,
                             self.mac))
            raise KeyError(e_msg)

        # Verify the given flowstation_mainline_slot_number is shared with the FlowStation.
        try:
            mainline = self.mainlines[_flowstation_mainline_slot_number]
        except KeyError:
            e_msg = ("Mainline '{0}' is not on FlowStationc '{1}'"
                     .format(_flowstation_mainline_slot_number,
                             self.mac))
            raise KeyError(e_msg)

        # Delete the point of control from the points_of_control dictionary
        del self.mainlines[_flowstation_mainline_slot_number]

        # by setting the slot number to zero, the point of control will be removed from the flow station
        _unassign_flow_station_mainline_slot_number = 0

        # Set the point of control slot number to 0.
        mainline.flow_station_slot_number =_unassign_flow_station_mainline_slot_number

        # Build the command string
        command = "{0},{1}={2},{3}={4},{5}={6},{7}={8}".format(
            ActionCommands.DO,                                  # {0}
            opcodes.assign_mainline_to_flowstation,             # {1}
            controller.sn,                                      # {2} serial number of the controller
            opcodes.mac_address,                                # {3}
            controller.mac,                                     # {4}
            opcodes.mainline,                                   # {5}
            _controller_mainline_address,                       # {6} address of the mainline on the controller
            opcodes.address_number,                             # {7}
            _unassign_flow_station_mainline_slot_number         # {8} slot number to assign in the FlowStation
        )

        try:
            # Send the command through the serial port
            self.send_command_with_reply(tosend=command)

        except Exception as e:
            e_msg = ("Exception occurred trying to un-assign point of control {0} on controller {1} to FlowStation "
                     "mainline {2} -> {3}".format(_controller_mainline_address, controller.sn,
                                                    _flowstation_mainline_slot_number, e.message))
            raise Exception(e_msg)
        else:
            print("Successfully un-assigned mainline {0} on controller {1} to FlowStation mainline "
                  " {2}".format(_controller_mainline_address,
                                controller.sn,
                                _flowstation_mainline_slot_number))

    #################################
    def save_programming_to_flow_station(self):
        # This command forces the controller to save data and send out updated packets. This simulates pressing
        # the 'Run' button.
        command = "KEY,DL=5"
        try:
            # Send the command through the serial port
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred saving data to the FlowStation'."
            raise Exception(e_msg)

    #################################
    def verify_connected_to_controller(self):
        """
        Verifies the connection to a controller from a FlowStation.

        """
        command = "{0},{1}={2}".format(
            ActionCommands.GET,                     # {0}
            ControllerCommands.Type.FLOWSTATION,    # {1}
            str(self.ad)                            # {2}
        )
        _expected_status = opcodes.connected
        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to get FlowStation {0}'s 'Controller Connection " \
                    "Status'.".format(self.ad)
            raise Exception(e_msg)

        else:
            cn_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if _expected_status != cn_ss_on_cn:
                e_msg = "Exception occurred trying to verify FlowStation {0}'s 'Controller Connection Status'. " \
                        "Expected Status: '{1}', Received: '{2}'.".format(
                            self.ad,
                            _expected_status,
                            cn_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified FlowStation {0}'s 'Controller Connection Status'. Status Received: " \
                      "'{1}'.".format(self.ad, _expected_status)
                return True

    #################################
    def verify_not_connected_to_controller(self):
        """
        Verifies the connection to a controller from a FlowStation.

        """
        command = "{0},{1}={2}".format(
            ActionCommands.GET,                     # {0}
            ControllerCommands.Type.FLOWSTATION,    # {1}
            str(self.ad)                            # {2}
        )
        _expected_status = opcodes.disconnected
        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to get FlowStation {0}'s 'Controller Connection " \
                    "Status'.".format(self.ad)
            raise Exception(e_msg)

        else:
            cn_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if _expected_status != cn_ss_on_cn:
                e_msg = "Exception occurred trying to verify FlowStation {0}'s 'Controller Connection Status'. " \
                        "Expected Status: '{1}', Received: '{2}'.".format(
                            self.ad,
                            _expected_status,
                            cn_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified FlowStation {0}'s 'Controller Connection Status'. Status Received: " \
                      "'{1}'.".format(self.ad, _expected_status)
                return True



    #################################
    def verify_full_configuration(self):
        """
        This goes through each object in the list and sorts them by address
        then it verifies all attributes of the object against what the controller thinks
        :return:
        :rtype:
        """

        # first save data to the Flow Station
        self.save_programming_to_flow_station()
        
        # build the list of object pointers
        list_of_object_pointers = [
            self.mainlines,
            self.points_of_control,
            self.water_sources
        ]

        for each_object in list_of_object_pointers:
            for _ad in sorted(each_object.keys()):
                # we only want to verify objects that are sh between the 3200 and the Flow Station.
                if each_object[_ad].sh:
                    # get the old controller type
                    old_controller_type = each_object[_ad].controller.controller_type
                    # get old ser to the base controller
                    old_ser_object = each_object[_ad].ser
                    # get old identifiers which is the address of this object on the base controller
                    old_identifiers = each_object[_ad].identifiers
                    # get a new identifier based on the address of the object on the Flow Station
                    new_identifier = [[old_identifiers[0][0], _ad]]
                    # set the identifiers of this item to the new identifier so that get data will retrieve the correct
                    # information
                    each_object[_ad].identifiers = new_identifier
                    # set the serial connection of this object to the serial connection of the Flow Station
                    each_object[_ad].ser = self.ser
                    # set the controller type on of the object to the FlowStation's Controller Type
                    each_object[_ad].controller.controller_type = self.controller_type
                    # verify who I am
                    each_object[_ad].verify_who_i_am()
                    # reset the serial connection to the old serial connection
                    each_object[_ad].ser = old_ser_object
                    # reset the identifiers to the old identifiers
                    each_object[_ad].identifiers = old_identifiers
                    # reset the controller type to what it was before
                    each_object[_ad].controller.controller_type = old_controller_type

        # TODO Description verification fails for a FlowStation
        self.verify_who_i_am()
    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Super classes are below                                                                                          #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################
    ################################
    def do_firmware_update(self,
                           were_from,
                           bm_id_number=None,
                           directory=None,
                           file_name=None,
                           maxlinelen=1000,
                           file_type=opcodes.zip_file,
                           unit_testing=False):
        """
        :param were_from:       This is the location of the firmware to update either from BaseManager or USB Storage
                                device (BaseManager, USB, local directory)
        :type were_from:        str
        :param bm_id_number:    This is the database id from the server where the firmware is located
        :type bm_id_number:     int
        :param directory:       Directory to grab firmware from "common/firmware_update_files/test.zip"
        :type directory:        str
        :type bm_id_number:     BaseManager database ID number of the firmware update
        :type bm_id_number:     int
        :param file_name:       This the the file name located on the USB
        :type file_name:        str
        :param maxlinelen:      The length of each packet (besides last one) from splitting up the firmware file we send
                                to the 3200.
        :type maxlinelen:       int
        :param file_type:       The file type that your firmware file is in. Usually '.zip'.
        :type file_type:        str
        :param unit_testing:    Parameter to make unit testing faster by ignoring sleeps
        :type unit_testing:     bool
        """
        super(FlowStation, self).do_firmware_update(
                           were_from=were_from,
                           bm_id_number=bm_id_number,
                           directory=directory,
                           file_name=file_name,
                           maxlinelen=maxlinelen,
                           file_type=file_type)

    #############################
    def do_reboot_controller(self, _version_with_ok_response=opcodes.new_reboot_version_fs,
                             _version_with_response_after_reboot=opcodes.new_version_with_ok_after_reboot_fs):
        """
        Reboot a FlowStation controller.

        :param _version_with_ok_response:   This is the FlowStation version where it responds to a reboot command with
                                            and 'OK' instead of rebooting immediately with no response.
        :param _version_with_response_after_reboot: This is the FlowStation version where it responds with an 'OK'
                                                    after it comes alive after a reboot.
        :return:
        """
        super(FlowStation, self).do_reboot_controller(
            _version_with_ok_response=_version_with_ok_response,
            _version_with_response_after_reboot=_version_with_response_after_reboot
        )

    #############################
    def wait_for_controller_after_reboot(self,  _version_with_response_after_reboot=None, unit_testing=False):
        """
        Waiting for a controller to wake up after a reboot.

        :param _version_with_response_after_reboot: This is the FlowStation version where it responds with an 'OK'
                                                    after it comes alive after a reboot.
        """
        super(FlowStation, self).wait_for_controller_after_reboot(
            _version_with_response_after_reboot=opcodes.new_version_with_ok_after_reboot_fs,
            unit_testing=False
        )

    #################################
    def do_increment_clock(self, hours=0, minutes=0, seconds=0, update_date_mngr=True, clock_format='%H:%M:%S'):
        """
        compare each value of hours, minutes, seconds \n
        this allows to send in one of the parameter to increment the clock in the controller \n
        the clock must be stopped in order it increment it

        :param hours:               Amount of hours to increment. \n
        :type hours:                int
        :param minutes:             Amount of minutes to increment. \n
        :type minutes:              int
        :param seconds:             Amount of seconds to increment. \n
        :type seconds:              int
        :param update_date_mngr:    If we also want to update the date manager value (VERY rare case where we don't
                                    want to). \n
        :type update_date_mngr:     bool
        :return:            ok from controller
        :rtype:             ok is a str

        """

        super(FlowStation, self).do_increment_clock(hours=hours,
                                                    minutes=minutes,
                                                    seconds=seconds,
                                                    update_date_mngr=update_date_mngr,
                                                    clock_format=clock_format)

    #############################
    def set_controller_to_off(self, command=''):
        command = 'KEY,DL=0'
        super(FlowStation, self).set_controller_to_off(command=command)

    #############################
    def set_controller_to_run(self, command=''):
        command = 'KEY,DL=1'
        super(FlowStation, self).set_controller_to_run(command=command)