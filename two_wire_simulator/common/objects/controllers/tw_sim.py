import warnings
from common.objects.base_classes.devices import BaseDevices
import time
from datetime import datetime, timedelta

from common.imports import opcodes
from common.variables.common import dictionary_for_status_codes
from common.objects.base_classes import messages
from common.objects.base_classes.devices import BaseDevices
from common.objects.base_classes.controller import BaseController
from common.imports.types import ControllerCommands, ActionCommands, BasemanagerCommands
from common.objects.statuses.controllers.tw_sim_statuses import TWSimulatorStatuses
from common import helper_methods
from common.date_package.date_resource import date_mngr

from common.object_factory import create_simulated_bicoder_object
__author__ = 'Tige'


class TWSimulator(BaseController):
    """
    Specify the types of bicoder dictionaries. You can really specify the type for each attribute for a TWSimulator here
    if needed. But I figured these were the only attributes for now that type specification was necessary. That way 
    we can have access to bicoder methods in the use_case as so:
    
        - self.config.TWSimulators[1].simulated_valve_bicoders['TSD0001'].foo()
        
    :type simulated_valve_bicoders: dict[str, common.objects.TWSimulator.valve_bicoder.ValveBicoder]
    :type simulated_flow_bicoders: dict[str, common.objects.TWSimulator.flow_bicoder.FlowBicoder]
    :type simulated_pump_bicoders: dict[str, common.objects.TWSimulator.pump_bicoder.PumpBicoder]
    :type simulated_switch_bicoders: dict[str, common.objects.TWSimulator.switch_bicoder.SwitchBicoder]
    :type simulated_moisture_bicoders: dict[str, common.objects.TWSimulator.moisture_bicoder.MoistureBicoder]
    :type simulated_temperature_bicoders: dict[str, common.objects.TWSimulator.temp_bicoder.TempBicoder]

    :type ser: common.objects.base_classes.ser.Ser
    """

    #############################
    def __init__(self, _mac, _serial_port, _serial_number, _port_address, _socket_port, _ip_address,
                 _firmware_version=None, _description=''):
        """
        Initialize a controller instance with the specified parameters \n
        :param _mac:                        Mac address for controller. \n
        :type _mac:                         str \n
        :param _serial_port:                Serial port of the controller to communicate with \n
        :type _serial_port:                 common.objects.base_classes.ser.Ser \n
        :param _serial_number:              this is the serial number of the controller \n
        :type _serial_number:               str | None \n
        :param_cn_firmware_version:         this is the firmware version of the controller \n
        :type _firmware_version:            str | None \n
        :param _port_address:
        :type _port_address:
        :param _socket_port:
        :type _socket_port:
        :param _ip_address:
        :type _ip_address:
        :param _description:                Description to put on the controller. \n
        :type _description:                 str
        """

        self.websocket = '{0},{1}' .format(_ip_address, _port_address)

        BaseController.__init__(self,
                                _type=ControllerCommands.Type.TWSIMULATOR,
                                _mac=_mac,
                                _serial_port=_serial_port,
                                _serial_number=_serial_number,
                                _ad=_ip_address,
                                _description=_description,
                                _firmware_version=_firmware_version
                                )

        self.ip_address = _ip_address
        self.socket_port_address = _socket_port

        # Read Only Attributes
        self.ty = ControllerCommands.Type.TWSIMULATOR               # TWSimulator Type
        self.controller_type = ControllerCommands.Type.TWSIMULATOR  # TWSimulator Type (again for the sake of
        self.ss = ''                                                # Status
        self.connected_controller = None                            # Controller who's two wire we are connected to

        # TWSimulator BiCoder serial numbers assigned.
        self.d1_bicoders = list()        # Single valve
        self.d2_bicoders = list()        # Dual valve
        self.d4_bicoders = list()        # Quad valve
        self.dd_bicoders = list()        # Twelve valve
        self.ms_bicoders = list()        # Moisture bicoder serial numbers
        self.fm_bicoders = list()        # Flow bicoder serial numbers
        self.ts_bicoders = list()        # Temperature bicoder serial numbers
        self.sw_bicoders = list()        # Switch bicoder serial numbers

        # TWSimulator BiCoder object dictionaries
        self.simulated_valve_bicoders = dict()        # Valve valve
        """:type: dict[str, common.objects.simulated_bicoders.valve_bicoder.SimulatedValveBicoder]"""
        self.simulated_analog_bicoders = dict()       # Analog bicoder serial numbers
        """:type: dict[str, common.objects.simulated_bicoders.analog_bicoder.SimulatedAnalogBicoder]"""
        self.simulated_flow_bicoders = dict()         # Flow bicoder serial numbers
        """:type: dict[str, common.objects.simulated_bicoders.flow_bicoder.SimulatedFlowBicoder]"""
        self.simulated_moisture_bicoders = dict()     # Moisture bicoder serial numbers
        """:type: dict[str, common.objects.simulated_bicoders.moisture_bicoder.SimulatedMoistureBicoder]"""
        self.simulated_temperature_bicoders = dict()  # Temperature bicoder serial numbers
        """:type: dict[str, common.objects.simulated_bicoders.temp_bicoder.SimulatedTempBicoder]"""
        self.simulated_switch_bicoders = dict()       # Switch bicoder serial numbers
        """:type: dict[str, common.objects.simulated_bicoders.switch_bicoder.SimulatedSwitchBicoder]"""
        self.simulated_pump_bicoders = dict()         # Pump bicoder serial numbers
        """:type: dict[str, common.objects.simulated_bicoders.pump_bicoder.SimulatedPumpBicoder]"""

        # create status reference to objects
        self.statuses = TWSimulatorStatuses(_controller_object=self)  # Controller statuses
        """:type: common.objects.statuses.controllers.tw_sim_statuses.TWSimulatorStatuses"""

        # Attribute so we can store the messages
        self.build_message_string = ''  # place to store the message that is compared to the TWSimulator message

        self.set_default_values()

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        command = "{0},{1},{2}={3},{4}={5}".format(
            ActionCommands.SET,                             # {0}
            ControllerCommands.Controller,                  # {1}
            ControllerCommands.Attributes.SERIAL_NUMBER,    # {2}
            self.sn,                                        # {3}
            ControllerCommands.Attributes.DESCRIPTION,      # {4}
            self.ds,                                        # {5}
        )

        try:
            # Attempt to set Controller default values
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set TWSimulator {0}'s 'Default values' to: '{1}'. " \
                    "Exception received: {2}".format(
                        self.mac,   # {0}
                        command,    # {1}
                        e.message   # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set TWSimulator {0}'s 'Default values' to: {1}".format(
                self.mac,   # {0}
                command)    # {1}
            )

    #################################
    def set_message(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        :param _helper_object
        :type _helper_object : BaseStartStopPause
        """
        try:
            self.build_message_string = messages.message_to_set(self,
                                                                _status_code=_status_code,
                                                                _ct_type=opcodes.controller,
                                                                _helper_object=_helper_object)
            self.ser.send_and_wait_for_reply(tosend=self.build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.set_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=self.build_message_string)

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this TWSimulator. \n

        :param _expected_status:    Expected status to compare against. \n
        :type _expected_status:     str \n
        :return:
        """
        # Get all information about the device from the TWSimulator.
        self.get_data()

        # Verify base attributes
        self.verify_description()
        self.verify_serial_number()

        if _expected_status is not None:
            self.verify_status(_expected_status=_expected_status)

    #################################
    def verify_full_configuration(self):
        """
        Verifies itself and all of the assigned bicoders.
        """
        self.verify_who_i_am()

        # Set default values for valve bicoders
        for valve_sn in self.simulated_valve_bicoders.keys():
            self.simulated_valve_bicoders[valve_sn].verify_who_i_am()

        # Set default values for flow bicoders
        for flow_sn in self.simulated_flow_bicoders.keys():
            self.simulated_flow_bicoders[flow_sn].verify_who_i_am()

        # Set default values for switch bicoders
        for switch_sn in self.simulated_switch_bicoders.keys():
            self.simulated_switch_bicoders[switch_sn].verify_who_i_am()

        # Set default values for moisture bicoders
        for moisture_sn in self.simulated_moisture_bicoders.keys():
            self.simulated_moisture_bicoders[moisture_sn].verify_who_i_am()

        # Set default values for temperature bicoders
        for temperature_sn in self.simulated_temperature_bicoders.keys():
            self.simulated_temperature_bicoders[temperature_sn].verify_who_i_am()

        # Set default values for analog bicoders
        for analog_sn in self.simulated_analog_bicoders.keys():
            self.simulated_analog_bicoders[analog_sn].verify_who_i_am()

        self.verify_who_i_am()

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Misc. Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def get_simulated_valve_bicoders(self, _serial_number):
        """
        Gets a specific valve biCoder (base on serial number) and flips its serial port to communicate with the
        biCoder. \n

        :param _serial_number: Serial number of the biCoder. \n
        :type _serial_number:  str \n
        """
        # Change the serial port, catch an exception if serial number does not exist
        try:
            self.simulated_valve_bicoders[_serial_number].ser = self.ser
        except KeyError:
            e_msg = "Could not find Valve Decoder {0} in the dictionary for the TWSimulator".format(
                _serial_number         # {0}
            )
            raise KeyError(e_msg)

        return self.simulated_valve_bicoders[_serial_number]

    #################################
    def get_simulated_analog_bicoders(self, _serial_number):
        """
        Gets a specific analog biCoder (base on serial number) and flips its serial port to communicate with the
        biCoder. \n

        :param _serial_number: Serial number of the biCoder. \n
        :type _serial_number:  str \n
        """
        # Change the serial port, catch an exception if serial number does not exist
        try:
            self.simulated_analog_bicoders[_serial_number].ser = self.ser
        except KeyError:
            e_msg = "Could not find Analog Decoder {0} in the dictionary for the TWSimulator".format(
                _serial_number         # {0}
            )
            raise KeyError(e_msg)

        return self.simulated_analog_bicoders[_serial_number]

    #################################
    def get_simulated_flow_bicoders(self, _serial_number):
        """
        Gets a specific flow biCoder (base on serial number) and flips its serial port to communicate with the
        biCoder. \n

        :param _serial_number: Serial number of the biCoder. \n
        :type _serial_number:  str \n
        """
        # Change the serial port, catch an exception if serial number does not exist
        try:
            self.simulated_flow_bicoders[_serial_number].ser = self.ser
        except KeyError:
            e_msg = "Could not find Flow Decoder {0} in the dictionary for the TWSimulator".format(
                _serial_number         # {0}
            )
            raise KeyError(e_msg)

        return self.simulated_flow_bicoders[_serial_number]

    #################################
    def get_simulated_moisture_bicoders(self, _serial_number):
        """
        Gets a specific moisture biCoder (base on serial number) and flips its serial port to communicate with the
        biCoder. \n

        :param _serial_number: Serial number of the biCoder. \n
        :type _serial_number:  str \n
        """
        # Change the serial port, catch an exception if serial number does not exist
        try:
            self.simulated_moisture_bicoders[_serial_number].ser = self.ser
        except KeyError:
            e_msg = "Could not find Moisture Decoder {0} in the dictionary for the TWSimulator".format(
                _serial_number         # {0}
            )
            raise KeyError(e_msg)

        return self.simulated_moisture_bicoders[_serial_number]

    #################################
    def get_simulated_temperature_bicoders(self, _serial_number):
        """
        Gets a specific temperature biCoder (base on serial number) and flips its serial port to communicate with the
        biCoder. \n

        :param _serial_number: Serial number of the biCoder. \n
        :type _serial_number:  str \n
        """
        # Change the serial port, catch an exception if serial number does not exist
        try:
            self.simulated_temperature_bicoders[_serial_number].ser = self.ser
        except KeyError:
            e_msg = "Could not find Temperature Decoder {0} in the dictionary for the TWSimulator".format(
                _serial_number         # {0}
            )
            raise KeyError(e_msg)

        return self.simulated_temperature_bicoders[_serial_number]

    #################################
    def get_simulated_switch_bicoders(self, _serial_number):
        """
        Gets a specific switch biCoder (base on serial number) and flips its serial port to communicate with the
        biCoder. \n

        :param _serial_number: Serial number of the biCoder. \n
        :type _serial_number:  str \n
        """
        # Change the serial port, catch an exception if serial number does not exist
        try:
            self.simulated_switch_bicoders[_serial_number].ser = self.ser
        except KeyError:
            e_msg = "Could not find Switch Decoder {0} in the dictionary for the TWSimulator".format(
                _serial_number         # {0}
            )
            raise KeyError(e_msg)

        return self.simulated_switch_bicoders[_serial_number]

    #################################
    def clear_simulated_bicoder_objects(self):
        """
        Clears all of this TWSimulator's biCoders dictionaries
        """
        self.simulated_valve_bicoders.clear()
        self.simulated_analog_bicoders.clear()
        self.simulated_flow_bicoders.clear()
        self.simulated_moisture_bicoders.clear()
        self.simulated_temperature_bicoders.clear()
        self.simulated_switch_bicoders.clear()
        self.simulated_pump_bicoders.clear()

    #############################
    def load_all_simulated_devices(self, d1_list=list(), mv_d1_list=list(), pm_d1_list=list(), d2_list=list(), mv_d2_list=list(),
                    pm_d2_list=list(), d4_list=list(), dd_list=list(), ms_list=list(), fm_list=list(),
                    ts_list=list(), sw_list=list(), an_list=list(), unit_test=False):
        """
        Helper method. Loads all devices from the configuration file into the controller. \n
        had to have mv_d1_list and mv_d2_list in order to loader decoder that can be used as master valves\n
        master valve can only be single or dual decoders
        :param d1_list:
        :type d1_list: list[]
        :param mv_d1_list
        :param pm_d1_list
        :param d2_list:
        :param mv_d2_list
        :param pm_d2_list
        :param d4_list:
        :param dd_list:
        :param ms_list:
        :param fm_list:
        :param ts_list:
        :param sw_list:
        :param an_list:
        :param unit_test:
        :return:
        """
        try:
            self.load_simulated_device(dv_type="D1", list_of_decoder_serial_nums=d1_list,
                                       device_type=opcodes.zone)
            self.load_simulated_device(dv_type="D1", list_of_decoder_serial_nums=mv_d1_list,
                                       device_type=opcodes.master_valve)
            self.load_simulated_device(dv_type="D1", list_of_decoder_serial_nums=pm_d1_list,
                                       device_type=opcodes.pump)
            self.load_simulated_device(dv_type="D2", list_of_decoder_serial_nums=d2_list,
                                       device_type=opcodes.zone)
            self.load_simulated_device(dv_type="D2", list_of_decoder_serial_nums=mv_d2_list,
                                       device_type=opcodes.master_valve)
            self.load_simulated_device(dv_type="D2", list_of_decoder_serial_nums=pm_d2_list,
                                       device_type=opcodes.pump)
            self.load_simulated_device(dv_type="D4", list_of_decoder_serial_nums=d4_list,
                                       device_type=opcodes.zone)
            self.load_simulated_device(dv_type="DD", list_of_decoder_serial_nums=dd_list,
                                       device_type=opcodes.zone)
            self.load_simulated_device(dv_type="MS", list_of_decoder_serial_nums=ms_list,
                                       device_type=opcodes.moisture_sensor)
            self.load_simulated_device(dv_type="FM", list_of_decoder_serial_nums=fm_list,
                                       device_type=opcodes.flow_meter)
            self.load_simulated_device(dv_type="TS", list_of_decoder_serial_nums=ts_list,
                                       device_type=opcodes.temperature_sensor)
            self.load_simulated_device(dv_type="SW", list_of_decoder_serial_nums=sw_list,
                                       device_type=opcodes.event_switch)

        except Exception as e:
            # Re-raise caught exception with additional info plus the exception info.
            e.message += self.ty + ": " + self.mac + " - Exception occurred loading all available devices " \
                                                       "to controller: " + str(e.message)
            raise
        else:
            # Added a sleep here with a theory that the controller was attempting to search for devices when the thread
            # spun to load all of the devices wasn't finished. Thus when we attempted to search and assign the first
            # zone, a BC was returned
            #
            # UPDATE: 3/21/17 - Added unit test flag to reduce unit test time for CN object
            if not unit_test:
                time.sleep(10)
            print "\nController: {0} ({1})\n-> [SUCCESS] Loaded all available devices.".format(self.mac, self.ty)

    #############################
    def load_simulated_device(self,
                              dv_type,
                              list_of_decoder_serial_nums,
                              device_type=None,
                              flow_bicoder_version=6.0,
                              valve_bicoder_version=None):
        """
        pass in the decoder type and serial number serial number
        pass that value to the verify functions
        param flow_bicoder_version:    The bicoder version to load to SB. Current released version is 5.41.

        :param dv_type:
        :type dv_type: str
        :param list_of_decoder_serial_nums:
        :type list_of_decoder_serial_nums: list
        :param device_type:
        :type device_type: str
        :param flow_bicoder_version:
        :type flow_bicoder_version: float
        :param valve_bicoder_version:
        :type valve_bicoder_version: float
        :return:
        """

        # Validate decoder type passed in against accepted types
        if dv_type not in [opcodes.single_valve_decoder, opcodes.two_valve_decoder, opcodes.four_valve_decoder,
                           opcodes.twelve_valve_decoder, opcodes.flow_meter, opcodes.moisture_sensor,
                           opcodes.temperature_sensor, opcodes.four_to_20_milliamp_sensor_decoder,
                           opcodes.event_switch, opcodes.alert_relay, opcodes.analog_decoder]:
            raise ValueError("[CONTROLLER] incorrect decoder type: " + str(dv_type))

        # For each serial number for the decoder type passed in from the list.
        for serial_number in list_of_decoder_serial_nums:

            # Validate Serial Number
            if len(serial_number) != 7:
                # count the number of characters in the string
                raise ValueError("Device id is not a valid serial number: " + str(serial_number))
            else:
                print "Loading device of type: " + str(dv_type) + " with serial: " + str(serial_number)
                try:
                    command = '{0},{1}={2}'.format(
                        ActionCommands.DEVICE,  # {0}
                        dv_type,                # {1}
                        str(serial_number))     # {2}

                    # If we are a substation and loading a Flow BiCoder, set the flow bicoder version to a version
                    # that uses raw count based flow readings instead of pulse-width based flow readings. This was added
                    # due to the fact that the test-engine (python code) doesn't have the pulse-width based flow
                    # readings implemented. In the future, pulse-width based could be implemented and then this function
                    # could be used to set a flow bicoder version > 5.5

                    if dv_type == opcodes.flow_meter:
                        command += ',{0}={1}'.format('VE', flow_bicoder_version)

                    if valve_bicoder_version:
                        if dv_type == opcodes.single_valve_decoder:
                            command += ',{0}={1}'.format('VE', valve_bicoder_version)

                        if dv_type == opcodes.two_valve_decoder:
                            command += ',{0}={1}'.format('VE', valve_bicoder_version)

                        if dv_type == opcodes.four_valve_decoder:
                            command += ',{0}={1}'.format('VE', valve_bicoder_version)

                        if dv_type == opcodes.twelve_valve_decoder:
                            command += ',{0}={1}'.format('VE', valve_bicoder_version)

                    self.ser.send_and_wait_for_reply(command)
                    create_simulated_bicoder_object(_controller=self,
                                                    _serial_number=serial_number,
                                                    _device_type=device_type,
                                                    _type=dv_type)

                except Exception as e:
                    e.message = "Load device(s) failed.. " + str(e.message)
                    raise

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Add Methods                                                                                                      #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #############################
    def add_single_valve_bicoder(self, _serial_number, _device_type=opcodes.zone, _valve_bicoder_version=None):
        """
        Add a single valve bicoder to the TWSimulator. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the TWSimulator. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |zn| \n
        :type _device_type: str \n
        :param _valve_bicoder_version: version of firmware for bicoder \n
        :type _valve_bicoder_version: float \n

        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Valve Bicoder {0} to a TWSimulator {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.simulated_valve_bicoders.keys():
                self.load_dv(dv_type=opcodes.single_valve_decoder,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=_device_type,
                             valve_bicoder_version=_valve_bicoder_version)

            # If this TWSimulator is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.connected_controller:
                self.connected_controller.valve_bicoders[_serial_number] = self.simulated_valve_bicoders[_serial_number]
                self.simulated_valve_bicoders[_serial_number].base_station = self.connected_controller

    #############################
    def add_dual_valve_bicoder(self, _serial_number, _device_type=opcodes.zone, _valve_bicoder_version=None):
        """
        Add a dual valve bicoder to the TWSimulator. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the TWSimulator. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |zn| \n
        :type _device_type: str \n
        :param _valve_bicoder_version: version of firmware for bicoder \n
        :type _valve_bicoder_version: float \n

        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Valve Bicoder {0} to a TWSimulator {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.simulated_valve_bicoders.keys():
                self.load_dv(dv_type=opcodes.two_valve_decoder,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=_device_type,
                             valve_bicoder_version=_valve_bicoder_version)

            # If this TWSimulator is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.connected_controller:
                self.connected_controller.valve_bicoders[_serial_number] = self.simulated_valve_bicoders[_serial_number]
                self.simulated_valve_bicoders[_serial_number].base_station = self.connected_controller

    #############################
    def add_quad_valve_bicoder(self, _serial_number, _device_type=opcodes.zone, _valve_bicoder_version=None):
        """
        Add a quad valve bicoder to the TWSimulator. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the TWSimulator. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |zn| \n
        :type _device_type: str \n
        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Valve Bicoder {0} to a TWSimulator {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.simulated_valve_bicoders.keys():
                self.load_dv(dv_type=opcodes.four_valve_decoder,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=_device_type,
                             valve_bicoder_version=_valve_bicoder_version)

            # If this TWSimulator is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.connected_controller:
                self.connected_controller.valve_bicoders[_serial_number] = self.simulated_valve_bicoders[_serial_number]
                self.simulated_valve_bicoders[_serial_number].base_station = self.connected_controller

    #############################
    def add_twelve_valve_bicoder(self, _serial_number, _device_type=opcodes.zone, _valve_bicoder_version=None):
        """
        Add a twelve valve bicoder to the TWSimulator. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the TWSimulator. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |zn| \n
        :type _device_type: str \n
        :param _valve_bicoder_version: version of firmware for bicoder \n
        :type _valve_bicoder_version: float \n

        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Valve Bicoder {0} to a TWSimulator {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.simulated_valve_bicoders.keys():
                self.load_dv(dv_type=opcodes.twelve_valve_decoder,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=_device_type,
                             valve_bicoder_version=_valve_bicoder_version)

            # If this TWSimulator is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.connected_controller:
                self.connected_controller.valve_bicoders[_serial_number] = self.simulated_valve_bicoders[_serial_number]
                self.simulated_valve_bicoders[_serial_number].base_station = self.connected_controller

    #############################
    def add_analog_bicoder(self, _serial_number, _device_type=opcodes.pressure_sensor):
        """
        Add an analog bicoder to the TWSimulator. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the TWSimulator. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |PS| \n
        :type _device_type: str \n

        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add an Analog Bicoder {0} to a TWSimulator {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.simulated_analog_bicoders.keys():
                self.load_dv(dv_type=opcodes.analog_bicoder, list_of_decoder_serial_nums=[_serial_number], device_type=_device_type)

            # If this TWSimulator is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.connected_controller:
                self.connected_controller.analog_bicoders[_serial_number] = self.simulated_analog_bicoders[_serial_number]
                self.simulated_analog_bicoders[_serial_number].base_station = self.connected_controller

    #############################
    def add_flow_bicoder(self, _serial_number, _device_type=opcodes.flow_meter):
        """
        Add a flow bicoder to the TWSimulator. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the TWSimulator. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |FM| \n
        :type _device_type: str \n

        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Flow Bicoder {0} to a TWSimulator {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.simulated_flow_bicoders.keys():
                self.load_dv(dv_type=opcodes.flow_meter, list_of_decoder_serial_nums=[_serial_number], device_type=_device_type)

            # If this TWSimulator is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.connected_controller:
                self.connected_controller.flow_bicoders[_serial_number] = self.simulated_flow_bicoders[_serial_number]
                self.simulated_flow_bicoders[_serial_number].base_station = self.connected_controller

    #############################
    def add_moisture_bicoder(self, _serial_number, _device_type=opcodes.moisture_sensor):
        """
        Add a moisture bicoder to the TWSimulator. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the TWSimulator. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |MS| \n
        :type _device_type: str \n
        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Moisture Bicoder {0} to a TWSimulator {1}: '{2}' the value " \
                    "must be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.simulated_moisture_bicoders.keys():
                self.load_dv(dv_type=opcodes.moisture_sensor,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=_device_type)

            # If this TWSimulator is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.connected_controller:
                self.connected_controller.moisture_bicoders[_serial_number] = self.simulated_moisture_bicoders[_serial_number]
                self.simulated_moisture_bicoders[_serial_number].base_station = self.connected_controller

    #############################
    def add_switch_bicoder(self, _serial_number, _device_type=opcodes.event_switch):
        """
        Add a switch bicoder to the TWSimulator. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the TWSimulator. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |SW| \n
        :type _device_type: str \n
        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Switch Bicoder {0} to a TWSimulator {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.simulated_switch_bicoders.keys():
                self.load_dv(dv_type=opcodes.event_switch, list_of_decoder_serial_nums=[_serial_number], device_type=_device_type)

            # If this TWSimulator is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.connected_controller:
                self.connected_controller.switch_bicoders[_serial_number] = self.simulated_switch_bicoders[_serial_number]
                self.simulated_switch_bicoders[_serial_number].base_station = self.connected_controller

    #############################
    def add_temperature_bicoder(self, _serial_number, _device_type=opcodes.temperature_sensor):
        """
        Add a temperature bicoder to the TWSimulator. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the TWSimulator. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |TS| \n
        :type _device_type: str \n

        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Temperature Bicoder {0} to a TWSimulator {1}: '{2}' the " \
                    "value must be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.simulated_temperature_bicoders.keys():
                self.load_dv(dv_type=opcodes.temperature_sensor, list_of_decoder_serial_nums=[_serial_number], device_type=_device_type)

            # If this TWSimulator is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.connected_controller:
                self.connected_controller.temperature_bicoders[_serial_number] = self.simulated_temperature_bicoders[_serial_number]
                self.simulated_temperature_bicoders[_serial_number].base_station = self.connected_controller

    def share_bicoders_with_controller(self):
        # Give the controller references to all of the TWSimulator objects
        self.connected_controller.valve_bicoders.update(self.simulated_valve_bicoders)
        self.connected_controller.analog_bicoders.update(self.simulated_analog_bicoders)
        self.connected_controller.flow_bicoders.update(self.simulated_flow_bicoders)
        self.connected_controller.moisture_bicoders.update(self.simulated_moisture_bicoders)
        self.connected_controller.temperature_bicoders.update(self.simulated_temperature_bicoders)
        self.connected_controller.switch_bicoders.update(self.simulated_switch_bicoders)
        self.connected_controller.pump_bicoders.update(self.simulated_pump_bicoders)

        for bicoder in self.simulated_valve_bicoders.values():
            bicoder.base_station = self.connected_controller
        for bicoder in self.simulated_analog_bicoders.values():
            bicoder.base_station = self.connected_controller
        for bicoder in self.simulated_flow_bicoders.values():
            bicoder.base_station = self.connected_controller
        for bicoder in self.simulated_moisture_bicoders.values():
            bicoder.base_station = self.connected_controller
        for bicoder in self.simulated_temperature_bicoders.values():
            bicoder.base_station = self.connected_controller
        for bicoder in self.simulated_switch_bicoders.values():
            bicoder.base_station = self.connected_controller
        for bicoder in self.simulated_pump_bicoders.values():
            bicoder.base_station = self.connected_controller

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Super classes are below                                                                                          #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #############################
    def do_reboot_controller(self, _version_with_ok_response=opcodes.new_reboot_version_sb,
                             _version_with_response_after_reboot=opcodes.new_version_with_ok_after_reboot_sb):
        """
        Reboot a TWSimulator controller.

        :param _version_with_ok_response:   This is the TWSimulator version where it responds to a reboot command with
                                            and 'OK' instead of rebooting immediately with no response.
        :param _version_with_response_after_reboot: This is the TWSimulator version where it responds with an 'OK'
                                                    after it comes alive after a reboot.
        :return:
        """
        super(TWSimulator, self).do_reboot_controller(
            _version_with_ok_response=_version_with_ok_response,
            _version_with_response_after_reboot=_version_with_response_after_reboot
        )

    #############################
    def wait_for_controller_after_reboot(self, _version_with_response_after_reboot=None, unit_testing=False):
        """
        Waiting for a controller to wake up after a reboot.

        :param _version_with_response_after_reboot: This is the TWSimulator version where it responds with an 'OK'
                                                    after it comes alive after a reboot.
        """
        super(TWSimulator, self).wait_for_controller_after_reboot(
            _version_with_response_after_reboot=opcodes.new_version_with_ok_after_reboot_sb,
            unit_testing=False
        )

    #################################
    def do_increment_clock(self, hours=0, minutes=0, seconds=0, update_date_mngr=True, clock_format='%H:%M:%S'):
        """
        compare each value of hours, minutes, seconds \n
        this allows to send in one of the parameter to increment the clock in the controller \n
        the clock must be stopped in order it increment it

        :param hours:               Amount of hours to increment. \n
        :type hours:                int
        :param minutes:             Amount of minutes to increment. \n
        :type minutes:              int
        :param seconds:             Amount of seconds to increment. \n
        :type seconds:              int
        :param update_date_mngr:    If we also want to update the date manager value (VERY rare case where we don't
                                    want to). \n
        :type update_date_mngr:     bool
        :return:                    ok from controller
        :rtype:                     ok is a str

        """

        super(TWSimulator, self).do_increment_clock(hours=hours,
                                                    minutes=minutes,
                                                    seconds=seconds,
                                                    update_date_mngr=update_date_mngr,
                                                    clock_format=clock_format)

    #############################
    def set_controller_to_off(self, command=''):
        command = 'KEY,DL=0'
        super(TWSimulator, self).set_controller_to_off(command=command)

    #############################
    def set_controller_to_run(self, command=''):
        command = 'KEY,DL=1'
        super(TWSimulator, self).set_controller_to_run(command=command)