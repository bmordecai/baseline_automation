from common.imports import opcodes
from common.objects.base_classes.devices import BaseDevices
from common.objects.controllers.cn_watering_engine import BaseStationWateringEngine
from common.objects.messages.controllers.bl_10_messages import BL1000Messages
from common.objects.statuses.controllers.bl_10_statuses import BL1000Statuses
from common.imports.types import ControllerCommands, ActionCommands, DeviceCommands,BiCoderCommands
from common.date_package.date_resource import date_mngr
from common import object_factory
import file_transfer_handler
# from common.objects.object_bucket import basemanager_connection
# from common.objects.programming.bm import BaseManagerConnection


__author__ = 'bens'


########################################################################################################################
# CONTROLLER CLASS
########################################################################################################################

class BaseStation1000(BaseStationWateringEngine):
    """
    1000 Controller Object \n
    """

    # zone objects are assigned to this variable from the object_bucket.py when test scripts are ran.

    #############################
    def __init__(self, _mac, _serial_port, _serial_number, _port_address, _socket_port,
                 _ip_address, _firmware_version=None, _address=None, _description=''):
        """
        Initialize a controller instance with the specified parameters \n

        :param _mac:                        Mac address for controller. \n
        :type _mac:                         str \n
        :param _serial_port:                Serial port of the controller to communicate with \n
        :type _serial_port:                 common.objects.base_classes.ser.Ser \n
        :param _serial_number:              this is the serial number of the controller \n
        :type _serial_number:               str | None \n
        :param _firmware_version:           This is the firmware version of the controller \n
        :type _firmware_version:            str | None \n
        :param _port_address:
        :type _port_address:
        :param _socket_port:
        :type _socket_port:
        :param _ip_address:
        :type _ip_address:
        :param _description:                Description to put on the controller. \n
        :type _description:                 str
        """

        # Initialize parent class for inheritance
        BaseStationWateringEngine.__init__(self,
                                           _type=ControllerCommands.Type.BASESTATION_1000,
                                           _ad=_address,
                                           _mac=_mac,
                                           _serial_port=_serial_port,
                                           _serial_number=_serial_number,
                                           _description=_description,
                                           _firmware_version=_firmware_version)

        self.messages = BL1000Messages(_controller_object=self)  # Controller messages
        """:type: common.objects.messages.controllers.bl_10_messages.BL1000Messages"""

        self.ser = _serial_port

        self.dv_type = opcodes.controller

        # Controller Attributes
        self.mu = 0.0                       # Memory_Usage  - GET ONLY

        self.va = 0                         # Two-wire current
        self.vv = 0                         # Two-wire voltage

        # Assigned device serial numbers per bicoder type.
        self.d1_bicoders = list()        # Single valve
        self.d2_bicoders = list()        # Dual valve
        self.d4_bicoders = list()        # Quad valve
        self.dd_bicoders = list()        # Twelve valve
        self.ms_bicoders = list()        # Moisture bicoder serial numbers
        self.fm_bicoders = list()        # Flow bicoder serial numbers
        self.ts_bicoders = list()        # Temperature bicoder serial numbers
        self.sw_bicoders = list()        # Switch bicoder serial numbers

        self.alert_relay_bicoders = dict()  # Alert Relay bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.alert_relay_bicoder.AlertRelayBicoder]"""

        self.set_default_values()

        # Date and time on the controller
        self.controller_object_current_date_time = date_mngr.controller_datetime

        # create status reference to object
        self.statuses = BL1000Statuses(_controller_object=self)  # Controller statuses
        """:type: common.objects.statuses.controllers.bl_10_statuses.BL1000Statuses"""

    #################################
    def __str__(self):
        """
        Returns string representation of this Controller Object. \n
        :return:    String representation of this Controller Object
        :rtype:     str
        """
        return_str = "\n-----------------------------------------\n" \
                     "Controller Object:\n" \
                     "Type:                     {0}\n" \
                     "Serial Number:            {1}\n" \
                     "Mac Address:              {2}\n" \
                     "Description:              {3}\n" \
                     "Latitude:                 {4}\n" \
                     "Longitude:                {5}\n" \
                     "Max Concurrent Zones:     {6}\n" \
                     "Rain Pause Days:          {7} days\n" \
                     "Code Version:             {8}\n" \
                     "Status:                   {9}\n" \
                     "Rain Jumper State:        {10}\n" \
                     "Controller rain value:    {11}\n" \
                     "Controller ETo Date:      {12}\n" \
                     "Initial Controller ETo:   {13}\n" \
                     "Memory Usage:             {14}\n" \
                     "-----------------------------------------\n".format(
                         self.ty,       # {0}
                         self.sn,       # {1}
                         self.mac,      # {2}
                         self.ds,       # {3}
                         str(self.la),  # {4}
                         str(self.lg),  # {5}
                         str(self.mc),  # {6}
                         str(self.rp),  # {7}
                         str(self.vr),  # {8}
                         self.ss,       # {9}
                         str(self.jr),  # {10}
                         str(BaseDevices.ra),  # {11}
                         str(self.ed),  # (12}
                         str(self.ei),  # {13}
                         str(self.mu)   # {14}
                     )
        return return_str

    #################################
    def add_program_to_controller(self, _program_address):
        """
        Add a program to the controller
        :param _program_address:    address of the program \n
        :type _program_address:     int \n
        """

        if not isinstance(_program_address, int):
            e_msg = "Exception occurred trying to add program {0}'s to controller '{1}'. The program address must " \
                    "be an int, you passed in an {2}.".format(
                        str(_program_address),  # {0}
                        str(self.sn),           # {1}
                        type(_program_address)  # {2}
                    )
            raise Exception(e_msg)
        else:
            # create program object here when this is called
            object_factory.create_1000_program_object(controller=self, program_address=_program_address)
            self.programs[_program_address].send_programming()

    #################################
    def add_poc(self, _poc_address):
        """
        Add a point of control to the controller

        :param _poc_address:            address of the point of control \n
        :type _poc_address:             int \n
        """
        # create water_source object

        if not isinstance(_poc_address, int):
            e_msg = "Exception occurred trying to add point of control {0}'to the controller: '{1}' the value must " \
                    "be an int".format(
                        str(_poc_address),  # {0}
                        str(self.ad)                     # {1}
                    )
            raise Exception(e_msg)
        else:
            # create program object here when this is called
            object_factory.create_1000_point_of_control_object(controller=self,
                                                               _point_of_control_address=_poc_address)
            self.points_of_control[_poc_address].send_programming()

    #################################
    def add_alert_relay_to_controller(self, _address, _serial_number):
        """
        Add a flow meter to the controller. \n

        :param _address:    Address of the alert relay \n
        :type _address:     int \n

        :param _serial_number:    Serial number of the alert relay \n
        :type _serial_number:     str \n
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add alert relay {0} to the controller: '{1}' the value must " \
                    "be an int".format(
                        str(_address),  # {0}
                        str(_address)   # {1}
                    )
            raise Exception(e_msg)
        else:
            # The alert relay should already be created, but if it isn't, then we create it here and load it
            if _serial_number not in self.alert_relay_bicoders.keys():
                self.load_dv(dv_type=opcodes.alert_relay,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=opcodes.alert_relay)

            # create flow meters object here when this is called
            object_factory.create_alert_relay_objects(controller=self,
                                                      address_range=[_address],
                                                      serial_numbers=[_serial_number])
            self.alert_relays[_address].set_address(_ad=_address)
            self.alert_relays[_address].set_default_values()

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        command = "SET,CN,SN={0},DS={1},LA={2},LG={3},MC={4},RP={5},JR={6}".format(
            self.sn,  # {0}
            str(self.ds),  # {1}
            str(self.la),  # {2}
            str(self.lg),  # {3}
            str(self.mc),  # {4}
            str(self.rp),  # {5}
            str(self.jr)   # {6}
        )
        try:
            # Attempt to set Controller default values
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Controller {0}'s 'Default values' to: '{1}'. " \
                    "Exception received: {2}".format(self.mac, command, e.message)
            raise Exception(e_msg)
        else:
            print("Successfully set Controller {0}'s 'Default values' to: {1}".format(self.mac, command))

    #############################
    def do_enable_packet_sending_to_bm(self):
        """
        1000 Specific method. Called after devices have been loaded on the controller to bring the 1000 out of a
        paused state.
        """
        print "Starting packet communication for 1000 between BaseManager and controller"
        try:
            self.do_increment_clock(minutes=5)
            self.set_sim_mode_to_off()
            self.start_clock()
            self.send_command_with_reply('DO,BM=PN')  # ping Basemanager to sync clock with basemanager
        except Exception as e:
            e_msg = "Controller {0} has failed to enable packet sending to BaseManager. Exception: {1}".format(
                        self.mac,
                        e.message
                    )
            raise Exception(e_msg)

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Controller'. \n
        :param _expected_status:    Expected status to compare against. \n
        :type _expected_status:     str \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify base attributes
        # self.verify_date_and_time()  # verify date time object against current date time of the controller
        self.verify_description()

        self.verify_serial_number()
        self.verify_latitude()
        self.verify_longitude()

        if _expected_status is not None:
            self.verify_status(_expected_status=_expected_status)

        # Verify 'Controller' specific attributes
        self.verify_rain_jumper_state(_data=self.data)
    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Super classes are below                                                                                          #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #############################
    def do_reboot_controller(self, _version_with_ok_response=opcodes.new_reboot_version_10,
                             _version_with_response_after_reboot=opcodes.new_version_with_ok_after_reboot_10):
        """
        Reboot a 1000 controller.

        :param _version_with_ok_response:   This is the 1000 version where it responds to a reboot command with
                                            and 'OK' instead of rebooting immediately with no response.
        :param _version_with_response_after_reboot: This is the 1000 version where it responds with an 'OK'
                                                    after it comes alive after a reboot.
        :return:
        """
        super(BaseStation1000, self).do_reboot_controller(
            _version_with_ok_response=_version_with_ok_response,
            _version_with_response_after_reboot=_version_with_response_after_reboot
        )

    #############################
    def wait_for_controller_after_reboot(self, _version_with_response_after_reboot=None, unit_testing=False):
        """
        Waiting for a controller to wake up after a reboot.

        :param _version_with_response_after_reboot: This is the 1000 version where it responds with an 'OK'
                                                    after it comes alive after a reboot.
        """
        super(BaseStation1000, self).wait_for_controller_after_reboot(
            _version_with_response_after_reboot=opcodes.new_version_with_ok_after_reboot_10,
            unit_testing=False
        )

    #################################
    def do_increment_clock(self, hours=0, minutes=0, seconds=0, update_date_mngr=True, clock_format='%H:%M:%S'):
        """
        compare each value of hours, minutes, seconds \n
        this allows to send in one of the parameter to increment the clock in the controller \n
        the clock must be stopped in order it increment it

        :param hours:               Amount of hours to increment. \n
        :type hours:                int
        :param minutes:             Amount of minutes to increment. \n
        :type minutes:              int
        :param seconds:             Amount of seconds to increment. \n
        :type seconds:              int
        :param update_date_mngr:    If we also want to update the date manager value (VERY rare case where we don't
                                    want to). \n
        :type update_date_mngr:     bool
        :return:            ok from controller
        :rtype:             ok is a str

        """

        super(BaseStation1000, self).do_increment_clock(hours=hours,
                                                        minutes=minutes,
                                                        seconds=seconds,
                                                        update_date_mngr=update_date_mngr,
                                                        clock_format=clock_format)

    #############################
    def set_controller_to_off(self, command=''):
        command = 'KEY,DL=1'
        super(BaseStation1000, self).set_controller_to_off(command=command)

    #############################
    def set_controller_to_run(self, command=''):
        command = 'KEY,DL=5'
        super(BaseStation1000, self).set_controller_to_run(command=command)