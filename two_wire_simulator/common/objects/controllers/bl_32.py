from datetime import datetime

import time
from common import helper_methods
from common.imports import opcodes
from common.epa_package import equations
from common.epa_package.wbw_imports import WaterSenseCodes
from common.objects.base_classes.devices import BaseDevices
from common.objects.controllers.cn_watering_engine import BaseStationWateringEngine
from common.imports.types import ControllerCommands, ActionCommands, DeviceCommands
from common.objects.messages.controllers.bl_32_messages import BL3200Messages
from common.objects.statuses.controllers.bl_32_statuses import BL3200Statuses
from common import object_factory

import file_transfer_handler
import warnings

from common.date_package.date_resource import date_mngr


__author__ = 'Ben'

########################################################################################################################
# CONTROLLER CLASS
########################################################################################################################


class BaseStation3200(BaseStationWateringEngine):
    """
    Controller Object \n
    """

    # zone objects are assigned to this variable from the object_bucket.py when test scripts are ran.
    #############################
    def __init__(self, _mac, _serial_port, _serial_number, _port_address, _socket_port,
                 _ip_address, _firmware_version=None, _address=None, _description=''):
        """
        Initialize a controller instance with the specified parameters \n
        :param _mac:                        Mac address for controller. \n
        :type _mac:                         str \n
        :param _serial_port:                Serial port of the controller to communicate with \n
        :type _serial_port:                 common.objects.base_classes.ser.Ser \n
        :param _serial_number:              this is the serial number of the controller \n
        :type _serial_number:               str | None \n
        :param _firmware_version:           This is the firmware version of the controller \n
        :type _firmware_version:            str | None \n
        :param _port_address:
        :type _port_address:
        :param _socket_port:
        :type _socket_port:
        :param _ip_address:
        :type _ip_address:
        :param _description:                Description to put on the controller. \n
        :type _description:                 str
        """

        et = 0.00   # Controller ETo Value
        ra = 0      # Controller Rain Value
        self.websocket = '{0},{1}' .format(_ip_address, _port_address)
        # Initialize parent class for inheritance
        BaseStationWateringEngine.__init__(self,
                                           _type=ControllerCommands.Type.BASESTATION_3200,
                                           _ad=_address,
                                           _mac=_mac,
                                           _serial_port=_serial_port,
                                           _serial_number=_serial_number,
                                           _description=_description,
                                           _firmware_version=_firmware_version
                                           )

        # create message reference to object
        self.messages = BL3200Messages(_controller_object=self)  # Controller messages
        """:type: common.objects.messages.controllers.bl_32_messages.BL3200Messages"""

        self.ser = _serial_port
        self.flow_station = None
        self.websocket = '{0},{1}' .format(_ip_address, _port_address)

        # Controller Attributes
        self.mu = 0.0                       # Memory_Usage  - GET ONLY
        self.jf = 'CL'                      # Flow Jumper (OP | CL)
        self.jp = 'CL'                      # Pause Jumper (OP | CL)

        self.va = 0                         # Two-wire current
        self.vv = 0                         # Two-wire voltage

        # Stop Conditions
        self.moisture_stop_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.stop_condition.MoistureStopCondition]"""
        self.temperature_stop_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.stop_condition.TemperatureStopCondition]"""
        self.event_switch_stop_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.stop_condition.SwitchStopCondition]"""
        self.pressure_stop_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.stop_condition.PressureStopCondition]"""

        # event stop dates, 0 to 8
        self.ed = [
        ]

        # Pause Conditions
        self.moisture_pause_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.pause_condition.MoisturePauseCondition]"""
        self.temperature_pause_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.pause_condition.TemperaturePauseCondition]"""
        self.event_switch_pause_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.pause_condition.SwitchPauseCondition]"""
        self.pressure_pause_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.pause_condition.PressurePauseCondition]"""

        #  substation Specific objects
        self.substations = dict()
        """:type: dict[int, common.objects.controllers.bl_sb.Substation]"""

        # set default values for the controller
        self.set_default_values()

        # Date and time on the controller
        self.controller_object_current_date_time = date_mngr.controller_datetime

        # create status reference to object
        self.statuses = BL3200Statuses(_controller_object=self)  # Controller statuses
        """:type: common.objects.statuses.controllers.bl_32_statuses.BL3200Statuses"""

    #################################
    def __str__(self):
        """
        This is used for unit testing
        Returns string representation of this Controller Object. \n
        :return:    String representation of this Controller Object
        :rtype:     str
        """
        return_str = "\n-----------------------------------------\n" \
                     "Controller Object:\n" \
                     "Type:                     {0}\n" \
                     "Serial Number:            {1}\n" \
                     "Mac Address:              {2}\n" \
                     "Description:              {3}\n" \
                     "Latitude:                 {4}\n" \
                     "Longitude:                {5}\n" \
                     "Max Concurrent Zones:     {6}\n" \
                     "Rain Pause Days:          {7} days\n" \
                     "Code Version:             {8}\n" \
                     "Status:                   {9}\n" \
                     "Rain Jumper State:        {10}\n" \
                     "Flow Jumper State:        {11}\n" \
                     "Pause Jumper State:       {12}\n" \
                     "Controller rain value:    {13}\n" \
                     "Controller ETo Date:      {14}\n" \
                     "Initial Controller ETo:   {15}\n" \
                     "Memory Usage:             {16}\n" \
                     "-----------------------------------------\n".format(
                         self.ty,       # {0}
                         self.sn,       # {1}
                         self.mac,      # {2}
                         self.ds,       # {3}
                         str(self.la),  # {4}
                         str(self.lg),  # {5}
                         str(self.mc),  # {6}
                         str(self.rp),  # {7}
                         str(self.vr),  # {8}
                         self.ss,       # {9}
                         str(self.jr),  # {10}
                         str(self.jf),  # {11}
                         str(self.jp),  # {12}
                         # str(BaseDevices.et),  # {13}
                         str(BaseDevices.ra),  # {14}
                         str(self.ed),  # (15}
                         str(self.ei),  # {16}
                         str(self.mu)   # {17}
                     )
        return return_str

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        """
        command = "{0},{1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13},{14}={15},{16}={17},{18}={19}".format(
            ActionCommands.SET,                                     # {0}
            DeviceCommands.Type.CONTROLLER,                         # {1}
            ControllerCommands.Attributes.SERIAL_NUMBER,            # {2}
            self.sn,                                                # {3}
            ControllerCommands.Attributes.DESCRIPTION,              # {4}
            str(self.ds),                                           # {5}
            ControllerCommands.Attributes.LATITUDE,                 # {6}
            str(self.la),                                           # {7}
            ControllerCommands.Attributes.LONGITUDE,                # {8}
            str(self.lg),                                           # {9}
            ControllerCommands.Attributes.MAX_CONCURRENT_ZONES,     # {10}
            str(self.mc),                                           # {11}
            ControllerCommands.Attributes.RAIN_PAUSE_DAYS,          # {12}
            str(self.rp),                                           # {13}
            ControllerCommands.Attributes.RAIN_JUMPER,              # {14}
            str(self.jr),                                           # {15}
            ControllerCommands.Attributes.FLOW_JUMPER,              # {16}
            str(self.jf),                                           # {17}
            ControllerCommands.Attributes.PAUSE_JUMPER,             # {18}
            str(self.jp),                                           # {19}
            ControllerCommands.Attributes.CURRENT,                  # {20}
            str(self.va),                                           # {21}
            ControllerCommands.Attributes.VOLTAGE,                  # {22}
            str(self.vv)                                            # {23}
            )

        try:
            # Attempt to set Controller default values
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Controller {0}'s 'Default values' to: '{1}'. " \
                    "Exception received: {2}".format(self.mac, command, e.message)
            raise Exception(e_msg)
        else:
            print("Successfully set Controller {0}'s 'Default values' to: {1}".format(self.mac, command))

    #################################
    def add_program_to_controller(self, _program_address):
        """
        Add a program to the controller
        :param _program_address:    address of the program \n
        :type _program_address:     int \n
        """

        if not isinstance(_program_address, int):
            e_msg = "Exception occurred trying to add program {0}'s to controller '{1}'. The program address must " \
                    "be an int, you passed in an {2}.".format(
                        str(_program_address),  # {0}
                        str(self.sn),           # {1}
                        type(_program_address)  # {2}
                    )
            raise Exception(e_msg)
        else:
            # create program object here when this is called
            object_factory.create_3200_program_object(controller=self, program_address=_program_address)
            self.programs[_program_address].send_programming()

    #################################
    def add_point_of_control_to_controller(self, _point_of_control_address):
        """
        Add a point of control to the controller

        :param _point_of_control_address:            address of the point of control \n
        :type _point_of_control_address:             int \n
        """
        # create water_source object

        if not isinstance(_point_of_control_address, int):
            e_msg = "Exception occurred trying to add point of control {0}'to the controller: '{1}' the value must " \
                    "be an int".format(
                str(_point_of_control_address),  # {0}
                str(self.ad)  # {1}
            )
            raise Exception(e_msg)
        else:
            # create program object here when this is called
            object_factory.create_point_of_control_object(controller=self,
                                                          _point_of_control_address=_point_of_control_address)
            self.points_of_control[_point_of_control_address].send_programming()

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Add Start Stop Pauses to the 3200                                                                                #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def add_moisture_stop_condition(self, _moisture_sensor_address):
        """
        Add a moisture stop condition on all programs. \n

        :param _moisture_sensor_address:    address of the moisture sensor \n
        :type _moisture_sensor_address:     int \n
        """
        from common.object_factory import create_moisture_stop_all_condition_object

        # Check if the moisture sensor address is an int
        if not isinstance(_moisture_sensor_address, int):
            e_msg = "Exception occurred trying to add Moisture Stop Condition {0} to controller {1}. The value of " \
                    "the moisture sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_moisture_sensor_address),     # {0}
                        str(self.mac),                     # {1}
                        type(_moisture_sensor_address)     # {2}
                    )
            raise Exception(e_msg)

        # loop through moisture sensor dict to see if the moisture sensor object is present
        if _moisture_sensor_address not in self.moisture_sensors.keys():
            e_msg = 'Exception occurred trying to add Moisture Start Condition {0} to controller {1}. The moisture ' \
                    'sensor address did not belong to any moisture sensors on the controller.'.format(
                        str(_moisture_sensor_address),      # {0}
                        str(self.mac),                      # {1}
                    )
            raise Exception(e_msg)

        # create condition object
        create_moisture_stop_all_condition_object(controller=self,
                                                  moisture_sensor_address=_moisture_sensor_address)

    #################################
    def add_moisture_pause_condition(self, _moisture_sensor_address):
        """
        Add a moisture pause condition on all programs. \n

        :param _moisture_sensor_address:    address of the moisture sensor \n
        :type _moisture_sensor_address:     int \n
        """
        from common.object_factory import create_moisture_pause_all_condition_object

        # Check if the moisture sensor address is an int
        if not isinstance(_moisture_sensor_address, int):
            e_msg = "Exception occurred trying to add Moisture Pause Condition {0} to controller {1}. The value of " \
                    "the moisture sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_moisture_sensor_address),  # {0}
                        str(self.mac),                  # {1}
                        type(_moisture_sensor_address)  # {2}
                    )
            raise Exception(e_msg)

        # loop through moisture sensor dict to see if moisture sensor object is present
        if _moisture_sensor_address not in self.moisture_sensors.keys():
            e_msg = 'Exception occurred trying to add Moisture Start Condition {0} to controller {1}. The moisture ' \
                    'sensor address did not belong to any moisture sensors on the controller.'.format(
                        str(_moisture_sensor_address),      # {0}
                        str(self.mac),                      # {1}
                    )
            raise Exception(e_msg)

        # create condition object
        create_moisture_pause_all_condition_object(controller=self,
                                                   moisture_sensor_address=_moisture_sensor_address)

    #################################
    def add_temperature_stop_condition(self, _temperature_sensor_address):
        """
        Add a temperature stop condition on all programs. \n

        :param _temperature_sensor_address:    address of the temperature sensor \n
        :type _temperature_sensor_address:     int \n
        """
        from common.object_factory import create_temperature_stop_all_condition_object

        # Check if the temperature sensor address is an int
        if not isinstance(_temperature_sensor_address, int):
            e_msg = "Exception occurred trying to add Temperature Stop Condition {0} to controller {1}. The value of " \
                    "the temperature sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_temperature_sensor_address),   # {0}
                        str(self.mac),                      # {1}
                        type(_temperature_sensor_address)   # {2}
                    )
            raise Exception(e_msg)

        # loop through temperature sensor dict to see if the temperature sensor object is present
        if _temperature_sensor_address not in self.temperature_sensors.keys():
            e_msg = 'Exception occurred trying to add Temperature Start Condition {0} to controller {1}. The ' \
                    'temperature sensor address did not belong to any temperature sensors on the controller.'.format(
                        str(_temperature_sensor_address),   # {0}
                        str(self.mac),                      # {1}
                    )
            raise Exception(e_msg)

        # create condition object
        create_temperature_stop_all_condition_object(controller=self,
                                                     temperature_sensor_address=_temperature_sensor_address)

    #################################
    def add_temperature_pause_condition(self, _temperature_sensor_address):
        """
        Add a temperature pause condition on all programs. \n

        :param _temperature_sensor_address:    address of the temperature sensor \n
        :type _temperature_sensor_address:     int \n
        """
        from common.object_factory import create_temperature_pause_all_condition_object

        # Check if the temperature sensor address is an int
        if not isinstance(_temperature_sensor_address, int):
            e_msg = "Exception occurred trying to add Temperature Pause Condition {0} to controller {1}. The value " \
                    "of the temperature sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_temperature_sensor_address),   # {0}
                        str(self.mac),                      # {1}
                        type(_temperature_sensor_address)   # {2}
                    )
            raise Exception(e_msg)

        # loop through temperature sensor dict to see if the temperature sensor object is present
        if _temperature_sensor_address not in self.temperature_sensors.keys():
            e_msg = 'Exception occurred trying to add Temperature Start Condition {0} to controller {1}. The ' \
                    'temperature sensor address did not belong to any temperature sensors on the controller.'.format(
                        str(_temperature_sensor_address),  # {0}
                        str(self.mac),                     # {1}
                    )
            raise Exception(e_msg)

        # create condition object
        create_temperature_pause_all_condition_object(controller=self,
                                                      temperature_sensor_address=_temperature_sensor_address)

    #################################
    def add_pressure_stop_condition(self, _pressure_sensor_address):
        """
        Add a pressure stop condition on all programs. \n

        :param _pressure_sensor_address:    address of the pressure sensor \n
        :type _pressure_sensor_address:     int \n
        """
        from common.object_factory import create_pressure_stop_all_condition_object

        # Check if the pressure sensor address is an int
        if not isinstance(_pressure_sensor_address, int):
            e_msg = "Exception occurred trying to add Pressure Stop Condition {0} to controller {1}. The value of " \
                    "the pressure sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_pressure_sensor_address),  # {0}
                        str(self.mac),                  # {1}
                        type(_pressure_sensor_address)  # {2}
                    )
            raise Exception(e_msg)

        # loop through pressure sensor dict to see if the pressure sensor object is present
        if _pressure_sensor_address not in self.pressure_sensors.keys():
            e_msg = 'Exception occurred trying to add Pressure Start Condition {0} to controller {1}. The pressure ' \
                    'sensor address did not belong to any pressure sensors on the controller.'.format(
                        str(_pressure_sensor_address),  # {0}
                        str(self.mac),                  # {1}
                    )
            raise Exception(e_msg)

        # create condition object
        create_pressure_stop_all_condition_object(controller=self,
                                                  pressure_sensor_address=_pressure_sensor_address)

    #################################
    def add_pressure_pause_condition(self, _pressure_sensor_address):
        """
        Add a pressure pause condition on all programs. \n

        :param _pressure_sensor_address:    address of the pressure sensor \n
        :type _pressure_sensor_address:     int \n
        """
        from common.object_factory import create_pressure_pause_all_condition_object

        # Check if the pressure sensor address is an int
        if not isinstance(_pressure_sensor_address, int):
            e_msg = "Exception occurred trying to add Pressure Pause Condition {0} to controller {1}. The value of " \
                    "the pressure sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_pressure_sensor_address),  # {0}
                        str(self.mac),                  # {1}
                        type(_pressure_sensor_address)  # {2}
                    )
            raise Exception(e_msg)

        # loop through pressure sensor dict to see if the pressure sensor object is present
        if _pressure_sensor_address not in self.pressure_sensors.keys():
            e_msg = 'Exception occurred trying to add Pressure Start Condition {0} to controller {1}. The pressure ' \
                    'sensor address did not belong to any pressure sensors on the controller.'.format(
                        str(_pressure_sensor_address),  # {0}
                        str(self.mac),                  # {1}
                    )
            raise Exception(e_msg)

        # create condition object
        create_pressure_pause_all_condition_object(controller=self,
                                                   pressure_sensor_address=_pressure_sensor_address)

    #################################
    def add_switch_stop_condition(self, _event_switch_address):
        """
        Add a switch stop condition on all programs. \n

        :param _event_switch_address:    address of the event switch \n
        :type _event_switch_address:     int \n
        """
        from common.object_factory import create_switch_stop_all_condition_object

        # Check if the event switch address is an int
        if not isinstance(_event_switch_address, int):
            e_msg = "Exception occurred trying to add Event Switch Stop Condition {0} to controller {1}. The value " \
                    "of the switch sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_event_switch_address),     # {0}
                        str(self.mac),                   # {1}
                        type(_event_switch_address)     # {2}
                    )
            raise Exception(e_msg)

        # loop through event switch dict to see if the event switch object is present
        if _event_switch_address not in self.event_switches.keys():
            e_msg = 'Exception occurred trying to add Event Switch Start Condition {0} to controller {1}. The switch ' \
                    'sensor address did not belong to any switch sensors on the controller.'.format(
                        str(_event_switch_address),     # {0}
                        str(self.mac),                   # {1}
                    )
            raise Exception(e_msg)

        # create condition object
        create_switch_stop_all_condition_object(controller=self,
                                                event_switch_address=_event_switch_address)

    #################################
    def add_switch_pause_condition(self, _event_switch_address):
        """
        Add a switch pause condition on all programs. \n

        :param _event_switch_address:    address of the substation \n
        :type _event_switch_address:     int \n
        """
        from common.object_factory import create_switch_pause_all_condition_object

        # Check if the event switch address is an int
        if not isinstance(_event_switch_address, int):
            e_msg = "Exception occurred trying to add Event Switch Pause Condition {0} to controller {1}. The value " \
                    "of the switch sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_event_switch_address),     # {0}
                        str(self.mac),                  # {1}
                        type(_event_switch_address)     # {2}
                    )
            raise Exception(e_msg)

        # loop through event switch dict to see if the event switch object is present
        if _event_switch_address not in self.event_switches.keys():
            e_msg = 'Exception occurred trying to add Event Switch Start Condition {0} to controller {1}. The switch ' \
                    'sensor address did not belong to any switch sensors on the controller.'.format(
                        str(_event_switch_address),     # {0}
                        str(self.mac),                  # {1}
                    )
            raise Exception(e_msg)

        # create condition object
        create_switch_pause_all_condition_object(controller=self, event_switch_address=_event_switch_address)

    #################################
    def add_water_source_to_controller(self, _water_source_address):
        """
        Add a water source to the controller

        :param _water_source_address:            address of the water source \n
        :type _water_source_address:             int \n
        """
        # create water_source object

        if not isinstance(_water_source_address, int):
            e_msg = "Exception occurred trying to add water source {0}'s to controller '{1}'. The program address " \
                    "must be an int, you passed in an {2}.".format(
                str(_water_source_address),  # {0}
                str(self.sn),  # {1}
                type(_water_source_address)  # {2}
            )
            raise Exception(e_msg)
        else:
            # create program object here when this is called
            object_factory.create_water_source_object(controller=self, _water_source_address=_water_source_address)
            self.water_sources[_water_source_address].send_programming()

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Add Substations to 3200                                                                                          #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_connection_to_substation_as_disconnected(self, _substation_address):
        """
        Disconnects the controller from the substation. \n

        :param _substation_address: The address the controller assigns to the substation (1-8) \n
        :type _substation_address: int
        """
        self.set_connection_to_substation(_substation_address=_substation_address, _enabled=opcodes.false)

    #################################
    def set_connection_to_substation_as_connected(self, _substation_address):
        """
        Connects/re-connects the controller to the substation. \n

        :param _substation_address: The address the controller assigns to the substation (1-8) \n
        :type _substation_address: int
        """
        self.set_connection_to_substation(_substation_address=_substation_address, _enabled=opcodes.true)

    #################################
    def set_connection_to_substation(self, _substation_address, _enabled=opcodes.true):
        """
        Establishes a connection from a 3200 to a substation using local IP's. \n

        :param _substation_address:        The address the controller assigns to the substation (1-8) \n
        :type _substation_address:         int

        :param _enabled:        Enabled state for the substation (TR | FA) \n
        :type _enabled:         str
        """

        # Verify the ranges for addressing
        if _substation_address not in range(1, 9):
            e_msg = "Can not establish a connection to a substation from a controller with an address that is not" \
                    "in the range 1 through 8. Value passed in: {0}.".format(_substation_address)
            raise ValueError(e_msg)

        # This code was getting the IP address of the socket port, we want the local IP
        # # Get the socket port address of the substation we want to connect to
        # _substation_socket_port = self.substations[_substation_address].socket_port_address
        # # Finds just the ip address portion of the substations socket port address
        # _substation_ip = re.findall( r'[0-9]+(?:\.[0-9]+){3}', _substation_socket_port)[0]

        # Get the local IP address of the substation
        substation_local_ip = self.substations[_substation_address].ip_address

        # Build the command string
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                     # {0}
            ControllerCommands.Type.SUBSTATION,     # {1}
            _substation_address,                    # {2}
            ControllerCommands.Attributes.ENABLED,  # {3}
            _enabled,                               # {4}
            opcodes.address,                        # {5}
            substation_local_ip                     # {6}
        )

        try:
            # Send the command through the serial port
            self.ser.send_and_wait_for_reply(tosend=command)

            # If passing in enabled=false, then we are trying to disconnect substation from controller. So we want to
            # specifically wait for correct connection status.
            expected_status = opcodes.connected
            if _enabled == opcodes.false:
                expected_status = opcodes.disconnected

            self.wait_for_controller_and_substations_connection_status(_substation_address=_substation_address,
                                                                       _max_wait=60,
                                                                       expected_status=expected_status)
        except Exception as e:
            e_msg = "Exception occurred trying to set Controller's 'Substation Connection' for " \
                    "Substation: '{0}', IP Address: '{1}', Enabled: '{2}'.\nException: {3}".format(
                        _substation_address,
                        substation_local_ip,
                        _enabled,
                        e.message)
            raise Exception(e_msg)
        else:
            print "Successfully set controller 'Substation Connection' for Substation: '{0}', IP Address: '{1}', " \
                  "Enabled: '{2}'.".format(
                        _substation_address,
                        substation_local_ip,
                        _enabled)

    #################################
    def add_substation_to_controller(self, _substation_address, _substation):
        """
        Add a substation to the controller. \n

        :param _substation_address: Address of the substation. \n
        :type _substation_address:  int

        :param _substation:         Substation object to put on the controller. \n
        :type _substation:          common.objects.controllers.bl_sb.Substation
        """
        if not isinstance(_substation_address, int):
            e_msg = "Exception occurred trying to add SubStation{0}'to the controller: '{1}' the value must " \
                    "be an int".format(
                str(_substation_address),  # {0}
                str(self.ad)  # {1}
            )
            raise Exception(e_msg)
        elif _substation_address not in range(1, 9):
            e_msg = "A Substation's address on a 3200 must be between 1 and 8, you passed in {0}.".format(
                _substation_address
            )
            raise Exception(e_msg)
        else:
            # Give each object a reference to each other
            self.substations[_substation_address] = _substation
            _substation.base_station_3200 = self

            # Send the command for the controller to connect to the substation and then wait for a "CN" response
            self.set_connection_to_substation(_substation_address=_substation_address)

            # Give the basestation references to all of the substation objects
            self.substations[_substation_address].share_bicoders_with_controller()

    #################################
    def wait_for_controller_and_substations_connection_status(self, _substation_address, _max_wait,
                                                              expected_status=opcodes.connected):
        """
        Waits for the Controller and Substation to establish a connection. \n

        :param _substation_address: Address of the substation. \n
        :type _substation_address:  int \n

        :param _max_wait:           Number of minutes to wait before raising exception.
        :type _max_wait:            int \n

        :param expected_status:
        :param expected_status:
        """
        max_wait_for_connection = _max_wait     # Maximum number of minutes to wait for reconnection to succeed
        current_wait_for_connection = 1         # Current number of minutes waited for reconnection

        while current_wait_for_connection <= max_wait_for_connection:

            try:
                time.sleep(1)
                helper_methods.increment_controller_substation_clocks(controller=self,
                                                                      substations={
                                                                          1: self.substations[_substation_address]
                                                                      },
                                                                      minutes=1)

                if expected_status == opcodes.connected:
                    # Verify Controller is connected
                    self.verify_connected_to_substation(_address=_substation_address)
                    # Verify Substation is connected.
                    self.substations[_substation_address].verify_connected_to_controller()
                else:
                    # Verify Controller is disconnected
                    self.verify_not_connected_to_substation(_address=_substation_address)
                    # Verify Substation is disconnected.
                    self.substations[_substation_address].verify_not_connected_to_controller()

                # If we hit this point we verified the connection successfully
                break

            # Catch ValueError exception for when status is not connected
            except ValueError as e:

                # If we have waited for 5 minutes and controllers haven't reconnected, raise exception.
                if current_wait_for_connection == max_wait_for_connection:
                    e_msg = "Connection timed out (waited up to {0} minutes) attempting to connect 3200 " \
                            "to substation.\n {1}".format(max_wait_for_connection, e.message)
                    raise Exception(e_msg)
                else:
                    current_wait_for_connection += 1

   #################################
    def verify_connected_to_substation(self, _address):
        """
        Verifies the substation connection against an expected status.

        :param _address: Address of substation to verify status for.
        :type _address: int

        :return:
        :rtype: bool
        """
        _expected_status = opcodes.connected

        command = "{0},{1}={2}".format(
            ActionCommands.GET,                     # {0}
            ControllerCommands.Type.SUBSTATION,     # {1}
            str(_address)                           # {2}
        )

        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to get Controller's 'Substation Connection Status' for " \
                    "Substation: '{0}'.".format(str(_address))
            raise Exception(e_msg)

        else:
            sb_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if _expected_status != sb_ss_on_cn:
                e_msg = "Exception occurred trying to verify Controller's 'Substation Connection Status' for " \
                        "Substation: '{0}'. Expected Status: '{1}', Received: '{2}'.".format(
                            str(_address),
                            _expected_status,
                            sb_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified Controller's 'Substation Connection Status' for Substation: '{0}', " \
                      "Status: '{1}'.".format(str(_address), _expected_status)
                return True

    #################################
    def verify_not_connected_to_substation(self, _address):
        """
        Verifies the substation connection against an expected status.

        :param _address: Address of substation to verify status for.
        :type _address: int

        :return:
        :rtype: bool
        """
        _expected_status = opcodes.disconnected

        command = "{0},{1}={2}".format(
            ActionCommands.GET,                     # {0}
            ControllerCommands.Type.SUBSTATION,     # {1}
            str(_address)                           # {2}
        )

        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to get Controller's 'Substation Connection Status' for " \
                    "Substation: '{0}'.".format(str(_address))
            raise Exception(e_msg)

        else:
            sb_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if _expected_status != sb_ss_on_cn:
                e_msg = "Exception occurred trying to verify Controller's 'Substation Connection Status' for " \
                        "Substation: '{0}'. Expected Status: '{1}', Received: '{2}'.".format(
                            str(_address),
                            _expected_status,
                            sb_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified Controller's 'Substation Connection Status' for Substation: '{0}', " \
                      "Status: '{1}'.".format(str(_address), _expected_status)
                return True
    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Add 3200 to the FlowStation                                                                                      #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_connection_to_flowstation(self, _address, _ip_address=None, _enabled=False):
        """
        Establishes a connection from a 3200 to a flowstation using local IP's \n

        :param _address:        The address the controller assigns to the flow stations \n
        :type _address:         int
        :param _ip_address:     The ip address of the flowstation we want to connect to (ex: 10.11.12.144) \n
        :type _ip_address:      str | None
        :param _enabled:        Enabled state for the flowstation (TR | FA) \n
        :type _enabled:         str
        """

        # Build the command string
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,     # {0}
            opcodes.flow_station,     # {1}
            _address,               # {2}
            opcodes.enabled,        # {3}
            opcodes.true)               # {4}

        if _ip_address:
            command += ",{0}={1}".format(
                opcodes.address,    # {0}
                _ip_address,        # {1}
            )

        try:
            # Send the command through the serial port
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'flowstation Connection' for " \
                    "FLowStation: '{0}', IP Address: '{1}', Enabled: '{2}'.".format(_address, _ip_address, opcodes.true)
            raise Exception(e_msg)
        else:
            print "Successfully set controller 'FlowStation Connection' for Substation: '{0}', IP Address: '{1}', " \
                  "Enabled: '{2}'.".format(_address, _ip_address, opcodes.true)

    #################################
    def set_disconnect_from_flowstation(self, _address, _ip_address=None, _enabled=opcodes.true):
        """
        Establishes a connection from a 3200 to a flowstation using local IP's \n

        :param _address:        The address the controller assigns to the flow stations \n
        :type _address:         int
        :param _ip_address:     The ip address of the flowstation we want to connect to (ex: 10.11.12.144) \n
        :type _ip_address:      str | None
        :param _enabled:        Enabled state for the flowstation (TR | FA) \n
        :type _enabled:         str
        """

        # Build the command string
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,     # {0}
            opcodes.flow_station,     # {1}
            _address,               # {2}
            opcodes.enabled,        # {3}
            opcodes.false)               # {4}

        if _ip_address:
            command += ",{0}={1}".format(
                opcodes.address,    # {0}
                _ip_address,        # {1}
            )

        try:
            # Send the command through the serial port
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'flowstation Connection' for " \
                    "FLowStation: '{0}', IP Address: '{1}', Enabled: '{2}'.".format(_address, _ip_address, opcodes.false)
            raise Exception(e_msg)
        else:
            print "Successfully set controller 'FlowStation Connection' for Substation: '{0}', IP Address: '{1}', " \
                  "Enabled: '{2}'.".format(_address, _ip_address, opcodes.false)

    #################################
    def verify_connected_to_flowstation(self):
        """
        Verifies the Controller connection status to the FlowStation against an expected status.

        :return:
        :rtype: bool
        """
        if self.controller_type in [ControllerCommands.Type.BASESTATION_1000, ControllerCommands.Type.BASESTATION_3200]:
            _expected_status = opcodes.connected
            command = "{0},{1}={2}".format(
                ActionCommands.GET,                 # {0}
                DeviceCommands.Type.FLOWSTATION,    # {1}
                "1"                                 # {2}
            )

            try:
                data = self.ser.get_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to get Controller's 'FlowStation Connection Status' for " \
                        "BaseStation 3200: '{0}'.".format(str(self.ad))
                raise Exception(e_msg)

            else:
                sb_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

                if _expected_status != sb_ss_on_cn:
                    e_msg = "Exception occurred trying to verify Controller's 'FlowStation Connection Status' for " \
                            "BaseStation 3200: '{0}'. Expected Status: '{1}', Received: '{2}'.".format(
                                str(self.ad),
                                _expected_status,
                                sb_ss_on_cn
                            )
                    raise ValueError(e_msg)
                else:
                    print "Successfully verified Controller's 'FlowStation Connection Status' for BaseStation 3200: " \
                          "'{0}', Status: '{1}'.".format(str(self.ad), _expected_status)
                    return True

        else:
            raise ValueError(
                "Attempting to verify 'Connection from Controller to FlowStation' on a BaseStation 3200 which "
                "is not supported.")

    #################################
    def verify_not_connected_to_flowstation(self):
        """
        Verifies the Controller connection status to the FlowStation against an expected status.

        :return:
        :rtype: bool
        """
        if self.ty in [ControllerCommands.Type.BASESTATION_1000, ControllerCommands.Type.BASESTATION_3200]:
            _expected_status = opcodes.disconnected
            command = "{0},{1}={2}".format(
                ActionCommands.GET,                 # {0}
                DeviceCommands.Type.FLOWSTATION,    # {1}
                str(self.ad)                        # {2} # TODO this should only be 1. Can only have one FS currently.
            )

            try:
                data = self.ser.get_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to get Controller's 'FlowStation Connection Status' for " \
                        "BaseStation 3200: '{0}'.".format(str(self.ad))
                raise Exception(e_msg)

            else:
                sb_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

                if _expected_status != sb_ss_on_cn:
                    e_msg = "Exception occurred trying to verify Controller's 'FlowStation Connection Status' for " \
                            "BaseStation 3200: '{0}'. Expected Status: '{1}', Received: '{2}'.".format(
                                str(self.ad),
                                _expected_status,
                                sb_ss_on_cn
                            )
                    raise ValueError(e_msg)
                else:
                    print "Successfully verified Controller's 'FlowStation Connection Status' for BaseStation 3200: " \
                          "'{0}', Status: '{1}'.".format(str(self.ad), _expected_status)
                    return True

        else:
            raise ValueError(
                "Attempting to verify 'Connection from Controller to FlowStation' on a BaseStation 3200 which "
                "is not supported.")

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # 3200 specific setters and verifiers                                                                              #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_event_stop_dates(self, _date_list=None):
        """
        Sets the event stop dates on the controller. \n
        :param _date_list:    List of up to 8 strings representing the date: mm/dd/yy \n
        :type _date_list:     list[str] \n
        :return:
        """
        # check to see if a start time list is passed in to overwrite current start time list
        if _date_list is not None:
            self.ed = self.verify_valid_event_stop_dates(_list_of_dates=_date_list)

        # Build event date string for sending to controller
        build_event_dates_string = self.build_event_stop_dates_string(_ed_list=self.ed)

        # Command for sending to controller
        command = "{0},{1},{2}={3}".format(
            ActionCommands.SET,                         # {0}
            opcodes.controller,                         # {1}
            opcodes.event_day,                          # {2}
            build_event_dates_string                    # {3}
        )

        # Attempt to send command to controller
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set controller 'Event Dates' : {0}".format(
                build_event_dates_string,    # {0}
                e.message
            )
            raise Exception(e_msg)
        else:
            print("Successfully set 'Event Dates' to controller: {0}".format(
                  build_event_dates_string   # {0}
                  ))
    #################################
    def verify_event_stop_dates(self, _date_list=None):
        pass
    #TODO need to implement

    #################################
    def set_flow_jumper_state(self, _value=None):
        """
        Set the flow jumper to be init_driver or closed based on the value passed in for a 3200. \n
        :param _value:      Accepted values: 'OP' (init_driver), 'CL' (closed) \n
        :type _value:       String. \n
        :return:
        """

        # Check if overwrite is intended
        if _value is not None:

            # Make sure entered correct value
            if _value not in [opcodes.closed, opcodes.open]:
                e_msg = "Exception occurred attempting to set incorrect 'Flow Jumper State' value for controller: " \
                        "'{0}', Expects 'CL' | 'OP'.".format(str(_value))
                raise ValueError(e_msg)
            else:
                self.jf = _value

        # Command for sending
        command = "{0},{1},{2}={3}".format(
            ActionCommands.SET,                         # {0}
            DeviceCommands.Type.CONTROLLER,             # {1}
            ControllerCommands.Attributes.FLOW_JUMPER,  # {2}
            str(self.jf)                                # {3}
        )
        try:
            # Send command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Flow Jumper State' to: '{0}'".format(
                str(self.jf)    # {0}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set controller's 'Flow Jumper' to: {0}".format(str(self.jf)))

    #################################
    def set_flow_jumper_to_closed(self):
        """
        Set the flow jumper to be closed. \n
        :return:
        """

        self.jf = opcodes.closed

        # Command for sending
        command = "{0},{1},{2}={3}".format(
            ActionCommands.SET,  # {0}
            DeviceCommands.Type.CONTROLLER,  # {1}
            ControllerCommands.Attributes.FLOW_JUMPER,  # {2}
            str(self.jf)  # {3}
        )
        try:
            # Send command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Flow Jumper State' to: '{0}'".format(
                str(self.jf)  # {0}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set controller's 'Flow Jumper' to: {0}".format(str(self.jf)))

    #################################
    def set_flow_jumper_to_open(self):
        """
        Set the flow jumper open. \n
        :return:
        """

        self.jf = opcodes.open

        # Command for sending
        command = "{0},{1},{2}={3}".format(
            ActionCommands.SET,  # {0}
            DeviceCommands.Type.CONTROLLER,  # {1}
            ControllerCommands.Attributes.FLOW_JUMPER,  # {2}
            str(self.jf)  # {3}
        )
        try:
            # Send command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Flow Jumper State' to: '{0}'".format(
                str(self.jf)  # {0}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set controller's 'Flow Jumper' to: {0}".format(str(self.jf)))

    #################################
    def verify_flow_jumper_state(self, _data=None):
        """
        Verifies the flow jumper's current state on the controller against the expected state passed in as a parameter.

        :param _data:   Key Value string \n
        :type _data:    status_parser.KeyValues \n
        """
        data = _data if _data else self.get_data()
        # Get the current state of the 'Flow Jumper' on the controller
        jf_state_from_cn = data.get_value_string_by_key(ControllerCommands.Attributes.FLOW_JUMPER)

        # Compare against what is on the controller
        if self.jf != jf_state_from_cn:
            e_msg = "Unable to verify Controller's 'Flow Jumper State'. Received: {0}, Expected: {1}".format(
                    str(jf_state_from_cn),  # {0}
                    self.jf                 # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Controller {0}'s 'Flow Jumper State': '{1}'.".format(
                  str(self.mac),    # {0}
                  self.jf           # {1}
                  ))

    #################################
    def set_pause_jumper_state(self, _value=None):
        """
        Set the pause jumper to be init_driver or closed based on the value passed in for a 3200. \n
        :param _value:      Accepted values: 'OP' (init_driver), 'CL' (closed) \n
        :type _value:       String. \n
        :return:
        """

        # Check if overwrite is intended
        if _value is not None:

            # Make sure entered correct value
            if _value not in [opcodes.closed, opcodes.open]:
                e_msg = "Exception occurred attempting to set incorrect 'Pause Jumper State' value for " \
                        "controller: '{0}', Expects 'CL' | 'OP'.".format(str(_value))
                raise ValueError(e_msg)
            else:
                self.jp = _value

        # Command for sending
        command = "{0},{1},{2}={3}".format(
            ActionCommands.SET,                             # {0}
            DeviceCommands.Type.CONTROLLER,                 # {1}
            ControllerCommands.Attributes.PAUSE_JUMPER,     # {2}
            str(self.jp)                                    # {3}
        )
        try:
            # Send command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Pause Jumper State' to: '{0}'" \
                    .format(str(self.jp))
            raise Exception(e_msg)
        else:
            print("Successfully set controller's 'Pause Jumper' to: {0}".format(str(self.jp)))

    #################################
    def set_pause_jumper_to_closed(self):
        """
        Set the pause jumper to be closed\n
        """

        self.jp = opcodes.closed

        # Command for sending
        command = "{0},{1},{2}={3}".format(
            ActionCommands.SET,  # {0}
            DeviceCommands.Type.CONTROLLER,  # {1}
            ControllerCommands.Attributes.PAUSE_JUMPER,  # {2}
            str(self.jp)  # {3}
        )
        try:
            # Send command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Pause Jumper State' to: '{0}'" \
                .format(str(self.jp))
            raise Exception(e_msg)
        else:
            print("Successfully set controller's 'Pause Jumper' to: {0}".format(str(self.jp)))

    #################################
    def set_pause_jumper_to_open(self):
        """
        Set the pause jumper to be open. \n
        """
        self.jp = opcodes.open
        # Command for sending
        command = "{0},{1},{2}={3}".format(
            ActionCommands.SET,                             # {0}
            DeviceCommands.Type.CONTROLLER,                 # {1}
            ControllerCommands.Attributes.PAUSE_JUMPER,     # {2}
            str(self.jp)                                    # {3}
        )
        try:
            # Send command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Pause Jumper State' to: '{0}'" \
                .format(str(self.jp))
            raise Exception(e_msg)
        else:
            print("Successfully set controller's 'Pause Jumper' to: {0}".format(str(self.jp)))


    #################################
    def verify_pause_jumper_state(self, _data=None):
        """
        Verifies the pause jumper's current state on the controller against the expected state passed in as a parameter.

        :param _data:   Key Value string \n
        :type _data:    status_parser.KeyValues \n
        """
        data = _data if _data else self.get_data()
        # Get the current state of the 'Pause Jumper' on the controller
        jp_state_from_cn = data.get_value_string_by_key(ControllerCommands.Attributes.PAUSE_JUMPER)

        # Compare against what is on the controller
        if self.jp != jp_state_from_cn:
            e_msg = "Unable to verify Controller's 'Pause Jumper State'. Received: {0}, Expected: {1}".format(
                str(jp_state_from_cn),  # {0}
                self.jp                 # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Controller {0}'s 'Pause Jumper State': '{1}'.".format(
                str(self.mac),    # {0}
                self.jp           # {1}
            ))

    #################################
    def get_memory_usage(self, _data=None):
        """
        Verifies the memory usage on controller. \n

        :param _data:   Address of substation to verify status for.
        :type _data:    StatusParser
        """
        # Get the current memory usage from controller
        try:
            data = _data if _data else self.get_data()
            self.mu = float(data.get_value_string_by_key(ControllerCommands.Attributes.MEMORY_USAGE))
        except Exception:
            e_msg = "Getting controller memory usage command failed"
            raise Exception(e_msg)
        print "Verified Controller {0}'s 'Memory Usage': '{1}' of 100 percent.".format(
            str(self.mac),  # {0}
            self.mu  # {1}
        )

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Controller'. \n

        :param _expected_status:    Expected status to compare against. \n
        :type _expected_status:     str \n
        """
        # Get all information about the device from the controller.
        _data = self.get_data()

        # Verify base attributes
        # self.verify_date_and_time()  # verify date time object against current date time of the controller
        self.verify_description()

        self.verify_serial_number()
        self.verify_latitude()
        self.verify_longitude()

        if _expected_status is not None:
            self.verify_status(_expected_status=_expected_status)

        # Verify 'Controller' specific attributes
        self.verify_code_version()
        self.verify_rain_jumper_state(_data=_data)

        # Verify 3200 specific attributes
        self.verify_flow_jumper_state(_data=_data)
        self.verify_pause_jumper_state(_data=_data)

    #################################
    def verify_full_configuration(self):
        """
        This goes through each object in the list and sorts them by address
        then it verifies all attributes of the object against what the controller thinks
        :return:
        :rtype:
        """

        list_of_object_pointers = [
            self.zones,
            self.moisture_sensors,
            self.master_valves,
            self.temperature_sensors,
            self.flow_meters,
            self.event_switches,
            self.programs,
            self.mainlines,
            self.points_of_control,
            self.pressure_sensors,
            self.pumps,
            self.alert_relays,
            self.water_sources
        ]

        for each_object in list_of_object_pointers:
            for _ad in sorted(each_object.keys()):
                if each_object in [self.water_sources]:
                    self.get_water_source(_ad).verify_who_i_am()
                elif each_object in [self.points_of_control]:
                    self.get_point_of_control(_ad).verify_who_i_am()
                elif each_object in [self.mainlines]:
                    self.get_mainline(_ad).verify_who_i_am()
                else:
                    each_object[_ad].verify_who_i_am()

        self.verify_who_i_am()
    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Super classes are below                                                                                          #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################
    #################################
    def do_increment_clock(self, hours=0, minutes=0, seconds=0, update_date_mngr=True, clock_format='%H:%M'):
        """
        compare each value of hours, minutes, seconds \n
        this allows to send in one of the parameter to increment the clock in the controller \n
        the clock must be stopped in order it increment it

        :param hours:               Amount of hours to increment. \n
        :type hours:                int
        :param minutes:             Amount of minutes to increment. \n
        :type minutes:              int
        :param seconds:             Amount of seconds to increment. \n
        :type seconds:              int
        :param update_date_mngr:    If we also want to update the date manager value (VERY rare case where we don't
                                    want to). \n
        :type update_date_mngr:     bool
        :return:            ok from controller
        :rtype:             ok is a str

        """

        super(BaseStation3200, self).do_increment_clock(hours=hours,
                                                        minutes=minutes,
                                                        seconds=seconds,
                                                        update_date_mngr=update_date_mngr,
                                                        clock_format=clock_format)

    ################################
    def do_firmware_update(self,
                           were_from,
                           bm_id_number=None,
                           directory=None,
                           file_name=None,
                           maxlinelen=1000,
                           file_type=opcodes.zip_file,
                           unit_testing=False):
        """
        :param were_from:       This is the location of the firmware to update either from BaseManager or USB Storage
                                device (BaseManager, USB, local directory)
        :type were_from:        str
        :param bm_id_number:    This is the database id from the server where the firmware is located
        :type bm_id_number:     int
        :param directory:       Directory to grab firmware from "common/firmware_update_files/test.zip"
        :type directory:        str
        :type bm_id_number:     BaseManager database ID number of the firmware update
        :type bm_id_number:     int
        :param file_name:       This the the file name located on the USB
        :type file_name:        str
        :param maxlinelen:      The length of each packet (besides last one) from splitting up the firmware file we send
                                to the 3200.
        :type maxlinelen:       int
        :param file_type:       The file type that your firmware file is in. Usually '.zip'.
        :type file_type:        str
        :param unit_testing:    Parameter to make unit testing faster by ignoring sleeps
        :type unit_testing:     bool
        """
        super(BaseStation3200, self).do_firmware_update(
                           were_from=were_from,
                           bm_id_number=bm_id_number,
                           directory=directory,
                           file_name=file_name,
                           maxlinelen=maxlinelen,
                           file_type=file_type)

    #############################
    def do_reboot_controller(self, _version_with_ok_response=opcodes.new_reboot_version_32,
                             _version_with_response_after_reboot=opcodes.new_version_with_ok_after_reboot_32):
        """
        Reboot a 3200 controller.

        :param _version_with_ok_response:   This is the 3200 version where it responds to a reboot command with
                                            and 'OK' instead of rebooting immediately with no response.
        :param _version_with_response_after_reboot: This is the 3200 version where it responds with an 'OK'
                                                    after it comes alive after a reboot.
        :return:
        """
        super(BaseStation3200, self).do_reboot_controller(
            _version_with_ok_response=_version_with_ok_response,
            _version_with_response_after_reboot=_version_with_response_after_reboot
        )

    #############################
    def wait_for_controller_after_reboot(self, _version_with_response_after_reboot=None, unit_testing=False):
        """
        Waiting for a controller to wake up after a reboot.

        :param _version_with_response_after_reboot: This is the 3200 version where it responds with an 'OK'
                                                    after it comes alive after a reboot.
        """
        super(BaseStation3200, self).wait_for_controller_after_reboot(
            _version_with_response_after_reboot=opcodes.new_version_with_ok_after_reboot_32,
            unit_testing=False
        )

    #############################
    def set_controller_to_off(self, command=''):
        command = 'KEY,DL=1'
        super(BaseStation3200, self).set_controller_to_off(command=command)

    #############################
    def set_controller_to_run(self, command=''):
        command = 'KEY,DL=15'
        super(BaseStation3200, self).set_controller_to_run(command=command)

