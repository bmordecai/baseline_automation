import bu_imports
import packet
import bu_helpers
from common.objects.base_classes import devices
from common.objects.programming import cn

__author__ = 'bens'


# ######################################################################################################################
# BaseUnit CLASS
# ######################################################################################################################
class BaseUnit(cn.Controller):
    """
    BaseUnit Object \n

    Methods to implement:
    1. Reboot BaseUnit Command
    --> Done 2. ReadBaseUnitVersion
    --> Done 3. PowerUpTwoWire
    --> Done 4. ReadPowerBaseUnit
    5. ResetRemoteDevice - Stubbed
    6. ReadSerialNumber - Stubbed
    7. UnlockSerialNumber - stubbed
    8. ReadRemoteVersionOEM - stubbed
    --> VB Command 9. WriteAddrChkSN
    10. TestTwoWireDrop - stubbed
    11. TestBaseChannel - stubbed
    12. TestRemoteChannel - stubbed
    13. SleepSerialNumber - stubbed
    -- Duplicate 14. ReadSerialNumber
    15. TurnValveOff - stubbed
    16. TurnValveOn - stubbed
    17. TurnAllValvesOff - stubbed
    18. ReadRemotePower - stubbed
    19. WriteTempAddress - stubbed
    """

    def __init__(self, _ser):
        """
        :param _ser: Serial Instance \n
        :type _ser:  serial.Ser \n
        """
        # super(BaseUnit).__init__(0.0, "12345678901234", "BaseUnit")
        cn.Controller.__init__(self, _type=0.0, _mac="12345678901234", _description="BaseUnit")

        self.ser = _ser

        # Base Unit attributes
        self.two_wire_operating_mode = None     # Advanced2Wire (data0)
        self.two_wire_powered_up = None         # Power2Wire (data1)
        self.two_wire_checksum_errors = None    # CkhSumCnt2Wire (data2)
        self.two_wire_frame_errors = None       # FrameCnt2Wire (data3)
        self.two_wire_collisions = None         # CollisionCnt2Wire (data4)
        self.pc_checksum_errors = None          # ChkSumSerialCnt (data5)
        self.pc_frame_errors = None             # FrameCntSerial (data6)
        self.pc_illegal_commands = None         # IllegalCmdCnt (data7)
        self.pre_comp_value = None              # PreComp (data8)

        # Additional BU Attributes
        self.current_a = 0.0
        self.current_b = 0.0
        self.voltage_a = 0.0
        self.voltage_b = 0.0

        self.data_string = None

        self.check_sum_hi = None
        self.check_sum_lo = None
        self.two_wire_check_sum_hi = None
        self.two_wire_check_sum_lo = None
        self.bit_time_base_long_hi = None
        self.bit_time_base_long_lo = None
        self.bit_time_base_short_hi = None
        self.bit_time_base_short_lo = None
        self.bit_time_remote_long_hi = None
        self.bit_time_remote_long_lo = None
        self.bit_time_remote_short_hi = None
        self.bit_time_remote_short_lo = None

        # Todo: Testing device has high bitskews so changed margin to .1 to pass the test
        # self.bit_time_margin = 0.08
        self.bit_time_margin = .1
        self.bit_time_scale_factor = 2.5
        self.decoder_bit_skew_scale = 3.0

        self.r_decoder_category = None
        self.r_temp_sn = None

        self.bit0 = None
        self.bit1 = None
        self.bit2 = None
        self.bit3 = None
        self.bit4 = None
        self.bit5 = None

        # User Input variables
        self.ui_temp_remote_sn = None  # Serial Number specified by user from UI
        self.ui_company_name = None
        self.ui_decoder_category = None


        # TYPE:         ty
        # VERSION:      vr
        # STATUS:       SS
        # self.type_minor = None
        # self.type_major = None
        # self.version_minor = None
        # self.version_major = None
        # self.status = None


        # self.base_sn = None         # Base Unit Serial Number

        # Four serial number places holders for bu
        # self.ser_num_1 = None
        # self.ser_num_2 = None
        # self.ser_num_3 = None
        # self.ser_num_4 = None

        # Remote device variables
        # self.r_temp_sn = None  # Actual Serial Number for the remote device in decimal
        # self.r_final_sn = [
        #     # r_final_sn[0] = Actual SN for all decoders 1, 2, and 4 valves,
        #     # r_final_sn[1] = SN for twelve valve decoder,
        #     # r_final_sn[2] = SN for 24 valve decoder
        # ]
        # self.r_type_minor = None
        # self.r_type_major = None
        # self.r_version_minor = None
        # self.r_version_major = None
        # self.r_current_hi = None
        # self.r_current_lo = None
        # self.r_voltage_hi = None
        # self.r_voltage_lo = None
        # self.r_status = None

    def __getattr__(self, name):
        """
        Python built-in: Called when you attempt access an objects attributes, ie: zones.sn (trying to access zone
        serial number)
        """
        if self is not None:
            return getattr(self, name)
        else:
            raise AttributeError("Unknown Attribute '" + name + "'")

    def __str__(self):
        """
        Returns string representation of this Controller Object. \n
        :return:    String representation of this Controller Object
        :rtype:     str
        """
        return_str = ""
        return return_str

    def compute_current_a(self, current_a_lo):
        """
        Computes the current from a raw byte value received through a BaseUnit packet and returns as a string with
        up to 6 decimal places. \n
        """
        current = ((float(226 - current_a_lo)) / 256.0) * 2.5
        current_text = float('{:.4f}'.format(current))
        return current_text

    def compute_current_b(self, current_b_lo):
        """
        Computes the current from a raw byte value received through a BaseUnit packet and returns as a string with
        up to 6 decimal places. \n
        """
        current = ((float(226 - current_b_lo)) / 256.0) * 2.5
        current_text = float('{:.4f}'.format(current))
        return current_text

    def compute_voltage_a(self, version, voltage_a_lo):
        """
        Computes the voltage from a raw byte value received through a BaseUnit packet and returns as a string with
        up to 2 decimal places \n
        :rtype: float
        """
        if float(version) > 1.1:
            voltage = ((float(voltage_a_lo) / 256.0) * 5) * (17.0 / 2.0) - 0.6
        else:
            voltage = ((float(voltage_a_lo) / 8.0) * 0.95) - 0.6

        if voltage < 0:
            voltage = 0

        # returns voltage with up to two decimals of precision
        return float('{:.3f}'.format(voltage))

    def compute_voltage_b(self, version, voltage_b_lo):
        """
        Computes the voltage from a raw byte value received through a BaseUnit packet and returns as a string with
        up to 2 decimal places \n
        """
        if float(version) > 1.1:
            voltage = ((float(voltage_b_lo) / 256.0) * 5) * (17.0 / 2.0) - 0.6
        else:
            voltage = ((float(voltage_b_lo) / 8.0) * 0.95) - 0.6

        if voltage < 0:
            voltage = 0

        # returns voltage with up to two decimals of precision
        return float('{:.3f}'.format(voltage))

    def get_version_text(self, v_major, v_minor):
        return str(v_major) + "." + str(v_minor)

    def get_type_text(self, t_major, t_minor):
        return str(t_major) + "." + str(t_minor)

    def get_bit_time_base(self, _limits=None):
        """
        :type _limits: dict
        """
        if _limits is None:
            return "{0}/{1},{2}/{3}"\
                .format(self.bit_time_base_long_hi,     # 0
                        self.bit_time_base_long_lo,     # 1
                        self.bit_time_base_short_hi,    # 2
                        self.bit_time_base_short_lo)    # 3
        else:
            return "{0},{1}"\
                .format(_limits["BitTime"]["BaseLong"],     # 0
                        _limits["BitTime"]["BaseShort"])    # 1

    def get_bit_time_remote(self, _limits=None):
        """
        :type _limits: dict
        """
        if _limits is None:
            return "{0}/{1},{2}/{3}"\
                .format(self.bit_time_remote_long_hi,     # 0
                        self.bit_time_remote_long_lo,     # 1
                        self.bit_time_remote_short_hi,    # 2
                        self.bit_time_remote_short_lo)    # 3
        else:
            return "{0},{1}"\
                .format(_limits["BitTime"]["RemoteLong"],     # 0
                        _limits["BitTime"]["RemoteShort"])    # 1

    def get_bit_times(self, _limits=None):
        """
        :type _limits: dict
        """
        if _limits is None:
            return "{0},{1}"\
                .format(self.get_bit_time_base(),     # 0
                        self.get_bit_time_remote())   # 1
        else:
            return "{0},{1}"\
                .format(self.get_bit_time_base(_limits),     # 0
                        self.get_bit_time_remote(_limits))   # 1

    # TODO: not used?
    # def get_remote_sn_text(self):
    #     """
    #     Returns the 7 character string representing the remote device's serial number
    #     :return:
    #     :rtype:
    #     """
    #     return hex(self.r_temp_sn)[2:].upper()
    #
    # def get_bu_sn_text(self):
    #     """
    #     Returns the 7 character string representing the base unit's serial number
    #     :return:
    #     :rtype:
    #     """
    #     return hex(self.base_sn)[2:].upper()

    # ########################################################
    # ################ Base Unit Commands ####################
    # ########################################################

    def power_up_2_wire(self):
        """
        Turn on the 2-wire network and return the current readings over the network. \n
        """
        # packet for sending
        s_packet = packet.BUSerialPacket(_address_base=self.ad,
                                         _command=bu_imports.BaseUnitCommands.POWER_UP_2WIRE)

        try:
            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=s_packet)
        except Exception as e:
            self.print_command_error(name_of_command='PowerUp2Wire', caught_exception_msg=e.message)

        else:
            # did we receive a reply?
            if r_packet:
                self.data_string = r_packet.data_string
                self.current_b = self.compute_current_b(current_b_lo=r_packet.data0)
                self.current_a = self.compute_current_a(current_a_lo=r_packet.data2)
                self.voltage_b = self.compute_voltage_b(version=self.vr, voltage_b_lo=r_packet.data6)
                self.voltage_a = self.compute_voltage_a(version=self.vr, voltage_a_lo=r_packet.data7)
                self.ss = r_packet.status
                # self.current_a_total = r_packet.data3 * 256.0 + r_packet.data2
                # self.current_b_total = r_packet.data1 * 256.0 + r_packet.data0
                # self.voltage_a_total = r_packet.data5 * 256.0 + r_packet.data4
                # self.voltage_b_total = r_packet.data7 * 256.0 + r_packet.data6

    def read_version(self):
        """
        Get the current version. \n
        """
        # packet for sending
        s_packet = packet.BUSerialPacket(_address_base=self.ad,
                                         _command=bu_imports.BaseUnitCommands.READ_VERSION)

        try:
            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=s_packet)
        except Exception as e:
            self.print_command_error(name_of_command='ReadVersion', caught_exception_msg=e.message)
        else:
            # did we receive a reply?
            if r_packet:
                self.data_string = r_packet.data_string
                self.ty = self.get_type_text(t_major=r_packet.data3, t_minor=r_packet.data2)
                self.vr = self.get_version_text(v_major=r_packet.data5, v_minor=r_packet.data4)
                self.ss = r_packet.status

    def read_power(self):
        """
        Get the last readings for voltage and current on the two-wire network. \n
        """
        # packet for sending
        s_packet = packet.BUSerialPacket(_address_base=self.ad,
                                         _command=bu_imports.BaseUnitCommands.READ_POWER)

        try:
            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=s_packet)
        except Exception as e:
            self.print_command_error('ReadPower', caught_exception_msg=e.message)
        else:
            # did we receive a reply?
            if r_packet:
                self.data_string = r_packet.data_string
                self.current_b = self.compute_current_b(current_b_lo=r_packet.data0)
                self.current_a = self.compute_current_a(current_a_lo=r_packet.data2)
                self.voltage_b = self.compute_voltage_b(version=self.vr, voltage_b_lo=r_packet.data6)
                self.voltage_a = self.compute_voltage_a(version=self.vr, voltage_a_lo=r_packet.data7)
                self.ss = r_packet.status
                # self.current_b_lo = r_packet.data0
                # self.current_b_hi = r_packet.data1
                # self.current_a_lo = r_packet.data2
                # self.current_a_hi = r_packet.data3
                # self.voltage_a_lo = r_packet.data4
                # self.voltage_a_hi = r_packet.data5
                # self.voltage_b_lo = r_packet.data6
                # self.voltage_b_hi = r_packet.data7
                # self.status = r_packet.status
                #
                # self.current_a_total = r_packet.data3 * 256.0 + r_packet.data2
                # self.current_b_total = r_packet.data1 * 256.0 + r_packet.data0
                # self.voltage_a_total = r_packet.data5 * 256.0 + r_packet.data4
                # self.voltage_b_total = r_packet.data7 * 256.0 + r_packet.data6

    def reboot(self, _base_address=0):
        """
        Reboot the base unit with the specified address
        :param _base_address: BaseUnit Address
        :type _base_address: int
        """
        # packet for sending
        s_packet = packet.BUSerialPacket(_address_base=_base_address,
                                         _command=bu_imports.BaseUnitCommands.RE_BOOT)
        try:
            self.ser.bu_send_with_no_reply(new_packet=s_packet)
        except Exception as e:
            raise Exception("Exception occurred sending reboot command to BaseUnit: " + str(e.message))

    def test_two_wire_drop(self, _remote_device):
        """
        Returns two wire drop value

        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=_remote_device.ad,
                                                 # _remote_command=bu_imports.RemoteCommands.GET_REMOTE_STATUS)
                                                 _remote_command=bu_imports.RemoteCommands.ECHO_PACKET)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.TEST_TWO_WIRE_DROP)

        # Merge two-wire packet into the bu_packet to send
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        try:
            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=bu_packet)
        except Exception as e:
            self.print_command_error(name_of_command='TestTwoWireDrop', caught_exception_msg=e.message)

        else:
            # did we receive a reply?
            if r_packet:
                self.data_string = r_packet.data_string
                _returned_current = self.compute_current_a(current_a_lo=r_packet.data2)
                _remote_device.vt = self.compute_voltage_a(version=self.vr, voltage_a_lo=r_packet.data4)
                self.ss = r_packet.status
                self.check_sum_hi = r_packet.check_sum_hi
                self.check_sum_lo = r_packet.check_sum_lo

                return _returned_current

    def test_base_channel(self, _remote_device, _pattern=7):
        """
        Returns BitSkews for Base Unit

        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices

        :param _bit_count: array to hold the returned bit skew values returned from remote_dev
        :type _bit_count: list

        :param _pattern: Bit pattern
        :type _pattern: int
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=_remote_device.ad,
                                                 _remote_command=bu_imports.RemoteCommands.ECHO_PACKET)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.TEST_BASE_CHANNEL)

        # Merge two-wire packet into the bu_packet to send
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        # Include pattern in packet
        bu_packet.data3 = _pattern

        try:
            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=bu_packet)
        except Exception as e:
            self.print_command_error(name_of_command='TestBaseChannel', caught_exception_msg=e.message)

        else:
            # did we receive a reply?
            if r_packet:
                self.data_string = r_packet.data_string
                self.bit0 = r_packet.data1
                self.bit1 = r_packet.data2
                self.bit2 = r_packet.data3
                self.bit3 = r_packet.data4
                self.bit4 = r_packet.data5
                self.bit5 = r_packet.data6
                self.two_wire_check_sum_lo = r_packet.data7
                self.two_wire_check_sum_hi = r_packet.data8
                self.ss = r_packet.status
                self.check_sum_lo = r_packet.check_sum_lo
                self.check_sum_hi = r_packet.check_sum_hi

    # ########################################################
    # ############ Remote Device Commands ####################
    # ########################################################

    def r_write_serial_number(self, _remote_device, _temp_sn=None):
        """
        Writes the serial number to the remote unit that has been previously unlocked. The remote unit returns it's
        unit address, or in the case of a multi-address device, the address corresponding to the specified serial number

        :param _remote_device: Remote device
        :type _remote_device: devices.BaseDevices

        :param _temp_sn: New serial number to write to device
        :type _temp_sn: str
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=bu_imports.BaseUnitConstants.GLOBAL_ADDRESS,
                                                 _remote_command=bu_imports.RemoteCommands.WRITE_SERIAL_NUMBER)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Insert two-wire packet information
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        # Take serial number
        if _temp_sn is not None:
            _sn = _temp_sn
        else:
            _sn = _remote_device.sn
        bu_helpers.put_sn_into_bu_serial_packet(sn=_sn, bu_serial_packet=bu_packet)

        try:

            # send packet and get reply
            r_packet = self.ser.bu_send_with_no_reply(new_packet=bu_packet)

        except Exception as e:
            self.print_command_error(name_of_command='SleepSerialNumber', to='RemoteUnit',
                                     caught_exception_msg=e.message)

        else:
            if r_packet:
                _remote_device.ss = r_packet.data6
                self.ss = r_packet.status

    def r_test_remote_channel(self, _remote_device, _pattern=7):
        """
        Returns BitSkews for remote device

        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices

        :param _pattern:
        :type _pattern: int
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=_remote_device.ad,
                                                 _remote_command=bu_imports.RemoteCommands.TEST_REMOTE_CHANNEL)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Merge two-wire packet into the bu_packet to send
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        # Include pattern in packet
        bu_packet.data3 = _pattern

        try:
            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=bu_packet)
        except Exception as e:
            self.print_command_error(name_of_command='TestRemoteChannel', to='RemoteUnit',
                                     caught_exception_msg=e.message)

        else:
            # did we receive a reply?
            if r_packet:
                self.data_string = r_packet.data_string
                self.bit0 = r_packet.data1
                self.bit1 = r_packet.data2
                self.bit2 = r_packet.data3
                self.bit3 = r_packet.data4
                self.bit4 = r_packet.data5
                self.bit5 = r_packet.data6
                self.two_wire_check_sum_lo = r_packet.data7
                self.two_wire_check_sum_hi = r_packet.data8
                self.ss = r_packet.status
                self.check_sum_lo = r_packet.check_sum_lo
                self.check_sum_hi = r_packet.check_sum_hi

    def r_reset_remote_device(self, _remote_device):
        """
        Resets two-wire device connected to BaseUnit

        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=_remote_device.ad,
                                                 _remote_command=bu_imports.RemoteCommands.RESET_REMOTE_DEVICE)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Insert two-wire packet information
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        try:
            # Send and don't wait for reply
            self.ser.bu_send_with_no_reply(new_packet=bu_packet)
        except Exception as e:
            self.print_command_error(name_of_command='ResetRemoteDevice', to='RemoteUnit',
                                     caught_exception_msg=e.message)

    def r_read_serial_number(self, _remote_device, _test_sn=False, _temp_addr=0):
        """
        Reads and saves two_wire device's serial number

        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices

        :param _test_sn: boolean that, if True, will return the serial number that was returned from remote device
        :type _test_sn: bool

        :param _temp_addr: if this variable is changed at the call of the method, this address is used in the call to
                            the remote device
        :type _temp_addr: int

        :return: Returns the serial number returned from remote device if _test_sn is True, else returns None
        """
        return_sn = None

        # two-wire packet for sending
        if _temp_addr == 0:
            two_wire_packet = packet.BUTwoWirePacket(_remote_address=_remote_device.ad,
                                                     _remote_command=bu_imports.RemoteCommands.READ_SERIAL_NUMBER)
        else:
            two_wire_packet = packet.BUTwoWirePacket(_remote_address=_temp_addr,
                                                     _remote_command=bu_imports.RemoteCommands.READ_SERIAL_NUMBER)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Merge two-wire packet into the bu_packet to send
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        try:
            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=bu_packet)
        except Exception as e:
            self.print_command_error(name_of_command='ReadSerialNumber', to='RemoteUnit',
                                     caught_exception_msg=e.message)
        else:
            # did we receive a reply?
            if r_packet:
                self.data_string = r_packet.data_string
                # remote unit status
                _remote_device.ss = r_packet.data6
                # base unit status
                self.ss = r_packet.status

                self.check_sum_hi = r_packet.check_sum_hi
                self.check_sum_lo = r_packet.check_sum_lo
                self.two_wire_check_sum_hi = r_packet.data7
                self.two_wire_check_sum_lo = r_packet.data8

                if _test_sn:
                    return_sn = bu_helpers.get_sn_from_packet_data(bu_serial_packet=r_packet)
                    return return_sn
                else:
                    _remote_device.final_sn[0] = bu_helpers.get_sn_from_packet_data(bu_serial_packet=r_packet)

                # self.r_status = r_packet.data6
                # self.status = r_packet.status

    def r_unlock_remote_global(self, _remote_device):
        """
        Unlocks the remote unit for programming (global)

        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=bu_imports.BaseUnitConstants.GLOBAL_ADDRESS,
                                                 _remote_command=bu_imports.RemoteCommands.UNLOCK_REMOTE)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Insert two-wire packet information
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        try:

            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=bu_packet)

        except Exception as e:
            self.print_command_error(name_of_command='UnlockRemoteGlobal', to='RemoteUnit',
                                     caught_exception_msg=e.message)

        else:
            # did we receive a reply?
            if r_packet:
                self.data_string = r_packet.data_string
                _remote_device.ad = r_packet.data1
                _remote_device.vr = self.get_version_text(v_minor=r_packet.data2, v_major=r_packet.data3)
                _remote_device.ty = self.get_type_text(t_minor=r_packet.data4, t_major=r_packet.data5)
                _remote_device.ss = r_packet.data6
                # self.r_version_minor = r_packet.data2
                # self.r_version_major = r_packet.data3
                # self.r_type_minor = r_packet.data4
                # self.r_type_major = r_packet.data5
                # self.r_status = r_packet.data6
                self.two_wire_check_sum_lo = r_packet.data7
                self.two_wire_check_sum_hi = r_packet.data8
                self.ss = r_packet.status
                self.check_sum_hi = r_packet.check_sum_hi
                self.check_sum_lo = r_packet.check_sum_lo

    def r_unlock_serial_number(self, _remote_device, _temp_sn=None):
        """
        Unlocks serial number of remote device to be editable
        Returns remote device serial number
        Returns type and version of remote device
        ** For BaseLine decoders

        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices

        :param _temp_sn: New serial number to write to remote unit
        :type _temp_sn: str

        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=bu_imports.BaseUnitConstants.GLOBAL_ADDRESS,
                                                 _remote_command=bu_imports.RemoteCommands.UNLOCK_SERIAL_NUMBER)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Merge two-wire packet into the bu_packet to send
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        # If user specified new serial number
        if _temp_sn:
            _sn = _temp_sn
        else:
            _sn = _remote_device.final_sn[0]

        # Insert serial number into packet arguments, this converts the serial from 7-char string to 4 8-bit ints
        bu_helpers.put_sn_into_bu_serial_packet(sn=_sn, bu_serial_packet=bu_packet)

        try:
            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=bu_packet)

        except Exception as e:
            self.print_command_error(name_of_command='UnlockSerialNumber', to='RemoteUnit',
                                     caught_exception_msg=e.message)

        else:
            # did we receive a reply?
            if r_packet:
                self.data_string = r_packet.data_string
                _remote_device.vr = self.get_version_text(v_minor=r_packet.data2, v_major=r_packet.data3)
                _remote_device.ty = self.get_type_text(t_minor=r_packet.data4, t_major=r_packet.data5)
                _remote_device.ss = r_packet.data6
                # self.r_version_minor = r_packet.data2
                # self.r_version_major = r_packet.data3
                # self.r_type_minor = r_packet.data4
                # self.r_type_major = r_packet.data5
                # self.r_status = r_packet.data6
                self.two_wire_check_sum_hi = r_packet.data7
                self.two_wire_check_sum_lo = r_packet.data8
                self.ss = r_packet.status
                self.check_sum_hi = r_packet.check_sum_hi
                self.check_sum_lo = r_packet.check_sum_lo

    def r_read_remote_version_oem(self, _remote_device):
        """
        Returns remote device serial number
        Returns type and version of remote device
        ** For other than BaseLine decoders

        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=_remote_device,
                                                 _remote_command=bu_imports.RemoteCommands.READ_REMOTE_VERSION)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Insert two-wire packet information
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        try:

            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=bu_packet)

        except Exception as e:
            self.print_command_error(name_of_command='ReadRemoteVersion', to='RemoteUnit',
                                     caught_exception_msg=e.message)

        else:
            # did we receive a reply?
            if r_packet:
                self.data_string = r_packet.data_string
                _remote_device.vr = self.get_version_text(v_minor=r_packet.data2, v_major=r_packet.data3)
                _remote_device.ty = self.get_type_text(t_minor=r_packet.data4, t_major=r_packet.data5)
                _remote_device.ss = r_packet.data6
                # self.r_version_minor = r_packet.data2
                # self.r_version_major = r_packet.data3
                # self.r_type_minor = r_packet.data4
                # self.r_type_major = r_packet.data5
                # self.r_status = r_packet.data6
                self.two_wire_check_sum_lo = r_packet.data7
                self.two_wire_check_sum_hi = r_packet.data8
                self.ss = r_packet.status
                self.check_sum_hi = r_packet.check_sum_hi
                self.check_sum_lo = r_packet.check_sum_lo

    def r_write_address(self, _remote_device, _new_address):
        """
        Writes address to the remote unit that had been previously unlocked

        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices

        :param _new_address: New address to assign to remote unit passed in
        :type _new_address: int
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=bu_imports.BaseUnitConstants.GLOBAL_ADDRESS,
                                                 _remote_command=bu_imports.RemoteCommands.WRITE_ADDRESS)

        # Insert new address for remote device
        two_wire_packet.arg0 = _new_address

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Insert two-wire packet information
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        try:

            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=bu_packet)

        except Exception as e:
            self.print_command_error(name_of_command='WriteAddress', to='RemoteUnit',
                                     caught_exception_msg=e.message)

        else:
            # did we receive a reply?
            if r_packet.status == bu_imports.BaseErrors.STATUS_OK:
                self.data_string = r_packet.data_string
                _remote_device.vr = self.get_version_text(v_minor=r_packet.data2, v_major=r_packet.data3)
                _remote_device.ty = self.get_type_text(t_minor=r_packet.data4, t_major=r_packet.data5)
                _remote_device.ss = r_packet.data6
                # self.r_version_minor = r_packet.data2
                # self.r_version_major = r_packet.data3
                # self.r_type_minor = r_packet.data4
                # self.r_type_major = r_packet.data5
                # self.r_status = r_packet.data6
                self.two_wire_check_sum_lo = r_packet.data7
                self.two_wire_check_sum_hi = r_packet.data8
                self.ss = r_packet.status
                self.check_sum_hi = r_packet.check_sum_hi
                self.check_sum_lo = r_packet.check_sum_lo

    def r_sleep_serial_number(self, _remote_device, _ser_num_index):
        """
        Sleeps the remote device passed in

        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=bu_imports.BaseUnitConstants.GLOBAL_ADDRESS,
                                                 _remote_command=bu_imports.RemoteCommands.SLEEP_SERIAL_NUMBER)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Insert two-wire packet information
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        # Take serial number
        bu_helpers.put_sn_into_bu_serial_packet(sn=_remote_device.final_sn[_ser_num_index], bu_serial_packet=bu_packet)

        try:

            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=bu_packet)

        except Exception as e:
            self.print_command_error(name_of_command='SleepSerialNumber', to='RemoteUnit',
                                     caught_exception_msg=e.message)

        else:
            # did we receive a reply?
            if r_packet.status == bu_imports.BaseErrors.STATUS_OK:
                self.data_string = r_packet.data_string
                _remote_device.ad = r_packet.data1
                _remote_device.vr = self.get_version_text(v_minor=r_packet.data2, v_major=r_packet.data3)
                _remote_device.ty = self.get_type_text(t_minor=r_packet.data4, t_major=r_packet.data5)
                _remote_device.ss = r_packet.data6
                # self.r_version_minor = r_packet.data2
                # self.r_version_major = r_packet.data3
                # self.r_type_minor = r_packet.data4
                # self.r_type_major = r_packet.data5
                # self.r_status = r_packet.data6
                self.two_wire_check_sum_lo = r_packet.data7
                self.two_wire_check_sum_hi = r_packet.data8
                self.ss = r_packet.status
                self.check_sum_hi = r_packet.check_sum_hi
                self.check_sum_lo = r_packet.check_sum_lo

    def r_turn_valve_on(self, _remote_device, _valve_number):
        """
        Turns specified valve on

        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices

        :param _valve_number: Zone/Valve number
        :type _valve_number: int
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=_valve_number,
                                                 _remote_command=bu_imports.RemoteCommands.TURN_VALVE_ON)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Insert two-wire packet information
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        try:

            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=bu_packet)

        except Exception as e:
            self.print_command_error(name_of_command='TurnValveOn', to='RemoteUnit',
                                     caught_exception_msg=e.message)

        else:
            # did we receive a reply?
            if r_packet.status == bu_imports.BaseErrors.STATUS_OK:
                self.data_string = r_packet.data_string
                _remote_device.ad = r_packet.data1
                _remote_device.r_va = self.compute_current_a(current_a_lo=r_packet.data2)
                _remote_device.r_vv = self.compute_voltage_a(version=_remote_device.vr, voltage_a_lo=r_packet.data4)
                _remote_device.ss = r_packet.data6
                # self.r_current_lo = r_packet.data2
                # self.r_current_hi = r_packet.data3
                # self.r_voltage_lo = r_packet.data4
                # self.r_voltage_hi = r_packet.data5
                # self.r_status = r_packet.data6
                self.two_wire_check_sum_lo = r_packet.data7
                self.two_wire_check_sum_hi = r_packet.data8
                self.ss = r_packet.status
                self.check_sum_hi = r_packet.check_sum_hi
                self.check_sum_lo = r_packet.check_sum_lo

    def r_turn_valve_off(self, _remote_device, _valve_number=1):
        """
        Turns specified valve off

        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices

        :param _valve_number: Zone/Valve number
        :type _valve_number: int
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=_remote_device.ad,
                                                 _remote_command=bu_imports.RemoteCommands.TURN_VALVE_OFF)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Insert two-wire packet information
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        try:

            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=bu_packet)

        except Exception as e:
            self.print_command_error(name_of_command='TurnValveOff', to='RemoteUnit',
                                     caught_exception_msg=e.message)

        else:
            # did we receive a reply?
            if r_packet.status == bu_imports.BaseErrors.STATUS_OK:
                self.data_string = r_packet.data_string
                _remote_device.ad = r_packet.data1
                _remote_device.r_va = self.compute_current_a(current_a_lo=r_packet.data2)
                _remote_device.r_vv = self.compute_voltage_a(version=_remote_device.vr, voltage_a_lo=r_packet.data4)
                _remote_device.ss = r_packet.data6
                # self.r_current_lo = r_packet.data2
                # self.r_current_hi = r_packet.data3
                # self.r_voltage_lo = r_packet.data4
                # self.r_voltage_hi = r_packet.data5
                # self.r_status = r_packet.data6
                self.two_wire_check_sum_lo = r_packet.data7
                self.two_wire_check_sum_hi = r_packet.data8
                self.ss = r_packet.status
                self.check_sum_hi = r_packet.check_sum_hi
                self.check_sum_lo = r_packet.check_sum_lo

    def r_turn_all_valves_off(self, _remote_device):
        """
        Turns all valves off


        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=_remote_device.ad,
                                                 _remote_command=bu_imports.RemoteCommands.TURN_ALL_VALVES_OFF)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Insert two-wire packet information
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        try:

            # send packet and continue
            self.ser.bu_send_with_no_reply(new_packet=bu_packet)

        except Exception as e:
            self.print_command_error(name_of_command='TurnAllValvesOff', to='RemoteUnit',
                                     caught_exception_msg=e.message)

    def r_read_remote_power(self, _remote_device, _valve_number=1):
        """
        Reads current and voltage readings from specified zone/valve

        :param _remote_device: Remote object instance to reference
        :type _remote_device: devices.BaseDevices

        :param _valve_number: Zone/Valve number
        :type _valve_number: int
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=_valve_number,
                                                 _remote_command=bu_imports.RemoteCommands.READ_REMOTE_POWER)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Insert two-wire packet information
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        try:

            # send packet and get reply
            r_packet = self.ser.bu_send_and_wait_for_reply(new_packet=bu_packet)

        except Exception as e:
            self.print_command_error(name_of_command='ReadRemotePower', to='RemoteUnit',
                                     caught_exception_msg=e.message)

        else:
            # did we receive a reply?
            if r_packet.status == bu_imports.BaseErrors.STATUS_OK:
                self.data_string = r_packet.data_string
                _remote_device.ad = r_packet.data1
                _remote_device.r_va = self.compute_current_a(current_a_lo=r_packet.data2)
                _remote_device.r_vv = self.compute_voltage_a(version=_remote_device.vr, voltage_a_lo=r_packet.data4)
                _remote_device.ss = r_packet.data6
                # self.r_current_lo = r_packet.data2
                # self.r_current_hi = r_packet.data3
                # self.r_voltage_lo = r_packet.data4
                # self.r_voltage_hi = r_packet.data5
                # self.r_status = r_packet.data6
                self.two_wire_check_sum_lo = r_packet.data7
                self.two_wire_check_sum_hi = r_packet.data8
                self.ss = r_packet.status
                self.check_sum_hi = r_packet.check_sum_hi
                self.check_sum_lo = r_packet.check_sum_lo

    def r_write_temp_address(self, _remote_device):
        """
        Writes address stored in _temp_address variable to base unit

        :param _remote_device: remote object to reference
        :type _remote_device: devices.BaseDevices
        """
        # two-wire packet for sending
        two_wire_packet = packet.BUTwoWirePacket(_remote_address=_remote_device.ad,
                                                 _remote_command=bu_imports.RemoteCommands.WRITE_TEMP_ADDRESS)

        # BU Serial Packet to send to base unit
        bu_packet = packet.BUSerialPacket(_address_base=self.ad,
                                          _command=bu_imports.BaseUnitCommands.FORWARD_COMMAND)

        # Insert two-wire packet information
        bu_packet.populate_data_with_two_wire_packet(bu_two_wire_packet=two_wire_packet)

        try:

            # send packet and continue
            self.ser.bu_send_with_no_reply(new_packet=bu_packet)

        except Exception as e:
            self.print_command_error(name_of_command='WriteTempAddress', to='RemoteUnit',
                                     caught_exception_msg=e.message)

    def print_command_error(self, name_of_command='', to='BaseUnit', caught_exception_msg=None):
        e_str = "Exception occurred sending command '{cmd}' to '{to}'".format(cmd=name_of_command, to=to)
        raise Exception(e_str)