import warnings
from common.objects.base_classes.devices import BaseDevices
import time
from datetime import datetime, timedelta

from common.imports import opcodes
from common.variables.common import dictionary_for_status_codes
from common.objects.base_classes import messages
from common.objects.base_classes.devices import BaseDevices
from common.objects.base_classes.controller import BaseController
from common.imports.types import ControllerCommands, ActionCommands, BasemanagerCommands
from common.objects.statuses.controllers.bl_sb_statuses import SubStationStatuses
from common import helper_methods
from common.date_package.date_resource import date_mngr

__author__ = 'bens'


class Substation(BaseController):
    """
    Specify the types of bicoder dictionaries. You can really specify the type for each attribute for a Substation here
    if needed. But I figured these were the only attributes for now that type specification was necessary. That way 
    we can have access to bicoder methods in the use_case as so:
    
        - self.config.substations[1].valve_bicoders['TSD0001'].foo()
        
    :type valve_bicoders: dict[str, common.objects.substation.valve_bicoder.ValveBicoder]
    :type flow_bicoders: dict[str, common.objects.substation.flow_bicoder.FlowBicoder]
    :type pump_bicoders: dict[str, common.objects.substation.pump_bicoder.PumpBicoder]
    :type switch_bicoders: dict[str, common.objects.substation.switch_bicoder.SwitchBicoder]
    :type moisture_bicoders: dict[str, common.objects.substation.moisture_bicoder.MoistureBicoder]
    :type temperature_bicoders: dict[str, common.objects.substation.temp_bicoder.TempBicoder]

    :type ser: common.objects.base_classes.ser.Ser
    """

    #############################
    def __init__(self, _mac, _serial_port, _serial_number, _port_address, _socket_port, _ip_address,
                 _firmware_version=None, _description=''):
        """
        Initialize a controller instance with the specified parameters \n
        :param _mac:                        Mac address for controller. \n
        :type _mac:                         str \n
        :param _serial_port:                Serial port of the controller to communicate with \n
        :type _serial_port:                 common.objects.base_classes.ser.Ser \n
        :param _serial_number:              this is the serial number of the controller \n
        :type _serial_number:               str | None \n
        :param_cn_firmware_version:         this is the firmware version of the controller \n
        :type _firmware_version:            str | None \n
        :param _port_address:
        :type _port_address:
        :param _socket_port:
        :type _socket_port:
        :param _ip_address:
        :type _ip_address:
        :param _description:                Description to put on the controller. \n
        :type _description:                 str
        """

        et = 0.00  # Controller ETo Value
        ra = 0  # Controller Rain Value
        self.websocket = '{0},{1}' .format(_ip_address, _port_address)

        BaseController.__init__(self,
                                _type=ControllerCommands.Type.SUBSTATION,
                                _mac=_mac,
                                _serial_port=_serial_port,
                                _serial_number=_serial_number,
                                _ad=_ip_address,
                                _description=_description,
                                _firmware_version=_firmware_version
                                )

        self.la = 43.609768     # Latitude
        self.lg = -116.310569   # Longitude
        self.ip_address = _ip_address
        self.socket_port_address = _socket_port
        self.base_station_3200 = None
        """:type: common.objects.controllers.bl_32.BaseStation3200"""

        # Read Only Attributes
        self.ty = opcodes.substation    # substation Type
        self.controller_type = opcodes.substation   # substation Type (again for the sake of compatibility in messages
        self.ss = ''                    # Status

        # Substation BiCoder serial numbers assigned.
        self.d1_bicoders = list()        # Single valve
        self.d2_bicoders = list()        # Dual valve
        self.d4_bicoders = list()        # Quad valve
        self.dd_bicoders = list()        # Twelve valve
        self.ms_bicoders = list()        # Moisture bicoder serial numbers
        self.fm_bicoders = list()        # Flow bicoder serial numbers
        self.ts_bicoders = list()        # Temperature bicoder serial numbers
        self.sw_bicoders = list()        # Switch bicoder serial numbers

        # Substation BiCoder object dictionaries
        self.valve_bicoders = dict()        # Valve valve
        """:type: dict[str, common.objects.bicoders.valve_bicoder.ValveBicoder]"""
        self.analog_bicoders = dict()       # Analog bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.analog_bicoder.AnalogBicoder]"""
        self.flow_bicoders = dict()         # Flow bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.flow_bicoder.FlowBicoder]"""
        self.moisture_bicoders = dict()     # Moisture bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.moisture_bicoder.MoistureBicoder]"""
        self.temperature_bicoders = dict()  # Temperature bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.temp_bicoder.TempBicoder]"""
        self.switch_bicoders = dict()       # Switch bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.switch_bicoder.SwitchBicoder]"""
        self.pump_bicoders = dict()         # Pump bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.pump_bicoder.PumpBicoder]"""

        # create status reference to objects
        self.statuses = SubStationStatuses(_controller_object=self)  # Controller statuses
        """:type: common.objects.statuses.controllers.bl_sb_statuses.SubStationStatuses"""

        # Attribute so we can store the messages
        self.build_message_string = ''  # place to store the message that is compared to the substation message

        self.set_default_values()
        
    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        command = "SET,CN,SN={0},DS={1},LA={2},LG={3}".format(
            self.sn,  # {0}
            self.ds,  # {1}
            self.la,  # {2}
            self.lg  # {3}
        )

        try:
            # Attempt to set Controller default values
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set Substation {0}'s 'Default values' to: '{1}'. " \
                    "Exception received: {2}".format(self.mac, command, e.message)
            raise Exception(e_msg)
        else:
            print("Successfully set Substation {0}'s 'Default values' to: {1}".format(self.mac, command))

    #################################
    def set_message(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        :param _helper_object
        :type _helper_object : BaseStartStopPause
        """
        try:
            self.build_message_string = messages.message_to_set(self,
                                                                _status_code=_status_code,
                                                                _ct_type=opcodes.controller,
                                                                _helper_object=_helper_object)
            self.ser.send_and_wait_for_reply(tosend=self.build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.set_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=self.build_message_string)

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_connected_to_controller(self):
        """
        Verifies the connection to a controller from a substation.

        """
        command = "{0},{1}".format(
            ActionCommands.GET,                 # {0}
            BasemanagerCommands.BaseManager,    # {1}
        )
        _expected_status = opcodes.connected
        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to get Substation {0}'s 'Controller Connection " \
                    "Status'.".format(self.ad)
            raise Exception(e_msg)

        else:
            cn_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if _expected_status != cn_ss_on_cn:
                e_msg = "Exception occurred trying to verify Substation {0}'s 'Controller Connection Status'. " \
                        "Expected Status: '{1}', Received: '{2}'.".format(
                            self.ad,
                            _expected_status,
                            cn_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified Substation {0}'s 'Controller Connection Status'. Status Received: " \
                      "'{1}'.".format(self.ad, _expected_status)
                return True

    #################################
    def verify_not_connected_to_controller(self):
        """
        Verifies the connection to a controller from a substation.

        """
        # command = "{0},{1}={2}".format(
        command = "{0},{1}".format(
            ActionCommands.GET,                 # {0}
            BasemanagerCommands.BaseManager     # {1}
        )
        _expected_status = opcodes.disconnected
        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to get Substation {0}'s 'Controller Connection " \
                    "Status'.".format(self.ad)
            raise Exception(e_msg)

        else:
            cn_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if _expected_status != cn_ss_on_cn:
                e_msg = "Exception occurred trying to verify Substation {0}'s 'Controller Connection Status'. " \
                        "Expected Status: '{1}', Received: '{2}'.".format(
                            self.ad,
                            _expected_status,
                            cn_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified Substation {0}'s 'Controller Connection Status'. Status Received: " \
                      "'{1}'.".format(self.ad, _expected_status)
                return True

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this Substation. \n

        :param _expected_status:    Expected status to compare against. \n
        :type _expected_status:     str \n
        :return:
        """
        # Get all information about the device from the substation.
        self.get_data()

        # Verify base attributes
        # self.verify_date_and_time()  # verify date time object against current date time of the substation
        self.verify_description()
        self.verify_serial_number()

        if _expected_status is not None:
            self.verify_status(_expected_status=_expected_status)

        # Verify 'Substation' specific attributes
        # self.verify_code_version(_data=self.data)

    #################################
    def set_bicoder_default_values(self):
        """
        For all bicoders on a substation set the default values.
        """
        # Set default values for valve bicoders
        for valve_sn in self.valve_bicoders.keys():
            self.valve_bicoders[valve_sn].set_default_values()

        # Set default values for flow bicoders
        for flow_sn in self.flow_bicoders.keys():
            self.flow_bicoders[flow_sn].set_default_values()

        # Set default values for switch bicoders
        for switch_sn in self.switch_bicoders.keys():
            self.switch_bicoders[switch_sn].set_default_values()

        # Set default values for moisture bicoders
        for moisture_sn in self.moisture_bicoders.keys():
            self.moisture_bicoders[moisture_sn].set_default_values()

        # Set default values for temperature bicoders
        for temperature_sn in self.temperature_bicoders.keys():
            self.temperature_bicoders[temperature_sn].set_default_values()

        # Set default values for analog bicoders
        for analog_sn in self.analog_bicoders.keys():
            self.analog_bicoders[analog_sn].verify_who_i_am()

        # Set default values for pump bicoders
        for pump_sn in self.pump_bicoders.keys():
            self.pump_bicoders[pump_sn].set_default_values()

    #################################
    def verify_full_configuration(self):
        """
        Verifies itself and all of the assigned bicoders.
        """
        self.verify_who_i_am()

        # Set default values for valve bicoders
        for valve_sn in self.valve_bicoders.keys():
            self.valve_bicoders[valve_sn].verify_who_i_am(_for_substation=True)

        # Set default values for flow bicoders
        for flow_sn in self.flow_bicoders.keys():
            self.flow_bicoders[flow_sn].verify_who_i_am()

        # Set default values for switch bicoders
        for switch_sn in self.switch_bicoders.keys():
            self.switch_bicoders[switch_sn].verify_who_i_am()

        # Set default values for moisture bicoders
        for moisture_sn in self.moisture_bicoders.keys():
            self.moisture_bicoders[moisture_sn].verify_who_i_am()

        # Set default values for temperature bicoders
        for temperature_sn in self.temperature_bicoders.keys():
            self.temperature_bicoders[temperature_sn].verify_who_i_am()

        # Set default values for analog bicoders
        for analog_sn in self.analog_bicoders.keys():
            self.analog_bicoders[analog_sn].verify_who_i_am()

        # TODO: HUH? No verify who i am for pumps
        # Set default values for temperature bicoders
        # for pump_sn in self.pump_bicoders.keys():
        #     self.pump_bicoders[pump_sn].verify_who_i_am()

        self.verify_who_i_am()

    #################################
    def clear_bicoder_objects(self):
        """
        Clears all of this SubStation's biCoders dictionaries
        """
        self.valve_bicoders.clear()
        self.analog_bicoders.clear()
        self.flow_bicoders.clear()
        self.moisture_bicoders.clear()
        self.temperature_bicoders.clear()
        self.switch_bicoders.clear()
        self.pump_bicoders.clear()

    #############################
    def load_assigned_devices(self, flow_bicoder_version=None, unit_testing=False):
        """
        Helper method. Loads all devices from the configuration file into the controller. \n
        :return: 
        """
        print "Loading all devices from configuration into the Substation {0}".format(self.mac)
        try:
            self.load_dv_to_cn(dv_type="D1", list_of_decoder_serial_nums=self.d1_bicoders)
            self.load_dv_to_cn(dv_type="D2", list_of_decoder_serial_nums=self.d2_bicoders)
            self.load_dv_to_cn(dv_type="D4", list_of_decoder_serial_nums=self.d4_bicoders)
            self.load_dv_to_cn(dv_type="DD", list_of_decoder_serial_nums=self.dd_bicoders)
            self.load_dv_to_cn(dv_type="MS", list_of_decoder_serial_nums=self.ms_bicoders)
            self.load_dv_to_cn(dv_type="FM", list_of_decoder_serial_nums=self.fm_bicoders, flow_bicoder_version=flow_bicoder_version)
            self.load_dv_to_cn(dv_type="TS", list_of_decoder_serial_nums=self.ts_bicoders)
            self.load_dv_to_cn(dv_type="SW", list_of_decoder_serial_nums=self.sw_bicoders)
        except Exception as e:
            # Re-raise caught exception with additional info plus the exception info.
            appended_msg = "Exception occurred loading all available devices to Substation {0}: {1}".format(
                                self.mac,
                                e.message
                            )
            raise Exception(appended_msg)
        else:
            if not unit_testing:
                # Added a sleep here with a theory that the controller was attempting to search for devices when the thread
                # spun to load all of the devices wasn't finished. Thus when we attempted to search and assign the first
                # zone, a BC was returned
                time.sleep(10)
            print "[SUCCESS] Loaded all available devices to Substation {0}".format(self.mac)

    #############################
    def load_all_dv_to_cn(self, d1_list, mv_d1_list, d2_list, mv_d2_list, d4_list, dd_list, ms_list, fm_list, ts_list,
                          sw_list, is_list, ar_list, unit_testing=False):
        """
        Helper method. Loads all devices from the configuration file into the Substation. \n
        had to have mv_d1_list and mv_d2_list in order to loader decoder that can be used as master valves\n
        master valve can only be single or dual decoders
        :param d1_list:
        :param mv_d1_list
        :param d2_list:
        :param mv_d2_list
        :param d4_list:
        :param dd_list:
        :param ms_list:
        :param fm_list:
        :param ts_list:
        :param sw_list:
        :param is_list:
        :param ar_list:
        :return:
        """
        print "Loading all devices from configuration on Substation {0}".format(self.mac)
        try:
            self.load_dv_to_cn(dv_type="D1", list_of_decoder_serial_nums=d1_list)
            self.load_dv_to_cn(dv_type="D1", list_of_decoder_serial_nums=mv_d1_list)
            self.load_dv_to_cn(dv_type="D2", list_of_decoder_serial_nums=d2_list)
            self.load_dv_to_cn(dv_type="D2", list_of_decoder_serial_nums=mv_d2_list)
            self.load_dv_to_cn(dv_type="D4", list_of_decoder_serial_nums=d4_list)
            self.load_dv_to_cn(dv_type="DD", list_of_decoder_serial_nums=dd_list)
            self.load_dv_to_cn(dv_type="MS", list_of_decoder_serial_nums=ms_list)
            self.load_dv_to_cn(dv_type="FM", list_of_decoder_serial_nums=fm_list)
            self.load_dv_to_cn(dv_type="TS", list_of_decoder_serial_nums=ts_list)
            self.load_dv_to_cn(dv_type="SW", list_of_decoder_serial_nums=sw_list)
            self.load_dv_to_cn(dv_type="AR", list_of_decoder_serial_nums=is_list)
            self.load_dv_to_cn(dv_type="IS", list_of_decoder_serial_nums=ar_list)
        except Exception as e:
            # Re-raise caught exception with additional info plus the exception info.
            appended_msg = "Exception occurred loading all available devices to Substation: " + str(e.message)
            raise Exception(appended_msg)
        else:
            # Added a sleep here with a theory that the substation was attempting to search for devices when the thread
            # spun to load all of the devices wasn't finished. Thus when we attempted to search and assign the first
            # zone, a BC was returned
            if not unit_testing:
                time.sleep(10)
            print "[SUCCESS] Loaded all available devices to Substation."


    #############################
    def add_single_valve_bicoder(self, _serial_number, _device_type=opcodes.zone, _valve_bicoder_version=None):
        """
        Add a single valve bicoder to the substation. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the substation. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |zn| \n
        :type _device_type: str \n
        :param _valve_bicoder_version: version of firmware for bicoder \n
        :type _valve_bicoder_version: float \n

        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Valve Bicoder {0} to a Substation {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.valve_bicoders.keys():
                self.load_dv(dv_type=opcodes.single_valve_decoder,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=_device_type,
                             valve_bicoder_version=_valve_bicoder_version)

            # If this substation is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.base_station_3200:
                self.base_station_3200.valve_bicoders[_serial_number] = self.valve_bicoders[_serial_number]
                self.valve_bicoders[_serial_number].base_station = self.base_station_3200

    #############################
    def add_dual_valve_bicoder(self, _serial_number, _device_type=opcodes.zone, _valve_bicoder_version=None):
        """
        Add a dual valve bicoder to the substation. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the substation. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |zn| \n
        :type _device_type: str \n
        :param _valve_bicoder_version: version of firmware for bicoder \n
        :type _valve_bicoder_version: float \n

        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Valve Bicoder {0} to a Substation {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.valve_bicoders.keys():
                self.load_dv(dv_type=opcodes.two_valve_decoder,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=_device_type,
                             valve_bicoder_version=_valve_bicoder_version)

            # If this substation is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.base_station_3200:
                self.base_station_3200.valve_bicoders[_serial_number] = self.valve_bicoders[_serial_number]
                self.valve_bicoders[_serial_number].base_station = self.base_station_3200

    #############################
    def add_quad_valve_bicoder(self, _serial_number, _device_type=opcodes.zone, _valve_bicoder_version=None):
        """
        Add a quad valve bicoder to the substation. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the substation. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |zn| \n
        :type _device_type: str \n
        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Valve Bicoder {0} to a Substation {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.valve_bicoders.keys():
                self.load_dv(dv_type=opcodes.four_valve_decoder,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=_device_type,
                             valve_bicoder_version=_valve_bicoder_version)

            # If this substation is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.base_station_3200:
                self.base_station_3200.valve_bicoders[_serial_number] = self.valve_bicoders[_serial_number]
                self.valve_bicoders[_serial_number].base_station = self.base_station_3200

    #############################
    def add_twelve_valve_bicoder(self, _serial_number, _device_type=opcodes.zone, _valve_bicoder_version=None):
        """
        Add a twelve valve bicoder to the substation. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the substation. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |zn| \n
        :type _device_type: str \n
        :param _valve_bicoder_version: version of firmware for bicoder \n
        :type _valve_bicoder_version: float \n

        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Valve Bicoder {0} to a Substation {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.valve_bicoders.keys():
                self.load_dv(dv_type=opcodes.twelve_valve_decoder,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=_device_type,
                             valve_bicoder_version=_valve_bicoder_version)

            # If this substation is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.base_station_3200:
                self.base_station_3200.valve_bicoders[_serial_number] = self.valve_bicoders[_serial_number]
                self.valve_bicoders[_serial_number].base_station = self.base_station_3200

    #############################
    def add_analog_bicoder(self, _serial_number, _device_type=opcodes.pressure_sensor):
        """
        Add an analog bicoder to the substation. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the substation. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |PS| \n
        :type _device_type: str \n

        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add an Analog Bicoder {0} to a Substation {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.analog_bicoders.keys():
                self.load_dv(dv_type=opcodes.analog_bicoder, list_of_decoder_serial_nums=[_serial_number], device_type=_device_type)

            # If this substation is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.base_station_3200:
                self.base_station_3200.analog_bicoders[_serial_number] = self.analog_bicoders[_serial_number]
                self.analog_bicoders[_serial_number].base_station = self.base_station_3200

    #############################
    def add_flow_bicoder(self, _serial_number, _device_type=opcodes.flow_meter):
        """
        Add a flow bicoder to the substation. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the substation. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |FM| \n
        :type _device_type: str \n

        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Flow Bicoder {0} to a Substation {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.flow_bicoders.keys():
                self.load_dv(dv_type=opcodes.flow_meter, list_of_decoder_serial_nums=[_serial_number], device_type=_device_type)

            # If this substation is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.base_station_3200:
                self.base_station_3200.flow_bicoders[_serial_number] = self.flow_bicoders[_serial_number]
                self.flow_bicoders[_serial_number].base_station = self.base_station_3200

    #############################
    def add_moisture_bicoder(self, _serial_number, _device_type=opcodes.moisture_sensor):
        """
        Add a moisture bicoder to the substation. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the substation. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |MS| \n
        :type _device_type: str \n
        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Moisture Bicoder {0} to a Substation {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.moisture_bicoders.keys():
                self.load_dv(dv_type=opcodes.moisture_sensor, list_of_decoder_serial_nums=[_serial_number], device_type=_device_type)

            # If this substation is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.base_station_3200:
                self.base_station_3200.moisture_bicoders[_serial_number] = self.moisture_bicoders[_serial_number]
                self.moisture_bicoders[_serial_number].base_station = self.base_station_3200

    #############################
    def add_pump_bicoder(self, _serial_number, _device_type=opcodes.pump):
        """
        Add a pump bicoder to the substation. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the substation. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |PM| \n
        :type _device_type: str \n

        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Pump Bicoder {0} to a Substation {1}: '{2}' the value must " \
                    "be a str".format(
                        str(_serial_number),    # {0}
                        str(self.mac),          # {1}
                        str(self.ad)            # {2}
                    )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.pump_bicoders.keys():
                self.load_dv(dv_type=opcodes.pump, list_of_decoder_serial_nums=[_serial_number], device_type=_device_type)

            # If this substation is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.base_station_3200:
                self.base_station_3200.pump_bicoders[_serial_number] = self.pump_bicoders[_serial_number]
                self.pump_bicoders[_serial_number].base_station = self.base_station_3200

    #############################
    def add_switch_bicoder(self, _serial_number, _device_type=opcodes.event_switch):
        """
        Add a switch bicoder to the substation. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the substation. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |SW| \n
        :type _device_type: str \n
        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Switch Bicoder {0} to a Substation {1}: '{2}' the value must " \
                    "be a str".format(
                str(_serial_number),    # {0}
                str(self.mac),          # {1}
                str(self.ad)            # {2}
            )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.switch_bicoders.keys():
                self.load_dv(dv_type=opcodes.event_switch, list_of_decoder_serial_nums=[_serial_number], device_type=_device_type)

            # If this substation is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.base_station_3200:
                self.base_station_3200.switch_bicoders[_serial_number] = self.switch_bicoders[_serial_number]
                self.switch_bicoders[_serial_number].base_station = self.base_station_3200

    #############################
    def add_temperature_bicoder(self, _serial_number, _device_type=opcodes.temperature_sensor):
        """
        Add a temperature bicoder to the substation. This creates the object and loads it, but does not search.
        Do a search if you want this bicoder to appear on the substation. \n

        :param _serial_number: Serial number of the bicoder. \n
        :type _serial_number:  str \n
        :param _device_type: |TS| \n
        :type _device_type: str \n

        :return:
        :rtype:
        """
        if not isinstance(_serial_number, str):
            e_msg = "Exception occurred trying to add a Temperature Bicoder {0} to a Substation {1}: '{2}' the value must " \
                    "be a str".format(
                str(_serial_number),    # {0}
                str(self.mac),          # {1}
                str(self.ad)            # {2}
            )
            raise Exception(e_msg)

        else:
            # Load the device on the controller, and in the process making the new bicoder object
            if _serial_number not in self.temperature_bicoders.keys():
                self.load_dv(dv_type=opcodes.temperature_sensor, list_of_decoder_serial_nums=[_serial_number], device_type=_device_type)

            # If this substation is 'connected' to a controller, give the controller a reference to the bicoder object
            if self.base_station_3200:
                self.base_station_3200.temperature_bicoders[_serial_number] = self.temperature_bicoders[_serial_number]
                self.temperature_bicoders[_serial_number].base_station = self.base_station_3200

    def share_bicoders_with_controller(self):
        # Give the basestation references to all of the substation objects
        self.base_station_3200.valve_bicoders.update(self.valve_bicoders)
        self.base_station_3200.analog_bicoders.update(self.analog_bicoders)
        self.base_station_3200.flow_bicoders.update(self.flow_bicoders)
        self.base_station_3200.moisture_bicoders.update(self.moisture_bicoders)
        self.base_station_3200.temperature_bicoders.update(self.temperature_bicoders)
        self.base_station_3200.switch_bicoders.update(self.switch_bicoders)
        self.base_station_3200.pump_bicoders.update(self.pump_bicoders)

        for bicoder in self.valve_bicoders.values():
            bicoder.base_station = self.base_station_3200
        for bicoder in self.analog_bicoders.values():
            bicoder.base_station = self.base_station_3200
        for bicoder in self.flow_bicoders.values():
            bicoder.base_station = self.base_station_3200
        for bicoder in self.moisture_bicoders.values():
            bicoder.base_station = self.base_station_3200
        for bicoder in self.temperature_bicoders.values():
            bicoder.base_station = self.base_station_3200
        for bicoder in self.switch_bicoders.values():
            bicoder.base_station = self.base_station_3200
        for bicoder in self.pump_bicoders.values():
            bicoder.base_station = self.base_station_3200
    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Super classes are below                                                                                          #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #############################
    def do_reboot_controller(self, _version_with_ok_response=opcodes.new_reboot_version_sb,
                             _version_with_response_after_reboot=opcodes.new_version_with_ok_after_reboot_sb):
        """
        Reboot a SubStation controller.

        :param _version_with_ok_response:   This is the SubStation version where it responds to a reboot command with
                                            and 'OK' instead of rebooting immediately with no response.
        :param _version_with_response_after_reboot: This is the SubStation version where it responds with an 'OK'
                                                    after it comes alive after a reboot.
        :return:
        """
        super(Substation, self).do_reboot_controller(
            _version_with_ok_response=_version_with_ok_response,
            _version_with_response_after_reboot=_version_with_response_after_reboot
        )

    #############################
    def wait_for_controller_after_reboot(self, _version_with_response_after_reboot=None, unit_testing=False):
        """
        Waiting for a controller to wake up after a reboot.

        :param _version_with_response_after_reboot: This is the SubStation version where it responds with an 'OK'
                                                    after it comes alive after a reboot.
        """
        super(Substation, self).wait_for_controller_after_reboot(
            _version_with_response_after_reboot=opcodes.new_version_with_ok_after_reboot_sb,
            unit_testing=False
        )

    #################################
    def do_increment_clock(self, hours=0, minutes=0, seconds=0, update_date_mngr=True, clock_format='%H:%M:%S'):
        """
        compare each value of hours, minutes, seconds \n
        this allows to send in one of the parameter to increment the clock in the controller \n
        the clock must be stopped in order it increment it

        :param hours:               Amount of hours to increment. \n
        :type hours:                int
        :param minutes:             Amount of minutes to increment. \n
        :type minutes:              int
        :param seconds:             Amount of seconds to increment. \n
        :type seconds:              int
        :param update_date_mngr:    If we also want to update the date manager value (VERY rare case where we don't
                                    want to). \n
        :type update_date_mngr:     bool
        :return:            ok from controller
        :rtype:             ok is a str

        """

        super(Substation, self).do_increment_clock(hours=hours,
                                                   minutes=minutes,
                                                   seconds=seconds,
                                                   update_date_mngr=update_date_mngr,
                                                   clock_format=clock_format)

    #############################
    def set_controller_to_off(self, command=''):
        command = 'KEY,DL=0'
        super(Substation, self).set_controller_to_off(command=command)

    #############################
    def set_controller_to_run(self, command=''):
        command = 'KEY,DL=1'
        super(Substation, self).set_controller_to_run(command=command)