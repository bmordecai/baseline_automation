from datetime import datetime, timedelta
import time
from common import helper_methods
from common.imports import opcodes
from common.epa_package import equations
from common.epa_package.wbw_imports import WaterSenseCodes
from common.objects.base_classes.devices import BaseDevices
from common.objects.base_classes.controller import BaseController
from common.imports.types import ControllerCommands, ActionCommands, DeviceCommands,BiCoderCommands
from common.date_package.date_resource import date_mngr
from common import object_factory
import warnings


__author__ = 'Tige'


########################################################################################################################
# CONTROLLER MID CLASS
########################################################################################################################

class BaseStationWateringEngine(BaseController):
    """
    Controller Object \n
    """

    # zone objects are assigned to this variable from the object_bucket.py when test scripts are ran.

    #############################
    def __init__(self, _type,  _mac, _serial_port, _serial_number, _firmware_version=None,
                 _description='', _ad=None):
        """
        Initialize a controller instance with the specified parameters \n
        :param _mac:                        Mac address for controller. \n
        :type _mac:                         str
        :param _serial_port:                Serial port of the controller to communicate with
        :type _serial_port:                 common.objects.base_classes.ser.Ser
        :param _serial_number:              this is the serial number of the controller
        :type _serial_number:               str | None \n
        :param _firmware_version:           this is the firmware version of the controller
        :type _firmware_version:            str | None \n
        :param _port_address:
        :type _port_address:
        :param _socket_port:
        :type _socket_port:
        :param _description:                Description to put on the controller. \n
        :type _description:                 str
        """

        et = 0.00  # Controller ETo Value
        ra = 0  # Controller Rain Value

        # Initialize parent class for inheritance
        BaseController.__init__(self,
                                _type=_type,
                                _ad=_ad,
                                _mac=_mac,
                                _serial_port=_serial_port,
                                _serial_number=_serial_number,
                                _description=_description,
                                _firmware_version=_firmware_version
                                )

        # Controller Attributes
        self.mc = 1                         # Max Concurrent Zones
        self.rp = 0                         # Rain Pause Days (Rain Delay)

        self.ed = '00000000'                # Controller ETo Date
        self.ei = ''                        # Initial Eto value
        # Controller Attributes
        self.mc = 1                         # Max Concurrent Zones
        self.rp = 0                         # Rain Pause Days (Rain Delay)

        self.ed = '00000000'                # Controller ETo Date
        self.ei = ''                        # Initial Eto value

        self.jr = 'CL'                  # Rain Jumper (OP | CL)

        # Assigned device serial numbers per bicoder type.
        self.valve_bicoders = dict()        # Valve bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.valve_bicoder.ValveBicoder | 
        common.objects.simulated_bicoders.valve_bicoder.SimulatedValveBicoder]"""
        self.analog_bicoders = dict()       # Analog bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.analog_bicoder.AnalogBicoder] | 
        common.objects.simulated_bicoders.analog_bicoder.SimulatedAnalogBicoder]"""
        self.flow_bicoders = dict()         # Flow bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.flow_bicoder.FlowBicoder] | 
        common.objects.simulated_bicoders.flow_bicoder.SimulatedFlowBicoder]"""
        self.moisture_bicoders = dict()     # Moisture bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.moisture_bicoder.MoistureBicoder] | 
        common.objects.simulated_bicoders.moisture_bicoder.SimulatedMoistureBicoder]"""
        self.temperature_bicoders = dict()  # Temperature bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.temp_bicoder.TempBicoder] | 
        common.objects.simulated_bicoders.temp_bicoder.SimulatedTempBicoder]"""
        self.switch_bicoders = dict()       # Switch bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.switch_bicoder.SwitchBicoder] | 
        common.objects.simulated_bicoders.switch_bicoder.SimulatedSwitchBicoder]"""
        self.pump_bicoders = dict()         # Pump bicoder serial numbers
        """:type: dict[str, common.objects.bicoders.pump_bicoder.PumpBicoder]"""

        #  device Specific objects
        self.zones = dict()                 # Zone objects
        """:type: dict[int, common.objects.devices.zn.Zone]"""
        self.moisture_sensors = dict()      # Moisture Sensor objects
        """:type: dict[int, common.objects.devices.ms.MoistureSensor]"""
        self.temperature_sensors = dict()   # Temperature Sensor objects
        """:type: dict[int, common.objects.devices.ts.TemperatureSensor]"""
        self.pressure_sensors = dict()      # Pressure Sensor objects
        """:type: dict[int, common.objects.devices.ps.PressureSensor]"""
        self.master_valves = dict()         # Master Valve objects
        """:type: dict[int, common.objects.devices.mv.MasterValve]"""
        self.pumps = dict()                 # Pump objects
        """:type: dict[int, common.objects.devices.pm.Pump]"""
        self.flow_meters = dict()           # Flow Meter objects
        """:type: dict[int, common.objects.devices.fm.FlowMeter]"""
        self.event_switches = dict()        # Event Switch objects
        """:type: dict[int, common.objects.devices.sw.EventSwitch]"""
        self.alert_relays = dict()          # Alert relay objects
        """:type: dict[int, common.objects.devices.ar.AlertRelay]"""

        #  Program Specific objects
        self.programs = dict()    # Program objects
        """:type: dict[int, common.objects.programming.pg_1000.PG1000|
        common.objects.programming.pg_3200.PG3200]"""

        #  substation Specific objects
        self.two_wire_simulator = None
        """:type: common.objects.controllers.tw_sim.TWSimulator"""

        self.dv_type = opcodes.controller

        # Assigned device serial numbers per bicoder type.
        self.d1_bicoders = list()        # Single valve
        self.d2_bicoders = list()        # Dual valve
        self.d4_bicoders = list()        # Quad valve
        self.dd_bicoders = list()        # Twelve valve
        self.ms_bicoders = list()        # Moisture bicoder serial numbers
        self.fm_bicoders = list()        # Flow bicoder serial numbers
        self.ts_bicoders = list()        # Temperature bicoder serial numbers
        self.sw_bicoders = list()        # Switch bicoder serial numbers

        # Date and time on the controller
        self.controller_object_current_date_time = date_mngr.controller_datetime

    ###############################
    def assign_all_devices(self):
        """
        - search and address the devices:
        if object is not in list than we do not search for the devices
            - zones                 {zn}
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        - once the devices are found they can be addressed so that they can be used in the programming
            - zones can use addresses {1-200}
            - Master Valves can use address {1-8}
        - the 3200 auto address certain devices in the order it receives them:
            - Master Valves         {mv}
            - Moisture Sensors      {ms}
            - Temperature Sensors   {ts}
            - Event Switches        {sw}
            - Flow Meter            {fm}
        """

        try:
            if not len(self.zones.values()) == 0:
                # assign zones an address between 1-200
                self.set_address_and_default_values_for_zn()
            if not len(self.master_valves.values()) == 0:
                self.set_address_and_default_values_for_mv()
            if not len(self.pumps.values()) == 0:
                self.set_address_and_default_values_for_pm()
            if not len(self.moisture_sensors.values()) == 0:
                self.set_address_and_default_values_for_ms()
            if not len(self.temperature_sensors.values()) == 0:
                self.set_address_and_default_values_for_ts()
            if not len(self.event_switches.values()) == 0:
                self.set_address_and_default_values_for_sw()
            if not len(self.flow_meters.values()) == 0:
                self.set_address_and_default_values_for_fm()
            if not len(self.pressure_sensors.values()) == 0:
                self.set_address_and_default_values_for_ps()

        except Exception as e:
            e_msg = 'Failed while setting address and default values for devices. Error Message: ' + e.message
            raise Exception(e_msg)

    #################################
    def set_rain_delay(self, _days_to_delay=None):
        """
        Sets the rain delay for the controller to the number of days specified in the parameter
        :param _days_to_delay:      Number of days to pause. \n
        :type _days_to_delay:       Integer. \n
        :return:
        """
        # Check if overwrite is intended
        if _days_to_delay is not None:

            # Verify integer is passed in for number of days to set for rain delay
            if not isinstance(_days_to_delay, int):
                e_msg = "Failed trying to set rain delay for controller. Invalid type passed in, " \
                        "expected integer. Type Received: {0}".format(type(_days_to_delay))
                raise TypeError(e_msg)
            else:
                # Set rain delay attribute to value passed in
                self.rp = _days_to_delay

        # Command for sending
        command = "{0},{1},{2}={3}".format(
            ActionCommands.SET,  # {0}
            DeviceCommands.Type.CONTROLLER,  # {1}
            ControllerCommands.Attributes.RAIN_PAUSE_DAYS,  # {2}
            str(self.rp)  # {3}
        )
        try:
            # Send command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Rain Delay' to: '{0}'".format(str(self.rp))
            raise Exception(e_msg)
        else:
            print("Successfully set controller's 'Rain Delay' to: {0}".format(str(self.rp)))

    ##############################
    def set_eto_and_date_stamp(self, _controller_et_value, _controller_rain_value, _controller_date):
        """
        - set an ETo value and rain on the controller
            - Send an ETo value
            - send a Rain value
            - send a valid date
                - The value takes effect on the controller after 1:00am (controller time)
        :param _controller_et_value:     Controller ETo value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _controller_et_value:     float \n

        :param _controller_rain_value:   Controller rain value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _controller_rain_value:   float \n

        :param _controller_date:         date format: 20150703 format (yyyymmdd)
        :type  _controller_date:         str \n
        """
        # Check if controller et value is a valid float
        if not isinstance(_controller_et_value, float):
            e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was and Invalid " \
                    "argument type, expected a float, received: {1}" \
                .format(_controller_et_value, type(_controller_et_value))
            raise TypeError(e_msg)

        # Check if in controller eto value is in range 0.00 - 5.00
        if _controller_et_value < 0.00 or _controller_et_value > 5.00:
            e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was not between" \
                    "(0.00 and 5.00)" \
                .format(_controller_et_value)
            raise ValueError(e_msg)
        else:
            # Here, 'format_float_value_to_the_hundredths returns a float value with 2 decimal places, ALWAYS
            BaseDevices.et = round(number=_controller_et_value, ndigits=3)

        # Check if in controller rain value range 0.00 - 5.00
        if _controller_rain_value < 0.00 or _controller_rain_value > 5.00:
            e_msg = "Exception occurred trying to set the controller Rain value. Value received {0} was not between" \
                    "(0.00 and 5.00)" \
                .format(_controller_rain_value)
            raise ValueError(e_msg)
        else:
            # Here, 'format_float_value_to_the_hundredths returns a float value with 2 decimal places, ALWAYS
            BaseDevices.ra = round(number=_controller_rain_value, ndigits=3)

        # Check if controller et date is a valid str
        if not isinstance(_controller_date, str):
            e_msg = "Exception occurred trying to set the controller ET Date. Value received {0} was and Invalid " \
                    "argument type, expected a str YYYYMMDD" \
                .format(_controller_date, type(_controller_date))
            raise TypeError(e_msg)

        # Verify the str format and length
        string_length = 8
        if not helper_methods.verify_value_is_string(string1=_controller_date,
                                                     string2=_controller_date,
                                                     length=string_length):
            e_msg = "Exception occurred trying to set the controller ET Date. Value received {0} was and invalid " \
                    "length, expected a {1} characters string (YYYYMMDD)" \
                .format(_controller_date, string_length)
            raise TypeError(e_msg)
        else:
            self.ed = _controller_date

        # Command for sending
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.DO,          # {0}
            opcodes.eto,                # {1}
            BaseDevices.et,             # {2}
            opcodes.date_time,          # {3}
            self.ed,                    # {4}
            WaterSenseCodes.Rain_Fall,  # {5}
            BaseDevices.ra              # {6}
        )

        try:
            # Attempt to set ETo and date stamp on the controller
            self.send_command_with_reply(tosend=command)

            for zone in self.zones:
                self.zones[zone].etc = equations.calculate_etc(_eto=BaseDevices.et, _kc=self.zones[zone].kc)
                # self.config.zones[zone].etc = equations.calculate_etc(_eto=Devices.et, _kc=self.config.zones[zone].kc)

        except Exception:
            e_msg = "Exception occurred trying to set the eto and date stamp on the controller command set was: {0}"\
                .format(
                    command  # {0}
                )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully set the eto and date stamp on the controller"
            print (e_msg)

    #################################
    def set_initial_eto_value(self, _initial_et_value):
        """
        - set an initial ETo value on the controller that will be populated to all zones giving them an initial ETo \n
          value\n
            - Send an ETo value
        :param _initial_et_value:     Initial ETo value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _initial_et_value:     float \n
        """
        # Check if controller et value is a valid float
        if not isinstance(_initial_et_value, float):
            e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was and Invalid " \
                    "argument type, expected a float, received: {1}" \
                .format(_initial_et_value, type(_initial_et_value))
            raise TypeError(e_msg)

        # Check if intial et value in range 0.00 - 5.00
        if _initial_et_value < 0.00 or _initial_et_value > 5.00:
            e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was not between" \
                    "(0.00 and 5.00)" \
                .format(_initial_et_value)
            raise ValueError(e_msg)
        else:
            self.ei = round(number=_initial_et_value, ndigits=3)

        # Command for sending
        # the command below send the controller a packet that set all ETo values of all zones
        command = "{0},{1}={2}".format(
            ActionCommands.DO,      # {0}
            opcodes.initial_ETo,    # {1}
            self.ei)                # {2}

        try:
            # Attempt to set ETo and date stamp on the controller
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set the initial eto on the controller command set was: {0}" \
                .format(
                    command  # {0}
                )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully set the initial eto on the controller"
            print (e_msg)

    #################################
    def add_mainline_to_controller(self, _mainline_address):
        """
        Add a mainline to the controller

        :param _mainline_address:   address of the mainline \n
        :type _mainline_address:    int \n
        """
        if not isinstance(_mainline_address, int):
            e_msg = "Exception occurred trying to add mainline{0}'to the controller: '{1}' the value must " \
                    "be an int".format(
                        str(_mainline_address),  # {0}
                        str(self.ad)  # {1}
                    )
            raise Exception(e_msg)
        else:
            # create mainline object here when this is called
            object_factory.create_mainline_object(controller=self, _mainline_address=_mainline_address)
            self.mainlines[_mainline_address].send_programming()

    #################################
    def add_zone_to_controller(self, _address, _serial_number):
        """
        Add a zone to the controller. \n

        :param _address:    Address of the zone \n
        :type _address:     int \n

        :param _serial_number:    Serial number of the zone \n
        :type _serial_number:     str \n
        """
        single_valve_bicoders = []
        dual_valve_bicoders = []
        quad_valve_bicoders = []
        twelve_valve_bicoders = []

        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add zone {0} to the controller: '{1}' the value must " \
                    "be an int".format(
                        str(_address),  # {0}
                        str(_address)   # {1}
                    )
            raise Exception(e_msg)
        else:
            valve_type = None
            if _serial_number.startswith(("TSD", "TPR", "TMV", "D", "H1")):
                valve_type = opcodes.single_valve_decoder
                single_valve_bicoders.append(_serial_number)
            elif _serial_number.startswith(("TVE", "TSE", "E", "H2")):
                valve_type = opcodes.two_valve_decoder
                dual_valve_bicoders.append(_serial_number)
            elif _serial_number.startswith(("TSQ", "Q", "H4")):
                valve_type = opcodes.four_valve_decoder
                quad_valve_bicoders.append(_serial_number)
            elif _serial_number.startswith(("TVA", "TVB", "V", "HA")):
                valve_type = opcodes.twelve_valve_decoder
                twelve_valve_bicoders.append(_serial_number)

            # The valve bicoder should already be created, but if it isn't, then we create it here and load it
            if _serial_number not in self.valve_bicoders.keys():
                self.load_dv(dv_type=valve_type, list_of_decoder_serial_nums=[_serial_number], device_type=opcodes.zone)

            # create zone object here when this is called
            object_factory.create_zone_objects(controller=self,
                                               zn_ad_range=[_address, _address],
                                               d1=single_valve_bicoders,
                                               d2=dual_valve_bicoders,
                                               d4=quad_valve_bicoders,
                                               dd=twelve_valve_bicoders)
            self.zones[_address].set_address(_ad=_address)
            self.zones[_address].set_default_values()

    #################################
    def add_master_valve_to_controller(self, _address, _serial_number):
        """
        Add a master valve to the controller. \n

        :param _address:    Address of the master valve \n
        :type _address:     int \n

        :param _serial_number:    Serial number of the master valve \n
        :type _serial_number:     str \n
        :return:
        :rtype:
        """
        single_valve_bicoders = []
        dual_valve_bicoders = []

        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add master valve {0} to the controller: '{1}' the value must " \
                    "be an int".format(
                        str(_address),  # {0}
                        str(_address)   # {1}
                    )
            raise Exception(e_msg)
        else:
            valve_type = None
            if _serial_number.startswith(("TSD", "TPR", "TMV", "D", 'HM')):
                valve_type = opcodes.single_valve_decoder
                single_valve_bicoders.append(_serial_number)
            elif _serial_number.startswith(("TVE", "TSE", "E", "HM")):
                valve_type = opcodes.two_valve_decoder
                dual_valve_bicoders.append(_serial_number)

            # The valve bicoder should already be created, but if it isn't, then we create it here and load it
            if _serial_number not in self.valve_bicoders.keys():
                self.load_dv(dv_type=valve_type,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=opcodes.master_valve)

            # create master valve object here when this is called
            object_factory.create_master_valve_objects(controller=self,
                                                       address_range=[_address],
                                                       serial_numbers=[_serial_number])
            self.master_valves[_address].set_address(_ad=_address)
            self.master_valves[_address].set_default_values()

    #################################
    def add_flow_meter_to_controller(self, _address, _serial_number):
        """
        Add a flow meter to the controller. \n

        :param _address:    Address of the flow meter \n
        :type _address:     int \n

        :param _serial_number:    Serial number of the flow meter \n
        :type _serial_number:     str \n
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add flow meter {0} to the controller: '{1}' the value must " \
                    "be an int".format(
                        str(_address),  # {0}
                        str(_address)   # {1}
                    )
            raise Exception(e_msg)
        else:
            # The flow bicoder should already be created, but if it isn't, then we create it here and load it
            if _serial_number not in self.flow_bicoders.keys():
                self.load_dv(dv_type=opcodes.flow_meter,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=opcodes.flow_meter)

            # create flow meters object here when this is called
            object_factory.create_flow_meter_objects(controller=self,
                                                     address_range=[_address],
                                                     serial_numbers=[_serial_number])
            self.flow_meters[_address].set_address(_ad=_address)
            self.flow_meters[_address].set_default_values()

    #################################
    def add_moisture_sensor_to_controller(self, _address, _serial_number, real_devices=False):
        """
        Add a moisture sensor to the controller. \n

        :param _address:    Address of the moisture sensor \n
        :type _address:     int \n

        :param _serial_number:    Serial number of the moisture sensor \n
        :type _serial_number:     str \n
        """

        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add moisture sensor {0} to the controller: '{1}' the value must " \
                    "be an int".format(
                        str(_address),  # {0}
                        str(_address)   # {1}
                    )
            raise Exception(e_msg)
        else:
            # The moisture bicoder should already be created, but if it isn't, then we create it here and load it
            if _serial_number not in self.moisture_bicoders.keys():
                self.load_dv(dv_type=opcodes.moisture_sensor,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=opcodes.moisture_sensor)

            # create moisture sensor object here when this is called
            object_factory.create_moisture_sensor_objects(controller=self,
                                                          address_range=[_address],
                                                          serial_numbers=[_serial_number])
            self.moisture_sensors[_address].set_address(_ad=_address)

            if real_devices is False:
                self.moisture_sensors[_address].set_default_values()

    #################################
    def add_temperature_sensor_to_controller(self, _address, _serial_number):
        """
        Add a temperature sensor to the controller. \n

        :param _address:    Address of the temperature sensor \n
        :type _address:     int \n

        :param _serial_number:    Serial number of the moisture sensor \n
        :type _serial_number:     str \n
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add temperature sensor {0} to the controller: '{1}' the value must " \
                    "be an int".format(
                        str(_address),  # {0}
                        str(_address)   # {1}
                    )
            raise Exception(e_msg)
        else:
            # The temperature bicoder should already be created, but if it isn't, then we create it here and load it
            if _serial_number not in self.temperature_bicoders.keys():
                self.load_dv(dv_type=opcodes.temperature_sensor,
                             list_of_decoder_serial_nums=[_serial_number],
                             device_type=opcodes.temperature_sensor)

            # create temperature sensor object here when this is called
            object_factory.create_temperature_sensor_objects(controller=self,
                                                             address_range=[_address],
                                                             serial_numbers=[_serial_number])
            self.temperature_sensors[_address].set_address(_ad=_address)
            self.temperature_sensors[_address].set_default_values()

    #################################
    def add_pump_to_controller(self, _address, _serial_number):
        """
        Add a pump to the controller. \n

        :param _address:    Address of the pump \n
        :type _address:     int \n

        :param _serial_number:    Serial number of the pump \n
        :type _serial_number:     str \n
        """
        single_valve_bicoders = []
        dual_valve_bicoders = []

        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add pump {0} to the controller: '{1}' the value must " \
                    "be an int".format(
                        str(_address),  # {0}
                        str(_address)   # {1}
                    )
            raise Exception(e_msg)
        else:
            valve_type = None
            if _serial_number.startswith(("TSD", "TPR", "PMV", "TMV", "D")):
                valve_type = opcodes.single_valve_decoder
                single_valve_bicoders.append(_serial_number)
            elif _serial_number.startswith(("TVE", "TSE", "E")):
                valve_type = opcodes.two_valve_decoder
                dual_valve_bicoders.append(_serial_number)

            # The valve bicoder should already be created, but if it isn't, then we create it here and load it
            if _serial_number not in self.valve_bicoders.keys():
                self.load_dv(dv_type=valve_type, list_of_decoder_serial_nums=[_serial_number], device_type=opcodes.pump)

            # create pump object here when this is called
            object_factory.create_pump_objects(controller=self,
                                               address_range=[_address],
                                               serial_numbers=[_serial_number])
            self.pumps[_address].set_address(_ad=_address)
            self.pumps[_address].set_default_values()

    #################################
    def add_pressure_sensor_to_controller(self, _address, _serial_number):
        """
        Add a pressure sensor to the controller. \n

        :param _address:    Address of the pressure sensor \n
        :type _address:     int \n

        :param _serial_number:    Serial number of the pressure sensor \n
        :type _serial_number:     str \n
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add pressure sensor {0} to the controller: '{1}' the value must " \
                    "be an int".format(
                        str(_address),  # {0}
                        str(_address)   # {1}
                    )
            raise Exception(e_msg)
        else:
            # The analog bicoder should already be created, but if it isn't, then we create it here and load it
            if _serial_number not in self.analog_bicoders.keys():
                self.load_dv(dv_type=opcodes.pressure_sensor,
                             list_of_decoder_serial_nums=[_serial_number])

            # create pressure sensor object here when this is called
            object_factory.create_pressure_sensor_objects(controller=self,
                                                          address_range=[_address],
                                                          serial_numbers=[_serial_number])
            self.pressure_sensors[_address].set_address(_ad=_address)
            self.pressure_sensors[_address].set_default_values()

    #################################
    def add_event_switch_to_controller(self, _address, _serial_number):
        """
        Add a event switch to the controller. \n

        :param _address:    Address of the event switch \n
        :type _address:     int \n

        :param _serial_number:    Serial number of the event switch \n
        :type _serial_number:     str \n
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add event switch {0} to the controller: '{1}' the value must " \
                    "be an int".format(
                        str(_address),  # {0}
                        str(_address)   # {1}
                    )
            raise Exception(e_msg)
        else:
            # The switch bicoder should already be created, but if it isn't, then we create it here and load it
            if _serial_number not in self.switch_bicoders.keys():
                self.load_dv(dv_type=opcodes.event_switch,
                             list_of_decoder_serial_nums=[_serial_number])

            # create event switch object here when this is called
            object_factory.create_event_switch_objects(controller=self,
                                                       address_range=[_address],
                                                       serial_numbers=[_serial_number])
            self.event_switches[_address].set_address(_ad=_address)
            self.event_switches[_address].set_default_values()

    #################################
    def set_max_concurrent_zones(self, _max_zones=None):
        """
        Sets the max number of concurrent zones for the controller. The current instance value can be overwritten by
        passing in a value for _max_zones. \n
        :param _max_zones:  Number of concurrent zones to set. \n
        :type _max_zones:   Int | None \n
        :return:
        """
        # Check if overwrite is intended
        if _max_zones is not None:
            # Verify integer is passed in for max number of concurrent zones to be set
            if not isinstance(_max_zones, int):
                e_msg = "Failed trying to set max concurrent zones for controller. Invalid type passed in, expected " \
                        "integer.  Type Received: {0}".format(type(_max_zones))
                raise TypeError(e_msg)
            else:
                # Overwrite existing max concurrent zones with new value
                self.mc = _max_zones

        # Command for sending
        command = "SET,CN,MC={0}".format(str(self.mc))

        try:
            # Attempt to set max concurrent zones at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Max Concurrent Zones' to: '{0}'".format(
                    str(self.mc))
            raise Exception(e_msg)
        else:
            print("Successfully set controller 'Max Concurrent Zones' to: {0}".format(str(self.mc)))

    #################################
    def set_rain_jumper_state(self, _value=None):
        """
        Set the rain jumper to be init_driver or closed based on the value passed in. \n
        :param _value:      Accepted values: 'OP' (init_driver), 'CL' (closed) \n
        :type _value:       str \n
        :return:
        """
        # Check if overwrite is intended
        if _value is not None:

            # Make sure entered correct value
            if _value not in ["CL", "OP"]:
                e_msg = "Exception occurred attempting to set incorrect 'Rain Jumper State' value for controller: " \
                        "'{0}', Expects 'CL' | 'OP'.".format(str(_value))
                raise ValueError(e_msg)
            else:
                self.jr = _value

        # Command for sending
        command = "SET,CN,JR={0}".format(str(self.jr))

        try:
            # Send command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Rain Jumper State' to: '{0}'".format(str(self.jr))
            raise Exception(e_msg)
        else:
            print("Successfully set controller's 'Rain Jumper' to: {0}".format(str(self.jr)))

    #################################
    def set_initial_eto_value_cn(self, _initial_et_value):
        """
        - set an initial ETo value on the controller that will be populated to all zones giving them an initial ETo \n
          value\n
            - Send an ETo value
        :param _initial_et_value:     Initial ETo value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _initial_et_value:     float \n
        """
        # Check if controller et value is a valid float
        if not isinstance(_initial_et_value, float):
            e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was and Invalid " \
                    "argument type, expected a float, received: {1}"\
                    .format(_initial_et_value, type(_initial_et_value))
            raise TypeError(e_msg)

        # Check if intial et value in range 0.00 - 5.00
        if _initial_et_value < 0.00 or _initial_et_value > 5.00:
            e_msg = "Exception occurred trying to set the controller ET value. Value received {0} was not between" \
                    "(0.00 and 5.00)"\
                    .format(_initial_et_value)
            raise ValueError(e_msg)
        else:
            self.ei = round(number=_initial_et_value, ndigits=3)

        # Command for sending
        command = "DO,{0}={1}".format(opcodes.initial_ETo, self.ei)

        try:
            # Attempt to set ETo and date stamp on the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set the controller ETo = '{0}' " \
                    "and Date Stamp = '{1}'".format(self.ei, self.ed)
            raise Exception(e_msg)

        # the command above send the controller a packet that set all ETo values of all zones
        # the below for loop sets all of the zones to have an initial ETo value this way we have a way to verify
        # the object against what the controller has
        BaseDevices.et = self.ei

        print("Successfully set the initial ETo value on controller and all zones = '{0}' "
              .format(self.ei))

    #############################
    def do_search_for_all_devices(self, _list_of_device_types):
        """
        Searches for all available devices on the two wire.
        :param _list_of_device_types:     list of device types loaded into Json file
        :type _list_of_device_types:      list \n
        :return:
        """

        try:
            # TODO Do we have to do a search if we don't have any of that device? If not, we can save some time
            if opcodes.zone in _list_of_device_types:
                self.do_search_for_zones()
            if opcodes.master_valve in _list_of_device_types:
                self.do_search_for_master_valves()
            if opcodes.pump in _list_of_device_types:
                self.do_search_for_pumps()
            if opcodes.moisture_sensor in _list_of_device_types:
                self.do_search_for_moisture_sensor()
            if opcodes.temperature_sensor in _list_of_device_types:
                self.do_search_for_temperature_sensor()
            if opcodes.event_switch in _list_of_device_types:
                self.do_search_for_event_switches()
            if opcodes.flow_meter in _list_of_device_types:
                self.do_search_for_flow_meters()
            if opcodes.analog_decoder in _list_of_device_types:
                self.do_search_for_analog_bicoders()
        except Exception as e:
            # Re-raise caught exception with additional info plus the exception info.
            appended_msg = "Exception occurred searching for all available devices on Controller {0}: {1}".format(
                self.mac,
                e.message
            )
            raise Exception(appended_msg)
        else:
            print "[SUCCESS] Searched for all available devices on Controller {0}".format(self.mac)

    #############################
    def do_search_for_dv(self, dv_type):
        """
        Tells the controller to search for two-wire devices of the specified type. \n
        """
        # Validate decoder type passed in against accepted types
        if dv_type not in [opcodes.zone, opcodes.moisture_sensor, opcodes.flow_meter, opcodes.master_valve,
                           opcodes.pump, opcodes.temperature_sensor, opcodes.event_switch, opcodes.alert_relay]:
            raise ValueError("[CONTROLLER] incorrect two-wire device type passed in: " + str(dv_type))

        print "Search for device type: " + str(dv_type)
        try:
            command = "DO,SR=" + str(dv_type)
            self.ser.send_and_wait_for_reply(command)
        except Exception as e:
            print(e.message)
            raise Exception("Searching for two-wire devices failed.. " + str(e.message))

    #############################
    def do_search_for_zones(self):
        """
        Tells the controller to search for Zones on the two-wire. \n
        """
        print "Search for Zones"
        command = '{0},{1}={2}'.format(
            ActionCommands.DO,  # {0}
            ActionCommands.SEARCH,  # {1}
            DeviceCommands.Type.ZONE  # {2}
        )
        try:
            self.ser.send_and_wait_for_reply(command)
        except Exception as e:
            print(e.message)
            raise Exception("Searching for two-wire Zones failed.. " + str(e.message))

    #############################
    def do_search_for_temperature_sensor(self):
        """
        Tells the controller to search for Temperature Sensors on the two-wire. \n
        """
        print "Search for Temperature Sensors"
        command = '{0},{1}={2}'.format(
            ActionCommands.DO,  # {0}
            ActionCommands.SEARCH,  # {1}
            DeviceCommands.Type.TEMPERATURE_SENSOR  # {2}
        )
        try:
            self.ser.send_and_wait_for_reply(command)
        except Exception as e:
            print(e.message)
            raise Exception("Searching for two-wire Temperature Sensors failed.. " + str(e.message))

    #############################
    def do_search_for_moisture_sensor(self):
        """
        Tells the controller to search for Moisture Sensors on the two-wire. \n
        """
        print "Search for Moisture Sensors"
        command = '{0},{1}={2}'.format(
            ActionCommands.DO,  # {0}
            ActionCommands.SEARCH,  # {1}
            DeviceCommands.Type.MOISTURE_SENSOR  # {2}
        )
        try:
            self.ser.send_and_wait_for_reply(command)
        except Exception as e:
            print(e.message)
            raise Exception("Searching for two-wire Moisture Sensors failed.. " + str(e.message))

    #############################
    def do_search_for_master_valves(self):
        """
        Tells the controller to search for Master Valves on the two-wire. \n
        """
        print "Search for Master Valves"
        command = '{0},{1}={2}'.format(
            ActionCommands.DO,  # {0}
            ActionCommands.SEARCH,  # {1}
            DeviceCommands.Type.MASTER_VALVE  # {2}
        )
        try:
            self.ser.send_and_wait_for_reply(command)
        except Exception as e:
            print(e.message)
            raise Exception("Searching for two-wire Master Valves failed.. " + str(e.message))

    #############################
    def do_search_for_pumps(self):
        """
        Tells the controller to search for Pumps on the two-wire. \n
        """
        print "Search for Pumps"
        command = '{0},{1}={2}'.format(
            ActionCommands.DO,  # {0}
            ActionCommands.SEARCH,  # {1}
            DeviceCommands.Type.PUMP  # {2}
        )
        try:
            self.ser.send_and_wait_for_reply(command)
        except Exception as e:
            print(e.message)
            raise Exception("Searching for two-wire Pumps failed.. " + str(e.message))

    #############################
    def do_search_for_analog_bicdoers(self):
        """
        Tells the controller to search for Pressure Sensors on the two-wire. \n
        """
        print "Search for Pressure Sensors"
        command = '{0},{1}={2}'.format(
            ActionCommands.DO,  # {0}
            ActionCommands.SEARCH,  # {1}
            BiCoderCommands.Type.ANALOG  # {2}
        )
        try:
            self.ser.send_and_wait_for_reply(command)
        except Exception as e:
            print(e.message)
            raise Exception("Searching for two-wire Pressure Senors failed.. " + str(e.message))

    #############################
    def do_search_for_event_switches(self):
        """
        Tells the controller to search for Event Switches on the two-wire. \n
        """
        print "Search for Event Switches"
        command = '{0},{1}={2}'.format(
            ActionCommands.DO,  # {0}
            ActionCommands.SEARCH,  # {1}
            DeviceCommands.Type.EVENT_SWITCH  # {2}
        )
        try:
            self.ser.send_and_wait_for_reply(command)
        except Exception as e:
            print(e.message)
            raise Exception("Searching for two-wire Event Switches failed.. " + str(e.message))

    #############################
    def do_search_for_analog_bicoders(self):
        """
        Tells the controller to search for Pressure Sensor on the two-wire. \n
        """
        print "Search for Pressure Sensor"
        command = '{0},{1}={2}'.format(
            ActionCommands.DO,  # {0}
            ActionCommands.SEARCH,  # {1}
            DeviceCommands.Type.ANALOG_BICODER  # {2}
        )
        try:
            self.ser.send_and_wait_for_reply(command)
        except Exception as e:
            print(e.message)
            raise Exception("Searching for two-wire Pressure Sensor failed.. " + str(e.message))

    #############################
    def do_search_for_flow_meters(self):
        """
        Tells the controller to search for Flow Meters on the two-wire. \n
        """
        print "Search for Flow Meters"
        command = '{0},{1}={2}'.format(
            ActionCommands.DO,  # {0}
            ActionCommands.SEARCH,  # {1}
            DeviceCommands.Type.FLOW_METER  # {2}
        )
        try:
            self.ser.send_and_wait_for_reply(command)
        except Exception as e:
            print(e.message)
            raise Exception("Searching for two-wire Flow Meters failed.. " + str(e.message))

    #################################
    def set_address_and_default_values_for_zn(self, set_address=True):
        """
        Address zones on the controller. \n
        Send zones default programing \n
        :return:
        """
        for _ad in sorted(self.zones.keys()):
            if set_address:
                self.zones[_ad].set_address(_ad=_ad)
            self.zones[_ad].set_default_values()

    #################################
    def set_address_and_default_values_for_ms(self, set_address=True):
        """
        Address Moisture Sensors on the controller. \n
        Send Moisture Sensors default programing \n
        :return:
        """
        for _ad in sorted(self.moisture_sensors.keys()):
            if set_address:
                self.moisture_sensors[_ad].set_address(_ad=_ad)
            self.moisture_sensors[_ad].set_default_values()

    #################################
    def set_address_and_default_values_for_ts(self, set_address=True):
        """
        Address Temperature Sensors on the controller. \n
        Send Temperature Sensors default programing \n
        :return:
        """
        for _ad in sorted(self.temperature_sensors.keys()):
            if set_address:
                self.temperature_sensors[_ad].set_address(_ad=_ad)
            self.temperature_sensors[_ad].set_default_values()

    #################################
    def set_address_and_default_values_for_ps(self, set_address=True):
        """
        Address Pressure Sensors on the controller. \n
        Send Pressure Sensors default programing \n
        :return:
        """
        for _ad in sorted(self.pressure_sensors.keys()):
            if set_address:
                self.pressure_sensors[_ad].set_address(_ad=_ad)
            self.pressure_sensors[_ad].set_default_values()

    #################################
    def set_address_and_default_values_for_fm(self, set_address=True):
        """
        Address Flow Meters on the controller. \n
        Send Flow Meters default programing \n
        :return:
        """
        for _ad in sorted(self.flow_meters.keys()):
            if set_address:
                self.flow_meters[_ad].set_address(_ad=_ad)
            self.flow_meters[_ad].set_default_values()

    #################################
    def set_address_and_default_values_for_mv(self, set_address=True):
        """
        Address Master Valves on the controller. \n
        Send Master Valves default programing \n
        :return:
        """
        for _ad in sorted(self.master_valves.keys()):
            if set_address:
                self.master_valves[_ad].set_address(_ad=_ad)
            self.master_valves[_ad].set_default_values()

    #################################
    def set_address_and_default_values_for_pm(self, set_address=True):
        """
        Address Pumps on the controller. \n
        Send Pumps default programing \n
        :return:
        """
        for _ad in sorted(self.pumps.keys()):
            if set_address:
                self.pumps[_ad].set_address(_ad=_ad)
            self.pumps[_ad].set_default_values()

    #################################
    def set_address_and_default_values_for_sw(self, set_address=True):
        """
        Address Pumps on the controller. \n
        Send Pumps default programing \n
        :return:
        """
        for _ad in sorted(self.event_switches.keys()):
            if set_address:
                self.event_switches[_ad].set_address(_ad=_ad)
            self.event_switches[_ad].set_default_values()

    #################################
    def set_learn_flow_enabled(self, _pg_ad, _zn_ad=None, _time_delay=None):
        """
        - Turns on the learn flow for a particular program or zone
        :param _pg_ad:     The program address that will enable the learn flow \n
        :type  _pg_ad:     Program \n
        :param _zn_ad:     The zone address that will enable the learn flow \n
        :type  _zn_ad:     Zone \n
        :param _time_delay:     The time delay in seconds \n
        :type  _time_delay:     int \n
        """
        # Check if the program address passed in is an int
        if not isinstance(_pg_ad, int):
            e_msg = "Exception occurred trying to enable learn flow. Argument passed in was an Invalid " \
                    "argument type, expected an int, received: {0}." \
                .format(type(_pg_ad))
            raise TypeError(e_msg)

        # Check if the zone address passed in is an int
        if _zn_ad is not None and not isinstance(_zn_ad, int):
            e_msg = "Exception occurred trying to enable learn flow. Argument passed in was an Invalid " \
                    "argument type, expected an int, received: {0}" \
                .format(type(_zn_ad))
            raise TypeError(e_msg)

        # Check if the zone address passed in was an int
        if _time_delay is not None and not isinstance(_time_delay, int):
            e_msg = "Exception occurred trying to enable learn flow. Time delay passed in was an Invalid " \
                    "argument type, expected an int, received: {0}" \
                .format(type(_time_delay))
            raise TypeError(e_msg)

        # Command for sending
        if _zn_ad is None:
            command = "DO,LF,{0}={1}".format(opcodes.program, _pg_ad)
        elif _time_delay is None:
            command = "DO,LF,{0}={1},{2}={3}".format(opcodes.program, _pg_ad, opcodes.zone, _zn_ad)
        else:
            command = "DO,LF,{0}={1},{2}={3},{4}={5}".format(opcodes.program, _pg_ad, opcodes.zone, _zn_ad,
                                                             opcodes.time_delay, _time_delay)

        try:
            # Send the learn flow command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set learn flow on program '{0}' and zone '{1}'.".format(
                _pg_ad,
                _zn_ad)
            raise Exception(e_msg)
        # If only a program was passed in then leave out the zone
        if _zn_ad is None:
            print "Successfully set the learn flow on program '{0}'".format(_pg_ad)
        else:
            print "Successfully set the learn flow on program '{0}' and zone '{1}'.".format(_pg_ad, _zn_ad)

    #################################
    def set_rain_jumper_to_closed(self):
        """
        Set the rain jumper to be closed\n
        """

        self.jr = opcodes.closed

        # Command for sending
        command = "{0},{1},{2}={3}".format(
            ActionCommands.SET,  # {0}
            DeviceCommands.Type.CONTROLLER,  # {1}
            ControllerCommands.Attributes.RAIN_JUMPER,  # {2}
            str(self.jr)  # {3}
        )
        try:
            # Send command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Rain Jumper State' to: '{0}'".format(
                str(self.jr))
            raise Exception(e_msg)
        else:
            print("Successfully set controller's 'Rain Jumper' to: {0}".format(str(self.jr)))

    #################################
    def set_rain_jumper_to_open(self):
        """
        Set the rain jumper to be opened \n
        """

        self.jr = opcodes.open

        # Command for sending
        command = "{0},{1},{2}={3}".format(
            ActionCommands.SET,  # {0}
            DeviceCommands.Type.CONTROLLER,  # {1}
            ControllerCommands.Attributes.RAIN_JUMPER,  # {2}
            str(self.jr)  # {3}
        )
        try:
            # Send command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Controller's 'Rain Jumper State' to: '{0}'".format(
                str(self.jr))
            raise Exception(e_msg)
        else:
            print("Successfully set controller's 'Rain Jumper' to: {0}".format(str(self.jr)))

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Add 3200 to the TwoWireSimulator                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_connection_to_two_wire_simulator(self, two_wire_simulator):
        """
        Create the connection in our objects between the controller and the two wire simulator

        :param two_wire_simulator:      The two wire simulator object
        :type two_wire_simulator:       common.objects.controllers.tw_sim.TWSimulator
        """
        # Give both the controller a reference to the two wire simulator and vice versa
        self.two_wire_simulator = two_wire_simulator
        two_wire_simulator.connected_controller = self

        # Share all of the biCoders on the TWSimulator two wire with this controller
        self.two_wire_simulator.share_bicoders_with_controller()

    #################################
    def search_for_simulated_bicoders(self):
        """
        Create the connection in our objects between the controller and the two wire simulator
        """
        # If we haven't assigned a two wire simulator yet, we cannot go any farther
        if not self.two_wire_simulator:
            e_msg = "No two wire simulator assigned to this Controller (TY: {0}, SN: {1}, MAC: {2})".format(
                self.controller_type,
                self.sn,
                self.mac
            )
            raise ValueError(e_msg)

        # We only need to do one search, and it will pick up every device on the two wire
        self.do_search_for_zones()

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifiers                                                                                                        #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_rain_jumper_state(self, _data=None):
        """
        Verifies the rain jumper's current state on the controller against the expected state passed in as a parameter.
        """
        # Get the current state of the rain jumper on the controller
        data = _data if _data else self.get_data()
        jr_state_from_cn = data.get_value_string_by_key(ControllerCommands.Attributes.RAIN_JUMPER)

        # Compare against what is on the controller
        if self.jr != jr_state_from_cn:
            e_msg = "Unable to verify Controller's 'Rain Jumper State'. Received: {0}, Expected: {1}".format(
                str(jr_state_from_cn),  # {0}
                self.jr  # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Controller {0}'s 'Rain Jumper State': '{1}'.".format(
                str(self.mac),  # {0}
                self.jr  # {1}
            ))

    #################################
    def verify_max_concurrent_zones(self, _data=None):
        """
        Verifies the max concurrent zones set for this controller. Expects the controller's value returned and
        this instance's value to be equal.
        :return:
        """
        # Get max concurrent zones set at the controller
        data = _data if _data else self.get_data()
        mc_from_cn = int(data.get_value_string_by_key(ControllerCommands.Attributes.MAX_CONCURRENT_ZONES))

        # Compare status versus what is on the controller
        if self.mc != mc_from_cn:
            e_msg = "Unable to verify Controller's 'Max Concurrent Zones'. Received: {0}, Expected: {1}".format(
                str(mc_from_cn),  # {0}
                str(self.mc)  # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s 'Max Concurrent Zones': '{2}' on controller".format(
                self.dv_type,  # {0}
                str(self.ad),  # {1}
                str(self.mc)  # {2}
            ))

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Controller'. \n
        :param _expected_status:    Expected status to compare against. \n
        :type _expected_status:     str \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify base attributes
        # self.verify_date_and_time()  # verify date time object against current date time of the controller
        # self.verify_description_on_cn()

        # TODO in the 3200 i need to be able to verify two different serial number either 3K or 3X
        self.verify_serial_number()
        self.verify_latitude()
        self.verify_longitude()

        # if _expected_status is not None:
        #     self.verify_status_on_cn(status=_expected_status)

        # Verify 'Controller' specific attributes
        self.verify_code_version()
        self.verify_rain_jumper_state()

        # For 3200 controller
        # if self.controller_type == "32":

        # Verify 3200 specific attributes
        # self.verify_flow_jumper_state_on_cn()
        # self.verify_pause_jumper_state_on_cn()

    #################################
    def verify_full_configuration(self):
        """
        This goes through each object in the list and sorts them by address
        then it verifies all attributes of the object against what the controller thinks
        :return:
        :rtype:
        """

        list_of_object_pointers = [
            self.zones,
            self.moisture_sensors,
            self.master_valves,
            self.temperature_sensors,
            self.flow_meters,
            self.event_switches,
            self.programs,
            self.mainlines,
            self.points_of_control,
            self.pressure_sensors,
            self.pumps,
            self.alert_relays,
            self.water_sources
        ]

        for each_object in list_of_object_pointers:
            for _ad in sorted(each_object.keys()):
                if each_object in [self.water_sources]:
                    self.get_water_source(_ad).verify_who_i_am()
                elif each_object in [self.points_of_control]:
                    self.get_point_of_control(_ad).verify_who_i_am()
                elif each_object in [self.mainlines]:
                    self.get_mainline(_ad).verify_who_i_am()
                else:
                    each_object[_ad].verify_who_i_am()

        self.verify_who_i_am()
