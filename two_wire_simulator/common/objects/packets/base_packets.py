from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, MoistureSensorCommands
from common.imports.types import Message
import time
__author__ = 'Baseline'


# class MoistureSensorPackets(BasePackets):
class BasePackets(object):

    #################################
    def __init__(self, _controller):
        """
        :param _controller:     Controller this object is "attached" to \n
        :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                                common.objects.controllers.bl_fs.FlowStation  \n
        """
        self.controller = _controller

    #################################
    def send_ts_packet(self, _data_group_number, _variable_string, _header_id=500):
        """
        Sends the specified packet through the web service's web socket. \n

        :param _data_group_number:  Number of the data group packet to send. \n
        :type _data_group_number:   str

        :param _variable_string:    The specific variables to be set for this object. \n
        :type _variable_string:     str

        :param _header_id:          Id of the header. \n
        :type _header_id:           int
        """
        base_packet = "{0}^{1}:{2}/{3}={4}/{5}={6}^{7}".format(
            opcodes.packet_ts,
            opcodes.data_group,
            _data_group_number,
            opcodes.message_id,
            _header_id,
            opcodes.header_sequence,
            0,
            _variable_string
        )
        self.controller.data_group_packets.resume_packet_logging()
        self.controller.data_group_packets.send_ts_packet_through_web_service(base_packet)
        time.sleep(15)
        self.controller.data_group_packets.stop_packet_logging()

    #################################
    def send_tq_packet(self, _data_group_number, _variable_string, _header_id=600):
        """
        Sends the specified packet through the web service's web socket. \n

        :param _data_group_number:  Number of the data group packet to send. \n
        :type _data_group_number:   str

        :param _variable_string:    The specific variables to be set for this object. \n
        :type _variable_string:     str

        :param _header_id:          Id of the header. \n
        :type _header_id:           int
        """
        base_packet = "{0}^{1}:{2}/{3}={4}^{5}".format(
            opcodes.packet_tq,
            opcodes.data_group,
            _data_group_number,
            opcodes.message_id,
            _header_id,
            _variable_string
        )
        self.controller.data_group_packets.resume_packet_logging()
        self.controller.data_group_packets.send_tq_packet_through_web_service(base_packet)
        time.sleep(15)
        self.controller.data_group_packets.stop_packet_logging()


        #TODO need to figure out a way to block until response from the controller with the data group we want

        self.controller.data_group_packets.parse_packet_log_file()





