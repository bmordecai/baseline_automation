from common.imports import opcodes
from common.objects.packets.base_packets import BasePackets

__author__ = 'Baseline'


class MoistureSensorPackets(BasePackets):

    #################################
    def __init__(self, _moisture_sensor_object):
        """
        :param _moisture_sensor_object:     Controller this object is "attached" to \n
        :type _moisture_sensor_object:      common.objects.devices.ms.MoistureSensor
        """
        self.moisture_sensor = _moisture_sensor_object
        # Set the attributes

        BasePackets.__init__(self, _controller=_moisture_sensor_object.controller)

    # |-------------------------------------------------|
    #         Packet: DG302
    # |-------------------------------------------------|

    #################################
    def set_test_values_dg302(self):
        self.moisture_sensor.ds = 'test table programming'
        self.moisture_sensor.en = opcodes.false
        self.moisture_sensor.la = 45.607999
        self.moisture_sensor.lg = -119.2356
        # Init parent class to inherit respective attributes
        self.set_moisture_sensor_dg302()


    #################################
    def set_moisture_sensor_dg302(self):
        """
        Build the packets variable string and send the specified packet through the web service's web socket. \n
        """
        data_group_number = '302'

        # build the message
        packet = "{0}:{1}/{2}={3}/{4}={5}/{6}={7}/{8}={9}".format(
            opcodes.moisture_sensor,        # {0}
            str(self.moisture_sensor.sn),   # {1}
            opcodes.description,            # {2}
            str(self.moisture_sensor.ds),   # {3}
            opcodes.latitude,               # {4}
            str(self.moisture_sensor.la),   # {5}
            opcodes.longitude,              # {6}
            str(self.moisture_sensor.lg),   # {7}
            opcodes.enabled,                # {8}
            self.moisture_sensor.en,        # {9}
        )
        self.send_ts_packet(
            _data_group_number=data_group_number,
            _variable_string=packet)

    #################################
    def verify_moisture_sensor_dg302(self):
        """
        Build the packets variable string and send the specified packet through the web service's web socket. \n
        """
        data_group_number = '302'

        # build tq message
        message = "{0}:{1}".format(
            opcodes.moisture_sensor,
            self.moisture_sensor.sn
        )
        self.send_tq_packet(
            _data_group_number=data_group_number,
            _variable_string=message)
        try:
            parsed_packet = self.controller.data_group_packets.te_packets[data_group_number]
        except KeyError:
            e_msg = "There is no data group packet for DG" + str(data_group_number)
            raise Exception(e_msg)
        else:
            self.moisture_sensor.data = parsed_packet[self.moisture_sensor.sn]
            if self.moisture_sensor.data is not None:
                self.moisture_sensor.verify_description()
                self.moisture_sensor.verify_serial_number()
                self.moisture_sensor.verify_latitude()
                self.moisture_sensor.verify_longitude()
