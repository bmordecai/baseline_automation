from common.imports import opcodes
from common.objects.packets.base_packets import BasePackets

__author__ = 'Baseline'


class FlowMeterPackets(BasePackets):

    #################################
    def __init__(self, _flow_meter_object):
        """
        :param _flow_meter_object:     Test Engine Flow Meter object\n
        :type _flow_meter_object:      common.objects.devices.fm.FlowMeter
        """
        self.flow_meter = _flow_meter_object
        # Set the attributes

        BasePackets.__init__(self, _controller=_flow_meter_object.controller)

    # |-------------------------------------------------|
    #         Packet: 131
    # |-------------------------------------------------|

    #################################
    # def set_test_values_dg111(self):
    #     self.master_valve.ds = 'test'
    #     self.master_valve.la = 115.52
    #     self.master_valve.lg = -115.5
    #     self.master_valve.en = opcodes.false
    #     self.master_valve.no = opcodes.true
    #     self.master_valve.ad = 3
    #
    #     self.set_master_valve_dg111()

    #################################
    def set_flow_meter_dg131(self):
        """
        Build the packets variable string and send the specified packet through the web service's web socket. \n
        """
        data_group_number = '131'
        # build the packet
        packet = "{0}:{1}/{2}={3}/{4}={5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}".format(
            opcodes.flow_meter,                                 # {0}
            str(self.flow_meter.sn),                            # {1}
            opcodes.description,                                # {2}
            str(self.flow_meter.ds),                            # {3}
            opcodes.latitude,                                   # {4}
            str(self.flow_meter.la),                            # {5}
            opcodes.longitude,                                  # {6}
            str(self.flow_meter.lg),                            # {7}
            opcodes.enabled,                                    # {8}
            self.flow_meter.en,                                 # {9}
            opcodes.k_value,                                    # {10}
            self.flow_meter.bicoder.kv,                         # {11}
            opcodes.address_number,                             # {12}
            self.flow_meter.ad,                                 # {13}
        )

        self.send_ts_packet(
            _data_group_number=data_group_number,
            _variable_string=packet)

    #################################
    def verify_flow_meter_dg131(self):
        """
        Build the packets variable string and send the specified packet through the web service's web socket. \n
        """
        data_group_number = '131'

        # build tq packet
        packet = "{0}:{1}".format(
            opcodes.flow_meter,
            self.flow_meter.sn
        )
        self.send_tq_packet(
            _data_group_number=data_group_number,
            _variable_string=packet)
        try:
            parsed_packet = self.controller.data_group_packets.te_packets[data_group_number]
        except KeyError:
            e_msg = "There is no data group packet for DG" + str(data_group_number)
            raise Exception(e_msg)
        else:
            try:
                flow_meter_data = parsed_packet[str(self.flow_meter.sn)]
                # set self.data for the Flow Meter
                self.flow_meter.data = flow_meter_data
                # set self.data for the bi coder
                self.flow_meter.bicoder.data = flow_meter_data
            except KeyError:
                e_msg = "There is no data for Flow Meter: " + str(self.flow_meter.sn)
                raise Exception(e_msg)
            if self.flow_meter.data is not None:
                self.flow_meter.verify_description()
                self.flow_meter.verify_serial_number()
                self.flow_meter.verify_latitude()
                self.flow_meter.verify_longitude()
                self.flow_meter.verify_enabled_state()
                self.flow_meter.bicoder.verify_k_value()
                fm_nu_on_cn = int(flow_meter_data.get_value_string_by_key(opcodes.address_number))
                # Compare status versus what is on the controller
                if self.flow_meter.ad != fm_nu_on_cn:
                    e_msg = "Unable to verify Flow Meter {0}'s Address. Received: {1}, Expected: {2}".format(
                        str(fm_nu_on_cn),           # {0}
                        str(fm_nu_on_cn),           # {1}
                        str(self.flow_meter.ad)     # {2}
                    )
                    raise ValueError(e_msg)
                else:
                    print("Verified Flow Meter {0}'s Address: '{1}' on controller".format(
                        str(self.flow_meter.sn),     # {0}
                        str(self.flow_meter.ad)      # {1}
                    ))