from common.imports import opcodes
from common.objects.packets.base_packets import BasePackets

__author__ = 'Baseline'


class MasterValvePackets(BasePackets):

    #################################
    def __init__(self, _master_valve_object):
        """
        :param _master_valve_object:     Test Engine Master Valve object\n
        :type _master_valve_object:      common.objects.devices.mv.MasterValve
        """
        self.master_valve = _master_valve_object
        # Set the attributes

        BasePackets.__init__(self, _controller=_master_valve_object.controller)

    # |-------------------------------------------------|
    #         Packet: 111
    # |-------------------------------------------------|

    #################################
    def set_test_values_dg111(self):
        self.master_valve.ds = 'test'
        self.master_valve.la = 115.52
        self.master_valve.lg = -115.5
        self.master_valve.en = opcodes.false
        self.master_valve.no = opcodes.true
        self.master_valve.ad = 3

        self.set_master_valve_dg111()

    #################################
    def set_master_valve_dg111(self):
        """
        Build the packets variable string and send the specified packet through the web service's web socket. \n
        """
        data_group_number = '111'

        # build the packet
        packet = "{0}:{1}/{2}={3}/{4}={5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}".format(
            opcodes.master_valve,                               # {0}
            str(self.master_valve.sn),                          # {1}
            opcodes.description,                                # {2}
            str(self.master_valve.ds),                          # {3}
            opcodes.latitude,                                   # {4}
            str(self.master_valve.la),                          # {5}
            opcodes.longitude,                                  # {6}
            str(self.master_valve.lg),                          # {7}
            opcodes.enabled,                                    # {8}
            self.master_valve.en,                               # {9}
            opcodes.normally_open,                              # {10}
            self.master_valve.no,                               # {11}
            opcodes.address_number,                             # {12}
            self.master_valve.ad,                               # {13}
        )

        self.send_ts_packet(
            _data_group_number=data_group_number,
            _variable_string=packet)

    #################################
    def verify_master_valve_dg111(self):
        """
        Build the packets variable string and send the specified packet through the web service's web socket. \n
        """
        data_group_number = '111'

        # build tq packet
        packet = "{0}:{1}".format(
            opcodes.master_valve,
            self.master_valve.sn
        )
        self.send_tq_packet(
            _data_group_number=data_group_number,
            _variable_string=packet)
        try:
            parsed_packet = self.controller.data_group_packets.te_packets[data_group_number]
        except KeyError:
            e_msg = "There is no data group packet for DG" + str(data_group_number)
            raise Exception(e_msg)
        else:
            try:
                master_valve_data = parsed_packet[str(self.master_valve.sn)]
                self.master_valve.data = master_valve_data
            except KeyError:
                e_msg = "There is no data for Master Valve: " + str(self.master_valve.sn)
                raise Exception(e_msg)
            if self.master_valve.data is not None:
                self.master_valve.verify_description()
                self.master_valve.verify_serial_number()
                self.master_valve.verify_latitude()
                self.master_valve.verify_longitude()
                self.master_valve.verify_enabled_state()
                self.master_valve.verify_normally_open_state()
                # verify master_valve.nu
                mv_nu_on_cn = int( master_valve_data.get_value_string_by_key(opcodes.address_number))
                # Compare status versus what is on the controller
                if self.master_valve.ad != mv_nu_on_cn:
                    e_msg = "Unable to verify Master Valve {0}'s Address. Received: {1}, Expected: {2}".format(
                        str(mv_nu_on_cn),               # {0}
                        str(mv_nu_on_cn),               # {1}
                        str(self.master_valve.ad)       # {2}
                    )
                    raise ValueError(e_msg)
                else:
                    print("Verified Master Valve {0}'s Address: '{1}' on controller".format(
                        str(self.master_valve.sn),     # {0}
                        str(self.master_valve.ad)      # {1}
                    ))