from common.imports import opcodes
from common.objects.packets.base_packets import BasePackets

__author__ = 'Baseline'


class ZonePackets(BasePackets):

    #################################
    def __init__(self, _zone_object):
        """
        :param _zone_object:     Test Engine Zone object\n
        :type _zone_object:      common.objects.devices.zn.Zone
        """
        self.zone = _zone_object
        # Set the attributes

        BasePackets.__init__(self, _controller=_zone_object.controller)

    # |-------------------------------------------------|
    #         Packet: DG302
    # |-------------------------------------------------|

    #################################
    def set_test_values_dg101(self):
        self.zone.ds = 'test'
        self.zone.la = 115.52
        self.zone.lg = -115.5
        self.zone.en = opcodes.false
        self.zone.df = 1.5
        self.zone.ml = 1
        self.zone.kc = 0.85
        self.zone.pr = 0.45
        self.zone.ef = 65
        self.zone.rz = 0.55
        self.set_zone_dg101()

    #################################
    def set_zone_dg101(self):
        """
        Build the packets variable string and send the specified packet through the web service's web socket. \n
        """
        data_group_number = '101'

        # build the packet
        packet = "{0}:{1}/{2}={3}/{4}={5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}/{14}={15}/{16}={17}/{18}={19}/{20}={21}".format(
            opcodes.zone,                               # {0}
            str(self.zone.ad),                          # {1}
            opcodes.description,                        # {2}
            str(self.zone.ds),                          # {3}
            opcodes.latitude,                           # {4}
            str(self.zone.la),                          # {5}
            opcodes.longitude,                          # {6}
            str(self.zone.lg),                          # {7}
            opcodes.enabled,                            # {8}
            self.zone.en,                               # {9}
            opcodes.design_flow,                        # {10}
            self.zone.df,                               # {11}
            opcodes.mainline,                           # {12}
            self.zone.ml,                               # {13}
            opcodes.crop_coefficient,                   # {14}
            self.zone.kc,                               # {15}
            opcodes.precipitation_rate,                 # {16}
            self.zone.pr,                               # {17}
            opcodes.efficiency_percentage,              # {18}
            self.zone.ef,                               # {19}
            opcodes.root_zone_working_water_storage,    # {20}
            self.zone.rz,                               # {21}
        )

        self.send_ts_packet(
            _data_group_number=data_group_number,
            _variable_string=packet)

    #################################
    def verify_zone_dg101(self):
        """
        Build the packets variable string and send the specified packet through the web service's web socket. \n
        """
        data_group_number = '101'

        # build tq message
        packet = "{0}:{1}".format(
            opcodes.zone,
            self.zone.ad
        )
        self.send_tq_packet(
            _data_group_number=data_group_number,
            _variable_string=packet)
        try:
            parsed_packet = self.controller.data_group_packets.te_packets[data_group_number]
        except KeyError:
            e_msg = "There is no data group packet for DG" + str(data_group_number)
            raise Exception(e_msg)
        else:
            try:
                zone_data = parsed_packet[str(self.zone.ad)]
                self.zone.data =  zone_data
            except KeyError:
                e_msg = "There is no data for zone: " + str(self.zone.ad)
                raise Exception(e_msg)
            if self.zone.data is not None:
                self.zone.verify_description()
                self.zone.verify_serial_number()
                self.zone.verify_latitude()
                self.zone.verify_longitude()
                self.zone.verify_enabled_state()
                self.zone.verify_design_flow()
                self.zone.verify_mainline_assignment()
                self.zone.verify_crop_coefficient_value()
                self.zone.verify_precipitation_rate_value()
                self.zone.verify_root_zone_working_storage_capacity()

                # verify zone.ef
                zn_ef_on_cn = float( zone_data.get_value_string_by_key(opcodes.efficiency_percentage))
                # Compare status versus what is on the controller
                if self.zone.ef != zn_ef_on_cn:
                    e_msg = "Unable to verify Zone {0}'s zone efficency percent. Received: {1}, Expected: {2}".format(
                        str(self.zone.ad),       # {0}
                        str(zn_ef_on_cn),          # {1}
                        str(self.zone.ef)        # {2}
                    )
                    raise ValueError(e_msg)
                else:
                    print("Verified Zone {0}'s design flow: '{1}' on controller".format(
                        str(self.zone.ad),     # {0}
                        str(self.zone.df)      # {1}
                    ))

                # verify zone.fd
                zn_fd_on_cn = str( zone_data.get_value_string_by_key(opcodes.learn_flow_date))
                # Compare status versus what is on the controller
                if self.zone.fd != zn_fd_on_cn:
                    e_msg = "Unable to verify Zone {0}'s zone Learn Flow Date. Received: {1}, Expected: {2}".format(
                        str(self.zone.ad),       # {0}
                        str(zn_fd_on_cn),        # {1}
                        str(self.zone.fd)        # {2}
                    )
                    raise ValueError(e_msg)
                else:
                    print("Verified Zone {0}'s Learn Flow Date: '{1}' on controller".format(
                        str(self.zone.ad),     # {0}
                        str(self.zone.fd)      # {1}
                    ))

                # verify zone.fn
                zn_fn_on_cn = float( zone_data.get_value_string_by_key(opcodes.learn_flow_gpm))
                # Compare status versus what is on the controller
                if self.zone.fn != zn_fn_on_cn:
                    e_msg = "Unable to verify Zone {0}'s zone Learn Flow Date GPM. Received: {1}, Expected: {2}".format(
                        str(self.zone.ad),       # {0}
                        str(zn_fn_on_cn),        # {1}
                        str(self.zone.fn)        # {2}
                    )
                    raise ValueError(e_msg)
                else:
                    print("Verified Zone {0}'s Learn Flow Date GPM: '{1}' on controller".format(
                        str(self.zone.ad),     # {0}
                        str(self.zone.fn)      # {1}
                    ))