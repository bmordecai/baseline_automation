
import common.objects.base_classes.opcode_parser as opcode_parser
import common.imports.opcodes as opcodes
import status_parser

__author__ = 'Kent'

"""
This module will verify the structure of a packet and return a list of key values if the packet has the correct
parameters for the given data group.
"""
# TODO need to add the ability to parse large packets when SL=FA

def verify_packet(packet):
    """
    This method is the driver for this module. This method does the following:
    1. Parse a packet string and create a packet object representing the given packet string
    2. Creates the key values list object which contains the test engine test commands
    3. Returns the list of valid key values
    :param packet: string representing the packet
    :return: list of valid key value objects
    """

    # parse the packet
    parsed_packet = opcode_parser.Packet(packet)

    # check to see if the packet is valid
    if parsed_packet.valid:
        # get the key values object list for the packet
        key_values_dictionary = get_key_values_object_list_from_packet(parsed_packet=parsed_packet)
    else:
        key_values_dictionary = None

    # verify the key_values_list is not empty
    if key_values_dictionary is None:
        e_msg = "verify_te_packet: Invalid packet: key_values_dictionary is NONE"
        raise Exception(e_msg)

    # Return None if the packet is not valid, other wise return the key_values_dictionary
    return key_values_dictionary


def get_key_values_object_list_from_packet(parsed_packet):
    """
    This method does the following:
    1. Iterates through the parsed_packet object
    2. Verifies that certain parts of the packet exist
    3. Calls create_key_values_object_list for a data group and its parameters
    4. Verifies that the created key values object is not empty
    5. Returns a valid key values object list
    :param parsed_packet: parsed packet object
    :return: list of valid key value objects
    """
    parsed_key_values = None
    if len(parsed_packet.message.commands) > 0:
        # get commands
        commands = parsed_packet.message.commands
        # iterate through the commands
        for command in commands:
            if command.opcode != 'TE':
                e_msg = "verify_te_packet: Invalid opcode - " + command.opcode + ". Should be 'TE' for packet verifier"
                raise Exception(e_msg)
            # get the list of command parameters
            parameters = command.getParameters()
            parameters_list = parameters.parameter_list
            # verify the parameters list has values in it
            if len(parameters_list) > 0:
                # verify the first parameter exists and is of type 'DG'
                first_parameter = parameters_list[0]
                if first_parameter.ID == 'DG':
                    # get the first parameter value list
                    first_parameter_value_list = first_parameter.value.value_list
                    if len(first_parameter_value_list) > 0:
                        # get the value of the Data Group
                        data_group = first_parameter_value_list[0]
                        # send the value of the Data Group with the parameter list to create_key_objects_list method
                        parsed_key_values = create_key_values_dictionary(data_group=data_group, parameters=parameters)
                    else:
                        e_msg = "verify_te_packet: Error - first parameter value list is EMPTY"
                        raise Exception(e_msg)
                else:
                    e_msg = "verify_te_packet: Error - first parameter is not 'DG'"
                    raise Exception(e_msg)
    return parsed_key_values


def create_key_values_dictionary(parameters, data_group):
    """
    Creates a key_values dictionary for the given data group
    :param parameters: Parameters of the given data group
    :return: key value dictionary of the packet parameters translated into the test engine commands
    """
    # Initialize variables

    sequence_last_detected = False
    done_done_detected = False

    parameter_to_key_value_dictionary = create_parameter_to_key_value_dictionary(data_group=data_group)
    valid_subproperty_list = list()
    subproperty_dictionary = {}
    if data_group in ['403']:
        valid_subproperty_list = [opcodes.type,
                            opcodes.moisture_sensor,
                            opcodes.enabled,
                            opcodes.event_switch,
                            opcodes.pressure_sensor_ps,
                            opcodes.empty_condition_moisture_limit,
                            opcodes.switch_empty_condition,
                            opcodes.empty_condition_pressure_limit,
                            opcodes.empty_condition_minimum_empty_time
                            ]

    if parameter_to_key_value_dictionary is None:
        # throw an exception if there is not a method to construct a key values dictionary
        e_msg = "verify_te_packet error: DG " + data_group + " is under construction"
        raise Exception(e_msg)


    object_type = parameter_to_key_value_dictionary["object_type"]
    if object_type is None:
        e_msg = "verify_te_packet error: object_type from parameter_to_key_value dictionary is None"
        raise Exception(e_msg)

    object_address_key = parameter_to_key_value_dictionary["object_address_key"]
    if object_address_key is None:
        e_msg = "verify_te_packet error: object_address_key from parameter_to_key_value dictionary is None"
        raise Exception(e_msg)

    object_list = {}
    # iterate through the parameter list
    for parameter in parameters.parameter_list:
        if parameter.ID == opcodes.data_group:
            continue

        #  verify ^SN
        if parameter.ID == opcodes.sequence_last:
            sequence_last_detected = True
            continue

        # verify ^DN
        if parameter.ID == opcodes.done:
            done_done_detected = True
            continue

        # verify ^object_type
        if parameter.ID == object_type:
            key_values_dictionary = {}
            object_address_value = parameter.value.value_list[0]
            key_values_dictionary[object_address_key] = object_address_value
            property_list = parameter.properties.property_list
            if len(property_list) > 0:
                for prop in property_list:
                    if prop.ID in parameter_to_key_value_dictionary:
                        key_value = parameter_to_key_value_dictionary.get(prop.ID)

                        if data_group in ['403'] and prop.ID in opcodes.empty_event_number:
                            subproperty_dictionary = {}
                            key_value = prop.value.value_list[0]
                            for subproperty in prop.subproperties.subproperty_list:
                                if subproperty.ID in valid_subproperty_list:
                                    subproperty_dictionary[subproperty.ID] = subproperty.value.value_list[0] if len(subproperty.value.value_list) > 0 else None
                                elif subproperty.ID is not None:
                                    e_msg = "verify_te_packet: Invalid sub property: " + subproperty.ID
                                    raise Exception(e_msg)
                            key_values_dictionary[key_value] = subproperty_dictionary
                        else:
                            # Set value to None value_list is empty (as when we get /FS= )
                            key_values_dictionary[key_value] = prop.value.value_list[0] if len(prop.value.value_list) > 0 else None
                    else:
                        e_msg = "verify_te_packet: Invalid property: " + prop.ID
                        raise Exception(e_msg)

            object_list[object_address_value] = key_values_dictionary
        else:
            e_msg = "verify_te_packet: Invalid parameter ID: " + parameter.ID
            raise Exception(e_msg)

    # verify SL
    if not sequence_last_detected and data_group not in['332']:
        e_msg = "verify_te_packet: ^SL not detected."
        raise Exception(e_msg)

    # verify DN
    if not done_done_detected:
        e_msg = "verify_te_packet: ^DN not detected."
        raise Exception(e_msg)

    if len(object_list) > 0:
        parsed_key_values_dictionary = {}
        for key, value in object_list.iteritems():
            # create the key value pair string based off of the key values dictionary
            if data_group in ['403']:
                return_empty_condition_dictionary = {}
                for sub_key, sub_val in value.iteritems():
                    if type(sub_val) is dict:
                        key_val_pair_string = create_key_value_pair_string(sub_val)
                        parsed_sub_values = status_parser.KeyValues(key_val_pair_string)
                        return_empty_condition_dictionary[sub_key] = parsed_sub_values
                parsed_key_values_dictionary[key] = return_empty_condition_dictionary
            else:
                key_value_pair_string = create_key_value_pair_string(value)

                # send the key_value_pair_string through the KeyValues parser
                parsed_key_values = status_parser.KeyValues(key_value_pair_string)
                parsed_key_values_dictionary[key] = parsed_key_values

        return parsed_key_values_dictionary
    else:
        e_msg = "verify_te_packet: data packet " + data_group + " contains no data for parameter: " + object_type
        raise Exception(e_msg)


def create_key_value_pair_string(key_values_dictionary):
    """
    create a key value pair string given a key values dictionary
    :param key_values_dictionary: dictionary of key values
    :return: string representing the key values
    """
    key_value_pair_string = ""

    # iterate through the key values in the dictionary
    for key in key_values_dictionary:
        value = key_values_dictionary[key]
        # Map a None value to /KEY=, anything else to /KEY=VALUE
        if value is not None:
            key_value_pair_string = key_value_pair_string + key + "=" + str(value) + ","
        else:
            key_value_pair_string = key_value_pair_string + key + "=,"

    # remove last comma
    key_value_pair_string = key_value_pair_string[:-1]

    # return key_value_string
    return key_value_pair_string


def create_parameter_to_key_value_dictionary(data_group):

    data_group_parameter_to_test_engine_parameter_dictionary = {}

    # object_type is the name of the parameter id for the data group
    # object_address_key is 'OBJ_AD' as a default. Otherwise,
    # it is the address key needed by the test engine object verifier
    data_group_parameter_to_test_engine_parameter_dictionary["object_address_key"] = 'OBJ_AD'

    if data_group == '302':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.moisture_sensor
        data_group_parameter_to_test_engine_parameter_dictionary["object_address_key"] = opcodes.serial_number
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.description] = opcodes.description
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.latitude] = opcodes.latitude
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.longitude] = opcodes.longitude
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.enabled] = opcodes.enabled
    elif data_group == '101':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.zone
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.description] = opcodes.description
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.enabled] = opcodes.enabled
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.latitude] = opcodes.latitude
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.longitude] = opcodes.longitude
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.design_flow] = opcodes.design_flow
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.serial_number] = opcodes.serial_number
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.mainline] = opcodes.mainline
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.crop_coefficient] = opcodes.crop_coefficient
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.precipitation_rate] = opcodes.precipitation_rate
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.efficiency_percentage] = opcodes.efficiency_percentage
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.root_zone_working_water_storage] = opcodes.root_zone_working_water_storage
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.learn_flow_date] = opcodes.learn_flow_date
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.learn_flow_gpm] = opcodes.learn_flow_gpm
    elif data_group == '111':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.master_valve
        data_group_parameter_to_test_engine_parameter_dictionary["object_address_key"] = opcodes.serial_number
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.description] = opcodes.description
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.latitude] = opcodes.latitude
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.longitude] = opcodes.longitude
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.enabled] = opcodes.enabled
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.address_number] = opcodes.address_number
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.normally_open] = opcodes.normally_open
    elif data_group == '131':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.flow_meter
        data_group_parameter_to_test_engine_parameter_dictionary["object_address_key"] = opcodes.serial_number
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.description] = opcodes.description
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.latitude] = opcodes.latitude
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.longitude] = opcodes.longitude
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.enabled] = opcodes.enabled
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.address_number] = opcodes.address_number
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.k_value] = opcodes.k_value
    elif data_group == '332':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.flow_station
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.description] = opcodes.description
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.enabled] = opcodes.enabled
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.ip_address] = opcodes.ip_address
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.latitude] = opcodes.latitude
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.longitude] = opcodes.longitude
    elif data_group == '402':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.water_source
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.description] = opcodes.description
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.enabled] = opcodes.enabled
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.priority] = opcodes.priority
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.monthly_water_budget] = opcodes.monthly_water_budget
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.shutdown_on_over_budget] = opcodes.shutdown_on_over_budget
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.water_rationing_enable] = opcodes.water_rationing_enable
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.point_of_control] = opcodes.point_of_control
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.flow_station] = opcodes.flow_station
    elif data_group == '403':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.water_source
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.empty_event_number] = opcodes.empty_event_number
    elif data_group == '412':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.point_of_control
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.description] = opcodes.description
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.enabled] = opcodes.enabled
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.target_flow] = opcodes.target_flow
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.flow_meter] =  opcodes.flow_meter
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.pump_on_poc] = opcodes.pump_on_poc
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.high_flow_limit] = opcodes.high_flow_limit
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.shutdown_on_high_flow] = opcodes.shutdown_on_high_flow
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.master_valve] = opcodes.master_valve
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.unscheduled_flow_limit] = opcodes.unscheduled_flow_limit
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.unscheduled_flow_shutdown] = opcodes.unscheduled_flow_shutdown
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.pressure_sensor_ps] = opcodes.pressure_sensor_ps
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.high_pressure_limit] = opcodes.high_pressure_limit
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.high_pressure_shutdown] = opcodes.high_pressure_shutdown
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.low_pressure_limit] = opcodes.low_pressure_limit
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.low_pressure_shutdown] = opcodes.low_pressure_shutdown
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.high_flow_limit] = opcodes.high_flow_limit
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.flow_station] = opcodes.flow_station
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.mainline] = opcodes.mainline
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.flow_station_mainline] = opcodes.flow_station_mainline
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.group] = opcodes.group
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.priority] = opcodes.priority
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.ignore_unscheduled_flow] = opcodes.ignore_unscheduled_flow
    elif data_group == '422':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.mainline
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.description] = opcodes.description
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.enabled] = opcodes.enabled
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.target_flow] = opcodes.target_flow
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.fill_time] = opcodes.fill_time
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.stable_flow_pressure_value] = opcodes.stable_flow_pressure_value
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.use_pressure_for_stable_flow] = opcodes.use_pressure_for_stable_flow
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.use_time_for_stable_flow] = opcodes.use_time_for_stable_flow
        data_group_parameter_to_test_engine_parameter_dictionary[ opcodes.standard_variance_shutdown] =  opcodes.standard_variance_shutdown
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.standard_variance_limit] = opcodes.standard_variance_limit
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.mainline_variance_limit] = opcodes.mainline_variance_limit
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.mainline_variance_shutdown] = opcodes.mainline_variance_shutdown
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.delay_first_zone] = opcodes.delay_first_zone
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.delay_for_next_zone] = opcodes.delay_for_next_zone
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.delay_after_zone] = opcodes.delay_after_zone
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.number_of_zones_to_delay] = opcodes.number_of_zones_to_delay
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.limit_zones_by_flow] = opcodes.limit_zones_by_flow
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.use_advanced_flow] = opcodes.use_advanced_flow
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.high_variance_limit_tier_one] = opcodes.high_variance_limit_tier_one
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.high_variance_limit_tier_two] =opcodes.high_variance_limit_tier_two
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.high_variance_limit_tier_three] = opcodes.high_variance_limit_tier_three
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.high_variance_limit_tier_four] = opcodes.high_variance_limit_tier_four
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.low_variance_limit_tier_one] = opcodes.low_variance_limit_tier_one
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.low_variance_limit_tier_two] =opcodes.low_variance_limit_tier_two
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.low_variance_limit_tier_three] = opcodes.low_variance_limit_tier_three
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.low_variance_limit_tier_four] = opcodes.low_variance_limit_tier_four
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.zone_high_variance_detection_shutdown] = opcodes.zone_high_variance_detection_shutdown
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.zone_low_variance_detection_shutdown] =  opcodes.zone_low_variance_detection_shutdown
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.pipe_fill_units] = opcodes.pipe_fill_units
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.flow_station] = opcodes.flow_station
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.priority] = opcodes.priority
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.mainline] = opcodes.mainline
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.point_of_control] = opcodes.point_of_control
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.use_dynamic_flow_allocation] = opcodes.use_dynamic_flow_allocation
    
    elif data_group == '500':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.flow_station
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.description] = opcodes.description
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.latitude] = opcodes.latitude
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.longitude] = opcodes.longitude
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.max_counts] = opcodes.max_counts
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.use_dynamic_flow_allocation] = opcodes.use_dynamic_flow_allocation
    elif data_group == '501':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.water_source
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.controller] = opcodes.controller
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.water_source] = opcodes.water_source
    elif data_group == '511':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.point_of_control
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.controller] = opcodes.controller
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.point_of_control] = opcodes.point_of_control
    elif data_group == '521':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.mainline
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.controller] = opcodes.controller
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.mainline] = opcodes.mainline
    elif data_group == '570':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.mac_address
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.controller] = opcodes.controller
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.serial_number] = opcodes.serial_number
    elif data_group == '571':
        data_group_parameter_to_test_engine_parameter_dictionary["object_type"] = opcodes.controller
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.serial_number] = opcodes.serial_number
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.address_number] = opcodes.address_number
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.description] = opcodes.description
        data_group_parameter_to_test_engine_parameter_dictionary[opcodes.enabled] = opcodes.enabled

    if len(data_group_parameter_to_test_engine_parameter_dictionary) == 1:
        return None
    else:
        return data_group_parameter_to_test_engine_parameter_dictionary

