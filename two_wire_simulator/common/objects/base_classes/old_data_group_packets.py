
"""
This class will handle data group operations and verifications
"""
import common.imports.opcodes as opcodes
import common.objects.base_classes.verify_te_packet as verify_te_packet
from threading import Thread, Event
import requests
import os
import time
from collections import deque
import hashlib


class OldDataGroupPackets(object):

    def __init__(self, controller, test_user_name, test_password, session_id=''):
        """
        :param controller:      controller which this DataGroupPackets objects is attached to
        :type                   common.objects.controllers.bl_32.BaseStation3200|
                                common.objects.controllers.bl_fs.FlowStation  \n
        :param test_user_name: test user name for connecting to Base Manager \n
        :type  test_user_name: str \n
        :param test_password: test password for connecting to Base Manager \n
        :type  test_password: str \n
        """
        self.controller = controller
        self.user_name = test_user_name
        self.password = hashlib.md5(test_password).hexdigest()
        self.log_file = None
        self.session_id = session_id
        self.te_packets = {}
        self.read_data_thread = None
        self.queue = deque()

        self.log_event = Event()
        self.wait_time = 2
        # Create thread for reading data from the second com port. Do not start yet.
        self.read_data_thread = Thread(target=self.read_from_com_port, args=(self.log_event, self.queue))

        # Create the file_path to the log_file for the current controller
        path = os.path.abspath(__file__).partition('common')
        self.log_file_path = os.path.join(path[0], 'common/log_files/' + 'controller_' + self.controller.mac + '_log.txt')
        if self.user_name != '' and self.password != '':
            if not self.session_id:
                payload = {"username":  self.user_name, "password": self.password}
                r = requests.post(url='https://p3.basemanager.net/baseservice2/login', json=payload)
                response_dictionary = r.json()
                try:
                    self.session_id = response_dictionary['sessionId']
                except KeyError:
                    e_msg = "Login authentication failed. Invalid username and password. Username: " + self.user_name + " "
                    e_msg += "Password " + self.password

    #################################
    def start_packet_logging(self):
        """
        This method opens the log file in read/write mode, sets the log_event to true so that the log_thread
        records data, and starts the logging thread.
        """
        self.log_file = open(self.log_file_path, 'w+')
        self.log_event.set()
        # set daemon to true so that the thread terminates when the main thread terminates
        self.read_data_thread.setDaemon(True)
        self.read_data_thread.start()

    #################################
    def resume_packet_logging(self):
        """
        This method resumes logging, deletes the data in the log_file, places the file pointer at the start of the file,
        and sets the log_event flag to true so the read_data_thread continues recording data
        """
        self.log_file.truncate(0)
        self.log_file.seek(0)
        self.log_event.set()
        self.te_packets = {}

    #################################
    def stop_packet_logging(self):
        """
        This method waits for the given wait time so that controller can finish sending data. It then clears the
        log_event flag so that the read_data_thread stops recording data.
        """
        time.sleep(self.wait_time)
        self.log_event.clear()

    #################################
    def parse_packet_log_file(self):
        """
        This method removes data from the queue and writes it to the log file which is then parsed and TE packets are
         extracted and placed in the te_packets dictionary if they are valid. Valid TE Packets are converted from
         a string representation to a key value pair object which is then used by the test engine object verifiers.
        """
        if self.log_file is not None and not self.log_file.closed:
            while True:
                try:
                    line = self.queue.popleft()
                    self.log_file.write(line)
                    self.log_file.write('\n')
                except IndexError:
                    break
            self.log_file.flush()
            self.log_file.seek(0)
            self.te_packets = {}
            for line in self.log_file:
                partitioned_line = line.partition('TX=')
                packet = partitioned_line[2]
                parsed_packet = None

                if "TE^DG:" in line:
                    dg = packet.partition("/")
                    dg = dg[0].partition(":")
                    data_group = dg[2]

                    try:
                        parsed_packet = verify_te_packet.verify_packet(packet)
                    except Exception, e:
                        print e.message

                    #  This will place the most recent valid packets that
                    # appear in the log file in the te_packets dictionary
                    if parsed_packet is not None:
                        self.te_packets[data_group] = parsed_packet
        else:
            e_msg = "Log file does not exist or is not open. It must be open in order for the " \
                    "log_data_thread to write to it."
            raise Exception(e_msg)

    #################################
    def close_logging_connections(self):
        """
        This method stops the read_data_thread, flushes the file buffer, closes the log_file, and then closes the
        serial connection on serial port 2
        :return:
        """
        # self.read_data_thread.join(0.0)
        if self.log_file is not None and  not self.log_file.closed:
            self.log_file.flush()
            self.log_file.close()
        if self.controller.ser2 is not None:
            if self.controller.ser2.serial_conn.isOpen():
                self.controller.ser2.close_connection()
    #################################
    def close_logging_file(self):
        self.log_file.flush()
        self.log_file.close()

    #################################
    def read_from_com_port(self, log_event, data_queue):
        """
        This method reads data from the com port.
        :param log_event: This is an event object which tells the thread to add data to the data queue or stop adding
        data
        :param data_queue: This is a queue that captures data as it comes through on serial port 2
        """
        if self.controller.ser2 is not None:
            ser2 = self.controller.ser2
            # This assumes that the log file has been opened prior to starting this method

            while True:
                if log_event.isSet():
                    new_line = ser2.recvln()
                    if new_line is not None:
                        data_queue.append(new_line)

    #################################
    def send_ts_packet(self, packet, controller_mac='',  session_id=''):
        """
        This method sends a ts packet to the given controller using a session_id by means of a HTTP call. This
        method opens table programming, sends the TS packet, and then closes table programming.
        :param packet: TQ packet to be send to the controller requesting data \n
        :param controller_mac: Controller to which this packet is to be sent \n
        :type controller_mac: string \n
        :param session_id: Valid session id to be used by the HTTP call \n
        :type session_id: String \n
        """

        if controller_mac == '':
            controller_mac = self.controller.mac

        if session_id == '':
            session_id = self.session_id

        enter_programming_packet = self.build_tp_packet_enter_table_programming()
        exit_programming_packet = self.build_tp_packet_exit_table_programming()

        enter_table_programming = 'https://p3.basemanager.net/baseservice2/sendcommand?sessionid=' + session_id
        enter_table_programming += '&controllermac=' + controller_mac + "&message=" + enter_programming_packet

        exit_table_programming = 'https://p3.basemanager.net/baseservice2/sendcommand?sessionid=' + session_id
        exit_table_programming += '&controllermac=' + controller_mac + "&message=" + exit_programming_packet

        packet_url = 'https://p3.basemanager.net/baseservice2/sendcommand?sessionid=' + session_id
        packet_url += '&controllermac=' + controller_mac + '&message=' + packet

        # send the post requests and throw exceptions if the status code is not between 200 and 299.
        r1 = requests.post(url=enter_table_programming, data={})
        if r1.status_code not in xrange(200, 300):
            raise Exception ("Enter Table programming failed. Received status code: " + str(r1.status_code))

        r2 = requests.post(url=packet_url, data={})
        if r2.status_code not in xrange(200, 300):
            raise Exception ("Send TS packet failed. Received status code: " + str(r2.status_code))

        r3 = requests.post(url=exit_table_programming, data={})
        if r3.status_code not in xrange(200, 300):
            raise Exception ("Exit Table programming failed. Received status code: " + str(r3.status_code))

        time.sleep(self.wait_time)

    #################################
    def send_tq_packet(self, packet, controller_mac='',  session_id=''):
        """
        This method sends a TQ packet to a controller using the given controller_mac and session_id. If none are give,
        the packet is sent to the controller set at initialization.
        :param packet: TQ packet to be send to the controller requesting data \n
        :param controller_mac: Controller to which this packet is to be sent \n
        :type controller_mac: string \n
        :param session_id: Valid session id to be used by the HTTP call \n
        :type session_id: String \n
        :return:
        """
        if controller_mac == '':
            controller_mac = self.controller.mac

        if session_id == '':
            session_id = self.session_id

        packet_url = 'https://p3.basemanager.net/baseservice2/sendcommand?sessionid=' + session_id
        packet_url += '&controllermac=' + controller_mac + "&message=" + packet

        r2 = requests.post(url=packet_url, data={})
        if r2.status_code not in xrange(200, 300):
            raise Exception ("Send TQ packet failed. Received status code: " + str(r2.status_code))
        time.sleep(self.wait_time)

    #################################
    def build_tp_packet_enter_table_programming(self, header_id=900):
        """
        This will build a TP packet for enter table programming
        :param header_id: This is the header id for the packet \n
        :type header_id: int
        :return: message \n
        :rtype: string \nr
        """
        message = "TP^MO:ET/{0}={1}".format(
            opcodes.message_id,
            str(header_id))
        return message

    def build_tp_packet_exit_table_programming(self, header_id=950):
        """
        This will build a TP packet to exit table programming
        :param header_id: This is the header id for the packet
        :type header_id: int
        :return: message \n
        :rtype: string \n
        """
        message = "TP^MO:EX/{0}={1}".format(
            opcodes.message_id,
            str(header_id))
        return message

    def build_tu_packet_exit_table_programming_success(self, header_id=951):
        """
        This will build a tu packet for a TU response from a controller when exit table programming is successful
        :param header_id: This is the header id for the packet
        :type header_id: int
        :return: message \n
        :rtype: string \n
        """
        message = "TU^MO:EX/{0}={1}^{2}:{3}/{4}=Success".format(
            opcodes.message_id,
            str(header_id),
            opcodes.status,
            opcodes.status_success,
            opcodes.message_text
        )
        return message

    def build_tu_packet_enter_table_programming_success(self, header_id=952):
        """
        This will create a TU packet for enter table programming success
        :param header_id: This is the header id for the packet
        :type header_id: int
        :return: message \n
        :rtype: string \n
        """
        message = "TU^MO:ET/{0}={1}^{2}:{3}/{4}=Success^DN:DN".format(
            opcodes.message_id,
            str(header_id),
            opcodes.status,
            opcodes.status_success,
            opcodes.message_text
        )
        return message

    ##############
    # Packet 302 #
    ##############
    def build_tq_packet_dg_302_get_all_moisture_sensors(self, header_id=599):
        """
        This will create a TQ packet string for dg 302 requesting all moisture sensors
        :param header_id: This is the header id for the packet
        :type header_id: int
        :return: message \n
        :rtype: string \n
        """
        message = "TQ^DG:302/{0}={1}^{2}:AL".format(
            opcodes.message_id,
            str(header_id),
            opcodes.moisture_sensor
        )
        return message

    def build_tq_packet_dg_302_get_single_moisture_sensor(self, moisture_sensor, header_id=600):
        """
        This will create a TQ packet string for DG302 for getting a single moisture sensor
        :param header_id: This is the header id for the packet
        :param moisture_sensor: Test engine Moisture Sensor object with set attributes
        :type moisture_sensor: common.objects.devices.ms.MoistureSensor \nt
        :return: message \n
        :rtype: string \n
        """
        message = "TQ^DG:302/{0}={1}^{2}:{3}".format(
            opcodes.message_id,
            str(header_id),
            opcodes.moisture_sensor,
            moisture_sensor.sn
        )
        return message

    def build_ts_packet_dg_302_single_moisture_sensor(self, moisture_sensor_obj, header_id=500):
        """
        This will build a TS or TE packet based off of the packet_type. The method will use the given test engine
        moisture sensor object.
        :param header_id: This is the header id for the packet
        :type header_id :int \n
        :param moisture_sensor_obj: Test Engine Moisture sensor object with set attributes
        :type moisture_sensor_obj: common.objects.devices.ms.MoistureSensor \n
        :return: message \n
        :rtype: string \n
        """

        # build the message
        message = "{0}^DG:302/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}".format(
            "TS",                           # {0}
            opcodes.message_id,             # {1}
            str(header_id),                 # {2}
            opcodes.header_sequence,        # {3}
            opcodes.moisture_sensor,        # {4}
            str(moisture_sensor_obj.sn),    # {5}
            opcodes.description,            # {6}
            str(moisture_sensor_obj.ds),    # {7}
            opcodes.latitude,               # {8}
            str(moisture_sensor_obj.la),    # {9}
            opcodes.longitude,              # {10}
            str(moisture_sensor_obj.lg),    # {11}
            opcodes.enabled,                # {12}
            moisture_sensor_obj.en,         # {13}
        )
        return message

    def verify_packet_dg_302_key_values(self, parsed_packet, test_engine_moisture_sensor):
        """
        This method verifies the key values for packet 302
        :param parsed_packet:
        :param test_engine_moisture_sensor: Test Engine Moisture sensor object with set attributes
        :type test_engine_moisture_sensor: common.objects.devices.ms.MoistureSensor \n

        """
        test_engine_moisture_sensor.data = parsed_packet[test_engine_moisture_sensor.sn]
        if test_engine_moisture_sensor.data is not None:
            test_engine_moisture_sensor.verify_description()
            test_engine_moisture_sensor.verify_serial_number()
            test_engine_moisture_sensor.verify_latitude()
            test_engine_moisture_sensor.verify_longitude()

    ##############
    # Packet 402 #
    ##############

    def build_tq_packet_dg_402_get_all_water_sources(self, header_id=500):
        """
        This will create a TQ packet string for dg 402 requesting all water sources
        :param header_id: This is the header id for the packet
        :type header_id :int \n
        :return: message
        :rtype: string \n
        """
        message = "TQ^DG:402/{0}={1}^{2}:AL".format(
            opcodes.message_id,
            str(header_id),
            opcodes.water_source
        )
        return message

    def build_tq_packet_dg_402_get_single_water_source(self, water_source, header_id=600):
        """
        This will create a TQ packet string for DG402 for getting a single water source
        :param header_id: This is the header id for the packet
        :type header_id :int \n
        :param water_source: Test engine object with all the parameters set needed to build a TS packet\n
        :type water_source: common.objects.programming.ws.WaterSource \n
        :return: message
        :rtype: string \n
        """
        message = "TQ^DG:402/{0}={1}^{2}:{3}".format(
            opcodes.message_id,
            str(header_id),
            opcodes.water_source,
            str(water_source.ad)
        )
        return message

    def build_ts_packet_dg_402_single_water_source(self, water_source_obj, header_id=700):
        """
        This will build a TS using the given test engine water source object.
        :param header_id: This is the header id for the packet\n
        :type header_id :int \n
        :param water_source_obj: Test engine object with all the parameters set needed to build a TS packet\n
        :type water_source_obj: common.objects.programming.ws.WaterSource \n
        :return: message \n
        :rtype: string \n
        """
        # set the type for the packet header
        packet_type = 'TS'
        # build the message
        message = "{0}^DG:402/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}/{14}={15}/{16}={17}".format(
            packet_type,                        # {0}
            opcodes.message_id,                 # {1}
            str(header_id),                     # {2}
            opcodes.header_sequence,            # {3}
            opcodes.water_source,               # {4}
            str(water_source_obj.ad),           # {5}
            opcodes.description,                # {6}
            str(water_source_obj.ds),           # {7}
            opcodes.enabled,                    # {8}
            water_source_obj.en,                # {9}
            opcodes.monthly_water_budget,       # {10}
            str(water_source_obj.wb),           # {11}
            opcodes.water_rationing_enable,     # {12}
            str(water_source_obj.wr),           # {13}
            opcodes.shutdown_on_over_budget,    # {14}
            water_source_obj.ws,                # {15}
            opcodes.priority,                   # {16}
            str(water_source_obj.pr),           # {17}
        )

        if self.controller.controller_type == '32':
            message += "/{0}={1}/{2}={3}".format(
                opcodes.flow_station,           # {0}
                water_source_obj.sh,            # {1}
                opcodes.point_of_control,       # {2}
                water_source_obj.pc             # {3}
            )

        return message

    def verify_packet_dg_402_key_values(self, parsed_packet, test_engine_water_source):
        """
        :param parsed_packet:                   Packet represented by a KeyValue pair objects. \n
        :param test_engine_water_source:        Moisture Sensor Object. \n
        :type test_engine_water_source:         common.objects.programming.ws.WaterSource \n
        """
        address_string = str(test_engine_water_source.ad)
        test_engine_water_source.data = parsed_packet[address_string]
        if test_engine_water_source.data is not None:
            test_engine_water_source.verify_description()
            test_engine_water_source.verify_enabled_state()
            test_engine_water_source.verify_priority()
            # this verifies both water_budget and water_budget_shutdown
            test_engine_water_source.verify_monthly_budget()

            test_engine_water_source.verify_water_rationing()

            if self.controller.controller_type == '32':
                test_engine_water_source.verify_point_of_control()
                test_engine_water_source.verify_share_with_flow_station()

    ###############################
    # Packet 412 Point of Control #
    ###############################

    def build_tq_packet_dg_412_get_all_points_of_control(self, header_id=400):
        """
        This will create a TQ packet string for dg 412 requesting all points of control
        :param header_id: This is the header id for the packet \n
        :type header_id: int \n
        :return: message \n
        :rtype: string \n
        """
        message = "TQ^DG:412/{0}={1}^{2}:AL".format(
            opcodes.message_id,
            str(header_id),
            opcodes.point_of_control
        )
        return message

    def build_tq_packet_dg_412_get_single_point_of_control(self, point_of_control, header_id=401):
        """
        This will create a TQ packet string for DG412 for getting a single point of control \n
        :param header_id: This is the header id for the packet \n
        :type header_id: int \n
        :param point_of_control: point of control test engine object \n
        :type point_of_control: common.objects.programming.point_of_control.PointOfControl \n
        :return: message \n
        :rtype: string \n
        """
        message = "TQ^DG:412/{0}={1}^{2}:{3}".format(
            opcodes.message_id,
            str(header_id),
            opcodes.point_of_control,
            str(point_of_control.ad)
        )
        return message

    def build_ts_packet_dg_412_point_of_control(self, point_of_control_obj, header_id=701):
        """
        This will build a TS or TE packet based off of the packet_type. The method will use the given test engine
        point of control object.
        :param header_id: This is the header id for the packet \n
        :type header_id :int \n
        :param point_of_control_obj: Test engine object with all the parameters set needed to build a TS packet \n
        :type point_of_control_obj: common.objects.programming.point_of_control.PointOfControl \n
        :return: message \n
        :rtype: string \n
        """

        # When references to devices are stored in the Test Engine POC, they are stored by the key in the
        # device dictionary of the POC. These keys are numeric, 1..n. When setting a value on DG 412 for a device,
        # the Serial Number is used. To get the Serial number, the syntax:
        #
        #   object_dictionary[object_dictionary_key].sn (where object_dictionary_key is a numeric value 1..n)
        #
        # is used. The serial number is set to a variable which is used to create the TS packet for DG412. This process
        # is followed in the code below for the Flow Meter, Pump, Master Valve, and Pressure Sensor attached to the
        # Point of Control.

        flow_meter_serial_number = ''
        if point_of_control_obj.fm is not None:
            flow_meter_serial_number = point_of_control_obj.controller.flow_meters[point_of_control_obj.fm].sn

        pump_serial_number = ''
        if point_of_control_obj.pp is not None:
            pump_serial_number = point_of_control_obj.controller.pumps[point_of_control_obj.pp].sn

        master_valve_serial_number = ''
        if point_of_control_obj.mv is not None:
            master_valve_serial_number = point_of_control_obj.controller.master_valves[point_of_control_obj.mv].sn

        pressure_sensor_serial_number = ''
        if point_of_control_obj.ps is not None:
            pressure_sensor_serial_number = point_of_control_obj.controller.pressure_sensors[point_of_control_obj.ps].sn

        # set the type for the packet header
        packet_type = 'TS'
        # build the message
        message =   "{0}^DG:412/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}/{14}={15}" \
                    "/{16}={17}/{18}={19}/{20}={21}/{22}={23}/{24}={25}/{26}={27}/{28}={29}/{30}={31}" \
                    "/{32}={33}/{34}={35}/{36}={37}".format(
            packet_type,                        # {0}
            opcodes.message_id,                 # {1}
            str(header_id),                     # {2}
            opcodes.header_sequence,            # {3}
            opcodes.point_of_control,           # {4}
            str(point_of_control_obj.ad),       # {5}
            opcodes.description,                # {6}
            str(point_of_control_obj.ds),       # {7}
            opcodes.enabled,                    # {8}
            str(point_of_control_obj.en),       # {9}
            opcodes.target_flow,                # {10}
            str(point_of_control_obj.fl),       # {11}
            opcodes.flow_meter,                 # {12}
            str(flow_meter_serial_number),      # {13}
            opcodes.pump_on_poc,                # {14}
            str(pump_serial_number),            # {15}
            opcodes.high_flow_limit,            # {16}
            str(point_of_control_obj.hf),       # {17}
            opcodes.shutdown_on_high_flow,      # {18}
            str(point_of_control_obj.hs),       # {19}
            opcodes.master_valve,               # {20}
            str(master_valve_serial_number),    # {21}
            opcodes.unscheduled_flow_limit,     # {22}
            str(point_of_control_obj.uf),       # {23}
            opcodes.unscheduled_flow_shutdown,  # {24}
            str(point_of_control_obj.us),       # {25}
            opcodes.pressure_sensor_ps,         # {26}
            str(pressure_sensor_serial_number), # {27}
            opcodes.high_pressure_limit,        # {28}
            str(point_of_control_obj.hp),       # {29}
            opcodes.high_pressure_shutdown,     # {30}
            str(point_of_control_obj.he),       # {31}
            opcodes.low_pressure_limit,         # {32}
            str(point_of_control_obj.lp),       # {33}
            opcodes.low_pressure_shutdown,      # {34}
            str(point_of_control_obj.le),       # {35}
            opcodes.high_flow_limit,            # {36}
            str(point_of_control_obj.hf),       # {37}
        )

        if self.controller.controller_type == '32':
            message += "/{0}={1}/{2}={3}".format(
                opcodes.flow_station,               # {0}
                point_of_control_obj.sh,            # {1}
                opcodes.mainline,                   # {2}
                point_of_control_obj.ml             # {3}
            )

        if self.controller.controller_type == opcodes.flow_station:
            pass
        # TODO need to add methods to POC for GR, MF, and UI attributes
        #     message += "/{0}={1}/{2}={3}".format(
        #         opcodes.group,                      # {0}
        #         point_of_control_obj.gr,            # {1}
        #         opcodes.flow_station_mainline,      # {2}
        #         point_of_control_obj.mf             # {3}
        # )

        return message

    def verify_packet_dg_412_key_values(self, parsed_packet, test_engine_poc):
        """
        :param parsed_packet:           Packet represented by a KeyValue pair objects. \n
        :param test_engine_poc:         Point of Control Object. \n
        :type test_engine_poc:          common.objects.programming.point_of_control.PointOfControl \n
        """
        address_string = str(test_engine_poc.ad)
        test_engine_poc.data = parsed_packet[address_string]
        if test_engine_poc.data is not None:
            test_engine_poc.verify_description()
            test_engine_poc.verify_enabled_state()
            test_engine_poc.verify_target_flow()
            test_engine_poc.verify_flow_meter_serial_number()
            test_engine_poc.verify_pump_serial_number()
            test_engine_poc.verify_high_flow_limit()
            test_engine_poc.verify_shutdown_on_high_flow()
            test_engine_poc.verify_master_valve_serial_number()
            test_engine_poc.verify_unscheduled_flow_limit()
            test_engine_poc.verify_shutdown_on_unscheduled()
            test_engine_poc.verify_pressure_sensor_serial_number()
            test_engine_poc.verify_high_pressure_limit()
            test_engine_poc.verify_high_pressure_shutdown()
            test_engine_poc.verify_low_pressure_shutdown()

            if self.controller.controller_type == '32':
                test_engine_poc.verify_mainline_on_point_of_control()
                # TODO need to fix SH parameter. Should be FS in this verification method
                # test_engine_poc.verify_share_with_flow_station()

            if self.controller.controller_type == 'FS':
                # TODO need to add verification methods to POC for GR, MF, and UI
                pass

    ##############
    # Packet 422 #
    ##############

    def build_tq_packet_mainline_dg_422_get_all_mainlines(self, header_id=2500):
        """
        This will create a TQ packet string for dg 422 requesting all mainlines
        :param header_id: This is the header id for the packet
        :return: TQ packet string
        """
        message = "TQ^DG:422/{0}={1}^{2}:AL".format(
            opcodes.message_id,
            str(header_id),
            opcodes.mainline
        )
        return message

    def build_tq_packet_mainline_dg_422_get_single_mainline(self, mainline_address, header_id=2600):
        """
        This will create a TQ packet string for DG422 for getting a single mainlines
        :param mainline_address: Address of mainline\n
        :type mainline_address: int \n
        :param header_id: This is the header id for the packet
        :return: TQ packet string
        """
        message = "TQ^DG:422/{0}={1}^{2}:{3}".format(
            opcodes.message_id,
            str(header_id),
            opcodes.mainline,
            str(mainline_address)
        )
        return message

    def build_ts_packet_mainline_dg_422_single_mainline(self, mainline_obj,
                                                                  header_id=2700):
        """
        This will build a TS or TE packet based off of the packet_type. The method will use the given test engine
        mainline object.
        :param mainline_obj: Test engine object with all the parameters set needed to build a TS packet\n
        :type mainline_obj:common.objects.programming.ml.Mainline \n
        :param header_id: This is the header id for the packet\n
        :type header_id :int \n
        :return: string \n
        """
        # set the type for the packet header
        fill_time_seconds = 0
        if mainline_obj.ft is not None:
            fill_time_seconds = mainline_obj.ft * 60

        packet_type = 'TS'
        # build the message
        message = ("{0}^DG:422/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}/{14}={15}/{16}={17}/{18}={19}"
                  "/{20}={21}/{22}={23}/{24}={25}/{26}={27}/{28}={29}/{30}={31}/{32}={33}/{34}={35}/{36}={37}/{38}={39}"
                  "/{40}={41}/{42}={43}/{44}={45}/{46}={47}"
                  "/{48}={49}/{50}={51}/{52}={53}/{54}={55}/{56}={57}/{58}={59}/{60}={61}".format(
            packet_type,                                     # {0}
            opcodes.message_id,                              # {1}
            str(header_id),                                  # {2}
            opcodes.header_sequence,                         # {3}
            opcodes.mainline,                                # {4}
            str(mainline_obj.ad),                            # {5}
            opcodes.description,                             # {6}
            str(mainline_obj.ds),                            # {7}
            opcodes.enabled,                                 # {8}
            mainline_obj.en,                                 # {9}
            opcodes.target_flow,                             # {10}
            str(mainline_obj.fl),                            # {11}
            opcodes.fill_time,                               # {12}
            str(fill_time_seconds),                          # {13}
            opcodes.stable_flow_pressure_value,              # {14}
            str(mainline_obj.sp),                            # {15}
            opcodes.use_pressure_for_stable_flow,            # {16}
            str(mainline_obj.up),                            # {17}
            opcodes.use_time_for_stable_flow,                # {18}
            str(mainline_obj.ut),                            # {19}
            opcodes.standard_variance_shutdown,              # {20}
            str(mainline_obj.fw),                            # {21}
            opcodes.standard_variance_limit,                 # {22}
            str(mainline_obj.fv),                            # {23}
            opcodes.mainline_variance_limit,                 # {24}
            str(mainline_obj.mf),                            # {25}
            opcodes.mainline_variance_shutdown,              # {26}
            str(mainline_obj.mw),                            # {27}
            opcodes.delay_first_zone,                        # {28}
            str(mainline_obj.tm),                            # {29}
            opcodes.delay_for_next_zone,                     # {30}
            str(mainline_obj.tz),                            # {31}
            opcodes.delay_after_zone,                        # {32}
            str(mainline_obj.tl),                            # {33}
            opcodes.number_of_zones_to_delay,                # {34}
            str(mainline_obj.zc),                            # {35}
            opcodes.limit_zones_by_flow,                     # {36}
            str(mainline_obj.lc),                            # {37}
            opcodes.use_advanced_flow,                       # {38}
            str(mainline_obj.af),                            # {39}
            opcodes.high_variance_limit_tier_one,            # {40}
            str(mainline_obj.ah),                            # {41}
            opcodes.high_variance_limit_tier_two,            # {42}
            str(mainline_obj.bh),                            # {43}
            opcodes.high_variance_limit_tier_three,          # {44}
            str(mainline_obj.ch),                            # {45}
            opcodes.high_variance_limit_tier_four,           # {46}
            str(mainline_obj.dh),                            # {47}
            opcodes.low_variance_limit_tier_one,             # {48}
            str(mainline_obj.al),                            # {49}
            opcodes.low_variance_limit_tier_two,             # {50}
            str(mainline_obj.bl),                            # {51}
            opcodes.low_variance_limit_tier_three,           # {52}
            str(mainline_obj.cl),                            # {53}
            opcodes.low_variance_limit_tier_four,            # {54}
            str(mainline_obj.dl),                            # {55}
            opcodes.zone_high_variance_detection_shutdown,   # {56}
            str(mainline_obj.zh),                            # {57}
            opcodes.zone_low_variance_detection_shutdown,    # {58}
            str(mainline_obj.zl),                            # {59}
            opcodes.pipe_fill_units,                         # {60}
            str(mainline_obj.un),                            # {61}
        ))

        if self.controller.controller_type == '32':
            message += "/{0}={1}".format(
                opcodes.flow_station,               # {0}
                mainline_obj.sh                     # {1}
            )

        # TODO: Uncomment once test object has the attributes in it
        # if self.controller.controller_type == 'FS':
        #     message += "/{0}={1}/{2}={3}/{4}={5}/{6}={7}".format(
        #         opcodes.priority,                     # {0}
        #         str(mainline_obj.pr),                 # {1}
        #         opcodes.mainline,                     # {2}
        #         str(mainline_obj.ml),                 # {3}
        #         opcodes.point_of_control,             # {4}
        #         str(mainline_obj).pc,                 # {5}
        #         opcodes.use_dynamic_flow_allocation,  # {6}
        #         str(mainline_obj.fp)                  # {7}
        #     )

        return message

    def verify_packet_422_key_values(self, parsed_packet, mainline_address, test_engine_mainline):
        """
        :param parsed_packet:               parsed packet. \n
        :param mainline_address:            address of mainline (will depend if it's a flowstation or a 3200 \n
        :type mainline_address:             int
        :param test_engine_mainline:        Mainline Object. \n
        :type test_engine_mainline:         common.objects.programming.ml.Mainline \n

        The logic here is more-or-less a copy of Mainline.verify_who_i_am()
        """

        address_string = str(mainline_address)
        test_engine_mainline.data = parsed_packet[address_string]

        if test_engine_mainline.data is not None:
            test_engine_mainline.verify_enabled_state()
            test_engine_mainline.verify_description()
            #  TODO currently the pipe_fill_time verifier is not working due to a bug on the controller. Once fixed,
            #  uncomment this line
            test_engine_mainline.verify_pipe_fill_time()
            test_engine_mainline.verify_target_flow()
            test_engine_mainline.verify_limit_zones_by_flow_state()
            if self.controller.is3200():
                test_engine_mainline.verify_share_with_flowstation()
            test_engine_mainline.verify_standard_variance_shutdown_enabled()
            test_engine_mainline.verify_standard_variance_limit()
            # TODO these two were taken out in version 16.0.536
            # test_engine_mainline.verify_mainline_variance_limit()
            # test_engine_mainline.verify_mainline_variance_shutdown_enabled()
            test_engine_mainline.verify_delay_units()

            test_engine_mainline.verify_delay_first_zone()
            test_engine_mainline.verify_delay_for_next_zone()
            test_engine_mainline.verify_delay_after_zone()
            test_engine_mainline.verify_number_of_zones_to_delay()

            test_engine_mainline.verify_advanced_flow_variance_setting()

            test_engine_mainline.verify_high_variance_limit_tier_one()
            test_engine_mainline.verify_low_variance_limit_tier_one()
            test_engine_mainline.verify_high_variance_limit_tier_two()
            test_engine_mainline.verify_low_variance_limit_tier_two()
            test_engine_mainline.verify_high_variance_limit_tier_three()
            test_engine_mainline.verify_low_variance_limit_tier_three()
            test_engine_mainline.verify_high_variance_limit_tier_four()
            test_engine_mainline.verify_low_variance_limit_tier_four()
            test_engine_mainline.verify_standard_low_variance_detection_shutdown()
            test_engine_mainline.verify_standard_high_variance_detection_shutdown()

        # address_string = str(test_engine_mainline.ad)
        # test_engine_mainline.data = parsed_packet[address_string]
        # if test_engine_mainline.data is not None:
        #     test_engine_mainline.verify_description()
        #     test_engine_mainline.verify_enabled_state()
        #     test_engine_mainline.verify_priority()
        #     # this verifies both water_budget and water_budget_shutdown
        #     test_engine_mainline.verify_monthly_budget()
        #
        #     test_engine_mainline.verify_water_rationing()
        #
        #     if test_engine_mainline.controller.controller_type == '32':
        #         test_engine_mainline.verify_mainline()
        #         test_engine_mainline.verify_share_with_flow_station()

    ####################################
    # Packet 570 Controller Assignment #
    ####################################

    def build_tq_packet_dg_570_get_all_controller_assignments(self, header_id=400):
        """
        This will create a TQ packet string for dg 412 requesting all points of control
        :param header_id: This is the header id for the packet
        :type header_id: int \n
        :return: TQ packet string
        :rtype: string \n
        """
        message = "TQ^DG:570/{0}={1}".format(
            opcodes.message_id,
            str(header_id),
        )
        return message

    def build_ts_packet_dg_570_controller_assignment(self, controller, controller_slot, header_id=500):
        """
        This will build a TS packet for dg 570 - Controller Assignments
        :param header_id: This is the header id for the packet \n
        :type header_id: int \n
        :param controller_slot: This is the header id for the packet \n
        :type controller_slot: int \n
        :param controller: This is the header id for the packet \n
        :type  controller: common.objects.controllers.bl_32.BaseStation3200 \n

        :return: message \n
        :rtype: string \n
        """
        packet_type = "TS"
        # build the message
        message = "{0}^DG:570/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}^{10}:{11}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.mac_address,                    # {4}
            str(controller.mac),                    # {5}
            opcodes.controller,                     # {6}
            str(controller_slot),                   # {7}
            opcodes.serial_number,                  # {8}
            str(controller.sn),                     # {9}
            opcodes.sequence_last,                  # {10}
            opcodes.true,                           # {11}
        )

        return message

    def verify_packet_570_key_values(self, parsed_packet, controller, controller_slot_in_flowstation):
        """
        :param parsed_packet:               parsed packet. \n
        :param controller: This is the controller object \n
        :type  controller: common.objects.controllers.bl_32.BaseStation3200 \n
        :param flowstation: This is the flowstation object \n
        :type  flowstation: common.objects.controllers.bl_fs.Flowstation \n
        """
        address_string = str(controller.mac)
        controller.data = parsed_packet[address_string]

        if controller.data is not None:
            controller.verify_serial_number()

            # Compare controller slot number from packet against what we tried to set
            assigned_controller_slot = int(controller.data.get_value_string_by_key(opcodes.controller))
            if assigned_controller_slot != controller_slot_in_flowstation:
                e_msg = "Unable to verify controller's assigned slot in flowstation. Received: {0}, Expected: {1}".format(
                    str(assigned_controller_slot),        # {0}
                    str(controller_slot_in_flowstation)   # {1}
                )
                raise ValueError(e_msg)
            else:
                print("Verified controller's slot in flowstation. Controller slot number: {0}".format(
                    str(controller_slot_in_flowstation),     # {0}
                ))

    ######################################
    # Packet 501 Water Source Assignment #
    ######################################

    def build_tq_packet_dg_501_get_all_water_source_assignments(self, header_id=400):
        """
        This will create a TQ packet string for dg 501 requesting all Water Source Assignments
        :param header_id: This is the header id for the packet
        :type header_id: int \n
        :return: TQ packet string
        :rtype: string \n
        """
        packet_type = "TQ"
        message = "{0}^DG:501/{1}={2}^{3}:{4}".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.water_source,       # {3}
            opcodes.all                 # {4}
        )
        return message

    def build_ts_packet_dg_501_water_source_assignment(self, controller, flow_station_slot, controller_slot, header_id=701):
        """
        This will build a TS packet for dg 501 - Water Source Assignments
        :param header_id: This is the header id for the packet \n
        :type header_id: int \n
        :param controller_slot: this is the Water Source slot number on the controller \n
        :type controller_slot: int \n
        :param controller: This is the the controller object to which the Water Source belongs \n
        :type  controller: common.objects.controllers.bl_32.BaseStation3200 \n
        :param flow_station_slot: this is the flow station slot number on the controller \n
        :type flow_station_slot: int \n
        :return: message \n
        :rtype: string \n
        """
        packet_type = "TS"
        # build the message
        message = "{0}^DG:501/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}^{10}:{11}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.water_source,                   # {4}
            str(flow_station_slot),                 # {5}
            opcodes.controller,                     # {6}
            str(controller.mac),                    # {7}
            opcodes.water_source,                   # {8}
            str(controller_slot),                   # {9}
            opcodes.sequence_last,                  # {10}
            opcodes.true,                           # {11}
        )
        return message

    def verify_packet_dg_501_key_values(self, parsed_packet, controller, fs_slot_number, controller_slot_number):
        """
        :param parsed_packet: Packet represented by a KeyValue Objects. \n
        :param controller_slot_number: this is the WS slot number on the controller \n
        :type controller_slot_number: int \n
        :param controller: This is the the controller object to which the POC belongs to \n
        :type  controller: common.objects.controllers.bl_32.BaseStation3200 \n
        :param fs_slot_number: this is the flow station slot number on the controller \n
        :type fs_slot_number: int \n
        """
        address_string = str(fs_slot_number)
        try:
            assignment_data = parsed_packet[address_string]
        except KeyError:
            e_msg = "There is no water source assignment for flow station water source slot number: " + str(fs_slot_number)
            raise KeyError(e_msg)

        controller_mac = assignment_data.get_value_string_by_key(opcodes.controller)

        # Compare controller mac
        if controller.mac != controller_mac:
            e_msg = "Unable verify 'flow station water source assignment {0} controller mac'. Received: {1}, Expected: {2}".format(
                fs_slot_number,         # {0}
                str(controller_mac),    # {1}
                controller.mac          # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 'flow station water source assignment {0} controller mac'. Controller mac: {1}".format(
                str(fs_slot_number),    # {0}
                controller.mac          # {1}
            ))

        # Compare controller water source slot number
        water_source_slot = int(assignment_data.get_value_string_by_key(opcodes.water_source))
        if water_source_slot != controller_slot_number:
            e_msg = "Unable to verify 'flow station water source assignment'{0}. Received: {1}, Expected: {2}".format(
                str(fs_slot_number),            # {0}
                str(water_source_slot),         # {1}
                str(controller_slot_number)     # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 'flow station water source assignment': '{0}'. Controller slot number: {1}".format(
                fs_slot_number,             # {0}
                controller_slot_number,     # {0}
            ))

    ###########################################
    # Packet 511 Point of Control Assignments #
    ###########################################

    def build_tq_packet_dg_511_get_all_point_of_control_assignments(self, header_id=402):
        """
        This will create a TQ packet string for dg 511 requesting all POC Assignments
        :param header_id: This is the header id for the packet
        :type header_id: int \n
        :return: message
        :rtype: string \n
        """
        packet_type = "TQ"
        message = "{0}^DG:511/{1}={2}^{3}:{4}".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.point_of_control,   # {3}
            opcodes.all                 # {4}
        )
        return message

    def build_tq_packet_dg_511_get_one_point_of_control_assignment(self, point_of_control_assignment, header_id=403):
        """
        This will create a TQ packet string for dg 511 requesting all POC Assignments
        :param header_id: This is the header id for the packet
        :type header_id: int \n
        :param point_of_control_assignment: This is the assigned point of control slot number on the Flow Station
        :type point_of_control_assignment: int \n
        :return: message
        :rtype: string \n
        """
        packet_type = "TQ"
        message = "{0}^DG:511/{1}={2}^{3}:{4}".format(
            packet_type,                    # {0}
            opcodes.message_id,             # {1}
            str(header_id),                 # {2}
            opcodes.point_of_control,       # {3}
            point_of_control_assignment     # {4}
        )
        return message

    def build_ts_packet_dg_511_point_of_control_assignment(self, controller, flow_station_slot, controller_slot, header_id=712):
        """
        This will build a TS packet for dg 511 - POC Assignments
        :param header_id: This is the header id for the packet \n
        :type header_id: int \n
        :param controller_slot: this is the POC slot number on the controller \n
        :type controller_slot: int \n
        :param controller: This is the the controller object to which the POC belongs to \n
        :type  controller: common.objects.controllers.bl_32.BaseStation3200 \nn
        :param flow_station_slot: this is the flow station slot number on the controller \n
        :type header_id: int \n
        :return: message \n
        :rtype: string \n
        """
        packet_type = "TS"
        # build the message
        message = "{0}^DG:511/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}^{10}:{11}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.point_of_control,               # {4}
            str(flow_station_slot),                 # {5}
            opcodes.controller,                     # {6}
            str(controller.mac),                    # {7}
            opcodes.point_of_control,               # {8}
            str(controller_slot),                   # {9}
            opcodes.sequence_last,                  # {10}
            opcodes.true,                           # {11}
        )
        return message

    def verify_packet_dg_511_key_values(self, parsed_packet, controller, fs_slot_number, controller_slot_number):
        """
        :param parsed_packet:                       Packet represented by a KeyValue pair objects. \n
        :param controller:                          controller. \n
        :type controller:                           common.objects.controllers.bl_32.BaseStation3200 \n
        :param fs_slot_number:                      Flow Station Slot Number \n
        :type  int:
        :param controller_slot_number:              Controller Slot Number \n
        :type  int:
        """
        address_string = str(fs_slot_number)
        try:
            assignment_data = parsed_packet[address_string]
        except KeyError:
            e_msg = "There is no point of control assignment for flow station point of control slot number: " + fs_slot_number
            raise KeyError(e_msg)

        controller_mac = assignment_data.get_value_string_by_key(opcodes.controller)

        # Compare controller mac
        if controller.mac != controller_mac:
            e_msg = "Unable verify 'flow station point of control assignment {0} controller mac'. Received: {1}, Expected: {2}".format(
                fs_slot_number,         # {0}
                str(controller_mac),    # {1}
                controller.mac          # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 'flow station point of control assignment {0} controller mac'. Controller mac: {1}".format(
                str(fs_slot_number),    # {0}
                controller.mac          # {1}
            ))

        # Compare controller point of control slot number
        poc_slot = int(assignment_data.get_value_string_by_key(opcodes.point_of_control))
        if poc_slot != controller_slot_number:
            e_msg = "Unable to verify 'flow station point of control assignment'{0}. Received: {1}, Expected: {2}".format(
                str(fs_slot_number),    # {0}
                str(poc_slot),          # {1}
                controller_slot_number  # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 'flow station point of control assignment': '{0}'. Controller slot number: {1}".format(
                fs_slot_number,             # {0}
                controller_slot_number,     # {0}
            ))

    ###################################
    # Packet 332 FlowStation Settings #
    ###################################

    def build_tq_packet_dg_332_get_all_flowstation_settings(self, header_id=415):
        """
        This will create a TQ packet string for dg 332 requesting all flowstation settings
        :param header_id: This is the header id for the packet
        :type header_id: int \n
        :return: message
        :rtype: string \n
        """
        packet_type = "TQ"
        message = "{0}^DG:332/{1}={2}^{3}:{4}".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.flow_station,       # {3}
            opcodes.all                 # {4}
        )
        return message

    def build_tq_packet_dg_332_get_one_flowstation_settings(self, flowstation_number, header_id=403):
        """
        This will create a TQ packet string for dg 332 requesting one flowstation setting packet for the given
        flowstation number.
        :param header_id: This is the header id for the packet
        :type header_id: int \n
        :param flowstation_number: this is the flow station number on the controller
        :type flowstation_number: int \n
        :return: message
        :rtype: string \n
        """
        packet_type = "TQ"
        message = "{0}^DG:332/{1}={2}^{3}:{4}".format(
            packet_type,                    # {0}
            opcodes.message_id,             # {1}
            str(header_id),                 # {2}
            opcodes.flow_station,           # {3}
            flowstation_number              # {4}
        )
        return message

    def build_ts_packet_dg_332_flowstation_settings(self,   flow_station,
                                                            flowstation_address=1,
                                                            description='Flowstation 1',
                                                            enabled=opcodes.true,
                                                            header_id=712):
        """
        This will build a TS packet for dg 332 - Flowstation settings
        :param header_id: This is the header id for the packet \n
        :type header_id: int \n
        :param flow_staiton: This is the header id for the packet \n
        :type flow_station: common.objects.controllers.bl_fs.FlowStation \n
        :param flowstation_address: this is the flow station address on the flow station \n
        :type flowstation_address: int \n
        :param enabled: enabled state of the flow station \n
        :type enabled: string \n
        :param description: description of flow station \n
        :type description: string \n
        :return: message \n
        :rtype: string \n
        """
        packet_type = "TS"
        # build the message
        message = "{0}^DG:332/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}/{10}={11}/{12}={13}/{14}={15}^{16}:{17}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.flow_station,                   # {4}
            str(flowstation_address),               # {5}
            opcodes.description,                    # {6}
            str(description),                       # {7}
            opcodes.enabled,                        # {8}
            str(enabled),                           # {9}
            opcodes.ip_address,                     # {10}
            str(flow_station.ad),                   # {11}
            opcodes.latitude,                       # {12}
            str(flow_station.la),                   # {13}
            opcodes.longitude,                      # {14}
            str(flow_station.lg),                   # {15}
            opcodes.sequence_last,                  # {16}
            opcodes.true,                           # {17}

        )
        return message

    def verify_packet_dg_332_key_values(self, parsed_packet,
                                        flow_station,
                                        flowstation_address,
                                        description,
                                        enabled):
        """
        This will build a TS packet for dg 332 - Flowstation settings
        :param flow_staiton: This is the header id for the packet \n
        :type flow_station: common.objects.controllers.bl_fs.FlowStation \n
        :param flowstation_address: this is the flow station address on the flow station \n
        :type flowstation_address: int \n
        :param enabled: enabled state of the flow station \n
        :type enabled: string \n
        :param description: description of flow station \n
        :type description: string \n
        """
        address_string = str(flowstation_address)
        try:
            assignment_data = parsed_packet[address_string]
        except KeyError:
            e_msg = "There is no flow station on controller: {0} assigned to slot: {1}".format(self.controller.mac,
                                                                                    flowstation_address)
            raise KeyError(e_msg)

        # Compare description
        actual_description = str(assignment_data.get_value_string_by_key(opcodes.description))
        if actual_description != description:
            e_msg = "Unable verify packet 332 description. Received: {0}, Expected: {1}".format(
                actual_description,         # {0}
                str(description),           # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified verify packet 332 description: {0}.".format(
                str(actual_description),    # {0}
            ))

        # Compare enabled state
        actual_enabled_state = str(assignment_data.get_value_string_by_key(opcodes.enabled))
        if actual_enabled_state != enabled:
            e_msg = "Unable verify packet 332 enabled state. Received: {0}, Expected: {1}".format(
                actual_enabled_state,       # {0}
                str(enabled),               # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified verify packet 332 enabled state: {0}.".format(
                str(actual_enabled_state),  # {0}
            ))

        # Compare flow station ip address
        actual_ip_address = str(assignment_data.get_value_string_by_key(opcodes.ip_address))
        if actual_ip_address != flow_station.ad:
            e_msg = "Unable verify packet 332 flowstation ip address: {0}, Expected: {1}".format(
                actual_ip_address,          # {0}
                str(flow_station.ad),       # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified verify packet 332 flowstation ip address: {0}.".format(
                str(actual_ip_address),  # {0}
            ))

    ######################################
    # Packet 521 Mainline Assignment #
    ######################################

    def build_tq_dg_521_get_all_mainline_assignments(self, header_id=400):
        """
        This will create a TQ packet string for dg 521 requesting all Mainline Assignments
        :param header_id: This is the header id for the packet
        :return: TQ packet string
        """
        packet_type = "TQ"
        message = "{0}^DG:521/{1}={2}^{3}:{4}".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.mainline,           # {3}
            opcodes.all                 # {4}
        )
        return message

    def build_ts_dg_521_mainline_assignment(self, controller, flow_station_slot, controller_slot, header_id=701):
        """
        This will build a TS packet for dg 521 - Mainline Assignments
        :param header_id: This is the header id for the packet \n
        :type header_id: int \n
        :param controller_slot: this is the mainline slot number on the controller \n
        :type header_id: int \n
        :param controller: This is the the controller object to which the mainline belongs to \n
        :type  common.objects.controllers.bl_32.BaseStation3200 \n
        :param flow_station_slot: this is the flow station slot number on the controller \n
        :type header_id: int \n
        :return: message \n
        :rtype: string \n
        """
        packet_type = "TS"
        # build the message
        message = "{0}^DG:521/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}^{10}:{11}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.mainline,                       # {4}
            str(flow_station_slot),                 # {5}
            opcodes.controller,                     # {6}
            str(controller.mac),                    # {7}
            opcodes.mainline,                       # {8}
            str(controller_slot),                   # {9}
            opcodes.sequence_last,                  # {10}
            opcodes.true                            # {11}
        )
        return message

    def verify_packet_521_key_values(self, parsed_packet, controller, fs_slot_number, controller_slot_number):
        """
        :param parsed_packet:               parsed packet. \n
        :param test_engine_mainline:        Mainline Object. \n
        :type test_engine_mainline:         common.objects.programming.ml.Mainline \n
        """
        address_string = str(fs_slot_number)
        try:
            assignment_data = parsed_packet[address_string]
        except KeyError:
            e_msg = "There is no mainline assignment for flow station mainline slot number: " + fs_slot_number
            raise KeyError(e_msg)

        controller_mac = assignment_data.get_value_string_by_key(opcodes.controller)

        # Compare controller mac
        if controller.mac != controller_mac:
            e_msg = "Unable to verify 'flow station mainline source assignment {0} controller mac'. Received: {1}, Expected: {2}".format(
                fs_slot_number,         # {0}
                str(controller_mac),    # {1}
                controller.mac          # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 'flow station mainline assignment {0} controller mac'. Controller mac: {1}".format(
                fs_slot_number,  # {0}
                controller.mac   # {1}
            ))

        # Compare controller mainline slot number
        mainline_slot = int(assignment_data.get_value_string_by_key(opcodes.mainline))
        if mainline_slot != controller_slot_number:
            e_msg = "Unable to verify 'flow station mainline assignment'{0}. Received: {1}, Expected: {2}".format(
                fs_slot_number,                 # {0}
                str(mainline_slot),             # {1}
                controller_slot_number          # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified 'flow station mainline assignment': '{0}'. Controller slot number: {1}".format(
                fs_slot_number,             # {0}
                controller_slot_number,     # {1}
            ))



    ######################################
    # Packet 500 Flowstation Setting #
    ######################################

    def build_tq_dg_500_get_flowstation_settings(self, header_id=400):
        """
        This will create a TQ packet string for dg 500 requesting the Flowstation Settings
        :param header_id: This is the header id for the packet
        :return: TQ packet string
        """
        packet_type = "TQ"
        message = "{0}^DG:500/{1}={2}".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
        )
        return message

    def build_ts_dg_500_flowstation_settings(self, flowstation, header_id=701):
        """
        This will build a TS packet for dg 500 - Flowstation Settings
        :param flowstation: This is the flowstation object \n
        :type  flowstation: common.objects.controllers.bl_fs.Flowstation \n
        :param header_id: This is the header id for the packet \n
        :type header_id: int \n
        """
        packet_type = "TS"
        # build the message
        message = "{0}^DG:500/{1}={2}/{3}=0^{4}:{5}/{6}={7}/{8}={9}/{10}={11}^{12}:{13}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.flow_station,                   # {4}
            str(flowstation.sn),                    # {5}
            opcodes.description,                    # {6}
            str(flowstation.ds),                    # {7}
            opcodes.latitude,                       # {8}
            str(flowstation.la),                    # {9}
            opcodes.longitude,                      # {10}
            flowstation.lg,                         # {11}
            opcodes.sequence_last,                  # {12}
            opcodes.true                            # {13}
        )
        return message

    def verify_packet_500_key_values(self, parsed_packet, flowstation):
        """
        :param parsed_packet:               parsed packet. \n
        :param flowstation: This is the flowstation object \n
        :type  flowstation: common.objects.controllers.bl_fs.Flowstation \n
        """
        address_string = str(flowstation.sn)
        flowstation.data = parsed_packet[address_string]

        if flowstation.data is not None:
            flowstation.verify_description()
            flowstation.verify_latitude()
            flowstation.verify_longitude()

    ######################################
    # Packet 571 Controller Settings     #
    ######################################

    def build_tq_dg_571_get_all_controller_settings(self, header_id=400):
        """
        This will create a TQ packet string for dg 571 requesting all Controller Settings
        :param header_id: This is the header id for the packet
        :return: TQ packet string
        """
        packet_type = "TQ"
        message = "{0}^DG:571/{1}={2}^{3}:{4}".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.controller,         # {3}
            opcodes.all                 # {4}
        )
        return message


    def build_tq_packet_dg_571_get_single_controller_setting(self, controller, header_id=600):
        """
        This will create a TQ packet string for DG571 for getting a single controller setting
        :param header_id: This is the header id for the packet
        :param controller: Test engine controller object with set attributes
        :type moisture_sensor: common.objects.controllers.bl_32.BaseStation3200 \n
        :return: message \n
        :rtype: string \n
        """
        message = "TQ^DG:571/{0}={1}^{2}:{3}".format(
            opcodes.message_id,      # {0}
            str(header_id),          # {1}
            opcodes.controller,      # {2}
            controller.mac           # {3}
        )
        return message


    def build_ts_dg_571_controller_settings(self, controller, enabled, header_id=701):
        """
        This will build a TS packet for dg 571 - Mainline Assignments
        :param header_id: This is the header id for the packet \n
        :type header_id: int \n
        :param controller: This is a controller object assigned to the flowstation \n
        :type  common.objects.controllers.bl_32.BaseStation3200 \n
        :param enabled: enable (or not) the controller on the flowstation \n
        :type header_id: bool \n
        :return: message \n
        :rtype: string \n
        """
        packet_type = "TS"
        enabled_str = "TR" if enabled else "FA"
        # build the message
        message = "{0}^DG:571/{1}={2}/{3}=0^{4}:{5}/{6}={7}^{8}:{9}".format(
            packet_type,                            # {0}
            opcodes.message_id,                     # {1}
            str(header_id),                         # {2}
            opcodes.header_sequence,                # {3}
            opcodes.controller,                     # {4}
            controller.mac,                         # {5}
            opcodes.enabled,                        # {6}
            enabled_str,                            # {7}
            opcodes.sequence_last,                  # {8}
            opcodes.true,                           # {9}
        )
        return message

    def verify_packet_571_key_values(self, parsed_packet, controller, enabled):
        """
        :param parsed_packet:               parsed packet. \n
        :param controller: This is a controller object assigned to the flowstation \n
        :type  controller: common.objects.controllers.bl_32.BaseStation3200 \n
        :param enabled: The expected state of the enabled flag for the controller in the flowstation
        "type  enabled: boolean
        """
        address_string = str(controller.mac)
        data = parsed_packet[address_string]
        expected_enabled = "TR" if enabled else "FA"

        if data is not None:
            # Compare the expected "enabled" flag to what is in the flowstation
            enabled_from_fs = data.get_value_string_by_key(opcodes.enabled)
            if enabled_from_fs != expected_enabled:
                e_msg = "Unable to verify controller's enabled state in flowstation. Received: {0}, Expected: {1}".format(
                    str(enabled_from_fs),      # {0}
                    str(expected_enabled)      # {1}
                )
                raise ValueError(e_msg)
            else:
                print("Verified controller's enabled state in flowstation. Enabled state:: {0}".format(
                    str(enabled_from_fs)     # {0}
                ))
        # TODO: Verify enabled?  Anything else?

    ######################################
    # Packet 403 Empty Conditions #
    ######################################

    def build_tq_dg_403_get_Empty_Conditions_for_all_water_sources(self,  header_id=1500):
        """
        This will create a TQ packet string for dg 403 requesting all Empty Conditions
        :param header_id: This is the header id for the packet
        :return: TQ packet string
        """
        packet_type = "TQ"
        message = "{0}^DG:403/{1}={2}/SQ=1^{3}:{4}".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.water_source,       # {3}
            opcodes.all                 # {4}
        )
        return message

    def build_tq_dg_403_get_Empty_Conditions_for_a_water_source(self, water_source_address,  header_id=1600):
        """
        This will create a TQ packet string for dg 403 requesting all Empty Conditions
        :param header_id: This is the header id for the packet
        :return: TQ packet string
        """
        packet_type = "TQ"
        message = "{0}^DG:403/{1}={2}/SQ=1^{3}:{4}".format(
            packet_type,                # {0}
            opcodes.message_id,         # {1}
            str(header_id),             # {2}
            opcodes.water_source,       # {3}
            water_source_address        # {4}
        )
        return message



