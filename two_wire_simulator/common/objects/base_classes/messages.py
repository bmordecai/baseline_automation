import datetime
from common.date_package.date_resource import date_mngr
from common.imports import opcodes
from common.imports.types import Message, MessageCategory, status_plus_priority_code_dict_3200, status_code_dict_1000, \
    program_1000_who_dict

__author__ = 'Baseline'


def message_to_set(_object, _status_code, _ct_type, _helper_object=None, _use_to_verify=False):
    """
    This method takes in an object and creates different messages depending on it's type and status code
    :param _object
    :type _object:
    :param _status_code
    :type _status_code: str
    :param _ct_type
    :type _ct_type: str
    :param _helper_object
    :type _helper_object:
    """

    # Check to make sure the status code is valid if controller is 3200
    if _object.controller_type is "32" and _status_code not in status_plus_priority_code_dict_3200[_ct_type].keys():
        e_msg = "Status Code '{0}' is not valid for object {1}.".format(
            _status_code,    # {0} Status code passed in
            _ct_type         # {1} Device type of the object
        )
        raise ValueError(e_msg)
    # Check to make sure the status code is valid if controller is 1000
    if _object.controller_type is "10" and _status_code not in status_code_dict_1000[_ct_type]:
        e_msg = "Status Code '{0}' is not valid for object {1}.".format(
            _status_code,    # {0} Status code passed in
            _ct_type         # {1} Device type of the object
        )
        raise ValueError(e_msg)
    # Check to make sure the status code is valid if we are using a substation
    if _object.controller_type is opcodes.substation and _status_code not in status_code_dict_1000[_ct_type]:
        e_msg = "Status Code '{0}' is not valid for object {1}.".format(
            _status_code,    # {0} Status code passed in
            _ct_type         # {1} BiCoder type of the object
        )
        raise ValueError(e_msg)

    # Check the device type and call the appropriate method to create a message to send to the controller
    if _ct_type in [opcodes.controller, opcodes.substation]:
        msg = create_controller_message(_controller_object=_object,
                                        _status_code=_status_code,
                                        _command_type=opcodes.set_action,
                                        _helper_object=_helper_object)
        return msg
    elif _ct_type is opcodes.zone:
        msg = create_zone_message(_zone_object=_object,
                                  _status_code=_status_code,
                                  _command_type=opcodes.set_action)
        return msg
    elif _ct_type is opcodes.event_switch:
        msg = create_event_switch_message(_event_switch_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.set_action)
        return msg
    elif _ct_type is opcodes.flow_meter:
        msg = create_flow_meter_message(_flow_meter_object=_object,
                                        _status_code=_status_code,
                                        _command_type=opcodes.set_action)
        return msg
    elif _ct_type is opcodes.mainline:
        msg = create_mainline_message(_mainline_object=_object,
                                      _status_code=_status_code,
                                      _command_type=opcodes.set_action)
        return msg
    elif _ct_type is opcodes.moisture_sensor:
        msg = create_moisture_sensor_message(_moisture_sensor_object=_object,
                                             _status_code=_status_code,
                                             _command_type=opcodes.set_action)
        return msg
    elif _ct_type is opcodes.master_valve:
        msg = create_master_valve_message(_master_valve_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.set_action)
        return msg
    elif _ct_type is opcodes.temperature_sensor:
        msg = create_temp_sensor_message(_temp_sensor_object=_object,
                                         _status_code=_status_code,
                                         _command_type=opcodes.set_action)
        return msg
    elif _ct_type is opcodes.program:
        msg = create_program_3200_message(_program_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.set_action,
                                          _helper_object=_helper_object)
        return msg
    elif _ct_type is opcodes.point_of_connection:
        msg = create_poc_3200_message(_poc_object=_object,
                                      _status_code=_status_code,
                                      _command_type=opcodes.set_action,
                                      _use_to_verify=_use_to_verify)
        return msg
    elif _ct_type is opcodes.zone_program:
        msg = create_zone_program_message(_zone_program_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.set_action)
        return msg
    elif _ct_type is opcodes.flow_station:
        msg = create_flow_station_message(_flow_station_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.set_action)
        return msg


def message_to_get(_object, _status_code, _ct_type, _helper_object=None, _use_to_verify=False):
    """
    This method takes in an object and creates different messages depending on it's type and status code
    :param _object
    :type _object:
    :param _status_code
    :type _status_code: str
    """
    # Check to make sure the status code is valid if controller is 3200
    if _object.controller_type is "32" and _status_code not in status_plus_priority_code_dict_3200[_ct_type].keys():
        e_msg = "Status Code '{0}' is not valid for object {1}.".format(
            _status_code,    # {0} Status code passed in
            _ct_type         # {1} Device type of the object
        )
        raise ValueError(e_msg)
    # Check to make sure the status code is valid if controller is 1000
    if _object.controller_type is "10" and _status_code not in status_code_dict_1000[_ct_type]:
        e_msg = "Status Code '{0}' is not valid for object {1}.".format(
            _status_code,    # {0} Status code passed in
            _ct_type         # {1} Device type of the object
        )
        raise ValueError(e_msg)

    # Check the device type and call the appropriate method to create a message to send to the controller
    if _ct_type in [opcodes.controller, opcodes.substation]:
        msg = create_controller_message(_controller_object=_object,
                                        _status_code=_status_code,
                                        _command_type=opcodes.get_action,
                                        _helper_object=_helper_object)
        return msg
    elif _ct_type is opcodes.zone:
        msg = create_zone_message(_zone_object=_object,
                                  _status_code=_status_code,
                                  _command_type=opcodes.get_action)
        return msg
    elif _ct_type is opcodes.event_switch:
        msg = create_event_switch_message(_event_switch_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.get_action)
        return msg
    elif _ct_type is opcodes.flow_meter:
        msg = create_flow_meter_message(_flow_meter_object=_object,
                                        _status_code=_status_code,
                                        _command_type=opcodes.get_action)
        return msg
    elif _ct_type is opcodes.mainline:
        msg = create_mainline_message(_mainline_object=_object,
                                      _status_code=_status_code,
                                      _command_type=opcodes.get_action)
        return msg
    elif _ct_type is opcodes.moisture_sensor:
        msg = create_moisture_sensor_message(_moisture_sensor_object=_object,
                                             _status_code=_status_code,
                                             _command_type=opcodes.get_action)
        return msg
    elif _ct_type is opcodes.master_valve:
        msg = create_master_valve_message(_master_valve_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.get_action)
        return msg
    elif _ct_type is opcodes.temperature_sensor:
        msg = create_temp_sensor_message(_temp_sensor_object=_object,
                                         _status_code=_status_code,
                                         _command_type=opcodes.get_action)
        return msg
    elif _ct_type is opcodes.program:
        msg = create_program_3200_message(_program_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.get_action,
                                          _helper_object=_helper_object)
        return msg
    elif _ct_type is opcodes.point_of_connection:
        msg = create_poc_3200_message(_poc_object=_object,
                                      _status_code=_status_code,
                                      _command_type=opcodes.get_action,
                                      _use_to_verify=_use_to_verify)
        return msg
    elif _ct_type is opcodes.zone_program:
        msg = create_zone_program_message(_zone_program_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.get_action)
        return msg
    elif _ct_type is opcodes.flow_station:
        msg = create_flow_station_message(_flow_station_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.get_action)
        return msg


def message_to_clear(_object, _status_code, _ct_type, _helper_object=None):
    """
    This method takes in an object and creates different messages depending on it's type and status code
    :param _object
    :type _object:
    :param _status_code
    :type _status_code: str
    :param _ct_type: str
    :type _ct_type: str
    """
    # Check to make sure the status code is valid if controller is 3200
    if _object.controller_type is "32" and _status_code not in status_plus_priority_code_dict_3200[_ct_type].keys():
        e_msg = "Status Code '{0}' is not valid for object {1}.".format(
            _status_code,   # {0} Status code passed in
            _ct_type        # {1} Device type of the object
        )
        raise ValueError(e_msg)
    # Check to make sure the status code is valid if controller is 1000
    if _object.controller_type is "10" and _status_code not in status_code_dict_1000[_ct_type]:
        e_msg = "Status Code '{0}' is not valid for object {1}.".format(
            _status_code,   # {0} Status code passed in
            _ct_type        # {1} Device type of the object
        )
        raise ValueError(e_msg)

    # Check the device type and call the appropriate method to create a message to send to the controller
    if _ct_type is opcodes.controller:
        msg = create_controller_message(_controller_object=_object,
                                        _status_code=_status_code,
                                        _command_type=opcodes.clear_action,
                                        _helper_object=_helper_object)
        return msg
    elif _ct_type is opcodes.zone:
        msg = create_zone_message(_zone_object=_object,
                                  _status_code=_status_code,
                                  _command_type=opcodes.clear_action)
        return msg
    elif _ct_type is opcodes.event_switch:
        msg = create_event_switch_message(_event_switch_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.clear_action)
        return msg
    elif _ct_type is opcodes.flow_meter:
        msg = create_flow_meter_message(_flow_meter_object=_object,
                                        _status_code=_status_code,
                                        _command_type=opcodes.clear_action)
        return msg
    elif _ct_type is opcodes.mainline:
        msg = create_mainline_message(_mainline_object=_object,
                                      _status_code=_status_code,
                                      _command_type=opcodes.clear_action)
        return msg
    elif _ct_type is opcodes.moisture_sensor:
        msg = create_moisture_sensor_message(_moisture_sensor_object=_object,
                                             _status_code=_status_code,
                                             _command_type=opcodes.clear_action)
        return msg
    elif _ct_type is opcodes.master_valve:
        msg = create_master_valve_message(_master_valve_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.clear_action)
        return msg
    elif _ct_type is opcodes.temperature_sensor:
        msg = create_temp_sensor_message(_temp_sensor_object=_object,
                                         _status_code=_status_code,
                                         _command_type=opcodes.clear_action)
        return msg
    elif _ct_type is opcodes.program:
        msg = create_program_3200_message(_program_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.clear_action,
                                          _helper_object=_helper_object)
        return msg
    elif _ct_type is opcodes.point_of_connection:
        msg = create_poc_3200_message(_poc_object=_object,
                                      _status_code=_status_code,
                                      _command_type=opcodes.clear_action)
        return msg
    elif _ct_type is opcodes.zone_program:
        msg = create_zone_program_message(_zone_program_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.clear_action)
        return msg
    elif _ct_type is opcodes.flow_station:
        msg = create_flow_station_message(_flow_station_object=_object,
                                          _status_code=_status_code,
                                          _command_type=opcodes.clear_action)
        return msg


def create_flow_station_message(_flow_station_object, _status_code, _command_type, _helper_object=None):
    """
    # Example ID=1_ZN_1_OK,DT=3/15/16 15:49:51,TX=Zone 1-TSQ0031\\\nSolenoid Short
    :param _flow_station_object
    :type _flow_station_object: FlowStation
    :param _status_code
    :type _status_code: str
    :param _command_type
    :type _command_type: str
    :param _helper_object
    :type _helper_object: pg_start_stop_pause_cond.BaseStartStopPause
    """
    port = str(_flow_station_object.ser.s_port)[9:21]  # this gives us only the ip address
    # Creates the base for all flow station object messages
    msg = "{0},{1},{2}={3},{4}={5}".format(
        _command_type,                  # {0} Command type passed in
        opcodes.message,                # {1} Message opcode
        opcodes.flow_station,           # {2} FlowStation opcode
        port,                           # {3} IP Address
        opcodes.status_code,            # {4} Status code opcode
        _status_code                    # {5} Status code passed in
    )

    # Creates the different value pairs to test against
    if _command_type is opcodes.get_action:
        # Example ID=1_ZN_1_OK,DT=3/15/16 15:49:51,TX=Zone 1-TSQ0031\\\nSolenoid Short
        _id = build_the_message_id(_object=_flow_station_object,
                                   ct_type=MessageCategory.flow_station,
                                   _status_code=_status_code,
                                   _helper_object=_helper_object)

        if _flow_station_object.controller_type is "32":
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_3200() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_3200()
        else:
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_1000() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_1000()

        _tx = build_message_title(_object=_flow_station_object,
                                  _ct_type=MessageCategory.flow_station,
                                  _status_code=_status_code,
                                  _helper_object=_helper_object)
        return {opcodes.message: msg,       # this is the get command that is sent to the controller
                opcodes.message_id: _id,     # this is the message id that will be received from the controller
                opcodes.date_time: _dt,      # this is the message date/time that will be received from the controller
                opcodes.message_text: _tx,   # this is the detailed text that is received from the controller
                }
    else:
        return msg


def create_controller_message(_controller_object, _status_code, _command_type, _helper_object=None):
    """
    # Example ID=1_ZN_1_OK,DT=3/15/16 15:49:51,TX=Zone 1-TSQ0031\\\nSolenoid Short
    :param _controller_object
    :type _controller_object: Controller
    :param _status_code
    :type _status_code: str
    :param _command_type
    :type _command_type: str
    :param _helper_object
    :type _helper_object: pg_start_stop_pause_cond.BaseStartStopPause
    """

    # Creates the base for all controller object messages
    msg = "{0},{1},{2},{3}={4}".format(
        _command_type,          # {0} Command type passed in
        opcodes.message,        # {1} Message opcode
        opcodes.controller,     # {2} Controller opcode
        opcodes.status_code,    # {3} Status code opcode
        _status_code            # {4} Status code passed in
    )
    # If the controller type is 3200, then check if the message requires any different variables
    if _controller_object.controller_type is "32":
        # If the status code is 'rain delay' and the command is 'SET' then add the amount of rain pause days to the message
        if _status_code is Message.rain_delay_stopped and _command_type in [opcodes.set_action, opcodes.get_action]:
            msg += ",{0}={1}".format(
                Message.variable_one,       # {0} Variable one opcode
                _controller_object.rp       # {1} Rain pause days on controller
            )
        elif _status_code is Message.two_wire_high_current_shutdown and _command_type is opcodes.set_action:
            msg += ",{0}={1}".format(
                Message.variable_two,       # {0} Variable two opcode
                _controller_object.r_va     # {1} current on controller that is inherited from device
            )
        # If status code has anything to do with a moisture sensor
        elif _status_code in [Message.pause_moisture_sensor, Message.stop_moisture_sensor]:
            # These if statements check to make sure the correct object is passed into the function
            if _helper_object is None:
                e_msg = "Status code '{0}' requires you to pass in an object that inherits from BaseStartStopPause " \
                        "and that it be set to have a MoistureSensor object attached to it.".format(
                            _status_code
                        )
                raise ValueError(e_msg)

            if _helper_object._device_serial is None:
                e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                        "MoistureSensor serial number. This can be done with the 'set' methods."
                raise ValueError(e_msg)

            ms_object = None
            # Loops through the moisture sensor objects to find one with a matching serial number
            for index in _controller_object.moisture_sensors.keys():
                if _controller_object.moisture_sensors[index].sn == _helper_object._device_serial:
                    ms_object = _controller_object.moisture_sensors[index]
                    break

            if ms_object is None:
                e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                        "MoistureSensor serial number. This can be done with the 'set' methods."
                raise ValueError(e_msg)

            msg += ",{0}={1}".format(
                Message.serial_number,
                _helper_object._device_serial
            )
            if _command_type is opcodes.set_action:
                msg += ",{0}={1},{2}={3}".format(
                    Message.variable_one,
                    _helper_object._threshold,
                    Message.variable_two,
                    ms_object.vp
                )
        # If status code has anything to do with a event switch
        elif _status_code in [Message.pause_event_switch, Message.stop_event_switch]:
            # These if statements check to make sure the correct object is passed into the function
            if _helper_object is None:
                e_msg = "Status code '{0}' requires you to pass in an object that inherits from BaseStartStopPause " \
                        "and that it be set to have a EventSwitch object attached to it.".format(
                            _status_code
                        )
                raise ValueError(e_msg)
            if _helper_object._device_serial is None:
                e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                        "EventSwitch serial number. This can be done with the 'set' methods."
                raise ValueError(e_msg)

            sw_object = None
            # Loops through the event switch objects to find one with a matching serial number
            for index in _controller_object.event_switches.keys():
                if _controller_object.event_switches[index].sn == _helper_object._device_serial:
                    sw_object = _controller_object.event_switches[index]
                    break

            if sw_object is None:
                e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                        "EventSwitch serial number. This can be done with the 'set' methods."
                raise ValueError(e_msg)

            msg += ",{0}={1}".format(
                Message.serial_number,
                _helper_object._device_serial
            )
            if _command_type is opcodes.set_action:
                msg += ",{0}={1},{2}={3}".format(
                    Message.variable_one,
                    _helper_object._mode,
                    Message.variable_two,
                    sw_object.vc
                )
        # If status code has anything to do with a temperature sensor
        elif _status_code in [Message.pause_temp_sensor, Message.stop_temp_sensor]:
            # These if statements check to make sure the correct object is passed into the function
            if _helper_object is None:
                e_msg = "Status code '{0}' requires you to pass in an object that inherits from BaseStartStopPause " \
                        "and that it be set to have a TemperatureSensor object attached to it.".format(
                            _status_code
                        )
                raise ValueError(e_msg)
            if _helper_object._device_serial is None:
                e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                        "TemperatureSensor serial number. This can be done with the 'set' methods."
                raise ValueError(e_msg)

            ts_object = None
            # Loops through the temperature sensor objects to find one with a matching serial number
            for index in _controller_object.temperature_sensors.keys():
                if _controller_object.temperature_sensors[index].sn == _helper_object._device_serial:
                    ts_object = _controller_object.temperature_sensors[index]
                    break

            if ts_object is None:
                e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                        "TemperatureSensor serial number. This can be done with the 'set' methods."
                raise ValueError(e_msg)

            msg += ",{0}={1}".format(
                Message.serial_number,
                _helper_object._device_serial
            )
            if _command_type is opcodes.set_action:
                msg += ",{0}={1},{2}={3}".format(
                    Message.variable_one,
                    _helper_object._threshold,
                    Message.variable_two,
                    ts_object.vd
                )

        elif _status_code in [opcodes.restore_failed, opcodes.restore_successful] and _command_type is opcodes.set_action:
            msg += ",{0}={1}".format(
                Message.variable_two,       # {0} Variable two opcode
                _helper_object              # {1} Source of backup file used opcode
            )

    # Creates the different value pairs to test against
    if _command_type is opcodes.get_action:
        _id = ""
        # At the present time we do not want to check the IDs that the substation returns us, so we do not build an id
        # string to verify against when we are dealing with a biCoder on the substation
        # if _controller_object.controller_type is not opcodes.substation:
        # Example ID=1_ZN_1_OK,DT=3/15/16 15:49:51,TX=Zone 1-TSQ0031\\\nSolenoid Short
        _id = build_the_message_id(_object=_controller_object,
                                   ct_type=MessageCategory.controller,
                                   _status_code=_status_code,
                                   _helper_object=_helper_object)

        if _controller_object.controller_type is "32":
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_3200() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_3200()
        else:
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_1000() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_1000()

        _tx = build_message_title(_object=_controller_object,
                                  _ct_type=MessageCategory.controller,
                                  _status_code=_status_code,
                                  _helper_object=_helper_object)
        return {opcodes.message: msg,       # this is the get command that is sent to the controller
                opcodes.message_id: _id,     # this is the message id that will be received from the controller
                opcodes.date_time: _dt,      # this is the message date/time that will be received from the controller
                opcodes.message_text: _tx,   # this is the detailed text that is received from the controller
                # opcodes.priority: pr
                }
    else:
        return msg


def create_event_switch_message(_event_switch_object, _status_code, _command_type):
    """
    # Example SET,MG,SW=RP00371,SS=BS
    :param _event_switch_object
    :type _event_switch_object: EventSwitch
    :param _status_code
    :type _status_code: str
    :param _command_type
    :type _command_type: str
    """
    # If the controller type is 3200, then create a 3200 message for the event_switch
    if _event_switch_object.controller_type is "32":
        # Creates the base for all event switch object messages for 3200 controller
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,              # {0} Command type passed in
            opcodes.message,            # {1} Message opcode
            opcodes.event_switch,       # {2} Event switch opcode
            _event_switch_object.sn,    # {3} Event switch serial number
            opcodes.status_code,        # {4} status_code opcode
            _status_code                # {5} Status code passed in
        )
    elif _event_switch_object.controller_type is opcodes.substation:
        # Creates the base for a zone object message on a substation
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,              # {1} Command type passed in
            opcodes.message,            # {2} Message opcode
            opcodes.event_switch,       # {3} Event Switch opcode
            _event_switch_object.sn,    # {4} Event Switch serial number
            opcodes.status_code,        # {5} Status code opcode
            _status_code                # {6} Status code passed in
        )
    else:
        # Creates the message for an event switch object that's part of a 1000 controller
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,              # {0} Command type passed in
            opcodes.message,            # {1} Message opcode
            opcodes.event_switch,       # {2} Event switch opcode
            _event_switch_object.ad,    # {3} Event switch address
            opcodes.status_code,        # {4} status_code opcode
            _status_code                # {5} Status code passed in
        )
    # Creates the different value pairs to test against
    if _command_type is opcodes.get_action:
        _id = ""
        # At the present time we do not want to check the IDs that the substation returns us, so we do not build an id
        # string to verify against when we are dealing with a biCoder on the substation
        if _event_switch_object.controller_type is not opcodes.substation:
            # Example ID=1_SW_1_OK,DT=3/15/16 15:49:51,TX=Event Switch 1-TSQ0031\\\nComm Error
            _id = build_the_message_id(_object=_event_switch_object,
                                       ct_type=MessageCategory.event_switch,
                                       _status_code=_status_code)

        if _event_switch_object.controller_type is "32":
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_3200() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_3200()
        else:
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_1000() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_1000()

        _tx = build_message_title(_object=_event_switch_object,
                                  _ct_type=MessageCategory.event_switch,
                                  _status_code=_status_code)
        return {opcodes.message: msg,       # this is the get command that is sent to the controller
                opcodes.message_id: _id,     # this is the message id that will be received from the controller
                opcodes.date_time: _dt,      # this is the message date/time that will be received from the controller
                opcodes.message_text: _tx}   # this is the detailed text that is received from the controller
    else:
        return msg


def create_flow_meter_message(_flow_meter_object, _status_code, _command_type):
    """
    # Example SET,MG,FM=WMV0705,SS=BS
    :param _flow_meter_object
    :type _flow_meter_object: FlowMeter
    :param _status_code
    :type _status_code: str
    :param _command_type
    :type _command_type: str
    """
    # If the controller type is 3200, then create a 3200 message for the flow meter
    if _flow_meter_object.controller_type is "32":
        # Creates the base for all flow meter object messages
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,              # {0} Command type passed in
            opcodes.message,            # {1} Message opcode
            opcodes.flow_meter,         # {2} Flow meter opcode
            _flow_meter_object.sn,      # {3} Flow meter serial number
            opcodes.status_code,        # {4} Status code opcode
            _status_code                # {5} Status code passed in
        )
        # Add a POC number to each message that doesn't contain the "UF" status and the "GET" or "DO" commands
        if _status_code is not Message.set_upper_limit_failed or _command_type is opcodes.set_action:
            poc_number = 0
            # Loops through the POC objects in the object bucket until a matching flow meter address is found and then
            # stores the "ad" of the POC and adds it to the message string
            for index in _flow_meter_object.controller.points_of_control.keys():
                if _flow_meter_object.controller.points_of_control[index].fm == _flow_meter_object.ad:
                    poc_number = _flow_meter_object.controller.points_of_control[index].ad
                    break
            msg += ",{0}={1}".format(
                opcodes.point_of_connection,    # {0} POC opcode
                poc_number                      # {1} Point of connection address associated with the passed in flow meter
            )

    elif _flow_meter_object.controller_type is opcodes.substation:
        # Creates the base for a zone object message on a substation
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,              # {1} Command type passed in
            opcodes.message,            # {2} Message opcode
            opcodes.flow_meter,         # {3} Flow Meter opcode
            _flow_meter_object.sn,      # {4} Flow Meter serial number
            opcodes.status_code,        # {5} Status code opcode
            _status_code                # {6} Status code passed in
        )
    else:
        # Creates the message for a flow meter object that is part of a 1000 controller
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,              # {0} Command type passed in
            opcodes.message,            # {1} Message opcode
            opcodes.flow_meter,         # {2} Flow meter opcode
            _flow_meter_object.ad,      # {3} Flow meter address
            opcodes.status_code,        # {4} Status code opcode
            _status_code                # {5} Status code passed in
        )
    # Creates the different value pairs to test against
    if _command_type is opcodes.get_action:
        _id = ""
        # At the present time we do not want to check the IDs that the substation returns us, so we do not build an id
        # string to verify against when we are dealing with a biCoder on the substation
        if _flow_meter_object.controller_type is not opcodes.substation:
            # Example ID=1_FM_1_OK,DT=3/15/16 15:49:51,TX=Flow Sensor 1-TSQ0031\\\nNo Reply
            _id = build_the_message_id(_object=_flow_meter_object,
                                       ct_type=MessageCategory.flow_meter,
                                       _status_code=_status_code)

        if _flow_meter_object.controller_type is "32":
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_3200() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_3200()
        else:
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_1000() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_1000()

        _tx = build_message_title(_object=_flow_meter_object,
                                  _ct_type=MessageCategory.flow_meter,
                                  _status_code=_status_code)
        return {opcodes.message: msg,        # this is the get command that is sent to the controller
                opcodes.message_id: _id,     # this is the message id that will be received from the controller
                opcodes.date_time: _dt,      # this is the message date/time that will be received from the controller
                opcodes.message_text: _tx}   # this is the detailed text that is received from the controller
    else:
        return msg


def create_mainline_message(_mainline_object, _status_code, _command_type):
    """
    # Example SET,MG,ML=1,SS=EV
    :param _mainline_object
    :type _mainline_object: Mainline
    :param _status_code
    :type _status_code: str
    :param _command_type
    :type _command_type: str
    """
    # Creates the base for all mainline object messages
    msg = "{0},{1},{2}={3},{4}={5}".format(
        _command_type,          # {0} Command type passed in
        opcodes.message,        # {1} Message opcode
        opcodes.mainline,       # {2} Program opcode
        _mainline_object.ad,    # {3} Mainline address
        opcodes.status_code,    # {4} Status code opcode
        _status_code            # {5} Status code passed in
    )

    # Finds the POC that is associated with the mainline that was passed in, if it is not found, raise ValueError
    poc = False
    for index in _mainline_object.controller.points_of_control.keys():
        if _mainline_object.controller.points_of_control[index].ml == _mainline_object.ad:
            poc = _mainline_object.controller.points_of_control[index]
            break
    if not poc:
        e_msg = "The address of the mainline object ({0}) that was passed in did not match any of the addresses of " \
                "mainline objects that were stored in the POC objects bucket. Status code '{1}' failed to create a " \
                "message.".format(
                    _mainline_object.ad,    # {0} Mainline address
                    _status_code            # {1} Status code passed in
                )
        raise ValueError(e_msg)

    # Finds the flow meter that is associated with the passed in mainline object by using the POC found in the previous
    # lines of code, if not found, raise ValueError.
    fm = False
    for index in _mainline_object.controller.flow_meters.keys():
        if _mainline_object.controller.flow_meters[index].ad == poc.fm:
            fm = _mainline_object.controller.flow_meters[index]
            break
    if not fm:
        e_msg = "The address of the flow meter object ({0}) that was associated with the mainline object did not " \
                "match any of the flow meter objects in the object bucket. Status code '{1} failed to create a " \
                "message.".format(
                    poc.fm,         # {0} Flow meter address associated with the passed in mainline object
                    _status_code    # {1} Status code passed in
                )
        raise ValueError(e_msg)

    # If the status code is dealing with "flow fail", add the POC number and the flow meter serial to the message
    if _status_code in [Message.learn_flow_fail_flow_biCoders_disabled, Message.learn_flow_fail_flow_biCoders_error]:
        msg += ",{0}={1}".format(
            opcodes.point_of_connection,    # {0} POC opcodes
            poc.ad                          # {1} Address of the POC object that is associated with passed in mainline
        )
        if _command_type is opcodes.set_action:
            msg += ",{0}={1}".format(
                Message.serial_number,      # {0} Serial number Message opcode
                fm.sn                       # {1} Flow meter serial number associated with passed in mainline
            )

    if _status_code in [Message.high_flow_variance_detected, Message.low_flow_variance_detected]:
        # Find the zone that is related to the mainline object by checking what zone program has a program with a
        # similar mainline address as the mainline object address
        zone = False
        for index in _mainline_object.controller.zone_programs.keys():
            if _mainline_object.controller.zone_programs[index].program.ml == poc.ml:
                zone = _mainline_object.controller.zone_programs[index].zone
                break
        # If no point of connection was found in the object bucket with the same mainline address as the address of the
        # mainline in the program object associated with the zone program, raise a ValueError
        if not zone:
            e_msg = "Could not find an association for a mainline and a zone. Please configure the objects so that" \
                    "there is a relationship between mainline and zone. Status code {0} failed.".format(
                        _status_code        # {1} Status code passed in
                    )
            raise ValueError(e_msg)

        msg += ",{0}={1}".format(
            opcodes.point_of_connection,
            poc.ad
        )

        if _command_type is opcodes.set_action:
            msg += ",{0}={1},{2}={3},{4}={5}".format(
                opcodes.zone,
                _mainline_object.ad,
                Message.variable_one,
                zone.df,
                Message.variable_two,
                fm.vr
            )

    # Creates the different value pairs to test against
    if _command_type is opcodes.get_action:
        # Example ID=1_ML_1_OK,DT=3/15/16 15:49:51,TX=Mainline 1-TSQ0031\\\nNo Reply
        _id = build_the_message_id(_object=_mainline_object,
                                   ct_type=MessageCategory.mainline,
                                   _status_code=_status_code)
        _dt = date_mngr.controller_datetime.msg_date_string_for_controller_3200() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_3200()

        _tx = build_message_title(_object=_mainline_object,
                                  _ct_type=MessageCategory.mainline,
                                  _status_code=_status_code)
        return {opcodes.message: msg,       # this is the get command that is sent to the controller
                opcodes.message_id: _id,     # this is the message id that will be received from the controller
                opcodes.date_time: _dt,      # this is the message date/time that will be received from the controller
                opcodes.message_text: _tx}   # this is the detailed text that is received from the controller
    else:
        return msg


def create_moisture_sensor_message(_moisture_sensor_object, _status_code, _command_type):
    """
    # Example SET,MG,MS=SB01429,SS=BS
    :param _moisture_sensor_object
    :type _moisture_sensor_object: MoistureSensor
    :param _status_code
    :type _status_code: str
    :param _command_type
    :type _command_type: str
    """
    # If the controller type is 3200, then create a 3200 message for the moisture sensor
    if _moisture_sensor_object.controller_type is "32":
        # Creates the base for all 3200 moisture sensor object messages
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,                  # {0} Command type passed in
            opcodes.message,                # {1} Message opcode
            opcodes.moisture_sensor,        # {2} Moisture sensor opcode
            _moisture_sensor_object.sn,     # {3} Moisture sensor serial number
            opcodes.status_code,            # {4} Status code opcode
            _status_code                    # {5} Status code passed in
        )
    elif _moisture_sensor_object.controller_type is opcodes.substation:
        # Creates the base for a zone object message on a substation
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,                  # {1} Command type passed in
            opcodes.message,                # {2} Message opcode
            opcodes.moisture_sensor,        # {3} Moisture Sensor opcode
            _moisture_sensor_object.sn,     # {4} Moisture Sensor serial number
            opcodes.status_code,            # {5} Status code opcode
            _status_code                    # {6} Status code passed in
        )
    else:
        # Creates the message for a moisture sensor object that is part of a 1000 controller
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,                  # {0} Command type passed in
            opcodes.message,                # {1} Message opcode
            opcodes.moisture_sensor,        # {2} Moisture sensor opcode
            _moisture_sensor_object.ad,     # {3} Moisture sensor address
            opcodes.status_code,            # {4} Status code opcode
            _status_code                    # {5} Status code passed in
        )
    # Creates the different value pairs to test against
    if _command_type is opcodes.get_action:
        _id = ""
        # At the present time we do not want to check the IDs that the substation returns us, so we do not build an id
        # string to verify against when we are dealing with a biCoder on the substation
        if _moisture_sensor_object.controller_type is not opcodes.substation:
            # Example ID=1_MS_1_OK,DT=3/15/16 15:49:51,TX=Moisture Sensor 1-00S0001\\\nComm Error
            _id = build_the_message_id(_object=_moisture_sensor_object,
                                       ct_type=MessageCategory.moisture_sensor,
                                       _status_code=_status_code)

        if _moisture_sensor_object.controller_type is "32":
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_3200() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_3200()
        else:
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_1000() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_1000()

        _tx = build_message_title(_object=_moisture_sensor_object,
                                  _ct_type=MessageCategory.moisture_sensor,
                                  _status_code=_status_code)
        return {opcodes.message: msg,       # this is the get command that is sent to the controller
                opcodes.message_id: _id,     # this is the message id that will be received from the controller
                opcodes.date_time: _dt,      # this is the message date/time that will be received from the controller
                opcodes.message_text: _tx}   # this is the detailed text that is received from the controller
    else:
        return msg


def create_master_valve_message(_master_valve_object, _status_code, _command_type):
    """
    # Example SET,MG,MV=D004048,SS=BS
    :param _master_valve_object
    :type _master_valve_object: MasterValve
    :param _status_code
    :type _status_code: str
    :param _command_type
    :type _command_type: str
    """
    # If the controller type is 3200, then check if the message requires any different variables
    if _master_valve_object.controller_type is "32":
        # Creates the base for all master valve object messages
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,              # {0} Command type passed in
            opcodes.message,            # {1} Message opcode
            opcodes.master_valve,       # {2} Master valve
            _master_valve_object.sn,    # {3} Master valve serial number
            opcodes.status_code,        # {4} Status code opcode
            _status_code                # {5} Status code passed in
        )
        # If the status code is either "open circuit" or "short circuit", zone number must be included.
        if _status_code in [Message.open_circuit, Message.short_circuit]:
            msg += ",{0}={1}".format(
                opcodes.zone,                       # {0} Zone opcode
                str(_master_valve_object.ad + 200)  # {1} Address of master valve with 200 added on
            )
    elif _master_valve_object.controller_type is opcodes.substation:
        # Creates the base for a zone object message on a substation
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,              # {1} Command type passed in
            opcodes.message,            # {2} Message opcode
            opcodes.master_valve,       # {3} Master Valve opcode
            _master_valve_object.sn,    # {4} Master Valve serial number
            opcodes.status_code,        # {5} Status code opcode
            _status_code                # {6} Status code passed in
        )
    else:
        # Creates the message for a master valve object that is part of a 1000 controller
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,              # {0} Command type passed in
            opcodes.message,            # {1} Message opcode
            opcodes.master_valve,       # {2} Master valve
            _master_valve_object.ad,    # {3} Master valve address
            opcodes.status_code,        # {4} Status code opcode
            _status_code                # {5} Status code passed in
        )
    # Creates the different value pairs to test against
    if _command_type is opcodes.get_action:
        _id = ""
        # At the present time we do not want to check the IDs that the substation returns us, so we do not build an id
        # string to verify against when we are dealing with a biCoder on the substation
        if _master_valve_object.controller_type is not opcodes.substation:
            # Example ID=1_MS_1_OK,DT=3/15/16 15:49:51,TX=Moisture Sensor 1-00S0001\\\nComm Error
            _id = build_the_message_id(_object=_master_valve_object,
                                       ct_type=MessageCategory.master_valve,
                                       _status_code=_status_code)

        if _master_valve_object.controller_type is "32":
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_3200() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_3200()
        else:
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_1000() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_1000()

        _tx = build_message_title(_object=_master_valve_object,
                                  _ct_type=MessageCategory.master_valve,
                                  _status_code=_status_code)
        return {opcodes.message: msg,       # this is the get command that is sent to the controller
                opcodes.message_id: _id,     # this is the message id that will be received from the controller
                opcodes.date_time: _dt,      # this is the message date/time that will be received from the controller
                opcodes.message_text: _tx}   # this is the detailed text that is received from the controller
    else:
        return msg


def create_temp_sensor_message(_temp_sensor_object, _status_code, _command_type):
    """
    # Example SET,MG,TS=SB01429,SS=BS
    :param _temp_sensor_object
    :type _temp_sensor_object: TemperatureSensor
    :param _status_code
    :type _status_code: str
    :param _command_type
    :type _command_type: str
    """
    # If the controller type is 3200, then check if the message requires any different variables
    if _temp_sensor_object.controller_type is "32":
        # Creates the base for all temperature object messages
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,                  # {0} Command type passed in
            opcodes.message,                # {1} Message opcode
            opcodes.temperature_sensor,     # {2} Temperature sensor opcode
            _temp_sensor_object.sn,         # {3} Temperature sensor serial number
            opcodes.status_code,            # {4} Status code opcode
            _status_code                    # {5} Status code passed in
        )
    elif _temp_sensor_object.controller_type is opcodes.substation:
        # Creates the base for a zone object message on a substation
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,                  # {1} Command type passed in
            opcodes.message,                # {2} Message opcode
            opcodes.temperature_sensor,     # {3} Temperature Sensor opcode
            _temp_sensor_object.sn,         # {4} Temperature Sensor serial number
            opcodes.status_code,            # {5} Status code opcode
            _status_code                    # {6} Status code passed in
        )
    else:
        # Creates the message for a temperature sensor object that is part of a 1000 controller
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,                  # {0} Command type passed in
            opcodes.message,                # {1} Message opcode
            opcodes.temperature_sensor,     # {2} Temperature sensor opcode
            _temp_sensor_object.ad,         # {3} Temperature sensor address
            opcodes.status_code,            # {4} Status code opcode
            _status_code                    # {5} Status code passed in
        )
    # Creates the different value pairs to test against
    if _command_type is opcodes.get_action:
        _id = ""
        # At the present time we do not want to check the IDs that the substation returns us, so we do not build an id
        # string to verify against when we are dealing with a biCoder on the substation
        if _temp_sensor_object.controller_type is not opcodes.substation:
            # Example ID=1_MS_1_OK,DT=3/15/16 15:49:51,TX=Moisture Sensor 1-00S0001\\\nComm Error
            _id = build_the_message_id(_object=_temp_sensor_object,
                                       ct_type=MessageCategory.temperature_sensor,
                                       _status_code=_status_code)

        if _temp_sensor_object.controller_type is "32":
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_3200() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_3200()
        else:
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_1000() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_1000()

        _tx = build_message_title(_object=_temp_sensor_object,
                                  _ct_type=MessageCategory.temperature_sensor,
                                  _status_code=_status_code)
        return {opcodes.message: msg,       # this is the get command that is sent to the controller
                opcodes.message_id: _id,     # this is the message id that will be received from the controller
                opcodes.date_time: _dt,      # this is the message date/time that will be received from the controller
                opcodes.message_text: _tx}   # this is the detailed text that is received from the controller
    else:
        return msg


def create_poc_3200_message(_poc_object, _status_code, _command_type, _use_to_verify=False):
    """
    # Example SET,MG,PG=1,SS=EV
    :param _poc_object
    :type _poc_object: POC3200
    :param _status_code
    :type _status_code: str
    :param _command_type
    :type _command_type: str
    """
    # If the controller type is 3200, then check if the message requires any different variables
    if _poc_object.controller_type is "32":
        # Creates the base for all poc object messages
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,                  # {0} Command type passed in
            opcodes.message,                # {1} Message opcode
            "PO",                           # {2} Point of connection opcode
            _poc_object.ad,                 # {3} POC address
            opcodes.status_code,            # {4} Status code opcode
            _status_code                    # {5} Status code passed in
        )
        # Finds the flow meter that is referenced by the POC that was passed in, if it is not found, raise ValueError
        flow_meter = False
        for index in _poc_object.controller.flow_meters.keys():
            if _poc_object.controller.flow_meters[index].ad == _poc_object.fm:
                flow_meter = _poc_object.controller.flow_meters[index]
        if not flow_meter:
            e_msg = "The address of the flow meter object ({0}) contained in the POC object did not match any of the " \
                    "flow meter object's addresses in the object bucket. Status code '{1}' failed to create a " \
                    "message.".format(
                        _poc_object.fm,     # {0} Flow meter address associated with the passed in POC object
                        _status_code        # {1} Status code passed in
                    )
            raise ValueError(e_msg)

        # If the status code deal with the water budget, add the "budget" and "used" variables to the message
        if _status_code in [Message.budget_exceeded, Message.budget_exceeded_shutdown] and _command_type is opcodes.set_action:
            msg += ",{0}={1},{2}={3}".format(
                Message.variable_one,       # {0} Variable one Message opcode
                _poc_object.wb,             # {1} POC monthly water budget
                Message.variable_two,       # {2} Variable two Message opcode
                flow_meter.vg               # {3} Flow reported usage in total gallons
            )
        # If the status code deals with high flow, add "high flow limit" and "current flow rate" to the message
        elif _command_type is opcodes.set_action and _status_code in [Message.high_flow_detected, Message.high_flow_shutdown]:
            msg += ",{0}={1},{2}={3}".format(
                Message.variable_one,       # {0} Variable one Message opcode
                _poc_object.hf,             # {1} POC high flow limit
                Message.variable_two,       # {2} Variable two Message opcode
                flow_meter.vr               # {3} Flow rate in gallons per minute
            )
        # If the status code deals with unscheduled flow add "unscheduled flow limit" and "current flow rate" to the message
        elif _command_type is opcodes.set_action and _status_code in [Message.unscheduled_flow_detected, Message.unscheduled_flow_shutdown]:
            msg += ",{0}={1},{2}={3}".format(
                Message.variable_one,       # {0} Variable one Message opcode
                _poc_object.uf,             # {1} POC unscheduled flow limit
                Message.variable_two,       # {2} Variable two Message opcode
                flow_meter.vr               # {3} Flow rate in gallons per minute
            )

        # Add the flow meter serial number to the end of every message except status code "empty shutdown"
        if _status_code != opcodes.empty_shutdown:
            msg += ",{0}={1}".format(
                Message.serial_number,      # {0} Serial number Message opcode
                flow_meter.sn               # {1} Flow meter serial number
            )
        if _status_code is Message.empty_shutdown:
            event_switch = False
            moisture_sensor = False
            # find event switch in the given POC object with a matching address
            for index in _poc_object.controller.event_switches.keys():
                if _poc_object.controller.event_switches[index].ad == _poc_object.sw:
                    event_switch = _poc_object.controller.event_switches[index]

            for index in _poc_object.controller.moisture_sensors.keys():
                if _poc_object.controller.moisture_sensors[index].ad == _poc_object.ms:
                    moisture_sensor = _poc_object.controller.moisture_sensors[index]
            # because this fails only if both a ms or a sw are not found we and we are just looking for an address
            # we used the _object_poc.sw for the error message
            if not event_switch and not moisture_sensor:
                e_msg = "None of the devices passed in str in the object bucket associated with this POC object " \
                        "({0}). Status code '{1}' failed.".format(
                         _poc_object.sw,  # {0} Event switch address associated with the passed in POC object
                         _status_code    # {1} Status code passed in
                         )
                raise ValueError(e_msg)
            # if an event switch is found the objct holds either a cl or an op we need to change them to open
            # or closed for the message
            switch_empty_condition = None
            event_switch_vc = None
            mapping = None
            if event_switch:
                mapping = {
                    'CL': 'Closed',
                    'OP': 'Open'
                }
            if event_switch and _use_to_verify:
                switch_empty_condition = mapping[_poc_object.se]
                event_switch_vc = mapping[event_switch.vc]

            if event_switch:
                msg += ",{0}={1},{2}={3},{4}={5}".format(
                    Message.serial_number,      # {0} this is "DV"
                    event_switch.sn,            # {1} this is the empty condition device serial number
                    Message.variable_one,       # {2}
                    switch_empty_condition,     # {3} POC's position of switch empty condition
                    Message.variable_two,       # {4}
                    event_switch_vc             # {5} Event switches contact state
                )
            if moisture_sensor:
                msg += ",{0}={1},{2}={3},{4}={5}".format(
                    Message.serial_number,   # {0} this is "DV"
                    moisture_sensor.sn,      # {1} this is the empty condition device serial number
                    Message.variable_one,    # {2}
                    _poc_object.me,          # {3} POC's moisture sensor threashold set limit
                    Message.variable_two,    # {4}
                    moisture_sensor.vp       # {5} current moisture reading
                )

    else:
        # Creates the message for a point of connection object that is part of a 1000 controller
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,                  # {0} Command type passed in
            opcodes.message,                # {1} Message opcode
            opcodes.point_of_connection,    # {2} Point of connection opcode
            _poc_object.ad,                 # {3} POC address
            opcodes.status_code,            # {4} Status code opcode
            _status_code                    # {5} Status code passed in
        )
    # Creates the different value pairs to test against
    if _command_type is opcodes.get_action:
        # Example ID=1_PC_1_OK,DT=3/15/16 15:49:51,TX=Program 1-00S0001\\\nComm Error
        _id = build_the_message_id(_object=_poc_object,
                                   ct_type=MessageCategory.point_of_connection,
                                   _status_code=_status_code)
        if _poc_object.controller_type is "32":
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_3200() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_3200()
        else:
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_1000() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_1000()

        _tx = build_message_title(_object=_poc_object,
                                  _ct_type=MessageCategory.point_of_connection,
                                  _status_code=_status_code,
                                  _use_to_verify=_use_to_verify)
        return {opcodes.message: msg,       # this is the get command that is sent to the controller
                opcodes.message_id: _id,     # this is the message id that will be received from the controller
                opcodes.date_time: _dt,      # this is the message date/time that will be received from the controller
                opcodes.message_text: _tx}   # this is the detailed text that is received from the controller
    else:
        return msg


def create_program_3200_message(_program_object, _status_code, _command_type, _helper_object=None):
    """
    # Example SET,MG,PG=1,SS=EV
    :param _program_object
    :type _program_object: PG3200
    :param _status_code
    :type _status_code: str
    :param _command_type
    :type _command_type: str
    :param _helper_object
        - This object should have inherited from BaseStartStopPause and have either a MoistureSensor, TemperatureSensor,
        or EventSwitch object as one of it's variables. _helper_object only needs to be passed in when the status code
        deals with one of the three aforementioned objects.
    :type _helper_object: pg_start_stop_pause_cond.BaseStartStopPause
    """
    # If the controller type is 3200, then check if the message requires any different variables
    if _program_object.controller_type is "32":
        # Creates the base for all 3200 program object messages
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,          # {0} Command type passed in
            opcodes.message,        # {1} Message opcode
            opcodes.program,        # {2} Program opcode
            _program_object.ad,     # {3} Program address
            opcodes.status_code,    # {4} Status code opcode
            _status_code            # {5} Status code passed in
        )
        # If status code has anything to do with a moisture sensor
        if _status_code in [Message.skipped_by_moisture_sensor, Message.started_by_bad_moisture_sensor,
                            Message.pause_moisture_sensor, Message.stop_moisture_sensor, Message.started_moisture_sensor]:
            # These if statements check to make sure the correct object is passed into the function
            if _helper_object is None:
                e_msg = "Status code '{0}' requires you to pass in an object that inherits from BaseStartStopPause " \
                        "and that it be set to have a MoistureSensor object attached to it.".format(
                            _status_code
                        )
                raise ValueError(e_msg)
            if _helper_object._device_serial is None:
                e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                        "MoistureSensor serial number. This can be done with the 'set' methods."
                raise ValueError(e_msg)

            ms_object = None
            # Loops through the moisture sensor objects to find one with a matching serial number
            for index in _program_object.controller.moisture_sensors.keys():
                if _program_object.controller.moisture_sensors[index].sn == _helper_object._device_serial:
                    ms_object = _program_object.controller.moisture_sensors[index]
                    break

            if ms_object is None:
                e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                        "MoistureSensor serial number. This can be done with the 'set' methods."
                raise ValueError(e_msg)

            msg += ",{0}={1}".format(
                Message.serial_number,
                _helper_object._device_serial
            )
            if _command_type is opcodes.set_action and _status_code in [Message.skipped_by_moisture_sensor,
                                                                        Message.pause_moisture_sensor,
                                                                        Message.stop_moisture_sensor,
                                                                        Message.started_moisture_sensor]:
                msg += ",{0}={1},{2}={3}".format(
                    Message.variable_one,
                    _helper_object._threshold,
                    Message.variable_two,
                    ms_object.vp
                )
        # If status code has anything to do with a event switch
        elif _status_code in [Message.started_event_switch, Message.pause_event_switch, Message.stop_event_switch]:
            # These if statements check to make sure the correct object is passed into the function
            if _helper_object is None:
                e_msg = "Status code '{0}' requires you to pass in an object that inherits from BaseStartStopPause " \
                        "and that it be set to have a EventSwitch object attached to it.".format(
                            _status_code
                        )
                raise ValueError(e_msg)
            if _helper_object._device_serial is None:
                e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                        "EventSwitch serial number. This can be done with the 'set' methods."
                raise ValueError(e_msg)

            sw_object = None
            # Loops through the event switch objects to find one with a matching serial number
            for index in _program_object.controller.event_switches.keys():
                if _program_object.controller.event_switches[index].sn == _helper_object._device_serial:
                    sw_object = _program_object.controller.event_switches[index]
                    break

            if sw_object is None:
                e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                        "EventSwitch serial number. This can be done with the 'set' methods."
                raise ValueError(e_msg)

            msg += ",{0}={1}".format(
                Message.serial_number,
                _helper_object._device_serial
            )
            if _command_type is opcodes.set_action:
                msg += ",{0}={1},{2}={3}".format(
                    Message.variable_one,
                    _helper_object._mode,
                    Message.variable_two,
                    sw_object.vc
                )
        # If status code has anything to do with a temperature sensor
        elif _status_code in [Message.started_temp_sensor, Message.pause_temp_sensor, Message.stop_temp_sensor]:
            # These if statements check to make sure the correct object is passed into the function
            if _helper_object is None:
                e_msg = "Status code '{0}' requires you to pass in an object that inherits from BaseStartStopPause " \
                        "and that it be set to have a TemperatureSensor object attached to it.".format(
                            _status_code
                        )
                raise ValueError(e_msg)
            if _helper_object._device_serial is None:
                e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                        "TemperatureSensor serial number. This can be done with the 'set' methods."
                raise ValueError(e_msg)

            ts_object = None
            # Loops through the temperature sensor objects to find one with a matching serial number
            for index in _program_object.controller.temperature_sensors.keys():
                if _program_object.controller.temperature_sensors[index].sn == _helper_object._device_serial:
                    ts_object = _program_object.controller.temperature_sensors[index]
                    break

            if ts_object is None:
                e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                        "TemperatureSensor serial number. This can be done with the 'set' methods."
                raise ValueError(e_msg)

            msg += ",{0}={1}".format(
                Message.serial_number,
                _helper_object._device_serial
            )
            if _command_type is opcodes.set_action:
                msg += ",{0}={1},{2}={3}".format(
                    Message.variable_one,
                    _helper_object._threshold,
                    Message.variable_two,
                    ts_object.vd
                )
        # If the command requires a POC number
        elif _status_code in [Message.learn_flow_with_errors, Message.learn_flow_success,
                              Message.restricted_time_by_water_ration]:
            # Try to find a POC in the object bucket that has the same mainline address as the program object that was
            # passed in
            poc_number = False
            for index in _program_object.controller.points_of_control.keys():
                if _program_object.controller.points_of_control[index].ml == _program_object.ml:
                    poc_number = _program_object.controller.points_of_control[index].ad
                    break
            # If no POC object was found that has a matching mainline address, raise a ValueError
            if not poc_number:
                e_msg = "The address of the program object's mainline ({0}) did not match any of the POC's mainline " \
                        "addresses. Status code '{1}' failed to create a message.".format(
                            _program_object.ml,     # {0} Mainline number for program
                            _status_code            # {1} Status code passed in
                        )
                raise ValueError(e_msg)
            # TODO water rationing variable needs to be found, sa is not correct
            if _status_code is Message.restricted_time_by_water_ration and _command_type is opcodes.set_action:
                msg += ",{0}={1},{2}={3}".format(
                    Message.variable_one,           # {0} Variable one opcode
                    _program_object.wr,             # {1} Seasonal adjusment percentage
                    opcodes.point_of_connection,    # {0} POC opcode
                    poc_number                      # {1} Point of connection address
                )
            else:
                msg += ",{0}={1}".format(
                    opcodes.point_of_connection,    # {0} POC opcode
                    poc_number                      # {1} Point of connection address
                )
    else:
        # Creates the message for a program object that is part of a 1000 controller
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,          # {0} Command type passed in
            opcodes.message,        # {1} Message opcode
            opcodes.program,        # {2} Program opcode
            _program_object.ad,     # {3} Program address
            opcodes.status_code,    # {4} Status code opcode
            _status_code            # {5} Status code passed in
        )
        # Checks "WHO" and appends it to the set/get/do message
        if _status_code in [Message.start, Message.stop, Message.pause]:
            msg += ",WO={0}".format(
                _helper_object
            )

    # Creates the different value pairs to test against
    if _command_type is opcodes.get_action:
        # Example ID=1_PG_1_OK,DT=3/15/16 15:49:51,TX=Program 1-00S0001\\\nComm Error
        _id = build_the_message_id(_object=_program_object,
                                   ct_type=MessageCategory.program,
                                   _status_code=_status_code,
                                   _helper_object=_helper_object)

        if _program_object.controller_type is "32":
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_3200() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_3200()
        else:
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_1000() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_1000()

        _tx = build_message_title(_object=_program_object,
                                  _ct_type=MessageCategory.program,
                                  _status_code=_status_code,
                                  _helper_object=_helper_object)
        return {opcodes.message: msg,       # this is the get command that is sent to the controller
                opcodes.message_id: _id,     # this is the message id that will be received from the controller
                opcodes.date_time: _dt,      # this is the message date/time that will be received from the controller
                opcodes.message_text: _tx}   # this is the detailed text that is received from the controller
    else:
        return msg


def create_zone_program_message(_zone_program_object, _status_code, _command_type):
    """
    # Example ID=1_ZN_1_OK,DT=3/15/16 15:49:51,TX=Zone 1-TSQ0031\\\nSolenoid Short
    :param _zone_program_object
    :type _zone_program_object: ZoneProgram
    :param _status_code
    :type _status_code: str
    :param _command_type
    :type _command_type: str
    """
    # Creates the base for all zone object messages
    msg = "{0},{1},{2}={3},{4}={5}".format(
        _command_type,                  # {0} Command type passed in
        opcodes.message,                # {1} Message opcode
        opcodes.zone,                   # {2} Zone opcode
        _zone_program_object.zone.ad,   # {3} Zone program zone's address
        opcodes.status_code,            # {4} Status code opcode
        _status_code                    # {5} Status code passed in
    )
    # If the status code deals with the moisture sensor of the zone program
    if _status_code in [Message.calibrate_failure_no_change, Message.calibrate_successful,
                        Message.calibrate_failure_no_saturation, Message.requires_soak_cycle]:
        # Try to find a moisture sensor in the object bucket that has the same address as the moisture sensor
        # associated with the zone program
        ms = False
        for index in _zone_program_object.controller.moisture_sensors.keys():
            if _zone_program_object.controller.moisture_sensors[index].ad == _zone_program_object.ms:
                ms = _zone_program_object.controller.moisture_sensors[index]
                break
        # If no moisture sensor was found in the object bucket with the same address as the address of the moisture
        # sensor associated with the zone program, raise a ValueError.
        if not ms:
            e_msg = "Zone {0} in Program {1} does not have a moisture sensor attached. " \
                    "To successfully run the {2}, {3}, {4}, or {5} commands, there must be a moisture sensor " \
                    "assigned to the primary zone.".format(
                        str(_zone_program_object.zone.ad),          # {0} zone address
                        str(_zone_program_object.program.ad),       # {1} program address
                        Message.calibrate_failure_no_change,
                        Message.calibrate_successful,
                        Message.calibrate_failure_no_saturation,
                        Message.requires_soak_cycle
                    )
            raise ValueError(e_msg)
        # Add the program number and moisture sensor's serial number to the message
        msg += ",{0}={1},{2}={3}".format(
            opcodes.program,                    # {0} Program opcode
            _zone_program_object.program.ad,    # {1} Program address associated with this zone program
            Message.serial_number,              # {2} Serial number Message opcode
            ms.sn                               # {3} Serial number of the moisture sensor associated with this zp
        )
        # If the status code is "no change" or "no saturation", add the moisture percentage to the message
        if _status_code in [Message.calibrate_failure_no_change, Message.calibrate_failure_no_saturation] and _command_type is opcodes.set_action:
            msg += ",{0}={1}".format(
                Message.variable_one,       # {0} Variable one Message opcode
                ms.vp                       # {1} Moisture percent on the moisture sensor
            )
        # If the status code is "calibrate successful", add both the lower(turn on) limit and upper(turn off) limit
        elif _status_code is Message.calibrate_successful:
            msg += ",{0}={1},{2}={3}".format(
                Message.variable_one,       # {0} Variable one Message opcode
                _zone_program_object.ll,    # {1} Zone program lower limit
                Message.variable_two,       # {2} Variable two Message opcode
                _zone_program_object.ul     # {3} Zone program upper limit
            )
    # If the status code deals with a flow shutdown by a flow station, add the program number to the message
    elif _status_code in [Message.low_flow_shutdown_by_flow_station, Message.high_flow_shutdown_by_flow_station]:
        msg += ",{0}={1}".format(
            opcodes.program,                    # {0} Program opcode
            _zone_program_object.program.ad     # {1} Program address associated with this zone program
        )
    elif _status_code in [Message.exceeds_design_flow, Message.flow_learn_errors, Message.flow_learn_ok,
                          Message.high_flow_variance_shutdown, Message.high_flow_variance_detected,
                          Message.low_flow_variance_shutdown, Message.low_flow_variance_detected]:
        # Try to find a point of connection in the object bucket that shares the same mainline address as the program
        # object that is associated with zone program
        poc = False
        for index in _zone_program_object.controller.points_of_control.keys():
            if _zone_program_object.controller.points_of_control[index].ml == _zone_program_object.program.ml:
                poc = _zone_program_object.controller.points_of_control[index]
                break
        # If no point of connection was found in the object bucket with the same mainline address as the address of the
        # mainline in the program object associated with the zone program, raise a ValueError
        if not poc:
            e_msg = "The address of the mainline object ({0}) that was associated with the passed in zone program " \
                    "could not be associated with any of the POCs in the object bucket. Status code '{1}' failed to " \
                    "create a message.".format(
                        _zone_program_object.program.ml,    # {0} Mainline associated with this zone program
                        _status_code                        # {1} Status code passed in
                    )
            raise ValueError(e_msg)
        # Try to find a point of connection in the object bucket that shares the same mainline address as the program
        # object that is associated with zone program
        fm = False
        for index in _zone_program_object.controller.flow_meters.keys():
            if _zone_program_object.controller.flow_meters[index].ad == poc.fm:
                fm = _zone_program_object.controller.flow_meters[index]
                break
        # If no point of connection was found in the object bucket with the same mainline address as the address of the
        # mainline in the program object associated with the zone program, raise a ValueError
        if not fm:
            e_msg = "The address of the flow meter object ({0}) that was associated with the passed in zone program " \
                    "could not be associated with any of the flow meter objects in the object bucket. Status code " \
                    "'{1}' failed to create a message.".format(
                        poc.fm,         # {0} Flow meter associated with this zone program
                        _status_code    # {1} Status code passed in
                    )
            raise ValueError(e_msg)
        # Add the point of connection number and program number to the messages
        msg += ",{0}={1},{2}={3}".format(
            opcodes.point_of_connection,        # {0} POC opcode
            poc.ad,                             # {1} Point of connection address
            opcodes.program,                    # {2} Program opcode
            _zone_program_object.program.ad     # {3} Program address associated with this zone program
        )

        if _status_code is Message.exceeds_design_flow and _command_type is opcodes.set_action:
            msg += ",{0}={1},{2}={3}".format(
                Message.variable_one,           # {0} Variable one Message opcode
                fm.vr,                          # {1} Flow rate in gallons per minute
                Message.variable_two,           # {2} Variable two Message opcode
                _zone_program_object.zone.df    # {3} Design flow in the zone associated with this zone program
            )
        # If the status code deals with flow learn, add the learn flow value to the message
        elif _status_code in [Message.flow_learn_errors, Message.flow_learn_ok] and _command_type is opcodes.set_action:
            # if the 3200 has a flow meter value of 0 or 0.0 the message it returns will always have 0 if learn errors
            # 0.0 if learn flow ok
            if Message.flow_learn_errors and fm.vr == 0 or fm.vr == 0.0:
                new_fm_vr = 0
            elif Message.flow_learn_ok and fm.vr == 0 or fm.vr == 0.0:
                new_fm_vr = 0.0
            else:
                new_fm_vr = fm.vr

            msg += ",{0}={1}".format(
                Message.variable_two,           # {0} Variable two Message opcode
                new_fm_vr                       # {1} Current flow as displayed by the flow meter
            )
        elif _status_code in [Message.high_flow_variance_shutdown, Message.high_flow_variance_detected,
                              Message.low_flow_variance_shutdown, Message.low_flow_variance_detected] \
                              and _command_type is opcodes.get_action:
            msg += ",{0}={1},{2}={3}".format(
                Message.variable_one,           # {0} Variable two Message opcode
                _zone_program_object.zone.df,   # {1} Design flow in the zone associated with this zone program
                Message.variable_two,           # {2} Variable two Message opcode
                fm.vr                           # {3} Current flow as displayed by the flow meter
            )
    # Creates the different value pairs to test against
    if _command_type is opcodes.get_action:
        # Example ID=1_PG_1_OK,DT=3/15/16 15:49:51,TX=Program 1-00S0001\\\nComm Error
        _id = build_the_message_id(_object=_zone_program_object,
                                   ct_type=MessageCategory.zone_program,
                                   _status_code=_status_code)

        _dt = date_mngr.controller_datetime.msg_date_string_for_controller_3200() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_3200()

        _tx = build_message_title(_object=_zone_program_object,
                                  _ct_type=MessageCategory.zone_program,
                                  _status_code=_status_code)
        return {opcodes.message: msg,       # this is the get command that is sent to the controller
                opcodes.message_id: _id,     # this is the message id that will be received from the controller
                opcodes.date_time: _dt,      # this is the message date/time that will be received from the controller
                opcodes.message_text: _tx}   # this is the detailed text that is received from the controller
    else:
        return msg


def create_zone_message(_zone_object, _status_code, _command_type):
    """
    # Example ID=1_ZN_1_OK,DT=3/15/16 15:49:51,TX=Zone 1-TSQ0031\\\nSolenoid Short
    :param _zone_object
    :type _zone_object: Zone
    :param _status_code
    :type _status_code: str
    :param _command_type
    :type _command_type: str
    """
    # If the controller type is 3200, then check if the message requires any different variables
    if _zone_object.controller_type is "32":
        # Creates the base for all zone object messages
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,          # {1} Command type passed in
            opcodes.message,        # {2} Message opcode
            opcodes.zone,           # {3} Zone opcode
            _zone_object.ad,        # {4} Zone address
            opcodes.status_code,    # {5} Status code opcode
            _status_code            # {6} Status code passed in
        )
        # If the status code is "bad serial" and the command is "SET", add a serial
        if _status_code is Message.bad_serial and _command_type is opcodes.set_action:
            msg += ",{0}={1}".format(
                Message.serial_number,      # {0} Serial number Message opcode
                _zone_object.sn             # {1} Zone object serial number
            )
        # If the status code is "open circuit" or "short circuit", include the Zone's serial in the message
        elif _status_code in [Message.open_circuit, Message.short_circuit]:
            msg += ",{0}={1}".format(
                Message.serial_number,      # {0} Serial number Message opcode
                _zone_object.sn             # {1} Zone object serial number
            )

    elif _zone_object.controller_type is opcodes.substation:
        # Creates the base for a zone object message on a substation
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,          # {1} Command type passed in
            opcodes.message,        # {2} Message opcode
            opcodes.zone,           # {3} Zone opcode
            _zone_object.sn,        # {4} Zone serial number
            opcodes.status_code,    # {5} Status code opcode
            _status_code            # {6} Status code passed in
        )

    else:
        # Creates the base for a zone object message on a 1000 controller
        msg = "{0},{1},{2}={3},{4}={5}".format(
            _command_type,          # {1} Command type passed in
            opcodes.message,        # {2} Message opcode
            opcodes.zone,           # {3} Zone opcode
            _zone_object.ad,        # {4} Zone address
            opcodes.status_code,    # {5} Status code opcode
            _status_code            # {6} Status code passed in
        )

    # Creates the different value pairs to test against
    if _command_type is opcodes.get_action:
        _id = ""
        # At the present time we do not want to check the IDs that the substation returns us, so we do not build an id
        # string to verify against when we are dealing with a biCoder on the substation
        if _zone_object.controller_type is not opcodes.substation:
            # Example ID=1_ZN_1_OK,DT=3/15/16 15:49:51,TX=Zone 1-TSQ0031\\\nSolenoid Short
            _id = build_the_message_id(_object=_zone_object,
                                       ct_type=MessageCategory.zone,
                                       _status_code=_status_code)

        if _zone_object.controller_type is "32":
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_3200() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_3200()
        else:
            _dt = date_mngr.controller_datetime.msg_date_string_for_controller_1000() + ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_1000()

        _tx = build_message_title(_object=_zone_object,
                                  _ct_type=MessageCategory.zone,
                                  _status_code=_status_code)
        return {opcodes.message: msg,        # this is the get command that is sent to the controller
                opcodes.message_id: _id,     # this is the message id that will be received from the controller
                opcodes.date_time: _dt,      # this is the message date/time that will be received from the controller
                opcodes.message_text: _tx}   # this is the detailed text that is received from the controller
    else:
        return msg


def build_the_message_id(_object, ct_type, _status_code, _helper_object=None):
    """
        this return the fist half of the Id string
    :param _object:
    :type _object:
    :param ct_type:
    :type ct_type:
    :param _status_code:
    :type _status_code:
    :return:
    :rtype:
    """

    # If the controller type is 3200, use the 3200 ID formats
    if _object.controller_type is "32":
        if ct_type is MessageCategory.basemanager:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str('701'),             # {0}
                opcodes.basemanager,    # {1}
                _object.sn,             # {2}
                _status_code            # {3}
            )

        elif ct_type is MessageCategory.controller:
            sn = ''
            # If the status code dealt with a moisture sensor, temperature sensor, or event switch, add the devices
            # serial to the ID, otherwise use the controller's serial number
            if _helper_object is not None and type(_helper_object) is not str:
                sn = _helper_object._device_serial
            else:
                sn = _object.sn
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str('000'),             # {0}
                opcodes.device_value,   # {1}
                sn,                     # {2} device sn for stop and pause
                _status_code            # {3}
            )

        elif ct_type is MessageCategory.event_switch:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(230).zfill(3),              # {0} event switch ID number
                opcodes.event_switch,           # {1}
                _object.sn,                     # {2} this is the serial number to the event switch
                _status_code                    # {3}
            )

        elif ct_type is MessageCategory.flow_meter:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(220).zfill(3),          # {0} flow meter ID number
                opcodes.flow_meter,         # {1}
                _object.sn,                 # {2} this is the serial number to the flow meter
                _status_code                # {3}
            )

        elif ct_type is MessageCategory.flow_station:
            port = str(_object.ser.s_port)[9:21]  # this gives us only the ip address of our 3200
            id_msg = '{0}_{1}_{2}_{3}'.format(
                "801",                      # {0} flow station ID number
                opcodes.flow_station,       # {1}
                port,                       # {2} the ip address of the 3200
                _status_code                # {3}
            )
        elif ct_type is MessageCategory.moisture_sensor:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(210).zfill(3),          # {0} moisture sensor ID number
                opcodes.moisture_sensor,    # {1}
                _object.sn,                 # {2} moisture serial number
                _status_code                # {3}
            )

        elif ct_type is MessageCategory.master_valve:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(_object.ad + 200).zfill(3),     # {0} master valve ID number
                opcodes.master_valve,               # {1}
                str(_object.ad),                    # {2} this is the zone address number
                _status_code                        # {3}
            )

        elif ct_type is MessageCategory.temperature_sensor:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(240).zfill(3),              # {0}this is the temperature sensor ID number
                opcodes.temperature_sensor,     # {1}
                _object.sn,                     # {2} this is the temperature sensor serial number
                _status_code                    # {3}
            )

        elif ct_type is MessageCategory.mainline:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(int(500 + _object.ad)).zfill(3),    # {0} this is the mainline ID number
                opcodes.mainline,                       # {1}
                _object.ad,                             # {2} this is the mainline address
                _status_code                            # {3}
            )

        elif ct_type is MessageCategory.point_of_connection:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(int(400 + _object.ad)).zfill(3),    # {0} this is the poc ID number
                opcodes.point_of_connection,            # {1}
                _object.ad,                             # {2} this is the poc address
                _status_code                            # {3}
            )

        elif ct_type is MessageCategory.program:
            # if the message doesnt have a device the controller returns a null in its place
            sn = 'Null'
            # If the status code dealt with a moisture sensor, temperature sensor, or event switch, add the devices
            # serial to the ID
            if _helper_object is not None:
                sn = _helper_object._device_serial

            id_msg = '{0}_{1}_{2}_{3}_{4}_{5}'.format(
                str(int(300 + _object.ad)).zfill(3),    # {0} this is the program ID number
                opcodes.program,                        # {1}
                _object.ad,                             # {2} this is the program address
                opcodes.device_value,                   # {3}
                str(sn),                                # {4} this is the serial number of the device  start, stop, pause. Empty string otherwise.
                _status_code                            # {5}
            )

        elif ct_type is MessageCategory.pump_station:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str('601'),                     # {0}
                opcodes.master_pump_station,    # {1}
                _object.ad,                     # {2} this is the program number
                _status_code                    # {3}
            )

        elif ct_type is MessageCategory.zone:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(_object.ad).zfill(3),   # {0} Zone ID number
                opcodes.zone,               # {1}
                _object.ad,                 # {2} the zone number address
                _status_code                # {3}
            )

        elif ct_type is MessageCategory.zone_program:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(_object.zone.ad).zfill(3),  # {0} Zone program ID number
                opcodes.zone,                   # {1}
                _object.zone.ad,                # {2} this is the zone number address
                _status_code,                   # {3}
            )
        else:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str('999'),             # {0}
                opcodes.device_value,   # {1}
                _object.sn,             # {2} this is the serial number of the device causing the alarm
                _status_code            # {3}
            )
    else:
        if ct_type is MessageCategory.controller:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str('0'),       # {0}
                "SY",           # {1}
                "SY",           # {2}
                _status_code    # {3}
            )

        elif ct_type is MessageCategory.zone:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                _object.ad,         # {0} this is the zone number address
                opcodes.zone,       # {1}
                _object.ad,         # {2} this is the zone number address
                _status_code        # {3}
            )

        elif ct_type is MessageCategory.master_valve:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(_object.ad + 150),  # {0} this is the master valve number address
                opcodes.master_valve,   # {1}
                _object.ad,             # {2} this is the master valve number address
                _status_code            # {3}
            )

        elif ct_type is MessageCategory.moisture_sensor:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(_object.ad + 158),      # {0} this is the moisture sensor number address
                opcodes.moisture_sensor,    # {1}
                _object.sn,                 # {2} this is the moisture sensor serial number
                _status_code                # {3}
            )

        elif ct_type is MessageCategory.flow_meter:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(_object.ad + 198),  # {0} this is the flow meter number address
                opcodes.flow_meter,     # {1}
                _object.sn,             # {2} this is the flow meter serial number
                _status_code            # {3}
            )

        elif ct_type is MessageCategory.event_switch:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(_object.ad + 206),  # {0} this is the event switch number address
                opcodes.event_switch,   # {1}
                _object.sn,             # {2} this is the event switch serial number
                _status_code            # {3}
            )

        elif ct_type is MessageCategory.temperature_sensor:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(_object.ad + 216),          # {0} this is the temperature sensor number address
                opcodes.temperature_sensor,     # {1}
                _object.sn,                     # {2} this is the temperature sensor serial number
                _status_code                    # {3}
            )

        elif ct_type is MessageCategory.program:
            sn = str(_object.ad)
            if _helper_object is not None and type(_helper_object) is not str :
                sn = _helper_object._device_serial

            # TODO make sure this is confirmed behavior, and if not make sure to change this part after an update
            if _status_code not in [opcodes.start, opcodes.pause, opcodes.stop]:
                id_msg = '{0}_{1}_{2}_{3}'.format(
                    str(_object.ad + 299),  # {0} this is the program number address
                    opcodes.program,        # {1}
                    sn,                     # {2} the serial number of the object attached to this program. Zeros by default
                    _status_code            # {3}
                )
            else:
                id_msg = '{0}_{1}_{2}_{3}'.format(
                    str(_object.ad + 299),  # {0} this is the program number address
                    opcodes.program,  # {1}
                    sn,  # {2} the serial number of the object attached to this program. Zeros by default
                    "OK"  # {3}
                )

        elif ct_type is MessageCategory.point_of_connection:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str(_object.ad + 399),          # {0} this is the zone number address
                opcodes.point_of_connection,    # {1}
                _object.ad,                     # {2} this is the zone number address
                _status_code                    # {3}
            )

        else:
            id_msg = '{0}_{1}_{2}_{3}'.format(
                str('999'),             # {0}
                opcodes.device_value,   # {1}
                _object.sn,             # {2} this is the serial number of the device causing the alarm
                _status_code            # {3}
            )

    return id_msg


def build_message_title(_object, _ct_type, _status_code, _helper_object=None, _use_to_verify=False):
    """
    this builds the title to the long message text
    Message Text
    The Message has the following format:
        '   ------------------------------
        '   Title line 1
        '   Date/Time, Priority
        '   ------------------------------
        '   Description line 1
        '   Description line 2
        '   Description line 3
        '   Description line 4
        '   Description line 5
    :param _object:
    :type _object:
    :param _ct_type: The category of what we are building. (ex: 'ZN', 'PG', etc.)
    :type _ct_type: str
    :param _status_code:
    :type _status_code: str
    :return:
    :rtype:
    """
    #TODO add try catch to check _ct_type
    # Select if .Category
    tx_msg = ''
    # If the object passed in part of a 1000 controller, create 3200 specific text messages
    if _object.controller_type is "32":
        if _ct_type is MessageCategory.basemanager:
            tx_msg = "BaseManager:  Message"

            if _status_code is Message.weather_eto_data_not_available:
                # TODO Find a way to get the 'previous ETo' value
                previous_date = (date_mngr.controller_datetime.obj - datetime.timedelta(days=1)).strftime("%m/%d/%Y")
                tx_msg = "{0}{1}{2}{3}".format(
                    "Weather ETo Data Not Available:",
                    "Updated ETo data not received from BaseManager.",
                    "Used Previous Date = " + previous_date,
                    "Used Previous ETo = " + "0"
                )

            if _status_code is Message.message:
                tx_msg = "BaseManger Text Message"

        # Create the title and header for the controller
        elif _ct_type is MessageCategory.controller:
            tx_msg = "Controller: {0}n".format(
                _object.ds
            )
            if _status_code is Message.commander_paused:
                tx_msg += "{0}{1}{2}".format(
                    "BL-Commander: ",
                    "Run ZonenAll watering has been pausedn",
                    "for manual operations."
                )
            elif _status_code is Message.boot_up:
                tx_msg += "{0}{1}{2}".format(
                    "Controller has been reset:n",
                    "Either from a power failure orn",
                    "a controller reset operation."
                )
            elif _status_code is Message.event_date_stop:
                tx_msg += "{0}{1}{2}".format(
                    "Event Date Blocked:n",
                    "All watering has been stoppedn",
                    "for this date."
                )
            elif _status_code is Message.usb_flash_storage_failure:
                tx_msg += "{0}{1}".format(
                    "USB Error:n",
                    "Unable to access the USB flash drive."
                )
            elif _status_code is Message.flow_jumper_stopped:
                tx_msg += "{0}{1}{2}".format(
                    "Flow Contacts Triggered:n",  # Spec says "Blocked" instead of "Triggered"
                    "All watering has been stopped.n",
                    "Value = Open"
                )

            elif _status_code is Message.restore_failed:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Restore Programming Failed:n",
                    "The backup file is missingn",
                    "or is corrupt.n",
                    "File from = {0}".format(_helper_object)  # Helper object should be an opcode of basemanager or USB
                )

            elif _status_code is Message.restore_successful:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Restore Programming Successful:n",
                    "The backup file has beenn",
                    "successfully restored.n",
                    "File from = {0}".format(_helper_object)  # Helper object should be an opcode of basemanager or USB
                )

            elif _status_code is Message.two_wire_high_current_shutdown:
                if _object.r_va is None:
                    tx_msg += "{0}{1}{2}{3}{4}".format(
                        "Two-wire Failure:n",
                        "High current indicates an",
                        "short circuit.n",
                        "Current = ",
                        "None"
                    )
                else:
                    tx_msg += "{0}{1}{2}{3}{4}".format(
                        "Two-wire Failure:n",
                        "High current indicates an",
                        "short circuit.n",
                        "Current = ",
                        _object.r_va
                    )

            elif _status_code is Message.pause_event_switch:
                sw_object = None
                # Loops through the event switch objects to find one with a matching serial number
                for index in _object.controller.event_switches.keys():
                    if _object.controller.event_switches[index].sn == _helper_object._device_serial:
                        sw_object = _object.controller.event_switches[index]
                        break

                if sw_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "EventSwitch serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Event biCoder Triggered:n",
                    "All watering has been paused.n",
                    "Event biCoder = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._mode) + " / ",
                    str(sw_object.vc)
                )

            elif _status_code is Message.pause_jumper:
                tx_msg += "{0}{1}{2}".format(
                    "Pause Contacts Triggered:n",
                    "All watering has been pausedn",
                    "for four hours minimum."
                )

            elif _status_code is Message.pause_moisture_sensor:
                ms_object = None
                # Loops through the moisture sensor objects to find one with a matching serial number

                for index in _object.controller.moisture_sensors.keys():
                    if _object.controller.moisture_sensors[index].sn == _helper_object._device_serial:
                        ms_object = _object.controller.moisture_sensors[index]
                        break

                if ms_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "MoistureSensor serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Moisture Limit Reached:n",
                    "All watering has been paused.n",
                    "Moisture biSensor = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._threshold) + " / ",
                    str(ms_object.vp)
                )

            elif _status_code is Message.pause_temp_sensor:
                ts_object = None
                # Loops through the temperature sensor objects to find one with a matching serial number
                for index in _object.controller.temperature_sensors.keys():
                    if _object.controller.temperature_sensors[index].sn == _helper_object._device_serial:
                        ts_object = _object.controller.temperature_sensors[index]
                        break

                if ts_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "TemperatureSensor serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Temperature Limit Reached:n",
                    "All watering has been paused.n",
                    "Temperature biSensor = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._threshold) + " / ",
                    str(ts_object.vd)
                )

            elif _status_code is Message.rain_delay_stopped:
                # This merges the date and time of our current computer date object and adds the number of rain delay days
                delay_date = (datetime.datetime.combine(date_mngr.curr_computer_date.obj, date_mngr.curr_computer_date.time_obj.obj)
                              + datetime.timedelta(days=_object.rp)).date()

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Rain Days Active:n",
                    "All watering has been stopped.n",
                    "Value = ",
                    str(_object.rp) + " daysn",
                    "Until = ",
                    str(delay_date.strftime('%m/%d/%y')) + " ",
                    '12:00:00 AM'
                )

            elif _status_code is Message.rain_jumper_stopped:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Rain Stop Contacts Triggered:n",
                    "All watering has been stopped.n",
                    "Value = Openn",
                    "Until = Closed"
                )

            elif _status_code is Message.stop_event_switch:
                sw_object = None
                # Loops through the event switch objects to find one with a matching serial number
                for index in _object.controller.event_switches.keys():
                    if _object.controller.event_switches[index].sn == _helper_object._device_serial:
                        sw_object = _object.controller.event_switches[index]
                        break

                if sw_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "EventSwitch serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Event biCoder Triggered:n",
                    "All watering has been stopped.n",
                    "Event biCoder = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._mode) + " / ",
                    str(sw_object.vc)
                )

            elif _status_code is Message.stop_moisture_sensor:
                ms_object = None
                # Loops through the moisture sensor objects to find one with a matching serial number
                for index in _object.controller.moisture_sensors.keys():
                    if _object.controller.moisture_sensors[index].sn == _helper_object._device_serial:
                        ms_object = _object.controller.moisture_sensors[index]
                        break

                if ms_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "MoistureSensor serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Moisture Limit Reached:n",
                    "All watering has been stopped.n",
                    "Moisture biSensor = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._threshold) + " / ",
                    str(ms_object.vp)
                )

            elif _status_code is Message.stop_temp_sensor:
                ts_object = None
                # Loops through the temperature sensor objects to find one with a matching serial number
                for index in _object.controller.temperature_sensors.keys():
                    if _object.controller.temperature_sensors[index].sn == _helper_object._device_serial:
                        ts_object = _object.controller.temperature_sensors[index]
                        break

                if ts_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "TemperatureSensor serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Temperature Limit Reached:n",
                    "All watering has been stopped.n",
                    "Temperature biSensor = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._threshold) + " / ",
                    str(ts_object.vd)
                )

        # Create the title and header for the event switch(event biCoder)
        elif _ct_type is MessageCategory.event_switch:
            tx_msg = "Event biCoder: {0}n".format(
                _object.ds      # {0} Event switch description
            )
            if _status_code is Message.bad_serial:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Device Error:n",
                    "Wrong device at this address.n",
                    "Device = ",
                    _object.sn
                )
            elif _status_code is Message.no_response:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Two-wire Communication Failure:n",
                    "Unable to talk with this device.n",
                    "Device = ",
                    _object.sn
                )

        # Create the title and header for the flow meter(flow biCoder)
        elif _ct_type is MessageCategory.flow_meter:
            tx_msg = "Flow biCoder: {0}n".format(
                _object.ds      # {0} Flow meter description
            )
            if _status_code is Message.bad_serial:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Device Error:n",
                    "Wrong device at this address.n",
                    "Device = ",
                    _object.sn
                )
            elif _status_code is Message.no_response:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Two-wire Communication Failure:n",
                    "Unable to talk with this device.n",
                    "Device = ",
                    _object.sn
                )
            elif _status_code is Message.set_upper_limit_failed:
                tx_msg += "{0}{1}{2}".format(
                    "Flow biCoder Failure:n",
                    "Unable to set high flow shutdownn",
                    "limit in Flow biCoder."
                )

        elif _ct_type is MessageCategory.flow_station:
            port = str(_object.ser.s_port)[9:21]  # this gives us only the ip address of our 3200
            tx_msg = "FlowStation: {0}n".format(port)
            if _status_code is Message.fallback_mode_booster_pump:
                tx_msg += "{0}{1}{2}".format(
                    "FlowStation Connection Error:n",
                    "Fallback Mode Activen",
                    "Booster Pump MV-200 Activated"
                )
            elif _status_code is Message.error_operation_terminated:
                zone = 0
                program = 0
                if _helper_object:  # helper object should be a zone program object
                    zone = _helper_object.zone.ad
                    program = _helper_object.program.ad
                # TODO how are we getting all the addresses? Is flow station going to have variables? Design decisions
                tx_msg += "{0}{1}{2}".format(
                    "FlowStation Error:n",
                    "Operation Terminated.n",
                    "Program = {0}nZone = {1}".format(program, zone)
                )
            elif _status_code is Message.fallback_mode_program:
                program = 0
                if _helper_object:
                    program = _helper_object.ad  # helper object is accessing Program number
                tx_msg += "{0}{1}{2}".format(
                    "FlowStation Connection Error:n",
                    "Fallback Mode Activen",
                    "Program {0} Activated".format(program)
                )
            elif _status_code is Message.fallback_mode_poc:
                poc = 0
                if _helper_object:
                    poc = _helper_object.ad  # helper object is accessing a POC address
                tx_msg += "{0}{1}{2}".format(
                    "FlowStation Connection Error:n",
                    "Fallback Mode Activen",
                    "POC {0} Activated".format(poc)
                )

        # Create the title and header for the mainline
        elif _ct_type is MessageCategory.mainline:
            tx_msg = "Mainline: {0}n".format(
                _object.ad
            )

            poc = 0
            for index in _object.controller.points_of_control.keys():
                if _object.controller.points_of_control[index].ml == _object.ml:
                    poc = _object.controller.points_of_control[index]
                    break
            fm = False
            # Uses the poc object from the previous step to find a flow meter that has a matching address so that it's
            # serial number can be used in constructing the messages
            for index in _object.controller.flow_meters.keys():
                if _object.controller.flow_meters[index].ad == poc.fm:
                    fm = _object.controller.flow_meters[index]
                    break
            # Find the zone that is related to the mainline object by checking what zone program has a program with a
            # similar mainline address as the mainline object address
            zone = False
            for index in _object.controller.programs.zone_programs.keys():
                if _object.controller.programs.zone_programs[index].program.ml == poc.ml:
                    zone = _object.controller.programs.zone_programs[index].zone
                    break

            if _status_code is Message.learn_flow_fail_flow_biCoders_disabled:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Flow biCoder Disabledn",
                    "Unable to do a Learn Flow operation.n",
                    "Flow biCoder = ",
                    fm.sn
                )
            elif _status_code is Message.learn_flow_fail_flow_biCoders_error:
                tx_msg += "{0}{1}{2}{3}{4}".format(
                    "Flow biCoder Error:n",
                    "Unable to read Flow biCoder.n",
                    "Learn Flow operation terminated.n",
                    "Flow biCoder = ",
                    fm.sn
                )
            elif _status_code is Message.high_flow_variance_detected:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                    "High Flow Variance Detected:n",
                    "Measured flow is above limit.n",
                    "Mainline = ",
                    str(_object.ad) + "n",
                    "Zone = ",
                    str(zone.ad) + "n",
                    "Limit/Value = ",
                    str(zone.df),
                    " / ",
                    str(fm.vr)
                )
            elif _status_code is Message.low_flow_variance_detected:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                    "Low Flow Variance Detected:n",
                    "Measured flow is below limit.n",
                    "Mainline = ",
                    str(_object.ad) + "n",
                    "Zone = ",
                    str(zone.ad) + "n",
                    "Limit/Value = ",
                    str(zone.df),
                    " / ",
                    str(fm.vr)
                )

        # Create the title and header for a moisture sensor(biSensor)
        elif _ct_type is MessageCategory.moisture_sensor:
            tx_msg = "biSensor: {0}n".format(
                _object.ds      # {0} moisture sensor description
            )
            if _status_code is Message.bad_serial:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Device Error:n",
                    "Wrong device at this address.n",
                    "Device = ",
                    _object.sn
                )
            elif _status_code is Message.sensor_disabled:
                tx_msg += "{0}{1}{2}{3}{4}{5}".format(
                    "Moisture biSensor Disabled:n",
                    "Unable to communicate withn",
                    "moisture biSensor.n",
                    "Moisture biSensor = ",
                    str(_object.sn) + "n",
                    "Watering = Timed")
            elif _status_code is Message.no_response:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Two-wire Communication Failure:n",
                    "Unable to talk with this device.n",
                    "Device = ",
                    _object.sn
                )
            elif _status_code is Message.zero_reading:
                tx_msg += "{0}{1}{2}{3}{4}".format(
                    "Moisture biSensor Failure:n",
                    "Unable to get a valid moisturen",
                    "reading from biSensor.n",
                    "Moisture biSensor = ",
                    str(_object.sn) + "n"
                )

        elif _ct_type is MessageCategory.master_valve:
            tx_msg = "MV{0}: {1}n".format(
                _object.ad,     # {0} address of master valve
                _object.ds      # {1} serial number description
            )

            if _status_code is Message.bad_serial:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Device Error:n",
                    "Wrong device at this address.n",
                    "Device = ",
                    _object.sn
                )
            elif _status_code is Message.no_response:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Two-wire Communication Failure:n",
                    "Unable to talk with this device.n",
                    "Device = ",
                    _object.sn
                )

            elif _status_code is Message.open_circuit:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Zone Failure:n",
                    "Open Circuit Solenoid.n",
                    "Unable to operate this valve.n",
                    "Zone = ",
                    str(_object.ad + 200) + "n",
                    "biCoder = ",
                    str(_object.sn) + "n"
                )

            elif _status_code is Message.short_circuit:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Zone Failure:n",
                    "Short Circuit Solenoid.n",
                    "Unable to operate this valve.n",
                    "Zone = ",
                    str(_object.ad + 200) + "n",
                    "biCoder = ",
                    str(_object.sn) + "n"
                )

        elif _ct_type is MessageCategory.point_of_connection:
            tx_msg = "POC: {0}n".format(
                _object.ds
            )
            # Finds the flow meter that is referenced by the POC that was passed in, if it is not found, raise ValueError
            fm = False
            for index in _object.controller.flow_meters.keys():
                if _object.controller.flow_meters[index].ad == _object.fm:
                    fm = _object.controller.flow_meters[index]

            if _status_code is Message.budget_exceeded:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                    "Monthly Budget Exceeded:n",
                    "Watering will continue.n",
                    "POC = ",
                    str(_object.ad) + "n",
                    "Budget/Used = ",
                    _object.wb,
                    " / ",
                    fm.vg
                )

            elif _status_code is Message.budget_exceeded_shutdown:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                    "Monthly Budget Exceeded:n",
                    "Watering has been shut down.n",
                    "POC = ",
                    str(_object.ad) + "n",
                    "Budget/Used = ",
                    _object.wb,
                    " / ",
                    fm.vg,
                )

            elif _status_code is Message.high_flow_detected:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                    "POC High Flow Detected:n",
                    "Measured flow exceeds limit.n",
                    "POC = ",
                    str(_object.ad) + "n",
                    "Flow biCoder = ",
                    str(fm.sn) + "n",
                    "Limit/Value = ",
                    _object.hf,
                    " / ",
                    fm.vr,
                )

            elif _status_code is Message.high_flow_shutdown:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                    "POC High Flow Detected:n",
                    "POC water has been shut down.n",
                    "POC = ",
                    str(_object.ad) + "n",
                    "Flow biCoder = ",
                    str(fm.sn) + "n",
                    "Limit/Value = ",
                    _object.hf,
                    " / ",
                    fm.vr
                )

            elif _status_code is Message.empty_shutdown:
                # Finds the event switch associated with this POC object, any errors would be caught in previous method
                moisture_sensor = None
                event_switch = None

                for index in _object.controller.event_switches.keys():
                    if _object.controller.event_switches[index].ad == _object.sw:
                        event_switch = _object.controller.event_switches[index]

                for index in _object.controller.moisture_sensors.keys():
                    if _object.controller.moisture_sensors[index].ad == _object.ms:
                        moisture_sensor = _object.controller.moisture_sensors[index]
                # because this fails only if both a ms or a sw are not found we and we are just looking for an address
                # we used the _object_poc.sw for the error message

                switch_empty_condition = None
                event_switch_vc = None
                mapping = None
                if event_switch:
                    mapping = {
                        'CL': 'Closed',
                        'OP': 'Open'
                    }
                if event_switch and _use_to_verify:
                    switch_empty_condition = mapping[_object.se]
                    event_switch_vc = mapping[event_switch.vc]
                if event_switch:
                    tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                        "POC Empty Condition: n",
                        "The POC has been shut down.n",
                        "POC = ",
                        str(_object.ad) + "n",
                        "Device = ",
                        str(event_switch.bicoder.sn) + "n",
                        "Limit/Value = ",
                        switch_empty_condition,
                        " / ",
                        event_switch_vc
                    )
                if moisture_sensor:
                    tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                        "POC Empty Condition: n",
                        "The POC has been shut down.n",
                        "POC = ",
                        str(_object.ad) + "n",
                        "Device = ",
                        str(moisture_sensor.bicoder.sn) + "n",
                        "Limit/Value = ",
                        str(_object.me),
                        " / ",
                        str(moisture_sensor.bicoder.vp)
                    )

            elif _status_code is Message.unscheduled_flow_detected:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                    "Flow biCoder Unexpected Flow:n",
                    "Measured flow is above limit.n",
                    "POC = ",
                    str(_object.ad) + "n",
                    "Flow biCoder = ",
                    str(fm.sn) + "n",
                    "Limit/Value = ",
                    str(_object.uf),
                    " / ",
                    fm.vr
                )

            elif _status_code is Message.unscheduled_flow_shutdown:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}".format(
                    "Flow biCoder Unexpected Flow:n",
                    "Measured flow is above limit.n",
                    "Watering has been shut down.n",
                    "POC = ",
                    str(_object.ad) + "n",
                    "Flow biCoder = ",
                    str(fm.sn) + "n",
                    "Limit/Value = ",
                    str(_object.uf),
                    " / ",
                    str(fm.vr)
                )

        elif _ct_type is MessageCategory.program:
            tx_msg = "Program: {0}n".format(
                _object.ds
            )

            if _status_code is Message.event_date_stop:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Program Stopped:n",
                    "Event Day is active for today.n",
                    "Program = ",
                    str(_object.ad)
                )

            elif _status_code is Message.skipped_by_moisture_sensor:
                ms_object = None
                # Loops through the moisture sensor objects to find one with a matching serial number
                for index in _object.controller.moisture_sensors.keys():
                    if _object.controller.moisture_sensors[index].sn == _helper_object._device_serial:
                        ms_object = _object.controller.moisture_sensors[index]
                        break

                if ms_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "MoistureSensor serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Not Started:n",
                    "Moisture limit has not been reachedn",
                    "Program = ",
                    str(_object.ad) + "n",
                    "Moisture biSensor = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._threshold) + " / ",
                    str(ms_object.vp)
                )

            elif _status_code is Message.learn_flow_with_errors:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Program Learn Flow Done:n",
                    "Completed with Errors.n",
                    "Program = ",
                    str(_object.ad)
                )

            elif _status_code is Message.learn_flow_success:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Program Learn Flow Done:n",
                    "Completed successfully.n",
                    "Program = ",
                    str(_object.ad)
                )

            elif _status_code is Message.started_by_bad_moisture_sensor:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Program Started:n",
                    "The limit was reached, due to n",
                    "a failed moisture biSensor.n",
                    "Program = ",
                    str(_object.ad) + "n",
                    "Moisture biSensor = ",
                    str(_helper_object._device_serial) + "n"
                )

            elif _status_code is Message.over_run_start_event:
                tx_msg += "{0}{1}{2}{3}{4}".format(
                    "Program Overrun:n",
                    "Program was running when an",
                    "start time was hit.n",
                    "Program = ",
                    str(_object.ad) + "n"
                )

            elif _status_code is Message.pause_event_switch:
                sw_object = None
                # Loops through the event switch objects to find one with a matching serial number
                for index in _object.controller.event_switches.keys():
                    if _object.controller.event_switches[index].sn == _helper_object._device_serial:
                        sw_object = _object.controller.event_switches[index]
                        break

                if sw_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "EventSwitch serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Paused:n",
                    "Event biCoder has been triggered.n",
                    "Program = ",
                    str(_object.ad) + "n",
                    "Event biCoder = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._mode) + " / ",
                    str(sw_object.vc)
                )

            elif _status_code is Message.pause_moisture_sensor:
                ms_object = None
                # Loops through the moisture sensor objects to find one with a matching serial number
                for index in _object.controller.moisture_sensors.keys():
                    if _object.controller.moisture_sensors[index].sn == _helper_object._device_serial:
                        ms_object = _object.controller.moisture_sensors[index]
                        break

                if ms_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "MoistureSensor serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Paused:n",
                    "Moisture limit has been reached.n",
                    "Program = ",
                    str(_object.ad) + "n",
                    "Moisture biSensor = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._threshold) + " / ",
                    str(ms_object.vp)                           # moisture sensor moisture percent
                )

            elif _status_code is Message.priority_paused:
                tx_msg += "{0}{1}{2}{3}{4}".format(
                    "Program Paused:n",
                    "A higher priority program hasn",
                    "preempted this program.n",
                    "Program = ",
                    str(_object.ad)
                )

            elif _status_code is Message.pause_temp_sensor:
                ts_object = None
                # Loops through the temperature sensor objects to find one with a matching serial number
                for index in _object.controller.temperature_sensors.keys():
                    if _object.controller.temperature_sensors[index].sn == _helper_object._device_serial:
                        ts_object = _object.controller.temperature_sensors[index]
                        break

                if ts_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "TemperatureSensor serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Paused:n",
                    "Temperature limit has been reached.n",
                    "Program = ",
                    str(_object.ad) + "n",
                    "Temperature biSensor = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._threshold) + " / ",
                    str(ts_object.vd)
                )

            elif _status_code is Message.restricted_time_by_water_ration:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                    "Program Time Restricted:n",
                    "Water Rationing is Activen",
                    "Program water time has been reduced.n",
                    "Program = ",
                    str(_object.ad) + "n",
                    "Run Times = ",
                    str(_object.wr),
                    " %"
                )

            elif _status_code is Message.stop_event_switch:
                sw_object = None
                # Loops through the event switch objects to find one with a matching serial number
                for index in _object.controller.event_switches.keys():
                    if _object.controller.event_switches[index].sn == _helper_object._device_serial:
                        sw_object = _object.controller.event_switches[index]
                        break

                if sw_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "EventSwitch serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Stopped:n",
                    "Event biCoder has been triggeredn",
                    "Program = ",
                    str(_object.ad) + "n",
                    "Event biCoder = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._mode) + " / ",
                    str(sw_object.vc)
                )

            elif _status_code is Message.stop_moisture_sensor:
                ms_object = None
                # Loops through the moisture sensor objects to find one with a matching serial number
                for index in _object.controller.moisture_sensors.keys():
                    if _object.controller.moisture_sensors[index].sn == _helper_object._device_serial:
                        ms_object = _object.controller.moisture_sensors[index]
                        break

                if ms_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "MoistureSensor serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Stopped:n",
                    "Moisture limit has been reachedn",
                    "Program = ",
                    str(_object.ad) + "n",
                    "Moisture biSensor = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._threshold) + " / ",
                    str(ms_object.vp)
                )

            elif _status_code is Message.stop_temp_sensor:
                ts_object = None
                # Loops through the temperature sensor objects to find one with a matching serial number
                for index in _object.controller.temperature_sensors.keys():
                    if _object.controller.temperature_sensors[index].sn == _helper_object._device_serial:
                        ts_object = _object.controller.temperature_sensors[index]
                        break

                if ts_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "TemperatureSensor serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Stopped:n",
                    "Temperature limit has been reachedn",
                    "Program = ",
                    str(_object.ad) + "n",
                    "Temperature biSensor = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._threshold) + " / ",
                    str(ts_object.vd)
                )

            elif _status_code is Message.started_event_switch:
                sw_object = None
                # Loops through the event switch objects to find one with a matching serial number
                for index in _object.controller.event_switches.keys():
                    if _object.controller.event_switches[index].sn == _helper_object._device_serial:
                        sw_object = _object.controller.event_switches[index]
                        break

                if sw_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "EventSwitch serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Started:n",
                    "Event biCoder has been triggeredn",
                    "Program = ",
                    str(_object.ad) + "n",
                    "Event biCoder = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._mode) + " / ",
                    str(sw_object.vc)
                )

            elif _status_code is Message.started_moisture_sensor:
                ms_object = None
                # Loops through the moisture sensor objects to find one with a matching serial number
                for index in _object.controller.moisture_sensors.keys():
                    if _object.controller.moisture_sensors[index].sn == _helper_object._device_serial:
                        ms_object = _object.controller.moisture_sensors[index]
                        break

                if ms_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "MoistureSensor serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Started:n",
                    "Moisture limit has been reachedn",
                    "Program = ",
                    str(_object.ad) + "n",
                    "Moisture biSensor = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._threshold) + " / ",
                    str(ms_object.vp)
                )

            elif _status_code is Message.started_temp_sensor:
                ts_object = None
                # Loops through the temperature sensor objects to find one with a matching serial number
                for index in _object.controller.temperature_sensors.keys():
                    if _object.controller.temperature_sensors[index].sn == _helper_object._device_serial:
                        ts_object = _object.controller.temperature_sensors[index]
                        break

                if ts_object is None:
                    e_msg = "Please make sure the BaseStartStopPause object you passed in has been assigned a valid " \
                            "TemperatureSensor serial number. This can be done with the 'set' methods."
                    raise ValueError(e_msg)

                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Started:n",
                    "Temperature limit has been reachedn",
                    "Program = ",
                    str(_object.ad) + "n",
                    "Temperature biSensor = ",
                    str(_helper_object._device_serial) + "n",
                    "Limit/Value = ",
                    str(_helper_object._threshold) + " / ",
                    str(ts_object.vd)
                )

            elif _status_code is Message.water_window_paused:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Program Paused:n",
                    "A water window is closedn",
                    "Program = ",
                    str(_object.ad)
                )

        elif _ct_type is MessageCategory.temperature_sensor:
            tx_msg = "Temperature biSensor: {0}n".format(
                _object.ds
            )
            if _status_code is Message.bad_serial:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Device Error:n",
                    "Wrong device at this address.n",
                    "Device = ",
                    _object.sn
                )
            elif _status_code is Message.no_response:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Two-wire Communication Failure:n",
                    "Unable to talk with this device.n",
                    "Device = ",
                    _object.sn
                )

        elif _ct_type is MessageCategory.zone:
            tx_msg = "Zone {0}: {1}n".format(
                str(_object.ad),     # {0} Zone address
                str(_object.ds)      # {1} Zone description
            )

            if _status_code is Message.bad_serial:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Device Error:n",
                    "Wrong device at this address.n",
                    "Device = ",
                    str(_object.sn)
                )

            elif _status_code is Message.no_24_vac:
                tx_msg += "{0}{1}{2}{3}{4}".format(
                    "Powered Valve biCoder Failure:n",
                    "Missing 24 VAC.n",
                    "Unable to operate this valve.n",
                    "Zone = ",
                    str(_object.ad)
                )

            elif _status_code is Message.no_response:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Two-wire Communication Failure:n",
                    "Unable to talk with this device.n",
                    "Device = ",
                    str(_object.sn)
                )

            elif _status_code is Message.open_circuit:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    # this change from zone failure to valve failure in verizon 16.0.515
                    "Valve Failure:n",
                    "Open Circuit Solenoid.n",
                    "Unable to operate this valve.n",
                    "Zone = ",
                    str(_object.ad) + "n",
                    "biCoder = ",
                    str(_object.sn) + "n"
                )

            elif _status_code is Message.short_circuit:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    # this change from zone failure to valve failure in verizon 16.0.515
                    "Valve Failure:n",
                    "Short Circuit Solenoid.n",
                    "Unable to operate this valve.n",
                    "Zone = ",
                    str(_object.ad) + "n",
                    "biCoder = ",
                    str(_object.sn) + "n"
                )

        elif _ct_type is MessageCategory.zone_program:
            # Look for a moisture sensor that matches up with this zoneprogram
            ms = False
            for index in _object.controller.moisture_sensors.keys():
                if _object.controller.moisture_sensors[index].ad == _object.ms:
                    ms = _object.controller.moisture_sensors[index]
                    break
            # Try to find a point of connection in the object bucket that shares the a mainline address as the program
            # object that is associated with zone program
            poc = False
            for index in _object.controller.points_of_control.keys():
                if _object.controller.points_of_control[index].ml == _object.program.ml:
                    poc = _object.controller.points_of_control[index]
                    break
            fm = False
            for index in _object.controller.flow_meters.keys():
                if _object.controller.flow_meters[index].ad == poc.fm:
                    fm = _object.controller.flow_meters[index]
                    break
            if _status_code in []: # this list is no longer needed
                tx_msg = "Zone {0}: {1}n".format(
                    # in version 16.0.515 removed 3 of these checkers because the ms serial number wasn't used anymore
                    _object.zone.ad,    # {0} Zone address
                    ms.sn               # {1} Moisture sensor serial number
                )
            else:
                tx_msg = "Zone {0}: {1}n".format(
                    _object.zone.ad,    # {0} Zone address
                    _object.zone.ds     # {1} Zone description
                )

            if _status_code is Message.calibrate_failure_no_change:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                    "Moisture biSensor Calibration Failed:n",
                    "Moisture did not increase.n",
                    "Primary Zone = ",
                    str(_object.zone.ad) + "n",
                    "Moisture biSensor = ",
                    str(ms.sn) + "n",
                    "Moisture = ",
                    str(ms.vp)
                )
            elif _status_code is Message.calibrate_successful:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                    "Moisture biSensor Calibration Done:n",
                    "New watering limits set.n",
                    "Primary Zone = ",
                    str(_object.zone.ad) + "n",
                    "Moisture biSensor = ",
                    str(ms.sn) + "n",
                    "Limit Lower/Upper = ",
                    str(_object.ll) + "n",
                    " / ",
                    str(_object.ul)
                )
            elif _status_code is Message.calibrate_failure_no_saturation:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                    "Moisture biSensor Calibration Failed:n",
                    "Did not reach Saturation.n",
                    "Primary Zone = ",
                    str(_object.zone.ad) + "n",
                    "Moisture biSensor = ",
                    str(ms.sn) + "n",
                    "Moisture = ",
                    str(ms.vp) + "n"
                )

            elif _status_code is Message.exceeds_design_flow:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Zone High Flow:n",
                    "Zone design flow exceeds limit.n",
                    "It will be run by itself.n",
                    "Zone = ",
                    str(_object.zone.ad) + "n",
                    "Design Flow = ",
                    str(_object.zone.df)
                )

            elif _status_code is Message.flow_learn_errors:
                # if the 3200 has a flow meter value of 0 or 0.0 the message it returns will always have 0
                if fm.vr == 0 or fm.vr == 0.0:
                    new_fm_vr = 0
                else:
                    new_fm_vr = fm.vr

                tx_msg += "{0}{1}{2}{3}{4}{5}".format(
                    "Zone Learn Flow Done:n",
                    "This zone had errors.n",
                    "Zone = ",
                    str(_object.zone.ad) + "n",
                    "Learn Flow = ",
                    str(new_fm_vr)
                )

            elif _status_code is Message.flow_learn_ok:
                # if the 3200 has a flow meter value of 0 or 0.0 the message it returns will always have 0 if learn errors
                # 0.0 if learn flow ok
                if fm.vr == 0 or fm.vr == 0.0:
                    new_fm_vr = 0
                else:
                    new_fm_vr = fm.vr

                tx_msg += "{0}{1}{2}{3}{4}{5}".format(
                    "Zone Learn Flow Done:n",
                    "Completed successfully.n",
                    "Zone = ",
                    str(_object.zone.ad) + "n",
                    "Learn Flow = ",
                    str(new_fm_vr)
                )

            elif _status_code is Message.high_flow_shutdown_by_flow_station:
                tx_msg += "{0}{1}{2}{3}{4}".format(
                    "FlowStation High Flow:n",
                    "Measured flow exceeds limit.n",
                    "Zone has been shut down.n",
                    "Zone = ",
                    str(_object.zone.ad) + "n"
                )

            elif _status_code is Message.high_flow_variance_shutdown:
                # If there is a limit (design flow) in zone
                if _object.zone.df:
                    tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                        "Zone High Flow Variance:n",
                        "Measured flow exceeds limit.n",
                        "Zone has been shut down.n",
                        "Zone = ",
                        str(_object.zone.ad) + "n",
                        "Limit/Value = ",
                        str(_object.zone.df),
                        " / ",
                        str(fm.vr)
                    )
                else:
                    # If there is not a limit in zone, print out a different message
                    tx_msg += "{0}{1}{2}{3}{4}{5}".format(
                        "Zone High Flow Variance:n",
                        "Measured flow exceeds limit.n",
                        "Zone has been shut down.n",
                        "Zone = ",
                        str(_object.zone.ad) + "n",
                        "Limit = "
                    )

            elif _status_code is Message.low_flow_shutdown_by_flow_station:
                tx_msg += "{0}{1}{2}{3}{4}".format(
                    "FlowStation Low Flow:n",
                    "Measured flow is below lower limit.n",
                    "Zone has been shut down.n",
                    "Zone = ",
                    str(_object.zone.ad) + "n"
                )

            elif _status_code is Message.low_flow_variance_shutdown:
                # If there is a limit (design flow) in zone
                if _object.zone.df:
                    tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                        "Zone Low Flow Variance:n",
                        "Measured flow is below lower limit.n",
                        "Zone has been shut down.n",
                        "Zone = ",
                        str(_object.zone.ad) + "n",
                        "Limit/Value = ",
                        str(_object.zone.df),
                        " / ",
                        str(fm.vr)
                    )
                else:
                    # If there is not a limit (design flow) in zone, print out a different message
                    tx_msg += "{0}{1}{2}{3}{4}{5}".format(
                        "Zone Low Flow Variance:n",
                        "Measured flow is below lower limit.n",
                        "Zone has been shut down.n",
                        "Zone = ",
                        str(_object.zone.ad) + "n",
                        "Limit = "
                    )

            elif _status_code is Message.requires_soak_cycle:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                    "Moisture biSensor Requirement:n",
                    "Soak Cycles must be used.n",
                    "Primary Zone = ",
                    str(_object.zone.ad) + "n",
                    "Program = ",
                    str(_object.program.ad) + "n",
                    "Moisture biSensor = ",
                    str(ms.sn)
                    )

            elif _status_code is Message.high_flow_variance_detected:
                # If there is a limit (design flow) in zone
                if _object.zone.df:
                    tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                        "Zone High Flow Variance:n",
                        "Measured flow exceeds limit.n",
                        "Zone = ",
                        str(_object.zone.ad) + "n",
                        "Limit/Value = ",
                        str(_object.zone.df),
                        " / ",
                        str(fm.vr)
                    )
                else:
                    # If there is not a limit (design flow) in zone, print out a different message
                    tx_msg += "{0}{1}{2}{3}{4}".format(
                        "Zone High Flow Variance:n",
                        "Measured flow exceeds limit.n",
                        "Zone = ",
                        str(_object.zone.ad) + "n",
                        "Limit = "
                    )
            elif _status_code is Message.low_flow_variance_detected:
                # If there is a limit (design flow) in zone
                if _object.zone.df:
                    tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                        "Zone Low Flow Variance:n",
                        "Measured flow is below lower limit.n",
                        "Zone = ",
                        str(_object.zone.ad) + "n",
                        "Limit/Value = ",
                        str(_object.zone.df),
                        " / ",
                        str(fm.vr)
                    )
                else:
                    # If there is not a limit in zone, print out a different message
                    tx_msg += "{0}{1}{2}{3}{4}".format(
                        "Zone Low Flow Variance:n",
                        "Measured flow is below lower limit.n",
                        "Zone = ",
                        str(_object.zone.ad) + "n",
                        "Limit = "
                    )

    # If the controller is a substation
    elif _object.controller_type is opcodes.substation:
        if _ct_type is MessageCategory.controller:
            if _status_code is Message.two_wire_over_current:
                tx_msg = "Two-Wire Over Current"

        elif _ct_type is MessageCategory.event_switch:
            tx_msg = "Event Switch {0}n".format(
                _object.sn      # {0} Event switch serial
            )
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"

        elif _ct_type is MessageCategory.flow_meter:
            tx_msg = "Flow Sensor {0}n".format(
                _object.sn      # {0} Flow meter serial
            )
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"

        elif _ct_type is MessageCategory.moisture_sensor:
            tx_msg = "Moisture Sensor {0}n".format(
                _object.sn      # {0} Moisture sensor serial
            )
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"

        elif _ct_type is MessageCategory.master_valve:
            tx_msg = "MV/Pump {0}n".format(
                _object.sn      # {0} Master valve serial
            )
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"
            elif _status_code is Message.low_voltage:
                tx_msg += "Voltage Too Low"
            elif _status_code is Message.open_circuit_1000:
                tx_msg += "Solenoid Open"
            elif _status_code is Message.short_circuit_1000:
                tx_msg += "Solenoid Short"

        elif _ct_type is MessageCategory.temperature_sensor:
            tx_msg = "Temp. Sensor {0}n".format(
                _object.sn      # {0} Temperature sensor serial
            )
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"

        elif _ct_type is MessageCategory.zone:
            tx_msg = "Zone {0}n".format(
                _object.sn      # {0} Zone serial
            )
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"
            elif _status_code is Message.low_voltage:
                tx_msg += "Voltage Too Low"
            elif _status_code is Message.open_circuit_1000:
                tx_msg += "Solenoid Open"
            elif _status_code is Message.short_circuit_1000:
                tx_msg += "Solenoid Short"

    # If the controller is a 1000
    else:
        if _ct_type is MessageCategory.controller:
            if _status_code is Message.two_wire_over_current:
                tx_msg = "Two-Wire Over Current/r/nOld ETo Used"

        elif _ct_type is MessageCategory.event_switch:
            tx_msg = "Event Switch {0}-{1}n".format(
                _object.ad,     # {0} Event switch address
                _object.sn      # {1} Event switch serial
            )
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"

        elif _ct_type is MessageCategory.flow_meter:
            tx_msg = "Flow Sensor {0}-{1}n".format(
                _object.ad,     # {0} Flow meter address
                _object.sn      # {1} Flow meter serial
            )
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"

        elif _ct_type is MessageCategory.moisture_sensor:
            tx_msg = "Moisture Sensor {0}-{1}n".format(
                _object.ad,     # {0} Moisture sensor address
                _object.sn      # {1} Moisture sensor serial
            )
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"

        elif _ct_type is MessageCategory.master_valve:
            tx_msg = "MV/Pump {0}-{1}n".format(
                _object.ad,     # {0} Master valve address
                _object.sn      # {1} Master valve serial
            )
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"
            elif _status_code is Message.low_voltage:
                tx_msg += "Voltage Too Low"
            elif _status_code is Message.open_circuit_1000:
                tx_msg += "Solenoid Open"
            elif _status_code is Message.short_circuit_1000:
                tx_msg += "Solenoid Short"

        elif _ct_type is MessageCategory.point_of_connection:
            tx_msg = "Water Source WS-{0}n".format(
                _object.ad,     # {0} Point of connection address
            )
            if _status_code is Message.device_error:
                tx_msg += "Device Error"
            elif _status_code is Message.high_flow_shutdown:
                tx_msg += "High Flow Fault"
            elif _status_code is Message.unscheduled_flow_shutdown:
                tx_msg += "Unexpected Flow Fault"

        elif _ct_type is MessageCategory.program:
            # Checks to see if the date has a leading zero, if it does, cut it out. Also cut out 'M' from 'PM' or 'AM'
            if date_mngr.controller_datetime.time_obj.obj.strftime('%I:%M%p')[0] == '0':
                date = "{0}, {1}".format(

                    date_mngr.controller_datetime.formatted_date_string('%b %d'),            # Date in a specified format
                    date_mngr.controller_datetime.formatted_time_string('%I:%M%p')[1:-1]     # Time in specified format
                )
            else:
                date = "{0}, {1}".format(
                    date_mngr.controller_datetime.formatted_date_string('%b %d'),        # Date in a specified format
                    date_mngr.controller_datetime.formatted_time_string('%I:%M%p')[:-1]  # Time cuts out the 'M' from 'PM'
                )

            # This checks to see if the date has a leading zero after the month. ex: Jun 09 becomes June 9
            if date[4] == '0':
                date = date[0:4] + date[5:]

            tx_msg = "Program {0}n".format(
                _object.ad,     # {0} Program address
            )
            if _status_code is Message.over_run_start_event:
                tx_msg += "Status: Program Overrun {0}".format(
                    date
                )
            elif _status_code is Message.learn_flow_with_errors:
                tx_msg += "Status: Learn Flow Failed {0}".format(
                    date
                )
            elif _status_code is Message.calibrate_failure_no_change:
                tx_msg += "Status: Calibration Failed {0}".format(
                    date
                )
            elif _status_code is Message.start:
                tx_msg += "Status: Not Tested Yet {0}nStart: {1} {2}".format(
                    date,
                    program_1000_who_dict[_status_code][_helper_object],
                    date
                )
            elif _status_code is Message.pause:
                tx_msg += "Status: Not Tested Yet {0}nPause: {1} {2}".format(
                    date,
                    program_1000_who_dict[_status_code][_helper_object],
                    date
                )
            elif _status_code is Message.stop:
                tx_msg += "Status: Not Tested Yet {0}nStop: {1} {2}".format(
                    date,
                    program_1000_who_dict[_status_code][_helper_object],
                    date
                )

        elif _ct_type is MessageCategory.temperature_sensor:
            tx_msg = "Temp. Sensor {0}-{1}n".format(
                _object.ad,     # {0} Temperature sensor address
                _object.sn      # {1} Temperature sensor serial
            )
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"

        elif _ct_type is MessageCategory.zone:
            tx_msg = "Zone {0}-{1}n".format(
                _object.ad,     # {0} Zone address
                _object.sn      # {1} Zone serial
            )
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"
            elif _status_code is Message.low_voltage:
                tx_msg += "Voltage Too Low"
            elif _status_code is Message.open_circuit_1000:
                tx_msg += "Solenoid Open"
            elif _status_code is Message.short_circuit_1000:
                tx_msg += "Solenoid Short"
            elif _status_code is Message.high_flow_variance_shutdown:
                tx_msg += "High Flow Variance"

    return tx_msg
