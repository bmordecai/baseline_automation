# Our modules
import status_parser
from common.variables import common as common_vars
from common.imports import opcodes
from common.imports.types import BiCoderCommands, DeviceCommands,  ActionCommands
from decimal import Decimal


from common.objects.base_classes import messages
# Python libraries
from datetime import datetime, timedelta

__author__ = 'Tige'


class SimulatedbiCoderBaseClass(object):

    """
    Parent Class for other BiCoder object's to inherit from. \n
    
    :type tw_simulator: common.objects.controllers.tw_sim.TWSimulator

    """
    dv_assign_op_code_dict = DeviceCommands.Dictionaries.DEVICE_ASSIGNMENTS
    bicoder_types = BiCoderCommands.Type
    device_type = DeviceCommands.Type

    def __init__(self, _serial_number, _controller, _address=0, _two_wire_address=255):
        """
        The initializer for the base class of any bicoder. It holds the basic attributes that all bicoders share.

        :param _controller:     Controller this object is "attached" to \n
        :type _controller:      common.objects.controllers.tw_sim.TWSimulator

        :param _serial_number:  Serial number to set for BiCoder. \n
        :type _serial_number:   str \n

        :param _address:        The address associated with this bicoder on the controller. \n
        :type _address:         int \n
        """
        self.controller = _controller   # The controller this BiCoder is "attached" to
        self.ser = _controller.ser      # The serial port this object will talk through

        # We import here to avoid a circular import
        self.controller_type = opcodes.tw_simulator
        self.base_station = None

        self.sn = _serial_number                # Serial Number for BiCoder

        self.ad = _two_wire_address             # This is the address on the two_wire specific
        self.two_wire_address = self.ad         # This is the address on the two_wire specific

        self.nu = _address                      # This is the address or slot number on the controller
        self.watering_engine_address = self.nu  # This is the address or slot number on the controller

        self.ve = 6.0
        self.ss = None

        self.ty = None               # BiCoder "TYPE" from BiCoderTypes class.
        self.id = ''                # The identifier of the BiCoder

        self.decoder_type = ''      # Set by child classes

        self.ss = ''                # status of the biCoder

        # Attributes that we will send to the substation by default
        self.default_attributes = [
        ]

        # Place holder for data from controller when needed.
        self.data = status_parser.KeyValues(string_in=None)

        # Attribute so we can store the messages
        self.build_message_string = ''  # place to store the message that is compared to the substation message

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Methods that write or set the value of the simulated biCoders                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def write_default_values(self):
        """
        Set the default values of the device on the tw_simulator. \n
        :return:
        :rtype:
        """
        # Build the basic command that will be added on to
        command = "{0},{1}={2}".format(
            ActionCommands.SET,     # {0}
            self.id,                # {1} Identifier for the biCoder (ex: 'ZN', 'FM', etc.)
            str(self.sn),           # {2} Serial number for the biCoder
        )

        # Loops through the attribute list and adds the values to our string that we will send to the tw_simulator
        for attribute_tuple in self.default_attributes:
            # attribute_tuple[0] is the name of the attribute in the string we send to the tw_simulator
            attribute_name = attribute_tuple[0]

            # attribute_tuple[1] is the attribute for the device to update
            bicoder_attribute = getattr(self, attribute_tuple[1])

            # Add the attribute to the command string
            command += ",{0}={1}".format(
                attribute_name,     # {0}
                bicoder_attribute)  # {1}

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Default values' to: '{2}'".format(
                self.ty,        # {0}
                str(self.sn),   # {0}
                command         # {1}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Default values' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                command)        # {2}
            )

    #################################
    def write_serial_number_to_bicoder(self, _serial_num):
        """
        Write the serial number to the biCoder.\n
        This is used in the manufacturing process
        :param _serial_num:  serial number. \n
        :type _serial_num:   str \n

        :return:
        """
        # take the serial number that is passed in from teh use case and write it the the bicoder

        # first take the serial number a see if it is a string
        if not isinstance(_serial_num, str):
            e_msg = "Serial number must be in a string format '{0}'".format(
                str(_serial_num))
            raise TypeError(e_msg)
        if _serial_num is not None:
            self.sn = _serial_num
        # count the number of characters in the string
        if len(self.sn) != 7:
            raise ValueError("Controller serial number must be a seven digit string, instead was " +
                             str(self.sn))

        # Example command: 'DO,D1,SN=TSD0001'
        command = "{0},{1},{2}={3}".format(
            ActionCommands.DO,                                      # {0}
            BiCoderCommands.Dictionaries.BICODER_TYPE[self.id],     # {1}
            BiCoderCommands.Attributes.SERIAL_NUMBER,               # {2}
            str(self.sn))
        try:
            # Attempt to set two wire drop for zone at the tw_simulator
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to Write the serial number on controller command sent was: {0}".format(
                command  # {0}
            )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully Wrote serial number to {0}" . format(_serial_num)
            print (e_msg)

    #################################
    def write_two_wire_address(self, _two_wire_ad=None):
        """
        Address the specified device type to the controller. \n
        :param _two_wire_ad:     Address to overwrite current address.
        :type _two_wire_ad:      int \n
        :return:
        """

        if _two_wire_ad is not None:
            self.two_wire_address = _two_wire_ad

        # Example command: 'SET,ZN=TSD0001,AD=1'
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,  # {0}
            self.dv_assign_op_code_dict[self.device_type],  # {1}
            str(self.sn),  # {2}
            opcodes.address,  # {3}
            str(self.two_wire_address)  # {4}
        )
        try:
            print("Sending command: " + command)
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to address {0} {1} with serial: {2} to the controller".format(
                self.device_type,  # {0}
                str(self.two_wire_address),  # {1}
                self.sn  # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully addressed Device: {0}, Serial: {1}, Address {2}".format(
                self.device_type,  # {0}
                self.sn,  # {1}
                str(self.ad))  # {2}
            )

    #################################
    def write_version_number(self, _versions_number=None):
        """
        Address the specified device type to the controller. \n

        :return:
        """

        if _versions_number is not None:
            self.ad = _versions_number
            self.two_wire_address = _versions_number  # Update the bicoders "device address" to be this objects address

        # Example command: 'DO,AZ=TSD0001,NU=1'
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.DO,                          # {0}
            self.dv_assign_op_code_dict[self.device_type],  # {1}
            str(self.sn),                               # {2}
            ActionCommands.ASSIGN,                      # {3}
            str(self.ad)                                # {4}
        )
        try:
            print("Sending command: " + command)
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to address {0} {1} with serial: {2} to the controller".format(
                self.device_type,   # {0}
                str(self.ad),       # {1}
                self.sn             # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully addressed Device: {0}, Serial: {1}, Address {2}".format(
                self.device_type,   # {0}
                self.sn,            # {1}
                str(self.ad))       # {2}
            )

    #################################
    def write_enable_biCoder(self, _versions_number=None):
        """
        Address the specified device type to the controller. \n
        :param _ad:     Address to overwrite current address.
        :type _ad:      int \n
        :return:
        """

        if _versions_number is not None:
            self.ad = _versions_number
            self.bicoder.nu = _versions_number   # Update the bicoders "device address" to be this objects address

        # Example command: 'DO,AZ=TSD0001,NU=1'
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.DO,                              # {0}
            self.dv_assign_op_code_dict[self.device_type],  # {1}
            str(self.sn),                                   # {2}
            ActionCommands.ASSIGN,                          # {3}
            str(self.ad)                                    # {4}
        )
        try:
            print("Sending command: " + command)
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to address {0} {1} with serial: {2} to the controller".format(
                self.device_type,   # {0}
                str(self.ad),       # {1}
                self.sn             # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully addressed Device: {0}, Serial: {1}, Address {2}".format(
                self.device_type,   # {0}
                self.sn,            # {1}
                str(self.ad))       # {2}
            )

    #################################
    def write_disable_biCoder(self, _versions_number=None):
        """
        Address the specified device type to the controller. \n
        :param _ad:     Address to overwrite current address.
        :type _ad:      int \n
        :return:
        """

        if _ad is not None:
            self.ad = _ad
            self.bicoder.nu = _ad   # Update the bicoders "device address" to be this objects address

        # Example command: 'DO,AZ=TSD0001,NU=1'
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.DO,                              # {0}
            self.dv_assign_op_code_dict[self.device_type],  # {1}
            str(self.sn),                                   # {2}
            ActionCommands.ASSIGN,                          # {3}
            str(self.ad)                                    # {4}
        )
        try:
            print("Sending command: " + command)
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to address {0} {1} with serial: {2} to the controller".format(
                self.device_type,   # {0}
                str(self.ad),       # {1}
                self.sn             # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully addressed Device: {0}, Serial: {1}, Address {2}".format(
                self.device_type,   # {0}
                self.sn,            # {1}
                str(self.ad))       # {2}
            )

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_serial_number_of_bicoder(self, _data=None):
        """
        Verifies the Serial Number set on the BiCoder. \n

        :param _data:   Data Object that holds the tw_simulator's attributes. If it isn't passed in we call get_data \n
        :type _data:    status_parser.KeyValues

        :return: True if the value in our object matches the value on the tw_simulator
        """
        # Calls get data if a data object was not passed in.
        # if self.ty == opcodes.single_valve_decoder:
        #     check one serial number so on and so forth
        data = _data if _data else self.read_data()

        # Get the serial number from the data object
        sn_on_sb = data.get_value_string_by_key(BiCoderCommands.Attributes.SERIAL_NUMBER)

        # Compare Serial Numbers
        if self.sn != sn_on_sb:
            e_msg = "Unable to verify {0} ({1})'s 'Serial Number'. Received: {2}, Expected: {3}".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(sn_on_sb),  # {2}
                str(self.sn)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0}'s ({1}) Serial Number: '{2}' on tw_simulator".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(self.sn)    # {2}
            ))
            return True

    #################################
    def verify_serial_number(self, _data=None):
        """
        Verifies the Serial Number set on the BiCoder. \n

        :param _data: Data Object that holds the tw_simulator's attributes. If it isn't passed in we call get_data \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the tw_simulator
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.read_data()

        # Get the serial number from the data object
        sn_on_sb = data.get_value_string_by_key(BiCoderCommands.Attributes.SERIAL_NUMBER)

        # Compare Serial Numbers
        if self.sn != sn_on_sb:
            e_msg = "Unable to verify {0} ({1})'s 'Serial Number'. Received: {2}, Expected: {3}".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(sn_on_sb),  # {2}
                str(self.sn)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0}'s ({1}) Serial Number: '{2}' on tw_simulator".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(self.sn)    # {2}
            ))
            return True

    #################################
    def verify_status(self, _status, _data=None):
        """
        Verifies the status on the tw_simulator for the specified biCoder type. \n

        :param _status: Status that we expect to be on the tw_simulator. \n
        :type _status: str

        :param _data: Data Object that holds the tw_simulator's attributes. If it isn't passed in we call get_data \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the tw_simulator
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.read_data()

        if _status not in common_vars.dictionary_for_status_codes:
            e_msg = "Unable to verify {0} ({1})'s status. Received invalid expected status: ({2}),\n" \
                    "Expected one of the following: ({3})".format(
                        str(self.ty),                                   # {0}
                        str(self.sn),                                   # {1}
                        str(_status),                                   # {2}
                        common_vars.dictionary_for_status_codes.keys()  # {3}
                    )
            raise KeyError(e_msg)

        # Get the actual status from our data object
        dv_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

        # Since status is a 'get' only, we want to overwrite our object with the expected status passed in to retain
        # the state of our object.
        self.ss = _status

        # Compare status versus what is on the tw_simulator
        if self.ss != dv_ss_on_cn:
            e_msg = "Unable to verify {0} ({1})'s status. Received: {2}, Expected: {3}".format(
                self.ty,            # {0}
                self.sn,            # {1}
                str(dv_ss_on_cn),   # {2}
                str(self.ss)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} ({1})'s status: '{2}' on tw_simulator".format(
                self.ty,    # {0}
                self.sn,    # {1}
                self.ss     # {2}
            ))
            return True

    #################################
    def verify_device_address(self, _data=None):
        """
        Verifies the address of the biCoder. \n
        :param _data: Data Object that holds the tw_simulator's attributes. If it isn't passed in we call get_data \n
        :type _data: status_parser.KeyValues
        :return:
        """

        # Calls get data if a data object was not passed in.
        data = _data if _data else self.read_data()

        nu_on_cn = int(data.get_value_string_by_key(DeviceCommands.Attributes.DEVICE_ADDRESS))

        # Compare the device address in our object to the one on the controller
        if self.nu != nu_on_cn:
            e_msg = "Unable verify {0}: {1}'s 'device address'. Received: {2}, Expected: {3}".format(
                self.id,        # {0}
                self.sn,        # {1}
                str(nu_on_cn),  # {2}
                str(self.nu)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s device address: '{2}' on controller".format(
                self.id,        # {0}
                self.sn,        # {1}
                str(self.nu)    # {2}
            ))
            return True

    #################################
    def verify_bicoder_not_present(self):
        """
        Verifies that this bicoder is no longer on the controller. \n
        """
        #  is if bicoder is not present a false is returned
        if self.is_bicoder_present():
            e_msg = "A {0} biCoder with serial number {1} WAS on the controller, which was un-expected.".format(
                self.ty,    # {0}
                self.sn     # {1}
            )
            raise Exception (e_msg)
        else:
            print "A {0} biCoder with serial number {1} WAS not on the controller, which was expected.".format(
                self.ty,    # {0}
                self.sn     # {1}
            )
            return True

    #################################
    def verify_bicoder_present(self):
        """
        Verifies that this bicoder is on the controller. \n
        """
        #  is if bicoder is not present a false is returned
        if self.is_bicoder_present():
            print "A {0} biCoder with serial number {1} WAS on the controller, which was expected.".format(
                self.ty,    # {0}
                self.sn     # {1}
            )
            return True
        else:
            e_msg = "A {0} biCoder with serial number {1} WAS NOT on the controller, which was un-expected.".format(
                self.ty,    # {0}
                self.sn     # {1}
            )
            raise Exception (e_msg)
            
    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def is_bicoder_present(self):
        """
        Verifies that this bicoder is no longer on the controller. \n
        """
        try:
            # This get data should fail, as the bicoder should no longer be on the controller
            self.read_data()
        except ValueError:
            return False
        else:
            return True

    #################################
    def do_self_test(self):
        """
        Do a test on a simulated biCoder that was loaded on the TwoWire Simulator \n
        """
        # Check if we have been assigned to a BaseStation controller yet, if not we can't do a search
        if not self.base_station:
            e_msg = "Cannot do a self test on {0} ({1}), it needs to first be assigned on a Controller.".format(
                self.id,
                self.sn
            )
            raise AttributeError(e_msg)
        # Build the command string
        command = "{0},{1},{2}={3}".format(
            ActionCommands.DO,          # {0}
            ActionCommands.DEVICETEST,  # {1}
            self.id,                    # {2}
            str(self.sn))               # {3}

        try:
            self.base_station.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to do a self test {0} {1}: {2}".format(
                self.ty,    # {0}
                self.sn,    # {1}
                e.message   # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully tested: {0} {1}".format(
                self.ty,    # {0}
                self.sn     # {1}
            ))

    #################################
    def read_data(self, _for_tw_simulator=False):
        """
        Gets the data for the simulated biCoder and returns it as an object we can parse through. \n

        :return: status_parser.KeyValues
        """
        # Build the command to get data from the tw_simulator
        command = "{0},{1}={2}".format(
            ActionCommands.GET,     # {0}
            self.id,                # {1}
            self.sn                 # {2}
        )

        # Attempt to get data from tw_tw_simulator
        try:
            self.data = self.ser.get_and_wait_for_reply(tosend=command, called_by_get_data=True)
        except AssertionError as ae:
            e_msg = "Unable to rea data for {0} ({1}) using command: '{2}'. Exception raised: {3}".format(
                self.ty,            # {0}
                self.sn,            # {1}
                command,            # {2}
                str(ae.message)     # {3}
            )
            raise ValueError(e_msg)
        else:
            return self.data
