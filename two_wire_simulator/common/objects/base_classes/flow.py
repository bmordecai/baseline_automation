import status_parser
from common.imports import opcodes
from common.objects.base_classes.ser import Ser
from common.objects.base_classes.base_methods import BaseMethods
from common.imports.types import ActionCommands
from common.objects.base_classes import messages
from datetime import datetime, timedelta

__author__ = 'baseline'


########################################################################################################################
# Base Flow  Class object
########################################################################################################################
class BaseFlow(BaseMethods):
    """
    Base Flow Class.
    """

    # Controller type for controller specific methods across all devices
    controller_type = ''

    #################################
    def __init__(self, _controller, _ad, _identifiers, _ty):
        """
        The initializer for the base class of any flow objects. It holds the basic attributes that all flow objects
        share.

        :param _controller:     Controller this object is "attached" to \n
        :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                                common.objects.controllers.bl_10.BaseStation1000 \n

        :param _ad:             Flow Identifier (address) \n
        :type _ad:              int \n

        :param _identifiers:    Key value pairs that define this controller object. \n
        :type _identifiers:     list[list[str]] \n

        :param _ty:             Flow object type ('WS' | 'POC') \n
        :type _ty:              str \n
        """

        # Init parent class to inherit respective attributes
        BaseMethods.__init__(self, _ser=_controller.ser, _identifiers=_identifiers, _type=_ty)

        # Give access to the controller
        self.controller = _controller

        # Place holder for when a WS/PC/ML is shared to FS. Gives the WS/PC/ML a back-reference to the FS
        self.flowstation = None

        # Address of this flow object
        self.ad = _ad

        # Target Flow
        self.fl = 0
        # these are used when the object is being managed by the flowstation
        self.flow_station_slot_number = None  # int |1 - 20|
        self.sh = opcodes.false

        # This variable tracks the type of controller that called the get_mainline(), get_point_of_control(), and
        # get_water_source() methods in BaseController.
        self.called_by_controller_type = ''

        # Used in the program summary reporter to layout pipe connections
        self.downstream_pipes = []  # list of all downstream pipes(poc/ml/zn objs)

    #################################
    def get_down_stream_pipes(self):
        """
        This method will return the list of downstream pipes.
        :return:
        """
        return self.downstream_pipes

    #################################
    def get_pipe_identifier(self):
        """
        This is going to return a unique identifier for a WS/PC/ML
        The format is MAC-Type-Address for the pipe object
        :return:  String
        """

        return_string = "{0}-{1}-{2}".format(self.controller.mac,   # {0}
                                             self.ty,               # {1}
                                             self.ad)               # {2}
        return return_string

    #################################
    def is_managed_by_flowstation(self):
        """
        this will allow you to share a water source, point of control or a mainline, with other controller
        :return:
        :rtype:
        """
        if self.sh == opcodes.true:
            return True
        else:
            return False

    #################################
    def set_manage_by_flowstation(self):
        """
        this will allow you to share a water source, point of control or a mainline, with other controller
        :return:
        :rtype:
        """
        if not self.is_managed_by_flowstation():
            self.sh = opcodes.true

            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                     # {0}
                self.identifiers[0][0],                 # {1}
                self.identifiers[0][1],                 # {2}
                opcodes.share_with_flowstation,         # {3}
                str(self.sh)                            # {4}
            )
            try:
                # Attempt to send command to the controller
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception as e:
                e_msg = "Exception occurred trying to set {0} {1}'s 'Share With FlowStation' to: '{2}' -> {3}".format(
                    str(self.identifiers[0][0]),    # {0}
                    str(self.identifiers[0][1]),    # {1}
                    str(self.sh),                   # {2}
                    e.message                       # {3}
                )
                raise Exception(e_msg)
            else:
                print("Successfully set {0} {1}'s 'Shared' to: {2}".format(
                    str(self.identifiers[0][0]),        # {0}
                    str(self.identifiers[0][1]),        # {1}
                    str(self.sh)))                      # {2}

    #################################
    def set_do_not_manage_by_flowstation(self):
        """
        this will stop you to from sharing a water source, point of control or a mainline, with other controller
        :return:
        :rtype:
        """
        if self.is_managed_by_flowstation():
            self.sh = opcodes.false

            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                     # {0}
                self.identifiers[0][0],                 # {1}
                self.identifiers[0][1],                 # {2}
                opcodes.share_with_flowstation,         # {3}
                str(self.sh)                            # {4}
            )
            try:
                # Attempt to send command to the controller
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception as e:
                e_msg = "Exception occurred trying to set {0} {1}'s 'Share With FlowStation' to: '{2}' -> {3}".format(
                    str(self.identifiers[0][0]),    # {0}
                    str(self.identifiers[0][1]),    # {1}
                    str(self.sh),                   # {2}
                    e.message                       # {3}
                )
                raise Exception(e_msg)
            else:
                print("Successfully set {0} {1}'s 'Shared' to: {2}".format(
                    str(self.identifiers[0][0]),        # {0}
                    str(self.identifiers[0][1]),        # {1}
                    str(self.sh)))                      # {2}

    #################################
    def set_target_flow(self, _gpm=None):
        """
        Sets the 'Target Flow' for the Point of Connection on the controller. \n

        :param _gpm:    New 'Target Flow' for Point Of Connection \n
        :type _gpm:     int \n
        """

        if _gpm is not None:
            # Assign new 'Target Flow' to object attribute
            self.fl = _gpm

        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.identifiers[0][0],     # {1}
            self.identifiers[0][1],     # {2}
            opcodes.target_flow,        # {3}
            str(self.fl)                # {4}
        )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set {0} {1}'s 'Target Flow' to: '{2}' -> {3}".format(
                str(self.identifiers[0][0]),    # {0}
                str(self.identifiers[0][1]),    # {1}
                str(self.fl),                   # {2}
                e.message                       # {3}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} {1}'s 'Target Flow' to: {2}".format(
                str(self.identifiers[0][0]),        # {0}
                str(self.identifiers[0][1]),        # {1}
                str(self.fl)))                      # {2}

    #################################
    def verify_target_flow(self):
        """
        Verifies the ML/PC 'Target Flow' set on the controller. \n
        :return:
        """
        # expect int type
        target_flow_from_controller = float(self.data.get_value_string_by_key(opcodes.target_flow))

        # Compare enabled state
        if self.fl != target_flow_from_controller:
            e_msg = "Unable to verify {0} {1}'s 'Target Flow'. Received: {2}, Expected: {3}".format(
                str(self.identifiers[0][0]),        # {0}
                str(self.ad),                       # {1}
                str(target_flow_from_controller),   # {2}
                str(self.fl)                        # {3}
            )
            print(e_msg)
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s 'Target Flow': '{1}' on controller".format(
                str(self.identifiers[0][0]),    # {0}
                str(self.ad),                   # {1}
                str(self.fl)                    # {2}
            ))

    #################################
    def set_priority(self, _priority_for_water_source):
        """This method is being overridden in Mainline, Point of Control, and Water Source"""
        e_msg = "This method is being overridden in Mainline, Point of Control, and Water Source"
        raise(NotImplementedError(e_msg))




