import status_parser
from common import helper_methods
from common.variables import common as common_vars
from common.date_package.date_resource import date_mngr as resource_date_mngr
from common.imports import opcodes
from common.imports.types import DeviceCommands, ActionCommands
from common.objects.base_classes.base_methods import BaseMethods

__author__ = 'bens'


class BaseDevices(BaseMethods):
    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    # Serial Instance
    # ser = Ser.serial_conn
    # dictionary of device descriptions
    description_dict = DeviceCommands.Dictionaries.DEFAULT_DESCRIPTIONS
    # Dictionary for converting device types into address assignment op code

    dv_assign_op_code_dict = DeviceCommands.Dictionaries.DEVICE_ASSIGNMENTS
    # Browser Instance
    browser = ''

    # Controller type for controller specific methods across all devices
    controller_lat = 0.0    # this gets overwritten with configuration.py when we run our test scripts
    controller_long = 0.0   # this gets overwritten with configuration.py when we run our test scripts

    # TODO this needs a setter on it
    # Attributes that are not sent to all devices where there is no setters or verifiers for these attributes

    date_mngr = resource_date_mngr

    ra = 0  # Controller Rain Value

    #################################
    def __init__(self, _ser, _identifiers, _la, _lg, _sn=None, _ad='', _ty='', is_shared=False):
        """
        The initializer for the base class of any device. It holds the basic attributes that all devices share.

        :param _ser:        Serial port of the controller this object is "attached" to \n
        :type _ser:         common.objects.base_classes.ser.Ser \n

        :param _sn:         serial number \n
        :type _sn:          str \n

        :param _la:         Latitude \n
        :type _la:          float \n

        :param _lg:         Longitude \n
        :type _lg:          float \n

        :param _ad:         Address Number \n
        :type _ad:          int \n

        :param _ty:         Type \n
        :type _ty:          str \n

        :param is_shared:   Is the device shared with a Substation. \n
        :type is_shared:    bool \n

        :return:
        :rtype:
        """

        BaseMethods.__init__(self, _identifiers=_identifiers, _type=_ty, _ser=_ser)
        self.is_shared_with_substation = is_shared

        # Get serial instance for use
        # UPDATE: Commented out because this was overwriting Devices.ser which is set in configuration.py,
        # thus we do not need to assign self.ser to Ser.serial_conn here because it has already been established
        # self.ser = Ser.serial_conn
        # Messages.__init__(self, _mg=_mg)
        # Initialize device type
        self.dv_type = _ty

        # ----- Instance Variables ----- #

        self.ad = _ad       # Address Number
        self.vr = ''        # This is the firmware version
        # TODO this needs to be the serial number of the bicoder
        self.sn = _sn       # Serial Number
        self.la = _la       # Latitude
        self.lg = _lg       # Longitude

        # For devices with biCoders
        self.bicoder = None

        # Initialize place holder for data received from controller when needed.
        self.data = status_parser.KeyValues(string_in=None)
        # this is a place holder for a date range of the test
        self.calendar_range = None       # this is the calendar ra range of the test

        # For Manufacturing Remote Units
        self.r_va = None  # current
        self.r_vv = None  # voltage
        self.r_temp_sn = self.sn
        if self.sn is not None:
            self.final_sn = [self.sn, "VB" + self.sn[2:], "VE" + self.sn[2:]]
        self.r_decoder_type = ''

        # messages
        self.build_message_string = ''  # place to store the message that is compared to the controller message

    #################################
    def status_is_disabled(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        Returns whether or not the device has a status of watering.

        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        self.get_data_if_necessary(_get_data)
        return self.ss == opcodes.disabled

    #################################
    def status_is_watering(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        Returns whether or not the device has a status of watering.

        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        self.get_data_if_necessary(_get_data)
        return self.ss == opcodes.watering

    #################################
    def status_is_paused(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        Returns whether or not the device has a status of watering.

        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        self.get_data_if_necessary(_get_data)
        return self.ss == opcodes.paused

    #################################
    def status_is_soaking(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        Returns whether or not the device has a status of watering.

        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        self.get_data_if_necessary(_get_data)
        return self.ss == opcodes.soaking

    #################################
    def status_is_waiting_to_water(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        Returns whether or not the device has a status of waiting to water.

        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_waiting_to_water():
                /* DO SOMETHING */

        :rtype: bool
        """
        self.get_data_if_necessary(_get_data)
        return self.ss == opcodes.waiting_to_water

    #################################
    def status_is_done_watering(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        Returns whether or not the device has a status of done watering.

        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_done_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        self.get_data_if_necessary(_get_data)
        return self.ss in [opcodes.done_watering, opcodes.error]

    #################################
    def status_is_not_done_watering(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        Returns whether or not the device has a status of not done watering.

        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_not_done_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        return not self.status_is_done_watering(_get_data)

    #################################
    def status_is_error(self, _get_data=False):
        """
        Convenience method to return if the device is not in an error state.
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_error():
                /* DO SOMETHING */

        :rtype: bool
        """
        self.get_data_if_necessary(_get_data)
        return self.ss == opcodes.error

    #################################
    def status_is_not_error(self, _get_data=False):
        """
        Convenience method to return if the device is not in an error state.
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_not_error():
                /* DO SOMETHING */

        :rtype: bool
        """
        self.get_data_if_necessary(_get_data)
        # use "not" here to negate the equality.
        #   Thus, if:
        #           "status == opcodes.error" IS TRUE
        #   then,
        #           "not status == opcodes.error" IS FALSE
        return not self.ss == opcodes.error

    #################################
    def status_is_learning_flow(self, _get_data=False):
        """
        Convenience method to return if the device is not in an error state.
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_not_error():
                /* DO SOMETHING */

        :rtype: bool
        """
        if _get_data or not self.data:
            self.get_data()

        return self.ss == opcodes.learn_flow_active

    #################################
    def status_transitioned_from_watering_to_soaking(self, _get_data=False):
        """
        Convenience method to return if the device went from watering to soaking.
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_transitioned_from_watering_to_soaking():
                /* DO SOMETHING */

        :rtype: bool
        """
        # Get data if we haven't already
        self.get_data_if_necessary(_get_data)
        return self.last_ss == opcodes.watering and self.ss in [opcodes.soaking]

    #################################
    def status_transitioned_from_soaking_to_watering(self, _get_data=False):
        """
        Convenience method to return if the device went from soaking to watering.
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_transitioned_from_soaking_to_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        # Get data if we haven't already
        self.get_data_if_necessary(_get_data)
        return self.last_ss == opcodes.watering and self.ss == opcodes.soaking

    #################################
    def get_bicoder(self):
        """
        Gets the specific bicoder for this object and flip it's serial port

        :rtype  common.objects.bicoders.valve_bicoder.ValveBicoder |
                common.objects.simulated_bicoders.valve_bicoder.SimulatedValveBicoder |
                common.objects.bicoders.analog_bicoder.AnalogBicoder] |
                common.objects.simulated_bicoders.analog_bicoder.SimulatedAnalogBicoder |
                common.objects.bicoders.flow_bicoder.FlowBicoder] |
                common.objects.simulated_bicoders.flow_bicoder.SimulatedFlowBicoder |
                common.objects.bicoders.moisture_bicoder.MoistureBicoder] |
                common.objects.simulated_bicoders.moisture_bicoder.SimulatedMoistureBicoder |
                common.objects.bicoders.temp_bicoder.TempBicoder] |
                common.objects.simulated_bicoders.temp_bicoder.SimulatedTempBicoder |
                common.objects.bicoders.switch_bicoder.SwitchBicoder] |
                common.objects.simulated_bicoders.switch_bicoder.SimulatedSwitchBicoder
        """
        # Change the serial port to use the same on as this device
        self.bicoder.ser = self.ser

        return self.bicoder

    # TODO this needs work

    # #################################
    # def set_date_range_for_test(self,start_date, end_date):
    #     self.date_mngr.set_dates_from_date_range(start_date=start_date, end_date=end_date)
    #     #
    #     # start = datetime.datetime.strptime(start_date, "%d-%m-%Y")
    #     # end = datetime.datetime.strptime(end_date, "%d-%m-%Y")
    #     # date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end-start).days)]
    #     #
    #     # for date in date_generated:
    #     #     return date.strftime("%d-%m-%Y")

    #################################
    def set_address(self, _ad=None):
        """
        Address the specified device type to the controller. \n
        :param _ad:     Address to overwrite current address.
        :type _ad:      int \n
        :return:
        """

        if _ad is not None:
            self.ad = _ad
            self.bicoder.nu = _ad   # Update the bicoders "device address" to be this objects address

        # Example command: 'DO,AZ=TSD0001,NU=1'
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.DO,                          # {0}
            self.dv_assign_op_code_dict[self.dv_type],  # {1}
            str(self.sn),                               # {2}
            ActionCommands.ASSIGN,                      # {3}
            str(self.ad)                                # {4}
        )
        try:
            print("Sending command: " + command)
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to address {0} {1} with serial: {2} to the controller".format(
                self.dv_type,   # {0}
                str(self.ad),   # {1}
                self.sn         # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully addressed Device: {0}, Serial: {1}, Address {2}".format(self.dv_type, self.sn,
                                                                                        str(self.ad)))

    #################################
    def set_latitude(self, _la=None):
        """
        Sets the current latitude for the object on the controller. If a latitude is passed in as a parameter,
        it will overwrite the object's latitude as well as set the new latitude on the controller. \n
        :param _la:             Latitude to overwrite current object latitude
        :return:
        """
        # If a latitude is passed in
        try:
            if _la is not None:
                self.la = helper_methods.format_lat_long(_lat=_la)
            else:
                self.la = helper_methods.format_lat_long(_lat=self.la)
        except Exception as e:
            e_msg = "Exception occurred trying to set latitude for device: {0}\n".format(self.dv_type) + str(e.message)
            raise Exception(e_msg)

        if self.dv_type not in common_vars.list_of_device_types:
            e_msg = "Invalid device type: {0} entered while trying to send latitude to cn".format(str(self.dv_type))
            raise ValueError(e_msg)

        # If we are a zone, need to use address instead of serial number on set command
        if self.dv_type == DeviceCommands.Type.ZONE:
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                     # {0}
                self.dv_type,                           # {1}
                str(self.ad),                           # {2}
                DeviceCommands.Attributes.LATITUDE,     # {3}
                str(self.la))                           # {4}
        else:
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                     # {0}
                self.dv_type,                           # {1}
                str(self.sn),                           # {2}
                DeviceCommands.Attributes.LATITUDE,     # {3}
                str(self.la))                           # {4}

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0}'s latitude to: {1}".format(self.dv_type, self.la)
            raise Exception(e_msg)
        else:
            print("Successfully set latitude for device: {0}, to: {1}".format(self.dv_type, str(self.la)))

    #################################
    def set_longitude(self, _lg=None):
        """
        Sets the current longitude for the object on the controller. If a longitude is passed in as a parameter,
        it will overwrite the object's longitude as well as set the new longitude on the controller. \n
        :param _lg:          Longitude to overwrite current object longitude
        :return:
        """
        # If a description is passed in
        try:
            if _lg is not None:
                self.lg = helper_methods.format_lat_long(_lat=_lg)
            else:
                self.lg = helper_methods.format_lat_long(_lat=self.lg)
        except Exception as e:
            e_msg = "Exception occurred trying to set longitude for device: {0}\n".format(self.dv_type) + str(e.message)
            raise Exception(e_msg)

        if self.dv_type not in common_vars.list_of_device_types:
            e_msg = "Invalid device type: {0} entered while trying to send longitude to cn".format(str(self.dv_type))
            raise ValueError(e_msg)

        # If we are a zone, need to use address instead of serial number on set command
        if self.dv_type == DeviceCommands.Type.ZONE:
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                         # {0}
                self.dv_type,                               # {1}
                str(self.ad),                               # {2}
                DeviceCommands.Attributes.LONGITUDE,        # {3}
                str(self.lg))                               # {4}
        else:
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,                         # {0}
                self.dv_type,                               # {1}
                str(self.sn),                               # {2}
                DeviceCommands.Attributes.LONGITUDE,        # {3}
                str(self.lg))                               # {4}

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0}'s lg to: {1}".format(self.dv_type, str(self.lg))
            raise Exception(e_msg)
        else:
            print("Successfully set longitude for device: {0}, to: {1}".format(self.dv_type, str(self.lg)))

    # UPDATE 10/27/17 - Ben
    #   Commented this out and moved down into base methods so the same concept could be used for programs.
    #################################
    # def get_data_and_verify_status(self, status):
    #     """
    #     Get the dat from the controller and than
    #     Verifies the status on the controller for the specified device type. \n
    #     :return:
    #     """
    #     self.get_data()
    #     self.verify_status(_expected_status=status)

    #################################
    def verify_latitude(self):
        """
        Verifies the latitude set on the controller for the specified device type. \n
        :return:
        """
        la_on_cn = float(self.data.get_value_string_by_key(DeviceCommands.Attributes.LATITUDE))

        # If we are a zone, use zone address to identify it in the exception print string, otherwise use serial number
        # Here, 'arg1' represents a Zone address or any other device serial number
        if self.dv_type == DeviceCommands.Type.ZONE:
            arg1 = str(self.ad)
        else:
            arg1 = self.sn

        # Compare latitudes
        # if self.la != la_on_cn:
        if abs(self.la - la_on_cn) > 0.0000001:
            e_msg = "Unable verify {0}: {1}'s 'latitude'. Received: {2}, Expected: {3}".format(
                    self.dv_type,   # {0}
                    arg1,           # {1}
                    str(la_on_cn),  # {2}
                    str(self.la)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s latitude: '{2}' on controller".format(
                self.dv_type,   # {0}
                arg1,           # {1}
                str(self.la)    # {2}
            ))

    #################################
    def verify_longitude(self):
        """
        Verifies the longitude set on the controller for the specified device type. \n
        :return:
        """
        lg_on_cn = float(self.data.get_value_string_by_key(DeviceCommands.Attributes.LONGITUDE))

        # If we are a zone, use zone address to identify it in the exception print string, otherwise use serial number
        # Here, 'arg1' represents a Zone address or any other device serial number
        if self.dv_type == DeviceCommands.Type.ZONE:
            arg1 = str(self.ad)
        else:
            arg1 = self.sn
        # Compare longitudes
        # if self.lg != lg_on_cn:
        if abs(self.lg - lg_on_cn) > 0.0000001:
            e_msg = "Unable verify {0}: {1}'s 'longitude'. Received: {2}, Expected: {3}".format(
                    self.dv_type,   # {0}
                    arg1,           # {1}
                    str(lg_on_cn),  # {2}
                    str(self.lg)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s longitude: '{2}' on controller".format(
                  self.dv_type,     # {0}
                  arg1,             # {1}
                  str(self.lg)      # {2}
                  ))
            
    #################################
    def verify_serial_number(self, _get_data=False):
        """
        Verifies the Serial Number set on the controller for the specified device type. \n
        :return:
        """

        # Get data if we haven't already
        self.get_data_if_necessary(_get_data)

        sn_on_cn = self.data.get_value_string_by_key(DeviceCommands.Attributes.SERIAL_NUMBER)

        # Compare Serial Numbers
        if self.sn != sn_on_cn:
            e_msg = "Unable verify {0}: {1}'s 'Serial Number'. Received: {2}, Expected: {3}".format(
                    self.dv_type,   # {0}
                    str(self.ad),   # {1}
                    str(sn_on_cn),  # {2}
                    str(self.sn)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s Serial Number: '{2}' on controller".format(
                  self.dv_type,     # {0}
                  str(self.ad),     # {1}
                  str(self.sn)      # {2}
                  ))

    #################################
    def verify_device_address(self, _data=None):
        """
        Verifies the Serial Number set on the controller for the specified device type. \n
        :return:
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        ad_on_cn = int(data.get_value_string_by_key(DeviceCommands.Attributes.DEVICE_ADDRESS))

        # Compare Serial Numbers
        if self.ad != ad_on_cn:
            e_msg = "Unable verify {0}: {1}'s 'Address'. Received: {2}, Expected: {3}".format(
                self.dv_type,   # {0}
                str(self.sn),   # {1}
                str(ad_on_cn),  # {2}
                str(self.ad)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'is addressed to: '{2}' on controller".format(
                self.dv_type,  # {0}
                str(self.sn),  # {1}
                str(self.ad)  # {2}
            ))

    #################################
    def verify_status_is_disabled(self):
        if not self.status_is_disabled(_get_data=True):
            e_msg = "Device: {0}: assigned to address '{1}'  with serial number '{2}'  should be disabled, " \
                    "Status received was: {3}".format(
                        self.ds,        # {0}
                        str(self.ad),   # {1}
                        self.sn,        # {2}
                        self.ss,        # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Device: {0}: assigned to address '{1}'  with serial number '{2}'  is Disabled".format(
                self.ds,        # {0}
                str(self.ad),   # {1}
                self.sn,        # {2}
                ))

    #################################
    def verify_status_is_watering(self):
        if not self.status_is_watering(_get_data=True):
            e_msg = "Device: {0}: assigned to address '{1}'  with serial number '{2}'  should be Watering, " \
                    "Status received was: {3}".format(
                        self.ds,        # {0}
                        str(self.ad),   # {1}
                        self.sn,        # {2}
                        self.ss,        # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Device: {0}: assigned to address '{1}'  with serial number '{2}'  is Watering".format(
                self.ds,        # {0}
                str(self.ad),   # {1}
                self.sn,        # {2}
                ))

    #################################
    def verify_status_is_done(self):
        if not self.status_is_done_watering(_get_data=True):
            e_msg = "Device: {0}: assigned to address '{1}'  with serial number '{2}' should be Done, " \
                    "Status received was: {3}".format(
                        self.ds,        # {0}
                        str(self.ad),   # {1}
                        self.sn,        # {2}
                        self.ss,        # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Device: {0}: assigned to address '{1}'  with serial number '{2}'  is Done Running".format(
                self.ds,  # {0}
                str(self.ad),  # {1}
                self.sn,  # {2}
            ))

    #################################
    def verify_status_is_soaking(self):
        if not self.status_is_soaking(_get_data=True):
            e_msg = "Device: {0}: assigned to address '{1}'  with serial number '{2}' should be Soaking," \
                    " Status received was: {3}".format(
                        self.ds,        # {0}
                        str(self.ad),   # {1}
                        self.sn,        # {2}
                        self.ss,        # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Device: {0}: assigned to address '{1}'  with serial number '{2}'  is Soaking".format(
                self.ds,        # {0}
                str(self.ad),   # {1}
                self.sn,        # {2}
                ))

    #################################
    def verify_status_is_waiting_to_water(self):
        if not self.status_is_waiting_to_water(_get_data=True):
            e_msg = "Device: {0}: assigned to address '{1}'  with serial number '{2}' should be Waiting, " \
                    "Status received was: {3}".format(
                        self.ds,        # {0}
                        str(self.ad),   # {1}
                        self.sn,        # {2}
                        self.ss,        # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Device: {0}: assigned to address '{1}'  with serial number '{2}' is Waiting To Run".format(
                self.ds,        # {0}
                str(self.ad),   # {1}
                self.sn,        # {2}
                ))

    #################################
    def verify_status_is_paused(self):
        if not self.status_is_paused(_get_data=True):
            e_msg = "Device: {0}: assigned to address '{1}'  with serial number '{2}' should be Paused, " \
                    "Status received was: {3}".format(
                        self.ds,        # {0}
                        str(self.ad),   # {1}
                        self.sn,        # {2}
                        self.ss,        # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Device: {0}: assigned to address '{1}'  with serial number '{2}'  is Paused".format(
                self.ds,        # {0}
                str(self.ad),   # {1}
                self.sn,        # {2}
                ))

    #################################
    def verify_status_is_learning_flow(self):
        if not self.status_is_learning_flow(_get_data=True):
            e_msg = "Zone: {0}'s should be learning flow, Status received was: {1}".format(
                str(self.ad),  # {0}
                self.ss,  # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s is learning flow".format(
                str(self.ad),  # {0}
            ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_data=True):
            e_msg = "Zone: {0}'s should be in error, Status received was: {1}".format(
                str(self.ad),  # {0}
                self.ss,       # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s is error".format(
                str(self.ad),    # {0}
            ))