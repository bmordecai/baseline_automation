import re
import status_parser
from common.imports import opcodes
from common.imports.types import ActionCommands,ProgramCommands
from common.objects.base_classes.base_methods import BaseMethods
from common.objects.statuses.programming.pg_3200_statuses import ProgramStatuses

__author__ = 'baseline'


########################################################################################################################
# Program Base Class
########################################################################################################################
class BasePrograms(BaseMethods):
    """
    Programs Base Class.
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    # Browser Instance
    browser = ''

    #################################
    def __init__(self, _controller, _ad):
        """
        The initializer for the base class of any program. It holds the basic attributes that all Programs share. \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller: common.objects.base_classes.controller.BaseController | common.objects.controllers.bl_32.BaseStation3200

        :param _ds:     A description for the program. \n
        :type _ds:      str \n

        :param _ad:     Program number \n
        :type _ad:      int \n

        :param _en:     The enabled state for the program. Enabled = 'TR', Disabled = 'FA'
        :type _en:      str \n

        :param _ww:     Water window for the program. Two cases accepted.\n
                        - Weekly:   A list with a single element is passed in, the single element having 24 0's or 1's
                                    in a string where 1=On, 0=Off, i.e: ['111111111111111111111111']
                        - Daily:    A list with seven elements, 1 for each day of the week starting with sunday,
                                    with the same format as above, \n
                                    i.e: ['111111111111111111111111', '111111111111111111111111',   \n
                                          '111111111111111111111111', '111111111111111111111111',   \n
                                          '111111111111111111111111', '111111111111111111111111',   \n
                                          '111111111111111111111111'] \n
        :type _ww:      list[str] \n

        :param _mc:     Max number of concurrent zones for programming \n
        :type _mc:      int\n

        :param _sa:     Seasonal adjust for programming with an integer representing a percentage, i.e: 100 = 100% \n
        :type _sa:      int \n

        :param _ss:     Status for program \n
        :type _ss:     str \n
        """
        # Init parent class to inherit respective attributes
        BaseMethods.__init__(self,_ser=_controller.ser, _identifiers=[[opcodes.program, _ad]], _type=opcodes.program)

        self.zone_programs = dict()    # Program Zone objects
        """:type: dict[int, common.objects.programming.zp.ZoneProgram]"""

        # Start Conditions
        self.moisture_start_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.start_condition.MoistureStartCondition]"""

        self.temperature_start_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.start_condition.TemperatureStartCondition]"""

        self.event_switch_start_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.start_condition.SwitchStartCondition]"""

        self.pressure_start_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.start_condition.PressureStartCondition]"""

        # Stop Conditions
        self.moisture_stop_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.stop_condition.MoistureStopCondition]"""

        self.temperature_stop_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.stop_condition.TemperatureStopCondition]"""

        self.event_switch_stop_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.stop_condition.SwitchStopCondition]"""

        self.pressure_stop_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.stop_condition.PressureStopCondition]"""

        # Pause Conditions
        self.moisture_pause_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.pause_condition.MoisturePauseCondition]"""

        self.temperature_pause_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.pause_condition.TemperaturePauseCondition]"""

        self.event_switch_pause_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.pause_condition.SwitchPauseCondition]"""

        self.pressure_pause_conditions = {}
        """:type: dict[int, common.objects.programming.conditions.pause_condition.PressurePauseCondition]"""

        # ----- Instance Variables ----- #
        self.controller = _controller

        # self.master_valves = master_valves
        # self.zp_objects = zone_programs
        self.ad = _ad       # Program Number

        # Place holder for program data from controller when needed.
        self.data = status_parser.KeyValues(string_in=None)

        # these are place holders to keep track of the status of the program for each minute
        self.seconds_program_ran = 0
        self.seconds_program_waited = 0

        # default enabled state to true if nothing is passed in to instance constructor
        self.en = opcodes.true
        # default water window is set to all 1's if nothing is passed in "None"
        # to watering all day if nothing is passed in to instance constructor
        # example weekly water window for 1 day 24 hours ['111111111111111111111111']
        # example daily water window for 7 day 24 hours per day:
        # _ww = [
        # '111111111111111111111111',   - Sunday (says water all day sunday)
        # '000000001111111111111111',   - Monday (says to not water Monday from Midnight to 8am)
        # '000011111111111111110000',   - Tuesday (says to not water from Midnight to 4am AND from 8pm to Midnight)
        # '111111111111111111111111',   - Wednesday
        # '111111111111111111111111',   - Thursday
        # '111111111111111111111111',   - Friday
        # '111111111111111111111111'    - Saturday
        # ]
        # Water Window Case
        self.ww = ['111111111111111111111111']

        # default max concurrent zones to 1 if nothing is passed in to instance constructor
        self.mc = 1

        # default seasonal adjust percentage to 100% if nothing is passed in to instance constructor
        self.sa = 100

        # default status to empty string if nothing passed in to instance constructor
        self.ss = ''

        # Holds the messages return value
        self.build_message_string = ''

        self.statuses = ProgramStatuses(_program_object=self)  # Program Statuses
        """:type: common.objects.statuses.programming.pg_3200_statuses.ProgramStatuses"""

    # ---------------------------------------------------------------------------------------------------------------- #
    # Convenient Status Helpers
    # ---------------------------------------------------------------------------------------------------------------- #

    #################################
    def status_is_disabled(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n

        Returns whether or not the device has a status of disabled.

        usage:

            if self.config.BaseStation3200[1].programs[1].status_is_disabled():
                /* DO SOMETHING */

        :rtype: bool
        """

        if _get_data or not self.data:
            self.get_data()

        return self.ss == opcodes.disabled

    #################################
    def status_is_running(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        Returns whether or not the device has a status of watering.

        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        if _get_data or not self.data:
            self.get_data()

        return self.ss == opcodes.running

    #################################
    def status_is_waiting_to_run(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n

        Returns whether or not the device has a status of waiting to water.

        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_waiting_to_water():
                /* DO SOMETHING */

        :rtype: bool
        """
        if _get_data or not self.data:
            self.get_data()

        return self.ss == opcodes.waiting_to_water

    #################################
    def status_is_soaking(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        Returns whether or not the device has a status of waiting to water.

        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_waiting_to_water():
                /* DO SOMETHING */

        :rtype: bool
        """
        if _get_data or not self.data:
            self.get_data()

        return self.ss == opcodes.soaking

    #################################
    def status_is_paused(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        Returns whether or not the device has a status of waiting to water.

        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_waiting_to_water():
                /* DO SOMETHING */

        :rtype: bool
        """
        if _get_data or not self.data:
            self.get_data()

        return self.ss == opcodes.paused

    #################################
    def status_is_done_running(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        Returns whether or not the device has a status of done watering.

        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_done_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        if _get_data or not self.data:
            self.get_data()

        return self.ss in [opcodes.done_watering, opcodes.error]

    #################################
    def status_is_not_done_running(self, _get_data=False):
        """
        Convenience wrapper
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        Returns whether or not the device has a status of not done watering.
        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_not_done_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        if _get_data or not self.data:
            self.get_data()

        # use "not" here to negate the equality.
        #   Thus, if:
        #           "status == opcodes.done_watering" IS TRUE
        #   then,
        #           "not status == opcodes.done_watering" IS FALSE
        return not self.ss == opcodes.done_watering

    #################################
    def status_is_error(self, _get_data=False):
        """
        Convenience method to return if the device is not in an error state.
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_error():
                /* DO SOMETHING */

        :rtype: bool
        """
        if _get_data or not self.data:
            self.get_data()

        return self.ss == opcodes.error

    #################################
    def status_is_not_error(self, _get_data=False):
        """
        Convenience method to return if the device is not in an error state.
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_not_error():
                /* DO SOMETHING */

        :rtype: bool
        """
        if _get_data or not self.data:
            self.get_data()

        # use "not" here to negate the equality.
        #   Thus, if:
        #           "status == opcodes.error" IS TRUE
        #   then,
        #           "not status == opcodes.error" IS FALSE
        return not self.ss == opcodes.error

    #################################
    def status_is_learning_flow(self, _get_data=False):
        """
        Convenience method to return if the device is not in an error state.
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_not_error():
                /* DO SOMETHING */

        :rtype: bool
        """
        if _get_data or not self.data:
            self.get_data()

        return self.ss == opcodes.learn_flow_active
    #################################
    def status_transitioned_from_running_to_soaking(self, _get_data=False):
        """
        Convenience method to return if the device went from watering to soaking.
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_transitioned_from_watering_to_soaking():
                /* DO SOMETHING */

        :rtype: bool
        """
        # Get data if we haven't already
        if not self.data:
            self.get_data()
        return self.last_ss == opcodes.running and self.ss in [opcodes.soaking]

    #################################
    def status_transitioned_from_soaking_to_running(self, _get_data=False):
        """
        Convenience method to return if the device went from soaking to watering.
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_transitioned_from_soaking_to_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        # Get data if we haven't already
        if _get_data or not self.data:
            self.get_data()
        return self.last_ss == opcodes.running and self.ss == opcodes.soaking

    #################################
    def status_transitioned_from_waiting_to_running(self, _get_data=False):
        """
        Convenience method to return if the device went from soaking to watering.
        :param _get_data:     if true than get data
        :type _get_data:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_transitioned_from_soaking_to_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        # Get data if we haven't already
        if _get_data or not self.data:
            self.get_data()
        return self.last_ss == opcodes.waiting_to_water and self.ss == opcodes.running

    #################################
    def set_learn_flow_to_start(self, _time_delay=None):
        """
        - Turns on the learn flow for a particular program or zone
        """
        # DO,LF,PG={AD}
        command = "{0},{1},{2}={3}".format(
            ActionCommands.DO,
            opcodes.learn_flow,
            opcodes.program,
            self.ad
        )

        # Check if the zone address passed in was an int
        if _time_delay is not None and not isinstance(_time_delay, int):
            e_msg = "Exception occurred trying to enable learn flow. Time delay passed in was an Invalid " \
                    "argument type, expected an int, received: {0}" \
                .format(type(_time_delay))
            raise TypeError(e_msg)

        elif _time_delay is not None:
            # DO,LF,PG={AD},TM={MINS}
            command += ",{0}={1}".format(
                opcodes.time_delay,
                _time_delay
            )
        try:
            # Send the learn flow command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set a learn flow on program '{0}' to start'.".format(
                self.ad)
            raise Exception(e_msg)
        else:
            print "Successfully set the learn flow on program '{0}' to start.".format(self.ad)

    #################################
    def set_learn_flow_to_start_advanced(self, _time_delay=None):
        """
        - Turns on the advanced learn flow for a particular program or zone
        """
        # DO,LF,PG={AD}
        command = "{0},{1},{2}={3},TY=AD".format(
            ActionCommands.DO,
            opcodes.learn_flow,
            opcodes.program,
            self.ad
        )

        # Check if the zone address passed in was an int
        if _time_delay is not None and not isinstance(_time_delay, int):
            e_msg = "Exception occurred trying to enable learn flow. Time delay passed in was an Invalid " \
                    "argument type, expected an int, received: {0}" \
                .format(type(_time_delay))
            raise TypeError(e_msg)

        elif _time_delay is not None:
            # DO,LF,PG={AD},TM={MINS}
            command += ",{0}={1}".format(
                opcodes.time_delay,
                _time_delay
            )
        try:
            # Send the learn flow command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set a learn flow on program '{0}' to start'.".format(
                self.ad)
            raise Exception(e_msg)
        else:
            print "Successfully set the learn flow on program '{0}' to start.".format(self.ad)

    #################################
    def set_learn_flow_to_start_quick(self, _time_delay=None):
        """
        - Turns on the learn flow for a particular program or zone
        """
        # DO,LF,PG={AD}
        command = "{0},{1},{2}={3},TY=QK".format(
            ActionCommands.DO,
            opcodes.learn_flow,
            opcodes.program,
            self.ad
        )

        # Check if the zone address passed in was an int
        if _time_delay is not None and not isinstance(_time_delay, int):
            e_msg = "Exception occurred trying to enable learn flow. Time delay passed in was an Invalid " \
                    "argument type, expected an int, received: {0}" \
                .format(type(_time_delay))
            raise TypeError(e_msg)

        elif _time_delay is not None:
            # DO,LF,PG={AD},TM={MINS}
            command += ",{0}={1}".format(
                opcodes.time_delay,
                _time_delay
            )
        try:
            # Send the learn flow command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set a learn flow on program '{0}' to start'.".format(
                self.ad)
            raise Exception(e_msg)
        else:
            print "Successfully set the learn flow on program '{0}' to start.".format(self.ad)

    #################################
    def set_learn_flow_to_start_standard(self, _time_delay=None):
        """
        - Turns on the learn flow for a particular program or zone
        """
        # DO,LF,PG={AD},TY=SD
        command = "{0},{1},{2}={3},TY=SD".format(
            ActionCommands.DO,
            opcodes.learn_flow,
            opcodes.program,
            self.ad
        )

        # Check if the zone address passed in was an int
        if _time_delay is not None and not isinstance(_time_delay, int):
            e_msg = "Exception occurred trying to enable learn flow. Time delay passed in was an Invalid " \
                    "argument type, expected an int, received: {0}" \
                .format(type(_time_delay))
            raise TypeError(e_msg)

        elif _time_delay is not None:
            # DO,LF,PG={AD},TY=SD,TM={MINS}
            command += ",{0}={1}".format(
                opcodes.time_delay,
                _time_delay
            )
        try:
            # Send the learn flow command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set a learn flow on program '{0}' to start'.".format(
                self.ad)
            raise Exception(e_msg)
        else:
            print "Successfully set the learn flow on program '{0}' to start.".format(self.ad)

    #################################
    def set_water_window(self, _ww=None, _is_weekly=True):
        """
        Sets the water window for the program instance on the controller. \n
        :param _ww:     Water Window for set. Requires binary notation for hours 0-23 (1's and 0's) \n
                        String of 1's and 0's (i.e. '00000011111111111111111' says don't water from midnight to 5am) \n
        :type _ww:      list[str] \n
        :return:
        """
        # If user wants to overwrite default value, they can pass in a value
        if _ww is not None:
            index = 0
            for each_item in _ww:
                # For each water window value (if weekly there wil only be one value, otherwise there will be a
                # string for each day of the week in the list.)
                # Searches the water window string input for anything that isn't a 0 or a 1, if it finds anything that
                # is not a 0 or a 1, then an exception is raised
                if re.search(r'\D|[2-9]+', each_item):
                    e_msg = "Invalid water window for program to set at index {0}: {1}. Valid water window must " \
                            "be a string of 0's and 1's.".format(str(index), str(_ww[index]))
                    raise ValueError(e_msg)
                if len(each_item) is not 24:
                    e_msg = "Invalid water window for program to set at index {0}: {1}. Valid water window must be" \
                            "exactly 24 characters long".format(str(index), str(_ww[index]))
                    raise ValueError(e_msg)
                index += 1

            if _is_weekly is True and len(_ww) is not 1:
                e_msg = "Invalid # of list elements for a water window: if weekly water window = true, " \
                        "# of arguments = 1"
                raise ValueError(e_msg)
            elif _is_weekly is False and len(_ww) is not 7:
                e_msg = "Invalid # of list elements for a water window: if weekly water window = false, " \
                        "# of arguments = 7"
                raise ValueError(e_msg)

            # Set water window list to new list, all items were valid syntax
            self.ww = _ww

        # get base string for set command for sending to controller.
        command = self.build_base_string_for_setters()

        # If water window type is weekly
        if _is_weekly:
            command += ",{0}={1}".format(
                ProgramCommands.Attributes.WATER_WINDOW,    # {0}
                self.ww[0]                                  # {1}
            )
        # Else, water window type is daily, append all days starting with w1=sunday through saturday
        else:
            command += ",W1={0},W2={1},W3={2},W4={3},W5={4},W6={5},W7={6}".format(
                self.ww[0],     # {0}
                self.ww[1],     # {1}
                self.ww[2],     # {2}
                self.ww[3],     # {3}
                self.ww[4],     # {4}
                self.ww[5],     # {5}
                self.ww[6]      # {6}
            )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0}'s Water Window to: {1}".format(str(self.ad), self.ww)
            raise Exception(e_msg)
        else:
            print("Successfully set Water Window for Program: {0} to: {1}".format(str(self.ad), self.ww))

    #################################
    def set_daily_water_window(self, _ww=None):
        """
        Sets the water window for the program instance on the controller. \n
        :param _ww:     Water Window for set. Requires binary notation for hours 0-23 (1's and 0's) \n
                        String of 1's and 0's (i.e. '00000011111111111111111' says don't water from midnight to 5am) \n
        :type _ww:      list[str] \n
        :return:
        """
        # If user wants to overwrite default value, they can pass in a value
        if _ww is not None:
            index = 0
            for each_item in _ww:
                # For each water window value (if weekly there wil only be one value, otherwise there will be a
                # string for each day of the week in the list.)
                # Searches the water window string input for anything that isn't a 0 or a 1, if it finds anything that
                # is not a 0 or a 1, then an exception is raised
                if re.search(r'\D|[2-9]+', each_item):
                    e_msg = "Invalid water window for program to set at index {0}: {1}. Valid water window must " \
                            "be a string of 0's and 1's.".format(str(index), str(_ww[index]))
                    raise ValueError(e_msg)
                if len(each_item) is not 24:
                    e_msg = "Invalid water window for program to set at index {0}: {1}. Valid water window must be" \
                            "exactly 24 characters long".format(str(index), str(_ww[index]))
                    raise ValueError(e_msg)
                index += 1

            if len(_ww) is not 1:
                e_msg = "Invalid # of list elements for a water window: if weekly water window = true, " \
                        "# of arguments = 1"
                raise ValueError(e_msg)
            # Set water window list to new list, all items were valid syntax
            self.ww = _ww

        # get base string for set command for sending to controller.
        command = self.build_base_string_for_setters()

        # If water window type is weekly
        command += ",{0}={1}".format(
            ProgramCommands.Attributes.WATER_WINDOW,  # {0}
            self.ww[0]  # {1}
        )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0}'s Water Window to: {1}".format(str(self.ad),
                                                                                                 self.ww)
            raise Exception(e_msg)
        else:
            print("Successfully set Water Window for Program: {0} to: {1}".format(str(self.ad), self.ww))

    #################################
    def set_weekly_water_window(self, _ww=None):
        """
        Sets the water window for the program instance on the controller. \n
        :param _ww:     Water Window for set. Requires binary notation for hours 0-23 (1's and 0's) \n
                        example daily water window for 7 day 24 hours per day:
                        _ww = [
                        '111111111111111111111111',   - Sunday (says water all day sunday)
                        '000000001111111111111111',   - Monday (says to not water Monday from Midnight to 8am)
                        '000011111111111111110000',   - Tuesday (says to not water from Midnight to 4am AND from 8pm to Midnight)
                        '111111111111111111111111',   - Wednesday
                        '111111111111111111111111',   - Thursday
                        '111111111111111111111111',   - Friday
                        '111111111111111111111111'    - Saturday
                        # ]
        :type _ww:      list[str] \n
        :return:
        """

        # If user wants to overwrite default value, they can pass in a value
        if _ww is not None:
            index = 0
            for each_item in _ww:
                # For each water window value (if weekly there wil only be one value, otherwise there will be a
                # string for each day of the week in the list.)
                # Searches the water window string input for anything that isn't a 0 or a 1, if it finds anything that
                # is not a 0 or a 1, then an exception is raised
                if re.search(r'\D|[2-9]+', each_item):
                    e_msg = "Invalid water window for program to set at index {0}: {1}. Valid water window must " \
                            "be a string of 0's and 1's.".format(str(index), str(_ww[index]))
                    raise ValueError(e_msg)
                if len(each_item) is not 24:
                    e_msg = "Invalid water window for program to set at index {0}: {1}. Valid water window must be" \
                            "exactly 24 characters long".format(str(index), str(_ww[index]))
                    raise ValueError(e_msg)
                index += 1
            if len(_ww) is not 7:
               e_msg = "Invalid # of list elements for a water window: if weekly water window = false, " \
                        "# of arguments = 7"
               raise ValueError(e_msg)

            # Set water window list to new list, all items were valid syntax
            self.ww = _ww

        # get base string for set command for sending to controller.
        command = self.build_base_string_for_setters()

        command += ",W1={0},W2={1},W3={2},W4={3},W5={4},W6={5},W7={6}".format(
                self.ww[0],  # {0}
                self.ww[1],  # {1}
                self.ww[2],  # {2}
                self.ww[3],  # {3}
                self.ww[4],  # {4}
                self.ww[5],  # {5}
                self.ww[6]  # {6}
            )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0}'s Water Window to: {1}".format(str(self.ad),
                                                                                                 self.ww)
            raise Exception(e_msg)
        else:
            print("Successfully set Water Window for Program: {0} to: {1}".format(str(self.ad), self.ww))
    #################################
    def set_max_concurrent_zones(self, _number_of_zones=None):
        """
        Sets the max number of concurrent zones to run during program. \n
        :param _number_of_zones:        Number of zones to set for concurrent zones for programming.
        :type _number_of_zones:         Integer \n
        :return:
        """
        # If user wants to overwrite default value, they can pass in a value
        if _number_of_zones is not None:

            # Verifies the number of zones passed in is an int
            if not isinstance(_number_of_zones, int):
                e_msg = "Failed trying to set PG {0} concurrent zones. Invalid type, expected an int, " \
                        "received: {1}".format(str(self.ad), type(_number_of_zones))
                raise TypeError(e_msg)
            else:
                self.mc = _number_of_zones

        # get base string for set command for sending to controller.
        command = self.build_base_string_for_setters()
        # Command for sending to controller.
        command += ",{0}={1}".format(
            ProgramCommands.Attributes.MAX_CONCURRENT_ZONES,    # {0}
            str(self.mc)                                        # {1}
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0}'s max concurrent zones to: {1}".format(str(self.ad),
                                                                                                         str(self.mc))
            raise Exception(e_msg)
        else:
            print("Successfully set Max Concurrent Zones for Program: {0} to: {1}".format(str(self.ad), str(self.mc)))

    #################################
    def set_seasonal_adjust(self, _percent=None):
        """
        Sets the seasonal adjust for program on the controller. \n
        :param _percent:        Percent to assign to seasonal adjust value for program. \n
        :type _percent:         Integer \n
        :return:
        """
        # If user wants to overwrite default value, they can pass in a value
        if _percent is not None:

            # Verifies the percent passed in is an int or float
            if not isinstance(_percent, int):
                e_msg = "Failed trying to set PG {0} seasonal adjust. Invalid type, expected an int, " \
                        "received: {1}".format(str(self.ad), type(_percent))
                raise TypeError(e_msg)
            else:
                self.sa = _percent
            # this goes through and updates the runtime of the zone in the zone program
            for index, zone in self.zone_programs.iteritems():
                if zone.program.ad == self.ad:
                    zone.rt = float(zone.rt) * (float(self.sa)/100)
                    zone.rt = 60*(int(round(float(zone.rt)/(float(60)))))

        # get base string for set command for sending to controller.
        command = self.build_base_string_for_setters()
        # Command for sending to controller.
        command += ",{0}={1}".format(
            ProgramCommands.Attributes.SEASONAL_ADJUST,     # {0}
            str(self.sa)                                    # {1}
                )
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Program {0}'s Seasonal Adjust to: {1}".format(str(self.ad),
                                                                                                    str(self.sa))
            raise Exception(e_msg)
        else:
            print("Successfully set Seasonal Adjust for Program: {0} to: {1}".format(str(self.ad), str(self.sa)))

    #################################
    def verify_water_window(self):
        """
        Verifies the Program Water Window set on the controller. \n
        :return:
        """
        # empty list place holder
        ww_from_cn = []

        # First check to see which water window type is currently being used, either a weekly or daily schedule.
        # If weekly schedule being used, then the length of our 'self.ww' list should be 1, otherwise the length of
        # 'self.ww' list should be 7 (one for each day of the week starting with sunday to saturday)
        if len(self.ww) == 1:
            ww_from_cn.append(self.data.get_value_string_by_key('WW'))

        # Else, assume daily water window scheduling, so append each day to the list
        else:
            # 'W1' - Sunday
            # 'W2' - Monday
            # 'W3' - Tuesday
            # 'W4' - Wednesday
            # 'W5' - Thursday
            # 'W6' - Friday
            # 'W7' - Saturday
            for each_weekday in ['W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7']:
                ww_from_cn.append(self.data.get_value_string_by_key(each_weekday))

        # Compare Water Window values, here we are comparing lists to each other, which is equivalent to comparing
        # each list item individually.
        if self.ww != ww_from_cn:
            e_msg = "Unable verify Program: {0}'s Water Window. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(ww_from_cn),    # {1}
                str(self.ww)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s Water Window: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  self.ww           # {1}
                  ))

    #################################
    def verify_max_concurrent_zones(self):
        """
        Verifies the Program Max Concurrent Zones set on the controller. \n
        :return:
        """
        mc = int(self.data.get_value_string_by_key(opcodes.max_concurrent_zones))

        # Compare Max Concurrent Zone values
        if self.mc != mc:
            e_msg = "Unable verify Program: {0}'s Max Concurrent Zones. Received: {1}, Expected: {2}".format(
                    str(self.ad),   # {0}
                    str(mc),        # {1}
                    str(self.mc)    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s Max Concurrent Zones: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.mc)      # {1}
                  ))

    #################################
    def verify_seasonal_adjust(self):
        """
        Verifies the Program Seasonal Adjust set on the controller. \n
        :return:
        """
        seasonal_adjust = int(self.data.get_value_string_by_key(opcodes.seasonal_adjust))

        # Compare Seasonal Adjust values
        if self.sa != seasonal_adjust:
            e_msg = "Unable verify Program: {0}'s Seasonal Adjust. Received: {1}, Expected: {2}".format(
                    str(self.ad),           # {0}
                    str(seasonal_adjust),   # {1}
                    str(self.sa)            # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s Seasonal Adjust: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.sa)      # {1}
                  ))

    #################################
    def verify_status_is_running(self):
        if not self.status_is_running(_get_data=True):
            e_msg = "Program: {0}'s should be Running Status received was: {1}".format(
                str(self.ad),   # {0}
                self.ss,        # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s is Running".format(
                str(self.ad),  # {0}
            ))

    #################################
    def verify_status_is_done(self):
        if not self.status_is_done_running(_get_data=True):
            e_msg = "Program: {0}'s should be Done, Status received was: {1}".format(
                str(self.ad),   # {0}
                self.ss,        # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s is Done Running".format(
                str(self.ad),  # {0}
            ))

    #################################
    def verify_status_is_soaking(self):
        if not self.status_is_soaking(_get_data=True):
            e_msg = "Program: {0}'s should be Soaking, Status received was: {1}".format(
                str(self.ad),   # {0}
                self.ss,        # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s is Soaking".format(
                str(self.ad),  # {0}
            ))

    #################################
    def verify_status_is_waiting_to_run(self):
        if not self.status_is_waiting_to_run(_get_data=True):
            e_msg = "Program: {0}'s should be Waiting, Status received was: {1}".format(
                str(self.ad),   # {0}
                self.ss,        # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s is Waiting To Run".format(
                str(self.ad),  # {0}
            ))

    #################################
    def verify_status_is_paused(self):
        if not self.status_is_paused(_get_data=True):
            e_msg = "Program: {0}'s should be Paused, Status received was: {1}".format(
                str(self.ad),  # {0}
                self.ss,  # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s is Paused".format(
                str(self.ad),  # {0}
            ))

    #################################
    def verify_status_is_disabled(self):
        if not self.status_is_disabled(_get_data=True):
            e_msg = "Program: {0}'s should be Disabled, Status received was: {1}".format(
                str(self.ad),  # {0}
                self.ss,  # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s is Disabled".format(
                str(self.ad),  # {0}
            ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_data=True):
            e_msg = "Program: {0}'s should have an Error, Status received was: {1}".format(
                str(self.ad),  # {0}
                self.ss,  # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s has an Error".format(
                str(self.ad),  # {0}
            ))

    #################################
    def verify_status_is_not_error(self):
        if not self.status_is_not_error(_get_data=True):
            e_msg = "Program: {0}'s should not have an Error, Status received was: {1}".format(
                str(self.ad),  # {0}
                self.ss,  # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s does not have an Error".format(
                str(self.ad),  # {0}
            ))

    #################################
    def verify_status_is_learning_flow(self):
        if not self.status_is_learning_flow(_get_data=True):
            e_msg = "Program: {0}'s should be learning flow, Status received was: {1}".format(
                str(self.ad),  # {0}
                self.ss,  # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Program {0}'s is learing flow".format(
                str(self.ad),  # {0}
            ))

    #################################
    def verify_last_start_date(self):
        """
        Verifies the Last Start Date set on the controller. \n
        :return:
        """
        ls = str(self.data.get_value_string_by_key(opcodes.last_start_date))

        # # Compare Max Concurrent Zone values
        # if self.mc != mc:
        #     e_msg = "Unable verify Program: {0}'s Max Concurrent Zones. Received: {1}, Expected: {2}".format(
        #         str(self.ad),   # {0}
        #         str(mc),        # {1}
        #         str(self.mc)    # {2}
        #     )
        #     raise ValueError(e_msg)
        # else:
        #     print("Verified Program {0}'s Max Concurrent Zones: '{1}' on controller".format(
        #         str(self.ad),     # {0}
        #         str(self.mc)      # {1}
        #     ))

    #################################
    def verify_last_stop_date(self):
        """
        Verifies the Last Start Date set on the controller. \n
        :return:
        """
        lf = str(self.data.get_value_string_by_key(opcodes.last_stop_date))

    #################################
    def verify_last_pause_date(self):
        """
        Verifies the Last Start Date set on the controller. \n
        :return:
        """
        lp = str(self.data.get_value_string_by_key(opcodes.last_pause_date))

    #################################
    def verify_last_run_duration(self):
        """
        Verifies the Last Start Date set on the controller. \n
        :return:
        """
        ld = str(self.data.get_value_string_by_key(opcodes.last_run_duration))

    #################################
    def verify_water_used_estimate(self):
        """
        Verifies the Last Start Date set on the controller. \n
        :return:
        """
        lu = str(self.data.get_value_string_by_key(opcodes.last_water_used_estimate))

    #################################
    def verify_start_estimate(self):
        """
        Verifies the Last Start Date set on the controller. \n
        :return:
        """
        ns = str(self.data.get_value_string_by_key(opcodes.next_start_estimate))

    #################################
    def verify_stop_estimate(self):
        """
        Verifies the Last Start Date set on the controller. \n
        :return:
        """
        nf = str(self.data.get_value_string_by_key(opcodes.last_stop_date))

    #################################
    def verify_duration_estimate(self):
        """
        Verifies the Last Start Date set on the controller. \n
        :return:
        """
        nd = str(self.data.get_value_string_by_key(opcodes.next_duration_estimate))

    #################################
    def verify_duration_estimate(self):
        """
        Verifies the Last Start Date set on the controller. \n
        :return:
        """
        nd = str(self.data.get_value_string_by_key(opcodes.next_duration_estimate))

    #################################
    def add_zone_to_program(self, _zone_address):
        """
        Add a zone to the program
        :param _zone_address:            address of the substation \n
        :type _zone_address:             int \n
        :return:
        :rtype:
        """
        from common.object_factory import create_zone_program_object
        # create water_source object
        if not isinstance(_zone_address, int):
            e_msg = "Exception occurred trying to add zone {0} to the program {1}. The value of the zone address " \
                    "must be an int, instead it was of type '{2}'.".format(
                         str(_zone_address),    # {0}
                         str(self.ad),          # {1}
                         type(_zone_address)    # {2}
                    )
            raise Exception(e_msg)
        # loop through zone dict to see if zone object is present
        if _zone_address not in self.controller.zones.keys():
            e_msg = 'Exception occurred trying to add zone {0} to program {1}. The zone address did not belong to any' \
                    'zones on the controller.'.format(
                        str(_zone_address),     # {0}
                        str(self.ad),           # {1}
                    )
            raise Exception(e_msg)

        # create program zone object
        create_zone_program_object(controller=self.controller, program_address=self.ad, zone_address=_zone_address)
        self.zone_programs[_zone_address].send_programming()

    #################################
    def add_moisture_start_condition(self, _moisture_sensor_address):
        """
        Add a moisture start condition on the program. \n

        :param _moisture_sensor_address:    address of the moisture sensor \n
        :type _moisture_sensor_address:     int \n

        :return:
        :rtype:
        """
        from common.object_factory import create_moisture_start_condition_object

        # create start condition object
        if not isinstance(_moisture_sensor_address, int):
            e_msg = "Exception occurred trying to add Moisture Start Condition {0} to the program {1}. The value of " \
                    "the moisture sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_moisture_sensor_address),  # {0}
                        str(self.ad),                   # {1}
                        type(_moisture_sensor_address)  # {2}
                    )
            raise Exception(e_msg)

        # loop through zone dict to see if zone object is present
        if _moisture_sensor_address not in self.controller.moisture_sensors.keys():
            e_msg = 'Exception occurred trying to add Moisture Start Condition {0} to program {1}. The moisture ' \
                    'sensor address did not belong to any moisture sensors on the controller.'.format(
                        str(_moisture_sensor_address),      # {0}
                        str(self.ad),                       # {1}
                    )
            raise Exception(e_msg)

        # create program zone object
        create_moisture_start_condition_object(controller=self.controller, program_address=self.ad,
                                               moisture_sensor_address=_moisture_sensor_address)

    #################################
    def add_moisture_stop_condition(self, _moisture_sensor_address):
        """
        Add a moisture stop condition on the program. \n

        :param _moisture_sensor_address:    address of the moisture sensor \n
        :type _moisture_sensor_address:     int \n

        :return:
        :rtype:
        """
        from common.object_factory import create_moisture_stop_condition_object

        # create start condition object
        if not isinstance(_moisture_sensor_address, int):
            e_msg = "Exception occurred trying to add Moisture Stop Condition {0} to the program {1}. The value of " \
                    "the moisture sensor address must be an int, instead it was of type '{2}'.".format(
                     str(_moisture_sensor_address),     # {0}
                     str(self.ad),                      # {1}
                     type(_moisture_sensor_address)     # {2}
                    )
            raise Exception(e_msg)

        # loop through zone dict to see if zone object is present
        if _moisture_sensor_address not in self.controller.moisture_sensors.keys():
            e_msg = 'Exception occurred trying to add Moisture Start Condition {0} to program {1}. The moisture ' \
                    'sensor address did not belong to any moisture sensors on the controller.'.format(
                        str(_moisture_sensor_address),      # {0}
                        str(self.ad),                       # {1}
                    )
            raise Exception(e_msg)

        # create program zone object
        create_moisture_stop_condition_object(controller=self.controller, program_address=self.ad,
                                              moisture_sensor_address=_moisture_sensor_address)

    #################################
    def add_moisture_pause_condition(self, _moisture_sensor_address):
        """
        Add a moisture pause condition on the program. \n

        :param _moisture_sensor_address:    address of the moisture sensor \n
        :type _moisture_sensor_address:     int \n

        :return:
        :rtype:
        """
        from common.object_factory import create_moisture_pause_condition_object

        # create start condition object
        if not isinstance(_moisture_sensor_address, int):
            e_msg = "Exception occurred trying to add Moisture Pause Condition {0} to the program {1}. The value of " \
                    "the moisture sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_moisture_sensor_address),  # {0}
                        str(self.ad),                   # {1}
                        type(_moisture_sensor_address)  # {2}
                    )
            raise Exception(e_msg)

        # loop through zone dict to see if zone object is present
        if _moisture_sensor_address not in self.controller.moisture_sensors.keys():
            e_msg = 'Exception occurred trying to add Moisture Start Condition {0} to program {1}. The moisture ' \
                    'sensor address did not belong to any moisture sensors on the controller.'.format(
                        str(_moisture_sensor_address),      # {0}
                        str(self.ad),                       # {1}
                    )
            raise Exception(e_msg)

        # create program zone object
        create_moisture_pause_condition_object(controller=self.controller, program_address=self.ad,
                                               moisture_sensor_address=_moisture_sensor_address)

    #################################
    def add_temperature_start_condition(self, _temperature_sensor_address):
        """
        Add a temperature start condition on the program. \n

        :param _temperature_sensor_address:    address of the temperature sensor \n
        :type _temperature_sensor_address:     int \n

        :return:
        :rtype:
        """
        from common.object_factory import create_temperature_start_condition_object

        # create start condition object
        if not isinstance(_temperature_sensor_address, int):
            e_msg = "Exception occurred trying to add Temperature Start Condition {0} to the program {1}. The value" \
                    "of the temperature sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_temperature_sensor_address),   # {0}
                        str(self.ad),                       # {1}
                        type(_temperature_sensor_address)   # {2}
                    )
            raise Exception(e_msg)

        # loop through zone dict to see if zone object is present
        if _temperature_sensor_address not in self.controller.temperature_sensors.keys():
            e_msg = 'Exception occurred trying to add Temperature Start Condition {0} to program {1}. The temperature' \
                    'sensor address did not belong to any temperature sensors on the controller.'.format(
                        str(_temperature_sensor_address),  # {0}
                        str(self.ad),  # {1}
                    )
            raise Exception(e_msg)

        # create program zone object
        create_temperature_start_condition_object(controller=self.controller, program_address=self.ad,
                                                  temperature_sensor_address=_temperature_sensor_address)

    #################################
    def add_temperature_stop_condition(self, _temperature_sensor_address):
        """
        Add a temperature stop condition on the program. \n

        :param _temperature_sensor_address:    address of the temperature sensor \n
        :type _temperature_sensor_address:     int \n

        :return:
        :rtype:
        """
        from common.object_factory import create_temperature_stop_condition_object

        # create start condition object
        if not isinstance(_temperature_sensor_address, int):
            e_msg = "Exception occurred trying to add Temperature Stop Condition {0} to the program {1}. The value of " \
                    "the temperature sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_temperature_sensor_address),   # {0}
                        str(self.ad),                       # {1}
                        type(_temperature_sensor_address)   # {2}
                    )
            raise Exception(e_msg)

        # loop through zone dict to see if zone object is present
        if _temperature_sensor_address not in self.controller.temperature_sensors.keys():
            e_msg = 'Exception occurred trying to add Temperature Start Condition {0} to program {1}. The temperature ' \
                    'sensor address did not belong to any temperature sensors on the controller.'.format(
                        str(_temperature_sensor_address),   # {0}
                        str(self.ad),                       # {1}
                    )
            raise Exception(e_msg)

        # create program zone object
        create_temperature_stop_condition_object(controller=self.controller, program_address=self.ad,
                                                 temperature_sensor_address=_temperature_sensor_address)

    #################################
    def add_temperature_pause_condition(self, _temperature_sensor_address):
        """
        Add a temperature pause condition on the program. \n

        :param _temperature_sensor_address:    address of the temperature sensor \n
        :type _temperature_sensor_address:     int \n

        :return:
        :rtype:
        """
        from common.object_factory import create_temperature_pause_condition_object

        # create start condition object
        if not isinstance(_temperature_sensor_address, int):
            e_msg = "Exception occurred trying to add Temperature Pause Condition {0} to the program {1}. The value of " \
                    "the temperature sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_temperature_sensor_address),   # {0}
                        str(self.ad),                       # {1}
                        type(_temperature_sensor_address)   # {2}
                    )
            raise Exception(e_msg)

        # loop through zone dict to see if zone object is present
        if _temperature_sensor_address not in self.controller.temperature_sensors.keys():
            e_msg = 'Exception occurred trying to add Temperature Start Condition {0} to program {1}. The temperature ' \
                    'sensor address did not belong to any temperature sensors on the controller.'.format(
                        str(_temperature_sensor_address),  # {0}
                        str(self.ad),                      # {1}
                    )
            raise Exception(e_msg)

        # create program zone object
        create_temperature_pause_condition_object(controller=self.controller, program_address=self.ad,
                                                  temperature_sensor_address=_temperature_sensor_address)

    #################################
    def add_pressure_start_condition(self, _pressure_sensor_address):
        """
        Add a pressure start condition on the program. \n

        :param _pressure_sensor_address:    address of the pressure sensor \n
        :type _pressure_sensor_address:     int \n

        :return:
        :rtype:
        """
        from common.object_factory import create_pressure_start_condition_object

        # create start condition object
        if not isinstance(_pressure_sensor_address, int):
            e_msg = "Exception occurred trying to add Pressure Start Condition {0} to the program {1}. The value of " \
                    "the pressure sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_pressure_sensor_address),  # {0}
                        str(self.ad),                   # {1}
                        type(_pressure_sensor_address)  # {2}
                    )
            raise Exception(e_msg)

        # loop through zone dict to see if zone object is present
        if _pressure_sensor_address not in self.controller.pressure_sensors.keys():
            e_msg = 'Exception occurred trying to add Pressure Start Condition {0} to program {1}. The pressure ' \
                    'sensor address did not belong to any pressure sensors on the controller.'.format(
                        str(_pressure_sensor_address),  # {0}
                        str(self.ad),                   # {1}
                    )
            raise Exception(e_msg)

        # create program zone object
        create_pressure_start_condition_object(controller=self.controller, program_address=self.ad,
                                               pressure_sensor_address=_pressure_sensor_address)

    #################################
    def add_pressure_stop_condition(self, _pressure_sensor_address):
        """
        Add a pressure stop condition on the program. \n

        :param _pressure_sensor_address:    address of the pressure sensor \n
        :type _pressure_sensor_address:     int \n

        :return:
        :rtype:
        """
        from common.object_factory import create_pressure_stop_condition_object

        # create start condition object
        if not isinstance(_pressure_sensor_address, int):
            e_msg = "Exception occurred trying to add Pressure Stop Condition {0} to the program {1}. The value of " \
                    "the pressure sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_pressure_sensor_address),  # {0}
                        str(self.ad),                   # {1}
                        type(_pressure_sensor_address)  # {2}
                    )
            raise Exception(e_msg)

        # loop through zone dict to see if zone object is present
        if _pressure_sensor_address not in self.controller.pressure_sensors.keys():
            e_msg = 'Exception occurred trying to add Pressure Start Condition {0} to program {1}. The pressure ' \
                    'sensor address did not belong to any pressure sensors on the controller.'.format(
                        str(_pressure_sensor_address),  # {0}
                        str(self.ad),                   # {1}
                    )
            raise Exception(e_msg)

        # create program zone object
        create_pressure_stop_condition_object(controller=self.controller, program_address=self.ad,
                                              pressure_sensor_address=_pressure_sensor_address)

    #################################
    def add_pressure_pause_condition(self, _pressure_sensor_address):
        """
        Add a pressure pause condition on the program. \n

        :param _pressure_sensor_address:    address of the pressure sensor \n
        :type _pressure_sensor_address:     int \n

        :return:
        :rtype:
        """
        from common.object_factory import create_pressure_pause_condition_object

        # create start condition object
        if not isinstance(_pressure_sensor_address, int):
            e_msg = "Exception occurred trying to add Pressure Pause Condition {0} to the program {1}. The value of " \
                    "the pressure sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_pressure_sensor_address),  # {0}
                        str(self.ad),                   # {1}
                        type(_pressure_sensor_address)  # {2}
                    )
            raise Exception(e_msg)

        # loop through zone dict to see if zone object is present
        if _pressure_sensor_address not in self.controller.pressure_sensors.keys():
            e_msg = 'Exception occurred trying to add Pressure Start Condition {0} to program {1}. The pressure ' \
                    'sensor address did not belong to any pressure sensors on the controller.'.format(
                        str(_pressure_sensor_address),  # {0}
                        str(self.ad),                   # {1}
                    )
            raise Exception(e_msg)

        # create program zone object
        create_pressure_pause_condition_object(controller=self.controller, program_address=self.ad,
                                               pressure_sensor_address=_pressure_sensor_address)

    #################################
    def add_switch_start_condition(self, _event_switch_address):
        """
        Add a switch start condition on the program. \n

        :param _event_switch_address:    address of the event switch \n
        :type _event_switch_address:     int \n

        :return:
        :rtype:
        """
        from common.object_factory import create_switch_start_condition_object

        # create start condition object
        if not isinstance(_event_switch_address, int):
            e_msg = "Exception occurred trying to add Event Switch Start Condition {0} to the program {1}. The value " \
                    "of the switch sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_event_switch_address),  # {0}
                        str(self.ad),                # {1}
                        type(_event_switch_address)  # {2}
                    )
            raise Exception(e_msg)

        # loop through zone dict to see if zone object is present
        if _event_switch_address not in self.controller.event_switches.keys():
            e_msg = 'Exception occurred trying to add Event Switch Start Condition {0} to program {1}. The switch ' \
                    'sensor address did not belong to any switch sensors on the controller.'.format(
                        str(_event_switch_address),     # {0}
                        str(self.ad),                   # {1}
                    )
            raise Exception(e_msg)

        # create program zone object
        create_switch_start_condition_object(controller=self.controller, program_address=self.ad,
                                             event_switch_address=_event_switch_address)

    #################################
    def add_switch_stop_condition(self, _event_switch_address):
        """
        Add a switch stop condition on the program. \n

        :param _event_switch_address:    address of the event switch \n
        :type _event_switch_address:     int \n

        :return:
        :rtype:
        """
        from common.object_factory import create_switch_stop_condition_object

        # create start condition object
        if not isinstance(_event_switch_address, int):
            e_msg = "Exception occurred trying to add Event Switch Stop Condition {0} to the program {1}. The value " \
                    "of the switch sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_event_switch_address),     # {0}
                        str(self.ad),                   # {1}
                        type(_event_switch_address)     # {2}
                    )
            raise Exception(e_msg)

        # loop through zone dict to see if zone object is present
        if _event_switch_address not in self.controller.event_switches.keys():
            e_msg = 'Exception occurred trying to add Event Switch Start Condition {0} to program {1}. The switch ' \
                    'sensor address did not belong to any switch sensors on the controller.'.format(
                        str(_event_switch_address),     # {0}
                        str(self.ad),                   # {1}
                    )
            raise Exception(e_msg)

        # create program zone object
        create_switch_stop_condition_object(controller=self.controller, program_address=self.ad,
                                            event_switch_address=_event_switch_address)


    #################################
    def add_switch_pause_condition(self, _event_switch_address):
        """
        Add a switch pause condition on the program. \n

        :param _event_switch_address:    address of the event switch \n
        :type _event_switch_address:     int \n

        :return:
        :rtype:
        """
        from common.object_factory import create_switch_pause_condition_object

        # create start condition object
        if not isinstance(_event_switch_address, int):
            e_msg = "Exception occurred trying to add Event Switch Pause Condition {0} to the program {1}. The value " \
                    "of the switch sensor address must be an int, instead it was of type '{2}'.".format(
                        str(_event_switch_address),     # {0}
                        str(self.ad),                   # {1}
                        type(_event_switch_address)     # {2}
                    )
            raise Exception(e_msg)

        # loop through zone dict to see if zone object is present
        if _event_switch_address not in self.controller.event_switches.keys():
            e_msg = 'Exception occurred trying to add Event Switch Start Condition {0} to program {1}. The switch ' \
                    'sensor address did not belong to any switch sensors on the controller.'.format(
                        str(_event_switch_address),     # {0}
                        str(self.ad),                   # {1}
                    )
            raise Exception(e_msg)

        # create program zone object
        create_switch_pause_condition_object(controller=self.controller, program_address=self.ad,
                                             event_switch_address=_event_switch_address)

    #################################
    def set_program_to_start_like_controller_manual_start(self):
        """
        - Set a program to either start
        """
        # this will mimic the controller behavior if a program is running and you send it a manual start the
        # controller will stop the program

        self.get_data()

        if self.ss != opcodes.done_watering:
            self.set_program_to_stop()

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.DO,      # {0}
            opcodes.program,        # {1}
            self.ad,                # {2}
            opcodes.function,       # {3}
            opcodes.start_program,  # {4}
        )
        try:
            # Attempt to start a program
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to start program '{0}'".format(self.ad)
            raise Exception(e_msg)
        print("Successfully started the program '{0}'.".format(self.ad))

    #################################
    def set_program_to_start(self):
        """
        - Set a program to either start
        """

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.DO,      # {0}
            opcodes.program,        # {1}
            self.ad,                # {2}
            opcodes.function,       # {3}
            opcodes.start_program,  # {4}
        )
        try:
            # Attempt to start a program
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to start program '{0}'".format(self.ad)
            raise Exception(e_msg)
        print("Successfully started the program '{0}'.".format(self.ad))

        #################################

    def set_program_to_stop(self):
        """
        - Set a program to either stop
        """
        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.DO,      # {0}
            opcodes.program,        # {1}
            self.ad,                # {2}
            opcodes.function,       # {3}
            opcodes.stop_program,   # {4}
        )
        try:
            # Attempt to stop a program
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to stop program '{0}'".format(self.ad)
            raise Exception(e_msg)
        print("Successfully stoped the program '{0}'.".format(self.ad))
